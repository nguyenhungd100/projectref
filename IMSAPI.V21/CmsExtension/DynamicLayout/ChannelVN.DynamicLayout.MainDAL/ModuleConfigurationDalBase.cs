﻿using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DynamicLayout.MainDAL
{
    public abstract class ModuleConfigurationDalBase
    {
        #region Function GET
        public List<ModuleConfigurationEntity> GetByModuleInPageID(int _ModuleInPageID)
        {
            const string commandText = "CmsDL_ModuleConfiguration_GetByModuleInPageID";
            try
            {
                List<ModuleConfigurationEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleInPageID", _ModuleInPageID);
                data = _db.GetList<ModuleConfigurationEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET
        public bool Insert(ModuleConfigurationEntity moduleConfigurationEntity, ref int moduleConfigId)
        {
            const string commandText = "CMSDL_ModuleConfiguration_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", moduleConfigurationEntity.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ModuleInPageID", moduleConfigurationEntity.ModuleInPageId);
                _db.AddParameter(cmd, "ConfigName", moduleConfigurationEntity.ConfigName);
                _db.AddParameter(cmd, "ConfigValue", moduleConfigurationEntity.ConfigValue);
                var result = cmd.ExecuteNonQuery();
                moduleConfigId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return moduleConfigId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ModuleConfigurationEntity moduleConfigurationEntity) {
            const string commandText = "CMSDL_ModuleConfiguration_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", moduleConfigurationEntity.Id);
                _db.AddParameter(cmd, "ModuleInPageID", moduleConfigurationEntity.ModuleInPageId);
                _db.AddParameter(cmd, "ConfigName", moduleConfigurationEntity.ConfigName);
                _db.AddParameter(cmd, "ConfigValue", moduleConfigurationEntity.ConfigValue);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho ModuleConfiguration
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected ModuleConfigurationDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
