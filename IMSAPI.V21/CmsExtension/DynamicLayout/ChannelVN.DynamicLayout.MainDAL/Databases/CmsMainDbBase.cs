﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.DynamicLayout.MainDal.Databases;
using ChannelVN.DynamicLayout.MainDAL;

namespace ChannelVN.DynamicLayout.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Page
        private PageDal _pageContentMainDal;
        public PageDalBase PageContentMainDal
        {
            get { return _pageContentMainDal ?? (_pageContentMainDal = new PageDal((CmsMainDb)this)); }
        }
        #endregion

        #region Module Skin
        private ModuleSkinDal _moduleSkinMainDal;
        public ModuleSkinDalBase ModuleSkinMainDal
        {
            get { return _moduleSkinMainDal ?? (_moduleSkinMainDal = new ModuleSkinDal((CmsMainDb)this)); }
        }
        #endregion

        #region Module IN Page
        private ModuleInPageDal _moduleInPageMainDal;
        public ModuleInPageDalBase ModuleInPageMainDal
        {
            get { return _moduleInPageMainDal ?? (_moduleInPageMainDal = new ModuleInPageDal((CmsMainDb)this)); }
        }
        #endregion

        #region Module dal
        private ModuleDal _moduleMainDal;
        public ModuleDalBase ModuleMainDal
        {
            get { return _moduleMainDal ?? (_moduleMainDal = new ModuleDal((CmsMainDb)this)); }
        }
        #endregion

        #region Module Config
        private ModuleConfigurationDal _moduleConfigMainDal;
        public ModuleConfigurationDalBase ModuleConfigMainDal
        {
            get { return _moduleConfigMainDal ?? (_moduleConfigMainDal = new ModuleConfigurationDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}