﻿using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DynamicLayout.MainDAL
{
    public abstract class ModuleDalBase
    {
        #region function GET
        public List<ModuleEntity> GetAll() {
            const string commandText = "CmsDL_Module_GetAll";
            try
            {
                List<ModuleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<ModuleEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ModuleEntity GetByModuleID(int _moduleID) {
            const string commandText = "CmsDL_Module_GetByModuleID";
            try
            {
                ModuleEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleID", _moduleID); 
                data = _db.Get<ModuleEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ModuleDetailEntity GetDetailByModuleID(int _moduleID) {
            const string commandText = "CmsDL_Module_GetByModuleID";
            try
            {
                ModuleDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleID", _moduleID);
                data = _db.Get<ModuleDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region function SET
        public bool Insert(ModuleEntity module, ref int MofuleID) {
            const string commandText = "CMSDL_Module_Insert";
            try
            {
                List<ModuleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", module.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd,"ModuleName",module.ModuleName);
                _db.AddParameter(cmd, "ModuleConfigSrc", module.ModuleConfigSrc);
                _db.AddParameter(cmd,"ModuleImage",module.ModuleImage);
                _db.AddParameter(cmd,"Status",module.Status);
                var result = cmd.ExecuteNonQuery();
                MofuleID = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return MofuleID > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Module
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected ModuleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
