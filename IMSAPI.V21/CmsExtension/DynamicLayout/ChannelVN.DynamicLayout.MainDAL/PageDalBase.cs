﻿using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.DynamicLayout.MainDAL
{
    public abstract class PageDalBase
    {
        #region function GET
        public List<PageEntity> GetAll() {
            const string commandText = "CmsDL_Page_GetAll";
            try
            {
                List<PageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                //_db.AddParameter(cmd, "UserName", userName);
                //_db.AddParameter(cmd, "PageIndex", pageIndex);
                //_db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PageEntity>(cmd);
               // totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public  PageEntity GetPageById(int pageId) {
            const string commandText = "CmsDL_Page_GetByPageId";
            try
            {
                PageEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", pageId);
                data = _db.Get<PageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PageEntity GetPageByPageSrc(string pageSrc) {
            const string commandText = "CmsDL_Page_GetPageByPageSrc";
            try
            {
                PageEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageSrc",pageSrc);
                data = _db.Get<PageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Page
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected PageDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
