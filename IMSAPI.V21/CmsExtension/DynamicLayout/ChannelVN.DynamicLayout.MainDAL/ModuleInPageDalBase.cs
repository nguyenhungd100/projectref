﻿using ChannelVN.CMS.Common;
using ChannelVN.DynamicLayout.Entity;
using ChannelVN.DynamicLayout.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.DynamicLayout.MainDAL
{
    public abstract class ModuleInPageDalBase
    {
        #region function GET
        public List<ModuleInPageEntity> GetByPlaceHolderName(string _PlaceHolderName, int _PageID)
        {
            const string commandText = "CmsDL_ModuleInPage_GetByPlaceHolderName";
            try
            {
                List<ModuleInPageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaceHolderName", _PlaceHolderName);
                _db.AddParameter(cmd, "PageID", _PageID);
                data = _db.GetList<ModuleInPageEntity>(cmd);
                // totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ModuleInPageEntity GetByModuleInPageId(int moduleInPageId)
        {
            const string commandText = "CmsDL_ModuleInPage_GetById";
            try
            {
                ModuleInPageEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleInPageId", moduleInPageId);
                data = _db.Get<ModuleInPageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ModuleInPageEntity> GetByPageId(int pageId)
        {
            const string commandText = "CmsDL_ModuleInPage_GetByPageId";
            try
            {
                List<ModuleInPageEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageId", pageId);
                data = _db.GetList<ModuleInPageEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(ModuleInPageEntity moduleInPage, ref int ModuleInPageID)
        {
            const string commandText = "CMSDL_ModuleInPage_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", moduleInPage.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageID", moduleInPage.PageId);
                _db.AddParameter(cmd, "ModuleID", moduleInPage.ModuleId);
                _db.AddParameter(cmd, "PlaceHolderName", moduleInPage.PlaceHolderName);
                _db.AddParameter(cmd, "Priority", moduleInPage.Priority);
                _db.AddParameter(cmd, "SkinSrc", moduleInPage.SkinSrc);

                var result = cmd.ExecuteNonQuery();
                ModuleInPageID = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return ModuleInPageID > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(int ModuleInPageID)
        {
            const string commandText = "CMSDL_ModuleInPage_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleInPageID", ModuleInPageID);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateSkinSrc(int moduleInPageID, string skinSrc)
        {
            const string commandText = "CMSDL_ModuleInPage_UpdateSkinSrc";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ModuleInPageID", moduleInPageID);
                _db.AddParameter(cmd, "SkinSrc", skinSrc);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(string listOfPriority) {
            const string commandText = "CMSDL_ModuleInPage_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listOfPriority);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho ModuleInPage
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected ModuleInPageDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}