﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DynamicLayout.Entity
{
    [DataContract]
    public class ModuleConfigurationEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ModuleInPageId { get; set; }
        [DataMember]
        public string ConfigName { get; set; }
        [DataMember]
        public string ConfigValue { get; set; }
    }
}
