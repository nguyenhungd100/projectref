﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DynamicLayout.Entity
{
    [DataContract]
    public class ModuleSkinEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ModuleId { get; set; }
        [DataMember]
        public string SkinName { get; set; }
        [DataMember]
        public string SkinSrc { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
