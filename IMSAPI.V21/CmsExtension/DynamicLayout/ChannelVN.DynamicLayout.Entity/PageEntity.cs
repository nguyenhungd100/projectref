﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DynamicLayout.Entity
{
    [DataContract]
    public class PageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PageUrl { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PageSrc { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class PageWithModuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PageUrl { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PageSrc { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public List<ModuleInPageEntity> ListModuleInPage { get; set; }
    }
    [DataContract]
    public class PageWithFullModuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PageUrl { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string PageSrc { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public List<FullModuleInPageWithConfigEntity> ListModuleInPage { get; set; }
    }
}
