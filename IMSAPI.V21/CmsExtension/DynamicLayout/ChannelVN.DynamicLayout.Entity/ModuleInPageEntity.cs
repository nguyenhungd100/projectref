﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DynamicLayout.Entity
{
    [DataContract]
    public class ModuleInPageEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public int ModuleId { get; set; }
        [DataMember]
        public string PlaceHolderName { get; set; }
        [DataMember]
        public string SkinSrc { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }

    [DataContract]
    public class ModuleInPageWithConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public string PlaceHolderName { get; set; }
        [DataMember]
        public string SkinSrc { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public List<ModuleConfigurationEntity> Configurations { get; set; }
        [DataMember]
        public ModuleDetailEntity Module { get; set; }
    }

    [DataContract]
    public class FullModuleInPageWithConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public string PlaceHolderName { get; set; }
        [DataMember]
        public string SkinSrc { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public List<ModuleConfigurationEntity> Configurations { get; set; }
        [DataMember]
        public ModuleDetailEntity Module { get; set; }
    }
}
