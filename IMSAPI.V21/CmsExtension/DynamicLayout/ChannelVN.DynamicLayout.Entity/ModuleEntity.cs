﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.DynamicLayout.Entity
{
    [DataContract]
    public class ModuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ModuleName { get; set; }
        [DataMember]
        public string ModuleConfigSrc { get; set; }
        [DataMember]
        public string ModuleImage { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class ModuleDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ModuleName { get; set; }
        [DataMember]
        public string ModuleConfigSrc { get; set; }
        [DataMember]
        public string ModuleImage { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public List<ModuleSkinEntity> ModuleSkin { get; set; }
    }
}
