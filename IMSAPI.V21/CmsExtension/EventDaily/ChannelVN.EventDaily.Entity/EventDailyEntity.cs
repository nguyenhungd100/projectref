﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.EventDaily.Entity
{
    [DataContract]
    public class EventDailyEntity : EntityBase
    {
        [DataMember]
        public int EventDaily_Id { get; set; }
        [DataMember]
        public string EventDaily_Title { get; set; }
        [DataMember]
        public string EventDaily_Link { get; set; }
        [DataMember]
        public DateTime EventDaily_Date { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
    }
}
