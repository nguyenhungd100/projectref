﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.EventDaily.Dal;
using ChannelVN.EventDaily.Entity;
using ChannelVN.EventDaily.Entity.ErrorCode;

namespace ChannelVN.EventDaily.Bo
{
    public class EventDailyBo
    {
        public static ErrorMapping.ErrorCodes Insert(EventDailyEntity eventDailyEntity, ref int eventDailyId)
        {
            try
            {
                var inserted = EventDailyDal.Insert(eventDailyEntity, ref eventDailyId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static EventDailyEntity GetById(int id)
        {
            try
            {
                return EventDailyDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new EventDailyEntity();
            }
        }

        public static List<EventDailyEntity> Search(string keyword)
        {
            try
            {
                return EventDailyDal.Search(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<EventDailyEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes Update(EventDailyEntity eventDailyEntity)
        {
            try
            {
                var inserted = EventDailyDal.Update(eventDailyEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = EventDailyDal.Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
