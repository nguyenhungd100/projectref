﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.EventDaily.Dal.Common;
using ChannelVN.EventDaily.Entity;
using ChannelVN.EventDaily.MainDal.Databases;

namespace ChannelVN.EventDaily.Dal
{
    public class EventDailyDal
    {
        public static bool Insert(EventDailyEntity eventDailyEntity, ref int eventDailyId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EventContentMainDal.Insert(eventDailyEntity, ref eventDailyId);
            }
            return retVal;
        }

        public static EventDailyEntity GetById(int id)
        {
            EventDailyEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EventContentMainDal.GetById(id);
            }
            return retVal;
        }

        public static List<EventDailyEntity> Search(string keyword)
        {
            List<EventDailyEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.EventContentMainDal.Search(keyword);
            }
            return retVal;
        }

        public static bool Update(EventDailyEntity eventDailyEntity)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EventContentMainDal.Update(eventDailyEntity);
            }
            return retVal;
        }

        public static bool Delete(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.EventContentMainDal.Delete(id);
            }
            return retVal;
        }
    }
}
