﻿using ChannelVN.CMS.Common;
using ChannelVN.EventDaily.Entity;
using ChannelVN.EventDaily.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.EventDaily.MainDal
{
    public abstract class EventDailyDalBase
    {
        #region function GET
        public EventDailyEntity GetById(int id)
        {
            const string commandText = "CMS_EventDaily_GetById";
            try
            {
                EventDailyEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<EventDailyEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<EventDailyEntity> Search(string keyword)
        {
            const string commandText = "CMS_EventDaily_Search";
            try
            {
                List<EventDailyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "keyword", keyword);
                data = _db.GetList<EventDailyEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(EventDailyEntity eventDailyEntity, ref int eventDailyId)
        {
            const string commandText = "CMS_EventDaily_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", eventDailyId, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "EventDaily_Title", eventDailyEntity.EventDaily_Title);
                _db.AddParameter(cmd, "EventDaily_Link", eventDailyEntity.EventDaily_Link);
                _db.AddParameter(cmd, "EventDaily_Date", eventDailyEntity.EventDaily_Date);
                _db.AddParameter(cmd, "Description", eventDailyEntity.Description);
                _db.AddParameter(cmd, "Status", eventDailyEntity.Status);
                _db.AddParameter(cmd, "Created_Date", eventDailyEntity.CreatedDate);
                _db.AddParameter(cmd, "Modified_Date", eventDailyEntity.ModifiedDate);
                _db.AddParameter(cmd, "Created_By", eventDailyEntity.CreatedBy);
                var result = cmd.ExecuteNonQuery();
                eventDailyId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return eventDailyId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(EventDailyEntity eventDailyEntity) {
            const string commandText = "CMS_EventDaily_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", eventDailyEntity.EventDaily_Id);
                _db.AddParameter(cmd, "EventDaily_Title", eventDailyEntity.EventDaily_Title);
                _db.AddParameter(cmd, "EventDaily_Link", eventDailyEntity.EventDaily_Link);
                _db.AddParameter(cmd, "EventDaily_Date", eventDailyEntity.EventDaily_Date);
                _db.AddParameter(cmd, "Description", eventDailyEntity.Description);
                _db.AddParameter(cmd, "Status", eventDailyEntity.Status);
                _db.AddParameter(cmd, "Modified_Date", eventDailyEntity.ModifiedDate);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id) {
            const string commandText = "CMS_EventDaily_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Email Daily
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected EventDailyDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
