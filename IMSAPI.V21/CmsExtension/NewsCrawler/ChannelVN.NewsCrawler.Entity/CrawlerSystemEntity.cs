﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{
    [DataContract]
    public class CrawlerSystemEntity : EntityBase
    {
        [DataMember]
        public string SiteUrl { get; set; }
        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public string NewsDetailUrlRegex { get; set; }
    }
}
