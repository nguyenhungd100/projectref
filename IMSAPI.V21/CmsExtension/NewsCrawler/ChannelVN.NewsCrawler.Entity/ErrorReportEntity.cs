﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{    
    [DataContract]
    public class ErrorReportEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string Title { get; set; } 
        [DataMember]
        public string Detail { get; set; }       
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
    [DataContract]
    public class ErrorReportInListEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Detail { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }
}
