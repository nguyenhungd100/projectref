﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{
    [DataContract]
    public enum EnumSiteStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        UnActive = 2
    }

    [DataContract]
    public class SiteEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string RegexForNewsDetailUrl { get; set; }
    }

    [DataContract]
    public class SiteDetailEntity : EntityBase
    {
        [DataMember]
        public SiteEntity Site { get; set; }
        [DataMember]
        public List<ZoneEntity> Zones { set; get; }
    }

    [DataContract]
    public class SiteDetailInListEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string RegexForNewsDetailUrl { get; set; }
        [DataMember]
        public List<ZoneEntity> Zones { set; get; }
    }
    [DataContract]
    public class NewsDetailRegexEntity : EntityBase
    {
        [DataMember]
        public string SiteUrl { get; set; }
        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public string NewsDetailUrlRegex { get; set; }
        [DataMember]
        public string DefaultNewsDetailUrlRegex { get; set; }
    }
}
