﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{
    [DataContract]
    public enum EnumNewsStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        UnActive = 2
    }

    [DataContract]
    public class NewsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string Title { get; set; } 
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }        
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CrawledDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Tag { get; set; }
    }
    [DataContract]
    public class NewsInListEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string Title { get; set; } 
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }        
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CrawledDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }

    [DataContract]
    public class NewsCountEntity:EntityBase
    {
        [DataMember]
        public int SiteId { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int TotalRow { get; set; }
    }

    [DataContract]
    public class NewsLinkCafeFEntity : EntityBase {
        [DataMember]
        public int LinkId { get; set; }
        [DataMember]
        public string LinkTitle { get; set; }
        [DataMember]
        public string LinkStatus { get; set; }
        [DataMember]
        public string LinkSapo { get; set; }
        [DataMember]
        public string LinkUrl { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public string CategoryCode { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public long MyNo { get; set; }
    }
    [DataContract]
    public class NewsLinkDetailCafeFEntity : EntityBase
    {
        [DataMember]
        public int LinkId { get; set; }
        [DataMember]
        public int PageId { get; set; }
        [DataMember]
        public string LinkTitle { get; set; }
        [DataMember]
        public string LinkStatus { get; set; }
        [DataMember]
        public string LinkSapo { get; set; }
        [DataMember]
        public string LinkUrl { get; set; }
        [DataMember]
        public DateTime LinkDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public bool isDelete { get; set; }
        [DataMember]
        public string CategoryCode { get; set; }
        [DataMember]
        public int NewsID { get; set; }
        [DataMember]
        public string LinkAvatar { get; set; }
        [DataMember]
        public string ContentReg { get; set; }
        [DataMember]
        public string SapoReg { get; set; }
    }
}
