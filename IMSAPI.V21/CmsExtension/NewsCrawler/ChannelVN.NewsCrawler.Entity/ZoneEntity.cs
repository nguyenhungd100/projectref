﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{
    [DataContract]
    public enum EnumZoneStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        UnActive = 2
    }

    [DataContract]
    public class ZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public DateTime LastCrawledDate { get; set; }
        [DataMember]
        public bool IsProcessed { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsInMyFavorite { get; set; }
        [DataMember]
        public string RegexForNewsDetailUrl { get; set; }
    }
}
