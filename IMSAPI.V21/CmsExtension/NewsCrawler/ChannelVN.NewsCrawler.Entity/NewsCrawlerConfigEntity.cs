﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.Entity
{
   
    [DataContract]
    public class NewsCrawlerConfigEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public bool IsShowNotify { get; set; }
    }
    [DataContract]
    public class NewsCrawlerConfigDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public bool IsShowNotify { get; set; }
        [DataMember]
        public SiteEntity SiteInfo { get; set; }
        [DataMember]
        public ZoneEntity ZoneInfo { get; set; }
    }
    [DataContract]
    public class NewsCrawlerConfigForEditEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public bool IsShowNotify { get; set; }        
    }
}
