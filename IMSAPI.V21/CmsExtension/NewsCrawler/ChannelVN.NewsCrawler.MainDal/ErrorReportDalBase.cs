﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class ErrorReportDalBase
    {
        public bool Insert(ErrorReportEntity entity)
        {
            const string commandText = "CMS_ErrorReports_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", entity.ZoneId);
                _db.AddParameter(cmd, "SiteId", entity.SiteId);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Detail", entity.Detail);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "CreatedBy", entity.CreatedBy);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ErrorReportInListEntity> Search(int siteId, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ErrorReports_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow);
                var numberOfRow = _db.GetList<ErrorReportInListEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ErrorReportEntity GetById(int id)
        {
            const string commandText = "CMS_ErrorReports_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ErrorReportEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(int id)
        {
            const string commandText = "CMS_ErrorReports_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsNewsCrawlerDb _db;

        protected ErrorReportDalBase(CmsNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
