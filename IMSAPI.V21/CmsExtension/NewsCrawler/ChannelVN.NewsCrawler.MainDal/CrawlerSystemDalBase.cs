﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class CrawlerSystemDalBase
    {
        public List<CrawlerSystemEntity> GetCrawlerSystemByUrl(string url)
        {
            const string commandText = @"SELECT NA.news_angent_link AS SiteUrl, PC.link AS ZoneUrl, PC.regex AS NewsDetailUrlRegex 
                            FROM news_angent AS NA 
                                INNER JOIN prototype_category1 AS PC ON NA.news_agent_id = PC.news_agent_id 
                            WHERE LOWER(NA.news_angent_link) = @Url OR LOWER(PC.link) = @Url";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "Url", url);
                var numberOfRow = _db.GetList<CrawlerSystemEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsNewsCrawlerDb _db;

        protected CrawlerSystemDalBase(CmsNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
