﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class ZoneDalBase
    {
        public bool Update(ZoneEntity entity, ref int zoneId)
        {
            const string commandText = "CMS_Zones_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "RegexForNewsDetailUrl", entity.RegexForNewsDetailUrl);
                _db.AddParameter(cmd, "SiteId", entity.SiteId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                zoneId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatus(int zoneId, int status)
        {
            const string commandText = "CMS_Zones_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateRegexForNewsDetailUrl(int zoneId, string regexForNewsDetailUrl)
        {
            const string commandText = "CMS_Zones_UpdateRegexForNewsDetailUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneId);
                _db.AddParameter(cmd, "RegexForNewsDetailUrl", regexForNewsDetailUrl);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateLastCrawledDate(int zoneId, DateTime lastCrawledDate)
        {
            const string commandText = "CMS_Zones_UpdateLastCrawledDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneId);
                _db.AddParameter(cmd, "LastCrawledDate", lastCrawledDate);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneEntity> Search(int status, int siteId)
        {
            const string commandText = "CMS_Zones_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<ZoneEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ZoneEntity GetById(int id)
        {
            const string commandText = "CMS_Zones_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<ZoneEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneEntity> GetByListId(int siteId, string listId)
        {
            const string commandText = "CMS_Zones_GetByListId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ListId", listId);
                var numberOfRow = _db.GetList<ZoneEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int zoneId)
        {
            const string commandText = "CMS_Zones_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsNewsCrawlerDb _db;

        protected ZoneDalBase(CmsNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
