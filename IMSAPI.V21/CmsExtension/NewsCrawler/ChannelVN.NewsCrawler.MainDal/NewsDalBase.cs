﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class NewsDalBase
    {
        public bool Insert(NewsEntity entity)
        {
            const string commandText = "CMS_News_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", entity.ZoneId);
                _db.AddParameter(cmd, "SiteId", entity.SiteId);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "SubTitle", entity.SubTitle);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "CreatedDate", entity.CreatedDate);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "Tag", entity.Tag);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> Search(int status, int siteId, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var numberOfRow = _db.GetList<NewsInListEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsCountEntity> NewsCounter(string accountName, int channelId)
        {
            const string commandText = "CMS_News_Counter";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "ChannelId", channelId);
                var numberOfRow = _db.GetList<NewsCountEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetById(long id)
        {
            const string commandText = "CMS_News_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<NewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(NewsEntity entity)
        {
            const string commandText = "CMS_News_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateReadMonitor(string accountName, int siteId, int zoneId, int channelId)
        {
            const string commandText = "CMS_NewsReadMonitor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ChannelId", channelId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region For CafeBiz
        public List<NewsLinkCafeFEntity> SearchCafeBiz(string keyword, int pageIndex, int pageSize, ref int totalRow, int time = 0, string filter = "A", int siteId = -1, int categoryId = -1, string page = "", int typeId = 0)
        {
            const string commandText = "sp_CafeF_Crawler_SearchLinks";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "itemCount", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "categoryId", categoryId);
                _db.AddParameter(cmd, "site", siteId);
                _db.AddParameter(cmd, "page", page);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "filter", filter);
                _db.AddParameter(cmd, "typeId", typeId);
                _db.AddParameter(cmd, "time", time);
                _db.AddParameter(cmd, "pageNumber", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<NewsLinkCafeFEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsLinkDetailCafeFEntity GetByIdCafeBiz(long LinkId)
        {
            const string commandText = "sp_CafeF_Crawler_Get_by_LinkId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LinkId", LinkId);
                var numberOfRow = _db.Get<NewsLinkDetailCafeFEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatusCrawlerCafeBiz(int LinkId, string LinkStatus)
        {
            const string commandText = "sp_CafeF_CrawlerLink_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LinkId", LinkId);
                _db.AddParameter(cmd, "LinkStatus", LinkStatus);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsNewsCrawlerDb _db;

        protected NewsDalBase(CmsNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
