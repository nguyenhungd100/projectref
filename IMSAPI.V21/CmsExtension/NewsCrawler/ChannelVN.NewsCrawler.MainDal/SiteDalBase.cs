﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class SiteDalBase
    {
        public bool Update(SiteEntity entity, ref int siteId)
        {
            const string commandText = "CMS_Sites_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "Name", entity.Name);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "RegexForNewsDetailUrl", entity.RegexForNewsDetailUrl);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                siteId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int siteId, int status)
        {
            const string commandText = "CMS_Sites_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", siteId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateRegexForNewsDetailUrl(int siteId, string regexForNewsDetailUrl)
        {
            const string commandText = "CMS_Sites_UpdateRegexForNewsDetailUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", siteId);
                _db.AddParameter(cmd, "RegexForNewsDetailUrl", regexForNewsDetailUrl);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<SiteEntity> Search(int status)
        {
            const string commandText = "CMS_Sites_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.GetList<SiteEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public SiteEntity GetById(int id)
        {
            const string commandText = "CMS_Sites_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<SiteEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public SiteEntity GetByUrl(string url)
        {
            const string commandText = "CMS_Sites_GetByUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Url", url);
                var numberOfRow = _db.Get<SiteEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<SiteEntity> GetByListId(string listId)
        {
            const string commandText = "CMS_Sites_GetByListId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", listId);
                var numberOfRow = _db.GetList<SiteEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsDetailRegexEntity> GetCrawlerSystemByUrl(string url)
        {
            const string commandText = "CMS_Site_GetCrawlerSystemByUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Url", url);
                var numberOfRow = _db.GetList<NewsDetailRegexEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int siteId)
        {
            const string commandText = "CMS_Sites_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", siteId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsNewsCrawlerDb _db;

        protected SiteDalBase(CmsNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
