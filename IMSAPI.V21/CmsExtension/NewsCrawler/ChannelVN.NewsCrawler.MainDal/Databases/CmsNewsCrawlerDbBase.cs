﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.NewsCrawler.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsCrawlerDb"/> class that 
    /// represents a connection to the <c>CmsCrawlerDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsCrawlerDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsNewsCrawlerDbBase : MainDbBase
    {
        #region Store procedures
        #region Crawler System
        private CrawlerSystemDal _crawlerSystemMainDal;
        public CrawlerSystemDal CrawlerSystemMainDal
        {
            get { return _crawlerSystemMainDal ?? (_crawlerSystemMainDal = new CrawlerSystemDal((CmsNewsCrawlerDb)this)); }
        }
        #endregion

        #region Error Report
        private ErrorReportDal _errorReportMainDal;
        public ErrorReportDal ErrorReportMainDal
        {
            get { return _errorReportMainDal ?? (_errorReportMainDal = new ErrorReportDal((CmsNewsCrawlerDb)this)); }
        }
        #endregion

         #region Error Report
        private NewsDal _newsMainDal;
        public NewsDal NewsMainDal
        {
            get { return _newsMainDal ?? (_newsMainDal = new NewsDal((CmsNewsCrawlerDb)this)); }
        }
        #endregion

        #region Error Report
        private SiteDal _siteMainDal;
        public SiteDal SiteMainDal
        {
            get { return _siteMainDal ?? (_siteMainDal = new SiteDal((CmsNewsCrawlerDb)this)); }
        }
        #endregion

        #region Error Report
        private ZoneDal _zoneMainDal;
        public ZoneDal ZoneMainDal
        {
            get { return _zoneMainDal ?? (_zoneMainDal = new ZoneDal((CmsNewsCrawlerDb)this)); }
        }
        #endregion
        
        
        #endregion

        #region Constructors

        protected CmsNewsCrawlerDbBase()
        {
        }
        protected CmsNewsCrawlerDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}