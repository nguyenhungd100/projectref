﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.NewsCrawler.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.NewsCrawler.MainDal.Databases
{
    public class CmsNewsCrawlerDb : CmsNewsCrawlerDbBase
    {
        private const string ConnectionStringName = "CmsCrawlerDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}