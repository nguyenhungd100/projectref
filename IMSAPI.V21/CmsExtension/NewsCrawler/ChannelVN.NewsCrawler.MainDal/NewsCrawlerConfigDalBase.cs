﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public abstract class NewsCrawlerConfigDalBase
    {
        public bool Update(NewsCrawlerConfigEntity entity)
        {
            const string commandText = "CMS_NewsCrawlerConfig_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", entity.Id);
                _db.AddParameter(cmd, "UserName", entity.UserName);
                _db.AddParameter(cmd, "SiteId", entity.SiteId);
                _db.AddParameter(cmd, "SiteName", entity.SiteName);
                _db.AddParameter(cmd, "ZoneId", entity.ZoneId);
                _db.AddParameter(cmd, "ZoneName", entity.ZoneName);
                _db.AddParameter(cmd, "IsShowNotify", entity.IsShowNotify);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCrawlerConfigEntity> Search(string accountName, int siteId, int zoneId)
        {
            const string commandText = "CMS_NewsCrawlerConfig_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = _db.GetList<NewsCrawlerConfigEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_NewsCrawlerConfig_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteBySiteIdAndZoneId(string username, int siteId, int zoneId)
        {
            const string commandText = "CMS_NewsCrawlerConfig_DeleteBySiteIdAndZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteBySiteId(string username, int siteId)
        {
            const string commandText = "CMS_NewsCrawlerConfig_DeleteBySiteId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "SiteId", siteId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAllBySiteId(int siteId)
        {
            const string commandText = "CMS_NewsCrawlerConfig_DeleteAllBySiteId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SiteId", siteId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAllByZoneId(int siteId, int zoneId)
        {
            const string commandText = "CMS_NewsCrawlerConfig_DeleteAllByZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsCrawlerConfigDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
