﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.MainDal
{
    public class NewsCrawlerConfigDal : NewsCrawlerConfigDalBase
    {
        internal NewsCrawlerConfigDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
