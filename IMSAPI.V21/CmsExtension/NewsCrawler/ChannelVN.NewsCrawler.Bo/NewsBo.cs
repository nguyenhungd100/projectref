﻿using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using System;

namespace ChannelVN.NewsCrawler.BO
{
    public class NewsBo
    {
        public static List<NewsInListEntity> Search(int status, int siteId, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsDal.Search(status, siteId, zoneId, keyword, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsCountEntity> NewsCounter(string accountName, int channelId)
        {
            return NewsDal.NewsCounter(accountName, channelId);
        }
        public static ErrorMapping.ErrorCodes Insert(NewsEntity entity)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                entity.Url = entity.Url.ToLower();
                return NewsDal.Insert(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.Insert:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes Update(NewsEntity entity)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                entity.Url = entity.Url.ToLower();
                return NewsDal.Update(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.Update:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateReadMonitor(string accountName, int siteId, int zoneId, int channelId)
        {
            try
            {
                return NewsDal.UpdateReadMonitor(accountName, siteId, zoneId, channelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.UpdateReadMonitor:{0}", ex.Message));
            }
        }
        public static NewsEntity GetById(long id)
        {
            return NewsDal.GetById(id);
        }

        #region Cafebiz
        public static List<NewsLinkCafeFEntity> SearchCafeBiz(string keyword, int pageIndex, int pageSize, ref int totalRow, int time = 0, string filter = "A", int siteId = -1, int categoryId = -1, string page = "", int typeId = 0)
        {
            return NewsDal.SearchCafeBiz(keyword, pageIndex, pageSize, ref totalRow, time, filter, siteId, categoryId, page, typeId);
        }
        public static NewsLinkDetailCafeFEntity GetByIdCafeBiz(long LinkId)
        {
            try
            {
                return NewsDal.GetByIdCafeBiz(LinkId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return null;
        }
        public static ErrorMapping.ErrorCodes UpdateStatusCrawlerCafeBiz(int LinkId, string LinkStatus)
        {
            try
            {
                return NewsDal.UpdateStatusCrawlerCafeBiz(LinkId, LinkStatus)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.Insert:{0}", ex.Message));
            }
        }
        #endregion
    }
}
