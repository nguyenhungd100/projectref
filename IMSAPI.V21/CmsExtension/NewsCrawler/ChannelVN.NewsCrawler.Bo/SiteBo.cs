﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using System;

namespace ChannelVN.NewsCrawler.BO
{
    public class SiteBo
    {
        public static List<SiteEntity> Search(int status)
        {
            return SiteDal.Search(status);
        }
        public static ErrorMapping.ErrorCodes Update(SiteEntity entity, ref int siteId)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                entity.Url = entity.Url.ToLower();
                var domain = new Uri(entity.Url).GetLeftPart(UriPartial.Authority);
                entity.Url = domain;

                return SiteDal.Update(entity, ref siteId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SiteBo.Update:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateStatus(int siteId, int status)
        {
            try
            {
                return SiteDal.UpdateStatus(siteId, status)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SiteBo.UpdateStatus:{0}", ex.Message));
            }
        }
        public static SiteEntity GetById(int id)
        {
            return SiteDal.GetById(id);
        }

        public static SiteDetailEntity GetSiteDetailByUrl(string url)
        {
            url = url.ToLower();
            var domain = new Uri(url).GetLeftPart(UriPartial.Authority);

            var site = SiteDal.GetByUrl(domain);
            if (site != null)
            {
                var allZoneInSite = ZoneDal.Search(1, site.Id);
                var allMyConfig = NewsCrawlerConfigDal.Search(WcfExtensions.WcfMessageHeader.Current.ClientUsername, site.Id, -1);
                var count = allZoneInSite.Count;
                for (var i = 0; i < count; i++)
                {
                    var temp = allZoneInSite[i];
                    temp.IsInMyFavorite = allMyConfig.Exists(item => item.ZoneId == allZoneInSite[i].Id);
                    allZoneInSite[i] = temp;
                }
                return new SiteDetailEntity
                {
                    Site = site,
                    Zones = allZoneInSite
                };
            }
            return null;
        }

        public static ErrorMapping.ErrorCodes DeleteSiteBySiteId(int siteId)
        {
            try
            {
                var crawlerConfig = NewsCrawlerConfigDal.Search("", siteId, -1);
                if (crawlerConfig.Count > 0)
                {
                    return ErrorMapping.ErrorCodes.CannotDeleteBecauseSiteInUsed;
                }

                var success = SiteDal.DeleteById(siteId);
                if (success)
                {
                    NewsCrawlerConfigDal.DeleteAllBySiteId(siteId);
                }
                return success ? ErrorMapping.ErrorCodes.Success
                 : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SiteBo.UpdateStatus:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateSiteDetail(SiteDetailEntity siteDetail)
        {
            try
            {
                var site = siteDetail.Site;
                var zones = siteDetail.Zones;
                var siteId = site.Id;

                //var crawlerSystemEntity = CrawlerSystemDal.GetCrawlerSystemByUrl(site.Url);
                //var firstOrDefault = crawlerSystemEntity.FirstOrDefault(c => c.NewsDetailUrlRegex != "");
                //if (firstOrDefault != null)
                //{
                //    site.RegexForNewsDetailUrl = firstOrDefault.NewsDetailUrlRegex;
                //}
                if (SiteDal.Update(site, ref siteId))
                {
                    if (site.Id <= 0) site.Id = siteId;
                    // Delete excluded zone
                    var currentZoneInSite = ZoneDal.Search(-1, site.Id);
                    foreach (var zone in currentZoneInSite)
                    {
                        if (!zones.Exists(item => item.Id == zone.Id))
                        {
                            ZoneDal.DeleteById(zone.Id);
                        }
                    }

                    var zoneId = 0;
                    foreach (var zone in zones)
                    {
                        //zone.RegexForNewsDetailUrl = GetRegexByZoneUrl(zone.Url);
                        zone.SiteId = siteId;
                        ZoneDal.Update(zone, ref zoneId);
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static string GetRegexByZoneUrl(string url)
        {
            var regexUrl = "";
            var crawlerSystemEntity = CrawlerSystemDal.GetCrawlerSystemByUrl(url);
            var firstOrDefault = crawlerSystemEntity.FirstOrDefault(c => c.NewsDetailUrlRegex != "");
            if (firstOrDefault != null)
            {
                regexUrl = firstOrDefault.NewsDetailUrlRegex;
            }
            return regexUrl;
        }

        public static List<NewsDetailRegexEntity> GetCrawlerSystemByUrl(string url)
        {
            return SiteDal.GetCrawlerSystemByUrl(url);
        }

    }
}
