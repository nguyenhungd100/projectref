﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;

namespace ChannelVN.NewsCrawler.Bo
{
    public class CrawlerSystemBo
    {
        public static List<CrawlerSystemEntity> GetCrawlerSystemByUrl(string url)
        {
            return CrawlerSystemDal.GetCrawlerSystemByUrl(url);
        }
    }
}
