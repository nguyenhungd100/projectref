﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using System;
using ChannelVN.CMS.Common;

namespace ChannelVN.NewsCrawler.BO
{
    public class NewsCrawlerConfigBo
    {
        public static List<NewsCrawlerConfigDetailEntity> Search(string accountName, int siteId, int zoneId)
        {
            var data = SearchConfig(accountName, siteId, zoneId);


            List<NewsCrawlerConfigDetailEntity> listConfigs = new List<NewsCrawlerConfigDetailEntity>();
            if (data != null && data.Count > 0)
            {
                foreach (var entity in data)
                {
                    var detail = new NewsCrawlerConfigDetailEntity
                        {
                            Id = entity.Id,
                            IsShowNotify = entity.IsShowNotify,
                            SiteId = entity.SiteId,
                            UserName = entity.UserName,
                            ZoneId = entity.ZoneId
                        };
                    detail.SiteInfo = SiteBo.GetById(entity.SiteId);
                    detail.ZoneInfo = ZoneBo.GetById(entity.ZoneId);
                    listConfigs.Add(detail);
                }
            }
            return listConfigs;
        }
        public static List<NewsCrawlerConfigEntity> SearchConfig(string accountName, int siteId, int zoneId)
        {
            var data = NewsCrawlerConfigDal.Search(accountName, siteId, zoneId);

            return data;
        }
        public static ErrorMapping.ErrorCodes AddSite(SiteEntity site, ref int siteId)
        {
            try
            {
                site.Url = site.Url.ToLower();
                var domain = new Uri(site.Url).GetLeftPart(UriPartial.Authority);
                site.Url = domain;

                var data = SiteBo.Update(site, ref siteId);
                if (siteId > 0)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsCrawlerConfigBo.AddSite:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes AddZone(NewsCrawlerConfigForEditEntity config, string accountName)
        {
            try
            {
                int zoneId = 0;
                config.ZoneUrl = config.ZoneUrl.ToLower();

                var data = ZoneBo.Update(new ZoneEntity
                                             {
                                                 Id = 0,
                                                 IsProcessed = true,
                                                 LastCrawledDate = DateTime.Now,
                                                 Name = config.ZoneName,
                                                 SiteId = config.SiteId,
                                                 Status = 1,
                                                 Url = config.ZoneUrl
                                             }, ref zoneId);
                if (zoneId > 0)
                {
                    return NewsCrawlerConfigDal.Update(new NewsCrawlerConfigEntity
                                                           {
                                                               Id = 0,
                                                               SiteId = config.SiteId,
                                                               ZoneId = zoneId,
                                                               IsShowNotify = config.IsShowNotify,
                                                               UserName = accountName
                                                           })
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsCrawlerConfigBo.AddZone:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes Update(NewsCrawlerConfigEntity entity)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return NewsCrawlerConfigDal.Update(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsCrawlerConfigBo.Update:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsCrawlerConfig(SiteDetailEntity siteDetail)
        {
            try
            {
                var currentUsername = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                #region Get site info
                var siteId = 0;
                var siteName = siteDetail.Site.Name;
                var siteUrl = siteDetail.Site.Url.ToLower();
                var siteDomain = new Uri(siteUrl).GetLeftPart(UriPartial.Authority);

                var existsSite = SiteDal.GetByUrl(siteDomain);
                if (existsSite != null)
                {
                    siteId = existsSite.Id;
                    if (string.IsNullOrEmpty(siteName)) siteName = existsSite.Name;
                    if (string.IsNullOrEmpty(existsSite.RegexForNewsDetailUrl))
                        SiteDal.UpdateRegexForNewsDetailUrl(siteId, siteDetail.Site.RegexForNewsDetailUrl);
                }
                else
                {
                    var newSite = new SiteEntity
                    {
                        Name = siteName,
                        Url = siteDomain,
                        RegexForNewsDetailUrl = siteDetail.Site.RegexForNewsDetailUrl
                    };
                    //Check Regex
                    var crawlerSystemEntity = CrawlerSystemDal.GetCrawlerSystemByUrl(siteDomain);
                    var firstOrDefault = crawlerSystemEntity.FirstOrDefault(c => c.NewsDetailUrlRegex != "");
                    if (firstOrDefault != null)
                    {
                        newSite.RegexForNewsDetailUrl = firstOrDefault.NewsDetailUrlRegex;
                    }
                    if (!SiteDal.Update(newSite, ref siteId))
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
                #endregion

                var listUpdateNewsCrawlerConfig = new List<NewsCrawlerConfigEntity>();

                var allZoneInSite = ZoneDal.Search(1, siteId);
                var listUpdateZones = siteDetail.Zones;
                if (listUpdateZones.Count == 0)
                {
                    var updateConfig = new NewsCrawlerConfigEntity
                    {
                        UserName = currentUsername,
                        SiteId = siteId,
                        SiteName = siteName,
                        ZoneId = 0,
                        ZoneName = "",
                        IsShowNotify = true
                    };
                    NewsCrawlerConfigDal.Update(updateConfig);
                    listUpdateNewsCrawlerConfig.Add(updateConfig);
                }
                else
                {
                    foreach (var currentZone in listUpdateZones)
                    {
                        #region get zone info

                        var zoneId = 0;
                        var zoneName = currentZone.Name;
                        var zoneUrl = currentZone.Url.ToLower();

                        var existsZone = allZoneInSite.Find(item => item.Url == zoneUrl);
                        if (existsZone != null)
                        {
                            zoneId = existsZone.Id;
                            if (string.IsNullOrEmpty(existsZone.RegexForNewsDetailUrl))
                                ZoneDal.UpdateRegexForNewsDetailUrl(zoneId, currentZone.RegexForNewsDetailUrl);
                        }
                        else
                        {
                            var newZone = new ZoneEntity
                                {
                                    Name = zoneName,
                                    SiteId = siteId,
                                    Url = zoneUrl,
                                    Status = 1,
                                    IsProcessed = false,
                                    LastCrawledDate = DateTime.Now,
                                    RegexForNewsDetailUrl = currentZone.RegexForNewsDetailUrl
                                };

                            newZone.RegexForNewsDetailUrl = SiteBo.GetRegexByZoneUrl(zoneUrl);
                            ZoneDal.Update(newZone, ref zoneId);
                        }

                        #endregion

                        var updateConfig = new NewsCrawlerConfigEntity
                            {
                                UserName = currentUsername,
                                SiteId = siteId,
                                SiteName =siteName,
                                ZoneId = zoneId,
                                ZoneName = zoneName,
                                IsShowNotify = true
                            };
                        NewsCrawlerConfigDal.Update(updateConfig);
                        listUpdateNewsCrawlerConfig.Add(updateConfig);
                    }
                }

                var allCurrentNewsCrawlerConfig = NewsCrawlerConfigDal.Search(currentUsername, siteId, -1);
                foreach (var currentConfig in allCurrentNewsCrawlerConfig)
                {
                    if (!listUpdateNewsCrawlerConfig.Exists(item => item.SiteId == currentConfig.SiteId && item.ZoneId == currentConfig.ZoneId))
                    {
                        NewsCrawlerConfigDal.Delete(currentConfig.Id);
                    }
                }

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteNewsCrawlerConfig(int id)
        {
            try
            {
                if (NewsCrawlerConfigDal.Delete(id))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteNewsCrawlerConfigBySiteIdAndZoneId(int siteId, int zoneId)
        {
            try
            {
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (NewsCrawlerConfigDal.DeleteBySiteIdAndZoneId(username, siteId, zoneId))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteNewsCrawlerConfigBySiteId(int siteId)
        {
            try
            {
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (NewsCrawlerConfigDal.DeleteBySiteId(username, siteId))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static List<SiteDetailInListEntity> GetAllSiteDetailInMyFavorite()
        {
            var listData = SearchConfig(WcfExtensions.WcfMessageHeader.Current.ClientUsername, -1, -1);
            listData.Sort((item1, item2) => item1.SiteId.CompareTo(item2.SiteId));
            var currentSiteId = 0;
            var listSiteIds = "";
            var listZoneIds = "";
            foreach (var data in listData)
            {
                if (currentSiteId != data.SiteId)
                {
                    currentSiteId = data.SiteId;
                    listSiteIds += ";" + currentSiteId;
                }
                listZoneIds += ";" + data.ZoneId;
            }
            if (!string.IsNullOrEmpty(listSiteIds)) listSiteIds = listSiteIds.Remove(0, 1);
            if (!string.IsNullOrEmpty(listZoneIds)) listZoneIds = listZoneIds.Remove(0, 1);

            var allSiteInMyFavorite = SiteDal.GetByListId(listSiteIds);
            var allZoneInMyFavorite = ZoneDal.GetByListId(-1, listZoneIds);

            var allSiteDetailInMyFavorite = new List<SiteDetailInListEntity>();
            var siteCount = allSiteInMyFavorite.Count;
            for (var i = 0; i < siteCount; i++)
            {
                var site = allSiteInMyFavorite[i];
                var listZone = new List<ZoneEntity>();
                foreach (var zone in allZoneInMyFavorite)
                {
                    if (zone.SiteId == site.Id)
                    {
                        listZone.Add(zone);
                    }
                }
                allSiteDetailInMyFavorite.Add(new SiteDetailInListEntity
                {
                    Id = site.Id,
                    Name = site.Name,
                    Url = site.Url,
                    Status = site.Status,
                    Zones = listZone,
                    RegexForNewsDetailUrl = site.RegexForNewsDetailUrl
                });
            }
            return allSiteDetailInMyFavorite;
        }
    }
}
