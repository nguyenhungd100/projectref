﻿using System.Collections.Generic;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using System;

namespace ChannelVN.NewsCrawler.BO
{
    public class ErrorReportBo
    {
        public static List<ErrorReportInListEntity> Search(int siteId, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ErrorReportDal.Search(siteId, zoneId, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes Insert(ErrorReportEntity entity)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                entity.Url = entity.Url.ToLower();
                return ErrorReportDal.Insert(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ErrorReportBo.Insert:{0}", ex.Message));
            }
        }        
        public static ErrorReportEntity GetById(int id)
        {
            return ErrorReportDal.GetById(id);
        }
    }
}
