﻿using System.Collections.Generic;
using ChannelVN.NewsCrawler.Dal;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.Entity.ErrorCode;
using System;

namespace ChannelVN.NewsCrawler.BO
{
    public class ZoneBo
    {
        public static List<ZoneEntity> Search(int status, int siteId)
        {
            return ZoneDal.Search(status, siteId);
        }
        public static ErrorMapping.ErrorCodes Update(ZoneEntity entity, ref int zoneId)
        {
            try
            {
                if (null == entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                entity.Url = entity.Url.ToLower();

                return ZoneDal.Update(entity, ref zoneId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneBo.Update:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateStatus(int zoneId, int status)
        {
            try
            {
                return ZoneDal.UpdateStatus(zoneId, status)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneBo.UpdateStatus:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateLastCrawledDate(int zoneId, DateTime lastCrawledDate)
        {
            try
            {
                return ZoneDal.UpdateLastCrawledDate(zoneId, lastCrawledDate)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneBo.UpdateLastCrawledDate:{0}", ex.Message));
            }
        }
        public static ZoneEntity GetById(int id)
        {
            return ZoneDal.GetById(id);
        }
    }
}
