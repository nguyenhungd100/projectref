﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class ErrorReportDal
    {
        public static bool Insert(ErrorReportEntity entity)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ErrorReportMainDal.Insert(entity);
            }
            return returnValue;
        }
        public static List<ErrorReportInListEntity> Search(int siteId, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ErrorReportInListEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ErrorReportMainDal.Search(siteId, zoneId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static ErrorReportEntity GetById(int id)
        {
            ErrorReportEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ErrorReportMainDal.GetById(id);
            }
            return returnValue;
        }
        public static bool DeleteById(int id)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ErrorReportMainDal.DeleteById(id);
            }
            return returnValue;
        }
    }
}
