﻿using System;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;

namespace ChannelVN.NewsCrawler.Dal.Common
{
    public class DbCommon
    {
        private const string _CONNECTION_DECRYPT_KEY = "nfvsMof35XnUdQEWuxgAZta";

        private static string GetConnectionName(Connection connection)
        {
            switch (connection)
            {
                case Connection.CmsMainDb:
                    return "CmsMainDb";
                case Connection.CmsNewsCrawlerDb:
                    return "CmsNewsCrawlerDb";
                case Connection.CmsSystemCrawlerDb:
                    return "CmsSystemCrawlerDb";
                default:
                    return "";
            }
        }

        public enum Connection
        {
            CmsMainDb = 4,
            CmsNewsCrawlerDb = 9,
            CmsSystemCrawlerDb = 10
        }
        public static DateTime MinDateTime = new DateTime(1980, 1, 1);
        public static string DatabaseSchema = "[dbo].";
        public static string GetConnectionString(Connection connection)
        {
            return ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace, GetConnectionName(connection),
                                                            _CONNECTION_DECRYPT_KEY);
        }
        public static bool IsUseMainDal
        {
            get
            {
                return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "IsUseMainDal"));
            }
        }
    }
}
