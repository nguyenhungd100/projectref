﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class ZoneDal
    {
        public static bool Update(ZoneEntity entity, ref int zoneId)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.Update(entity, ref zoneId);
            }
            return returnValue;
        }
        public static bool UpdateStatus(int zoneId, int status)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.UpdateStatus(zoneId, status);
            }
            return returnValue;
        }
        public static bool UpdateRegexForNewsDetailUrl(int zoneId, string regexForNewsDetailUrl)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.UpdateRegexForNewsDetailUrl(zoneId, regexForNewsDetailUrl);
            }
            return returnValue;
        }
        public static bool UpdateLastCrawledDate(int zoneId, DateTime lastCrawledDate)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.UpdateLastCrawledDate(zoneId, lastCrawledDate);
            }
            return returnValue;
        }
        public static List<ZoneEntity> Search(int status, int siteId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.Search(status, siteId);
            }
            return returnValue;
        }
        public static ZoneEntity GetById(int id)
        {
            ZoneEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetByListId(int siteId, string listId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.GetByListId(siteId, listId);
            }
            return returnValue;
        }

        public static bool DeleteById(int zoneId)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.ZoneMainDal.DeleteById(zoneId);
            }
            return returnValue;
        }
    }
}
