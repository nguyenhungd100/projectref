﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class SiteDal
    {
        public static bool Update(SiteEntity entity, ref int siteId)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.Update(entity, ref siteId);
            }
            return returnValue;
        }
        public static bool UpdateStatus(int siteId, int status)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.UpdateStatus(siteId, status);
            }
            return returnValue;
        }
        public static bool UpdateRegexForNewsDetailUrl(int siteId, string regexForNewsDetailUrl)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.UpdateRegexForNewsDetailUrl(siteId, regexForNewsDetailUrl);
            }
            return returnValue;
        }
        public static List<SiteEntity> Search(int status)
        {
            List<SiteEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.Search(status);
            }
            return returnValue;
        }
        public static SiteEntity GetById(int id)
        {
            SiteEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.GetById(id);
            }
            return returnValue;
        }

        public static SiteEntity GetByUrl(string url)
        {
            SiteEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.GetByUrl(url);
            }
            return returnValue;
        }
        public static List<SiteEntity> GetByListId(string listId)
        {
            List<SiteEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.GetByListId(listId);
            }
            return returnValue;
        }
        public static List<NewsDetailRegexEntity> GetCrawlerSystemByUrl(string url)
        {
            List<NewsDetailRegexEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.GetCrawlerSystemByUrl(url);
            }
            return returnValue;
        }

        public static bool DeleteById(int siteId)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.SiteMainDal.DeleteById(siteId);
            }
            return returnValue;
        }
    }
}
