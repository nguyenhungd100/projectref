﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class NewsDal
    {
        public static bool Insert(NewsEntity entity)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.Insert(entity);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> Search(int status, int siteId, int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.Search(status, siteId, zoneId, keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsCountEntity> NewsCounter(string accountName, int channelId)
        {
            List<NewsCountEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.NewsCounter(accountName, channelId);
            }
            return returnValue;
        }

        public static NewsEntity GetById(long id)
        {
            NewsEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Update(NewsEntity entity)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.Update(entity);
            }
            return returnValue;
        }
        public static bool UpdateReadMonitor(string accountName, int siteId, int zoneId, int channelId)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.UpdateReadMonitor(accountName, siteId, zoneId, channelId);
            }
            return returnValue;
        }

        #region For CafeBiz
        public static List<NewsLinkCafeFEntity> SearchCafeBiz(string keyword, int pageIndex, int pageSize, ref int totalRow, int time = 0, string filter = "A", int siteId = -1, int categoryId = -1, string page = "", int typeId = 0)
        {
            List<NewsLinkCafeFEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.SearchCafeBiz(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static NewsLinkDetailCafeFEntity GetByIdCafeBiz(long LinkId)
        {
            NewsLinkDetailCafeFEntity returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.GetByIdCafeBiz(LinkId);
            }
            return returnValue;
        }

        public static bool UpdateStatusCrawlerCafeBiz(int LinkId, string LinkStatus)
        {
            bool returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.NewsMainDal.UpdateStatusCrawlerCafeBiz(LinkId, LinkStatus);
            }
            return returnValue;
        }
        #endregion
    }
}
