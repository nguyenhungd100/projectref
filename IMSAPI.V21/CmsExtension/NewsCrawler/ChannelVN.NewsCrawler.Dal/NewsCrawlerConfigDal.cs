﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class NewsCrawlerConfigDal
    {
        public static bool Update(NewsCrawlerConfigEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.Update(entity);
            }
            return returnValue;
        }

        public static List<NewsCrawlerConfigEntity> Search(string accountName, int siteId, int zoneId)
        {
            List<NewsCrawlerConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.Search(accountName, siteId, zoneId);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.Delete(id);
            }
            return returnValue;
        }

        public static bool DeleteBySiteIdAndZoneId(string username, int siteId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.DeleteBySiteIdAndZoneId(username, siteId, zoneId);
            }
            return returnValue;
        }

        public static bool DeleteBySiteId(string username, int siteId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.DeleteBySiteId(username, siteId);
            }
            return returnValue;
        }
        public static bool DeleteAllBySiteId(int siteId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.DeleteAllBySiteId(siteId);
            }
            return returnValue;
        }
        public static bool DeleteAllByZoneId(int siteId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCrawlerConfigMainDal.DeleteAllByZoneId(siteId, zoneId);
            }
            return returnValue;
        }
    }
}
