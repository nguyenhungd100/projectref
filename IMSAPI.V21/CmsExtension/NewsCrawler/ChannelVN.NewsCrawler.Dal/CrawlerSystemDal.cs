﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.NewsCrawler.Dal.Common;
using ChannelVN.NewsCrawler.Entity;
using ChannelVN.NewsCrawler.MainDal.Databases;

namespace ChannelVN.NewsCrawler.Dal
{
    public class CrawlerSystemDal
    {
        //
        public static List<CrawlerSystemEntity> GetCrawlerSystemByUrl(string url)
        {
            List<CrawlerSystemEntity> returnValue;
            using (var db = new CmsNewsCrawlerDb())
            {
                returnValue = db.CrawlerSystemMainDal.GetCrawlerSystemByUrl(url);
            }
            return returnValue;
        }
    }
}
