﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.DAL.Common;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.DAL.SEOMetaTags
{
    public class SEOMetaTagsDal
    {
        public static bool Insert(SEOMetaTagsEntity seoMetaTagsEntity, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTagsMainDal.Insert(seoMetaTagsEntity, ref tagId);
            }
            return returnValue;
        }

        public static bool Update(SEOMetaTagsEntity seoMetaTagsEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTagsMainDal.Update(seoMetaTagsEntity);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTagsMainDal.Delete(id);
            }
            return returnValue;
        }
        public static SEOMetaTagsEntity GetSeoMetaTagsById(long id)
        {
            SEOMetaTagsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTagsMainDal.GetSeoMetaTagsById(id);
            }
            return returnValue;
        }

        public static List<SEOMetaTagsEntity> GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<SEOMetaTagsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTagsMainDal.GetListSeoMetaTags(keyWord, zoneId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

    }
}
