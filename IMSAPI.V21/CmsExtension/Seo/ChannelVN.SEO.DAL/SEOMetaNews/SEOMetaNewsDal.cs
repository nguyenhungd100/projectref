﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.DAL.Common;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.DAL.SEOMetaNews
{
    public class SEOMetaNewsDal
    {
        public static bool Insert(SEOMetaNewsEntity seoMetaTagsEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaNewsMainDal.Insert(seoMetaTagsEntity);
            }
            return returnValue;
        }

        public static bool Update(SEOMetaNewsEntity seoMetaTagsEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaNewsMainDal.Update(seoMetaTagsEntity);
            }
            return returnValue;
        }

        public static SEOMetaNewsEntity GetSeoMetaNewsById(long id) 
        {
            SEOMetaNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaNewsMainDal.GetSeoMetaNewsById(id);
            }
            return returnValue;
        }
    }
}
