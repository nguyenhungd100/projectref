﻿using System.Collections.Generic;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.DAL.SEOMetaTopic
{
    public class SEOMetaTopicDal
    {
        public static bool Insert(SEOMetaTopicEntity seoMetaTopicEntity, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTopicMainDal.Insert(seoMetaTopicEntity, ref tagId);
            }
            return returnValue;
        }

        public static bool Update(SEOMetaTopicEntity seoMetaTopicEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTopicMainDal.Update(seoMetaTopicEntity);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTopicMainDal.Delete(id);
            }
            return returnValue;
        }
        
        public static SEOMetaTopicEntity GetSeoMetaTopicByTopicId(int topicId)
        {
            SEOMetaTopicEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTopicMainDal.GetSeoMetaTopicByTopicId(topicId);
            }
            return returnValue;
        }

        public static List<SEOMetaTopicEntity> GetListSeoMetaTopic(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<SEOMetaTopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaTopicMainDal.GetListSeoMetaTopic(keyWord, zoneId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

    }
}
