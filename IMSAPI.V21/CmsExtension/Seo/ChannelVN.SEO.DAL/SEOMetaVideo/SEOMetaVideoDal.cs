﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.DAL.Common;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.DAL.SEOMetaVideo
{
    public class SEOMetaVideoDal
    {
        public static bool Insert(SEOMetaVideoEntity seoMetaTagsEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaVideoMainDal.Insert(seoMetaTagsEntity);
            }
            return returnValue;
        }

        public static bool Update(SEOMetaVideoEntity seoMetaTagsEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaVideoMainDal.Update(seoMetaTagsEntity);
            }
            return returnValue;
        }

        public static SEOMetaVideoEntity GetSeoMetaVideoById(int id) 
        {
            SEOMetaVideoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SEOMetaVideoMainDal.GetSeoMetaVideoById(id);
            }
            return returnValue;
        }

    }
}
