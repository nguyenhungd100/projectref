﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.MainDAL.SEOMetaNews
{
    public abstract class SEOMetaNewsDalBase
    {
        public bool Insert(SEOMetaNewsEntity seoMetaTagsEntity)
        {
            const string commandText = "CMS_SEOMetaNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", seoMetaTagsEntity.NewsId);
                _db.AddParameter(cmd, "MetaTitle", seoMetaTagsEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "MetaNewsKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "KeywordFocus", seoMetaTagsEntity.KeywordFocus);
                _db.AddParameter(cmd, "CreatedBy", seoMetaTagsEntity.CreatedBy);
                _db.AddParameter(cmd, "SocialTitle", seoMetaTagsEntity.SocialTitle);
                _db.AddParameter(cmd, "KeywordSub", seoMetaTagsEntity.KeywordSub);

                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(SEOMetaNewsEntity seoMetaTagsEntity)
        {
            const string commandText = "CMS_SEOMetaNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", seoMetaTagsEntity.NewsId);
                _db.AddParameter(cmd, "MetaTitle", seoMetaTagsEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "MetaNewsKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "KeywordFocus", seoMetaTagsEntity.KeywordFocus);
                _db.AddParameter(cmd, "CreatedBy", seoMetaTagsEntity.CreatedBy);
                _db.AddParameter(cmd, "SocialTitle", seoMetaTagsEntity.SocialTitle);
                _db.AddParameter(cmd, "KeywordSub", seoMetaTagsEntity.KeywordSub);

                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public SEOMetaNewsEntity GetSeoMetaNewsById(long id) 
        {
            const string commandText = "CMS_SEOMetaNews_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                var numberOfRow = _db.Get<SEOMetaNewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected SEOMetaNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
