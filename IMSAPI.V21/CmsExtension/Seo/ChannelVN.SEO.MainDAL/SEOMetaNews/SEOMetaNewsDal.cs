﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.MainDAL.SEOMetaNews
{
    public class SEOMetaNewsDal : SEOMetaNewsDalBase
    {
        internal SEOMetaNewsDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
