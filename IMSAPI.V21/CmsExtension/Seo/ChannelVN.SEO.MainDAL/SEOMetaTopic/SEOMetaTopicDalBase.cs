﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;


namespace ChannelVN.SEO.MainDAL.SEOMetaTopic
{
    public abstract class SEOMetaTopicDalBase
    {
        public bool Insert(SEOMetaTopicEntity seoMetaTopicEntity, ref int tagId)
        {
            const string commandText = "CMS_SEOMetaTopic_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "MetaTitle", seoMetaTopicEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTopicEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTopicEntity.MetaDescription);
                _db.AddParameter(cmd, "TopicId", seoMetaTopicEntity.TopicId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(SEOMetaTopicEntity seoMetaTopicEntity)
        {
            const string commandText = "CMS_SEOMetaTopic_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "MetaTitle", seoMetaTopicEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTopicEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTopicEntity.MetaDescription);
                _db.AddParameter(cmd, "TopicId", seoMetaTopicEntity.TopicId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_SEOMetaTopic_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        public SEOMetaTopicEntity GetSeoMetaTopicByTopicId(int topicId)
        {
            const string commandText = "CMS_SEOMetaTopic_GetByTopicId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", topicId);
                var numberOfRow = _db.Get<SEOMetaTopicEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<SEOMetaTopicEntity> GetListSeoMetaTopic(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_SEOMetaTopic_GetList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyWord);
                _db.AddParameter(cmd, "TopicId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);                
                var numberOfRow = _db.GetList<SEOMetaTopicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected SEOMetaTopicDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
