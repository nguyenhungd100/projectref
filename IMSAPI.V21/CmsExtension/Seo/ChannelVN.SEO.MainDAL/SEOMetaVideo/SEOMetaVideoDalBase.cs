﻿using System;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;

namespace ChannelVN.SEO.MainDAL.SEOMetaVideo
{
    public abstract class SEOMetaVideoDalBase
    {
        public bool Insert(SEOMetaVideoEntity seoMetaTagsEntity)
        {
            const string commandText = "CMS_SEOMetaVideo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", seoMetaTagsEntity.VideoId);
                _db.AddParameter(cmd, "MetaTitle", seoMetaTagsEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "MetaVideoKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "KeywordFocus", seoMetaTagsEntity.KeywordFocus);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(SEOMetaVideoEntity seoMetaTagsEntity)
        {
            const string commandText = "CMS_SEOMetaVideo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", seoMetaTagsEntity.VideoId);
                _db.AddParameter(cmd, "MetaTitle", seoMetaTagsEntity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "MetaVideoKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "KeywordFocus", seoMetaTagsEntity.KeywordFocus);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public SEOMetaVideoEntity GetSeoMetaVideoById(int id) 
        {
            const string commandText = "CMS_SEOMetaVideo_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", id);
                var numberOfRow = _db.Get<SEOMetaVideoEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected SEOMetaVideoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
