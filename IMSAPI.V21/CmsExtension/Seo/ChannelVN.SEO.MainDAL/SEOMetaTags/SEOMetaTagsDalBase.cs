﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.MainDAL.Databases;


namespace ChannelVN.SEO.MainDAL.SEOMetaTags
{
    public abstract class SEOMetaTagsDalBase
    {
        public bool Insert(SEOMetaTagsEntity seoMetaTagsEntity, ref int tagId)
        {
            const string commandText = "CMS_SEOMetaTags_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", seoMetaTagsEntity.Name);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "ZoneId", seoMetaTagsEntity.ZoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(SEOMetaTagsEntity seoMetaTagsEntity)
        {
            const string commandText = "CMS_SEOMetaTags_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", seoMetaTagsEntity.Id);
                _db.AddParameter(cmd, "Name", seoMetaTagsEntity.Name);
                _db.AddParameter(cmd, "MetaKeyword", seoMetaTagsEntity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", seoMetaTagsEntity.MetaDescription);
                _db.AddParameter(cmd, "ZoneId", seoMetaTagsEntity.ZoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_SEOMetaTags_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public SEOMetaTagsEntity GetSeoMetaTagsById(long id)
        {
            const string commandText = "CMS_SEOMetaTags_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<SEOMetaTagsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<SEOMetaTagsEntity> GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_SEOMetaTags_GetList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyWord);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);                
                var numberOfRow = _db.GetList<SEOMetaTagsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected SEOMetaTagsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
