﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.SEO.MainDAL.SEOMetaNews;
using ChannelVN.SEO.MainDAL.SEOMetaTags;
using ChannelVN.SEO.MainDAL.SEOMetaVideo;
using ChannelVN.SEO.MainDAL.SEOMetaTopic;

namespace ChannelVN.SEO.MainDAL.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region SEOMetaNews

        private SEOMetaNewsDal _sEOMetaNewsMainDal;
        public SEOMetaNewsDal SEOMetaNewsMainDal
        {
            get { return _sEOMetaNewsMainDal ?? (_sEOMetaNewsMainDal = new SEOMetaNewsDal((CmsMainDb)this)); }
        }

        #endregion

        #region SEOMetaTags

        private SEOMetaTagsDal _sEOMetaTagsMainDal;
        public SEOMetaTagsDal SEOMetaTagsMainDal
        {
            get { return _sEOMetaTagsMainDal ?? (_sEOMetaTagsMainDal = new SEOMetaTagsDal((CmsMainDb)this)); }
        }

        #endregion

        #region SEOMetaVideo

        private SEOMetaVideoDal _sEOMetaVideoMainDal;
        public SEOMetaVideoDal SEOMetaVideoMainDal
        {
            get { return _sEOMetaVideoMainDal ?? (_sEOMetaVideoMainDal = new SEOMetaVideoDal((CmsMainDb)this)); }
        }

        #endregion

        #region SEOMetaTopic

        private SEOMetaTopicDal _sEOMetaTopicMainDal;
        public SEOMetaTopicDal SEOMetaTopicMainDal
        {
            get { return _sEOMetaTopicMainDal ?? (_sEOMetaTopicMainDal = new SEOMetaTopicDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}