﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.SEO.DAL.SEOMetaVideo;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.Entity.ErrorCode;

namespace ChannelVN.SEO.BO.SEOMetaVideo
{
    public class SEOMetaVideoBo
    {

        public static SEOMetaVideoEntity GetSEOMetaVideoById(int id)
        {
            return SEOMetaVideoDal.GetSeoMetaVideoById(id);
        }


        public static ErrorMapping.ErrorCodes Insert(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            try
            {
                if (null == seoMetaVideoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaVideoDal.Insert(seoMetaVideoEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaVideo.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(SEOMetaVideoEntity seoMetaVideoEntity)
        {
            try
            {
                if (null == seoMetaVideoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaVideoDal.Update(seoMetaVideoEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaVideo.Update:{0}", ex.Message));
            }
        }

    }
}
