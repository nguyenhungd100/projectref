﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.SEO.DAL.SEOMetaTags;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.Entity.ErrorCode;
namespace ChannelVN.SEO.BO.SEOMetaTags
{
    public class SEOMetaTagsBo
    {

        public static SEOMetaTagsEntity GetSeoMetaTagsById (long id)
        {
            return SEOMetaTagsDal.GetSeoMetaTagsById(id);
        }

        public static List<SEOMetaTagsEntity> GetListSeoMetaTags(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return SEOMetaTagsDal.GetListSeoMetaTags(keyWord, zoneId, pageIndex, pageSize,  ref totalRow);
        }

        public static ErrorMapping.ErrorCodes Insert(SEOMetaTagsEntity seoMetaTagsEntity, ref int tagId)
        {
            try
            {                
                if (null == seoMetaTagsEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var res = SEOMetaTagsDal.Insert(seoMetaTagsEntity, ref tagId);
                if (tagId == 0)
                {
                    return ErrorMapping.ErrorCodes.DuplicateError;
                }
                return res ? ErrorMapping.ErrorCodes.Success: ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTags.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(SEOMetaTagsEntity seoMetaTagsEntity)
        {
            try
            {
                if (null == seoMetaTagsEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaTagsDal.Update(seoMetaTagsEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTags.Update:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                if (id <=0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaTagsDal.Delete(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTags.Delete:{0}", ex.Message));
            }
        }
    }
}
