﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.SEO.DAL.SEOMetaNews;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.Entity.ErrorCode;

namespace ChannelVN.SEO.BO.SEOMetaNews
{
    public class SEOMetaNewsBo
    {

        public static SEOMetaNewsEntity GetSEOMetaNewsById(long id)
        {
            return SEOMetaNewsDal.GetSeoMetaNewsById(id);
        }


        public static ErrorMapping.ErrorCodes Insert(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            try
            {
                if (null == seoMetaNewsEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaNewsDal.Insert(seoMetaNewsEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaNews.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(SEOMetaNewsEntity seoMetaNewsEntity)
        {
            try
            {
                if (null == seoMetaNewsEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaNewsDal.Update(seoMetaNewsEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaNews.Update:{0}", ex.Message));
            }
        }

    }
}
