﻿using System;
using System.Collections.Generic;
using ChannelVN.SEO.DAL.SEOMetaTopic;
using ChannelVN.SEO.Entity;
using ChannelVN.SEO.Entity.ErrorCode;
namespace ChannelVN.SEO.BO.SEOMetaTopic
{
    public class SEOMetaTopicBo
    {        
        public static SEOMetaTopicEntity GetSeoMetaTopicByTopicId(int topicId)
        {
            return SEOMetaTopicDal.GetSeoMetaTopicByTopicId(topicId);
        }

        public static List<SEOMetaTopicEntity> GetListSeoMetaTopic(string keyWord, int zoneId, int pageIndex, int pageSize, ref int totalRow)
        {
            return SEOMetaTopicDal.GetListSeoMetaTopic(keyWord, zoneId, pageIndex, pageSize,  ref totalRow);
        }

        public static ErrorMapping.ErrorCodes Insert(SEOMetaTopicEntity seoMetaTopicEntity, ref int tagId)
        {
            try
            {                
                if (null == seoMetaTopicEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var res = SEOMetaTopicDal.Insert(seoMetaTopicEntity, ref tagId);
                
                return res ? ErrorMapping.ErrorCodes.Success: ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTopic.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(SEOMetaTopicEntity seoMetaTopicEntity)
        {
            try
            {
                if (null == seoMetaTopicEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaTopicDal.Update(seoMetaTopicEntity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTopic.Update:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes Delete(int topicid)
        {
            try
            {
                if (topicid <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return SEOMetaTopicDal.Delete(topicid)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SEOMetaTopic.Delete:{0}", ex.Message));
            }
        }
    }
}
