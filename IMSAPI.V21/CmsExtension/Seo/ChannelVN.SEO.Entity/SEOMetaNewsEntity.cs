﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.SEO.Entity
{
    [DataContract]
    public class SEOMetaNewsEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public string MetaTitle { get; set; }

        [DataMember]
        public string MetaKeyword { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string MetaNewsKeyword { get; set; }

        [DataMember]
        public string KeywordFocus { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string SocialTitle { get; set; }

        [DataMember]
        public string KeywordSub { get; set; }

        [DataMember]
        public int ReviewStatus { get; set; }

        [DataMember]
        public string ModifiedBy { get; set; }

        [DataMember]
        public string ReviewedBy { get; set; }

        [DataMember]
        public DateTime ReviewedDate { get; set; }

        public static implicit operator SEOMetaNewsEntity(SEOMetaVideoEntity v)
        {
            throw new NotImplementedException();
        }
    }
}