﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.SEO.Entity
{
    [DataContract]
    public class SEOMetaVideoEntity : EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }

        [DataMember]
        public string MetaTitle { get; set; }

        [DataMember]
        public string MetaKeyword { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public string MetaVideoKeyword { get; set; }

        [DataMember]
        public string KeywordFocus { get; set; }
    }
}