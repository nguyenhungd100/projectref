﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;
namespace ChannelVN.SEO.Entity
{
    [DataContract]
    public class SEOMetaTagsEntity : EntityBase
    {

        [DataMember] 
        public int Id { get; set; }

        [DataMember] 
        public string Name { get; set; }

        [DataMember] 
        public string MetaKeyword { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public string ZoneName { get; set; }
    }
}
