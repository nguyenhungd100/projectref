﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
namespace ChannelVN.SEO.Entity
{
    [DataContract]
    public class SEOMetaTopicEntity : EntityBase
    {

        //[DataMember] 
        //public int Id { get; set; }

        [DataMember] 
        public string MetaTitle { get; set; }

        [DataMember] 
        public string MetaKeyword { get; set; }

        [DataMember]
        public string MetaDescription { get; set; }

        [DataMember]
        public int TopicId { get; set; }        
    }
}
