﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;

namespace ChannelVN.GameProfile.BO
{
    public class GameBo
    {
        public static List<GameViewForAllListEntity> Search(string keyword, int catId, int pageIndex, int pageSize, ref int totalRow)
        {
            return GameDal.Search(keyword, catId, pageIndex, pageSize, ref totalRow);
        }
        public static GameEntity GetGameById(int id)
        {
            return GameDal.GetGameById(id);
        }
        public static int InsertGame(GameEntity game)
        {
            try
            {
                if (null == game)
                {
                    return 0;
                }
                return GameDal.InsertGame(game);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBo.InsertGame:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGame(GameEntity game)
        {
            try
            {
                if (null == game)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameDal.UpdateGame(game)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBo.UpdateGame:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes DeleteGame(int id)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameDal.DeleteGameByID(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameCategory.DeleteCategory:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGameStatusByID(int gameId, byte status)
        {
            try
            {
                if (null == gameId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameDal.UpdateGameStatusByID(gameId, status)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBo.UpdateGameStatusByID:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGameIsNewByID(long gameId, bool isNew)
        {
            try
            {
                if (null == gameId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameDal.UpdateGameIsNewByID(gameId, isNew)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBo.UpdateGameIsNewByID:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGameIsHighLightByID(int gameId, bool isHighLight)
        {
            try
            {
                if (null == gameId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameDal.UpdateGameIsHighLightByID(gameId, isHighLight)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBo.UpdateGameIsHighLightByID:{0}", ex.Message));
            }
        }

    }
}
