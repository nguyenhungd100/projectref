﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;

namespace ChannelVN.GameProfile.BO
{
    public class GameCategoryBo
    {
        public static List<GameCategoryEntity> GetAll()
        {
            return GameCategoryDal.GetAll();
        }        
        public static GameCategoryEntity GetCategoryById(int id)
        {
            return GameCategoryDal.GetCategoryById(id);
        }
        public static ErrorMapping.ErrorCodes InsertCategory(GameCategoryEntity Category)
        {
            try
            {
                if (null == Category)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameCategoryDal.InsertCategory(Category)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameCategory.InsertCategory:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateCategory(GameCategoryEntity Category)
        {
            try
            {
                if (null == Category)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameCategoryDal.UpdateCategory(Category)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameCategory.UpdateCategory:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes DeleteCategory(int id)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameCategoryDal.DeleteCategory(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameCategory.DeleteCategory:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateStatusByID(int  newsCatId, byte status)
        {
            try
            {
                if (null == newsCatId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameCategoryDal.UpdateStatusByID(newsCatId, status)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameCategory.UpdateStatusByID:{0}", ex.Message));
            }
        }
    }
}
