﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;

namespace ChannelVN.GameProfile.BO
{
    public class GameMediaBo
    {
        #region Get
        public static List<GameMediaEntity> GetListByGameId(int gameId)
        {
            return GameMediaDal.GetListByGameId(gameId);
        }
        public static GameMediaEntity GetGameMediaById(int id)
        {
            return GameMediaDal.GetGameMediaById(id);
        }
        public static int CountGameMediaInGame(int gameId, byte mediaType)
        {
            return GameMediaDal.CountGameMediaInGame(gameId, mediaType);
        }
        #endregion

        #region Set
        public static ErrorMapping.ErrorCodes UpdateMultiGameMedia(string listName, string listDesc, string listNote, string listUrl, int mediaType, int gameId)
        {
            var listOldMedia = GameMediaBo.GetListByGameId(gameId);
            try
            {
                GameMediaBo.DeleteGameMediaByGameID(gameId, mediaType);
                int i = 0;
                foreach (var media in listUrl.Split(new char[] { ';' }))
                {
                    if (!string.IsNullOrEmpty(media.Trim()))
                    {
                        GameMediaEntity entity = new GameMediaEntity();
                        entity.CreatedDate = DateTime.Now;
                        entity.GameId = gameId;
                        entity.MediaDesc = listDesc.Split(new char[] { ';' })[i];
                        entity.MediaUrl = listUrl.Split(new char[] { ';' })[i];
                        entity.MediaNote = listNote.Split(new char[] { ';' })[i];
                        entity.MediaName = listName.Split(new char[] { ';' })[i];
                        entity.SortOrder = i;
                        entity.MediaType = byte.Parse(mediaType.ToString());
                        GameMediaBo.InsertGameMedia(entity);
                    }
                    i++;
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                foreach (var gameMediaEntity in listOldMedia)
                {
                    GameMediaBo.InsertGameMedia(gameMediaEntity);
                }
                throw new Exception(string.Format("GameMediaBo.UpdateMultiGameMedia:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes InsertGameMedia(GameMediaEntity gameMedia)
        {
            try
            {
                if (null == gameMedia)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameMediaDal.InsertGameMedia(gameMedia)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameMediaBo.InsertGameMedia:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGameMedia(GameMediaEntity gameMedia)
        {
            try
            {
                if (null == gameMedia)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameMediaDal.UpdateGameMedia(gameMedia)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameMediaBo.UpdateGameMedia:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes DeleteGameMedia(int id)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameMediaDal.DeleteGameMediaByID(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameMediaBo.DeleteGameMedia:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes DeleteGameMediaByGameID(int gameId, int mediaType)
        {
            try
            {
                if (gameId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameMediaDal.DeleteGameMediaByGameID(gameId, mediaType)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameMediaBo.DeleteGameMediaByGameID:{0}", ex.Message));
            }
        }
        #endregion
    }
}
