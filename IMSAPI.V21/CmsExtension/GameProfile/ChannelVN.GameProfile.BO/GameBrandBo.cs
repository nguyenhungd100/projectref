﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;

namespace ChannelVN.GameProfile.BO
{
    public class GameBrandBo
    {
        public static List<GameBrandEntity> GetAll()
        {
            return GameBrandDal.GetAll();
        }
        public static List<GameBrandEntity> GetList(int pageIndex, int pageSize, ref int totalRow)
        {
            return GameBrandDal.GetList(pageIndex, pageSize, ref totalRow);
        }
        public static List<GameBrandEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return GameBrandDal.Search(keyword, pageIndex, pageSize, ref totalRow);
        }
        public static GameBrandEntity GetBrandById(int id)
        {
            return GameBrandDal.GetBrandById(id);
        }
        public static ErrorMapping.ErrorCodes InsertBrand(GameBrandEntity brand)
        {
            try
            {
                if (null == brand)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameBrandDal.InsertBrand(brand)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBrand.InsertBrand:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateBrand(GameBrandEntity brand)
        {
            try
            {
                if (null == brand)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameBrandDal.UpdateBrand(brand)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBrand.UpdateBrand:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes DeleteBrand(int id)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameBrandDal.DeleteBrand(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBrand.DeleteBrand:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateStatusByID(long productId, byte status)
        {
            try
            {
                if (null == productId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameBrandDal.UpdateStatusByID(productId, status)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBrand.UpdateStatusByID:{0}", ex.Message));
            }
        }
    }
}
