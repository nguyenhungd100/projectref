﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;
using ChannelVN.CMS.Common;

namespace ChannelVN.GameProfile.BO
{
    public class GameGiftCodeBo
    {
        public static ErrorMapping.ErrorCodes InsertGiftCode(long newsId, string giftCode, string tags)
        {
            try
            {
                if (null == newsId || string.IsNullOrEmpty(giftCode.Trim()))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GameGiftCodeDal.InsertGiftCode(newsId, giftCode, tags)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static GameGiftCodeEntity GetGiftCodeByNewsId(long newsId)
        {
            return GameGiftCodeDal.GetGiftCodeByNewsId(newsId);
        }
    }
}
