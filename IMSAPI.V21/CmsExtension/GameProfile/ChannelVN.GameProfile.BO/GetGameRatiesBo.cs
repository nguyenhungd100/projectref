﻿using System.Collections.Generic;
using ChannelVN.GameProfile.DAL;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.Entity.ErrorCode;
using System;

namespace ChannelVN.GameProfile.BO
{
    public class GateGameRatiesBo
    {
        public static List<GateGameRatiesEntity> GetListByDate(DateTime lastModifyDate, ref DateTime maxVoteDate)
        {
            return GateGameRatiesDal.GetListById(lastModifyDate, ref maxVoteDate);
        }

        public static ErrorMapping.ErrorCodes SyncGateGameRaties(GateGameRatiesEntity gameRate)
        {
            try
            {
                return GateGameRatiesDal.SyncGateGameRaties(gameRate)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GateGameRatiesBo.SyncGateGameRaties:{0}", ex.Message));
            }
        }
        public static GameRateEntity GetGameRateByGameId(int gameId)
        {
            return GateGameRatiesDal.GetGameRateByGameId(gameId);
        }
        public static ErrorMapping.ErrorCodes UpdateGameRate(GameRateEntity gameRate)
        {
            try
            {
                if (null == gameRate)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return GateGameRatiesDal.UpdateGameRate(gameRate)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameRateBo.UpdateGameRateBrand:{0}", ex.Message));
            }
        }    
    }
}
