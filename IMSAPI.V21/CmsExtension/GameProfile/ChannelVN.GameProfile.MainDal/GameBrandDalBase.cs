﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GameBrandDalBase
    {
        #region Get methods
        public List<GameBrandEntity> GetAll()
        {
            const string commandText = "dai_Gate_GameBrands_GetList";
            try
            {
                List<GameBrandEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<GameBrandEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<GameBrandEntity> GetList(int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "dai_Gate_GameBrands_GetByPaging";
            try
            {
                List<GameBrandEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRecord", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<GameBrandEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<GameBrandEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "dai_Gate_GameBrands_Search";
            try
            {
                List<GameBrandEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRecord", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Keyword", keyword);
                data = _db.GetList<GameBrandEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public GameBrandEntity GetBrandById(int id)
        {
            const string commandText = "dai_Gate_GameBrands_GetById";
            SqlDataReader reader = null;
            try
            {
                GameBrandEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", id);
                data = _db.Get<GameBrandEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Set methods
        public bool InsertBrand(GameBrandEntity brand)
        {
            const string commandText = "dai_Gate_GameBrands_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandName", brand.BrandName);
                _db.AddParameter(cmd, "BrandDescription", brand.BrandDescription);
                _db.AddParameter(cmd, "IsManufacturer", brand.IsManufacturer);
                _db.AddParameter(cmd, "IsPublisher", brand.IsPublisher);
                _db.AddParameter(cmd, "SortOrder", brand.SortOrder);
                _db.AddParameter(cmd, "ID", brand.ID);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateBrand(GameBrandEntity brand)
        {
            const string commandText = "dai_Gate_GameBrands_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BrandName", brand.BrandName);
                _db.AddParameter(cmd, "BrandDescription", brand.BrandDescription);
                _db.AddParameter(cmd, "IsManufacturer", brand.IsManufacturer);
                _db.AddParameter(cmd, "IsPublisher", brand.IsPublisher);
                _db.AddParameter(cmd, "SortOrder", brand.SortOrder);
                _db.AddParameter(cmd, "ID", brand.ID);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool DeleteBrand(int id)
        {

            const string commandText = "dai_Gate_GameBrands_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateStatusByID(long productId, byte status)
        {

            const string commandText = "dai_Gate_GameProduct_UpdateStatusByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ID", productId);
                _db.AddParameter(cmd, "Status", status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}",commandText, ex.Message));
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho GameBrandDalBase
        /// minhduongvan
        /// 2014/01/11
        /// </summary>

        #region Constructor

        private readonly CmsMainDbBase _db;

        protected GameBrandDalBase(CmsMainDbBase db)
        {
            _db = db;
        }

        protected CmsMainDbBase Database
        {
            get { return _db; }
        }

        #endregion
    }
}
