﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.GameProfile.MainDal.Databases;
using ChannelVN.GameProfile.MainDal;

namespace ChannelVN.GameProfile.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsGameProfileDbBase : MainDbBase
    {
        #region Store procedures

        #region Game GiftCode
        private GameGiftCodeDal _GameGiftCodeDal;
        public GameGiftCodeDalBase GameGiftCodeDal
        {
            get { return _GameGiftCodeDal ?? (_GameGiftCodeDal = new GameGiftCodeDal((CmsGameProfileDb)this)); }
        }
        #endregion


        private GateGameRatiesDal _GateGameRatiesDal;
        public GateGameRatiesDalBase GateGameRatiesDal
        {
            get { return _GateGameRatiesDal ?? (_GateGameRatiesDal = new GateGameRatiesDal((CmsGameProfileDb)this)); }
        }

        #endregion

        #region Constructors

        protected CmsGameProfileDbBase()
        {
        }
        protected CmsGameProfileDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}