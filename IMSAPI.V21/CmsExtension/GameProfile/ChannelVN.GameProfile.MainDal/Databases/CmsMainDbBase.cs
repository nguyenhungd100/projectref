﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.GameProfile.MainDal.Databases;
using ChannelVN.GameProfile.MainDal;

namespace ChannelVN.GameProfile.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures
        private MainGateGameRatiesDal _MainGateGameRatiesDal;
        public MainGateGameRatiesDalBase MainGateGameRatiesDal
        {
            get { return _MainGateGameRatiesDal ?? (_MainGateGameRatiesDal = new MainGateGameRatiesDal((CmsMainDb)this)); }
        }
        #region Game brand
        private GameBrandDal _GameBrandContentMainDal;
        public GameBrandDalBase GameBrandContentMainDal
        {
            get { return _GameBrandContentMainDal ?? (_GameBrandContentMainDal = new GameBrandDal((CmsMainDb)this)); }
        }
        #endregion

        #region Game Category
        private GameCategoryDal _GameCategoryDal;
        public GameCategoryDalBase GameCategoryDal
        {
            get { return _GameCategoryDal ?? (_GameCategoryDal = new GameCategoryDal((CmsMainDb)this)); }
        }
        #endregion
        #region Game
        private GameDal _GameDal;
        public GameDalBase GameDal
        {
            get { return _GameDal ?? (_GameDal = new GameDal((CmsMainDb)this)); }
        }
        #endregion
        #region Game Media
        private GameMediaDal _GameMediaDal;
        public GameMediaDalBase GameMediaDal
        {
            get { return _GameMediaDal ?? (_GameMediaDal = new GameMediaDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}