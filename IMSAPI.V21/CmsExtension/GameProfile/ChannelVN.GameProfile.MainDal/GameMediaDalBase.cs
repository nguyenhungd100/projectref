﻿using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GameMediaDalBase
    {
        #region Get methods
        public List<GameMediaEntity> GetListByGameId(int gameId)
        {

            const string commandText = "dai_Gate_Media_GetByGameId";
            try
            {
                List<GameMediaEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameID", gameId);

                data = _db.GetList<GameMediaEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public GameMediaEntity GetGameMediaById(int mediaId)
        {

            const string commandText = "dai_Gate_Media_GetById";
            try
            {
                GameMediaEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MediaId", mediaId);

                data = _db.Get<GameMediaEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int CountGameMediaInGame(int gameId, byte mediaType)
        {

            int count = 0;
            const string commandText = "TUPA_Gate_GameMedia_CountMediaInGame";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                _db.AddParameter(cmd, "MediaType", mediaType);

                count = (int)cmd.ExecuteScalar();
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Set methods
        public bool InsertGameMedia(GameMediaEntity gameMedia)
        {

            const string commandText = "dai_Gate_Media_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MediaId", gameMedia.MediaId);
                _db.AddParameter(cmd, "MediaName", gameMedia.MediaName);
                _db.AddParameter(cmd, "MediaDesc", gameMedia.MediaDesc);
                _db.AddParameter(cmd, "MediaUrl", gameMedia.MediaUrl);
                _db.AddParameter(cmd, "MediaNote", gameMedia.MediaNote);
                _db.AddParameter(cmd, "MediaType", gameMedia.MediaType);
                _db.AddParameter(cmd, "CreatedDate", gameMedia.CreatedDate);
                _db.AddParameter(cmd, "SortOrder", gameMedia.SortOrder);
                _db.AddParameter(cmd, "GameId", gameMedia.GameId);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateGameMedia(GameMediaEntity gameMedia)
        {
            const string commandText = "dai_Gate_Media_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MediaId", gameMedia.MediaId);
                _db.AddParameter(cmd, "MediaName", gameMedia.MediaName);
                _db.AddParameter(cmd, "MediaDesc", gameMedia.MediaDesc);
                _db.AddParameter(cmd, "MediaUrl", gameMedia.MediaUrl);
                _db.AddParameter(cmd, "MediaNote", gameMedia.MediaNote);
                _db.AddParameter(cmd, "MediaType", gameMedia.MediaType);
                //, _db.AddParameter(cmd,"CreatedDate", gameMedia.CreatedDate);
                _db.AddParameter(cmd, "SortOrder", gameMedia.SortOrder);
                _db.AddParameter(cmd, "GameId", gameMedia.GameId);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }

        public bool DeleteGameMediaByID(int mediaId)
        {

            const string commandText = "dai_Gate_Media_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MediaId", mediaId);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool DeleteGameMediaByGameID(int gameId, int mediaType)
        {

            const string commandText = "TuPA_Gate_GameMedia_DeleteByGameId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                _db.AddParameter(cmd, "MediaType", mediaType);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho GameMediaDalBase
        /// minhduongvan
        /// 2014/01/11
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected GameMediaDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
