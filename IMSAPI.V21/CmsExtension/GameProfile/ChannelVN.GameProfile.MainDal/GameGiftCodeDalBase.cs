﻿using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GameGiftCodeDalBase
    {
        public bool InsertGiftCode(long newsId, string giftCode, string tags)
        {

            const string commandText = "TuPA_GiftCode_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "GiftCode", giftCode);
                _db.AddParameter(cmd, "Tags", tags);

                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public GameGiftCodeEntity GetGiftCodeByNewsId(long newsId)
        {

            const string commandText = "CMS_GiftCode_GetByNewsId";
            try
            {
                GameGiftCodeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);

                data = _db.Get<GameGiftCodeEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho GameDalBase
        /// minhduongvan
        /// 2014/01/11
        /// </summary>

        #region Constructor

        private readonly CmsGameProfileDb _db;

        protected GameGiftCodeDalBase(CmsGameProfileDb db)
        {
            _db = db;
        }

        protected CmsGameProfileDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
