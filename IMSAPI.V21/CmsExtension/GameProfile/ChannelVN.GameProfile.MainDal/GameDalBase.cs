﻿using ChannelVN.CMS.Common;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GameDalBase
    {
        #region Get methods
        public List<GameViewForAllListEntity> Search(string keyword, int catId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "dai_Gate_Games_Search";
            try
            {
                List<GameViewForAllListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRecord", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Cat_ID", catId);

                data = _db.GetList<GameViewForAllListEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public GameEntity GetGameById(int id)
        {
            const string commandText = "DaiTT_BE_Gate_Games_GetById";
            try
            {
                GameEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", id);
                data = _db.Get<GameEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Set methods
        public int InsertGame(GameEntity game)
        {
            const string commandText = "ThanhDT_Gate_Games_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", game.GameId, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "GameName", game.GameName);
                _db.AddParameter(cmd, "GameDescption", game.GameDescption);
                _db.AddParameter(cmd, "GameAvartar", game.GameAvartar);
                _db.AddParameter(cmd, "ImageScover", game.ImageScover);
                _db.AddParameter(cmd, "Domain", game.Domain);
                _db.AddParameter(cmd, "HomeUrl", game.HomeUrl);
                _db.AddParameter(cmd, "DownloadUrl", game.DownloadUrl);
                _db.AddParameter(cmd, "Manufacturer", game.Manufacturer);
                _db.AddParameter(cmd, "Publisher", game.Publisher);
                _db.AddParameter(cmd, "IsNewGame", game.IsNewGame);
                _db.AddParameter(cmd, "IsHighLightGame", game.IsHighLightGame);
                _db.AddParameter(cmd, "Cat_ID", game.Cat_ID);
                _db.AddParameter(cmd, "CloseBetaDate", game.CloseBetaDate);
                _db.AddParameter(cmd, "CloseBetaDateStamp", game.CloseBetaDateStamp);
                _db.AddParameter(cmd, "OpenBetaDate", game.OpenBetaDate);
                _db.AddParameter(cmd, "OpenBetaDateStamp", game.OpenBetaDateStamp);
                _db.AddParameter(cmd, "Status", game.Status);
                _db.AddParameter(cmd, "SortOrder", game.SortOrder);
                _db.AddParameter(cmd, "Mark", game.Mark);
                var data = cmd.ExecuteNonQuery();
                game.GameId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return game.GameId;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return 0;
            }
        }
        public bool UpdateGame(GameEntity game)
        {

            const string commandText = "ThanhDT_Gate_Games_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", game.GameId);
                _db.AddParameter(cmd, "GameName", game.GameName);
                _db.AddParameter(cmd, "GameDescption", game.GameDescption);
                _db.AddParameter(cmd, "GameAvartar", game.GameAvartar);
                _db.AddParameter(cmd, "ImageScover", game.ImageScover);
                _db.AddParameter(cmd, "Domain", game.Domain);
                _db.AddParameter(cmd, "HomeUrl", game.HomeUrl);
                _db.AddParameter(cmd, "DownloadUrl", game.DownloadUrl);
                _db.AddParameter(cmd, "Manufacturer", game.Manufacturer);
                _db.AddParameter(cmd, "Publisher", game.Publisher);
                _db.AddParameter(cmd, "IsNewGame", game.IsNewGame);
                _db.AddParameter(cmd, "IsHighLightGame", game.IsHighLightGame);
                _db.AddParameter(cmd, "Cat_ID", game.Cat_ID);
                _db.AddParameter(cmd, "CloseBetaDate", game.CloseBetaDate);
                _db.AddParameter(cmd, "CloseBetaDateStamp", game.CloseBetaDateStamp);
                _db.AddParameter(cmd, "OpenBetaDate", game.OpenBetaDate);
                _db.AddParameter(cmd, "OpenBetaDateStamp", game.OpenBetaDateStamp);
                _db.AddParameter(cmd, "Status", game.Status);
                _db.AddParameter(cmd, "SortOrder", game.SortOrder);
                _db.AddParameter(cmd, "Mark", game.Mark);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateGameStatusByID(long gameId, byte status)
        {

            const string commandText = "dai_Gate_Games_UpdateStatusById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                _db.AddParameter(cmd, "Status", status);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateGameIsNewByID(long gameId, bool isNew)
        {
            const string commandText = "dai_Gate_Games_UpdateIsNewById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                _db.AddParameter(cmd, "IsNew", isNew);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateGameIsHighLightByID(int gameId, bool isHighLight)
        {

            const string commandText = "dai_Gate_Games_UpdateIsHighLightById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                _db.AddParameter(cmd, "IsHighLight", isHighLight);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool DeleteGameByID(int gameId)
        {

            const string commandText = "dai_Gate_Games_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho GameDalBase
        /// minhduongvan
        /// 2014/01/11
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected GameDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
