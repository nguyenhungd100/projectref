﻿using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GameCategoryDalBase
    {
        #region Get methods
        public List<GameCategoryEntity> GetAll()
        {
            const string commandText = "dai_Gate_GameCategories_GetList";
            try
            {
                List<GameCategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<GameCategoryEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public GameCategoryEntity GetCategoryById(int id)
        {

            const string commandText = "dai_Gate_GameCategories_GetById";
            try
            {
                GameCategoryEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Cat_ID", id);
                data = _db.Get<GameCategoryEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Set methods
        public bool InsertCategory(GameCategoryEntity category)
        {
            const string commandText = "dai_Gate_GameCategories_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Cat_ID", 0);
                _db.AddParameter(cmd, "Cat_ParentID", category.Cat_ParentID);
                _db.AddParameter(cmd, "Cat_Name", category.Cat_Name);
                _db.AddParameter(cmd, "Cat_Desc", category.Cat_Desc);
                _db.AddParameter(cmd, "Cat_Status", category.Cat_Status);
                _db.AddParameter(cmd, "Cat_Order", category.Cat_Order);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateCategory(GameCategoryEntity category)
        {
            const string commandText = "dai_Gate_GameCategories_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Cat_ID", category.Cat_ID);
                _db.AddParameter(cmd, "Cat_ParentID", category.Cat_ParentID);
                _db.AddParameter(cmd, "Cat_Name", category.Cat_Name);
                _db.AddParameter(cmd, "Cat_Desc", category.Cat_Desc);
                _db.AddParameter(cmd, "Cat_Status", category.Cat_Status);
                _db.AddParameter(cmd, "Cat_Order", category.Cat_Order);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool DeleteCategory(int id)
        {

            const string commandText = "dai_Gate_GameCategories_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Cat_ID", id);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        public bool UpdateStatusByID(int newsCatId, byte status)
        {
            const string commandText = "dai_Gate_GameCategories_UpdateStatusByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Cat_ID", newsCatId);
                _db.AddParameter(cmd, "Cat_Status", status);
                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}",commandText, ex.Message));
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho GameCategoryDalBase
        /// minhduongvan
        /// 2014/01/11
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected GameCategoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
