﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class MainGateGameRatiesDalBase
    {
        public bool SyncGateGameRaties(GateGameRatiesEntity gameRate)
        {

            const string commandText = "CMS_Gate_GameRaties_SyncFromExternal";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameRate.GameId);
                _db.AddParameter(cmd, "Vote_Mark1", gameRate.Vote_Mark1);
                _db.AddParameter(cmd, "Vote_Mark1_5", gameRate.Vote_Mark1_5);
                _db.AddParameter(cmd, "Vote_Mark2", gameRate.Vote_Mark2);
                _db.AddParameter(cmd, "Vote_Mark2_5", gameRate.Vote_Mark2_5);
                _db.AddParameter(cmd, "Vote_Mark3", gameRate.Vote_Mark3);
                _db.AddParameter(cmd, "Vote_Mark3_5", gameRate.Vote_Mark3_5);
                _db.AddParameter(cmd, "Vote_Mark4", gameRate.Vote_Mark4);
                _db.AddParameter(cmd, "Vote_Mark4_5", gameRate.Vote_Mark4_5);
                _db.AddParameter(cmd, "Vote_Mark5", gameRate.Vote_Mark5);
                _db.AddParameter(cmd, "Vote_Mark5_5", gameRate.Vote_Mark5_5);
                _db.AddParameter(cmd, "Vote_Mark6", gameRate.Vote_Mark6);
                _db.AddParameter(cmd, "Vote_Mark6_5", gameRate.Vote_Mark6_5);
                _db.AddParameter(cmd, "Vote_Mark7", gameRate.Vote_Mark7);
                _db.AddParameter(cmd, "Vote_Mark7_5", gameRate.Vote_Mark7_5);
                _db.AddParameter(cmd, "Vote_Mark8", gameRate.Vote_Mark8);
                _db.AddParameter(cmd, "Vote_Mark8_5", gameRate.Vote_Mark8_5);
                _db.AddParameter(cmd, "Vote_Mark9", gameRate.Vote_Mark9);
                _db.AddParameter(cmd, "Vote_Mark9_5", gameRate.Vote_Mark9_5);
                _db.AddParameter(cmd, "Vote_Mark10", gameRate.Vote_Mark10);
                _db.AddParameter(cmd, "AverageMark", gameRate.AverageMark);
                _db.AddParameter(cmd, "VoteTotal", gameRate.VoteTotal);
                _db.AddParameter(cmd, "MarkTotal", gameRate.MarkTotal);
                _db.AddParameter(cmd, "UniqueVote", gameRate.UniqueVote);
                _db.AddParameter(cmd, "LastestVoteDate", gameRate.LastestVoteDate);
                _db.AddParameter(cmd, "LastestMark", gameRate.LastestMark);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Get methods

        public GameRateEntity GetGameRateByGameId(int gameId)
        {

            const string commandText = "TuPA_Gate_GameRate_GetByGameId";
            try
            {
                GameRateEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GameId", gameId);
                data = _db.Get<GameRateEntity>(cmd);
                return data;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Set methods

        public bool UpdateGameRate(GameRateEntity gameRate)
        {

            const string commandText = "TuPA_Gate_GameRate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AverageMark", gameRate.AverageMark);
                _db.AddParameter(cmd, "VoteTotal", gameRate.VoteTotal);
                _db.AddParameter(cmd, "MarkTotal", gameRate.MarkTotal);
                _db.AddParameter(cmd, "UniqueVote", gameRate.UniqueVote);
                _db.AddParameter(cmd, "GameId", gameRate.GameId);

                int numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }
        #endregion
        
        #region Constructor

        private readonly CmsMainDb _db;

        protected MainGateGameRatiesDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
