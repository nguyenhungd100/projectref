﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.MainDal
{
    public abstract class GateGameRatiesDalBase
    {
        #region Get methods
        public List<GateGameRatiesEntity> GetListByDate(DateTime lastVoteDate, ref DateTime maxVoteDate)
        {

            const string commandText = "CMS_Gate_GameRaties_GetListByDate";
            try
            {
                List<GateGameRatiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (lastVoteDate == DateTime.MinValue)
                {
                    lastVoteDate = DateTime.Now;
                }
                //lastVoteDate = DateTime.Now.AddYears(-50);
                _db.AddParameter(cmd, "LastestVoteDate", lastVoteDate);
                _db.AddParameter(cmd, "MaxVoteDate", maxVoteDate, System.Data.ParameterDirection.Output);
                data = _db.GetList<GateGameRatiesEntity>(cmd);
                maxVoteDate = Utility.ConvertToDateTime(_db.GetParameterValueFromCommand(cmd, 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion
        

        #region Constructor

        private readonly CmsGameProfileDb _db;

        protected GateGameRatiesDalBase(CmsGameProfileDb db)
        {
            _db = db;
        }

        protected CmsGameProfileDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
