﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GameMediaDal
    {
        #region Get methods
        public static List<GameMediaEntity> GetListByGameId(int gameId)
        {
            List<GameMediaEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.GetListByGameId(gameId);
            }
            return retVal;
        }
        public static GameMediaEntity GetGameMediaById(int mediaId)
        {
            GameMediaEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.GetGameMediaById(mediaId);
            }
            return retVal;
        }
        public static int CountGameMediaInGame(int gameId, byte mediaType)
        {
            int retVal = 0;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.CountGameMediaInGame(gameId, mediaType);
            }
            return retVal;
        }
        #endregion

        #region Set methods
        public static bool InsertGameMedia(GameMediaEntity gameMedia)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.InsertGameMedia(gameMedia);
            }
            return retVal;
        }
        public static bool UpdateGameMedia(GameMediaEntity gameMedia)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.UpdateGameMedia(gameMedia);
            }
            return retVal;
        }

        public static bool DeleteGameMediaByID(int mediaId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.DeleteGameMediaByID(mediaId);
            }
            return retVal;
        }
        public static bool DeleteGameMediaByGameID(int gameId, int mediaType)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameMediaDal.DeleteGameMediaByGameID(gameId, mediaType);
            }
            return retVal;
        }
        #endregion
    }
}
