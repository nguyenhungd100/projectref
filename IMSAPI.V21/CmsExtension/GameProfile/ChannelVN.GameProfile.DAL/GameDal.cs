﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GameDal
    {
        #region Get methods
        public static List<GameViewForAllListEntity> Search(string keyword, int catId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<GameViewForAllListEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.Search(keyword, catId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static GameEntity GetGameById(int id)
        {
            GameEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.GetGameById(id);
            }
            return retVal;
        }
        #endregion

        #region Set methods
        public static int InsertGame(GameEntity game)
        {
            int retVal = 0;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.InsertGame(game);
            }
            return retVal;
        }
        public static bool UpdateGame(GameEntity game)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.UpdateGame(game);
            }
            return retVal;
        }
        public static bool UpdateGameStatusByID(long gameId, byte status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.UpdateGameStatusByID(gameId, status);
            }
            return retVal;
        }
        public static bool UpdateGameIsNewByID(long gameId, bool isNew)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.UpdateGameIsNewByID(gameId, isNew);
            }
            return retVal;
        }
        public static bool UpdateGameIsHighLightByID(int gameId, bool isHighLight)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.UpdateGameIsHighLightByID(gameId, isHighLight);
            }
            return retVal;
        }
        public static bool DeleteGameByID(int gameId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameDal.DeleteGameByID(gameId);
            }
            return retVal;
        }
        #endregion
    }
}
