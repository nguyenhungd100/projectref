﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GateGameRatiesDal
    {
        #region Get methods
        public static List<GateGameRatiesEntity> GetListById(DateTime lastVoteDate, ref DateTime maxVoteDate)
        {
            List<GateGameRatiesEntity> retVal = null;
            using (var db = new CmsGameProfileDb())
            {
                retVal = db.GateGameRatiesDal.GetListByDate(lastVoteDate, ref maxVoteDate);
            }
            return retVal;
        }
        #endregion

        public static bool SyncGateGameRaties(GateGameRatiesEntity gameRate)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MainGateGameRatiesDal.SyncGateGameRaties(gameRate);
            }
            return retVal;
        }

        #region Get methods

        public static GameRateEntity GetGameRateByGameId(int gameId)
        {
            GameRateEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.MainGateGameRatiesDal.GetGameRateByGameId(gameId);
            }
            return retVal;
        }
        #endregion

        #region Set methods

        public static bool UpdateGameRate(GameRateEntity gameRate)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MainGateGameRatiesDal.UpdateGameRate(gameRate);
            }
            return retVal;
        }
        #endregion
    }
}
