﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GameGiftCodeDal
    {
        #region Set methods

        public static bool InsertGiftCode(long newsId, string giftCode, string tags)
        {
            bool retVal = false;
            using (var db = new CmsGameProfileDb())
            {
                retVal = db.GameGiftCodeDal.InsertGiftCode(newsId, giftCode, tags);
            }
            return retVal;
        }
        #endregion

        public static GameGiftCodeEntity GetGiftCodeByNewsId(long newsId)
        {
            GameGiftCodeEntity retVal = null;
            using (var db = new CmsGameProfileDb())
            {
                retVal = db.GameGiftCodeDal.GetGiftCodeByNewsId(newsId);
            }
            return retVal;
        }
    }
}
