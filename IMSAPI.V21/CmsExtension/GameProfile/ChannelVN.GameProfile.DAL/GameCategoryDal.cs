﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GameCategoryDal
    {
        #region Get methods
        public static List<GameCategoryEntity> GetAll()
        {
            List<GameCategoryEntity> retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.GetAll();
            }
            return retVal;
        }
        public static GameCategoryEntity GetCategoryById(int id)
        {
            GameCategoryEntity retVal = null;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.GetCategoryById(id);
            }
            return retVal;
        }
        #endregion

        #region Set methods
        public static bool InsertCategory(GameCategoryEntity category)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.InsertCategory(category);
            }
            return retVal;
        }
        public static bool UpdateCategory(GameCategoryEntity category)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.UpdateCategory(category);
            }
            return retVal;
        }
        public static bool DeleteCategory(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.DeleteCategory(id);
            }
            return retVal;
        }
        public static bool UpdateStatusByID(int newsCatId, byte status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameCategoryDal.UpdateStatusByID(newsCatId, status);
            }
            return retVal;
        }
        #endregion
    }
}
