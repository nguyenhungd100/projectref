﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.GameProfile.DAL.Common;
using ChannelVN.GameProfile.Entity;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.GameProfile.MainDal.Databases;

namespace ChannelVN.GameProfile.DAL
{
    public class GameBrandDal
    {
        #region Get methods
        public static List<GameBrandEntity> GetAll()
        {
            List<GameBrandEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.GetAll();
            }
            return retVal;
        }
        public static List<GameBrandEntity> GetList(int pageIndex, int pageSize, ref int totalRow)
        {
            List<GameBrandEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.GetList(pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<GameBrandEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<GameBrandEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static GameBrandEntity GetBrandById(int id)
        {
            GameBrandEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.GetBrandById(id);
            }
            return retVal;
        }
        #endregion

        #region Set methods
        public static bool InsertBrand(GameBrandEntity brand)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.InsertBrand(brand);
            }
            return retVal;
        }
        public static bool UpdateBrand(GameBrandEntity brand)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.UpdateBrand(brand);
            }
            return retVal;
        }
        public static bool DeleteBrand(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.DeleteBrand(id);
            }
            return retVal;
        }
        public static bool UpdateStatusByID(long productId, byte status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.GameBrandContentMainDal.UpdateStatusByID(productId, status);
            }
            return retVal;
        }
        #endregion
    }
}
