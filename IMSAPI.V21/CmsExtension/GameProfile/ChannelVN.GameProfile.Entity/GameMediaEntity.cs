﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.GameProfile.Entity
{
    public class GameMediaEntity : EntityBase
    {
        [DataMember]
        public int MediaId { get; set; }
        [DataMember]
        public string MediaName { get; set; }
        [DataMember]
        public string MediaDesc { get; set; }
        [DataMember]
        public string MediaUrl { get; set; }
        [DataMember]
        public string MediaNote { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public byte MediaType { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int GameId { get; set; }
    }
    
}
