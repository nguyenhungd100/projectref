﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.GameProfile.Entity
{
    [DataContract]
    public class LikeTypeEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long MapId { get; set; }
        [DataMember]
        public int Counter { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public bool isProcess { get; set; }
    }
}
