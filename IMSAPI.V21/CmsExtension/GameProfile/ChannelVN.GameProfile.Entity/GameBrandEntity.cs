﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.GameProfile.Entity
{
    [DataContract]
    public class GameBrandEntity : EntityBase
    {

        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string BrandName { get; set; }
        [DataMember]
        public string BrandDescription { get; set; }
        [DataMember]
        public bool IsManufacturer { get; set; }
        [DataMember]
        public bool IsPublisher { get; set; }
        [DataMember]
        public int? SortOrder { get; set; }
    }
}