﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.GameProfile.Entity
{
    public class GameRateEntity : EntityBase
    {
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public double MarkTotal { get; set; }
        [DataMember]
        public double AverageMark { get; set; }
        [DataMember]
        public long VoteTotal { get; set; }
        [DataMember]
        public long UniqueVote { get; set; }
    }
}
