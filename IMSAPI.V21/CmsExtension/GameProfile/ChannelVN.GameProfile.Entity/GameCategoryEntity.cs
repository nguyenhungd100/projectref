﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.GameProfile.Entity
{
    [DataContract]
    public class GameCategoryEntity : EntityBase
    {
        [DataMember]
        public int Cat_ID { get; set; }
        [DataMember]
        public int? Cat_ParentID { get; set; }
        [DataMember]
        public string Cat_Name { get; set; }
        [DataMember]
        public string Cat_Desc { get; set; }
        [DataMember]
        public byte? Cat_Status { get; set; }
        [DataMember]
        public int? Cat_Order { get; set; }


    }
    [DataContract]
    public class GameCategoryViewForAllListEntity : EntityBase
    {
        [DataMember]
        public int Cat_ID { get; set; }
        [DataMember]
        public int Cat_ParentID { get; set; }
        [DataMember]
        public string Cat_Name { get; set; }
        [DataMember]
        public string Cat_ParentName { get; set; }
        [DataMember]
        public string Cat_Desc { get; set; }
        [DataMember]
        public byte Cat_Status { get; set; }
        [DataMember]
        public string Cat_StatusName { get; set; }
        [DataMember]
        public int Cat_Order { get; set; }
    }
}