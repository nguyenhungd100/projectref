﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.GameProfile.Entity
{
    public class GameEntity : EntityBase
    {
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public string GameName { get; set; }
        [DataMember]
        public string GameDescption { get; set; }
        [DataMember]
        public string GameAvartar { get; set; }
        [DataMember]
        public string ImageScover { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string HomeUrl { get; set; }
        [DataMember]
        public string DownloadUrl { get; set; }
        [DataMember]
        public int Manufacturer { get; set; }
        [DataMember]
        public int Publisher { get; set; }
        [DataMember]
        public bool? IsNewGame { get; set; }
        [DataMember]
        public bool? IsHighLightGame { get; set; }
        [DataMember]
        public int Cat_ID { get; set; }
        [DataMember]
        public DateTime? CloseBetaDate { get; set; }
        [DataMember]
        public double? CloseBetaDateStamp { get; set; }
        [DataMember]
        public DateTime? OpenBetaDate { get; set; }
        [DataMember]
        public double? OpenBetaDateStamp { get; set; }
        [DataMember]
        public byte Status { get; set; }
        [DataMember]
        public int? SortOrder { get; set; }
        [DataMember]
        public double? Mark { get; set; }
    }

    public class GameViewForAllListEntity : EntityBase
    {
        [DataMember]
        public int GameId { get; set; }
        [DataMember]
        public string GameName { get; set; }
        [DataMember]
        public string GameAvartar { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public bool? IsNewGame { get; set; }
        [DataMember]
        public bool? IsHighLightGame { get; set; }
        [DataMember]
        public int Cat_ID { get; set; }
        [DataMember]
        public string Cat_Name { get; set; }
        [DataMember]
        public DateTime? CloseBetaDate { get; set; }
        [DataMember]
        public DateTime? OpenBetaDate { get; set; }
        [DataMember]
        public byte Status { get; set; }
        [DataMember]
        public int? SortOrder { get; set; }
        [DataMember]
        public int Views { get; set; }
    }
}
