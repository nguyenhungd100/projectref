﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.GameProfile.Entity
{
    [DataContract]
    public class GateGameRatiesEntity : EntityBase
    {
        [DataMember]
        public  int GameId { set; get; }
        [DataMember]
        public  int Vote_Mark1 { set; get; }
        [DataMember]
        public  int Vote_Mark1_5 { set; get; }
        [DataMember]
        public  int Vote_Mark2 { set; get; }
        [DataMember]
        public  int Vote_Mark2_5 { set; get; }
        [DataMember]
        public  int Vote_Mark3 { set; get; }
        [DataMember]
        public  int Vote_Mark3_5 { set; get; }
        [DataMember]
        public  int Vote_Mark4 { set; get; }
        [DataMember]
        public  int Vote_Mark4_5 { set; get; }
        [DataMember]
        public  int Vote_Mark5 { set; get; }
        [DataMember]
        public  int Vote_Mark5_5 { set; get; }
        [DataMember]
        public  int Vote_Mark6 { set; get; }
        [DataMember]
        public  int Vote_Mark6_5 { set; get; }
        [DataMember]
        public  int Vote_Mark7 { set; get; }
        [DataMember]
        public  int Vote_Mark7_5 { set; get; }
        [DataMember]
        public  int Vote_Mark8 { set; get; }
        [DataMember]
        public  int Vote_Mark8_5 { set; get; }
        [DataMember]
        public  int Vote_Mark9 { set; get; }
        [DataMember]
        public  int Vote_Mark9_5 { set; get; }
        [DataMember]
        public  int Vote_Mark10 { set; get; }
        [DataMember]
        public  double AverageMark { set; get; }
        [DataMember]
        public  long VoteTotal { set; get; }
        [DataMember]
        public  double MarkTotal { set; get; }
        [DataMember]
        public  long UniqueVote { set; get; }
        [DataMember]
        public  DateTime LastestVoteDate { set; get; }
        [DataMember]
        public double LastestMark { set; get; }
    }
}