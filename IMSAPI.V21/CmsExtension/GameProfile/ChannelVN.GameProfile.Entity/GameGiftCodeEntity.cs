﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.GameProfile.Entity
{
    public class GameGiftCodeEntity : EntityBase
    {
        [DataMember]
        public int UsedCode { get; set; }
        [DataMember]
        public int FreeCode { get; set; }
    }
}
