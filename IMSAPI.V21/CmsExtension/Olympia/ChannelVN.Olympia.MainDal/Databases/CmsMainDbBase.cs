﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Olympia.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region OlympiaWinner

        private OlympiaWinnerDal _olympiaWinnerMainDal;
        public OlympiaWinnerDal OlympiaWinnerMainDal
        {
            get { return _olympiaWinnerMainDal ?? (_olympiaWinnerMainDal = new OlympiaWinnerDal((CmsMainDb)this)); }
        }

        #endregion

        #region Olympia Calendar

        private OlympiaCalendarDal _olympiaCalendarMainDal;
        public OlympiaCalendarDal OlympiaCalendarMainDal
        {
            get { return _olympiaCalendarMainDal ?? (_olympiaCalendarMainDal = new OlympiaCalendarDal((CmsMainDb)this)); }
        }

        #endregion

        #region OlympiaCompetition

        private OlympiaCompetitionDal _olympiaCompetitionMainDal;
        public OlympiaCompetitionDal OlympiaCompetitionMainDal
        {
            get { return _olympiaCompetitionMainDal ?? (_olympiaCompetitionMainDal = new OlympiaCompetitionDal((CmsMainDb)this)); }
        }

        #endregion

        #region OlympiaMedia

        private OlympiaMediaDal _olympiaMediaMainDal;
        public OlympiaMediaDal OlympiaMediaMainDal
        {
            get { return _olympiaMediaMainDal ?? (_olympiaMediaMainDal = new OlympiaMediaDal((CmsMainDb)this)); }
        }

        #endregion

        #region OlympiaPlayer

        private OlympiaPlayerDal _olympiaPlayerMainDal;
        public OlympiaPlayerDal OlympiaPlayerMainDal
        {
            get { return _olympiaPlayerMainDal ?? (_olympiaPlayerMainDal = new OlympiaPlayerDal((CmsMainDb)this)); }
        }

        #endregion

        #region OlympiaSchedule

        private OlympiaScheduleDal _olympiaScheduleMainDal;
        public OlympiaScheduleDal OlympiaScheduleMainDal
        {
            get { return _olympiaScheduleMainDal ?? (_olympiaScheduleMainDal = new OlympiaScheduleDal((CmsMainDb)this)); }
        }

        #endregion

        #region OlympiaScore

        private OlympiaScoreDal _olympiaScoreMainDal;
        public OlympiaScoreDal OlympiaScoreMainDal
        {
            get { return _olympiaScoreMainDal ?? (_olympiaScoreMainDal = new OlympiaScoreDal((CmsMainDb)this)); }
        }

        #endregion


        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}