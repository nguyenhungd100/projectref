﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.MainDal.Databases;


namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaCompetitionDalBase
    {
        public List<OlympiaCompetitionEntity> GetList(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Olympia_Competition_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", Keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaCompetitionEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(OlympiaCompetitionEntity elm, ref int id)
        {
            const string commandText = "CMS_Olympia_Competition_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Date", elm.Date);
                _db.AddParameter(cmd, "Description", elm.Description);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertResult(OlympiaResultEntity elm, ref int id)
        {
            const string commandText = "CMS_Olympia_Result_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "CompetitionID", elm.CompetitionID);
                _db.AddParameter(cmd, "PlayerID", elm.PlayerID);
                _db.AddParameter(cmd, "Score", elm.Score);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(OlympiaCompetitionEntity elm)
        {
            const string commandText = "CMS_Olympia_Competition_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Name", elm.Name);
                _db.AddParameter(cmd, "Date", elm.Date);
                _db.AddParameter(cmd, "Description", elm.Description);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public OlympiaCompetitionEntity Select(int Id)
        {
            const string commandText = "CMS_Olympia_Competition_Select";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<OlympiaCompetitionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_Olympia_Competition_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteResult(int competitionID)
        {
            const string commandText = "CMS_Olympia_Result_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "competitionID", competitionID);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaCompetitionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
