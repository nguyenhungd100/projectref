﻿using System;
using System.Collections.Generic;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.MainDal.Databases;


namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaCalendarDalBase
    {
        public List<OlympiaCalendarEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Olympia_Calendar_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaCalendarEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(OlympiaCalendarEntity elm, ref int id)
        {
            const string commandText = "CMS_Olympia_Calendar_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Date", elm.Date);
                _db.AddParameter(cmd, "Info", elm.Info);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(OlympiaCalendarEntity elm)
        {
            const string commandText = "CMS_Olympia_Calendar_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "Date", elm.Date);
                _db.AddParameter(cmd, "Info", elm.Info);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public OlympiaCalendarEntity Select(int Id)
        {
            const string commandText = "CMS_Olympia_Calendar_Select";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<OlympiaCalendarEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_Olympia_Calendar_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaCalendarDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
