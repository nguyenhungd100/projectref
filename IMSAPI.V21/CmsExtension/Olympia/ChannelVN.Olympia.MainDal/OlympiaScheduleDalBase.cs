﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.MainDal.Databases;


namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaScheduleDalBase
    {

        public List<OlympiaScheduleEntity> GetListSchedule(string keyword, int ScheduleType, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Olympia_Schedule_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "ScheduleType", ScheduleType);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaScheduleEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(OlympiaScheduleEntity elm, ref int id)
        {
            const string commandText = "CMS_Olympia_Schedule_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ScheduleType", elm.ScheduleType);
                _db.AddParameter(cmd, "ScheduleDate", elm.ScheduleDate);
                _db.AddParameter(cmd, "ScheduleNote", elm.ScheduleNote);
                _db.AddParameter(cmd, "ScheduleUrl", elm.ScheduleUrl);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(OlympiaScheduleEntity elm)
        {
            const string commandText = "CMS_Olympia_Schedule_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "ScheduleType", elm.ScheduleType);
                _db.AddParameter(cmd, "ScheduleDate", elm.ScheduleDate);
                _db.AddParameter(cmd, "ScheduleNote", elm.ScheduleNote);
                _db.AddParameter(cmd, "ScheduleUrl", elm.ScheduleUrl);
                _db.AddParameter(cmd, "Status", elm.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public OlympiaScheduleEntity Select(int Id)
        {
            const string commandText = "CMS_Olympia_Schedule_Select";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<OlympiaScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_Olympia_Schedule_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
