﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaWinnerDalBase
    {
        #region Update

        public bool UpdateOlympiaWinner(OlympiaWinnerEntity olympiaWinner)
        {
            const string commandText = "CMS_OlympiaWinner_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", olympiaWinner.Id);
                _db.AddParameter(cmd, "Status", olympiaWinner.Status);
                _db.AddParameter(cmd, "WinnerAvatar", olympiaWinner.WinnerAvatar);
                _db.AddParameter(cmd, "WinnerDate", olympiaWinner.WinnerDate);
                _db.AddParameter(cmd, "WinnerName", olympiaWinner.WinnerName);
                _db.AddParameter(cmd, "WinnerNote", olympiaWinner.WinnerNote);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteOlympiaWinnerById(int id)
        {
            const string commandText = "CMS_OlympiaWinner_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Get

        public OlympiaWinnerEntity GetOlympiaWinnerById(int id)
        {
            const string commandText = "CMS_OlympiaWinner_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<OlympiaWinnerEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<OlympiaWinnerEntity> SearchOlympiaWinner(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_OlympiaWinner_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaWinnerEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaWinnerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
