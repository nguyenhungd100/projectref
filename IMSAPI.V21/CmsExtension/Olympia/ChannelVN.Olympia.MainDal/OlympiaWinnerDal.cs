﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.MainDal
{
    public class OlympiaWinnerDal : OlympiaWinnerDalBase
    {
        internal OlympiaWinnerDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
