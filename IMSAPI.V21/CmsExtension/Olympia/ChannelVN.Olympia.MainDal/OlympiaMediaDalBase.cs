﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;

using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaMediaDalBase
    {
        #region Update

        public bool UpdateOlympiaMedia(OlympiaMediaEntity olympiaMedia)
        {
            const string commandText = "CMS_OlympiaMedia_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", olympiaMedia.Id);
                _db.AddParameter(cmd, "Title", olympiaMedia.Title);
                _db.AddParameter(cmd, "MediaType", olympiaMedia.MediaType);
                _db.AddParameter(cmd, "Year", olympiaMedia.Year);
                _db.AddParameter(cmd, "Quarter", olympiaMedia.Quarter);
                _db.AddParameter(cmd, "Month", olympiaMedia.Month);
                _db.AddParameter(cmd, "Week", olympiaMedia.Week);
                _db.AddParameter(cmd, "MediaThumb", olympiaMedia.MediaThumb);
                _db.AddParameter(cmd, "MediaSrc", olympiaMedia.MediaSrc);
                _db.AddParameter(cmd, "MediaDate", olympiaMedia.MediaDate);
                _db.AddParameter(cmd, "Status", olympiaMedia.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateOlympiaMediaStatus(int id, EnumOlympiaMediaStatus status)
        {
            const string commandText = "CMS_OlympiaMedia_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", (int)status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Get

        public OlympiaMediaEntity GetOlympiaMediaById(int id)
        {
            const string commandText = "CMS_OlympiaMedia_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<OlympiaMediaEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<OlympiaMediaEntity> SearchOlympiaMedia(string keyword, int year, int quarter, int month, int week, EnumOlympiaMediaType mediaType, EnumOlympiaMediaStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_OlympiaMedia_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Year", year);
                _db.AddParameter(cmd, "Quarter", quarter);
                _db.AddParameter(cmd, "Month", month);
                _db.AddParameter(cmd, "Week", week);
                _db.AddParameter(cmd, "MediaType", (int)mediaType);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaMediaEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaMediaDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
