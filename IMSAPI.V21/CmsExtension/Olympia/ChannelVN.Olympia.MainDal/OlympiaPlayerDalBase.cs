﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.MainDal.Databases;


namespace ChannelVN.Olympia.MainDal
{
    public abstract class OlympiaPlayerDalBase
    {
        public List<OlympiaPlayerEntity> GetList(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Olympia_Player_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", Keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<OlympiaPlayerEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<OlympiaPlayerEntity> ListByCompetition(int CompetitionID)
        {
            const string commandText = "CMS_Olympia_Player_ListByCompetition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CompetitionID", CompetitionID);
                var numberOfRow = _db.GetList<OlympiaPlayerEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<OlympiaPlayerEntity> ListByPlayer(int playerId)
        {
            const string commandText = "CMS_Olympia_Player_ListByPlayer";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlayerId", playerId);
                var numberOfRow = _db.GetList<OlympiaPlayerEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(OlympiaPlayerEntity elm, ref int id)
        {
            const string commandText = "CMS_Olympia_Player_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "CodeNo", elm.CodeNo);
                _db.AddParameter(cmd, "FullName", elm.FullName);
                _db.AddParameter(cmd, "Birthday", elm.Birthday);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "School", elm.School);
                _db.AddParameter(cmd, "Description", elm.Description);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(OlympiaPlayerEntity elm)
        {
            const string commandText = "CMS_Olympia_Player_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", elm.Id);
                _db.AddParameter(cmd, "CodeNo", elm.CodeNo);
                _db.AddParameter(cmd, "FullName", elm.FullName);
                _db.AddParameter(cmd, "Birthday", elm.Birthday);
                _db.AddParameter(cmd, "Avatar", elm.Avatar);
                _db.AddParameter(cmd, "School", elm.School);
                _db.AddParameter(cmd, "Description", elm.Description);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public OlympiaPlayerEntity Select(int Id)
        {
            const string commandText = "CMS_Olympia_Player_Select";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<OlympiaPlayerEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int Id)
        {
            const string commandText = "CMS_Olympia_Player_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected OlympiaPlayerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
