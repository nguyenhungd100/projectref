﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaFaqEntity : EntityBase
    {
        [DataMember]
        public Int64 Id { get; set; }
        [DataMember]
        public string Question { get; set; }
        [DataMember]
        public string Answer { get; set; }
        [DataMember]
        public string SenderEmail { get; set; }
        [DataMember]
        public string SenderFullName { get; set; }
        [DataMember]
        public string ApproveBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
    }
}
