﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaCompetitionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime Date { get; set; }       
        [DataMember]
        public string Description { get; set; }   
    }

    [DataContract]
    public class OlympiaResultEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int CompetitionID { get; set; }
        [DataMember]
        public int PlayerID { get; set; }
        [DataMember]
        public int Score { get; set; }
    }  
}
