﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaWinnerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string WinnerName { get; set; }
        [DataMember]
        public string WinnerNote { get; set; }
        [DataMember]
        public string WinnerAvatar { get; set; }
        [DataMember]
        public DateTime WinnerDate { get; set; }
        [DataMember]
        public int Status { get; set; } 
    }
}

