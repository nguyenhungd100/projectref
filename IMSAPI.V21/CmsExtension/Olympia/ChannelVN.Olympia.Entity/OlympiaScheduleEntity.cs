﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaScheduleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ScheduleType { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public string ScheduleNote { get; set; }
        [DataMember]
        public string ScheduleUrl { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
