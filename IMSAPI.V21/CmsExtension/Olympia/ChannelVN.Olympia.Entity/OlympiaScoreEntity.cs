﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Olympia.Entity
{
   [DataContract]
    public class OlympiaScoreEntity  :  EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]     
        public string PlayerName { get; set; }
        [DataMember]
        public int PlayerScore { get; set; }
        [DataMember]       
        public int Status { get; set; }        
    }
}
