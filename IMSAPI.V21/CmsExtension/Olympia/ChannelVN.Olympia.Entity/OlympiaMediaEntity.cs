﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaMediaEntity : EntityBase
    {
        /*
         MediaType,
			[Year],
			[Quarter],
			[Month],
			[Week],
			MediaThumb,
			MediaSrc,
			MediaDate,
			[Status]*/
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int MediaType { get; set; }
        [DataMember]
        public int Year { get; set; }
        [DataMember]
        public int Quarter { get; set; }
        [DataMember]
        public int Month { get; set; } 
        [DataMember]
        public int Week { get; set; } 
        [DataMember]
        public string MediaThumb { get; set; }
        [DataMember]
        public string MediaSrc { get; set; }
        [DataMember]
        public DateTime MediaDate { get; set; }
        [DataMember]
        public int Status { get; set; } 
    }
    [DataContract]
    public enum EnumOlympiaMediaType : int
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        NormalPhoto = 1,
        [EnumMember]
        GifPhoto = 2,
        [EnumMember]
        YoutubeVideo = 3,
        [EnumMember]
        TVCVideo = 4,
    }
    [DataContract]
    public enum EnumOlympiaMediaStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Temporary = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        UnPublished = 3,
    }
}

