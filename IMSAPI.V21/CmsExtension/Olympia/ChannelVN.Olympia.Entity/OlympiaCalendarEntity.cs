﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaCalendarEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string Info { get; set; }        
        [DataMember]
        public int Status { get; set; }
    }
}
