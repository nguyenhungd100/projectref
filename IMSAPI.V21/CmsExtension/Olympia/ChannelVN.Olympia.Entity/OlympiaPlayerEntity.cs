﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.Olympia.Entity
{
    [DataContract]
    public class OlympiaPlayerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string CodeNo { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string School { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Score { get; set; }
        [DataMember]
        public int PlayerID { get; set; }
        [DataMember]
        public string NameCompetition { get; set; }
    }
}
