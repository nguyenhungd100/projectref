﻿using System;
using System.Collections.Generic;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaFaqDal
    {
        public static List<OlympiaFaqEntity> List(string keyword,int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_Olympia_FAQ_List";
            try
            {
                var parameters = new[]
                    {       
                        new SqlParameter("@Keyword", keyword),
                        new SqlParameter("@Status", status),
                        new SqlParameter("@PageIndex", pageIndex),
                        new SqlParameter("@PageSize", pageSize),
                        new SqlParameter("@TotalRow", totalRow) {Direction = ParameterDirection.Output}
                    };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);
                var reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                    CommandType.StoredProcedure,
                    DbCommon.DatabaseSchema + storeProcedure,
                    parameters);

                var list = new List<OlympiaFaqEntity>();
                while (reader.Read())
                {
                    var entity = new OlympiaFaqEntity();
                    EntityBase.SetObjectValue(reader, ref entity);
                    list.Add(entity);
                }
                reader.Close();
                reader.Dispose();

                var totalValue = parameters[parameters.Length - 1].Value;
                totalValue = (totalValue == null || (string.IsNullOrWhiteSpace(totalValue.ToString()))) ? "0" : totalValue;
                totalRow = int.Parse(totalValue.ToString());

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + storeProcedure + ":{0}", ex.Message));
            }
        }
        public static bool Insert(OlympiaFaqEntity elm, ref Int64 id)
        {
            const string storeProcedure = "CMS_Olympia_FAQ_Insert";

            try
            {
                id = 0;
                var parameters = new[]
                    {
                        new SqlParameter("@Question", elm.Question),
                        new SqlParameter("@Answer", elm.Answer),
                        new SqlParameter("@SenderEmail", elm.SenderEmail),
                        new SqlParameter("@SenderFullName", elm.SenderFullName),
                        new SqlParameter("@ApproveBy", elm.ApproveBy),
                        new SqlParameter("@Status", elm.Status),
                        new SqlParameter("@CreatedDate", elm.CreatedDate),              
                        new SqlParameter("@Id", id) {Direction = ParameterDirection.Output}
                    };

                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + storeProcedure,
                                                          parameters);

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                            CommandType.StoredProcedure,
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);

                id = int.Parse(parameters[parameters.Length - 1].Value.ToString());

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public static bool Update(OlympiaFaqEntity elm)
        {
            const string storeProcedure = "CMS_Olympia_FAQ_Update";
            try
            {
                var parameters = new[]
                    {   new SqlParameter("@Id", elm.Id),
                        new SqlParameter("@Question", elm.Question),
                        new SqlParameter("@Answer", elm.Answer),
                        new SqlParameter("@SenderEmail", elm.SenderEmail),
                        new SqlParameter("@SenderFullName", elm.SenderFullName),
                        new SqlParameter("@ApproveBy", elm.ApproveBy),
                        new SqlParameter("@Status", elm.Status),
                        new SqlParameter("@LastModifiedDate", elm.LastModifiedDate),
                        (elm.PublishedDate == DateTime.MinValue ? 
                            new SqlParameter("@PublishedDate", DBNull.Value):
                            new SqlParameter("@PublishedDate", elm.PublishedDate)
                        ),
                        new SqlParameter("@DeletedBy", elm.DeletedBy),                 
                          (elm.DeletedDate == DateTime.MinValue ? 
                            new SqlParameter("@DeletedDate", DBNull.Value):
                            new SqlParameter("@DeletedDate", elm.DeletedDate)
                        )
                    };

                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + storeProcedure,
                                                          parameters);

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                            CommandType.StoredProcedure,
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);

                var id = elm.Id;

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public static OlympiaFaqEntity Select(Int64 id)
        {
            const string storeProcedure = "CMS_Olympia_Faq_Select";
            try
            {
                var parameters = new[]
                    {       
                        new SqlParameter("@Id", id)
                    };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);
                var reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                    CommandType.StoredProcedure,
                    DbCommon.DatabaseSchema + storeProcedure,
                    parameters);

                var el = new OlympiaFaqEntity();
                if (reader.Read())
                {
                    var entity = new OlympiaFaqEntity();
                    EntityBase.SetObjectValue(reader, ref entity);
                    el = entity;
                }
                reader.Close();
                reader.Dispose();
                return el;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + storeProcedure + ":{0}", ex.Message));
            }
        }
        public static bool Delete(int id)
        {
            const string storeProcedure = "CMS_Olympia_FAQ_Delete";
            try
            {               
                var parameters = new[]
                    {   new SqlParameter("@Id", id)
                    };

                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + storeProcedure,
                                                          parameters);

                var numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                            CommandType.StoredProcedure,
                                                            DbCommon.DatabaseSchema + storeProcedure,
                                                            parameters);           

                return numberOfRow > 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "{0}:{1}", storeProcedure, ex.Message));
            }
        }
    }
}
