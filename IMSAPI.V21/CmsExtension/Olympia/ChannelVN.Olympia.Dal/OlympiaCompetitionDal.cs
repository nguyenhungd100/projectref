﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaCompetitionDal
    {
        public static List<OlympiaCompetitionEntity> GetList(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaCompetitionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.GetList(Keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(OlympiaCompetitionEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool InsertResult(OlympiaResultEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.InsertResult(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(OlympiaCompetitionEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.Update(elm);
            }
            return returnValue;
        }
        public static OlympiaCompetitionEntity Select(int Id)
        {
            OlympiaCompetitionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.Select(Id);
            }
            return returnValue;
        }
        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.Delete(Id);
            }
            return returnValue;
        }
        public static bool DeleteResult(int competitionID)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCompetitionMainDal.DeleteResult(competitionID);
            }
            return returnValue;
        }
    }
}
