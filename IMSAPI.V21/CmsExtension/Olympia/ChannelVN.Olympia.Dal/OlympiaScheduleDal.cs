﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaScheduleDal
    {
        public static List<OlympiaScheduleEntity> GetListSchedule(string keyword, int ScheduleType, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScheduleMainDal.GetListSchedule(keyword, ScheduleType, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(OlympiaScheduleEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScheduleMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(OlympiaScheduleEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScheduleMainDal.Update(elm);
            }
            return returnValue;
        }
        public static OlympiaScheduleEntity Select(int Id)
        {
            OlympiaScheduleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScheduleMainDal.Select(Id);
            }
            return returnValue;
        }
        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScheduleMainDal.Delete(Id);
            }
            return returnValue;
        }
    }
}
