﻿using System;
using System.Collections.Generic;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaPlayerDal
    {
        public static List<OlympiaPlayerEntity> GetList(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaPlayerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.GetList(Keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<OlympiaPlayerEntity> ListByCompetition(int CompetitionID)
        {
            List<OlympiaPlayerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.ListByCompetition(CompetitionID);
            }
            return returnValue;
        }
        public static List<OlympiaPlayerEntity> ListByPlayer(int playerId)
        {
            List<OlympiaPlayerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.ListByPlayer(playerId);
            }
            return returnValue;
        }
        public static bool Insert(OlympiaPlayerEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(OlympiaPlayerEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.Update(elm);
            }
            return returnValue;
        }
        public static OlympiaPlayerEntity Select(int Id)
        {
            OlympiaPlayerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.Select(Id);
            }
            return returnValue;
        }
        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaPlayerMainDal.Delete(Id);
            }
            return returnValue;
        }
    }
}
