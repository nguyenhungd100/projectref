﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaScoreDal
    {

        public static List<OlympiaScoreEntity> GetListScore(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaScoreEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScoreMainDal.GetListScore(Keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(OlympiaScoreEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScoreMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(OlympiaScoreEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScoreMainDal.Update(elm);
            }
            return returnValue;
        }
        public static OlympiaScoreEntity Select(int Id)
        {
            OlympiaScoreEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScoreMainDal.Select(Id);
            }
            return returnValue;
        }
        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaScoreMainDal.Delete(Id);
            }
            return returnValue;
        }
    }
}
