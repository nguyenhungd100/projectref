﻿using System;
using System.Collections.Generic;
using ChannelVN.Olympia.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaCalendarDal
    {

        public static List<OlympiaCalendarEntity> GetList(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaCalendarEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCalendarMainDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool Insert(OlympiaCalendarEntity elm, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCalendarMainDal.Insert(elm, ref id);
            }
            return returnValue;
        }
        public static bool Update(OlympiaCalendarEntity elm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCalendarMainDal.Update(elm);
            }
            return returnValue;
        }
        public static OlympiaCalendarEntity Select(int Id)
        {
            OlympiaCalendarEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCalendarMainDal.Select(Id);
            }
            return returnValue;
        }
        public static bool Delete(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaCalendarMainDal.Delete(Id);
            }
            return returnValue;
        }
    }
}
