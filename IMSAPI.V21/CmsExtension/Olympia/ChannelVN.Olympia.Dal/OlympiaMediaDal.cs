﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaMediaDal
    {
        #region Update

        public static bool UpdateOlympiaMedia(OlympiaMediaEntity olympiaMedia)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaMediaMainDal.UpdateOlympiaMedia(olympiaMedia);
            }
            return returnValue;
        }
        public static bool UpdateOlympiaMediaStatus(int id, EnumOlympiaMediaStatus status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaMediaMainDal.UpdateOlympiaMediaStatus(id, status);
            }
            return returnValue;
        }
        #endregion

        #region Get

        public static OlympiaMediaEntity GetOlympiaMediaById(int id)
        {
            OlympiaMediaEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaMediaMainDal.GetOlympiaMediaById(id);
            }
            return returnValue;
        }
        public static List<OlympiaMediaEntity> SearchOlympiaMedia(string keyword, int year, int quarter, int month, int week, EnumOlympiaMediaType mediaType, EnumOlympiaMediaStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaMediaEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaMediaMainDal.SearchOlympiaMedia(keyword, year, quarter, month, week, mediaType, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion
    }
}
