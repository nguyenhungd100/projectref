﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.Olympia.Dal.Common;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.MainDal.Databases;

namespace ChannelVN.Olympia.Dal
{
    public class OlympiaWinnerDal
    {
        #region Update

        public static bool UpdateOlympiaWinner(OlympiaWinnerEntity olympiaWinner)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaWinnerMainDal.UpdateOlympiaWinner(olympiaWinner);
            }
            return returnValue;
        }
        public static bool DeleteOlympiaWinnerById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaWinnerMainDal.DeleteOlympiaWinnerById(id);
            }
            return returnValue;
        }
        #endregion

        #region Get

        public static OlympiaWinnerEntity GetOlympiaWinnerById(int id)
        {
            OlympiaWinnerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaWinnerMainDal.GetOlympiaWinnerById(id);
            }
            return returnValue;
        }
        public static List<OlympiaWinnerEntity> SearchOlympiaWinner(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<OlympiaWinnerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.OlympiaWinnerMainDal.SearchOlympiaWinner(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion
    }
}
