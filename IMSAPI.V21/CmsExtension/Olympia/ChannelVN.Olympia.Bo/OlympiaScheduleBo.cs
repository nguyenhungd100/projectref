﻿using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaScheduleBo
    {
        public static List<OlympiaScheduleEntity> GetListSchedule(string keyword, int ScheduleType, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaScheduleDal.GetListSchedule(keyword, ScheduleType,pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertSchedule(OlympiaScheduleEntity elm, ref int Id)
        {
            return OlympiaScheduleDal.Insert(elm, ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateSchedule(OlympiaScheduleEntity elm)
        {
            return OlympiaScheduleDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static OlympiaScheduleEntity SelectSchedule(int Id)
        {
            return OlympiaScheduleDal.Select(Id);
        }
        public static ErrorMapping.ErrorCodes DeleteSchedule(int Id)
        {
            return OlympiaScheduleDal.Delete(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
