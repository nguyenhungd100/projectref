﻿using System;
using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System.Collections.Generic;   

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaFaqBo
    {
        public static List<OlympiaFaqEntity> ListFaq(string keyword,int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaFaqDal.List(keyword,status, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertFaq(OlympiaFaqEntity elm, ref Int64 id)
        {
            return OlympiaFaqDal.Insert(elm, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateFaq(OlympiaFaqEntity elm)
        {
            return OlympiaFaqDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static OlympiaFaqEntity SelectFaq(Int64 id)
        {
            return OlympiaFaqDal.Select(id);
        }
        public static ErrorMapping.ErrorCodes DeleteFaq(int id)
        {
            return OlympiaFaqDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
