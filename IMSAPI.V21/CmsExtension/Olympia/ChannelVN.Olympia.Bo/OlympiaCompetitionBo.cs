﻿using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaCompetitionBo
    {
        public static List<OlympiaCompetitionEntity> GetListCompetition(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaCompetitionDal.GetList(Keyword, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertCompetition(OlympiaCompetitionEntity elm, ref int Id)
        {
            return OlympiaCompetitionDal.Insert(elm, ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes InsertResult(OlympiaResultEntity elm, ref int Id)
        {
            return OlympiaCompetitionDal.InsertResult(elm, ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateCompetition(OlympiaCompetitionEntity elm)
        {
            return OlympiaCompetitionDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static OlympiaCompetitionEntity SelectCompetition(int Id)
        {
            return OlympiaCompetitionDal.Select(Id);
        }
        public static ErrorMapping.ErrorCodes DeleteCompetition(int Id)
        {
            return OlympiaCompetitionDal.Delete(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteResult(int competitionID)
        {
            return OlympiaCompetitionDal.DeleteResult(competitionID) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
