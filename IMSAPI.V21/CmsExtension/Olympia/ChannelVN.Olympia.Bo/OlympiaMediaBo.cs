﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaMediaBO
    {
        #region OlympiaMedia

        #region Update

        public static ErrorMapping.ErrorCodes UpdateOlympiaMedia(OlympiaMediaEntity olympiaMedia)
        {
            try
            {
                if (null == olympiaMedia)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return OlympiaMediaDal.UpdateOlympiaMedia(olympiaMedia) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateOlympiaMediaStatus(int id, EnumOlympiaMediaStatus status)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return OlympiaMediaDal.UpdateOlympiaMediaStatus(id, status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static OlympiaMediaEntity GetOlympiaMediaById(int id)
        {
            try
            {
                return OlympiaMediaDal.GetOlympiaMediaById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<OlympiaMediaEntity> SearchOlympiaMedia(string keyword, int year, int quarter, int month, int week, EnumOlympiaMediaType mediaType, EnumOlympiaMediaStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return OlympiaMediaDal.SearchOlympiaMedia(keyword, year, quarter, month, week, mediaType, status,
                                                          pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<OlympiaMediaEntity>();
            }
        }

        #endregion

        #endregion
    }
}
