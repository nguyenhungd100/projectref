﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaWinnerBO
    {
        #region OlympiaWinner

        #region Update

        public static ErrorMapping.ErrorCodes UpdateOlympiaWinner(OlympiaWinnerEntity olympiaMedia)
        {
            try
            {
                if (null == olympiaMedia)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return OlympiaWinnerDal.UpdateOlympiaWinner(olympiaMedia) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteOlympiaWinnerById(int id)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return OlympiaWinnerDal.DeleteOlympiaWinnerById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static OlympiaWinnerEntity GetOlympiaWinnerById(int id)
        {
            try
            {
                return OlympiaWinnerDal.GetOlympiaWinnerById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<OlympiaWinnerEntity> SearchOlympiaWinner(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return OlympiaWinnerDal.SearchOlympiaWinner(keyword, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<OlympiaWinnerEntity>();
            }
        }

        #endregion

        #endregion
    }
}
