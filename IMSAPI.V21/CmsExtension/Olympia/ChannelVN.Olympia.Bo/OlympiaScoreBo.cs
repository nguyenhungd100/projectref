﻿using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaScoreBo
    {
        public static List<OlympiaScoreEntity> GetListScore(string Keyword,int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaScoreDal.GetListScore(Keyword,pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes InsertScore(OlympiaScoreEntity elm,ref int Id)
        {
            return OlympiaScoreDal.Insert(elm,ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateScore(OlympiaScoreEntity elm)
        {
            return OlympiaScoreDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static OlympiaScoreEntity SelectScore(int Id)
        {
            return OlympiaScoreDal.Select(Id);
        }
        public static ErrorMapping.ErrorCodes DeleteScore(int Id)
        {
            return OlympiaScoreDal.Delete(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
