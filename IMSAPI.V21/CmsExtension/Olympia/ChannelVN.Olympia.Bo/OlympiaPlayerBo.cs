﻿using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System.Collections.Generic;

namespace ChannelVN.Olympia.Bo
{
    public class OlympiaPlayerBo
    {
        public static List<OlympiaPlayerEntity> GetListPlayer(string Keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaPlayerDal.GetList(Keyword, pageIndex, pageSize, ref totalRow);
        }
        public static List<OlympiaPlayerEntity> GetListPlayerByCompetition(int CompetitionID)
        {
            return OlympiaPlayerDal.ListByCompetition(CompetitionID);
        }
        public static List<OlympiaPlayerEntity> GetListPlayerByPlayer(int playerId)
        {
            return OlympiaPlayerDal.ListByPlayer(playerId);
        }
        public static ErrorMapping.ErrorCodes InsertPlayer(OlympiaPlayerEntity elm, ref int Id)
        {
            return OlympiaPlayerDal.Insert(elm, ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdatePlayer(OlympiaPlayerEntity elm)
        {
            return OlympiaPlayerDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static OlympiaPlayerEntity SelectPlayer(int Id)
        {
            return OlympiaPlayerDal.Select(Id);
        }
        public static ErrorMapping.ErrorCodes DeletePlayer(int Id)
        {
            return OlympiaPlayerDal.Delete(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
