﻿using ChannelVN.Olympia.Dal;
using ChannelVN.Olympia.Entity;
using ChannelVN.Olympia.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Olympia.Bo
{
   public class OlympiaCalendarBo
    {
       public static List<OlympiaCalendarEntity> GetListCalendar(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return OlympiaCalendarDal.GetList(keyword, pageIndex, pageSize, ref totalRow);
        }
       public static ErrorMapping.ErrorCodes InsertCalendar(OlympiaCalendarEntity elm, ref int Id)
        {
            return OlympiaCalendarDal.Insert(elm, ref Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
       public static ErrorMapping.ErrorCodes UpdateCalendar(OlympiaCalendarEntity elm)
        {
            return OlympiaCalendarDal.Update(elm) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
       public static OlympiaCalendarEntity SelectCalendar(int Id)
        {
            return OlympiaCalendarDal.Select(Id);
        }
       public static ErrorMapping.ErrorCodes DeleteCalendar(int Id)
        {
            return OlympiaCalendarDal.Delete(Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
