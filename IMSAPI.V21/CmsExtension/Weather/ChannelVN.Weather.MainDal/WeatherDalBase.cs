﻿using ChannelVN.Weather.Entity;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.Weather.MainDal.Databases;

namespace ChannelVN.Weather.MainDal
{
    public abstract class WeatherDalBase
    {
        #region Get
        public List<WeatherEntity> GetAll()
        {
            const string commandText = "CMS_Weather_GetAll"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<WeatherEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public WeatherEntity GetByDate(DateTime weatherDate)
        {
            const string commandText = "CMS_Weather_GetByDate"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "WeatherDate", weatherDate);
                var numberOfRow = _db.Get<WeatherEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public WeatherEntity GetById(int Id)
        {
            const string commandText = "CMS_Weather_GetById"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<WeatherEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Set
        public bool Insert(WeatherEntity info)
        {
            const string commandText = "CMS_Weather_Insert"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CodeID", info.CodeId);
                _db.AddParameter(cmd, "WeatherDate", info.WeatherDate);
                _db.AddParameter(cmd, "LocationName", info.LocationName);
                _db.AddParameter(cmd, "Humidity", info.Humidity);
                _db.AddParameter(cmd, "Wind", info.Wind);
                _db.AddParameter(cmd, "Sunset", info.Sunset);
                _db.AddParameter(cmd, "Sunrise", info.Sunrise);
                _db.AddParameter(cmd, "Temp", info.Temp);
                _db.AddParameter(cmd, "Hight", info.Hight);
                _db.AddParameter(cmd, "Low", info.Low);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(WeatherEntity info)
        {
            const string commandText = "CMS_Weather_Update"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CodeID", info.CodeId);
                _db.AddParameter(cmd, "WeatherDate", info.WeatherDate);
                _db.AddParameter(cmd, "LocationName", info.LocationName);
                _db.AddParameter(cmd, "Humidity", info.Humidity);
                _db.AddParameter(cmd, "Wind", info.Wind);
                _db.AddParameter(cmd, "Sunset", info.Sunset);
                _db.AddParameter(cmd, "Sunrise", info.Sunrise);
                _db.AddParameter(cmd, "Temp", info.Temp);
                _db.AddParameter(cmd, "Hight", info.Hight);
                _db.AddParameter(cmd, "Low", info.Low);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(int id)
        {
            const string commandText = "CMS_Weather_DeleteByID"; try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected WeatherDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
