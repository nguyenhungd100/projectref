﻿using ChannelVN.Weather.Dal.Common;
using ChannelVN.Weather.Entity;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.Weather.MainDal.Databases;

namespace ChannelVN.Weather.Dal
{
    public class WeatherDal
    {
        #region Get
        public static List<WeatherEntity> GetAll()
        {
            List<WeatherEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.GetAll();
            }
            return returnValue;
        }
        public static WeatherEntity GetByDate(DateTime weatherDate)
        {
            WeatherEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.GetByDate(weatherDate);
            }
            return returnValue;
        }
        public static WeatherEntity GetById(int Id)
        {
            WeatherEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.GetById(Id);
            }
            return returnValue;
        }
        #endregion

        #region Set
        public static bool Insert(WeatherEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.Insert(info);
            }
            return returnValue;
        }
        public static bool Update(WeatherEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.Update(info);
            }
            return returnValue;
        }
        public static bool DeleteById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.WeatherMainDal.DeleteById(id);
            }
            return returnValue;
        }

        #endregion
    }
}
