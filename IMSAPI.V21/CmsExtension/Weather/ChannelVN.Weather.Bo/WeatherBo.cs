﻿using ChannelVN.Weather.Dal;
using ChannelVN.Weather.Entity;
using ChannelVN.Weather.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.Weather.Bo
{
    public class WeatherBo
    {
        public static List<WeatherEntity> GetAllWeather()
        {
            return WeatherDal.GetAll();
        }
        public static WeatherEntity GetWeatherByDate(DateTime weatherDate)
        {
            return WeatherDal.GetByDate(weatherDate);
        }
        public static WeatherEntity GetWeatherById(int id)
        {
            return WeatherDal.GetById(id);
        }
        public virtual ErrorMapping.ErrorCodes DeleteById(int id)
        {
            return WeatherDal.DeleteById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateWeather(WeatherEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = WeatherDal.Update(info);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
