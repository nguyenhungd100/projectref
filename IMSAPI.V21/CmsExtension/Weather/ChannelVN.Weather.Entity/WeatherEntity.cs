﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Weather.Entity
{
    [DataContract]
    public class WeatherEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int CodeId { get; set; }
        [DataMember]
        public DateTime WeatherDate { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public string Humidity { get; set; }        
        [DataMember]
        public string Wind { get; set; }
        [DataMember]
        public string Sunset { get; set; }
        [DataMember]
        public string Sunrise { get; set; }        
        [DataMember]
        public string Temp { get; set; }
        [DataMember]
        public string Hight { get; set; }        
        [DataMember]
        public string Low { get; set; }             
    }
}
