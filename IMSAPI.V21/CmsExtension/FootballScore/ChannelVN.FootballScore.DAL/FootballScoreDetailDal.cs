﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.DAL.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.MainDal.Databases;

namespace ChannelVN.FootballScore.DAL
{
    public class FootballScoreDetailDal
    {
        #region Update

        public static bool InsertFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail, ref int newId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDetailDal.InsertFootballScoreDetail(footballScoreDetail, ref newId);
            }
            return retVal;
        }

        public static bool UpdateFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDetailDal.UpdateFootballScoreDetail(footballScoreDetail);
            }
            return retVal;
        }

        public static bool DeleteFootballScoreDetail(int detailId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDetailDal.DeleteFootballScoreDetail(detailId);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static FootballScoreDetailEntity GetFootballScoreDetailById(int detailId)
        {
            FootballScoreDetailEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDetailDal.GetFootballScoreDetailById(detailId);
            }
            return retVal;
        }

        public static List<FootballScoreDetailEntity> SearchFootballScoreDetail(string keyword, int FootballScoreId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FootballScoreDetailEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDetailDal.SearchFootballScoreDetail(keyword, FootballScoreId, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }

        #endregion
    }
}
