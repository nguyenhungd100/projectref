﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.DAL.Common;
using ChannelVN.FootballScore.MainDal.Databases;

namespace ChannelVN.FootballScore.DAL
{
    public class FootballScoreDal
    {
        #region Update

        public static bool InsertFootballScore(FootballScoreEntity footballScore, ref int newId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.InsertFootballScore(footballScore, ref newId);
            }
            return retVal;
        }
        public static bool UpdateFootballScore(FootballScoreEntity footballScore)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.UpdateFootballScore(footballScore);
            }
            return retVal;
        }

        public static bool DeleteFootballScore(int footballScoreId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.DeleteFootballScore(footballScoreId);
            }
            return retVal;
        }
        public static bool UpdatePriority(string listOfPriority)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.UpdatePriority(listOfPriority);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static FootballScoreEntity GetFootballScoreById(int footballScoreId)
        {
            FootballScoreEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.GetFootballScoreById(footballScoreId);
            }
            return retVal;
        }
        public static List<FootballScoreEntity> SearchFootballScore(string keyword, EnumFootballScoreStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FootballScoreEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootBallScoreDal.SearchFootballScore(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }       

        #endregion
    }
}
