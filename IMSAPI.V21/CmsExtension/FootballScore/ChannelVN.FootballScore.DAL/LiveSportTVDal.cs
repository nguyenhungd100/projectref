﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.DAL.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.MainDal.Databases;

namespace ChannelVN.FootballScore.DAL
{
    public class LiveSportTVDal
    {
        #region Update

        public static bool InsertLiveSportTV(LiveSportTVEntity liveSportTV, ref int newliveSportTVId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.LiveSportMainDal.InsertLiveSportTV(liveSportTV, ref newliveSportTVId);
            }
            return retVal;
        }
        public static bool UpdateLiveSportTV(LiveSportTVEntity liveSportTV)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.LiveSportMainDal.UpdateLiveSportTV(liveSportTV);
            }
            return retVal;
        }

        public static bool DeleteLiveSportTV(int liveSportTVId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.LiveSportMainDal.DeleteLiveSportTV(liveSportTVId);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static LiveSportTVEntity GetLiveSportTVById(int liveSportTVId)
        {
            LiveSportTVEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.LiveSportMainDal.GetLiveSportTVById(liveSportTVId);
            }
            return retVal;
        }
        public static List<LiveSportTVEntity> SearchLiveSportTV(string keyword, EnumLiveSportTVStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveSportTVEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.LiveSportMainDal.SearchLiveSportTV(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        
        #endregion
    }
}
