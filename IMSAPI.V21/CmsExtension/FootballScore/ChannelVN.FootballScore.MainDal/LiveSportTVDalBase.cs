﻿using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.FootballScore.MainDal
{
    public abstract class LiveSportTVDalBase
    {
        #region function SET
        public bool InsertLiveSportTV(LiveSportTVEntity liveSportTV, ref int newliveSportTVId)
        {
            const string commandText = "CMS_LiveSportTV_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveSportTV.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", liveSportTV.Title);
                _db.AddParameter(cmd, "TeamA", liveSportTV.TeamA);
                _db.AddParameter(cmd, "TeamB", liveSportTV.TeamB);
                _db.AddParameter(cmd, "AvatarTeamA", liveSportTV.AvatarTeamA);
                _db.AddParameter(cmd, "AvatarTeamB", liveSportTV.AvatarTeamB);
                _db.AddParameter(cmd, "StartDate", liveSportTV.StartDate);
                _db.AddParameter(cmd, "LiveLink1", liveSportTV.LiveLink1);
                _db.AddParameter(cmd, "LiveLink2", liveSportTV.LiveLink2);
                _db.AddParameter(cmd, "LiveLink3", liveSportTV.LiveLink3);
                _db.AddParameter(cmd, "LiveLink4", liveSportTV.LiveLink4);
                _db.AddParameter(cmd, "LiveLink5", liveSportTV.LiveLink5);
                _db.AddParameter(cmd, "CreatedBy", liveSportTV.CreatedBy);
                _db.AddParameter(cmd, "Status", liveSportTV.Status);
                var result = cmd.ExecuteNonQuery();
                newliveSportTVId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return newliveSportTVId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateLiveSportTV(LiveSportTVEntity liveSportTV)
        {
            const string commandText = "CMS_LiveSportTV_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveSportTV.Id);
                _db.AddParameter(cmd, "Title", liveSportTV.Title);
                _db.AddParameter(cmd, "TeamA", liveSportTV.TeamA);
                _db.AddParameter(cmd, "TeamB", liveSportTV.TeamB);
                _db.AddParameter(cmd, "AvatarTeamA", liveSportTV.AvatarTeamA);
                _db.AddParameter(cmd, "AvatarTeamB", liveSportTV.AvatarTeamB);
                _db.AddParameter(cmd, "StartDate", liveSportTV.StartDate);
                _db.AddParameter(cmd, "LiveLink1", liveSportTV.LiveLink1);
                _db.AddParameter(cmd, "LiveLink2", liveSportTV.LiveLink2);
                _db.AddParameter(cmd, "LiveLink3", liveSportTV.LiveLink3);
                _db.AddParameter(cmd, "LiveLink4", liveSportTV.LiveLink4);
                _db.AddParameter(cmd, "LiveLink5", liveSportTV.LiveLink5);
                _db.AddParameter(cmd, "LastModifiedBy", liveSportTV.LastModifiedBy);
                _db.AddParameter(cmd, "Status", liveSportTV.Status);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteLiveSportTV(int liveSportTVId)
        {
            const string commandText = "CMS_LiveSportTV_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveSportTVId);
                var result = cmd.ExecuteNonQuery();

                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #endregion

        #region function GET
        public LiveSportTVEntity GetLiveSportTVById(int liveSportTVId)
        {
            const string commandText = "CMS_LiveSportTV_GetById";
            try
            {
                LiveSportTVEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveSportTVId);
                data = _db.Get<LiveSportTVEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<LiveSportTVEntity> SearchLiveSportTV(string keyword, EnumLiveSportTVStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveSportTV_Search";
            try
            {
                List<LiveSportTVEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<LiveSportTVEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho LiveSportTVDalBase
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected LiveSportTVDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
