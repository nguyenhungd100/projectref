﻿using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.FootballScore.MainDal
{
    public abstract class FootballScoreDalBase
    {
        #region SET
        public bool InsertFootballScore(FootballScoreEntity footballScore, ref int newId)
        {
            const string commandText = "CMS_FootballScore_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScore.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", footballScore.Name);
                _db.AddParameter(cmd, "Avatar", footballScore.Avatar);
                _db.AddParameter(cmd, "SortOrder", footballScore.SortOrder);
                _db.AddParameter(cmd, "Data", footballScore.Data);
                _db.AddParameter(cmd, "Status", footballScore.Status);
                var result = cmd.ExecuteNonQuery();
                newId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return newId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFootballScore(FootballScoreEntity footballScore) {
            const string commandText = "CMS_FootballScore_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScore.Id);
                _db.AddParameter(cmd, "Name", footballScore.Name);
                _db.AddParameter(cmd, "Avatar", footballScore.Avatar);
                _db.AddParameter(cmd, "SortOrder", footballScore.SortOrder);
                _db.AddParameter(cmd, "Data", footballScore.Data);
                _db.AddParameter(cmd, "Status", footballScore.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFootballScore(int footballScoreId) {
            const string commandText = "CMS_FootballScore_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScoreId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(string listOfPriority) {
            const string commandText = "CMS_FootballScore_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listOfPriority);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region GET
        public FootballScoreEntity GetFootballScoreById(int footballScoreId) {
            const string commandText = "CMS_FootballScore_GetById";
            try
            {
                FootballScoreEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScoreId);
                data = _db.Get<FootballScoreEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FootballScoreEntity> SearchFootballScore(string keyword, EnumFootballScoreStatus status, int pageIndex, int pageSize, ref int totalRow) {
            const string commandText = "CMS_FootballScore_Search";
            try
            {
                List<FootballScoreEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow,System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FootballScoreEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// Chuyển đổi DAL cho FootballScoreDalBase
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected FootballScoreDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
