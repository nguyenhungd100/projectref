﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.FootballScore.MainDal.Databases;
using ChannelVN.FootballScore.MainDal;

namespace ChannelVN.FootballScore.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Live Sport
        private LiveSportTVDal _liveSportMainDal;
        public LiveSportTVDalBase LiveSportMainDal
        {
            get { return _liveSportMainDal ?? (_liveSportMainDal = new LiveSportTVDal((CmsMainDb)this)); }
        }
        #endregion

        #region Football score detail
        private FootballScoreDetailDal _footBallScoreDetailDal;
        public FootballScoreDetailDalBase FootBallScoreDetailDal
        {
            get { return _footBallScoreDetailDal ?? (_footBallScoreDetailDal = new FootballScoreDetailDal((CmsMainDb)this)); }
        }
        #endregion

        #region Football score
        private FootballScoreDal _footBallScoreDal;
        public FootballScoreDalBase FootBallScoreDal
        {
            get { return _footBallScoreDal ?? (_footBallScoreDal = new FootballScoreDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}