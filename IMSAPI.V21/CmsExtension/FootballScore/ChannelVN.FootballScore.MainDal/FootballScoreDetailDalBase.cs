﻿using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.FootballScore.MainDal
{
    public abstract class FootballScoreDetailDalBase
    {
        #region function SET
        public bool InsertFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail, ref int newId)
        {
            const string commandText = "CMS_FootballScoreDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScoreDetail.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "FootballScoreId", footballScoreDetail.FootballScoreId);
                _db.AddParameter(cmd, "StartTime", footballScoreDetail.StartTime);
                _db.AddParameter(cmd, "TeamA", footballScoreDetail.TeamA);
                _db.AddParameter(cmd, "TeamB", footballScoreDetail.TeamB);
                _db.AddParameter(cmd, "LogoTeamA", footballScoreDetail.LogoTeamA);
                _db.AddParameter(cmd, "LogoTeamB", footballScoreDetail.LogoTeamB);
                _db.AddParameter(cmd, "ScoreOfTeamA", footballScoreDetail.ScoreOfTeamA);
                _db.AddParameter(cmd, "ScoreOfTeamB", footballScoreDetail.ScoreOfTeamB);
                _db.AddParameter(cmd, "Status", footballScoreDetail.Status);
                _db.AddParameter(cmd, "Description", footballScoreDetail.Description);
                _db.AddParameter(cmd, "LiveChannel", footballScoreDetail.LiveChannel);
                _db.AddParameter(cmd, "NewsUrl", footballScoreDetail.NewsUrl);
                var result = cmd.ExecuteNonQuery();
                newId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return newId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail)
        {
            const string commandText = "CMS_FootballScoreDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", footballScoreDetail.Id);
                _db.AddParameter(cmd, "FootballScoreId", footballScoreDetail.FootballScoreId);
                _db.AddParameter(cmd, "StartTime", footballScoreDetail.StartTime);
                _db.AddParameter(cmd, "TeamA", footballScoreDetail.TeamA);
                _db.AddParameter(cmd, "TeamB", footballScoreDetail.TeamB);
                _db.AddParameter(cmd, "LogoTeamA", footballScoreDetail.LogoTeamA);
                _db.AddParameter(cmd, "LogoTeamB", footballScoreDetail.LogoTeamB);
                _db.AddParameter(cmd, "ScoreOfTeamA", footballScoreDetail.ScoreOfTeamA);
                _db.AddParameter(cmd, "ScoreOfTeamB", footballScoreDetail.ScoreOfTeamB);
                _db.AddParameter(cmd, "Status", footballScoreDetail.Status);
                _db.AddParameter(cmd, "Description", footballScoreDetail.Description);
                _db.AddParameter(cmd, "LiveChannel", footballScoreDetail.LiveChannel);
                _db.AddParameter(cmd, "NewsUrl", footballScoreDetail.NewsUrl);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteFootballScoreDetail(int detailId)
        {
            const string commandText = "CMS_FootballScoreDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", detailId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function GET
        public FootballScoreDetailEntity GetFootballScoreDetailById(int detailId)
        {
            const string commandText = "CMS_FootballScoreDetail_GetById";
            try
            {
                FootballScoreDetailEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", detailId);
                data = _db.Get<FootballScoreDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FootballScoreDetailEntity> SearchFootballScoreDetail(string keyword, int FootballScoreId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FootballScoreDetail_Search";
            try
            {
                List<FootballScoreDetailEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FootballScoreId", FootballScoreId);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FootballScoreDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho FootballScoreDetailDalBase
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected FootballScoreDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
