﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.DAL;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.Entity.ErrorCode;

namespace ChannelVN.FootballScore.BO
{
    public class FootballScoreDetailBo
    {
        #region FootballScore

        #region Update

        public static ErrorMapping.ErrorCodes InsertFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail, ref int newId)
        {
            try
            {
                if (null == footballScoreDetail)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballScoreDetailDal.InsertFootballScoreDetail(footballScoreDetail, ref newId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateFootballScoreDetail(FootballScoreDetailEntity footballScoreDetail)
        {
            try
            {
                if (null == footballScoreDetail)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballScoreDetailDal.UpdateFootballScoreDetail(footballScoreDetail) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteFootballScoreDetail(int detailId)
        {
            try
            {
                return FootballScoreDetailDal.DeleteFootballScoreDetail(detailId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FootballScoreDetailEntity GetFootballScoreDetailById(int id)
        {
            try
            {
                return FootballScoreDetailDal.GetFootballScoreDetailById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<FootballScoreDetailEntity> SearchFootballScoreDetail(string keyword, int FootballScoreId, EnumFootballScoreDetailStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FootballScoreDetailDal.SearchFootballScoreDetail(keyword, FootballScoreId, (int)status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FootballScoreDetailEntity>();
            }
        }


        #endregion

        #endregion
    }
}
