﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.DAL;
using ChannelVN.FootballScore.Entity.ErrorCode;
using ChannelVN.FootballScore.Entity;

namespace ChannelVN.FootballScore.BO
{
    public class LiveSportTVBo
    {
        #region LiveSportTV

        #region Update

        public static ErrorMapping.ErrorCodes InsertLiveSportTV(LiveSportTVEntity liveSportTV, ref int newliveSportTVId)
        {
            try
            {
                if (null == liveSportTV)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return LiveSportTVDal.InsertLiveSportTV(liveSportTV, ref newliveSportTVId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateLiveSportTV(LiveSportTVEntity liveSportTV)
        {
            try
            {
                if (null == liveSportTV)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return LiveSportTVDal.UpdateLiveSportTV(liveSportTV) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteLiveSportTV(int photoId)
        {
            try
            {
                return LiveSportTVDal.DeleteLiveSportTV(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static LiveSportTVEntity GetLiveSportTVById(int id)
        {
            try
            {
                return LiveSportTVDal.GetLiveSportTVById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<LiveSportTVEntity> SearchLiveSportTV(string keyword, EnumLiveSportTVStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return LiveSportTVDal.SearchLiveSportTV(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<LiveSportTVEntity>();
            }
        }


        #endregion

        #endregion
    }
}
