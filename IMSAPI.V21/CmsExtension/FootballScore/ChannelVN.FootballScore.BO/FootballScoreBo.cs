﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.FootballScore.DAL;
using ChannelVN.FootballScore.Entity;
using ChannelVN.FootballScore.Entity.ErrorCode;

namespace ChannelVN.FootballScore.BO
{
    public class FootballScoreBo
    {
        #region FootballScore

        #region Update

        public static ErrorMapping.ErrorCodes InsertFootballScore(FootballScoreEntity footballScore, ref int newId)
        {
            try
            {
                if (null == footballScore)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballScoreDal.InsertFootballScore(footballScore, ref newId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFootballScore(FootballScoreEntity footballScore)
        {
            try
            {
                if (null == footballScore)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballScoreDal.UpdateFootballScore(footballScore) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteFootballScore(int photoId)
        {
            try
            {
                return FootballScoreDal.DeleteFootballScore(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFootballScorePriority(string listOfPriority)
        {
            try
            {
                return FootballScoreDal.UpdatePriority(listOfPriority) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FootballScoreEntity GetFootballScoreById(int id)
        {
            try
            {
                return FootballScoreDal.GetFootballScoreById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<FootballScoreEntity> SearchFootballScore(string keyword, EnumFootballScoreStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FootballScoreDal.SearchFootballScore(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FootballScoreEntity>();
            }
        }
       

        #endregion

        #endregion

    }
}
