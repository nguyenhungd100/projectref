﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.FootballScore.Entity
{
    [DataContract]
    public class FootballScoreEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }       
        [DataMember]
        public string Name { get; set; }      
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public string Data { get; set; }       
        [DataMember]
        public int Status { get; set; }
        
    }
    [DataContract]
    public enum EnumFootballScoreStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Show = 1,
        [EnumMember]
        Hide = 2,
    }
}
