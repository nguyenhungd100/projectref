﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.FootballScore.Entity
{
    [DataContract]
    public enum EnumLiveSportTVStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Hidden = 0,
        [EnumMember]
        Show = 1,
        [EnumMember]
        Finished = 2
    }
    [DataContract]
    public class LiveSportTVEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string TeamA { get; set; }
        [DataMember]
        public string TeamB { get; set; }
        [DataMember]
        public string AvatarTeamA { get; set; }
        [DataMember]
        public string AvatarTeamB { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public string LiveLink1 { get; set; }
        [DataMember]
        public string LiveLink2 { get; set; }
        [DataMember]
        public string LiveLink3 { get; set; }
        [DataMember]
        public string LiveLink4 { get; set; }
        [DataMember]
        public string LiveLink5 { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
