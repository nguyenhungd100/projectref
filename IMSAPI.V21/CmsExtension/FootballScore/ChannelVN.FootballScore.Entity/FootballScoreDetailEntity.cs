﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.FootballScore.Entity
{
    [DataContract]
    public class FootballScoreDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FootballScoreId { get; set; }
        [DataMember]
        public string FootballScoreName { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public string TeamA { get; set; }
        [DataMember]
        public string TeamB { get; set; }
        [DataMember]
        public string LogoTeamA { get; set; }
        [DataMember]
        public string LogoTeamB { get; set; }
        [DataMember]
        public string ScoreOfTeamA { get; set; }
        [DataMember]
        public string ScoreOfTeamB { get; set; }
        [DataMember] 
        public int Status { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string LiveChannel { get; set; }
        [DataMember]
        public string NewsUrl { get; set; }

    }
    [DataContract]
    public enum EnumFootballScoreDetailStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Show = 1,
        [EnumMember]
        Hide = 2,
    }
}
