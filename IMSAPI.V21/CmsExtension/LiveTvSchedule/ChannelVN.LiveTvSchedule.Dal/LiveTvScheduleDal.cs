﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.LiveTvSchedule.Dal.Common;
using ChannelVN.LiveTvSchedule.Entity;
using ChannelVN.LiveTvSchedule.MainDal.Databases;

namespace ChannelVN.LiveTvSchedule.Dal
{
    public class LiveTvScheduleDal
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveTvSchedule
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(LiveTvScheduleEntity entity, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.Insert(entity, ref id);
            }
            return returnValue;
        }

        /// <summary>
        /// Sửa dữ liệu cho bảng LiveTvSchedule
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static bool Update(LiveTvScheduleEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.Update(entity);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveTvSchedule theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.Delete(id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static LiveTvScheduleEntity GetById(int id)
        {
            LiveTvScheduleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.GetById(id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách LiveTvSchedule
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<LiveTvScheduleEntity> Search(string keyword, int scheduleType, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveTvScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.Search(keyword, scheduleType, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách LiveTvSchedule theo ngày
        /// </summary>
        /// <param name="scheduleDate"></param>
        /// <param name="programChannelId"></param>
        /// <returns></returns>
        public static List<LiveTvScheduleEntity> GetByDate(DateTime scheduleDate, int programChannelId)
        {
            List<LiveTvScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveTvScheduleMainDal.GetByDate(scheduleDate, programChannelId);
            }
            return returnValue;
        }
    }
}
