﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.LiveTvSchedule.Entity;
using ChannelVN.LiveTvSchedule.MainDal.Databases;

namespace ChannelVN.LiveTvSchedule.MainDal
{
    public class LiveTvScheduleDal : LiveTvScheduleDalBase
    {
        internal LiveTvScheduleDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
