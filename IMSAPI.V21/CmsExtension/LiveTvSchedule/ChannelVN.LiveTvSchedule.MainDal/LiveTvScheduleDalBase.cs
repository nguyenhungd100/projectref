﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.LiveTvSchedule.Entity;
using ChannelVN.LiveTvSchedule.MainDal.Common;
using ChannelVN.LiveTvSchedule.MainDal.Databases;

namespace ChannelVN.LiveTvSchedule.MainDal
{
    public abstract class LiveTvScheduleDalBase
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveTvSchedule
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Insert(LiveTvScheduleEntity entity, ref int id)
        {
            const string commandText = "CMS_LiveTvSchedule_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ScheduleName", entity.ScheduleName);
                _db.AddParameter(cmd, "ProgramChannelId", entity.ProgramChannelId);
                if (entity.StartTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartTime", DBNull.Value);
                else _db.AddParameter(cmd, "StartTime", entity.StartTime);
                if (entity.EndTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndTime", DBNull.Value);
                else _db.AddParameter(cmd, "EndTime", entity.EndTime);
                _db.AddParameter(cmd, "ScheduleType", entity.ScheduleType);
                _db.AddParameter(cmd, "NewsIdForLockedState", entity.NewsIdForLockedState);
                _db.AddParameter(cmd, "VideoIdForLockedState", entity.VideoIdForLockedState);
                _db.AddParameter(cmd, "CreatedBy", entity.CreatedBy);
                _db.AddParameter(cmd, "Status", entity.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Sửa dữ liệu cho bảng LiveTvSchedule
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(LiveTvScheduleEntity entity)
        {
            const string commandText = "CMS_LiveTvSchedule_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ScheduleName", entity.ScheduleName);
                _db.AddParameter(cmd, "ProgramChannelId", entity.ProgramChannelId);
                if (entity.StartTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartTime", DBNull.Value);
                else _db.AddParameter(cmd, "StartTime", entity.StartTime);
                if (entity.EndTime <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndTime", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndTime", entity.EndTime);
                _db.AddParameter(cmd, "ScheduleType", entity.ScheduleType);
                _db.AddParameter(cmd, "NewsIdForLockedState", entity.NewsIdForLockedState);
                _db.AddParameter(cmd, "VideoIdForLockedState", entity.VideoIdForLockedState);
                _db.AddParameter(cmd, "CreatedBy", entity.CreatedBy);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "Id", entity.Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveTvSchedule theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            const string commandText = "CMS_LiveTvSchedule_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LiveTvScheduleEntity GetById(int id)
        {
            const string commandText = "CMS_LiveTvSchedule_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<LiveTvScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách LiveTvSchedule
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="scheduleType"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<LiveTvScheduleEntity> Search(string keyword, int scheduleType, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveTvSchedule_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ScheduleType", scheduleType);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<LiveTvScheduleEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Lấy danh sách LiveTvSchedule theo ngày
        /// </summary>
        /// <param name="scheduleDate"></param>
        /// <param name="programChannelId"></param>
        /// <returns></returns>
        public List<LiveTvScheduleEntity> GetByDate(DateTime scheduleDate, int programChannelId)
        {
            const string commandText = "CMS_LiveTvSchedule_GetByDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ProgramChannelId", programChannelId);
                _db.AddParameter(cmd, "ScheduleDate", scheduleDate);
                var numberOfRow = _db.GetList<LiveTvScheduleEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveTvScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
