﻿using System;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.LiveTvSchedule.Entity
{
    [DataContract]
    public class LiveTvScheduleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ScheduleName { get; set; }
        [DataMember]
        public int ProgramChannelId { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public DateTime EndTime { get; set; }
        [DataMember]
        public int ScheduleType { get; set; }
        [DataMember]
        public Int64 NewsIdForLockedState { get; set; }
        [DataMember]
        public Int64 VideoIdForLockedState { get; set; }
        [DataMember]
        public string VideoForLockedState { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
