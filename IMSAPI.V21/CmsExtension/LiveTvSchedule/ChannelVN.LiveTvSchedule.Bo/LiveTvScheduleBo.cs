﻿using System;
using System.Collections.Generic;
using ChannelVN.LiveTvSchedule.Dal;
using ChannelVN.LiveTvSchedule.Entity;
using ChannelVN.LiveTvSchedule.Entity.ErrorCode;

namespace ChannelVN.LiveTvSchedule.Bo
{
    public class LiveTvScheduleBo
    {
        public static List<LiveTvScheduleEntity> Search(string keyword, int scheduleType, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return LiveTvScheduleDal.Search(keyword, scheduleType, status, pageIndex, pageSize, ref totalRow);
        }
        public static List<LiveTvScheduleEntity> GetLiveTvScheduleByScheduleDate(DateTime scheduleDate, int programChannelId)
        {
            return LiveTvScheduleDal.GetByDate(scheduleDate, programChannelId);
        }
        public static LiveTvScheduleEntity GetById(int id)
        {
            return LiveTvScheduleDal.GetById(id);
        }

        public static ErrorMapping.ErrorCodes Insert(LiveTvScheduleEntity enity, ref int id)
        {
            return LiveTvScheduleDal.Insert(enity, ref id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(LiveTvScheduleEntity enity)
        {
            return LiveTvScheduleDal.Update(enity)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return LiveTvScheduleDal.Delete(id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
