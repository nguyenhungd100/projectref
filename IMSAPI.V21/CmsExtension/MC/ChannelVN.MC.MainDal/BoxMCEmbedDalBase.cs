﻿using System;
using System.Collections.Generic;
using ChannelVN.MC.Entity;
using ChannelVN.MC.MainDal.Databases;

namespace ChannelVN.MC.MainDal
{
    public abstract class BoxMCEmbedDalBase
    {
        public List<MCEntity> GetListMCEmbedBoxOnPage(int zoneId, int type)
        {
            const string commandText = "CMS_BoxMCEmbed_GetListByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", Convert.ToInt32(zoneId));
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                List<MCEntity> data = _db.GetList<MCEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(BoxMCEmbedEntity newsEmbebBox)
        {
            const string commandText = "CMS_BoxMCEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", newsEmbebBox.ZoneId);
                _db.AddParameter(cmd, "MCId", newsEmbebBox.MCId);
                _db.AddParameter(cmd, "SortOrder", newsEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", newsEmbebBox.Type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public void Update(string listNewsId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxMCEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Delete(int mcId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxMCEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MCId", mcId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxMCEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
