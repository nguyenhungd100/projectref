﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.MC.Entity;
using ChannelVN.MC.MainDal.Common;
using ChannelVN.MC.MainDal.Databases;

namespace ChannelVN.MC.MainDal
{
    public abstract class MCDalBase
    {
        public bool Insert(MCEntity expertEntity, ref int expertId)
        {
            const string commandText = "CMS_MC_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "MCName", expertEntity.MCName);
                _db.AddParameter(cmd, "DOB", expertEntity.DOB);
                _db.AddParameter(cmd, "Description", expertEntity.Description);
                _db.AddParameter(cmd, "Avatar", expertEntity.Avatar);
                _db.AddParameter(cmd, "HomeTown", expertEntity.HomeTown);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                _db.AddParameter(cmd, "Status", expertEntity.Status);
                _db.AddParameter(cmd, "FacebookLink", expertEntity.FacebookLink);
                _db.AddParameter(cmd, "FacebookTime", expertEntity.FacebookTime);
                _db.AddParameter(cmd, "FullDescription", expertEntity.FullDescription);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                expertId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MCEntity GetById(int id)
        {
            const string commandText = "CMS_MC_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                MCEntity data = _db.Get<MCEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(MCEntity expertEntity)
        {
            const string commandText = "CMS_MC_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", expertEntity.Id);
                _db.AddParameter(cmd, "MCName", expertEntity.MCName);
                _db.AddParameter(cmd, "DOB", expertEntity.DOB);
                _db.AddParameter(cmd, "Description", expertEntity.Description);
                _db.AddParameter(cmd, "Avatar", expertEntity.Avatar);
                _db.AddParameter(cmd, "HomeTown", expertEntity.HomeTown);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                _db.AddParameter(cmd, "Status", expertEntity.Status);
                _db.AddParameter(cmd, "FacebookLink", expertEntity.FacebookLink);
                _db.AddParameter(cmd, "FacebookTime", expertEntity.FacebookTime);
                _db.AddParameter(cmd, "FullDescription", expertEntity.FullDescription);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MCEntity> Search(string keyword)
        {
            const string commandText = "CMS_MC_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                List<MCEntity> data = _db.GetList<MCEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<MCInNewsEntity> ListForEditNews(long NewsId)
        {
            const string commandText = "CMS_MC_ListForEditNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsID", NewsId);
                List<MCInNewsEntity> data = _db.GetList<MCInNewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_MC_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MCEntity GetMCByNewsId(long id)
        {
            const string commandText = "CMS_MCInNews_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                MCEntity data = _db.Get<MCEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertMCToNews(MCInNews expertEntity)
        {
            const string commandText = "CMS_MCInNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MCId", expertEntity.MCId);
                _db.AddParameter(cmd, "NewsId", expertEntity.NewsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMCToNews(MCInNews expertEntity)
        {
            const string commandText = "CMS_MCInNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MCId", expertEntity.MCId);
                _db.AddParameter(cmd, "NewsId", expertEntity.NewsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteMCInNews(long id)
        {
            const string commandText = "CMS_MCInNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchMCNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchMCNewsWhichPublished";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNews(int mcId, string deleteNewsId, string addNewsId, int newsType = 1)
        {
            const string commandText = "CMS_MC_UpdateNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "McId", mcId);
                _db.AddParameter(cmd, "DeleteNewsIds", deleteNewsId);
                _db.AddParameter(cmd, "AddNewsIds", addNewsId);
                _db.AddParameter(cmd, "NewsType", newsType);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MCInNews_Update(string mcIds, long newsId)
        {
            const string commandText = "CMS_MCInNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "McIds", mcIds);
                _db.AddParameter(cmd, "NewsId", newsId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MCInPhoto> GetListPhotoByMcId(int McId, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_MCPhoto_GetListByMcId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "MCId", McId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var data = _db.GetList<MCInPhoto>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePhotoInMC(int McId, string listPhoto, string listDelId)
        {
            const string commandText = "CMS_MC_UpdatePhoto";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MCId", McId);
                _db.AddParameter(cmd, "DeletePhotoIds", listDelId);
                _db.AddParameter(cmd, "AddPhotoIds", listPhoto);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MCDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
