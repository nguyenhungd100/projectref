﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.MC.Entity
{
    [DataContract]
    public class BoxMCEmbedEntity : EntityBase
    {
        [DataMember]
        public long ZoneId { get; set; }

        [DataMember]
        public long MCId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }
    }
}
