﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.MC.Entity
{
    [DataContract]
    public class MCEntity: EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string MCName { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public DateTime DOB { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public string HomeTown { set; get; }
        [DataMember]
        public string FacebookLink { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public DateTime FacebookTime { set; get; }
        [DataMember]
        public string FullDescription { set; get; }
    }
    public class MCInPhoto
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int MCId { set; get; }
        [DataMember]
        public string Photo { set; get; }
    }
    public class MCInNewsEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string MCName { set; get; }
        [DataMember]
        public long NewsId { get; set; }
    }
}
