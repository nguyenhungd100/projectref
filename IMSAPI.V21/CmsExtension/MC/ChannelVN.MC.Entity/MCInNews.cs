﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.MC.Entity
{
    [DataContract]
    public class MCInNews : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int MCId { set; get; }
        [DataMember]
        public long NewsId { set; get; }
    }
}
