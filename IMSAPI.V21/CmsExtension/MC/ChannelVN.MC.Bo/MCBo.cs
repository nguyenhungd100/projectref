﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.MC.Dal;
using ChannelVN.MC.Entity;

namespace ChannelVN.MC.Bo
{
    public class MCBo
    {
        public static ErrorMapping.ErrorCodes Insert(MCEntity MCEntity, ref int MCId)
        {
            try
            {
                var inserted = MCDal.Insert(MCEntity, ref MCId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static MCEntity GetById(int id)
        {
            try
            {
                return MCDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new MCEntity();
            }
        }

        public static ErrorMapping.ErrorCodes Update(MCEntity expertEntity)
        {
            try
            {
                var inserted = MCDal.Update(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<MCEntity> Search(string keyword)
        {
            try
            {
                return MCDal.Search(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MCEntity>();
            }
        }
        public static List<MCInNewsEntity> ListForEditNews(long NewsId)
        {
            try
            {
                return MCDal.ListForEditNews(NewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MCInNewsEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = MCDal.Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static MCEntity GetMCByNewsId(long id)
        {
            try
            {
                return MCDal.GetMCByNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new MCEntity();
            }
        }

        public static ErrorMapping.ErrorCodes InsertMCIntoNews(MCInNews expertEntity)
        {
            try
            {
                var expertInNews = MCDal.GetMCByNewsId(expertEntity.NewsId);
                // Nếu không chọn chuyên gia
                if (expertEntity.MCId == 0)
                {
                    // Nếu đã chèn chuyên gia trước
                    if (expertInNews != null && expertInNews.Id > 0)
                    {
                        // Xóa chuyên gia
                        MCDal.DeleteMCNewsId(expertEntity.NewsId);
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                bool inserted;
                if (expertInNews != null && expertInNews.Id > 0)
                {
                    if (expertEntity.MCId == 0)
                    {
                        MCDal.DeleteMCNewsId(expertEntity.NewsId);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    inserted = MCDal.UpdateMCInNews(expertEntity);
                    return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                inserted = MCDal.InsertMCToNews(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<NewsInListEntity> SearchMCNewsWhichPublished(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return MCDal.SearchMCNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsInTagNews(int mcId, string deleteNewsId, string addNewsId, int newsType = 1)
        {
            return MCDal.UpdateNews(mcId, deleteNewsId, addNewsId, newsType)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes MCInNews_Update(string mcIds, long NewsId)
        {
            return MCDal.MCInNews_Update(mcIds, NewsId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<MCInPhoto> GetListPhotoByMcId(int McId, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                return MCDal.GetListPhotoByMcId(McId, pageIndex, pageSize, ref totalRows);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<MCInPhoto>();
            }
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoInMC(int mcId, string addPhotoId, string deletePhotoId)
        {
            return MCDal.UpdatePhotoInMC(mcId, addPhotoId, deletePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
