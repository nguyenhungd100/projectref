﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.MC.Dal;
using ChannelVN.MC.Entity;

namespace ChannelVN.MC.Bo
{
    public class BoxMCEmbedBo
    {
        public static List<MCEntity> GetListMCEmbed(int zoneId, int type)
        {
            var newsEmbedBox = new List<MCEntity>();
            try
            {

                return BoxMCEmbedDal.GetListBoxMCEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static ErrorMapping.ErrorCodes Insert(BoxMCEmbedEntity newsEmbebBox)
        {
            try
            {
                if (null != newsEmbebBox)
                {
                    return BoxMCEmbedDal.Insert(newsEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxMCEmbedDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listNewsId, int zoneId, int type)
        {
            try
            {
                if (null != listNewsId)
                {
                    BoxMCEmbedDal.Update(listNewsId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxMCEmbedDal.Update:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(int mcId, int zoneId, int type)
        {
            try
            {
                if (mcId > 0 && zoneId > 0)
                {
                    BoxMCEmbedDal.Delete(mcId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxMCEmbedDal.Delete:{0}", ex.Message));
            }

        }

    }
}
