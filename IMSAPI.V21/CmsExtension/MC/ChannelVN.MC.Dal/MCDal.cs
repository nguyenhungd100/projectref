﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.MC.Entity;
using ChannelVN.MC.MainDal.Databases;

namespace ChannelVN.MC.Dal
{
    public class MCDal
    {
        public static bool Insert(MCEntity expertEntity, ref int newsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.Insert(expertEntity, ref newsAuthorId);
            }
            return returnValue;
        }

        public static MCEntity GetById(int id)
        {
            MCEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Update(MCEntity expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.Update(expertEntity);
            }
            return returnValue;
        }

        public static List<MCEntity> Search(string keyword)
        {
            List<MCEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.Search(keyword);
            }
            return returnValue;
        }
        public static List<MCInNewsEntity> ListForEditNews(long NewsId)
        {
            List<MCInNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.ListForEditNews(NewsId);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.Delete(id);
            }
            return returnValue;
        }

        public static MCEntity GetMCByNewsId(long id)
        {
            MCEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.GetMCByNewsId(id);
            }
            return returnValue;
        }

        public static bool InsertMCToNews(MCInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.InsertMCToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool UpdateMCInNews(MCInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.UpdateMCToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool DeleteMCNewsId(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.DeleteMCInNews(id);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchMCNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.SearchMCNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref  totalRow);
            }
            return returnValue;
        }

        public static bool UpdateNews(int mcId, string deleteNewsId, string addNewsId, int newsType = 1)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.UpdateNews(mcId, deleteNewsId, addNewsId, newsType);
            }
            return returnValue;
        }
        public static bool MCInNews_Update(string mcIds, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.MCInNews_Update(mcIds, newsId);
            }
            return returnValue;
        }
        public static List<MCInPhoto> GetListPhotoByMcId(int McId, int pageIndex, int pageSize, ref int totalRows)
        {
            List<MCInPhoto> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.GetListPhotoByMcId(McId, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }
        public static bool UpdatePhotoInMC(int mcId, string addPhotoId, string deletePhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MCMainDal.UpdatePhotoInMC(mcId, addPhotoId, deletePhotoId);
            }
            return returnValue;
        }
    }
}
