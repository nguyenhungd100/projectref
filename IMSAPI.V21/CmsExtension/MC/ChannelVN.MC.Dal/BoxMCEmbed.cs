﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.MC.Entity;
using ChannelVN.MC.MainDal.Databases;

namespace ChannelVN.MC.Dal
{
    public class BoxMCEmbedDal
    {
        public static List<MCEntity> GetListBoxMCEmbed(int zoneId, int type)
        {
            List<MCEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxMCEmbedMainDal.GetListMCEmbedBoxOnPage(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxMCEmbedEntity newsEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxMCEmbedMainDal.Insert(newsEmbebBox);
            }
            return returnValue;

        }

        public static void Update(string listNewsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxMCEmbedMainDal.Update(listNewsId, zoneId, type);
            }
        }

        public static void Delete(int mcId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxMCEmbedMainDal.Delete(mcId, zoneId, type);
            }
        }
    }
}
