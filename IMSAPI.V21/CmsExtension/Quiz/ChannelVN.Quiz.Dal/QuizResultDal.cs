﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.MainDal.Databases;

namespace ChannelVN.Quiz.Dal
{
    public class QuizResultDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quizResult"></param>
        /// <returns></returns>
        public static bool InsertQuizResult(QuizResultEntity quizResult)
        {
            bool retVal = false;
            using (var db = new QuizDb())
            {
                retVal = db.QuizResultDal.Insert(quizResult);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quizId"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="facebook"></param>
        /// <param name="google"></param>
        /// <param name="vietId"></param>
        /// <returns></returns>
        public static QuizResultEntity GetQuizResultByAccount(long id, long quizId, string email, string phone, string facebook, string google, string vietId)
        {
            QuizResultEntity retVal = null;
            using (var db = new QuizDb())
            {
                retVal = db.QuizResultDal.GetByAccount(id, quizId, email, phone, facebook, google, vietId);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="phone"></param>
        /// <param name="facebook"></param>
        /// <param name="google"></param>
        /// <param name="vietId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<QuizResultEntity> SearchQuizResult(long quizId, string phone, string facebook, string google, string vietId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuizResultEntity> retVal = new List<QuizResultEntity>();
            using (var db = new QuizDb())
            {
                retVal = db.QuizResultDal.Search(quizId, phone, facebook, google, vietId, pageIndex, pageSize,
                    ref totalRow);
            }
            return retVal;
        }
    }
}
