﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.MainDal.Databases;

namespace ChannelVN.Quiz.Dal
{
    public class QuizDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool InsertQuiz(QuizEntity quiz)
        {
            bool retVal = false;
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.Insert(quiz);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool UpdateQuiz(QuizEntity quiz)
        {
            bool retVal = false;
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.Update(quiz);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static bool UpdateQuizConfig(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            bool retVal = false;
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.UpdateConfig(id, interactiveData, interactiveConfig, lastModifiedBy);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static bool QuizPublish(int id, string interactiveData, string interactiveConfig, string lastModifiedBy)
        {
            bool retVal = false;
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.Publish(id, interactiveData, interactiveConfig, lastModifiedBy);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static QuizEntity GetQuizById(int id)
        {
            QuizEntity retVal = null;
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.GetById(id);
            }
            return retVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<QuizEntity> SearchQuiz(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuizEntity> retVal = new List<QuizEntity>();
            using (var db = new QuizDb())
            {
                retVal = db.QuizDal.Search(keyword, type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
    }
}
