﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Quiz.Dal;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.Quiz.Bo
{
    public class QuizResultBo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertQuizResult(QuizResultEntity quiz)
        {
            try
            {
                return QuizResultDal.InsertQuizResult(quiz)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static QuizResultEntity GetQuizResultByAccount(long id, long quizId, string email, string phone, string facebook, string google, string vietId)
        {
            try
            {
                return QuizResultDal.GetQuizResultByAccount(id, quizId, email, phone, facebook, google, vietId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public static List<QuizResultEntity> SearchQuizResult(long quizId, string phone, string facebook, string google, string vietId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return QuizResultDal.SearchQuizResult(quizId, phone, facebook, google, vietId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<QuizResultEntity>();
            }
        }
    }
}
