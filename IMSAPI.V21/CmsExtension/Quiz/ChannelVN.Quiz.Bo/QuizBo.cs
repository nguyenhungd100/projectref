﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Quiz.Dal;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.Quiz.Bo
{
    public class QuizBo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes InsertQuiz(QuizEntity quiz)
        {
            try
            {
                quiz.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return QuizDal.InsertQuiz(quiz)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateQuiz(QuizEntity quiz)
        {
            try
            {
                quiz.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return QuizDal.UpdateQuiz(quiz)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateQuizConfig(int id, string interactiveData, string interactiveConfig)
        {
            try
            {
                return QuizDal.UpdateQuizConfig(id, interactiveData, interactiveConfig, WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="interactiveData"></param>
        /// <param name="interactiveConfig"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes QuizPublish(int id, string interactiveData, string interactiveConfig)
        {
            try
            {
                return QuizDal.QuizPublish(id, interactiveData, interactiveConfig, WcfExtensions.WcfMessageHeader.Current.ClientUsername)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static QuizEntity GetQuizById(int id)
        {
            try
            {
                return QuizDal.GetQuizById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="createdBy"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<QuizEntity> SearchQuiz(string keyword, EnumQuizType type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return QuizDal.SearchQuiz(keyword, (int)type, createdBy, status, siteId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<QuizEntity>();
            }
        }
    }
}
