﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Quiz.Entity
{
    public enum EnumQuizType : int
    {
        All = -1,
        Quiz = 1,
        Tarot = 2,
        QuizOneResult = 3,
        Horoscope = 4
    }

    public class QuizEntity : EntityBase
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Title { get; set; }
        public string Sapo { get; set; }
        public string Avatar { get; set; }
        public string BackgroundCover { get; set; }
        public string Author { get; set; }
        public string Source { get; set; }
        public string QuizData { get; set; }
        public string QuizConfig { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int Status { get; set; }
        public int SiteId { get; set; }

        public string QuizPublishedData { get; set; }
        public string QuizPublishedConfig { get; set; }
    }
}
