﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Quiz.Entity
{    
    public class QuizResultEntity : EntityBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Facebook { get; set; }
        public string Google { get; set; }
        public string VietId { get; set; }
        public string Source { get; set; }
        public long QuizId { get; set; }
        public DateTime QuizDate { get; set; }
        public long Duration { get; set; }
        public int Mark { get; set; }
        public string Data { get; set; }
    }
}
