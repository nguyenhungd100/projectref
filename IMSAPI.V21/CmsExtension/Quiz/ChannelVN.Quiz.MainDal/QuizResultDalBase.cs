﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.Quiz.MainDal
{
    public abstract class QuizResultDalBase
    {
        public bool Insert(QuizResultEntity quiz)
        {
            const string storeProcedure = "CMS_QuizResult_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Name", quiz.Name);
                _db.AddParameter(cmd, "Email", quiz.Email);
                _db.AddParameter(cmd, "Phone", quiz.Phone);
                _db.AddParameter(cmd, "Facebook", quiz.Facebook);
                _db.AddParameter(cmd, "Google", quiz.Google);
                _db.AddParameter(cmd, "VietId", quiz.VietId);
                _db.AddParameter(cmd, "QuizId", quiz.QuizId);
                _db.AddParameter(cmd, "Duration", quiz.Duration);
                _db.AddParameter(cmd, "Mark", quiz.Mark);
                _db.AddParameter(cmd, "Data", quiz.Data);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public QuizResultEntity GetByAccount(long id, long quizId, string email, string phone, string facebook, string google, string vietId)
        {
            const string storeProcedure = "CMS_QuizResult_GetByAccount";
            try
            {
                QuizResultEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "QuizId", quizId);
                _db.AddParameter(cmd, "Email", email);
                _db.AddParameter(cmd, "Phone", phone);
                _db.AddParameter(cmd, "Facebook", facebook);
                _db.AddParameter(cmd, "Google", google);
                _db.AddParameter(cmd, "VietId", vietId);
                data = _db.Get<QuizResultEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<QuizResultEntity> Search(long quizId, string phone, string facebook, string google, string vietId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_QuizResult_Search";
            try
            {
                List<QuizResultEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Phone", phone);
                _db.AddParameter(cmd, "Facebook", facebook);
                _db.AddParameter(cmd, "Google", google);
                _db.AddParameter(cmd, "VietId", vietId);
                _db.AddParameter(cmd, "QuizId", quizId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<QuizResultEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        #region Constructor

        private readonly QuizDb _db;

        protected QuizResultDalBase(QuizDb db)
        {
            _db = db;
        }

        protected QuizDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
