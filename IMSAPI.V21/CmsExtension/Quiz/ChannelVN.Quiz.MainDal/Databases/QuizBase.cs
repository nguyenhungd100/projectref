﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Quiz.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="QuizDb"/> class that 
    /// represents a connection to the <c>InfoGraphDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the InfoGraphDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class QuizDbBase : MainDbBase
    {
        #region Store procedures
        private QuizDal _quizContentDal;
        public QuizDalBase QuizDal
        {
            get { return _quizContentDal ?? (_quizContentDal = new QuizDal((QuizDb)this)); }
        }

        private QuizResultDal _quizResultDal;
        public QuizResultDalBase QuizResultDal
        {
            get { return _quizResultDal ?? (_quizResultDal = new QuizResultDal((QuizDb)this)); }
        }
       

        #endregion

        #region Constructors

        protected QuizDbBase()
        {
        }
        protected QuizDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}