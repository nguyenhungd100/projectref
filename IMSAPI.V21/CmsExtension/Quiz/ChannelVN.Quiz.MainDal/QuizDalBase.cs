﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.Quiz.Entity;
using ChannelVN.Quiz.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.Quiz.MainDal
{
    public abstract class QuizDalBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public bool Insert(QuizEntity quiz)
        {
            const string storeProcedure = "CMS_Quiz_Insert";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Type", quiz.Type);
                _db.AddParameter(cmd, "Title", quiz.Title);
                _db.AddParameter(cmd, "Sapo", quiz.Sapo);
                _db.AddParameter(cmd, "Avatar", quiz.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", quiz.BackgroundCover);
                _db.AddParameter(cmd, "Author", quiz.Author);
                _db.AddParameter(cmd, "Source", quiz.Source);
                _db.AddParameter(cmd, "QuizData", quiz.QuizData);
                _db.AddParameter(cmd, "QuizConfig", quiz.QuizConfig);
                _db.AddParameter(cmd, "CreatedBy", quiz.CreatedBy);
                _db.AddParameter(cmd, "Status", quiz.Status);
                _db.AddParameter(cmd, "SiteId", quiz.SiteId);

                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns></returns>
        public bool Update(QuizEntity quiz)
        {
            const string storeProcedure = "CMS_Quiz_Update";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", quiz.Id);
                _db.AddParameter(cmd, "Type", quiz.Type);
                _db.AddParameter(cmd, "Title", quiz.Title);
                _db.AddParameter(cmd, "Sapo", quiz.Sapo);
                _db.AddParameter(cmd, "Avatar", quiz.Avatar);
                _db.AddParameter(cmd, "BackgroundCover", quiz.BackgroundCover);
                _db.AddParameter(cmd, "Author", quiz.Author);
                _db.AddParameter(cmd, "Source", quiz.Source);
                _db.AddParameter(cmd, "QuizData", quiz.QuizData);
                _db.AddParameter(cmd, "QuizConfig", quiz.QuizConfig);
                _db.AddParameter(cmd, "LastModifiedBy", quiz.LastModifiedBy);
                _db.AddParameter(cmd, "Status", quiz.Status);
                _db.AddParameter(cmd, "SiteId", quiz.SiteId);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="quizData"></param>
        /// <param name="quizConfig"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public bool UpdateConfig(int id, string quizData, string quizConfig, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_Quiz_UpdateConfig";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "QuizData", quizData);
                _db.AddParameter(cmd, "QuizConfig", quizConfig);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public bool Publish(int id, string quizData, string quizConfig, string lastModifiedBy)
        {
            const string storeProcedure = "CMS_Quiz_Publish";

            try
            {

                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "QuizData", quizData);
                _db.AddParameter(cmd, "QuizConfig", quizConfig);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                int numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public QuizEntity GetById(long id)
        {
            const string storeProcedure = "CMS_Quiz_GetByid";
            try
            {
                QuizEntity data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<QuizEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", storeProcedure, ex.Message));
            }
        }
        public List<QuizEntity> Search(string keyword, int type, string createdBy, int status, int siteId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string storeProcedure = "CMS_Quiz_Search";
            try
            {
                List<QuizEntity> data = null;
                var cmd = _db.CreateCommand(storeProcedure, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SiteId", siteId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuizEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(storeProcedure + ":{0}", ex.Message));
            }
        }

        #region Constructor

        private readonly QuizDb _db;

        protected QuizDalBase(QuizDb db)
        {
            _db = db;
        }

        protected QuizDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
