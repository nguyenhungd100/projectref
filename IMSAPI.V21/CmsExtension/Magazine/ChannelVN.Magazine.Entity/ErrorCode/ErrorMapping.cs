﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Magazine.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable[ErrorCodes.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodes.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodes.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodes.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodes.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodes.TimeOutSession] = "Phiên làm việc của bạn đã hết.";            

            InnerHashtable[ErrorCodes.UpdateMagazineInvalidTitle] = "Invalid Title";
            InnerHashtable[ErrorCodes.UpdateMagazineNotFoundMagazine] = "Not Found Magazine";
            InnerHashtable[ErrorCodes.UpdateMagazineNewsNotFoundMagazineNews] = "Not Found Magazine News";
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            UpdateMagazineInvalidTitle = 1001,
            UpdateMagazineNotFoundMagazine = 1002,
            UpdateMagazineNewsNotFoundMagazineNews = 1003
            #endregion
        }
    }
}
