﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.Magazine.Entity
{
    [DataContract]
    public class MagazineNewsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int MagazineId { get; set; }
        [DataMember]
        public int NewsId { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
