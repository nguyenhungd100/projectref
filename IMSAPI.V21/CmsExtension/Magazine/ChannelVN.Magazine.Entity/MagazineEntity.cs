﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;

namespace ChannelVN.Magazine.Entity
{
    [DataContract]
    public enum EnumMagazineStatus: int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Active = 1,
        [EnumMember]
        UnActive = 2
    }

    [DataContract]
    public class MagazineEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
