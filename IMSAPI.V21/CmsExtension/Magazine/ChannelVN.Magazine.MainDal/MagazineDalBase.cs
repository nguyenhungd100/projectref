﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.MainDal.Common;
using ChannelVN.Magazine.MainDal.Databases;

namespace ChannelVN.Magazine.MainDal
{
    public abstract class MagazineDalBase
    {
        public bool Insert(MagazineEntity magazine, ref int newMagazineId)
        {
            const string commandText = "CMS_Magazine_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newMagazineId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "Url", magazine.Url);
                _db.AddParameter(cmd, "FilePath", magazine.FilePath);
                _db.AddParameter(cmd, "Type", magazine.Type);
                _db.AddParameter(cmd, "PublishedDate", magazine.PublishedDate);
                _db.AddParameter(cmd, "CreatedDate", magazine.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", magazine.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedDate", magazine.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", magazine.LastModifiedBy);
                _db.AddParameter(cmd, "Status", magazine.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newMagazineId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(MagazineEntity magazine)
        {
            const string commandText = "CMS_Magazine_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", magazine.Title);
                _db.AddParameter(cmd, "Avatar", magazine.Avatar);
                _db.AddParameter(cmd, "Url", magazine.Url);
                _db.AddParameter(cmd, "FilePath", magazine.FilePath);
                _db.AddParameter(cmd, "Type", magazine.Type);
                _db.AddParameter(cmd, "PublishedDate", magazine.PublishedDate);              
                _db.AddParameter(cmd, "LastModifiedDate", magazine.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", magazine.LastModifiedBy);
                _db.AddParameter(cmd, "Status", magazine.Status);
                _db.AddParameter(cmd, "Id", magazine.Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int MagazineId)
        {
            const string commandText = "CMS_Magazine_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MagazineId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MagazineEntity GetMagazineById(int id)
        {
            const string commandText = "CMS_Magazine_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<MagazineEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MagazineEntity> Search(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Magazine_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<MagazineEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MagazineDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
