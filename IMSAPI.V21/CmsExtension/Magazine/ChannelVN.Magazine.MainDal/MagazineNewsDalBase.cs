﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.MainDal.Common;
using ChannelVN.Magazine.MainDal.Databases;

namespace ChannelVN.Magazine.MainDal
{
    public abstract class MagazineNewsDalBase
    {
        public bool Insert(MagazineNewsEntity entity, ref int newMagazineId)
        {
            const string commandText = "CMS_MagazineNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newMagazineId, ParameterDirection.Output);
                _db.AddParameter(cmd, "MagazineId", entity.MagazineId);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Priority", entity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                newMagazineId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int MagazineNewsId)
        {
            const string commandText = "CMS_MagazineNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MagazineNewsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MagazineNewsEntity GetMagazineNewsById(int id)
        {
            const string commandText = "CMS_MagazineNews_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.Get<MagazineNewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMagazineNews(int magazineId, string deleteNewsId, string addNewsId)
        {
            const string commandText = "CMS_MagazineNews_UpdateNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MagazineId", magazineId);
                _db.AddParameter(cmd, "DeleteNewsIds", deleteNewsId);
                _db.AddParameter(cmd, "AddNewsIds", addNewsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MagazineNewsEntity> Search(string keyword)
        {
            const string commandText = "CMS_MagazineNews_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                var numberOfRow = _db.GetList<MagazineNewsEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MagazineNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
