﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Magazine.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Magazine

        private MagazineDal _magazineMainDal;
        public MagazineDal MagazineMainDal
        {
            get { return _magazineMainDal ?? (_magazineMainDal = new MagazineDal((CmsMainDb)this)); }
        }

        #endregion

        #region Magazine News

        private MagazineNewsDal _magazineNewsMainDal;
        public MagazineNewsDal MagazineNewsMainDal
        {
            get { return _magazineNewsMainDal ?? (_magazineNewsMainDal = new MagazineNewsDal((CmsMainDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}