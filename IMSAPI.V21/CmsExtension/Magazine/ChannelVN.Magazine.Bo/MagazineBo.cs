﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.Magazine.Dal;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.Magazine.Bo
{
    public class MagazineBo
    {
        #region Magazine
        
        public static ErrorMapping.ErrorCodes InsertMagazine(MagazineEntity entity, ref int newId)
        {
            if (string.IsNullOrEmpty(entity.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineInvalidTitle;
            }
            
            return MagazineDal.Insert(entity, ref newId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateMagazine(MagazineEntity entity)
        {
            if (entity.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
            }
            var currentMagazine = MagazineDal.GetMagazineById(entity.Id);
            if (currentMagazine == null)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
            }
            if (string.IsNullOrEmpty(entity.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineInvalidTitle;
            }
            if (entity.PublishedDate == DateTime.MinValue)
                entity.PublishedDate = DateTime.Now;

            currentMagazine.Title = entity.Title;
            currentMagazine.Avatar = entity.Avatar;
            currentMagazine.Url = entity.Url;
            currentMagazine.FilePath = entity.FilePath;
            currentMagazine.Type = entity.Type;
            currentMagazine.PublishedDate = entity.PublishedDate;
            currentMagazine.LastModifiedDate = entity.LastModifiedDate ;
            currentMagazine.LastModifiedBy = entity.LastModifiedBy;
            currentMagazine.Status = entity.Status;

            return MagazineDal.Update(currentMagazine)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteMagazine(int magazineId)
        {
            if (magazineId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
            }
            var currentMagazine = MagazineDal.GetMagazineById(magazineId);
            if (currentMagazine == null)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNotFoundMagazine;
            }
            return MagazineDal.Delete(magazineId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static MagazineEntity GetMagazineById(int magazineId)
        {
            if (magazineId <= 0)
            {
                return null;
            }
            return MagazineDal.GetMagazineById(magazineId);
        }

        public static List<MagazineEntity> MagazineSearch(string keyword, int type, EnumMagazineStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MagazineDal.Search(keyword, type, (int)status, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region Magazine News

        public static ErrorMapping.ErrorCodes InsertMagazineNews(MagazineNewsEntity entity, ref int newId)
        {
            return MagazineNewsDal.Insert(entity, ref newId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteMagazineNews(int magazineNewsId)
        {
            if (magazineNewsId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNewsNotFoundMagazineNews;
            }
            var currentMagazineNews = MagazineNewsDal.GetMagazineNewsById(magazineNewsId);
            if (currentMagazineNews == null)
            {
                return ErrorMapping.ErrorCodes.UpdateMagazineNewsNotFoundMagazineNews;
            }

            return MagazineNewsDal.Delete(magazineNewsId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateMagazineNews(int magazineId, string deleteNewsId, string addNewsId)
        {
            return MagazineNewsDal.UpdateMagazineNews(magazineId, deleteNewsId, addNewsId) 
                ? ErrorMapping.ErrorCodes.Success 
                : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static List<MagazineNewsEntity> MagazineNewsSearch(string keyword)
        {
            return MagazineNewsDal.Search(keyword);
        }

        #endregion
    }
}
