﻿using System.Collections.Generic;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.MainDal.Databases;

namespace ChannelVN.Magazine.Dal
{
    public class MagazineDal
    {
        public static bool Insert(MagazineEntity magazine, ref int newMagazineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineMainDal.Insert(magazine, ref newMagazineId);
            }
            return returnValue;
        }

        public static bool Update(MagazineEntity magazine)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineMainDal.Update(magazine);
            }
            return returnValue;
        }

        public static bool Delete(int MagazineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineMainDal.Delete(MagazineId);
            }
            return returnValue;
        }

        public static MagazineEntity GetMagazineById(int id)
        {
            MagazineEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineMainDal.GetMagazineById(id);
            }
            return returnValue;
        }

        public static List<MagazineEntity> Search(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<MagazineEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineMainDal.Search(keyword, type, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
