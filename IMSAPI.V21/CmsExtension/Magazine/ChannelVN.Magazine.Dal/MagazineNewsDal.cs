﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.Common;
using ChannelVN.Magazine.Dal.Common;
using ChannelVN.Magazine.Entity;
using ChannelVN.Magazine.MainDal.Databases;

namespace ChannelVN.Magazine.Dal
{
    public class MagazineNewsDal
    {
        public static bool Insert(MagazineNewsEntity entity, ref int newMagazineId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineNewsMainDal.Insert(entity, ref newMagazineId);
            }
            return returnValue;
        }

        public static bool Delete(int MagazineNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineNewsMainDal.Delete(MagazineNewsId);
            }
            return returnValue;
        }

        public static MagazineNewsEntity GetMagazineNewsById(int id)
        {
            MagazineNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineNewsMainDal.GetMagazineNewsById(id);
            }
            return returnValue;
        }

        public static bool UpdateMagazineNews(int magazineId, string deleteNewsId, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineNewsMainDal.UpdateMagazineNews(magazineId, deleteNewsId, addNewsId);
            }
            return returnValue;
        }

        public static List<MagazineNewsEntity> Search(string keyword)
        {
            List<MagazineNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MagazineNewsMainDal.Search(keyword);
            }
            return returnValue;
        }
    }
}
