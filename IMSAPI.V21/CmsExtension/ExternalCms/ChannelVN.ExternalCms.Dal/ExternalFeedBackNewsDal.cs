﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.ExternalCms.Dal
{
    public class ExternalFeedBackNewsDal
    {
        public static ExternalFeedBackNewsEntity GetById(long id)
        {
            ExternalFeedBackNewsEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalFeedBackDal.GetById(id);
            }
            return retVal;
        }
        public static List<ExternalFeedBackNewsCountByParentEntity> GetAllParentIdHasExternalNews()
        {
            List<ExternalFeedBackNewsCountByParentEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalFeedBackDal.GetAllParentIdHasExternalNews();
            }
            return retVal;
        }
        public static List<ExternalFeedBackNewsWithSimpleFieldEntity> Search(string keyword, long parentId, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExternalFeedBackNewsWithSimpleFieldEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalFeedBackDal.Search(keyword, parentId, dateFrom, dateTo, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }

        public static bool DeleteById(long id)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalFeedBackDal.DeleteById(id);
            }
            return retVal;
        }
        public static bool Receive(long id)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalFeedBackDal.Receive(id);
            }
            return retVal;
        }

        public static List<ExFeedBackNewsEntity> SearchNews(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExFeedBackNewsEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.FeedBackExtDal.SearchNews(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static ExFeedBackNewsEntity GetNewsById(long id)
        {
            ExFeedBackNewsEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.FeedBackExtDal.GetById(id);
            }
            return retVal;
        }

        public static bool UpdateStatusIms(long newsId, int status, long feedbackId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.FeedBackExtDal.UpdateStatusIms(newsId, status, feedbackId);
            }
            return retVal;
        }

    }
}
