﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class QuickNewsDal
    {
        #region DB ngoài
        public static QuickNewsEntity ExtDBGetById(long id)
        {
            QuickNewsEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBGetById(id);
            }
            return retVal;
        }
        public static List<QuickNewsEntity> ExtDBSearch(string keyword, int type, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuickNewsEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBSearch(keyword, type, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool ExtDBReceive(long id, long newsId, string receivedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBReceive(id, newsId, receivedBy);
            }
            return retVal;
        }
        public static bool ExtDBDeleteById(long id, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBDeleteById(id, deletedBy);
            }
            return retVal;
        }
        public static bool ExtDBDeleteByIds(string idList, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBDeleteByIds(idList, deletedBy);
            }
            return retVal;
        }
        public static int ExtDBCount(int type, int status)
        {
            int retVal = 0;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsExternalDal.ExtDBCount(type, status);
            }
            return retVal;
        }
        #endregion

        #region DB CMS
        public static bool Insert(QuickNewsEntity quickNews)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickNewsDal.Insert(quickNews);
            }
            return retVal;
        }
        public static QuickNewsEntity GetById(long id)
        {
            QuickNewsEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickNewsDal.GetById(id);
            }
            return retVal;
        }
        #endregion
    }
}
