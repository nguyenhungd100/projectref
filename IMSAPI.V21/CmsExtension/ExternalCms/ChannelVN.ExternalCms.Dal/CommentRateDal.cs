﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CommentRateDal
    {
        public static List<CommentRateEntity> GetUnprocessComment()
        {
            List<CommentRateEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.CommentRateDal.GetUnprocessComment();
            }
            return retVal;
        }

        public static bool UpdateProcessedComment(long commentId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.CommentRateDal.UpdateProcessedComment(commentId);
            }
            return retVal;
        }

        public static List<CommentRateEntity> GetUnprocessLikeComment(DateTime lastModifieddate)
        {
            List<CommentRateEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.CommentRateDal.GetUnprocessLikeComment(lastModifieddate);
            }
            return retVal;
        }
    }
}
