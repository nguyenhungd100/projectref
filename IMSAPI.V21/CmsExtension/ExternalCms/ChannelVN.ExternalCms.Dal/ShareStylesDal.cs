﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class ShareStylesDal
    {
        public static ShareStylesEntity GetById(int styleId)
        {
            ShareStylesEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.ShareStyleDal.GetById(styleId);
            }
            return retVal;
        }
        public static List<ShareStylesEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ShareStylesEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.ShareStyleDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool DeleteById(int styleId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.ShareStyleDal.DeleteById(styleId);
            }
            return retVal;
        }
        public static bool Receive(int styleId, long newsId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.ShareStyleDal.Receive(styleId, newsId);
            }
            return retVal;
        }
    }
}
