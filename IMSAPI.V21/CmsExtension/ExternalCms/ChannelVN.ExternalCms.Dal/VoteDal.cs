﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class VoteDal
    {
        #region Gets
        public static List<VoteAnswersEntity> GetListAnswersByVoteIdExt(int voteId)
        {
            List<VoteAnswersEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.VoteMainDal.GetListAnswersByVoteId(voteId);
            }
            return returnValue;
        }

        #endregion
    }
}
