﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class InterviewQuestionDal
    {
        public static InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            InterviewQuestionEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            }
            return retVal;
        }
        public static List<InterviewQuestionEntity> SearchInterviewQuestion(int interviewId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InterviewQuestionEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.SearchInterviewQuestion(interviewId, keyword, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<InterviewQuestionEntity> GetNotReceiveQuestion()
        {
            List<InterviewQuestionEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.GetNotReceiveQuestion();
            }
            return retVal;
        }
        public static bool ReceiveListQuestion(string listQuestionId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.ReceiveListQuestion(listQuestionId);
            }
            return retVal;
        }
        public static bool ReceiveExternaQuestion(int interviewQuestionId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.ReceiveExternaQuestion(interviewQuestionId);
            }
            return retVal;
        }
        public static bool DeleteExternaQuestion(int interviewQuestionId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.DeleteExternaQuestion(interviewQuestionId);
            }
            return retVal;
        }
        public static bool UpdateExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.UpdateExternalQuestion(interviewQuestion);
            }
            return retVal;
        }
        public static bool InsertExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.InsertExternalQuestion(interviewQuestion);
            }
            return retVal;
        }
        public static int GetInterviewQuestionCount(int interviewId)
        {
            int retVal = 0;
            using (var db = new CmsExternalDb())
            {
                retVal = db.InterviewExtDal.GetInterviewQuestionCount(interviewId);
            }
            return retVal;
        }
    }
}
