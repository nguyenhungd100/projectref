﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CommentPublishedDal
    {

        public static List<CommentPublishedEntity> SearchCommentPublished(string objectTitle, int objectType, string title,
            long parentId, CommentPublishedStatus status, string receivedBy,
            long objectId, int pageIndex, int pageSize, DateTime publishDate, int TitleSearchMode, ref int totalRow)
        {
            List<CommentPublishedEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.SearchCommentPublished(objectTitle, objectType, title, parentId, status,
                                                                    receivedBy, objectId, pageIndex, pageSize,
                                                                    publishDate, TitleSearchMode, ref totalRow);
            }
            return retVal;
        }
        public static CommentPublishedEntity GetCommentPublishedById(long id)
        {
            CommentPublishedEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetCommentPublishedById(id);
            }
            return retVal;
        }
        public static int GetPublishedCommentCountByObjectId(int objectType, long objectId)
        {
            int retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetPublishedCommentCountByNewsId(objectType, objectId);
            }
            return retVal;
        }
        public static int GetReceivedCommentCountByObjectId(int objectType, long objectId)
        {
            int retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetPublishedCommentCountByNewsId(objectType, objectId);
            }
            return retVal;
        }

        public static List<CommentCounterEntity> GetCommentCounterInternalByObjectId(string lstId, int objectType)
        {
            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@ObjectId", lstId),
                                         new SqlParameter("@ObjectType", objectType)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_CommentPublished_GetWaitApproveCommentCouter",
                                                     parameters);
                var lstCommentCounter = new List<CommentCounterEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentCounterEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    lstCommentCounter.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                return lstCommentCounter;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_CommentPublished_GetWaitApproveCommentCouter:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }

        public static List<CommentPublishedEntity> GetPublishedDateNewsByNewsId(string NewsIds)
        {
            List<CommentPublishedEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetPublishedDateNewsByNewsId(NewsIds);
            }
            return retVal;
        }

        public static bool ReceiveCommentPublished(CommentPublishedEntity comment)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.ReceiveCommentPublished(comment);
            }
            return retVal;
        }

        public static bool CommentPublishedAdminInsert(CommentPublishedEntity comment)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.CommentPublishedAdminInsert(comment);
            }
            return retVal;
        }

        public static bool ApproveCommentPublished(long id, string approveBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.ApproveCommentPublished(id, approveBy);
            }
            return retVal;
        }
        public static bool RemoveCommentPublished(long id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.RemoveCommentPublished(id);
            }
            return retVal;
        }
        public static bool UnapproveCommentPublished(long id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UnapproveCommentPublished(id);
            }
            return retVal;
        }

        public static bool UpdateCommentRate(long commentId, int liked, DateTime lastLikeDate)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UpdateCommentRate(commentId, liked, lastLikeDate);
            }
            return retVal;
        }

        public static bool UpdateCommentPublishedById(CommentPublishedEntity commentPublished)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UpdateCommentPublishedById(commentPublished);
            }
            return retVal;
        }

        public static bool DeleteCommentPublishedById(long commentPublishedId, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.DeleteCommentPublishedById(commentPublishedId, deletedBy);
            }
            return retVal;
        }

        public static List<CommentPublishedEntity> StatisticsGetCommentPublished(int objectType, CommentPublishedStatus status, DateTime fromDate, DateTime toDate)
        {
            List<CommentPublishedEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.StatisticsGetCommentPublished(objectType, status, fromDate, toDate);
            }
            return retVal;
        }
        public static List<CommentPublishedEntity> GetListCommentByListId(string commentIds)
        {
            List<CommentPublishedEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetListByListId(commentIds);
            }
            return retVal;
        }
        public static bool DeleteListComments(string commentIds, string LastModifiedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.DeleteListComments(commentIds, LastModifiedBy);
            }
            return retVal;
        }

        public static bool UnApproveListCommentPublished(string commentIds, string LastModifiedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UnApproveListCommentPublished(commentIds, LastModifiedBy);
            }
            return retVal;
        }

        public static bool ApproveListCommentPublished(string commentIds, string PublishBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.ApproveListCommentPublished(commentIds, PublishBy);
            }
            return retVal;
        }

        public static bool ReApproveListCommentPublished(string commentIds, string PublishedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.ReApproveListCommentPublished(commentIds, PublishedBy);
            }
            return retVal;
        }

        public static List<CommentEntity> GetCommentByNewsId(long id, long parentId)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@NewsId", id),
                                         new SqlParameter("@ParentId", parentId)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_CommentPublished_GetPublishedByNewsId",
                                                     parameters);
                List<CommentEntity> lst = new List<CommentEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    lst.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                return lst;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_CommentPublished_GetPublishedByNewsId:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }

        //CuongNP: Add- 2014-11-28
        public static List<NewsHasNewCommentEntity> GetNewsHasNewComment(string objectTitle, int objectType, CommentPublishedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsHasNewCommentEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.GetNewsHasNewComment(objectTitle, objectType, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }

        public static List<CommentPublishedEntity> SearchCommentPublishedV2(string objectTitle, int objectType, string content,
            long parentId, CommentPublishedStatus status, string receivedBy,
            long objectId, string zoneIds, int pageIndex, int pageSize, DateTime publishDate, ref int totalRow)
        {
            List<CommentPublishedEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.SearchCommentPublishedV2(objectTitle, objectType, content, parentId, status,
                                                                    receivedBy, objectId, zoneIds, pageIndex, pageSize,
                                                                    publishDate, ref totalRow);
            }
            return retVal;
        }
        public static List<CommentPublishedCountEntity> CommentPublishedCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            List<CommentPublishedCountEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.CommentPublishedCount(fromDate, toDate, zoneIds);
            }
            return retVal;
        }
        public static List<CommentPublishedStatisticsByAccount> CommentPublished_Statistics(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            List<CommentPublishedStatisticsByAccount> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.CommentPublished_Statistics(fromDate, toDate, zoneIds);
            }
            return retVal;
        }
        public static List<CommentPublishedCountByZoneEntity> CommentPublishedCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            List<CommentPublishedCountByZoneEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.CommentPublishedCountByZone(fromDate, toDate, zoneIds);
            }
            return retVal;
        }

        public static bool UpdateLikeCountComment(string value)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CommentPublishDal.UpdateLikeCountComment(value);
            }
            return returnValue;
        }

        public static bool InsertCommentLogAction(CommentLogEntity commentLog)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.InsertCommentLogAction(commentLog);
            }
            return retVal;
        }
        public static bool UpdateMailForwarded(long id, string mailForwarded)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UpdateMailForwarded(id, mailForwarded);
            }
            return retVal;
        }
        public static bool UpdateMailReplied(long id, string mailReplied)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.CommentPublishDal.UpdateMailReplied(id, mailReplied);
            }
            return retVal;
        }
    }
}
