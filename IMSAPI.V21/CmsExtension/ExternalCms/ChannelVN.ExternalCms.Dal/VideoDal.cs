﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class VideoDal
    {
        #region Gets

        public static VideoEntity GetById(int videoId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.VideoMainDal.GetById(videoId, ref zoneName);
            }
            return returnValue;
        }

        #endregion
    }
}
