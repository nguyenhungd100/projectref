﻿using System.Collections.Generic;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class VideoRoyaltyAuthorDal
    {
        #region GET
        public static List<VideoRoyaltyAuthorEntity> GetByVideoId(long id)
        {
            List<VideoRoyaltyAuthorEntity> returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.GetByVideoId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
