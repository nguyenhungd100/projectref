﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class QuickNewsAuthorDal
    {
        #region DB ngoài
        public static QuickNewsAuthorEntity ExtDBGetById(long vietId)
        {
            QuickNewsAuthorEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsAuthorExtDal.ExtDBGetById(vietId);
            }
            return retVal;
        }
        public static QuickNewsAuthorEntity ExtDBGetByEmail(string email)
        {
            QuickNewsAuthorEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsAuthorExtDal.ExtDBGetByEmail(email);
            }
            return retVal;
        }
        public static List<QuickNewsAuthorEntity> ExtDBSearch(string keyword, string email, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuickNewsAuthorEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsAuthorExtDal.ExtDBSearch(keyword, email, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }        
        #endregion

        #region DB CMS
        public static bool Insert(QuickNewsAuthorEntity quickNewsAuthor)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickNewsAuthorDal.Insert(quickNewsAuthor);
            }
            return retVal;
        }
        public static bool DeleteById(int id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickNewsAuthorDal.DeleteById(id);
            }
            return retVal;
        }
        #endregion
    }
}
