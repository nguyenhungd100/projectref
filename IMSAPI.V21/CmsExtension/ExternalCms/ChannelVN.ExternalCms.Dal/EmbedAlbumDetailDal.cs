﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class EmbedAlbumDetailDal
    {
        public static List<EmbedAlbumDetailEntity> GetByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            List<EmbedAlbumDetailEntity> returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.GetByEmbedAlbumId(embedAlbumId, topPhoto);

            }
            return returnValue;
        }
        public static EmbedAlbumDetailEntity GetById(long embedAlbumId)
        {
            EmbedAlbumDetailEntity returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.GetById(embedAlbumId);

            }
            return returnValue;
        }

        public static bool Insert(EmbedAlbumDetailEntity embedAlbum, ref int newEmbedAlbumDetailId)
        {
            bool returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.Insert(embedAlbum, ref  newEmbedAlbumDetailId);

            }
            return returnValue;
        }
        public static bool Update(EmbedAlbumDetailEntity embedAlbum)
        {
            bool returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.Update(embedAlbum);

            }
            return returnValue;
        }
        public static bool DeleteByEmbedAlbumIdExcudeListIds(int embedAlbumId, string excludeListIds)
        {
            bool returnValue;
            using (var db = new ImsExternalDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.DeleteByEmbedAlbumIdExcudeListIds(embedAlbumId, excludeListIds);

            }
            return returnValue;
        }
    }
}
