﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class QuickNewsImagesDal
    {
        #region DB ngoài
        public static List<QuickNewsImagesEntity> ExtDBGetByQuickNewsId(long quickNewsId)
        {
            List<QuickNewsImagesEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickNewsImagesExternalDal.ExtDBGetByQuickNewsId(quickNewsId);
            }
            return retVal;
        }
        #endregion

        #region DB CMS
        public static bool Insert(QuickNewsImagesEntity quickNewsImage)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickNewsImagesDal.Insert(quickNewsImage);
            }
            return retVal;
        }
        #endregion
    }
}
