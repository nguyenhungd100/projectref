﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class FeedBackNewsDal
    {
        public static FeedBackNewsEntity GetById(long id)
        {
            FeedBackNewsEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.GetById(id);
            }
            return retVal;
        }
        public static FeedBackNewsEntity GetByParentId(long parentId)
        {
            FeedBackNewsEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.GetByParentId(parentId);
            }
            return retVal;
        }
        public static List<FeedBackNewsWithSimpleFieldEntity> Search(string keyword, long parentId, int status, int filterDateField, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FeedBackNewsWithSimpleFieldEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.Search(keyword, parentId, status, filterDateField, dateFrom,
                                                dateTo, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNewsHasExternalNews(string listParentId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FeedBackNewsWithSimpleFieldEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.SearchFeedBackNewsHasExternalNews(listParentId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }

        public static bool DeleteById(long id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.DeleteById(id);
            }
            return retVal;
        }
        public static bool Insert(FeedBackNewsEntity feedBackNews)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.Insert(feedBackNews);
            }
            return retVal;
        }
        public static bool Update(FeedBackNewsEntity feedBackNews)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.Update(feedBackNews);
            }
            return retVal;
        }
        public static bool Publish(long id, string publishedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.Publish(id, publishedBy);
            }
            return retVal;
        }
        public static bool Unpublish(long id)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FeedBackDal.Unpublish(id);
            }
            return retVal;
        }

        public static bool ExtDBInsert(FeedBackNewsEntity feedBackNews)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.FeedBackExtDal.ExtDBInsert(feedBackNews);
            }
            return retVal;
        }
    }
}
