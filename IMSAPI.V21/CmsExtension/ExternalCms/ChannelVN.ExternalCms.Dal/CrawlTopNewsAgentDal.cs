﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CrawlTopNewsAgentDal
    {
        public static List<CrawlTopNewsAgentEntity> GetAllAgent()
        {
            List<CrawlTopNewsAgentEntity> retVal;
            using (var db = new CmsCrawlerDb())
            {
                retVal = db.CrawlerTopNewsAgentDal.GetAllAgent();
            }
            return retVal;
        }
    }
}
