﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CrawlTopNewsCategoryDal
    {
        public static List<CrawlTopNewsCategoryEntity> GetCategoryByAgent(int id)
        {
            //    List<CrawlTopNewsAgentEntity> retVal;
            //    using (var db = new CmsCrawlerDb())
            //    {
            //        retVal = db.CrawlerTopNewsAgentDal.GetAllAgent();
            //    }
            //    return retVal;

            //const string query = "SELECT a.ID , a.ParentID , a.CategoryName , a.Status , a.Site FROM dbo.Category a INNER JOIN dbo.news_angent b ON a.Site=b.Source WHERE a.Status = 1 AND (@Id = 0 OR (@Id > 0 AND b.news_agent_id=@Id))";
            //const string query = "SELECT a.ID , a.ParentID , a.CategoryName , a.Status , a.Site FROM dbo.Category a INNER JOIN dbo.news_angent b ON a.Site=b.Source WHERE a.Status = 1";
            const string query =
                "SELECT a.Cat_ID AS ID , a.Cat_ParentID AS ParentID , a.CategoryName , CASE a.Status WHEN 1 THEN 1 ELSE 0 END AS Status , a.Source AS Site FROM dbo.Category a INNER JOIN dbo.news_angent b ON a.Source=b.Source WHERE a.Status = 1";
            SqlDataReader reader = null;
            try
            {
                var parameters = new SqlParameter[]
                                     {
                                         new SqlParameter("@Id", id)
                                     };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.CmsCrawlerDb),
                                                          query,
                                                          parameters);
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.CmsCrawlerDb),
                                                    CommandType.Text,
                                                    query, parameters);

                var listData = new List<CrawlTopNewsCategoryEntity>();
                while (reader.Read())
                {
                    var data = new CrawlTopNewsCategoryEntity();
                    EntityBase.SetObjectValue(reader, ref data);
                    listData.Add(data);
                }
                return listData;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CrawlTopNewsCategoryDal.GetCategoryByAgent() error => {0}", ex));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
    }
}
