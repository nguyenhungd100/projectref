﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CrawlTopNewsDal
    {
        public static CrawlTopNewsEntity GetById(int id)
        {
            CrawlTopNewsEntity retVal;
            using (var db = new CmsCrawlerDb())
            {
                retVal = db.CrawlerTopNewsDal.GetById(id);
            }
            return retVal;
        }
        public static List<CrawlTopNewsEntity> Search(int newsAngentId, string keyword, int pageIndex, int pageSize, int category, DateTime from, DateTime to, ref int totalRow)
        {
            List<CrawlTopNewsEntity> retVal;
            using (var db = new CmsCrawlerDb())
            {
                retVal = db.CrawlerTopNewsDal.Search(newsAngentId, keyword, pageIndex, pageSize, category, from, to, ref totalRow);
            }
            return retVal;
        }
    }
}
