﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class CommentDal
    {
        #region Get methods

        public static List<CommentEntity> Search(string objectTitle, int objectType, string title, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                    {
                                        new SqlParameter("@ObjectTitle",objectTitle),
                                        new SqlParameter("@ObjectType",objectType),
                                        new SqlParameter("@Title", title),
                                        new SqlParameter("@ParentId", parentId),
                                        new SqlParameter("@Status",(int)status),
                                        new SqlParameter("@ObjectId",objectId),
                                        new SqlParameter("@PageIndex",pageIndex),
                                        new SqlParameter("@PageSize",pageSize),
                                        new SqlParameter("@ZoneIds", zoneIds),
                                       new SqlParameter("@TotalRow", totalRow)
                                             {Direction = ParameterDirection.Output}
                                     };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + "CMS_Comment_Search",
                                                          parameters);

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_Comment_Search", parameters);

                var commentEntities = new List<CommentEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    commentEntities.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                totalRow = int.Parse(parameters[parameters.Length - 1].Value.ToString());
                return commentEntities;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Search:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
        public static CommentEntity GetCommentById(long id)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@Id", id)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_Comment_GetById",
                                                     parameters);
                var commentEntity = new CommentEntity();
                if (reader.Read())
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                else commentEntity = null;
                reader.Close();
                reader.Dispose();
                return commentEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_GetById:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
        public static List<CommentEntity> StatisticsGetComment(int objectType, CommentStatus status, DateTime fromDate, DateTime toDate)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                    {
                                        new SqlParameter("@ObjectType",objectType),
                                        new SqlParameter("@Status",(int)status),
                                        (fromDate <= DbCommon.MinDateTime
                                              ? new SqlParameter("@DateFrom", DBNull.Value)
                                              : new SqlParameter("@DateFrom", fromDate)),
                                         (toDate <= DbCommon.MinDateTime
                                              ? new SqlParameter("@DateTo", DBNull.Value)
                                              : new SqlParameter("@DateTo", toDate))
                                     };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + "CMS_Comment_CommentStatisticByStatus",
                                                          parameters);

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_Comment_CommentStatisticByStatus", parameters);

                var commentEntities = new List<CommentEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    commentEntities.Add(commentEntity);
                }

                reader.Close();
                reader.Dispose();
                return commentEntities;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_StatisticsGetComment:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
        public static int GetWaitApproveCommentCountByObjectId(int objectType, long objectId)
        {
            if (DbCommon.IsUseMainDal)
            {
                int retVal;
                using (var db = new CmsExternalDb())
                {
                    retVal = db.CommentDal.GetWaitApproveCommentCountByObjectId(objectType, objectId);
                }
                return retVal;
            }
            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@ObjectType", objectType),
                                         new SqlParameter("@ObjectId", objectId)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_Comment_GetWaitApproveCommentCountByNewsId",
                                                     parameters);
                var commentCount = 0;
                if (reader.Read())
                    commentCount = Utility.ConvertToInt(reader[0]);
                reader.Close();
                reader.Dispose();
                return commentCount;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_GetWaitApproveCommentCountByNewsId:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
        public static List<CommentCounterEntity> GetCommentCounterByObjectId(string lstId, int objectType)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@ObjectId", lstId),
                                         new SqlParameter("@ObjectType", objectType)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_Comment_GetUnApproveCommentCounter",
                                                     parameters);
                var lstCommentCounter = new List<CommentCounterEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentCounterEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    lstCommentCounter.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                return lstCommentCounter;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_GetUnApproveComment:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }
        //Add NPC
        public static List<CommentCounterEntity> GetCommentCounterByObjectIdAndStatus(string lstId, int objectType, int status)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]{
                                         new SqlParameter("@ObjectId", lstId),
                                         new SqlParameter("@ObjectType", objectType),
                                         new SqlParameter("@Status", status)
                                     };

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                     CommandType.StoredProcedure,
                                                     DbCommon.DatabaseSchema + "CMS_Comment_CounterByObjectIdAndStatus",
                                                     parameters);
                var lstCommentCounter = new List<CommentCounterEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentCounterEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    lstCommentCounter.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                return lstCommentCounter;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_CounterByObjectIdAndStatus:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }


        public static List<NewsHasNewCommentExtEntity> GetNewsHasNewCommentExt(string objectTitle, int objectType, CommentStatus status, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                    {
                                        new SqlParameter("@ObjectTitle",objectTitle),
                                        new SqlParameter("@ObjectType",objectType),
                                        new SqlParameter("@Status",(int)status),
                                        new SqlParameter("@ZoneIds",zoneIds),
                                        new SqlParameter("@PageIndex",pageIndex),
                                        new SqlParameter("@PageSize",pageSize),
                                        new SqlParameter("@TotalRow", totalRow)
                                             {Direction = ParameterDirection.Output}
                                     };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + "CMS_News_GetNewsHasNewCommentExt",
                                                          parameters);

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_News_GetNewsHasNewCommentExt", parameters);

                var commentEntities = new List<NewsHasNewCommentExtEntity>();
                while (reader.Read())
                {
                    var commentEntity = new NewsHasNewCommentExtEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    commentEntities.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                //totalRow = int.Parse(parameters[parameters.Length - 1].Value.ToString());
                totalRow = 0;
                return commentEntities;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_News_GetNewsHasNewCommentExt:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }

        public static List<CommentEntity> SearchV2(string objectTitle, int objectType, string content, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                    {
                                        new SqlParameter("@ObjectTitle",objectTitle),
                                        new SqlParameter("@ObjectType",objectType),
                                        new SqlParameter("@Content", content),
                                        new SqlParameter("@ParentId", parentId),
                                        new SqlParameter("@Status",(int)status),
                                        new SqlParameter("@ObjectId",objectId),
                                        new SqlParameter("@PageIndex",pageIndex),
                                        new SqlParameter("@PageSize",pageSize),
                                        new SqlParameter("@ZoneIds", zoneIds),
                                       new SqlParameter("@TotalRow", totalRow)
                                             {Direction = ParameterDirection.Output}
                                     };
                SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                          DbCommon.DatabaseSchema + "CMS_Comment_SearchV2",
                                                          parameters);

                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_Comment_SearchV2", parameters);

                var commentEntities = new List<CommentEntity>();
                while (reader.Read())
                {
                    var commentEntity = new CommentEntity();
                    EntityBase.SetObjectValue(reader, ref commentEntity);
                    commentEntities.Add(commentEntity);
                }
                reader.Close();
                reader.Dispose();
                totalRow = int.Parse(parameters[parameters.Length - 1].Value.ToString());
                return commentEntities;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_SearchV2:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }

        public static List<CommentEntity> SearchObjectByUser(long userId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.CommentDal.SearchObjectByUser(userId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CommentEntity> SearchCommentByUserObject(long userId, int status, long objectId)
        {
            List<CommentEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.CommentDal.SearchCommentByUserObject(userId, status, objectId);
            }
            return returnValue;
        }
        public static List<CommentCountEntity> CommentCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            List<CommentCountEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.CommentDal.CommentCount(fromDate, toDate, zoneIds);
            }
            return returnValue;
        }
        public static List<CommentCountByZoneEntity> CommentCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            List<CommentCountByZoneEntity> returnValue;
            using (var db = new CmsExternalDb())
            {
                returnValue = db.CommentDal.CommentCountByZone(fromDate, toDate, zoneIds);
            }
            return returnValue;
        }
        #endregion

        #region Set methods

        public static bool UpdateCommentStatusById(long id, string approveBy)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@Id", id),
                                    new SqlParameter("@ApproveBy", approveBy)
                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_Approve",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Approve:{0}", ex.Message));
            }
        }

        public static bool DeleteCommentById(long id, string deletedBy)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@Id", id),
                                    new SqlParameter("@DeletedBy", deletedBy)
                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_Delete",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Delete:{0}", ex.Message));
            }
        }

        public static bool UpdateCommentById(CommentEntity comment)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@Id", comment.Id),
                                    new SqlParameter("@Title", comment.Title),
                                    new SqlParameter("@Content", comment.Content),
                                    new SqlParameter("@SenderEmail", comment.SenderEmail),
                                    new SqlParameter("@SenderFullName", comment.SenderFullName),
                                    new SqlParameter("@LastModifiedBy", comment.LastModifiedBy)
                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_Update",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Update:{0}", ex.Message));
            }
        }

        public static bool RemoveCommentById(long id, string LastModifiedBy)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@Id", id),
                                    new SqlParameter("@LastModifiedBy", LastModifiedBy),
                                    new SqlParameter("@DeletedBy", LastModifiedBy)
                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_UpdateStatus",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_UpdateStatus:{0}", ex.Message));
            }
        }

        public static bool DeleteListComments(string commentIds, string LastModifiedBy)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@commentIds", commentIds),
                                    new SqlParameter("@LastModifiedBy", LastModifiedBy),
                                    new SqlParameter("@DeletedBy", LastModifiedBy)

                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_Delete_ListComments",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Delete_ListComments:{0}", ex.Message));
            }
        }

        public static bool InsertComment(CommentEntity comment)
        {

            try
            {
                var parameters = new[]
                                {
                                    new SqlParameter("@ParentId", comment.ParentId),
                                    new SqlParameter("@ObjectId", comment.ObjectId),
                                    new SqlParameter("@ObjectTitle", comment.ObjectTitle),
                                    new SqlParameter("@ObjectUrl", comment.ObjectUrl),
                                    new SqlParameter("@ZoneId", comment.ZoneId),
                                    new SqlParameter("@Title", comment.Title),
                                    new SqlParameter("@Content", comment.Content),
                                    new SqlParameter("@Emotion", comment.Emotion),
                                    new SqlParameter("@SenderEmail", comment.SenderEmail),
                                    new SqlParameter("@SenderFullName", comment.SenderFullName),
                                    new SqlParameter("@SenderAvatar", comment.SenderAvatar),
                                    new SqlParameter("@SenderAddress", comment.SenderAddress),
                                    new SqlParameter("@SenderPhoneNumber", comment.SenderPhoneNumber),
                                    new SqlParameter("@CreatedDate", comment.CreatedDate)
                                };

                int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.ExternalCmsDb),
                                                               CommandType.StoredProcedure,
                                                               DbCommon.DatabaseSchema + "CMS_Comment_Insert",
                                                               parameters);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Comment_Insert:{0}", ex.Message));
            }
        }


        public static bool UpdateMailForwarded(long id, string mailForwarded)
        {
            bool retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.CommentDal.UpdateMailForwarded(id, mailForwarded);
            }
            return retVal;
        }
        public static bool UpdateMailReplied(long id, string mailReplied)
        {
            bool retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.CommentDal.UpdateMailReplied(id, mailReplied);
            }
            return retVal;
        }

        #endregion
    }
}
