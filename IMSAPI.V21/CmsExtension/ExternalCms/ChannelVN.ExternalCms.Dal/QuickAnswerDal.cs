﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class QuickAnswerDal
    {
        #region DB ngoài
        public static QuickAnswerEntity ExtDBGetById(int id)
        {
            QuickAnswerEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBGetById(id);
            }
            return retVal;
        }
        public static List<QuickAnswerEntity> ExtDBSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuickAnswerEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBSearch(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool ExtDBReceive(int id, string receivedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBReceive(id, receivedBy);
            }
            return retVal;
        }
        public static bool ExtDBDeleteById(int id, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBDeleteById(id, deletedBy);
            }
            return retVal;
        }
        public static bool ExtDBDeleteByIds(string idList, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBDeleteByIds(idList, deletedBy);
            }
            return retVal;
        }
        public static int ExtDBCount(int type, int status)
        {
            int retVal = 0;
            using (var db = new CmsExternalDb())
            {
                retVal = db.QuickAnswerExternalDal.ExtDBCount(type, status);
            }
            return retVal;
        }
        #endregion

        #region DB CMS
        public static bool Insert(QuickAnswerEntity quickAnswer)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.Insert(quickAnswer);
            }
            return retVal;
        }
        public static bool Update(QuickAnswerEntity quickAnswer)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.Update(quickAnswer);
            }
            return retVal;
        }
        public static QuickAnswerEntity GetById(int id)
        {
            QuickAnswerEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.GetById(id);
            }
            return retVal;
        }
        public static List<QuickAnswerEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuickAnswerEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool DeleteById(int id, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.DeleteById(id, deletedBy);
            }
            return retVal;
        }
        public static bool DeleteByIds(string idList, string deletedBy)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.QuickAnswerDal.DeleteByIds(idList, deletedBy);
            }
            return retVal;
        }        
        #endregion
    }
}
