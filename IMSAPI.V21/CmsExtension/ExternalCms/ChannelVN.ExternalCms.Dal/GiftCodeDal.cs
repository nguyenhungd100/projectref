﻿using System;
using System.Collections.Generic;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class GiftCodeDal
    {
        #region Get methods

        public static List<GiftCodeEntity> GetByNewsId(long newsId)
        {
            List<GiftCodeEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.GiftCodeDal.GetByNewsId(newsId);
            }
            return retVal;
        }

        #endregion
    }
}
