﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class WeatherDal
    {
        #region Get methods

        public static List<ExternalWeatherEntity> GetAll()
        {
            List<ExternalWeatherEntity> retVal;
            using (var db = new ExternalCmsMainDb())
            {
                retVal = db.WeatherContentMainDal.GetAll();
            }
            return retVal;
        }
        public static ExternalWeatherEntity GetByCodeId(int id)
        {
            SqlDataReader reader = null;
            try
            {
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.ExternalWeatherDb), CommandType.Text, "SELECT * FROM Weather WHERE CodeId = " + id);
                var weatherEntity = new ExternalWeatherEntity();
                if (reader.Read())
                    EntityBase.SetObjectValue(reader, ref weatherEntity);
                else weatherEntity = null;
                reader.Close();
                reader.Dispose();
                return weatherEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("SELECT * FROM Weather WHERE CodeId = ...: {0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed) reader.Close();
            }
        }

        #endregion
    }
}
