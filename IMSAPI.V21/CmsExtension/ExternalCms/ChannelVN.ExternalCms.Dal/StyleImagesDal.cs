﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class StyleImagesDal
    {
        public static List<StyleImagesEntity> GetByStyleId(int styleId, int top)
        {
            List<StyleImagesEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.StyleImagesContentMainDal.GetByStyleId(styleId, top);
            }
            return retVal;
        }
    }
}
