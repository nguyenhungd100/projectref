﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class ExternalVideoDal
    {
        public static ExternalVideoEntity GetById(int id)
        {
            ExternalVideoEntity retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalVideoDal.GetById(id);
            }
            return retVal;
        }
        public static List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExternalVideoEntity> retVal;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalVideoDal.GetUnprocessExternalVideo(keyword, zoneVideoId, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static bool RecieveVideo(int videoId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalVideoDal.RecieveVideo(videoId);
            }
            return retVal;
        }
        public static bool DeleteVideo(int videoId)
        {
            bool retVal = false;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalVideoDal.DeleteVideo(videoId);
            }
            return retVal;
        }
        public static int GetVideoCounter()
        {
            int retVal = 0;
            using (var db = new CmsExternalDb())
            {
                retVal = db.externalVideoDal.GetVideoCounter();
            }
            return retVal;
        }
    }
}
