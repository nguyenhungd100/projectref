﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.Dal
{
    public class ExternalNewsDal
    {
        #region Get methods
        public static List<ExternalNewsEntity> SearchNews(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExternalNewsEntity> retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.SearchNews(keyword, zoneIds, fromDate, toDate, type, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<ExternalNewsEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ExternalNewsEntity> retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.SearchNews(keyword, username, zoneIds, fromDate, toDate, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static ExternalNewsEntity GetExternalNewsById(long id)
        {
            ExternalNewsEntity retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.GetExternalNewsById(id);
            }
            return retVal;
        }
        public static ExternalNewsEntity GetExternalNewsByImsNewsId(long id)
        {
            ExternalNewsEntity retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.GetExternalNewsByImsNewsId(id);
            }
            return retVal;
        }

        public static List<ExternalNewsInZoneEntity> GetZoneByNewsId(long id)
        {
            List<ExternalNewsInZoneEntity> retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.GetZoneByNewsId(id);
            }
            return retVal;
        }

        public static List<ExternalNewsAuthorEntity> GetAuthorByNewsId(long id)
        {
            List<ExternalNewsAuthorEntity> retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.GetAuthorByNewsId(id);
            }
            return retVal;
        }

        #endregion

        #region Set methods
        public static bool ReceiveExternalNews(ExternalNewsEntity ExternalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.ReceiveExternalNews(ExternalNews);
            }
            return retVal;
        }
        public static bool PublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.PublishExternalNews(ExternalNews);
            }
            return retVal;
        }
        public static bool ReturnExternalNews(ExternalNewsEntity ExternalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.ReturnExternalNews(ExternalNews);
            }
            return retVal;
        }
        public static bool UnpublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.UnpublishExternalNews(ExternalNews);
            }
            return retVal;
        }
        public static bool UpdateImsNewsInfo(ExternalNewsEntity ExternalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.UpdateImsNewsInfo(ExternalNews);
            }
            return retVal;
        }
        public static bool UpdateImsNewsBody(ExternalNewsEntity externalNews)
        {
            bool retVal;
            using (var db = new ImsExternalDb())
            {
                retVal = db.ExternalNewsDal.UpdateImsNewsBody(externalNews);
            }
            return retVal;
        }
        #endregion
    }
}
