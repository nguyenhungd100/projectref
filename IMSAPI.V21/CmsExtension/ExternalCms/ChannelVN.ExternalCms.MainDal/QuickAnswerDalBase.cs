﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickAnswerDalBase
    {
        #region function SET
        public bool Insert(QuickAnswerEntity quickAnswer)
        {
            const string commandText = "CMS_QuickAnswer_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", quickAnswer.Id);
                _db.AddParameter(cmd, "FullName", quickAnswer.FullName);
                _db.AddParameter(cmd, "Address", quickAnswer.Address);
                _db.AddParameter(cmd, "Email", quickAnswer.Email);
                _db.AddParameter(cmd, "Phone", quickAnswer.Phone);                
                _db.AddParameter(cmd, "Title", quickAnswer.Title);
                _db.AddParameter(cmd, "AttachFile", quickAnswer.AttachFile);
                _db.AddParameter(cmd, "Status", quickAnswer.Status);
                _db.AddParameter(cmd, "QuestedDate", quickAnswer.QuestedDate);
                _db.AddParameter(cmd, "AnsweredBy", quickAnswer.AnsweredBy);
                _db.AddParameter(cmd, "AnsweredDate", quickAnswer.AnsweredDate);
                _db.AddParameter(cmd, "ModifiedBy", quickAnswer.ModifiedBy);                                              
                _db.AddParameter(cmd, "ModifiedDate", quickAnswer.ModifiedDate);                                
                _db.AddParameter(cmd, "QuestContent", quickAnswer.QuestContent);
                _db.AddParameter(cmd, "AnswerContent", quickAnswer.AnswerContent);

                var result = _db.ExecuteNonQuery(cmd);
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(QuickAnswerEntity quickAnswer)
        {
            const string commandText = "CMS_QuickAnswer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", quickAnswer.Id);
                //_db.AddParameter(cmd, "FullName", quickAnswer.FullName);
                //_db.AddParameter(cmd, "Address", quickAnswer.Address);
                //_db.AddParameter(cmd, "Email", quickAnswer.Email);
                //_db.AddParameter(cmd, "Phone", quickAnswer.Phone);
                _db.AddParameter(cmd, "Title", quickAnswer.Title);
                //_db.AddParameter(cmd, "AttachFile", quickAnswer.AttachFile);
                //_db.AddParameter(cmd, "Status", quickAnswer.Status);
                //_db.AddParameter(cmd, "QuestedDate", quickAnswer.QuestedDate);
                //_db.AddParameter(cmd, "AnsweredBy", quickAnswer.AnsweredBy);
                //_db.AddParameter(cmd, "AnsweredDate", quickAnswer.AnsweredDate);
                _db.AddParameter(cmd, "ModifiedBy", quickAnswer.ModifiedBy);
                _db.AddParameter(cmd, "ModifiedDate", quickAnswer.ModifiedDate);
                _db.AddParameter(cmd, "QuestContent", quickAnswer.QuestContent);
                _db.AddParameter(cmd, "AnswerContent", quickAnswer.AnswerContent);

                var result = _db.ExecuteNonQuery(cmd);
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public QuickAnswerEntity GetById(int id)
        {
            const string commandText = "CMS_QuickAnswer_GetById";
            try
            {
                QuickAnswerEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<QuickAnswerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<QuickAnswerEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_QuickAnswer_Search";
            try
            {
                List<QuickAnswerEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuickAnswerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int id, string deletedBy)
        {
            const string commandText = "CMS_QuickAnswer_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByIds(string idList, string deletedBy)
        {
            const string commandText = "CMS_QuickAnswer_DeleteByIdList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", idList);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho QuickAnswer
        /// chinhnb
        /// 2018/11/16
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuickAnswerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
