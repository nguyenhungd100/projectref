﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class ExternalVideoDalBase
    {
        public ExternalVideoEntity GetById(int id)
        {
            const string commandText = "CMS_UploadFiles_GetById";
            try
            {
                ExternalVideoEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ExternalVideoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_UploadFiles_Search";
            try
            {
                List<ExternalVideoEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneVideo", zoneVideoId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow);
                data = _db.GetList<ExternalVideoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RecieveVideo(int videoId) {

            const string commandText = "CMS_UploadFiles_RecieveVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteVideo(int videoId) {
            const string commandText = "CMS_UploadFiles_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetVideoCounter() {
            const string commandText = "CMS_UploadFiles_GetCounter";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = cmd.ExecuteReader();
                if (data.Read())
                    return (int)data[0];
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho ExternalVideo
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected ExternalVideoDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
