﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class CrawlTopNewsDalBase
    {
        public CrawlTopNewsEntity GetById(int id)
        {
            const string commandText = "SELECT newsID, news_title, news_des, news_title_char, news_des_char, news_link, news_date, news_angent_id, news_key_id, news_content, news_image, slide_image, news_status, news_url, news_main_tag, news_tag, news_sitemap, news_crawl, news_follow, news_load, news_duyet, news_non_html, datecreate, category, click, status, is_move_cms FROM news WHERE newsID = @Id";
            try
            {
                CrawlTopNewsEntity data;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<CrawlTopNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CrawlTopNewsEntity> Search(int newsAngentId, string keyword, int pageIndex, int pageSize, int category, DateTime from, DateTime to, ref int totalRow)
        {
            var fromWhere = string.Empty;
            var toWhere = string.Empty;
            if (from != DateTime.MinValue)
            {
                fromWhere = " a.datecreate>= '" + from.ToString("yyyy-MM-dd HH:mm:ss") + "' AND ";
            }
            if (to != DateTime.MinValue)
            {
                toWhere += " a.datecreate< '" + to.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "' AND ";
            }
            string where = " WHERE " + fromWhere + toWhere + " (@news_angent_id <= 0 OR (@news_angent_id > 0 AND a.news_angent_id = @news_angent_id)) AND (@keyword = '' OR (@keyword <> '' AND PATINDEX(@keyword, a.news_title) > 0))  AND (@category<=0 OR a.category=@category) AND (a.news_status = 0) ";
            var commandText = "";
            if (pageIndex <= 1)
            {
                commandText += "SELECT @TotalRow=COUNT(*) FROM news a INNER JOIN category b on a.category=b.id " + where + ";";
                commandText += "SELECT TOP(" + pageSize + ") a.newsID, b.CategoryName, d.news_angent_name as SourceName, a.news_title, a.news_image, a.news_status, a.news_url, a.datecreate, a.status FROM news a INNER JOIN category b on a.category=b.id inner join news_channel_agent d on a.news_angent_id=d.news_agent_id " + where;
                commandText += "ORDER BY a.datecreate desc, a.news_status";
            }
            else
            {
                commandText += "SELECT @TotalRow=COUNT(*) FROM news a INNER JOIN category b on a.category=b.id " + where + ";";
                commandText += "SELECT * FROM (";
                commandText += "SELECT a.newsID, b.CategoryName, d.news_angent_name as SourceName, a.news_title, a.news_image, a.news_status, a.news_url, a.datecreate, a.status, row_number() OVER (ORDER BY a.datecreate desc, a.news_status) AS RowNum FROM news a INNER JOIN category b on a.category=b.id inner join news_channel_agent d on a.news_angent_id=d.news_agent_id ";
                commandText += where + ") AS n WHERE RowNum BETWEEN ((@pageIndex - 1) * @pageSize + 1) AND (@pageIndex * @pageSize) ";
                commandText += "ORDER BY datecreate desc, news_status";
            }

            try
            {
                Logger.WriteLog(Logger.LogType.Debug, commandText);
                List<CrawlTopNewsEntity> data;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "totalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "news_angent_id", newsAngentId);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);
                _db.AddParameter(cmd, "category", category);
                data = _db.GetList<CrawlTopNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho  CrawlTopNews
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsCrawlerDb _db;

        protected CrawlTopNewsDalBase(CmsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
