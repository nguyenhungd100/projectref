﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class GiftCodeDalBase
    {
        public List<GiftCodeEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_GiftCode_GetByNewsId";
            try
            {
                List<GiftCodeEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsid", newsId);

                data = _db.GetList<GiftCodeEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Constructor

        private readonly CmsExternalDb _db;

        protected GiftCodeDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
