﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickAnswerExternalDalBase
    {
        #region function SET
        public QuickAnswerEntity ExtDBGetById(int id)
        {
            const string commandText = "CMS_QuickAnswer_GetById";
            try
            {
                QuickAnswerEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<QuickAnswerEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuickAnswerEntity> ExtDBSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_QuickAnswer_Search";
            try
            {
                List<QuickAnswerEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);                
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuickAnswerEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ExtDBReceive(int id, string receivedBy) {
            const string commandText = "CMS_QuickAnswer_Receive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuickAnswerId", id);               
                _db.AddParameter(cmd, "ReceivedBy", receivedBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ExtDBDeleteById(int id, string deletedBy) {
            const string commandText = "CMS_QuickAnswer_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ExtDBDeleteByIds(string idList, string deletedBy) {
            const string commandText = "CMS_QuickAnswer_DeleteByIdList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", idList);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int ExtDBCount(int type, int status) {
            const string commandText = "CMS_QuickAnswer_Count";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                var result = cmd.ExecuteReader();
                if (result.Read())
                    return (int)result[0];
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho QuickAnswer
        /// chinhnb
        /// 2018/11/16
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected QuickAnswerExternalDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
