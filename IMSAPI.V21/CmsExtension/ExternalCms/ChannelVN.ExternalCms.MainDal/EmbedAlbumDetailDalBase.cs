﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class EmbedAlbumDetailDalBase
    {
        public List<EmbedAlbumDetailEntity> GetByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            const string commandText = "CMS_EmbedAlbumDetail_GetByEmbedAlbumId";
            try
            {
                List<EmbedAlbumDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "EmbedAlbumId", embedAlbumId);
                _db.AddParameter(cmd, "TopPhoto", topPhoto);
                data = _db.GetList<EmbedAlbumDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public EmbedAlbumDetailEntity GetById(long embedAlbumId)
        {
            const string commandText = "CMS_EmbedAlbumDetail_GetById";
            try
            {
                EmbedAlbumDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbumId);
                data = _db.Get<EmbedAlbumDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(EmbedAlbumDetailEntity embedAlbum, ref int newEmbedAlbumDetailId)
        {
            const string commandText = "CMS_EmbedAlbumDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbum.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "EmbedAlbumId", embedAlbum.EmbedAlbumId);
                _db.AddParameter(cmd, "Title", embedAlbum.Title);
                _db.AddParameter(cmd, "Sapo", embedAlbum.Sapo);
                _db.AddParameter(cmd, "Note", embedAlbum.Note);
                _db.AddParameter(cmd, "ImageUrl", embedAlbum.ImageUrl);
                _db.AddParameter(cmd, "Priority", embedAlbum.Priority);
                _db.AddParameter(cmd, "IsAvatar", embedAlbum.IsAvatar);
                _db.AddParameter(cmd, "DisplayStyle", embedAlbum.DisplayStyle);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                newEmbedAlbumDetailId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Update(EmbedAlbumDetailEntity embedAlbum)
        {
            const string commandText = "CMS_EmbedAlbumDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", embedAlbum.Id);
                //_db.AddParameter(cmd, "EmbedAlbumId", embedAlbum.EmbedAlbumId);
                _db.AddParameter(cmd, "Title", embedAlbum.Title);
                _db.AddParameter(cmd, "Sapo", embedAlbum.Sapo);
                _db.AddParameter(cmd, "Note", embedAlbum.Note);
                _db.AddParameter(cmd, "ImageUrl", embedAlbum.ImageUrl);
                _db.AddParameter(cmd, "Priority", embedAlbum.Priority);
                _db.AddParameter(cmd, "IsAvatar", embedAlbum.IsAvatar);
                _db.AddParameter(cmd, "DisplayStyle", embedAlbum.DisplayStyle);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByEmbedAlbumIdExcudeListIds(int embedAlbumId, string excludeListIds)
        {
            const string commandText = "CMS_EmbedAlbumDetail_DeleteByEmbedAlbumIdExcudeListIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "EmbedAlbumId", embedAlbumId);
                _db.AddParameter(cmd, "ExcludeListIds", excludeListIds);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Core members

        private readonly ImsExternalDb _db;

        protected EmbedAlbumDetailDalBase(ImsExternalDb db)
        {
            _db = db;
        }

        protected ImsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
