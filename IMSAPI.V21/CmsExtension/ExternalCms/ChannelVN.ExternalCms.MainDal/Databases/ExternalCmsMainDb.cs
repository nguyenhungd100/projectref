﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.MainDal.Databases
{
    public class ExternalCmsMainDb : ExternalCmsMainDbBase
    {
        private const string ConnectionStringName = "ExternalWeatherDb";
        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
} 