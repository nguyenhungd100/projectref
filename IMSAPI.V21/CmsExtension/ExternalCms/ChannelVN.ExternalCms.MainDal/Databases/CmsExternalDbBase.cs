﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.ExternalCms.MainDal.Databases;
using ChannelVN.ExternalCms.MainDal;

namespace ChannelVN.ExternalCms.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsExternalDbBase : MainDbBase
    {
        #region Store procedures

        #region StyleImages
        private StyleImagesDal _StyleImagesContentMainDal;
        public StyleImagesDalBase StyleImagesContentMainDal
        {
            get { return _StyleImagesContentMainDal ?? (_StyleImagesContentMainDal = new StyleImagesDal((CmsExternalDb)this)); }
        }
        #endregion
        #region Share Style
        private ShareStylesDal _shareStyleDal;
        public ShareStylesDalBase ShareStyleDal
        {
            get { return _shareStyleDal ?? (_shareStyleDal = new ShareStylesDal((CmsExternalDb)this)); }
        }
        #endregion
        #region Quick News Images Extenernal
        private QuickNewsImagesExternalDal _quickNewsImagesExternalDal;
        public QuickNewsImagesExternalDalBase QuickNewsImagesExternalDal
        {
            get { return _quickNewsImagesExternalDal ?? (_quickNewsImagesExternalDal = new QuickNewsImagesExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #region QuickNews External
        private QuickNewsExternalDal _quickNewsExternalDal;
        public QuickNewsExternalDalBase QuickNewsExternalDal
        {
            get { return _quickNewsExternalDal ?? (_quickNewsExternalDal = new QuickNewsExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #region QuickNews Author
        private QuickNewsAuthorExternalDal _quickNewsAuthorExtDal;
        public QuickNewsAuthorExternalDalBase  QuickNewsAuthorExtDal
        {
            get { return _quickNewsAuthorExtDal ?? (_quickNewsAuthorExtDal = new QuickNewsAuthorExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #region Interview
        private  InterviewQuestionExternalDal _interviewExtDal;
        public InterviewQuestionExternalDalBase InterviewExtDal
        {
            get { return _interviewExtDal ?? (_interviewExtDal = new InterviewQuestionExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #region FeedBack
        private FeedBackNewsExternalDal _feedbackExtDal;
        public FeedBackNewsExtenalDalBase FeedBackExtDal
        {
            get { return _feedbackExtDal ?? (_feedbackExtDal = new FeedBackNewsExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #region externalVideo
        private ExternalVideoDal _externalVideoDal;
        public ExternalVideoDalBase externalVideoDal
        {
            get { return _externalVideoDal ?? (_externalVideoDal = new ExternalVideoDal((CmsExternalDb)this)); }
        }
        #endregion
        #region external Feedback
        private ExternalFeedBackNewsDal _externalFeebBackDal;
        public ExternalFeedBackNewsDalBase externalFeedBackDal
        {
            get { return _externalFeebBackDal ?? (_externalFeebBackDal = new ExternalFeedBackNewsDal((CmsExternalDb)this)); }
        }
        #endregion
        #region Comment rate
        private CommentRateDal _commentRateDal;
        public CommentRateDalBase CommentRateDal
        {
            get { return _commentRateDal ?? (_commentRateDal = new CommentRateDal((CmsExternalDb)this)); }
        }
        #endregion
        #region Comment
        private CommentDal _commentDal;
        public CommentDalBase CommentDal
        {
            get { return _commentDal ?? (_commentDal = new CommentDal((CmsExternalDb)this)); }
        }
        #endregion

        private GiftCodeDal _giftCodeDal;
        public GiftCodeDalBase GiftCodeDal
        {
            get { return _giftCodeDal ?? (_giftCodeDal = new GiftCodeDal((CmsExternalDb)this)); }
        }
        #region Vote
        private VoteDal _voteMainDal;
        public VoteDal VoteMainDal
        {
            get { return _voteMainDal ?? (_voteMainDal = new VoteDal((CmsExternalDb)this)); }
        }
        #endregion

        #region QuickAnswer External
        private QuickAnswerExternalDal _quickAnswerExternalDal;
        public QuickAnswerExternalDalBase QuickAnswerExternalDal
        {
            get { return _quickAnswerExternalDal ?? (_quickAnswerExternalDal = new QuickAnswerExternalDal((CmsExternalDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected CmsExternalDbBase()
        {
        }
        protected CmsExternalDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}