﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.ExternalCms.MainDal.Databases;
using ChannelVN.ExternalCms.MainDal;

namespace ChannelVN.ExternalCms.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ExternalCmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Weather
        private WeatherDal _weatherContentMainDal;
        public WeatherDalBase WeatherContentMainDal
        {
            get { return _weatherContentMainDal ?? (_weatherContentMainDal = new WeatherDal((ExternalCmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected ExternalCmsMainDbBase()
        {
        }
        protected ExternalCmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}