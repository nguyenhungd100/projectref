﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.ExternalCms.MainDal.Databases;
using ChannelVN.ExternalCms.MainDal;

namespace ChannelVN.ExternalCms.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ImsExternalDbBase : MainDbBase
    {
        #region Store procedures
        private ExternalNewsDal _externalNewsDal;
        public ExternalNewsDalBase ExternalNewsDal
        {
            get { return _externalNewsDal ?? (_externalNewsDal = new ExternalNewsDal((ImsExternalDb)this)); }
        }

        private VideoDal _VideoDal;
        public VideoDalBase VideoMainDal
        {
            get { return _VideoDal ?? (_VideoDal = new VideoDal((ImsExternalDb)this)); }
        }

        private VideoRoyaltyAuthorDal _VideoRoyaltyAuthorDal;
        public VideoRoyaltyAuthorDalBase VideoRoyaltyAuthorMainDal
        {
            get { return _VideoRoyaltyAuthorDal ?? (_VideoRoyaltyAuthorDal = new VideoRoyaltyAuthorDal((ImsExternalDb)this)); }
        }

        #region EmbedAlbumDetailDal
        private EmbedAlbumDetailDal _embedAlbumDetailMainDal;
        public EmbedAlbumDetailDal EmbedAlbumDetailMainDal
        {
            get { return _embedAlbumDetailMainDal ?? (_embedAlbumDetailMainDal = new EmbedAlbumDetailDal((ImsExternalDb)this)); }
        }
        #endregion

        #region EmbedAlbumDal
        private EmbedAlbumDal _embedAlbumMainDal;
        public EmbedAlbumDal EmbedAlbumMainDal
        {
            get { return _embedAlbumMainDal ?? (_embedAlbumMainDal = new EmbedAlbumDal((ImsExternalDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected ImsExternalDbBase()
        {
        }
        protected ImsExternalDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}