﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.ExternalCms.MainDal.Databases;
using ChannelVN.ExternalCms.MainDal;

namespace ChannelVN.ExternalCms.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {        
        #region QuickNews Images
        private QuickNewsImagesDal _quickNewsImagesDal;
        public QuickNewsImagesDalBase QuickNewsImagesDal
        {
            get { return _quickNewsImagesDal ?? (_quickNewsImagesDal = new QuickNewsImagesDal((CmsMainDb)this)); }
        }
        #endregion

        #region QuickNews
        private QuickNewsDal _quickNewsDal;
        public QuickNewsDalBase QuickNewsDal
        {
            get { return _quickNewsDal ?? (_quickNewsDal = new QuickNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region QuickNews Author
        private QuickNewsAuthorDal _quickNewsAuthorDal;
        public QuickNewsAuthorDalBase QuickNewsAuthorDal
        {
            get { return _quickNewsAuthorDal ?? (_quickNewsAuthorDal = new QuickNewsAuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #region FeedBack
        private FeedBackNewsDal _FeedBackDal;
        public FeedBackNewsDalBase FeedBackDal
        {
            get { return _FeedBackDal ?? (_FeedBackDal = new FeedBackNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region Comment Publish
        private CommentPublishedDal _commentPublishDal;
        public CommentPublishedDalBase CommentPublishDal
        {
            get { return _commentPublishDal ?? (_commentPublishDal = new CommentPublishedDal((CmsMainDb)this)); }
        }
        #endregion

        #region QuickAnswer
        private QuickAnswerDal _quickAnswerDal;
        public QuickAnswerDalBase QuickAnswerDal
        {
            get { return _quickAnswerDal ?? (_quickAnswerDal = new QuickAnswerDal((CmsMainDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}