﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class CommentRateDalBase
    {
        public List<CommentRateEntity> GetUnprocessComment() {
            const string commandText = "CMS_CommentRate_GetUnprocessComment";
            try
            {
                List<CommentRateEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<CommentRateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentRateEntity> GetUnprocessLikeComment(DateTime lastModifiedDate)
        {
            const string commandText = "CMS_CommentRate_GetUpdateComment";
            try
            {
                List<CommentRateEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LastModifiedDate", lastModifiedDate);
                data = _db.GetList<CommentRateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateProcessedComment(long commentId) {
            const string commandText = "CMS_CommentRate_UpdateProcessedComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CommentId", commentId);
                var data = cmd.ExecuteNonQuery(); ;
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho  CommentRateDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected CommentRateDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
