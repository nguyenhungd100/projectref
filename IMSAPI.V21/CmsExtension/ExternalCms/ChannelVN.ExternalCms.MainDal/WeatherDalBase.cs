﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class WeatherDalBase
    {
        #region function GET
        public List<ExternalWeatherEntity> GetAll()
        {
            const string commandText = "SELECT * FROM WEATHER";
            try
            {
                List<ExternalWeatherEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);
                data = _db.GetList<ExternalWeatherEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho WeatherDal
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly ExternalCmsMainDb _db;

        protected WeatherDalBase(ExternalCmsMainDb db)
        {
            _db = db;
        }

        protected ExternalCmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
