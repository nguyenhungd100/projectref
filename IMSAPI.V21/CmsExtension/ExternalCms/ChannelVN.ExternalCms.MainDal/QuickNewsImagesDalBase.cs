﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickNewsImagesDalBase
    {
        #region function SET
        public bool Insert(QuickNewsImagesEntity quickNewsImage)
        {
            const string commandText = "CMS_QuickNewsImages_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CreatedDate", quickNewsImage.CreatedDate);
                _db.AddParameter(cmd, "Description", quickNewsImage.Description);
                _db.AddParameter(cmd, "Image", quickNewsImage.Image);
                _db.AddParameter(cmd, "IsAvatar", quickNewsImage.IsAvatar);
                _db.AddParameter(cmd, "QuickNewsId", quickNewsImage.QuickNewsId);
                _db.AddParameter(cmd, "QuickNewsImagesId", quickNewsImage.QuickNewsImagesId);
                _db.AddParameter(cmd, "Status", quickNewsImage.Status);
                _db.AddParameter(cmd, "Type", quickNewsImage.Type);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho QuickNewsImages
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuickNewsImagesDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
