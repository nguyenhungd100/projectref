﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickNewsAuthorDalBase
    {
        public bool Insert(QuickNewsAuthorEntity quickNewsAuthor)
        {
            const string commandText = "CMS_QuickNewsAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VietId", quickNewsAuthor.VietId);
                _db.AddParameter(cmd, "UserName", quickNewsAuthor.UserName);
                _db.AddParameter(cmd, "FullName", quickNewsAuthor.FullName);
                _db.AddParameter(cmd, "Avatar", quickNewsAuthor.Avatar);
                _db.AddParameter(cmd, "Phone", quickNewsAuthor.Phone);
                _db.AddParameter(cmd, "Address", quickNewsAuthor.Address);
                _db.AddParameter(cmd, "Email", quickNewsAuthor.Email);
                _db.AddParameter(cmd, "Status", quickNewsAuthor.Status);
                _db.AddParameter(cmd, "Job", quickNewsAuthor.Job);
                _db.AddParameter(cmd, "Slogan", quickNewsAuthor.Slogan);
                _db.AddParameter(cmd, "PenName", quickNewsAuthor.PenName);
                _db.AddParameter(cmd, "CreatedDate", quickNewsAuthor.CreatedDate);
                _db.AddParameter(cmd, "LastLoginDate", quickNewsAuthor.LastLoginDate);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(int id)
        {
            const string commandText = "CMS_QuickNewsAuthor_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@Id", id);

                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho QuickNewsAuthor
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuickNewsAuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
