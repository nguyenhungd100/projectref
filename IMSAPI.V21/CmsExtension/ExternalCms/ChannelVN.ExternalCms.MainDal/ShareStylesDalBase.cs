﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class ShareStylesDalBase
    {
        #region function GET
        public ShareStylesEntity GetById(int styleId) {
            const string commandText = "CMS_ShareStyles_GetById";
            try
            {
                ShareStylesEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StyleId", styleId);
                data = _db.Get<ShareStylesEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ShareStylesEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow) {
            const string commandText = "CMS_ShareStyles_Search";
            try
            {
                List<ShareStylesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd,"TotalRow", totalRow,System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd,"Keyword", keyword);
                _db.AddParameter(cmd,"PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ShareStylesEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool DeleteById(int styleId) {
            const string commandText = "CMS_ShareStyles_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StyleId", styleId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Receive(int styleId, long newsId) {
            const string commandText = "CMS_ShareStyles_Receive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StyleId", styleId);
                _db.AddParameter(cmd, "NewsId", newsId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho ShareStyles
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected ShareStylesDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
