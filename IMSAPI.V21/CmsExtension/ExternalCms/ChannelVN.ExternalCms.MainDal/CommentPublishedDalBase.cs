﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class CommentPublishedDalBase
    {
        public bool UpdateLikeCountComment(string value)
        {

            const string commandText = "CMS_CommentPublished_UpdateLikeCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "value", value);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetPublishedCommentCountByNewsId(int objectType, long objectId)
        {
            const string commandText = "CMS_CommentPublished_GetPublishedCommentCountByNewsId";
            try
            {
                int data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ObjectId", objectId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CommentPublishedEntity> SearchCommentPublished(string objectTitle, int objectType, string title,
            long parentId, CommentPublishedStatus status, string receivedBy,
            long objectId, int pageIndex, int pageSize, DateTime publishDate, int TitleSearchMode, ref int totalRow)
        {
            const string commandText = "CMS_CommentPublished_Search";
            try
            {
                List<CommentPublishedEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectTitle", objectTitle);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "publishDate", publishDate);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ReceivedBy", receivedBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TitleSearchMode", TitleSearchMode);
                data = _db.GetList<CommentPublishedEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public CommentPublishedEntity GetCommentPublishedById(long id)
        {
            const string commandText = "CMS_CommentPublished_GetById";
            try
            {
                CommentPublishedEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<CommentPublishedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentPublishedEntity> GetPublishedDateNewsByNewsId(string NewsIds)
        {
            const string commandText = "CMS_CommentPublished_GetPublishedDateNewsByNewsId";
            try
            {
                List<CommentPublishedEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", NewsIds);
                data = _db.GetList<CommentPublishedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentPublishedEntity> GetListByListId(string commentIds)
        {
            const string commandText = "CMS_Comment_GetList_ByListId";
            try
            {
                List<CommentPublishedEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "commentIds", commentIds);
                data = _db.GetList<CommentPublishedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReceiveCommentPublished(CommentPublishedEntity comment)
        {
            const string commandText = "CMS_CommentPublished_Receive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", comment.Id);
                _db.AddParameter(cmd, "ObjectId", comment.ObjectId);
                _db.AddParameter(cmd, "ObjectTitle", comment.ObjectTitle);
                _db.AddParameter(cmd, "ObjectUrl", comment.ObjectUrl);
                _db.AddParameter(cmd, "ObjectType", comment.ObjectType);
                _db.AddParameter(cmd, "Title", comment.Title);
                _db.AddParameter(cmd, "Content", comment.Content);
                _db.AddParameter(cmd, "Emotion", comment.Emotion);
                _db.AddParameter(cmd, "SenderEmail", comment.SenderEmail);
                _db.AddParameter(cmd, "SenderFullName", comment.SenderFullName);
                _db.AddParameter(cmd, "SenderAvatar", comment.SenderAvatar);
                _db.AddParameter(cmd, "SenderAddress", comment.SenderAddress);
                _db.AddParameter(cmd, "SenderPhoneNumber", comment.SenderPhoneNumber);
                _db.AddParameter(cmd, "ReceivedBy", comment.ReceivedBy);
                _db.AddParameter(cmd, "ParentId", comment.ParentId);
                _db.AddParameter(cmd, "CreatedDate", comment.CreatedDate);
                _db.AddParameter(cmd, "ZoneId", comment.ZoneId);
                _db.AddParameter(cmd, "IpLog", comment.IPLog);
                _db.AddParameter(cmd, "UserId", comment.UserId);
                _db.AddParameter(cmd, "MailReplied", comment.MailReplied);
                _db.AddParameter(cmd, "MailForwarded", comment.MailForwarded);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool CommentPublishedAdminInsert(CommentPublishedEntity comment)
        {
            const string commandText = "CMS_CommentPublished_AdminInsert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", comment.Id);
                _db.AddParameter(cmd, "ObjectId", comment.ObjectId);
                _db.AddParameter(cmd, "ObjectTitle", comment.ObjectTitle);
                _db.AddParameter(cmd, "ObjectUrl", comment.ObjectUrl);
                _db.AddParameter(cmd, "ObjectType", comment.ObjectType);
                _db.AddParameter(cmd, "Title", comment.Title);
                _db.AddParameter(cmd, "Content", comment.Content);
                _db.AddParameter(cmd, "Emotion", comment.Emotion);
                _db.AddParameter(cmd, "SenderEmail", comment.SenderEmail);
                _db.AddParameter(cmd, "SenderFullName", comment.SenderFullName);
                _db.AddParameter(cmd, "SenderAvatar", comment.SenderAvatar);
                _db.AddParameter(cmd, "SenderAddress", comment.SenderAddress);
                _db.AddParameter(cmd, "SenderPhoneNumber", comment.SenderPhoneNumber);
                _db.AddParameter(cmd, "LastModifiedBy", comment.LastModifiedBy);
                _db.AddParameter(cmd, "ParentId", comment.ParentId);
                _db.AddParameter(cmd, "ZoneId", comment.ZoneId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool ApproveCommentPublished(long id, string approveBy)
        {
            const string commandText = "CMS_CommentPublished_Approve";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApproveBy", approveBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }

        public bool RemoveCommentPublished(long id)
        {
            const string commandText = "CMS_CommentPublished_Remove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool UnapproveCommentPublished(long id)
        {
            const string commandText = "CMS_CommentPublished_UnApprove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool UpdateCommentRate(long commentId, int liked, DateTime lastLikeDate)
        {
            const string commandText = "CMS_CommentPublished_UpdateCommentRate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CommentId", commentId);
                _db.AddParameter(cmd, "Liked", liked);
                _db.AddParameter(cmd, "LastLikeDate", lastLikeDate);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool UpdateCommentPublishedById(CommentPublishedEntity commentPublished)
        {
            const string commandText = "CMS_CommentPublished_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", commentPublished.Id);
                _db.AddParameter(cmd, "Title", commentPublished.Title);
                _db.AddParameter(cmd, "Content", commentPublished.Content);
                _db.AddParameter(cmd, "Emotion", commentPublished.Emotion);
                _db.AddParameter(cmd, "SenderEmail", commentPublished.SenderEmail);
                _db.AddParameter(cmd, "SenderFullName", commentPublished.SenderFullName);
                _db.AddParameter(cmd, "SenderAvatar", commentPublished.SenderAvatar);
                _db.AddParameter(cmd, "SenderAddress", commentPublished.SenderAddress);
                _db.AddParameter(cmd, "SenderPhoneNumber", commentPublished.SenderPhoneNumber);
                _db.AddParameter(cmd, "LastModifiedBy", commentPublished.LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool DeleteCommentPublishedById(long commentPublishedId, string deletedBy)
        {
            const string commandText = "CMS_CommentPublished_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", commentPublishedId);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public List<CommentPublishedEntity> StatisticsGetCommentPublished(int objectType, CommentPublishedStatus status, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_CommentPublished_StatisticsGetCommentPublished";
            try
            {
                List<CommentPublishedEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "DateFrom", fromDate);
                _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<CommentPublishedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }

        public bool DeleteListComments(string commentIds, string LastModifiedBy)
        {
            const string commandText = "CMS_CommentPublished_Delete_ListComments";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "commentIds", commentIds);
                _db.AddParameter(cmd, "LastModifiedBy", LastModifiedBy);
                _db.AddParameter(cmd, "DeletedBy", LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool UnApproveListCommentPublished(string commentIds, string LastModifiedBy)
        {
            const string commandText = "CMS_CommentPublished_UnApproveListCommentPublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "commentIds", commentIds);
                _db.AddParameter(cmd, "LastModifiedBy", LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool ApproveListCommentPublished(string commentIds, string PublishBy)
        {
            const string commandText = "CMS_CommentPublished_ApproveListCommentPublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CommentIds", commentIds);
                _db.AddParameter(cmd, "PublishedBy", PublishBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        public bool ReApproveListCommentPublished(string commentIds, string PublishedBy)
        {
            const string commandText = "CMS_CommentPublished_ReApproveListCommentPublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CommentIds", commentIds);
                _db.AddParameter(cmd, "PublishedBy", PublishedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
        }
        //CuongNP: Add- 2014-11-28
        public List<NewsHasNewCommentEntity> GetNewsHasNewComment(string objectTitle, int objectType, CommentPublishedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "NewsHasNewCommentEntity";
            try
            {
                List<NewsHasNewCommentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectTitle", objectTitle);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsHasNewCommentEntity>(cmd);
                //totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                totalRow = Utility.ConvertToInt("0");
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentPublishedEntity> SearchCommentPublishedV2(string objectTitle, int objectType, string content,
           long parentId, CommentPublishedStatus status, string receivedBy,
           long objectId,string zoneIds, int pageIndex, int pageSize, DateTime publishDate, ref int totalRow)
        {
            const string commandText = "CMS_CommentPublished_SearchV2";
            try
            {
                List<CommentPublishedEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectTitle", objectTitle);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Content", content);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "publishDate", publishDate);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "ReceivedBy", receivedBy);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<CommentPublishedEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CommentPublishedCountEntity> CommentPublishedCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            const string commandText = "CMS_CommentPublished_Count";
            try
            {
                List<CommentPublishedCountEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<CommentPublishedCountEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentPublishedStatisticsByAccount> CommentPublished_Statistics(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            const string commandText = "CMS_CommentPublished_Statistics";
            try
            {
                List<CommentPublishedStatisticsByAccount> data;
                var cmd = _db.CreateCommand(commandText, true);
                if(fromDate > new DateTime(1980,1,1))
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                if (toDate > new DateTime(1980, 1, 1))
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<CommentPublishedStatisticsByAccount>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentPublishedCountByZoneEntity> CommentPublishedCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            const string commandText = "CMS_CommentPublished_CountByZone";
            try
            {
                List<CommentPublishedCountByZoneEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<CommentPublishedCountByZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertCommentLogAction(CommentLogEntity commentLog)
        {

            const string commandText = "CommentLog_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ActionName", commentLog.ActionName);
                _db.AddParameter(cmd, "CreatedDate", commentLog.CreatedDate);
                _db.AddParameter(cmd, "Description", commentLog.Description);
                _db.AddParameter(cmd, "IP", commentLog.IP);
                _db.AddParameter(cmd, "ObjectId", commentLog.ObjectId);
                _db.AddParameter(cmd, "Status", commentLog.Status);
                _db.AddParameter(cmd, "UserDoAction", commentLog.UserDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMailForwarded(long id, string mailForwarded)
        {
            const string commandText = "CMS_CommentPublished_UpdateMailForwarded";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "MailForwarded", mailForwarded);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateMailReplied(long id, string mailReplied)
        {
            const string commandText = "CMS_CommentPublished_UpdateMailReplied";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "MailReplied", mailReplied);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        /// <summary>
        /// Chuyển đổi DAL cho  CommentPublishedDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected CommentPublishedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
