﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class VideoDalBase
    {
        #region Gets
        public VideoEntity GetById(int videoId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetById";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly ImsExternalDb _db;

        protected VideoDalBase(ImsExternalDb db)
        {
            _db = db;
        }

        protected ImsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
