﻿using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Common;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class ExternalNewsDalBase
    {
        public List<ExternalNewsEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ExternalNews_Search";
            try
            {
                List<ExternalNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ExternalNewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalNewsEntity> SearchNews(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ExternalNews_Ims_Search";
            try
            {
                List<ExternalNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ExternalNewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ExternalNewsEntity GetExternalNewsById(long id)
        {
            const string commandText = "CMS_ExternalNews_GetById";
            try
            {
                ExternalNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<ExternalNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ExternalNewsEntity GetExternalNewsByImsNewsId(long id)
        {
            const string commandText = "CMS_ExternalNews_GetByImsNewsId";
            try
            {
                ExternalNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<ExternalNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalNewsInZoneEntity> GetZoneByNewsId(long id)
        {
            const string commandText = "CMS_ExternalNewsInZone_GetByNewsId";
            try
            {
                List<ExternalNewsInZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<ExternalNewsInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalNewsAuthorEntity> GetAuthorByNewsId(long id)
        {
            const string commandText = "CMS_ExternalNewsAuthor_GetByNewsId";
            try
            {
                List<ExternalNewsAuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<ExternalNewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReceiveExternalNews(ExternalNewsEntity ExternalNews)
        {
            const string commandText = "CMS_ExternalNews_Receive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", ExternalNews.Id);
                _db.AddParameter(cmd, "ImsNewsId", ExternalNews.ImsNewsId);
                _db.AddParameter(cmd, "ImsNewsUrl", ExternalNews.ImsNewsUrl);
                _db.AddParameter(cmd, "ReceivedBy", ExternalNews.CreatedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            const string commandText = "CMS_ExternalNews_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", ExternalNews.Id);
                _db.AddParameter(cmd, "ImsNewsId", ExternalNews.ImsNewsId);
                _db.AddParameter(cmd, "ImsNewsUrl", ExternalNews.ImsNewsUrl);
                _db.AddParameter(cmd, "PublishedBy", ExternalNews.CreatedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReturnExternalNews(ExternalNewsEntity ExternalNews)
        {
            const string commandText = "CMS_ExternalNews_Return";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", ExternalNews.Id);
                _db.AddParameter(cmd, "ReturnBy", ExternalNews.CreatedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UnpublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            const string commandText = "CMS_ExternalNews_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", ExternalNews.Id);
                _db.AddParameter(cmd, "UnpublishedBy", ExternalNews.CreatedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateImsNewsInfo(ExternalNewsEntity ExternalNews)
        {
            const string commandText = "CMS_ExternalNews_UpdateImsNewsInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", ExternalNews.Id);
                _db.AddParameter(cmd, "ImsNewsId", ExternalNews.ImsNewsId);
                _db.AddParameter(cmd, "ImsNewsUrl", ExternalNews.ImsNewsUrl);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateImsNewsBody(ExternalNewsEntity externalNews)
        {
            const string commandText = "CMS_ExternalNews_Ims_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", externalNews.Id);
                _db.AddParameter(cmd, "Title", externalNews.Title);
                _db.AddParameter(cmd, "SubTitle", externalNews.SubTitle);
                _db.AddParameter(cmd, "Sapo", externalNews.Sapo);
                _db.AddParameter(cmd, "Body", externalNews.Body);
                _db.AddParameter(cmd, "Avatar", externalNews.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", externalNews.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", externalNews.Avatar2);
                _db.AddParameter(cmd, "Avatar3", externalNews.Avatar3);
                _db.AddParameter(cmd, "Avatar4", externalNews.Avatar4);
                _db.AddParameter(cmd, "Avatar5", externalNews.Avatar5);
                _db.AddParameter(cmd, "Author", externalNews.Author);
                _db.AddParameter(cmd, "Type", externalNews.Type);
                _db.AddParameter(cmd, "Note", externalNews.Note);
                _db.AddParameter(cmd, "Tags", externalNews.Tags);
                _db.AddParameter(cmd, "NewsCategory", externalNews.NewsCategory);
                _db.AddParameter(cmd, "ImageCategory", externalNews.ImageCategory);
                _db.AddParameter(cmd, "ImsNewsId", externalNews.ImsNewsId);
                _db.AddParameter(cmd, "ImsNewsUrl", externalNews.ImsNewsUrl);
                _db.AddParameter(cmd, "EditedBy", externalNews.EditedBy);
                _db.AddParameter(cmd, "ZoneId", externalNews.ZoneId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #region Constructor

        private readonly ImsExternalDb _db;

        protected ExternalNewsDalBase(ImsExternalDb db)
        {
            _db = db;
        }

        protected ImsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
