﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public class FeedBackNewsExtenalDalBase
    {
        public bool ExtDBInsert(FeedBackNewsEntity feedBackNews)
        {
            const string commandText = "CMS_ExternalFeedBackNews_Insert_FromCMS";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", feedBackNews.Id);
                _db.AddParameter(cmd, "ParentId", feedBackNews.ParentId);
                _db.AddParameter(cmd, "ZoneId", feedBackNews.ZoneId);
                _db.AddParameter(cmd, "Title", feedBackNews.Title);
                _db.AddParameter(cmd, "Subtitle", feedBackNews.Subtitle);
                _db.AddParameter(cmd, "Avatar", feedBackNews.Avatar);
                _db.AddParameter(cmd, "AvatarDescription", feedBackNews.AvatarDescription);
                _db.AddParameter(cmd, "InitContent", feedBackNews.InitContent);
                _db.AddParameter(cmd, "Content", feedBackNews.Content);
                _db.AddParameter(cmd, "Source", feedBackNews.Source);
                _db.AddParameter(cmd, "Author", feedBackNews.Author);
                _db.AddParameter(cmd, "IsFocus", feedBackNews.IsFocus);
                _db.AddParameter(cmd, "WordCount", feedBackNews.WordCount);
                _db.AddParameter(cmd, "Note", feedBackNews.Note);
                _db.AddParameter(cmd, "MetaTitle", feedBackNews.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", feedBackNews.MetaDescription);
                _db.AddParameter(cmd, "FullName", feedBackNews.FullName);
                _db.AddParameter(cmd, "Email", feedBackNews.Email);
                _db.AddParameter(cmd, "Phone", feedBackNews.Phone);
                _db.AddParameter(cmd, "Address", feedBackNews.Address);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<ExFeedBackNewsEntity> SearchNews(string keyword, int status,int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FeedBackNews_News_Search";
            try
            {
                List<ExFeedBackNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<ExFeedBackNewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ExFeedBackNewsEntity GetById(long id)
        {
            const string commandText = "CMS_FeedBackNews_GetById";
            try
            {
                ExFeedBackNewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ExFeedBackNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatusIms(long newsId, int status, long feedbackId)
        {
            const string commandText = "CMS_FeedBackNews_UpdateStatusIms";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ImsNewsId", newsId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Id", feedbackId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho FeedbackExternal
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected FeedBackNewsExtenalDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
