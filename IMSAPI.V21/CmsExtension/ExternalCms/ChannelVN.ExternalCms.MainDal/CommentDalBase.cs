﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class CommentDalBase
    {
        public List<CommentEntity> Search(string objectTitle, int objectType, string title, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {
            const string commandText = "CMS_Comment_Search";
            try
            {
                List<CommentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ObjectTitle", objectTitle);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);

                data = _db.GetList<CommentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public CommentEntity GetCommentById(long id)
        {
            const string commandText = "CMS_Comment_GetById";
            try
            {
                CommentEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<CommentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetWaitApproveCommentCountByObjectId(int objectType, long objectId)
        {
            const string commandText = "CMS_Comment_GetWaitApproveCommentCountByNewsId";
            try
            {
                int data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ObjectId", objectId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> StatisticsGetComment(int objectType, CommentStatus status, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Comment_CommentStatisticByStatus";
            try
            {
                List<CommentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "DateFrom", fromDate);
                _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<CommentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateCommentStatusById(long id, string approveBy)
        {
            const string commandText = "CMS_Comment_Approve";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApproveBy", approveBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool DeleteCommentById(long id, string deletedBy)
        {
            const string commandText = "CMS_Comment_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "DeletedBy", deletedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateCommentById(CommentEntity comment)
        {
            const string commandText = "CMS_Comment_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", comment.Id);
                _db.AddParameter(cmd, "Title", comment.Title);
                _db.AddParameter(cmd, "Content", comment.Content);
                _db.AddParameter(cmd, "SenderEmail", comment.SenderEmail);
                _db.AddParameter(cmd, "SenderFullName", comment.SenderFullName);
                _db.AddParameter(cmd, "LastModifiedBy", comment.LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemoveCommentById(long id, string LastModifiedBy)
        {
            const string commandText = "CMS_Comment_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", LastModifiedBy);
                _db.AddParameter(cmd, "DeletedBy", LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteListComments(string commentIds, string LastModifiedBy)
        {
            const string commandText = "CMS_Comment_Delete_ListComments";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "commentIds", commentIds);
                _db.AddParameter(cmd, "LastModifiedBy", LastModifiedBy);
                _db.AddParameter(cmd, "DeletedBy", LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> SearchObjectByUser(long userId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Comment_GetObjectByUser";
            try
            {
                List<CommentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<CommentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentEntity> SearchCommentByUserObject(long userId, int status, long objectId)
        {
            const string commandText = "CMS_Comment_GetByObjectId";
            try
            {
                List<CommentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "ObjectId", objectId);
                _db.AddParameter(cmd, "Status", status);
                //_db.AddParameter(cmd, "PageIndex", pageIndex);
                //_db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<CommentEntity>(cmd);
                //totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CommentCountEntity> CommentCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            const string commandText = "CMS_Comment_Count";
            try
            {
                List<CommentCountEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<CommentCountEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<CommentCountByZoneEntity> CommentCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            const string commandText = "CMS_Comment_CountByZone";
            try
            {
                List<CommentCountByZoneEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<CommentCountByZoneEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMailForwarded(long id, string mailForwarded)
        {
            const string commandText = "CMS_Comment_UpdateMailForwarded";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "MailForwarded", mailForwarded);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateMailReplied(long id, string mailReplied)
        {
            const string commandText = "CMS_Comment_UpdateMailReplied";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "MailReplied", mailReplied);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        /// <summary>
        /// Chuyển đổi DAL cho  CommentDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected CommentDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
