﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class VoteDalBase
    {
        #region Gets
      
        public List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            const string commandText = "CMS_VoteAnswers_SelectListByVoteId";
            try
            {
                List<VoteAnswersEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VoteId", voteId);
                data = _db.GetList<VoteAnswersEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsExternalDb _db;

        protected VoteDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
