﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class CrawlTopNewsAgentDalBase
    {
        public List<CrawlTopNewsAgentEntity> GetAllAgent() {
            const string commandText = "SELECT news_agent_id, news_angent_link, news_angent_name, news_angent_status, Source FROM news_channel_agent WHERE (news_angent_status = 2)";
            try
            {
                List<CrawlTopNewsAgentEntity> data;
                var cmd = _db.CreateCommand(commandText, false);
                data = _db.GetList<CrawlTopNewsAgentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho  CrawlTopNewsAgent
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsCrawlerDb _db;

        protected CrawlTopNewsAgentDalBase(CmsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
