﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class StyleImagesDalBase
    {
        #region function GET
        public List<StyleImagesEntity> GetByStyleId(int styleId, int top) {
            const string commandText = "CMS_StyleImages_GetByStyleId";
            try
            {
                List<StyleImagesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd,"StyleId", styleId);
                _db.AddParameter(cmd,"Top", top);
                data = _db.GetList<StyleImagesEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho StyleImages
        /// minhduongvan
        /// 2014/01/09
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected StyleImagesDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
