﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class InterviewQuestionExternalDalBase
    {
        public InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_GetExternalQuestionById";
            try
            {
                InterviewQuestionEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                data = _db.Get<InterviewQuestionEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewQuestionEntity> SearchInterviewQuestion(int interviewId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_InterviewQuestion_SearchExternalQuestion";
            try
            {
                List<InterviewQuestionEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<InterviewQuestionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewQuestionEntity> GetNotReceiveQuestion()
        {
            const string commandText = "CMS_InterviewQuestion_GetNotReceiveQuestion";
            try
            {
                List<InterviewQuestionEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<InterviewQuestionEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReceiveListQuestion(string listQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_ReceiveListQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListQuestionId", listQuestionId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReceiveExternaQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_ReceiveExternalQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteExternaQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_DeleteExternalQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            const string commandText = "CMS_InterviewQuestion_UpdateExternalQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestion.Id);
                _db.AddParameter(cmd, "Content", interviewQuestion.Content);
                _db.AddParameter(cmd, "FullName", interviewQuestion.FullName);
                _db.AddParameter(cmd, "Sex", interviewQuestion.Sex);
                _db.AddParameter(cmd, "Age", interviewQuestion.Age);
                _db.AddParameter(cmd, "Email", interviewQuestion.Email);
                _db.AddParameter(cmd, "Job", interviewQuestion.Job);
                _db.AddParameter(cmd, "Address", interviewQuestion.Address);
                _db.AddParameter(cmd, "Mobile", interviewQuestion.Mobile);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            const string commandText = "CMS_InterviewQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewQuestion.InterviewId);
                _db.AddParameter(cmd, "Content", interviewQuestion.Content);
                _db.AddParameter(cmd, "FullName", interviewQuestion.FullName);
                _db.AddParameter(cmd, "Sex", interviewQuestion.Sex);
                _db.AddParameter(cmd, "Age", interviewQuestion.Age);
                _db.AddParameter(cmd, "Email", interviewQuestion.Email);
                _db.AddParameter(cmd, "Job", interviewQuestion.Job);
                _db.AddParameter(cmd, "Address", interviewQuestion.Address);
                _db.AddParameter(cmd, "Mobile", interviewQuestion.Mobile);
                _db.AddParameter(cmd, "CreatedDate", interviewQuestion.CreatedDate);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetInterviewQuestionCount(int interviewId)
        {
            const string commandText = "CMS_InterviewQuestion_GetCountExternalQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                var ret = cmd.ExecuteReader();
                if (ret.Read())
                    return Utility.ConvertToInt(ret[0]);
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho Interview
        /// CuongNP Edit
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected InterviewQuestionExternalDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
