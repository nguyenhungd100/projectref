﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickNewsAuthorExternalDalBase
    {
        public QuickNewsAuthorEntity ExtDBGetById(long vietId)
        {
            const string commandText = "CMS_QuickNewsAuthor_GetById";
            try
            {
                QuickNewsAuthorEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", vietId);
                data = _db.Get<QuickNewsAuthorEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public QuickNewsAuthorEntity ExtDBGetByEmail(string email)
        {
            const string commandText = "CMS_QuickNewsAuthor_GetByEmail";
            try
            {
                QuickNewsAuthorEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Email", email);
                data = _db.Get<QuickNewsAuthorEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuickNewsAuthorEntity> ExtDBSearch(string keyword, string email, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_QuickNewsAuthor_Search";
            try
            {
                List<QuickNewsAuthorEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Email", email);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuickNewsAuthorEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho QuickNewsAuthor
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected QuickNewsAuthorExternalDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
