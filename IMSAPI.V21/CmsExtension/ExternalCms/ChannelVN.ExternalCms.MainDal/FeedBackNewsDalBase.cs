﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public class FeedBackNewsDalBase
    {
        #region function GET
        public FeedBackNewsEntity GetById(long id)
        {
            const string commandText = "CMS_FeedBackNews_GetById";
            try
            {
                FeedBackNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@Id", id);
                data = _db.Get<FeedBackNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public FeedBackNewsEntity GetByParentId(long parentId)
        {
            const string commandText = "CMS_FeedBackNews_GetByParentIdId";
            try
            {
                FeedBackNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@ParentId", parentId);
                data = _db.Get<FeedBackNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FeedBackNewsWithSimpleFieldEntity> Search(string keyword, long parentId, int status, int filterDateField, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FeedBackNews_Search";
            try
            {
                List<FeedBackNewsWithSimpleFieldEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "@Keyword", keyword);
                _db.AddParameter(cmd, "@ParentId", parentId);
                _db.AddParameter(cmd, "@Status", status);
                _db.AddParameter(cmd, "@FilterDateField", filterDateField);
                _db.AddParameter(cmd, "@DateFrom", dateFrom);
                _db.AddParameter(cmd, "@DateTo", dateTo);
                _db.AddParameter(cmd, "@PageIndex", pageIndex);
                _db.AddParameter(cmd, "@PageSize", pageSize);
                data = _db.GetList<FeedBackNewsWithSimpleFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNewsHasExternalNews(string listParentId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FeedBackNews_SearchFeedBackNewsHasExternalNews";
            try
            {
                List<FeedBackNewsWithSimpleFieldEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "@ListParentId", listParentId);
                _db.AddParameter(cmd, "@PageIndex", pageIndex);
                _db.AddParameter(cmd, "@PageSize", pageSize);
                data = _db.GetList<FeedBackNewsWithSimpleFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region function SET
        public bool DeleteById(long id)
        {
            const string commandText = "CMS_FeedBackNews_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "@Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(FeedBackNewsEntity feedBackNews)
        {
            const string commandText = "CMS_FeedBackNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", feedBackNews.Id);//for kenh14
                _db.AddParameter(cmd, "ParentId", feedBackNews.ParentId);
                _db.AddParameter(cmd, "ZoneId", feedBackNews.ZoneId);
                _db.AddParameter(cmd, "Title", feedBackNews.Title);
                _db.AddParameter(cmd, "Subtitle", feedBackNews.Subtitle);
                _db.AddParameter(cmd, "Avatar", feedBackNews.Avatar);
                _db.AddParameter(cmd, "AvatarDescription", feedBackNews.AvatarDescription);
                _db.AddParameter(cmd, "InitContent", feedBackNews.InitContent);
                _db.AddParameter(cmd, "Content", feedBackNews.Content);
                _db.AddParameter(cmd, "Source", feedBackNews.Source);
                _db.AddParameter(cmd, "Author", feedBackNews.Author);
                _db.AddParameter(cmd, "IsFocus", feedBackNews.IsFocus);
                _db.AddParameter(cmd, "WordCount", feedBackNews.WordCount);
                _db.AddParameter(cmd, "Note", feedBackNews.Note);
                _db.AddParameter(cmd, "MetaTitle", feedBackNews.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", feedBackNews.MetaDescription);
                _db.AddParameter(cmd, "Url", feedBackNews.Url);
                _db.AddParameter(cmd, "FullName", feedBackNews.FullName);
                _db.AddParameter(cmd, "Email", feedBackNews.Email);
                _db.AddParameter(cmd, "Phone", feedBackNews.Phone);
                _db.AddParameter(cmd, "Address", feedBackNews.Address);
                _db.AddParameter(cmd, "ReceivedBy", feedBackNews.ReceivedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(FeedBackNewsEntity feedBackNews)
        {
            const string commandText = "CMS_FeedBackNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", feedBackNews.Id);
                _db.AddParameter(cmd, "ZoneId", feedBackNews.ZoneId);
                _db.AddParameter(cmd, "Title", feedBackNews.Title);
                _db.AddParameter(cmd, "Subtitle", feedBackNews.Subtitle);
                _db.AddParameter(cmd, "Avatar", feedBackNews.Avatar);
                _db.AddParameter(cmd, "AvatarDescription", feedBackNews.AvatarDescription);
                _db.AddParameter(cmd, "InitContent", feedBackNews.InitContent);
                _db.AddParameter(cmd, "Content", feedBackNews.Content);
                _db.AddParameter(cmd, "Source", feedBackNews.Source);
                _db.AddParameter(cmd, "Author", feedBackNews.Author);
                _db.AddParameter(cmd, "IsFocus", feedBackNews.IsFocus);
                _db.AddParameter(cmd, "WordCount", feedBackNews.WordCount);
                _db.AddParameter(cmd, "Note", feedBackNews.Note);
                _db.AddParameter(cmd, "MetaTitle", feedBackNews.MetaTitle);
                _db.AddParameter(cmd, "MetaDescription", feedBackNews.MetaDescription);
                _db.AddParameter(cmd, "Url", feedBackNews.Url);
                _db.AddParameter(cmd, "FullName", feedBackNews.FullName);
                _db.AddParameter(cmd, "Email", feedBackNews.Email);
                _db.AddParameter(cmd, "Phone", feedBackNews.Phone);
                _db.AddParameter(cmd, "Address", feedBackNews.Address);
                _db.AddParameter(cmd, "LastModifiedBy", feedBackNews.LastModifiedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Publish(long id, string publishedBy)
        {
            const string commandText = "CMS_FeedBackNews_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Unpublish(long id) {
            const string commandText = "CMS_FeedBackNews_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Feedback
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected FeedBackNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
