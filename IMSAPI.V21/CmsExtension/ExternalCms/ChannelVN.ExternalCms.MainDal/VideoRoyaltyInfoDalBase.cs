﻿using System;
using System.Collections.Generic;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class VideoRoyaltyAuthorDalBase
    {
        #region function GET
        public  List<VideoRoyaltyAuthorEntity> GetByVideoId(long id)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_GetByVideoId";
            try
            {
                List<VideoRoyaltyAuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", id);
                data = _db.GetList<VideoRoyaltyAuthorEntity>(cmd) ?? new List<VideoRoyaltyAuthorEntity>();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        
        #region Constructor

        private readonly ImsExternalDb _db;

        protected VideoRoyaltyAuthorDalBase(ImsExternalDb db)
        {
            _db = db;
        }

        protected ImsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
