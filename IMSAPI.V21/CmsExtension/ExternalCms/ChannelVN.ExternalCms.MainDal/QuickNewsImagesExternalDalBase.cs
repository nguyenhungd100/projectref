﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickNewsImagesExternalDalBase
    {
        #region function GET
        public List<QuickNewsImagesEntity> ExtDBGetByQuickNewsId(long quickNewsId)
        {
            const string commandText = "CMS_QuickNewsImages_GetByQuickNewsId";
            try
            {
                List<QuickNewsImagesEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuickNewsId", quickNewsId);
                data = _db.GetList<QuickNewsImagesEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho QuickNewsImages
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected QuickNewsImagesExternalDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
