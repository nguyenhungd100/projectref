﻿using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class ExternalFeedBackNewsDalBase
    {
        public ExternalFeedBackNewsEntity GetById(long id)
        {
            const string commandText = "CMS_ExternalFeedBackNews_GetById";
            try
            {
                ExternalFeedBackNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ExternalFeedBackNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalFeedBackNewsCountByParentEntity> GetAllParentIdHasExternalNews()
        {
            const string commandText = "CMS_ExternalFeedBackNews_GetAllParentIdHasExternalNews";
            try
            {
                List<ExternalFeedBackNewsCountByParentEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<ExternalFeedBackNewsCountByParentEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalFeedBackNewsWithSimpleFieldEntity> Search(string keyword, long parentId, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ExternalFeedBackNews_Search";
            try
            {
                List<ExternalFeedBackNewsWithSimpleFieldEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "DateFrom", dateFrom);
                _db.AddParameter(cmd, "DateTo", dateTo);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ExternalFeedBackNewsWithSimpleFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long id) {
            const string commandText = "CMS_ExternalFeedBackNews_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Receive(long id) {
            const string commandText = "CMS_ExternalFeedBackNews_Receive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho  ExternalFeedBackNews
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsExternalDb _db;

        protected ExternalFeedBackNewsDalBase(CmsExternalDb db)
        {
            _db = db;
        }

        protected CmsExternalDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
