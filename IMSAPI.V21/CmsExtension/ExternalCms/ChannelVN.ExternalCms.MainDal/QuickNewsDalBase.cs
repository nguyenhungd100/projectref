﻿using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.ExternalCms.MainDal
{
    public abstract class QuickNewsDalBase
    {
        #region function SET
        public bool Insert(QuickNewsEntity quickNews)
        {
            const string commandText = "CMS_QuickNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuickNewsId", quickNews.QuickNewsId);
                _db.AddParameter(cmd, "CatId", quickNews.CatId);
                _db.AddParameter(cmd, "VietId", quickNews.VietId);
                _db.AddParameter(cmd, "News_ID", quickNews.News_ID);
                _db.AddParameter(cmd, "Title", quickNews.Title);
                _db.AddParameter(cmd, "Sapo", quickNews.Sapo);
                _db.AddParameter(cmd, "Content", quickNews.Content);
                _db.AddParameter(cmd, "Note", quickNews.Note);
                _db.AddParameter(cmd, "Link", quickNews.Link);
                _db.AddParameter(cmd, "FullName", quickNews.FullName);
                _db.AddParameter(cmd, "Phone", quickNews.Phone);
                _db.AddParameter(cmd, "Email", quickNews.Email);
                _db.AddParameter(cmd, "Type", quickNews.Type);
                _db.AddParameter(cmd, "Status", quickNews.Status);
                _db.AddParameter(cmd, "CreatedDate", quickNews.CreatedDate);
                _db.AddParameter(cmd, "LastModifiedDate", quickNews.LastModifiedDate);
                _db.AddParameter(cmd, "PenName", quickNews.PenName);
                var result = _db.ExecuteNonQuery(cmd);
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public QuickNewsEntity GetById(long id)
        {
            const string commandText = "CMS_QuickNews_GetById";
            try
            {
                QuickNewsEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<QuickNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho QuickNews
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected QuickNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
