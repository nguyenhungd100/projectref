﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CrawlTopNewsAgentEntity : EntityBase
    {
        [DataMember]
        public int news_agent_id { get; set; }
        [DataMember]
        public string news_angent_link { get; set; }
        [DataMember]
        public string news_angent_name { get; set; }
        [DataMember]
        public int news_angent_status { get; set; }
        [DataMember]
        public int Source { get; set; }
    }
}
