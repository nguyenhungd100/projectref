﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class VoteAnswersEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int VoteId { get; set; }

        [DataMember]
        public int VoteItemId { get; set; }

        [DataMember]
        public double VoteRate { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Priority { get; set; }
    }
}
