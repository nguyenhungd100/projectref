﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class ShareStylesEntity : EntityBase
    {
        [DataMember]
        public int StyleId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string YoutubeVideo { get; set; }
        [DataMember]
        public string Facebook { get; set; }
        [DataMember]
        public string Blog { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long VietId { get; set; }
    }
    [DataContract]
    public class ShareStylesDetailEntity : EntityBase
    {
        [DataMember]
        public ShareStylesEntity ShareStyles { get; set; }
        [DataMember]
        public List<StyleImagesEntity> TopStyleImages { get; set; }
    }
}
