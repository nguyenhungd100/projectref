﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    public class ExternalVideoEntity : EntityBase
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortContent { get; set; }
        public string LinkVideo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime DateAdded { get; set; }
        public int ZoneVideo { get; set; }
        public int Status { get; set; }
    }
}
