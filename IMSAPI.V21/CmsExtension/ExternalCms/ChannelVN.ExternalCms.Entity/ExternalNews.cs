﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class ExternalNewsEntity
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string SubTitle { set; get; }
        [DataMember]
        public string Sapo { set; get; }
        [DataMember]
        public string Body { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public string AvatarDesc { set; get; }
        [DataMember]
        public string Avatar2 { set; get; }
        [DataMember]
        public string Avatar3 { set; get; }
        [DataMember]
        public string Avatar4 { set; get; }
        [DataMember]
        public string Avatar5 { set; get; }
        [DataMember]
        public string Author { set; get; }
        [DataMember]
        public string Source { set; get; }
        [DataMember]
        public string SourceUrl { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public int Type { set; get; }
        [DataMember]
        public DateTime CreatedDate { set; get; }
        [DataMember]
        public string CreatedBy { set; get; }
        [DataMember]
        public DateTime EditedDate { set; get; }
        [DataMember]
        public string EditedBy { set; get; }
        [DataMember]
        public DateTime PublishedDate { set; get; }
        [DataMember]
        public string PublishedBy { set; get; }
        [DataMember]
        public DateTime LastModifiedDate { set; get; }
        [DataMember]
        public DateTime DistributionDate { set; get; }
        [DataMember]
        public string Note { set; get; }
        [DataMember]
        public string Tags { set; get; }
        [DataMember]
        public int NewsCategory { set; get; }
        [DataMember]
        public int ImageCategory { set; get; }
        [DataMember]
        public long ImsNewsId { set; get; }
        [DataMember]
        public string ImsNewsUrl { set; get; }
        [DataMember]
        public int ZoneId { set; get; }
    }

    [DataContract]
    public class ExternalNewsInZoneEntity
    {
        [DataMember]
        public long ExternalNewsId { set; get; }
        [DataMember]
        public int ZoneId { set; get; }
        [DataMember]
        public int IsPrimary { set; get; }
    }

    [DataContract]
    public class ExternalNewsAuthorEntity
    {
        [DataMember]
        public long ExternalNewsId { set; get; }
        [DataMember]
        public int AuthorId { set; get; }
        [DataMember]
        public string AuthorName { set; get; }
        [DataMember]
        public int Rate { set; get; }
        [DataMember]
        public string Note { set; get; }
        [DataMember]
        public int AuthorType { set; get; }
    }

    [DataContract]
    public enum ExternalNewsStatus
    {
        /// <summary>
        /// Không xác định
        /// </summary>
        [EnumMember]
        Unknow = -1,
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = 0,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 1,
        /// <summary>
        /// Đợi biên tập
        /// </summary>
        [EnumMember]
        WaitForEdit = 2,
        /// <summary>
        /// Nhận biên tập
        /// </summary>
        [EnumMember]
        ReceivedForEdit = 3,
        /// <summary>
        /// Trả lại phóng viên
        /// </summary>
        [EnumMember]
        ReturnedToReporter = 4,
        /// <summary>
        /// Đợi xuất bản
        /// </summary>
        [EnumMember]
        WaitForPublish = 5,
        /// <summary>
        /// Nhận xuất bản
        /// </summary>
        [EnumMember]
        ReceivedForPublish = 6,
        /// <summary>
        /// Trả lại biên tập viên
        /// </summary>
        [EnumMember]
        ReturnedToEditor = 7,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Published = 8,
        /// <summary>
        /// Xóa tạm
        /// </summary>
        [EnumMember]
        MovedToTrash = 9,
        /// <summary>
        /// Gỡ xuất bản
        /// </summary>
        [EnumMember]
        Unpublished = 10
    }
}
