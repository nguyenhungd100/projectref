﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CommentEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectTitle { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Emotion { get; set; }
        [DataMember]
        public string SenderEmail { get; set; }
        [DataMember]
        public string SenderFullName { get; set; }
        [DataMember]
        public string SenderAvatar { get; set; }
        [DataMember]
        public string SenderAddress { get; set; }
        [DataMember]
        public string SenderPhoneNumber { get; set; }
        [DataMember]
        public string ApproveBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }

        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public string IPLog { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string MailReplied { get; set; }
        [DataMember]
        public string MailForwarded { get; set; }
    }
    [DataContract]
    public class CommentCountEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
    [DataContract]
    public class CommentCountByZoneEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Count { get; set; }
    }

    [DataContract]
    public class CommentCounterEntity : EntityBase
    {
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
    }

    [DataContract]
    public enum EnumCommentObjectType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        ForNews = 1,
        [EnumMember]
        ForVideo = 2,
        [EnumMember]
        ForVote = 3,
        [EnumMember]
        ForLiveMatch = 4
    }
    [DataContract]
    public enum CommentStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Approved = 2,
        [EnumMember]
        UnApproved = 1,
        [EnumMember]
        Deleted = 3
    }

    //CuongNP: Add- 2014/12/01
    [DataContract]
    public class NewsHasNewCommentExtEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectTitle { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Emotion { get; set; }
        [DataMember]
        public string SenderEmail { get; set; }
        [DataMember]
        public string SenderFullName { get; set; }
        [DataMember]
        public string SenderAvatar { get; set; }
        [DataMember]
        public string SenderAddress { get; set; }
        [DataMember]
        public string SenderPhoneNumber { get; set; }
        [DataMember]
        public string ApproveBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
    }

}
