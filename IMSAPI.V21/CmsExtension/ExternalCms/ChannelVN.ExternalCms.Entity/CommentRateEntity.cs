﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CommentRateEntity : EntityBase
    {
        [DataMember]
        public long CommentId { get; set; }
        [DataMember]
        public int Liked { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
    }    
}
