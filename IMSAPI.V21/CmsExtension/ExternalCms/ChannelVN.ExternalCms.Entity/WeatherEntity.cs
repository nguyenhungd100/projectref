﻿
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class ExternalWeatherEntity : EntityBase
    {
        [DataMember]
        public int CodeId { get; set; }
        [DataMember]
        public int Temperature { get; set; }
        [DataMember]
        public string WeatherDate { get; set; }
        [DataMember]
        public string LocationName { get; set; }
        [DataMember]
        public string Humidity { get; set; }
        [DataMember]
        public string Wind { get; set; }
        [DataMember]
        public string Sunset { get; set; }
        [DataMember]
        public string Sunrise { get; set; }
        [DataMember]
        public string Temp { get; set; }

       
        [DataMember]
        public string Day1 { get; set; }
        [DataMember]
        public string Temp1 { get; set; }
        [DataMember]
        public string Hight1 { get; set; }
        [DataMember]
        public string Low1 { get; set; }       
        [DataMember]
        public string Day2 { get; set; }
        [DataMember]
        public string Temp2 { get; set; }
        [DataMember]
        public string Hight2 { get; set; }
        [DataMember]
        public string Low2 { get; set; }
        [DataMember]
        public string Day3 { get; set; }
        [DataMember]
        public string Temp3 { get; set; }
        [DataMember]
        public string Hight3 { get; set; }
        [DataMember]
        public string Low3 { get; set; }
        [DataMember]
        public string Day4 { get; set; }
        [DataMember]
        public string Temp4 { get; set; }
        [DataMember]
        public string Hight4 { get; set; }
        [DataMember]
        public string Low4 { get; set; }
        [DataMember]
        public string Day5 { get; set; }
        [DataMember]
        public string Temp5 { get; set; }
        [DataMember]
        public string Hight5 { get; set; }
        [DataMember]
        public string Low5 { get; set; }
        [DataMember]      
        public string Day6 { get; set; }
        [DataMember]
        public string Temp6 { get; set; }
        [DataMember]
        public string Hight6 { get; set; }
        [DataMember]
        public string Low6 { get; set; }
        [DataMember]
        public string Day7 { get; set; }
        [DataMember]
        public string Temp7 { get; set; }
        [DataMember]
        public string Hight7 { get; set; }
        [DataMember]
        public string Low7 { get; set; }
        [DataMember]
        public string Day8 { get; set; }
        [DataMember]
        public string Temp8 { get; set; }
        [DataMember]
        public string Hight8 { get; set; }
        [DataMember]
        public string Low8 { get; set; }
        [DataMember]
        public string Day9 { get; set; }
        [DataMember]
        public string Temp9 { get; set; }
        [DataMember]
        public string Hight9 { get; set; }
        [DataMember]
        public string Low9 { get; set; }
        [DataMember]
        public string Day10 { get; set; }
        [DataMember]
        public string Temp10 { get; set; }
        [DataMember]
        public string Hight10 { get; set; }
        [DataMember]
        public string Low10 { get; set; }
    }
}
