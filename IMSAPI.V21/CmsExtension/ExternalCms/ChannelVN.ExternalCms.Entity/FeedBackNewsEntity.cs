﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public enum EnumFeedBackNewsFilterDateField
    {
        [EnumMember]
        ReceivedDate = 0,
        [EnumMember]
        PublishedDate = 1,
        [EnumMember]
        LastModified = 2
    }
    [DataContract]
    public enum EnumFeedBackNewsStatus
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Temporary = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        Unpublished = 3
    }

    [DataContract]
    public class FeedBackNewsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Subtitle { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDescription { get; set; }
        [DataMember]
        public string InitContent { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public long ViewCount { get; set; }
        [DataMember]
        public int FeedBackCount { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginaUrl { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedChildFeedBackDate { get; set; }
    }
    [DataContract]
    public class FeedBackNewsWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public long ViewCount { get; set; }
        [DataMember]
        public int FeedBackCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedChildFeedBackDate { get; set; }

        [DataMember]
        public string ParentTitle { get; set; }
    }

    [DataContract]
    public class ExFeedBackNewsEntity : EntityBase
    {
        [DataMember]
        public long FeedBack_ID { set; get; }
        [DataMember]
        public long News_ID { set; get; }
        [DataMember]
        public int Cat_ID { set; get; }
        [DataMember]
        public string News_Title { set; get; }
        [DataMember]
        public string News_Subtitle { set; get; }
        [DataMember]
        public string News_Image { set; get; }
        [DataMember]
        public string News_ImageNote { set; get; }
        [DataMember]
        public string News_Source { set; get; }
        [DataMember]
        public string News_InitContent { set; get; }
        [DataMember]
        public string News_Content { set; get; }
        [DataMember]
        public string News_Athor { set; get; }
        [DataMember]
        public string News_Approver { set; get; }
        [DataMember]
        public int News_Status { set; get; }
        [DataMember]
        public DateTime News_PublishDate { set; get; }
        [DataMember]
        public bool News_isFocus { set; get; }
        [DataMember]
        public int News_Mode { set; get; }
        [DataMember]
        public string News_Relation { set; get; }
        [DataMember]
        public long News_Rate { set; get; }
        [DataMember]
        public DateTime News_ModifedDate { set; get; }
        [DataMember]
        public string News_OtherCat { set; get; }
        [DataMember]
        public bool isComment { set; get; }
        [DataMember]
        public bool isUserRate { set; get; }
        [DataMember]
        public int Template { set; get; }
        [DataMember]
        public string Icon { set; get; }
        [DataMember]
        public int Nhuanbut { set; get; }
        [DataMember]
        public string News_Relation1 { set; get; }
        [DataMember]
        public int WordCount { set; get; }
        [DataMember]
        public string Note { set; get; }
        [DataMember]
        public string Meta_Title { set; get; }
        [DataMember]
        public string Meta_Description { set; get; }
        [DataMember]
        public string FullName { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public string Phone { set; get; }
        [DataMember]
        public string Address { set; get; }
        [DataMember]
        public int FeedBack_Count { set; get; }
        [DataMember]
        public DateTime FeedBack_ModifedDate { set; get; }
        [DataMember]
        public long ImsNewsId { set; get; }
    }
}
