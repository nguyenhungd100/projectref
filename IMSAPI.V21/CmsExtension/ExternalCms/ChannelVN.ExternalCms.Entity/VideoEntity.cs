﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public enum EnumVideoStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Temporary = 0,
        [EnumMember]
        Published = 1,
        [EnumMember]
        Unpublished = 2,
        [EnumMember]
        WaitForPublish = 3,
        [EnumMember]
        Return = 4,
        [EnumMember]
        WaitForEdit = 5,
        [EnumMember]
        YoutubeUploading = 99
    }
    [DataContract]
    public enum EnumVideoSortOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        NameDesc = 2,
        [EnumMember]
        NameAsc = 3,
        [EnumMember]
        IdAsc = 4,
        [EnumMember]
        IdDesc = 5,
        [EnumMember]
        PublishDateDesc = 6,
        [EnumMember]
        PublishDateAsc = 7
    }
    [DataContract]
    public class VideoEntity : EntityBase
    {
        // Video Info
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string Pname { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishBy { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string VideoRelation { get; set; }
        // VideoFileInfo
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        // For advertisement
        [DataMember]
        public bool AllowAd { get; set; }

        [DataMember]
        public bool IsRemoveLogo { get; set; }

        [DataMember]
        public DateTime PlayOnTime { get; set; }

        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int VideoType { get; set; }
    }
}
