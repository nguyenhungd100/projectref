﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CommentLogEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ObjectId { set; get; }
        [DataMember]
        public string ActionName { set; get; }
        [DataMember]
        public DateTime CreatedDate { set; get; }
        [DataMember]
        public string UserDoAction { set; get; }
        [DataMember]
        public string IP { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public string Description { set; get; }
    }
}
