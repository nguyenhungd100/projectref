﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    public enum StatusEnum
    {
        //all
        All=-1,
        //None => Ngoài site insert
        None=0,
        //Đã Nhận => và trả lời
        Received=1,
        //Xóa
        Delete=2
    }
    [DataContract]
    public class QuickAnswerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string AttachFile { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime QuestedDate { get; set; }
        [DataMember]
        public string AnsweredBy { get; set; }
        [DataMember]
        public DateTime AnsweredDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }        
        [DataMember]
        public string QuestContent { get; set; }
        [DataMember]
        public string AnswerContent { get; set; }        
    }    
}
