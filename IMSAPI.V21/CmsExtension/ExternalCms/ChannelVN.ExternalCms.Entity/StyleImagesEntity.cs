﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class StyleImagesEntity : EntityBase
    {
        [DataMember]
        public int ImageId { get; set; }
        [DataMember]
        public long News_ID { get; set; }
        [DataMember]
        public int StyleId { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public bool IsAvatar { get; set; }
        [DataMember]
        public int ImageOrder { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
