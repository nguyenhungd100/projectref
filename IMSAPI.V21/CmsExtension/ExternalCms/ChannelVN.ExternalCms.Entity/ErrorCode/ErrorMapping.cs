﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            InnerHashtable.Add(ErrorCodes.CommentNotFound, "Không tìm thấy comment");
            InnerHashtable.Add(ErrorCodes.CommentReceivedByOtherUser, "Comment đã được xử lý bởi tài khoản khác");

            #region FeedBackNews

            InnerHashtable.Add(ErrorCodes.ExternalFeedBackNewsNotFound, "Không tìm thấy bài bạn đọc gửi từ hệ thống ngoài");
            InnerHashtable.Add(ErrorCodes.ParentFeedBackNewsNotFound, "Không tìm thấy bài gốc");
            InnerHashtable.Add(ErrorCodes.FeedBackNewsNotFound, "Không tìm thấy bài");

            #endregion
        }
        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = ErrorCodeBase.Success,
            [EnumMember]
            UnknowError = ErrorCodeBase.UnknowError,
            [EnumMember]
            Exception = ErrorCodeBase.Exception,
            [EnumMember]
            BusinessError = ErrorCodeBase.BusinessError,
            [EnumMember]
            InvalidRequest = ErrorCodeBase.InvalidRequest,
            [EnumMember]
            TimeOutSession = ErrorCodeBase.TimeOutSession,

            #endregion

            CommentNotFound = 10001,
            CommentReceivedByOtherUser = 10002,

            #region FeedBackNews

            ExternalFeedBackNewsNotFound = 20001,
            ParentFeedBackNewsNotFound = 20002,
            FeedBackNewsNotFound = 20003,

            #endregion
        }
    }
}
