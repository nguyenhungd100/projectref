﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CrawlTopNewsCategoryEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int ParentID { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Site { get; set; }
    }
}
