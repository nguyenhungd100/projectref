﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class QuickNewsEntity : EntityBase
    {
        [DataMember]
        public long QuickNewsId { get; set; }
        [DataMember]
        public int CatId { get; set; }
        [DataMember]
        public long VietId { get; set; }
        [DataMember]
        public long News_ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Link { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string PenName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
    }

    [DataContract]
    public class QuickNewsDetailEntity : EntityBase
    {
        [DataMember]
        public QuickNewsEntity QuickNews { get; set; }
        [DataMember]
        public QuickNewsAuthorEntity QuickNewsAuthor { get; set; }
        [DataMember]
        public List<QuickNewsImagesEntity> QuickNewsImages { get; set; }
    }
}
