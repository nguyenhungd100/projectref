﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class QuickNewsImagesEntity : EntityBase
    {
        [DataMember]
        public long QuickNewsImagesId { get; set; }
        [DataMember]
        public long QuickNewsId { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsAvatar { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
