﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CrawlTopNewsEntity : EntityBase
    {
        [DataMember]
        public int newsID { get; set; }
        [DataMember]
        public string news_title { get; set; }
        [DataMember]
        public string news_des { get; set; }
        [DataMember]
        public string news_title_char { get; set; }
        [DataMember]
        public string news_des_char { get; set; }
        [DataMember]
        public string news_link { get; set; }
        [DataMember]
        public DateTime news_date { get; set; }
        [DataMember]
        public int news_angent_id { get; set; }
        [DataMember]
        public int news_key_id { get; set; }
        [DataMember]
        public string news_content { get; set; }
        [DataMember]
        public string news_image { get; set; }
        [DataMember]
        public string slide_image { get; set; }
        [DataMember]
        public int news_status { get; set; }
        [DataMember]
        public string news_url { get; set; }
        [DataMember]
        public string news_main_tag { get; set; }
        [DataMember]
        public string news_tag { get; set; }
        [DataMember]
        public string news_sitemap { get; set; }
        [DataMember]
        public int news_crawl { get; set; }
        [DataMember]
        public int news_follow { get; set; }
        [DataMember]
        public int news_load { get; set; }
        [DataMember]
        public int news_duyet { get; set; }
        [DataMember]
        public int news_non_html { get; set; }
        [DataMember]
        public DateTime datecreate { get; set; }
        [DataMember]
        public int category { get; set; }
        [DataMember]
        public int click { get; set; }
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public bool is_move_cms { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public string SourceName { get; set; }
    }
    [DataContract]
    public class CrawlTopNewsWithSimplrFieldEntity : EntityBase
    {
        [DataMember]
        public int newsID { get; set; }
        [DataMember]
        public string news_title { get; set; }
        [DataMember]
        public string news_image { get; set; }
        [DataMember]
        public int news_status { get; set; }
        [DataMember]
        public string news_url { get; set; }
        [DataMember]
        public DateTime datecreate { get; set; }
        [DataMember]
        public int status { get; set; }
        [DataMember]
        public bool is_move_cms { get; set; }
    }
}
