﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class InterviewQuestionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Sex { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
