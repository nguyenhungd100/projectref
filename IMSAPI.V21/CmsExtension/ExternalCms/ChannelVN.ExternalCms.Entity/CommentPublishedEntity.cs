﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class CommentPublishedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectTitle { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Emotion { get; set; }
        [DataMember]
        public string SenderEmail { get; set; }
        [DataMember]
        public string SenderFullName { get; set; }
        [DataMember]
        public string SenderAvatar { get; set; }
        [DataMember]
        public string SenderAddress { get; set; }
        [DataMember]
        public string SenderPhoneNumber { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Liked { get; set; }
        [DataMember]
        public DateTime LastLikeDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }

        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public bool IsAdminComment { get; set; }
        [DataMember]
        public int CountChildren { get; set; }
        [DataMember]
        public string IPLog { get; set; }
        [DataMember]
        public long UserId { get; set; }
        [DataMember]
        public string MailReplied { get; set; }
        [DataMember]
        public string MailForwarded { get; set; }

    }

    [DataContract]
    public class CommentPublishedCountEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
    [DataContract]
    public class CommentPublishedStatisticsByAccount
    {
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
    [DataContract]
    public class CommentPublishedStatisticsEntity : EntityBase
    {
        [DataMember]
        public List<CommentPublishedCountEntity> CommentPublishedCount { get; set; }
        [DataMember]
        public List<CommentPublishedStatisticsByAccount> CommentPublishedStatistics { get; set; }
        [DataMember]
        public int TotalComment { get; set; }
    }
    [DataContract]
    public class CommentPublishedCountByZoneEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
    [DataContract]
    public enum EnumCommentPublishedObjectType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        ForNews = 1,
        [EnumMember]
        ForVideo = 2,
        [EnumMember]
        ForVote = 3,
        [EnumMember]
        ForLiveMatch = 4
    }
    [DataContract]
    public enum CommentPublishedStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        RemoveBeforeApprove = 3,
        [EnumMember]
        WaitForApproved = 2,
        [EnumMember]
        Approved = 1,
        [EnumMember]
        UnApproved = 0
    }
    //CuongNP: Add- 2014/11/28
    [DataContract]
    public class NewsHasNewCommentEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string ObjectTitle { get; set; }
        [DataMember]
        public string ObjectUrl { get; set; }
        [DataMember]
        public int ObjectType { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string SenderEmail { get; set; }
        [DataMember]
        public string SenderFullName { get; set; }

        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Liked { get; set; }
        [DataMember]
        public DateTime LastLikeDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string DeletedBy { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public bool IsAdminComment { get; set; }
        [DataMember]
        public int CommentCount { get; set; }

    }
}
