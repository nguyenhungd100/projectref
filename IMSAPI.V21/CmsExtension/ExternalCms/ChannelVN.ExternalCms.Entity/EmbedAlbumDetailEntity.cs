﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class EmbedAlbumDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int EmbedAlbumId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public bool IsAvatar { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
