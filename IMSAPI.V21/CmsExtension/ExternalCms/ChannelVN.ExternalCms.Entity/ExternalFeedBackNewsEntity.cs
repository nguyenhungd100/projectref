﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.ExternalCms.Entity
{
    [DataContract]
    public class ExternalFeedBackNewsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Subtitle { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDescription { get; set; }
        [DataMember]
        public string InitContent { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginaUrl { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    [DataContract]
    public class ExternalFeedBackNewsWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
    [DataContract]
    public class ExternalFeedBackNewsCountByParentEntity : EntityBase
    {
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public int ChildFeedBackNewsCount { get; set; }
    }
}
