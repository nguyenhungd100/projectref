﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class StyleToShareBo
    {
        #region Share Style

        public static ShareStylesEntity GetShareStyleByShareStyleId(int styleId)
        {
            return ShareStylesDal.GetById(styleId);
        }
        public static ShareStylesDetailEntity GetShareStyleDetailByShareStyleId(int styleId)
        {
            var shareStyle= ShareStylesDal.GetById(styleId);
            if (shareStyle != null)
            {
                return new ShareStylesDetailEntity
                    {
                        ShareStyles = shareStyle,
                        TopStyleImages = StyleImagesDal.GetByStyleId(styleId, 0)
                    };
            }
            return null;
        }
        public static List<ShareStylesEntity> SearchShareStyle(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return ShareStylesDal.Search(keyword, pageIndex, pageSize, ref totalRow);
        }
        public static List<ShareStylesDetailEntity> SearchShareStyleDetail(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var listShareStyle = ShareStylesDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            return listShareStyle.Select(shareStyles => new ShareStylesDetailEntity
                {
                    ShareStyles = shareStyles,
                    TopStyleImages = StyleImagesDal.GetByStyleId(shareStyles.StyleId, 5)
                }).ToList();
        }
        public static ErrorMapping.ErrorCodes DeleteShareStyleByShareStyleId(int styleId)
        {
            return ShareStylesDal.DeleteById(styleId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReceiveShareStyle(int styleId, long newsId)
        {
            return ShareStylesDal.Receive(styleId, newsId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        #region Style images

        public static List<StyleImagesEntity> GetStyleImageByStyleId(int styleId, int top)
        {
            return StyleImagesDal.GetByStyleId(styleId, top);
        }

        #endregion
    }
}
