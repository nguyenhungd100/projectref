﻿using System.Collections.Generic;
using System;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.SocialNetwork.BO;
using System.Linq;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.ExternalCms.Bo
{
    public class CommentPublishedBo
    {
        private const string RedisKeyForCountCommentByNewsId = "CmsCommentCountByNewsId[{0}]";
        private const string RedisKeyForCountCommentByVideoId = "CmsCommentCountByVideoId[{0}]";
        private const string RedisKeyForCountCommentByVoteId = "CmsCommentCountByVoteId[{0}]";
        public class CommentCountEntity
        {
            public long ObjectId { get; set; }
            public int CommentCount { get; set; }
            public int CommentWaitApproveCount { get; set; }
        }
        public static void UpdateCommentCountIntoRedis(EnumCommentPublishedObjectType objectType, long objectId)
        {
            try
            {
                var commentCount = CommentPublishedDal.GetPublishedCommentCountByObjectId((int)objectType, objectId);
                var commentWaitApproveCount = CommentDal.GetWaitApproveCommentCountByObjectId((int)objectType, objectId);

                var keyName = "";
                switch (objectType)
                {
                    case EnumCommentPublishedObjectType.ForVideo:
                        keyName = string.Format(RedisKeyForCountCommentByVideoId, objectId);
                        break;
                    case EnumCommentPublishedObjectType.ForVote:
                        keyName = string.Format(RedisKeyForCountCommentByVoteId, objectId);
                        break;
                    default:
                        keyName = string.Format(RedisKeyForCountCommentByNewsId, objectId);
                        break;
                }
                var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));

                var redisClient = new RedisStackClient(dbNumber);
                if (commentCount > 0 || commentWaitApproveCount > 0)
                {
                    redisClient.Remove(keyName);
                    redisClient.Add(keyName,
                        new
                        {
                            ObjectId = objectId,
                            CommentCount = commentCount,
                            CommentWaitApproveCount = commentWaitApproveCount
                        }, DateTime.Now.AddYears(1).TimeOfDay);
                }
                else
                {
                    redisClient.Remove(keyName);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
        }

        public static List<CommentCounterEntity> GetCommentCounterInternalByObjectId(string lstId, int objectType)
        {
            return CommentPublishedDal.GetCommentCounterInternalByObjectId(lstId, objectType);
        }

        public static int GetPublishedCommentCountByNewsId(EnumCommentPublishedObjectType objectType, long objectId)
        {
            return CommentPublishedDal.GetPublishedCommentCountByObjectId((int)objectType, objectId);
        }

        public static List<CommentPublishedEntity> SearchCommentPublished(string objectTitle, EnumCommentPublishedObjectType objectType, string title, long parentId, CommentPublishedStatus status, string receivedBy, long objectId, int pageIndex, int pageSize, DateTime publishDate, int TitleSearchMode, ref int totalRow)
        {
            return CommentPublishedDal.SearchCommentPublished(objectTitle, (int)objectType, title, parentId, status, receivedBy, objectId, pageIndex, pageSize, publishDate, TitleSearchMode, ref totalRow);
        }

        public static CommentPublishedEntity GetCommentPublishedById(long id)
        {
            return CommentPublishedDal.GetCommentPublishedById(id);
        }

        public static ErrorMapping.ErrorCodes ReceiveCommentPublished(CommentPublishedEntity comment)
        {
            try
            {
                if (null == comment)
                {
                    return ErrorMapping.ErrorCodes.CommentNotFound;
                }
                comment.ReceivedBy = WcfMessageHeader.Current.ClientUsername;
                var result = CommentPublishedDal.ReceiveCommentPublished(comment)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;

                UpdateCommentCountIntoRedis((EnumCommentPublishedObjectType)comment.ObjectType, comment.ObjectId);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.ReceiveCommentPublished:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes CommentPublishedAdminInsert(CommentPublishedEntity comment)
        {
            try
            {
                if (null == comment)
                {
                    return ErrorMapping.ErrorCodes.CommentNotFound;
                }
                comment.ReceivedBy = WcfMessageHeader.Current.ClientUsername;
                var result = CommentPublishedDal.CommentPublishedAdminInsert(comment)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    if (comment.Status == (int)CommentPublishedStatus.Approved)
                    {
                        EnumCommentPublishedObjectType objectType;
                        if (!Enum.TryParse(comment.ObjectType.ToString(), out objectType))
                        {
                            objectType = EnumCommentPublishedObjectType.ForNews;
                        }
                        var allow = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "AllowUpdateCommentCountIntoRedis");
                        if (allow != "0")
                        {
                            UpdateCommentCountIntoRedis(objectType, comment.ObjectId);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.CommentPublishedAdminInsert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes ApproveCommentPublished(long id)
        {
            try
            {
                var result = CommentPublishedDal.ApproveCommentPublished(id, WcfMessageHeader.Current.ClientUsername)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                var commentPublish = GetCommentPublishedById(id);
                if (commentPublish != null)
                {
                    EnumCommentPublishedObjectType objectType;
                    if (!Enum.TryParse(commentPublish.ObjectType.ToString(), out objectType))
                    {
                        objectType = EnumCommentPublishedObjectType.ForNews;
                    }
                    var allow = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "AllowUpdateCommentCountIntoRedis");
                    if (allow != "0")
                    {
                        UpdateCommentCountIntoRedis(objectType, commentPublish.ObjectId);
                    }

                    //log hành động xuất bản bình luận
                    ActivityBo.LogPublishComment(id, WcfMessageHeader.Current.ClientUsername, commentPublish.Content);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.Re:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes RemoveCommentPublished(long id)
        {
            try
            {
                var result = CommentPublishedDal.RemoveCommentPublished(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    var comment = GetCommentPublishedById(id);
                    //log hành động xóa bình luận
                    ActivityBo.LogDeleteComment(id, WcfMessageHeader.Current.ClientUsername, comment == null ? "" : comment.Content);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.RemoveCommentPublished:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UnapproveCommentPublished(long id)
        {
            try
            {
                var result = CommentPublishedDal.UnapproveCommentPublished(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (result== ErrorMapping.ErrorCodes.Success)
                {
                    var comment = GetCommentPublishedById(id);
                    //log hành động gỡ bình luận
                    ActivityBo.LogUnPublishComment(id, WcfMessageHeader.Current.ClientUsername, comment == null ? "" : comment.Content);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.UnapproveCommentPublished:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateCommentPublishedById(CommentPublishedEntity commentPublished)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            if (commentPublished != null)
            {
                result = CommentPublishedDal.UpdateCommentPublishedById(commentPublished)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                commentPublished = GetCommentPublishedById(commentPublished.Id);
                if (result == ErrorMapping.ErrorCodes.Success &&
                    commentPublished.Status == (int)CommentPublishedStatus.Approved)
                {
                    EnumCommentPublishedObjectType objectType;
                    if (!Enum.TryParse(commentPublished.ObjectType.ToString(), out objectType))
                    {
                        objectType = EnumCommentPublishedObjectType.ForNews;
                    }
                    UpdateCommentCountIntoRedis(objectType, commentPublished.ObjectId);
                }
            }

            return result;
        }
        public static ErrorMapping.ErrorCodes DeleteCommentPublishedById(long commentPublishedId, string deletedBy)
        {
            var result = CommentPublishedDal.DeleteCommentPublishedById(commentPublishedId, deletedBy)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                var comment = CommentBo.GetCommentById(commentPublishedId);
                //log hành động xóa bình luận
                ActivityBo.LogDeleteComment(commentPublishedId, deletedBy, comment == null ? "" : comment.Content);
            }
            return result;
        }

        public static List<CommentPublishedEntity> StatisticsGetCommentPublished(EnumCommentPublishedObjectType objectType, CommentPublishedStatus status, DateTime fromDate, DateTime toDate)
        {
            return CommentPublishedDal.StatisticsGetCommentPublished((int)objectType, status, fromDate, toDate);
        }

        public static ErrorMapping.ErrorCodes DeleteListCommentPublished(string commentIds, string LastModifiedBy)
        {
            var result = CommentPublishedDal.DeleteListComments(commentIds, LastModifiedBy)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                var comment = GetListCommentByListId(commentIds);
                //log hành động xóa bình luận
                foreach (var item in comment)
                {
                    ActivityBo.LogDeleteComment(item.Id, LastModifiedBy, item.Content);
                }
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UnApproveListCommentPublished(string commentIds, string LastModifiedBy)
        {
            try
            {
                var result = CommentPublishedDal.UnApproveListCommentPublished(commentIds, LastModifiedBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    var comment = GetListCommentByListId(commentIds);
                    //log hành động gỡ bình luận
                    foreach (var item in comment)
                    {
                        ActivityBo.LogUnPublishComment(item.Id, LastModifiedBy, item.Content);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.UnApproveListCommentPublished:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes ApproveListCommentPublished(string commentIds, string PublishBy)
        {
            try
            {
                var result = CommentPublishedDal.ApproveListCommentPublished(commentIds, PublishBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;

                var listCommmentId = commentIds.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var commentId in listCommmentId)
                {
                    var id = Utility.ConvertToLong(commentId);
                    var commentPublish = GetCommentPublishedById(id);
                    if (commentPublish != null)
                    {
                        EnumCommentPublishedObjectType objectType;
                        if (!Enum.TryParse(commentPublish.ObjectType.ToString(), out objectType))
                        {
                            objectType = EnumCommentPublishedObjectType.ForNews;
                        }
                        UpdateCommentCountIntoRedis(objectType, commentPublish.ObjectId);

                        //log hành động xuất bản bình luận
                        ActivityBo.LogPublishComment(commentPublish.Id, PublishBy, commentPublish.Content);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.UnApproveListCommentPublished:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes ReApproveListCommentPublished(string commentIds, string PublishBy)
        {
            try
            {
                var result = CommentPublishedDal.ReApproveListCommentPublished(commentIds, PublishBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;

                var listCommmentId = commentIds.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var commentId in listCommmentId)
                {
                    var id = Utility.ConvertToLong(commentId);
                    var commentPublish = GetCommentPublishedById(id);
                    if (commentPublish != null)
                    {
                        EnumCommentPublishedObjectType objectType;
                        if (!Enum.TryParse(commentPublish.ObjectType.ToString(), out objectType))
                        {
                            objectType = EnumCommentPublishedObjectType.ForNews;
                        }
                        UpdateCommentCountIntoRedis(objectType, commentPublish.ObjectId);

                        //log hành động xuất bản bình luận
                        ActivityBo.LogPublishComment(commentPublish.Id, PublishBy, commentPublish.Content);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("CommentPublishedBo.ReApproveListCommentPublished:{0}", ex.Message));
            }
        }

        public static List<CommentPublishedEntity> GetPublishedDateNewsByNewsId(string NewsIds)
        {
            return CommentPublishedDal.GetPublishedDateNewsByNewsId(NewsIds);
        }
        public static List<CommentPublishedEntity> GetListCommentByListId(string commentIds)
        {
            try
            {
                return CommentPublishedDal.GetListCommentByListId(commentIds);
            }
            catch (Exception)
            {
                return new List<CommentPublishedEntity>();
            }
        }

        public static List<CommentEntity> GetCommentsByNewsId(long newsId, long parentId)
        {
            return CommentPublishedDal.GetCommentByNewsId(newsId, parentId);
        }

        // CuongNP: Add
        public static List<NewsHasNewCommentEntity> GetNewsHasNewComment(string objectTitle, EnumCommentPublishedObjectType objectType, CommentPublishedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentPublishedDal.GetNewsHasNewComment(objectTitle, (int)objectType, status, pageIndex, pageSize, ref totalRow);
        }
        public static List<CommentPublishedEntity> SearchCommentPublishedV2(string objectTitle, EnumCommentPublishedObjectType objectType, string content, long parentId, CommentPublishedStatus status, string receivedBy, long objectId,string zoneIds, int pageIndex, int pageSize, DateTime publishDate, ref int totalRow)
        {
            return CommentPublishedDal.SearchCommentPublishedV2(objectTitle, (int)objectType, content, parentId, status, receivedBy, objectId,zoneIds, pageIndex, pageSize, publishDate, ref totalRow);
        }

        public static List<CommentPublishedCountEntity> CommentPublishedCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            return CommentPublishedDal.CommentPublishedCount(fromDate, toDate, zoneIds);
        }
        public static CommentPublishedStatisticsEntity CommentPublished_Statistics(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            try
            {
                var countList = new CommentPublishedStatisticsEntity();
                countList.CommentPublishedCount = new List<CommentPublishedCountEntity>();
                countList.CommentPublishedStatistics = new List<CommentPublishedStatisticsByAccount>();
                var data = CommentPublishedDal.CommentPublished_Statistics(fromDate, toDate, zoneIds);
                if (data.Count > 0)
                {
                    countList.CommentPublishedCount = (from f in data
                                                       where f.PublishedBy == "1" || f.PublishedBy == "2"
                                                       select new CommentPublishedCountEntity
                                                       {
                                                           Count = f.Count,
                                                           Status = Utility.ConvertToInt(f.PublishedBy)
                                                       }).ToList();
                    countList.CommentPublishedStatistics = (from f in data
                                                            where f.PublishedBy != "1" && f.PublishedBy != "2"
                                                            select new CommentPublishedStatisticsByAccount
                                                            {
                                                                Count = f.Count,
                                                                PublishedBy = f.PublishedBy
                                                            }).OrderByDescending(k=>k.Count).ToList();
                }
                var Total = CommentDal.CommentCount(fromDate, toDate, zoneIds);
                countList.TotalComment = Total.Sum(k => k.Count);
                return countList;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new CommentPublishedStatisticsEntity();
            }
        }

        public static List<CommentPublishedCountByZoneEntity> CommentPublishedCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            return CommentPublishedDal.CommentPublishedCountByZone(fromDate, toDate, zoneIds);
        }

        public static ErrorMapping.ErrorCodes UpdateLikeCountComment(string value)
        {
            return CommentPublishedDal.UpdateLikeCountComment(value) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes InsertCommentLogAction(CommentLogEntity commentLog)
        {
            return CommentPublishedDal.InsertCommentLogAction(commentLog) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes UpdateMailForwarded(long id, string mailForwarded)
        {
            return CommentPublishedDal.UpdateMailForwarded(id, mailForwarded) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateMailReplied(long id, string mailReplied)
        {
            return CommentPublishedDal.UpdateMailReplied(id, mailReplied) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

    }
}
