﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class QuickAnswerBo
    {
        #region DB ngoài

        #region QuickAnswerExternal

        public static QuickAnswerEntity ExternalGetQuickAnswerById(int id)
        {
            return QuickAnswerDal.ExtDBGetById(id);
        }               
        public static ErrorMapping.ErrorCodes ExternalReceiveQuickAnswer(int id, string receivedBy)
        {
            return QuickAnswerDal.ExtDBReceive(id, receivedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ExternalDeleteQuickAnswer(int id, string deletedBy)
        {
            return QuickAnswerDal.ExtDBDeleteById(id, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ExternalDeleteMultiQuickAnswer(string idList, string deletedBy)
        {
            return QuickAnswerDal.ExtDBDeleteByIds(idList, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<QuickAnswerEntity> ExternalSearchQuickAnswer(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickAnswerDal.ExtDBSearch(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static int ExternalCountQuickAnswer(int type, int status)
        {
            return QuickAnswerDal.ExtDBCount(type, status);
        }

        #endregion               

        #endregion

        #region DB CMS

        #region QuickAnswer

        public static ErrorMapping.ErrorCodes InsertQuickAnswer(QuickAnswerEntity quickAnswer)
        {
            return QuickAnswerDal.Insert(quickAnswer) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateQuickAnswer(QuickAnswerEntity quickAnswer)
        {
            return QuickAnswerDal.Update(quickAnswer) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static QuickAnswerEntity GetQuickAnswerById(int id)
        {
            var quickAnswer = QuickAnswerDal.GetById(id);
            return quickAnswer;
        }

        public static List<QuickAnswerEntity> SearchQuickAnswer(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return QuickAnswerDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes DeleteQuickAnswer(int id, string deletedBy)
        {
            return QuickAnswerDal.DeleteById(id, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteMultiQuickAnswer(string idList, string deletedBy)
        {
            return QuickAnswerDal.DeleteByIds(idList, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion               

        #endregion
    }
}
