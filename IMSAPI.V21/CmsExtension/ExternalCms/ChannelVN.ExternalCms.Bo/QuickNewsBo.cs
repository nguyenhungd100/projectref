﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class QuickNewsBo
    {
        #region DB ngoài

        #region QuickNewsExternal

        public static QuickNewsEntity ExternalGetQuickNewsByQuickNewsId(long id)
        {
            return QuickNewsDal.ExtDBGetById(id);
        }
        public static QuickNewsDetailEntity ExternalGetQuickNewsDetailByQuickNewsId(long id)
        {
            var quickNews = QuickNewsDal.ExtDBGetById(id);
            return new QuickNewsDetailEntity
                       {
                           QuickNews = quickNews,
                           QuickNewsAuthor = QuickNewsAuthorDal.ExtDBGetById(quickNews.VietId),
                           QuickNewsImages = QuickNewsImagesDal.ExtDBGetByQuickNewsId(quickNews.QuickNewsId)
                       };
        }
        public static QuickNewsEntity GetQuickNewsDetailByQuickNewsId(long id)
        {
            var quickNews = QuickNewsDal.GetById(id);
            return quickNews;
        }
        public static ErrorMapping.ErrorCodes ExternalReceiveQuickNews(long id, long newsId, string receivedBy)
        {
            return QuickNewsDal.ExtDBReceive(id, newsId, receivedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ExternalDeleteQuickNews(long id, string deletedBy)
        {
            return QuickNewsDal.ExtDBDeleteById(id, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ExternalDeleteMultiQuickNews(string idList, string deletedBy)
        {
            return QuickNewsDal.ExtDBDeleteByIds(idList, deletedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<QuickNewsEntity> ExternalSearchQuickNews(string keyword, int type, int status, int pageIndex,
                                                        int pageSize, ref int totalRow)
        {
            return QuickNewsDal.ExtDBSearch(keyword, type, status, pageIndex, pageSize, ref totalRow);
        }
        public static int ExternalCountQuickNews(int type, int status)
        {
            return QuickNewsDal.ExtDBCount(type, status);
        }

        #endregion

        #region QuickNewsAuthorExternal

        public static QuickNewsAuthorEntity ExternalGetQuickNewsAuthorById(long vietId)
        {
            return QuickNewsAuthorDal.ExtDBGetById(vietId);
        }
        public static QuickNewsAuthorEntity ExternalGetQuickNewsAuthorByEmail(string email)
        {
            return QuickNewsAuthorDal.ExtDBGetByEmail(email);
        }

        public static List<QuickNewsAuthorEntity> ExternalSearchQuickNewsAuthor(string keyword, string email, int status, int pageIndex,
                                                              int pageSize, ref int totalRow)
        {
            return QuickNewsAuthorDal.ExtDBSearch(keyword, email, status, pageIndex, pageSize, ref totalRow);
        }
        #endregion

        #region QuickNewsImagesExternal
        public static List<QuickNewsImagesEntity> ExternalGetQuickNewsImagesByQuickNewsId(long quickNewsId)
        {
            return QuickNewsImagesDal.ExtDBGetByQuickNewsId(quickNewsId);
        }
        #endregion

        #endregion

        #region DB CMS

        #region QuickNews

        public static ErrorMapping.ErrorCodes InsertQuickNews(QuickNewsEntity quickNews)
        {
            return QuickNewsDal.Insert(quickNews) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
        #region QuickNewsAuthor
        public static ErrorMapping.ErrorCodes InsertQuickNewsAuthor(QuickNewsAuthorEntity quickNewsAuthor)
        {
            return QuickNewsAuthorDal.Insert(quickNewsAuthor) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
        #region QuickNewsImages
        public static ErrorMapping.ErrorCodes InsertQuickNewsImage(QuickNewsImagesEntity quickNewsImage)
        {
            return QuickNewsImagesDal.Insert(quickNewsImage) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion

        #endregion
    }
}
