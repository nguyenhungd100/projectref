﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.Bo
{
    public class VideoBo
    {
        #region Get

        public static VideoEntity GetById(int videoId, ref string zoneName)
        {
            try
            {
                return VideoDal.GetById(videoId, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion
    }
}
