﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Dal;

namespace ChannelVN.ExternalCms.Bo
{
    public class CommentRateBo
    {
        public static bool UpdateUnprocessCommentRate()
        {
            try
            {
                var unprocessCommentRates = CommentRateDal.GetUnprocessComment();
                foreach (var unprocessCommentRate in unprocessCommentRates)
                {
                    if (CommentPublishedDal.UpdateCommentRate(unprocessCommentRate.CommentId,
                        unprocessCommentRate.Liked,
                        unprocessCommentRate.LastModifiedDate == DateTime.MinValue ? DateTime.Now : unprocessCommentRate.LastModifiedDate))
                    {
                        CommentRateDal.UpdateProcessedComment(unprocessCommentRate.CommentId);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "UpdateUnprocessCommentRate error ==> " + ex);
            }
            return false;
        }

        public static List<CommentRateEntity> GetUnprocessLikeComment(DateTime lastModifieddate)
        {
            return CommentRateDal.GetUnprocessLikeComment(lastModifieddate);
        }
    }
}
