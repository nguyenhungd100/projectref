﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class VoteBo
    {
        public static List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            var extVoteAnswers = VoteDal.GetListAnswersByVoteIdExt(voteId);
            return extVoteAnswers;
        }
    }
}
