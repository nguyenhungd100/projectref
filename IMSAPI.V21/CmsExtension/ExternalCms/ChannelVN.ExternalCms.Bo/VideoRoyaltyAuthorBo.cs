﻿using System.Collections.Generic;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.ExternalCms.Bo
{
    public class VideoRoyaltyAuthorBo
    {
        #region VideoRoyaltyAuthor
        public static List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id)
        {
            return VideoRoyaltyAuthorDal.GetByVideoId(id);
        }
        #endregion
    }
}
