﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Bo.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class FeedBackNewsBo
    {
        private static long SetPrimaryId()
        {
            return long.Parse(DateTime.Now.ToString(BoConstants.FEEDBACK_NEWS_FORMAT_ID));
        }

        public static FeedBackNewsEntity GetFeedBackNewsById(long id)
        {
            return FeedBackNewsDal.GetById(id);
        }
        public static FeedBackNewsEntity GetFeedBackNewsByParentFeedBackNewsId(long parentId)
        {
            return FeedBackNewsDal.GetByParentId(parentId);
        }

        public static List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNews(string keyword, long parentId, EnumFeedBackNewsStatus status, EnumFeedBackNewsFilterDateField filterDateField,
                                                      DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            return FeedBackNewsDal.Search(keyword, parentId, (int)status, (int)filterDateField,
                                          dateFrom, dateTo, pageIndex, pageSize,
                                          ref totalRow);
        }
        public static List<FeedBackNewsWithSimpleFieldEntity> SearchFeedBackNewsHasExternalNews(int pageIndex, int pageSize, ref int totalRow)
        {
            var listCountExternalNews = ExternalFeedBackNewsDal.GetAllParentIdHasExternalNews();
            var parentIds = "";
            foreach (var externalNews in listCountExternalNews)
            {
                parentIds = ";" + externalNews.ParentId;
            }
            if (!string.IsNullOrEmpty(parentIds))
            {
                parentIds = parentIds.Remove(0, 1);
            }

            var listParentFeedBackNews = FeedBackNewsDal.SearchFeedBackNewsHasExternalNews(parentIds, pageIndex, pageSize, ref totalRow);
            foreach (var parentFeedBackNew in listParentFeedBackNews)
            {
                parentFeedBackNew.FeedBackCount =
                    listCountExternalNews.Find(item => item.ParentId == parentFeedBackNew.Id).ChildFeedBackNewsCount;
            }
            return listParentFeedBackNews;
        }

        public static ErrorMapping.ErrorCodes DeleteFeedBackNewsById(long id)
        {
            var feedBackNews = GetFeedBackNewsById(id);
            if (feedBackNews == null) return ErrorMapping.ErrorCodes.FeedBackNewsNotFound;

            return FeedBackNewsDal.DeleteById(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes InsertFeedBackNews(FeedBackNewsEntity feedBackNews)
        {
            var result = false;

            feedBackNews.Id = SetPrimaryId();
            feedBackNews.Url = BuildLinkUrlForFeedBackNews(feedBackNews.Id, feedBackNews.Title);

            result = FeedBackNewsDal.Insert(feedBackNews);
            if (result) { result = FeedBackNewsDal.ExtDBInsert(feedBackNews); }
            else {
                result = DeleteFeedBackNewsById(feedBackNews.Id) == ErrorMapping.ErrorCodes.Success ? true : false;
            }

            return result == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateFeedBackNews(FeedBackNewsEntity feedBackNews)
        {
            var currentFeedBackNews = GetFeedBackNewsById(feedBackNews.Id);
            if (currentFeedBackNews == null) return ErrorMapping.ErrorCodes.FeedBackNewsNotFound;

            currentFeedBackNews.ZoneId = feedBackNews.ZoneId;
            currentFeedBackNews.Title = feedBackNews.Title;
            currentFeedBackNews.Subtitle = feedBackNews.Subtitle;
            currentFeedBackNews.Avatar = feedBackNews.Avatar;
            currentFeedBackNews.AvatarDescription = feedBackNews.AvatarDescription;
            currentFeedBackNews.InitContent = feedBackNews.InitContent;
            currentFeedBackNews.Content = feedBackNews.Content;
            currentFeedBackNews.Source = feedBackNews.Source;
            currentFeedBackNews.Author = feedBackNews.Author;
            currentFeedBackNews.IsFocus = feedBackNews.IsFocus;
            currentFeedBackNews.WordCount = feedBackNews.WordCount;
            currentFeedBackNews.Note = feedBackNews.Note;
            currentFeedBackNews.MetaTitle = feedBackNews.MetaTitle;
            currentFeedBackNews.MetaDescription = feedBackNews.MetaDescription;
            currentFeedBackNews.Url = BuildLinkUrlForFeedBackNews(feedBackNews.Id, feedBackNews.Title);
            currentFeedBackNews.FullName = feedBackNews.FullName;
            currentFeedBackNews.Email = feedBackNews.Email;
            currentFeedBackNews.Phone = feedBackNews.Phone;
            currentFeedBackNews.Address = feedBackNews.Address;
            currentFeedBackNews.DistributionDate = feedBackNews.DistributionDate;
            currentFeedBackNews.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;


            return FeedBackNewsDal.Update(feedBackNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes PublishFeedBack(long id, string publishedBy)
        {
            return FeedBackNewsDal.Publish(id, publishedBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UnpublishFeedBack(long id)
        {
            return FeedBackNewsDal.Unpublish(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<ExFeedBackNewsEntity> SearchNews(string keyword, int status, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {
            var newsList = new List<ExFeedBackNewsEntity>();
            try
            {
                return ExternalFeedBackNewsDal.SearchNews(keyword,status, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static ExFeedBackNewsEntity GetNewsById(long id)
        {
            var newsList = new ExFeedBackNewsEntity();
            try
            {
                return ExternalFeedBackNewsDal.GetNewsById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static ErrorMapping.ErrorCodes UpdateStatusIms(long newsId, int status, long feedbackId)
        {
            return ExternalFeedBackNewsDal.UpdateStatusIms(newsId, status, feedbackId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static string BuildLinkUrlForFeedBackNews(long newsId, string newsTitle)
        {
            try
            {
                var formatUrl = BoConstants.NewsUrlFormatForFeedBackNews;
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, titleUnsignAndSlash, newsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }

    public class NewsInListEntity
    {
    }
}
