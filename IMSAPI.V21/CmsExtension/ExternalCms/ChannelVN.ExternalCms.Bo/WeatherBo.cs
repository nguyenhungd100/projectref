﻿using System.Collections.Generic;
using System;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.Bo
{
    public class WeatherBo
    {
        #region Get
        public static List<ExternalWeatherEntity> GetAll()
        {
            return WeatherDal.GetAll();
        }
        public static ExternalWeatherEntity GetByCodeId(int id)
        {
            return WeatherDal.GetByCodeId(id);
        }
        #endregion
    }
}
