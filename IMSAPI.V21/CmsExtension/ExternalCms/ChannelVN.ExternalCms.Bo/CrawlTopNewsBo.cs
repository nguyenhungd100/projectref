﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.ExternalCms.Bo
{
    public class CrawlTopNewsBo
    {
        public static List<CrawlTopNewsAgentEntity> GetAllCrawlTopNewsAgent()
        {
            return CrawlTopNewsAgentDal.GetAllAgent();
        }

        public static CrawlTopNewsEntity GetCrawlTopNewsById(int id)
        {
            return CrawlTopNewsDal.GetById(id);
        }

        public static List<CrawlTopNewsEntity> SearchCrawlTopNews(int newsAngentId, string keyword, int pageIndex, int pageSize, int category, DateTime from, DateTime to,
                                                      ref int totalRow)
        {
            return CrawlTopNewsDal.Search(newsAngentId, keyword, pageIndex, pageSize, category, from, to, ref totalRow);
        }

        public static List<CrawlTopNewsCategoryEntity> GetCategoryByAgent(int id)
        {
            return CrawlTopNewsCategoryDal.GetCategoryByAgent(id);
        }
    }
}
