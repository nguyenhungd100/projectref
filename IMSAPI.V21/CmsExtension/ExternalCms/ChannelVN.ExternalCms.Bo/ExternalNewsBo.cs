﻿using System.Collections.Generic;
using System;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.Bo
{
    public class ExternalNewsBo
    {
        #region Get
        public static List<ExternalNewsEntity> SearchNews(string keyword, string zoneIds, DateTime fromDate, DateTime toDate, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return ExternalNewsDal.SearchNews(keyword, zoneIds, fromDate, toDate, type, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<ExternalNewsEntity>();
        }
        public static List<ExternalNewsEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return ExternalNewsDal.SearchNews(keyword, username, zoneIds, fromDate, toDate, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<ExternalNewsEntity>();
        }
        public static ExternalNewsEntity GetExternalNewsById(long id)
        {
            return ExternalNewsDal.GetExternalNewsById(id);
        }
        public static ExternalNewsEntity GetExternalNewsByImsNewsId(long id)
        {
            return ExternalNewsDal.GetExternalNewsByImsNewsId(id);
        }

        public static List<ExternalNewsInZoneEntity> GetZoneByNewsId(long id)
        {
            try
            {
                return ExternalNewsDal.GetZoneByNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<ExternalNewsInZoneEntity>();
        }

        public static List<ExternalNewsAuthorEntity> GetAuthorByNewsId(long id)
        {
            try
            {
                return ExternalNewsDal.GetAuthorByNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<ExternalNewsAuthorEntity>();
        }

        #endregion

        public static ErrorMapping.ErrorCodes ReceiveExternalNews(ExternalNewsEntity ExternalNews)
        {
            return ExternalNewsDal.ReceiveExternalNews(ExternalNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes PublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            return ExternalNewsDal.PublishExternalNews(ExternalNews)
                            ? ErrorMapping.ErrorCodes.Success
                            : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReturnExternalNews(ExternalNewsEntity ExternalNews)
        {
            return ExternalNewsDal.ReturnExternalNews(ExternalNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UnpublishExternalNews(ExternalNewsEntity ExternalNews)
        {
            return ExternalNewsDal.UnpublishExternalNews(ExternalNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateImsNewsInfo(ExternalNewsEntity ExternalNews)
        {
            return ExternalNewsDal.UpdateImsNewsInfo(ExternalNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateImsNewsBody(ExternalNewsEntity externalNews)
        {
            return ExternalNewsDal.UpdateImsNewsBody(externalNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
