﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.Bo.Common
{
    public class BoConstants
    {
        public static string NewsUrlFormatForFeedBackNews
        {
            get { return ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "UrlFormatFeedBackNews"); }
        }

        public const string FEEDBACK_NEWS_FORMAT_ID = "yyyyMMddHHmmssFFF";
    }
}
