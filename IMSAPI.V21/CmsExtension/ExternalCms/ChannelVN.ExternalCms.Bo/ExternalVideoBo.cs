﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class ExternalVideoBo
    {
        public static ExternalVideoEntity GetExternalVideoById(int id)
        {
            return ExternalVideoDal.GetById(id);
        }
        public static List<ExternalVideoEntity> GetUnprocessExternalVideo(string keyword, int zoneVideoId, int pageIndex, int pageSize, ref int totalRow)
        {
            return ExternalVideoDal.GetUnprocessExternalVideo(keyword, zoneVideoId, pageIndex, pageSize, ref totalRow);
        }
        public static int GetExternalVideoCount()
        {
            return ExternalVideoDal.GetVideoCounter();
        }
        public static ErrorMapping.ErrorCodes RecieveExternalVideo(int videoId)
        {
            return ExternalVideoDal.RecieveVideo(videoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteExternalVideo(int videoId)
        {
            return ExternalVideoDal.RecieveVideo(videoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
