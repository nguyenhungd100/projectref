﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Dal.Common;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class InterviewQuestionBo
    {
        public static InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            return InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
        }
        public static List<InterviewQuestionEntity> SearchInterviewQuestion(int interviewId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            return InterviewQuestionDal.SearchInterviewQuestion(interviewId, keyword, pageIndex, pageSize, ref totalRow);
        }
        public static List<InterviewQuestionEntity> GetNotReceiveInterviewQuestion()
        {
            return InterviewQuestionDal.GetNotReceiveQuestion();
        }
        public static int GetInterviewQuestionCount(int interviewId)
        {
            return InterviewQuestionDal.GetInterviewQuestionCount(interviewId);
        }
        public static ErrorMapping.ErrorCodes ReceiveListInterviewQuestion(string listQuestionId)
        {
            return InterviewQuestionDal.ReceiveListQuestion(listQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReceiveExternaQuestion(int interviewQuestionId)
        {
            return InterviewQuestionDal.ReceiveExternaQuestion(interviewQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteExternaQuestion(int interviewQuestionId)
        {
            return InterviewQuestionDal.DeleteExternaQuestion(interviewQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            return InterviewQuestionDal.UpdateExternalQuestion(interviewQuestion)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes InsertExternalQuestion(InterviewQuestionEntity interviewQuestion)
        {
            return InterviewQuestionDal.InsertExternalQuestion(interviewQuestion)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
