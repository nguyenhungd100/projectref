﻿using System.Collections.Generic;
using System;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.ExternalCms.Bo
{
    public class CommentBo
    {
        #region Get
        public static List<CommentEntity> Search(string newsTitle, EnumCommentObjectType objectType, string title, long parentId, CommentStatus status, long newsId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {
            try
            {
                return CommentDal.Search(newsTitle, (int)objectType, title, parentId, status, newsId, pageIndex, pageSize, zoneIds, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentEntity>();
        }
        public static CommentEntity GetCommentById(long id)
        {
            return CommentDal.GetCommentById(id);
        }
        public static List<CommentEntity> StatisticsGetComment(EnumCommentObjectType objectType, CommentStatus status, DateTime fromDate, DateTime toDate)
        {
            return CommentDal.StatisticsGetComment((int)objectType, status, fromDate, toDate);
        }
        public static List<CommentCounterEntity> GetCommentCounterByObjectId(string lstId, int objectType)
        {
            return CommentDal.GetCommentCounterByObjectId(lstId, objectType);
        }

        public static List<CommentCounterEntity> GetCommentCounterByObjectIdAndStatus(string lstId, int objectType, int status)
        {
            return CommentDal.GetCommentCounterByObjectIdAndStatus(lstId, objectType, status);
        }

        // CuongNP: Add-2014/11/25
        public static List<NewsHasNewCommentExtEntity> GetNewsHasNewCommentExt(string objectTitle, EnumCommentObjectType objectType, CommentStatus status, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentDal.GetNewsHasNewCommentExt(objectTitle, (int)objectType, status, zoneIds, pageIndex, pageSize, ref totalRow);
        }

        public static List<CommentEntity> SearchV2(string objectTitle, EnumCommentObjectType objectType, string content, long parentId, CommentStatus status, long objectId, int pageIndex, int pageSize, string zoneIds, ref int totalRow)
        {
            try
            {
                return CommentDal.SearchV2(objectTitle, (int)objectType, content, parentId, status, objectId, pageIndex, pageSize, zoneIds, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentEntity>();
        }

        public static List<CommentEntity> SearchObjectByUser(long userId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return CommentDal.SearchObjectByUser(userId, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentEntity>();
        }

        public static List<CommentEntity> SearchCommentByUserObject(long userId, int status, long objectId)
        {
            try
            {
                return CommentDal.SearchCommentByUserObject(userId, status, objectId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentEntity>();
        }
        public static List<CommentCountEntity> CommentCount(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            try
            {
                return CommentDal.CommentCount(fromDate, toDate, zoneIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentCountEntity>();
        }
        public static List<CommentCountByZoneEntity> CommentCountByZone(DateTime fromDate, DateTime toDate, string zoneIds)
        {
            try
            {
                return CommentDal.CommentCountByZone(fromDate, toDate, zoneIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<CommentCountByZoneEntity>();
        }
        #endregion

        #region Set
        public static ErrorMapping.ErrorCodes ReceiveCommentById(long id, ref long commentPublishedId)
        {
            var comment = CommentBo.GetCommentById(id);
            if (null != comment)
            {
                //if (comment.Status == 1)
                //{
                var pubishedComment = new CommentPublishedEntity
                    {
                        Id = comment.Id,
                        ObjectId = comment.ObjectId,
                        ObjectTitle = comment.ObjectTitle,
                        ObjectUrl = comment.ObjectUrl,
                        ObjectType = comment.ObjectType,
                        Title = comment.Title,
                        Content = comment.Content,
                        Emotion = comment.Emotion,
                        SenderAddress = comment.SenderAddress,
                        SenderAvatar = comment.SenderAvatar,
                        SenderEmail = comment.SenderEmail,
                        SenderFullName = comment.SenderFullName,
                        SenderPhoneNumber = comment.SenderPhoneNumber,
                        ParentId = comment.ParentId,
                        CreatedDate = comment.CreatedDate,
                        ZoneId = comment.ZoneId,
                        IPLog = comment.IPLog,
                        UserId = comment.UserId,
                        MailReplied = comment.MailReplied,
                        MailForwarded = comment.MailForwarded
                    };
                var resutl = CommentPublishedBo.ReceiveCommentPublished(pubishedComment);
                if (resutl == ErrorMapping.ErrorCodes.Success)
                {
                    commentPublishedId = comment.Id;
                    return UpdateCommentStatusById(comment.Id, WcfMessageHeader.Current.ClientUsername);
                }
                //}

                return ErrorMapping.ErrorCodes.CommentReceivedByOtherUser;
            }
            else
            {
                return ErrorMapping.ErrorCodes.CommentNotFound;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateCommentStatusById(long id, string approveBy)
        {
            try
            {
                if (id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return CommentDal.UpdateCommentStatusById(id, approveBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("GameBrand.InsertBrand:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateCommentById(CommentEntity comment)
        {
            return CommentDal.UpdateCommentById(comment)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteCommentById(long id, string deletedBy)
        {
            return CommentDal.DeleteCommentById(id, deletedBy)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteListComments(string commentIds, string LastModifiedBy)
        {
            return CommentDal.DeleteListComments(commentIds, LastModifiedBy)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes RemoveCommentById(long id, string LastModifiedBy)
        {
            try
            {
                if (id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return CommentDal.RemoveCommentById(id, LastModifiedBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("RemoveCommentById:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes InsertComment(CommentEntity comment)
        {
            return CommentDal.InsertComment(comment)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMailForwarded(long id, string mailForwarded)
        {
            return CommentDal.UpdateMailForwarded(id, mailForwarded)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMailReplied(long id, string mailReplied)
        {
            return CommentDal.UpdateMailReplied(id, mailReplied)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
