﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.ExternalCms.Bo
{
    public class ExternalFeedBackNewsBo
    {
        public static ExternalFeedBackNewsEntity GetExternalFeedBackNewsById(long id)
        {
            return ExternalFeedBackNewsDal.GetById(id);
        }

        public static List<ExternalFeedBackNewsWithSimpleFieldEntity> SearchExternalFeedBackNews(string keyword, long parentId,
                                                      DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            return ExternalFeedBackNewsDal.Search(keyword, parentId,
                                                      dateFrom, dateTo, pageIndex, pageSize,
                                                      ref totalRow);
        }

        public static ErrorMapping.ErrorCodes DeleteExternalFeedBackNewsById(long id)
        {
            var externalFeedBackNews = GetExternalFeedBackNewsById(id);
            if (externalFeedBackNews == null) return ErrorMapping.ErrorCodes.ExternalFeedBackNewsNotFound;

            return ExternalFeedBackNewsDal.DeleteById(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReceiveExternalFeedBackNews(long id)
        {
            var externalFeedBackNews = GetExternalFeedBackNewsById(id);
            if (externalFeedBackNews == null) return ErrorMapping.ErrorCodes.ExternalFeedBackNewsNotFound;

            var parentFeedBackNews = FeedBackNewsBo.GetFeedBackNewsByParentFeedBackNewsId(externalFeedBackNews.ParentId);
            if (parentFeedBackNews == null) return ErrorMapping.ErrorCodes.ParentFeedBackNewsNotFound;

            var errorCode = FeedBackNewsBo.InsertFeedBackNews(new FeedBackNewsEntity
                {
                    ParentId = externalFeedBackNews.ParentId,
                    ZoneId = externalFeedBackNews.ZoneId,
                    Title = externalFeedBackNews.Title,
                    Subtitle = externalFeedBackNews.Subtitle,
                    Avatar = externalFeedBackNews.Avatar,
                    AvatarDescription = externalFeedBackNews.AvatarDescription,
                    InitContent = externalFeedBackNews.InitContent,
                    Content = externalFeedBackNews.Content,
                    Source = externalFeedBackNews.Source,
                    Author = externalFeedBackNews.Author,
                    IsFocus = externalFeedBackNews.IsFocus,
                    WordCount = externalFeedBackNews.WordCount,
                    Note = externalFeedBackNews.Note,
                    MetaTitle = externalFeedBackNews.MetaTitle,
                    MetaDescription = externalFeedBackNews.MetaDescription,
                    Url = externalFeedBackNews.Url,
                    OriginaUrl = externalFeedBackNews.OriginaUrl,
                    FullName = externalFeedBackNews.FullName,
                    Email = externalFeedBackNews.Email,
                    Phone = externalFeedBackNews.Phone,
                    Address = externalFeedBackNews.Address,
                    Status = externalFeedBackNews.Status,
                    ReceivedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername
                });

            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                return ExternalFeedBackNewsDal.Receive(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }

            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
