﻿using System.Collections.Generic;
using System;
using ChannelVN.CMS.Common;
using ChannelVN.ExternalCms.Dal;
using ChannelVN.ExternalCms.Entity;

namespace ChannelVN.ExternalCms.Bo
{
    public class GiftCodeBo
    {
        #region Get
        public static List<GiftCodeEntity> GetByNewsId(long newsId)
        {
            try
            {
                return GiftCodeDal.GetByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return new List<GiftCodeEntity>();
        }

        #endregion

    }
}
