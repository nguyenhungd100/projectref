﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Dantri.Dal;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;

namespace ChannelVN.Dantri.Bo
{
    public class NewsRoyaltyInfoBo
    {
        #region NewsRoyaltyInfo
        public static NewsRoyaltyInfoEntity GetNewsRoyaltyInfoByNewsId(long id)
        {
            return NewsRoyaltyInfoDal.GetByNewsId(id);
        }
        public static ErrorMapping.ErrorCodes InsertNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            return NewsRoyaltyInfoDal.Insert(NewsRoyaltyInfo)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateNewsRoyaltyInfo(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            return NewsRoyaltyInfoDal.Update(NewsRoyaltyInfo)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return NewsRoyaltyInfoDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteByNewsId(long tagId)
        {
            return NewsRoyaltyInfoDal.DeleteByNewsId(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
