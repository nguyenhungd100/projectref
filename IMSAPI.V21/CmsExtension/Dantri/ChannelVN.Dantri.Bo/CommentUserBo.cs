﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Dantri.Dal;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;

namespace ChannelVN.Dantri.Bo
{
    public class CommentUserBo
    {
        #region CommentUser
        public static List<CommentUserEntity> Search(string userName, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return CommentUserDal.Search(userName, status, pageIndex, pageSize, ref totalRow);
        }
        public static CommentUserEntity GetById(long id)
        {
            return CommentUserDal.GetById(id);
        }
        public static ErrorMapping.ErrorCodes InsertNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            return NewsRoyaltyAuthorDal.Insert(NewsRoyaltyAuthor)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateCommentUser(CommentUserEntity commentUser)
        {
            return CommentUserDal.Update(commentUser)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateStatusCommentUser(CommentUserEntity commentUser)
        {
            return CommentUserDal.UpdateStatus(commentUser)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return NewsRoyaltyAuthorDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteByNewsId(long tagId)
        {
            return NewsRoyaltyAuthorDal.DeleteByNewsId(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
