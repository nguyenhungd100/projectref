﻿using ChannelVN.Dantri.Dal;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;

namespace ChannelVN.Dantri.Bo
{
    public class VideoRoyaltyInfoBo
    {
        #region VideoRoyaltyInfo
        public static VideoRoyaltyInfoEntity GetVideoRoyaltyInfoByVideoId(long id)
        {
            return VideoRoyaltyInfoDal.GetByVideoId(id);
        }
        public static ErrorMapping.ErrorCodes InsertVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo)
        {
            return VideoRoyaltyInfoDal.Insert(VideoRoyaltyInfo)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoRoyaltyInfo(VideoRoyaltyInfoEntity VideoRoyaltyInfo)
        {
            return VideoRoyaltyInfoDal.Update(VideoRoyaltyInfo)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return VideoRoyaltyInfoDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteByVideoId(long tagId)
        {
            return VideoRoyaltyInfoDal.DeleteByVideoId(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
