﻿using System.Collections.Generic;
using ChannelVN.Dantri.Dal;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;

namespace ChannelVN.Dantri.Bo
{
    public class VideoRoyaltyAuthorBo
    {
        #region VideoRoyaltyAuthor
        public static List<VideoRoyaltyAuthorEntity> GetVideoRoyaltyAuthorByVideoId(long id)
        {
            return VideoRoyaltyAuthorDal.GetByVideoId(id);
        }
        public static ErrorMapping.ErrorCodes InsertVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            return VideoRoyaltyAuthorDal.Insert(videoRoyaltyAuthor)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoRoyaltyAuthor(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            return VideoRoyaltyAuthorDal.Update(videoRoyaltyAuthor)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return VideoRoyaltyAuthorDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteByVideoId(long tagId)
        {
            return VideoRoyaltyAuthorDal.DeleteByVideoId(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
