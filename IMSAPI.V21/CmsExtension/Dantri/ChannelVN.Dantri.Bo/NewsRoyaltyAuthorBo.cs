﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Dantri.Dal;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.Entity.ErrorCode;

namespace ChannelVN.Dantri.Bo
{
    public class NewsRoyaltyAuthorBo
    {
        #region NewsRoyaltyAuthor
        public static List<NewsRoyaltyAuthorEntity> GetNewsRoyaltyAuthorByNewsId(long id)
        {
            return NewsRoyaltyAuthorDal.GetByNewsId(id);
        }
        public static ErrorMapping.ErrorCodes InsertNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            return NewsRoyaltyAuthorDal.Insert(NewsRoyaltyAuthor)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateNewsRoyaltyAuthor(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            return NewsRoyaltyAuthorDal.Update(NewsRoyaltyAuthor)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long tagId)
        {
            return NewsRoyaltyAuthorDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteByNewsId(long tagId)
        {
            return NewsRoyaltyAuthorDal.DeleteByNewsId(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
