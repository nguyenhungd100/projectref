﻿using ChannelVN.Dantri.Dal.Common;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.Dal
{
    public class VideoRoyaltyInfoDal
    {
        #region SET

        public static bool Insert(VideoRoyaltyInfoEntity videoRoyaltyInfo)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyInfoMainDal.Insert(videoRoyaltyInfo);
            }
            return returnValue;
        }

        public static bool Update(VideoRoyaltyInfoEntity videoRoyaltyInfo)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyInfoMainDal.Update(videoRoyaltyInfo);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyInfoMainDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static bool DeleteByVideoId(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyInfoMainDal.DeleteByVideoId(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static VideoRoyaltyInfoEntity GetByVideoId(long id)
        {
            VideoRoyaltyInfoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyInfoMainDal.GetByVideoId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
