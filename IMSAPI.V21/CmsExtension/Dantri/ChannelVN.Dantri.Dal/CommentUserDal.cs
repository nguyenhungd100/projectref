﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Dantri.Dal.Common;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.Dal
{
    public class CommentUserDal
    {
        #region SET

        public static bool Insert(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.Insert(NewsRoyaltyAuthor);
            }
            return returnValue;
        }

        public static bool Update(CommentUserEntity commentUser)
        {
            bool returnValue = false;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CommentUserMainDal.Update(commentUser);
            }
            return returnValue;
        }

        public static bool UpdateStatus(CommentUserEntity commentUser)
        {
            bool returnValue = false;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CommentUserMainDal.UpdateStatus(commentUser);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static bool DeleteByNewsId(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.DeleteByNewsId(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<CommentUserEntity> Search(string userName, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CommentUserEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CommentUserMainDal.Search(userName, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static CommentUserEntity GetById(long id)
        {
            CommentUserEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.CommentUserMainDal.GetById(id);
            }
            return returnValue;
        }
        #endregion
    }
}
