﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Dantri.Dal.Common;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.Dal
{
    public class NewsRoyaltyAuthorDal
    {
        #region SET

        public static bool Insert(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.Insert(NewsRoyaltyAuthor);
            }
            return returnValue;
        }

        public static bool Update(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.Update(NewsRoyaltyAuthor);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static bool DeleteByNewsId(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.DeleteByNewsId(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<NewsRoyaltyAuthorEntity> GetByNewsId(long id)
        {
            List<NewsRoyaltyAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyAuthorMainDal.GetByNewsId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
