﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Dantri.Dal.Common;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.Dal
{
    public class NewsRoyaltyInfoDal
    {
        #region SET

        public static bool Insert(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyInfoMainDal.Insert(NewsRoyaltyInfo);
            }
            return returnValue;
        }

        public static bool Update(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyInfoMainDal.Update(NewsRoyaltyInfo);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyInfoMainDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static bool DeleteByNewsId(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyInfoMainDal.DeleteByNewsId(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static NewsRoyaltyInfoEntity GetByNewsId(long id)
        {
            NewsRoyaltyInfoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltyInfoMainDal.GetByNewsId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
