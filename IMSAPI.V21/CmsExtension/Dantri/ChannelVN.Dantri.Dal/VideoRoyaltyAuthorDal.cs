﻿using System.Collections.Generic;
using ChannelVN.Dantri.Dal.Common;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.Dal
{
    public class VideoRoyaltyAuthorDal
    {
        #region SET

        public static bool Insert(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.Insert(videoRoyaltyAuthor);
            }
            return returnValue;
        }

        public static bool Update(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.Update(videoRoyaltyAuthor);
            }
            return returnValue;
        }

        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static bool DeleteByVideoId(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.DeleteByVideoId(tagId);
            }
            return returnValue;
        }
        #endregion

        #region GET

        public static List<VideoRoyaltyAuthorEntity> GetByVideoId(long id)
        {
            List<VideoRoyaltyAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoRoyaltyAuthorMainDal.GetByVideoId(id);
            }
            return returnValue;
        }
        #endregion
    }
}
