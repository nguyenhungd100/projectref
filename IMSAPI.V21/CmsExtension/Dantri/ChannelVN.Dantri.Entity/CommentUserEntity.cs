﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Dantri.Entity
{
    [DataContract]
    public class CommentUserEntity : EntityBase
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public string Password { set; get; }
        [DataMember]
        public string FacebookId { set; get; }
        [DataMember]
        public string GoogleId { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public bool Sex { set; get; }
        [DataMember]
        public DateTime BirthDay { set; get; }
        [DataMember]
        public string Address { set; get; }
        [DataMember]
        public string Job { set; get; }
        [DataMember]
        public string Phone { set; get; }
        [DataMember]
        public string IdentityNumber { set; get; }
    }

}
