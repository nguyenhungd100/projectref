﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Dantri.Entity
{
    [DataContract]
    public class NewsRoyaltyInfoEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int PictureType { get; set; }
        [DataMember]
        public string QualityFactor { get; set; }
        [DataMember]
        public string ContributionFactor { get; set; }
        [DataMember]
        public string RiskFactor { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; } 
         [DataMember]
        public DateTime LastModifyDate { get; set; }
        [DataMember]
        public string LastModifyBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int NumberOfPicture { get; set; }
    }
}
