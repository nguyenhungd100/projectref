﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Dantri.Entity
{
    [DataContract]
    public class VideoRoyaltyInfoEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long VideoId { get; set; }
        [DataMember]
        public int VideoType { get; set; }
        [DataMember]
        public string QualityFactor { get; set; }
        [DataMember]
        public string ContributionFactor { get; set; }
        [DataMember]
        public string RiskFactor { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; } 
         [DataMember]
        public DateTime LastModifyDate { get; set; }
        [DataMember]
        public string LastModifyBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
