﻿using System;
using System.Collections.Generic;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.DAntri.MainDal
{
    public abstract class VideoRoyaltyAuthorDalBase
    {
        #region Function GET

        public List<VideoRoyaltyAuthorEntity> GetByVideoId(long id)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_GetByVideoId";
            try
            {
                List<VideoRoyaltyAuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", id);
                data = _db.GetList<VideoRoyaltyAuthorEntity>(cmd) ?? new List<VideoRoyaltyAuthorEntity>();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Insert(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoRoyaltyAuthor.VideoId);
                _db.AddParameter(cmd, "PenName", videoRoyaltyAuthor.PenName);
                _db.AddParameter(cmd, "Rate", videoRoyaltyAuthor.Rate);
                _db.AddParameter(cmd, "Note", videoRoyaltyAuthor.Note);
                _db.AddParameter(cmd, "CreatedBy", videoRoyaltyAuthor.CreatedBy);
                _db.AddParameter(cmd, "Status", videoRoyaltyAuthor.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(VideoRoyaltyAuthorEntity videoRoyaltyAuthor)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoRoyaltyAuthor.Id);
                _db.AddParameter(cmd, "VideoId", videoRoyaltyAuthor.VideoId);
                _db.AddParameter(cmd, "PenName", videoRoyaltyAuthor.PenName);
                _db.AddParameter(cmd, "Rate", videoRoyaltyAuthor.Rate);
                _db.AddParameter(cmd, "Note", videoRoyaltyAuthor.Note);
                _db.AddParameter(cmd, "Status", videoRoyaltyAuthor.Status);
                _db.AddParameter(cmd, "LastModifyBy", videoRoyaltyAuthor.LastModifyBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByVideoId(long tagId)
        {
            const string commandText = "CMS_VideoRoyaltyAuthor_DeleteByVideoID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected VideoRoyaltyAuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
