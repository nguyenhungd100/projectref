﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.Dantri.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="ExternalCmsDb"/> class that 
    /// represents a connection to the <c>ExternalCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the ExternalCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ExternalCmsDbBase : MainDbBase
    {
        #region Store procedures

        #region VTVLiveShow Register

        private CommentUserDal _commentUserMainDal;
        public CommentUserDal CommentUserMainDal
        {
            get { return _commentUserMainDal ?? (_commentUserMainDal = new CommentUserDal((ExternalCmsDb)this)); }
        }

        #endregion

        #endregion

        #region Constructors

        protected ExternalCmsDbBase()
        {
        }
        protected ExternalCmsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}