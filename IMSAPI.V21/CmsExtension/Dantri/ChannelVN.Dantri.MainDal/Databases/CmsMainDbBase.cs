﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.DAntri.MainDal;

namespace ChannelVN.Dantri.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region NewsRoyaltyAuthor
        private NewsRoyaltyAuthorDal _newsRoyaltyAuthorMainDal;
        public NewsRoyaltyAuthorDalBase NewsRoyaltyAuthorMainDal
        {
            get { return _newsRoyaltyAuthorMainDal ?? (_newsRoyaltyAuthorMainDal = new NewsRoyaltyAuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsRoyaltyInfo
        private NewsRoyaltyInfoDal _newsRoyaltyInfoMainDal;
        public NewsRoyaltyInfoDalBase NewsRoyaltyInfoMainDal
        {
            get { return _newsRoyaltyInfoMainDal ?? (_newsRoyaltyInfoMainDal = new NewsRoyaltyInfoDal((CmsMainDb)this)); }
        }
        #endregion

        #region VideoRoyaltyAuthor
        private VideoRoyaltyAuthorDal _videoRoyaltyAuthorMainDal;
        public VideoRoyaltyAuthorDalBase VideoRoyaltyAuthorMainDal
        {
            get { return _videoRoyaltyAuthorMainDal ?? (_videoRoyaltyAuthorMainDal = new VideoRoyaltyAuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #region VideoRoyaltyInfo
        private VideoRoyaltyInfoDal _videoRoyaltyInfoMainDal;
        public VideoRoyaltyInfoDalBase VideoRoyaltyInfoMainDal
        {
            get { return _videoRoyaltyInfoMainDal ?? (_videoRoyaltyInfoMainDal = new VideoRoyaltyInfoDal((CmsMainDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}