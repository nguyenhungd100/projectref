﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.DAntri.MainDal
{
    public abstract class NewsRoyaltyAuthorDalBase
    {
        #region Function GET

        public List<NewsRoyaltyAuthorEntity> GetByNewsId(long id)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_GetByNewsId";
            try
            {
                List<NewsRoyaltyAuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                data = _db.GetList<NewsRoyaltyAuthorEntity>(cmd) ?? new List<NewsRoyaltyAuthorEntity>();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Insert(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsRoyaltyAuthor.NewsId);
                _db.AddParameter(cmd, "PenName", NewsRoyaltyAuthor.PenName);
                _db.AddParameter(cmd, "Rate", NewsRoyaltyAuthor.Rate);
                _db.AddParameter(cmd, "Note", NewsRoyaltyAuthor.Note);
                _db.AddParameter(cmd, "CreatedBy", NewsRoyaltyAuthor.CreatedBy);
                _db.AddParameter(cmd, "Status", NewsRoyaltyAuthor.Status);
                _db.AddParameter(cmd, "AuthorType", NewsRoyaltyAuthor.AuthorType);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", NewsRoyaltyAuthor.Id);
                _db.AddParameter(cmd, "NewsId", NewsRoyaltyAuthor.NewsId);
                _db.AddParameter(cmd, "PenName", NewsRoyaltyAuthor.PenName);
                _db.AddParameter(cmd, "Rate", NewsRoyaltyAuthor.Rate);
                _db.AddParameter(cmd, "Note", NewsRoyaltyAuthor.Note);
                _db.AddParameter(cmd, "Status", NewsRoyaltyAuthor.Status);
                _db.AddParameter(cmd, "LastModifyBy", NewsRoyaltyAuthor.LastModifyBy);
                _db.AddParameter(cmd, "AuthorType", NewsRoyaltyAuthor.AuthorType);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByNewsId(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_DeleteByNewsID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsRoyaltyAuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
