﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.DAntri.MainDal
{
    public abstract class NewsRoyaltyInfoDalBase
    {
        #region Function GET

        public NewsRoyaltyInfoEntity GetByNewsId(long id)
        {
            const string commandText = "CMS_NewsRoyaltyInfo_GetByNewsId";
            try
            {
                NewsRoyaltyInfoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                data = _db.Get<NewsRoyaltyInfoEntity>(cmd) ?? new NewsRoyaltyInfoEntity();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Insert(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            const string commandText = "CMS_NewsRoyaltyInfo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsRoyaltyInfo.NewsId);
                _db.AddParameter(cmd, "PictureType", NewsRoyaltyInfo.PictureType);
                _db.AddParameter(cmd, "QualityFactor", NewsRoyaltyInfo.QualityFactor);
                _db.AddParameter(cmd, "ContributionFactor", NewsRoyaltyInfo.ContributionFactor);
                _db.AddParameter(cmd, "RiskFactor", NewsRoyaltyInfo.RiskFactor);
                _db.AddParameter(cmd, "CreatedBy", NewsRoyaltyInfo.CreatedBy);
                _db.AddParameter(cmd, "NumberOfPicture", NewsRoyaltyInfo.NumberOfPicture);
                _db.AddParameter(cmd, "Status", NewsRoyaltyInfo.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(NewsRoyaltyInfoEntity NewsRoyaltyInfo)
        {
            const string commandText = "CMS_NewsRoyaltyInfo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", NewsRoyaltyInfo.Id);
                _db.AddParameter(cmd, "NewsId", NewsRoyaltyInfo.NewsId);
                _db.AddParameter(cmd, "PictureType", NewsRoyaltyInfo.PictureType);
                _db.AddParameter(cmd, "QualityFactor", NewsRoyaltyInfo.QualityFactor);
                _db.AddParameter(cmd, "ContributionFactor", NewsRoyaltyInfo.ContributionFactor);
                _db.AddParameter(cmd, "RiskFactor", NewsRoyaltyInfo.RiskFactor);
                _db.AddParameter(cmd, "NumberOfPicture", NewsRoyaltyInfo.NumberOfPicture);
                _db.AddParameter(cmd, "Status", NewsRoyaltyInfo.Status);
                _db.AddParameter(cmd, "LastModifyBy", NewsRoyaltyInfo.LastModifyBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyInfo_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByNewsId(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyInfo_DeleteByNewsID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsRoyaltyInfoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
