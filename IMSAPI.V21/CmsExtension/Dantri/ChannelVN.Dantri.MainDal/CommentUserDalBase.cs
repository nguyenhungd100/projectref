﻿using System.Data;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.Dantri.MainDal
{
    public abstract class CommentUserDalBase
    {
        #region Function GET

        public List<CommentUserEntity> Search(string userName, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_CommentUser_Search";
            try
            {
                List<CommentUserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                data = _db.GetList<CommentUserEntity>(cmd) ?? new List<CommentUserEntity>();
                totalRow = Convert.ToInt32(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public CommentUserEntity GetById(long id)
        {
            const string commandText = "CMS_CommentUser_GetById";
            try
            {
                CommentUserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", id);
                data = _db.Get<CommentUserEntity>(cmd) ?? new CommentUserEntity();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Insert(NewsRoyaltyAuthorEntity NewsRoyaltyAuthor)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsRoyaltyAuthor.NewsId);
                _db.AddParameter(cmd, "PenName", NewsRoyaltyAuthor.PenName);
                _db.AddParameter(cmd, "Rate", NewsRoyaltyAuthor.Rate);
                _db.AddParameter(cmd, "Note", NewsRoyaltyAuthor.Note);
                _db.AddParameter(cmd, "CreatedBy", NewsRoyaltyAuthor.CreatedBy);
                _db.AddParameter(cmd, "Status", NewsRoyaltyAuthor.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(CommentUserEntity commentUser)
        {
            const string commandText = "CMS_CommentUser_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", commentUser.Id);
                _db.AddParameter(cmd, "Email", commentUser.Email);
                _db.AddParameter(cmd, "FacebookId", commentUser.FacebookId);
                _db.AddParameter(cmd, "GoogleId", commentUser.GoogleId);
                _db.AddParameter(cmd, "Name", commentUser.Name);
                _db.AddParameter(cmd, "Avatar", commentUser.Avatar);
                _db.AddParameter(cmd, "Status", commentUser.Status);
                _db.AddParameter(cmd, "Sex", commentUser.Sex);
                _db.AddParameter(cmd, "BirthDay", commentUser.BirthDay);
                _db.AddParameter(cmd, "Address", commentUser.Address);
                _db.AddParameter(cmd, "Job", commentUser.Job);
                _db.AddParameter(cmd, "Phone", commentUser.Phone);
                _db.AddParameter(cmd, "IdentityNumber", commentUser.IdentityNumber);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(CommentUserEntity commentUser)
        {
            const string commandText = "CMS_CommentUser_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", commentUser.Id);
                _db.AddParameter(cmd, "Status", commentUser.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByNewsId(long tagId)
        {
            const string commandText = "CMS_NewsRoyaltyAuthor_DeleteByNewsID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly ExternalCmsDb _db;

        protected CommentUserDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
