﻿using System;
using ChannelVN.Dantri.Entity;
using ChannelVN.Dantri.MainDal.Databases;

namespace ChannelVN.DAntri.MainDal
{
    public abstract class VideoRoyaltyInfoDalBase
    {
        #region Function GET

        public VideoRoyaltyInfoEntity GetByVideoId(long id)
        {
            const string commandText = "CMS_VideoRoyaltyInfo_GetByVideoId";
            try
            {
                VideoRoyaltyInfoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", id);
                data = _db.Get<VideoRoyaltyInfoEntity>(cmd) ?? new VideoRoyaltyInfoEntity();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Function SET

        public bool Insert(VideoRoyaltyInfoEntity videoRoyaltyInfo)
        {
            const string commandText = "CMS_VideoRoyaltyInfo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoRoyaltyInfo.VideoId);
                _db.AddParameter(cmd, "VideoType", videoRoyaltyInfo.VideoType);
                _db.AddParameter(cmd, "QualityFactor", videoRoyaltyInfo.QualityFactor);
                _db.AddParameter(cmd, "ContributionFactor", videoRoyaltyInfo.ContributionFactor);
                _db.AddParameter(cmd, "RiskFactor", videoRoyaltyInfo.RiskFactor);
                _db.AddParameter(cmd, "Note", videoRoyaltyInfo.Note);
                _db.AddParameter(cmd, "CreatedBy", videoRoyaltyInfo.CreatedBy);
                _db.AddParameter(cmd, "Status", videoRoyaltyInfo.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(VideoRoyaltyInfoEntity videoRoyaltyInfo)
        {
            const string commandText = "CMS_VideoRoyaltyInfo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoRoyaltyInfo.Id);
                _db.AddParameter(cmd, "VideoId", videoRoyaltyInfo.VideoId);
                _db.AddParameter(cmd, "VideoType", videoRoyaltyInfo.VideoType);
                _db.AddParameter(cmd, "QualityFactor", videoRoyaltyInfo.QualityFactor);
                _db.AddParameter(cmd, "ContributionFactor", videoRoyaltyInfo.ContributionFactor);
                _db.AddParameter(cmd, "RiskFactor", videoRoyaltyInfo.RiskFactor);
                _db.AddParameter(cmd, "Note", videoRoyaltyInfo.Note);
                _db.AddParameter(cmd, "Status", videoRoyaltyInfo.Status);
                _db.AddParameter(cmd, "LastModifyBy", videoRoyaltyInfo.LastModifyBy);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_VideoRoyaltyInfo_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByVideoId(long tagId)
        {
            const string commandText = "CMS_VideoRoyaltyInfo_DeleteByVideoID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected VideoRoyaltyInfoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
