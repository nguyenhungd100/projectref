﻿using ChannelVN.CalendarManager.Entity;
using ChannelVN.CalendarManager.MainDal.Databases;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CalendarManager.MainDal
{
    public abstract class CalendarDalBase
    {
        #region function GET
        public CalendarEntity GetDataByid(int id)
        {
            const string commandText = "CMS_Calendar_GetById";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Id", id);
                //_db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.Get<CalendarEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CalendarInviteEntity> GetInviteByCalendarId(int calendarId)
        {
            const string commandText = "CMS_CalendarInvite_GetByCalendarId";
            try
            {
                List<CalendarInviteEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CalendarId", calendarId);
                data = _db.GetList<CalendarInviteEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CalendarEntity> GetDataByUser(int month, string user, int type, int dateType)
        {
            const string commandText = "CMS_Calendar_GetByUser";
            try
            {
                List<CalendarEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Month", month);
                _db.AddParameter(cmd, "User", user);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "dateType", dateType);
                data = _db.GetList<CalendarEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CalendarEntity> GetDataByDate(DateTime date, string user, int type)
        {
            const string commandText = "CMS_Calendar_GetByDate";
            try
            {
                List<CalendarEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Date", date);
                _db.AddParameter(cmd, "User", user);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<CalendarEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CalendarEntity> GetDataToNotify(DateTime inputDate, string user)
        {
            const string commandText = "CMS_Calendar_GetDataNotify";
            try
            {
                List<CalendarEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InputDate", inputDate);
                _db.AddParameter(cmd, "User", user);
                data = _db.GetList<CalendarEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CalendarLogEntity> GetLogCalendar(DateTime inputDate, string user)
        {
            const string commandText = "CMS_CalendarLog_GetLogCalendar";
            try
            {
                List<CalendarLogEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InputDate", inputDate);
                _db.AddParameter(cmd, "User", user);
                data = _db.GetList<CalendarLogEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function SET
        public bool Insert(CalendarEntity calendarInfo, ref int id)
        {
            const string commandText = "CMS_Calendar_Insert";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", calendarInfo.Title);
                _db.AddParameter(cmd, "Description", calendarInfo.Description);
                _db.AddParameter(cmd, "StartDate", calendarInfo.StartDate);
                _db.AddParameter(cmd, "EndDate", calendarInfo.EndDate);
                _db.AddParameter(cmd, "IsAllDay", calendarInfo.IsAllDayEvent);
                _db.AddParameter(cmd, "AlterBeforeMinute", calendarInfo.AlertBeforeMinute);
                _db.AddParameter(cmd, "Status", calendarInfo.Status);
                _db.AddParameter(cmd, "AlertType", calendarInfo.AlertType);
                _db.AddParameter(cmd, "CreatedBy", calendarInfo.CreatedBy);
                _db.AddParameter(cmd, "ListInviteUser", calendarInfo.ListInviteUser);
                _db.AddParameter(cmd, "ListRequiredUser", calendarInfo.ListRequiredUser);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(CalendarEntity calendarInfo)
        {
            const string commandText = "CMS_Calendar_Update";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", calendarInfo.Id);
                _db.AddParameter(cmd, "Title", calendarInfo.Title);
                _db.AddParameter(cmd, "Description", calendarInfo.Description);
                _db.AddParameter(cmd, "StartDate", calendarInfo.StartDate);
                _db.AddParameter(cmd, "EndDate", calendarInfo.EndDate);
                _db.AddParameter(cmd, "IsAllDay", calendarInfo.IsAllDayEvent);
                _db.AddParameter(cmd, "AlterBeforeMinute", calendarInfo.AlertBeforeMinute);
                _db.AddParameter(cmd, "Status", calendarInfo.Status);
                _db.AddParameter(cmd, "AlertType", calendarInfo.AlertType);
                _db.AddParameter(cmd, "CreatedBy", calendarInfo.CreatedBy);
                _db.AddParameter(cmd, "ListInviteUser", calendarInfo.ListInviteUser);
                _db.AddParameter(cmd, "ListRequiredUser", calendarInfo.ListRequiredUser);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_Calendar_Delete";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertInvite(CalendarInviteEntity inviteInfo, ref int id)
        {
            const string commandText = "CMS_CalendarInvite_Insert";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", inviteInfo.Id);
                _db.AddParameter(cmd, "CalendarID", inviteInfo.Calendar_Id);
                _db.AddParameter(cmd, "InviteUser", inviteInfo.InviteUser);
                _db.AddParameter(cmd, "Status", inviteInfo.Status);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateInviteStatus(string user, int status, int calendarId, string reason)
        {
            const string commandText = "CMS_CalendarInvite_UpdateStatus";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "User", user);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "CalendarId", calendarId);
                _db.AddParameter(cmd, "reason", reason);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatusEvent(int calendarId, int status)
        {
            const string commandText = "CMS_Calendar_UpdateStatus";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "CalendarId", calendarId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteInvite(int calendarId, string userInvite)
        {
            const string commandText = "CMS_CalendarInvite_Delete";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "user", userInvite);
                _db.AddParameter(cmd, "CalendarId", calendarId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertLog(CalendarLogEntity logInfo, ref int id)
        {
            const string commandText = "CMS_CalendarLog_Insert";
            try
            {
                CalendarEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "CalendarId", logInfo.CalendarId);
                _db.AddParameter(cmd, "ActionLog", logInfo.ActionLog);
                _db.AddParameter(cmd, "User", logInfo.User);
                _db.AddParameter(cmd, "LogDate", logInfo.LogDate);
                var result = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        /// <summary>
        /// Chuyển đổi DAL cho Calendar
        /// minhduongvan
        /// 2014/01/08
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected CalendarDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
