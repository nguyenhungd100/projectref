﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CalendarManager.Entity;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CalendarManager.Dal.Common;
using ChannelVN.CalendarManager.MainDal.Databases;

namespace ChannelVN.CalendarManager.Dal
{
    public class CalendarDal
    {
        /// <summary>
        /// Insert Data to Calendar table
        /// </summary>
        /// <param name="calendarInfo"></param>
        /// <param name="id"></param>
        /// <returns>true/fale</returns>
        public static bool Insert(CalendarEntity calendarInfo, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.Insert(calendarInfo, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Update calendar Info
        /// </summary>
        /// <param name="calendarInfo"></param>
        /// <returns>true/fale</returns>
        public static bool Update(CalendarEntity calendarInfo)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.Update(calendarInfo);
            }
            return returnValue;
        }
        /// <summary>
        /// Delete calendar by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.Delete(id);
            }
            return returnValue;
        }
        /// <summary>
        /// Insert invite user info 
        /// </summary>
        /// <param name="inviteInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool InsertInvite(CalendarInviteEntity inviteInfo, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.InsertInvite(inviteInfo, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Update calendar invite status
        /// </summary>
        /// <param name="calendarInfo"></param>
        /// <returns>true/fale</returns>
        public static bool UpdateInviteStatus(string user, int status, int calendarId, string reason)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.UpdateInviteStatus(user, status, calendarId, reason);
            }
            return returnValue;
        }
        /// <summary>
        /// Cập nhật trạng thái notify cho event. Khi event đã thông báo cho user cần cập nhật lại trạng thái này
        /// </summary>
        /// <param name="calendarId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool UpdateStatusEvent(int calendarId, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.UpdateStatusEvent(calendarId, status);
            }
            return returnValue;
        }
        /// <summary>
        /// Delete invite user by calendarId
        /// </summary>
        /// <param name="calendarId"></param>
        /// <returns></returns>
        public static bool DeleteInvite(int calendarId, string userInvite)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.DeleteInvite(calendarId, userInvite);
            }
            return returnValue;
        }
        /// <summary>
        /// Get calendar info by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static CalendarEntity GetDataByid(int id)
        {
            CalendarEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetDataByid(id);
            }
            return returnValue;
        }
        /// <summary>
        /// Get invite user info by calendarId
        /// </summary>
        /// <param name="calendarId"></param>
        /// <returns>list invite user</returns>
        public static List<CalendarInviteEntity> GetInviteByCalendarId(int calendarId)
        {
            List<CalendarInviteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetInviteByCalendarId(calendarId);
            }
            return returnValue;
        }
        /// <summary>
        /// List calendar by user
        /// </summary>
        /// <param name="user">CreatedBy or InvitedUser</param>
        /// <param name="type"></param>
        /// <param name="dateType">1. lấy dữ liệu theo tháng; 2. lấy dữ liệu theo ngày trong tháng</param>
        /// <returns></returns>
        public static List<CalendarEntity> GetDataByUser(int month, string user, int type, int dateType)
        {
            List<CalendarEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetDataByUser(month, user, type, dateType);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="month"></param>
        /// <param name="user"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<CalendarEntity> GetDataByDate(DateTime date, string user, int type)
        {
            List<CalendarEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetDataByDate(date, user, type);
            }
            return returnValue;
        }
        /// <summary>
        /// Get list calendar info by input date
        /// </summary>
        /// <param name="inputDate"></param>
        /// <returns>list calendar</returns>
        public static List<CalendarEntity> GetDataToNotify(DateTime inputDate, string user)
        {
            List<CalendarEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetDataToNotify(inputDate, user);
            }
            return returnValue;
        }
        /// <summary>
        /// process log action calendar by User
        /// </summary>
        /// <param name="logInfo"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool InsertLog(CalendarLogEntity logInfo, ref int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.InsertLog(logInfo, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputDate"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<CalendarLogEntity> GetLogCalendar(DateTime inputDate, string user)
        {
            List<CalendarLogEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CalendarContentMainDal.GetLogCalendar(inputDate, user);
            }
            return returnValue;
        }
    }
}
