﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CalendarManager.Entity
{

    [DataContract]
    public enum EnumStatusType
    {
        [EnumMember]
        Invite = 0,
        [EnumMember]
        Accept = 1,
        [EnumMember]
        Reject = 2,
        [EnumMember]
        Required = 3,
    }
    [DataContract]
    public class CalendarInviteEntity: EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Calendar_Id { get; set; }
        [DataMember]
        public string InviteUser { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int StatusConfirm { get; set; }
        [DataMember]
        public string ReasonReject { get; set; }
    }
}
