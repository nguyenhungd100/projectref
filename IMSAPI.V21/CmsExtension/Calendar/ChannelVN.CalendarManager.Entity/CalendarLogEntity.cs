﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CalendarManager.Entity
{
    [DataContract]
    public class CalendarLogEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int CalendarId { get; set; }
        [DataMember]
        public string User { get; set; }
        [DataMember]
        public string ActionLog { get; set; }
        [DataMember]
        public DateTime LogDate { get; set; }
    }
}
