﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CalendarManager.Entity
{
    [DataContract]
    public enum EnumCalendarType
    {
        [EnumMember]
        normal = 0,
        [EnumMember]
        viewed = 1
    }

    [DataContract]
    public enum EnumCalendarInviteType
    {
        [EnumMember]
        Accept = 1,
        [EnumMember]
        Reject = 2,
        [EnumMember]
        Required = 3,
        [EnumMember]
        normal = 0
    }

    [DataContract]
    public enum EnumAlterType
    {
        [EnumMember]
        Email = 1,
        [EnumMember]
        Sms = 2,
        [EnumMember]
        All = 3,
        [EnumMember]
        Popup = 0
    }
    [DataContract]
    public enum EnumActionStatusInvite
    {
        [EnumMember]
        normal = 0,
        [EnumMember]
        deleted = 1
    }
    [DataContract]
    public class CalendarEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public bool IsAllDayEvent { get; set; }
        [DataMember]
        public int AlertBeforeMinute { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int AlertType { get; set; }

        [DataMember]
        public string ListInviteUser { get; set; }
        [DataMember]
        public string ListRequiredUser { get; set; }
        [DataMember]
        public int StatusInvite { get; set; }
        [DataMember]
        public int StatusConfirm { get; set; }

    }
    [DataContract]
    public class CalendarDetailEntity : EntityBase
    {
        [DataMember]
        public CalendarEntity InfoCalendar { get; set; }
        [DataMember]
        public List<CalendarInviteEntity> InviteUser { get; set; }
        [DataMember]
        public List<CalendarInviteEntity> RequiredUser { get; set; }
        [DataMember]
        public List<CalendarInviteEntity> RejectUser { get; set; }
    }
}
