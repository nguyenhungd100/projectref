﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.BO;
using ChannelVN.CalendarManager.Entity.ErrorCode;
using ChannelVN.CalendarManager.Entity;
using ChannelVN.CalendarManager.Dal;

namespace ChannelVN.CalendarManager.Bo
{
    public class CalendarBo
    {
        public static ErrorMapping.ErrorCodes InsertCalendar(CalendarEntity info, ref int newId)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = CalendarDal.Insert(info, ref newId);
            if (createSuccess && newId > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateCalendar(CalendarEntity info)
        {
            return CalendarDal.Update(info) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteCalendar(int id)
        {
            return CalendarDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteCalendarInvite(int calendarId, string user)
        {
            return CalendarDal.DeleteInvite(calendarId, user) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateInviteStatus(string user, int status, int calendarId, string reason)
        {
            return CalendarDal.UpdateInviteStatus(user, status, calendarId,reason) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateStatusEvent(int calendarId, int status)
        {
            return CalendarDal.UpdateStatusEvent(calendarId,status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static CalendarEntity GetCalendarById(int id)
        {
            return CalendarDal.GetDataByid(id);
        }

        public static CalendarDetailEntity GetCalendarInviteById(int calendarId)
        {
            var calendarInfo = CalendarDal.GetDataByid(calendarId);
            if (calendarInfo != null)
            {
                var calendarDetail = new CalendarDetailEntity
                {
                    InfoCalendar = calendarInfo,
                    InviteUser = new List<CalendarInviteEntity>(),
                    RequiredUser = new List<CalendarInviteEntity>(),
                    RejectUser = new List<CalendarInviteEntity>()
                };
                var inviteUser = CalendarDal.GetInviteByCalendarId(calendarId);
                foreach (var item in inviteUser)
                {
                    if (item.StatusConfirm == (int)EnumCalendarInviteType.Reject)
                    {
                        calendarDetail.RejectUser.Add(item);
                    }
                    if (item.Status != (int)EnumCalendarInviteType.Required)
                        calendarDetail.InviteUser.Add(item);
                    else
                        calendarDetail.RequiredUser.Add(item);
                }
                return calendarDetail;
            }
            return null;
        }
        /// <summary>
        /// List calendar by user
        /// </summary>
        /// <param name="user">1. CreatedBy or 2. InvitedUser</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<CalendarEntity> GetDataByUser(int month, string user, int type, int dateType)
        {
            return CalendarDal.GetDataByUser(month, user, type, dateType);
        }
        public static List<CalendarEntity> GetDataByDate(DateTime date, string user, int type)
        {
            return CalendarDal.GetDataByDate(date, user, type);
        }
        /// <summary>
        /// List calendar data for notification
        /// </summary>
        /// <param name="inputDate"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<CalendarEntity> GetDataToNotify(DateTime inputDate, string user)
        {
            return CalendarDal.GetDataToNotify(inputDate, user);
        }

        public static ErrorMapping.ErrorCodes ProcessCalendarInput(CalendarEntity calendarInfo, List<string> inviteUser, List<string> requiredUser, ref int id)
        {
            #region mapping listInvite to calendar info
            calendarInfo.ListInviteUser = "";
            foreach (var inviteItem in inviteUser)
            {
                calendarInfo.ListInviteUser += ";" + inviteItem;
            }
            calendarInfo.ListRequiredUser = "";
            foreach (var itemRequired in requiredUser)
            {
                calendarInfo.ListRequiredUser += ";" + itemRequired;
            }
            #endregion

            var updateResult = ErrorMapping.ErrorCodes.BusinessError;
            if (calendarInfo.Id > 0)
            {
                ///GOTO
                ///Update calendarInfo
                ///
                var result = CalendarDal.Update(calendarInfo);
                if (result)
                {
                    id = calendarInfo.Id;
                    updateResult = ErrorMapping.ErrorCodes.Success;
                }
            }
            else
            {
                ///GOTO
                ///Insert calendar info
                ///
                var result = CalendarDal.Insert(calendarInfo, ref id);
                if (result && id > 0)
                    updateResult = ErrorMapping.ErrorCodes.Success;
            }

            return updateResult;
        }

        public static ErrorMapping.ErrorCodes InsertLog(CalendarLogEntity logInfo, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = CalendarDal.InsertLog(logInfo, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
    }
}
