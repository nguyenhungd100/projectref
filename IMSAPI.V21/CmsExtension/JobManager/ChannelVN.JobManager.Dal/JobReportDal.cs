﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.Dal
{
    public class JobReportDal
    {
        public static List<JobReportEntity> Search(int jobId, int parentId, ref int totalRow)
        {
            List<JobReportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobReportMainDal.Search(jobId, parentId, ref totalRow);
            }
            return returnValue;
        }

        public static bool Insert(JobReportEntity report, ref int newReportId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobReportMainDal.Insert(report, ref newReportId);
            }
            return returnValue;
        }
    }
}
