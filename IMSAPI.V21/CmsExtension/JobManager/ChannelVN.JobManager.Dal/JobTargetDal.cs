﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.Dal
{
    public class JobTargetDal
    {
        public static bool Update(int jobId, int targetType, int targetValue)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobTargetMainDal.Update(jobId, targetType, targetValue);
            }
            return returnValue;
        }
        public static bool DeleteByJobId(int jobId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobTargetMainDal.DeleteByJobId(jobId);
            }
            return returnValue;
        }
        public static List<JobTargetEntity> GetByJobId(int jobId)
        {
            List<JobTargetEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobTargetMainDal.GetByJobId(jobId);
            }
            return returnValue;
        }
    }
}
