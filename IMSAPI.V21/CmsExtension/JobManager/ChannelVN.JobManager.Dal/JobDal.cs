﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.Dal
{
    public class JobDal
    {
        public static bool Insert(JobEntity job, ref int newJobId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.Insert(job, ref newJobId);
            }
            return returnValue;
        }

        public static bool Update(JobEntity job)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.Update(job);
            }
            return returnValue;
        }

        public static bool UpdateStatus(JobEntity job)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.UpdateStatus(job);
            }
            return returnValue;
        }

        public static bool Delete(int jobId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.Delete(jobId);
            }
            return returnValue;
        }

        public static JobEntity GetJobById(int jobId)
        {
            JobEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.GetJobById(jobId);
            }
            return returnValue;
        }

        public static List<JobEntity> Search(string keyword, int jobType, int jobParentId, string createdBy, string userAssigned, int status, int zoneId, int isHightLight, int isUnreadComment, int pageIndex, int pageSize, ref int totalRow)
        {
            List<JobEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobMainDal.Search(keyword, jobType, jobParentId, createdBy, userAssigned, status, zoneId, isHightLight, isUnreadComment, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
