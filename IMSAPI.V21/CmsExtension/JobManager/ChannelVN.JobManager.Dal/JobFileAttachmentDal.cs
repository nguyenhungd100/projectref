﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.Dal
{
    public class JobFileAttachmentDal
    {
        public static bool Insert(JobFileAttachmentEntity jobFileAttachment)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobFileAttachmentMainDal.Insert(jobFileAttachment);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobFileAttachmentMainDal.Delete(id);
            }
            return returnValue;
        }
        public static bool DeleteByJobId(int jobId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobFileAttachmentMainDal.DeleteByJobId(jobId);
            }
            return returnValue;
        }
        public static List<JobFileAttachmentEntity> GetByJobId(int jobId)
        {
            List<JobFileAttachmentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobFileAttachmentMainDal.GetByJobId(jobId);
            }
            return returnValue;
        }
        public static List<JobFileAttachmentEntity> GetByJobReportId(int jobReportId)
        {
            List<JobFileAttachmentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobFileAttachmentMainDal.GetByJobReportId(jobReportId);
            }
            return returnValue;
        }
    }
}
