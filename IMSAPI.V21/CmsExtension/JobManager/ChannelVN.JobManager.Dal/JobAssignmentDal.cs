﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.Dal
{
    public class JobAssignmentDal
    {
        public static bool SetUnread(int jobId, int jobReportId, string userAssigned, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.SetUnread(jobId, jobReportId, userAssigned, status);
            }
            return returnValue;
        }
        public static int GetUnread(int jobId, string userAssigned)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.GetUnread(jobId, userAssigned);
            }
            return returnValue;
        }
        public static int GetUnread(int jobId, int jobReportId, string userAssigned)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.GetUnread(jobId, jobReportId, userAssigned);
            }
            return returnValue;
        }
        public static bool GetHightlight(int jobId, string userAssigned)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.GetHightlight(jobId, userAssigned);
            }
            return returnValue;
        }
        public static bool HightLight(int jobId, string userAssigned)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.HightLight(jobId, userAssigned);
            }
            return returnValue;
        }
        public static List<JobAssignmentEntity> GetByJobId(int jobId)
        {
            List<JobAssignmentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.JobAssignmentMainDal.GetByJobId(jobId);
            }
            return returnValue;
        }
    }
}
