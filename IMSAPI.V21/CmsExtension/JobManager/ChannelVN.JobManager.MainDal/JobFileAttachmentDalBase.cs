﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.MainDal
{
    public abstract class JobFileAttachmentDalBase
    {
        public bool Insert(JobFileAttachmentEntity jobFileAttachment)
        {
            const string commandText = "CMS_JobFileAttachment_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobFileAttachment.JobId);
                _db.AddParameter(cmd, "JobReportId", jobFileAttachment.JobReportId);
                _db.AddParameter(cmd, "FileName", jobFileAttachment.FileName);
                _db.AddParameter(cmd, "FileUrl", jobFileAttachment.FileUrl);
                _db.AddParameter(cmd, "FileSize", jobFileAttachment.FileSize);
                _db.AddParameter(cmd, "Avatar", jobFileAttachment.Avatar);
                _db.AddParameter(cmd, "Note", jobFileAttachment.Note);
                _db.AddParameter(cmd, "CreatedBy", jobFileAttachment.CreatedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_JobFileAttachment_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByJobId(int jobId)
        {
            const string commandText = "CMS_JobFileAttachment_DeleteByJobId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<JobFileAttachmentEntity> GetByJobId(int jobId)
        {
            const string commandText = "CMS_JobFileAttachment_GetByJobId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                var numberOfRow = _db.GetList<JobFileAttachmentEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<JobFileAttachmentEntity> GetByJobReportId(int jobReportId)
        {
            const string commandText = "CMS_JobFileAttachment_GetByJobReportId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobReportId", jobReportId);
                var numberOfRow = _db.GetList<JobFileAttachmentEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected JobFileAttachmentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
