﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.MainDal
{
    public abstract class JobReportDalBase
    {
        public List<JobReportEntity> Search(int jobId, int parentId, ref int totalRow)
        {
            const string commandText = "CMS_Job_Report_List";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "ParentId", parentId);
                var numberOfRow = _db.GetList<JobReportEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(JobReportEntity report, ref int newReportId)
        {
            const string commandText = "CMS_Job_Insert_Report";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newReportId, ParameterDirection.Output);
                _db.AddParameter(cmd, "JobId", report.JobId);
                _db.AddParameter(cmd, "ParentId", report.ParentId);
                _db.AddParameter(cmd, "ReportContent", report.ReportContent);
                _db.AddParameter(cmd, "PercentComplete", report.PercentComplete);
                _db.AddParameter(cmd, "CreatedBy", report.CreatedBy);
                _db.AddParameter(cmd, "ListReceiveUser", report.ListReceiveUser);
                var numberOfRow = cmd.ExecuteNonQuery();
                newReportId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected JobReportDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
