﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.MainDal
{
    public abstract class JobAssignmentDalBase
    {
        public bool SetUnread(int jobId, int jobReportId, string userAssigned, int status)
        {
            const string commandText = "CMS_JobAssigment_SetUnreadComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "JobReportId", jobReportId);
                _db.AddParameter(cmd, "User", userAssigned);
                _db.AddParameter(cmd, "status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetUnread(int jobId, string userAssigned)
        {
            const string commandText = "CMS_JobAssigment_GetUnreadComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "User", userAssigned);
                var numberOfRow = cmd.ExecuteScalar();

                return Utility.ConvertToInt(numberOfRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetUnread(int jobId, int jobReportId, string userAssigned)
        {
            const string commandText = "CMS_JobAssigment_GetUnreadCommentByjobReport";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "JobReportId", jobReportId);
                _db.AddParameter(cmd, "User", userAssigned);
                var numberOfRow = cmd.ExecuteScalar();

                return Utility.ConvertToInt(numberOfRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool GetHightlight(int jobId, string userAssigned)
        {
            const string commandText = "CMS_JobAssigment_GetHightLight";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "User", userAssigned);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool HightLight(int jobId, string userAssigned)
        {
            const string commandText = "CMS_JobAssigment_SetHightLight";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                _db.AddParameter(cmd, "User", userAssigned);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<JobAssignmentEntity> GetByJobId(int jobId)
        {
            const string commandText = "CMS_JobAssignment_GetByJobId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", jobId);
                var numberOfRow = _db.GetList<JobAssignmentEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected JobAssignmentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
