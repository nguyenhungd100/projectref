﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.MainDal.Common;
using ChannelVN.JobManager.MainDal.Databases;

namespace ChannelVN.JobManager.MainDal
{
    public abstract class JobDalBase
    {
        public bool Insert(JobEntity job, ref int newJobId)
        {
            const string commandText = "CMS_Job_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newJobId, ParameterDirection.Output);
                _db.AddParameter(cmd, "JobParentId", job.JobParentId);
                _db.AddParameter(cmd, "ZoneId", job.ZoneId);
                _db.AddParameter(cmd, "Name", job.Name);
                _db.AddParameter(cmd, "JobType", job.JobType);
                _db.AddParameter(cmd, "JobDescription", job.JobDescription);
                _db.AddParameter(cmd, "JobNote", job.JobNote);
                if (job.StartedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartedDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartedDate", job.StartedDate);
                if (job.Deadline <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "Deadline", DBNull.Value);
                else _db.AddParameter(cmd, "Deadline", job.Deadline);
                if (job.FinishedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FinishedDate", DBNull.Value);
                else _db.AddParameter(cmd, "FinishedDate", job.FinishedDate);
                _db.AddParameter(cmd, "PercentComplete", job.PercentComplete);
                _db.AddParameter(cmd, "CreatedBy", job.CreatedBy);
                _db.AddParameter(cmd, "Status", job.Status == 0 ? 1 : job.Status);

                _db.AddParameter(cmd, "ListProcessUser", job.ListProcessUser);
                _db.AddParameter(cmd, "ListFollowUser", job.ListFollowUser);
                var numberOfRow = cmd.ExecuteNonQuery();
                newJobId = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(JobEntity job)
        {
            const string commandText = "CMS_Job_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobParentId", job.JobParentId);
                _db.AddParameter(cmd, "ZoneId", job.ZoneId);
                _db.AddParameter(cmd, "Name", job.Name);
                _db.AddParameter(cmd, "JobDescription", job.JobDescription);
                _db.AddParameter(cmd, "JobNote", job.JobNote);
                if (job.StartedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartedDate", DBNull.Value);
                else _db.AddParameter(cmd, "StartedDate", job.StartedDate);
                if (job.Deadline <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "Deadline", DBNull.Value);
                else _db.AddParameter(cmd, "Deadline", job.Deadline);
                if (job.FinishedDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FinishedDate", DBNull.Value);
                else _db.AddParameter(cmd, "FinishedDate", job.FinishedDate);
                _db.AddParameter(cmd, "PercentComplete", job.PercentComplete);
                _db.AddParameter(cmd, "Status", job.Status);

                _db.AddParameter(cmd, "ListProcessUser", job.ListProcessUser);
                _db.AddParameter(cmd, "ListFollowUser", job.ListFollowUser);

                _db.AddParameter(cmd, "Id", job.Id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatus(JobEntity job)
        {
            const string commandText = "CMS_Job_Update_Status";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "JobId", job.Id);
                _db.AddParameter(cmd, "JobNote", job.JobNote);
                _db.AddParameter(cmd, "Status", job.Status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int jobId)
        {
            const string commandText = "CMS_Job_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", jobId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public JobEntity GetJobById(int jobId)
        {
            const string commandText = "CMS_Job_GetbyId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", jobId);
                var numberOfRow = _db.Get<JobEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<JobEntity> Search(string keyword, int jobType, int jobParentId, string createdBy, string userAssigned, int status, int zoneId, int isHightLight, int isUnreadComment, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Job_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "JobType", jobType);
                _db.AddParameter(cmd, "JobParentId", jobParentId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "UserAssigned", userAssigned);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "isHightLight", isHightLight);
                _db.AddParameter(cmd, "isUnreadComment", isUnreadComment);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<JobEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected JobDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
