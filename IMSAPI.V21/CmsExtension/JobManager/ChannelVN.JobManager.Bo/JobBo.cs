﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.JobManager.Dal;
using ChannelVN.JobManager.Dal.Common;
using ChannelVN.JobManager.Entity;
using ChannelVN.JobManager.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO.CmsDiscussion;
using ChannelVN.WcfExtensions;

namespace ChannelVN.JobManager.Bo
{
    public class JobBo
    {
        public static ErrorMapping.ErrorCodes GetHighlightJob(int jobId, string userAssigned)
        {
            return JobAssignmentDal.GetHightlight(jobId, userAssigned) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static int GetUnreadComment(int jobId, string userAssigned)
        {
            return JobAssignmentDal.GetUnread(jobId, userAssigned);

        }
        public static ErrorMapping.ErrorCodes SetUnreadComment(int jobId, int jobReportId, string userAssigned, int status)
        {
            if (jobId <= 0) return ErrorMapping.ErrorCodes.InvalidJob;
            var job = JobDal.GetJobById(jobId);
            if (job == null) return ErrorMapping.ErrorCodes.InvalidJob;

            return JobAssignmentDal.SetUnread(jobId, jobReportId, userAssigned, status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes HighlightJob(int jobId, string userAssigned)
        {
            if (jobId <= 0) return ErrorMapping.ErrorCodes.InvalidJob;
            var job = JobDal.GetJobById(jobId);
            if (job == null) return ErrorMapping.ErrorCodes.InvalidJob;

            return JobAssignmentDal.HightLight(jobId, userAssigned) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteJob(int jobId)
        {
            if (jobId <= 0) return ErrorMapping.ErrorCodes.InvalidJob;
            var job = JobDal.GetJobById(jobId);
            if (job == null) return ErrorMapping.ErrorCodes.InvalidJob;

            if (JobDal.Delete(jobId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes JobUpdateStatus(JobEntity job)
        {
            return JobDal.UpdateStatus(job) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static JobEntity GetJobByJobId(int jobId)
        {
            return JobDal.GetJobById(jobId);
        }

        public static JobDetailEntity GetJobDetailByJobId(int jobId)
        {
            var job = JobDal.GetJobById(jobId);
            if (job != null)
            {
                var jobDetail = new JobDetailEntity
                    {
                        Job = job,
                        FollowUsers = new List<JobAssignmentEntity>(),
                        ProcessUsers = new List<JobAssignmentEntity>(),
                        Targets = JobTargetDal.GetByJobId(jobId),
                        FileAttachments = JobFileAttachmentDal.GetByJobId(jobId)
                    };
                var jobAssignments = JobAssignmentDal.GetByJobId(jobId);
                var count = jobAssignments.Count;
                for (var i = 0; i < count; i++)
                {
                    if (jobAssignments[i].AssignmentType == (int)EnumJobAssignmentType.ProcessUser)
                    {
                        jobDetail.ProcessUsers.Add(jobAssignments[i]);
                    }
                    else
                    {
                        jobDetail.FollowUsers.Add(jobAssignments[i]);
                    }
                }
                return jobDetail;
            }
            return null;
        }
        /// <summary>
        /// Get Job info by JobId and Attachment by JobId or JobReportId
        /// </summary>
        /// <param name="jobId">1. JobId, 2. JobReportId</param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static JobDetailEntity GetJobDetailByJobId(int jobId, int jobReportId)
        {
            //var job = JobDal.GetJobById(jobId);
            if (jobId > 0)
            {
                var jobDetail = new JobDetailEntity
                {
                    //Job = job,
                    FollowUsers = new List<JobAssignmentEntity>(),
                    ProcessUsers = new List<JobAssignmentEntity>(),
                    //Targets = JobTargetDal.GetByJobId(jobId),
                    UnreadComment = new List<JobAssignmentEntity>(),
                    FileAttachments = (jobReportId == 0) ? JobFileAttachmentDal.GetByJobId(jobId)
                                                       : JobFileAttachmentDal.GetByJobReportId(jobReportId)
                };
                var jobAssignments = JobAssignmentDal.GetByJobId(jobId);
                var count = jobAssignments.Count;
                for (var i = 0; i < count; i++)
                {
                    if (jobAssignments[i].UnreadComment == (int)EnumJobAssignmentType.Unread)
                    {
                        jobDetail.UnreadComment.Add(jobAssignments[i]);
                    }
                }
                return jobDetail;
            }
            return null;
        }

        public static bool GetJobUnReadCommentByUserAssigned(int jobId, int jobReportId, string UserAssigned)
        {
            var jobAssignments = JobAssignmentDal.GetUnread(jobId, jobReportId, UserAssigned);
            if (jobAssignments > 0)
            {
                return true;
            }
            return false;
        }

        public static JobDetailEntity GetJobDetailAndDiscussionByJobId(int jobId, int topParentDiscussion, int topChildDiscussion)
        {
            var job = JobDal.GetJobById(jobId);
            if (job != null)
            {
                var totalParentDiscussion = 0;
                job.ListDiscussion = CmsDiscussionForJobManagerBo.GetTopDiscussionByJobId(job.Id,
                                                                                          topParentDiscussion,
                                                                                          topChildDiscussion,
                                                                                          ref
                                                                                              totalParentDiscussion);
                job.TotalParentDiscussion = totalParentDiscussion;
                var jobDetail = new JobDetailEntity
                {
                    Job = job,
                    FollowUsers = new List<JobAssignmentEntity>(),
                    ProcessUsers = new List<JobAssignmentEntity>(),
                    Targets = JobTargetDal.GetByJobId(jobId),
                    FileAttachments = JobFileAttachmentDal.GetByJobId(jobId)
                };
                var jobAssignments = JobAssignmentDal.GetByJobId(jobId);
                var count = jobAssignments.Count;
                for (var i = 0; i < count; i++)
                {
                    if (jobAssignments[i].AssignmentType == (int)EnumJobAssignmentType.ProcessUser)
                    {
                        jobDetail.ProcessUsers.Add(jobAssignments[i]);
                    }
                    else
                    {
                        jobDetail.FollowUsers.Add(jobAssignments[i]);
                    }
                }
                return jobDetail;
            }
            return null;
        }

        public static List<JobEntity> SearchJob(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int pageIndex, int pageSize, ref int totalRow)
        {
            return JobDal.Search(keyword, (int)jobType, jobParentId, createdBy, userAssigned, (int)status, zoneId,isHightLight, isUnreadComment, pageIndex, pageSize, ref totalRow);
        }
        public static List<JobReportEntity> ListReport(int jobId, int parentId, ref int totalRow)
        {
            return JobReportDal.Search(jobId, parentId, ref totalRow);
        }
        public static List<JobEntity> SearchJobWithDiscussion(string keyword, EnumJobType jobType, int jobParentId, string createdBy, string userAssigned, EnumJobStatus status, int zoneId, int isHightLight, int isUnreadComment, int topParentDiscussion, int topChildDiscussion, int pageIndex, int pageSize, ref int totalRow)
        {
            var listJob = JobDal.Search(keyword, (int)jobType, jobParentId, createdBy, userAssigned, (int)status, zoneId, isHightLight, isUnreadComment, pageIndex, pageSize, ref totalRow);

            var jobCount = listJob.Count;
            for (var i = 0; i < jobCount; i++)
            {
                var job = listJob[i];
                var totalParentDiscussion = 0;
                job.ListDiscussion = CmsDiscussionForJobManagerBo.GetTopDiscussionByJobId(job.Id,
                                                                                          topParentDiscussion,
                                                                                          topChildDiscussion,
                                                                                          ref
                                                                                              totalParentDiscussion);
                job.TotalParentDiscussion = totalParentDiscussion;
                listJob[i] = job;
            }

            return listJob;
        }

        public static ErrorMapping.ErrorCodes UpdateTargetJob(JobEntity job, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments, ref int newJobId)
        {
            #region Kiểm tra dữ liệu đầu vào

            if (job == null) return ErrorMapping.ErrorCodes.InvalidJob;
            if (string.IsNullOrEmpty(job.Name)) return ErrorMapping.ErrorCodes.InvalidJob;

            job.CreatedBy = WcfMessageHeader.Current.ClientUsername;

            // Chỉ tiêu thì JobParentId=0 (lúc nào cũng là Job gốc)
            job.JobParentId = 0;
            job.JobType = (int)EnumJobType.TargetJob;

            var zone = ZoneBo.GetZoneById(job.ZoneId);
            if (zone == null)
            {
                return ErrorMapping.ErrorCodes.InvalidZoneForJob;
            }

            #endregion

            #region Kiểm tra trạng thái của job

            EnumJobStatus jobStatus;
            if (!Enum.TryParse(job.Status.ToString(), true, out jobStatus))
            {
                jobStatus = EnumJobStatus.Processing;
            }

            // Job start luôn
            if (jobStatus == EnumJobStatus.Processing)
            {
                // Nếu chưa set StartedDate thì lấy luôn thời điểm hiện tại
                if (job.StartedDate <= DbCommon.MinDateTime)
                {
                    job.StartedDate = DateTime.Now;
                }
                // Job mới start nên chưa có ngày kết thúc
                job.FinishedDate = DateTime.MinValue;
            }
            //Job đã kết thúc
            if (jobStatus == EnumJobStatus.JobFailed || jobStatus == EnumJobStatus.JobCompleted)
            {
                // Nếu chưa có ngày bắt đầu thì báo lỗi
                if (job.StartedDate <= DbCommon.MinDateTime) return ErrorMapping.ErrorCodes.JobMustHaveStartedDateForFinishedState;
                // Chưa set ngày kết thúc
                if (job.FinishedDate <= DbCommon.MinDateTime)
                {
                    // Lấy thời điểm hiện tại là ngày kết thúc
                    job.FinishedDate = DateTime.Now;
                    // Nếu ngày kết thúc (thời điểm hiện tại) nhỏ hơn ngày bắt đầu thì lấy ngày kết thúc là ngày bắt đầu luôn
                    if (job.StartedDate > job.FinishedDate)
                    {
                        job.FinishedDate = job.StartedDate;
                    }
                }
                // Nếu ngày bắt đầu lớn hơn ngày kết thúc thì báo lỗi
                if (job.StartedDate > job.FinishedDate) return ErrorMapping.ErrorCodes.FinishedDateMustGreaterThanStartedDate;
            }

            #endregion

            #region Chuẩn hóa job assignment

            var allZoneSecretary = UserBo.GetNormalUserByPermissionIdAndZoneId(EnumPermission.ArticleAdmin, job.ZoneId);

            job.ListProcessUser = "";

            foreach (var user in allZoneSecretary)
            {
                job.ListProcessUser += ";" + user.UserName;
            }
            if (!string.IsNullOrEmpty(job.ListProcessUser)) job.ListProcessUser = job.ListProcessUser.Remove(0, 1);

            job.ListFollowUser = job.CreatedBy;
            foreach (var username in listFollowUser.Where(username => username != job.CreatedBy))
            {
                job.ListFollowUser += ";" + username;
            }

            #endregion

            var updateResult = ErrorMapping.ErrorCodes.BusinessError;
            if (job.Id > 0)
            {
                #region JobId > 0 ==> Update

                var createdJobSuccess = JobDal.Update(job);
                if (createdJobSuccess)
                {
                    newJobId = job.Id;

                    updateResult = ErrorMapping.ErrorCodes.Success;
                }

                #endregion
            }
            else
            {
                #region JobId <= 0 ==> Insert new job

                var createdJobSuccess = JobDal.Insert(job, ref newJobId);
                if (createdJobSuccess && newJobId > 0)
                {
                    updateResult = ErrorMapping.ErrorCodes.Success;
                }

                #endregion
            }

            if (updateResult == ErrorMapping.ErrorCodes.Success)
            {
                JobTargetDal.DeleteByJobId(newJobId);
                foreach (var jobTarget in jobTargets)
                {
                    JobTargetDal.Update(newJobId, jobTarget.TargetType, jobTarget.TargetValue);
                }

                var listFileAttachment = JobFileAttachmentDal.GetByJobId(newJobId);
                foreach (
                    var fileAttachment in
                        listFileAttachment.Where(
                            fileAttachment => !jobFileAttachments.Exists(item => item.Id == fileAttachment.Id)))
                {
                    JobFileAttachmentDal.Delete(fileAttachment.Id);
                }

                foreach (
                    var jobFileAttachment in jobFileAttachments.Where(jobFileAttachment => jobFileAttachment.Id <= 0))
                {
                    jobFileAttachment.JobId = newJobId;
                    jobFileAttachment.JobReportId = 0;
                    jobFileAttachment.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                    JobFileAttachmentDal.Insert(jobFileAttachment);
                }
            }

            return updateResult;
        }

        public static ErrorMapping.ErrorCodes InsertReport(JobReportEntity report, List<JobFileAttachmentEntity> jobFileAttachments, ref int newReportId)
        //public static ErrorMapping.ErrorCodes InsertReport(JobReportEntity report, ref int newReportId)
        {
            var updateResult = ErrorMapping.ErrorCodes.BusinessError;
            var createdJobSuccess = JobReportDal.Insert(report, ref newReportId);
            if (createdJobSuccess && newReportId > 0)
            {
                updateResult = ErrorMapping.ErrorCodes.Success;
                if (jobFileAttachments != null && jobFileAttachments.Count > 0)
                {
                    foreach (var jobFileAttachment in jobFileAttachments)
                    {
                        jobFileAttachment.JobId = 0;
                        jobFileAttachment.JobReportId = newReportId;
                        jobFileAttachment.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                        JobFileAttachmentDal.Insert(jobFileAttachment);
                    }
                }
            }
            return updateResult;
        }

        public static ErrorMapping.ErrorCodes UpdateNormalJob(JobEntity job, List<string> listProcessUser, List<string> listFollowUser, List<JobTargetEntity> jobTargets, List<JobFileAttachmentEntity> jobFileAttachments, ref int newJobId)
        {
            #region Kiểm tra dữ liệu đầu vào

            if (job == null) return ErrorMapping.ErrorCodes.InvalidJob;
            if (string.IsNullOrEmpty(job.Name)) return ErrorMapping.ErrorCodes.InvalidJob;

            job.CreatedBy = WcfMessageHeader.Current.ClientUsername;

            JobEntity parentJob = null;
            if (job.JobParentId > 0)
            {
                parentJob = JobDal.GetJobById(job.JobParentId);
                if (parentJob == null)
                {
                    return ErrorMapping.ErrorCodes.InvalidParentJob;
                }
            }
            job.JobType = (int)EnumJobType.NormalJob;

            #endregion

            #region Kiểm tra trạng thái của job

            EnumJobStatus jobStatus;
            if (!Enum.TryParse(job.Status.ToString(), true, out jobStatus))
            {
                jobStatus = EnumJobStatus.Processing;
            }

            // Job start luôn
            if (jobStatus == EnumJobStatus.Processing)
            {
                // Nếu chưa set StartedDate thì lấy luôn thời điểm hiện tại
                if (job.StartedDate <= DbCommon.MinDateTime)
                {
                    job.StartedDate = DateTime.Now;
                }
                // Job mới start nên chưa có ngày kết thúc
                job.FinishedDate = DateTime.MinValue;
            }
            //Job đã kết thúc
            if (jobStatus == EnumJobStatus.JobFailed || jobStatus == EnumJobStatus.JobCompleted)
            {
                // Nếu chưa có ngày bắt đầu thì báo lỗi
                if (job.StartedDate <= DbCommon.MinDateTime) return ErrorMapping.ErrorCodes.JobMustHaveStartedDateForFinishedState;
                // Chưa set ngày kết thúc
                if (job.FinishedDate <= DbCommon.MinDateTime)
                {
                    // Lấy thời điểm hiện tại là ngày kết thúc
                    job.FinishedDate = DateTime.Now;
                    // Nếu ngày kết thúc (thời điểm hiện tại) nhỏ hơn ngày bắt đầu thì lấy ngày kết thúc là ngày bắt đầu luôn
                    if (job.StartedDate > job.FinishedDate)
                    {
                        job.FinishedDate = job.StartedDate;
                    }
                }
                // Nếu ngày bắt đầu lớn hơn ngày kết thúc thì báo lỗi
                if (job.StartedDate > job.FinishedDate) return ErrorMapping.ErrorCodes.FinishedDateMustGreaterThanStartedDate;
            }

            #endregion

            #region Chuẩn hóa job assignment

            job.ListProcessUser = "";
            foreach (var username in listProcessUser)
            {
                job.ListProcessUser += ";" + username;
            }
            if (!string.IsNullOrEmpty(job.ListProcessUser)) job.ListProcessUser = job.ListProcessUser.Remove(0, 1);

            job.ListFollowUser = job.CreatedBy;
            foreach (var username in listFollowUser.Where(username => username != job.CreatedBy))
            {
                job.ListFollowUser += ";" + username;
            }

            // Nếu là job gắn với chỉ tiêu thì add thêm người tạo chỉ tiêu và những người liên quan tới chỉ tiêu để cùng Follow công việc
            if (parentJob != null)
            {
                // Người tạo job follow
                var currentFollowUser = ";" + job.ListFollowUser + ";";
                if (currentFollowUser.IndexOf(";" + parentJob.CreatedBy + ";", StringComparison.CurrentCultureIgnoreCase) < 0)
                {
                    job.ListFollowUser += ";" + parentJob.CreatedBy;
                    currentFollowUser += parentJob.CreatedBy + ";";
                }
                // Người liên quan đến chỉ tiêu Follow
                var parentJobAssignment = JobAssignmentDal.GetByJobId(parentJob.Id);
                foreach (var jobAssignment in parentJobAssignment)
                {
                    if (currentFollowUser.IndexOf(";" + jobAssignment.UserAssigned + ";", StringComparison.CurrentCultureIgnoreCase) < 0)
                    {
                        job.ListFollowUser += ";" + parentJob.CreatedBy;
                        currentFollowUser += parentJob.CreatedBy + ";";
                    }
                }
            }

            #endregion

            var updateResult = ErrorMapping.ErrorCodes.BusinessError;
            if (job.Id > 0)
            {
                #region JobId > 0 ==> Update

                var createdJobSuccess = JobDal.Update(job);
                if (createdJobSuccess)
                {
                    newJobId = job.Id;

                    updateResult = ErrorMapping.ErrorCodes.Success;
                }

                #endregion
            }
            else
            {
                #region JobId <= 0 ==> Insert new job

                var createdJobSuccess = JobDal.Insert(job, ref newJobId);
                if (createdJobSuccess && newJobId > 0)
                {
                    updateResult = ErrorMapping.ErrorCodes.Success;
                }

                #endregion
            }

            if (updateResult == ErrorMapping.ErrorCodes.Success)
            {
                JobTargetDal.DeleteByJobId(newJobId);
                foreach (var jobTarget in jobTargets)
                {
                    JobTargetDal.Update(newJobId, jobTarget.TargetType, jobTarget.TargetValue);
                }

                var listFileAttachment = JobFileAttachmentDal.GetByJobId(newJobId);
                foreach (
                    var fileAttachment in
                        listFileAttachment.Where(
                            fileAttachment => !jobFileAttachments.Exists(item => item.Id == fileAttachment.Id)))
                {
                    JobFileAttachmentDal.Delete(fileAttachment.Id);
                }

                foreach (
                    var jobFileAttachment in jobFileAttachments.Where(jobFileAttachment => jobFileAttachment.Id <= 0))
                {
                    jobFileAttachment.JobId = newJobId;
                    jobFileAttachment.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                    JobFileAttachmentDal.Insert(jobFileAttachment);
                }
            }

            return updateResult;
        }

    }
}
