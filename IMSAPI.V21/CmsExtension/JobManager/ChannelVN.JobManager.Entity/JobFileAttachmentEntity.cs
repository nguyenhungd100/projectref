﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.JobManager.Entity
{
    [DataContract]
    public class JobFileAttachmentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int JobId { get; set; }
        [DataMember]
        public int JobReportId { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string FileUrl { get; set; }
        [DataMember]
        public long FileSize { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }
}
