﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.JobManager.Entity
{
    [DataContract]
    public class JobReportEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int JobId { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ReportContent { get; set; }
        [DataMember]
        public double PercentComplete { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ListReceiveUser  { get; set; }
    }
}
