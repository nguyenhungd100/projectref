﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.JobManager.Entity
{
    [DataContract]
    public enum JobTargetType
    {
        [EnumMember]
        AllTarget = -1,
        [EnumMember]
        ViewTarget = 1,
        [EnumMember]
        NumberOfArticleTarget = 2
    }
    [DataContract]
    public class JobTargetEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int JobId { get; set; }
        [DataMember]
        public int TargetType { get; set; }
        [DataMember]
        public int TargetValue { get; set; }
    }
}
