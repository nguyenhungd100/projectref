﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.JobManager.Entity
{
    [DataContract]
    public enum EnumJobAssignmentType
    {
        [EnumMember]
        AllJob = -1,
        [EnumMember]
        ProcessUser = 1,
        [EnumMember]
        FollowUser = 2,
        [EnumMember]
        Unread = 1,
    }
    [DataContract]
    public class JobAssignmentEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int JobId { get; set; }
        [DataMember]
        public int JobReportId { get; set; }
        [DataMember]
        public string UserAssigned { get; set; }
        [DataMember]
        public int AssignmentType { get; set; }
        [DataMember]
        public bool IsHighlight { get; set; }
        [DataMember]
        public int UnreadComment { get; set; }
    }
}
