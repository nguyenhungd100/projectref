﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.Entity;

namespace ChannelVN.JobManager.Entity
{
    [DataContract]
    public enum EnumJobType
    {
        [EnumMember]
        AllJob = -1,
        [EnumMember]
        TargetJob = 1,
        [EnumMember]
        NormalJob = 2
    }
    [DataContract]
    public enum EnumJobStatus
    {
        [EnumMember]
        AllJob = -1,
        [EnumMember]
        Processing = 1,
        [EnumMember]
        JobCompleted = 3,
        [EnumMember]
        JobFailed = 4
    }
    [DataContract]
    public class JobEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int JobParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int JobType { get; set; }
        [DataMember]
        public string JobDescription { get; set; }
        [DataMember]
        public string JobNote { get; set; }
        [DataMember]
        public DateTime StartedDate { get; set; }
        [DataMember]
        public DateTime Deadline { get; set; }
        [DataMember]
        public DateTime FinishedDate { get; set; }
        [DataMember]
        public double PercentComplete { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string ListProcessUser { get; set; }
        [DataMember]
        public string ListFollowUser { get; set; }
        [DataMember]
        public List<CmsDiscussionEntity> ListDiscussion { get; set; }
        [DataMember]
        public int TotalParentDiscussion { get; set; }
    }

    [DataContract]
    public class JobDetailEntity : EntityBase
    {
        [DataMember]
        public JobEntity Job { get; set; }
        [DataMember]
        public List<JobAssignmentEntity> ProcessUsers { get; set; }
        [DataMember]
        public List<JobAssignmentEntity> FollowUsers { get; set; }
        [DataMember]
        public List<JobTargetEntity> Targets { get; set; }
        [DataMember]
        public List<JobFileAttachmentEntity> FileAttachments { get; set; }
        [DataMember]
        public List<JobAssignmentEntity> UnreadComment { get; set; }
    }
}
