﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.MainDal.Databases;

namespace ChannelVN.FunnyNews.DAL
{
    public class ZoneDal
    {
        public static List<ZoneUseFunnyNewsEntity>  GetAllZoneUseForFunnyNews()
        {
            List<ZoneUseFunnyNewsEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.ZoneContentMainDal.GetAllZoneUseForFunnyNews();
            }
            return retVal;
        }
        public static ZoneUseFunnyNewsEntity GetZoneUseForFunnyNewsByZoneId(int zoneId)
        {
            ZoneUseFunnyNewsEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.ZoneContentMainDal.GetZoneUseForFunnyNewsByZoneId(zoneId);
            }
            return retVal;
        }
    }
}
