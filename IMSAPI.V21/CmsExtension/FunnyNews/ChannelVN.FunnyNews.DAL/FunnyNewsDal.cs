﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.DAL.Common;
using ChannelVN.FunnyNews.MainDal.Databases;

namespace ChannelVN.FunnyNews.DAL
{
    public class FunnyNewsDal
    {
        #region Update

        public static bool InsertFunnyNews(FunnyNewsEntity funnyNews, string tagIdList, ref int newPhotoId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.InsertFunnyNews(funnyNews, tagIdList, ref newPhotoId);
            }
            return retVal;
        }
        public static bool UpdateFunnyNews(FunnyNewsEntity funnyNews, string tagIdList)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.UpdateFunnyNews(funnyNews, tagIdList);
            }
            return retVal;
        }
        public static bool UpdateFunnyNewsStatus(int id, EnumFunnyNewsStatus status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.UpdateFunnyNewsStatus(id, status);
            }
            return retVal;
        }
        public static bool UpdateFunnyNewsViewCount(int id, int viewCount)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.UpdateFunnyNewsViewCount(id, viewCount);
            }
            return retVal;
        }
        public static bool DeleteFunnyNews(int funnyNewsId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.DeleteFunnyNews(funnyNewsId);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static FunnyNewsEntity GetFunnyNewsById(int funnyNewsId)
        {
            FunnyNewsEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.GetFunnyNewsById(funnyNewsId);
            }
            return retVal;
        }
        public static List<FunnyNewsEntity> SearchFunnyNews(string keyword, int zoneId, EnumFunnyNewsMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumFunnyNewsStatus status, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FunnyNewsEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.SearchFunnyNews(keyword, zoneId, mediaType,
                                                        fromDate, toDate, createdBy, author,
                                                        status, isHot, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<FunnyNewsCountEntity> CountFunnyNews(string username, int zoneId)
        {
            List<FunnyNewsCountEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsDal.CountFunnyNews(username, zoneId);
            }
            return retVal;
        }

        #endregion
    }
}
