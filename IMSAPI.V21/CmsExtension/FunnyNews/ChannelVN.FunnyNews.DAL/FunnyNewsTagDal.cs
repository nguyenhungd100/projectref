﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.DAL.Common;
using ChannelVN.FunnyNews.MainDal.Databases;

namespace ChannelVN.FunnyNews.DAL
{
    public class FunnyNewsTagDal
    {
        public static List<FunnyNewsTagEntity> GetTagByFunnyNewsId(int funnyNewsId)
        {
            List<FunnyNewsTagEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FunnyNewsTagDal.GetTagByFunnyNewsId(funnyNewsId);
            }
            return retVal;
        }
    }
}
