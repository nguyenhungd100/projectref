﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.FunnyNews.Entity
{
    [DataContract]
    public class FunnyNewsTagEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long FunnyNewsId { get; set; }     
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
    }   
}
