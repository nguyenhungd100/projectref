﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.FunnyNews.Entity
{
    [DataContract]
    public class ZoneUseFunnyNewsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
    }
}
