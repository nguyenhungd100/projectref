﻿using ChannelVN.CMS.Common;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.FunnyNews.MainDal
{
    public abstract class FunnyNewsDalBase
    {
        public bool InsertFunnyNews(FunnyNewsEntity funnyNews, string tagIdList, ref int newPhotoId)
        {
            const string commandText = "CMS_FunnyNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", funnyNews.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", funnyNews.ZoneId);
                _db.AddParameter(cmd, "MediaType", funnyNews.MediaType);
                _db.AddParameter(cmd, "Title", funnyNews.Title);
                _db.AddParameter(cmd, "MediaContent", funnyNews.MediaContent);
                _db.AddParameter(cmd, "Description", funnyNews.Description);
                _db.AddParameter(cmd, "Avatar", funnyNews.Avatar);
                _db.AddParameter(cmd, "Status", funnyNews.Status);
                _db.AddParameter(cmd, "IsHot", funnyNews.IsHot);
                _db.AddParameter(cmd, "CreatedBy", funnyNews.CreatedBy);
                _db.AddParameter(cmd, "Author", funnyNews.Author);
                _db.AddParameter(cmd, "DistributionDate", funnyNews.DistributionDate);
                _db.AddParameter(cmd, "UnSignName", funnyNews.UnSignName);
                _db.AddParameter(cmd, "Tags", funnyNews.Tags);
                _db.AddParameter(cmd, "NewsId", funnyNews.NewsId);
                _db.AddParameter(cmd, "Url", funnyNews.Url);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                var data = cmd.ExecuteNonQuery();
                newPhotoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return newPhotoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFunnyNews(FunnyNewsEntity funnyNews, string tagIdList)
        {

            const string commandText = "CMS_FunnyNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", funnyNews.Id);
                _db.AddParameter(cmd, "ZoneId", funnyNews.ZoneId);
                _db.AddParameter(cmd, "MediaType", funnyNews.MediaType);
                _db.AddParameter(cmd, "Title", funnyNews.Title);
                _db.AddParameter(cmd, "MediaContent", funnyNews.MediaContent);
                _db.AddParameter(cmd, "Description", funnyNews.Description);
                _db.AddParameter(cmd, "Avatar", funnyNews.Avatar);
                _db.AddParameter(cmd, "Status", funnyNews.Status);
                _db.AddParameter(cmd, "IsHot", funnyNews.IsHot);
                _db.AddParameter(cmd, "CreatedBy", funnyNews.CreatedBy);
                _db.AddParameter(cmd, "Author", funnyNews.Author);
                _db.AddParameter(cmd, "DistributionDate", funnyNews.DistributionDate);
                _db.AddParameter(cmd, "UnSignName", funnyNews.UnSignName);
                _db.AddParameter(cmd, "Tags", funnyNews.Tags);
                _db.AddParameter(cmd, "NewsId", funnyNews.NewsId);
                _db.AddParameter(cmd, "Url", funnyNews.Url);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFunnyNewsStatus(int id, EnumFunnyNewsStatus status)
        {

            const string commandText = "CMS_FunnyNews_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFunnyNewsViewCount(int id, int viewCount)
        {

            const string commandText = "CMS_FunnyNews_UpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ViewCount", viewCount);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFunnyNews(int funnyNewsId)
        {
            const string commandText = "CMS_FunnyNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", funnyNewsId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public FunnyNewsEntity GetFunnyNewsById(int funnyNewsId)
        {
            const string commandText = "CMS_FunnyNews_GetById";
            try
            {
                FunnyNewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", funnyNewsId);
                data = _db.Get<FunnyNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FunnyNewsEntity> SearchFunnyNews(string keyword, int zoneId, EnumFunnyNewsMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumFunnyNewsStatus status, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FunnyNews_Search";
            try
            {
                List<FunnyNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "MediaType", (int)mediaType);
                _db.AddParameter(cmd, "CreatedDateFrom", fromDate);
                _db.AddParameter(cmd, "CreatedDateTo", toDate);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "IsHot", isHot);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FunnyNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FunnyNewsCountEntity> CountFunnyNews(string username, int zoneId) {
            const string commandText = "CMS_FunnyNews_GetPhotoCount";
            try
            {
                List<FunnyNewsCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username",username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<FunnyNewsCountEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho FunnyNewsDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected FunnyNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
