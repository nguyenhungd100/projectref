﻿using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.FunnyNews.MainDal
{
    public abstract class ZoneDalBase
    {
        public List<ZoneUseFunnyNewsEntity> GetAllZoneUseForFunnyNews() {
            const string commandText = "CMS_Zone_GetAllZoneUseForFunnyNews";
            try
            {
                List<ZoneUseFunnyNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<ZoneUseFunnyNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ZoneUseFunnyNewsEntity GetZoneUseForFunnyNewsByZoneId(int zoneId) {
            const string commandText = "CMS_Zone_GetZoneUseForFunnyNewsByZoneId";
            try
            {
                ZoneUseFunnyNewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd,"ZoneId", zoneId);
                data = _db.Get<ZoneUseFunnyNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho ZoneDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected ZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
