﻿using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.FunnyNews.MainDal
{
    public abstract class FunnyNewsTagDalBase
    {
        public List<FunnyNewsTagEntity> GetTagByFunnyNewsId(int funnyNewsId) {
            const string commandText = "CMS_FunnyNews_GetTagsByFunnyNewsId";
            try
            {
                List<FunnyNewsTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FunnyNewsId", funnyNewsId);
                data = _db.GetList<FunnyNewsTagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Chuyển đổi DAL cho FunnyNewsTagDalBase
        /// minhduongvan
        /// 2014/01/10
        /// </summary>

        #region Constructor

        private readonly CmsMainDb _db;

        protected FunnyNewsTagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
