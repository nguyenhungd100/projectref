﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.FunnyNews.MainDal.Databases;
using ChannelVN.FunnyNews.MainDal;

namespace ChannelVN.FunnyNews.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Zone
        private ZoneDal _zoneContentMainDal;
        public ZoneDalBase ZoneContentMainDal
        {
            get { return _zoneContentMainDal ?? (_zoneContentMainDal = new ZoneDal((CmsMainDb)this)); }
        }
        #endregion
        #region Funny News
        private FunnyNewsDal _funnyNewsDal;
        public FunnyNewsDalBase FunnyNewsDal
        {
            get { return _funnyNewsDal ?? (_funnyNewsDal = new FunnyNewsDal((CmsMainDb)this)); }
        }
        #endregion
        #region Funny News Tag
        private FunnyNewsTagDal _funnyNewsTagDal;
        public FunnyNewsTagDalBase FunnyNewsTagDal
        {
            get { return _funnyNewsTagDal ?? (_funnyNewsTagDal = new FunnyNewsTagDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}