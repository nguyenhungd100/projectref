﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.FunnyNews.DAL;
using ChannelVN.FunnyNews.Entity;
using ChannelVN.FunnyNews.Entity.ErrorCode;

namespace ChannelVN.FunnyNews.BO
{
    public class FunnyNewsBo
    {
        #region FunnyNews

        #region Update

        public static ErrorMapping.ErrorCodes InsertFunnyNews(FunnyNewsEntity funnyNews, string tagIdList, ref int funnyNewsId)
        {
            try
            {
                if (null == funnyNews)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (FunnyNewsDal.InsertFunnyNews(funnyNews, tagIdList, ref funnyNewsId))
                {
                    funnyNews.Id = funnyNewsId;
                    UpdateFunnyNews(funnyNews, tagIdList);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFunnyNews(FunnyNewsEntity funnyNews, string tagIdList)
        {
            try
            {
                if (null == funnyNews)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var zone = ZoneBo.GetZoneUseForFunnyNewsByZoneId(funnyNews.ZoneId);
                if (zone != null)
                {
                    funnyNews.Url = BuildLinkUrl(funnyNews.Id, zone.ShortUrl, funnyNews.Title);
                }

                return FunnyNewsDal.UpdateFunnyNews(funnyNews, tagIdList) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFunnyNewsStatus(int id, EnumFunnyNewsStatus status)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FunnyNewsDal.UpdateFunnyNewsStatus(id, status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFunnyNewsViewCount(int id, int viewCount)
        {
            try
            {
                return FunnyNewsDal.UpdateFunnyNewsViewCount(id, viewCount) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteFunnyNews(int photoId)
        {
            try
            {
                return FunnyNewsDal.DeleteFunnyNews(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FunnyNewsEntity GetFunnyNewsById(int id)
        {
            try
            {
                return FunnyNewsDal.GetFunnyNewsById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<FunnyNewsEntity> SearchFunnyNews(string keyword, int zoneId, EnumFunnyNewsMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumFunnyNewsStatus status, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FunnyNewsDal.SearchFunnyNews(keyword, zoneId, mediaType, fromDate, toDate, createdBy, author, status, isHot, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FunnyNewsEntity>();
            }
        }
        public static List<FunnyNewsCountEntity> CountFunnyNews(string username, int zoneId)
        {
            try
            {
                return FunnyNewsDal.CountFunnyNews(username, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        #endregion

        #region FunnyNewsTag
        public static List<FunnyNewsTagEntity> GetFunnyNewsTagsByFunnyNewsId(int id)
        {
            try
            {
                return FunnyNewsTagDal.GetTagByFunnyNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FunnyNewsTagEntity>();
            }
        }
        #endregion

        public static string BuildLinkUrl(long funnyNewsId, string zoneUrl, string newsTitle)
        {
            try
            {
                string formatUrl = BoConstants.NewsUrlFormatForFunnyNews;
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, funnyNewsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
