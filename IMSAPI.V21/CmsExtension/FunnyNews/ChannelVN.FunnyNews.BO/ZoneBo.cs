﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.FunnyNews.DAL;
using ChannelVN.FunnyNews.Entity;

namespace ChannelVN.FunnyNews.BO
{
    public class ZoneBo
    {
        public static List<ZoneUseFunnyNewsEntity> GetAllZoneUseForFunnyNews()
        {
            return ZoneDal.GetAllZoneUseForFunnyNews();
        }
        public static ZoneUseFunnyNewsEntity GetZoneUseForFunnyNewsByZoneId(int zoneId)
        {
            return ZoneDal.GetZoneUseForFunnyNewsByZoneId(zoneId);
        }
    }
}
