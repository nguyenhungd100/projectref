﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameLeague.Dal.Common;
using ChannelVN.LiveGameLeague.Entity;
using ChannelVN.LiveGameLeague.MainDal.Databases;

namespace ChannelVN.LiveGameLeague.Dal
{
    public class LiveGameLeagueDal
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveGameLeague
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Insert(LiveGameLeagueEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameLeagueMainDal.Insert(info, ref id);
            }
            return returnValue;
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveGameLeague
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool Update(LiveGameLeagueEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameLeagueMainDal.Update(info);
            }
            return returnValue;
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveGameLeague theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public static bool Delete(int liveGameId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameLeagueMainDal.Delete(liveGameId);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy thông tin live game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static LiveGameLeagueEntity GetLiveGameLeagueById(int Id)
        {
            LiveGameLeagueEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameLeagueMainDal.GetLiveGameLeagueById(Id);
            }
            return returnValue;
        }
        /// <summary>
        /// Lấy danh sách LiveGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LiveGameLeagueEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LiveGameLeagueMainDal.ListLiveGameLeagueByPages(search, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
