﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.LiveGameLeague.Entity
{
    [DataContract]
    public class LiveGameLeagueEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public string TagUrl { get; set; }
        [DataMember]
        public long TagId { get; set; }
    }
}
