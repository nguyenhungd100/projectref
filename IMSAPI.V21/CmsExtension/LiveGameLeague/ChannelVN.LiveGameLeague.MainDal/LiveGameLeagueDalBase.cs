﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.LiveGameLeague.Entity;
using ChannelVN.LiveGameLeague.MainDal.Databases;

namespace ChannelVN.LiveGameLeague.MainDal
{
    public abstract class LiveGameLeagueDalBase
    {
        /// <summary>
        /// Thêm mới dữ liệu vào bảng LiveGameLeague
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Insert(LiveGameLeagueEntity info, ref int id)
        {
            const string commandText = "CMS_LiveGameLeague_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Sửa dữ liệu cho bảng LiveGameLeague
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public bool Update(LiveGameLeagueEntity info)
        {
            const string commandText = "CMS_LiveGameLeague_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", info.Name);
                _db.AddParameter(cmd, "TagItem", !string.IsNullOrEmpty(info.TagItem) ? info.TagItem : null);
                _db.AddParameter(cmd, "TagUrl", !string.IsNullOrEmpty(info.TagUrl) ? info.TagUrl : null);
                _db.AddParameter(cmd, "TagId", info.TagId);
                _db.AddParameter(cmd, "Id", info.Id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Xóa dữ liệu trong bảng LiveGameLeague theo Id
        /// </summary>
        /// <param name="liveGameId"></param>
        /// <returns></returns>
        public bool Delete(int liveGameId)
        {
            const string commandText = "CMS_LiveGameLeague_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", liveGameId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy thông tin live game theo id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public LiveGameLeagueEntity GetLiveGameLeagueById(int Id)
        {
            const string commandText = "CMS_LiveGameLeague_SelectById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.Get<LiveGameLeagueEntity>(cmd);

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Lấy danh sách LiveGame
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public List<LiveGameLeagueEntity> ListLiveGameLeagueByPages(string search, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LiveGameLeague_Listdata";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Search", search);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<LiveGameLeagueEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected LiveGameLeagueDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
