﻿using System.Data;
using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.AFamily.MainDal.Databases;

namespace ChannelVN.AFamily.MainDal
{
    public abstract class NewsDalBase
    {
        #region Function SET
        
        public bool UpdateUpdateParentNewsId(long newsId, long id)
        {
            const string commandText = "CMS_News_UpdateParentNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId);
                _db.AddParameter(cmd, "ParentNewsId", id);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
