﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.AFamily.Dal;
using ChannelVN.AFamily.Entity;
using ChannelVN.AFamily.Entity.ErrorCode;

namespace ChannelVN.AFamily.Bo
{
    public class NewsBo
    {
        #region CommentUser
        public static ErrorMapping.ErrorCodes UpdateUpdateParentNewsId(long newsId, long id)
        {
            return NewsDal.UpdateUpdateParentNewsId(newsId,id)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
