﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.AFamily.Dal.Common;
using ChannelVN.AFamily.MainDal.Databases;

namespace ChannelVN.AFamily.Dal
{
    public class NewsDal
    {
        #region SET
        public static bool UpdateUpdateParentNewsId(long newsId, long id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateUpdateParentNewsId(newsId, id);
            }
            return returnValue;
        }
        #endregion
    }
}
