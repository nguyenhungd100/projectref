﻿using System;
using System.Collections.Generic;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.MainDal.Databases;

namespace ChannelVN.FootballRanking.DAL
{
    public class FootballRankingDetailDal
    {
        #region Update

        public static bool InsertFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail, ref int newId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDetailDal.InsertFootballRankingDetail(footballRankingDetail, ref newId);
            }
            return retVal;
        }

        public static bool UpdateFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDetailDal.UpdateFootballRankingDetail(footballRankingDetail);
            }
            return retVal;
        }

        public static bool DeleteFootballRankingDetail(int detailId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDetailDal.DeleteFootballRankingDetail(detailId);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static FootballRankingDetailEntity GetFootballRankingDetailById(int detailId)
        {
            FootballRankingDetailEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDetailDal.GetFootballRankingDetailById(detailId);
            }
            return retVal;
        }

        public static List<FootballRankingDetailEntity> SearchFootballRankingDetail(string keyword,
            int FootballRankingId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FootballRankingDetailEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDetailDal.SearchFootballRankingDetail(keyword, FootballRankingId, status,
                    pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        #endregion
    }
}
