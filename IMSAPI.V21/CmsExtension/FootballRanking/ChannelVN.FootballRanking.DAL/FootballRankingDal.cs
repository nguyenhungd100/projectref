﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.MainDal.Databases;
using ChannelVN.FootballRanking.DAL.Common;

namespace ChannelVN.FootballRanking.DAL
{
    public class FootballRankingDal
    {
        #region Update

        public static bool InsertFootballRanking(FootballRankingEntity footballRanking, ref int newId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.InsertFootballRanking(footballRanking, ref newId);
            }
            return retVal;
        }
        public static bool UpdateFootballRanking(FootballRankingEntity footballRanking)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.UpdateFootballRanking(footballRanking);
            }
            return retVal;
        }

        public static bool DeleteFootballRanking(int footballRankingId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.DeleteFootballRanking(footballRankingId);
            }
            return retVal;
        }
        public static bool UpdatePriority(string listOfPriority)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.UpdatePriority(listOfPriority);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static FootballRankingEntity GetFootballRankingById(int footballRankingId)
        {
            FootballRankingEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.GetFootballRankingById(footballRankingId);
            }
            return retVal;
        }
        public static List<FootballRankingEntity> SearchFootballRanking(string keyword, EnumFootballRankingStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FootballRankingEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.FootballRankingDal.SearchFootballRanking(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        #endregion
    }
}
