﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.FootballRanking.Entity
{
    [DataContract]
    public class FootballRankingDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int FootballRankingId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public int TagId   { get; set; }
        [DataMember]
        public string TagName   { get; set; }
        [DataMember]
        public string TagUrl   { get; set; }
        /// <summary>
        /// Tổng số trận
        /// </summary>
        [DataMember]
        public int GP { get; set; }
        /// <summary>
        /// Trận thắng
        /// </summary>
        [DataMember]
        public int W { get; set; }
        /// <summary>
        /// Trận hòa
        /// </summary>
        [DataMember]
        public int D { get; set; }
        /// <summary>
        /// Trận thua
        /// </summary>
        [DataMember]
        public int L { get; set; }
        /// <summary>
        /// Tổng bàn thắng
        /// </summary>
        [DataMember]
        public int GF { get; set; }
        /// <summary>
        /// Tổng số thủng lưới
        /// </summary>
        [DataMember]
        public int GA { get; set; }
        /// <summary>
        /// Tổng số điểm
        /// </summary>
        [DataMember]
        public int PTS { get; set; }
        [DataMember] 
        public int Status { get; set; }
        [DataMember]
        public string Description { get; set; }

    }
    [DataContract]
    public enum EnumFootballRankingDetailStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Show = 1,
        [EnumMember]
        Hide = 2,
    }
}
