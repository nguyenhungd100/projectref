﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.FootballRanking.DAL;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.Entity.ErrorCode;

namespace ChannelVN.FootballRanking.BO
{
    public class FootballRankingDetailBo
    {
        #region FootballRanking

        #region Update

        public static ErrorMapping.ErrorCodes InsertFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail, ref int id)
        {
            try
            {
                if (null == footballRankingDetail)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballRankingDetailDal.InsertFootballRankingDetail(footballRankingDetail, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateFootballRankingDetail(FootballRankingDetailEntity footballRankingDetail)
        {
            try
            {
                if (null == footballRankingDetail)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballRankingDetailDal.UpdateFootballRankingDetail(footballRankingDetail) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteFootballRankingDetail(int detailId)
        {
            try
            {
                return FootballRankingDetailDal.DeleteFootballRankingDetail(detailId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FootballRankingDetailEntity GetFootballRankingDetailById(int id)
        {
            try
            {
                return FootballRankingDetailDal.GetFootballRankingDetailById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<FootballRankingDetailEntity> SearchFootballRankingDetail(string keyword, int FootballRankingId, EnumFootballRankingDetailStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FootballRankingDetailDal.SearchFootballRankingDetail(keyword, FootballRankingId, (int)status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FootballRankingDetailEntity>();
            }
        }


        #endregion

        #endregion
    }
}
