﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.FootballRanking.DAL;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.Entity.ErrorCode;

namespace ChannelVN.FootballRanking.BO
{
    public class FootballRankingBo
    {
        #region FootballRanking

        #region Update

        public static ErrorMapping.ErrorCodes InsertFootballRanking(FootballRankingEntity footballRanking, ref int newId)
        {
            try
            {
                if (null == footballRanking)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballRankingDal.InsertFootballRanking(footballRanking, ref newId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFootballRanking(FootballRankingEntity footballRanking)
        {
            try
            {
                if (null == footballRanking)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return FootballRankingDal.UpdateFootballRanking(footballRanking) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteFootballRanking(int photoId)
        {
            try
            {
                return FootballRankingDal.DeleteFootballRanking(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFootballRankingPriority(string listOfPriority)
        {
            try
            {
                return FootballRankingDal.UpdatePriority(listOfPriority) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FootballRankingEntity GetFootballRankingById(int id)
        {
            try
            {
                return FootballRankingDal.GetFootballRankingById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<FootballRankingEntity> SearchFootballRanking(string keyword, EnumFootballRankingStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FootballRankingDal.SearchFootballRanking(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FootballRankingEntity>();
            }
        }
       

        #endregion

        #endregion

    }
}
