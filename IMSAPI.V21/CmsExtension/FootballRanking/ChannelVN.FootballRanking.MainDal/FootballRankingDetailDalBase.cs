﻿using ChannelVN.CMS.Common;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.FootballRanking.MainDal
{
    public abstract class FootballRankingDetailDalBase
    {
        #region function SET
        public bool InsertFootballRankingDetail(FootballRankingDetailEntity FootballRankingDetail, ref int newId)
        {
            const string commandText = "CMS_FootballRankingDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRankingDetail.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "FootballRankingId", FootballRankingDetail.FootballRankingId);
                _db.AddParameter(cmd, "StartTime", FootballRankingDetail.StartTime);
                _db.AddParameter(cmd, "Name", FootballRankingDetail.Name);
                _db.AddParameter(cmd, "Logo", FootballRankingDetail.Logo);
                _db.AddParameter(cmd, "TagId", FootballRankingDetail.TagId);
                _db.AddParameter(cmd, "TagName", FootballRankingDetail.TagName);
                _db.AddParameter(cmd, "TagUrl", FootballRankingDetail.TagUrl);
                _db.AddParameter(cmd, "GP", FootballRankingDetail.GP);
                _db.AddParameter(cmd, "W", FootballRankingDetail.W);
                _db.AddParameter(cmd, "D", FootballRankingDetail.D);
                _db.AddParameter(cmd, "L", FootballRankingDetail.L);
                _db.AddParameter(cmd, "GF", FootballRankingDetail.GF);
                _db.AddParameter(cmd, "GA", FootballRankingDetail.GA);
                _db.AddParameter(cmd, "PTS", FootballRankingDetail.PTS);
                _db.AddParameter(cmd, "Status", FootballRankingDetail.Status);
                _db.AddParameter(cmd, "Description", FootballRankingDetail.Description);
                var result = cmd.ExecuteNonQuery();
                newId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return newId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateFootballRankingDetail(FootballRankingDetailEntity FootballRankingDetail)
        {
            const string commandText = "CMS_FootballRankingDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRankingDetail.Id);
                _db.AddParameter(cmd, "FootballRankingId", FootballRankingDetail.FootballRankingId);
                _db.AddParameter(cmd, "StartTime", FootballRankingDetail.StartTime);
                _db.AddParameter(cmd, "Name", FootballRankingDetail.Name);
                _db.AddParameter(cmd, "Logo", FootballRankingDetail.Logo);
                _db.AddParameter(cmd, "TagId", FootballRankingDetail.TagId);
                _db.AddParameter(cmd, "TagName", FootballRankingDetail.TagName);
                _db.AddParameter(cmd, "TagUrl", FootballRankingDetail.TagUrl);
                _db.AddParameter(cmd, "GP", FootballRankingDetail.GP);
                _db.AddParameter(cmd, "W", FootballRankingDetail.W);
                _db.AddParameter(cmd, "D", FootballRankingDetail.D);
                _db.AddParameter(cmd, "L", FootballRankingDetail.L);
                _db.AddParameter(cmd, "GF", FootballRankingDetail.GF);
                _db.AddParameter(cmd, "GA", FootballRankingDetail.GA);
                _db.AddParameter(cmd, "PTS", FootballRankingDetail.PTS);
                _db.AddParameter(cmd, "Status", FootballRankingDetail.Status);
                _db.AddParameter(cmd, "Description", FootballRankingDetail.Description);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteFootballRankingDetail(int detailId)
        {
            const string commandText = "CMS_FootballRankingDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", detailId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region function GET
        public FootballRankingDetailEntity GetFootballRankingDetailById(int detailId)
        {
            const string commandText = "CMS_FootballRankingDetail_GetById";
            try
            {
                FootballRankingDetailEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", detailId);
                data = _db.Get<FootballRankingDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FootballRankingDetailEntity> SearchFootballRankingDetail(string keyword, int FootballRankingId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FootballRankingDetail_Search";
            try
            {
                List<FootballRankingDetailEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FootballRankingId", FootballRankingId);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FootballRankingDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Constructor

        private readonly CmsMainDb _db;

        protected FootballRankingDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
