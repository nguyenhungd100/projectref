﻿using ChannelVN.CMS.Common;
using ChannelVN.FootballRanking.Entity;
using ChannelVN.FootballRanking.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.FootballRanking.MainDal
{
    public abstract class FootballRankingDalBase
    {
        #region SET
        public bool InsertFootballRanking(FootballRankingEntity FootballRanking, ref int newId)
        {
            const string commandText = "CMS_FootballRanking_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRanking.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", FootballRanking.Name);
                _db.AddParameter(cmd, "Avatar", FootballRanking.Avatar);
                _db.AddParameter(cmd, "SortOrder", FootballRanking.SortOrder);
                _db.AddParameter(cmd, "Data", FootballRanking.Data);
                _db.AddParameter(cmd, "Status", FootballRanking.Status);
                var result = cmd.ExecuteNonQuery();
                newId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return newId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFootballRanking(FootballRankingEntity FootballRanking) {
            const string commandText = "CMS_FootballRanking_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRanking.Id);
                _db.AddParameter(cmd, "Name", FootballRanking.Name);
                _db.AddParameter(cmd, "Avatar", FootballRanking.Avatar);
                _db.AddParameter(cmd, "SortOrder", FootballRanking.SortOrder);
                _db.AddParameter(cmd, "Data", FootballRanking.Data);
                _db.AddParameter(cmd, "Status", FootballRanking.Status);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFootballRanking(int FootballRankingId) {
            const string commandText = "CMS_FootballRanking_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRankingId);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(string listOfPriority) {
            const string commandText = "CMS_FootballRanking_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listOfPriority);
                var result = cmd.ExecuteNonQuery();
                return result > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region GET
        public FootballRankingEntity GetFootballRankingById(int FootballRankingId) {
            const string commandText = "CMS_FootballRanking_GetById";
            try
            {
                FootballRankingEntity data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", FootballRankingId);
                data = _db.Get<FootballRankingEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FootballRankingEntity> SearchFootballRanking(string keyword, EnumFootballRankingStatus status, int pageIndex, int pageSize, ref int totalRow) {
            const string commandText = "CMS_FootballRanking_Search";
            try
            {
                List<FootballRankingEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow,System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FootballRankingEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        
        #region Constructor

        private readonly CmsMainDb _db;

        protected FootballRankingDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
