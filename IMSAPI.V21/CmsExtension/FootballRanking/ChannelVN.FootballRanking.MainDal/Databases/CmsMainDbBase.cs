﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.FootballRanking.MainDal.Databases;
using ChannelVN.FootballRanking.MainDal;

namespace ChannelVN.FootballRanking.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Football score detail
        private FootballRankingDetailDal _FootballRankingDetailDal;
        public FootballRankingDetailDalBase FootballRankingDetailDal
        {
            get { return _FootballRankingDetailDal ?? (_FootballRankingDetailDal = new FootballRankingDetailDal((CmsMainDb)this)); }
        }
        #endregion

        #region Football score
        private FootballRankingDal _FootballRankingDal;
        public FootballRankingDalBase FootballRankingDal
        {
            get { return _FootballRankingDal ?? (_FootballRankingDal = new FootballRankingDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}