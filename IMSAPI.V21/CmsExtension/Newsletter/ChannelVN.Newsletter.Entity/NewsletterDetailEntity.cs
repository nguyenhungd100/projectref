﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Newsletter.Entity
{
    [DataContract]
    public class NewsletterDetailEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int NewsLetter_ID { get; set; }
        [DataMember]
        public long newsId { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string viewNewsId { get; set; }
    }
}
