﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.Newsletter.Entity
{
    [DataContract]
    public class NewsletterEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public DateTime UpdateDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        //News letter detail
        [DataMember]
        public int NewsLetter_ID { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string TitleNews { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
}
