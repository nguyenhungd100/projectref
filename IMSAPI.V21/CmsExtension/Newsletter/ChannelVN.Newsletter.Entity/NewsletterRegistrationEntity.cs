﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.Newsletter.Entity
{
    [DataContract]
    public class NewsletterRegistrationEntity : EntityBase
    {
        [DataMember]
        public long NewsletterId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime RegistrationDate { get; set; }
        [DataMember]
        public DateTime LastSentDate { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
