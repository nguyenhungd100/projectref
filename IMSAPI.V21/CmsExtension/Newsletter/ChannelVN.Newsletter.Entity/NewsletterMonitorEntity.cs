﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.Newsletter.Entity
{
    [DataContract]
    public class NewsletterMonitorEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime InsertedDate { get; set; }
        [DataMember]
        public DateTime ProcessedDate { get; set; }
        [DataMember]
        public bool IsProcessed { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string ThreadName { get; set; }
        [DataMember]
        public string ThreadUrl { get; set; }
    }
}
