﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.Newsletter.Dal;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.Entity.ErrorCode;

namespace ChannelVN.Newsletter.Bo
{
    public class NewsletterBo
    {
        #region NewsletterRegistration

        public static List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForThread()
        {
            return NewsletterRegistrationDal.GetAllNewsletterForThread();
        }

        public static List<NewsletterRegistrationEntity> GetAllNewsletterRegistrationForZone()
        {
            return NewsletterRegistrationDal.GetAllNewsletterForZone();
        }
        // CươngNP - 2014-09-16
        public static NewsletterRegistrationEntity GetNewsletterRegistrationById(int newsletterId)
        {
            return NewsletterRegistrationDal.GetNewsletterRegistrationById(newsletterId);
        }
        public static List<NewsletterRegistrationEntity> NewsletterRegistrationSearch(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsletterRegistrationDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
        }
        public static ErrorMapping.ErrorCodes NewsletterRegistrationUpdateStatus(int newsletterId, int status)
        {
            return NewsletterRegistrationDal.UpdateStatus(newsletterId, status) ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes NewsletterRegistrationDelete(int newsletterId)
        {
            return NewsletterRegistrationDal.Delete(newsletterId) ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
        #region NewsletterMonitor

        public static List<NewsletterMonitorEntity> GetUnprocessNewsletterMonitorItem()
        {
            return NewsletterMonitorDal.GetUnprocessItem();
        }
        public static ErrorMapping.ErrorCodes UpdateProcessNewsletterMonitorItemState(string listOfProcessId)
        {
            return NewsletterMonitorDal.UpdateProcessItemState(listOfProcessId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
        #region NewsLetter
        public static ErrorMapping.ErrorCodes InsertNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.InsertNewsLetter(entity, ref id);
            if (id > 0)
            {
                foreach (var item in entityDetail)
                {
                    item.NewsLetter_ID = id;
                    NewsletterDal.InsertNewsLetterDetail(item);
                }
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateNewsLetter(NewsletterEntity entity, List<NewsletterDetailEntity> entityDetail)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.UpdateNewsLetter(entity);
            if (createSuccess)
            {
                if (entityDetail != null && entityDetail.Count > 0)
                {
                    //Lấy toàn bộ tin bài liên quan tới bản tin
                    //Nếu tin bài đã tồn tại thì update, nếu chưa thì thêm mới
                    var newsLetterDetail = NewsletterDal.SelectDetailDataByNewsLetterId(entity.ID);
                    if (newsLetterDetail != null && newsLetterDetail.Count > 0)
                    {
                        var count = 0;
                        if (newsLetterDetail.Count > entityDetail.Count)
                            count = newsLetterDetail.Count;
                        else
                            count = entityDetail.Count;

                        for (var i = 0; i < count; i++)
                        {
                            if (i >= newsLetterDetail.Count)
                            {
                                NewsletterDal.InsertNewsLetterDetail(entityDetail[i]);
                            }
                            else
                            {
                                if (newsLetterDetail[i].newsId == entityDetail[i].newsId && newsLetterDetail[i].NewsLetter_ID == entityDetail[i].NewsLetter_ID)
                                {
                                    entityDetail[i].ID = newsLetterDetail[i].ID;
                                    NewsletterDal.UpdateNewsLetterDetail(entityDetail[i]);
                                }
                                else
                                    NewsletterDal.DeleteNewsLetterDetail(newsLetterDetail[i].ID);
                            }

                        }
                    }
                    else
                    {
                        foreach (var itemDetail in entityDetail)
                        {
                            NewsletterDal.InsertNewsLetterDetail(itemDetail);
                        }
                    }
                }
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }

        public static List<NewsletterEntity> SelectData(string keyword, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsletterDal.SelectData(keyword, pageIndex, pageSize, ref totalRows);
        }
        public static NewsletterEntity SelectDataById(int Id)
        {
            return NewsletterDal.SelectDataById(Id);
        }
        public static List<NewsletterDetailEntity> SelectNewsletterDetailById(int newsLetterId)
        {
            return NewsletterDal.SelectDetailDataByNewsLetterId(newsLetterId);
        }
        public static ErrorMapping.ErrorCodes DeleteNewsLetter(int Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.DeleteNewsLetter(Id);
            if (createSuccess)
            {
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes DeleteAllNewsLetterDetail(int newsLetterId)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.DeleteAllNewsLetterDetail(newsLetterId);
            if (createSuccess)
            {
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes DeleteNewsLetterDetail(int Id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.DeleteNewsLetterDetail(Id);
            if (createSuccess)
            {
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdatePriority(int newsLetterId, string listPriority)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsletterDal.UpdatePriority(newsLetterId, listPriority);
            if (createSuccess)
            {
                result = ErrorMapping.ErrorCodes.Success;
            }
            return result;
        }

        #endregion
    }
}
