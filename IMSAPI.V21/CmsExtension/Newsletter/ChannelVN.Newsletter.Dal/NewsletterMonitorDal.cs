﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Dal.Common;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.Dal
{
    public class NewsletterMonitorDal
    {
        public static List<NewsletterMonitorEntity> GetUnprocessItem()
        {
            List<NewsletterMonitorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMonitorMainDal.GetUnprocessItem();
            }
            return returnValue;
        }
        public static bool UpdateProcessItemState(string listOfProcessId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMonitorMainDal.UpdateProcessItemState(listOfProcessId);
            }
            return returnValue;
        }
    }
}
