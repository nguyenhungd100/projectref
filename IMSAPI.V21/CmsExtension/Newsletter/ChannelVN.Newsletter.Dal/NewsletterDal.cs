﻿using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Dal.Common;
using ChannelVN.Newsletter.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.Dal
{
    public class NewsletterDal
    {
        public static List<NewsletterEntity> SelectData(string keyword, int pageIndex, int pageSize, ref int totalRows)
        {
            List<NewsletterEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.SelectData(keyword, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }
        public static NewsletterEntity SelectDataById(int newsLetterId)
        {
            NewsletterEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.SelectDataById(newsLetterId);
            }
            return returnValue;
        }
        public static List<NewsletterDetailEntity> SelectDetailDataByNewsLetterId(int newsLetterId)
        {
            List<NewsletterDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.SelectDetailDataByNewsLetterId(newsLetterId);
            }
            return returnValue;
        }
        public static NewsletterDetailEntity SelectDetailDataByNewsId(long newsId, int newsletterId)
        {
            NewsletterDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.SelectDetailDataByNewsId(newsId, newsletterId);
            }
            return returnValue;
        }
        public static bool InsertNewsLetter(NewsletterEntity entity, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.InsertNewsLetter(entity, ref id);
            }
            return returnValue;
        }
        public static bool UpdateNewsLetter(NewsletterEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.UpdateNewsLetter(entity);
            }
            return returnValue;
        }
        public static bool InsertNewsLetterDetail(NewsletterDetailEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.InsertNewsLetterDetail(entity);
            }
            return returnValue;
        }
        public static bool UpdateNewsLetterDetail(NewsletterDetailEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.UpdateNewsLetterDetail(entity);
            }
            return returnValue;
        }
        public static bool DeleteNewsLetter(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.DeleteNewsLetter(id);
            }
            return returnValue;
        }
        public static bool DeleteAllNewsLetterDetail(int newsLetterId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.DeleteAllNewsLetterDetail(newsLetterId);
            }
            return returnValue;
        }
        public static bool DeleteNewsLetterDetail(int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.DeleteNewsLetterDetail(Id);
            }
            return returnValue;
        }
        public static bool UpdatePriority(int newsLetterId, string listPriority)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsletterMainDal.UpdatePriority(newsLetterId, listPriority);
            }
            return returnValue;
        }
    }
}
