﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Dal.Common;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.Dal
{
    public class NewsletterRegistrationDal
    {
        public static List<NewsletterRegistrationEntity> GetAllNewsletterForThread()
        {
            List<NewsletterRegistrationEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.GetAllNewsletterForThread();
            }
            return returnValue;
        }
        public static List<NewsletterRegistrationEntity> GetAllNewsletterForZone()
        {
            List<NewsletterRegistrationEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.GetAllNewsletterForZone();
            }
            return returnValue;
        }
        public static bool UpdateStatus(int newsletterId, int status)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.UpdateStatus(newsletterId, status);
            }
            return returnValue;
        }
        public static bool Delete(int newsletterId)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.Delete(newsletterId);
            }
            return returnValue;
        }
        public static NewsletterRegistrationEntity GetNewsletterRegistrationById(int newsletterId)
        {
            NewsletterRegistrationEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.GetNewsletterRegistrationById(newsletterId);
            }
            return returnValue;
        }
        public static List<NewsletterRegistrationEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsletterRegistrationEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsletterRegistrationMainDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
