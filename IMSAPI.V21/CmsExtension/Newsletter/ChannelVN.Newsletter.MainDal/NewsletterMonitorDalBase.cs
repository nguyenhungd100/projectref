﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.MainDal
{
    public abstract class NewsletterMonitorDalBase
    {
        public List<NewsletterMonitorEntity> GetUnprocessItem()
        {
            const string commandText = "CMS_NewsletterMonitor_GetUnprocessItem";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<NewsletterMonitorEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateProcessItemState(string listOfProcessId)
        {
            const string commandText = "CMS_NewsletterMonitor_UpdateProcessItemState";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfProcessId", listOfProcessId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsletterMonitorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
