﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.MainDal
{
    public abstract class NewsletterRegistrationDalBase
    {
        public List<NewsletterRegistrationEntity> GetAllNewsletterForThread()
        {
            const string commandText = "CMS_NewsletterRegistration_GetAllNewsletterForThread";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<NewsletterRegistrationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsletterRegistrationEntity> GetAllNewsletterForZone()
        {
            const string commandText = "CMS_NewsletterRegistration_GetAllNewsletterForZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var numberOfRow = _db.GetList<NewsletterRegistrationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int newsletterId, int status)
        {
            const string commandText = "CMS_NewsletterRegistration_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsletterId", newsletterId);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int newsletterId)
        {
            const string commandText = "CMS_NewsletterRegistration_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsletterId", newsletterId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsletterRegistrationEntity GetNewsletterRegistrationById(int newsletterId)
        {
            const string commandText = "CMS_NewsletterRegistration_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsletterId", newsletterId);
                var numberOfRow = _db.Get<NewsletterRegistrationEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsletterRegistrationEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsletterRegistration_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<NewsletterRegistrationEntity>(cmd);
                totalRow = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDb _db;

        protected NewsletterRegistrationDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
