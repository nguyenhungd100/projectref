﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Entity;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.MainDal
{
    public class NewsletterRegistrationDal : NewsletterRegistrationDalBase
    {
        internal NewsletterRegistrationDal(ExternalCmsDb db)
            : base(db)
        {
        }
    }
}
