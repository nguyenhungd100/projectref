﻿using ChannelVN.CMS.Common;
using ChannelVN.Newsletter.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.Newsletter.MainDal.Databases;

namespace ChannelVN.Newsletter.MainDal
{
    public abstract class NewsletterDalBase
    {
        public List<NewsletterEntity> SelectData(string keyword, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_Newsletter_GetAllData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "totalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);
                var numberOfRow = _db.GetList<NewsletterEntity>(cmd);
                totalRows = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsletterEntity SelectDataById(int newsLetterId)
        {
            const string commandText = "CMS_Newsletter_GetDataById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsLetterId", newsLetterId);
                var numberOfRow = _db.Get<NewsletterEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsletterDetailEntity> SelectDetailDataByNewsLetterId(int newsLetterId)
        {
            const string commandText = "CMS_NewsletterDetail_GetDataByNewsletterId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsLetterId", newsLetterId);
                var numberOfRow = _db.GetList<NewsletterDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsletterDetailEntity SelectDetailDataByNewsId(long newsId, int newsletterId)
        {
            const string commandText = "CMS_NewsletterDetail_GetDataByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                _db.AddParameter(cmd, "newsLetterId", newsId);
                var numberOfRow = _db.Get<NewsletterDetailEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertNewsLetter(NewsletterEntity entity, ref int id)
        {
            const string commandText = "CMS_Newsletter_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Status", entity.Status);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsLetter(NewsletterEntity entity)
        {
            const string commandText = "CMS_Newsletter_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Status", entity.Status);
                _db.AddParameter(cmd, "Id", entity.ID);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertNewsLetterDetail(NewsletterDetailEntity entity)
        {
            const string commandText = "CMS_NewsletterDetail_Insert";
            try
            {
                var id = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsLetter_ID", entity.NewsLetter_ID);
                _db.AddParameter(cmd, "NewsId", entity.newsId);
                _db.AddParameter(cmd, "TitleNews", entity.Title);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "DisplayStyle", entity.DisplayStyle);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "Priority", entity.Priority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                id = Utility.ConvertToInt(cmd.Parameters[0]);
                return numberOfRow != 0 && id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsLetterDetail(NewsletterDetailEntity entity)
        {
            const string commandText = "CMS_NewsletterDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.newsId);
                _db.AddParameter(cmd, "TitleNews", entity.Title);
                _db.AddParameter(cmd, "Sapo", entity.Sapo);
                _db.AddParameter(cmd, "Avatar", entity.Avatar);
                _db.AddParameter(cmd, "DisplayStyle", entity.DisplayStyle);
                _db.AddParameter(cmd, "Url", entity.Url);
                _db.AddParameter(cmd, "Priority", entity.Priority);
                _db.AddParameter(cmd, "Id", entity.ID);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteNewsLetter(int id)
        {
            const string commandText = "CMS_Newsletter_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAllNewsLetterDetail(int newsLetterId)
        {
            const string commandText = "CMS_NewsletterDetail_DeleteAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsLetterId", newsLetterId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteNewsLetterDetail(int Id)
        {
            const string commandText = "CMS_NewsletterDetail_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(int newsLetterId, string listPriority)
        {
            const string commandText = "CMS_NewsletterDetail_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsLetterId", newsLetterId);
                _db.AddParameter(cmd, "listPriority", listPriority);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsletterDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
