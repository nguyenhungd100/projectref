﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx
{
    [DataContract]
    public class NewsRoyaltiesExEntity : EntityBase
    {        
        public long PNewsID { get; set; }                     
    }

    [DataContract]
    public class NewsCategoryEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
