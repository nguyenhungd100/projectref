﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.LogAnalytics
{
    public class LogAnalyticsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ActionName { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int ActionCount { get; set; }
    }
}
