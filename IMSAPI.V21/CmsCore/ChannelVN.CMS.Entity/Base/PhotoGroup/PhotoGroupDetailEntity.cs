﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.PhotoGroup
{
    [DataContract]
    public class PhotoGroupDetailEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string Avatar1 { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string RelatedLink { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string DistributionBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public string UnSignName { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public int PhotoGroupId { get; set; }
        [DataMember]
        public string SecondZoneId { get; set; }
        [DataMember]
        public List<PhotoGroupDetailInZonePhotoEntity> ListSecondZone { get; set; }
        [DataMember]
        public List<PhotoGroupDetailInZoneEntity> ListPhotoGroupZone { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
    }
    [DataContract]
    public enum EnumPhotoGroupDetailStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Temporary = 0,
        [EnumMember]
        WaitForPublish = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        Unpublished = 3
    }
    [DataContract]
    public class PhotoGroupDetailCountEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
    [DataContract]
    public class PhotoGroupDetailInPhotoTagEntity : EntityBase
    {
        [DataMember]
        public int PhotoGroupDetailId { get; set; }
        [DataMember]
        public int PhotoTagId { get; set; }
    }
}
