﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.PhotoGroup
{
    [DataContract]
    public class PhotoGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }        
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
