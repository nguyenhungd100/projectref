﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.LandingTemplate
{
    [DataContract]
    public class LandingTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Html { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int CategoryId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
    }

    [DataContract]
    public class LandingTemplateCategoryEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int ParentId { get; set; }        
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Type { get; set; }
    }

    public enum EnumStatusLandingTemplate
    {
        //all
        All = -1,
        //Lưu tạm
        Temporary = 0,
        //Xuất bản
        Published = 1,
        //Gỡ
        UnPublished = 2
    }

    public enum EnumTypeLandingTemplate
    {
        //all
        All = -1,
        //default
        Default = 0,
        //layout
        Layout = 1,
        //blog
        Blog = 2
    }

    public enum EnumStatusLandingTemplateCategory
    {
        //all
        All = -1,
        //UnActive
        UnActive = 0,
        //Active
        Active = 1        
    }
}
