﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewV3AnswersEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int QuestionId { get; set; }
        [DataMember]
        public int GuestId { get; set; }
        [DataMember]
        public string AnswersContent { get; set; }
        [DataMember]
        public DateTime AnswersDate { get; set; }
        [DataMember]
        public string AnswersBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }

    [DataContract]
    public class InterviewV3AnswersCustomEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string AnswersBy { get; set; }
        [DataMember]
        public string AnswersContent { get; set; }
        [DataMember]
        public int GuestId { get; set; }
        [DataMember]
        public int QuestionId { get; set; }
        [DataMember]
        public DateTime AnswersDate { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string OnlineUserName { get; set; }
        [DataMember]
        public string Desc { get; set; }
    }
}
