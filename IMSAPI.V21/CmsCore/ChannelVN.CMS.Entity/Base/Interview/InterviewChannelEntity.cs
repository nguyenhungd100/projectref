﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public enum EnumInterviewChannelRole : int
    {
        [EnumMember]
        Admin = 1,
        [EnumMember]
        Collector = 2,
        [EnumMember]
        Distributor = 3,
        [EnumMember]
        ChannelUser = 4,
        [EnumMember]
        Editor = 5,
        [EnumMember]
        Publisher = 6
    }

    [DataContract]
    public class InterviewChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int ChannelRole { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public string ChannelDescription { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string ChannelDetail { get; set; }

        [DataMember]
        public int QuestionCountWaitForCollect { get; set; }
        [DataMember]
        public int QuestionCountWaitForDistribute { get; set; }
        [DataMember]
        public int QuestionCountAnswer { get; set; }
        [DataMember]
        public int QuestionCountWaitForAnswer { get; set; }
        [DataMember]
        public int QuestionCountWaitForEdit { get; set; }
        [DataMember]
        public int QuestionCountWaitForPublish { get; set; }
        [DataMember]
        public int QuestionCountCollected { get; set; }
        [DataMember]
        public int QuestionCountDistributed { get; set; }
        [DataMember]
        public int QuestionCountPublished { get; set; }
        [DataMember]
        public int QuestionCountEdited { get; set; }
        [DataMember]
        public int QuestionCountAllPublished { get; set; }
        [DataMember]
        public int QuestionCountAllUnpublished { get; set; }
    }

}
