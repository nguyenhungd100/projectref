﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UnsignTitle { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public bool IsActived { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
    }
    [DataContract]
    public class InterviewDetailEntity : EntityBase
    {
        [DataMember]
        public InterviewEntity Interview { get; set; }
        [DataMember]
        public List<InterviewChannelEntity> InterviewChannels { get; set; }
    }
    [DataContract]
    public class InterviewWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public bool IsActived { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public List<InterviewChannelEntity> InterviewChannels { get; set; }
        [DataMember]
        public List<NewsForSuggestionEntity> NewsUseThisInterview { get; set; }
        [DataMember]
        public int QuestionCount { get; set; }
        [DataMember]
        public int QuestionCountWaitForCollect { get; set; }
        [DataMember]
        public int QuestionCountWaitForDistribute { get; set; }
        [DataMember]
        public int QuestionCountWaitForAnswer { get; set; }
        [DataMember]
        public int QuestionCountWaitForEdit { get; set; }
        [DataMember]
        public int QuestionCountWaitForPublish { get; set; }
        [DataMember]
        public int QuestionCountPublished { get; set; }
        [DataMember]
        public int QuestionCountUnpublished { get; set; }
    }
    [DataContract]
    public class InterviewForSuggestionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
