﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewV3GuestsEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public string OnlineUserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Desc { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public bool Status { get; set; }
    }
}
