﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewPublishedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public bool IsActived { get; set; }
        [DataMember]
        public string AscQuestionContent { get; set; }
        [DataMember]
        public string DescQuestionContent { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
    }
}
