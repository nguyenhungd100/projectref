﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewV3QuestionEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string ProcessUser { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Sex { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
    }

    public class InterviewV3QuestionDisplayListEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string ProcessUser { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public int TotalActiveAnswer { get; set; }
        [DataMember]
        public int TotalDisabledAnswer { get; set; }
    }

    [DataContract]
    public enum EnumInterviewV3QuestionType : int
    {
        [EnumMember]
        NormalQuestion = 0,
        [EnumMember]
        ExtensionData = 1
    }

    [DataContract]
    public enum EnumInterviewV3QuestionStatus : int
    {
        [EnumMember]
        WaitForCollect = 0,
        [EnumMember]
        WaitForDistribute = 1,
        [EnumMember]
        WaitForAnswer = 2,
        [EnumMember]
        WaitForPublish = 3,
        [EnumMember]
        Published = 4,
        [EnumMember]
        Unpublished = 5,
        [EnumMember]
        WaitForEdit = 6
    }
    public enum EnumInterviewV3QuestionOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        ReceivedDateDesc = 1,
        [EnumMember]
        DistributedDateDesc = 2,
        [EnumMember]
        EditedateDesc = 3,
        [EnumMember]
        PublishedDateDesc = 4,
        [EnumMember]
        LastModifiedDateDesc = 5,
        [EnumMember]
        PriorityAsc = 6
    }
}
