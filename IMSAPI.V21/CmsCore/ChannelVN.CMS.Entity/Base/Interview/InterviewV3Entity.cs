﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public class InterviewV3Entity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UnsignTitle { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public bool IsActived { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
    }

    [DataContract]
    public class InterviewV3DetailEntity : EntityBase
    {
        [DataMember]
        public InterviewV3Entity InterviewV3Info { get; set; }
        [DataMember]
        public List<InterviewV3QuestionEntity> QuestionList { get; set; }
    }
}
