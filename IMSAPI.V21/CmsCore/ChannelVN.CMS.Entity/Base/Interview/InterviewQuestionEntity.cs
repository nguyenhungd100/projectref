﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Interview
{
    [DataContract]
    public enum EnumInterviewQuestionType : int
    {
        [EnumMember]
        NormalQuestion = 0,
        [EnumMember]
        ExtensionData = 1
    }
    [DataContract]
    public enum EnumInterviewQuestionStatus :int
    {
        [EnumMember]
        WaitForCollect = 0,
        [EnumMember]
        WaitForDistribute = 1,
        [EnumMember]
        WaitForAnswer = 2,
        [EnumMember]
        WaitForPublish = 3,
        [EnumMember]
        Published = 4,
        [EnumMember]
        Unpublished = 5,
        [EnumMember]
        WaitForEdit = 6
    }
    [DataContract]
    public enum EnumInterviewQuestionOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        ReceivedDateDesc = 1,
        [EnumMember]
        DistributedDateDesc = 2,
        [EnumMember]
        EditedateDesc = 3,
        [EnumMember]
        PublishedDateDesc = 4,
        [EnumMember]
        LastModifiedDateDesc = 5,
        [EnumMember]
        PriorityAsc = 6
    }
    [DataContract]
    public enum EnumInterviewQuestionFieldFilterForUsername : int
    {
        [EnumMember]
        ReceivedBy = 1,
        [EnumMember]
        DistributedBy = 2,
        [EnumMember]
        EditedBy = 3,
        [EnumMember]
        PublishedBy = 4,
        [EnumMember]
        LastModifiedBy = 5,
        [EnumMember]
        ChannelUser = 6,
        [EnumMember]
        ProcessUser = 7
    }
    [DataContract]
    public class InterviewQuestionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Answer { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Sex { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime DistributedDate { get; set; }
        [DataMember]
        public string DistributedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public string ChannelAvatar { get; set; }
    }
    [DataContract]
    public class InterviewQuestionWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Sex { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ReceivedDate { get; set; }
        [DataMember]
        public string ReceivedBy { get; set; }
        [DataMember]
        public DateTime DistributedDate { get; set; }
        [DataMember]
        public string DistributedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public string ChannelUser { get; set; }
    }
    [DataContract]
    public class InterviewQuestionForPublishEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Answer { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int Sex { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Job { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public DateTime DistributedDate { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public string ChannelAvatar { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public class QuestionCountByChannelEntity : EntityBase
    {
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public int QuestionCount { get; set; }
    }
}
