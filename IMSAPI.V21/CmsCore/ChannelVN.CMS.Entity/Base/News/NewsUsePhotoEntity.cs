﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;
using ChannelVN.CMS.Entity.Base.Photo;

namespace ChannelVN.CMS.Entity.Base.News
{
    public class NewsUsePhotoEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public long PhotoPublishedId { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public PhotoPublishedEntity PhotoPublished { get; set; }
    }
}
