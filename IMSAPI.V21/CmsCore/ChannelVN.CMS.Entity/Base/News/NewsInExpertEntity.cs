﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.Entity.Base.News
{    
    [DataContract]
    public class NewsInExpertEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return NewsId.ToString(); } }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime SendDate { get; set; }
        [DataMember]
        public DateTime ConfirmDate { get; set; }
        [DataMember]
        public DateTime ReturnDate { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string ExpertName { get; set; }
        [DataMember]
        public string AccountName { get; set; }
    }
    [DataContract]
    public enum EnumApproveExpertStatus
    {
        [EnumMember]
        WaitingExpert = 0,
        [EnumMember]
        ConfirmExpert = 1,
        [EnumMember]
        ReturnExpert = 2
    }
}
