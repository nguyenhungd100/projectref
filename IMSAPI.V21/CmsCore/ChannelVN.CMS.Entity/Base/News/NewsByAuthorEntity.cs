﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsByAuthorEntity: EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
    }
}
