﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public enum EnumMaskSourceId { 
        [EnumMember]
        gameK = 10,
        [EnumMember]
        cafeBiz = 5,
        [EnumMember]
        genK = 7,
        [EnumMember]
        soha = 8,
        [EnumMember]
        cafeF = 4
    }
    [DataContract]
    public enum EnumMaskStatus
    {
        [EnumMember]
        Actived = 2,
        [EnumMember]
        Published = 3
    }

    [DataContract]
    public class NewsMaskOnlineEntity:EntityBase
    {
        [DataMember]
        public long News_ID { get; set; }
        [DataMember]
        public int Cat_ID { get; set; }
        [DataMember]
        public string News_Title { get; set; }
        [DataMember]
        public string News_Subtitle { get; set; }
        [DataMember]
        public string News_Image { get; set; }
        [DataMember]
        public string News_ImageNote { get; set; }
        [DataMember]
        public string News_Source { get; set; }
        [DataMember]
        public string News_InitialContent { get; set; }
        [DataMember]
        public string News_Content { get; set; }
        [DataMember]
        public string News_Author { get; set; }
        [DataMember]
        public string News_CurrEditor { get; set; }
        [DataMember]
        public string News_Approver { get; set; }
        [DataMember]
        public int News_Status { get; set; }
        [DataMember]
        public DateTime News_SwitchTime { get; set; }
        [DataMember]
        public DateTime News_PublishDate { get; set; }
        [DataMember]
        public bool News_isFocus { get; set; }
        [DataMember]
        public int News_Mode { get; set; }
        [DataMember]
        public int News_ViewNum { get; set; }
        [DataMember]
        public DateTime News_CreateDate { get; set; }
        [DataMember]
        public DateTime News_ModifiedDate { get; set; }
        [DataMember]
        public string News_Relation { get; set; }
        [DataMember]
        public double News_Rate { get; set; }
        [DataMember]
        public string News_OtherCat { get; set; }
        [DataMember]
        public bool isComment { get; set; }
        [DataMember]
        public bool isUserRate { get; set; }
        [DataMember]
        public int Template { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public int DomainAvatar { get; set; }
        [DataMember]
        public long SourceNewsID { get; set; }
        [DataMember]
        public int SourceID { get; set; }
    }
}
