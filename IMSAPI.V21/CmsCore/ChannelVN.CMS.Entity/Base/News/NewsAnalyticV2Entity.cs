﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsAnalyticV2Entity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public long LastUpdateViewCountId { get; set; }
        [DataMember]
        public DateTime LastUpdateViewCount { get; set; }
    }
}
