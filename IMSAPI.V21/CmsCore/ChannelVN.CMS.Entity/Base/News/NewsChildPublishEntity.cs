﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsChildPublishEntity : EntityBase
    {
        [DataMember]
        public long NewsParentId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
