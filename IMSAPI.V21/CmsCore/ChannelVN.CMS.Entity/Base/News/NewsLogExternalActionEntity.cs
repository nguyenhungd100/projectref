﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public enum EnumNewsLogExternalAction
    {
        [EnumMember]
        PegaBreakingNewsNotification = 1,
        [EnumMember]
        PegaHotNewsNotification = 2,
        [EnumMember]
        AppNewsNotification = 3
    }

    [DataContract]
    public class NewsLogExternalActionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ExternalActionType { get; set; }
        [DataMember]
        public DateTime ExternalActionDate { get; set; }
        [DataMember]
        public string ProcessedBy { get; set; }
    }
}
