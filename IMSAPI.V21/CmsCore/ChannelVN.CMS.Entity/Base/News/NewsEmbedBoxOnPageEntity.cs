﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsEmbedBoxOnPageEntity : EntityBase
    {
        [DataMember]
        public long ZoneId { get; set; }

        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }
    }

    [DataContract]
    public class NewsEmbedBoxOnPageListEntity : EntityBase
    {

        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int ViewCount { get; set; }
    }
}

