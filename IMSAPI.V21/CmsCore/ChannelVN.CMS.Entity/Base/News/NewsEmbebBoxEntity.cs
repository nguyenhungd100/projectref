﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsEmbebBoxEntity:EntityBase
    {
        [DataMember]
        public long News_ID { get; set; }
        [DataMember]
        public int ThuTu { get; set; }
        [DataMember]
        public int Type { get; set; }
    }

    [DataContract]
    public class NewsEmbedBoxListEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public long News_ID { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(News_ID); } set { } }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int ThuTu { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

}
