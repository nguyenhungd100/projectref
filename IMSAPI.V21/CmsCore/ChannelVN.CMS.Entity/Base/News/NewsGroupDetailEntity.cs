﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsGroupDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int GroupNewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int ZoneIdForNews { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int LikeShareCount { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public long TagSubTitleId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public int NewsType { get; set; }
    }
}
