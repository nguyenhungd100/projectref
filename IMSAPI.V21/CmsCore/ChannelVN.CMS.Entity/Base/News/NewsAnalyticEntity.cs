﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsAnalyticEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int ViewCountDaily { get; set; }
        [DataMember]
        public int ViewCountHourly { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public int ViewCountMobileDaily { get; set; }
        [DataMember]
        public int ViewCountMobileHourly { get; set; }
        [DataMember]
        public DateTime LastUpdateViewCount { get; set; }
    }
    [DataContract]
    public class NewsAnalyticV3Entity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
    }   
}
