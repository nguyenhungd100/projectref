﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsConfigGroupEntity : EntityBase
    {
        [DataMember]
        public string ConfigGroupKey { get; set; }
        [DataMember]
        public string ConfigGroupName { get; set; }
    }
}
