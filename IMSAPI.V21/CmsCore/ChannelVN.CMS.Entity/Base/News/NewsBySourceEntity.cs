﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsBySourceEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int SourceId { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int SourceType { get; set; }
    }
}
