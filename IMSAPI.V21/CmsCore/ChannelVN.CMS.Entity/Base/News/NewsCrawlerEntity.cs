﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsCrawlerEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Source { get; set; }
        [DataMember]
        public int SourcePega { get; set; }
        [DataMember]
        public int CatID { get; set; }
        [DataMember]
        public string NewsURL { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string ContentProcess { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public string HomeAvatar { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int AvatarWidth { get; set; }
        [DataMember]
        public int CodeStatus { get; set; }
        [DataMember]
        public int SohaStatus { get; set; }
        [DataMember]
        public string SiteName { get; set; }


    }

    [DataContract]
    public class CategoryCrawlerEntity : EntityBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int SiteID { get; set; }
        [DataMember]
        public int CatID { get; set; }
        [DataMember]
        public string CatName { get; set; }
        [DataMember]
        public string CatLink { get; set; }
        [DataMember]
        public string HotPattern { get; set; }
        [DataMember]
        public string LinkPattern { get; set; }
        [DataMember]
        public string LinkDetail { get; set; }
        [DataMember]
        public int CatPega { get; set; }
    }


    [DataContract]
    public class SiteCrawlerEntity : EntityBase
    {
        [DataMember]
        public int SiteID { get; set; }
        [DataMember]
        public int SourcePega { get; set; }
        [DataMember]
        public int SourceSoHa { get; set; }
        [DataMember]
        public string SiteLink { get; set; }
        [DataMember]
        public string SiteName { get; set; }
        [DataMember]
        public string Des { get; set; }
        [DataMember]
        public int DomainType { get; set; }

    }


#region


    //public class NewsCrawlerEntity : EntityBase
    //{

    //    public int newsID { get; set; }
    //    public string news_title { get; set; }
    //    public string news_des { get; set; }
    //    public string news_title_char { get; set; }
    //    public string news_des_char { get; set; }
    //    public string news_link { get; set; }
    //    public DateTime news_date { get; set; }
    //    public int news_angent_id { get; set; }
    //    public int news_key_id { get; set; }
    //    public string news_content { get; set; }
    //    public string news_image { get; set; }
    //    public int news_status { get; set; }
    //    public string news_url { get; set; }
    //    public string news_main_tag { get; set; }
    //    public string news_tag { get; set; }
    //    public string news_sitemap { get; set; }
    //    public int news_crawl { get; set; }
    //    public int news_follow { get; set; }
    //    public int news_load { get; set; }
    //    public int news_duyet { get; set; }
    //    public int news_non_html { get; set; }
    //    public DateTime datecreate { get; set; }
    //    public int category { get; set; }
    //    public int click { get; set; }
    //    public int status { get; set; }
    //    public bool is_move_cms { get; set; }

    //}

    //public class CategoryCrawlerEntity : EntityBase
    //{
    //    public int Cat_ID { get; set; }
    //    public int Cat_ParentID { get; set; }
    //    public string Cat_Name { get; set; }
    //    public int HLevel { get; set; }
    //    public string Row { get; set; } 
    //}
     

    //public class NewsAngentCrawlerEntity : EntityBase
    //{
    //    public int news_agent_id { get; set; }
    //    public string news_angent_link { get; set; }
    //    public string news_angent_name { get; set; }
    //    public int news_angent_status { get; set; }

    //}
#endregion  
}
