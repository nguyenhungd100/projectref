﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsPrEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ContentId { get; set; }
        [DataMember]
        public long DistributionId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public int Duration { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long BookingId { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int ViewPlusBannerId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
    public class NewsPrCurrentDateEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Type { get; set; }
    }
    public class ViewPlusEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int BannerId { get; set; }
        [DataMember]
        public int BidType { get; set; }
        [DataMember]
        public int Budget { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public DateTime EndTime { get; set; }
        [DataMember]
        public string ReturnMessage { get; set; }
    }
    public class ViewPlusListEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int BannerId { get; set; }
        [DataMember]
        public int BidType { get; set; }
        [DataMember]
        public int Budget { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class ZoneViewPlusEntity : EntityBase
    {
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string TypeName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int Web { get; set; }
        [DataMember]
        public int Mobile { get; set; }
        [DataMember]
        public int ShowTime { get; set; }
        [DataMember]
        public int TimeToPost { get; set; }
    }
}
