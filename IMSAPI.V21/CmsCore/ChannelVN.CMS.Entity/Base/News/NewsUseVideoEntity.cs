﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsUseVideoEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public long VideoId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string Pname { get; set; }             
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        public DateTime DistributionDate { get; set; }      
        [DataMember]
        public string Url { get; set; }        
        // VideoFileInfo
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        // For advertisement
        [DataMember]
        public bool AllowAd { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
