﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsLabelEntity : EntityBase
    {
        [DataMember]
        public int Id;
        [DataMember]
        public int UserId;
        [DataMember]
        public string LabelName;
        [DataMember]
        public string LabelColor;
        [DataMember]
        public bool IsSystemLabel;
        [DataMember]
        public int Priority;
    }
}
