﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string GroupNewsName { get; set; }
        [DataMember]
        public string GroupNewsDescription { get; set; }
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public bool IsFilterByZone { get; set; }
    }
}
