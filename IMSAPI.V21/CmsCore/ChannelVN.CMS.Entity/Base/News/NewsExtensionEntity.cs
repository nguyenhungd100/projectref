﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public enum EnumNewsExtensionType
    {
        #region All CMS
        [EnumMember]
        InterviewId = 1,
        [EnumMember]
        RollingNewsId = 2,
        [EnumMember]
        LiveMatchId = 3,
        [EnumMember]
        CrawlerSystemNewsId = 4,
        [EnumMember]
        NewsDetailTemplate = 10,
        [EnumMember]
        InteractiveTemplate = 17,
        [EnumMember]
        IsGoogleNews = 18,
        [EnumMember]
        IsFacebookArticle = 19,
        [EnumMember]
        LandingPageId = 20,
        [EnumMember]
        IsSensitiveNews = 21,
        [EnumMember]
        BigstoryId = 25,
        [EnumMember]
        FacebookArticle = 26,
        [EnumMember]
        IsAMP = 27,
        [EnumMember]
        GeneralDeepArticle = 28,
        [EnumMember]
        GeneralDeepArticleData = 29,
        [EnumMember]
        PictureAuthor = 30,
        [EnumMember]
        ZaloArticle = 31,
        [EnumMember]
        SlideAlbum = 32,
        #endregion

        #region Magazine                
        [EnumMember]
        MagazineType = 300,
        [EnumMember]
        MagazineWebUrl = 3001,
        [EnumMember]
        MagazineMobileUrl = 3002,
        [EnumMember]
        ShowMagazineHeader = 3003,
        [EnumMember]
        MagazineCoverMobile = 3004,
        [EnumMember]
        MagazineMiniCover = 3005,
        [EnumMember]
        MagazineBackgroundColor = 3006,
        [EnumMember]
        MagazineTextColor = 3007,
        [EnumMember]
        MagazineVideoCover = 21061,
        [EnumMember]
        MagazineVideoCoverText = 21062,
        #endregion

        #region VNEconomy        
        [EnumMember]
        ShowAvatar = 2410,
        #endregion

        #region TTO
        [EnumMember]
        TuoiTre_TourContent = 2000,
        [EnumMember]
        TuoiTre_RattingContent = 2001,
        [EnumMember]
        TuoiTre_AvatarBgColor = 2002,
        [EnumMember]
        TuoiTre_Slug = 2003,
        [EnumMember]
        TuoiTre_School = 2004,
        #endregion

        #region For CafeF
        [EnumMember]
        CafeF_RelatedStockSymbol = 5001,
        [EnumMember]
        CafeF_RelatedCeoCode = 5002,
        [EnumMember]
        CafeF_RelatedExtension = 5003,
        [EnumMember]
        CafeF_RelatedDisplay = 5004,
        #endregion

        #region For VTV
        [EnumMember]
        VTV_LiveChannelID = 6001,
        [EnumMember]
        VTV_GuestLive = 6002,
        [EnumMember]
        VTV_ProgramScheduleDetail = 6003,
        [EnumMember]
        VTV_AvatarCoverMagazine = 6004,
        #endregion        

        #region For SohaNews
        [EnumMember]
        SohaNews_DonateCode = 7001,
        [EnumMember]
        SohaNews_ShowBlinkText = 7002,
        [EnumMember]
        SohaNews_NB24h = 7003,
        [EnumMember]
        SohaNews_RecommendNews = 7004,
        [EnumMember]
        SohaNews_MiniCover = 7005,
        [EnumMember]
        SohaNews_BackgroundArticle = 7006,
        [EnumMember]
        SohaNews_TextColor = 7007,
        #endregion

        #region For Kenh14
        [EnumMember]
        LaCoolPhotoList = 8001,
        [EnumMember]
        OverviewNewsList = 8002,
        [EnumMember]
        FooterContentRelatedNews = 8003,
        [EnumMember]
        ShowPenName = 8004,
        [EnumMember]
        FullWidthAvatar = 8005,
        [EnumMember]
        MagazineId = 8006,
        [EnumMember]
        KeepPhotoHeight = 8007,
        [EnumMember]
        Kenh14_MiniCover = 8008,
        [EnumMember]
        Kenh14_BackgroundArticle = 8009,
        [EnumMember]
        Kenh14_FontColorArticle = 8010,
        #endregion

        #region For GameK
        [EnumMember]
        GameK_GameId = 9000,
        [EnumMember]
        NewMagazineDistributionId = 9002,
        [EnumMember]
        GameK_NewsTemplateColumnLeft = 9500,
        [EnumMember]
        GameK_GiftCode_CardId = 9501,
        [EnumMember]
        GameK_Live_Match = 9502,       
        #endregion

        #region GenK
        [EnumMember]
        Genk_NewsReview = 9009,
        #endregion

        #region Afamily
        [EnumMember]
        AFamily_HorizontalAvatar = 10000,
        #endregion

        #region suckhoehangngay
        [EnumMember]
        InboundTemplateId = 33,        
        [EnumMember]
        LastVersionNewsOfExpert = 52,
        #endregion
                
        #region Vod
        [EnumMember]
        LiveStreamEmbed = 42,
        [EnumMember]
        LiveVODInfo = 43,
        #endregion

    }
    [DataContract]
    public enum EnumNewsLabelType
    {
        [EnumMember]
        LiveLabel = 1,
        [EnumMember]
        UpdateLabel = 2,
        [EnumMember]
        EditorChoiceLabel = 3,
    }
    [DataContract]
    public class NewsExtensionEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public int NumericValue { get; set; }
    }
        
}
