﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public enum EnumBoxBannerDisplayStyle
    {
        [EnumMember]
        AvatarOnly = 0,
        [EnumMember]
        TitleOnly = 1,
        [EnumMember]
        AvatarAndTitle = 2,
        [EnumMember]
        AvatarAndTitleAndSapo = 3
    }
    [DataContract]
    public enum EnumBoxBannerStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Inactived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public class BoxBannerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public List<BoxBannerInZoneEntity> ListInZone { get; set; }
    }
    [DataContract]
    public enum EnumBoxBannerType
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Link = 1,
        [EnumMember]
        News = 2,
        [EnumMember]
        Tag = 3,
        [EnumMember]
        Thread = 4,
        [EnumMember]
        Poll = 5,
        [EnumMember]
        Quotation = 6
    }
    [DataContract]
    public class BoxBannerZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }

    }
    [DataContract]
    public class BoxBannerInZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int BoxBannerId { get; set; }        
    }

    [DataContract]
    public class BoxInboundComponentEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int Status { get; set; }        
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string NewsIdStr { get { return NewsId.ToString(); } }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string Embed { get; set; }
        [DataMember]
        public string DataJson { get; set; }
    }

    [DataContract]
    public class BoxInboundTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TypeId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public string ListTopicId { get; set; }
        [DataMember]
        public string ListThreadId { get; set; }
        [DataMember]
        public string DataJson { get; set; }
        [DataMember]
        public string DataEmbed { get; set; }
    }

    [DataContract]
    public class BoxLivePageEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PageUrl { get; set; }
        [DataMember]
        public string PageName { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }                       
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatdBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }        
        [DataMember]
        public string DataJson { get; set; }
    }
}
