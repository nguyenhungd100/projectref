﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsNotificationEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int NewsStatus { get; set; }
        [DataMember]
        public int LabelId { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public DateTime LastNotifiedDate { get; set; }
        [DataMember]
        public bool IsWarning { get; set; }
        [DataMember]
        public string LabelName { get; set; }
        [DataMember]
        public string LabelColor { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}
