﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsAuthorEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserData { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public byte AuthorType { get; set; }
        [DataMember]
        public string OriginalName { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int NewsRoyaltiesRoleId { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }
        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public enum AuthorType
    {
        [EnumMember]
        Default = 1, //map với account hệ thống
        [EnumMember]
        AccountMing = 2,// map với ming
        [EnumMember]
        UserDefine = 3//không map với tài khoản nào cả
    }
}
