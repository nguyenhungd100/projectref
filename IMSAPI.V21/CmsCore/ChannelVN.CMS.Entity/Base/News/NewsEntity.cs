﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.Base.Nodejs;
using ChannelVN.Kenh14.Entity;
using ChannelVN.CMS.Entity.Base.Topic;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public enum NewsWaitRepublishStatus
    {
        [EnumMember]
        All = 0,
        [EnumMember]
        Wating = 1,
        [EnumMember]
        Processed = 2
    }
    [DataContract]
    public enum NewsActionType
    {
        [EnumMember]
        AllAction = -1,
        [EnumMember]
        Addnew = 0,
        [EnumMember]
        Update = 1,
        [EnumMember]
        Delete = 2,
        [EnumMember]
        SendToEdit = 3,
        [EnumMember]
        SendToPublish = 4,
        [EnumMember]
        ReturnEditor = 5,
        [EnumMember]
        ReturnReporter = 6,
        [EnumMember]
        ReceiveToEdit = 7,
        [EnumMember]
        ReceiveToPublish = 8,
        [EnumMember]
        Publish = 9,
        [EnumMember]
        UnPublish = 10,
        [EnumMember]
        SetNewsPosition = 11,
        [EnumMember]
        SwapNewsPosition = 12,
        [EnumMember]
        BombNewsPosition = 13,
        [EnumMember]
        NewsSlide = 14,
        [EnumMember]
        SendToEditorialBoard = 15,
        [EnumMember]
        ErrorCheck = 16,
        [EnumMember]
        InsertDerectTag = 17,
        [EnumMember]
        DeleteDerectTag = 18,
        [EnumMember]
        UpdateStausNewsPR = 19
    }

    [DataContract]
    public enum NewsStatus
    {
        /// <summary>
        /// Không xác định
        /// </summary>
        [EnumMember]
        Unknow = -1,
        /// <summary>
        /// Tất cả các trạng thái
        /// </summary>
        [EnumMember]
        All = 0,
        /// <summary>
        /// Lưu tạm
        /// </summary>
        [EnumMember]
        Temporary = 1,
        /// <summary>
        /// Chờ biên tập
        /// </summary>
        [EnumMember]
        WaitForEdit = 2,
        /// <summary>
        /// Nhận biên tập
        /// </summary>
        [EnumMember]
        ReceivedForEdit = 3,
        /// <summary>
        /// Trả lại phóng viên
        /// </summary>
        [EnumMember]
        ReturnedToReporter = 4,
        /// <summary>
        /// Chờ xuất bản
        /// </summary>
        [EnumMember]
        WaitForPublish = 5,
        /// <summary>
        /// Nhận xuất bản
        /// </summary>
        [EnumMember]
        ReceivedForPublish = 6,
        /// <summary>
        /// Trả lại biên tập viên
        /// </summary>
        [EnumMember]
        ReturnedToEditor = 7,
        /// <summary>
        /// Xuất bản
        /// </summary>
        [EnumMember]
        Published = 8,
        /// <summary>
        /// Xóa tạm
        /// </summary>
        [EnumMember]
        MovedToTrash = 9,
        /// <summary>
        /// Gỡ xuất bản
        /// </summary>
        [EnumMember]
        Unpublished = 10,
        /// <summary>
        /// Trả về Cộng tác viên
        /// </summary>
        [EnumMember]
        ReturnToCooperator = 11,
        /// <summary>
        /// Đợi tổng biên tập duyệt
        /// </summary>
        [EnumMember]
        WaitForEditorialBoard = 14,
        /// <summary>
        /// Tổng biên tập nhật duyệt
        /// </summary>
        [EnumMember]
        ReceivedForEditorialBoard = 16,
        /// <summary>
        /// Ban biên tập trả thư ký
        /// </summary>
        [EnumMember]
        ReturnedToEditorialSecretary = 19,
        /// <summary>
        /// Trả lại đích danh biên tập viên
        /// </summary>
        [EnumMember]
        ReturnedToMyEditor = 20,
        /// <summary>
        /// crawler tin bài
        /// </summary>
        [EnumMember]
        CrawlerNews = 21,
        /// <summary>
        /// crawler by vccorp
        /// </summary>
        [EnumMember]
        CrawlerByVccorp = 22
    }

    [DataContract]
    public enum NewsType
    {
        [EnumMember]
        All = -1,
        //Bài thường
        [EnumMember]
        Normal = 0,
        [EnumMember]
        SlidePhoto = 1,
        //Bài video
        [EnumMember]
        Video = 2,
        //Bài ảnh
        [EnumMember]
        Image = 3,
        [EnumMember]
        PR = 4,
        //Hệ thống ngoài // External CMS
        [EnumMember]        
        EXT = 5,
        //LiveInteractive
        [EnumMember]
        Live = 6,
        //Bài cả ảnh và video
        [EnumMember]
        ImageAndVideo = 7,
        //Quiz
        [EnumMember]
        Quizz = 8,
        //Giao lưu trực tuyến
        [EnumMember]
        Interview = 9,
        //Bài tường thuật sự kiện
        [EnumMember]
        RollingNews = 10,
        //Bài tường thuật bóng đá
        [EnumMember]
        LiveMatch = 11,
        //Không hiện lên chuyên mục
        [EnumMember]
        InvisibledNews = 12,
        //Video tự chạy
        [EnumMember]
        VideoAutoPlay = 13,
        //SlideShow
        [EnumMember]
        SlideShow = 14,
        //NoAvatarOnDetail
        [EnumMember]
        NoAvatarOnDetail = 15,
        //size L
        [EnumMember]
        FullContentWidth = 16,
        //Interactive
        [EnumMember]
        Interactive = 17,
        //LandingPage
        [EnumMember]
        LandingPage = 18,
        //Trắc nghiệm KP đúng sai
        [EnumMember]
        Survey = 19,
        //Infographic
        [EnumMember]
        Infographic = 20,
        //EmbedCode
        [EnumMember]
        FakeNewsUrl = 21,
        //GiftCode
        [EnumMember]
        Quiz = 22,
        //LiveFacebook
        [EnumMember]
        LiveFacebook = 23,
        //BigStory
        [EnumMember]
        BigStory = 25,
        //FacebookArticle
        [EnumMember]
        FacebookArticle = 26,
        //Magazine
        [EnumMember]
        Magazine = 27,
        //MiniMagazine
        [EnumMember]
        MiniMagazine = 28,
        //Photopia
        [EnumMember]
        Photopia = 31,

        // For SohaNews -> bor
        //[EnumMember]
        //SohaNews_QuestionAndAnswer = 81,

        //Bài Size S: Có thể bạn quan tâm
        [EnumMember]
        SizeS = 81,
        //Bài Size S: Multipart
        [EnumMember]
        SizeS_Multipart = 82,
        //Bài Size S: Review
        [EnumMember]
        SizeS_Review = 83,
        //Bài Size S: Sự kiện
        [EnumMember]
        SizeS_Event = 84,

        // For Kenh14
        //Độc giả gửi ảnh Facebook
        [EnumMember]
        Kenh14_SentPhotoViaFacebook = 91,
        //We Send
        [EnumMember]
        Kenh14_WeSent = 92,
        //We Send + Độc giả gửi ảnh Facebook
        [EnumMember]
        Kenh14_SentPhotoViaFacebookAndWeSent = 93,
        //Vote VietnamIdol
        [EnumMember]
        Kenh14_VoteVietnamIdol = 94,
        //Bài Magazine (không dùng)
        [EnumMember]
        Kenh14_Magazine = 95,
        //Bài Sponsor
        [EnumMember]
        NewsSponsor = 98,
        //Bài vùng nội dung Small
        [EnumMember]
        SizeS_Small = 99,

        [EnumMember]
        NewsPr = 999,
    }

    [DataContract]
    public enum NewsType2
    {
        //Không hiển thị icon
        [EnumMember]
        NoShowIcon = 0,
        //Hiển thị icon ảnh
        [EnumMember]
        ShowIconPhoto = 1,
        //Hiển thị icon video
        [EnumMember]
        ShowIconVideo = 2,
        //Hiển thị icon ảnh và video
        [EnumMember]
        ShowIconImageAndPhoto = 3,
        //GiftCode
        [EnumMember]
        GiftCode = 4
    }

    [DataContract]
    public enum EnumNewsDisplayPosition
    {
        [EnumMember]
        NoPosition = 0,
        [EnumMember]
        SpecialOnHome = 1,
        [EnumMember]
        FocusOnCategory = 2
    }

    [DataContract]
    public enum NewsSortExpression
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        DistributionDateDesc = 2,
        [EnumMember]
        DistributionDateAsc = 3,
        [EnumMember]
        TitleDesc = 4,
        [EnumMember]
        TitleAsc = 5,
        [EnumMember]
        ViewCountDesc = 6,
        [EnumMember]
        ViewCountAsc = 7,
        [EnumMember]
        LastModifiedDateDesc = 8,
        [EnumMember]
        LastModifiedDateAsc = 9
    }

    [DataContract]
    public enum NewsFilterFieldForUsername
    {
        [EnumMember]
        CreatedBy = 0,
        [EnumMember]
        LastModifiedBy = 1,
        [EnumMember]
        PublishedBy = 2,
        [EnumMember]
        EditedBy = 3,
        [EnumMember]
        LastReceiver = 4,
        [EnumMember]
        ApprovedBy = 5,
        [EnumMember]
        ReturnedBy = 6,
        [EnumMember]
        SentBy = 7,
        [EnumMember]
        ErrorCheckedBy = 8,
        [EnumMember]
        SensitiveCheckedBy = 9
    }

    [DataContract]
    public class NewsEntity : EntityBase
    {
        public NewsEntity()
        {
            Price = 0;
            CreatedDate = DateTime.Now;
            LastModifiedDate = DateTime.Now;
            Status = 0;
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string StrId
        {
            get
            {
                return Id.ToString();
            }
            set { }
        }
        [DataMember]        
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string TagPrimary { get; set; }
        [DataMember]
        public decimal? Price { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }

        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }

        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string TagItem { get; set; }

        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public string InitSapo { get; set; }

        [DataMember]
        public string TemplateName { get; set; }
        [DataMember]
        public string TemplateConfig { get; set; }

        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public int PrPosition { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public string PrBookingNumber { get; set; }

        [DataMember]
        public bool IsBreakingNews { get; set; }

        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public int PegaBreakingNews { get; set; }

        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public long QuickNewsVietId { get; set; }

        [DataMember]
        public long CmsAccountVietId { get; set; }

        [DataMember]
        public string PenName { get; set; }

        [DataMember]
        public bool IsActivePenName { get; set; }

        [DataMember]
        public bool IsShowPenNameCTV { get; set; }

        [DataMember]
        public int ContentFooterType { get; set; }

        [DataMember]
        public int LocationType { get; set; }

        [DataMember]
        public DateTime ExpiredDate { get; set; }
        [DataMember]
        public string SourceURL { get; set; }

        [DataMember]
        public decimal? BonusPrice { get; set; }

        [DataMember]
        public string ShortTitle { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }        
        //chinhnb add
        [DataMember]
        public string ApprovedBy { get; set; }
        [DataMember]
        public DateTime ApprovedDate { get; set; }
        [DataMember]
        public string ReturnedBy { get; set; }
        [DataMember]
        public string SentBy { get; set; }
        [DataMember]
        public string ErrorCheckedBy { get; set; }
        [DataMember]
        public DateTime ErrorCheckedDate { get; set; }
        [DataMember]
        public string SensitiveCheckedBy { get; set; }
        [DataMember]
        public DateTime SensitiveCheckedDate { get; set; }
        [DataMember]
        public bool IsProd { get; set; }
    }

    [DataContract]
    public class NewsDetailForEditEntity : EntityBase
    {
        [DataMember]
        public NewsEntity NewsInfo { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllZone { get; set; }
        [DataMember]
        public List<NewsInZoneEntity> NewsInZone { get; set; }
        [DataMember]
        public List<TagNewsWithTagInfoEntity> TagInNews { get; set; }
        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelation { get; set; }
        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelationSpecial { get; set; }
        [DataMember]
        public List<NewsByAuthorEntity> NewsByAuthor { get; set; }
        [DataMember]
        public List<NewsBySourceEntity> NewsBySource { get; set; }
        [DataMember]
        public List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> ListVersion { get; set; }
        [DataMember]
        public List<NewsChildEntity> ListNewsChild { get; set; }
        [DataMember]
        public List<NewsExtensionEntity> NewsExtensions { get; set; }
        [DataMember]
        public string NewsContentWithTemplate { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public string ExpertQuote { get; set; }
        [DataMember]
        public VoteEntity VoteInfo { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public List<TopicInNews> TopicInNews { get; set; }
        [DataMember]
        public List<BrandContentEntity> BrandContent { get; set; }
        [DataMember]
        public List<ThreadInNewsEntity> ThreadNewsInfo { get; set; }
        [DataMember]
        public NewsPrEntity NewsPr { get; set; }
        [DataMember]
        public List<StickerEntity> Sticker { get; set; }
        [DataMember]
        public NewsParentEntity ParentNews { get; set; }
    }

    [DataContract]
    public class NewsInListEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime ApprovedDate { get; set; }
        [DataMember]
        public string ApprovedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public long ActiveUsers { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int WordCount { get; set; }

        //For BoxBanner Soha
        [DataMember]
        public int InterviewId { get; set; }

        // Temp for is Validate Auto, GameK, GenK
        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public decimal BonusPrice { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        // K14
        [DataMember]
        public int MobileCount { get; set; }
        [DataMember]
        public int PictureCount { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }

        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public long ParentNewsId { get; set; }        
    }

    [DataContract]
    public class NewsForExportEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Comments { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public decimal BonusPrice { get; set; }
    }

    [DataContract]
    public class NewsForOtherAPIEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string ZoneShortUrl { get; set; }
        [DataMember]
        public string Url { get; set; }
    }

    [DataContract]
    public class NewsInListWithNotifyEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public bool IsNotify { get; set; }
        [DataMember]
        public bool IsWarning { get; set; }
        [DataMember]
        public bool IsRead { get; set; }
        [DataMember]
        public string LabelName { get; set; }
        [DataMember]
        public string LabelColor { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
        [DataMember]
        public int MobileCount { get; set; }
        [DataMember]
        public int PictureCount { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
    }

    [DataContract]
    public class NewsForValidateEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string ListZoneId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string LastReceiver { get; set; }
    }

    public class NewsInfoForCachedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int PrimaryZoneId { get; set; }
        [DataMember]
        public int PrimaryZoneParentId { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public string PrimaryZoneUrl { get; set; }
        [DataMember]
        public string PrimaryZoneParentUrl { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class NewsCounterEntity : EntityBase
    {
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
    [DataContract]
    public class CategoryCounterEntity : EntityBase
    {
        [DataMember]
        public int TotalRow { get; set; }
        [DataMember]
        public int Id { get; set; }
    }

    [DataContract]
    public class NewsForRoyaltiesEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string StrId
        {
            get
            {
                return Id.ToString();
            }
            set { }
        }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int LikeCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string NoteRoyalties { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string NewsRoyaltiesType { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public decimal BonusPrice { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public int PictureType { get; set; }
        [DataMember]
        public string QualityFactor { get; set; }
        [DataMember]
        public string ContributionFactor { get; set; }
        [DataMember]
        public string RiskFactor { get; set; }
        [DataMember]
        public int NumberOfPicture { get; set; }
        [DataMember]
        public string PenName { get; set; }
        [DataMember]
        public string Rate { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string PictureTypeName { get; set; }
        [DataMember]
        public int NewsCategory { get; set; }
        [DataMember]
        public string PrPrice { get; set; }
        [DataMember]
        public string TotalAmount { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
        [DataMember]
        public string UserPicture { get; set; }
        [DataMember]
        public string NewNews { get; set; }
        [DataMember]
        public string TranslatedNews { get; set; }
        [DataMember]
        public string Edited1News { get; set; }
        [DataMember]
        public string Edited2News { get; set; }
        [DataMember]
        public string EditedNews { get; set; }
        [DataMember]
        public string NewArticle { get; set; }
        [DataMember]
        public string TranslatedArticle { get; set; }
    }

    [DataContract]
    public class NewsRoyaltiesV2Entity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }        
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public bool IsProd { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
    [DataContract]
    public class NewsRoyaltiesV2OutEntity : EntityBase
    {
        [DataMember]
        public long id { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string original_url { get; set; }
        [DataMember]
        public int zone_id { get; set; }
        [DataMember]
        public string author { get; set; }
        [DataMember]
        public string source { get; set; }
        [DataMember]
        public int type { get; set; }
        [DataMember]
        public int thread_id { get; set; }
        [DataMember]
        public string created_date { get; set; }
        [DataMember]
        public string distribution_date { get; set; }
        [DataMember]
        public string last_modified_date { get; set; }
        [DataMember]
        public string created_by { get; set; }
        [DataMember]
        public bool is_pr { get; set; }
        [DataMember]
        public int wordCount { get; set; }
        [DataMember]
        public int is_prod { get; set; }
        [DataMember]
        public int origin_id { get; set; }
        [DataMember]
        public int status { get; set; }
    }

    public class NewsForSuggestionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
    }

    public class NewsWithAnalyticEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId
        {
            get
            {
                return NewsId.ToString();
            }
        }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int ViewCountHourly { get; set; }
        [DataMember]
        public int ViewCountDaily { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int ActiveUser { get; set; }
    }
    public class NewsWithExpertEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return NewsId.ToString(); } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public decimal Price { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }

    public class NewsWithDistributionDateEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        public DateTime DistributionDate { get; set; }
    }
    public class NewsUnPublishEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public bool IsPr { get; set; }

    }
    public class NewsHotEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long newsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(newsId); } set { } }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string categoryName { get; set; }
        [DataMember]
        public int rank { get; set; }
        [DataMember]
        public string predictDesc { get; set; }
        [DataMember]
        public string pictureUrl { get; set; }
        [DataMember]
        public string categoryHref { get; set; }
        [DataMember]
        public string predictColor { get; set; }
        [DataMember]
        public string sapo { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public DateTime publishDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public bool inCover { get; set; }
        [DataMember]
        public bool sticked { get; set; }
    }

    public class SeoerNewsEntity
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string MetaTitle { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaDescription { get; set; }
        [DataMember]
        public string ReviewerName { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public List<TagNewsWithTagInfoEntity> TagInNews { get; set; }
        [DataMember]
        public int ReviewStatus { get; set; }
    }

    public class ExpertNewsEntity
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Body { get; set; }        
        [DataMember]
        public string ExpertName { get; set; }
        [DataMember]
        public string AccountName { get; set; }
    }

    public enum SEOMetaNewsReviewStatusEnum
    {
        //btv đã review
        Reviewed = 1,
        //chờ btv review
        ReviewWaiting =2        
    }

    public class NewsFullLogEntity
    {
        [DataMember]
        public string SourceId { get; set; }
        [DataMember]
        public string SourceName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ActionText { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string OwnerId { get; set; }

    }
    public class NewsViewPlusListEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int WordCount { get; set; }

        [DataMember]
        public bool IsOnMobile { get; set; }

        [DataMember]
        public int MobileCount { get; set; }
        [DataMember]
        public int ViewPlusId { get; set; }
        [DataMember]
        public int BannerId { get; set; }
        [DataMember]
        public DateTime StartTime { get; set; }
        [DataMember]
        public DateTime EndTime { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    public class NewsDashBoardUserEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }

    public class NewsInTopicSearch
    {
        public int TotalRow { get; set; }
        public List<NewsEntity> News { get; set; }
    }

    public class NewsHotCafeFEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public int Rank { get; set; }
        [DataMember]
        public string PredictDesc { get; set; }
        [DataMember]
        public string PictureUrl { get; set; }
        [DataMember]
        public string CategoryHref { get; set; }
        [DataMember]
        public string PredictColor { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public bool inCover { get; set; }
        [DataMember]
        public bool sticked { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public string ExpertName { get; set; }
        [DataMember]
        public string JobTitle { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool DisplayJobTitle { get; set; }
        [DataMember]
        public string Quote { get; set; }
        [DataMember]
        public string AvatarExpert { get; set; }
        [DataMember]
        public string Value { get; set; }

    }
    [DataContract]
    public class NewsParentEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string ParentNewsId { get { return Id.ToString(); } set { } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }        
    }
    [DataContract]
    public class NewsAppBotEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }        
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }
    public class NewsSimpleExpertEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return Id.ToString(); } }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }        
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }        
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string NoteExpert { get; set; }
        [DataMember]
        public DateTime SendDateExpert { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public int StatusExpert { get; set; }
    }
}
