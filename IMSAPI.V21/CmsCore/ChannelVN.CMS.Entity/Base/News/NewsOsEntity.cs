﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsOsEntity : EntityBase
    {
        [DataMember]
        public long News_ID { get; set; }

        [DataMember]
        public int Cat_ID { get; set; }


        [DataMember]
        public string News_Title { get; set; }


        [DataMember]
        public string News_Subtitle { get; set; }

        [DataMember]
        public string News_Image { get; set; }

        [DataMember]
        public string News_ImageNote { get; set; }

        [DataMember]
        public string News_Source { get; set; }

        [DataMember]
        public string News_InitialContent { get; set; }
        [DataMember]
        public string News_SubInitialContent { get; set; }

        [DataMember]
        public string News_Content { get; set; }

        [DataMember]
        public string News_Author { get; set; }
        [DataMember]
        public string News_CurrEditor { get; set; }

        [DataMember]
        public string News_Approver { get; set; }

        [DataMember]
        public int News_Status { get; set; }

        [DataMember]
        public DateTime News_SwitchTime { get; set; }

        [DataMember]
        public DateTime News_PublishDate { get; set; }

        [DataMember]
        public bool News_isFocus { get; set; }

        [DataMember]
        public int News_Mode { get; set; }

        [DataMember]
        public int News_ViewNum { get; set; }

        [DataMember]
        public DateTime News_CreateDate { get; set; }

        [DataMember]
        public DateTime News_ModifiedDate { get; set; }

        [DataMember]
        public string News_Relation { get; set; }

        [DataMember]
        public decimal News_Rate { get; set; }

        [DataMember]
        public string News_OtherCat { get; set; }

        [DataMember]
        public bool isComment { get; set; }

        [DataMember]
        public bool isUserRate { get; set; }

        [DataMember]
        public int Template { get; set; }

        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public long ViewCount { get; set; }

        [DataMember]
        public string Icon { get; set; }

        [DataMember]
        public string Extension1 { get; set; }

        [DataMember]
        public string Extension2 { get; set; }

        [DataMember]
        public string Extension3 { get; set; }

        [DataMember]
        public int Extension4 { get; set; }

        [DataMember]
        public string LastSender { get; set; }

        [DataMember]
        public string LastReceiver { get; set; }

        [DataMember]
        public int Flag { get; set; }

        [DataMember]
        public int NewsFromSource { get; set; }

        [DataMember]
        public string FileAttachPath { get; set; }

    }

    [DataContract]
    public class ListNewsOsEntity : EntityBase
    {
        [DataMember]
        public string News_ID { get; set; }

        [DataMember]
        public int Cat_ID { get; set; }


        [DataMember]
        public string News_Title { get; set; }


        [DataMember]
        public string News_Subtitle { get; set; }

        [DataMember]
        public string News_Image { get; set; }

        [DataMember]
        public string News_ImageNote { get; set; }

        [DataMember]
        public string News_Source { get; set; }

        [DataMember]
        public string News_InitialContent { get; set; }
        [DataMember]
        public string News_SubInitialContent { get; set; }

        [DataMember]
        public string News_Content { get; set; }

        [DataMember]
        public string News_Author { get; set; }
        [DataMember]
        public string News_CurrEditor { get; set; }

        [DataMember]
        public string News_Approver { get; set; }

        [DataMember]
        public int News_Status { get; set; }

        [DataMember]
        public DateTime News_SwitchTime { get; set; }

        [DataMember]
        public DateTime News_PublishDate { get; set; }

        [DataMember]
        public bool News_isFocus { get; set; }

        [DataMember]
        public int News_Mode { get; set; }

        [DataMember]
        public int News_ViewNum { get; set; }

        [DataMember]
        public DateTime News_CreateDate { get; set; }

        [DataMember]
        public DateTime News_ModifiedDate { get; set; }

        [DataMember]
        public string News_Relation { get; set; }

        [DataMember]
        public decimal News_Rate { get; set; }

        [DataMember]
        public string News_OtherCat { get; set; }

        [DataMember]
        public bool isComment { get; set; }

        [DataMember]
        public bool isUserRate { get; set; }

        [DataMember]
        public int Template { get; set; }

        [DataMember]
        public int WordCount { get; set; }

        [DataMember]
        public long ViewCount { get; set; }

        [DataMember]
        public string Icon { get; set; }

        [DataMember]
        public string Extension1 { get; set; }

        [DataMember]
        public string Extension2 { get; set; }

        [DataMember]
        public string Extension3 { get; set; }

        [DataMember]
        public int Extension4 { get; set; }

        [DataMember]
        public string LastSender { get; set; }

        [DataMember]
        public string LastReceiver { get; set; }

        [DataMember]
        public int Flag { get; set; }

        [DataMember]
        public int NewsFromSource { get; set; }

        [DataMember]
        public string FileAttachPath { get; set; }
    }
}
