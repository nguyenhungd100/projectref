﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class NewsConfigEntity : EntityBase
    {
        [DataMember]
        public string ConfigName { get; set; }
        [DataMember]
        public string ConfigGroupKey { get; set; }
        [DataMember]
        public string ConfigLabel { get; set; }
        [DataMember]
        public string ConfigValue { get; set; }
        [DataMember]
        public int ConfigValueType { get; set; }
        [DataMember]
        public string ConfigInitValue { get; set; }
        [DataMember]
        public string ConfigGroup { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public enum EnumStaticHtmlTemplateType
    {
        [EnumMember]
        AllType = 0,
        [EnumMember]
        Normal = 1,
        [EnumMember]
        HotNews = 2,
        [EnumMember]
        GroupNewsBox = 3,
        [EnumMember]
        Live = 4,
    }
}
