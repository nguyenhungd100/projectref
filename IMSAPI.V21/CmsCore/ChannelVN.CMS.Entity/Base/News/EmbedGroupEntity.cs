﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.News
{
    [DataContract]
    public class EmbedGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public bool IsVisible { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
    [DataContract]
    public class ZoneInEmbedGroupEntity : EntityBase
    {
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
    }
    [DataContract]
    public class ZoneInEmbedGroupWithDetailEntity : EntityBase
    {       
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
    }
    [DataContract]
    public class EmbedGroupWithDetailEntity : EntityBase
    {
        [DataMember]
        public EmbedGroupEntity EmbedGroup { get; set; }
        [DataMember]
        public List<ZoneInEmbedGroupWithDetailEntity> ListOfZones { get; set; }
    }
}
