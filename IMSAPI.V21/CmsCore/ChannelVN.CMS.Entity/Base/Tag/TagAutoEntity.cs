﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public class TagAutoEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string TagIdList { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public bool InProcess { get; set; }
    }
}
