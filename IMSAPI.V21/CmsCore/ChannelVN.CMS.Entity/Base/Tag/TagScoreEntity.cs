﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public class TagScoreEntity  : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Score { get; set; }
        [DataMember]
        public double Weight { get; set; }
    }
    
}
