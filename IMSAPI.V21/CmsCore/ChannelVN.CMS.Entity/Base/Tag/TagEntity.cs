﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public enum EnumTagType : int
    {
        [EnumMember]
        ForNews = 0,
        [EnumMember]
        ForImage = 1,
        [EnumMember]
        ForVideo = 2,
        [EnumMember]
        TagAuto = 3
    }
    [DataContract]
    public enum EnumSearchTagOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        NewsCountDesc = 1,
        [EnumMember]
        NewsCountAsc = 2,
        [EnumMember]
        TagHot = 3
    }
    [DataContract]
    public class TagEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public bool IsThread { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public string TagContent { get; set; }
        [DataMember]
        public string TagTitle { get; set; }
        [DataMember]
        public string TagInit { get; set; }
        [DataMember]
        public string TagMetaKeyword { get; set; }
        [DataMember]
        public string TagMetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public int ZoneId { get; set; }        
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsAmp { get; set; }
    }
    [DataContract]
    public class TagEntityDetail : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public bool IsThread { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public string TagContent { get; set; }
        [DataMember]
        public string TagTitle { get; set; }
        [DataMember]
        public string TagInit { get; set; }
        [DataMember]
        public string TagMetaKeyword { get; set; }
        [DataMember]
        public string TagMetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public List<TagZoneEntity> TagZone { get; set; }
        [DataMember]
        public List<TagWithSimpleFieldEntity> TagRelation { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public string NewsCoverIdStr { get { return NewsCoverId.ToString(); } }
        [DataMember]
        public bool IsAmp { get; set; }
    }
    [DataContract]
    public class TagWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
    }
    public class CountTagNewsEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public int CountNews { get; set; }        
    }

    [DataContract]
    public class BoxTagSeoEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public long TagId { get; set; }        

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string ObjectId { get; set; }

        [DataMember]
        public int ObjectType { get; set; }

        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public enum EnumObjectTypeBoxSeoTagEmbed : int
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        News = 1,
        [EnumMember]
        Video = 2,
        [EnumMember]
        Topic = 3
    }
}
