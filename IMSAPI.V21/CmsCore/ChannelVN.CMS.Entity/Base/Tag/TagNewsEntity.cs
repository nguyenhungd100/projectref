﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public enum EnumTagModes
    {
        [EnumMember]
        TagForNews = 0,
        [EnumMember]
        TagForNewsSubtitle = 1
    }
    [DataContract]
    public class TagNewsEntity : EntityBase
    {

        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public Int16 TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
    [DataContract]
    public class TagNewsWithTagInfoEntity : EntityBase
    {
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public Int16 TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string StrTagId { get { return TagId.ToString(); } }
        [DataMember]
        public string StrNewsId { get { return NewsId.ToString(); } }
    }

    public class TagJson {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
