﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public class TagWordEntity : EntityBase
    {
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public bool IsSuggestion { get; set; }
    }
    [DataContract]
    public class TagWordCheckedEntity : EntityBase
    {
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public bool IsChecked { get; set; }
    }
}
