﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.Entity.Base.Tag
{
    [DataContract]
    public class GateGameTagEntity : EntityBase
    {
        [DataMember]
        public int TagId { get; set; }
        [DataMember]
        public int GateGameId { get; set; }

    }
}
