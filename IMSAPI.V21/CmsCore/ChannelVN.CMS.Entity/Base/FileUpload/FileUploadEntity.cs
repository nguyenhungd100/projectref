﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.FileUpload
{
    [DataContract]
    public enum EnumFileUploadStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Inactived = 0,
        [EnumMember]
        Actived = 1
    }

    [DataContract]
    public class FileUploadExtEntity
    {
        [DataMember]
        public string FileExt { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

    [DataContract]
    public class FileUploadEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string FileDownloadPath { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public string FileExt { get; set; }
        [DataMember]
        public int FileSize { get; set; }
        [DataMember]
        public DateTime UploadedDate { get; set; }
        [DataMember]
        public string UploadedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
