﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Statistic
{
    [DataContract]
    public class PageViewAllSiteEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public int PageView { get; set; }
        [DataMember]
        public int PageViewMobile { get; set; }
        [DataMember]
        public DateTime LogDate { get; set; }
        [DataMember]
        public long LogDateStamp { get; set; }
    }
}
