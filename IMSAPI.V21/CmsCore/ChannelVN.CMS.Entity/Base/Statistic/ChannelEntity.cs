﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Statistic
{
    [DataContract]
    public class ChannelEntity
    {
        [DataMember]
        public int ChannelID { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public double VisitCountRatio { get; set; }
    }
}
