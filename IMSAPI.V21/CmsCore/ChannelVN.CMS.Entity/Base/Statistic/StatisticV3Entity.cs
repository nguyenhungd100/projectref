﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Statistic
{
    public enum ChannelId
    {
        Unknown = 0,
        Kenh14 = 1,
        GenK = 2,
        Afamily = 4,
        CafeF = 5,
        Skds = 6,
        GiaDinh = 7,
        VnEconomy = 8,
        Dddn = 9,
        DanTri = 10,
        GameK = 11,
        AutoPro = 12,
        Nld = 13,
        Soha = 14,
        CafeBiz = 15,
        VTV = 16,
        IctNews = 17,
        VTVEnglish = 18,
    }

    public class DepartmentViewCountEntity : EntityBase
    {
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int ViewCountMobile { get; set; }
        [DataMember]
        public string DepartmentName { get; set; }
    }

    public class StatisticV3ViewCountEntity : EntityBase
    {
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public Int64 DateStamp { get; set; }
        [DataMember]
        public int VisitCount { get; set; }
        [DataMember]
        public int yValues { get; set; }
        [DataMember]
        public string xValues { get; set; }
    }
    public class StatisticV3CategoryEntity : EntityBase
    {
        [DataMember]
        public int CatId { get; set; }
        [DataMember]
        public DateTime DailyTime { get; set; }
        [DataMember]
        public Int64 DailyTimeStamp { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int yValues { get; set; }
        [DataMember]
        public string xValues { get; set; }
        [DataMember]
        public int zValues { get; set; }
    }

    public class StatisticV3TagVisitEntity : EntityBase
    {
        [DataMember]
        public DateTime DailyTime { get; set; }
        [DataMember]
        public Int64 DailyTimeStamp { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public string TagUrl { get; set; }
    }
    public enum StatisticTagChannel
    {
        Unknown = 0,
        K14 = 1,
        GenK = 2,
        Afamily = 4,
        CafeF = 5,
        Skds = 6,
        GiaDinh = 7,
        VnEconomy = 8,
        Dddn = 9,
        DanTri = 10,
        GameK = 11,
        AutoPro = 12,
        Nld = 13,
        Soha = 14,
        CafeBiz = 15,
        VTV = 16,
        TechVccloud = 17,
        VtvVfc = 18,
        WebTheThao = 19,
        TienPhong = 20,
        Sport5 = 20,
        ThoiDai = 21,
        SucKhoeHangNgay=22,
        Demo = 23
    }
}
