﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Statistic
{
    [DataContract]
    public class NewsStatisticEntity : EntityBase
    {
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ValuesDate { get; set; }
        [DataMember]
        public int ValuesData { get; set; }
    }

    [DataContract]
    public class NewsStatisticTotalProduction : EntityBase
    {
        [DataMember]
        public string ValuesDate { get; set; }
        [DataMember]
        public int ValuesDataPublish { get; set; }
        [DataMember]
        public int ValuesDataCreated { get; set; }
    }
    [DataContract]
    public class ViewCountByUserEntity : EntityBase
    {
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public long ViewCount { get; set; }
    }
    [DataContract]
    public class NewsStatisticSourceEntity : EntityBase
    {
        [DataMember]
        public string SourceName { get; set; }
        [DataMember]
        public string ValuesDate { get; set; }
        [DataMember]
        public int ValuesData { get; set; }
    }
}
