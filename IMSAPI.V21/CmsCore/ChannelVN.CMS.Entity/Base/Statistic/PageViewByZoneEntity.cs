﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Statistic
{
    [DataContract]
    public class PageViewByZoneEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long ChannelId { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int PageView { get; set; }
        [DataMember]
        public int PageViewMobile { get; set; }
        [DataMember]
        public DateTime LogDate { get; set; }
        [DataMember]
        public long LogDateStamp { get; set; }
    }
}
