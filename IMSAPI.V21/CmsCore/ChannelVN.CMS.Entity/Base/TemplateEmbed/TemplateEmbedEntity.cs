﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.TemplateEmbed
{
    [DataContract]
    public enum EnumTemplateEmbedStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Hidden = 0,
        [EnumMember]
        Show = 1
    }
    [DataContract]
    public class TemplateEmbedEntity : EntityBase
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Css { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Descripton { get; set; }
        [DataMember]
        public string ContentHtml { get; set; }
        [DataMember]
        public string ContentJson { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public string CreateBy { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
