﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.RollingNews
{
    [DataContract]
    public enum EnumRollingNewsAuthorRole
    {
        [EnumMember]
        AllRole = 0,
        [EnumMember]
        Reporter = 1,
        [EnumMember]
        Publisher = 2
    }
    [DataContract]
    public class RollingNewsAuthorEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int AuthorRole { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Username { get; set; }
    }
}
