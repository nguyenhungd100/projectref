﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.RollingNews
{
    [DataContract]
    public enum  EnumRollingNewsStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        NotStarted = 0,
        [EnumMember]
        Started = 1,
        [EnumMember]
        Finished =2
    }
    [DataContract]
    public class RollingNewsEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public bool ShowTime { get; set; }
        [DataMember]
        public bool ShowAuthor { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public string PublishedContent { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsShowRollingNewsLabel { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string NewsTitle { get; set; }
    }
    [DataContract]
    public class RollingNewsForSuggestionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}
