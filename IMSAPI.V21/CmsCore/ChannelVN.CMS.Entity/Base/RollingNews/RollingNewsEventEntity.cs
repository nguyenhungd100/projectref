﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.RollingNews
{
    [DataContract]
    public enum EnumRollingNewsEventType
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        BreakingNews = 0,
        [EnumMember]
        Video = 1,
        [EnumMember]
        Photo = 2,
        [EnumMember]
        Quotation = 3,
        [EnumMember]
        Facebook = 4,
        [EnumMember]
        Phone = 5,
        [EnumMember]
        SmsMessage = 6,
        [EnumMember]
        Email = 7
    }
    [DataContract]
    public enum EnumRollingNewsEventStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        WaitForPublish = 0,
        [EnumMember]
        Published = 1,
        [EnumMember]
        Unpublished = 2
    }
    [DataContract]
    public class RollingNewsEventEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int EventType { get; set; }
        [DataMember]
        public string EventContent { get; set; }
        [DataMember]
        public string EventNote { get; set; }
        [DataMember]
        public DateTime EventTime { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string MobileContent { get; set; }
        [DataMember]
        public string ShortContent { get; set; }
        [DataMember]
        public string Images { get; set; }
        [DataMember]
        public int ImageCount { get; set; }
    }
    public class RollingNewsEventInfo
    {
        public int Id { get; set; }
        public string EventContent { get; set; }
        public DateTime EventTime { get; set; }
        public int EventType { get; set; }
        public bool IsFocus { get; set; }
        public string EventNote { get; set; }
        public string MobileContent { get; set; }
        public string ShortContent { get; set; }
        public string Images { get; set; }
        public int ImageCount { get; set; }
    }
}
