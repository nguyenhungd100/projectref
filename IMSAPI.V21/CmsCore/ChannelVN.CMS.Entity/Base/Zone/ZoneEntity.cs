﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Zone
{
    [DataContract]
    public class ZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string AvatarCover { get; set; }
        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        public string MetaAvatar { get; set; }

        [DataMember]
        public bool IsPrimary { get; set; }

        [DataMember]
        public string ZoneContent { get; set; }
    }
    [DataContract]
    public class ZoneWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ShortUrl { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool AllowComment { get; set; }
        [DataMember]
        public string Domain { get; set; }        
    }
    [DataContract]
    public class PhotoGroupDetailInZonePhotoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int PhotoGroupDetailId { get; set; }
        [DataMember]
        public int ZonePhotoId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public class PhotoGroupDetailInZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int PhotoGroupDetailId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
    }
    [DataContract]
    public class ZonePhotoWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
