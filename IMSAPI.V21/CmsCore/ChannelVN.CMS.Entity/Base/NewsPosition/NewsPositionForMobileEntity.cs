﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsPosition
{
    [DataContract]
    public enum NewsPositionTypeForMobile
    {
        [EnumMember]
        DefaultType = 1
    }
    [DataContract]
    public class NewsPositionForMobileEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool AllowAutoUpdate { get; set; }
        [DataMember]
        public int ViewCount { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        
        //[DataMember]
        //public string CreatedBy { get; set; }
        //[DataMember]
        //public string PublishedBy { get; set; }
    }
    [DataContract]
    public class NewsPositionForMobileWithSimpleFieldsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }
}
