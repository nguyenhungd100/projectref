﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsPosition
{
    [DataContract]
    public enum NewsPositionEditGroup
    {
        [EnumMember]
        ForHomePage = 1,
        [EnumMember]
        ForListPage = 2
    }
    [DataContract]
    public enum NewsPositionType
    {
        [EnumMember]
        HomeNewsFocus = 1,
        [EnumMember]
        HomeLastestNewsOnHome = 2,
        [EnumMember]
        NewsFocusByZone = 3,
        [EnumMember]
        LastestNewsByZone = 4,
        [EnumMember]
        LockedTimelineOnHome = 5
    }

    public enum NewsPositionTypeForAction
    {
        [EnumMember]
        HomeNewsFocus = 1,
        [EnumMember]
        HomeLastestNews = 2,
        [EnumMember]
        ListNewsFocusByZone = 3,
        [EnumMember]
        LastestNewsByZone = 4,
        [EnumMember]
        HomeMobileFocus = 5,
        [EnumMember]
        HomeNewsFocus_MienNam = 6

    }

    [DataContract]
    public class NewsPositionForHomePageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightHomeFocus;
        [DataMember]
        public List<NewsPositionEntity> LockedTimelineOnHome;
    }
    [DataContract]
    public class NewsPositionForListPageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightListFocusByZone;
    }
    [DataContract]
    public class NewsPositionEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsIdForBomd { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar1 { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string NewsRelation { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool AllowAutoUpdate { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public bool IsOnMobile { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public bool IsRssPepsi { get; set; }
        [DataMember]
        public string BlogSapo { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public int ViewCount { get; set; }

        [DataMember]
        public int LocationType { get; set; }

        [DataMember]
        public DateTime ExpiredDate { get; set; }
        
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public int ZoneIdForNews { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
    [DataContract]
    public class NewsPositionWithSimpleFieldsEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
    }
    [DataContract]
    public class NewsPositionForCheckAutoUpdateEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(Id); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Order { get; set; }
        [DataMember]
        public bool Lockable { get; set; }
        [DataMember]
        public bool Locked { get; set; }
        [DataMember]
        public DateTime ExpiredLock { get; set; }
        [DataMember]
        public long NewsIdForBomd { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public bool AllowAutoUpdate { get; set; }
    }

    [DataContract]
    public class DisplayPositionEmtity : EntityBase
    {
        [DataMember]
        public string NewsId { get; set; }
        [DataMember]
        public string NewsType { get; set; }
    }

    [DataContract]
    public class PriorityEmtity : EntityBase
    {
        [DataMember]
        public string NewsId { get; set; }
        [DataMember]
        public string NewsType { get; set; }
    }

    [DataContract]
    public class NewsVidepPositionEntity : EntityBase
    {       
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public int Position { get; set; }
        [DataMember]
        public string VideoUrl { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }        
        [DataMember]
        public long VideoId { get; set; }
    }
}
