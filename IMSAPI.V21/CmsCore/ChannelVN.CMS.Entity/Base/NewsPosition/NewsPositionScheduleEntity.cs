﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsPosition
{
    [DataContract]
    public class NewsPositionScheduleEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int PositionTypeId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Order { get; set; }        
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public DateTime ScheduleDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime ProcessedDate { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
    }
}
