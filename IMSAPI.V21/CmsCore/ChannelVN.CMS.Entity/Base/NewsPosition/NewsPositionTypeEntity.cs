﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsPosition
{
    [DataContract]
    public class NewsPositionTypeEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string PositionTypeName { get; set; }
        [DataMember]
        public byte FilterByZoneId { get; set; }
        [DataMember]
        public bool AllowAutoUpdate { get; set; }
    }
}
