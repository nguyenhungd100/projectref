﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsNotify
{
    [DataContract]
    public class NewsNotifyEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsProcess { get; set; }
        [DataMember]
        public DateTime PushDate { get; set; }
        [DataMember]
        public int ChannelId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string ZoneShortUrl { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
    }
}
