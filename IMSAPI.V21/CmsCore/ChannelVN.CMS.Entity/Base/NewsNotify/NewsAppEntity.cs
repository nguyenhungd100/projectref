﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.NewsNotify
{
    [DataContract]
    public class NewsAppEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Content { get; set; }
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsVideo { get; set; }
        [DataMember]
        public string VideoUrl { get; set; }
        [DataMember]
        public int Source { get; set; }
        [DataMember]
        public bool IsBreaking { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime PushDate { get; set; }
        [DataMember]
        public DateTime InputDate { get; set; }
        [DataMember]
        public long NotifyId { get; set; }
    }
}
