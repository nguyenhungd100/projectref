﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Topic
{
    [DataContract]
    public class NewsInTopicEntity : EntityBase
    {
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
    }
}
