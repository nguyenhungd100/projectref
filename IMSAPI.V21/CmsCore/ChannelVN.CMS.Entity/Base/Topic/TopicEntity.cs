﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.SEO.Entity;

namespace ChannelVN.CMS.Entity.Base.Topic
{
    [DataContract]
    public class TopicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public string LogoFancyClose { get; set; }
        [DataMember]
        public string LogoTopicName { get; set; }
        [DataMember]
        public string LogoSubMenu { get; set; }
        [DataMember]
        public string Cover { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsIconActive { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DisplayUrl { get; set; }        
        [DataMember]
        public List<TagWithSimpleFieldEntity> ListTag { get; set; }
        [DataMember]
        public string TagInString { get; set; }
        [DataMember]
        public int DefaultViewMode { get; set; }
        [DataMember]
        public string GuideToSendMail { get; set; }
        [DataMember]
        public string TopicEmail { get; set; }
        [DataMember]
        public bool IsTopToolbar { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ParentName { get; set; }
        [DataMember]
        public string RelationTopic { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public bool IsAmp { get; set; }
    }
    [DataContract]
    public class TopicDetailEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public string Cover { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
        [DataMember]
        public bool IsIconActive { get; set; }
        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public List<TopicInZoneEntity> TopicInZone { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string DisplayUrl { get; set; }
        [DataMember]
        public string LogoFancyClose { get; set; }
        [DataMember]
        public string LogoTopicName { get; set; }
        [DataMember]
        public string LogoSubMenu { get; set; }
        [DataMember]
        public List<TagWithSimpleFieldEntity> ListTag { get; set; }
        [DataMember]
        public string TagInString { get; set; }
        [DataMember]
        public int DefaultViewMode { get; set; }
        [DataMember]
        public string GuideToSendMail { get; set; }
        [DataMember]
        public string TopicEmail { get; set; }
        [DataMember]
        public bool IsTopToolbar { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ParentName { get; set; }
        [DataMember]
        public string RelationTopic { get; set; }
        [DataMember]
        public List<TopicRelation> ListRelationTopic { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
        [DataMember]
        public SEOMetaTopicEntity SeoMetaTopic { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public bool IsAmp { get; set; }
    }
    [DataContract]
    public class TopicDataAppBotEntity
    {
        [DataMember]
        public TopicEntity CurrentTopic { get; set; }
        [DataMember]
        public List<TopicEntity> ListTopic { get; set; }
    }

    [DataContract]
    public class BoxTopicEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int TopicId { get; set; }

        [DataMember]
        public string Label { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }
    }

    [DataContract]
    public class BoxTopicOnPageEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public long TopicId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        
        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        public string TopicName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }
    }

    [DataContract]
    public class ChangeTopicEmbedData
    {
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string[] ArrTopicId { get; set; }
        [DataMember]
        public string LastModifiedDate { get; set; }
    }

}
