﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Topic
{
    [DataContract]
    public class TopicInZoneEntity : EntityBase
    {
        [DataMember]
        public long TopicId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
    }

    [DataContract]
    public class TopicInNews : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ParentName { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
    }

    [DataContract]
    public class TopicRelation : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string ParentName { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }

    [DataContract]
    public class TopicParent : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string DisplayUrl { get; set; }
        [DataMember]
        public string DisplayName { get; set; }
    }
}
