﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class PhotoByAuthorEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public long PhotoId { get; set; }

        [DataMember]
        public int AuthorId { get; set; }
    }
}
