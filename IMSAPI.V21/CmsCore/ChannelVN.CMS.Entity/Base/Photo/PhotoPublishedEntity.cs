﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class PhotoPublishedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long PhotoId { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string ImageNote { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string DistributedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public string Tags { get; set; }
    }
    [DataContract]
    public class PhotoPublishedDetailEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long PhotoId { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string ImageNote { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string DistributedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public string Tags { get; set; }

        [DataMember]
        public string NewsTitle { get; set; }
        [DataMember]
        public string NewsUrl { get; set; }
        [DataMember]
        public string NewsZoneName{ get; set; }
    }

    [DataContract]
    public class PhotoPublishedUpdateEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageNote { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
