﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class PhotoTagsEntity : EntityBase
    {
        [DataMember]
        public long PhotoId { get; set; }
        [DataMember]
        public long TagId { get; set; }
        [DataMember]
        public byte TagMode { get; set; }
        [DataMember]
        public string TagProperty { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class PhotoTagEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Url { get; set; }
    }
    [DataContract]
    public enum EnumPhotoTagStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        InActived = 0,
        [EnumMember]
        Actived = 1
    }
}
