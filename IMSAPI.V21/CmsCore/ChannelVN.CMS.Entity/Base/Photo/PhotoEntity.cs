﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public enum EnumPhotoStatus : int
    {
        //ALL
        [EnumMember]
        AllStatus = -1,
        //Lưu tạm
        [EnumMember]
        Temporary = 0,
        //Đã xuất bản
        [EnumMember]
        Published = 1,
        //Gỡ bỏ
        [EnumMember]
        Unpublished = 2,
        //Chờ xuất bản
        [EnumMember]
        WaitForPublish = 3,
        //Trả lại
        [EnumMember]
        Return = 4       
    }

    [DataContract]
    public class PhotoEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }        
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }
        [DataMember]
        public string ImageNote { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public double Capacity { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string OriginalName { get; set; }
        [DataMember]
        public string UnSignName { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string ImagePreviewUrl { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string ImageDistribution { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public short ImageDimension { get; set; }
        [DataMember]
        public string ImageExif { get; set; }
        [DataMember]
        public string RestrictionContent { get; set; }
        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string AlbumName { get; set; }

        [DataMember]
        public int PhotoFolderId { get; set; }
        [DataMember]
        public int PhotoLabelId { get; set; }
    }
    [DataContract]
    public class PhotoSimpleEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ImageUrl { get; set; }        
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string ImagePreviewUrl { get; set; }                   
    }
    [DataContract]
    public class PhotoDetailEntity : EntityBase
    {
        [DataMember]
        public PhotoEntity PhotoInfo { get; set; }
        [DataMember]
        public List<PhotoTagsEntity> PhotoTags { get; set; }
        [DataMember]
        public AuthorDetail AuthorDetail { get; set; }
    }

    [DataContract]
    public class AuthorDetail
    {
        [DataMember]
        public int AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
    }

    [DataContract]
    public class ZonePhotoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Description { get; set; }
        
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Order { get; set; }
        
        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public bool ShowOnHome { get; set; }

        [DataMember]
        public string Logo { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }

        [DataMember]
        public int CatId { get; set; }

        [DataMember]
        public string ZoneRelation { get; set; }
    }

    [DataContract]
    public enum EnumZonePhotoStatus : int
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        InActived = 2
    }

    [DataContract]
    public class PhotoEventEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public string Name { get; set; }        
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime BeginDate { get; set; }
        [DataMember]
        public DateTime EndDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }

        [DataMember]
        public int ImageCount { get; set; }                        
        [DataMember]
        public int AlbumCount { get; set; }
        [DataMember]
        public int ViewsCount { get; set; }
        [DataMember]
        public string Location { get; set; }
    }
    [DataContract]
    public class PhotoEventSimpleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }        
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Url { get; set; }        
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }        
    }

    [DataContract]
    public enum EnumPhotoEventStatus : int
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        InActived = 2
    }    
}
