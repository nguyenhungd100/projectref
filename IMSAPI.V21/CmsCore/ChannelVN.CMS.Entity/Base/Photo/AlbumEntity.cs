﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class AlbumEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ThumbImage { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Author { get; set; }

    }

    [DataContract]
    public class AlbumDetailEntity : EntityBase
    {
        [DataMember]
        public AlbumEntity Album { get; set; }
        [DataMember]
        public List<PhotoEventSimpleEntity> ListEvents { get; set; }
        [DataMember]
        public List<PhotoTagEntity> ListTags { get; set; } 
        [DataMember]
        public AuthorDetail AuthorDetail { get; set; }
    }
   
    [DataContract]
    public enum EnumPhotoAlbumStatus : int
    {
        //ALL
        [EnumMember]
        AllStatus = -1,
        //Lưu tạm
        [EnumMember]
        Temporary = 0,
        //Đã xuất bản
        [EnumMember]
        Published = 1,
        //Gỡ bỏ
        [EnumMember]
        Unpublished = 2,
        //Chờ xuất bản
        [EnumMember]
        WaitForPublish = 3,
        //Trả lại
        [EnumMember]
        Return = 4
    }

    public class CountAlbumStatus
    {
        public int Status { get; set; }
        public int Count { get; set; }
    }
}
