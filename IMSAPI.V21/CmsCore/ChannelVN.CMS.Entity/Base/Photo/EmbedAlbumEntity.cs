﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class EmbedAlbumEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
    }

    [DataContract]
    public class EmbedAlbumForEditEntity : EntityBase
    {
        [DataMember]
        public EmbedAlbumEntity EmbedAlbum { get; set; }
        [DataMember]
        public List<EmbedAlbumDetailEntity> ListEmbedAlbumDetail { get; set; }
    }
}
