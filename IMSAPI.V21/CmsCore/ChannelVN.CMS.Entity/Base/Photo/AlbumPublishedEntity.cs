﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Photo
{
    [DataContract]
    public class AlbumPublishedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ThumbImage { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string DistributedBy { get; set; }
        [DataMember]
        public string Tags { get; set; }
    }

    [DataContract]
    public class  AlbumPublishedDetailEntity : EntityBase
    {
        [DataMember]
        public AlbumPublishedEntity AlbumPublished { get; set; }
        [DataMember]
        public List<PhotoPublishedEntity> ListPhotoPublished { get; set; }
    }
}
