﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public class RoyaltiesEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public long PhotoId { get; set; }
        [DataMember]
        public int RoyaltiesMemberId { get; set; }
        [DataMember]
        public string MemberAlias { get; set; }
        [DataMember]
        public int RoyaltiesCategoryId { get; set; }
        [DataMember]
        public int RoyaltiesRate { get; set; }
        [DataMember]
        public int RoyaltiesValue { get; set; }
        [DataMember]
        public int RoyaltiesBonus { get; set; }
        [DataMember]
        public int RoyaltiesPenalty { get; set; }
        [DataMember]
        public int RoyaltiesTotalValue { get; set; }
        [DataMember]
        public int CountPhotoClassA { get; set; }
        [DataMember]
        public int CountPhotoClassB { get; set; }
        [DataMember]
        public int PhotoRoyaltiesValue { get; set; }
        [DataMember]
        public int CountClipClassA { get; set; }
        [DataMember]
        public int CountClipClassB { get; set; }
        [DataMember]
        public int ClipRoyaltiesValue { get; set; }
        [DataMember]
        public int SelfProduceClipRoyaltiesValue { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string RoyaltiesCategoryName { get; set; }
        [DataMember]
        public string RoyaltiesRoleName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public int RoyaltiesOldTotalValue { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsManager { get; set; }
    }
    [DataContract]
    public class RoyaltiesGroupByUserEntity : EntityBase
    {
        [DataMember]
        public string MemberAlias { get; set; }
        [DataMember]
        public int RoyaltiesTotalValue { get; set; }
        [DataMember]
        public string RoyaltiesRoleName { get; set; }
        [DataMember]
        public string FullName { get; set; }
    }
}
