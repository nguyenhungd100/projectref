﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public class RoyaltiesHistoryEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long RoyaltiesId { get; set; }
        [DataMember]
        public int RoyaltiesCategoryId { get; set; }
        [DataMember]
        public int RoyaltiesRate { get; set; }
        [DataMember]
        public int RoyaltiesValue { get; set; }
        [DataMember]
        public int RoyaltiesBonus { get; set; }
        [DataMember]
        public int RoyaltiesPenalty { get; set; }
        [DataMember]
        public int RoyaltiesTotalValue { get; set; }
        [DataMember]
        public int CountPhotoClassA { get; set; }
        [DataMember]
        public int CountPhotoClassB { get; set; }
        [DataMember]
        public int PhotoRoyaltiesValue { get; set; }
        [DataMember]
        public int CountClipClassA { get; set; }
        [DataMember]
        public int CountClipClassB { get; set; }
        [DataMember]
        public int ClipRoyaltiesValue { get; set; }
        [DataMember]
        public int SelfProduceClipRoyaltiesValue { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }

        [DataMember]
        public string RoyaltiesCategoryName { get; set; }
    }
}
