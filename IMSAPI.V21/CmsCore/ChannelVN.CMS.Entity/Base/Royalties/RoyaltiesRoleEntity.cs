﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public class RoyaltiesRoleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public bool IsManager { get; set; }
    }
}
