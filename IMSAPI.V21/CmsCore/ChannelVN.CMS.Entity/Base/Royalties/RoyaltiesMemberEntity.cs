﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public enum EnumRoyaltiesMemberStatus
    {
        [EnumMember]
        All = -1,
        [EnumMember]
        Inactived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public class RoyaltiesMemberEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RoyaltiesRoleId { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string MemberAlias { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string EmplyeeCode { get; set; }
        [DataMember]
        public string TaxCode { get; set; }
        [DataMember]
        public string BankAccount { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public bool IsManager { get; set; }
        [DataMember]
        public string RoyaltiesRoleName { get; set; }
    }
}
