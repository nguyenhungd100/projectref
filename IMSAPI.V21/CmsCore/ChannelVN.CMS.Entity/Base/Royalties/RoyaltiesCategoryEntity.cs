﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public enum EnumRoyaltiesCategoryType
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        ForArticle = 0,
        [EnumMember]
        ForPhoto = 1,
        [EnumMember]
        ForClip = 2
    }
    [DataContract]
    public class RoyaltiesCategoryEntity :EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }
}
