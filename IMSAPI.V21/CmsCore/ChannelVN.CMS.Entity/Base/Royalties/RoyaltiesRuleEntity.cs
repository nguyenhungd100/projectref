﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Royalties
{
    [DataContract]
    public class RoyaltiesRuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RoyaltiesCategoryId { get; set; }
        [DataMember]
        public bool IsManager { get; set; }
        [DataMember]
        public int RoyaltiesRate { get; set; }
        [DataMember]
        public int RoyaltiesValue { get; set; }
        [DataMember]
        public int RoyaltiesCategoryType { get; set; }
    }
}
