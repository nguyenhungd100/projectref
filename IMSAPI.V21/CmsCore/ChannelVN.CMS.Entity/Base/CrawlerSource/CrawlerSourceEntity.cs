﻿using System.Collections.Generic;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.CrawlerSource
{
    [DataContract]
    public class CrawlerSourceTopicWrapperEntity : EntityBase
    {
        [DataMember]
        public CrawlerSourceEntity CrawlerSourceEntity { get; set; }
        [DataMember]
        public CrawlerSourceTopicEntity CrawlerSourceTopicEntity { get; set; }
    }

    [DataContract]
    public class CrawlerSourceEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public int SourceId { get; set; }
        [DataMember]
        public string SourceName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
    [DataContract]
    public class CrawlerSourceTopicEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int CrawlerSourceId { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
