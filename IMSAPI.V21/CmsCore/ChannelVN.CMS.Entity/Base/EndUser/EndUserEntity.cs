﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.EndUser
{
    [DataContract]
    public enum EndUserStatus
    {
        //All
        [EnumMember]
        All =-1,
        //Active
        [EnumMember]
        Active =1,
        //Join
        [EnumMember]
        Join =2       
    }

    [DataContract]
    public class PlayListCountEntity : EntityBase
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int PlayListId { get; set; }

        [DataMember]
        public int Count { get; set; }
    }

    [DataContract]
    public class VideoChannelCountEntity : EntityBase
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ChannelId { get; set; }

        [DataMember]
        public int Count { get; set; }
    }

    public class UserStatisticEntity : EntityBase
    {        
        [DataMember]
        public string ValuesDate { get; set; }
        [DataMember]
        public int ValuesDataActive { get; set; }
        [DataMember]
        public int ValuesDataJoin { get; set; }        
    }

    [DataContract]
    public class EndUserEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FacebookId { get; set; }

        [DataMember]
        public string VietId { get; set; }

        [DataMember]
        public string GoogleId { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public int Sex { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public string Address { get; set; }     
          
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }        
        
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }        
    }

    [DataContract]
    public class PlayListFollowEntity : EntityBase
    {
        [DataMember]
        public int PlayListId { get; set; }

        [DataMember]
        public int UserId { get; set; }        

        [DataMember]
        public DateTime FollowedDate { get; set; }        
    }

    [DataContract]
    public class ZoneVideoFollowEntity : EntityBase
    {
        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public DateTime FollowedDate { get; set; }
    }

    [DataContract]
    public class VideoChannelFollowEntity : EntityBase
    {
        [DataMember]
        public int ChannelId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public DateTime FollowedDate { get; set; }
    }

    [DataContract]
    public class VideoLabelFollowEntity : EntityBase
    {
        [DataMember]
        public int LabelId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public DateTime FollowedDate { get; set; }
    }

    [DataContract]
    public class PublisherFollowEntity : EntityBase
    {
        [DataMember]
        public int PublisherId { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public DateTime FollowedDate { get; set; }
    }
}
