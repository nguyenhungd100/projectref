﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Thread
{
    [DataContract]
    public class ThreadNewsEntity : EntityBase
    {
        [DataMember]
        public long ThreadId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
    }
}
