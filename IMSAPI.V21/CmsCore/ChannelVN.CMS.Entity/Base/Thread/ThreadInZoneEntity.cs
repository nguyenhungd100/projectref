﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Thread
{
    [DataContract]
    public class ThreadInZoneEntity : EntityBase
    {
        [DataMember]
        public long ThreadId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
}
