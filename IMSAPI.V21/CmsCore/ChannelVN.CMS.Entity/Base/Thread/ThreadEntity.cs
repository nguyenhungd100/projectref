﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Thread
{
    [DataContract]
    public enum EnumSearchThreadOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0
    }
    [DataContract]
    public class ThreadEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string HomeAvatar { get; set; }
        [DataMember]
        public string SpecialAvatar { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }

        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
    }
    [DataContract]
    public class ThreadDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string HomeAvatar { get; set; }
        [DataMember]
        public string SpecialAvatar { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string MetaKeyword { get; set; }
        [DataMember]
        public string MetaContent { get; set; }
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }

        [DataMember]
        public int NewsCount { get; set; }
        [DataMember]
        public List<ThreadInZoneEntity> ThreadInZone { get; set; }
        [DataMember]
        public List<ThreadWithSimpleFieldEntity> ThreadRelation { get; set; }
        [DataMember]
        public long NewsCoverId { get; set; }
        [DataMember]
        public string NewsCoverIdStr { get { return NewsCoverId.ToString(); }}
    }
    [DataContract]
    public class ThreadWithSimpleFieldEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
    }

    [DataContract]
    public class ThreadInNewsEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public bool Invisibled { get; set; }
    }
}
