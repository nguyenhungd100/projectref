﻿using ChannelVN.CMS.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Entity.Base.Nodejs
{
    public class NewsVersion
    {
    }

    #region Entity
    [DataContract]
    public class NewsVersionActionResponse
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string message { get; set; }
        public NewsVersionActionResponse()
        {
            success = false;
            message = "";
            code = 0;
        }
    }
    public class error
    {
        public string message { get; set; }
        public int code { get; set; }
    }
    [DataContract]
    public class ResponseListNewsVersion
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<NewsVersionNodeJsEntity> data { get; set; }
        public ResponseListNewsVersion()
        {
            success = false;
            message = "";
            code = 0;
            data = new List<NewsVersionNodeJsEntity>();
        }
    }
    [DataContract]
    public class ResponseListNewsVersionSample
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> data { get; set; }
        public ResponseListNewsVersionSample()
        {
            success = false;
            message = "";
            code = 0;
            data = new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
        }
    }
    [DataContract]
    public class ResponseNewsVersion
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public NewsVersionNodeJsEntity data { get; set; }
        public ResponseNewsVersion()
        {
            success = false;
            message = "";
            code = 0;
            data = new NewsVersionNodeJsEntity();
        }
    }

    [DataContract]
    public class NewsVersionNodeJsEntity : EntityBase
    {
        [DataMember]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "version")]
        public int Version { get; set; }
        [JsonProperty(PropertyName = "created_date_version")]
        public DateTime CreatedDateVersion { get; set; }
        [JsonProperty(PropertyName = "is_editing")]
        public string _IsEditing { get; set; }
        public bool IsEditing { get { return _IsEditing == "1" ? true : false; } set { } }

        [JsonProperty(PropertyName = "news_id")]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public string Title { get; set; }
        [JsonProperty("sub_title")]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [JsonProperty("avatar_desc")]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public string Avatar4 { get; set; }
        [DataMember]
        public string Avatar5 { get; set; }
        [DataMember]
        public string Author { get; set; }
        [JsonProperty("news_relation")]
        public string NewsRelation { get; set; }
        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }
        [DataMember]
        public string Source { get; set; }
        [JsonProperty(PropertyName = "is_focus")]
        public string _IsFocus { get; set; }
        public bool IsFocus { get { return _IsFocus == "1" ? true : false; } set { } }
        [JsonProperty(PropertyName = "type")]
        public int Type { get; set; }
        [JsonProperty(PropertyName = "news_type")]
        public int NewsType { get; set; }
        [JsonProperty(PropertyName = "thread_id")]
        public int ThreadId { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("modified_date")]
        public DateTime LastModifiedDate { get; set; }
        [JsonProperty("distribution_date")]
        public DateTime DistributionDate { get; set; }
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }
        [JsonProperty("modified_by")]
        public string LastModifiedBy { get; set; }
        [JsonProperty("published_by")]
        public string PublishedBy { get; set; }
        [JsonProperty("edited_by")]
        public string EditedBy { get; set; }
        [JsonProperty("last_receiver")]
        public string LastReceiver { get; set; }
        [JsonProperty(PropertyName = "word_count")]
        public int WordCount { get; set; }
        [JsonProperty(PropertyName = "view_count")]
        public int ViewCount { get; set; }
        [JsonProperty(PropertyName = "priority")]
        public int Priority { get; set; }
        [JsonProperty("list_zone_id")]
        public string ListZoneId { get; set; }
        [DataMember]
        public string Tag { get; set; }
        [DataMember]
        public string Note { get; set; }

        [JsonProperty(PropertyName = "display_style")]
        public int DisplayStyle { get; set; }
        [JsonProperty(PropertyName = "display_position")]
        public int DisplayPosition { get; set; }
        [JsonProperty(PropertyName = "display_in_slide")]
        public int DisplayInSlide { get; set; }
        [JsonProperty("avatar_custom")]
        public string AvatarCustom { get; set; }
        [JsonProperty(PropertyName = "original_id")]
        public int OriginalId { get; set; }
        [JsonProperty(PropertyName = "is_on_home")]
        public string _IsOnHome { get; set; }
        public bool IsOnHome { get { return _IsOnHome == "1" ? true : false; } set { } }
        [DataMember]
        public decimal Price { get; set; }

        [DataMember]
        public string Url { get; set; }
        [JsonProperty("note_royalties")]
        public string NoteRoyalties { get; set; }
        [JsonProperty("tag_item")]
        public string TagItem { get; set; }

        [JsonProperty(PropertyName = "news_category")]
        public int NewsCategory { get; set; }
        [JsonProperty("init_sapo")]
        public string InitSapo { get; set; }
        [JsonProperty("short_title")]
        public string ShortTitle { get; set; }
        [JsonProperty("interview_id")]
        public string InterviewId { get; set; }
        [JsonProperty("zone_id")]
        public int ZoneId { get; set; }
    }
    [DataContract]
    public class NewsVersionWithSimpleFieldsNodejsForParseJsonEntity : EntityBase
    {
        [DataMember]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "version")]
        public int Version { get; set; }
        [JsonProperty(PropertyName = "created_date_version")]
        public DateTime CreatedDateVersion { get; set; }
        [JsonProperty(PropertyName = "is_editing")]
        public string _IsEditing { get; set; }
        public bool IsEditing { get { return _IsEditing == "1" ? true : false; } set { } }

        [JsonProperty(PropertyName = "news_id")]
        public long NewsId { get; set; }
    }
    public class NewsVersionWithSimpleFieldsNodejsEntity : EntityBase
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int Version { get; set; }
        [DataMember]
        public DateTime CreatedDateVersion { get; set; }
        [DataMember]
        public bool IsEditing { get; set; }
        [DataMember]
        public long NewsId { get; set; }
    }
    #endregion
}
