﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.DirectTag
{
    [DataContract]
    public class DirectTagEntity
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public string TagLink { get; set; }
        [DataMember]
        public string QuoteFormat { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int CountInNews { get; set; }
    }
}
