﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.MediaAlbum
{
    [DataContract]
    public class MediaAlbumDetailEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int AlbumId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string UnSignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int MediaType { get; set; }
        [DataMember]
        public string MediaContent { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string DistributionBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int VoteCount { get; set; }
        [DataMember]
        public int CommentCount { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public bool IsHot { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
    [DataContract]
    public enum EnumMediaAlbumDetailStatus : int
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Temporary = 1,
        [EnumMember]
        Published = 2,
        [EnumMember]
        UnPublished = 3,
    }
    [DataContract]
    public enum EnumMediaAlbumDetailMediaType : int
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        NormalPhoto = 1,
        [EnumMember]
        GifPhoto = 2,
        [EnumMember]
        YoutubeVideo = 3,
        [EnumMember]
        TVCVideo = 4,
    }
    [DataContract]
    public class MediaAlbumDetailCountEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }
}
