﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.MediaAlbum
{
    [DataContract]
    public class MediaAlbumEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public int CountItem { get; set; }
    }

    public enum MediaAlbumStatusEnum
    {
        AllStatus = -1,
        //Lưu tạm
        Temporary = 0,
        //Xuất bản
        Published = 1,
        //Gỡ
        UnPublished = 2
    }       

    [DataContract]
    public class MediaAlbumForEditEntity : EntityBase
    {
        [DataMember]
        public MediaAlbumEntity MediaAlbum { get; set; }
        [DataMember]
        public List<MediaAlbumDetailEntity> ListMediaAlbumDetail { get; set; }
    }
}
