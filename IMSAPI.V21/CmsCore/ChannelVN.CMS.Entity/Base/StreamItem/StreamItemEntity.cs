﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.StreamItem
{
    [DataContract]
    public enum EnumStreamItemType : int
    {
        [EnumMember]
        ForNews = 0,
        [EnumMember]
        ForImage = 1,
        [EnumMember]
        ForVideo = 2,
        [EnumMember]
        StreamItemAuto = 3
    }
    [DataContract]
    public enum EnumSearchStreamItemOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        NewsCountDesc = 1,
        [EnumMember]
        NewsCountAsc = 2,
        [EnumMember]
        StreamItemHot = 3
    }
    [DataContract]
    public class StreamItemEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long Order { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public string TemplateId { get; set; }
        [DataMember]
        public int Status { get; set; }        
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }        
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string DataJson { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
    [DataContract]
    public class StreamItemDetailEntity : EntityBase
    {        
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(ObjectId); } set { } }
        [DataMember]
        public int ObjectType { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }

        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public string AuthorAvatar { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }            
    }

    public class StreamItemMobileEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long Order { get; set; }
        [DataMember]
        public int TypeId { get; set; }
        [DataMember]
        public string TemplateId { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public string DataJson { get; set; }
    }
    [DataContract]
    public class StreamItemMobileDetailEntity : EntityBase
    {
        [DataMember]
        public long ObjectId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(ObjectId); } set { } }
        [DataMember]
        public int ObjectType { get; set; }

        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }

        [DataMember]
        public string ZoneUrl { get; set; }
        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string AuthorAvatar { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }

        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }    
}
