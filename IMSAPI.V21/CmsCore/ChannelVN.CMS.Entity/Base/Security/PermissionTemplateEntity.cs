﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public class PermissionTemplateEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string TemplateName { get; set; }
    }
}
