﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public class PermissionTemplateDetailEntity : EntityBase
    {
        [DataMember]
        public int TemplateId { get; set; }
        [DataMember]
        public int PermissionId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
    [DataContract]
    public class PermissionTemplateWithPermissionDetailEntity : EntityBase
    {      
        [DataMember]
        public List<GroupPermissionDetailEntity> AllGroupPermission { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllParentZone { get; set; }
        [DataMember]
        public List<PermissionTemplateDetailEntity> PermissionTemplateDetailList { get; set; }
    }
}
