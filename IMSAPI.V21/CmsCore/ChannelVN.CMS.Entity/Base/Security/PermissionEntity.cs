﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public enum EnumPermission
    {
        [EnumMember]
        ArticleReporter = 1, // Phóng viên
        [EnumMember]
        ArticleEditor = 2, // Biên tập viên
        [EnumMember]
        ArticleAdmin = 3, // Thư ký
        [EnumMember]
        ArticleAdminEditor = 4, // Tổng biên tập
        [EnumMember]
        ArticleErrorCheck = 5, // Soát lỗi
        /*####################################*/
        [EnumMember]
        AccountManager = 11, // Quản lý User
        [EnumMember]
        AccountPermissionManager = 12, // Quản lý phân quyền User
        [EnumMember]
        DeleteAccountManager = 13, // Xóa User
        [EnumMember]
        Statistic = 15, // Thống kê
        [EnumMember]
        StatisticAllSite = 16, // Thống kê toàn trang
        /*####################################*/
        [EnumMember]
        SetupNewsPosition = 17, // Cài bài trang chủ
        [EnumMember]
        SetupNewsPositionByZone = 18, // Cài bài chuyên mục
        /*####################################*/
        [EnumMember]
        PhotoManager = 20, // Quản lý ảnh
        [EnumMember]
        ApprovePhoto = 77, // Duyệt photo
        [EnumMember]
        VideoManager = 21, // Quản lý video
        [EnumMember]
        ApproveVideo = 32, // Duyệt video
        /*####################################*/
        [EnumMember]
        EmbedView = 22, // Xem box nhúng
        [EnumMember]
        EmbedSetup = 23, // Cài bài box nhúng
        [EnumMember]
        EmbedApprover = 30, // Duyệt box nhúng ngoài trang
        /*####################################*/
        [EnumMember]
        SetupMetaTag = 24, // Quản lý MetaTag
        [EnumMember]
        TagManager = 25, // Quản lý Tag
        /*####################################*/
        [EnumMember]
        ViewRoyaltis = 26, // Xem nhuận bút
        [EnumMember]
        SetRoyaltis = 27, // Chấm nhuận bút
        /*####################################*/
        [EnumMember]
        ApproveComment = 31, // Duyệt comment
        [EnumMember]
        ReceiveComment = 39, // Duyệt comment
        /*####################################*/
        [EnumMember]
        CreateTopicDiscussion = 33, // Create topic discussion
        [EnumMember]
        CommentOnlyOnDiscussion = 34, // Comment only on discussion
        /*####################################*/
        [EnumMember]
        InterviewManager = 35, // Interview manager
        [EnumMember]
        RollingNewsManager = 36, // Rolling news manager
        [EnumMember]
        LiveSportManager = 37, // Live sport manager
        [EnumMember]
        FunnyNewsManager = 38, // FunnyNews manager
        /*####################################*/
        [EnumMember]
        AdvertismentManager = 40, // Advertisment manager        
        [EnumMember]
        PollManager = 41, // Quản lý bình chọn
        [EnumMember]
        PollEditor = 42, // Quản lý bình chọn của chính mình
        [EnumMember]
        CommentPollEditor = 43, // Xem và trích xuất Comment poll
        [EnumMember]
        CommentPollManager = 44, // Duyệt Comment poll
        [EnumMember]
        UpdateNewsToPartner = 45, // Đẩy bài sang đối tác

        [EnumMember]
        CommentVideoEditor = 46, // Nhận comment video
        [EnumMember]
        CommentVideoManager = 47, // Duyệt comment video

        [EnumMember]
        ManageCrawlerNews = 48, // Quản lý tin Crawler

        [EnumMember]
        LandingPageEditor = 60,//Landing Page cá nhân
        [EnumMember]
        LandingPageManager = 61,//Landing Page toàn bộ

        [EnumMember]
        InteractiveEditor = 62,//Interactive cá nhân
        [EnumMember]
        InteractiveManager = 63,//Interactive toàn bộ

        [EnumMember]
        QuizEditor = 64,//Quiz cá nhân
        [EnumMember]
        QuizManager = 65,//Quiz toàn bộ

        [EnumMember]
        MagazineManager = 66,//Quản lý báo giấy

        [EnumMember]
        VideoSharing = 9000,//Chia sẻ video (VTV)
        [EnumMember]
        VideoInZone = 9001,//Video chuyên trang

        [EnumMember]
        FileManager = 67, //Quan ly file
        [EnumMember]
        FacebookImageUploadManager = 68,//Bài độc giả gửi ảnh facebook
        [EnumMember]
        EPLViewReplayOnly = 69,//Chỉ xem video phát lại
        [EnumMember]
        DeleteArticle = 70,//Xóa bài viết
        [EnumMember]
        PushNotifyApp = 71,//Push notify        
        [EnumMember]
        ZoneManager = 72,//Quản lý chuyên mục    
        [EnumMember]
        GalleryManager = 73,//Quản lý trang ảnh gallery 
        [EnumMember]
        DocumentManager = 74,//Quản lý tài liệu
        [EnumMember]
        QuestionAnswerManager = 75,//Quản lý hỏi đáp 
        [EnumMember]
        TickNoAds = 76,//Cho phép tick bỏ quảng cáo trang chi tiết
    }

    [DataContract]
    public class PermissionEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int GroupId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public bool IsGrantByCategory { get; set; }
    }    
}
