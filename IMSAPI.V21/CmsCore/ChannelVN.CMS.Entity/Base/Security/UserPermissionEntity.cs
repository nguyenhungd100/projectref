﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public class UserPermissionEntity : EntityBase
    {
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public int PermissionId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
}
