﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.Base.Video;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public enum IsRole
    {        
        [EnumMember]
        ErrorCheck = 1,
        [EnumMember]
        Secretary = 2,
        [EnumMember]
        EditorialBoard = 3,
        [EnumMember]
        SendOverLevel = 4
    }
    [DataContract]
    public enum UserStatus
    {
        [EnumMember]
        Unknow = -1,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Locked = 0
    }
    [DataContract]
    public enum UserSortExpression
    {
        [EnumMember]
        UserNameDesc = 0,
        [EnumMember]
        UserNameAsc = 1,
        [EnumMember]
        CreateDateDesc = 2,
        [EnumMember]
        CreateDateAsc = 3,
        [EnumMember]
        LastLoginDesc = 4,
        [EnumMember]
        LastLoginAsc = 5,
        [EnumMember]
        LastChangePassDesc = 6,
        [EnumMember]
        LastChangePassAsc = 7
    }

    [DataContract]
    public class UserEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        //[DataMember]
        //public string EncryptId { get { return CryptonForId.EncryptId(Id); } }
        [DataMember]
        public string EncryptId { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public bool IsFullPermission { get; set; }
        [DataMember]
        public bool IsFullZone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Address { get; set; }
        [DataMember]
        public DateTime Birthday { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public DateTime LastLogined { get; set; }
        [DataMember]
        public DateTime LastChangePass { get; set; }
        [DataMember]
        public int PermissionCount { get; set; }
        [DataMember]
        public bool IsSystem { get; set; }
        [DataMember]
        public int UserType { get; set; }
        [DataMember]
        public string Domain { get; set; }
        [DataMember]
        public int IsRole { get; set; }
        [DataMember]
        public bool IsSendOver { get; set; }
        [DataMember]
        public string StaffCode { get; set; }
        [DataMember]
        public long? TelegramId { get; set; }
        [DataMember]
        public int DepartmentId { get; set; }
    }
    [DataContract]
    public class UserWithPermissionEntity : EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }
        [DataMember]
        public List<UserPermissionEntity> UserPermissions { get; set; }
    }
    [DataContract]
    public class UserWithPermissionDetailEntity : EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }
        [DataMember]
        public List<GroupPermissionDetailEntity> AllGroupPermission { get; set; }
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllParentZone { get; set; }
        [DataMember]
        public List<ZoneVideoEntity> AllParentZoneVideo { get; set; }
        [DataMember]
        public List<UserPermissionEntity> UserPermissionList { get; set; }
    }
    [DataContract]
    public class UserStandardEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        //[DataMember]
        //public string EncryptId { get { return CryptonForId.EncryptId(Id); } }
        [DataMember]
        public string EncryptId { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public bool IsFullPermission { get; set; }
        [DataMember]
        public bool IsFullZone { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int PermissionCount { get; set; }
        [DataMember]
        public int IsRole { get; set; }
        [DataMember]
        public string StaffCode { get; set; }
    }
    [DataContract]
    public class UserSearchAutocompleteEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
    }

    [DataContract]
    public enum UserActionType
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Add = 1,
        [EnumMember]
        Update = 2,
        [EnumMember]
        ResetOtp = 3,
        [EnumMember]
        LockUser = 4,
        [EnumMember]
        UnLockUser = 5,
        [EnumMember]
        UpdatePermission = 6
    }
}
