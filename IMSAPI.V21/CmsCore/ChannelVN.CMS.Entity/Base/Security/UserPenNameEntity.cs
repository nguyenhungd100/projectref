﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Security
{
    [DataContract]
    public class UserPenNameEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string PenName { get; set; }
        [DataMember]
        public long VietId { get; set; }
        [DataMember]
        public string Email { get; set; }
    }
}
