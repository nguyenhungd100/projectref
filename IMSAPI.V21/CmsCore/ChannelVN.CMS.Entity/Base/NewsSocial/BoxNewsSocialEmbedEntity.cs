﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsSocial
{
    [DataContract]
    public enum EnumBoxNewsSocialEmbedType : int
    {
        [EnumMember]
        Active = 1,
        [EnumMember]
        NoActive = 0
    }
    
    [DataContract]
    public class BoxNewsSocialEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int SortOrder { get; set; }
        [DataMember]
        public int Type { get; set; }        
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
    }

    [DataContract]
    public class BoxNewsSocialEmbedCdData
    {
        [DataMember]
        public string[] ListNewsId { get; set; }
        [DataMember]
        public int TypeNewsEmbed { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
}
