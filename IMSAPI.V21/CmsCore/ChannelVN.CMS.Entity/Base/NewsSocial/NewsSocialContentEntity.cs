﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsSocial
{
    [DataContract]
    public enum EnumNewsSocialContentStatus : int
    {
        [EnumMember]
        Active = 1,
        [EnumMember]
        NoActive = 0
    }
    
    [DataContract]
    public class NewsSocialContentEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Body { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string OriginalName { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public string AvatarCustom { get; set; }
        [DataMember]
        public string Avatar1 { get; set; }
        [DataMember]
        public string Avatar2 { get; set; }
        [DataMember]
        public string Avatar3 { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public string UpdatedBy { get; set; }

        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public string Note { get; set; }        

        [DataMember]
        public string RawId { get; set; }
    }

    [DataContract]
    public class DataNewsJson
    {
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public string byline { get; set; }
        [DataMember]
        public string dir { get; set; }
        [DataMember]
        public string content { get; set; }
        [DataMember]
        public string textContent { get; set; }
        [DataMember]
        public int? length { get; set; }
        [DataMember]
        public string excerpt { get; set; }
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string domain { get; set; }
        [DataMember]
        public string avatar { get; set; }
        [DataMember]
        public string logo { get; set; }        
    }
}
