﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsSocial
{
    [DataContract]
    public enum EnumNewsSocialEmbedStatus : int
    {
        [EnumMember]
        Active = 1,
        [EnumMember]
        NoActive = 0
    }

    [DataContract]
    public enum EnumNewsSocialEmbedOriginalId : int
    {
        [EnumMember]
        Tweeter = 1,
        [EnumMember]
        Intagram = 2,
        [EnumMember]
        Facebook = 3,
        [EnumMember]
        FacebookVideo = 4,
        [EnumMember]
        News = 5,
        [EnumMember]
        GoogleDocs = 6
    }  
      
    [DataContract]
    public class NewsSocialEmbedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string RawId { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public long Priority { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int OriginalId { get; set; }                       
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Embed { get; set; }
    }

    [DataContract]
    public class TwitterJson
    {
        [DataMember]
        public string url { get; set; }
        [DataMember]
        public string author_name { get; set; }
        [DataMember]
        public string author_url { get; set; }
        [DataMember]
        public string html { get; set; }
        [DataMember]
        public int? width { get; set; }
        [DataMember]
        public int? height { get; set; }
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string cache_age { get; set; }
        [DataMember]
        public string provider_name { get; set; }
        [DataMember]
        public string provider_url { get; set; }
        [DataMember]
        public string version { get; set; }
    }
}
