﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsMobileStream
{
    [DataContract]
    public class NewsMobileStreamEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string SubTitle { get; set; }
        [DataMember]
        public string Sapo { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AvatarDesc { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public int NewsType { get; set; }
        [DataMember]
        public int ThreadId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int DisplayStyle { get; set; }
        [DataMember]
        public int DisplayPosition { get; set; }
        [DataMember]
        public int DisplayInSlide { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string InitSapo { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public int InterviewId { get; set; }
        [DataMember]
        public bool IsBreakingNews { get; set; }
        [DataMember]
        public bool IsPr { get; set; }
        [DataMember]
        public bool AdStore { get; set; }
        [DataMember]
        public string AdStoreUrl { get; set; }
        [DataMember]
        public bool IsOnMobile { get; set; }
        [DataMember]
        public int RollingNewsId { get; set; }
        [DataMember]
        public int TagSubTitleId { get; set; }
        [DataMember]
        public bool IsRssPepsi { get; set; }
        [DataMember]
        public string BlogSapo { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string TagItem { get; set; }
        [DataMember]
        public int ViewCount { get; set; }

        [DataMember]
        public bool IsHighlight { get; set; }

        [DataMember]
        public int Status { get; set; }

    }

    [DataContract]
    public class UpdateNewsMobileStreamEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public bool IsHighlight { get; set; }

        [DataMember]
        public int Status { get; set; }

    }

}
