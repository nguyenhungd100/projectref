﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.NewsWarning
{
    [DataContract]
    public class ContentWarningEntity
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public int Level { get; set; }
        [DataMember]
        public string LimitedCategory { get; set; }
        [DataMember]
        public string ListTopic { get; set; }
        [DataMember]
        public string ListEnity { get; set; }
        [DataMember]
        public string ListKeyword { get; set; }
        [DataMember]
        public string ListTopicKeyword { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public List<ContentWarningLogEntity> ConfirmLog { get; set; }
    }
    [DataContract]
    public class ContentWarningLogEntity
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
        [DataMember]
        public string Account { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
    [DataContract]
    public class CheckContentWarningEntity
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public ReasonEntity reason { get; set; }        
        [DataMember]
        public int level { get; set; }
    }

    [DataContract]
    public class ReasonEntity
    {
        [DataMember]
        public List<Topics> topics { get; set; }
        [DataMember]
        public string limitedCategory { get; set; }
        [DataMember]
        public List<string> entity { get; set; }
        [DataMember]
        public List<string> keywords { get; set; }
        [DataMember]
        public List<string> TopicKeywords { get; set; }
    }
    [DataContract]
    public class Topics
    {
        [DataMember]
        public string TopicName { get; set; }
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string Scope { get; set; }
    }

    #region V2
    [DataContract]
    public class CheckContentWarningV2Entity
    {
        [DataMember]
        public string status { get; set; }
        [DataMember]
        public ReasonV2Entity Title { get; set; }
        [DataMember]
        public ReasonV2Entity Sapo { get; set; }
        [DataMember]
        public ReasonV2Entity Body { get; set; }        
    }

    [DataContract]
    public class ReasonV2Entity
    {
        [DataMember]
        public double respond_time { get; set; }
        [DataMember]
        public TypenameEntity typename { get; set; }
        [DataMember]
        public bool status { get; set; }
        [DataMember]
        public string msg { get; set; }
        [DataMember]
        public int label { get; set; }
        [DataMember]
        public bool success { get; set; }
    }

    [DataContract]
    public class TypenameEntity
    {
        [DataMember]
        public List<string> bad_words { get; set; }
        [DataMember]
        public List<string> warn_words { get; set; }
        [DataMember]
        public List<string> politic_words { get; set; }
        [DataMember]
        public bool khong_dau { get; set; }
        [DataMember]
        public List<string> non_words { get; set; }
    }
    #endregion
}
