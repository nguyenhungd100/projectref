﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.GroupQuestion
{
    [DataContract]
    public class AnswerEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int QuestionId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public bool IsCorrect { get; set; }
        [DataMember]
        public string ReadMoreLink { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public bool IsDeleted { get; set; }
    }
}
