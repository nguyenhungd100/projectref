﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsCacheMonitor
{
    [DataContract]
    public class UpdateCachedEntity : EntityBase
    {
        [DataMember]
        public string UpdateUrl { get; set; }
        [DataMember]
        public List<long> ListOfProcessId { get; set; }
        [DataMember]
        public int UpdateOrder { get; set; }
    }
}
