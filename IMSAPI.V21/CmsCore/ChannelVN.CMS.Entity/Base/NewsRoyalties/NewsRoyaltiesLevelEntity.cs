﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public class NewsRoyaltiesLevelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string LevelName { get; set; }
    }
}
