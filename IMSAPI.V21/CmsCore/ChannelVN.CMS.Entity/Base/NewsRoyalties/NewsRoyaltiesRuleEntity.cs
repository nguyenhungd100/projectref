﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public class NewsRoyaltiesRuleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public int LevelId { get; set; }
        [DataMember]
        public int CategoryId { get; set; }
        [DataMember]
        public int MediaId { get; set; }
        [DataMember]
        public int Value { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public string LevelName { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public string MediaName { get; set; }
    }
}
