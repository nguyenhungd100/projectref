﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public enum EnumRoyaltiesCategoryType
    {
        [EnumMember]
        AllType = -1,
        [EnumMember]
        ForArticle = 0,
        [EnumMember]
        ForPhoto = 1,
        [EnumMember]
        ForClip = 2
    }
    [DataContract]
    public class NewsRoyaltiesCategoryEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
    }
}
