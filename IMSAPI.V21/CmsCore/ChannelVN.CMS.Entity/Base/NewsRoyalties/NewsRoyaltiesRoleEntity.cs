﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public class NewsRoyaltiesRoleEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string RoleName { get; set; }
    }
}
