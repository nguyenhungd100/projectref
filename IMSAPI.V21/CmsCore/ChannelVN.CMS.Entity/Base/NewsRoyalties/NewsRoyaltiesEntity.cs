﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public class NewsRoyaltiesEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string PenName { get; set; }
        [DataMember]
        public int RoleId { get; set; }
        [DataMember]
        public int LevelId { get; set; }
        [DataMember]
        public int CategoryId { get; set; }
        [DataMember]
        public int MediaId { get; set; }
        [DataMember]
        public int NumberOfMedia { get; set; }
        [DataMember]
        public int Value { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string RoleName { get; set; }
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public string LevelName { get; set; }
        [DataMember]
        public string MediaName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int ViewCount { get; set; }
        [DataMember]
        public int WordCount { get; set; }
        [DataMember]
        public string DisplayDate { get; set; }
        [DataMember]
        public string DisplayViewCount { get; set; }
        [DataMember]
        public string DisplayWordCount { get; set; }
        [DataMember]
        public string DisplayType { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
}
