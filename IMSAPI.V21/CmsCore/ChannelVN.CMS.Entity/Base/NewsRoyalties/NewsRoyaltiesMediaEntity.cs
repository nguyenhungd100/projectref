﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.NewsRoyalties
{
    [DataContract]
    public class NewsRoyaltiesMediaEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string MediaName { get; set; }
        [DataMember]
        public int MaxMediaItem { get; set; }
    }
}
