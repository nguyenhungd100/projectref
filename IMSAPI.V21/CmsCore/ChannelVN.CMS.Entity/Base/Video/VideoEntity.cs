﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumVideoType : int
    {
        //ALL
        [EnumMember]
        All = -1,
        //Bản gốc
        [EnumMember]
        Default = 0,
        //App(Web)
        [EnumMember]
        AppWeb = 1,        
        //Facebook
        [EnumMember]
        FB_Zalo = 2,
        //Youtube
        [EnumMember]
        Youtube = 3,
        //SmartTV
        [EnumMember]
        SmartTV = 4
    }

    [DataContract]
    public enum EnumVideoStatus : int
    {
        //ALL
        [EnumMember]
        AllStatus = -1,
        //Lưu tạm
        [EnumMember]
        Temporary = 0,
        //Đã xuất bản
        [EnumMember]
        Published = 1,
        //Gỡ bỏ
        [EnumMember]
        Unpublished = 2,
        //Chờ xuất bản
        [EnumMember]
        WaitForPublish = 3,
        //Trả lại
        [EnumMember]
        Return = 4,
        //Chờ biên tập
        [EnumMember]
        WaitForEdit = 5,
        //Video youtube
        [EnumMember]
        YoutubeUploading = 99,
        //Clone Video vtv
        [EnumMember]
        CloneVideo = 7,
        //Done Video vtv
        [EnumMember]
        DoneVideo = 6,
        //Move Trash
        [EnumMember]
        MoveTrash = 9,
        //kingcontent share viddeo
        [EnumMember]
        ShareVideo =22
    }

    [DataContract]
    public enum EnumVideoSortOrder : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        NameDesc = 2,
        [EnumMember]
        NameAsc = 3,
        [EnumMember]
        IdAsc = 4,
        [EnumMember]
        IdDesc = 5,
        [EnumMember]
        PublishDateDesc = 6,
        [EnumMember]
        PublishDateAsc = 7,
        [EnumMember]
        LastModifyDateDesc = 8,
        [EnumMember]
        LastModifyDateAsc = 9,
        [EnumMember]
        CountViewAsc = 10,
        [EnumMember]
        CountViewDesc = 11
    }

    [DataContract]
    public class VideoEntity : EntityBase
    {
        // Video Info
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string Pname { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishBy { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string VideoRelation { get; set; }
        // VideoFileInfo
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        // For advertisement
        [DataMember]
        public bool AllowAd { get; set; }
        [DataMember]
        public bool IsRemoveLogo { get; set; }
        [DataMember]
        public string OriginalUrl { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public bool IsConverted { get; set; }
        [DataMember]
        public string AvatarShareFacebook { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public bool IsNewProcessed { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string HashId { get; set; }
        [DataMember]
        public string TrailerUrl { get; set; }
        [DataMember]
        public string MetaAvatar { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string Location { get; set; }
        [DataMember]
        public string PolicyContentId { get; set; }

        //ext
        [DataMember]
        public int VideoFolderId { get; set; }
        [DataMember]
        public DateTime PlayOnTime { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Source { get; set; }        
        [DataMember]
        public List<VideoExtensionEntity> VideoExtension { get; set; }        
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public string DisplayStyle { get; set; }
        [DataMember]
        public string Namespace { get; set; }
        [DataMember]
        public bool IsOnHome { get; set; }
        [DataMember]
        public bool IsAMP { get; set; }
        [DataMember]
        public int SourceId { get; set; }
    }

    public class VideoNotPublishCountInPlaylist : EntityBase
    {        
        [DataMember]
        public int PlaylistId { get; set; }

        [DataMember]
        public int VideoNotPublishCount { get; set; }
    }

    [DataContract]
    public class VideoDistributionEntity : EntityBase
    {
        // Video Info
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public string FileName { set; get; }
        [DataMember]
        public int Team1Id { get; set; }

        [DataMember]
        public string Team1Name { get; set; }

        [DataMember]
        public string Team2Name { get; set; }

        [DataMember]
        public int Team2Id { get; set; }
    }

    // Video Export Dân trí
    [DataContract]
    public class VideoExportEntity : EntityBase
    {
        // Video Info
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public string Pname { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int Views { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Tags { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishBy { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string VideoRelation { get; set; }
        // VideoFileInfo
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        // For advertisement
        [DataMember]
        public bool AllowAd { get; set; }

        [DataMember]
        public bool IsRemoveLogo { get; set; }

        [DataMember]
        public DateTime PlayOnTime { get; set; }

        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string Source { get; set; }
        // Royalty
        [DataMember]
        public string RoyaltyInfoNote { get; set; }

        [DataMember]
        public int RoyaltyInfoVideoType { get; set; }

        [DataMember]
        public string RoyaltyAuthorPenname { get; set; }

        [DataMember]
        public string RoyaltyAuthorRate { get; set; }
        [DataMember]
        public string QualityFactor { get; set; }
        [DataMember]
        public string ContributionFactor { get; set; }
        [DataMember]
        public string RiskFactor { get; set; }
        [DataMember]
        public string RoyaltyInfoTypeName { get; set; }
    }

    [DataContract]
    public class VideoDetailForEdit : EntityBase
    {
        [DataMember]
        public VideoEntity VideoInfo { get; set; }

        [DataMember]
        public List<VideoTagEntity> TagList { get; set; }

        [DataMember]
        public List<VideoRelationEntity> VideoRelation { get; set; }

        [DataMember]
        public List<VideoByAuthorEntity> VideoByAuthor { get; set; }

        [DataMember]
        public List<PlaylistEntity> Playlist { get; set; }

        [DataMember]
        public List<VideoChannelEntity> VideoChannelList { get; set; }

        [DataMember]
        public List<ZoneVideoEntity> ZoneVideoList { get; set; }

        [DataMember]
        public string ShareLink { get; set; }

        [DataMember]
        public List<VideoExtensionEntity> VideoExtension { get; set; }
    }

    [DataContract]
    public class VideoInfoForSync : EntityBase
    {
        [DataMember]
        public VideoEntity Video { get; set; }
        [DataMember]
        public List<VideoTagEntity> TagList { get; set; }
        [DataMember]
        public string ZoneIds { get; set; }
    }

    [DataContract]
    public class VideoInNewsEntity : EntityBase
    {
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public int VideoId { get; set; }
    }

    [DataContract]
    public enum EnumVideoFromYoutubeStatus : int
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Uploading = 1,
        [EnumMember]
        UploadSuccess = 2,
        [EnumMember]
        UploadFailed = 3,
        [EnumMember]
        FileInUsed = 4
    }

    [DataContract]
    public class VideoFromYoutubeEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string SourceVideoUrl { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CompletedDate { get; set; }
        [DataMember]
        public int VideoId { get; set; }
    }

    [DataContract]
    public class VideoFromYoutubeDetailEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public string SourceVideoUrl { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime CompletedDate { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }

    [DataContract]
    public class VideoFromYoutubeForEditEntity : EntityBase
    {
        [DataMember]
        public VideoFromYoutubeEntity VideoFromYoutubeInfo { get; set; }

        [DataMember]
        public VideoEntity VideoInfo { get; set; }

        [DataMember]
        public List<VideoTagEntity> TagList { get; set; }

        [DataMember]
        public List<VideoRelationEntity> VideoRelation { get; set; }

        [DataMember]
        public List<PlaylistEntity> Playlist { get; set; }

        [DataMember]
        public List<ZoneVideoEntity> ZoneVideoList { get; set; }
    }

    [DataContract]
    public class VideoCounterEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }

    [DataContract]
    public class VideoSharingEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
    }

    [DataContract]
    public class VideoKeyEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
    }

    [DataContract]
    public class VideoModeNotifyEntity : EntityBase
    {
        [DataMember]
        public int NumberOfVideo { get; set; }
        [DataMember]
        public int Mode { get; set; }
    }

    public class VideoInPlaylistSearch
    {
        public int TotalRow { get; set; }
        public int MaxPriority { get; set; }
        public List<VideoEntity> Videos { get; set; }
    }

    [DataContract]
    public class VideoByAuthorEntity : EntityBase
    {
        [DataMember]
        public long VideoId { get; set; }
        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(VideoId); } set { } }
        [DataMember]
        public int AuthorId { get; set; }
        [DataMember]
        public string AuthorName { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string AuthorTitle { get; set; }
        [DataMember]
        public int AuthorType { get; set; }
    }

    [DataContract]
    public class VideoRelationEntity : EntityBase
    {        
        [DataMember]
        public int Id { get; set; }        
        [DataMember]
        public string Name { get; set; }        
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }        
        [DataMember]
        public int Status { get; set; }       
        [DataMember]
        public DateTime DistributionDate { get; set; }        
        [DataMember]
        public string Url { get; set; }        
        [DataMember]
        public string FileName { get; set; }        
        [DataMember]
        public string Author { get; set; }        
        [DataMember]
        public string MetaAvatar { get; set; }       
    }
}
