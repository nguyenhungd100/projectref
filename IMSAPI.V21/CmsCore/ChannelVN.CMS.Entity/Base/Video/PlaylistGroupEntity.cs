﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class PlaylistGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int ParentId { set; get; }
        [DataMember]
        public string GroupName { set; get; }
        [DataMember]
        public int Status { set; get; }
    }
}
