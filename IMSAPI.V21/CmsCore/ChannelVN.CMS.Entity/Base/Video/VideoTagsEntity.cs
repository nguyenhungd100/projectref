﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class VideoTagsEntity: EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int TagId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
    }
}
