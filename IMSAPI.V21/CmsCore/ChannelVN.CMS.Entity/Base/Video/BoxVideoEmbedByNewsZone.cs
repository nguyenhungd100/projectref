﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class BoxVideoEmbedByNewsZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public long VideoId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
