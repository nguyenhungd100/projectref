﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class VideoInTagEntity : EntityBase
    {
        [DataMember]
        public int VideoTagId { get; set; }
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int TagMode { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
