﻿
namespace ChannelVN.CMS.Entity.Base.Video
{
    public class VideoApiEntity
    {
        public int videoId { get; set; }
        public string mathCode { get; set; }
        public string hashTags { get; set; }
        public string title { get; set; }
        public string fileName { get; set; }
        public string duration { get; set; }
        public string image { get; set; }
        public string createdDate { get; set; }
        public int type { get; set; }
        public string nameSpace { get; set; }                
    }
}
