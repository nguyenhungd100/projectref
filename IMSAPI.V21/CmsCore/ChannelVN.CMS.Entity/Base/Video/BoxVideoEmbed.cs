﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class BoxVideoEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public long VideoId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Embed { get; set; }
    }
    
    [DataContract]
    public enum BoxPositionEmbedType
    {
        //Kiểu play list
        [EnumMember]
        Playlist = 1,
        //Kiểu video
        [EnumMember]
        Video = 2,
        //Kiểu live vtv
        [EnumMember]
        VideoLive = 3
    }

    [DataContract]
    public class BoxPlayEmbedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public long VideoId { get; set; }        

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }

        [DataMember]
        public int Priority { get; set; }
    }

    public class BoxVideoHighlightEmbedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public long VideoId { get; set; }        

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public string Url { get; set; }        
    }

    public class BoxProgramHotHomeEmbedEntity : EntityBase
    {
        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public long VideoId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public string Url { get; set; }
    }
    
    [DataContract]
    public enum VideoPositionType
    {
        //Video nổi bật (Set tay)
        [EnumMember]
        VideoHighLight = 8,
        //Video box mục (Set tay)
        [EnumMember]
        VideoBoxMuc = 9,
        //Video chương trình Hot trang chủ (Set tay)
        [EnumMember]
        VideoProgramHotHome = 10,
        //Video chương trình Hot trang mục (Set tay)
        [EnumMember]
        VideoProgramHotMuc = 11
    }

    [DataContract]
    public enum FollowType
    {
        //Type nổi bật -> Video nổi bật (Set tay=8)
        [EnumMember]
        PlayList = 1,
        [EnumMember]
        Video = 2,
        [EnumMember]
        VideoLive = 3,
        [EnumMember]
        Zone = 4,
        [EnumMember]
        Channel = 5,
        [EnumMember]
        Label = 6
    }

    [DataContract]
    public enum VideoPositionDisplayStyle
    {
        //Default
        [EnumMember]
        Default = 1,
        //TV Show
        [EnumMember]
        TVShow = 2
    }

    [DataContract]
    public class BoxVideoEmbedEplEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ZoneId { get; set; }

        [DataMember]
        public string Embed { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class EmbedVideoEsData
    {
        [DataMember]
        public string[] ListVideoId { get; set; }
        [DataMember]
        public int TypeVideoEmbed { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
    }
}
