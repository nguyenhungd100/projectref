﻿using ChannelVN.CMS.Common;
using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class Mp3Entity : EntityBase
    {        
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string HtmlCode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string PublishBy { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string Url { get; set; }        
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string Duration { get; set; }
        [DataMember]
        public string Size { get; set; }
        [DataMember]
        public int Capacity { get; set; }
    }
}
