﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumVideoTagStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        InActived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public enum EnumVideoTagSort
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        VideoCountDesc = 1,
        [EnumMember]
        VideoCountAsc = 2
    }
    [DataContract]
    public enum EnumVideoTagMode
    {
        [EnumMember]
        AllMode = -1,
        [EnumMember]
        NormalTag = 0,
        [EnumMember]
        HotTag = 1
    }
    [DataContract]
    public class VideoTagEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ParentId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsHotTag { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int ChildTagCount { get; set; }
    }
}
