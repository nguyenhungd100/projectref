using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumZoneVideoStatus : int
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Inactived = 2
    }

    [DataContract]
    public class ZoneVideoEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int Order { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }

        [DataMember]
        public int CatId { get; set; }

        [DataMember]
        public int NumberOfChild { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }

        [DataMember]
        public bool ShowOnHome { get; set; }

        [DataMember]
        public bool Invisibled { get; set; }

        [DataMember]
        public string ListNewsZoneId { get; set; }

        [DataMember]
        public string ParentName { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string AvatarCover { get; set; }

        [DataMember]
        public string ZoneRelation { get; set; }

        [DataMember]
        public string Logo { get; set; }

        [DataMember]
        public string MetaAvatar { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Description { get; set; }
    }    

    public class ZoneVideoDetailEntity : EntityBase
    {
        public ZoneVideoEntity ZoneVideo { get; set; }
        public List<VideoTagEntity> VideoTags { get; set; }
    }
}
