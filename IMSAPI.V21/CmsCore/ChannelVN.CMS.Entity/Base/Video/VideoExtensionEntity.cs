﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumVideoExtensionType
    {
        #region All CMS

        [EnumMember]
        PushVideoToIcom = 1000,
        [EnumMember]
        MissSeaVideo = 1001
        #endregion
    }
    [DataContract]
    public class VideoExtensionEntity : EntityBase
    {
        [DataMember]
        public long VideoId { get; set; }
        [DataMember]
        public int Type { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public int NumericValue { get; set; }
    }
}
