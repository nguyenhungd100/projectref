﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumVideoThreadStatus : int
    {
        [EnumMember]
        Actived = 1,
        [EnumMember]
        InActived = 2
    }

    [DataContract]
    public class VideoThreadEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public bool IsHotThread { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    public class VideoThreadDetailEntity : EntityBase
    {
        public VideoThreadEntity VideoThread { get; set; }
        public List<VideoEntity> Videos { get; set; }
    }
}
