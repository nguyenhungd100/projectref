﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Entity.Base.Video
{
    public class VideoProductionStatistic
    {
        public string UserName { get; set; }
        public List<int> ListVideoCount = new List<int>();
    }
}
