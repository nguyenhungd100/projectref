﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class PlaylistInPlaylistGroupEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int PlaylistId { set; get; }
        [DataMember]
        public int @PlaylistGroupId { set; get; }
    }
}
