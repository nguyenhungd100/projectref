﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class VideoPlaylistEntity : EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int PlaylistId { get; set; }
        [DataMember]
        public int Priority { get; set; }
    }
}
