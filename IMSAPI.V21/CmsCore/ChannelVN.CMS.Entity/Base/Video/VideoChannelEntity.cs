﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System.Collections.Generic;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class VideoChannelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int PublisherId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int FollowCount { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int Rank { get; set; }
        [DataMember]
        public string IntroClip { get; set; }
        [DataMember]
        public string ChannelRelation { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string Url { get; set; }

        //relasionship
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int LabelId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }

        [DataMember]
        public string Cover { get; set; }

        [DataMember]
        public string MetaJson { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string MetaAvatar { get; set; }
    }

    [DataContract]
    public class VideoChannelCachedEntity : EntityBase
    {
        [DataMember]
        public VideoChannelEntity VideoChannelInfo { get; set; }
        [DataMember]
        public List<PlaylistEntity> PlayList { get; set; }
        [DataMember]
        public List<ZoneVideoEntity> ZoneVideoList { get; set; }
    }
}
