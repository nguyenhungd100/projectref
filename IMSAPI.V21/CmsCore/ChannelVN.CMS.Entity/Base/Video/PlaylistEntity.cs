﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumPlayListStatus : int
    {
        //all
        [EnumMember]
        AllStatus = -1,
        //lưu tạm
        [EnumMember]
        Temporary = 0,
        //xuất bản
        [EnumMember]
        Published = 1,
        //Gỡ
        [EnumMember]
        Unpublished = 2,
        //Chờ xuất bản
        [EnumMember]
        WaitForPublished = 3,
        //Trả lại
        [EnumMember]
        ReturnForEditor = 4,
        //Xóa tạm
        [EnumMember]
        MoveTrash = 5
    }
    [DataContract]
    public enum EnumPlayListMode : int
    {
        [EnumMember]
        AllMode = -1,
        [EnumMember]
        NormalPlaylist = 0,
        [EnumMember]
        HotPlaylist = 1
    }
    [DataContract]
    public enum EnumPlayListSort : int
    {
        [EnumMember]
        CreatedDateDesc = 0,
        [EnumMember]
        CreatedDateAsc = 1,
        [EnumMember]
        PlayListNameAsc = 2,
        [EnumMember]
        PlayListNameDesc = 3
    }
    [DataContract]
    public class PlaylistEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UnsignName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Mode { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public DateTime DistributionDate { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime PublishedDate { get; set; }
        [DataMember]
        public string PublishedBy { get; set; }
        [DataMember]
        public DateTime LastInsertVideoDate { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int FollowCount { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string IntroClip { get; set; }
        [DataMember]
        public string PlaylistRelation { get; set; }
        [DataMember]
        public string Cover { get; set; }
        [DataMember]
        public string MetaJson { get; set; }
        [DataMember]
        public DateTime EditedDate { get; set; }
        [DataMember]
        public string EditedBy { get; set; }
        [DataMember]
        public string MetaAvatar { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
    }

    [DataContract]
    public class PlaylistCountFollowEntity : EntityBase
    {
        [DataMember]
        public int PlayListId { get; set; }
        [DataMember]
        public int VideoCount { get; set; }
        [DataMember]
        public int FollowCount { get; set; }
    }

    [DataContract]
    public class PlaylistCounterEntity : EntityBase
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Count { get; set; }
    }

    [DataContract]
    public class PlaylistInZoneEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int PlaylistId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public string Avatar { get; set; }
    }

    [DataContract]
    public class PlaylistWithZoneEntity : EntityBase
    {
        [DataMember]
        public PlaylistEntity Playlist { get; set; }
        [DataMember]
        public List<PlaylistInZoneEntity> PlaylistInZone { get; set; }
    }

    [DataContract]
    public class PlayListCachedEntity : EntityBase
    {
        [DataMember]
        public PlaylistEntity PlaylistInfo { get; set; }
        [DataMember]
        public List<ZoneVideoEntity> ZoneVideoList { get; set; }
    }

    [DataContract]
    public class VideoPriorityEntity : EntityBase
    {
        [DataMember]
        public int VideoId { get; set; }
        [DataMember]
        public int Priority { get; set; }        
    }
}
