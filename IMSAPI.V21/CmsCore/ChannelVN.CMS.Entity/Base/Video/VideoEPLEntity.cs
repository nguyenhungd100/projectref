﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using System;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public class VideoEPLEntity : EntityBase
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int IsOriginal { get; set; }
    }
    [DataContract]
    public class VideoEPLZoneToZoneEntity : EntityBase
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int ZoneId1 { get; set; }
        [DataMember]
        public int ZoneId2 { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int IsOriginal { get; set; }
    }
    public class EPLPlayListEntity : EntityBase
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string KeyVideo { get; set; }
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public DateTime PublishDate { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public int UserId { get; set; }
        [DataMember]
        public string Domain { get; set; }
    }
}
