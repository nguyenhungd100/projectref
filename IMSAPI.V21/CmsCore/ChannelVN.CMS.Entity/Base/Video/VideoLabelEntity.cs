using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Video
{
    [DataContract]
    public enum EnumVideoLabelStatus : int
    {
        [EnumMember]
        AllStatus = 0,
        [EnumMember]
        Actived = 1,
        [EnumMember]
        Inactived = 2
    }

    [DataContract]
    public class VideoLabelEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public Boolean IsSystem { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }        
    }
        
}
