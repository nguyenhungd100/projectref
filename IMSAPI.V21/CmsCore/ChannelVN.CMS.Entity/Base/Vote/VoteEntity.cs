﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Vote
{
    [DataContract]
    public class VoteEntity: EntityBase
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Sapo { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime ModifiedDate { get; set; }

        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string LastModifiedBy { get; set; }

        [DataMember]
        public int Priority { get; set; }

        [DataMember]
        public int DisplayPosition { get; set; }

        [DataMember]
        public int DisplayStyle { get; set; }

        [DataMember]
        public int ViewCount { get; set; }

        [DataMember]
        public int RateCount { get; set; }

        [DataMember]
        public int MaxAnswers { get; set; }
        [DataMember]
        public bool ShowInZone { get; set; }
        [DataMember]
        public bool ShowInFooter { get; set; }

        [DataMember]
        public DateTime StartedDate { get; set; }

        [DataMember]
        public DateTime EndedDate { get; set; }
    }

    [DataContract]
    public class VoteDetailEntity : EntityBase
    {
        [DataMember]
        public VoteEntity VoteInfo { get; set; }
        [DataMember]
        public List<VoteAnswersEntity> ListAnswers { get; set; }
        [DataMember]
        public List<VoteInZoneEntity> ListZoneInVote { get; set; }

    }
}
