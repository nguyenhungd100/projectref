﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Vote
{
    [DataContract]
    public enum EnumVoteYesNoGroupStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Inactived = 0,
        [EnumMember]
        Actived = 1
    }
    [DataContract]
    public class VoteYesNoGroupEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string GroupName { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
