﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.Base.Vote
{
    [DataContract]
    public class VoteInZoneEntity : EntityBase
    {

        [DataMember]
        public int VoteID { get; set; }
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public bool IsPrimary { get; set; }

    }
}
