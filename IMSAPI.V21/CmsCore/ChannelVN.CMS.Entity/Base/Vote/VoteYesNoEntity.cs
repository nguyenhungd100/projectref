﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.Base.Vote
{
    [DataContract]
    public enum EnumVoteYesNoStatus
    {
        [EnumMember]
        AllStatus = -1,
        [EnumMember]
        Teporary = 0,
        [EnumMember]
        Published = 1,
        [EnumMember]
        Unpublished = 2
    }
    [DataContract]
    public class VoteYesNoEntity:EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int VoteYesNoGroupId { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Avatar { get; set; }
        [DataMember]
        public string YesAnswer { get; set; }
        [DataMember]
        public string NoAnswer { get; set; }
        [DataMember]
        public string Source { get; set; }
        [DataMember]
        public string Author { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string LastModifiedBy { get; set; }
        [DataMember]
        public DateTime LastModifiedDate { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public bool IsFocus { get; set; }
        [DataMember]
        public int Status { get; set; }
    }
}
