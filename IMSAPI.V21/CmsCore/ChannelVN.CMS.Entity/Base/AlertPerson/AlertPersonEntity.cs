﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.Base.AlertPerson
{
    [DataContract]
    public class AlertPersonEntity : EntityBase
    {
        [DataMember]
        public string PersonName { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Id { get; set; }
    }
}
