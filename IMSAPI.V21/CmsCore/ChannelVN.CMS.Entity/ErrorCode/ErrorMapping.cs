﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.ErrorCode
{
    [DataContract]
    public class ErrorMapping : BoErrorMappingBase<ErrorMapping.ErrorCodes>
    {
        private static ErrorMapping _current;
        public static ErrorMapping Current
        {
            get { return _current ?? (_current = new ErrorMapping()); }
        }

        protected override void InitErrorMapping()
        {
            #region General error

            InnerHashtable[ErrorCodes.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodes.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodes.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodes.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodes.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodes.TimeOutSession] = "Phiên làm việc của bạn đã hết.";
            InnerHashtable[ErrorCodes.DuplicateError] = "Đã tồn tại";

            #endregion

            #region User error
            InnerHashtable[ErrorCodes.UpdateAccountInvalidUsername] = "Tên đăng nhập không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidPassword] = "Mật khẩu không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidRetypePassword] = "Mật khẩu nhập lại không chính xác";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidEmail] = "Địa chỉ Email không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidMobile] = "Số điện thoại di động không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateAccountUsernameExists] = "Tên đăng nhập đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateAccountEmailExists] = "Địa chỉ email đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateAccountUserNotFound] = "Không tìm thấy thông tin tài khoản";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidOldPassword] = "Mật khẩu cũ không chính xác";
            InnerHashtable[ErrorCodes.UpdateAccountPermissionNotFound] = "Không tìm thấy thông tin quyền";
            InnerHashtable[ErrorCodes.UpdateAccountZoneNotFound] = "Không tìm thấy thông tin chuyên mục";
            InnerHashtable[ErrorCodes.UpdateAccountUserHasBeenLocked] = "Tài khoản đang bị khóa";
            InnerHashtable[ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch] = "Mật khẩu cũ và mật khẩu mới không giống nhau";
            InnerHashtable[ErrorCodes.UpdateAccountCantNotEditSystem] = "Bạn không có quyền sửa thông tin account này";
            InnerHashtable[ErrorCodes.UpdateAccountInvalidStatus] = "Trạng thái không hợp lệ";

            InnerHashtable[ErrorCodes.ValidAccountInvalidUsername] = "Không tìm thấy tài khoản";
            InnerHashtable[ErrorCodes.ValidAccountInvalidPassword] = "Mật khẩu không hợp lệ";
            InnerHashtable[ErrorCodes.ValidAccountUserLocked] = "Tài khoản đang bị khóa";
            InnerHashtable[ErrorCodes.ValidAccountInvalidSmsCode] = "Mã sms không hợp lệ";
            InnerHashtable[ErrorCodes.ValidAccountNotHavePermission] = "Không có quyền xử lý";

            InnerHashtable[ErrorCodes.SwitchCurrentRoleUserNotFound] = "Tài khoản không hợp lệ";
            InnerHashtable[ErrorCodes.SwitchCurrentRoleInvalidRole] = "Vai trò không hợp lệ";
            InnerHashtable[ErrorCodes.SwitchCurrentRoleUserNotHaveRole] = "Tài khoản không có vai trò cần chuyển";
            #endregion

            #region News error

            InnerHashtable[ErrorCodes.UpdateNewsNewsNotFound] = "Không tìm thấy bài";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidPrimaryZone] = "Bài chưa có chuyên mục chính";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidTitle] = "Bài chưa có tiêu đề";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidAvatar] = "Bài chưa có avatar";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidTag] = "Bài chưa có tag. Bạn phải nhập tag mới được xuất bản";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidSapo] = "Bài chưa có mô tả";
            InnerHashtable[ErrorCodes.UpdateNewsInvalidBody] = "Bài chưa có nội dung";
            InnerHashtable[ErrorCodes.UpdateNewsNotAllowEdit] = "Trạng thái hiện tại không cho phép sửa";
            InnerHashtable[ErrorCodes.UpdateNewsNotAllowDelete] = "Trạng thái hiện tại không cho phép xóa";
            InnerHashtable[ErrorCodes.UpdateNewsDuplicateId] = "ID của bài đã tồn tại";
            InnerHashtable[ErrorCodes.UpdateNewsLockedByOtherUserForEdit] = "Bài này cũng đang được cập nhật bởi {0}. Bạn có muốn tiếp tục không?";
            InnerHashtable[ErrorCodes.UpdateNewsTitleIsExisted] = "Tiêu đề đã tồn tại";            

            InnerHashtable[ErrorCodes.ChangeNewsStatusNewsNotFound] = "Không tìm thấy bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToSend] = "Không được phép gửi bài lên";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceive] = "Không được phép nhận bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToPublish] = "Không được phép xuất bản bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToUnpublish] = "Không được phép gỡ bài đã xuất bản";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReturn] = "Không được phép trả bài về";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToGetBack] = "Không được phép rút lại bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore] = "Không được phép nhận bài trả về";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash] = "Không được phép xóa bài";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash] = "Không được phép phục hồi bài bị xóa";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToForward] = "Không được phép chuyển bài cho người khác";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReturnYourself] = "Bạn viết bài này nên không được phép trả lại chính bạn";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToRelease] = "Hiện tại không trả lại được bài viết này. thử lại sau!";
            InnerHashtable[ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator] = "Không được phép nhận bài Cộng tác viên";

            InnerHashtable[ErrorCodes.UpdateNewsPositionNotAllowToUpdate] = "Không có quyền cập nhật vị trí";
            InnerHashtable[ErrorCodes.UpdateNewsPositionHasBeenLocked] = "Vị trí tin đang bị khóa";
            InnerHashtable[ErrorCodes.UpdateNewsPositionInvalidUpdateGroup] = "Không tìm thấy nhóm vị trí tin";

            InnerHashtable[ErrorCodes.UpdateNewsNotifictionInvalidUser] = "Không tìm thấy tài khoản cần gửi thông báo";
            InnerHashtable[ErrorCodes.UpdateNewsNotifictionInvalidLabel] = "Không tìm thấy nhãn cần gán cho tin";
            InnerHashtable[ErrorCodes.KhongTimThayNguoiXuLy] = "Không tìm thấy người xử lý";
            InnerHashtable[ErrorCodes.TieuDeBaiVietVuotQua255KyTu] = "Tiêu đề bài viết vượt quá 255 ký tự.";

            #endregion

            #region Tag error
            InnerHashtable[ErrorCodes.UpdateTagTagNotFound] = "Không tìm thấy tag";
            InnerHashtable[ErrorCodes.UpdateTagInvalidTagName] = "Chưa nhập tag";
            InnerHashtable[ErrorCodes.UpdateTagConflictTagName] = "Trùng tên tag";
            #endregion

            #region Zone error

            InnerHashtable[ErrorCodes.UpdateZoneNotAllow] = "Bạn không có quyền cập nhật chuyên mục.";

            InnerHashtable[ErrorCodes.UpdateZoneInvalidName] = "Tên của chuyên mục không hợp lệ.";

            #endregion

            #region Thread error
            #endregion

            #region Comment error
            #endregion

            #region Video errors

            InnerHashtable[ErrorCodes.VideoNotAvailable] = "Video này không tồn tại";
            InnerHashtable[ErrorCodes.VideoTitleCouldNotBeEmpty] = "Tiêu đề của video không được trống";
            InnerHashtable[ErrorCodes.VideoMustHaveFileToSend] = "Bạn phải upload file video trước khi gửi lên chờ xuất bản";
            InnerHashtable[ErrorCodes.VideoMustHaveFileToPublish] = "Bạn phải upload file video trước khi xuất bản";
            InnerHashtable[ErrorCodes.VideoNotAllowToSend] = "Bạn không có quyền gửi video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToReturn] = "Bạn không có quyền trả lại video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToUpdate] = "Bạn không có quyền sửa video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToPublish] = "Bạn không có quyền xuất bản video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToUnpublish] = "Bạn không có quyền gỡ xuất bản video này";
            InnerHashtable[ErrorCodes.VideoNotAllowToDelete] = "Bạn không có quyền xóa video này";

            InnerHashtable[ErrorCodes.YoutubeVideoUrlExists] = "Địa chỉ youtube đã tồn tại, vui lòng kiểm tra lại trong danh sách";

            InnerHashtable[ErrorCodes.ZoneVideoMustHaveName] = "Bạn chưa nhập tên chuyên mục của video";
            InnerHashtable[ErrorCodes.VideoLabelMustHaveName] = "Bạn chưa nhập tên nhãn";

            InnerHashtable[ErrorCodes.VideoTagMustHaveName] = "Bạn chưa nhập tên tag video";

            InnerHashtable[ErrorCodes.VideoPlaylistMustHaveName] = "Bạn chưa nhập tên playlist cho video";
            InnerHashtable[ErrorCodes.ExceptionError0984639770] = "Không tìm thấy video bởi HashId.";
            #endregion

            #region Photo errors

            InnerHashtable[ErrorCodes.PhotoNotFound] = "không tồn tại";
            InnerHashtable[ErrorCodes.PhotoNotAllowEdit] = "Không có quyền cập nhật";
            InnerHashtable[ErrorCodes.PhotoNotAllowToSend] = "Bạn không có quyền gửi";
            InnerHashtable[ErrorCodes.PhotoNotAllowToReturn] = "Bạn không có quyền trả lại";
            InnerHashtable[ErrorCodes.PhotoNotAllowToPublish] = "Bạn không có quyền xuất bản";
            InnerHashtable[ErrorCodes.PhotoNotAllowToUnpublish] = "Bạn không có quyền gỡ";

            #endregion

            #region NewsLive
            InnerHashtable[ErrorCodes.UpdateNewsLiveNotPermission] = "Bạn không có quyền cập nhật tường thuật bài này";
            InnerHashtable[ErrorCodes.UpdateNewsLiveNotAllowPublish] = "Bạn không có quyền duyệt bài tường thuật này";
            InnerHashtable[ErrorCodes.UpdateNewsLiveNotAllowDelete] = "Bạn không có quyền xóa bài tường thuật này";
            InnerHashtable[ErrorCodes.NewsLiveIsFinished] = "Bài tường thuật đã kết thúc";
            #endregion

            #region Thread error


            InnerHashtable[ErrorCodes.UpdateThreadThreadNotFound] = "Không tìm thấy dòng sự kiện";

            InnerHashtable[ErrorCodes.UpdateThreadInvalidThreadName] = "Bạn chưa nhập tên dòng sự kiện";

            InnerHashtable[ErrorCodes.UpdateThreadConflictThreadName] = "Tên dòng sự kiện đã tồn tại, bạn hãy chọn tên khác";

            #endregion

            #region Interview error


            InnerHashtable[ErrorCodes.UpdateInterviewNotSuccess] = "Không cập nhật được cuộc giao lưu";
            InnerHashtable[ErrorCodes.UpdateInterviewNotFound] = "Không tìm thấy cuộc giao lưu";
            InnerHashtable[ErrorCodes.UpdateInterviewTitleIsEmpty] = "Bạn chưa nhập Tiêu đề cuộc giao lưu";
            InnerHashtable[ErrorCodes.UpdateInterviewCannotPublishEmptyContent] = "Không để xuất bản được nội dung cho cuộc giao lưu";

            InnerHashtable[ErrorCodes.UpdateInterviewChannelUsernameNotValid] = "Tên tài khoản không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelInterviewNotFound] = "Không tìm thấy cuộc giao lưu này";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelUserAssignedSameRole] = "Tài khoản đã được gán cùng 1 vị trí trong cuộc giao lưu này";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelInterviewChannelNotFound] = "Không tìm thấy tài khoản trong cuộc giao lưu này";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelNotSuccess] = "Không cập nhật được tài khoản cho cuộc giao lưu này";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelNotAllowToUpdate] = "Bạn không có quyền cập nhật tài khoản cho cuộc giao lưu này";
            InnerHashtable[ErrorCodes.UpdateInterviewChannelAlreadyAnswerQuestion] = "Không được phép xóa tài khoản trực kênh vì tài khoản đã xử lý câu hỏi";

            InnerHashtable[ErrorCodes.UpdateInterviewQuestionInvalidAnswer] = "Câu trả lời không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionInvalidFullName] = "Tên người gửi câu hỏi không hợp lệ";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionInterviewNotFound] = "Không tìm thấy cuộc giao lưu";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound] = "Không tìm thấy câu trả lời";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotSuccess] = "Không cập nhật được câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionChannelNotFound] = "Không tìm thấy câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionMustDistributeToChannelUserOnly] = "Câu trả lời chỉ phân phối được cho tài khoản trực kênh";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowDistributeThisQuestion] = "Không được phép phân phối câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowForwardThisQuestion] = "Không được phép chuyển tiếp câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowSendAnswerForThisQuestion] = "Không được phép gửi câu trả lời";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowPublishThisQuestion] = "Không được phép xuất bản câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowUnpublishThisQuestion] = "Không được phép gỡ xuất bản câu hỏi";
            InnerHashtable[ErrorCodes.UpdateInterviewQuestionNotAllowDeleteThisQuestion] = "Không được phép xóa câu hỏi";

            #endregion

            #region Author error

            InnerHashtable[ErrorCodes.ValidAuthorNameExist] = "Tên tác giả đã tồn tại";

            #endregion

            #region VideoChannel errors
                        
            InnerHashtable[ErrorCodes.VideoChannelTitleCouldNotBeEmpty] = "Tiêu đề của kênh không được trống";

            #endregion

            #region playlist errors

            InnerHashtable[ErrorCodes.PlayListNotAllowToSend] = "Không có quyền gửi PlayList này.";
            InnerHashtable[ErrorCodes.PlayListNotAvailable] = "Không tồn tại PlayList này.";
            InnerHashtable[ErrorCodes.PlayListNotAllowToReturn] = "Không được phép trả lại PlayList này.";
            InnerHashtable[ErrorCodes.PlayListNotPublish] = "Để xuất bản playlist, thì trong playlist phải có video đã xuất bản!";

            #endregion

            #region #
            InnerHashtable[ErrorCodes.ParamNewsIdNotExit] = "Tham số NewsId là bắt buộc.";
            InnerHashtable[ErrorCodes.AcctionIsNotSupport] = "Action này không được hỗ trợ.";
            InnerHashtable[ErrorCodes.AccountCrawlerUseApi] = "Chỉ tài khoản crawler mới được sử dụng api này.";
            InnerHashtable[ErrorCodes.ParamUrlNotExit] = "Tham số url là bắt buộc.";
            InnerHashtable[ErrorCodes.NotFileHtmlRootFolder] = "Không tìm thấy file index.html tại thư mục gốc";
            InnerHashtable[ErrorCodes.BodyHtmlNotClassMagazineObject] = "Nội dung file html không có thẻ div với thuộc tính magazine-object=comment-box";
            InnerHashtable[ErrorCodes.IdsNotEmpty] = "ids không được để trống!";

            InnerHashtable[ErrorCodes.KeyVideoNotEmpty] = "KeyVideo không được để trống.";
            InnerHashtable[ErrorCodes.NameVideoNotEmpty] = "Tên video không được để trống.";
            InnerHashtable[ErrorCodes.NotFindKeyVideo] = "Không tìm thấy keyVideo.";
            InnerHashtable[ErrorCodes.YouNotPermissionPage] = "Bạn không có quyền truy cập trang này. Thanks!";
            InnerHashtable[ErrorCodes.DateTimeNotFormat] = "Ngày tháng không theo định dạng dd/MM/yyyy";
            InnerHashtable[ErrorCodes.NameLableNotEmpty] = "Tên nhãn không được để trống.";
            InnerHashtable[ErrorCodes.NotifyApiNotSupport] = "Thông báo: Hiện tại api không hỗ trợ nữa. Thanks!";

            InnerHashtable[ErrorCodes.NotFoundExpert] = "Không tìm thấy chuyên gia";
            InnerHashtable[ErrorCodes.NotFoundThread] = "Không tìm thấy dòng thời gian";
            InnerHashtable[ErrorCodes.LinkNotEmpty] = "Link không được để trống.";
            InnerHashtable[ErrorCodes.WebsiteNotIdentifyTryAgainAfter5Minute] = "Website này chưa được nhận diện, hãy bấm vào báo lỗi và thử lại sau 5 phút.";
            InnerHashtable[ErrorCodes.ContentNotEmpty] = "Nội dung không được để trống.";
            InnerHashtable[ErrorCodes.NotCheckContent] = "Không kiểm tra được nội dung.";
            InnerHashtable[ErrorCodes.NotFoundRollingNews] = "Không tìm thấy sự kiện";
            InnerHashtable[ErrorCodes.NotFoundChuDe] = "Không tìm thấy chủ đề";

            InnerHashtable[ErrorCodes.NamePlayListNotEmpty] = "Tên playlist không được để trống.";
            InnerHashtable[ErrorCodes.CodeUserIsGreaterThan20Characte] = "Mã nhân viên không được nhập lớn hơn 20 ký tự.";
            InnerHashtable[ErrorCodes.NameProgramNotEmpty] = "Tên Chương Trình không được để trống.";
            InnerHashtable[ErrorCodes.NameScheduleNotEmpty] = "Tên lịch phát sóng không được để trống.";
            InnerHashtable[ErrorCodes.TitleScheduleNotEmpty] = "Tiêu đề lịch không được để trống.";
            InnerHashtable[ErrorCodes.NotSelectFileUpload] = "Chưa chọn file cần upload!";
            InnerHashtable[ErrorCodes.NotThreaObject] = "Không tìm thấy dòng thời gian";
            InnerHashtable[ErrorCodes.NotTopicObject] = "Không tìm thấy chủ đề";
            InnerHashtable[ErrorCodes.NameVideoChannelNotEmpty] = "Tên kênh không được để trống.";

            InnerHashtable[ErrorCodes.SocialNewsUrlDulicate] = "Link đã được lấy tin bài về.";
            InnerHashtable[ErrorCodes.FileUploadTitleDulicate] = "Tên file upload đã tồn tại.";

            #endregion
        }

        [DataContract]
        public enum ErrorCodes
        {
            #region General error

            [EnumMember]
            Success = 0,
            [EnumMember]
            UnknowError = 9999,
            [EnumMember]
            Exception = 9998,
            [EnumMember]
            BusinessError = 9997,
            [EnumMember]
            InvalidRequest = 9996,
            [EnumMember]
            TimeOutSession = -100,
            [EnumMember]
            DuplicateError = 19997,

            #endregion

            #region User error

            [EnumMember]
            UpdateAccountUserNotFound = 01000,
            [EnumMember]
            UpdateAccountInvalidUsername = 01001,
            [EnumMember]
            UpdateAccountInvalidPassword = 01002,
            [EnumMember]
            UpdateAccountInvalidRetypePassword = 01003,
            [EnumMember]
            UpdateAccountInvalidEmail = 01004,
            [EnumMember]
            UpdateAccountInvalidMobile = 01005,
            [EnumMember]
            UpdateAccountUsernameExists = 01006,
            [EnumMember]
            UpdateAccountEmailExists = 01007,
            [EnumMember]
            UpdateAccountInvalidOldPassword = 01008,
            [EnumMember]
            UpdateAccountPermissionNotFound = 01009,
            [EnumMember]
            UpdateAccountZoneNotFound = 01010,
            [EnumMember]
            UpdateAccountUserHasBeenLocked = 01011,
            [EnumMember]
            UpdateAccountOldAndNewPasswordNotMatch = 01012,
            [EnumMember]
            UpdateAccountInvalidStatus = 01013,
            [EnumMember]
            UpdateAccountCantNotEditSystem = 01014,

            [EnumMember]
            ValidAccountInvalidUsername = 02001,
            [EnumMember]
            ValidAccountInvalidPassword = 02002,
            [EnumMember]
            ValidAccountUserLocked = 02003,
            [EnumMember]
            ValidAccountInvalidSmsCode = 02004,
            [EnumMember]
            ValidAccountNotHavePermission = 02005,

            [EnumMember]
            SwitchCurrentRoleUserNotFound = 02006,
            [EnumMember]
            SwitchCurrentRoleInvalidRole = 02007,
            [EnumMember]
            SwitchCurrentRoleUserNotHaveRole = 02008,

            #endregion

            #region News error

            [EnumMember]
            UpdateNewsNewsNotFound = 03000,
            [EnumMember]
            UpdateNewsInvalidPrimaryZone = 03001,
            [EnumMember]
            UpdateNewsInvalidTitle = 03002,
            [EnumMember]
            UpdateNewsInvalidAvatar = 03003,
            [EnumMember]
            UpdateNewsInvalidSapo = 03004,
            [EnumMember]
            UpdateNewsInvalidBody = 03005,
            [EnumMember]
            UpdateNewsNotAllowEdit = 03006,
            [EnumMember]
            UpdateNewsNotAllowDelete = 03007,
            [EnumMember]
            UpdateNewsDuplicateId = 03008,
            [EnumMember]
            UpdateNewsLockedByOtherUserForEdit = 03009,
            [EnumMember]
            UpdateNewsInvalidTag = 030010,
            [EnumMember]
            UpdateNewsTitleIsExisted = 03011,

            [EnumMember]
            UpdateNewsPositionNotAllowToUpdate = 04051,
            [EnumMember]
            UpdateNewsPositionHasBeenLocked = 04052,
            [EnumMember]
            UpdateNewsPositionInvalidUpdateGroup = 04053,

            [EnumMember]
            UpdateNewsNotifictionInvalidUser = 04031,
            [EnumMember]
            UpdateNewsNotifictionInvalidLabel = 04032,

            [EnumMember]
            ChangeNewsStatusNewsNotFound = 04001,
            [EnumMember]
            ChangeNewsStatusNotAllowToSend = 04002,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceive = 04003,
            [EnumMember]
            ChangeNewsStatusNotAllowToPublish = 04004,
            [EnumMember]
            ChangeNewsStatusNotAllowToUnpublish = 04005,
            [EnumMember]
            ChangeNewsStatusNotAllowToReturn = 04006,
            [EnumMember]
            ChangeNewsStatusNotAllowToGetBack = 04007,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromReturnStore = 04008,
            [EnumMember]
            ChangeNewsStatusNotAllowToMoveToTrash = 04009,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromTrash = 04010,
            [EnumMember]
            ChangeNewsStatusNotAllowToForward = 04011,
            [EnumMember]
            ChangeNewsStatusNotAllowToReturnYourself = 04012,
            [EnumMember]
            ChangeNewsStatusNotAllowToRelease = 04013,
            [EnumMember]
            ChangeNewsStatusNotAllowToReceiveFromCooperator = 04014,
            [EnumMember]
            KhongTimThayNguoiXuLy = 05003,
            [EnumMember]
            TieuDeBaiVietVuotQua255KyTu = 05004,

            #endregion

            #region Tag error

            [EnumMember]
            UpdateTagTagNotFound = 05000,
            [EnumMember]
            UpdateTagInvalidTagName = 05001,
            [EnumMember]
            UpdateTagConflictTagName = 05002,
            [EnumMember]
            DeleteTagNotPermission = 05003,

            #endregion

            #region Zone error
            [EnumMember]
            UpdateZoneNotAllow = 06000,
            [EnumMember]
            UpdateZoneInvalidName = 06001,
            #endregion

            #region Comment error
            #endregion

            #region Video
            [EnumMember]
            VideoNotAvailable = 07001,
            [EnumMember]
            VideoTitleCouldNotBeEmpty = 07002,
            [EnumMember]
            VideoMustHaveFileToSend = 07003,
            [EnumMember]
            VideoMustHaveFileToPublish = 07004,
            [EnumMember]
            VideoNotAllowToSend = 07005,
            [EnumMember]
            VideoNotAllowToReturn = 07006,
            [EnumMember]
            VideoNotAllowToUpdate = 07007,
            [EnumMember]
            VideoNotAllowToPublish = 07008,
            [EnumMember]
            VideoNotAllowToUnpublish = 07009,
            [EnumMember]
            VideoNotAllowToDelete = 07010,
            [EnumMember]
            YoutubeVideoUrlExists = 07011,
            [EnumMember]
            ZoneVideoMustHaveName = 07012,            
            [EnumMember]
            VideoTagMustHaveName = 07013,
            [EnumMember]
            VideoPlaylistMustHaveName = 07014,
            [EnumMember]
            VideoLabelMustHaveName = 07015,
            [EnumMember]
            ProgramChannelMustHaveName = 07016,
            [EnumMember]
            ExceptionError0984639770 = 0984639770,
            #endregion

            #region Photo

            [EnumMember]
            PhotoNotFound = 08001,
            [EnumMember]
            PhotoNotAllowEdit = 08002,
            [EnumMember]
            PhotoNotAllowToSend = 07005,
            [EnumMember]
            PhotoNotAllowToReturn = 07006,
            [EnumMember]
            PhotoNotAllowToPublish = 08008,
            [EnumMember]
            PhotoNotAllowToUnpublish = 07009,
            #endregion

            #region Mp3

            #endregion

            #region NewsLive
            [EnumMember]
            UpdateNewsLiveNotPermission = 09000,
            [EnumMember]
            UpdateNewsLiveNotAllowPublish = 09001,
            [EnumMember]
            UpdateNewsLiveNotAllowDelete = 09002,
            [EnumMember]
            NewsLiveIsFinished = 09003,
            #endregion

            #region Thread error

            [EnumMember]
            UpdateThreadThreadNotFound = 10000,
            [EnumMember]
            UpdateThreadInvalidThreadName = 10001,
            [EnumMember]
            UpdateThreadConflictThreadName = 10002,

            #endregion

            #region Interview error

            [EnumMember]
            UpdateInterviewNotSuccess = 11000,
            [EnumMember]
            UpdateInterviewNotFound = 11001,
            [EnumMember]
            UpdateInterviewTitleIsEmpty = 11002,
            [EnumMember]
            UpdateInterviewCannotPublishEmptyContent = 11003,

            [EnumMember]
            UpdateInterviewChannelUsernameNotValid = 11004,
            [EnumMember]
            UpdateInterviewChannelInterviewNotFound = 11005,
            [EnumMember]
            UpdateInterviewChannelUserAssignedSameRole = 11006,
            [EnumMember]
            UpdateInterviewChannelInterviewChannelNotFound = 11007,
            [EnumMember]
            UpdateInterviewChannelNotSuccess = 11008,
            [EnumMember]
            UpdateInterviewChannelNotAllowToUpdate = 11021,
            [EnumMember]
            UpdateInterviewChannelAlreadyAnswerQuestion = 11024,

            [EnumMember]
            UpdateInterviewQuestionInvalidAnswer = 11009,
            [EnumMember]
            UpdateInterviewQuestionInvalidFullName = 11010,
            [EnumMember]
            UpdateInterviewQuestionInterviewNotFound = 11011,
            [EnumMember]
            UpdateInterviewQuestionInterviewQuestionNotFound = 11012,
            [EnumMember]
            UpdateInterviewQuestionNotSuccess = 11013,
            [EnumMember]
            UpdateInterviewQuestionChannelNotFound = 11014,
            [EnumMember]
            UpdateInterviewQuestionMustDistributeToChannelUserOnly = 11015,
            [EnumMember]
            UpdateInterviewQuestionNotAllowDistributeThisQuestion = 11016,
            [EnumMember]
            UpdateInterviewQuestionNotAllowForwardThisQuestion = 11017,
            [EnumMember]
            UpdateInterviewQuestionNotAllowSendAnswerForThisQuestion = 11018,
            [EnumMember]
            UpdateInterviewQuestionNotAllowPublishThisQuestion = 11019,
            [EnumMember]
            UpdateInterviewQuestionNotAllowUnpublishThisQuestion = 11020,
            [EnumMember]
            UpdateInterviewQuestionNotAllowDeleteThisQuestion = 11021,
            [EnumMember]
            UpdateInterviewQuestionNotAllowEditThisQuestion = 11022,
            [EnumMember]
            UpdateInterviewQuestionNotAllowReturnThisQuestion = 11023,

            [EnumMember]
            InsertInterviewGuestNotSuccess = 11024,
            [EnumMember]
            UpdateInterviewGuestNotSuccess = 11025,
            [EnumMember]
            UpdateInterviewGuestFullNameIsEmpty = 11026,

            [EnumMember]
            InsertInterviewAnswerNotSuccess = 11024,
            [EnumMember]
            UpdateInterviewAnswerNotSuccess = 11025,
            #endregion

            #region author error
            [EnumMember]
            ValidAuthorNameExist = 11027,
            #endregion

            #region VideoChannel            
            [EnumMember]
            VideoChannelTitleCouldNotBeEmpty = 12002,
            #endregion

            #region EndUser            
            [EnumMember]
            EndUserTitleCouldNotBeEmpty = 13002,
            #endregion

            #region playlist
            [EnumMember]
            PlayListNotAvailable = 17001,
            [EnumMember]
            PlayListNotAllowToSend = 17005,
            [EnumMember]
            PlayListNotAllowToReturn = 17006,
            [EnumMember]
            PlayListNotPublish = 17007,
            #endregion

            #region #
            [EnumMember]
            ParamNewsIdNotExit = 18000,
            AcctionIsNotSupport=18001,
            AccountCrawlerUseApi=18002,
            ParamUrlNotExit=18003,
            NotFileHtmlRootFolder=18004,
            BodyHtmlNotClassMagazineObject=18005,
            IdsNotEmpty=18006,

            KeyVideoNotEmpty=18007,
            NameVideoNotEmpty=18008,
            NotFindKeyVideo=18009,
            YouNotPermissionPage=18010,
            DateTimeNotFormat=18011,
            NameLableNotEmpty=18012,
            NotifyApiNotSupport=18013,

            NotFoundExpert=18014,
            NotFoundThread=18015,
            LinkNotEmpty=18016,
            WebsiteNotIdentifyTryAgainAfter5Minute=18017,
            ContentNotEmpty=18018,
            NotCheckContent=18019,
            NotFoundRollingNews=18020,
            NotFoundChuDe=18021,

            NamePlayListNotEmpty=18022,
            CodeUserIsGreaterThan20Characte=18023,
            NameProgramNotEmpty=18024,
            NameScheduleNotEmpty=18025,
            TitleScheduleNotEmpty=18026,
            NotSelectFileUpload=18027,
            NotThreaObject=18028,
            NotTopicObject=18029,
            NameVideoChannelNotEmpty=18030,
            
            SocialNewsUrlDulicate = 18031,
            FileUploadTitleDulicate = 18032,

            #endregion
        }        
    }    
}
