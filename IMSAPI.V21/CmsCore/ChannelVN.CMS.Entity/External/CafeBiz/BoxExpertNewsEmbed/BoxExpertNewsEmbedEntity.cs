﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed
{
    [DataContract]
    public class BoxExpertNewsEmbedEntity : EntityBase
    {
        [DataMember]
        public long ZoneId { get; set; }

        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }
    }

    [DataContract]
    public class BoxExpertNewsEmbedListEntity : EntityBase
    {

        [DataMember]
        public long NewsId { get; set; }

        [DataMember]
        public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }

        [DataMember]
        public string Avatar { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int SortOrder { get; set; }

        [DataMember]
        public int Type { get; set; }
    }
}
