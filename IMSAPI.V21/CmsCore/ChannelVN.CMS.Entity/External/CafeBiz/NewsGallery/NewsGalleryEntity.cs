﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.External.CafeBiz.NewsGallery
{
    [DataContract]
    public class NewsGalleryEntity : EntityBase
    {
        [DataMember]
        public int SlideId { get; set; }
        [DataMember]
        public string SlideTitle { get; set; }
        [DataMember]
        public int SlideOrder { get; set; }
        [DataMember]
        public string SlideDesc { get; set; }
        [DataMember]
        public string SlideImage { get; set; }
        [DataMember]
        public string SlideVideo { get; set; }
        [DataMember]
        public string SlideStatus { get; set; }
        [DataMember]
        public DateTime SlideDate { get; set; }
        [DataMember]
        public long NewsId { get; set; }
        [DataMember]
        public bool FullImage { get; set; }
        [DataMember]
        public bool NoImage { get; set; }
    }
}
