﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.External.CafeF.Expert
{
    [DataContract]
    public class ExpertEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string ExpertName { set; get; }
        [DataMember]
        public string JobTitle { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public bool DisplayJobTitle { set; get; }
        [DataMember]
        public string Quote { set; get; }
        [DataMember]
        public bool AcceptFeedback { set; get; }
        [DataMember]
        public string FacebookLink { set; get; }
        [DataMember]
        public string Email { set; get; }

    }
}
