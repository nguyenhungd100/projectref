﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Entity.External.CafeF.Expert
{
    [DataContract]
    public class ExpertInNews
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public int ExpertId { set; get; }
        [DataMember]
        public string Quote { set; get; }
    }
}
