﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler
{
    [DataContract]
    public class BusinessNewsCrawlerDetailEntity
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public int Source_ID { set; get; }
        [DataMember]
        public string Link { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Image { set; get; }
        [DataMember]
        public string FileAttach { set; get; }
        [DataMember]
        public string Sapo { set; get; }
        [DataMember]
        public string Content { set; get; }
        [DataMember]
        public DateTime LastModified { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public string SourceName { set; get; }
    }
}
