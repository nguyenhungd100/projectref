﻿using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler
{
    [DataContract]
    public class BusinessNewsCrawlerSourceEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Symbol { set; get; }
    }
}
