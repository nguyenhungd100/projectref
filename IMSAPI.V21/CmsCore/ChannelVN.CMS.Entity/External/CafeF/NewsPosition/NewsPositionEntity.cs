﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.CafeF.NewsPosition
{
    [DataContract]
    public enum NewsPositionEditGroup
    {
        [EnumMember]
        ForHomePage = 1,
        [EnumMember]
        ForListPage = 2
    }
    [DataContract]
    public enum NewsPositionType
    {
        [EnumMember]
        HighlightHomeFocus = 1,
        [EnumMember]
        FirstPageHome = 2,
        [EnumMember]
        HighlightListFocusByZone = 3,
        [EnumMember]
        FirstPageListByZone = 4,
        [EnumMember]
        TimelineLockedPositionOnHome = 5
    }

    [DataContract]
    public class NewsPositionForHomePageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightHomeFocus;
        [DataMember]
        public List<NewsPositionEntity> FocusPositionOnLastestNews;
    }
    [DataContract]
    public class NewsPositionForListPageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightListFocusByZone;
        [DataMember]
        public List<NewsPositionEntity> FocusPositionOnLastestNews;
    }
}
