﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.Kenh14.NewsPosition
{
    [DataContract]
    public enum NewsDisplayPosition
    {
        [EnumMember]
        Normal = 0,
        [EnumMember]
        HomePage = 1,
        [EnumMember]
        ListPage = 2
    }    
}
