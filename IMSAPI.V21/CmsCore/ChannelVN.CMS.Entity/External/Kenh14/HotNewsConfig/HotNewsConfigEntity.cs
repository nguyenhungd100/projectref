﻿using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig
{
    [DataContract]
    public class HotNewsConfigEntity : EntityBase
    {
        [DataMember]
        public int ZoneId { get; set; }
        [DataMember]
        public int ZoneParentId { get; set; }
        [DataMember]
        public string ZoneName { get; set; }
        [DataMember]
        public int MinViewCount { get; set; }
    }
}
