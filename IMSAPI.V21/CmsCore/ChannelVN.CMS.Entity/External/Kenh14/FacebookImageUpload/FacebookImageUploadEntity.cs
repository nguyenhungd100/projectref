﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.Kenh14.FacebookImageUpload
{
    public class FacebookImageUploadEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Image { set; get; }
        [DataMember]
        public string Sapo { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public DateTime DateCreate { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public string FbUserId { set; get; }
        [DataMember]
        public string FbName { set; get; }
        [DataMember]
        public string FbAvatar { set; get; }
        [DataMember]
        public string FbEmail { set; get; }
        [DataMember]
        public string Btv { set; get; }
        [DataMember]
        public int Sort { set; get; }

    }
}
