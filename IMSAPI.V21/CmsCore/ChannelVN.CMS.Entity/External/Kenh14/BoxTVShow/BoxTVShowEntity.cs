﻿using System;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow
{
    [DataContract]
    public class BoxTVShowEntity : EntityBase
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public DateTime DisplayTime { get; set; }
        [DataMember]
        public string Icon { get; set; }
        [DataMember]
        public string Note { get; set; }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public DateTime ModifiedDate { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string ModifiedBy { get; set; }
        [DataMember]
        public string TagName { get; set; }
        [DataMember]
        public string TagUrl { get; set; }
        [DataMember]
        public int TagId { get; set; }
    }
}
