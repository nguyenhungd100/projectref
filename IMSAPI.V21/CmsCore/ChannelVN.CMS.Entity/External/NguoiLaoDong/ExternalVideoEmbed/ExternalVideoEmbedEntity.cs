﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed
{
    [DataContract]
    public class ExternalVideoEmbedEntity : EntityBase
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public int ZoneId { set; get; }
        [DataMember]
        public string Title { set; get; }
        [DataMember]
        public string Avatar { set; get; }
        [DataMember]
        public string Url { set; get; }
        [DataMember]
        public string Sapo { set; get; }
        [DataMember]
        public int SortOrder { set; get; }
        [DataMember]
        public int Type { set; get; }
        [DataMember]
        public DateTime LastModifyDate { set; get; }
        [DataMember]
        public string KeyVideo { set; get; }
        [DataMember]
        public DateTime DistributionDate { set; get; }
        [DataMember]
        public string FileName { set; get; }
    }
}
