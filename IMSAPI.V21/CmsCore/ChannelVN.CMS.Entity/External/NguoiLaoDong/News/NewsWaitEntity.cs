﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.Entity.External.NguoiLaoDong.NewsWaitEntity
{
    [DataContract]
    public enum NewsWaitStatus
    {
        [EnumMember]
        All = 0,
        [EnumMember]
        Wating = 1,
        [EnumMember]
        Processed = 2
    }
}
