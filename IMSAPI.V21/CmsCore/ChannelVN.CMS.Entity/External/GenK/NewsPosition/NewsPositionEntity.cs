﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.GenK.NewsPosition
{
    [DataContract]
    public enum NewsPositionEditGroup
    {
        [EnumMember]
        ForHomePage = 1,
        [EnumMember]
        ForListPage = 2
    }
    [DataContract]
    public enum NewsPositionType
    {
        [EnumMember]
        HomeNewsFocus = 1,
        [EnumMember]
        HomeLastestNewsOnHome = 2,
        [EnumMember]
        HomeLastestNewsAll = 3,
        [EnumMember]
        NewsFocusByZone = 4,
        [EnumMember]
        LastestNewsOnHomeByZone = 5,
        [EnumMember]
        LastestNewsAllByZone = 6,
        [EnumMember]
        LockedTimelineOnHome = 7
    }
    [DataContract]
    public class NewsPositionForHomePageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightHomeFocus;
        //[DataMember]
        //public List<NewsPositionEntity> LockedTimelineOnHome;
    }
    [DataContract]
    public class NewsPositionForListPageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightListFocusByZone;
    }
}
