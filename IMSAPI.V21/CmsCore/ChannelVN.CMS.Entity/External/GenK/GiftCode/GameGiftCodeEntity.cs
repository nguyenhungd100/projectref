﻿using ChannelVN.CMS.Common;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.External.GenK.GiftCode
{
    [DataContract]
    public class GameGiftCodeEntity : EntityBase
    {
        [DataMember]
        public int UsedCode { get; set; }
        [DataMember]
        public int FreeCode { get; set; }
    }
}
