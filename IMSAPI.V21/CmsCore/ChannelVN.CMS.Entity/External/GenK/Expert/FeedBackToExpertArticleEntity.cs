﻿using System;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.Entity.External.GenK.Expert
{
    [DataContract]
    public class FeedBackToExpertArticleEntity
    {
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public string NewsTitle { set; get; }
        [DataMember]
        public string NewsUrl { set; get; }
        [DataMember]
        public string Email { set; get; }
        [DataMember]
        public string FullName { set; get; }
        [DataMember]
        public string PhoneNumber { set; get; }
        [DataMember]
        public string Message { set; get; }
        [DataMember]
        public int ExpertId { set; get; }
        [DataMember]
        public string ExpertName { set; get; }
        [DataMember]
        public DateTime SendDate { set; get; }
        [DataMember]
        public int Status { set; get; }
        [DataMember]
        public string ExpertEmail { set; get; }

    }
}
