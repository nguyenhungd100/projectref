﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.GameK.NewsPosition
{
    [DataContract]
    public enum NewsPositionEditGroup
    {
        [EnumMember]
        ForHomePage = 1,
        [EnumMember]
        ForListPage = 2
    }
    [DataContract]
    public enum NewsPositionType
    {
        [EnumMember]
        HomeNewsFocus = 1,
        [EnumMember]
        HomeLastestNews = 2,
        [EnumMember]
        NewsFocusByZone = 3,
        [EnumMember]
        LastestNewsByZone = 4
    }
    [DataContract]
    public class NewsPositionForHomePageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightHomeFocus;
    }
    [DataContract]
    public class NewsPositionForListPageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightListFocusByZone;
    }
}
