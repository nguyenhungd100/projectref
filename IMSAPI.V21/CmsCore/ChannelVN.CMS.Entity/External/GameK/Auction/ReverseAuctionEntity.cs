﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.GameK.Auction
{
    [DataContract]
    public class ReverseAuctionEntity : EntityBase
    {
        [DataMember]
        public long Id { set; get; }
        [DataMember]
        public long NewsId { set; get; }
        [DataMember]
        public string FullName { set; get; }
        [DataMember]
        public string Mobile { set; get; }
        [DataMember]
        public string FacebookLink { set; get; }
        [DataMember]
        public decimal BidPrice { set; get; }
        [DataMember]
        public DateTime BidTime { set; get; }
        [DataMember]
        public int Status { set; get; }
    }
}
