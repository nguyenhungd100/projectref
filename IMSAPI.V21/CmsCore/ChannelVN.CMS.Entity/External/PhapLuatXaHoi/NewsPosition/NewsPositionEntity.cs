﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.Entity.External.PhapLuatXaHoi.NewsPosition
{
    [DataContract]
    public enum NewsPositionEditGroup
    {
        [EnumMember]
        ForHomePage = 1,
        [EnumMember]
        ForListPage = 2
    }
    [DataContract]
    public enum NewsPositionType
    {
        [EnumMember]
        HomeNewsFocus = 1,
        [EnumMember]
        TopLastestNewsForHome = 2,
        [EnumMember]
        NewsForcusByZone = 3,
        [EnumMember]
        TopLastestNewsOnListByZone = 4
    }
    [DataContract]
    public class NewsPositionForHomePageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightHomeFocus;
        [DataMember]
        public List<NewsPositionEntity> TopLastestNewsForHome;
    }
    [DataContract]
    public class NewsPositionForListPageEntity : EntityBase
    {
        [DataMember]
        public List<NewsPositionEntity> HighlightListFocusByZone;
    }
}
