﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.External.GameK;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.GameK.Auction;

namespace ChannelVN.CMS.BO.External.GameK.Auction
{
    public class ReverseAuctionBo
    {
        public static ErrorMapping.ErrorCodes AddNew(ReverseAuctionEntity auction, ref int newAuctionId)
        {
            try
            {
                if (ReverseAuctionDal.AddNewAuction(auction, ref newAuctionId))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(ReverseAuctionEntity auction)
        {
            var returnData = ReverseAuctionDal.UpdateAuctionById(auction) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            return returnData;
        }

        public static ErrorMapping.ErrorCodes Delete(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.DeleteUserById(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }

        public static ReverseAuctionEntity GetById(long id)
        {
            if (id <= 0)
            {
                return null;
            }
            var auctionEntity = ReverseAuctionDal.GetById(id);
            return auctionEntity;
        }

        public static List<ReverseAuctionEntity> Search(string keyword,
                                                            int status,
                                                            long newsId,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
        {
            var auctionList = ReverseAuctionDal.Search(keyword, status, newsId, pageIndex, pageSize, ref totalRow);

            return auctionList;
        }

    }
}
