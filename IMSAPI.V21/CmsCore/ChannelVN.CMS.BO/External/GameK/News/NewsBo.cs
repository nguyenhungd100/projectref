﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.BO.External.GameK.News
{
    public class NewsBo : Base.News.NewsBo
    {
        private static readonly IDictionary<string, List<string>> DictsNewsTitleUpdating = new Dictionary<string, List<string>>();

        public static ErrorMapping.ErrorCodes InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
            string newsRelationSpecialIdList,
                                            string usernameForUpdateAction, ref long newNewsId,
                                            ref string newEncryptNewsId, List<string> authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            try
            {
                var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
                var RelationFormatJson = bool.Parse(BoConstants.RelationFormatJson);
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }
                if (null == news || string.IsNullOrEmpty(news.Title))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle;
                }

                #region Updating News

                if (IsUpdatingThisNews(news.Title))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                UpdatingThisNews(news.Title);

                #endregion

                if (zoneId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
                }

                newNewsId = SetPrimaryId();
                news.Id = newNewsId;

                //var existsNews = NewsDal.GetNewsForValidateById(news.Id);
                var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsForValidateById(news.Id);
                if (null != existsNews)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsDuplicateId;
                }
                //var existsZone = ZoneDal.GetZoneById(zoneId);
                var existsZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                if (null == existsZone)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
                }

                var newsRelationList = "";
                if (null != newsRelationIdList && newsRelationIdList.Any())
                {
                    try
                    {
                        var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                        if (null != relatedNews && relatedNews.Count > 0)
                        {
                            if (RelationFormatJson)
                                newsRelationList = NewtonJson.Serialize(relatedNews);
                            else
                            {
                                foreach (var itemRelation in relatedNews)
                                {
                                    newsRelationList += "," + itemRelation.NewsId;
                                }
                                if (newsRelationList != string.Empty) newsRelationList = newsRelationList.Remove(0, 1);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
                news.NewsRelation = newsRelationList;

                var tagLinkList = "";

                if (news.TagSubTitleId > 0)
                    tagIdList += (!string.IsNullOrEmpty(tagIdList) ? ";" : "") + news.TagSubTitleId;
                var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);

                var tagFormat = BoConstants.NewsUrlFormatForTag;
                var tagItems = "";
                var TagJSON = "";
                if (tagList != null && tagList.Count > 0)
                {
                    var lstTag = new List<TagJson>();
                    foreach (var tag in tagList)
                    {
                        if (TagFormatJson)
                        {
                            var tagTmp = new TagJson
                            {
                                Id = tag.Id,
                                Name = tag.Name,
                                Url = tag.Url
                            };
                            lstTag.Add(tagTmp);
                        }
                        else
                        {
                            tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        }
                        tagItems += ";" + tag.Name;
                    }
                    //build tag to json
                    // var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    if (TagFormatJson)
                    {
                        TagJSON = NewtonJson.Serialize(lstTag);
                    }
                    else
                    {
                        if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    }
                    if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                }

                news.TagPrimary = tagIdListForPrimary;
                news.Tag = TagJSON;//tagLinkList;
                news.TagItem = tagItems;

                /* Build link theo primary zone */
                //var primaryZone = ZoneDal.GetZoneById(zoneId);
                var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
                if (null == primaryZone)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
                }
                news.Url = BuildLinkUrl(newNewsId, news.Type, primaryZone.ShortUrl, news.Title, zoneId);

                if (news.AdStore)
                {
                    news.AdStoreUrl = BuildLinkUrlAdStore(newNewsId, primaryZone.ShortUrl, primaryZone.Id, news.Title);
                }

                if (news.NewsType == (int)NewsType.EXT)
                {
                    // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"
                    if (
                        CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction,
                                                                                            (int)
                                                                                            EnumPermission.ArticleAdmin) ==
                        ErrorMapping.ErrorCodes.Success)
                    {
                        news.Status = (int)NewsStatus.ReceivedForPublish;
                        news.PublishedBy = usernameForUpdateAction;
                        news.EditedBy = usernameForUpdateAction;
                    }
                    // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận biên tập"
                    else if (
                        CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction,
                                                                                            (int)
                                                                                            EnumPermission.ArticleEditor) ==
                        ErrorMapping.ErrorCodes.Success)
                    {
                        news.Status = (int)NewsStatus.ReceivedForEdit;
                        news.EditedBy = usernameForUpdateAction;
                    }
                    // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
                    else
                    {
                        news.Status = (int)NewsStatus.Temporary;
                    }
                }
                else
                {
                    // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"
                    if (
                        CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction,
                                                                                            (int)
                                                                                            EnumPermission.ArticleAdmin) ==
                        ErrorMapping.ErrorCodes.Success)
                    {
                        news.Status = (int)NewsStatus.ReceivedForPublish;
                        news.PublishedBy = usernameForUpdateAction;
                        news.EditedBy = usernameForUpdateAction;
                        news.CreatedBy = usernameForUpdateAction;
                    }
                    // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận biên tập"
                    else if (
                        CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction,
                                                                                            (int)
                                                                                            EnumPermission.ArticleEditor) ==
                        ErrorMapping.ErrorCodes.Success)
                    {
                        news.Status = (int)NewsStatus.ReceivedForEdit;
                        news.EditedBy = usernameForUpdateAction;
                        news.CreatedBy = usernameForUpdateAction;
                    }
                    // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
                    else
                    {
                        news.Status = (int)NewsStatus.Temporary;
                        news.CreatedBy = usernameForUpdateAction;
                    }
                }

                var inserted = DAL.External.GameK.NewsDal.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary,
                                                  newsRelationIdList, newsRelationSpecialIdList, authorList[0], authorList[1], authorList[2],
                                                  sourceId);
                if (!inserted)
                {
                    newNewsId = 0;
                }
                else
                {
                    foreach (
                        var newsExtension in
                            newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                    {
                        NewsExtensionDal.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                    }
                    NewsNotificationDal.UpdateNotification(newNewsId, usernameForUpdateAction,
                                                           (int)NewsStatus.ReceivedForEdit, false,
                                                           "[" + usernameForUpdateAction + "] vừa viết bài");

                    NewsHistoryBo.InsertNews(newNewsId, news.Status, usernameForUpdateAction);

                    var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                    updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;

                    news.ListZoneId = updateZoneIds;
                    UpdateFirstVersion(news);
                    ReleaseFirstVersion(newNewsId, usernameForUpdateAction);
                }
                NewsUpdated(news.Title);

                CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(newNewsId.ToString());

                if (inserted)
                {
                    newEncryptNewsId = CryptonForId.EncryptId(newNewsId);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                NewsUpdated(news.Title);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
            string newsRelationSpecialIdList,
                                            string usernameForUpdateAction, bool isRebuildLink, ref int newsStatus, List<string> authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
            var RelationFormatJson = bool.Parse(BoConstants.RelationFormatJson);
            if (null == authorList || authorList.Count != 3)
            {
                authorList = new List<string> { "", "", "" };
            }
            Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_1_" + usernameForUpdateAction);
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
            }
            if (null == news || news.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            var existsNews = CacheObjectBase.GetInstance<NewsCached>().GetNewsById(news.Id);
            if (null == existsNews)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            var user = CacheObjectBase.GetInstance<UserCached>().GetUserByUsername(usernameForUpdateAction);
            if (null == user) return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;

            /* Chỉ được quyền edit khi tin ở một trong các trạng thái:
             *  - "Lưu tạm", "Trả lại phóng viên" và người cập nhật là người gửi
             *  - "Chờ biên tập", "Nhận biên tập", "Trả lại biên tập viên" và người cập nhật là người đã nhận biên tập tin này
             *  - "Chờ xuất bản", "Nhận xuất bản", "Đã xuất bản", "Bị gỡ xuống" và người cập nhạt là người đã nhận xuất bản tin này
             *  - "Xóa tạm" và người cập nhật là người cập nhật cuối
             */
            if (
                !(((existsNews.Status == (int)NewsStatus.Temporary ||
                    existsNews.Status == (int)NewsStatus.ReturnedToReporter)
                   && existsNews.CreatedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.WaitForEdit ||
                    existsNews.Status == (int)NewsStatus.ReceivedForEdit ||
                    existsNews.Status == (int)NewsStatus.ReturnedToEditor)
                   && existsNews.EditedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.WaitForPublish ||
                    existsNews.Status == (int)NewsStatus.ReceivedForPublish ||
                    existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished)
                   && existsNews.PublishedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.MovedToTrash)
                   &&
                   existsNews.LastModifiedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished)
                   &&
                   ((existsNews.PublishedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase) ||
                     user.IsFullPermission)))))
            {
                Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_2_" + existsNews.Status);
                // Nếu bài đã xuất bản thì check thêm trường hợp người sửa là thư ký có quyền xử lý chuyên mục
                if (existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished)
                {
                    // Lấy quyền của người nhận
                    var userPermissionsForUpdater = CacheObjectBase.GetInstance<PermissionCached>().GetListByUserName(usernameForUpdateAction);
                    // Người xử lý không có quyền thư ký trong chuyên mục của bài này => không được phép sửa
                    if (!(user.IsFullPermission ||
                          (CacheObjectBase.GetInstance<PermissionCached>().CheckUserInGroupPermission(user.Id,
                                                                         (int)EnumPermission.ArticleAdmin) &&
                           user.IsFullZone) ||
                          IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                          existsNews.ListZoneId, user)))
                    {
                        return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
                    }
                }
                else
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
                }
            }

            var existsZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            var newsRelationList = "";
            try
            {
                var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                if (null != relatedNews && relatedNews.Count > 0)
                {
                    if (!RelationFormatJson)
                    {
                        foreach (var itemRelation in relatedNews)
                        {
                            newsRelationList += "," + itemRelation.NewsId;
                        }
                        if (!string.IsNullOrEmpty(newsRelationList)) newsRelationList = newsRelationList.Remove(0, 1);
                    }
                    else
                    {
                        newsRelationList = NewtonJson.Serialize(relatedNews);
                    }
                    //var newsRelationFormat = AppSettings.GetString(BoConstants.NEWS_FORMAT_NEWSRELATION);
                    //newsRelationList = relatedNews.Aggregate(newsRelationList, (current, newsItem) => current + ("," + string.Format(newsRelationFormat, newsItem.Url, newsItem.Title)));

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            news.NewsRelation = newsRelationList;

            if (news.TagSubTitleId > 0) tagIdList += (!string.IsNullOrEmpty(tagIdList) ? ";" : "") + news.TagSubTitleId;
            var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
            //Convert tag về chuỗi json
            var tagLinkList = "";

            var tagFormat = BoConstants.NewsUrlFormatForTag;
            var tagItems = "";
            string TagJSON = "";
            if (tagList != null && tagList.Count > 0)
            {
                var lstTag = new List<TagJson>();
                foreach (var tag in tagList)
                {
                    if (TagFormatJson)
                    {
                        var tagTmp = new TagJson
                        {
                            Id = tag.Id,
                            Name = tag.Name,
                            Url = tag.Url
                        };
                        lstTag.Add(tagTmp);
                    }
                    else
                    {
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                    }
                    tagItems += ";" + tag.Name;
                }
                //build tag to json
                //var oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                if (TagFormatJson)
                {
                    TagJSON = NewtonJson.Serialize(lstTag);
                }
                else
                {
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                }
                if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
            }
            news.TagPrimary = tagIdListForPrimary;
            news.Tag = TagJSON;//tagLinkList;

            existsNews.NewsRelation = news.NewsRelation;

            existsNews.Author = news.Author;

            existsNews.Avatar = news.Avatar;
            existsNews.AvatarDesc = news.AvatarDesc;
            existsNews.Avatar2 = news.Avatar2;
            existsNews.Avatar3 = news.Avatar3;
            existsNews.Avatar4 = news.Avatar4;
            existsNews.Avatar5 = news.Avatar5;
            existsNews.DistributionDate = news.DistributionDate;
            existsNews.Sapo = news.Sapo;
            existsNews.Title = news.Title;
            existsNews.Body = news.Body;
            existsNews.NewsRelation = news.NewsRelation;
            existsNews.SubTitle = news.SubTitle;
            existsNews.LastModifiedBy = news.LastModifiedBy;
            existsNews.LastModifiedDate = news.LastModifiedDate;
            existsNews.WordCount = news.WordCount;
            existsNews.Source = news.Source;
            existsNews.Tag = news.Tag;
            existsNews.TagPrimary = news.TagPrimary;
            existsNews.Note = news.Note;
            existsNews.Type = news.Type;
            existsNews.NewsType = news.NewsType;
            existsNews.OriginalId = news.OriginalId;
            existsNews.TagItem = tagItems;
            existsNews.InitSapo = news.InitSapo;

            //quangnv added on 07/01/2013
            existsNews.AdStore = news.AdStore;
            existsNews.TagSubTitleId = news.TagSubTitleId;

            existsNews.IsFocus = news.IsFocus;
            existsNews.IsOnHome = news.IsOnHome;
            existsNews.IsOnMobile = news.IsOnMobile;
            existsNews.ThreadId = news.ThreadId;
            existsNews.AvatarCustom = news.AvatarCustom;
            existsNews.DisplayInSlide = news.DisplayInSlide;
            existsNews.DisplayPosition = news.DisplayPosition;
            existsNews.DisplayStyle = news.DisplayStyle;

            existsNews.IsActivePenName = news.IsActivePenName;
            existsNews.IsShowPenNameCTV = news.IsShowPenNameCTV;
            existsNews.PenName = news.PenName;
            existsNews.CmsAccountVietId = news.CmsAccountVietId;

            existsNews.InterviewId = news.InterviewId;
            existsNews.RollingNewsId = news.RollingNewsId;

            existsNews.Priority = news.Priority;
            existsNews.LocationType = news.LocationType;
            existsNews.ExpiredDate = news.ExpiredDate;
            existsNews.SourceURL = news.SourceURL;
            existsNews.BonusPrice = news.BonusPrice;

            // Neu la thu ky thi moi cho sua cac thong tin nay
            if (
                CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(WcfExtensions.WcfMessageHeader.Current.ClientUsername,
                                                 (int)EnumPermission.ArticleAdmin, zoneId) ==
                ErrorMapping.ErrorCodes.Success)
            {
                existsNews.Price = news.Price;
                existsNews.NewsCategory = news.NewsCategory;
                existsNews.NoteRoyalties = news.NoteRoyalties;

                existsNews.TemplateName = news.TemplateName;
                existsNews.TemplateConfig = news.TemplateConfig;

                existsNews.IsBreakingNews = news.IsBreakingNews;
                existsNews.PegaBreakingNews = news.PegaBreakingNews;
            }

            /* Build link theo primary zone */
            var primaryZone = CacheObjectBase.GetInstance<ZoneCached>().GetZoneById(zoneId);
            if (null == primaryZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }
            if (existsNews.Status == (int)NewsStatus.Published)
            {
                if (isRebuildLink)
                    existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title, zoneId);
            }
            else
                existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title, zoneId);

            existsNews.AdStore = news.AdStore;

            if (existsNews.AdStore)
            {
                existsNews.AdStoreUrl = BuildLinkUrlAdStore(existsNews.Id, primaryZone.ShortUrl, primaryZone.Id, news.Title);
            }

            // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"
            if ((existsNews.Status == (int)NewsStatus.WaitForPublish ||
                 existsNews.Status == (int)NewsStatus.Unpublished) &&
                CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin) ==
                ErrorMapping.ErrorCodes.Success)
            {
                existsNews.Status = (int)NewsStatus.ReceivedForPublish;
                existsNews.PublishedBy = usernameForUpdateAction;
            }

                // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận biên tập"
            else if ((existsNews.Status == (int)NewsStatus.WaitForEdit ||
                      existsNews.Status == (int)NewsStatus.ReturnedToEditor) &&
                     CacheObjectBase.GetInstance<PermissionCached>().CheckUserPermission(usernameForUpdateAction,
                                                      (int)EnumPermission.ArticleEditor) ==
                     ErrorMapping.ErrorCodes.Success)
            {
                existsNews.Status = (int)NewsStatus.ReceivedForEdit;
                existsNews.EditedBy = usernameForUpdateAction;
            }
            // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
            else if (existsNews.Status == (int)NewsStatus.ReturnedToReporter)
            {
                existsNews.Status = (int)NewsStatus.Temporary;
                existsNews.CreatedBy = usernameForUpdateAction;
            }

            if (DAL.External.GameK.NewsDal.UpdateNews(existsNews, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, newsRelationSpecialIdList, authorList[0], authorList[1], authorList[2], sourceId))
            {
                NewsExtensionDal.DeleteByNewsId(existsNews.Id);
                foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                {
                    NewsExtensionDal.SetValue(existsNews.Id, newsExtension.Type, newsExtension.Value);
                }

                //NewsHistoryBo.UpdateNews(news.Id, news.Status, usernameForUpdateAction);

                var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;

                news.ListZoneId = updateZoneIds;
                UpdateVersion(existsNews, usernameForUpdateAction);
                ReleaseVersion(news.Id, usernameForUpdateAction);

                newsStatus = existsNews.Status;
                // Nếu bài đã published thì republish lại để update newspublish, newscontent và cache monitor
                if (existsNews.Status == (int)NewsStatus.Published)
                {
                    /* publish bài
                     */
                    NewsDal.ChangeStatusToPublished(existsNews.Id, 0, 0,
                                                    0, 0, usernameForUpdateAction,
                                                    Utility.SetPublishedDate(news.DistributionDate), publishedContent);
                    try
                    {
                        var instance = BoFactory.GetInstance<NewsPositionBo>();
                        instance.UpdateNewsIntoAutoUpdatePosition(existsNews, zoneId, updateZoneIds);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "UpdateNews => UpdateNewsIntoAutoUpdatePosition(" + NewtonJson.Serialize(existsNews) + ") error => " + ex);
                    }
                }
                // Log hành động sửa bài viết
                ActivityBo.LogUpdateNews(existsNews.Id, usernameForUpdateAction, existsNews.Title, existsNews.Status);

                CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(news.Id.ToString());
                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.UnknowError;
        }

        private static void NewsUpdated(string newsTitle)
        {
            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                var currentList = DictsNewsTitleUpdating[accountName];
                if (currentList != null)
                {
                    var index = currentList.FindIndex(item => item == newsTitle);
                    while (index >= 0)
                    {
                        currentList.RemoveAt(index);
                        index = currentList.FindIndex(item => item == newsTitle);
                    }
                }
                DictsNewsTitleUpdating[accountName] = currentList;
            }
        }

        private static bool IsUpdatingThisNews(string newsTitle)
        {
            if (string.IsNullOrEmpty(newsTitle)) return false;

            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            return (DictsNewsTitleUpdating.ContainsKey(accountName) && DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle));
        }

        private static void UpdatingThisNews(string newsTitle)
        {
            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                if (!DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle))
                {
                    DictsNewsTitleUpdating[accountName].Add(newsTitle);
                }
            }
            else
            {
                DictsNewsTitleUpdating.Add(accountName, new List<string> { newsTitle });
            }
        }

        public static NewsDetailForEditEntity GetNewsForEditByNewsId(long newsId, string currentUsername)
        {
            var newsVersionId = "";
            var newsDetail = new NewsDetailForEditEntity();
            //newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeview(ZoneBo.GetListZoneByUsername(currentUsername), "--- ");
            newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(currentUsername, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin), "--");
            newsDetail.NewsInfo = (newsId < 0 ? null : GetNewsByNewsId(newsId, currentUsername, ref newsVersionId));
            if (null != newsDetail.NewsInfo)
            {
                // Zone
                if (newsId > 0)
                {
                    newsDetail.NewsInZone = GetNewsInZoneByNewsId(newsId);
                    newsDetail.NewsRelation = GetRelatedNewsByTypeNewsId(newsId, 1);
                    newsDetail.NewsRelationSpecial = GetRelatedNewsByTypeNewsId(newsId, 2);
                    newsDetail.TagInNews = GetTagNewsWithTagInfoByNewsId(newsId);
                }
                else
                {
                    newsDetail.NewsInZone = GetNewsInZoneByZoneIdList(newsId, newsDetail.NewsInfo.ListZoneId);
                    newsDetail.NewsRelation = GetRelatedNewsByNewsIds(newsDetail.NewsInfo.NewsRelation);
                    newsDetail.TagInNews = GetTagNewsWithTagInfo(newsId, newsDetail.NewsInfo.TagPrimary,
                                                                 newsDetail.NewsInfo.Tag);
                }

                newsDetail.NewsBySource = BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId);
                newsDetail.NewsByAuthor = NewsAuthorBo.GetListInNews(newsId);
                //newsDetail.NewsByAuthor = new List<NewsByAuthorEntity>();
            }
            newsDetail.ListVersion = GetListVersionByNewsId((newsId < 0 ? 0 : newsId), currentUsername);
            //if (newsVersionId > 0)
            //{
            //    var newsChildVersions = NewsChildVersionDal.GetByNewsId(newsVersionId, false);
            //    newsDetail.ListNewsChild = newsChildVersions.Select(newsChildVersion => new NewsChildEntity
            //    {
            //        NewsParentId =
            //            newsChildVersion.
            //            NewsParentId,
            //        Order =
            //            newsChildVersion.
            //            Order,
            //        Body =
            //            newsChildVersion.
            //            Body
            //    }).ToList();
            //}
            //else
            //{
            //    newsDetail.ListNewsChild = NewsChildDal.GetByNewsId(newsId, false);
            //}
            var newsContent = NewsContentDal.GetByNewsId(newsId);
            if (newsContent != null)
            {
                newsDetail.NewsContentWithTemplate = newsContent.Body;
            }
            else
            {
                newsDetail.NewsContentWithTemplate = newsDetail.NewsInfo != null ? newsDetail.NewsInfo.Body : "";
            }
            newsDetail.NewsExtensions = NewsExtensionDal.GetByNewsId(newsId);
            return newsDetail;
        }

    }
}
