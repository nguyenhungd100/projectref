﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.GenK.NewsPosition;
using ChannelVN.CMS.BO.Common;

namespace ChannelVN.CMS.BO.External.GenK.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="listOfFocusPositionOnLastestNews"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsFocusByZone,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage(string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        //public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        //{
        //    var errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
        //    if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    {
        //        var updateForUnlockPosition = new List<UnlockPosition>();
        //        var count = newsPositions.Count;
        //        for (var i = 0; i < count; i++)
        //        {
        //            var newsPosition = newsPositions[i];
        //            NewsPositionType typeId;
        //            if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
        //            {
        //                typeId = NewsPositionType.HomeNewsFocus;
        //            }
        //            if (typeId == NewsPositionType.HomeNewsFocus)
        //            {
        //                if (updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionType.HomeLastestNewsAll && item.ZoneId == 0))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionType.HomeLastestNewsAll,
        //                        ZoneId = 0
        //                    });
        //                }
        //                if (updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionType.HomeLastestNewsOnHome && item.ZoneId == 0))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionType.HomeLastestNewsOnHome,
        //                        ZoneId = 0
        //                    });
        //                }
        //            }
        //            else if (typeId == NewsPositionType.NewsFocusByZone)
        //            {
        //                if (updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionType.LastestNewsOnHomeByZone && item.ZoneId == newsPosition.ZoneId))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionType.LastestNewsOnHomeByZone,
        //                        ZoneId = newsPosition.ZoneId
        //                    });
        //                }
        //                if (updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionType.LastestNewsAllByZone && item.ZoneId == newsPosition.ZoneId))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionType.LastestNewsAllByZone,
        //                        ZoneId = newsPosition.ZoneId
        //                    });
        //                }
        //            }
        //        }
        //        foreach (var position in updateForUnlockPosition)
        //        {
        //            NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //        }
        //    }
        //    return errorCode;
        //}

        //public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        //{
        //    var errorCode = ErrorMapping.ErrorCodes.BusinessError;

        //    if (newsPositions == null || newsPositions.Count <= 0) return errorCode;

        //    try
        //    {
        //        // Truong hop 1 giao dien chi update 1 typeid => lay typeid va zoneid cua ban ghi dau tien
        //        var typeId = newsPositions[0].TypeId;
        //        var zoneId = newsPositions[0].ZoneId;
        //        // Lay danh sach position hien tai tuong ung voi type
        //        var currentNewsPositions = NewsPositionDal.GetListByTypeAndZoneId(typeId, zoneId);

        //        // UPDATE VAO VI TRI CAN CAI NOI BAT
        //        errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);

        //        // UPDATE VI TRI THANH CONG
        //        if (errorCode == ErrorMapping.ErrorCodes.Success)
        //        {
        //            #region Lay ra danh sach cac tin update moi vao vi tri noi bat (keo bang tay tu ben ngoai vao)
        //            var newsIdForAutoUpdate = "";
        //            foreach (var newsPosition in newsPositions)
        //            {
        //                if (
        //                    !currentNewsPositions.Exists(
        //                        item => item.TypeId == newsPosition.TypeId && item.NewsId == newsPosition.NewsId))
        //                {
        //                    newsIdForAutoUpdate += ";" + newsPosition.NewsId;
        //                }
        //            }
        //            #endregion

        //            var updateForUnlockPosition = new List<UnlockPosition>();

        //            if (!string.IsNullOrEmpty(newsIdForAutoUpdate))
        //            {
        //                if (typeId == (int)NewsPositionType.HomeNewsFocus)
        //                {
        //                    #region Noi bat trang chu

        //                    #region Check cac vung auto
        //                    foreach (var newsPosition in newsPositions)
        //                    {
        //                        #region HomeLastestNewsOnHome
        //                        if (!updateForUnlockPosition.Exists(
        //                                item =>
        //                                item.PositionType == (int)NewsPositionType.HomeLastestNewsOnHome &&
        //                                item.ZoneId == 0 && item.NewsId == newsPosition.NewsId))
        //                        {
        //                            updateForUnlockPosition.Add(new UnlockPosition
        //                            {
        //                                PositionType =
        //                                    (int)NewsPositionType.HomeLastestNewsOnHome,
        //                                ZoneId = 0,
        //                                NewsId = newsPosition.NewsId
        //                            });
        //                        }
        //                        #endregion

        //                        #region HomeLastestNewsAll
        //                        if (!updateForUnlockPosition.Exists(
        //                                item =>
        //                                item.PositionType == (int)NewsPositionType.HomeLastestNewsAll &&
        //                                item.ZoneId == 0 && item.NewsId == newsPosition.NewsId))
        //                        {
        //                            updateForUnlockPosition.Add(new UnlockPosition
        //                            {
        //                                PositionType =
        //                                    (int)NewsPositionType.HomeLastestNewsAll,
        //                                ZoneId = 0,
        //                                NewsId = newsPosition.NewsId
        //                            });
        //                        }
        //                        #endregion
        //                    }
        //                    #endregion

        //                    #endregion
        //                }
        //                else if (typeId == (int)NewsPositionType.NewsFocusByZone)
        //                {
        //                    #region Noi bat muc

        //                    #region Check cac vung auto
        //                    foreach (var newsPosition in newsPositions)
        //                    {
        //                        #region LastestNewsOnHomeByZone
        //                        if (!updateForUnlockPosition.Exists(
        //                                item =>
        //                                item.PositionType == (int)NewsPositionType.LastestNewsOnHomeByZone &&
        //                                item.ZoneId == newsPosition.ZoneId && item.NewsId == newsPosition.NewsId))
        //                        {
        //                            updateForUnlockPosition.Add(new UnlockPosition
        //                            {
        //                                PositionType =
        //                                    (int)NewsPositionType.LastestNewsOnHomeByZone,
        //                                ZoneId = newsPosition.ZoneId,
        //                                NewsId = newsPosition.NewsId
        //                            });
        //                        }
        //                        #endregion

        //                        #region LastestNewsAllByZone
        //                        if (!updateForUnlockPosition.Exists(
        //                                item =>
        //                                item.PositionType == (int)NewsPositionType.LastestNewsAllByZone &&
        //                                item.ZoneId == newsPosition.ZoneId && item.NewsId == newsPosition.NewsId))
        //                        {
        //                            updateForUnlockPosition.Add(new UnlockPosition
        //                            {
        //                                PositionType =
        //                                    (int)NewsPositionType.LastestNewsAllByZone,
        //                                ZoneId = newsPosition.ZoneId,
        //                                NewsId = newsPosition.NewsId
        //                            });
        //                        }
        //                        #endregion
        //                    }
        //                    #endregion

        //                    #endregion
        //                }
        //            }

        //            foreach (var position in updateForUnlockPosition)
        //            {
        //                if (position.PositionType == (int) NewsPositionType.HomeLastestNewsOnHome ||
        //                    position.PositionType == (int) NewsPositionType.HomeLastestNewsAll)
        //                {
        //                    //UpdateAutoWhenChangeManualPosition(position.ZoneId,
        //                    //                                   position.PositionType,
        //                    //                                   position.NewsId,
        //                    //                                   ((int)NewsPositionType.HomeNewsFocus).ToString(),
        //                    //                                   false, (position.PositionType == (int)NewsPositionType.HomeLastestNewsOnHome ? 1 : -1), -1, -1, -1, -1);
        //                    BoFactory.GetInstance<NewsPositionBo>()
        //                         .UpdateAutoWhenEditNewsV2(true, position.ZoneId, position.PositionType,
        //                                                   Utility.ConvertToLong(position.NewsId),
        //                                                       ((int)NewsPositionType.HomeNewsFocus).ToString(),
        //                                                       false, (position.PositionType == (int)NewsPositionType.HomeLastestNewsOnHome ? 1 : -1), -1, -1, -1, -1);
        //                }
        //                else
        //                {
        //                    //UpdateAutoWhenChangeManualPosition(position.ZoneId,
        //                    //                                   position.PositionType,
        //                    //                                   position.NewsId,
        //                    //                                   ((int)NewsPositionType.NewsFocusByZone).ToString(),
        //                    //                                   false, (position.PositionType == (int)NewsPositionType.LastestNewsOnHomeByZone ? 1 : -1), -1, -1, -1, -1);
        //                    BoFactory.GetInstance<NewsPositionBo>()
        //                         .UpdateAutoWhenEditNewsV2(true, position.ZoneId, position.PositionType,
        //                                                   Utility.ConvertToLong(position.NewsId),
        //                                                       ((int)NewsPositionType.NewsFocusByZone).ToString(),
        //                                                       false, (position.PositionType == (int)NewsPositionType.LastestNewsOnHomeByZone ? 1 : -1), -1, -1, -1, -1);
        //                }
        //                //NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //    }
        //    return errorCode;
        //}

        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.HomeLastestNewsAll, "0", news.Id,
                                                             primaryZoneId);
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.LastestNewsAllByZone, allZoneIdIncludePrimary, news.Id,
                                                             0);
            if (news.IsOnHome)
            {
                NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.HomeLastestNewsOnHome, "0", news.Id,
                                                                 primaryZoneId);
                NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.LastestNewsOnHomeByZone, allZoneIdIncludePrimary, news.Id,
                                                                 0);
            }
            else
            {
                NewsPositionDal.RemoveNewsFromAutoUpdatePosition((int)NewsPositionType.HomeLastestNewsOnHome, news.Id);
                NewsPositionDal.RemoveNewsFromAutoUpdatePosition((int)NewsPositionType.LastestNewsOnHomeByZone, news.Id);
            }

            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HomeNewsFocus + "," +
                                           (int)NewsPositionType.NewsFocusByZone + "," +
                                           (int)NewsPositionType.LockedTimelineOnHome;

                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus || 
                    newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
