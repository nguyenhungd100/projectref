﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.SucKhoeDoiSong.NewsPosition;

namespace ChannelVN.CMS.BO.External.SucKhoeDoiSong.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        public static int TuVanSucKhoeZoneId
        {
            get
            {
                return 63;
            }
        }
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsForcusByZone,
                            zoneId),

                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0),
                    TopLastestNewsForHome = new List<NewsPositionEntity>()
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        //public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        //{
        //    var errorCode = ErrorMapping.ErrorCodes.BusinessError;

        //    if (newsPositions == null || newsPositions.Count <= 0) return errorCode;

        //    try
        //    {
        //        // Truong hop 1 giao dien chi update 1 typeid => lay typeid va zoneid cua ban ghi dau tien
        //        var typeId = newsPositions[0].TypeId;
        //        var zoneId = newsPositions[0].ZoneId;
        //        // Lay danh sach position hien tai tuong ung voi type
        //        var currentNewsPositions = NewsPositionDal.GetListByTypeAndZoneId(typeId, zoneId);

        //        // UPDATE VAO VI TRI CAN CAI NOI BAT
        //        errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);

        //        // UPDATE VI TRI THANH CONG
        //        if (errorCode == ErrorMapping.ErrorCodes.Success)
        //        {
        //            #region Lay ra danh sach cac tin update moi vao vi tri noi bat (keo bang tay tu ben ngoai vao)
        //            var newsIdForAutoUpdate = "";
        //            foreach (var newsPosition in newsPositions)
        //            {
        //                if (
        //                    !currentNewsPositions.Exists(
        //                        item => item.TypeId == newsPosition.TypeId && item.NewsId == newsPosition.NewsId))
        //                {
        //                    newsIdForAutoUpdate += ";" + newsPosition.NewsId;
        //                }
        //            }
        //            #endregion

        //            var updateForUnlockPosition = new List<UnlockPosition>();

        //            if (!string.IsNullOrEmpty(newsIdForAutoUpdate))
        //            {
        //                var excludePositionTypeIds = "";

        //                if (typeId == (int)NewsPositionType.HomeNewsFocus)
        //                {
        //                    #region Noi bat trang chu

        //                    #region Kiem tra vung noi bat muc
        //                    var listNewsInNewsFocusByZonePosition =
        //                            NewsPositionDal.GetListNewsByListNewsId((int)NewsPositionType.NewsForcusByZone, 0,
        //                                                                    newsIdForAutoUpdate);
        //                    // Bai nam trong vung bai noi bat muc => update lai vung nay
        //                    if (listNewsInNewsFocusByZonePosition.Count > 0)
        //                    {
        //                        foreach (
        //                            var position in
        //                                listNewsInNewsFocusByZonePosition)
        //                        {
        //                            if (!updateForUnlockPosition.Exists(item => item.ZoneId == position.ZoneId && item.NewsId == position.NewsId))
        //                            {
        //                                excludePositionTypeIds = ((int)NewsPositionType.HomeNewsFocus).ToString();
        //                                UpdateAutoWhenChangeManualPosition(position.ZoneId,
        //                                                                   (int)NewsPositionType.NewsForcusByZone,
        //                                                                   newsIdForAutoUpdate,
        //                                                                   excludePositionTypeIds,
        //                                                                   false, -1, -1, -1, -1, -1);

        //                                updateForUnlockPosition.Add(new UnlockPosition
        //                                {
        //                                    PositionType = (int)NewsPositionType.TopLastestNewsOnListByZone,
        //                                    ZoneId = position.ZoneId,
        //                                    NewsId = position.NewsId
        //                                });
        //                            }
        //                        }
        //                    }
        //                    #endregion

        //                    #region Kiem tra bai tin nong
        //                    var listNewsInLastestNewsForHomePosition =
        //                                 NewsPositionDal.GetListNewsByListNewsId(
        //                                     (int)NewsPositionType.TopLastestNewsForHome, 0, newsIdForAutoUpdate);
        //                    // Bai dang nam trong vung bai tin moi trang chu => update lai vung nay
        //                    if (listNewsInLastestNewsForHomePosition.Count > 0)
        //                    {
        //                        excludePositionTypeIds = ((int)NewsPositionType.HomeNewsFocus).ToString();
        //                        excludePositionTypeIds += ";" + ((int)NewsPositionType.NewsForcusByZone).ToString();
        //                        UpdateAutoWhenChangeManualPosition(0,
        //                                                           (int) NewsPositionType.TopLastestNewsForHome,
        //                                                           newsIdForAutoUpdate,
        //                                                           excludePositionTypeIds,
        //                                                           false, 1, 1,
        //                                                           -1, -1, -1);
        //                    }
        //                    #endregion

        //                    #endregion
        //                }
        //                else if (typeId == (int)NewsPositionType.NewsForcusByZone)
        //                {
        //                    #region Noi bat muc

        //                    #region Kiem tra bai tin nong
        //                    var listNewsInLastestNewsForHomePosition =
        //                                 NewsPositionDal.GetListNewsByListNewsId(
        //                                     (int)NewsPositionType.TopLastestNewsForHome, 0, newsIdForAutoUpdate);
        //                    // Bai dang nam trong vung bai tin moi trang chu => update lai vung nay
        //                    if (listNewsInLastestNewsForHomePosition.Count > 0)
        //                    {
        //                        excludePositionTypeIds = ((int)NewsPositionType.HomeNewsFocus).ToString();
        //                        excludePositionTypeIds += ";" + ((int)NewsPositionType.NewsForcusByZone).ToString();
        //                        UpdateAutoWhenChangeManualPosition(0,
        //                                                           (int)NewsPositionType.TopLastestNewsForHome,
        //                                                           newsIdForAutoUpdate,
        //                                                           excludePositionTypeIds,
        //                                                           false, 1, 1,
        //                                                           -1, -1, -1);
        //                    }
        //                    #endregion

        //                    #region Check cac vung auto
        //                    foreach (var newsPosition in newsPositions)
        //                    {
        //                        if (!updateForUnlockPosition.Exists(
        //                                item =>
        //                                item.PositionType == (int)NewsPositionType.TopLastestNewsOnListByZone &&
        //                                item.ZoneId == newsPosition.ZoneId && item.NewsId == newsPosition.NewsId))
        //                        {
        //                            updateForUnlockPosition.Add(new UnlockPosition
        //                            {
        //                                PositionType =
        //                                    (int)NewsPositionType.TopLastestNewsOnListByZone,
        //                                ZoneId = newsPosition.ZoneId,
        //                                NewsId = newsPosition.NewsId
        //                            });
        //                        }
        //                    }
        //                    #endregion

        //                    #endregion
        //                }
        //            }

        //            foreach (var position in updateForUnlockPosition)
        //            {
        //                //UpdateAutoWhenChangeManualPosition(position.ZoneId,
        //                //                                   position.PositionType,
        //                //                                   position.NewsId,
        //                //                                   ((int)NewsPositionType.NewsForcusByZone).ToString(),
        //                //                                   false, -1, -1, -1, -1, -1);
        //                BoFactory.GetInstance<NewsPositionBo>()
        //                         .UpdateAutoWhenEditNewsV2(true, position.ZoneId, position.PositionType,
        //                                                   Utility.ConvertToLong(position.NewsId),
        //                                                   ((int)NewsPositionType.NewsForcusByZone).ToString(),
        //                                                   false, -1, -1, -1, -1, -1);
        //                //NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //    }
        //    return errorCode;















        //    //var typeId = newsPositions != null && newsPositions.Count > 0 ? newsPositions[0].TypeId : -1;
        //    //var currentNewsPositions = NewsPositionDal.GetListByTypeAndZoneId(typeId, 0);
        //    //var errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
        //    //if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    //{
        //    //    var updateForUnlockPosition = new List<UnlockPosition>();

        //    //    if (typeId == (int)NewsPositionType.HomeNewsFocus || typeId == (int)NewsPositionType.TopLastestNewsForHome)
        //    //    {
        //    //        #region HomeNewsFocus

        //    //        var newsForAutoUpdate = "";
        //    //        foreach (var newsPosition in newsPositions)
        //    //        {
        //    //            if (!currentNewsPositions.Exists(item => item.TypeId == newsPosition.TypeId && item.NewsId == newsPosition.NewsId))
        //    //            {
        //    //                newsForAutoUpdate += ";" + newsPosition.NewsId;
        //    //            }
        //    //        }
        //    //        // Co news chua ton tai trong Position cu => keo them bai vao => update danh sach
        //    //        if (!string.IsNullOrEmpty(newsForAutoUpdate))
        //    //        {
        //    //            try
        //    //            {
        //    //                // Bài mới trang chủ
        //    //                if (typeId == (int)NewsPositionType.HomeNewsFocus)
        //    //                {
        //    //                    if (!updateForUnlockPosition.Exists(
        //    //                            item =>
        //    //                            item.PositionType == (int)NewsPositionType.TopLastestNewsForHome && item.ZoneId == 0))
        //    //                    {
        //    //                        updateForUnlockPosition.Add(new UnlockPosition
        //    //                        {
        //    //                            PositionType =
        //    //                                (int)NewsPositionType.TopLastestNewsForHome,
        //    //                            ZoneId = 0
        //    //                        });
        //    //                    }
        //    //                }

        //    //                newsForAutoUpdate = newsForAutoUpdate.Remove(0, 1);
        //    //                var newsInZones = NewsInZoneDal.GetNewsInZoneByListNewsId(newsForAutoUpdate);
        //    //                foreach (var newsInZone in newsInZones)
        //    //                {
        //    //                    // Bài mới mục Tư vấn sức khỏe
        //    //                    if (newsInZone.ZoneId == NewsPositionBo.TuVanSucKhoeZoneId && !updateForUnlockPosition.Exists(
        //    //                        item =>
        //    //                        item.PositionType == (int)NewsPositionType.TopLastestNewsOnListForTuVanSucKhoe && item.ZoneId == newsInZone.ZoneId))
        //    //                    {
        //    //                        updateForUnlockPosition.Add(new UnlockPosition
        //    //                        {
        //    //                            PositionType =
        //    //                                (int)NewsPositionType.TopLastestNewsOnListForTuVanSucKhoe,
        //    //                            ZoneId = newsInZone.ZoneId
        //    //                        });
        //    //                    }
        //    //                    // Nổi bật mục
        //    //                    //if (!updateForUnlockPosition.Exists(
        //    //                    //    item =>
        //    //                    //    item.PositionType == (int)NewsPositionType.NewsForcusByZone && item.ZoneId == newsInZone.ZoneId))
        //    //                    //{
        //    //                    //    updateForUnlockPosition.Add(new UnlockPosition
        //    //                    //    {
        //    //                    //        PositionType =
        //    //                    //            (int)NewsPositionType.NewsForcusByZone,
        //    //                    //        ZoneId = newsInZone.ZoneId
        //    //                    //    });
        //    //                    //}
        //    //                    // Bài mới mục trang chủ
        //    //                    if (!updateForUnlockPosition.Exists(
        //    //                        item =>
        //    //                        item.PositionType == (int)NewsPositionType.TopLastestNewsOnHomeByZone && item.ZoneId == newsInZone.ZoneId))
        //    //                    {
        //    //                        updateForUnlockPosition.Add(new UnlockPosition
        //    //                        {
        //    //                            PositionType =
        //    //                                (int)NewsPositionType.TopLastestNewsOnHomeByZone,
        //    //                            ZoneId = newsInZone.ZoneId
        //    //                        });
        //    //                    }
        //    //                    // Bài mới mục
        //    //                    if (!updateForUnlockPosition.Exists(
        //    //                        item =>
        //    //                        item.PositionType == (int)NewsPositionType.TopLastestNewsOnListByZone && item.ZoneId == newsInZone.ZoneId))
        //    //                    {
        //    //                        updateForUnlockPosition.Add(new UnlockPosition
        //    //                        {
        //    //                            PositionType =
        //    //                                (int)NewsPositionType.TopLastestNewsOnListByZone,
        //    //                            ZoneId = newsInZone.ZoneId
        //    //                        });
        //    //                    }
        //    //                }
        //    //                Logger.WriteLog(Logger.LogType.Info, "updateForUnlockPosition => " + NewtonJson.Serialize(updateForUnlockPosition));
        //    //            }
        //    //            catch (Exception ex)
        //    //            {
        //    //                Logger.WriteLog(Logger.LogType.Info, "Error => " + ex.ToString());
        //    //            }

        //    //        }
        //    //        #endregion
        //    //    }
        //    //    foreach (var position in updateForUnlockPosition)
        //    //    {
        //    //        NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //    //    }
        //    //}
        //    //return errorCode;
        //}



        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.TopLastestNewsOnListByZone, allZoneIdIncludePrimary, news.Id,
                                                             0);

            if (news.DisplayInSlide == 1)
            {
                NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.TopLastestNewsForHome, "0", news.Id,
                                                                 primaryZoneId);
            }
            else
            {
                NewsPositionDal.RemoveNewsFromAutoUpdatePosition((int)NewsPositionType.TopLastestNewsForHome, news.Id);
            }
            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HomeNewsFocus + "," +
                                           (int)NewsPositionType.NewsForcusByZone;

                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsForcusByZone)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.NewsForcusByZone)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
