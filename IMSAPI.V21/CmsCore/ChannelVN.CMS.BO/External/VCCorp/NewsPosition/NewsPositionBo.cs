﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.VCCorp.NewsPosition;

namespace ChannelVN.CMS.BO.External.VCCorp.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsFocusByZone,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0),
                    TopLastestNewsForHome =
                        GetListByTypeAndZoneIdWithOrderByDistritbutionDate(
                            (int)NewsPositionType.HotDailyEvent, 0, 15)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url)
        {
            //lấy danh sách bài cũ ra, chèn vào đề xuất nổi bật trang chủ (phải lấy ra trước khi cài)
            var oldPos = BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
            var data = ChannelVN.CMS.DAL.External.VCCorp.NewsPositionDal.SaveLinkPosition((int)type, position, zoneId, title, avatar, url);
            if (data == true)
            {

                foreach (var pos in oldPos.HighlightHomeFocus)
                {
                    if (pos.Order == position)
                    {
                        NewsBo.UpdateDisplayPosition(pos.NewsId, 1, WcfExtensions.WcfMessageHeader.Current.ClientUsername);
                        break;
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            if (!string.IsNullOrEmpty(allZoneIdIncludePrimary) && ("," + allZoneIdIncludePrimary + ",").IndexOf("," + primaryZoneId + ",", StringComparison.Ordinal) < 0)
            {
                allZoneIdIncludePrimary += "," + primaryZoneId;
            }
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.LastestNewsByZone, allZoneIdIncludePrimary, news.Id,
                                                             0);
            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HomeNewsFocus + "," +
                                           (int)NewsPositionType.HotDailyEvent + "," +
                                           (int)NewsPositionType.NewsFocusByZone;

                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.HotDailyEvent ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.HotDailyEvent ||
                    newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }


    }
}
