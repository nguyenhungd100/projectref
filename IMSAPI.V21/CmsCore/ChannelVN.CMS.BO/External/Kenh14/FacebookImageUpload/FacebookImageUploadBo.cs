﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.Kenh14;
using ChannelVN.CMS.Entity.External.Kenh14.FacebookImageUpload;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Kenh14.NewsPosition;

namespace ChannelVN.CMS.BO.External.Kenh14.FacebookImageUpload
{
    public class FacebookImageUploadBo
    {
        public static List<FacebookImageUploadEntity> GetByNewsId(long newsId, int pageIndex, int pageSize, int status, ref int totalRow)
        {
            List<FacebookImageUploadEntity> returnValue = FacebookImageUploadDal.GetByNewsId(newsId, pageIndex, pageSize, status, ref totalRow);
            return returnValue;
        }

        public static ErrorMapping.ErrorCodes UpdateStatus(int id, int status)
        {
            return FacebookImageUploadDal.UpdateStatus(id, status)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
