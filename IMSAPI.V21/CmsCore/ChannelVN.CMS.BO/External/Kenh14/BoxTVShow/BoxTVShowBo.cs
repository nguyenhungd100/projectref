﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.Kenh14;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow;

namespace ChannelVN.CMS.BO.External.Kenh14.BoxTVShow
{
    public class BoxTVShowBo
    {
        public static ErrorMapping.ErrorCodes UpdateBoxTVShow(BoxTVShowEntity hotNewsConfigEntity)
        {
            BoxTVShowDal.Update(hotNewsConfigEntity);
            return ErrorMapping.ErrorCodes.Success;
        }

        public static ErrorMapping.ErrorCodes DeleteBoxTVShow(int zoneId)
        {
            BoxTVShowDal.Delete(zoneId);
            return ErrorMapping.ErrorCodes.Success;
        }

        public static BoxTVShowEntity GetByIdBoxTVShow(int id)
        {
            try
            {
                return BoxTVShowDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new BoxTVShowEntity();
            }
        }
        public static List<BoxTVShowEntity> GetListBoxTVShow()
        {
            try
            {
                return BoxTVShowDal.GetList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<BoxTVShowEntity>();
            }
        }
    }
}
