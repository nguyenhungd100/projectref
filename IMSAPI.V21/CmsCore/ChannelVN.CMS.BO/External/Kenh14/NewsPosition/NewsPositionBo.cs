﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.Kenh14;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Kenh14.NewsPosition;

namespace ChannelVN.CMS.BO.External.Kenh14.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="listOfFocusPositionOnLastestNews"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    //HomeNewsFocusByZone =
                    //    GetListNewsPositionByTypeAndZoneId(
                    //        (int)NewsPositionType.HomeNewsFocusByZone,
                    //        zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage(string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url, string sapo)
        {
            //lấy danh sách bài cũ ra, chèn vào đề xuất nổi bật trang chủ (phải lấy ra trước khi cài)
            var oldPos = BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage("");
            var data = NewsPositionDal.SaveLinkPosition((int)type, position, zoneId, title, avatar, url, sapo);
            if (data == true)
            {

                foreach (var pos in oldPos.HighlightHomeFocus)
                {
                    if (pos.Order == position)
                    {
                        NewsBo.UpdateDisplayPosition(pos.NewsId, 1, WcfExtensions.WcfMessageHeader.Current.ClientUsername);
                        break;
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        #region override methods
        public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode = ErrorMapping.ErrorCodes.BusinessError;

            if (newsPositions == null || newsPositions.Count <= 0) return errorCode;

            try
            {
                // Truong hop 1 giao dien chi update 1 typeid => lay typeid va zoneid cua ban ghi dau tien
                //var typeId = newsPositions[0].TypeId;
                //var zoneId = newsPositions[0].ZoneId;
                //// Lay danh sach position hien tai tuong ung voi type
                //var currentNewsPositions = NewsPositionDal.GetListByTypeAndZoneId(typeId, zoneId);

                // UPDATE VAO VI TRI CAN CAI NOI BAT
                errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);

                // UPDATE VI TRI THANH CONG
                //if (errorCode == ErrorMapping.ErrorCodes.Success)
                //{
                //    #region Lay ra danh sach cac tin update moi vao vi tri noi bat (keo bang tay tu ben ngoai vao)
                //    var newsIdForAutoUpdate = "";
                //    foreach (var newsPosition in newsPositions)
                //    {
                //        if (
                //            !currentNewsPositions.Exists(
                //                item => item.TypeId == newsPosition.TypeId && item.NewsId == newsPosition.NewsId))
                //        {
                //            newsIdForAutoUpdate += ";" + newsPosition.NewsId;
                //        }
                //    }
                //    #endregion

                //    var updateForUnlockPosition = new List<UnlockPosition>();

                //    if (!string.IsNullOrEmpty(newsIdForAutoUpdate))
                //    {
                //        if (typeId == (int)NewsPositionType.NewsFocusByZone)
                //        {
                //            #region Noi bat muc

                //            #region Check cac vung auto
                //            foreach (var newsPosition in newsPositions)
                //            {
                //                var index =
                //                    updateForUnlockPosition.FindIndex(
                //                        item =>
                //                        item.PositionType == (int)NewsPositionType.TopLastestNewsByZone &&
                //                        item.ZoneId == newsPosition.ZoneId);
                //                if (index >= 0)
                //                {
                //                    var unlockPosition = updateForUnlockPosition[index];
                //                    unlockPosition.ListNewsId += ";" + newsPosition.NewsId;
                //                    updateForUnlockPosition[index] = unlockPosition;
                //                }
                //                else
                //                {
                //                    updateForUnlockPosition.Add(new UnlockPosition
                //                    {
                //                        PositionType =
                //                            (int)NewsPositionType.TopLastestNewsByZone,
                //                        ZoneId = newsPosition.ZoneId,
                //                        ListNewsId = newsPosition.NewsId.ToString()
                //                    });
                //                }
                //            }
                //            #endregion

                //            #endregion
                //        }
                //    }

                //    foreach (var position in updateForUnlockPosition)
                //    {
                //        UpdateAutoWhenChangeManualPosition(position.ZoneId,
                //                                           position.PositionType,
                //                                           position.ListNewsId,
                //                                           ((int)NewsPositionType.NewsFocusByZone).ToString(),
                //                                           false, -1, -1, -1, -1, -1);
                //    }
                //}
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return errorCode;


            //var errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
            //if (errorCode == ErrorMapping.ErrorCodes.Success)
            //{
            //    var updateForUnlockPosition = new List<UnlockPosition>();
            //    var count = newsPositions.Count;
            //    for (var i = 0; i < count; i++)
            //    {
            //        var newsPosition = newsPositions[i];
            //        NewsPositionType typeId;
            //        if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
            //        {
            //            typeId = NewsPositionType.HomeNewsFocus;
            //        }
            //        if (typeId == NewsPositionType.HomeNewsFocus)
            //        {
            //            if (updateForUnlockPosition.Exists(
            //                item =>
            //                item.PositionType == (int)NewsPositionType.HomeLastestNews && item.ZoneId == 0))
            //            {
            //                updateForUnlockPosition.Add(new UnlockPosition
            //                {
            //                    PositionType =
            //                        (int)NewsPositionType.HomeLastestNews,
            //                    ZoneId = 0
            //                });
            //            }
            //        }
            //        else if (typeId == NewsPositionType.HomeNewsFocusByZone)
            //        {
            //            if (updateForUnlockPosition.Exists(
            //                item =>
            //                item.PositionType == (int)NewsPositionType.HomeLastestNewsByZone && item.ZoneId == newsPosition.ZoneId))
            //            {
            //                updateForUnlockPosition.Add(new UnlockPosition
            //                {
            //                    PositionType =
            //                        (int)NewsPositionType.HomeLastestNewsByZone,
            //                    ZoneId = newsPosition.ZoneId
            //                });
            //            }
            //        }
            //        else if (typeId == NewsPositionType.NewsFocusByZone)
            //        {
            //            if (updateForUnlockPosition.Exists(
            //                item =>
            //                item.PositionType == (int)NewsPositionType.TopLastestNewsByZone && item.ZoneId == newsPosition.ZoneId))
            //            {
            //                updateForUnlockPosition.Add(new UnlockPosition
            //                {
            //                    PositionType =
            //                        (int)NewsPositionType.TopLastestNewsByZone,
            //                    ZoneId = newsPosition.ZoneId
            //                });
            //            }
            //        }
            //    }
            //    foreach (var position in updateForUnlockPosition)
            //    {
            //        NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
            //    }
            //}
            //return errorCode;
        }
        #endregion

        //Cài bài từ danh sách k14

        public static ErrorMapping.ErrorCodes SetNewsPostitionInList(int currentType, int newType,
                                                               int position, long newsId, string accountName)
        {
            var data = ErrorMapping.ErrorCodes.Success;
            var zoneId = NewsBo.GetNewsInZoneByNewsId(newsId).FirstOrDefault(it => it.IsPrimary == true).ZoneId;
            if (currentType == (int)NewsDisplayPosition.ListPage)
            {
                if (newType == (int)NewsDisplayPosition.HomePage)
                {
                    // Từ nổi bật mục sang nổi bật trang chủ. ==>Tham số bắt buộc: currentType, newType, newsId, position
                    //1. Insert vào bảng NewsPosition
                    //NewsPositionEntity newsPosition = new NewsPositionEntity
                    //                                      {
                    //                                          Type = newType,
                    //                                          ZoneId = zoneId,
                    //                                          Order = position,
                    //                                          NewsId = newsId,
                    //                                          ExpiredLock = DateTime.Now.AddDays(1)
                    //                                      };
                    //List<NewsPositionEntity> newsPositions = new List<NewsPositionEntity>();
                    //newsPositions.Add(newsPosition);

                    //data = BoFactory.GetInstance<NewsPositionBo>()
                    //         .SaveNewsPosition(new List<NewsPositionEntity>(newsPositions), -1, false, accountName,
                    //                           true);
                    //2. Update DisplayPosition = 1
                    if (data == ErrorMapping.ErrorCodes.Success)
                    {
                        data = NewsBo.UpdateDisplayPosition(newsId, 1, accountName);
                    }
                }
                if (newType == 0)
                {
                    //Từ nổi bật mục sang tin thường ==> Tham số bắt buộc: currentType, newType, newsId
                    // Update DisplayPosition = 0
                    data = NewsBo.UpdateDisplayPosition(newsId, 0, accountName);
                }
            }
            else if (currentType == (int)NewsDisplayPosition.HomePage)
            {
                //Nổi bật trang chủ sang nổi bật mục hoặc sang tin thường ==>Tham số bắt buộc: currentType, newType, newsId
                //1. Lấy bài đề xuất nổi bật trang chủ mới nhất ra, cho vào vị trí tin cũ.
                //var topLastestPositionOnZone = NewsDal.GetTopLastestNewsForAutoUpdateNewsPosition(1, 1, 0, "1", "",
                //                                                                                  true, -1, -1, -1,
                //                                                                                  1, -1).FirstOrDefault();
                //var currentPositionData =
                //    BoFactory.GetInstance<NewsPositionBo>()
                //             .GetListNewsPositionForHomePage("")
                //             .HighlightHomeFocus.FirstOrDefault(it => it.NewsId == newsId);
                //if (currentPositionData != null)
                //{
                //    var order = currentPositionData.Order;
                //    NewsPositionEntity newsPosition = new NewsPositionEntity
                //                                          {
                //                                              TypeId = currentType,
                //                                              ZoneId = 0,
                //                                              Order = order,
                //                                              NewsId = topLastestPositionOnZone.NewsId,
                //                                              // Bài mới nhất trong danh sách đề xuất nổi bật trang chủ
                //                                              ExpiredLock = DateTime.Now.AddDays(1)
                //                                          };
                //    List<NewsPositionEntity> newsPositions = new List<NewsPositionEntity>();
                //    newsPositions.Add(newsPosition);

                //    data = BoFactory.GetInstance<NewsPositionBo>()
                //                    .SaveNewsPosition(new List<NewsPositionEntity>(newsPositions), -1, false,
                //                                      accountName,
                //                                      true);
                //2. Update DisplayPosition
                if (newType == (int)NewsDisplayPosition.ListPage)
                {
                    //Update DisplayPosition = 2 Nếu là NB trang chủ sang NB mục
                    data = NewsBo.UpdateDisplayPosition(newsId, 2, accountName);
                }
                else if (newType == 0)
                {
                    //Update DisplayPosition = 0 Nếu là NB trang chủ sang tin thông thường
                    data = NewsBo.UpdateDisplayPosition(newsId, 0, accountName);
                }
                //}
                else
                {
                    data = ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            else if (currentType == (int)NewsDisplayPosition.Normal)
            {
                if (newType == (int)NewsDisplayPosition.HomePage)
                {
                    //Tin thường lên nổi bật trang chủ ==> Tham số bắt buộc: currentType, newType, newsId, position
                    //NewsPositionEntity newsPosition = new NewsPositionEntity
                    //                                      {
                    //                                          TypeId = newType,
                    //                                          ZoneId = zoneId,
                    //                                          Order = position,
                    //                                          NewsId = newsId,
                    //                                          ExpiredLock = DateTime.Now.AddDays(1)
                    //                                      };
                    //List<NewsPositionEntity> newsPositions = new List<NewsPositionEntity>();
                    //newsPositions.Add(newsPosition);

                    //data = BoFactory.GetInstance<NewsPositionBo>()
                    //         .SaveNewsPosition(new List<NewsPositionEntity>(newsPositions), -1, false, accountName,
                    //                           true);
                    //2.Update DisplayPosition = 1
                    data = NewsBo.UpdateDisplayPosition(newsId, 1, accountName);
                }
                else if (newType == (int)NewsDisplayPosition.ListPage)
                {
                    //Update DisplayPosition = 2
                    data = NewsBo.UpdateDisplayPosition(newsId, 2, accountName);
                }
            }
            return data;
        }
    }
}
