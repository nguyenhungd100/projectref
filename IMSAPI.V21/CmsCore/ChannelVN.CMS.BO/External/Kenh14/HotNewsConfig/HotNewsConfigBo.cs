﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.Kenh14;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;

namespace ChannelVN.CMS.BO.External.Kenh14.HotNewsConfig
{
    public class HotNewsConfigBo
    {
        public static ErrorMapping.ErrorCodes UpdateHotNewsConfig(HotNewsConfigEntity hotNewsConfigEntity)
        {
            HotNewsConfigDal.Update(hotNewsConfigEntity);
            return ErrorMapping.ErrorCodes.Success;
        }

        public static ErrorMapping.ErrorCodes DeleteHotNewsConfig(int zoneId)
        {
            HotNewsConfigDal.Delete(zoneId);
            return ErrorMapping.ErrorCodes.Success;
        }

        public static List<HotNewsConfigEntity> GetListHotNewsConfig()
        {
            try
            {
                return HotNewsConfigDal.GetList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<HotNewsConfigEntity>();
            }
        }
    }
}
