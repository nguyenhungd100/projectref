﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.TimDiemThi.NewsPosition;

namespace ChannelVN.CMS.BO.External.TimDiemThi.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsFocusByZone,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0),
                    TopLastestNewsForHome = new List<NewsPositionEntity>()
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode = ErrorMapping.ErrorCodes.BusinessError;
            
            if (newsPositions == null || newsPositions.Count <= 0) return errorCode;

            try
            {
                // UPDATE VAO VI TRI CAN CAI NOI BAT
                errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return errorCode;
        }

        public virtual bool RemoveNewsDisplayInSlide(string newsIdList)
        {
            return ChannelVN.CMS.DAL.External.NguoiLaoDong.NewsPositionDal.RemoveNewsDisplayInSlide(newsIdList);
        }

        public virtual bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate)
        {
            var isValid = false;
            var minDate = DateTime.MaxValue;
            if (newsId > 0)
            {
                var newsList = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.TopHotNewsForHome, 0, "");
                var newsDistribution = BoFactory.GetInstance<NewsBo>().GetNewsByNewsId(newsId).DistributionDate;
                foreach (var newsPositionEntity in newsList)
                {
                    if (minDate > newsPositionEntity.DistributionDate)
                        minDate = newsPositionEntity.DistributionDate;
                    if (newsPositionEntity.DistributionDate < newsDistribution)
                    {
                        isValid = true;
                    }
                }
            }
            else
            {
                isValid = true;
            }
            minValidDate = minDate;
            return isValid;
        }
    }
}
