﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.External.GiaDinhNet;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BO.External.GiaDinhNet.News
{
    public class NewsBo : Base.News.NewsBo
    {
        #region Magazine News
        public static List<NewsEntity> SearchNewsByMagazineIdWithPaging(int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsByMagazineIdWithPaging(magazineId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.SearchNewsByMagazineIdWithPaging:{0}", ex.Message));
            }
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInMagazine(int zoneId, string keyword, int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsPublishExcludeNewsInMagazine(zoneId, keyword, magazineId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsBo.SearchNewsPublishExcludeNewsInMagazine:{0}", ex.Message));
            }
        }
        #endregion
    }
}
