﻿using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.DAL.External.Interactive;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.Interactive.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BO.External.Interactive.Account
{
    public class UserBo
    {
        /// <summary>
        /// Expired on 5 minutes
        /// </summary>
        private const int SmsExpired = 5;

        public static ErrorMapping.ErrorCodes AddNew(UserEntity user, ref int newUserId, ref string newEncryptUserId)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }
            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }
            var existsUser = UserDal.GetUserByUsername(user.UserName);
            if (null != existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUsernameExists;
            }

            //existsUser = UserDal.GetUserByEmail(user.Email);
            //if (null != existsUser)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;
            //}

            user.Password = Crypton.Encrypt(user.Password);

            //Gen ở web để gửi mail luôn.
            //user.ActiveCode = Crypton.Encrypt(string.Format("{0};{1}", user.UserName, DateTime.Now.Ticks));

            try
            {
                if (UserDal.AddnewUser(user, ref newUserId))
                {
                    newEncryptUserId = CryptonForId.EncryptId(newUserId);
                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidMobile;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            try
            {
                user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

                var existsUser = UserDal.GetUserById(user.Id);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }
                // Nếu không update password thì lấy password cũ
                if (string.IsNullOrEmpty(user.Password))
                {
                    user.Password = existsUser.Password;
                }

                var accountLogin = GetUserByUsername(accountName);
                if (accountLogin == null)
                    return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

                var existsAccount = GetById(user.Id);
                if (existsAccount.IsSystem && !accountLogin.IsSystem)
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                //existsUser = UserDal.GetUserByEmail(user.Email);
                //if (null != existsUser && existsUser.Id != user.Id)
                //{
                //    return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;
                //}
                var returnData = UserDal.UpdateUserById(user) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(user.Id);
                    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(user.Id);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateProfile(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidMobile;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

            var existsUser = UserDal.GetUserById(user.Id);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            // Nếu không update password thì lấy password cũ
            if (string.IsNullOrEmpty(user.Password))
            {
                user.Password = existsUser.Password;
            }

            var accountLogin = GetUserByUsername(accountName);
            if (accountLogin == null)
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

            var existsAccount = GetById(user.Id);
            if (existsAccount.IsSystem && !accountLogin.IsSystem)
                return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

            //var checkUserEmail = UserDal.GetUserByEmail(user.Email);
            //if (null != checkUserEmail && checkUserEmail.Id != user.Id)
            //{
            //    if (existsAccount.Email!=user.Email)
            //    {
            //        return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;   
            //    }                
            //}

            existsUser.Address = user.Address;
            existsUser.Avatar = user.Avatar;
            if (user.Birthday > DbCommon.MinDateTime) existsUser.Birthday = user.Birthday;
            existsUser.FullName = user.FullName;
            existsUser.Mobile = user.Mobile;
            existsUser.ModifiedDate = DateTime.Now;
            existsUser.Password = user.Password;
            //existsUser.Status = existsUser.Status;
            existsUser.Status = user.Status;
            existsUser.Description = user.Description;
            existsAccount.Email = user.Email;
            existsUser.SiteId = user.SiteId;
            existsAccount.ActiveCode = user.ActiveCode;
            try
            {
                return UserDal.UpdateUserById(existsUser) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.DeleteUserById(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes UpdateAvatar(string userName, string avatar)
        {


            return UserDal.UpdateUserAvatar(userName, avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes ChangeStatus(int userId, UserStatus status)
        {
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserStatusByById(userId, (int)status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes ChangePassword(int userId, string oldPassword, string newPassword, string accountNameLogin)
        {
            try
            {
                var existsUser = UserDal.GetUserById(userId);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }

                oldPassword = Crypton.Encrypt(oldPassword);
                if (existsUser.Password != oldPassword)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
                }
                var accountLogin = GetUserByUsername(accountNameLogin);
                // Nếu account đăng nhập ko phải account hệ thống
                // và Account được reset pass là account hệ thống
                // Thì ko được sửa pass
                if (accountLogin == null || (!accountLogin.IsSystem && existsUser.IsSystem))
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                return UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword))
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ResetPassword(int userId, string newPassword)
        {
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            return UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword)) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes ResetPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            if (string.IsNullOrEmpty(oldPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
            if (string.IsNullOrEmpty(cfNewPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidRetypePassword;

            var existsUser = UserDal.GetUserByUsername(accountName);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            newPassword = Crypton.Encrypt(newPassword);
            oldPassword = Crypton.Encrypt(oldPassword);

            if (existsUser.Password != oldPassword)
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;

            return UserDal.UpdateUserPasswordByById(existsUser.Id, newPassword) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static UserEntity GetById(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }
            var user = UserDal.GetUserById(userId);
            if (null != user)
            {
                user.EncryptId = CryptonForId.EncryptId(user.Id);
                user.Id = 0;
            }
            return user;
        }

        public static UserEntity GetById(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }
            var user = UserDal.GetUserById(userId);
            if (null != user)
            {
                user.EncryptId = CryptonForId.EncryptId(user.Id);
                user.Id = 0;
            }
            return user;
        }

        public static UserEntity GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }
                var user = UserDal.GetUserByUsername(username);
                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserEntity GetUserByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    return null;
                }
                var user = UserDal.GetUserByEmail(email);
                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static UserEntity GetUserByActiveCode(string activeCode)
        {
            try
            {
                if (string.IsNullOrEmpty(activeCode))
                {
                    return null;
                }
                var decryptCode = Crypton.Decrypt(activeCode);
                var userName = decryptCode.Split(new char[] { ';' })[0];
                var user = UserDal.GetUserByUserNameAndActiveCode(userName, activeCode);
                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(int userId)
        {
            try
            {
                var user = UserDal.GetUserById(userId);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                    {
                        User = user,
                        UserPermissions = ChannelVN.CMS.DAL.Base.Account.UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                    };
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserName(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }

                var user = UserDal.GetUserByUsername(username);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                    {
                        User = user,
                        UserPermissions = ChannelVN.CMS.DAL.Base.Account.UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                    };
                    if (null != userWithPermission.User)
                    {
                        userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                        userWithPermission.User.Id = 0;
                    }
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<UserStandardEntity> SearchUser(string keyword,
                                                            UserStatus status,
                                                            UserSortExpression sortOrder,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users = UserDal.SearchUser(keyword, (int)status, (int)sortOrder, pageIndex, pageSize, ref totalRow);

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        SiteId = users[i].SiteId
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }

        /// <summary>
        /// Lấy danh sách user với quyền và zoneId theo userName
        /// check permission và zoneId của userName để lấy ra danh sách user tương ứng.
        /// Logic xử lý trong store
        /// </summary>
        /// <param name="permissionId"></param>
        /// <param name="zoneId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(ChannelVN.CMS.Entity.Base.Security.EnumPermission permissionId, long newsId, string userName)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users = UserDal.GetUserWithFullPermissionAndZoneId((int)permissionId, newsId, userName);

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionIdAndZoneList(ChannelVN.CMS.Entity.Base.Security.EnumPermission permissionId, string zoneIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users = UserDal.GetUserByPermissionIdAndZoneList((int)permissionId, zoneIds);

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionListAndZoneList(string zoneIds, params ChannelVN.CMS.Entity.Base.Security.EnumPermission[] permissionIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var listPermissionId = permissionIds.Aggregate("",
                                                           (current, permissionId) =>
                                                           current + (";" + (int)permissionId));

            if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

            var users = UserDal.GetUserByPermissionListAndZoneList(listPermissionId, zoneIds);

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(ChannelVN.CMS.Entity.Base.Security.EnumPermission permissionId, int zoneId)
        {
            return UserDal.GetNormalUserByPermissionIdAndZoneId((int)permissionId, zoneId);
        }

        public static ErrorMapping.ErrorCodes AddnewSmsCodeForUser(int userId)
        {
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (existsUser.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
            }

            var userSms = new ChannelVN.CMS.Entity.Base.Security.UserSmsEntity
            {
                UserId = userId,
                SmsCode = Utility.GenerateRandomString(4),
                ExpiredDate = DateTime.Now.AddMinutes(SmsExpired)
            };

            return ChannelVN.CMS.DAL.Base.Account.UserSmsDal.AddnewSmsCode(userSms) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes RemoveInUsingState(int userId)
        {
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (existsUser.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
            }

            return ChannelVN.CMS.DAL.Base.Account.UserSmsDal.RemoveInUsing(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ChannelVN.CMS.Entity.Base.Security.UserSmsEntity GetUserSmsCode(int userId)
        {
            return ChannelVN.CMS.DAL.Base.Account.UserSmsDal.GetUserSmsByUserId(userId);
        }

        public static string GetOtpSecretKeyByUsername(string username)
        {
            return UserDal.GetOtpSecretKeyByUsername(username);
        }

        public static ErrorMapping.ErrorCodes UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            if (UserDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateActiveCodeForUser(string username, string activeCode)
        {
            if (UserDal.UpdateActiveCodeForUser(username, activeCode))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        #region Business

        public static UserStandardEntity GetUserStandardByUserId(string encryptUserId)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }

            var user = GetById(encryptUserId);

            if (null == user)
            {
                return null;
            }
            return new UserStandardEntity
            {
                Id = 0,
                EncryptId = encryptUserId,
                UserName = user.UserName,
                FullName = user.FullName,
                Avatar = user.Avatar,
                Email = user.Email,
                Mobile = user.Mobile,
                IsFullPermission = user.IsFullPermission,
                IsFullZone = user.IsFullZone,
                Status = user.Status,
                SiteId = user.SiteId
            };
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(string encryptUserId)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);

                if (userId <= 0)
                {
                    return null;
                }

                var userWithPermission = GetUserWithPermissionByUserId(userId);
                if (null != userWithPermission && null != userWithPermission.User)
                {
                    userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                    userWithPermission.User.Id = 0;
                }
                return userWithPermission;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            var userWithPermissionDetail = new UserWithPermissionDetailEntity
            {
                AllGroupPermission = ChannelVN.CMS.BO.Base.Security.PermissionBo.GetAllPermissionGroupDetail(),
                AllParentZone = ZoneBo.GetListParentZoneActivedByParentId(),
                User = GetById(userId),
                UserPermissionList = ChannelVN.CMS.BO.Base.Security.PermissionBo.GetListByUserId(userId, isGetChildZone)
            };
            return userWithPermissionDetail;
        }

        public static List<UserStandardEntity> Search(string keyword,
                                                           UserStatus status,
                                                           UserSortExpression sortOrder,
                                                           int pageIndex,
                                                           int pageSize,
                                                           ref int totalRow)
        {
            var userStandards = SearchUser(keyword, status, sortOrder, pageIndex, pageSize, ref totalRow);

            if (null != userStandards && userStandards.Count > 0)
            {
                int count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds, params ChannelVN.CMS.Entity.Base.Security.EnumPermission[] permissionIds)
        {
            var userStandards = GetUserByPermissionListAndZoneList(zoneIds, permissionIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        public static List<UserStandardEntity> GetUserListByPermissionIdAndZoneList(ChannelVN.CMS.Entity.Base.Security.EnumPermission permissionId, string zoneIds)
        {
            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> GetUserWhoCanManageThisNews(ChannelVN.CMS.Entity.Base.Security.EnumPermission permissionId, long newsId)
        {
            var zones = ZoneBo.GetZoneByNewsId(newsId);
            var zoneIds = zones.Aggregate("", (current, zone) => current + ("," + zone.Id));
            if (!string.IsNullOrEmpty(zoneIds)) zoneIds = zoneIds.Substring(1);

            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        #endregion

        public static ErrorMapping.ErrorCodes ValidAccount(string username, string password, int siteId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }
            if (user.Password != Crypton.Encrypt(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            //if (user.SiteId != siteId)
            //{
            //    return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            //}
            UserBo.AddnewSmsCodeForUser(user.Id);
            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
