﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.BlogChannelvn.NewsPosition;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;

namespace ChannelVN.CMS.BO.External.BlogChannelvn.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="listOfFocusPositionOnLastestNews"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone = new List<NewsPositionEntity>()
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage(string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #region override methods
        public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
            if (errorCode == ErrorMapping.ErrorCodes.Success)
            {
                var updateForUnlockPosition = new List<UnlockPosition>();
                var count = newsPositions.Count;
                for (var i = 0; i < count; i++)
                {
                    var newsPosition = newsPositions[i];
                    NewsPositionType typeId;
                    if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
                    {
                        typeId = NewsPositionType.HomeNewsFocus;
                    }
                    if (typeId == NewsPositionType.HomeNewsFocus)
                    {
                        if (updateForUnlockPosition.Exists(
                            item =>
                            item.PositionType == (int)NewsPositionType.HomeLastestNews && item.ZoneId == 0))
                        {
                            updateForUnlockPosition.Add(new UnlockPosition
                            {
                                PositionType =
                                    (int)NewsPositionType.HomeLastestNews,
                                ZoneId = 0
                            });
                        }
                    }
                }
                foreach (var position in updateForUnlockPosition)
                {
                    NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
                }
            }
            return errorCode;
        }
        #endregion
    }
}
