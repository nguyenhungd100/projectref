﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.TheGuide.NewsPosition;

namespace ChannelVN.CMS.BO.External.TheGuide.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType type, int position, int zoneId, string title, string avatar, string url, string sapo)
        {
            //lấy danh sách bài cũ ra, chèn vào đề xuất nổi bật trang chủ (phải lấy ra trước khi cài)
            var oldPos = BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage();
            var data = ChannelVN.CMS.DAL.External.TheGuide.NewsPositionDal.SaveLinkPosition((int)type, position, zoneId, title, avatar, url, sapo);
            if (data == true)
            {

                foreach (var pos in oldPos.HighlightHomeFocus)
                {
                    if (pos.Order == position)
                    {
                        NewsBo.UpdateDisplayPosition(pos.NewsId, 1, WcfExtensions.WcfMessageHeader.Current.ClientUsername);
                        break;
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0),
                    TopLastestNewsForHome =
                        GetListByTypeAndZoneIdWithOrderByDistritbutionDate(
                            (int)NewsPositionType.HotDailyEvent, 0, 15)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

    }
}
