﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.External.SohaNews;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;

namespace ChannelVN.CMS.BO.External.SohaNews.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="listOfFocusPositionOnLastestNews"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HighlightListFocusByZone,
                            zoneId),
                    FocusPositionOnLastestNews =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.TimelineLockedPositionOnHome,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                //return new NewsPositionForHomePageEntity
                //    {
                //        HighlightHomeFocus =
                //            GetListNewsPositionByTypeAndZoneId(
                //                (int) NewsPositionType.HighlightHomeFocus, 0),
                //        FocusPositionOnLastestNews =
                //            GetListNewsPositionByTypeAndZoneId(
                //                (int) NewsPositionType.FirstPageHome, 0, "1;5")
                //    };

                #region Temporary Comment
                var highlightHomeFocus = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.HighlightHomeFocus, 0);
                var timelineLockedPosition = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.TimelineLockedPositionOnHome, 0);

                var lockedPosition = new List<NewsPositionEntity>();
                var unlockedPosition = new List<NewsPositionEntity>();
                foreach (var position in timelineLockedPosition)
                {
                    if (position.Locked)
                    {
                        lockedPosition.Add(position);
                    }
                    else
                    {
                        unlockedPosition.Add(position);
                    }
                }
                // Neu co vi tri unlock
                if (unlockedPosition.Count > 0)
                {
                    // Lay danh sach timeline va sap xep theo thoi gian (giong voi tieu chi sap xep cua frontend)
                    var timelineUnlockedPosition = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.FirstPageHome, 0);
                    var unlockCount = timelineUnlockedPosition.Count;
                    for (var i = unlockCount - 1; i >= 0; i--)
                    {
                        if (lockedPosition.Exists(item => item.NewsId == timelineUnlockedPosition[i].NewsId))
                        {
                            timelineUnlockedPosition.RemoveAt(i);
                        }
                    }
                    timelineUnlockedPosition.Sort(
                        (item1, item2) =>
                        item2.DistributionDate != item1.DistributionDate
                            ? item2.DistributionDate.CompareTo(item1.DistributionDate)
                            : item1.Id.CompareTo(item2.Id));

                    // Sap xep lai cac vi tri locked de chen vao timeline
                    lockedPosition.Sort((item1, item2) => item1.Order.CompareTo(item2.Order));
                    foreach (var position in lockedPosition)
                    {
                        timelineUnlockedPosition.Insert(position.Order, position);
                    }

                    // Lap qua cac vi tri unlock va update bai nam o vi tri tuong ung trong timeline vao cac vi tri nay
                    var count = timelineLockedPosition.Count;
                    for (var i = 0; i < count; i++)
                    {
                        var timelineLockedPositionIndex = timelineLockedPosition[i].Order - 1;
                        if (!timelineLockedPosition[i].Locked && timelineLockedPositionIndex <= timelineUnlockedPosition.Count)
                        {

                            timelineLockedPosition[i].Title = timelineUnlockedPosition[timelineLockedPositionIndex].Title;
                            timelineLockedPosition[i].SubTitle = timelineUnlockedPosition[timelineLockedPositionIndex].SubTitle;
                            timelineLockedPosition[i].Sapo = timelineUnlockedPosition[timelineLockedPositionIndex].Sapo;
                            timelineLockedPosition[i].Avatar = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar;
                            timelineLockedPosition[i].AvatarDesc = timelineUnlockedPosition[timelineLockedPositionIndex].AvatarDesc;
                            timelineLockedPosition[i].Avatar1 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar1;
                            timelineLockedPosition[i].Avatar2 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar2;
                            timelineLockedPosition[i].Avatar3 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar3;
                            timelineLockedPosition[i].Avatar4 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar4;
                            timelineLockedPosition[i].Avatar5 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar5;
                            timelineLockedPosition[i].IsFocus = timelineUnlockedPosition[timelineLockedPositionIndex].IsFocus;
                            timelineLockedPosition[i].NewsType = timelineUnlockedPosition[timelineLockedPositionIndex].NewsType;
                            timelineLockedPosition[i].Source = timelineUnlockedPosition[timelineLockedPositionIndex].Source;
                            timelineLockedPosition[i].DistributionDate = timelineUnlockedPosition[timelineLockedPositionIndex].DistributionDate;
                            timelineLockedPosition[i].Url = timelineUnlockedPosition[timelineLockedPositionIndex].Url;
                            timelineLockedPosition[i].ViewCount = timelineUnlockedPosition[timelineLockedPositionIndex].ViewCount;
                            timelineLockedPosition[i].AvatarCustom = timelineUnlockedPosition[timelineLockedPositionIndex].AvatarCustom;
                            timelineLockedPosition[i].OriginalId = timelineUnlockedPosition[timelineLockedPositionIndex].OriginalId;
                        }
                    }
                }

                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus = highlightHomeFocus,
                    FocusPositionOnLastestNews = timelineLockedPosition
                };
                #endregion
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #region For zone sport

        ///// <summary>
        ///// Edited: NANIA
        ///// LastModifiedDate: 2013-01-24 14:42
        ///// </summary>
        ///// <param name="newsPositionType"></param>
        ///// <param name="zoneId"></param>
        ///// <returns></returns>
        //public virtual NewPositionForZoneSportWithLastestNews GetListNewsPositionForZoneSportByTypeAndZoneId(
        //    int newsPositionType, int zoneId, string listOfOrder = "")
        //{
        //    var newsPositions = new List<NewsPositionForZoneSportEntity>();
        //    try
        //    {
        //        NewsPositionForZoneSportType typeId;
        //        if (!Enum.TryParse(newsPositionType.ToString(), true, out typeId))
        //        {
        //            typeId = NewsPositionForZoneSportType.NewsFocusOnHome;
        //        }
        //        return new NewPositionForZoneSportWithLastestNews
        //        {
        //            PrimaryNewsPosition =
        //                NewsPositionForZoneSportDal.GetListByTypeAndZoneId((int)typeId, zoneId),
        //            LastestNews =
        //                NewsPositionForZoneSportDal.GetListByTypeAndZoneId(
        //                    (int)
        //                    NewsPositionForZoneSportType.
        //                        LastestNewsByZoneOnList, zoneId, listOfOrder)
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //    return new NewPositionForZoneSportWithLastestNews();
        //}

        ///// <summary>
        ///// Edited: NANIA
        ///// LastModifiedDate: 2013-01-24 14:45
        ///// </summary>
        ///// <param name="newsPositions"></param>
        ///// <param name="avatarIndex"></param>
        ///// <param name="checkNewsExists"></param>
        ///// <returns></returns>
        //public virtual ErrorMapping.ErrorCodes SaveNewsPositionForZoneSport(List<NewsPositionForZoneSportEntity> newsPositions,
        //                                                      int avatarIndex, bool checkNewsExists)
        //{
        //    try
        //    {
        //        var updateForUnlockPosition = new List<UnlockPosition>();
        //        var count = newsPositions.Count;
        //        for (var i = 0; i < count; i++)
        //        {
        //            var newsPosition = newsPositions[i];
        //            NewsPositionForZoneSportType typeId;
        //            if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
        //            {
        //                typeId = NewsPositionForZoneSportType.NewsFocusOnHome;
        //            }
        //            NewsPositionForZoneSportDal.SaveNewsPosition((int)typeId, newsPosition.ZoneId, newsPosition.Order,
        //                                                         newsPosition.NewsId, newsPosition.ExpiredLock,
        //                                                         newsPosition.Avatar, avatarIndex, checkNewsExists);
        //            if (typeId == NewsPositionForZoneSportType.NewsFocusOnHome ||
        //                typeId == NewsPositionForZoneSportType.ZoneHauTruongFocusOnHome)
        //            {
        //                if (!updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionForZoneSportType.LastestNewsOnHome && item.ZoneId == 0))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionForZoneSportType.LastestNewsOnHome,
        //                        ZoneId = 0
        //                    });
        //                }
        //            } 
        //            else if (typeId == NewsPositionForZoneSportType.FocusNewsByZoneOnList)
        //            {
        //                if (!updateForUnlockPosition.Exists(
        //                    item =>
        //                    item.PositionType == (int)NewsPositionForZoneSportType.LastestNewsByZoneOnList && item.ZoneId == newsPosition.ZoneId))
        //                {
        //                    updateForUnlockPosition.Add(new UnlockPosition
        //                    {
        //                        PositionType =
        //                            (int)NewsPositionForZoneSportType.LastestNewsByZoneOnList,
        //                        ZoneId = newsPosition.ZoneId
        //                    });
        //                }
        //            }
        //        }
        //        foreach (var position in updateForUnlockPosition)
        //        {
        //            NewsPositionForZoneSportDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //        }
        //        return ErrorMapping.ErrorCodes.Success;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}

        #endregion

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType type, int position, int zoneId,
                                                               string title, string avatar, string url)
        {
            return NewsPositionDal.SaveLinkPosition((int) type, position, zoneId, title, avatar, url)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        //private class CurrentPositionData
        //{
        //    public int TypeId { get; set; }
        //    public int ZoneId { get; set; }
        //    public List<NewsPositionEntity> CurrentPosition { get; set; }
        //}

        #region override methods
        //public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        //{
        //    #region Temporary comment
        //    var errorCode = ErrorMapping.ErrorCodes.BusinessError;

        //    if (newsPositions == null || newsPositions.Count <= 0) return errorCode;

        //    try
        //    {
        //        var currentNewsPositions = new List<CurrentPositionData>();
        //        foreach (var newsPosition in newsPositions.Where(newsPosition => !currentNewsPositions.Exists(
        //            item => item.TypeId == newsPosition.TypeId && item.ZoneId == newsPosition.ZoneId)))
        //        {
        //            currentNewsPositions.Add(new CurrentPositionData
        //                {
        //                    TypeId = newsPosition.TypeId,
        //                    ZoneId = newsPosition.ZoneId,
        //                    CurrentPosition =
        //                        NewsPositionDal.GetListByTypeAndZoneId(newsPosition.Type, newsPosition.ZoneId)
        //                });
        //        }
        //        //// Truong hop 1 giao dien chi update 1 typeid => lay typeid va zoneid cua ban ghi dau tien
        //        //var typeId = newsPositions[0].TypeId;
        //        //var zoneId = newsPositions[0].ZoneId;
        //        //// Lay danh sach position hien tai tuong ung voi type
        //        //var currentNewsPositions = NewsPositionDal.GetListByTypeAndZoneId(typeId, zoneId);

        //        // UPDATE VAO VI TRI CAN CAI NOI BAT
        //        errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);

        //        // UPDATE VI TRI THANH CONG
        //        if (errorCode == ErrorMapping.ErrorCodes.Success)
        //        {
        //            var updateForUnlockPosition = new List<UnlockPosition>();

        //            foreach (var currentNewsPosition in currentNewsPositions)
        //            {
        //                var typeId = currentNewsPosition.TypeId;
        //                var zoneId = currentNewsPosition.ZoneId;

        //                #region Lay ra danh sach cac tin update moi vao vi tri noi bat (keo bang tay tu ben ngoai vao)
        //                var newsIdForAutoUpdate = "";
        //                foreach (var newsPosition in newsPositions)
        //                {
        //                    if (
        //                        !currentNewsPosition.CurrentPosition.Exists(
        //                            item => item.TypeId == newsPosition.TypeId && item.NewsId == newsPosition.NewsId))
        //                    {
        //                        newsIdForAutoUpdate += ";" + newsPosition.NewsId;
        //                    }
        //                }
        //                #endregion

        //                if (!string.IsNullOrEmpty(newsIdForAutoUpdate))
        //                {
        //                    var excludePositionTypeIds = "";

        //                    if (typeId == (int)NewsPositionType.HighlightHomeFocus || typeId == (int)NewsPositionType.TimelineLockedPositionOnHome)
        //                    {
        //                        #region Noi bat trang chu

        //                        #region Kiem tra bai tin nong

        //                        //var listNewsInLastestNewsForHomePosition =
        //                        //    NewsPositionDal.GetListNewsByListNewsId(
        //                        //        (int)NewsPositionType.FirstPageHome, 0, newsIdForAutoUpdate);
        //                        //// Bai dang nam trong vung bai tin nong trang chu => update lai vung nay
        //                        //if (listNewsInLastestNewsForHomePosition.Count > 0)
        //                        //{
        //                        //    excludePositionTypeIds = ((int) NewsPositionType.HighlightHomeFocus).ToString();
        //                        //    var updatedListNewsId = UpdateAutoWhenChangeManualPosition(0,
        //                        //                                                               (int)
        //                        //                                                               NewsPositionType
        //                        //                                                                   .FirstPageHome,
        //                        //                                                               newsIdForAutoUpdate,
        //                        //                                                               excludePositionTypeIds,
        //                        //                                                               false, 1, -1,
        //                        //                                                               -1, -1, -1);
        //                        //    if (!string.IsNullOrEmpty(updatedListNewsId))
        //                        //        newsIdForAutoUpdate += ";" + updatedListNewsId;
        //                        //}

        //                        #endregion

        //                        #region Check cac vung auto

        //                        foreach (var newsPosition in newsPositions)
        //                        {
        //                            if (!updateForUnlockPosition.Exists(
        //                                    item =>
        //                                    item.PositionType == (int)NewsPositionType.FirstPageHome &&
        //                                    item.ZoneId == 0 && item.NewsId == newsPosition.NewsId))
        //                            {
        //                                updateForUnlockPosition.Add(new UnlockPosition
        //                                {
        //                                    PositionType =
        //                                        (int)NewsPositionType.FirstPageHome,
        //                                    ZoneId = 0,
        //                                    NewsId = newsPosition.NewsId
        //                                });
        //                            }
        //                        }

        //                        #endregion

        //                        #endregion
        //                    }
        //                    else if (typeId == (int)NewsPositionType.HighlightListFocusByZone)
        //                    {
        //                        #region Noi bat muc

        //                        #region Check cac vung auto

        //                        foreach (var newsPosition in newsPositions)
        //                        {
        //                            if (!updateForUnlockPosition.Exists(
        //                                    item =>
        //                                    item.PositionType == (int)NewsPositionType.FirstPageListByZone &&
        //                                    item.ZoneId == newsPosition.ZoneId && item.NewsId == newsPosition.NewsId))
        //                            {
        //                                updateForUnlockPosition.Add(new UnlockPosition
        //                                {
        //                                    PositionType =
        //                                        (int)NewsPositionType.FirstPageListByZone,
        //                                    ZoneId = newsPosition.ZoneId,
        //                                    NewsId = newsPosition.NewsId
        //                                });
        //                            }
        //                        }

        //                        #endregion

        //                        #endregion
        //                    }
        //                }
        //            }

        //            foreach (var position in updateForUnlockPosition)
        //            {
        //                if (position.PositionType == (int) NewsPositionType.FirstPageHome)
        //                {
        //                    //UpdateAutoWhenChangeManualPosition(0,
        //                    //                                   position.PositionType,
        //                    //                                   position.NewsId,
        //                    //                                   ((int) NewsPositionType.HighlightHomeFocus).ToString(),
        //                    //                                   true, 1, -1, -1, -1, -1);
        //                    BoFactory.GetInstance<NewsPositionBo>()
        //                         .UpdateAutoWhenEditNewsV2(true, position.ZoneId, position.PositionType,
        //                                                   Utility.ConvertToLong(position.NewsId),
        //                                                       ((int)NewsPositionType.HighlightHomeFocus).ToString(),
        //                                                       true, 1, -1, -1, -1, -1);
        //                }
        //                else if (position.PositionType == (int) NewsPositionType.FirstPageListByZone)
        //                {
        //                    //UpdateAutoWhenChangeManualPosition(position.ZoneId,
        //                    //                                   position.PositionType,
        //                    //                                   position.NewsId,
        //                    //                                   ((int) NewsPositionType.HighlightHomeFocus).ToString(),
        //                    //                                   false, -1, -1, -1, -1, -1);
        //                    BoFactory.GetInstance<NewsPositionBo>()
        //                         .UpdateAutoWhenEditNewsV2(true, position.ZoneId, position.PositionType,
        //                                                   Utility.ConvertToLong(position.NewsId),
        //                                                       ((int)NewsPositionType.HighlightHomeFocus).ToString(),
        //                                                       false, -1, -1, -1, -1, -1);
        //                }
        //                //NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
        //    }
        //    return errorCode;
        //    #endregion



        //    //var errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
        //    //if (errorCode == ErrorMapping.ErrorCodes.Success)
        //    //{
        //    //    var updateForUnlockPosition = new List<UnlockPosition>();
        //    //    var count = newsPositions.Count;
        //    //    for (var i = 0; i < count; i++)
        //    //    {
        //    //        var newsPosition = newsPositions[i];
        //    //        NewsPositionType typeId;
        //    //        if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
        //    //        {
        //    //            typeId = NewsPositionType.HighlightHomeFocus;
        //    //        }
        //    //        if (typeId == NewsPositionType.HighlightHomeFocus)
        //    //        {
        //    //            if (!updateForUnlockPosition.Exists(
        //    //                item =>
        //    //                item.PositionType == (int)NewsPositionType.FirstPageHome && item.ZoneId == 0))
        //    //            {
        //    //                updateForUnlockPosition.Add(new UnlockPosition
        //    //                {
        //    //                    PositionType =
        //    //                        (int)NewsPositionType.FirstPageHome,
        //    //                    ZoneId = 0
        //    //                });
        //    //            }
        //    //        }
        //    //        else if (typeId == NewsPositionType.HighlightListFocusByZone)
        //    //        {
        //    //            if (!updateForUnlockPosition.Exists(
        //    //                item =>
        //    //                item.PositionType == (int)NewsPositionType.FirstPageListByZone && item.ZoneId == newsPosition.ZoneId))
        //    //            {
        //    //                updateForUnlockPosition.Add(new UnlockPosition
        //    //                {
        //    //                    PositionType =
        //    //                        (int)NewsPositionType.FirstPageListByZone,
        //    //                    ZoneId = newsPosition.ZoneId
        //    //                });
        //    //            }
        //    //            if (!updateForUnlockPosition.Exists(
        //    //                item =>
        //    //                item.PositionType == (int)NewsPositionType.FirstPageHome && item.ZoneId == 0))
        //    //            {
        //    //                updateForUnlockPosition.Add(new UnlockPosition
        //    //                {
        //    //                    PositionType =
        //    //                        (int)NewsPositionType.FirstPageHome,
        //    //                    ZoneId = 0
        //    //                });
        //    //            }
        //    //        }
        //    //    }
        //    //    foreach (var position in updateForUnlockPosition)
        //    //    {
        //    //        NewsPositionDal.UpdateForUnlockedPosition(position.PositionType, position.ZoneId);
        //    //    }
        //    //}
        //    //return errorCode;
        //}
        #endregion

        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            if (!string.IsNullOrEmpty(allZoneIdIncludePrimary) && ("," + allZoneIdIncludePrimary + ",").IndexOf("," + primaryZoneId + ",", StringComparison.Ordinal) < 0)
            {
                allZoneIdIncludePrimary += "," + primaryZoneId;
            }
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.FirstPageListByZone, allZoneIdIncludePrimary, news.Id,
                                                             0);

            //if (news.IsOnHome)
            //{
                NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.FirstPageHome, "0", news.Id,
                                                                 primaryZoneId);
            //}
            //else
            //{
            //    NewsPositionDal.RemoveNewsFromAutoUpdatePosition((int)NewsPositionType.FirstPageHome, news.Id);
            //}
            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HighlightHomeFocus + "," +
                                               (int)NewsPositionType.HighlightListFocusByZone + "," +
                                               (int)NewsPositionType.TimelineLockedPositionOnHome + "," +
                                               (int)NewsPositionType.HighlightMobileFocus;

                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HighlightHomeFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.HighlightListFocusByZone ||
                        newsPosition.TypeId == (int)NewsPositionType.TimelineLockedPositionOnHome ||
                        newsPosition.TypeId == (int)NewsPositionType.HighlightMobileFocus)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int) NewsPositionType.HighlightHomeFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.HighlightListFocusByZone ||
                    newsPosition.TypeId == (int)NewsPositionType.TimelineLockedPositionOnHome ||
                    newsPosition.TypeId == (int)NewsPositionType.HighlightMobileFocus)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
