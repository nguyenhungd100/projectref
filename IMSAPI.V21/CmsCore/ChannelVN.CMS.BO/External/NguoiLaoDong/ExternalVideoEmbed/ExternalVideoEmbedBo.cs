﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.NguoiLaoDong;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.ExternalCms.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.External.NguoiLaoDong.ExternalVideoEmbed
{
    public class ExternalVideoEmbedBo
    {
        public static ErrorMapping.ErrorCodes Insert(ExternalVideoEmbedEntity externalVideoEmbed)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(externalVideoEmbed));
                if (ExternalVideoEmbedDal.Insert(externalVideoEmbed))
                {
                    Logger.WriteLog(Logger.LogType.Trace, " ==> OK");
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int zoneId, int type)
        {
            try
            {
                if (ExternalVideoEmbedDal.Delete(zoneId, type))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type)
        {
            return ExternalVideoEmbedDal.GetListByZoneType(zoneId, type);
        }
    }
}
