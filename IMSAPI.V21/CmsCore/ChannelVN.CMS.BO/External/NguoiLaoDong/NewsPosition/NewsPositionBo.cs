﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.News;
using System.Linq;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.NewsPosition;

namespace ChannelVN.CMS.BO.External.NguoiLaoDong.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        public static int ForumZoneId
        {
            get
            {
                return 1264;
            }
        }
        public static int IctNewsZoneId
        {
            get
            {
                return 1317;
            }
        }
        public static int DuLichNewsZoneId
        {
            get
            {
                return 1348;
            }
        }
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsFocusByZone,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0),
                    TopLastestNewsForHome =
                        GetListByTypeAndZoneIdWithOrderByDistritbutionDate(
                            (int)NewsPositionType.TopHotNewsForHome, 0, 15)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public override ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex, bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            var errorCode = ErrorMapping.ErrorCodes.BusinessError;
            try
            {
                // UPDATE VAO VI TRI CAN CAI NOI BAT
                errorCode = base.SaveNewsPosition(newsPositions, avatarIndex, checkNewsExists, usernameForAction, isSwap);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return errorCode;
        }

        public virtual bool CheckNewsDistributionDateForLastestNews(long newsId, ref DateTime minValidDate)
        {
            var isValid = false;
            var minDate = DateTime.MaxValue;
            if (newsId > 0)
            {
                var newsList = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.TopHotNewsForHome, 0, "");
                var newsDistribution = BoFactory.GetInstance<NewsBo>().GetNewsByNewsId(newsId).DistributionDate;
                foreach (var newsPositionEntity in newsList)
                {
                    if (minDate > newsPositionEntity.DistributionDate)
                        minDate = newsPositionEntity.DistributionDate;
                    if (newsPositionEntity.DistributionDate < newsDistribution)
                    {
                        isValid = true;
                    }
                }
            }
            else
            {
                isValid = true;
            }
            minValidDate = minDate;
            return isValid;
        }
        public virtual bool RemoveNewsDisplayInSlide(string newsIdList)
        {
            return ChannelVN.CMS.DAL.External.NguoiLaoDong.NewsPositionDal.RemoveNewsDisplayInSlide(newsIdList);
        }


        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.LastestNewsByZone, allZoneIdIncludePrimary, news.Id,
                                                             0);
            if (news.DisplayInSlide == 1)
            {
                NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.TopHotNewsForHome, "0", news.Id,
                    primaryZoneId);
            }
            else
            {
                NewsPositionDal.RemoveNewsFromAutoUpdatePosition((int)NewsPositionType.TopHotNewsForHome, news.Id);
            }
            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HomeNewsFocus + "," +
                                           (int)NewsPositionType.NewsFocusByZone;

                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
