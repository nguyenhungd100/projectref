﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler;
using ChannelVN.CMS.Entity.External.CafeF.Expert;

namespace ChannelVN.CMS.BO.External.CafeF
{
    public class BusinessNewsBo
    {
        public static BusinessNewsCrawlerDetailEntity GetById(long id)
        {
            try
            {
                return BusinessNewsDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new BusinessNewsCrawlerDetailEntity();
            }
        }

        public static List<BusinessNewsCrawlerSourceEntity> GetAll()
        {
            try
            {
                return BusinessNewsDal.GetAll();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BusinessNewsCrawlerSourceEntity>();
            }
        }

        public static List<BusinessNewsCrawlerDetailEntity> SearchNews(int source, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return BusinessNewsDal.SearchNews(source, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<BusinessNewsCrawlerDetailEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes UpdateStatus(long id, int status)
        {
            try
            {
                var inserted = BusinessNewsDal.UpdateStatus(id, status);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
