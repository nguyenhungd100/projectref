﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.Base.News;
using NewsPositionEntity = ChannelVN.CMS.Entity.Base.NewsPosition.NewsPositionEntity;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.NewsPosition;

namespace ChannelVN.CMS.BO.External.CafeF.NewsPosition
{
    public class NewsPositionBo : Base.NewsPosition.NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HighlightListFocusByZone,
                            zoneId),
                    FocusPositionOnLastestNews =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.TimelineLockedPositionOnHome,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage()
        {
            try
            {
                //return new NewsPositionForHomePageEntity
                //    {
                //        HighlightHomeFocus =
                //            GetListNewsPositionByTypeAndZoneId(
                //                (int) NewsPositionType.HighlightHomeFocus, 0),
                //        FocusPositionOnLastestNews =
                //            GetListNewsPositionByTypeAndZoneId(
                //                (int) NewsPositionType.FirstPageHome, 0, "1;5")
                //    };

                #region Temporary Comment
                var highlightHomeFocus = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.HighlightHomeFocus, 0);
                var timelineLockedPosition = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.TimelineLockedPositionOnHome, 0);

                var lockedPosition = new List<NewsPositionEntity>();
                var unlockedPosition = new List<NewsPositionEntity>();
                foreach (var position in timelineLockedPosition)
                {
                    if (position.Locked)
                    {
                        lockedPosition.Add(position);
                    }
                    else
                    {
                        unlockedPosition.Add(position);
                    }
                }
                // Neu co vi tri unlock
                if (unlockedPosition.Count > 0)
                {
                    // Lay danh sach timeline va sap xep theo thoi gian (giong voi tieu chi sap xep cua frontend)
                    var timelineUnlockedPosition = GetListNewsPositionByTypeAndZoneId((int)NewsPositionType.FirstPageHome, 0);
                    var unlockCount = timelineUnlockedPosition.Count;
                    for (var i = unlockCount - 1; i >= 0; i--)
                    {
                        if (lockedPosition.Exists(item => item.NewsId == timelineUnlockedPosition[i].NewsId))
                        {
                            timelineUnlockedPosition.RemoveAt(i);
                        }
                    }
                    timelineUnlockedPosition.Sort(
                        (item1, item2) =>
                        item2.DistributionDate != item1.DistributionDate
                            ? item2.DistributionDate.CompareTo(item1.DistributionDate)
                            : item1.Id.CompareTo(item2.Id));

                    // Sap xep lai cac vi tri locked de chen vao timeline
                    lockedPosition.Sort((item1, item2) => item1.Order.CompareTo(item2.Order));
                    foreach (var position in lockedPosition)
                    {
                        timelineUnlockedPosition.Insert(position.Order, position);
                    }

                    // Lap qua cac vi tri unlock va update bai nam o vi tri tuong ung trong timeline vao cac vi tri nay
                    var count = timelineLockedPosition.Count;
                    for (var i = 0; i < count; i++)
                    {
                        var timelineLockedPositionIndex = timelineLockedPosition[i].Order - 1;
                        if (!timelineLockedPosition[i].Locked && timelineLockedPositionIndex <= timelineUnlockedPosition.Count)
                        {

                            timelineLockedPosition[i].Title = timelineUnlockedPosition[timelineLockedPositionIndex].Title;
                            timelineLockedPosition[i].SubTitle = timelineUnlockedPosition[timelineLockedPositionIndex].SubTitle;
                            timelineLockedPosition[i].Sapo = timelineUnlockedPosition[timelineLockedPositionIndex].Sapo;
                            timelineLockedPosition[i].Avatar = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar;
                            timelineLockedPosition[i].AvatarDesc = timelineUnlockedPosition[timelineLockedPositionIndex].AvatarDesc;
                            timelineLockedPosition[i].Avatar1 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar1;
                            timelineLockedPosition[i].Avatar2 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar2;
                            timelineLockedPosition[i].Avatar3 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar3;
                            timelineLockedPosition[i].Avatar4 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar4;
                            timelineLockedPosition[i].Avatar5 = timelineUnlockedPosition[timelineLockedPositionIndex].Avatar5;
                            timelineLockedPosition[i].IsFocus = timelineUnlockedPosition[timelineLockedPositionIndex].IsFocus;
                            timelineLockedPosition[i].NewsType = timelineUnlockedPosition[timelineLockedPositionIndex].NewsType;
                            timelineLockedPosition[i].Source = timelineUnlockedPosition[timelineLockedPositionIndex].Source;
                            timelineLockedPosition[i].DistributionDate = timelineUnlockedPosition[timelineLockedPositionIndex].DistributionDate;
                            timelineLockedPosition[i].Url = timelineUnlockedPosition[timelineLockedPositionIndex].Url;
                            timelineLockedPosition[i].ViewCount = timelineUnlockedPosition[timelineLockedPositionIndex].ViewCount;
                            timelineLockedPosition[i].AvatarCustom = timelineUnlockedPosition[timelineLockedPositionIndex].AvatarCustom;
                            timelineLockedPosition[i].OriginalId = timelineUnlockedPosition[timelineLockedPositionIndex].OriginalId;
                        }
                    }
                }

                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus = highlightHomeFocus,
                    FocusPositionOnLastestNews = timelineLockedPosition
                };
                #endregion
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType type, int position, int zoneId,
                                                               string title, string avatar, string url)
        {
            return NewsPositionDal.SaveLinkPosition((int)type, position, zoneId, title, avatar, url)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        //private class CurrentPositionData
        //{
        //    public int TypeId { get; set; }
        //    public int ZoneId { get; set; }
        //    public List<NewsPositionEntity> CurrentPosition { get; set; }
        //}

        public override ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.FirstPageListByZone, 
                allZoneIdIncludePrimary, news.Id, 0);
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.FirstPageHome, 
                "0", news.Id, primaryZoneId);

            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HighlightHomeFocus + "," +
                                           (int)NewsPositionType.HighlightListFocusByZone + "," +
                                           (int)NewsPositionType.TimelineLockedPositionOnHome;
                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HighlightHomeFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.HighlightListFocusByZone ||
                        newsPosition.TypeId == (int)NewsPositionType.TimelineLockedPositionOnHome)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }

        public override ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HighlightHomeFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.HighlightListFocusByZone ||
                    newsPosition.TypeId == (int)NewsPositionType.TimelineLockedPositionOnHome)
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }
    }
}
