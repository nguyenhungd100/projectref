﻿using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.NewsGallery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BO.External.CafeF.NewsGallery
{
    public class NewsGalleryBo : ChannelVN.CMS.BO.Base.News.NewsBo
    {
        public static ErrorMapping.ErrorCodes SaveNewsGallery(NewsGalleryEntity info, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = NewsGalleryDal.SaveNewsGallery(info, ref id);
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes SaveListNewsGallery(List<NewsGalleryEntity> lstInfo, ref int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;

            var createSuccess = false;
            foreach (var info in lstInfo)
            {
                if (info.SlideId == 0)
                    createSuccess = NewsGalleryDal.SaveNewsGallery(info, ref id);
                else if (info.SlideId > 0)
                    createSuccess = NewsGalleryDal.UpdateNewsGallery(info);

            }
            if (createSuccess && id > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateNewsGallery(NewsGalleryEntity info)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;

            var createSuccess = false;
            createSuccess = NewsGalleryDal.UpdateNewsGallery(info);
            if (createSuccess && info.SlideId > 0)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static ErrorMapping.ErrorCodes DeleteNewsGallery(int id)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;

            var createSuccess = false;
            createSuccess = NewsGalleryDal.DeleteNewsGallery(id);
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }

        public static NewsGalleryEntity SelectNewsGalleryById(int id)
        {
            return NewsGalleryDal.SelectNewsGalleryById(id);
        }

        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId)
        {
            return NewsGalleryDal.SelectNewsGalleryByNewsId(newsId);
        }
        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows)
        {
            return NewsGalleryDal.SelectNewsGalleryByNewsId(newsId, pageIndex, pageSize, ref totalRows);
        }
    }
}
