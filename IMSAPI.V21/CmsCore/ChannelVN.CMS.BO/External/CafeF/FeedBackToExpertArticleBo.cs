﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.Expert;

namespace ChannelVN.CMS.BO.External.CafeF
{
    public class FeedBackToExpertArticleBo
    {
        public static FeedBackToExpertArticleEntity GetById(int id)
        {
            try
            {
                return FeedBackToExpertArticleDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new FeedBackToExpertArticleEntity();
            }
        }

        public static List<FeedBackToExpertArticleEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FeedBackToExpertArticleDal.Search(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FeedBackToExpertArticleEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes UpdateStatus(int id, int status)
        {
            try
            {
                var inserted = FeedBackToExpertArticleDal.UpdateStatus(id, status);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
