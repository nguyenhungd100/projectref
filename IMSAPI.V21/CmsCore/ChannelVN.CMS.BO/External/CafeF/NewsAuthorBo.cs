﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeF.Expert;

namespace ChannelVN.CMS.BO.External.CafeF
{
    public class NewsAuthorBo
    {
        public static ErrorMapping.ErrorCodes Insert(ExpertEntity ExpertEntity, ref int ExpertId)
        {
            try
            {
                var inserted = ExpertDal.Insert(ExpertEntity, ref ExpertId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ExpertEntity GetById(int id)
        {
            try
            {
                return ExpertDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ExpertEntity();
            }
        }

        public static ErrorMapping.ErrorCodes Update(ExpertEntity expertEntity)
        {
            try
            {
                var inserted = ExpertDal.Update(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<ExpertEntity> Search(string keyword)
        {
            try
            {
                return ExpertDal.Search(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ExpertEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = ExpertDal.Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            try
            {                
                return ExpertDal.GetExpertByNewsId(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ExpertEntity();
            }
        }

        public static ErrorMapping.ErrorCodes InsertExpertIntoNews(ExpertInNews expertEntity)
        {
            try
            {
                var expertInNews = ExpertDal.GetExpertByNewsId(expertEntity.NewsId);
                bool inserted;
                if (expertInNews != null && expertInNews.Id > 0)
                {
                    if (expertEntity.ExpertId == 0)
                    {
                        ExpertDal.DeleteExpertNewsId(expertEntity.NewsId);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    inserted = ExpertDal.UpdateExpertInNews(expertEntity);
                    return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                inserted = ExpertDal.InsertExpertToNews(expertEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:23
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="currentUsername"></param>
        /// <returns></returns>
        public static NewsDetailForEditEntity GetNewsForEditByNewsId(long newsId, string currentUsername)
        {
            var newsVersionId = "";
            var newsDetail = new NewsDetailForEditEntity();
            //newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeview(ZoneBo.GetListZoneByUsername(currentUsername), "--- ");
            newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(currentUsername, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin), "--");
            newsDetail.NewsInfo = (newsId < 0 ? null : NewsBo.GetNewsByNewsId(newsId, currentUsername, ref newsVersionId));
            var ExpertEntity = GetExpertByNewsId(newsId);
            if (ExpertEntity == null)
            {
                newsDetail.ExpertId = 0;
            }
            else
            {
                var expertnews = GetExpertByNewsId(newsId);
                newsDetail.ExpertId = expertnews.Id;
                newsDetail.ExpertQuote = expertnews.Quote;
            }
            if (null != newsDetail.NewsInfo)
            {
                // Zone
                if (newsId > 0)
                {
                    newsDetail.NewsInZone = NewsBo.GetNewsInZoneByNewsId(newsId);
                    newsDetail.NewsRelation = NewsBo.GetRelatedNewsByNewsId(newsId);
                    newsDetail.TagInNews = NewsBo.GetTagNewsWithTagInfoByNewsId(newsId);
                }
                else
                {
                    newsDetail.NewsInZone = NewsBo.GetNewsInZoneByZoneIdList(newsId, newsDetail.NewsInfo.ListZoneId);
                    newsDetail.NewsRelation = NewsBo.GetRelatedNewsByNewsIds(newsDetail.NewsInfo.NewsRelation);
                    newsDetail.TagInNews = NewsBo.GetTagNewsWithTagInfo(newsId, newsDetail.NewsInfo.TagPrimary,
                                                                 newsDetail.NewsInfo.Tag);
                }

                newsDetail.NewsBySource = BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId);
                newsDetail.NewsByAuthor = Base.News.NewsAuthorBo.GetListInNews(newsId);
                //newsDetail.NewsByAuthor = new List<NewsByAuthorEntity>();
            }
            newsDetail.ListVersion = NewsBo.GetListVersionByNewsId((newsId < 0 ? 0 : newsId), currentUsername);
            //if (newsVersionId > 0)
            //{
            //    var newsChildVersions = NewsChildVersionDal.GetByNewsId(newsVersionId, false);
            //    newsDetail.ListNewsChild = newsChildVersions.Select(newsChildVersion => new NewsChildEntity
            //    {
            //        NewsParentId =
            //            newsChildVersion.
            //            NewsParentId,
            //        Order =
            //            newsChildVersion.
            //            Order,
            //        Body =
            //            newsChildVersion.
            //            Body
            //    }).ToList();
            //}
            //else
            //{
            //    newsDetail.ListNewsChild = NewsChildDal.GetByNewsId(newsId, false);
            //}
            var newsContent = NewsContentDal.GetByNewsId(newsId);
            if (newsContent != null)
            {
                newsDetail.NewsContentWithTemplate = newsContent.Body;
            }
            else
            {
                newsDetail.NewsContentWithTemplate = newsDetail.NewsInfo != null ? newsDetail.NewsInfo.Body : "";
            }
            newsDetail.NewsExtensions = NewsExtensionDal.GetByNewsId(newsId);
            return newsDetail;
        }

        public static List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return ExpertDal.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

    }
}
