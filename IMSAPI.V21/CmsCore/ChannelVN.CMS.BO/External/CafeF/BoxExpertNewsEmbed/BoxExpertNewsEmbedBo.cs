﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.External.CafeF;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;

namespace ChannelVN.CMS.BO.External.CafeF.BoxExpertNewsEmbed
{
    public class BoxExpertNewsEmbedBo
    {
        public static List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            var newsEmbedBox = new List<BoxExpertNewsEmbedListEntity>();
            try
            {

                return BoxExpertNewsEmbedDal.GetListBoxExpertNewsEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static ErrorMapping.ErrorCodes Insert(BoxExpertNewsEmbedEntity newsEmbebBox)
        {
            try
            {
                if (null != newsEmbebBox)
                {
                    return BoxExpertNewsEmbedDal.Insert(newsEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxExpertNewsEmbedDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listNewsId, int zoneId, int type)
        {
            try
            {
                if (null != listNewsId)
                {
                    BoxExpertNewsEmbedDal.Update(listNewsId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxExpertNewsEmbedDal.Update:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(long newsId, int zoneId, int type)
        {
            try
            {
                if (newsId > 0 && zoneId > 0)
                {
                    BoxExpertNewsEmbedDal.Delete(newsId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxExpertNewsEmbedDal.Delete:{0}", ex.Message));
            }

        }

    }
}
