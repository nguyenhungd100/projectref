﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.External.CafeF.EmbedAlbumb
{
    public class EmbedAlbumBo
    {
        public static ErrorMapping.ErrorCodes CreateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            if (embedAlbumForEdit == null || embedAlbumForEdit.EmbedAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (string.IsNullOrEmpty(embedAlbumForEdit.EmbedAlbum.Name))
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var hasAvatar = false;
            foreach (var embedAlbumDetail in embedAlbumForEdit.ListEmbedAlbumDetail.Where(embedAlbumDetail => embedAlbumDetail.IsAvatar))
            {
                if (hasAvatar)
                {
                    embedAlbumDetail.IsAvatar = false;
                }
                else
                {
                    hasAvatar = true;
                    embedAlbumForEdit.EmbedAlbum.Avatar = embedAlbumDetail.ImageUrl;
                }
            }
            if (!hasAvatar && embedAlbumForEdit.ListEmbedAlbumDetail.Count > 0)
            {
                embedAlbumForEdit.ListEmbedAlbumDetail[0].IsAvatar = true;
                embedAlbumForEdit.EmbedAlbum.Avatar = embedAlbumForEdit.ListEmbedAlbumDetail[0].ImageUrl;
            }
            embedAlbumForEdit.EmbedAlbum.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            embedAlbumForEdit.EmbedAlbum.Status = 1;

            var newEmbedAlbumId = 0;
            if (DAL.External.CafeF.EmbedAlbumDal.Insert(embedAlbumForEdit.EmbedAlbum, ref newEmbedAlbumId))
            {
                if (newEmbedAlbumId > 0)
                {
                    foreach (var embedAlbumDetail in embedAlbumForEdit.ListEmbedAlbumDetail)
                    {
                        embedAlbumDetail.EmbedAlbumId = newEmbedAlbumId;
                        embedAlbumDetail.Status = 1;

                        var newEmbedAlbumDetailId = 0;
                        EmbedAlbumDetailDal.Insert(embedAlbumDetail, ref newEmbedAlbumDetailId);
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateEmbedAlbum(EmbedAlbumForEditEntity embedAlbumForEdit)
        {
            if (embedAlbumForEdit == null || embedAlbumForEdit.EmbedAlbum == null || embedAlbumForEdit.EmbedAlbum.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var embedAlbumId = embedAlbumForEdit.EmbedAlbum.Id;
            var currentEmbedAlbum = EmbedAlbumDal.GetById(embedAlbumId);
            if (currentEmbedAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (string.IsNullOrEmpty(embedAlbumForEdit.EmbedAlbum.Name))
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var hasAvatar = false;
            foreach (var embedAlbumDetail in embedAlbumForEdit.ListEmbedAlbumDetail.Where(embedAlbumDetail => embedAlbumDetail.IsAvatar))
            {
                if (hasAvatar)
                {
                    embedAlbumDetail.IsAvatar = false;
                }
                else
                {
                    hasAvatar = true;
                    embedAlbumForEdit.EmbedAlbum.Avatar = embedAlbumDetail.ImageUrl;
                }
            }
            if (!hasAvatar && embedAlbumForEdit.ListEmbedAlbumDetail.Count > 0)
            {
                embedAlbumForEdit.ListEmbedAlbumDetail[0].IsAvatar = true;
                embedAlbumForEdit.EmbedAlbum.Avatar = embedAlbumForEdit.ListEmbedAlbumDetail[0].ImageUrl;
            }
            embedAlbumForEdit.EmbedAlbum.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

            if (DAL.External.CafeF.EmbedAlbumDal.Update(embedAlbumForEdit.EmbedAlbum))
            {
                var listEmbedAlbumDetailIds = "";
                foreach (var embedAlbumDetail in embedAlbumForEdit.ListEmbedAlbumDetail)
                {
                    if (embedAlbumDetail.Id > 0)
                    {
                        embedAlbumDetail.EmbedAlbumId = embedAlbumId;
                        EmbedAlbumDetailDal.Update(embedAlbumDetail);
                        listEmbedAlbumDetailIds += ";" + embedAlbumDetail.Id;
                    }
                    else
                    {
                        embedAlbumDetail.EmbedAlbumId = embedAlbumId;
                        embedAlbumDetail.Status = 1;

                        var newEmbedAlbumDetailId = 0;
                        if (EmbedAlbumDetailDal.Insert(embedAlbumDetail, ref newEmbedAlbumDetailId))
                        {
                            listEmbedAlbumDetailIds += ";" + newEmbedAlbumDetailId;
                        }
                    }
                }
                EmbedAlbumDetailDal.DeleteByEmbedAlbumIdExcudeListIds(embedAlbumId, listEmbedAlbumDetailIds);
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
