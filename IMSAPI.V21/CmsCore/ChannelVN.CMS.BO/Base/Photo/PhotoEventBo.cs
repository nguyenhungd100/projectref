﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.Photo;
using System;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoEventBo
    {
        public static List<PhotoEventEntity> SerchPhotoEvent(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {            
            return PhotoEventDal.SearchPhotoEvent(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);            
        }
        public static PhotoEventEntity GetPhotoEventById(int id)
        {
            try
            {
                return PhotoEventDal.GetById(id);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZonePhotoDal.GetById:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes InsertPhotoEvent(PhotoEventEntity photoEvent, ref int zonePhotoId)
        {
            if (string.IsNullOrEmpty(photoEvent.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = PhotoEventDal.Insert(photoEvent, ref zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                photoEvent.Id = zonePhotoId;
                //BoCached.Base.ZonePhoto.ZonePhotoDalFactory.AddZonePhoto(zonePhoto);
            }

            return result;
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoEvent(PhotoEventEntity photoEvent)
        {
            if (string.IsNullOrEmpty(photoEvent.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = PhotoEventDal.Update(photoEvent)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //BoCached.Base.ZonePhoto.ZonePhotoDalFactory.AddZonePhoto(zonePhoto);
            }
            return result;
        }        
        public static ErrorMapping.ErrorCodes DeletePhotoEvent(int zonePhotoId)
        {
            return PhotoEventDal.Delete(zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }


        public static List<PhotoEventEntity> GetPrivateZonePhoto(string username, int parentZoneId, int status)
        {
            try
            {
                var zones = GetPhotoEventActivedByUsernameAndPermissionIds(username, (int)EnumPermission.ApprovePhoto, (int)EnumPermission.PhotoManager);
                if (parentZoneId != -1)
                {
                    zones = zones.Where(s=>s.Status == status).ToList();
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<PhotoEventEntity>();
            }
        }
        public static List<PhotoEventEntity> GetPhotoEventActivedByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

                return PhotoEventDal.GetPhotoEventActivedByUsernameAndPermissionIds(username, listPermissionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<PhotoEventEntity>();
            }
        }
    }
}
