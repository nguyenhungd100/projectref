﻿using AutoMapper;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoByAuthorBo
    {
        public static ErrorMapping.ErrorCodes SavePhotoByAuthor(PhotoByAuthorEntity photoByAuthor, ref int photoByAuthorId)
        {
            var res = false;
            try
            {
                if (null == photoByAuthor)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = PhotoByAuthorDal.SavePhotoByAuthor(photoByAuthor, ref photoByAuthorId);
                if (result)
                {
                    photoByAuthor.Id = photoByAuthorId;
                    var photoByAuthorES = Mapper.DynamicMap<PhotoByAuthorSearchEntity>(photoByAuthor);

                    res = BoSearch.Base.Photo.PhotoByAuthorDalFactory.SavePhotoByAuthor(photoByAuthorES);
                }
                return res ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
    }
}
