﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoTagBo
    {
        public static ErrorMapping.ErrorCodes InsertPhotoTag(PhotoTagEntity photoTag, ref int photoTagId)
        {
            if (string.IsNullOrEmpty(photoTag.Name))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }           
                        
            return PhotoTagDal.Insert(photoTag, ref photoTagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoTag(PhotoTagEntity photoTag)
        {
            if (string.IsNullOrEmpty(photoTag.Name))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }                        
            return PhotoTagDal.Update(photoTag)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoTag(string tagName, ref int tagId)
        {
            if (string.IsNullOrEmpty(tagName))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }
            var tag = new PhotoTagEntity()
            {
                Name = tagName,               
                Url = Utility.UnicodeToKoDauAndGach(tagName),                
                Status = (int)EnumPhotoTagStatus.Actived
            };
            return PhotoTagDal.UpdateByName(tag, ref tagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<PhotoTagEntity> SearchPhotoTag(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return PhotoTagDal.SearchTag(keyword, status, pageIndex, pageSize, ref totalRow);
        }        
        public static PhotoTagEntity GetPhotoTagById(int id)
        {
            return PhotoTagDal.GetById(id);
        }

        public static PhotoTagEntity GetPhotoTagByName(string name)
        {
            return PhotoTagDal.GetTagByTagName(name);
        }

        //public static List<PhotoTagEntity> GetPhotoTagByIds(string tagIds)
        //{
        //    var data = BoCached.Base.Photo.PhotoTagDalFactory.GetPhotoTagByIds(tagIds);
        //    if(data==null || (data!=null && data.Count() <= 0))
        //    {
        //        data= PhotoTagDal.GetByIds(tagIds);
        //    }

        //    return data;
        //}
    }
}
