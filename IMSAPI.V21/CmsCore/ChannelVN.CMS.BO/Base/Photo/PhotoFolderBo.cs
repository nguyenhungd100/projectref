﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoFolderBo
    {
        #region Update

        public static ErrorMapping.ErrorCodes InsertPhotoFolder(PhotoFolderEntity photoFolder, ref int newPhotoFolderId)
        {
            try
            {
                if (null == photoFolder)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoFolderDal.Insert(photoFolder, ref newPhotoFolderId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoFolder(PhotoFolderEntity photoFolder)
        {
            try
            {
                if (null == photoFolder)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoFolderDal.Update(photoFolder) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoFolder(int photoFolderId)
        {
            try
            {
                return PhotoFolderDal.Delete(photoFolderId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static PhotoFolderEntity GetPhotoFolderByPhotoFolderId(int photoFolderId)
        {
            try
            {
                return PhotoFolderDal.GetById(photoFolderId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoFolderEntity> GetPhotoFolderByParentId(int parentId, string createdBy)
        {
            try
            {
                return PhotoFolderDal.GetByParentId(parentId, createdBy);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoFolderEntity> PhotoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            try
            {
                return PhotoFolderDal.Search(parentId, name, createdBy, status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<PhotoFolderEntity> GetAllFolderWithTreeView(string createdBy)
        {
            var listFolder = PhotoFolderDal.Search(-1, "", createdBy, 1);
            return BindFolderToTreeview(listFolder,"--");

        }
        public static List<PhotoFolderEntity> BindFolderToTreeview(List<PhotoFolderEntity> allFolder, string prefix)
        {
            var allFolderWithSimpleField = new List<PhotoFolderEntity>();
            var count = allFolder.Count;
            foreach (var folder in allFolder)
            {
                allFolderWithSimpleField.Add(new PhotoFolderEntity
                {
                    Id = folder.Id,
                    ParentId = folder.ParentId,
                    Name = folder.Name,
                    Status = folder.Status,
                    CreatedBy = folder.CreatedBy
                });
            }

            allFolderWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.CreatedDate);
            var outputFolder = new List<PhotoFolderEntity>();
            BindFolderToTreeview(allFolderWithSimpleField.ToList(), 0,prefix, ref outputFolder);
            return outputFolder;
        }

        private static void BindFolderToTreeview(List<PhotoFolderEntity> allFolder, int currentParentId, string prefix, ref List<PhotoFolderEntity> outputFolder)
        {
            if (allFolder == null || allFolder.Count <= 0) return;
            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allFolder.Count;
            for (var i = 0; i < count; i++)
            {
                if (allFolder[i].ParentId == currentParentId)
                {
                    allFolder[i].Name = currentPrefix + " " + allFolder[i].Name;
                    outputFolder.Add(allFolder[i]);
                    BindFolderToTreeview(allFolder, allFolder[i].Id, currentPrefix + prefix, ref outputFolder);
                }
            }
        }
        #endregion
    }
}
