﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class AlbumByAuthorBo
    {
        public static ErrorMapping.ErrorCodes SaveAlbumByAuthor(AlbumByAuthorEntity albumByAuthor)
        {
            var res = false;
            try
            {
                if (null == albumByAuthor)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = AlbumByAuthorDal.SaveAlbumByAuthor(albumByAuthor);
                if (result)
                {
                    var photoByAuthorES = Mapper.DynamicMap<AlbumByAuthorSearchEntity>(albumByAuthor);

                    res = BoSearch.Base.Photo.AlbumByAuthorDalFactory.SaveAlbumByAuthor(photoByAuthorES);
                }
                return res ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
    }
}
