﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoLabelBo
    {
        #region Update

        public static ErrorMapping.ErrorCodes InsertPhotoLabel(PhotoLabelEntity photoLabel, ref int newPhotoLabelId)
        {
            try
            {
                if (null == photoLabel)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoLabelDal.Insert(photoLabel, ref newPhotoLabelId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoLabel(PhotoLabelEntity photoLabel)
        {
            try
            {
                if (null == photoLabel)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoLabelDal.Update(photoLabel) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoLabel(int photoLabelId)
        {
            try
            {
                return PhotoLabelDal.Delete(photoLabelId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static PhotoLabelEntity GetPhotoLabelByPhotoLabelId(int photoLabelId)
        {
            try
            {
                return PhotoLabelDal.GetById(photoLabelId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoLabelEntity> GetPhotoLabelByParentLabelId(int parentLabelId, string createdBy)
        {
            try
            {
                return PhotoLabelDal.GetByParentId(parentLabelId, createdBy);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion
    }
}
