﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.DAL.Base.News;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class AlbumBo
    {
        #region Album

        #region Update

        public static ErrorMapping.ErrorCodes InsertAlbum(AlbumEntity album, string tagIdList, string eventIdList, ref int newAlbumId)
        {
            try
            {
                if (null == album)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = AlbumDal.Insert(album, tagIdList, eventIdList, ref newAlbumId);
                if (result)
                {                                    
                    var albumES = new PhotoAlbumSearchEntity
                    {
                        Id = newAlbumId,
                        ZoneId = album.ZoneId,
                        Name = album.Name,
                        ThumbImage = album.ThumbImage,
                        Status = album.Status,
                        CreatedBy = album.CreatedBy,
                        CreatedDate = album.CreatedDate,
                        ModifiedBy = album.ModifiedBy,
                        ModifiedDate = album.ModifiedDate,
                        Tags = album.Tags,
                        Description = album.Description,
                        ZoneName = album.ZoneName,
                        EventIds = eventIdList.Split(new char[] { ',',';' },StringSplitOptions.RemoveEmptyEntries),
                        Author = album.Author
                    };
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.AddPhotoAlbum(albumES);
                }

                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateAlbum(AlbumEntity album, string tagIdList, string eventIdList)
        {
            try
            {
                if (null == album)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = AlbumDal.Update(album, tagIdList, eventIdList);
                if (result)
                {                                                     
                    var albumES = new PhotoAlbumSearchEntity
                    {
                        Id = album.Id,
                        ZoneId = album.ZoneId,
                        Name = album.Name,
                        ThumbImage = album.ThumbImage,
                        Status = album.Status,
                        CreatedBy = album.CreatedBy,
                        CreatedDate = album.CreatedDate,
                        ModifiedBy = album.ModifiedBy,
                        ModifiedDate = album.ModifiedDate,
                        Tags = album.Tags,
                        Description = album.Description,
                        ZoneName = album.ZoneName,
                        EventIds = eventIdList.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries),
                        Author = album.Author
                    };
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.UpdatePhotoAlbum(albumES);
                }

                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteAlbum(int albumId, string accountName)
        {
            try
            {
                var result = AlbumDal.Delete(albumId);
                if (result)
                {
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.MoveTrash(albumId, accountName);
                }
                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SendAlbum(int albumId, string userDoAction)
        {
            try
            {
                if (0 >= albumId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var album = GetById(albumId);// AlbumDal.GetById(albumId);
                if (album == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (album.Status == (int)EnumPhotoAlbumStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToSend;
                }

                var success = AlbumDal.Send(albumId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.ChangeStatus(albumId, (int)EnumPhotoAlbumStatus.WaitForPublish, userDoAction);

                    ActivityBo.LogActionAlbum(1, album.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes PublishAlbum(int albumId, string userDoAction)
        {
            try
            {
                if (0 >= albumId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(userDoAction, (int)EnumPermission.ApprovePhoto))
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToPublish;
                }
                var album = GetById(albumId); //AlbumDal.GetById(albumId);
                if (album == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (album.Status == (int)EnumPhotoAlbumStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToPublish;
                }

                var success = AlbumDal.Publish(albumId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.ChangeStatus(albumId, (int)EnumPhotoAlbumStatus.Published, userDoAction);

                    ActivityBo.LogActionAlbum(2, album.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UnPublishAlbum(int albumId, string userDoAction)
        {
            try
            {
                if (0 >= albumId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(userDoAction, (int)EnumPermission.ApprovePhoto))
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToUnpublish;
                }
                var album = GetById(albumId); //AlbumDal.GetById(albumId);
                if (album == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (album.Status != (int)EnumPhotoAlbumStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToUnpublish;
                }

                var success = AlbumDal.UnPublish(albumId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.ChangeStatus(albumId, (int)EnumPhotoAlbumStatus.Unpublished, userDoAction);

                    ActivityBo.LogActionAlbum(3, album.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes ReturnAlbum(int albumId, string userDoAction)
        {
            try
            {
                if (0 >= albumId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }                
                var album = GetById(albumId); //AlbumDal.GetById(albumId);
                if (album == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (album.Status == (int)EnumPhotoAlbumStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToReturn;
                }

                var success = AlbumDal.Return(albumId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoAlbumDalFactory.ChangeStatus(albumId, (int)EnumPhotoAlbumStatus.Return, userDoAction);

                    ActivityBo.LogActionAlbum(4, album.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes AddPhotoInAlbum(string addPhotoIds, string deletePhotoIds, int albumId, string albumName = "")
        {
            try
            {
                if (albumId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var rs = AlbumDal.AddPhotoInAlbum(addPhotoIds, deletePhotoIds, albumId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (rs == ErrorMapping.ErrorCodes.Success)
                {
                    //var listAll = new List<string>();
                    //var a = addPhotoIds.Split(';').ToList();
                    //var b = deletePhotoIds.Split(';').ToList();
                    //listAll.AddRange(a);
                    //listAll.AddRange(b);
                    //var listDistinct = listAll.Distinct().ToList();
                    //BoCached.Base.Video.VideoDalFactory.DeletePlayListInVideo(listDistinct);

                    //update videoinplaylist ->es
                    //BoSearch.Base.Video.VideoDalFactory.UpdateVideoInPlayList(albumId, a, b, albumName);                    
                }

                return rs;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.AddVideoList:{0}", ex.Message));
            }
        }
        #endregion

        #region Get

        public static AlbumDetailEntity GetAlbumByAlbumId(int albumId)
        {
            try
            {
                var album = GetById(albumId);//AlbumDal.GetById(albumId);
                var events = PhotoEventDal.GetByAlbumId(albumId);
                var tags = PhotoTagDal.GetByAlbumId(albumId);

                var result = new AlbumDetailEntity {
                    Album = album,
                    ListEvents = events,
                    ListTags = tags
                };
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    var authorId = BoSearch.Base.Photo.AlbumByAuthorDalFactory.GetAuthorIdByAlbumId(albumId);
                    var newsAuthor = NewsAuthorDal.GetById(authorId);
                    if (newsAuthor != null)
                    {
                        result.AuthorDetail = new AuthorDetail
                        {
                            AuthorId = newsAuthor.Id,
                            AuthorName = newsAuthor.AuthorName,
                            AuthorType = newsAuthor.AuthorType,
                            AuthorTitle = newsAuthor.AuthorTitle,
                            Avatar = newsAuthor.Avatar
                        };
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new AlbumDetailEntity();
            }
        }

        public static AlbumEntity GetById(int albumId)
        {
            try
            {
                var album = BoSearch.Base.Photo.PhotoAlbumDalFactory.GetById(albumId);
                if (album == null)
                {
                    return AlbumDal.GetById(albumId);
                }

                return new AlbumEntity {
                    Id = album.Id,
                    ZoneId = album.ZoneId,
                    Name = album.Name,
                    ThumbImage = album.ThumbImage,
                    Status = album.Status,
                    CreatedBy = album.CreatedBy,
                    CreatedDate = album.CreatedDate,
                    ModifiedBy = album.ModifiedBy,
                    ModifiedDate = album.ModifiedDate,
                    Tags = album.Tags,
                    Description = album.Description,
                    ZoneName = album.ZoneName
                };                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new AlbumEntity();
            }
        }

        public static List<AlbumEntity> SearchAlbum(string keyword, int zoneId,int eventId,int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data= BoSearch.Base.Photo.PhotoAlbumDalFactory.SearchPhotoAlbum(keyword, zoneId, eventId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
                if(data==null || (data!=null && data.Count == 0))
                {
                    return AlbumDal.Search(keyword, zoneId, eventId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
                }
                return data.Select(s => new AlbumEntity {
                    Id = s.Id,
                    ZoneId = s.ZoneId,
                    Name = s.Name,
                    ThumbImage = s.ThumbImage,
                    Status = s.Status,
                    CreatedBy = s.CreatedBy,
                    CreatedDate = s.CreatedDate,
                    ModifiedBy = s.ModifiedBy,
                    ModifiedDate = s.ModifiedDate,
                    Tags = s.Tags,
                    Description = s.Description,
                    ZoneName = s.ZoneName,
                    Author = s.Author
                }).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<CountAlbumStatus> CountAlbum(string username, string listStatus, string userAction)
        {
            try
            {
                var list = listStatus.Split(',').ToList();
                var data= BoSearch.Base.Photo.PhotoAlbumDalFactory.CountPhotoAlbum(username, list, userAction);
                if (data == null || (data != null && data.Count == 0)) {
                    return AlbumDal.CountAlbum(username, list, userAction);
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }
        #endregion

        #endregion

        #region Album published

        #region Update

        public static ErrorMapping.ErrorCodes UpdateAlbumPublished(AlbumPublishedEntity photoPublished)
        {
            try
            {
                if (null == photoPublished)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return AlbumPublishedDal.Update(photoPublished) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateAlbumPublishedInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        {
            try
            {
                return AlbumPublishedDal.UpdateAlbumInUsed(listAlbumPublishedIdInUsed, newsId, createdBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        //public static ErrorMapping.ErrorCodes UpdateAlbumPublishedInUsedWhenInsertNews(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    try
        //    {
        //        return AlbumPublishedDal.UpdateAlbumInUsedWhenInsertNews(listAlbumPublishedIdInUsed, newsId, createdBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.Exception;
        //    }
        //}

        #endregion

        #region Get

        public static AlbumPublishedEntity GetAlbumPublishedByAlbumPublishedId(long photoPublishedId)
        {
            try
            {
                return AlbumPublishedDal.GetById(photoPublishedId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<AlbumPublishedEntity> GetAlbumPublishedByAlbumId(long photoId)
        {
            try
            {
                return AlbumPublishedDal.GetByAlbumId(photoId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPhotoAlbum(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = AlbumDal.InitRedisAllPhotoAlbum(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = AlbumDal.InitRedisAllPhotoAlbum(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new PhotoAlbumSearchEntity
                {
                    Id = s.Id,
                    ZoneId = s.ZoneId,
                    Name = s.Name,
                    ThumbImage = s.ThumbImage,
                    Status = s.Status,
                    CreatedBy = s.CreatedBy,
                    CreatedDate = s.CreatedDate,
                    ModifiedBy = s.ModifiedBy,
                    ModifiedDate = s.ModifiedDate,
                    Tags = s.Tags,
                    Description = s.Description,
                    ZoneName = s.ZoneName,
                    EventIds = PhotoEventDal.GetByAlbumId(s.Id).Select(z => z.Id.ToString()).ToArray()
                }).ToList();
                BoSearch.Base.Photo.PhotoAlbumDalFactory.InitAllPhotoAlbum(list);
            }, action);
        }
        #endregion
    }
}
