﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Photo;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.BoSearch.Entity;
using AutoMapper;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.DAL.Base.News;

namespace ChannelVN.CMS.BO.Base.Photo
{
    public class PhotoBo
    {
        #region Photo

        #region Update

        public static ErrorMapping.ErrorCodes InsertPhoto(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (!string.IsNullOrEmpty(tagIdList))
                {
                    var tagFormat = BoConstants.NewsUrlFormatForTag;

                    var tags = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
                    var tagLinkList = tags.Aggregate("", (current, tag) => current + ("," + string.Format(tagFormat, tag.Url, tag.Name)));
                    photo.Tags = tagLinkList;
                }

                return PhotoDal.Insert(photo, tagIdList, ref newPhotoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes InsertPhotoV2(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var result = PhotoDal.InsertV2(photo, tagIdList, ref newPhotoId);
                if (result)
                {
                    photo.Id = newPhotoId;
                    var photoES = Mapper.DynamicMap<PhotoSearchEntity>(photo);

                    BoSearch.Base.Photo.PhotoDalFactory.AddPhoto(photoES);
                }

                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhoto(PhotoEntity photo, string tagIdList)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (!string.IsNullOrEmpty(tagIdList))
                {
                    var tagFormat = BoConstants.NewsUrlFormatForTag;

                    var tags = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
                    var tagLinkList = tags.Aggregate("", (current, tag) => current + ("," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name)));
                    photo.Tags = tagLinkList;
                }

                return PhotoDal.Update(photo, tagIdList) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoV2(PhotoEntity photo, string tagIdList)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = PhotoDal.UpdateV2(photo, tagIdList);
                if (result)
                {
                    var photoES = Mapper.DynamicMap<PhotoSearchEntity>(photo);

                    BoSearch.Base.Photo.PhotoDalFactory.UpdatePhoto(photoES);
                }

                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }      

        public static ErrorMapping.ErrorCodes DeletePhoto(long photoId)
        {
            try
            {
                var result = PhotoDal.Delete(photoId);
                if (result)
                {
                    BoSearch.Base.Photo.PhotoDalFactory.MoveTrash(photoId, "");
                }
                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoInAlbum(string listPhotoId)
        {
            try
            {
                var result = PhotoDal.DeletePhotoInAlbum(listPhotoId);
                if (result)
                {
                    BoSearch.Base.Photo.PhotoDalFactory.MoveTrash(listPhotoId, "");
                }

                return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static List<PhotoPublishedEntity> PublishPhoto(string listPhotoId, int albumId, long newsId, int zoneId, string distributedBy)
        {
            try
            {
                var album = (albumId > 0 ? AlbumDal.GetById(albumId) : null);

                var listOfPhotoId = listPhotoId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                var updatedAlbumThumb = false;

                var listOfPhotoPublished = new List<PhotoPublishedEntity>();
                foreach (var id in listOfPhotoId.Select(Utility.ConvertToLong).Where(id => id > 0))
                {
                    listOfPhotoPublished.Add(PhotoDal.Publish(id, newsId, zoneId, albumId, distributedBy));

                    if (updatedAlbumThumb || null == album || !string.IsNullOrEmpty(album.ThumbImage)) continue;
                    AlbumDal.UpdateThumbImageByPhotoId(albumId, id);
                    updatedAlbumThumb = true;
                }

                return listOfPhotoPublished;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes SendPhoto(long photoId, string userDoAction)
        {
            try
            {
                if (0 >= photoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var photo = PhotoDal.GetById(photoId);
                if (photo == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (photo.Status == (int)EnumPhotoStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToSend;
                }

                var success = PhotoDal.Send(photoId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoDalFactory.ChangeStatus(photoId, (int)EnumPhotoStatus.WaitForPublish, userDoAction);

                    ActivityBo.LogActionPhoto(1, photo.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes PublishPhotoV2(long photoId, string userDoAction)
        {
            try
            {
                if (0 >= photoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(userDoAction, (int)EnumPermission.ApprovePhoto))
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToPublish;
                }
                var photo = PhotoDal.GetById(photoId);
                if (photo == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (photo.Status == (int)EnumPhotoStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToPublish;
                }

                var success = PhotoDal.PublishV2(photoId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoDalFactory.ChangeStatus(photoId, (int)EnumPhotoStatus.Published, userDoAction);

                    ActivityBo.LogActionPhoto(2, photo.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UnPublishPhoto(long photoId, string userDoAction)
        {
            try
            {
                if (0 >= photoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(userDoAction, (int)EnumPermission.ApprovePhoto))
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToUnpublish;
                }
                var photo = PhotoDal.GetById(photoId);
                if (photo == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (photo.Status != (int)EnumPhotoStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToUnpublish;
                }

                var success = PhotoDal.UnPublish(photoId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoDalFactory.ChangeStatus(photoId, (int)EnumPhotoStatus.Unpublished, userDoAction);

                    ActivityBo.LogActionPhoto(3, photo.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes ReturnPhoto(long photoId, string userDoAction)
        {
            try
            {
                if (0 >= photoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var photo = PhotoDal.GetById(photoId);
                if (photo == null)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotFound;
                }
                if (photo.Status == (int)EnumPhotoStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.PhotoNotAllowToReturn;
                }

                var success = PhotoDal.Return(photoId, userDoAction);
                if (success)
                {
                    //redis
                    //BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Photo.PhotoDalFactory.ChangeStatus(photoId, (int)EnumPhotoStatus.Return, userDoAction);

                    ActivityBo.LogActionPhoto(4, photo.Name, userDoAction);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static PhotoDetailEntity GetPhotoByPhotoId(long photoId)
        {
            try
            {
                var data = PhotoDal.GetById(photoId);
                var photoTags = PhotoTagDal.GetByPhotoId(photoId);
                return new PhotoDetailEntity
                {
                    PhotoInfo = data,
                    PhotoTags = photoTags
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static PhotoDetailEntity GetPhotoByPhotoIdV2(long photoId)
        {
            try
            {
                var data = GetById(photoId);//PhotoDal.GetByIdV2(photoId);
                var photoTags = PhotoTagDal.GetByPhotoId(photoId);
                
                var result = new PhotoDetailEntity
                {
                    PhotoInfo = data,
                    PhotoTags = photoTags
                };
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    var authorId = BoSearch.Base.Photo.PhotoByAuthorDalFactory.GetAuthorByPhotoId(photoId);
                    var newsAuthor = NewsAuthorDal.GetById(authorId);
                    if (newsAuthor != null)
                    {
                        result.AuthorDetail = new AuthorDetail
                        {
                            AuthorId = newsAuthor.Id,
                            AuthorName = newsAuthor.AuthorName,
                            AuthorType = newsAuthor.AuthorType,
                            AuthorTitle = newsAuthor.AuthorTitle,
                            Avatar = newsAuthor.Avatar
                        };
                    }                  
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static PhotoEntity GetById(long photoId)
        {
            try
            {
                var data = BoSearch.Base.Photo.PhotoDalFactory.GetById(photoId);
                if (data == null)
                    return PhotoDal.GetByIdV2(photoId);
                return Mapper.DynamicMap<PhotoEntity>(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static PhotoEntity GetPhotoByImageUrl(string imageUrl)
        {
            try
            {
                var data = PhotoDal.GetByImageUrl(imageUrl);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoEntity> SearchPhoto(string keyword, int photoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return PhotoDal.Search(keyword, photoLabelId, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoEntity> SearchPhotoV2(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Photo.PhotoDalFactory.SearchPhoto(keyword, zoneId, albumId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
                if (data == null || (data != null && data.Count == 0))
                    return PhotoDal.SearchV2(keyword, zoneId, albumId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
                return data.Select(s => Mapper.DynamicMap<PhotoEntity>(s)).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<PhotoEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Photo.PhotoDalFactory.ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);
                if (data == null || (data != null && data.Count == 0))
                    return PhotoDal.ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);
                return data.Select(s => Mapper.DynamicMap<PhotoEntity>(s)).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<CountAlbumStatus> CountPhoto(string username, string listStatus, string userAction)
        {
            try
            {
                var list = listStatus.Split(',').ToList();
                var data = BoSearch.Base.Photo.PhotoDalFactory.CountPhoto(username, list, userAction);
                if (data == null || (data != null && data.Count == 0))
                    return PhotoDal.CountPhoto(username, list, userAction);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }
        #endregion

        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPhoto(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = PhotoDal.InitRedisAllPhoto(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = PhotoDal.InitRedisAllPhoto(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => Mapper.DynamicMap<PhotoSearchEntity>(s)).ToList();
                BoSearch.Base.Photo.PhotoDalFactory.InitAllPhoto(list);
            }, action);
        }

        #endregion

        #region Photo published

        #region Update

        public static ErrorMapping.ErrorCodes UpdatePhotoPublished(PhotoPublishedEntity photoPublished)
        {
            try
            {
                if (null == photoPublished)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoPublishedDal.Update(photoPublished) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoPublishedAlbumid(PhotoPublishedUpdateEntity photoPublished)
        {
            try
            {
                if (null == photoPublished)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoPublishedDal.UpdateAlbumId(photoPublished) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoPublishedInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy)
        {
            try
            {
                return PhotoPublishedDal.UpdatePhotoInUsed(listPhotoPublishedIdInUsed, newsId, albumId, createdBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoPublishedPriority(string listPhotoPublishedId)
        {
            try
            {
                return PhotoPublishedDal.UpdatePriority(listPhotoPublishedId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoPublished(string listPhotoPublishedId)
        {
            try
            {
                return PhotoPublishedDal.Delete(listPhotoPublishedId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static PhotoPublishedEntity GetPhotoPublishedByPhotoPublishedId(long photoPublishedId)
        {
            try
            {
                return PhotoPublishedDal.GetById(photoPublishedId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoPublishedEntity> GetPhotoPublishedByPhotoId(long photoId)
        {
            try
            {
                return PhotoPublishedDal.GetByPhotoId(photoId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoPublishedEntity> SearchInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return PhotoPublishedDal.SearchInAlbum(keyword, albumId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        #endregion

        public static PhotoTagEntity GetTagByTagName(string name)
        {
            return PhotoTagDal.GetTagByTagName(name);
        }

        public static List<PhotoTagEntity> SearchTag(string name)
        {
            return PhotoTagDal.SearchTag(name);
        }

        public static ErrorMapping.ErrorCodes InsertPhotoTag(PhotoTagEntity photoTag, ref int newPhotoTagId)
        {
            try
            {
                if (null == photoTag)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoTagDal.Insert(photoTag, ref newPhotoTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }       
    }
}
