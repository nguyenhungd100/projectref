﻿using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.Photo;
using System;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.BO.Base.Zone
{
    public class ZonePhotoBo
    {
        public static List<ZonePhotoEntity> GetPrivateZonePhoto(string username, int parentZoneId, int status)
        {
            try
            {
                var zones = GetListZonePhotoActivedByUsernameAndPermissionIds(username, (int)EnumPermission.ApprovePhoto, (int)EnumPermission.PhotoManager);
                if (parentZoneId != -1)
                {
                    zones = zones.Where(s => s.ParentId == parentZoneId && s.Status == status).ToList();
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZonePhotoEntity>();
            }
        }
        public static List<ZonePhotoEntity> GetListZonePhotoActivedByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

                //var allZone = BoCached.Base.ZonePhoto.ZonePhotoDalFactory.GetZonePhotoActivedByUsernameAndPermissionIds(username, listPermissionId);
                //if (allZone == null || (allZone != null && allZone.Count() <= 0))
                //{
                //    allZone = ZonePhotoDal.GetZoneVideoActivedByUsernameAndPermissionIds(username, listPermissionId);
                //}

                return ZonePhotoDal.GetZonePhotoActivedByUsernameAndPermissionIds(username, listPermissionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZonePhotoEntity>();
            }
        }
        public static ErrorMapping.ErrorCodes InsertZonePhoto(ZonePhotoEntity zonePhoto, ref int zonePhotoId)
        {
            if (string.IsNullOrEmpty(zonePhoto.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = ZonePhotoDal.Insert(zonePhoto, ref zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                zonePhoto.Id = zonePhotoId;
                //BoCached.Base.ZonePhoto.ZonePhotoDalFactory.AddZonePhoto(zonePhoto);
            }

            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateZonePhoto(ZonePhotoEntity zonePhoto)
        {
            if (string.IsNullOrEmpty(zonePhoto.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = ZonePhotoDal.Update(zonePhoto)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //BoCached.Base.ZonePhoto.ZonePhotoDalFactory.AddZonePhoto(zonePhoto);
            }
            return result;
        }
        public static List<ZonePhotoEntity> GetListByParentId(int parentId, int status)
        {
            try
            {
                //var data= BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoByParentId(parentId, status);
                //if (data == null || (data != null && data.Count() <= 0))
                //{
                //    data = ZoneVideoDal.GetListByParentId(parentId, status);
                //    if(data!=null && data.Count > 0)
                //    {
                //        BoCached.Base.ZoneVideo.ZoneVideoDalFactory.InitAllZoneVideo(data);
                //    }
                //}
                return ZonePhotoDal.GetListByParentId(parentId, status);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZonePhotoDal.GetListByParentId:{0}", ex.Message));
            }
        }
        public static ZonePhotoEntity GetZonePhotoById(int id)
        {
            try
            {                              
                return ZonePhotoDal.GetById(id);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZonePhotoDal.GetById:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes MoveZonePhotoUp(int zonePhotoId)
        {
            return ZonePhotoDal.MoveUp(zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes MoveZonePhotoDown(int zonePhotoId)
        {
            return ZonePhotoDal.MoveDown(zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteZonePhoto(int zonePhotoId)
        {
            return ZonePhotoDal.Delete(zonePhotoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<ZonePhotoWithSimpleFieldEntity> BindAllOfZoneToTreeviewFullDepth(List<ZoneEntity> allZones, string prefix)
        {
            var allZonesWithSimpleField = new List<ZonePhotoWithSimpleFieldEntity>();
            var count = allZones.Count;
            foreach (var zone in allZones)
            {
                allZonesWithSimpleField.Add(new ZonePhotoWithSimpleFieldEntity
                {
                    Id = zone.Id,
                    ParentId = zone.ParentId,
                    Name = zone.Name,
                    Status = zone.Status,
                    Description = zone.Description
                });
            }

            allZonesWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.Id);
            var outputZones = new List<ZonePhotoWithSimpleFieldEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZonesWithSimpleField.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        private static void BindAllOfZoneToTreeviewFullDepth(List<ZonePhotoWithSimpleFieldEntity> allZones, int currentParentId, string prefix, ref List<ZonePhotoWithSimpleFieldEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentId == currentParentId)
                {
                    allZones[i].Name = currentPrefix + " " + allZones[i].Name;
                    outputZones.Add(allZones[i]);
                    BindAllOfZoneToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }

        public static List<ZonePhotoWithSimpleFieldEntity> GetAllZonePhotoWithTreeView()
        {
            return BindAllOfZoneToTreeviewFullDepth(ZonePhotoDal.GetZoneByParentId(-1), "--");
        }

        public static List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id)
        {
            return ZonePhotoDal.GetZonePhotoByPhotoId(id);
        }
    }
}
