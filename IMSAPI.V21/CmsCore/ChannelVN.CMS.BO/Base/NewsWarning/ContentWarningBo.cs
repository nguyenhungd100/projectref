﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsWarning;
using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BO.Base.NewsWarning
{
    public class ContentWarningBo
    {        
        public static ErrorMapping.ErrorCodes UpdateContentWarning(ContentWarningEntity contentWarning)
        {            
            var result= ContentWarningDal.Update(contentWarning) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            if(result== ErrorMapping.ErrorCodes.Success)
            {
                //update redis
                BoCached.Base.NewsContent.NewsContentDalFactory.UpdateContentWarning(contentWarning);
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes ContentWarningLog_Insert(long newsId, string account)
        {
            var result = ContentWarningDal.ContentWarningLog_Insert(newsId, account) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //update redis
                BoCached.Base.NewsContent.NewsContentDalFactory.AddContentWarningLog(newsId, account);
            }
            return result;
        }

        public static List<ContentWarningEntity> GetListContentWarningByLastModifiedDate(DateTime fromDate, DateTime toDate)
        {            
            return ContentWarningDal.GetListByLastModifiedDate(fromDate, toDate);
        }

        public static ContentWarningEntity GetContentWarningByNewsId(long newsId)
        {
            return ContentWarningDal.GetByNewsId(newsId);            
        }

        public static List<ContentWarningEntity> GetContentWarningByListNewsId(string listNewsId)
        {
            var listContentWarning = new List<ContentWarningEntity>();
            try {
                if (!string.IsNullOrEmpty(listNewsId))
                {
                    var newsIds = listNewsId.Split(',').ToList();
                    listContentWarning = BoCached.Base.NewsContent.NewsContentDalFactory.ListContentWarningByListNewsId(newsIds);

                    if (listContentWarning == null || (listContentWarning != null && listContentWarning.Count <= 0))
                    {
                        listContentWarning = ContentWarningDal.GetByListNewsId(listNewsId);
                        var ConfirmLog = ContentWarningLog_GetByListNewsId(listNewsId);
                        foreach (var contentWarning in listContentWarning)
                        {
                            contentWarning.ConfirmLog = ConfirmLog.Where(k => k.NewsId == contentWarning.NewsId).ToList();
                            //add to redis
                            BoCached.Base.NewsContent.NewsContentDalFactory.UpdateContentWarning(contentWarning);
                        }                        
                    }
                }
            }
            catch { }
            return listContentWarning;
        }
        public static List<ContentWarningLogEntity> ContentWarningLog_GetByListNewsId(string listNewsId)
        {
            try
            {
                return ContentWarningDal.ContentWarningLog_GetByListNewsId(listNewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ContentWarningLogEntity>();
            }
        }
        public static List<ContentWarningLogEntity> ContentWarningLog_GetByNewsId(long NewsId)
        {
            try
            {
                return ContentWarningDal.ContentWarningLog_GetByNewsId(NewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ContentWarningLogEntity>();
            }
        }
        public static ContentWarningEntity CheckNewsContentWarning(string title, string sapo, string content, string topic, string tags, string channel)
        {
            var contentW = new ContentWarningEntity();
            try
            {                
                return contentW;
            }
            catch
            {
                return contentW;
            }
        }
    }
}
