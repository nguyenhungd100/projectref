﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsCacheMonitor;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.DAL.External.SohaNews;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsCacheMonitor;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.Cached.Entity;
using ChannelVN.CMS.Entity.ErrorCode;
using NCalc;

namespace ChannelVN.CMS.BO.Base.NewsCacheMonitor
{
    public class NewsCachedMonitorBo
    {
        protected struct UpdatedNewsPosition
        {
            public int PositionType;
            public int ZoneId;
            public string ExcludeNewsInPositionTypeIds;
        }

        protected struct UpdateNewsInPosition
        {
            public int PositionType;
            public int ZoneId;
            public long NewsId;
            public bool IsChangeDistributionDate;
        }

        protected struct NewsPositionForBomd
        {
            public int PositionType;
            public int Order;
            public int ZoneId;
            public long NewsId;
            public string PublishedBy;
        }

        #region Public methods

        public virtual List<UpdateCachedEntity> GetUpdateCachedItemForNews(PageSetting pageSetting)
        {
            Logger.WriteLog(Logger.LogType.Debug, "Invoked ChannelVN.CMS.BO.Base.NewsCacheMonitor");

            var updateUrlItems = new List<UpdateCachedEntity>();
            var newsPositionForBomd = new List<NewsPositionForBomd>();

            // Update expired news position (nếu có trường hợp set bài PR thì mới cần)
            // NewsPositionDal.UpdateExpiredPosition();

            using (var reader = NewsCachedMonitorDal.GetChangedRecordForNews())
            {
                while (reader.Read())
                {
                    var zoneId = Utility.ConvertToInt(reader["ZoneId"]);
                    var newsId = Utility.ConvertToLong(reader["NewsId"]);

                    #region Update news position for bomd

                    NewsEntity existsNews = newsId > 0 ? DAL.Base.News.NewsDal.GetNewsById(newsId) : null;
                    if (existsNews != null)
                    {
                        var positionTypeForHome = Utility.ConvertToInt(reader["PositionTypeForHome"]);
                        var positionOrderForHome = Utility.ConvertToInt(reader["PositionOrderForHome"]);
                        if (positionTypeForHome > 0 && positionOrderForHome > 0)
                        {
                            UpdateNewsPositionForBomd(ref newsPositionForBomd,
                                               positionTypeForHome, positionOrderForHome,
                                               0,
                                               newsId, existsNews.PublishedBy);
                        } 
                        
                        var positionTypeForList = Utility.ConvertToInt(reader["PositionTypeForList"]);
                        var positionOrderForList = Utility.ConvertToInt(reader["PositionOrderForList"]);
                        if (positionTypeForList > 0 && positionOrderForList > 0)
                        {
                            UpdateNewsPositionForBomd(ref newsPositionForBomd,
                                                      positionTypeForList, positionOrderForList,
                                                      zoneId,
                                                      newsId, existsNews.PublishedBy);
                        }
                    }

                    #endregion

                    #region Khởi tạo danh sách url update cached

                    var hasUrl = false;
                    var updateOrder = 1; // Order=0 dành cho url delete
                    if (null != pageSetting && pageSetting.PageSettingItems.Length > 0)
                    {
                        var pageCount = pageSetting.PageSettingItems.Length;
                        for (var pageIndex = 0; pageIndex < pageCount; pageIndex++)
                        {
                            #region For cached data

                            var settingItem = pageSetting.PageSettingItems[pageIndex];
                            // Không xử lý page với type là tag hoặc thread
                            if (settingItem.Type != CacheSettingConstants.PageSettingType.ForNews) continue;

                            hasUrl = true;

                            #region Check condition

                            try
                            {
                                var conditionForCreateUpdateCachedUrl = settingItem.ConditionForCreateUpdateCachedUrl;
                                if (!string.IsNullOrEmpty(conditionForCreateUpdateCachedUrl))
                                {
                                    conditionForCreateUpdateCachedUrl =
                                        conditionForCreateUpdateCachedUrl.Replace("{zone}", "[zone]").Replace("{news}",
                                                                                                              "[news]")
                                            .Replace("{tag}", "[tag]");
                                    var condition = new Expression(conditionForCreateUpdateCachedUrl);
                                    condition.Parameters["zone"] = zoneId;
                                    condition.Parameters["news"] = newsId;
                                    condition.Parameters["tag"] = 0;
                                    var result = condition.Evaluate();
                                    if (result == null || result.ToString().Replace("=", "") != "True")
                                    {
                                        continue;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "GetUpdateCachedItemForNews:\n" + ex.ToString());
                            }

                            #endregion

                            #region Init Url

                            // Lấy QueryString format để update tham số
                            var queryString = settingItem.QueryStringFormat;
                            // Update tham số cho zone
                            var isParameterUpdated = AddParameterIntoUrlForUpdateNewsCached(queryString, "{zone}",
                                                                                        reader["ShortURL"],
                                                                                        ref queryString);
                            // Update tham số cho News
                            isParameterUpdated = isParameterUpdated &&
                                                 AddParameterIntoUrlForUpdateNewsCached(queryString, "{news}",
                                                                                        Utility.ConvertToLong(
                                                                                            reader["NewsId"]),
                                                                                        ref queryString);
                            // Nếu có lỗi thì chuyển sang page khác luôn
                            if (!isParameterUpdated || string.IsNullOrEmpty(queryString)) continue;

                            var updateUrl = settingItem.PageFile + "?updatecached=1&" + queryString; // Url để update cached
                            var deleteUrl = settingItem.PageFile + "?updatecached=2&" + queryString; // Url để xóa cached
                            // Add url delete cached vào danh sách url update
                            AddUrlIntoUpdateListForUpdateCached(ref updateUrlItems, deleteUrl,
                                                                Utility.ConvertToLong(reader["Id"]), updateOrder++, true);
                            // Xét đến các extend querystring cần update
                            var extendQueryStrings = settingItem.ExtendQueryStringForCachedUpdates;
                            var extendQueryStringCount = extendQueryStrings.Length;
                            if (extendQueryStringCount > 0)
                            {
                                // Nếu có extend querystring thì Add từng extend querystring vào danh sách url dùng để update
                                for (var extQueryIndex = 0; extQueryIndex < extendQueryStringCount; extQueryIndex++)
                                {
                                    AddUrlIntoUpdateListForUpdateCached(ref updateUrlItems,
                                                                        updateUrl + "&" +
                                                                        extendQueryStrings[extQueryIndex],
                                                                        Utility.ConvertToLong(reader["Id"]), updateOrder++, false);
                                }
                            }
                            else
                            {
                                // Nếu không có extend querystring thì Add url update vừa tạo vào danh sách url dùng để update
                                AddUrlIntoUpdateListForUpdateCached(ref updateUrlItems, updateUrl,
                                                                    Utility.ConvertToLong(reader["Id"]), updateOrder++, false);
                            }
                            #endregion

                            #endregion
                        }
                    }
                    if (!hasUrl)
                    {
                        AddUrlIntoUpdateListForUpdateCached(ref updateUrlItems, "",
                                                                    Utility.ConvertToLong(reader["Id"]), updateOrder, false);
                    }
                    else
                    {
                        updateUrlItems.Sort((object1, object2) => object1.UpdateOrder.CompareTo(object2.UpdateOrder));
                    }

                    #endregion
                }
            }
            Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(updateUrlItems));

            #region update news position

            foreach (var newsPosition in newsPositionForBomd)
            {
                Logger.WriteLog(Logger.LogType.Debug, "API before update NewsPositionDal.UpdateForBomd(" + newsPosition.PositionType + ", " + newsPosition.ZoneId + ", " + newsPosition.Order + ", " + newsPosition.NewsId + ")");
                BoFactory.GetInstance<NewsPositionBo>().SaveNewsPosition(new List<NewsPositionEntity>
                    {
                        new NewsPositionEntity
                            {
                                TypeId = newsPosition.PositionType,
                                ZoneId = newsPosition.ZoneId,
                                Order = newsPosition.Order,
                                NewsId = newsPosition.NewsId
                            }
                    }, -1, false, newsPosition.PublishedBy, false);
                Logger.WriteLog(Logger.LogType.Debug, "API update completed NewsPositionDal.UpdateForBomd(" + newsPosition.PositionType + ", " + newsPosition.ZoneId + ", " + newsPosition.Order + ", " + newsPosition.NewsId + ")");
            }

            #endregion

            return updateUrlItems;
        }

        private static void UpdateNewsPositionForBomd(ref List<NewsPositionForBomd> newsPositions, int positionType, int order, int zoneId, long newsId, string publishedBy)
        {
            var newsPositionType = GetNewsPositionType(positionType);
            if (null != newsPositionType)
            {
                if (newsPositionType.FilterByZoneId != 1)
                {
                    zoneId = 0;
                }
            }

            var count = newsPositions.Count;
            for (var i = 0; i < count; i++)
            {
                if (newsPositions[i].PositionType != positionType || newsPositions[i].Order != order ||
                    newsPositions[i].ZoneId != zoneId) continue;
                newsPositions[i] = new NewsPositionForBomd
                {
                    PositionType = positionType,
                    Order = order,
                    ZoneId = zoneId,
                    NewsId = newsId,
                    PublishedBy = publishedBy
                };
                return;
            }
            newsPositions.Add(new NewsPositionForBomd
            {
                PositionType = positionType,
                Order = order,
                ZoneId = zoneId,
                NewsId = newsId,
                PublishedBy = publishedBy
            });
        }

        public virtual ErrorMapping.ErrorCodes UpdateCachedProcessIdForNews(string listOfProcessId)
        {
            return NewsCachedMonitorDal.UpdateProcessItemStateForNews(listOfProcessId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public virtual ErrorMapping.ErrorCodes UpdateCachedProcessIdForTag(string listOfProcessId)
        {
            return NewsCachedMonitorDal.UpdateProcessItemStateForTag(listOfProcessId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        #endregion

        #region Private methods

        protected static List<NewsPositionTypeEntity> _newsPositionTypes;

        protected static NewsPositionTypeEntity GetNewsPositionType(int positionTypeId)
        {
            if (null == _newsPositionTypes) _newsPositionTypes = NewsPositionTypeDal.GetAll();

            return _newsPositionTypes.FirstOrDefault(newsPositionType => newsPositionType.Id == positionTypeId);
        }

        protected static bool AddParameterIntoUrlForUpdateNewsCached(string urlFormat, string parameterKeyPattern, object parameterValue, ref string outputUrl)
        {
            if (parameterValue == null) parameterValue = "";
            if (urlFormat.IndexOf(parameterKeyPattern) >= 0)
            {
                //if (null != parameterValue && parameterValue.ToString() != "")
                //{
                    outputUrl = outputUrl.Replace(parameterKeyPattern, parameterValue.ToString());
                    return true;
                //}
                //else
                //{
                //    outputUrl = urlFormat;
                //    return false;
                //}
            }
            outputUrl = urlFormat;
            return true;
        }

        protected static void AddUrlIntoUpdateListForUpdateCached(ref List<UpdateCachedEntity> updateUrls, string url, long id, int updateOrder, bool isDeleteUrl = false)
        {
            url = url.ToLower();

            if (!updateUrls.Exists(item => item.UpdateUrl.Equals(url, StringComparison.CurrentCultureIgnoreCase)))
            {
                var ids = new List<long> { id };

                updateUrls.Add(new UpdateCachedEntity { UpdateUrl = url, ListOfProcessId = ids, UpdateOrder = (isDeleteUrl ? 0 : updateOrder) });
            }
            else
            {
                var updateUrl = updateUrls.Find(item => item.UpdateUrl.Equals(url, StringComparison.CurrentCultureIgnoreCase));
                if (!updateUrl.ListOfProcessId.Contains(id))
                {
                    updateUrl.ListOfProcessId.Add(id);
                }
            }
        }

        #endregion
    }
}
