﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.LogAnalytics;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.LogAnalytics;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.LogAnalytics
{
    public class LogAnalyticsBo
    {

        public static ErrorMapping.ErrorCodes Update(LogAnalyticsEntity log)
        {
            try
            {
                var inserted = LogAnalyticsDal.Update(log);

                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

    }
}
