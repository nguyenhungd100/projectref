﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.MediaAlbum;
using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BO.Base.MediaAlbum
{
    public class MediaAlbumBo
    {
        public static List<MediaAlbumEntity> SearchMediaAlbum(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return MediaAlbumDal.Search(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
        }
        public static MediaAlbumEntity GetMediaAlbumByAlbumId(int MediaAlbumId)
        {
            var MediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);            
            return MediaAlbum;
        }
        public static MediaAlbumForEditEntity GetMediaAlbumForEditByMediaAlbumId(int MediaAlbumId, int topPhoto)
        {
            var MediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (MediaAlbum != null)
            {
                return new MediaAlbumForEditEntity
                {
                    MediaAlbum = MediaAlbum,
                    ListMediaAlbumDetail = MediaAlbumDetailDal.GetMediaAlbumDetailByAlbumId(MediaAlbumId)
                };
            }
            return null;
        }

        public static ErrorMapping.ErrorCodes CreateMediaAlbum(MediaAlbumEntity MediaAlbumForEdit, ref int newMediaAlbumId)
        {
            if (MediaAlbumForEdit == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
                        
            if (MediaAlbumDal.Insert(MediaAlbumForEdit, ref newMediaAlbumId))
            {                
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMediaAlbum(MediaAlbumEntity MediaAlbumForEdit)
        {
            if (MediaAlbumForEdit == null || MediaAlbumForEdit.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            var MediaAlbumId = MediaAlbumForEdit.Id;
            var currentMediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (currentMediaAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }                       

            if (MediaAlbumDal.Update(MediaAlbumForEdit))
            {                
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteMediaAlbum(int MediaAlbumId)
        {
            if (MediaAlbumId <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (MediaAlbumDal.Delete(MediaAlbumId))
                return ErrorMapping.ErrorCodes.Success;

            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateStatusMediaAlbum(int MediaAlbumId, string action)
        {
            if (MediaAlbumId <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            switch (action)
            {
                case "publib":break;
            }

            var currentMediaAlbum = MediaAlbumDal.GetById(MediaAlbumId);
            if (currentMediaAlbum == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            if(MediaAlbumDal.Delete(MediaAlbumId))
                return ErrorMapping.ErrorCodes.Success;
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteMediaAlbumDetail(long id)
        {
            if (id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            MediaAlbumDetailDal.DeleteMediaAlbumDetail(id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static MediaAlbumDetailEntity GetMediaAlbumDetailForEditById(int MediaAlbumId)
        {
            var MediaAlbum = MediaAlbumDetailDal.GetMediaAlbumDetailById(MediaAlbumId);
            if (MediaAlbum != null)
            {
                return MediaAlbum;
            }
            return null;
        }
        public static ErrorMapping.ErrorCodes CreateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit, ref int newMediaAlbumId)
        {            
            if (MediaAlbumDetailDal.InsertMediaAlbumDetail(MediaAlbumForEdit, string.Empty, ref newMediaAlbumId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumForEdit)
        {

            if (MediaAlbumDetailDal.UpdateMediaAlbumDetail(MediaAlbumForEdit, string.Empty))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
