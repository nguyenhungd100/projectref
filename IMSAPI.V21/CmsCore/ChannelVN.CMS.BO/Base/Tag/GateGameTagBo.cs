﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class GateGameTagBo
    {
        public virtual GateGameTagEntity GetGateGameTagByTagId(long tagId)
        {
            return GateGameTagDal.GetGateGameTagByTagId(tagId);
        }

        public virtual ErrorMapping.ErrorCodes Insert(GateGameTagEntity gateGameTagEntity, ref int gateGameTagId)
        {
            try
            {
                var inserted = GateGameTagDal.Insert(gateGameTagEntity, ref gateGameTagId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
