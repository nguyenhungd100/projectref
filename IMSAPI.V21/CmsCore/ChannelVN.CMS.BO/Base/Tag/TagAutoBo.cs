﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class TagAutoBo
    {

        public virtual  List<TagAutoEntity>GetAutoTagList()
        {
            return TagAutoDal.GetAutoTagList();
        } 

        public virtual ErrorMapping.ErrorCodes UpdateAutoTagIsProcess(long newsId)
        {
            return TagAutoDal.UpdateAutoTagIsProcess(newsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
