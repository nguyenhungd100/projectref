﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class TagCloudBo
    {

        public virtual List<QueueUpdateTagChannelEntity> GetUnProcess(int channelId)
        {
            return TagCloudDal.GetUnProcess(channelId);
        }

        public virtual ErrorMapping.ErrorCodes UpdateIsProcess(long newsId)
        {
            return TagCloudDal.UpdateIsProcess(newsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
