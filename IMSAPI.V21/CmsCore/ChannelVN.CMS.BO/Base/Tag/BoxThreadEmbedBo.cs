﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class BoxThreadEmbedBo
    {
        public virtual List<BoxThreadEmbedEntity> GetListThreadEmbed(int zoneId, int type)
        {
            var ThreadEmbed = new List<BoxThreadEmbedEntity>();
            try
            {

                return BoxThreadEmbedDal.GetListThreadEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return ThreadEmbed;

        }

        public virtual ErrorMapping.ErrorCodes Insert(BoxThreadEmbedEntity ThreadEmbebBox)
        {
            try
            {
                if (null != ThreadEmbebBox)
                {
                    return BoxThreadEmbedDal.Insert(ThreadEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxThreadEmbedDal.Insert:{0}", ex.Message));
            }

        }

        public virtual ErrorMapping.ErrorCodes Update(string listThreadId, int zoneId, int type)
        {
            try
            {
                if (null != listThreadId)
                {
                    BoxThreadEmbedDal.Update(listThreadId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxThreadEmbedDal.Update:{0}", ex.Message));
            }

        }

        public virtual ErrorMapping.ErrorCodes Delete(long ThreadId, int zoneId, int type)
        {
            try
            {
                if (ThreadId > 0 && zoneId > 0)
                {
                    BoxThreadEmbedDal.Delete(ThreadId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxThreadEmbedDal.Delete:{0}", ex.Message));
            }

        }
    }
}
