﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class TagScoreBo
    {
        public virtual List<TagScoreEntity> ListTagScore(long TagId)
        {
            try
            {
                return TagScoreDal.List(TagId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagScoreEntity>();
        }
        public virtual ErrorMapping.ErrorCodes InsertTagScore(TagScoreEntity tagscore)
        {
            return TagScoreDal.Insert(tagscore) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes DeleteTagScore(long TagId)
        {
            return TagScoreDal.Delete(TagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
