﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Statistic;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.MessageQueue;
using ChannelVN.CMS.MessageQueue.Constants;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class TagBo
    {
        public virtual List<TagEntity> SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            return TagDal.SearchAllByKeyword(keyword, zoneId, isThread);
        }
        public virtual List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, EnumSearchTagOrder orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            var tags= new List<TagEntity>();
            try
            {
                
                if (orderBy == EnumSearchTagOrder.NewsCountDesc || orderBy == EnumSearchTagOrder.NewsCountAsc)
                {
                    tags = TagDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
                }
                else {
                    var data = BoSearch.Base.Tag.TagDalFactory.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);

                    if (data == null || (data != null && data.Count <= 0))
                    {
                        tags = TagDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, (int)orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
                    }
                    else
                    {
                        var countNewsInTag = TagDal.CountTagNewsByListTagId(string.Join(",", data.Select(s => s.Id).ToArray()));

                        tags = data.Select(s => new TagEntity
                        {
                            Id = s.Id,
                            ParentId = s.ParentId,
                            Name = s.Name,
                            ZoneId = s.ZoneId,
                            Avatar = s.Avatar,
                            IsHotTag = s.IsHotTag,
                            IsThread = s.IsThread,
                            CreatedDate = s.CreatedDate,
                            CreatedBy = s.CreatedBy,
                            Url = s.Url,
                            Status = s.Status,
                            NewsCount = MappingCountTagNews(s.Id, countNewsInTag),//s.NewsCount,
                            Invisibled = s.Invisibled,
                            ZoneName = s.ZoneName
                        }).ToList();
                    }
                }
                
                return tags;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return tags;
            }             
        }

        private int MappingCountTagNews(long tagId, List<CountTagNewsEntity> data)
        {
            try
            {
                if(data!=null && data.Count > 0)
                {
                    foreach(var item in data)
                    {
                        if (item.TagId == tagId)
                            return item.CountNews;
                    }
                }
                return 0;
            }
            catch
            {
                return 0;
            }
        }


        private int CountNewsInTag(long tagId)
        {
            var count = 0;
            BoSearch.Base.Tag.TagDalFactory.UpdateTagNewsCount(tagId,count);
            return count;
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByTagAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = TagDal.InitESAllTag(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = TagDal.InitESAllTag(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new TagSearchEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    ZoneId = s.ZoneId,
                    CreatedDate = s.CreatedDate,
                    IsHotTag=s.IsHotTag,
                    IsThread=s.IsThread,
                    ParentId=s.ParentId,
                    Avatar=s.Avatar,
                    CreatedBy=s.CreatedBy,
                    Invisibled=s.Invisibled,
                    NewsCount=s.NewsCount,
                    Status=s.Status,
                    Url=s.Url,
                    ZoneName=s.ZoneName
                }).ToList();
                BoSearch.Base.Tag.TagDalFactory.InitAllTag(list);
            }, action);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByTagAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = TagDal.InitESAllTag(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = TagDal.InitESAllTag(page, pageSize, startDate, endDate, ref totalRows);                
                BoCached.Base.Tag.TagDalFactory.InitAllTag(data);
            }, action);
        }

        public static List<TagEntity> Tag_SearchInBoxEmbed(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return TagDal.Tag_SearchInBoxEmbed(keyword,isHotTag, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagEntity>();
        }
        public static List<TagEntity> Tag_SearchInBoxEmbed_Approved(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return TagDal.Tag_SearchInBoxEmbed_Approved(keyword, isHotTag, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagEntity>();
        }
        public static List<TagWithSimpleFieldEntity> Tag_GetRelation(long TagId)
        {
            try
            {
                return TagDal.Tag_GetRelation(TagId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TagWithSimpleFieldEntity>();
        }
        public virtual ErrorMapping.ErrorCodes Update_RelationTag(long tagId, string tagRelations)
        {
            try
            {
                return TagDal.Update_RelationTag(tagId, tagRelations) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual List<TagWithSimpleFieldEntity> SearchTagForSuggestion(int top, int zoneId, string keyword, int isHot, EnumSearchTagOrder orderBy, bool getTagHasNewsOnly)
        {
            var tags = new List<TagWithSimpleFieldEntity>();
            try
            {
                //auto sugget chi lay isInvisibled = 0(hien thi)
                var totalRow = 0;
                var data = BoSearch.Base.Tag.TagDalFactory.SearchTag(keyword, 0, -1, zoneId, 0, (int)orderBy, isHot, 1, top, ref totalRow, getTagHasNewsOnly, 0);

                if (data == null || (data != null && data.Count <= 0))
                {
                    tags = TagDal.SearchTagForSuggestion(top, zoneId, keyword, isHot, (int)orderBy, getTagHasNewsOnly);
                }
                else
                {
                    tags = data.Select(s => new TagWithSimpleFieldEntity
                    {
                        Id = s.Id,                        
                        Name = s.Name,                        
                        Url = s.Url,                        
                        NewsCount = s.NewsCount,
                        UnsignName= Utility.ConvertTextToUnsignName(s.Name)
                    }).ToList();
                }

                //tags = TagDal.SearchTagForSuggestion(top, zoneId, keyword, isHot, (int)orderBy, getTagHasNewsOnly);

                return tags;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return tags;
            }                       
        }

        public virtual TagEntity GetTagDbByTagId(long tagId)
        {
            return TagDal.GetTagByTagId(tagId);
        }
        public virtual TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            return TagDal.GetTagByTagName(name, getTagHasNewsOnly);
        }
        public virtual TagEntity GetTagByTagNameOrUrl(string name, string url)
        {
            return TagDal.GetTagByTagNameOrUrl(name, url);
        }
        public virtual TagEntityDetail GetTagByTagId(long tagId)
        {
            TagEntityDetail tagDetail = null;
            var tag = TagDal.GetTagByTagId(tagId);
            if (null != tag)
            {
                tagDetail = new TagEntityDetail()
                {
                    Id = tag.Id,
                    ParentId = tag.ParentId,
                    Name = tag.Name,
                    Description = tag.Description,
                    Url = tag.Url,
                    Invisibled = tag.Invisibled,
                    IsHotTag = tag.IsHotTag,
                    Type = tag.Type,
                    CreatedDate = tag.CreatedDate,
                    ModifiedDate = tag.ModifiedDate,
                    CreatedBy = tag.CreatedBy,
                    EditedBy = tag.EditedBy,
                    UnsignName = tag.UnsignName,
                    IsThread = tag.IsThread,
                    Avatar = tag.Avatar,
                    Priority = tag.Priority,
                    TagContent = tag.TagContent,
                    TagTitle = tag.TagTitle,
                    TagInit = tag.TagInit,
                    TagMetaContent = tag.TagMetaContent,
                    TagMetaKeyword = tag.TagMetaKeyword,
                    TemplateId = tag.TemplateId,
                    SubTitle = tag.SubTitle,
                    NewsCount = tag.NewsCount,
                    NewsCoverId = tag.NewsCoverId,
                    IsAmp=tag.IsAmp
                };
                tagDetail.TagZone = TagZoneDal.GetTagNewsByTagId(tag.Id);
                tagDetail.TagRelation = Tag_GetRelation(tag.Id);
            }
            return tagDetail;
        }
        public virtual List<TagEntity> GetTagByListOfTagId(string listOfTagId)
        {
            return TagDal.GetTagByListOfId(listOfTagId);
        }
        public virtual List<TagEntity> GetTagByParentTagId(long parentTagId)
        {
            return TagDal.GetTagByParentTagId(parentTagId);
        }
        public virtual ErrorMapping.ErrorCodes InsertTag(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateTagInvalidTagName;
            }
            
            var result= TagDal.Insert(tag, zoneId, zoneIdList, ref newTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if(result== ErrorMapping.ErrorCodes.Success)
            {
                //es
                tag.Id = newTagId;
                if (!BoSearch.Base.Tag.TagDalFactory.AddTag(tag))
                {
                    QueueManager.AddToQueue(ActionName.ADD_TAG,TopicName.ES, tag);
                }

                //redis
                if (!BoCached.Base.Tag.TagDalFactory.AddTag(tag))
                {
                    QueueManager.AddToQueue(ActionName.ADD_TAG, TopicName.REDIS, tag);
                }
            }
            return result;
        }
        public virtual ErrorMapping.ErrorCodes InsertTag(TagEntity tag, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateTagInvalidTagName;
            }
            if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
            return TagDal.Insert(tag, ref newTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual string UpdateTagList(string[] tagNames, int zoneId, string zoneIdList)
        {
            var tagIds = "";
            var tagId = 0L;

            foreach (var tagName in tagNames)
            {
                tagId = 0L;
                if (!TagDal.Insert(new TagEntity
                {
                    Name = tagName,
                    Description = tagName,
                    IsThread = false,
                    UnsignName = Utility.ConvertTextToUnsignName(tagName),
                    Url = Utility.UnicodeToKoDauAndGach(tagName),
                    CreatedDate = DateTime.Now
                }, zoneId, zoneIdList, ref tagId)) continue;
                if (tagId > 0)
                {
                    tagIds += ";" + tagId;
                }
            }
            return !string.IsNullOrEmpty(tagIds) ? tagIds.Substring(1) : "";
        }
        public virtual ErrorMapping.ErrorCodes Update(TagEntity tag, int zoneId, string zoneIdList)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateTagTagNotFound;
            }
            var validTagEntity = TagDal.GetTagByTagName(tag.Name);
            if (null != validTagEntity && validTagEntity.Id != tag.Id && validTagEntity.IsThread == tag.IsThread)
            {
                return ErrorMapping.ErrorCodes.UpdateTagConflictTagName;
            }

            tag.CreatedDate = validTagEntity.CreatedDate;
            var result= TagDal.Update(tag, zoneId, zoneIdList) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if(result== ErrorMapping.ErrorCodes.Success)
            {
                //es               
                if (!BoSearch.Base.Tag.TagDalFactory.UpdateTag(tag))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG, TopicName.ES, tag);
                }

                //redis
                if (!BoCached.Base.Tag.TagDalFactory.AddTag(tag))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG,TopicName.REDIS,tag);
                }
            }

            return result;
        }
        public virtual ErrorMapping.ErrorCodes UpdateViewCountByUrl(int viewCount, string url)
        {
            return TagDal.UpdateViewCountByUrl(viewCount, url) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateListViewCountByUrl(string value)
        {
            return TagDal.UpdateListViewCountByUrl(value) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes DeleteById(long tagId, string username)
        {
            try {
                //check user delete
                var user = UserDal.GetUserByUsername(username);
                if (null != user && user.IsFullPermission && user.IsSystem)
                {
                    var result = TagDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                    if (result == ErrorMapping.ErrorCodes.Success)
                    {
                        //es             
                        if (!BoSearch.Base.Tag.TagDalFactory.DeleteById(tagId))
                        {
                            QueueManager.AddToQueue(ActionName.DELETE_TAG, TopicName.ES,new TagEntity { Id= tagId });
                        }

                        //redis
                        if (!BoCached.Base.Tag.TagDalFactory.DeleteById(tagId))
                        {
                            QueueManager.AddToQueue(ActionName.DELETE_TAG, TopicName.REDIS, new TagEntity { Id = tagId });
                        }
                    }

                    return result;

                }
                return ErrorMapping.ErrorCodes.DeleteTagNotPermission;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public virtual ErrorMapping.ErrorCodes UpdatePriority(long tagId, long priority)
        {
            return TagDal.UpdatePriority(tagId, priority) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateTagHot(long tagId, bool isHotTag)
        {
            var result= TagDal.UpdateTagHot(tagId, isHotTag) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if(result== ErrorMapping.ErrorCodes.Success)
            {
                //es
                if(!BoSearch.Base.Tag.TagDalFactory.UpdateTagHot(tagId, isHotTag))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG_HOT, TopicName.ES, new TagEntity { Id=tagId,IsHotTag=isHotTag});
                }
                //redis
                if(!BoCached.Base.Tag.TagDalFactory.UpdateTagHot(tagId, isHotTag))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG_HOT, TopicName.REDIS, new TagEntity { Id = tagId, IsHotTag = isHotTag });
                }
            }
            return result;
        }
        public virtual ErrorMapping.ErrorCodes UpdateTagInvisibled(long tagId, bool invisibled)
        {
            var result = TagDal.UpdateTagInvisibled(tagId, invisibled) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es
                if(!BoSearch.Base.Tag.TagDalFactory.UpdateTagInvisibled(tagId, invisibled))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG_INVISIBLED, TopicName.ES, new TagEntity { Id = tagId, Invisibled = invisibled });
                }
                //redis
                if(!BoCached.Base.Tag.TagDalFactory.UpdateTagInvisibled(tagId, invisibled))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_TAG_INVISIBLED, TopicName.REDIS, new TagEntity { Id = tagId, Invisibled = invisibled });
                }
            }
            return result;
        }
        public virtual ErrorMapping.ErrorCodes AddNews(string newsIds, long tagId, int tagMode = 1)
        {
            return TagDal.AddNews(newsIds, tagId, tagMode) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateNewsInTagNews(long tagId, string deleteNewsId, string addNewsId, int tagMode = 1)
        {
            var result = TagNewsDal.UpdateNews(tagId, deleteNewsId, addNewsId, tagMode)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                var count = 0;
                if (!string.IsNullOrEmpty(addNewsId))
                {
                    count = addNewsId.Split(';').Length;
                }
                if (!string.IsNullOrEmpty(deleteNewsId))
                {                    
                    count = count - deleteNewsId.Split(';').Length;                   
                }
                //es
                BoSearch.Base.Tag.TagDalFactory.UpdateTagNewsCount(tagId, count);
                //redis
                BoCached.Base.Tag.TagDalFactory.UpdateTagNewsCount(tagId, count);

                //xoa taginnews
                BoCached.Base.Tag.TagDalFactory.RemoveTagInNews(deleteNewsId, addNewsId);
            }
            return result;
        }
        public virtual List<TagWithSimpleFieldEntity> GetTagByListOfTagName(List<string> listTagName)
        {
            var listTag = listTagName.Aggregate("", (current, tagName) => current + ("^" + tagName));
            if (!string.IsNullOrEmpty(listTag)) listTag = listTag.Remove(0, 1);

            return TagDal.GetTagByListOfTagName(listTag);
        }

        public virtual List<TagWithSimpleFieldEntity> GetCleanTagByTagWordCount(int wordCount)
        {
            try
            {
                const int cachedExpired = 10; // In minute

                const string cachedKeyFormat = "CleanTagWithWordCount[{0}]";
                var cachedKey = string.Format(cachedKeyFormat, wordCount);

                var returnCleanTag = HttpContext.Current.Cache.Get(cachedKey) as List<TagWithSimpleFieldEntity>;
                if (returnCleanTag == null)
                {
                    returnCleanTag = TagDal.GetAllCleanTag("", wordCount, wordCount);
                    HttpContext.Current.Cache.Add(cachedKey, returnCleanTag, null, DateTime.Now.AddMinutes(cachedExpired),
                                                  TimeSpan.Zero, CacheItemPriority.High, null);
                }
                return returnCleanTag;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, "GetCleanTagByTagWordCount(" + wordCount + ") error => " + ex);
                return new List<TagWithSimpleFieldEntity>();
            }
        }
        public virtual List<TagWithSimpleFieldEntity> GetCleanTagStartByKeyword(string startByKeyword, int minWordCount, int maxWordCount)
        {
            try
            {
                const int cachedExpired = 10; // In minute

                startByKeyword = startByKeyword.ToLower();

                const string cachedKeyFormat = "CleanTagStartByKeyword[{0}-{1}-{2}]";
                var cachedKey = string.Format(cachedKeyFormat, startByKeyword, minWordCount, maxWordCount);

                var returnCleanTag = HttpContext.Current.Cache.Get(cachedKey) as List<TagWithSimpleFieldEntity>;
                if (returnCleanTag == null)
                {
                    returnCleanTag = TagDal.GetAllCleanTag(startByKeyword, minWordCount, maxWordCount);
                    HttpContext.Current.Cache.Add(cachedKey, returnCleanTag, null, DateTime.Now.AddMinutes(cachedExpired),
                                                  TimeSpan.Zero, CacheItemPriority.High, null);
                }
                return returnCleanTag;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal,
                                "GetCleanTagStartByKeyword(\"" + startByKeyword + "\", " + minWordCount + ", " + maxWordCount +
                                ") error => " + ex);
                return new List<TagWithSimpleFieldEntity>();
            }
        }
        /// <summary>
        /// Lấy dạm sách tag theo bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual List<TagEntity> GetListTagByNewsId(long newsId)
        {
            return TagDal.GetListTagByNewsId(newsId);
        }
        public virtual List<TagWithSimpleFieldEntity> GetListTagAndNewsInfoByNewsId(long newsId, ref int newsStatus, ref string newsUrl, ref int zoneId)
        {
            var returnData = new List<TagWithSimpleFieldEntity>();

            var news = NewsDal.GetNewsInfoForCachedById(newsId);
            if (news != null)
            {
                newsStatus = news.Status;
                newsUrl = news.Url;
                zoneId = news.PrimaryZoneId;

                var listTagNews = TagDal.GetListTagByNewsId(newsId);
                returnData = listTagNews.Select(tagNews => new TagWithSimpleFieldEntity
                {
                    Id = tagNews.Id,
                    Name = tagNews.Name,
                    NewsCount = tagNews.NewsCount,
                    UnsignName = tagNews.UnsignName,
                    Url = tagNews.Url
                }).ToList();
            }
            return returnData;
        }
        public virtual ErrorMapping.ErrorCodes DeleteTagNewsById(string tagName, long newsId)
        {
            return TagDal.DeleteTagNewsById(tagName, newsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateTagNews(long newsId, string tagName)
        {
            return TagDal.UpdateTagNews(newsId, tagName) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<StatisticV3TagVisitEntity> GetTagVisitByDay(string prefix, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListTagViewCount({0}, {1}, {2})", prefix, fromDate, toDate));
                return StatisticV3Dal.GetTagVisitByDay(prefix, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3TagVisitEntity>();
            }
        }
        public virtual ErrorMapping.ErrorCodes RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long newTagId)
        {
            if (string.IsNullOrEmpty(tag.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateTagInvalidTagName;
            }
            if (tag.CreatedDate == DateTime.MinValue) tag.CreatedDate = DateTime.Now;
            return TagDal.RecieveFromCloud(tag, zoneId, zoneIdList, ref newTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        #region Seoer_Insert_Tag
        public static bool Seoer_Insert_Tag(string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            try
            {
                return TagDal.Seoer_Insert_Tag(TagNames, CreatedBy, PrimaryZoneId, OtherZoneId, ref Ids);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return false;
        }
        #endregion

        #region BoxTagSeoEmbed

        public static List<BoxTagSeoEmbedEntity> ListBoxTagSeoEmbed(long tagId, int type)
        {
            var box = new List<BoxTagSeoEmbedEntity>();
            try
            {

                return TagDal.ListBoxTagSeoEmbed(tagId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return box;

        }

        public static ErrorMapping.ErrorCodes InsertBoxTagSeoEmbed(BoxTagSeoEmbedEntity box, ref int id)
        {
            try
            {
                if (null != box)
                {
                    return TagDal.InsertBoxTagSeoEmbed(box, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TagDal.InsertBoxTagSeoEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes UpdateBoxTagSeoEmbed(BoxTagSeoEmbedEntity box)
        {
            try
            {
                if (null != box)
                {
                    var result = TagDal.UpdateBoxTagSeoEmbed(box);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TagDal.UpdateBoxTagSeoEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes DeleteBoxTagSeoEmbed(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = TagDal.DeleteBoxTagSeoEmbed(id);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TagDal.DeleteBoxTagSeoEmbed:{0}", ex.Message));
            }

        }

        #endregion
    }
}
