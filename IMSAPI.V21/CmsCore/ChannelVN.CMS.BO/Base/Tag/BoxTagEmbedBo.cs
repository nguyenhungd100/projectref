﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Tag
{
    public class BoxTagEmbedBo
    {
        #region Tag
        public virtual List<BoxTagEmbedEntity> GetListTagEmbed(int zoneId, int type)
        {
            var tagEmbed = new List<BoxTagEmbedEntity>();
            try
            {

                return BoxTagEmbedDal.GetListTagEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return tagEmbed;

        }
        public virtual ErrorMapping.ErrorCodes Update(string listTagId, int zoneId, int type)
        {
            try
            {
                if (null != listTagId)
                {
                    BoxTagEmbedDal.Update(listTagId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxTagEmbedDal.Update:{0}", ex.Message));
            }
        }
        public virtual ErrorMapping.ErrorCodes UpdateExternalTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            try
            {
                if (null != listTagId)
                {
                    BoxTagEmbedDal.UpdateExternalTag(listTagId, zoneId, type, listTitle, listUrl);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxTagEmbedDal.UpdateExternalTag:{0}", ex.Message));
            }
        }
        #endregion

        #region VideoTag
        public virtual List<BoxTagEmbedEntity> GetListVideoTagEmbed(int zoneId, int type)
        {
            var tagEmbed = new List<BoxTagEmbedEntity>();
            try
            {

                return BoxTagEmbedDal.GetListVideoTagEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return tagEmbed;

        }
        public virtual ErrorMapping.ErrorCodes UpdateVideoTag(string listTagId, int zoneId, int type)
        {
            try
            {
                if (null != listTagId)
                {
                    BoxTagEmbedDal.UpdateVideoTag(listTagId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxTagEmbedDal.Update:{0}", ex.Message));
            }
        }
        public virtual ErrorMapping.ErrorCodes UpdateExternalVideoTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            try
            {
                if (null != listTagId)
                {
                    BoxTagEmbedDal.UpdateExternalVideoTag(listTagId, zoneId, type, listTitle, listUrl);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxTagEmbedDal.UpdateExternalTag:{0}", ex.Message));
            }
        }
        #endregion
    }
}
