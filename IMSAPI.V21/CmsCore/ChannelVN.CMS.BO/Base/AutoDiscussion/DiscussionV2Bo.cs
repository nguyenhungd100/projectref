﻿using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.DiscussionV2.Bo;
using ChannelVN.DiscussionV2.Entity;
using ChannelVN.DiscussionV2.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BO.Base.AutoDiscussion
{
    public class DiscussionV2Bo
    {

        #region Auto Discussion
        /// <summary>
        /// Tự động tạo topic khi tin bài gửi lên chờ biên tập
        /// </summary>
        /// <param name="newsId">long</param>
        /// <param name="topicId">int</param>
        /// <returns>True/false</returns>
        public static ErrorMapping.ErrorCodes AutoTopicWaitEditor(Int64 newsId, string titleNews, string createdBy, ref int topicId)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            try {
                var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));                
                var createSuccess = false;
                if (!configDiscussion)
                {
                    result = ErrorMapping.ErrorCodes.Invalid;
                    return result;
                }
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                if (newsId > 0)
                {
                    #region Danh sách CC User
                    //Lấy tất cả danh sách người cc
                    //var lstTK = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleAdmin, newsId);
                    var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                    string listCCUser = "";

                    if (lstTK.Count > 0)
                    {
                        foreach (var itemCC in lstTK)
                        {
                            listCCUser += itemCC.UserName + ",";
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser.Substring(0, listCCUser.Length - 1);
                    //=========== Kết thúc lấy danh sách người cc
                    string listToUser = "";
                    //var lstBTV = UserBo.GetUserWhoCanManageThisNews(EnumPermission.ArticleEditor, newsId);
                    //if (lstBTV.Count > 0)
                    //{
                    //    string[] arrCCuser = null;
                    //    string tmp = listCCUser;
                    //    if (listCCUser.Length > 0)
                    //    {
                    //        arrCCuser = tmp.Substring(0, tmp.Length - 1).Split(',');
                    //    }
                    //    if (arrCCuser != null && arrCCuser.Length > 0)
                    //    {
                    //        for (int i = 0; i < lstBTV.Count; i++)
                    //        {
                    //            if (!arrCCuser[i].Equals(lstBTV[i].UserName))
                    //            {
                    //                listCCUser += lstBTV[i].UserName + ",";
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        foreach (var itemCC in lstBTV)
                    //        {
                    //            listCCUser += itemCC.UserName + ",";
                    //        }
                    //    }

                    //}
                    #endregion
                    var topicInfo = new DiscussionTopicV2Entity
                    {
                        ObjectId = newsId,
                        Title = newsInfo.Title,
                        Type = (int)DiscussionTopicEnum.News,
                        CreatedBy = createdBy,
                        ListCCUser = listCCUser,
                        ListToUser = createdBy,
                        Content = createdBy + " - đã gửi bài lên chờ biên tập"
                    };
                    var attach = new DiscussionAttachV2Entity
                    {
                        AttachName = newsInfo.Title,
                        AttachUrl = newsId.ToString(),
                        Avatar = newsInfo.Avatar
                    };
                    var listAttach = new List<DiscussionAttachV2Entity>();
                    listAttach.Add(attach);
                    //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó
                    var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                    if (data == null)
                    {
                        data = new DiscussionTopicV2Entity();
                    }
                    if (string.IsNullOrEmpty(data.Title))
                    {
                        DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                    }
                    else
                    {
                        var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                        //Lấy danh sách To user

                        if (discussionAssignList.Count > 0)
                        {
                            foreach (var itemAssign in discussionAssignList)
                            {
                                if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                                    listToUser += itemAssign.UserAssigned + ",";
                            }
                        }

                        if (listToUser.Length > 0)
                            listToUser = listToUser.Substring(0, listToUser.Length - 1);
                        //=========================
                        var discussionInfo = new DiscussionV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            ParentId = 0,
                            CreatedBy = createdBy,
                            Content = createdBy + " - đã gửi bài lên chờ biên tập"
                        };
                        if (listCCUser.Length > 0)
                        {
                            //discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                            discussionInfo.ListReceiver = listToUser;
                            foreach (var tmpItem in listCCUser.Split(','))
                            {
                                if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                                {
                                    discussionInfo.ListReceiver += "," + tmpItem;
                                }
                            }
                        }
                        else
                            discussionInfo.ListReceiver = listToUser;

                        int disId = 0;
                        if (discussionInfo.DiscussionTopicId > 0)
                        {
                            DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                            if (disId > 0)
                            {
                                int Retassign = 0;
                                var assignInfo = new DiscussionAssignV2Entity
                                {
                                    DiscussionTopicId = data.Id,
                                    DiscussionId = disId,
                                    IsRead = true,
                                    Type = (int)DiscussionAssignEnum.To,
                                    UserAssigned = createdBy
                                };
                                DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                                DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                                //Đính kèm Attach
                                if (listAttach != null && listAttach.Count > 0)
                                {
                                    foreach (var item in listAttach)
                                    {
                                        int idAttach = 0;
                                        item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                        item.DiscussionId = disId;
                                        DiscussionAttachV2Bo.Insert(item, ref idAttach);
                                    }
                                }
                            }
                        }
                    }
                    createSuccess = true;
                }
                if (createSuccess)
                    result = ErrorMapping.ErrorCodes.Success;
            }
            catch(Exception ex) {
                Logger.WriteLog(Logger.LogType.Error,ex.Message);
            }

            return result;
        }
        /// <summary>
        /// Tạo topic hoặc gửi discussion khi action => nhận tin bài về biên tập
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="autoFollow"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicEditor(Int64 newsId, string titleNews, string createdBy, ref int topicId, bool autoFollow = true)
        {
            var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = false;
            if (!configDiscussion)
            {
                result = ErrorMapping.ErrorCodes.Invalid;
                return result;
            }
            if (newsId > 0)
            {
                var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                if (data == null)
                {
                    data = new DiscussionTopicV2Entity();
                }
                var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                #region Danh sách cc user
                //Lấy tất cả danh sách người cc (Thư ký mục + Thư ký tòa soạn) nếu autoFollow = true
                string listCCUser = "";
                if (autoFollow)
                {
                    if (discussionAssignList.Count <= 0)
                    {
                        var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                        if (lstTK.Count > 0)
                        {
                            foreach (var itemCC in lstTK)
                            {
                                listCCUser += itemCC.UserName + ",";
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCC in discussionAssignList)
                        {
                            if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                            {
                                listCCUser += itemCC.UserAssigned + ",";
                            }
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                }
                //=========== Kết thúc lấy danh sách người cc
                #endregion

                #region Danh sách To user
                string listToUser = "";

                //Lấy danh sách To user
                if (discussionAssignList.Count > 0)
                {
                    foreach (var itemAssign in discussionAssignList)
                    {
                        if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                            listToUser += itemAssign.UserAssigned + ",";
                    }
                }
                if (listToUser.Length > 0)
                    listToUser = listToUser.Substring(0, listToUser.Length - 1);
                //=========================
                #endregion

                var topicInfo = new DiscussionTopicV2Entity
                {
                    ObjectId = newsId,
                    Title = titleNews,
                    Type = (int)DiscussionTopicEnum.News,
                    CreatedBy = createdBy,
                    ListCCUser = listCCUser,
                    ListToUser = createdBy + ',' + listToUser,
                    Content = createdBy + " - đã nhận tin bài về biên tập"
                };

                var attach = new DiscussionAttachV2Entity
                {
                    AttachName = newsInfo.Title,
                    AttachUrl = newsId.ToString(),
                    Avatar = newsInfo.Avatar
                };
                var listAttach = new List<DiscussionAttachV2Entity>();
                listAttach.Add(attach);
                //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó

                if (string.IsNullOrEmpty(data.Title))
                {
                    DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                }
                else
                {

                    var discussionInfo = new DiscussionV2Entity
                    {
                        DiscussionTopicId = data.Id,
                        ParentId = 0,
                        CreatedBy = createdBy,
                        Content = createdBy + " -  đã nhận tin bài về biên tập"
                    };
                    if (listCCUser.Length > 0)
                    {
                        //discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                        discussionInfo.ListReceiver = listToUser;
                        foreach (var tmpItem in listCCUser.Split(','))
                        {
                            if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                            {
                                discussionInfo.ListReceiver += "," + tmpItem;
                            }
                        }
                    }
                    else
                        discussionInfo.ListReceiver = listToUser;

                    int disId = 0;
                    if (discussionInfo.DiscussionTopicId > 0)
                    {
                        DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                        if (disId > 0)
                        {
                            int Retassign = 0;
                            var assignInfo = new DiscussionAssignV2Entity
                            {
                                DiscussionTopicId = data.Id,
                                DiscussionId = disId,
                                IsRead = true,
                                Type = (int)DiscussionAssignEnum.To,
                                UserAssigned = createdBy
                            };
                            DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                            DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                            //Đính kèm Attach
                            if (listAttach != null && listAttach.Count > 0)
                            {
                                foreach (var item in listAttach)
                                {
                                    int idAttach = 0;
                                    item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                    item.DiscussionId = disId;
                                    DiscussionAttachV2Bo.Insert(item, ref idAttach);
                                }
                            }
                        }
                    }
                }
                createSuccess = true;
            }
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        /// <summary>
        /// Tự động tạo topic hoặc gửi discussion khi tin bài gửi lên chờ xuất bản
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicWaitPublish(Int64 newsId, string titleNews, string createdBy, ref int topicId, bool autoFollow = true)
        {
            var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = false;
            if (!configDiscussion)
            {
                result = ErrorMapping.ErrorCodes.Invalid;
                return result;
            }
            if (newsId > 0)
            {
                var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                if (data == null)
                {
                    data = new DiscussionTopicV2Entity();
                }
                var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                #region Danh sách CC User
                //Lấy tất cả danh sách người cc
                string listCCUser = "";
                if (autoFollow)
                {
                    if (discussionAssignList.Count <= 0)
                    {
                        var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                        if (lstTK.Count > 0)
                        {
                            foreach (var itemCC in lstTK)
                            {
                                listCCUser += itemCC.UserName + ",";
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCC in discussionAssignList)
                        {
                            if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                                listCCUser += itemCC.UserAssigned + ",";
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                }
                //=========== Kết thúc lấy danh sách người cc
                #endregion

                #region Danh sách To User
                //Lấy danh sách To user
                string listToUser = "";
                if (discussionAssignList.Count > 0)
                {
                    foreach (var itemAssign in discussionAssignList)
                    {
                        if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                            listToUser += itemAssign.UserAssigned + ",";
                    }
                }
                if (listToUser.Length > 0)
                    listToUser = listToUser.Substring(0, listToUser.Length - 1);
                //=========================
                #endregion
                var topicInfo = new DiscussionTopicV2Entity
                {
                    ObjectId = newsId,
                    Title = titleNews,
                    Type = (int)DiscussionTopicEnum.News,
                    CreatedBy = createdBy,
                    ListCCUser = listCCUser,
                    ListToUser = createdBy + ',' + listToUser,
                    Content = createdBy + " - đã gửi bài lên chờ xuất bản"
                };
                var attach = new DiscussionAttachV2Entity
                {
                    AttachName = newsInfo.Title,
                    AttachUrl = newsId.ToString(),
                    Avatar = newsInfo.Avatar
                };
                var listAttach = new List<DiscussionAttachV2Entity>();
                listAttach.Add(attach);
                //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó

                if (string.IsNullOrEmpty(data.Title))
                {
                    DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                }
                else
                {
                    var discussionInfo = new DiscussionV2Entity
                    {
                        DiscussionTopicId = data.Id,
                        ParentId = 0,
                        CreatedBy = createdBy,
                        Content = createdBy + " - đã gửi bài lên chờ xuất bản"
                    };
                    if (listCCUser.Length > 0)
                    {
                        //discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                        discussionInfo.ListReceiver = listToUser;
                        foreach (var tmpItem in listCCUser.Split(','))
                        {
                            if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                            {
                                discussionInfo.ListReceiver += "," + tmpItem;
                            }
                        }
                    }
                    else
                        discussionInfo.ListReceiver = listToUser;

                    int disId = 0;
                    DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                    if (disId > 0)
                    {
                        int Retassign = 0;
                        var assignInfo = new DiscussionAssignV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            DiscussionId = disId,
                            IsRead = true,
                            Type = (int)DiscussionAssignEnum.To,
                            UserAssigned = createdBy
                        };
                        DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                        DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                        //Đính kèm Attach
                        if (listAttach != null && listAttach.Count > 0)
                        {
                            foreach (var item in listAttach)
                            {
                                int idAttach = 0;
                                item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                item.DiscussionId = disId;
                                DiscussionAttachV2Bo.Insert(item, ref idAttach);
                            }
                        }
                    }
                }
                createSuccess = true;
            }
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        /// <summary>
        /// tạo topic hoặc gửi disucssion duyệt xuất bản tin bài
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="topicId"></param>
        /// <param name="autoFollow"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicWaitApproval(Int64 newsId, string titleNews, string createdBy, string msg, ref int topicId, bool autoFollow = true)
        {
            var result = ErrorMapping.ErrorCodes.BusinessError;
            try {
                var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
                
                var createSuccess = false;
                if (!configDiscussion)
                {
                    result = ErrorMapping.ErrorCodes.Invalid;
                    return result;
                }
                if (newsId > 0)
                {
                    var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                    if (data == null)
                    {
                        data = new DiscussionTopicV2Entity();
                    }
                    var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                    //lấy thông tin news theo newsId
                    var newsInfo = NewsDal.GetNewsById(newsId);
                    #region Danh sách CC User
                    //Lấy tất cả danh sách người cc
                    string listCCUser = "";
                    if (autoFollow)
                    {
                        if (discussionAssignList.Count <= 0)
                        {
                            var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                            if (lstTK.Count > 0)
                            {
                                foreach (var itemCC in lstTK)
                                {
                                    listCCUser += itemCC.UserName + ",";
                                }
                            }
                        }
                        else
                        {
                            foreach (var itemCC in discussionAssignList)
                            {
                                if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                                    listCCUser += itemCC.UserAssigned + ",";
                            }
                        }
                        if (listCCUser.Length > 0)
                            listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                        //=========== Kết thúc lấy danh sách người cc
                    }
                    #endregion

                    #region Danh sách To User
                    //Lấy danh sách To user
                    string listToUser = "";
                    if (discussionAssignList.Count > 0)
                    {
                        foreach (var itemAssign in discussionAssignList)
                        {
                            if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                                listToUser += itemAssign.UserAssigned + ",";
                        }
                    }
                    if (listToUser.Length > 0)
                        listToUser = listToUser.Substring(0, listToUser.Length - 1);
                    //=========================
                    #endregion
                    var topicInfo = new DiscussionTopicV2Entity
                    {
                        ObjectId = newsId,
                        Title = titleNews,
                        Type = (int)DiscussionTopicEnum.News,
                        CreatedBy = createdBy,
                        ListCCUser = listCCUser,
                        ListToUser = createdBy + ',' + listToUser,
                        // Content = createdBy + " - đã nhận tin bài về duyệt xuất bản"
                        Content = msg
                    };
                    var attach = new DiscussionAttachV2Entity
                    {
                        AttachName = newsInfo.Title,
                        AttachUrl = newsId.ToString(),
                        Avatar = newsInfo.Avatar
                    };
                    var listAttach = new List<DiscussionAttachV2Entity>();
                    listAttach.Add(attach);
                    //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó
                    if (string.IsNullOrEmpty(data.Title))
                    {
                        DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                    }
                    else
                    {
                        var discussionInfo = new DiscussionV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            ParentId = 0,
                            CreatedBy = createdBy,
                            //   Content = createdBy + " -  đã nhận tin bài về duyệt xuất bản"
                            Content = msg
                        };
                        if (listCCUser.Length > 0)
                        {
                            discussionInfo.ListReceiver = listToUser;
                            foreach (var tmpItem in listCCUser.Split(','))
                            {
                                if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                                {
                                    discussionInfo.ListReceiver += "," + tmpItem;
                                }
                            }
                            // + "," + listCCUser;
                        }
                        else
                            discussionInfo.ListReceiver = listToUser;

                        int disId = 0;
                        if (discussionInfo.DiscussionTopicId > 0)
                        {
                            DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                            if (disId > 0)
                            {
                                int Retassign = 0;
                                var assignInfo = new DiscussionAssignV2Entity
                                {
                                    DiscussionTopicId = data.Id,
                                    DiscussionId = disId,
                                    IsRead = true,
                                    Type = (int)DiscussionAssignEnum.To,
                                    UserAssigned = createdBy
                                };
                                DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                                DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                                //Đính kèm Attach
                                if (listAttach != null && listAttach.Count > 0)
                                {
                                    foreach (var item in listAttach)
                                    {
                                        int idAttach = 0;
                                        item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                        item.DiscussionId = disId;
                                        DiscussionAttachV2Bo.Insert(item, ref idAttach);
                                    }
                                }
                            }
                        }
                    }
                    createSuccess = true;
                }
                if (createSuccess)
                    result = ErrorMapping.ErrorCodes.Success;
            }
            catch { }
            return result;
        }
        /// <summary>
        /// Tự động tạo topic hoặc gửi discussion khi tin xuất bản tin bài
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicPublish(Int64 newsId, string titleNews, string createdBy, ref int topicId, bool autoFollow = true)
        {
            var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = false;
            if (!configDiscussion)
            {
                result = ErrorMapping.ErrorCodes.Invalid;
                return result;
            }
            if (newsId > 0)
            {
                var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                if (data == null)
                {
                    data = new DiscussionTopicV2Entity();
                }
                var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                #region Danh sách CC User
                string listCCUser = "";
                if (autoFollow)
                {
                    //Lấy tất cả danh sách người cc
                    if (discussionAssignList.Count <= 0)
                    {
                        var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                        if (lstTK.Count > 0)
                        {
                            foreach (var itemCC in lstTK)
                            {
                                listCCUser += itemCC.UserName + ",";
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCC in discussionAssignList)
                        {
                            if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                                listCCUser += itemCC.UserAssigned + ",";
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                    //=========== Kết thúc lấy danh sách người cc
                }
                #endregion

                #region Danh sách To User
                //Lấy danh sách To user
                string listToUser = "";
                if (discussionAssignList.Count > 0)
                {
                    foreach (var itemAssign in discussionAssignList)
                    {
                        if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                            listToUser += itemAssign.UserAssigned + ",";
                    }
                }
                if (listToUser.Length > 0)
                    listToUser = listToUser.Substring(0, listToUser.Length - 1);
                //=========================
                #endregion
                var topicInfo = new DiscussionTopicV2Entity
                {
                    ObjectId = newsId,
                    Title = titleNews,
                    Type = (int)DiscussionTopicEnum.News,
                    CreatedBy = createdBy,
                    ListCCUser = listCCUser,
                    ListToUser = createdBy + ',' + listToUser,
                    Content = createdBy + " - đã xuất bản tin bài"
                };
                var attach = new DiscussionAttachV2Entity
                {
                    AttachName = newsInfo.Title,
                    AttachUrl = newsId.ToString(),
                    Avatar = newsInfo.Avatar
                };
                var listAttach = new List<DiscussionAttachV2Entity>();
                listAttach.Add(attach);
                //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó

                if (string.IsNullOrEmpty(data.Title))
                {
                    DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                }
                else
                {

                    var discussionInfo = new DiscussionV2Entity
                    {
                        DiscussionTopicId = data.Id,
                        ParentId = 0,
                        CreatedBy = createdBy,
                        Content = createdBy + " - đã xuất bản tin bài"
                    };
                    if (listCCUser.Length > 0)
                    {
                        // discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                        discussionInfo.ListReceiver = listToUser;
                        foreach (var tmpItem in listCCUser.Split(','))
                        {
                            if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                            {
                                discussionInfo.ListReceiver += "," + tmpItem;
                            }
                        }
                    }
                    else
                        discussionInfo.ListReceiver = listToUser;

                    int disId = 0;
                    DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                    if (disId > 0)
                    {
                        int Retassign = 0;
                        var assignInfo = new DiscussionAssignV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            DiscussionId = disId,
                            IsRead = true,
                            Type = (int)DiscussionAssignEnum.To,
                            UserAssigned = createdBy
                        };
                        DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                        DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                        //Đính kèm Attach
                        if (listAttach != null && listAttach.Count > 0)
                        {
                            foreach (var item in listAttach)
                            {
                                int idAttach = 0;
                                item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                item.DiscussionId = disId;
                                DiscussionAttachV2Bo.Insert(item, ref idAttach);
                            }
                        }
                    }
                }
            }
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        /// <summary>
        /// Tự động tạo topic hoặc gửi discussion khi tin gỡ tin bài
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicUnPublish(Int64 newsId, string titleNews, string createdBy, ref int topicId, bool autoFollow = true)
        {
            var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = false;
            if (!configDiscussion)
            {
                result = ErrorMapping.ErrorCodes.Invalid;
                return result;
            }
            if (newsId > 0)
            {
                var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                if (data == null)
                {
                    data = new DiscussionTopicV2Entity();
                }
                var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                #region Danh sách CC User
                string listCCUser = "";
                if (autoFollow)
                {
                    //Lấy tất cả danh sách người cc
                    if (discussionAssignList.Count <= 0)
                    {
                        var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                        if (lstTK.Count > 0)
                        {
                            foreach (var itemCC in lstTK)
                            {
                                listCCUser += itemCC.UserName + ",";
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCC in discussionAssignList)
                        {
                            if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                                listCCUser += itemCC.UserAssigned + ",";
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                    //=========== Kết thúc lấy danh sách người cc
                }
                #endregion

                #region Danh sách To User
                //Lấy danh sách To user
                string listToUser = "";
                if (discussionAssignList.Count > 0)
                {
                    foreach (var itemAssign in discussionAssignList)
                    {
                        if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                            listToUser += itemAssign.UserAssigned + ",";
                    }
                }
                if (listToUser.Length > 0)
                    listToUser = listToUser.Substring(0, listToUser.Length - 1);
                //=========================
                #endregion
                var topicInfo = new DiscussionTopicV2Entity
                {
                    ObjectId = newsId,
                    Title = titleNews,
                    Type = (int)DiscussionTopicEnum.News,
                    CreatedBy = createdBy,
                    ListCCUser = listCCUser,
                    ListToUser = createdBy + ',' + listToUser,
                    Content = createdBy + " - đã gỡ tin bài"
                };
                var attach = new DiscussionAttachV2Entity
                {
                    AttachName = newsInfo.Title,
                    AttachUrl = newsId.ToString(),
                    Avatar = newsInfo.Avatar
                };
                var listAttach = new List<DiscussionAttachV2Entity>();
                listAttach.Add(attach);
                //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó

                if (string.IsNullOrEmpty(data.Title))
                {
                    DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                }
                else
                {

                    var discussionInfo = new DiscussionV2Entity
                    {
                        DiscussionTopicId = data.Id,
                        ParentId = 0,
                        CreatedBy = createdBy,
                        Content = createdBy + " - đã gỡ tin bài"
                    };
                    if (listCCUser.Length > 0)
                    {
                        discussionInfo.ListReceiver = listToUser;
                        foreach (var tmpItem in listCCUser.Split(','))
                        {
                            if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                            {
                                discussionInfo.ListReceiver += "," + tmpItem;
                            }
                        }
                        //discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                    }
                    else
                        discussionInfo.ListReceiver = listToUser;

                    int disId = 0;
                    DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                    if (disId > 0)
                    {
                        int Retassign = 0;
                        var assignInfo = new DiscussionAssignV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            DiscussionId = disId,
                            IsRead = true,
                            Type = (int)DiscussionAssignEnum.To,
                            UserAssigned = createdBy
                        };
                        DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                        DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                        if (listAttach != null && listAttach.Count > 0)
                        {
                            foreach (var item in listAttach)
                            {
                                int idAttach = 0;
                                item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                item.DiscussionId = disId;
                                DiscussionAttachV2Bo.Insert(item, ref idAttach);
                            }
                        }
                    }
                }
            }
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        /// <summary>
        /// Tự động tạo topic hoặc gửi discussion khi tin trả lại tin bài.
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="titleNews"></param>
        /// <param name="createdBy"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoTopicReturn(Int64 newsId, string titleNews, string createdBy, string msg, ref int topicId, bool autoFollow = true)
        {
            var configDiscussion = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "Bo.Discussion"));
            var result = ErrorMapping.ErrorCodes.BusinessError;
            var createSuccess = false;
            if (!configDiscussion)
            {
                result = ErrorMapping.ErrorCodes.Invalid;
                return result;
            }
            if (newsId > 0)
            {
                var data = DiscussionTopicV2Bo.GetTopicByNewsId(newsId);
                if (data == null)
                {
                    data = new DiscussionTopicV2Entity();
                }
                var discussionAssignList = DiscussionAssignV2Bo.GetNewDiscussionAssignByTopicId(data.Id, createdBy);
                //lấy thông tin news theo newsId
                var newsInfo = NewsDal.GetNewsById(newsId);
                #region Danh sách CC User
                string listCCUser = "";
                if (autoFollow)
                {
                    //Lấy tất cả danh sách người cc
                    if (discussionAssignList.Count <= 0)
                    {
                        var lstTK = UserBo.GetUserWithFullPermissionAndZoneId(EnumPermission.ArticleAdmin, newsId, createdBy);
                        if (lstTK.Count > 0)
                        {
                            foreach (var itemCC in lstTK)
                            {
                                listCCUser += itemCC.UserName + ",";
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCC in discussionAssignList)
                        {
                            if (itemCC.Type == (int)DiscussionAssignEnum.CC)
                                listCCUser += itemCC.UserAssigned + ",";
                        }
                    }
                    if (listCCUser.Length > 0)
                        listCCUser = listCCUser.Substring(0, listCCUser.Length - 1);
                    //=========== Kết thúc lấy danh sách người cc
                }
                #endregion

                #region Danh sách To User
                //Lấy danh sách To user
                //Trường hợp này có thể tính tới việc gửi cho người trước đó đã nhận. HIện tại là trả về all.
                string listToUser = "";
                if (discussionAssignList.Count > 0)
                {
                    foreach (var itemAssign in discussionAssignList)
                    {
                        if (itemAssign.Type == (int)DiscussionAssignEnum.To)
                            listToUser += itemAssign.UserAssigned + ",";
                    }
                }
                if (listToUser.Length > 0)
                    listToUser = listToUser.Substring(0, listToUser.Length - 1);
                //=========================
                #endregion
                var topicInfo = new DiscussionTopicV2Entity
                {
                    ObjectId = newsId,
                    Title = titleNews,
                    Type = (int)DiscussionTopicEnum.News,
                    CreatedBy = createdBy,
                    ListCCUser = listCCUser,
                    ListToUser = createdBy + ',' + listToUser,
                    Content = createdBy + " - " + msg + ""
                };
                var attach = new DiscussionAttachV2Entity
                {
                    AttachName = newsInfo.Title,
                    AttachUrl = newsId.ToString(),
                    Avatar = newsInfo.Avatar
                };
                var listAttach = new List<DiscussionAttachV2Entity>();
                listAttach.Add(attach);
                //Kiểm tra topic với newsId đã được tạo hay chưa. Nếu chưa tạo topic với newsId, nếu đã tồn tại thì gửi discussion cho topic đó

                if (data == null && string.IsNullOrEmpty(data.Title))
                {
                    DiscussionTopicV2Bo.Insert(topicInfo, listAttach, ref topicId);
                }
                else
                {

                    var discussionInfo = new DiscussionV2Entity
                    {
                        DiscussionTopicId = data.Id,
                        ParentId = 0,
                        CreatedBy = createdBy,
                        Content = createdBy + " - " + msg + ""
                    };
                    if (listCCUser.Length > 0)
                    {
                        //discussionInfo.ListReceiver = listToUser + "," + listCCUser;
                        discussionInfo.ListReceiver = listToUser;
                        foreach (var tmpItem in listCCUser.Split(','))
                        {
                            if (discussionInfo.ListReceiver.IndexOf(tmpItem) < 0)
                            {
                                discussionInfo.ListReceiver += "," + tmpItem;
                            }
                        }
                    }
                    else
                        discussionInfo.ListReceiver = listToUser;

                    int disId = 0;
                    DiscussionTopicV2Bo.InsertDisscussion(discussionInfo, ref disId);
                    if (disId > 0)
                    {
                        int Retassign = 0;
                        var assignInfo = new DiscussionAssignV2Entity
                        {
                            DiscussionTopicId = data.Id,
                            DiscussionId = disId,
                            IsRead = true,
                            Type = (int)DiscussionAssignEnum.To,
                            UserAssigned = createdBy
                        };
                        DiscussionAssignV2Bo.InsertOnUser(assignInfo, ref Retassign);
                        DiscussionAssignV2Bo.InsertMultiUser(data.Id, disId, listToUser, listCCUser);
                        if (listAttach != null && listAttach.Count > 0)
                        {
                            foreach (var item in listAttach)
                            {
                                int idAttach = 0;
                                item.DiscussionTopicId = discussionInfo.DiscussionTopicId;
                                item.DiscussionId = disId;
                                DiscussionAttachV2Bo.Insert(item, ref idAttach);
                            }
                        }
                    }
                }
            }
            if (createSuccess)
                result = ErrorMapping.ErrorCodes.Success;
            return result;
        }
        #endregion
    }
}
