﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsMobileStream
{
    public class NewsMobileStreamBo
    {

        /// <summary>
        /// CREATED BY: TƯỚNG CƯƠNG
        /// LastModifiedDate: 2015-10-28
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual List<NewsMobileStreamEntity> GetList(int zoneId)
        {
            var newsMobileStreamEntity = new List<NewsMobileStreamEntity>();
            try
            {
                return NewsMobileStreamDal.GetList(zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return newsMobileStreamEntity;
        }

        /// <summary>
        /// CREATED BY: TƯỚNG CƯƠNG
        /// LastModifiedDate: 2015-10-27 14:45
        /// </summary>
        /// <param name="newsMobileStreams"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SaveNewsMobileStream(List<UpdateNewsMobileStreamEntity> newsMobileStreams)
        {
            try
            {
                var count = newsMobileStreams.Count;
                for (var i = 0; i < count; i++)
                {
                    var newsMobileStream = newsMobileStreams[i];
                    NewsMobileStreamDal.SaveNewsMobileStream(newsMobileStream.ZoneId, newsMobileStream.NewsId, newsMobileStream.Status, newsMobileStream.IsHighlight);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine, int newsPositionTypeIdOnMobileFocus)
        {
            try
            {
                return NewsMobileStreamDal.NewsMobileStream_RemoveTopLessView(topRemove, numberOfNewsOnTimeLine, newsPositionTypeIdOnMobileFocus)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsMobileStream_RemoveTopLessView:{0}", ex.Message));
            }
        }
    }
}
