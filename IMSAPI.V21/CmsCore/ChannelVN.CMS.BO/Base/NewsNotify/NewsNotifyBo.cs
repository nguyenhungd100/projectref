﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsNotify;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.AutoDiscussion;

namespace ChannelVN.CMS.BO.Base.NewsNotify
{
    public class NewsNotifyBo
    {
        public static ErrorMapping.ErrorCodes InsertNews(NewsNotifyEntity news)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, "NewsNotify.InsertNews => " + NewtonJson.Serialize(news));
                if (NewsNotifyDal.InsertNews(news))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes Notify_InsertNews(NewsNotifyEntity news)
        {
            try
            {
                if (NewsNotifyDal.Notify_InsertNews(news))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertNewsApp(NewsAppEntity news)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, "NewsNotify.InsertNewsApp => " + NewtonJson.Serialize(news));
                if (NewsNotifyDal.InsertAppNews(news))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

    }
}
