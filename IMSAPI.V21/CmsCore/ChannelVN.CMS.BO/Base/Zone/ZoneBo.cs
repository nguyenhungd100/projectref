﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.MessageQueue;
using ChannelVN.CMS.MessageQueue.Constants;

namespace ChannelVN.CMS.BO.Base.Zone
{
    public class ZoneBo
    {
        public static ZoneEntity GetZoneById(int zoneId)
        {
            try
            {
                var zone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(zoneId);
                if (zone == null)
                {
                    zone= ZoneDal.GetZoneById(zoneId);
                }

                return zone;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ZoneEntity();
            }
        }
        public static ZoneEntity GetZoneDbById(int zoneId)
        {
            try
            {                
                return ZoneDal.GetZoneById(zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new ZoneEntity();
            }
        }

        public static List<ZoneEntity> GetListZoneActiveByParentId(int parentZoneId)
        {
            try
            {
                return ZoneDal.GetZoneActiveByParentId(parentZoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
        public static List<ZoneEntity> GetListZoneByParentId(int parentZoneId)
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneByParentId(parentZoneId);
                if (zones == null || (zones != null && zones.Count == 0))
                {
                    zones = ZoneDal.GetZoneByParentId(parentZoneId);                    
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListZoneActivedByParentId(int parentZoneId)
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneActivedByParentId(parentZoneId);
                if (zones == null || (zones != null && zones.Count == 0))
                {
                    zones = ZoneDal.GetZoneActivedByParentId(parentZoneId);
                }
                return zones;                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListParentZoneByParentId()
        {
            try
            {
                var zones = ZoneDal.GetZoneByParentId(0);
                var simpleZones = new List<ZoneWithSimpleFieldEntity>();
                simpleZones.AddRange(zones.Select(zone => new ZoneWithSimpleFieldEntity()
                                                              {
                                                                  Id = zone.Id,
                                                                  Name = zone.Name,
                                                                  ParentId = zone.ParentId,
                                                                  ShortUrl = zone.ShortUrl,
                                                                  SortOrder = zone.SortOrder,
                                                                  Invisibled = zone.Invisibled,
                                                                  Status = zone.Status
                                                              }));
                return simpleZones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListParentZoneActivedByParentId()
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneActivedByParentId(0);
                if (zones == null || (zones!=null && zones.Count()<=0))
                {
                    zones = ZoneDal.GetZoneActivedByParentId(0);                   
                }
                return zones;                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneEntity> GetAllZone()
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneByParentId(-1);
                if (zones == null || (zones!=null && zones.Count==0))
                {
                    zones = ZoneDal.GetZoneByParentId(-1);
                    if (zones != null)
                    {
                        //init redis
                        BoCached.Base.Zone.ZoneDalFactory.InitAllZone(zones);
                    }
                }

                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetAllZoneActive()
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneByParentId(-1);
                if (zones == null || (zones != null && zones.Count == 0))
                {
                    zones = ZoneDal.GetZoneByParentId(-1);
                    if (zones != null)
                    {
                        //init redis
                        BoCached.Base.Zone.ZoneDalFactory.InitAllZone(zones);
                    }
                }

                return zones.Where(z=>z.Status==1)?.ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetPrivateZone(string username,int parentZoneId)
        {            
            try
            {          
                var zones= GetListZoneActivedByUsernameAndPermissionIds(username, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin, (int)EnumPermission.ArticleAdminEditor);
                if (parentZoneId != -1)
                {
                    zones= zones.Where(s => s.ParentId == parentZoneId).ToList();
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllZone(string name, DateTime startDate, DateTime endDate, int pageSize,string action=null)
        {                                
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = ZoneDal.GetZoneByParentId(-1);
                //init redis
                BoCached.Base.Zone.ZoneDalFactory.InitAllZone(data);
            }, action);            
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActived()
        {
            try
            {
                return ZoneDal.GetZoneActivedByParentId(-1);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneEntity> GetListZoneByUserId(int userId)
        {
            try
            {
                var user = UserDal.GetUserById(userId);
                if (null != user)
                {
                    return (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(user.UserName, (int)EnumPermission.ZoneManager) || user.IsFullZone) ? ZoneDal.GetZoneByParentId(-1) : ZoneDal.GetZoneByUserId(userId);                    
                }
                return new List<ZoneEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            try
            {
                //var data = new List<ZoneEntity>();
                //data = BoCached.Base.Zone.ZoneDalFactory.GetZoneByNewsId(newsId);
                //if (data == null || (data!=null && data.Count()<=0))
                //{
                //    data= ZoneDal.GetZoneByNewsId(newsId);                    
                //}

                return ZoneDal.GetZoneByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetListZoneByUsername(string username)
        {
            try
            {
                var user = UserBo.GetUserByUsername(username);
                if (null != user)
                {
                    return (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || user.IsFullZone) ? GetAllZone() : GetZoneByUserId(user.Id);                    
                }
                return new List<ZoneEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetZoneByUserId(int id)
        {
            try
            {
                var zones = BoCached.Base.Zone.ZoneDalFactory.GetZoneByUserId(id);
                if (zones == null || (zones != null && zones.Count <= 0))
                {
                    zones= ZoneDal.GetZoneByUserId(id);
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetListZoneByUsernameAndPermissionId(string username, int permissionId)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (null != user)
                {
                    return (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || user.IsFullZone)
                               ? ZoneDal.GetZoneByParentId(-1)
                               : ZoneDal.GetZoneByUserIdAndPermissionId(user.Id, permissionId);                                        
                }
                return new List<ZoneEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneEntity> GetListZoneByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                if (!String.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);
                return ZoneDal.GetZoneByUsernameAndPermissionIds(username, listPermissionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetListZoneActivedByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

                var allZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneActivedByUsernameAndPermissionIds(username, listPermissionId);
                if (allZone == null || (allZone!=null && allZone.Count()<=0))
                {
                    allZone= ZoneDal.GetZoneActivedByUsernameAndPermissionIds(username, listPermissionId);
                    if(allZone!=null && allZone.Count() > 0)
                    {
                        //BoCached.Base.Zone.ZoneDalFactory.AddZoneActivedByUsernameAndPermissionIds(newsId, allZone);
                    }
                }

                return allZone;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneWithSimpleFieldEntity>();
            }
        }

        public static List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            try
            {
                return ZoneDal.GetZoneByKeyword(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes Insert(ZoneEntity zoneEntity, string username)
        {
            try
            {
                //var user = UserDal.GetUserByUsername(username);
                var user = UserDataBo.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    var id = ZoneDal.GetMaxIdZone() + 1;
                    zoneEntity.Id = id;

                    if (ZoneDal.Insert(zoneEntity))
                    {
                        if (!BoCached.Base.Zone.ZoneDalFactory.AddZone(zoneEntity))
                        {
                            QueueManager.AddToQueue(ActionName.ADD_ZONE,TopicName.REDIS, zoneEntity);
                        }
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertZoneDB(ZoneEntity zoneEntity, string username)
        {
            try
            {
                ////var user = UserDal.GetUserByUsername(username);
                //var user = UserDataBo.GetUserByUsername(username);
                //if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                //{
                    //var id = ZoneDal.GetMaxIdZone() + 1;
                    //zoneEntity.Id = id;

                    if (ZoneDal.InsertInitDB(zoneEntity))
                    {
                        BoCached.Base.Zone.ZoneDalFactory.AddZone(zoneEntity);
                    }
                    return ErrorMapping.ErrorCodes.Success;

                //}
                //return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(ZoneEntity zoneEntity, string username)
        {
            try
            {
                //var user = UserDal.GetUserByUsername(username);
                var user = UserDataBo.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    if (ZoneDal.Update(zoneEntity))
                    {
                        if (!BoCached.Base.Zone.ZoneDalFactory.AddZone(zoneEntity))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_ZONE, TopicName.REDIS, zoneEntity);
                        }
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes MoveUp(int zoneId, string username)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    ZoneDal.MoveUp(zoneId);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes MoveDown(int zoneId, string username)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    ZoneDal.MoveDown(zoneId);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ToggleZoneInvisibled(int zoneId, string username)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    if (ZoneDal.ToggleZoneInvisibled(zoneId))
                    {
                        if (!BoCached.Base.Zone.ZoneDalFactory.ToggleZoneInvisibled(zoneId))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_ZONE_INVISIBLED, TopicName.REDIS, new ZoneEntity { Id=zoneId});
                        }
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ToggleZoneStatus(int zoneId, string username)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    if (ZoneDal.ToggleZoneStatus(zoneId))
                    {
                        if (!BoCached.Base.Zone.ZoneDalFactory.ToggleZoneStatus(zoneId))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_ZONE_STATUS, TopicName.REDIS, new ZoneEntity { Id = zoneId });
                        }
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ToggleZoneAllowComment(int zoneId, string username)
        {
            try
            {
                var user = UserDal.GetUserByUsername(username);
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ZoneManager) || (null != user && user.IsFullPermission && user.IsFullZone))
                {
                    if (ZoneDal.ToggleZoneAllowComment(zoneId))
                    {
                        if (!BoCached.Base.Zone.ZoneDalFactory.ToggleZoneAllowComment(zoneId))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_ZONE_COMMENT, TopicName.REDIS, new ZoneEntity { Id = zoneId });
                        }
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }        

        public static List<ZoneWithSimpleFieldEntity> BindAllOfZoneToTreeviewFullDepth(List<ZoneWithSimpleFieldEntity> allZones, string prefix)
        {
            allZones.OrderBy(item => item.ParentId).ThenBy(item => item.SortOrder);
            var outputZones = new List<ZoneWithSimpleFieldEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZones.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        public static List<ZoneWithSimpleFieldEntity> BindAllOfZoneToTreeviewFullDepth(List<ZoneEntity> allZones, string prefix)
        {
            var allZonesWithSimpleField = new List<ZoneWithSimpleFieldEntity>();
            var count = allZones.Count;
            foreach (var zone in allZones)
            {
                allZonesWithSimpleField.Add(new ZoneWithSimpleFieldEntity
                {
                    Id = zone.Id,
                    ParentId = zone.ParentId,
                    Name = zone.Name,
                    ShortUrl = zone.ShortUrl,
                    SortOrder = zone.SortOrder,
                    AllowComment = zone.AllowComment,
                    Status = zone.Status,
                    Invisibled = zone.Invisibled,
                    Domain = zone.Domain
                });
            }

            allZonesWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.SortOrder);
            var outputZones = new List<ZoneWithSimpleFieldEntity>();
            BindAllOfZoneToTreeviewFullDepth(allZonesWithSimpleField.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        private static void BindAllOfZoneToTreeviewFullDepth(List<ZoneWithSimpleFieldEntity> allZones, int currentParentId, string prefix, ref List<ZoneWithSimpleFieldEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentId == currentParentId)
                {
                    allZones[i].Name = currentPrefix + " " + allZones[i].Name;
                    outputZones.Add(allZones[i]);
                    BindAllOfZoneToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeView(string username)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneByUsername(username), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeViewAndPermission(string username, int permissionId)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByUsernameAndPermissionIds(username, permissionId), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneByUserWithTreeViewAndPermission(string username, params int[] permissionIds)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneByUsernameAndPermissionIds(username, permissionIds), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActivedByUserWithTreeViewAndPermission(string username, params int[] permissionIds)
        {
            return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByUsernameAndPermissionIds(username, permissionIds), "--");
        }

        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActiveWithTreeView(bool getParentZoneOnly)
        {
            if (getParentZoneOnly)
            {
                return BindAllOfZoneToTreeviewFullDepth(GetListZoneActiveByParentId(0), "--");
            }
            else
            {
                return BindAllOfZoneToTreeviewFullDepth(GetAllZone(), "--");
            }
        }
        public static List<ZoneWithSimpleFieldEntity> GetAllZoneWithTreeView(bool getParentZoneOnly)
        {
            if (getParentZoneOnly)
            {
                return BindAllOfZoneToTreeviewFullDepth(GetListZoneByParentId(0), "--");
            }
            else
            {
                return BindAllOfZoneToTreeviewFullDepth(GetAllZone(), "--");
            }
        }
        public static List<ZoneWithSimpleFieldEntity> GetAllZoneActivedWithTreeView(bool getParentZoneOnly)
        {
            if (getParentZoneOnly)
            {
                return BindAllOfZoneToTreeviewFullDepth(GetListZoneActivedByParentId(0), "--");
            }
            else
            {
                return BindAllOfZoneToTreeviewFullDepth(GetAllZoneActived(), "--");
            }
        }

        public static List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {
            return ZoneDefaultTagDal.GetAllDefaultTagForZone();
        }

        public static List<ZoneEntity> GetGroupNewsZoneByParentId(int zoneId)
        {
            try
            {
                return ZoneDal.GetGroupNewsZoneByParentId(zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneEntity>();
            }
        }
    }
}
