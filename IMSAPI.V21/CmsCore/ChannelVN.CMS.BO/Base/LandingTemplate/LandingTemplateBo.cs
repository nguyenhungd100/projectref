﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.LandingTemplate;
using ChannelVN.CMS.Entity.Base.LandingTemplate;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BO.Base.LandingTemplate
{
    public class LandingTemplateBo
    {
        #region LandingTemplate
        public static List<LandingTemplateEntity> Search(string keyword, int categoryId, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return LandingTemplateDal.Search(keyword, categoryId, status, type, pageIndex, pageSize, ref totalRow);
        }
        public static LandingTemplateEntity GetById(int id)
        {
            var LandingTemplate = LandingTemplateDal.GetById(id);            
            return LandingTemplate;
        }        
        public static ErrorMapping.ErrorCodes Create(LandingTemplateEntity obj, ref int id)
        {
            if (obj == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
                        
            if (LandingTemplateDal.Insert(obj, ref id))
            {                
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Update(LandingTemplateEntity obj)
        {
            if (obj == null || obj.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
                        
            if (LandingTemplateDal.Update(obj))
            {                
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            if (id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (LandingTemplateDal.Delete(id))
                return ErrorMapping.ErrorCodes.Success;

            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateStatus(int id, string action)
        {
            try {
                if (id <= 0)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }

                var errorCode = ErrorMapping.ErrorCodes.BusinessError;
                switch (action?.Trim().ToLower())
                {
                    case "temporary":
                        errorCode = LandingTemplateDal.UpdateStatus(id, (int)EnumStatusLandingTemplate.Temporary) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                        break;
                    case "published":
                        errorCode = LandingTemplateDal.UpdateStatus(id, (int)EnumStatusLandingTemplate.Published) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                        break;
                    case "UnPublished":
                        errorCode = LandingTemplateDal.UpdateStatus(id, (int)EnumStatusLandingTemplate.UnPublished) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                        break;
                    default:
                        errorCode = ErrorMapping.ErrorCodes.BusinessError;
                        break;
                }

                return errorCode;

            }catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        #endregion

        #region LandingTemplateCategory
        public static List<LandingTemplateCategoryEntity> GetAllCategory()
        {
            return LandingTemplateDal.GetAllCategory();
        }
        public static LandingTemplateCategoryEntity GetCategoryById(int id)
        {            
            return LandingTemplateDal.GetCategoryById(id);
        }
        public static ErrorMapping.ErrorCodes CreateCategory(LandingTemplateCategoryEntity obj, ref int id)
        {
            if (obj == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

            if (LandingTemplateDal.InsertCategory(obj, ref id))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateCategory(LandingTemplateCategoryEntity obj)
        {
            if (obj == null || obj.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
            if (LandingTemplateDal.UpdateCategory(obj))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteCategory(int id)
        {
            if (id <= 0)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            if (LandingTemplateDal.DeleteCategory(id))
                return ErrorMapping.ErrorCodes.Success;

            return ErrorMapping.ErrorCodes.BusinessError;
        }       
        #endregion
    }
}
