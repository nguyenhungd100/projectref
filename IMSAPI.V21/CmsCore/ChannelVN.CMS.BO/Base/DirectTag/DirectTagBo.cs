﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.DAL.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.BO.Base.DirectTag
{
    public class DirectTagBo
    {
        public static ErrorMapping.ErrorCodes InsertDirectTag(ref int tagId, string tagName, string tagLink, string quoteFormat, int tagType)
        {
            var data = DirectTagDal.InsertDirectTag(ref tagId, tagName, tagLink, quoteFormat, tagType);
            if (data == true)
            {                                
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateDirectTag(int tagId, string tagName, string tagLink, string quoteFormat, int tagType)
        {
            var data = DirectTagDal.UpdateDirectTag(tagId, tagName, tagLink, quoteFormat, tagType);
            if (data == true)
            {                
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteDirectTag(string tagIds)
        {
            var data = DirectTagDal.DeleteDirectTag(tagIds);
            if (data == true)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertDirectTagNewsMulti(string tagIds, string newsIds, string account)
        {
            var data = DirectTagDal.InsertDirectTagNewsMulti(tagIds, newsIds);
            if (data == true)
            {
                //log action sua seo tag for news
                ActivityBo.LogInsertDirectTagNews(tagIds, newsIds, 0, account);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertDirectTagNewsMultiByType(string tagIds, string newsIds, int type, string account)
        {
            var data = DirectTagDal.InsertDirectTagNewsMultiByType(tagIds, newsIds, type);
            if (data == true)
            {
                //log action sua seo tag for news
                ActivityBo.LogInsertDirectTagNews(tagIds, newsIds, type, account);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteDirectTagNewsFromNews(int tagId, long newsId, int type, string account)
        {
            var data = DirectTagDal.DeleteDirectTagNewsFromNews(tagId, newsId, type);
            if (data == true)
            {
                //log action sua seo tag for news
                ActivityBo.LogDeleteDirectTagNews(tagId, newsId, type, account);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<DirectTagEntity> SelectAll(int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagDal.SelectAll(pageIndex, pageSize, ref totalRow);
        }

        public static List<DirectTagEntity> SelectAllByType(int pageIndex, int pageSize, int type, ref int totalRow)
        {
            return DirectTagDal.SelectAllByType(pageIndex, pageSize, type, ref totalRow);
        }

        public static List<DirectTagEntity> SearchDirectTag(string keyword, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagDal.SearchDirectTag(keyword, type, pageIndex, pageSize, ref totalRow);
        }

        public static DirectTagEntity IsTagAvailable(string tagName)
        {
            return DirectTagDal.IsTagAvailable(tagName);
        }

        public static List<DirectTagEntity> SelectDirectTagByNewsId(Int64 newsId)
        {
            return DirectTagDal.SelectDirectTagByNewsId(newsId);
        }

        public static List<NewsPublishForSearchEntity> SearchNewsPublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagDal.SearchNewsPublish(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public static List<NewsPublishForSearchEntity> SearchNewsPublish_DirectTag_LessThan_3Tag(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return DirectTagDal.SearchNewsPublish_DirectTag_LessThan_3Tag(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
    }
}
