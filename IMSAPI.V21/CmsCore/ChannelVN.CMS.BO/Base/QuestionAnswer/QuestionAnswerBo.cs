﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.QuestionAnswer;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.GroupQuestion;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.AutoDiscussion;

namespace ChannelVN.CMS.BO.Base.QuestionAnswer
{
    public class QuestionAnswerBo
    {
        public static ErrorMapping.ErrorCodes InsertQuestion(QuestionEntity news, ref int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.InsertQuestion(news, ref id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertQuestionInEvent(int questionId, int eventId)
        {
            try
            {
                var inserted = QuestionAnswerDal.InsertQuestionInEvent(questionId, eventId);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes InsertEventQuestion(EventQuestionEntity news, ref int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.InsertEventQuestion(news, ref id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes InsertGroupQuestion(GroupQuestionEntity news, ref int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.InsertGroupQuestion(news, ref id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateEventQuestion(EventQuestionEntity news)
        {
            try
            {
                var inserted = QuestionAnswerDal.UpdateEventQuestion(news);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateGroupQuestion(GroupQuestionEntity news)
        {
            try
            {
                var inserted = QuestionAnswerDal.UpdateGroupQuestion(news);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteGroupQuestion(int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.DeleteGroupQuestion(id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteEventQuestion(int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.DeleteEventQuestion(id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertAnswer(AnswerEntity news, ref int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.InsertAnswer(news, ref id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteQuestion(int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.DeleteQuestion(id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteAnswer(int id)
        {
            try
            {
                var inserted = QuestionAnswerDal.DeleteAnswer(id);

                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateQuestion(QuestionEntity news)
        {
            try
            {
                var result = QuestionAnswerDal.UpdateQuestion(news);
                if (result)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            return ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateAnswer(AnswerEntity news)
        {
            try
            {
                var result = QuestionAnswerDal.UpdateAnswer(news);
                if (result)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            return ErrorMapping.ErrorCodes.UnknowError;
        }
        public static List<GroupQuestionEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<GroupQuestionEntity>();
            try
            {
                return QuestionAnswerDal.Search(keyword, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<EventQuestionEntity> SearchEvent(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<EventQuestionEntity>();
            try
            {
                return QuestionAnswerDal.SearchEvent(keyword, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<QuestionEntity> GetListQuestionByGroup(string keyword, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<QuestionEntity>();
            try
            {
                return QuestionAnswerDal.GetListQuestionByGroup(keyword, groupId, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<QuestionEntity> GetListQuestionByGroupEvent(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<QuestionEntity>();
            try
            {
                return QuestionAnswerDal.GetListQuestionByGroupEvent(keyword, groupId, eventId, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<QuestionEntity> GetListQuestionBefore(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<QuestionEntity>();
            try
            {
                return QuestionAnswerDal.GetListQuestionBefore(keyword, groupId, eventId, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<AnswerEntity> GetListAnswer(int questionId)
        {
            var newsList = new List<AnswerEntity>();
            try
            {
                return QuestionAnswerDal.GetListAnswer(questionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static QuestionEntity GetQuestionById(int questionId)
        {
            var newsList = new QuestionEntity();
            try
            {
                return QuestionAnswerDal.GetQuestionById(questionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static GroupQuestionEntity GetGroupQuestionById(int questionId)
        {
            var newsList = new GroupQuestionEntity();
            try
            {
                return QuestionAnswerDal.GetGroupQuestionById(questionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static EventQuestionEntity GetEventQuestionById(int questionId)
        {
            var newsList = new EventQuestionEntity();
            try
            {
                return QuestionAnswerDal.GetEventQuestionById(questionId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
    }
}
