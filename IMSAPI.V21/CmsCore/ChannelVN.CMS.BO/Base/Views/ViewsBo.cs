﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.BO.Base.Views
{
    public class ViewsBo
    {
        #region Views

        public static List<NodeJs_ViewsEntity> SortViews(int pageIndex, int pageSize, string newsIds, string sort, int skipSort, long viewFrom, long viewTo, ref int totalRow)
        {
            //call nodejs
            return ViewsNodeJsServices.SortViews(pageIndex, pageSize, newsIds, sort, skipSort, viewFrom, viewTo, ref totalRow);
        }

        #endregion

    }
}
