﻿using ChannelVN.CMS.DAL.Base.NewsMaskOnline;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BO.Base.NewsMaskOnline
{
    public class NewsMaskOnlineBo
    {
        public static List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                   EnumMaskSourceId SourceID, int status, int pageIndex, int pageSize, int Mode = -1)
        {
            return NewsMaskOnlineDal.ListNewsMaskOnline(keyword, fromDate, toDate, (int)SourceID, status, pageIndex, pageSize, Mode);
        }
    }
}
