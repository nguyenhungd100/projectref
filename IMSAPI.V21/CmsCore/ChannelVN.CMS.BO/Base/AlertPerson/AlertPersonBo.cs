﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.AlertPerson;
using ChannelVN.CMS.DAL.Base.Statistic;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BO.Base.AlertPerson
{
    public class AlertPersonBo
    {
        public virtual List<AlertPersonEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return AlertPersonDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<AlertPersonEntity>();
        }
        public virtual AlertPersonEntity GetAlertPersonById(int personId)
        {
            var person = AlertPersonDal.GetAlertPersonById(personId);
            return person;
        }
        public virtual ErrorMapping.ErrorCodes InsertTag(AlertPersonEntity tag, ref int newTagId)
        {
            return AlertPersonDal.Insert(tag, ref newTagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes Update(AlertPersonEntity tag)
        {
            return AlertPersonDal.Update(tag) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes DeleteById(int tagId)
        {
            return AlertPersonDal.DeleteById(tagId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }


    }
}
