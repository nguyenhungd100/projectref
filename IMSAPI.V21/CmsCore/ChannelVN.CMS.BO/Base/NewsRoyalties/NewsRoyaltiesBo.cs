﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsRoyalties;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.NewsRoyalties
{
    public class NewsRoyaltiesBo
    {
        #region Get

        public static List<NewsRoyaltiesEntity> GetByNewsId(long newsId)
        {
            return NewsRoyaltiesDal.GetByNewsId(newsId);
        }

        public static List<NewsRoyaltiesEntity> SearchRoyalties(string userName, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            if (fromDate > DbCommon.MinDateTime)
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            if (toDate > DbCommon.MinDateTime)
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

            var listRoyalties = NewsRoyaltiesDal.Search(userName, royaltiesCategoryId, royaltiesRoleId,
                                                    keyword,
                                                    royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            return listRoyalties;
        }

        public static NewsRoyaltiesEntity GetById(long Id)
        {
            return NewsRoyaltiesDal.GetById(Id);
        }

        #endregion

        public static ErrorMapping.ErrorCodes InsertRoyalties(NewsRoyaltiesEntity royalties)
        {
            royalties.CreatedBy = WcfMessageHeader.Current.ClientUsername;
            var newRoyaltiesId = 0L;

            var result = NewsRoyaltiesDal.Insert(royalties, ref newRoyaltiesId);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyalties(NewsRoyaltiesEntity royalties)
        {
            var currentRoyalties = NewsRoyaltiesDal.GetById(royalties.Id);
            if (currentRoyalties == null) return ErrorMapping.ErrorCodes.BusinessError;

            currentRoyalties.CategoryId = royalties.CategoryId;
            currentRoyalties.CreatedBy = royalties.CreatedBy;
            currentRoyalties.CreatedDate = royalties.CreatedDate;
            currentRoyalties.Id = royalties.Id;
            currentRoyalties.LastModifiedBy = royalties.LastModifiedBy;
            currentRoyalties.LastModifiedDate = royalties.LastModifiedDate;
            currentRoyalties.LevelId = royalties.LevelId;
            currentRoyalties.MediaId = royalties.MediaId;
            currentRoyalties.NewsId = royalties.NewsId;
            currentRoyalties.Note = royalties.Note;
            currentRoyalties.NumberOfMedia = royalties.NumberOfMedia;
            currentRoyalties.PenName = royalties.PenName;
            currentRoyalties.RoleId = royalties.RoleId;
            currentRoyalties.Status = 1;
            currentRoyalties.Username = royalties.Username;
            currentRoyalties.Value = royalties.Value;

            var result = NewsRoyaltiesDal.Update(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesValue(NewsRoyaltiesEntity royalties)
        {
            var currentRoyalties = NewsRoyaltiesDal.GetById(royalties.Id);
            if (currentRoyalties == null) return ErrorMapping.ErrorCodes.BusinessError;

            currentRoyalties.Id = royalties.Id;
            currentRoyalties.LastModifiedBy = royalties.LastModifiedBy;
            currentRoyalties.Value = royalties.Value;

            var result = NewsRoyaltiesDal.Update(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesMediaNumber(NewsRoyaltiesEntity royalties)
        {
            var currentRoyalties = NewsRoyaltiesDal.GetById(royalties.Id);
            if (currentRoyalties == null) return ErrorMapping.ErrorCodes.BusinessError;

            currentRoyalties.Id = royalties.Id;
            currentRoyalties.LastModifiedBy = royalties.LastModifiedBy;
            currentRoyalties.NumberOfMedia = royalties.NumberOfMedia;

            var result = NewsRoyaltiesDal.Update(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesLevelId(NewsRoyaltiesEntity royalties)
        {
            var currentRoyalties = NewsRoyaltiesDal.GetById(royalties.Id);
            if (currentRoyalties == null) return ErrorMapping.ErrorCodes.BusinessError;

            currentRoyalties.Id = royalties.Id;
            currentRoyalties.LastModifiedBy = royalties.LastModifiedBy;
            currentRoyalties.LevelId = royalties.LevelId;

            var result = NewsRoyaltiesDal.Update(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteRoyalties(int royaltiesId)
        {
            return NewsRoyaltiesDal.Delete(royaltiesId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateListRoyalties(long newsId, List<NewsRoyaltiesEntity> listRoyalties)
        {
            try
            {
                foreach (var royalty in listRoyalties)
                {
                    royalty.NewsId = newsId;
                    royalty.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                    royalty.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;

                    if (royalty.Id > 0)
                    {
                        UpdateRoyalties(royalty);
                    }
                    else
                    {
                        InsertRoyalties(royalty);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
