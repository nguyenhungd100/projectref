﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.NewsRoyalties;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsRoyalties
{
    public class NewsRoyaltiesRoleBo
    {
        #region Get

        public static List<NewsRoyaltiesRoleEntity> GetAllNewsRoyaltiesRole()
        {
            return NewsRoyaltiesRoleDal.GetAll();
        }

        #endregion
    }
}
