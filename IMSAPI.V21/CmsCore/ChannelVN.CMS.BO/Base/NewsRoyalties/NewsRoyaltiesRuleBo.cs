﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsRoyalties
{
    public class NewsRoyaltiesRuleBo
    {
        #region Get

        public static List<NewsRoyaltiesRuleEntity> GetListRoleLevel()
        {
            return NewsRoyaltiesRuleDal.GetListRoleLevel();
        }

        public static NewsRoyaltiesRuleEntity GetValueByRoleLevelCategoryId(int roleId, int levelId, int categoryId)
        {
            return NewsRoyaltiesRuleDal.GetValueByRoleLevelCategoryId(roleId, levelId, categoryId);
        }

        public static NewsRoyaltiesRuleEntity GetValueByRoleLevelMediaId(int roleId, int levelId, int mediaId)
        {
            return NewsRoyaltiesRuleDal.GetValueByRoleLevelMediaId(roleId, levelId, mediaId);
        }

        public static List<NewsRoyaltiesRuleEntity> GetAll()
        {
            return NewsRoyaltiesRuleDal.GetAll();
        }

        #endregion

        public static ErrorMapping.ErrorCodes UpdateValueByRoleLevelCategoryId(NewsRoyaltiesRuleEntity royalties)
        {
            var currentRoyalties = new NewsRoyaltiesRuleEntity();
            currentRoyalties.LevelId = royalties.LevelId;
            currentRoyalties.CategoryId = royalties.CategoryId;
            currentRoyalties.RoleId = royalties.RoleId;
            currentRoyalties.Value = royalties.Value;

            var result = NewsRoyaltiesRuleDal.UpdateValueByRoleLevelCategoryId(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateValueByRoleLevelMediaId(NewsRoyaltiesRuleEntity royalties)
        {
            var currentRoyalties = new NewsRoyaltiesRuleEntity();
            currentRoyalties.MediaId = royalties.MediaId;
            currentRoyalties.LevelId = royalties.LevelId;
            currentRoyalties.RoleId = royalties.RoleId;
            currentRoyalties.Value = royalties.Value;

            var result = NewsRoyaltiesRuleDal.UpdateValueByRoleLevelMediaId(currentRoyalties);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
