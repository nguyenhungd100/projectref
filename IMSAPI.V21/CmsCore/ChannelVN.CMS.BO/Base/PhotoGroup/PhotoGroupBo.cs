﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.PhotoGroup;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.PhotoGroup
{
    public class PhotoGroupBo
    {
        #region PhotoGroupDetail

        #region Update
        public static ErrorMapping.ErrorCodes InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity photo)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoGroupDal.InsertPhotoGroupDetailInPhotoTag(photo) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes InsertPhotoGroupDetail(PhotoGroupDetailEntity photo, ref int newPhotoId)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoGroupDal.InsertPhotoGroupDetail(photo, ref newPhotoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoGroupDetail(PhotoGroupDetailEntity photo)
        {
            try
            {
                if (null == photo)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoGroupDal.UpdatePhotoGroupDetail(photo) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status)
        {
            try
            {
                if (null == id)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return PhotoGroupDal.UpdatePhotoGroupDetailStatus(id, status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoGroupDetail(int photoId)
        {
            try
            {
                return PhotoGroupDal.DeletePhotoGroupDetail(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeletePhotoGroupDetailInPhotoTag(int photoId)
        {
            try
            {
                return PhotoGroupDal.DeletePhotoGroupDetailInPhotoTag(photoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static PhotoGroupDetailEntity GetPhotoGroupDetailById(int id)
        {
            try
            {
                var photogroup = PhotoGroupDal.GetPhotoGroupDetailById(id);
                if (photogroup != null)
                    photogroup.ListPhotoGroupZone = PhotoGroupDal.PhotoGroupDetailInZone_GetAll(id);
                return photogroup;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<PhotoGroupDetailEntity> SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int photoGroupId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return PhotoGroupDal.SearchPhotoGroupDetail(keyword, zoneId, fromDate, toDate, createdBy, status, isHot, photoGroupId, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<PhotoGroupDetailCountEntity> CountPhotoGroupDetail(string username)
        {
            try
            {
                return PhotoGroupDal.CountPhotoGroupDetail(username);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<PhotoGroupEntity> GetAllPhotoGroup(ref int totalRow)
        {
            try
            {
                return PhotoGroupDal.GetAllPhotoGroup(ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion

        #endregion
    }
}
