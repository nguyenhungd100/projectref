﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.Interview;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.RedisClientHelper;
using System;
using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Entity.Base.News;
using System.Linq;

namespace ChannelVN.CMS.BO.Base.Interview
{
    public class InterviewV3Bo
    {
        #region InterviewV3

        #region GET
        public static NodeJs_InterviewEntity InterviewV3GetById(string Id)
        {
            //return InterviewV3Dal.InterviewV3GetById(Id);
            //goi nodejs
            return InterviewNodeJsServices.Interview_GetById(Id);
        }

        public static List<NodeJs_InterviewEntity> SearchInterviewV3(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            //call nodejs
            return InterviewNodeJsServices.Interview_Search(keyword, pageIndex, pageSize, ref totalRow);
        }

        public static List<NodeJs_InterviewEntity2> ListInterviewByIds(string ids)
        {
            //lay id nodejs trong extension to redis
            var newsIds = new List<string>();
            newsIds.AddRange(ids.Split(','));
            var data = BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(newsIds);
            var node_ids = new List<NewsExtensionEntity>();
            if (data!=null && data.Count > 0)
            {
                foreach(var item in data)
                {
                    var ext = item.NewsExtensions;
                    if (ext != null && ext.Count > 0)
                    {
                        foreach (var e in ext)
                        {
                            if (e.Type == (int)EnumNewsExtensionType.InterviewId && e.Value != "0")
                            {
                                node_ids.Add(e);
                                break;
                            }
                        }
                    }
                }
            }
            //call nodejs
            var list= InterviewNodeJsServices.ListInterviewByIds(string.Join(",", node_ids.Select(s=>s.Value)));
            var listEdit = new List<NodeJs_InterviewEntity2>();
            if(list!=null && list.Count > 0)
            {
                foreach(var itm in list)
                {
                    var flg = false;
                    foreach(var elm in node_ids)
                    {
                        if (itm.Id == elm.Value)
                        {
                            itm.NewsId = elm.NewsId.ToString();
                            flg = true;
                            listEdit.Add(itm);
                            break;
                        }
                    }
                    if (!flg)
                    {
                        listEdit.Add(itm);
                    }                   
                }
            }
            return listEdit;
        }

        public static NodeJs_InterviewEntity InterviewV3GetInterview(string Id)
        {
            //return InterviewV3Dal.InterviewV3GetById(Id);
            //goi nodejs
            return InterviewNodeJsServices.Interview_GetInterview(Id);
        }

        #endregion

        #region UPDATE
        public static ErrorMapping.ErrorCodes InterviewV3Insert(NodeJs_Interview interviewV3Entity,ref string interviewId)
        {
            //if (string.IsNullOrEmpty(interviewV3Entity.Title))
            //{
            //    interviewId = 0;
            //    return ErrorMapping.ErrorCodes.UpdateInterviewTitleIsEmpty;
            //}
            //return InterviewV3Dal.InterviewV3Insert(interviewV3Entity,out interviewId) ? 
            //    ErrorMapping.ErrorCodes.Success : 
            //    ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;

            var response = InterviewNodeJsServices.Interview_Insert(interviewV3Entity);
            interviewId = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes InterviewV3Update(NodeJs_Interview interviewV3Entity)
        {
            //if (string.IsNullOrEmpty(interviewV3Entity.Title))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateInterviewTitleIsEmpty;
            //}
            //return InterviewV3Dal.InterviewV3Update(interviewV3Entity) ? 
            //    ErrorMapping.ErrorCodes.Success : 
            //    ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
            var response = InterviewNodeJsServices.Interview_Update(interviewV3Entity);            
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes InterviewV3UpdateStatus(int id,int status)
        {
            return InterviewV3Dal.InterviewV3UpdateStatus(id,status) ?
                ErrorMapping.ErrorCodes.Success :
                ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        #endregion

        #endregion

        #region InterviewV3Guests
        #region GET
        public static NodeJs_InterviewGuestEntity InterviewV3GuestsGetById(string id)
        {
            //return InterviewV3GuestsDal.InterviewV3GuestsGetById(id);
            return InterviewNodeJsServices.InterviewGuest_GetById(id);
        }

        public static List<NodeJs_InterviewGuestEntity> InterviewV3GuestsGetByInterviewId(string interviewId, int status)
        {
            return InterviewNodeJsServices.InterviewGuest_GetByInterviewId(interviewId, status);
        }

        //public static InterviewV3GuestsEntity InterviewV3GuestsGetInterviewIdAndUserName(int interviewId, string userName)
        //{
        //    return InterviewV3GuestsDal.InterviewV3GuestsGetInterviewIdAndUserName(interviewId, userName);
        //}

        //public static List<InterviewV3GuestsEntity> InterviewV3GuestsGetByInterviewId(int interviewId,bool status)
        //{
        //    return InterviewV3GuestsDal.InterviewV3GuestsGetByInterviewId(interviewId,status);
        //}
        #endregion

        #region UPDATE
        public static ErrorMapping.ErrorCodes InterviewV3GuestsInsert(NodeJs_InterviewGuestEntity interviewV3Guests,ref string guestId)
        {

            var response = InterviewNodeJsServices.Interview_Guest_Insert(interviewV3Guests);
            guestId = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //if (string.IsNullOrEmpty(interviewV3Guests.FullName))
            //{
            //    guestId = 0;
            //    return ErrorMapping.ErrorCodes.UpdateInterviewGuestFullNameIsEmpty;
            //}
            //return InterviewV3GuestsDal.InterviewV3GuestsInsert(interviewV3Guests,out guestId)? 
            //    ErrorMapping.ErrorCodes.Success : 
            //    ErrorMapping.ErrorCodes.InsertInterviewGuestNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewV3GuestsUpdateInfo(NodeJs_InterviewGuestEntity interviewV3Guests)
        {
            var success = InterviewNodeJsServices.Interview_Guest_Update(interviewV3Guests).Success;
            if (success)
            {
                //var currentEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEvent.Id);
                //if (currentEvent != null && currentEvent.Status == (int)EnumRollingNewsEventStatus.Published)
                //{
                //    UpdateRollingNewsEventIntoRedis(currentEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;

            //if (string.IsNullOrEmpty(interviewV3Guests.FullName))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateInterviewGuestFullNameIsEmpty;
            //}
            //return InterviewV3GuestsDal.InterviewV3GuestsUpdateInfo(interviewV3Guests)? 
            //    ErrorMapping.ErrorCodes.Success : 
            //    ErrorMapping.ErrorCodes.UpdateInterviewGuestNotSuccess;
        }

        //public static ErrorMapping.ErrorCodes InterviewV3GuestsUpdateStatus(int guestId,bool status)
        //{
        //    return InterviewV3GuestsDal.InterviewV3GuestsUpdateStatus(guestId, status) ?
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewGuestNotSuccess;
        //}

        public static ErrorMapping.ErrorCodes InterviewV3GuestsDelete(string guestId)
        {
            var success = InterviewNodeJsServices.Interview_Guest_Delete(guestId).Success;
            if (success)
            {
                //var currentEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEvent.Id);
                //if (currentEvent != null && currentEvent.Status == (int)EnumRollingNewsEventStatus.Published)
                //{
                //    UpdateRollingNewsEventIntoRedis(currentEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3GuestsDal.InterviewV3GuestsDelete(guestId, interviewId) ?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewGuestNotSuccess;
        }
        #endregion
        #endregion

        #region InterviewV3Question
        #region GET
        //public static List<InterviewV3QuestionEntity> SearchInterviewV3Question(int interviewId, string keyword, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy, int pageIndex, int pageSize, ref int totalRow)
        //{
        //    return InterviewV3QuestionDal.SearchInterviewV3Question(interviewId, keyword, (int)status,
        //                                                        (int)orderBy, pageIndex, pageSize,
        //                                                        ref totalRow);
        //}
        public static List<NodeJs_InterviewQuestionEntity> InterviewV3QuestionGetListByInterviewId(string interviewId, EnumNodeJsInterviewQuestionStatus status)
        {
            //return InterviewV3QuestionDal.InterviewV3QuestionGetListByInterviewId(interviewId);
            return InterviewNodeJsServices.InterviewQuestion_Search(interviewId, status);
        }

        //public static List<InterviewV3QuestionDisplayListEntity> InterviewV3QuestionSimpleListByInterviewId(int interviewId)
        //{
        //    return InterviewV3QuestionDal.InterviewV3QuestionSimpleListByInterviewId(interviewId);
        //}

        //public static List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByStatus(int interviewId, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy)
        //{
        //    return InterviewV3QuestionDal.InterviewV3QuestionGetListByStatus(interviewId, status,orderBy);
        //}

        public static NodeJs_InterviewQuestionEntity InterviewV3QuestionGetById(string id)
        {
            //return InterviewV3QuestionDal.InterviewV3QuestionGetById(id);
            return InterviewNodeJsServices.InterviewQuestion_GetById(id);
        }

        public static ErrorMapping.ErrorCodes InsertExtensionInterviewV3QuestionData(int interviewId, string data, ref int interviewQuestionId)
        {
            if (string.IsNullOrEmpty(data))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidAnswer;
            }
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var interview = InterviewV3Dal.InterviewV3GetById(interviewId);
            if (null == interview)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }

            return InterviewV3QuestionDal.InsertExtensionData(interviewId, data, WcfMessageHeader.Current.ClientUsername,
                                                            ref interviewQuestionId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewV3QuestionPriority(int interviewId, string listOfInterviewQuestionId)
        {
            if (interviewId <= 0 || string.IsNullOrEmpty(listOfInterviewQuestionId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            }
            var interview = InterviewV3Dal.InterviewV3GetById(interviewId);
            if (interview == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var updateSuccess = InterviewV3QuestionDal.UpdatePriority(interviewId, listOfInterviewQuestionId);
            if (updateSuccess)
            {
                //PublishInterviewContent(interviewId);
                //UpdateInterviewQuestionIntoRedis(interviewId);
            }
            return updateSuccess
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewQuestionIntoRedis(int interviewId)
        {
            try
            {
                var allData = new List<InterviewQuestionForPublishEntity>();

                var interviewQuestionHtmlFormatForAllContent = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "InterviewQuestionFormat");

                var allInterviewContent = new StringBuilder();
                var interviewQuestions = GetAllPublishedQuestionByInterviewId(interviewId);
                if (interviewQuestions != null && interviewQuestions.Count > 0)
                {
                    foreach (var interviewQuestion in interviewQuestions)
                    {
                        var eventInfo = new InterviewQuestionForPublishEntity
                        {
                            Id = interviewQuestion.Id,
                            Priority = interviewQuestion.Priority,
                            Type = interviewQuestion.Type,
                            Content = interviewQuestion.Content,                            
                            FullName = interviewQuestion.FullName,
                            Sex = interviewQuestion.Sex,
                            Age = interviewQuestion.Age,
                            Email = interviewQuestion.Email,
                            Job = interviewQuestion.Job,
                            Address = interviewQuestion.Address,
                            Mobile = interviewQuestion.Mobile,                           
                            LastModifiedDate = interviewQuestion.LastModifiedDate
                        };
                        allData.Add(eventInfo);

                        var askInfo = interviewQuestion.FullName;
                        askInfo += interviewQuestion.Sex == 1 ? " - Nam" : (interviewQuestion.Sex == 0 ? " - Nữ" : "");
                        if (interviewQuestion.Age > 0)
                        {
                            askInfo += " " + interviewQuestion.Age + " tuổi";
                        }
                        if (!string.IsNullOrEmpty(interviewQuestion.Address))
                        {
                            askInfo += " - " + interviewQuestion.Address;
                        }
                        allInterviewContent.AppendFormat(interviewQuestionHtmlFormatForAllContent, askInfo, interviewQuestion.Content, "",
                                                        interviewQuestion.Content);
                    }


                    //var newsUseRollingNews = NewsDal.GetNewsUseRollingNews(interviewId);

                    //foreach (var news in newsUseRollingNews)
                    //{
                    //    NewsDal.PublishNewsContentOnly(news.Id, allInterviewContent);
                    //}


                    //var focusDataKeyFormat = ChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsHotData");
                    //var focusDataKey = string.Format(focusDataKeyFormat, rollingNewsId);
                    //RedisHelper.Remove(dbNumber, focusDataKey);
                    //RedisHelper.Add(dbNumber, focusDataKey, focusData);
                }

                //var file = File.CreateText("D:\\Hosting\\NLD\\NewCms\\Nld\\_Logs\\API\\Version 5.0.0.3.8\\20140811\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");
                //file.WriteLine(allInterviewContent.ToString());
                //file.Close();
                //file.Dispose();

                var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));
                var allDataKeyFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsInterviewData");
                var allDataKey = string.Format(allDataKeyFormat, interviewId);

                var redisClient = new RedisStackClient(dbNumber);
                redisClient.Remove(allDataKey);
                redisClient.Add<List<InterviewQuestionForPublishEntity>>(allDataKey, allData, DateTime.Now.AddYears(1).TimeOfDay);

                redisClient.Remove(allDataKey + "all");

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,
                                "UpdateInterviewQuestionIntoRedis(" + interviewId + ")" + ex);
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<InterviewV3QuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            return InterviewV3QuestionDal.GetAllPublishedQuestionByInterviewId(interviewId);
        }
        #endregion

        #region UPDATE
        //public static ErrorMapping.ErrorCodes InterviewV3QuestionUpdateQuestionContent(int questionId,string questionContent,string updateBy)
        //{
        //    if (string.IsNullOrEmpty(questionContent))
        //    {
        //        return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        //    }

        //    return InterviewV3QuestionDal.InterviewV3QuestionUpdateQuestionContent(questionId,questionContent,updateBy) ?
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        //}

        //public static ErrorMapping.ErrorCodes InterviewV3QuestionUpdateStatus(int questionId, EnumInterviewV3QuestionStatus status)
        //{
        //    return InterviewV3QuestionDal.InterviewV3QuestionUpdateStatus(questionId, status)?
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        //}

        //public static ErrorMapping.ErrorCodes InterviewV3QuestionUpdatePriority(int questionId, int priority)
        //{
        //    return InterviewV3QuestionDal.InterviewV3QuestionUpdatePriority(questionId, priority)?
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        //}

        public static ErrorMapping.ErrorCodes InterviewV3QuestionUpdate(NodeJs_InterviewQuestionEntity interviewV3QuestionEntity)
        {
            var response = InterviewNodeJsServices.Interview_Question_Update(interviewV3QuestionEntity);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3QuestionDal.InterviewV3QuestionUpdate(interviewV3QuestionEntity)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewV3QuestionInsert(NodeJs_InterviewQuestionEntity interviewV3QuestionEntity,ref string questionId)
        {
            var response = InterviewNodeJsServices.Interview_Question_Insert(interviewV3QuestionEntity);
            questionId = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3QuestionDal.InterviewV3QuestionInsert(interviewV3QuestionEntity, ref questionId)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewQuestionPublish(string id, string interview_id, string published_by)
        {
            var response = InterviewNodeJsServices.Interview_Question_Publish(id, interview_id, published_by);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3QuestionDal.InterviewV3QuestionUpdate(interviewV3QuestionEntity)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewQuestionUnPublish(string id, string interview_id, string published_by)
        {
            var response = InterviewNodeJsServices.Interview_Question_UnPublish(id, interview_id, published_by);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3QuestionDal.InterviewV3QuestionUpdate(interviewV3QuestionEntity)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewQuestionReturnWaitForAnswer(string id, string interview_id, string published_by)
        {
            var response = InterviewNodeJsServices.Interview_Question_ReturnWaitForAnswer(id, interview_id, published_by);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3QuestionDal.InterviewV3QuestionUpdate(interviewV3QuestionEntity)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewQuestionUpdatePriority(string id, int Priority, string modified_by)
        {
            var response = InterviewNodeJsServices.Interview_Question_UpdatePrioriry(id, Priority, modified_by);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;       
        }


        public static ErrorMapping.ErrorCodes InterviewV3QuestionReceiveQuestion(int questionId, string processUser, EnumInterviewV3QuestionStatus status)
        {
            return InterviewV3QuestionDal.InterviewV3QuestionReceiveQuestion(questionId, processUser, status)?
                ErrorMapping.ErrorCodes.Success :
                ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        
        public static bool CheckChannelRoleForUser(int interviewId, string username, EnumInterviewChannelRole channelRole)
        {
            var userOnChannelRole = InterviewChannelDal.GetUserOnChannelRole(interviewId, (int)channelRole, username);
            return userOnChannelRole != null && userOnChannelRole.Count > 0;
        }

        public static ErrorMapping.ErrorCodes InterviewV3QuestionDelete(string questionId)
        {
            var response = InterviewNodeJsServices.Interview_Question_Delete(questionId);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3AnswersDal.InterviewV3AnswersDelete(answersId) ?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewGuestNotSuccess;
        }
        #endregion
        #endregion

        #region InterviewV3Answers
        #region GET
        public static NodeJs_InterviewAnswerEntity InterviewV3AnswersGetById(string id)
        {
            //return InterviewV3AnswersDal.InterviewV3AnswersGetById(id);
            return InterviewNodeJsServices.InterviewAnswer_GetById(id);
        }

        //public static List<InterviewV3AnswersCustomEntity> InterviewV3AnswersGetCustomListByQuestionId(int questionId)
        //{
        //    return InterviewV3AnswersDal.InterviewV3AnswersGetCustomListByQuestionId(questionId);
        //}
        public static List<NodeJs_InterviewAnswerEntity> InterviewV3AnswersGetListByQuestionId(string questionId)
        {
            //return InterviewV3AnswersDal.InterviewV3AnswersGetListByQuestionId(questionId);
            return InterviewNodeJsServices.InterviewAnswer_GetByQuestionId(questionId);
        }
        #endregion

        #region UPDATE
        public static ErrorMapping.ErrorCodes InterviewV3AnswersInsert(NodeJs_InterviewAnswerEntity interviewV3AnswersEntity,ref string answerId)
        {
            var response = InterviewNodeJsServices.Interview_Answer_Insert(interviewV3AnswersEntity);
            answerId = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3AnswersDal.InterviewV3AnswersInsert(interviewV3AnswersEntity,out answerId)?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.InsertInterviewAnswerNotSuccess;
        }

        public static ErrorMapping.ErrorCodes InterviewV3AnswersUpdate(NodeJs_InterviewAnswerEntity interviewV3AnswersEntity)
        {
            var response = InterviewNodeJsServices.Interview_Answer_Update(interviewV3AnswersEntity);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3AnswersDal.InterviewV3AnswersUpdate(interviewV3AnswersEntity) ?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewAnswerNotSuccess;
        }

        //public static ErrorMapping.ErrorCodes InterviewV3AnswersUpdateAnswerContent(int answerId,string content,string updateBy)
        //{
        //    return InterviewV3AnswersDal.InterviewV3AnswersUpdateAnswerContent(answerId, content, updateBy)?
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewAnswerNotSuccess;
        //}

        //public static ErrorMapping.ErrorCodes InterviewV3AnswersUpdateAnswerStatus(int answerId, bool status)
        //{
        //    return InterviewV3AnswersDal.InterviewV3AnswersUpdateAnswerStatus(answerId, status)? 
        //        ErrorMapping.ErrorCodes.Success :
        //        ErrorMapping.ErrorCodes.UpdateInterviewAnswerNotSuccess;
        //}
        public static ErrorMapping.ErrorCodes InterviewV3AnswersDelete(string answersId)
        {
            var response = InterviewNodeJsServices.Interview_Answer_Delete(answersId);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //return InterviewV3AnswersDal.InterviewV3AnswersDelete(answersId) ?
            //    ErrorMapping.ErrorCodes.Success :
            //    ErrorMapping.ErrorCodes.UpdateInterviewGuestNotSuccess;
        }
        #endregion
        #endregion
    }
}
