﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Base.Interview;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.BO.Base.Interview
{
    public class InterviewBo
    {
        #region Interview

        #region Get

        public static InterviewDetailEntity GetInterviewForEdit(int interviewId)
        {
            var interview = InterviewDal.GetInterviewByInterviewId(interviewId);
            if (interview == null) return null;

            return new InterviewDetailEntity()
                {
                    Interview = interview,
                    InterviewChannels = InterviewChannelDal.GetChannelDetailByInterviewId(interviewId, "")
                };
        }
        public static InterviewEntity GetInterviewByInterviewId(int interviewId)
        {
            return InterviewDal.GetInterviewByInterviewId(interviewId);
        }
        public static InterviewWithSimpleFieldEntity GetInterviewShortInfoByInterviewId(int interviewId)
        {
            return InterviewDal.GetInterviewShortInfoByInterviewId(interviewId);
        }
        public static List<InterviewWithSimpleFieldEntity> SearchInterview(string keyword, int isFocus, int isActived, int status, DateTime startDateFrom, DateTime startDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            var interviews = InterviewDal.SearchInterview(keyword, isFocus, isActived, status, startDateFrom,
                                                startDateTo, pageIndex, pageSize, ref totalRow);

            var interviewIds = interviews.Aggregate("", (current, interview) => current + (";" + interview.Id));
            if (!string.IsNullOrEmpty(interviewIds)) interviewIds = interviewIds.Remove(0, 1);

            var channelList = InterviewChannelDal.GetByListOfInterviewId(interviewIds);
            foreach (var interview in interviews)
            {
                var interviewId = interview.Id;
                interview.InterviewChannels = new List<InterviewChannelEntity>();
                foreach (var channel in channelList.Where(channel => channel.InterviewId == interviewId))
                {
                    interview.InterviewChannels.Add(channel);
                }
            }

            var newsList = NewsDal.GetNewsUseInterviewByListInterviewId(interviewIds);
            foreach (var interview in interviews)
            {
                var interviewId = interview.Id;
                interview.NewsUseThisInterview = new List<NewsForSuggestionEntity>();
                foreach (var news in newsList.Where(newsItem => newsItem.InterviewId == interviewId))
                {
                    interview.NewsUseThisInterview.Add(news);
                }
            }

            return interviews;
        }
        public static List<InterviewForSuggestionEntity> SearchInterviewForSuggestion(int top, string keyword, int isFocus, int isActived)
        {
            return InterviewDal.SearchInterviewForSuggestion(top, keyword, isFocus, isActived);
        }
        public static InterviewPublishedEntity GetInterviewPublishedByInterviewId(int interviewId)
        {
            return InterviewDal.GetInterviewPublishedByInterviewId(interviewId);
        }

        #endregion

        #region Update

        public static ErrorMapping.ErrorCodes InsertInterview(InterviewEntity interview, ref int interviewId)
        {
            if (string.IsNullOrEmpty(interview.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewTitleIsEmpty;
            }
            interview.UnsignTitle = Utility.UnicodeToKoDau(interview.Title);
            interview.Status = 1;

            return InterviewDal.Insert(interview, ref interviewId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterview(InterviewEntity interview)
        {
            if (interview.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            if (string.IsNullOrEmpty(interview.Title))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewTitleIsEmpty;
            }
            var currentInterview = InterviewDal.GetInterviewByInterviewId(interview.Id);
            if (currentInterview == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            currentInterview.Title = interview.Title;
            currentInterview.UnsignTitle = Utility.UnicodeToKoDau(interview.Title);
            currentInterview.StartDate = interview.StartDate;
            currentInterview.IsFocus = interview.IsFocus;
            currentInterview.IsActived = interview.IsActived;
            currentInterview.LastModifiedBy = interview.LastModifiedBy;

            return InterviewDal.Update(currentInterview)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes DeleteInterviewByInterviewId(int interviewId)
        {
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            return InterviewDal.DeleteById(interviewId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewFocus(int interviewId, bool isFocus)
        {
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            return InterviewDal.UpdateInterviewFocus(interviewId, isFocus)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewActive(int interviewId, bool isActived)
        {
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            return InterviewDal.UpdateInterviewActive(interviewId, isActived)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes PublishInterviewContent(int interviewId)
        {
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewNotFound;
            }
            var questions = InterviewQuestionDal.GetAllPublishedQuestionByInterviewId(interviewId);

            var questionFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "InterviewQuestionFormat");

            #region ASC

            var ascQuestionContent = new StringBuilder();
            foreach (var question in questions)
            {
                var askInfo = "";
                if (question.Type == (int) EnumInterviewQuestionType.NormalQuestion)
                {
                    askInfo = question.FullName;
                    askInfo += question.Sex == 1 ? " - Nam" : (question.Sex == 0 ? " - Nữ" : "");   
                }
                if (question.Age > 0)
                {
                    askInfo += " " + question.Age + " tuổi";
                }
                if (!string.IsNullOrEmpty(question.Address))
                {
                    askInfo += " - " + question.Address;
                }
                ascQuestionContent.AppendFormat(questionFormat, askInfo, question.Content, question.ChannelName,
                                                question.Answer);
            }

            #endregion

            #region DESC

            var descQuestionContent = new StringBuilder();
            questions.Sort((item1, item2) => item2.Priority.CompareTo(item1.Priority));
            foreach (var question in questions)
            {
                var askInfo = question.FullName;
                askInfo += question.Sex == 1 ? " - Nam" : (question.Sex == 0 ?" - Nữ" : "");
                if (question.Age > 0)
                {
                    askInfo += " " + question.Age + " tuổi";
                }
                if (!string.IsNullOrEmpty(question.Address))
                {
                    askInfo += " - " + question.Address;
                }
                descQuestionContent.AppendFormat(questionFormat, askInfo, question.Content, question.ChannelName,
                                                question.Answer);
            }

            #endregion

            var publishedBy = WcfMessageHeader.Current.ClientUsername;
            return InterviewDal.PublishInterviewContent(interviewId, ascQuestionContent.ToString(), descQuestionContent.ToString(),
                                                        publishedBy)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewQuestionIntoRedis(int interviewId)
        {
            try
            {
                var allData = new List<InterviewQuestionForPublishEntity>();

                var interviewQuestionHtmlFormatForAllContent = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "InterviewQuestionFormat");
                
                var allInterviewContent = new StringBuilder();
                var interviewQuestions = GetAllPublishedQuestionByInterviewId(interviewId);
                if (interviewQuestions != null && interviewQuestions.Count > 0)
                {
                    foreach (var interviewQuestion in interviewQuestions)
                    {
                        var eventInfo = new InterviewQuestionForPublishEntity
                        {
                            Id = interviewQuestion.Id,
                            Priority = interviewQuestion.Priority,
                            Type = interviewQuestion.Type,
                            Content = interviewQuestion.Content,
                            Answer = interviewQuestion.Answer,
                            FullName = interviewQuestion.FullName,
                            Sex = interviewQuestion.Sex,
                            Age = interviewQuestion.Age,
                            Email = interviewQuestion.Email,
                            Job = interviewQuestion.Job,
                            Address = interviewQuestion.Address,
                            Mobile = interviewQuestion.Mobile,
                            DistributedDate = interviewQuestion.DistributedDate,
                            ChannelId = interviewQuestion.ChannelId,
                            ChannelName = interviewQuestion.ChannelName,
                            ChannelAvatar = interviewQuestion.ChannelAvatar,
                            LastModifiedDate = interviewQuestion.LastModifiedDate
                        };
                        allData.Add(eventInfo);

                        var askInfo = interviewQuestion.FullName;
                        askInfo += interviewQuestion.Sex == 1 ? " - Nam" : (interviewQuestion.Sex == 0 ? " - Nữ" : "");
                        if (interviewQuestion.Age > 0)
                        {
                            askInfo += " " + interviewQuestion.Age + " tuổi";
                        }
                        if (!string.IsNullOrEmpty(interviewQuestion.Address))
                        {
                            askInfo += " - " + interviewQuestion.Address;
                        }
                        allInterviewContent.AppendFormat(interviewQuestionHtmlFormatForAllContent, askInfo, interviewQuestion.Content, interviewQuestion.ChannelName,
                                                        interviewQuestion.Answer);
                    }

                    
                    //var newsUseRollingNews = NewsDal.GetNewsUseRollingNews(interviewId);

                    //foreach (var news in newsUseRollingNews)
                    //{
                    //    NewsDal.PublishNewsContentOnly(news.Id, allInterviewContent);
                    //}


                    //var focusDataKeyFormat = ChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsHotData");
                    //var focusDataKey = string.Format(focusDataKeyFormat, rollingNewsId);
                    //RedisHelper.Remove(dbNumber, focusDataKey);
                    //RedisHelper.Add(dbNumber, focusDataKey, focusData);
                }

                //var file = File.CreateText("D:\\Hosting\\NLD\\NewCms\\Nld\\_Logs\\API\\Version 5.0.0.3.8\\20140811\\" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt");
                //file.WriteLine(allInterviewContent.ToString());
                //file.Close();
                //file.Dispose();

                var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));
                var allDataKeyFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsInterviewData");
                var allDataKey = string.Format(allDataKeyFormat, interviewId);

                var redisClient = new RedisStackClient(dbNumber);
                redisClient.Remove(allDataKey);
                redisClient.Add<List<InterviewQuestionForPublishEntity>>(allDataKey, allData, DateTime.Now.AddYears(1).TimeOfDay);

                redisClient.Remove(allDataKey + "all");

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,
                                "UpdateInterviewQuestionIntoRedis(" + interviewId + ")" + ex);
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        #endregion

        #region Interview channel

        #region Get

        public static InterviewChannelEntity GetInterviewChannelByInterviewChannelId(int interviewChannelId)
        {
            return InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewChannelId);
        }
        public static List<InterviewChannelEntity> GetInterviewChannelDetailByInterviewId(int interviewId, string username)
        {
            return InterviewChannelDal.GetChannelDetailByInterviewId(interviewId, username);
        }
        public static List<InterviewChannelEntity> GetInterviewChannelByInterviewIdAndUsername(int interviewId, string username)
        {
            return InterviewChannelDal.GetByInterviewIdAndUsername(interviewId, username);
        }
        public static bool CheckChannelRoleForUser(int interviewId, string username, EnumInterviewChannelRole channelRole)
        {
            var userOnChannelRole = InterviewChannelDal.GetUserOnChannelRole(interviewId, (int)channelRole, username);
            return userOnChannelRole != null && userOnChannelRole.Count > 0;
        }
        public static List<InterviewChannelEntity> GetInterviewChannelProcess(int interviewChannelId)
        {
            return InterviewChannelDal.GetInterviewChannelProcess(interviewChannelId);
        }

        #endregion

        #region Update


        public static ErrorMapping.ErrorCodes InsertInterviewChannel(InterviewChannelEntity interviewChannel, ref int interviewChannelId)
        {
            if (interviewChannel.InterviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewNotFound;
            }
            if (string.IsNullOrEmpty(interviewChannel.Username))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelUsernameNotValid;
            }

            if (Security.PermissionBo.CheckUserPermission(WcfMessageHeader.Current.ClientUsername,
                                                          (int)EnumPermission.InterviewManager) !=
                ErrorMapping.ErrorCodes.Success)
            {
                var currentRoleForUser = InterviewChannelDal.GetUserOnChannelRole(interviewChannel.InterviewId,
                                                                              (int)EnumInterviewChannelRole.Admin,
                                                                              WcfMessageHeader.Current.ClientUsername);
                if (currentRoleForUser != null && currentRoleForUser.Count <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateInterviewChannelNotAllowToUpdate;
                }
            }

            var user = UserBo.GetUserByUsername(interviewChannel.Username);
            if (user == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelUsernameNotValid;
            }

            return InterviewChannelDal.Insert(interviewChannel, ref interviewChannelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewChannelNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewChannel(InterviewChannelEntity interviewChannel)
        {
            if (interviewChannel.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewChannelNotFound;
            }
            if (string.IsNullOrEmpty(interviewChannel.Username))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelUsernameNotValid;
            }

            if (Security.PermissionBo.CheckUserPermission(WcfMessageHeader.Current.ClientUsername,
                                                          (int)EnumPermission.InterviewManager) !=
                ErrorMapping.ErrorCodes.Success)
            {
                var currentRoleForUser = InterviewChannelDal.GetUserOnChannelRole(interviewChannel.InterviewId,
                                                                              (int)EnumInterviewChannelRole.Admin,
                                                                              WcfMessageHeader.Current.ClientUsername);
                if (currentRoleForUser != null && currentRoleForUser.Count <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateInterviewChannelNotAllowToUpdate;
                }
            }

            var user = UserBo.GetUserByUsername(interviewChannel.Username);
            if (user == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelUsernameNotValid;
            }
            if (interviewChannel.InterviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewNotFound;
            }

            var currentInterviewChannel = InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewChannel.Id);
            if (currentInterviewChannel == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewChannelNotFound;
            }

            currentInterviewChannel.Username = interviewChannel.Username;
            currentInterviewChannel.ChannelRole = interviewChannel.ChannelRole;
            currentInterviewChannel.ChannelName = interviewChannel.ChannelName;
            currentInterviewChannel.ChannelDescription = interviewChannel.ChannelDescription;
            currentInterviewChannel.Avatar = interviewChannel.Avatar;
            currentInterviewChannel.ChannelDetail = interviewChannel.ChannelDetail;

            return InterviewChannelDal.Update(currentInterviewChannel)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewChannelNotSuccess;
        }
        public static ErrorMapping.ErrorCodes RemoveInterviewChannel(int interviewChannelId)
        {
            if (interviewChannelId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewChannelNotFound;
            }

            var currentInterviewChannel = InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewChannelId);
            if (currentInterviewChannel == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelInterviewChannelNotFound;
            }

            if (Security.PermissionBo.CheckUserPermission(WcfMessageHeader.Current.ClientUsername,
                                                          (int)EnumPermission.InterviewManager) !=
                ErrorMapping.ErrorCodes.Success)
            {
                var currentRoleForUser = InterviewChannelDal.GetUserOnChannelRole(currentInterviewChannel.InterviewId,
                                                                              (int)EnumInterviewChannelRole.Admin,
                                                                              WcfMessageHeader.Current.ClientUsername);
                if (currentRoleForUser != null && currentRoleForUser.Count <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateInterviewChannelNotAllowToUpdate;
                }
            }

            var questionCountByChannel = InterviewQuestionDal.GetQuestionCountByChannel(currentInterviewChannel.InterviewId);
            if (questionCountByChannel.Exists(item => item.ChannelId == interviewChannelId && item.QuestionCount > 0))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewChannelAlreadyAnswerQuestion;
            }

            return InterviewChannelDal.Remove(interviewChannelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewChannelNotSuccess;
        }

        #endregion

        #endregion

        #region Interview question

        #region Get

        public static InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            return InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
        }

        public static List<InterviewQuestionWithSimpleFieldEntity> SearchInterviewQuestion(int interviewId,
                                                                                           string keyword, int type,
                                                                                           string username,
                                                                                           EnumInterviewQuestionFieldFilterForUsername
                                                                                               fieldFilterForUsername,
                                                                                           EnumInterviewQuestionStatus
                                                                                               status, bool invertStatus,
                                                                                           EnumInterviewQuestionOrder
                                                                                               orderBy, int pageIndex,
                                                                                           int pageSize,
                                                                                           ref int totalRow)
        {
            return InterviewQuestionDal.SearchInterviewQuestion(interviewId, keyword, type, username,
                                                                (int) fieldFilterForUsername, (int) status, invertStatus,
                                                                (int) orderBy, pageIndex, pageSize,
                                                                ref totalRow);
        }

        public static List<InterviewQuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            return InterviewQuestionDal.GetAllPublishedQuestionByInterviewId(interviewId);
        }

        #endregion

        #region Working flow

        public static ErrorMapping.ErrorCodes ReceivedInterviewQuestion(int interviewQuestionId)
        {
            return InterviewQuestionDal.ReceivedQuestion(interviewQuestionId, WcfMessageHeader.Current.ClientUsername)
                              ? ErrorMapping.ErrorCodes.Success
                              : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes DistributeInterviewQuestion(int interviewQuestionId, int interviewChannelId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaitForDistribute
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForDistribute)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowDistributeThisQuestion;
            }
            var distributedBy = WcfMessageHeader.Current.ClientUsername;
            // User phai co vai tro Distributor
            if (!CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         distributedBy,
                                         EnumInterviewChannelRole.Distributor))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowDistributeThisQuestion;
            }
            var channel = InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewChannelId);
            // Khong tim thay channel can dieu phoi cau hoi toi
            if (null == channel)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionChannelNotFound;
            }
            // channel can dieu phoi den khong phai la ChannelUser (truc kenh)
            if (channel.ChannelRole != (int)EnumInterviewChannelRole.ChannelUser)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionMustDistributeToChannelUserOnly;
            }

            return InterviewQuestionDal.DistributeQuestion(interviewQuestionId, interviewChannelId, distributedBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes ForwardInterviewQuestion(int interviewQuestionId, int interviewChannelId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaiForAnswer
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForAnswer)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowForwardThisQuestion;
            }
            var channelForCurrentUser = InterviewChannelDal.GetByInterviewIdAndUsername(currentInterviewQuestion.InterviewId,
                                                                          WcfMessageHeader.Current.ClientUsername);
            // Khong tim thay channel dang xu ly yeu cau hoac channel dang xu ly yeu cau khong phai la channel duoc phan cong tra loi cau hoi
            if (channelForCurrentUser == null || channelForCurrentUser.Count <= 0 ||
                !channelForCurrentUser.Exists(item => item.Id == currentInterviewQuestion.ChannelId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowForwardThisQuestion;
            }
            var channel = InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewChannelId);
            // Khong tim thay channel can forward
            if (null == channel)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionChannelNotFound;
            }
            // Channel can forward khong phai la ChannelUser (Truc kenh)
            if (channel.ChannelRole != (int)EnumInterviewChannelRole.ChannelUser)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionMustDistributeToChannelUserOnly;
            }

            return InterviewQuestionDal.ForwardQuestion(interviewQuestionId, interviewChannelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes SendAnswerForInterviewQuestion(int interviewQuestionId, string answerContent)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaitForAnswer
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForAnswer)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowSendAnswerForThisQuestion;
            }
            var channelForCurrentUser = InterviewChannelDal.GetByInterviewIdAndUsername(currentInterviewQuestion.InterviewId,
                                                                          WcfMessageHeader.Current.ClientUsername);
            // Khong tim thay channel dang xu ly yeu cau hoac channel dang xu ly yeu cau khong phai la channel duoc phan cong tra loi cau hoi
            if (channelForCurrentUser == null || channelForCurrentUser.Count <= 0 ||
                !channelForCurrentUser.Exists(item => item.Id == currentInterviewQuestion.ChannelId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowSendAnswerForThisQuestion;
            }
            else
            {
                return InterviewQuestionDal.SendAnswerForQuestion(interviewQuestionId, answerContent, WcfMessageHeader.Current.ClientUsername)
                               ? ErrorMapping.ErrorCodes.Success
                               : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            }
        }

        public static ErrorMapping.ErrorCodes ReturnInterviewQuestion(int interviewQuestionId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaiForAnswer
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForAnswer)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowReturnThisQuestion;
            }
            var channelForCurrentUser = InterviewChannelDal.GetByInterviewIdAndUsername(currentInterviewQuestion.InterviewId,
                                                                          WcfMessageHeader.Current.ClientUsername);
            // Khong tim thay channel dang xu ly yeu cau hoac channel dang xu ly yeu cau khong phai la channel duoc phan cong tra loi cau hoi
            if (channelForCurrentUser == null || channelForCurrentUser.Count <= 0 ||
                !channelForCurrentUser.Exists(item => item.Id == currentInterviewQuestion.ChannelId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowReturnThisQuestion;
            }
            else
            {
                return InterviewQuestionDal.ReturnQuestion(interviewQuestionId)
                               ? ErrorMapping.ErrorCodes.Success
                               : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            }
        }

        public static ErrorMapping.ErrorCodes SendInterviewQuestionWaitForPublish(int interviewQuestionId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaiForAnswer
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForEdit)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowEditThisQuestion;
            }
            var editedBy = WcfMessageHeader.Current.ClientUsername;
            // User phai co vai tro editor
            if (!CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         editedBy,
                                         EnumInterviewChannelRole.Editor))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowEditThisQuestion;
            }

            return InterviewQuestionDal.SendWaitForPublish(interviewQuestionId, editedBy)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes PublishInterviewQuestion(int interviewQuestionId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaitForPublish
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForPublish &&
                currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.Unpublished)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowPublishThisQuestion;
            }
            var publishedBy = WcfMessageHeader.Current.ClientUsername;
            // User phai co vai tro Publisher
            if (!CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         publishedBy,
                                         EnumInterviewChannelRole.Editor) &&
                !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         publishedBy,
                                         EnumInterviewChannelRole.Publisher))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowPublishThisQuestion;
            }
            var publishSuccess = InterviewQuestionDal.PublishQuestion(interviewQuestionId, publishedBy);
            if (publishSuccess)
            {
                //PublishInterviewContent(currentInterviewQuestion.InterviewId);
                UpdateInterviewQuestionIntoRedis(currentInterviewQuestion.InterviewId);
            }
            return publishSuccess
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        public static ErrorMapping.ErrorCodes UnpublishInterviewQuestion(int interviewQuestionId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaitForPublish
            if (currentInterviewQuestion.Status != (int)EnumInterviewQuestionStatus.Published)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowUnpublishThisQuestion;
            }
            // User phai co vai tro Publisher
            if (!CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         WcfMessageHeader.Current.ClientUsername,
                                         EnumInterviewChannelRole.Editor) &&
                !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                         WcfMessageHeader.Current.ClientUsername,
                                         EnumInterviewChannelRole.Publisher))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowUnpublishThisQuestion;
            }
            var unpublishSuccess = InterviewQuestionDal.UnpublishQuestion(interviewQuestionId);
            if (unpublishSuccess)
            {
                //PublishInterviewContent(currentInterviewQuestion.InterviewId);
                UpdateInterviewQuestionIntoRedis(currentInterviewQuestion.InterviewId);
            }
            return unpublishSuccess 
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        #endregion

        #region Update

        public static ErrorMapping.ErrorCodes InsertInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            if (string.IsNullOrEmpty(interviewQuestion.FullName))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidFullName;
            }
            if (interviewQuestion.InterviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var interview = InterviewDal.GetInterviewByInterviewId(interviewQuestion.InterviewId);
            if (null == interview)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            interviewQuestion.ReceivedBy = WcfMessageHeader.Current.ClientUsername;
            interviewQuestion.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
            interviewQuestion.CreatedDate = DateTime.Now;

            if (interviewQuestion.ChannelId > 0)
            {
                if (CheckChannelRoleForUser(interviewQuestion.InterviewId,
                                            WcfMessageHeader.Current.ClientUsername,
                                            EnumInterviewChannelRole.Admin) ||
                    CheckChannelRoleForUser(interviewQuestion.InterviewId,
                                            WcfMessageHeader.Current.ClientUsername,
                                            EnumInterviewChannelRole.Distributor) ||
                    CheckChannelRoleForUser(interviewQuestion.InterviewId,
                                            WcfMessageHeader.Current.ClientUsername,
                                            EnumInterviewChannelRole.ChannelUser))
                {
                    var interviewChannel =
                        InterviewChannelDal.GetInterviewChannelByInterviewChannelId(interviewQuestion.ChannelId);
                    if (interviewChannel != null)
                    {
                        if (interviewChannel.ChannelRole == (int) EnumInterviewChannelRole.ChannelUser)
                        {
                            interviewQuestion.Status = (int) EnumInterviewQuestionStatus.WaitForAnswer;
                        }
                    }
                    else
                    {
                        interviewQuestion.ChannelId = 0;
                        interviewQuestion.Status = (int) EnumInterviewQuestionStatus.WaitForDistribute;
                    }
                }
                else
                {
                    interviewQuestion.ChannelId = 0;
                    interviewQuestion.Status = (int)EnumInterviewQuestionStatus.WaitForDistribute;
                }
            }
            else
            {
                interviewQuestion.ChannelId = 0;
                interviewQuestion.Status = (int)EnumInterviewQuestionStatus.WaitForDistribute;
            }

            return InterviewQuestionDal.Insert(interviewQuestion, ref interviewQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes InsertExtensionInterviewQuestionData(int interviewId, string data, ref int interviewQuestionId)
        {
            if (string.IsNullOrEmpty(data))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidAnswer;
            }
            if (interviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var interview = InterviewDal.GetInterviewByInterviewId(interviewId);
            if (null == interview)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }

            return InterviewQuestionDal.InsertExtensionData(interviewId, data, WcfMessageHeader.Current.ClientUsername,
                                                            ref interviewQuestionId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes InsertExternalInterviewQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            if (string.IsNullOrEmpty(interviewQuestion.FullName))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidFullName;
            }
            if (interviewQuestion.InterviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var interview = InterviewDal.GetInterviewByInterviewId(interviewQuestion.InterviewId);
            if (null == interview)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            interviewQuestion.ReceivedBy = WcfMessageHeader.Current.ClientUsername;
            interviewQuestion.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
            interviewQuestion.CreatedDate = DateTime.Now;

            return InterviewQuestionDal.InsertExternalQuestion(interviewQuestion, ref interviewQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewQuestion(InterviewQuestionEntity interviewQuestion)
        {
            if (string.IsNullOrEmpty(interviewQuestion.Answer) &&
                interviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForCollect &&
                interviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForDistribute &&
                interviewQuestion.Status != (int)EnumInterviewQuestionStatus.WaitForAnswer)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidAnswer;
            }
            if (interviewQuestion.InterviewId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var interview = InterviewDal.GetInterviewByInterviewId(interviewQuestion.InterviewId);
            if (null == interview)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestion.Id);
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            if (interviewQuestion.Type == (int)EnumInterviewQuestionType.NormalQuestion && string.IsNullOrEmpty(interviewQuestion.FullName))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInvalidFullName;
            }
            currentInterviewQuestion.Status = interviewQuestion.Status;
            currentInterviewQuestion.Content = interviewQuestion.Content;
            currentInterviewQuestion.Answer = interviewQuestion.Answer;
            currentInterviewQuestion.FullName = interviewQuestion.FullName;
            currentInterviewQuestion.Sex = interviewQuestion.Sex;
            currentInterviewQuestion.Age = interviewQuestion.Age;
            currentInterviewQuestion.Email = interviewQuestion.Email;
            currentInterviewQuestion.Job = interviewQuestion.Job;
            currentInterviewQuestion.Address = interviewQuestion.Address;
            currentInterviewQuestion.Mobile = interviewQuestion.Mobile;
            currentInterviewQuestion.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;

            var result = InterviewQuestionDal.Update(currentInterviewQuestion)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                if (currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.Published)
                {
                    UpdateInterviewQuestionIntoRedis(currentInterviewQuestion.InterviewId);
                }
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateInterviewQuestionPriority(int interviewId, string listOfInterviewQuestionId)
        {
            if (interviewId <= 0 || string.IsNullOrEmpty(listOfInterviewQuestionId))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            }
            var interview = InterviewDal.GetInterviewByInterviewId(interviewId);
            if (interview == null)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewNotFound;
            }
            var updateSuccess = InterviewQuestionDal.UpdatePriority(interviewId, listOfInterviewQuestionId);
            if (updateSuccess)
            {
                //PublishInterviewContent(interviewId);
                UpdateInterviewQuestionIntoRedis(interviewId);
            }
            return updateSuccess
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }
        public static ErrorMapping.ErrorCodes DeleteInterviewQuestion(int interviewQuestionId)
        {
            var currentInterviewQuestion = InterviewQuestionDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            // Khong tim thay cau hoi
            if (null == currentInterviewQuestion)
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionInterviewQuestionNotFound;
            }
            // Trang thai cau hoi phai la WaitForPublish
            if ((currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.WaitForCollect &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.Collector)) ||
                (currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.WaitForDistribute &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.Distributor)) ||
                (currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.WaitForAnswer &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.ChannelUser)) ||
                (currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.WaitForPublish &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.Editor)) ||
                (currentInterviewQuestion.Status == (int) EnumInterviewQuestionStatus.Published &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.Editor) &&
                 !CheckChannelRoleForUser(currentInterviewQuestion.InterviewId,
                                          WcfMessageHeader.Current.ClientUsername,
                                          EnumInterviewChannelRole.Publisher)))
            {
                return ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotAllowDeleteThisQuestion;
            }
            var result= InterviewQuestionDal.DeleteQuestion(interviewQuestionId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
            if (result == ErrorMapping.ErrorCodes.Success)
            {

                if (currentInterviewQuestion.Status == (int)EnumInterviewQuestionStatus.Published)
                {
                    UpdateInterviewQuestionIntoRedis(currentInterviewQuestion.InterviewId);
                }
            }
            return result;
        }

        #endregion

        #endregion

        #region Interview channel process

        #region Update

        public static ErrorMapping.ErrorCodes UpdateInterviewChannelProcess(int interviewChannelId, string listProcessInterviewChannelId)
        {
            return InterviewChannelProcessDal.Update(interviewChannelId, listProcessInterviewChannelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UpdateInterviewQuestionNotSuccess;
        }

        #endregion

        #endregion
    }
}
