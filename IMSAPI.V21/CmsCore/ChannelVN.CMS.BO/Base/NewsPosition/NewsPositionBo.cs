﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.NewsPosition;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using System.Threading.Tasks;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.Base.News;
using ChannelVN.WcfExtensions;
using System.Web;

namespace ChannelVN.CMS.BO.Base.NewsPosition
{
    public class NewsPositionBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="listOfFocusPositionOnLastestNews"></param>
        /// <returns></returns>
        public virtual NewsPositionForListPageEntity GetListNewsPositionForListPage(int zoneId, string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForListPageEntity
                {
                    HighlightListFocusByZone =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.NewsFocusByZone,
                            zoneId)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:39
        /// </summary>
        /// <returns></returns>
        public virtual NewsPositionForHomePageEntity GetListNewsPositionForHomePage(string listOfFocusPositionOnLastestNews)
        {
            try
            {
                return new NewsPositionForHomePageEntity
                {
                    HighlightHomeFocus =
                        GetListNewsPositionByTypeAndZoneId(
                            (int)NewsPositionType.HomeNewsFocus, 0)
                };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }


        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:42
        /// </summary>
        /// <param name="newsPositionType"></param>
        /// <param name="zoneId"></param>
        /// <param name="listOfOrder"></param>
        /// <returns></returns>
        public virtual List<NewsPositionEntity> GetListNewsPositionByTypeAndZoneId(int newsPositionType, int zoneId, string listOfOrder = "")
        {
            var newsPositions = new List<NewsPositionEntity>();
            try
            {
                return NewsPositionDal.GetListByTypeAndZoneId(newsPositionType, zoneId, listOfOrder);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return newsPositions;
        }
        public virtual List<NewsPositionEntity> GetListNewsPositionAllBom(int newsPositionType, int zoneId)
        {
            var newsPositions = new List<NewsPositionEntity>();
            try
            {
                return NewsPositionDal.GetListNewsPositionAllBom(newsPositionType, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return newsPositions;
        }
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:42
        /// </summary>
        /// <param name="newsPositionType"></param>
        /// <param name="zoneId"></param>
        /// <param name="listOfOrder"></param>
        /// <returns></returns>
        public virtual List<NewsPositionEntity> GetListByTypeAndZoneIdWithOrderByDistritbutionDate(int newsPositionType, int zoneId, int top)
        {
            var newsPositions = new List<NewsPositionEntity>();
            try
            {
                return NewsPositionDal.GetListByTypeAndZoneIdWithOrderByDistritbutionDate(newsPositionType, zoneId, top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsPositions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public virtual List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId)
        {
            var newsPositions = new List<NewsPositionEntity>();
            try
            {
                return NewsPositionDal.GetScheduleByTypeAndOrder(typeId, order, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsPositions;
        }

        public virtual NewsPositionEntity GetNewsPositionById(int newsPositionId)
        {
            return NewsPositionDal.GetNewsPositionById(newsPositionId);
        }

        public struct UnlockPosition
        {
            public int PositionType;
            public int ZoneId;
            public long NewsId;
        }

        public static ErrorMapping.ErrorCodes SaveLinkPosition(NewsPositionType typeId, int position, int zoneId, long newsId, int type, string title, string avatar, string url, string sapo)
        {
            //lấy danh sách bài cũ ra, chèn vào đề xuất nổi bật trang chủ (phải lấy ra trước khi cài)
            var oldPos = BoFactory.GetInstance<NewsPositionBo>().GetListNewsPositionForHomePage("");
            var data = NewsPositionDal.SaveLinkPosition((int)typeId, position, zoneId, newsId, type, title, avatar, url, sapo);
            if (data == true)
            {
                foreach (var pos in oldPos.HighlightHomeFocus)
                {
                    if (pos.Order == position)
                    {
                        NewsBo.UpdateDisplayPosition(pos.NewsId, 1, WcfExtensions.WcfMessageHeader.Current.ClientUsername);
                        break;
                    }
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:45
        /// </summary>
        /// <param name="newsPositions"></param>
        /// <param name="avatarIndex"></param>
        /// <param name="checkNewsExists"></param>
        /// <param name="usernameForAction"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes SaveNewsPosition(List<NewsPositionEntity> newsPositions, int avatarIndex,
                                                  bool checkNewsExists, string usernameForAction, bool isSwap)
        {
            try
            {
                var count = newsPositions.Count;
                for (var i = 0; i < count; i++)
                {
                    var newsPosition = newsPositions[i];

                    Logger.WriteLog(Logger.LogType.Trace, "SaveNewsPosition => " + usernameForAction + ", newsPosition: "+NewtonJson.Serialize(newsPosition));

                    NewsPositionDal.SaveNewsPosition(newsPosition.TypeId, newsPosition.ZoneId, newsPosition.Order,
                                                     newsPosition.NewsId, newsPosition.ExpiredLock, newsPosition.Avatar,
                                                     avatarIndex, checkNewsExists);
                    
                    NewTaskRunAction(() =>
                    {
                        try {
                            //tự update displayposition=2 ->loại bỏ khỏi sugget ES va Redis =>chỉ dùng cho afamily
                            if (WcfMessageHeader.CustomWcfHeader.Namespace.ToLower().Equals("afamily"))
                            {                                
                                BoSearch.Base.News.NewsDalFactory.UpdateDisplayPosition(newsPosition.NewsId, 2, usernameForAction);
                                BoCached.Base.News.NewsDalFactory.UpdateDisplayPosition(newsPosition.NewsId, 2, usernameForAction);
                            }
                        }
                        catch { }
                        // Log hành động đổi vị trí bài nổi bật
                        var zoneName = string.Empty;
                        if (newsPosition.ZoneId > 0)
                        {
                            var zoneEntity = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(newsPosition.ZoneId);
                            if (zoneEntity == null)
                                zoneEntity = ZoneDal.GetZoneById(newsPosition.ZoneId);

                            zoneName = zoneEntity.Name;
                        }
                        var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(newsPosition.NewsId);
                        if (newsInfo == null)
                            newsInfo = NewsDal.GetNewsById(newsPosition.NewsId);

                        if (isSwap)
                        {
                            ActivityBo.LogSwapNewsPosition(newsPosition.NewsId, usernameForAction, newsInfo.Title, newsPosition.TypeId, newsPosition.Order, zoneName);
                        }
                        else
                        {
                            ActivityBo.LogSetNewsPosition(newsPosition.NewsId, usernameForAction, newsInfo.Title, newsPosition.TypeId, newsPosition.Order, zoneName);
                        }
                        // Kết thúc log2
                    });
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId, string listScheduleDate, string listRemovePosition, string usernameForAction)
        {
            try
            {
                NewsPositionDal.SaveNewsPositionSchedule(typeId, zoneId, order, listNewsId, listScheduleDate, listRemovePosition);
                //var count = newsPositions.Count;
                //for (var i = 0; i < count; i++)
                //{
                //    var newsPosition = newsPositions[i];


                // Log hành động đổi vị trí bài nổi bật
                //var zoneName = string.Empty;
                //if (newsPosition.ZoneId > 0)
                //{
                //    var zoneEntity = ZoneDal.GetZoneById(newsPosition.ZoneId);
                //    zoneName = zoneEntity.Name;
                //}
                //var newsInfo = NewsDal.GetNewsById(newsPosition.NewsId);
                //if (isSwap)
                //{
                //    ActivityBo.LogBombNewsPosition(newsPosition.NewsId, usernameForAction, newsInfo.Title,
                //                                   newsPosition.TypeId, newsPosition.Order, zoneName);
                //}
                //else
                //{
                //var zoneName = string.Empty;
                //if (zoneId > 0)
                //{
                //    var zoneEntity = ZoneDal.GetZoneById(zoneId);
                //    zoneName = zoneEntity.Name;
                //}

                var arrNewsId = listNewsId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                var arrScheduleDate = listScheduleDate.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < arrNewsId.Length; i++)
                {
                    try
                    {
                        Logger.WriteLog(Logger.LogType.Debug, string.Format("Bomb news {0} Zone {1} TypeId {2} Schedule {3}", arrNewsId[i], zoneId, typeId, arrScheduleDate[i]));
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Khong ghi duoc log bomb thoi ma " + listNewsId + " / " + listScheduleDate + " / " + zoneId + " / " + typeId);
                    }
                }

                //}
                // Kết thúc log2
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:46
        /// </summary>
        /// <param name="positionId"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes UnlockNewsPosition(int positionId)
        {
            try
            {
                return NewsPositionDal.UnlockPosition(positionId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:46
        /// </summary>
        /// <param name="positionId"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes UpdatePositionOrder(int typeId, int zoneId, string listPositionId)
        {
            try
            {
                return NewsPositionDal.UpdatePositionOrder(typeId, zoneId, listPositionId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:46
        /// </summary>
        /// <param name="positionId"></param>
        /// <param name="positionOrder"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes UpdatePositionOrderByPositionId(long positionId, int positionOrder)
        {
            try
            {
                return NewsPositionDal.UpdatePositionOrderByPositionId(positionId, positionOrder) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:46
        /// </summary>
        /// <param name="positionId"></param>
        /// <param name="expiredLock"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes LockNewsPosition(int positionId, int lockForZoneId, DateTime expiredLock)
        {
            try
            {
                return NewsPositionDal.LockPosition(positionId, lockForZoneId, expiredLock) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }


        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:50
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public virtual List<NewsPositionForMobileEntity> GetListNewsPositionForMobileByTypeAndZoneId(int typeId,
                                                                                                    int zoneId)
        {
            var newsPositions = new List<NewsPositionForMobileEntity>();
            try
            {
                return NewsPositionForMobileDal.GetListByTypeAndZoneId(typeId, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsPositions;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:50
        /// </summary>
        /// <param name="newsPositionsForMobile"></param>
        /// <param name="avatarIndex"></param>
        /// <param name="checkNewsExists"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes SaveNewsPositionForMobile(List<NewsPositionForMobileEntity> newsPositionsForMobile,
                                                           int avatarIndex, bool checkNewsExists)
        {
            try
            {
                var count = newsPositionsForMobile.Count;
                for (var i = 0; i < count; i++)
                {
                    var newsPosition = newsPositionsForMobile[i];
                    NewsPositionTypeForMobile typeId;
                    if (!Enum.TryParse(newsPosition.TypeId.ToString(), true, out typeId))
                    {
                        typeId = NewsPositionTypeForMobile.DefaultType;
                    }
                    NewsPositionForMobileDal.SaveNewsPosition((int)typeId, newsPosition.ZoneId, newsPosition.Order,
                                                              newsPosition.NewsId, newsPosition.ExpiredLock,
                                                              newsPosition.Avatar, avatarIndex, checkNewsExists);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public virtual string UpdateAutoWhenChangeManualPosition(int zoneId, int typeId, string listNewsId, string excludeListTypeId, bool includeChildZone, int isOnhome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            try
            {
                // Giả sử update x bài vào vị trí nổi bật vùng manual
                // Lấy ra x vị trí trong vùng auto có newsid vừa update vào vùng manual
                var listNewsInCurrentPosition = NewsPositionDal.GetListNewsByListNewsId(typeId, zoneId, listNewsId);
                // Lấy top x bài mới nhất thỏa mãn: không thuộc vùng nổi bật manual, không nằm trong danh sách vùng auto
                var lastestNewsForAutoUpdate = NewsDal.GetTopLastestNewsForAutoUpdateNewsPosition(listNewsInCurrentPosition.Count, typeId, zoneId, excludeListTypeId, "", includeChildZone, isOnhome, displayInSlide, isBreakingNews, displayPosition, isFocus);
                // update x bài mới này vào các vị trí đã lấy được ở #2
                var updatedListNewsId = "";
                for (var i = 0; i < listNewsInCurrentPosition.Count && i < lastestNewsForAutoUpdate.Count; i++)
                {
                    if (NewsPositionDal.UpdateNewsPositionForAutoUpdate(listNewsInCurrentPosition[i].Id,
                                                                        lastestNewsForAutoUpdate[i]))
                    {
                        updatedListNewsId += ";" + lastestNewsForAutoUpdate[i].NewsId;
                    }
                }
                if (!string.IsNullOrEmpty(updatedListNewsId)) updatedListNewsId = updatedListNewsId.Remove(0, 1);
                return updatedListNewsId;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return "";
            }
        }

        public virtual ErrorMapping.ErrorCodes UpdateAutoWhenEditNews(bool isChangeDistributionDate, int zoneId, int typeId, long newsId, string excludeListTypeId, bool includeChildZone, int isOnhome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            try
            {
                var existsNews = NewsDal.GetNewsForValidateById(newsId);

                if (isChangeDistributionDate)
                {
                    var positionType = NewsPositionDal.GetNewsPositionTypeById(typeId);

                    if (positionType != null)
                    {
                        if (positionType.AllowAutoUpdate)
                        {
                            // Kiểm tra bài xem có đang nằm trong danh sách auto hay không?
                            var newsInPosition = NewsPositionDal.GetNewsInfoInPosition(typeId, zoneId, newsId);
                            Logger.WriteLog(Logger.LogType.Warning,
                                            string.Format("GetNewsInfoInPosition({0}, {1}, {2}) => {3}", typeId, zoneId,
                                                          newsId, NewtonJson.Serialize(newsInPosition)));
                            // Nếu đang nằm trong danh sách auto:
                            if (newsInPosition != null)
                            {
                                // Lấy top 1 bài mới nhất không nằm trong danh sách auto (lấy cả Id của bài vào và sắp xếp theo thời gian)
                                var lastestNewsForAutoUpdate = NewsDal.GetTopLastestNewsForAutoUpdateNewsPosition(1, typeId, zoneId, excludeListTypeId, newsId.ToString(), includeChildZone, isOnhome, displayInSlide, isBreakingNews, displayPosition, isFocus);
                                // Nếu Id của bài lấy được khác với bài bị sửa thì update bài này vào vị trí của bài bị sửa
                                if (lastestNewsForAutoUpdate != null && lastestNewsForAutoUpdate[0].NewsId != newsId)
                                {
                                    // Nếu Id của bài lấy được khác với bài bị sửa thì update bài này vào vị trí của bài bị sửa
                                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsInPosition.Id, lastestNewsForAutoUpdate[0]);
                                }
                                else // Nếu Id của bài lấy được trùng với bài bị sửa thì update DistributionDate vào vị trí của bài bị sửa
                                {
                                    NewsPositionDal.UpdateDistributionDate(newsInPosition.Id, existsNews.DistributionDate);
                                }
                            }
                            else // Nếu không nằm trong danh sách auto:
                            {
                                var countNewsByDistributionDate = NewsPositionDal.GetCountNewsInPositionByDistributionDate(typeId, zoneId, existsNews.DistributionDate);
                                if (countNewsByDistributionDate > 0)
                                {
                                    var newsForUpdate = NewsPublishDal.GetByNewsIdAndZoneId(zoneId, newsId, excludeListTypeId, isOnhome, displayInSlide, isBreakingNews, displayPosition, isFocus);
                                    if (newsForUpdate.Count > 0)
                                    {
                                        var oldestPosition = NewsPositionDal.GetOldestPosition(typeId, zoneId);
                                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(oldestPosition.Id, newsForUpdate[0]);
                                    }
                                }
                            }
                        }
                    }
                }
                // Update newsinfo for all position
                NewsPositionDal.UpdateNewsInfoIntoNewsPosition(newsId);
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public virtual ErrorMapping.ErrorCodes UpdateAutoWhenEditNewsV2(bool isChangeDistributionDate, int zoneId, int typeId, long newsId, string excludeListTypeId, bool includeChildZone, int isOnhome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            try
            {
                var existsNews = NewsDal.GetNewsForValidateById(newsId);

                if (isChangeDistributionDate)
                {
                    var positionType = NewsPositionDal.GetNewsPositionTypeById(typeId);

                    if (positionType != null)
                    {
                        if (positionType.AllowAutoUpdate)
                        {
                            Logger.WriteLog(Logger.LogType.Trace, "===== BEGIN =====");
                            // Kiểm tra bài xem có đang nằm trong danh sách auto hay không?
                            var newsInPosition = NewsPositionDal.GetNewsInfoInPosition(typeId, zoneId, newsId);
                            Logger.WriteLog(Logger.LogType.Warning,
                                            string.Format("GetNewsInfoInPosition({0}, {1}, {2}) => {3}", typeId, zoneId,
                                                          newsId, NewtonJson.Serialize(newsInPosition)));
                            Logger.WriteLog(Logger.LogType.Trace, "Bai trong danh sach auto => " + NewtonJson.Serialize(newsInPosition));
                            // Nếu đang nằm trong danh sách auto:
                            if (newsInPosition != null)
                            {
                                // Lấy bài mới nhất trong NewsPublish không nằm trong PositionTypeId đang xét và không nằm trong ExcludePositionTypeId
                                var excludePositionTypeId = typeId + (string.IsNullOrEmpty(excludeListTypeId) ? "" : ";" + excludeListTypeId);
                                var lastestNewsNotInPosition = NewsPublishDal.GetLastestNewsExcludeListPositionType(newsId, zoneId, excludePositionTypeId, includeChildZone, isOnhome, displayInSlide, isBreakingNews, displayPosition, isFocus);
                                Logger.WriteLog(Logger.LogType.Trace, "Bai moi nhat trong NewsPublish => " + NewtonJson.Serialize(lastestNewsNotInPosition));
                                // Có bài
                                if (lastestNewsNotInPosition != null)
                                {
                                    if (lastestNewsNotInPosition.NewsId != newsInPosition.NewsId)
                                    {
                                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsInPosition.Id, lastestNewsNotInPosition);
                                    }


                                    // Lấy top 1 bài cũ nhất (kể cả newsId vừa lấy được)
                                    //var oldestNewsInPosition = NewsPositionDal.GetOldestPositionIncludeNewsId(typeId, zoneId, lastestNewsNotInPosition.NewsId);
                                    //Logger.WriteLog(Logger.LogType.Trace, "Bai cu nhat trong NewsPosition (kem bai moi nhat)  [" + typeId + ", " + zoneId + ", " + lastestNewsNotInPosition.NewsId + "] => " + NewtonJson.Serialize(oldestNewsInPosition));
                                    //// Nếu bài cũ nhất lây được không phải là bài ở bước 1 -> update bài ở bước 1 vào vị trí bài cũ nhất
                                    //if (oldestNewsInPosition.NewsId != lastestNewsNotInPosition.NewsId)
                                    //{
                                    //    Logger.WriteLog(Logger.LogType.Trace, "UpdateNewsPositionForAutoUpdate[" + oldestNewsInPosition.Id + "] => " + NewtonJson.Serialize(oldestNewsInPosition));
                                    //    NewsPositionDal.UpdateNewsPositionForAutoUpdate(oldestNewsInPosition.Id, lastestNewsNotInPosition);
                                    //}
                                }
                            }
                            else // Nếu không nằm trong danh sách auto:
                            {
                                // Lấy top 1 bài cũ nhất (kể cả newsId vừa lấy được)
                                var oldestNewsInPosition = NewsPositionDal.GetOldestPositionIncludeNewsId(typeId, zoneId, newsId);
                                Logger.WriteLog(Logger.LogType.Trace, "Bai cu nhat trong newsPosition (kem bai dang xu ly) [" + typeId + ", " + zoneId + ", " + newsId + "] => " + NewtonJson.Serialize(oldestNewsInPosition));
                                // Nếu bài cũ nhất lây được không phải là bài ở bước 1 -> update bài ở bước 1 vào vị trí bài cũ nhất
                                if (oldestNewsInPosition.NewsId != newsId)
                                {
                                    var currentNews = NewsPublishDal.GetByNewsIdAndZoneId(newsId, zoneId);
                                    Logger.WriteLog(Logger.LogType.Trace, "currentNews(" + newsId + ", " + zoneId + ") => " + NewtonJson.Serialize(currentNews));

                                    Logger.WriteLog(Logger.LogType.Trace, "UpdateNewsPositionForAutoUpdate[" + oldestNewsInPosition.Id + "] => " + NewtonJson.Serialize(oldestNewsInPosition));
                                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(oldestNewsInPosition.Id, currentNews);
                                }
                            }
                            Logger.WriteLog(Logger.LogType.Trace, "===== END =====");
                        }
                    }
                }
                // Update newsinfo for all position
                NewsPositionDal.UpdateNewsInfoIntoNewsPosition(newsId);
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }



        #region NewsPositionSchedule
        public virtual ErrorMapping.ErrorCodes InserNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            try
            {
                return NewsPositionScheduleDal.Insert(newsPosition) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes UpdateNewsPositionSchedule(NewsPositionScheduleEntity newsPosition)
        {
            try
            {
                return NewsPositionScheduleDal.Update(newsPosition) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes DeleteNewsPositionSchedule(int newsPositionScheduleId)
        {
            try
            {
                return NewsPositionScheduleDal.Delete(newsPositionScheduleId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual List<NewsPositionScheduleEntity> GetListNewsPositionScheduleGetByPositionTypeId(int positionTypeId)
        {
            try
            {
                return NewsPositionScheduleDal.GetListScheduleByPositionTypeId(positionTypeId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return new List<NewsPositionScheduleEntity>();
        }
        public virtual List<NewsPositionEntity> GetListNewsPositionScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId)
        {
            try
            {
                return NewsPositionScheduleDal.GetListScheduleByPositionTypeIdAndOrder(typeId, order, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return new List<NewsPositionEntity>();
        }
        public virtual ErrorMapping.ErrorCodes AutoUpdateNewsPositionSchedule(int maxProcess)
        {
            var listProcessedId = "";
            //using (var reader = NewsPositionScheduleDal.GetUnprocessItem(maxProcess))
            //{
            //    while (reader.Read())
            //    {
            //        var typeId = Utility.ConvertToInt(reader["PositionTypeId"]);
            //        var zoneId = Utility.ConvertToInt(reader["ZoneId"]);
            //        var order = Utility.ConvertToInt(reader["Order"]);
            //        var newsId = Utility.ConvertToLong(reader["NewsId"]);


            //        if (typeId > 0 && order > 0 && newsId > 0)
            //        {
            //            NewsPositionDal.SaveNewsPosition(typeId, zoneId, order,
            //                                             newsId, DateTime.Now.AddDays(1),
            //                                             "",
            //                                             0, true);
            //            listProcessedId += ";" + reader["Id"];
            //        }
            //    }
            //    reader.Close();
            //}
            List<NewsPositionScheduleEntity> lst = NewsPositionScheduleDal.GetUnprocessItem(maxProcess);
            foreach (var item in lst)
            {
                var typeId = Utility.ConvertToInt(item.PositionTypeId);
                var zoneId = Utility.ConvertToInt(item.ZoneId);
                var order = Utility.ConvertToInt(item.Order);
                var newsId = Utility.ConvertToLong(item.NewsId);


                if (typeId > 0 && order > 0 && newsId > 0)
                {
                    NewsPositionDal.SaveNewsPosition(typeId, zoneId, order,
                                                     newsId, DateTime.Now.AddDays(1),
                                                     "",
                                                     0, true);
                    listProcessedId += ";" + item.Id;
                }
            }
            if (!string.IsNullOrEmpty(listProcessedId))
            {
                listProcessedId = listProcessedId.Remove(0, 1);
                NewsPositionScheduleDal.UpdateProcessedState(listProcessedId);
            }
            return ErrorMapping.ErrorCodes.Success;
        }
        #endregion

        public virtual ErrorMapping.ErrorCodes UpdateNewsIntoAutoUpdatePosition(NewsEntity news, int primaryZoneId, string allZoneIdIncludePrimary)
        {
            allZoneIdIncludePrimary = allZoneIdIncludePrimary.Replace(";", ",");
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.LastestNewsByZone,
                allZoneIdIncludePrimary, news.Id, 0);
            NewsPositionDal.UpdateNewsIntoAutoUpdatePosition((int)NewsPositionType.HomeLastestNewsOnHome,
                "0", news.Id, primaryZoneId);

            if (news.DistributionDate <= DateTime.Now)
            {
                var listManualPositionTypeId = (int)NewsPositionType.HomeNewsFocus + "," +
                                           (int)NewsPositionType.NewsFocusByZone + "," +
                                           (int)NewsPositionType.LockedTimelineOnHome;
                NewsPositionDal.UpdateNewsIntoManualUpdatePosition(listManualPositionTypeId, news.Id);
            }
            else
            {
                var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(news.Id);
                foreach (var newsPosition in currentPositionHasNewsId)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone ||
                        newsPosition.TypeId == (int)NewsPositionType.LockedTimelineOnHome)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, news.Id, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }
            return ErrorMapping.ErrorCodes.Success;
        }
        public virtual ErrorMapping.ErrorCodes RemoveNewsFromPosition(long newsId, List<long> positionIds)
        {
            NewsPositionDal.RemoveNewsFromAutoUpdatePosition(0, newsId);

            var currentPositionHasNewsId = NewsPositionDal.GetAllPositionHasNewsId(newsId);
            foreach (var newsPosition in currentPositionHasNewsId)
            {
                if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                    newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone ||
                    newsPosition.TypeId == (int)NewsPositionType.LockedTimelineOnHome || (newsPosition.TypeId == (int)NewsPositionType.HomeLastestNewsOnHome && WcfMessageHeader.Current.Namespace == "TuoiTre"))
                {
                    var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                    NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                }
            }

            if (positionIds.Count > 0)
            {
                var ids = string.Join(",", positionIds);
                var data = NewsPositionDal.GetAllPositionHasIds(ids);
                foreach (var newsPosition in data)
                {
                    if (newsPosition.TypeId == (int)NewsPositionType.HomeNewsFocus ||
                        newsPosition.TypeId == (int)NewsPositionType.NewsFocusByZone ||
                        //newsPosition.TypeId == (int)NewsPositionType.HomeLastestNewsOnHome ||
                        //newsPosition.TypeId == (int)NewsPositionType.LockedTimelineOnHome || (newsPosition.TypeId == (int)NewsPositionType.HomeLastestNewsOnHome && WcfMessageHeader.Current.Namespace == "TuoiTre"))
                        newsPosition.TypeId == (int)NewsPositionType.LockedTimelineOnHome)
                    {
                        var lastestNews = NewsPublishDal.GetLastestNewsByZoneIdExcludeNewsInPosition(newsPosition.ZoneId, newsId, newsPosition.TypeId);
                        NewsPositionDal.UpdateNewsPositionForAutoUpdate(newsPosition.Id, lastestNews);
                    }
                }
            }

            return ErrorMapping.ErrorCodes.Success;
        }

        public virtual List<NewsPositionEntity> GetAllPositionHasNewsId(long newsId)
        {
            return NewsPositionDal.GetAllPositionHasNewsId(newsId);
        }

        public virtual ErrorMapping.ErrorCodes LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock)
        {
            try
            {
                if (expiredLock <= DbCommon.MinDateTime) expiredLock = DateTime.Now.AddYears(1);

                return NewsPositionDal.LockPositionByType(newsPositionId, lockTypeId, order, expiredLock)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock)
        {
            try
            {
                if (expiredLock <= DbCommon.MinDateTime) expiredLock = DateTime.Now.AddYears(1);

                return NewsPositionDal.LockPositionByNewsId(newsId, zoneId, lockTypeId, order, expiredLock)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes UnlockPositionByType(int lockTypeId, int order)
        {
            try
            {
                return NewsPositionDal.UnlockPositionByType(lockTypeId, order)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        private static void NewTaskRunAction(System.Action action)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                try {
                    action();
                }
                catch { }
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }
    }
}
