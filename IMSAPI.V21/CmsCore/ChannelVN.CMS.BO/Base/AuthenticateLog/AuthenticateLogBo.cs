﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.AuthenticateLog;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.BO.Base.AuthenticateLog
{
    public class AuthenticateLogBo
    {
        public static ErrorMapping.ErrorCodes Insert(AuthenticateLogEntity authenticate, ref long newLogId)
        {
            try
            {
                var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
                if (ApiNodeJsEnable)
                {
                  if (ActivityBo.LogActionLogin(authenticate))
                        return ErrorMapping.ErrorCodes.Success;
                }
                else if (AuthenticateLogDal.Insert(authenticate, ref newLogId))
                {
                    //add redis
                    BoCached.Base.AuthenticateLog.AuthenticateLogDalFactory.Insert(newLogId, authenticate);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
