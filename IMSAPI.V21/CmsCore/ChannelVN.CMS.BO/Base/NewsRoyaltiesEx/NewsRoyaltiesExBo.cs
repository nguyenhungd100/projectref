﻿using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.DAL.Base.NewsRoyaltiesEx;
using ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BO.Base.NewsRoyaltiesEx
{
    public class NewsRoyaltiesExBo
    {
        public static List<NewsSearchEntity> SearchNewsRoyalty(string accountName, string zoneIds, string catIds, DateTime fromDate, DateTime toDate, int pageIndex, int pagaSize, ref int totalRows)
        {
            var result = new List<NewsSearchEntity>();
            var data = NewsRoyaltiesExDal.SearchNewsRoyalty(accountName, zoneIds, catIds, fromDate, toDate, pageIndex, pagaSize, ref totalRows);
            if (data != null && data.Count > 0)
            {
                //result = BoSearch.Base.News.NewsDalFactory.GetListNewsByListId(data.Select(s => s.PNewsID.ToString()).ToList());
                var rep = BoCached.Base.News.NewsDalFactory.GetNewsByListId(data.Select(s => s.PNewsID.ToString()).ToList());
                if (rep != null && rep.Count > 0)
                {
                    result.AddRange(rep.Select(s => new NewsSearchEntity
                    {
                        Id = s.Id,
                        EncryptId=s.EncryptId,
                        Title = s.Title,
                        Avatar = s.Avatar,
                        DistributionDate = s.DistributionDate,
                        ZoneIds = new string[] { s.ZoneId.ToString() },
                        ZoneName = s.ZoneName,
                        CreatedBy=s.CreatedBy,
                        CreatedDate=s.CreatedDate,
                        Author=s.Author,
                        ApprovedBy=s.ApprovedBy,
                        ApprovedDate=s.ApprovedDate,
                        DisplayPosition=s.DisplayPosition,
                        EditedBy=s.EditedBy,
                        IsOnHome=s.IsOnHome,
                        Note=s.Note,
                        Url=s.Url,
                        Type=s.Type,
                        PublishedBy=s.PublishedBy,
                        NewsType=s.NewsType,
                        Status=s.Status,
                        Source=s.Source,
                        Priority=s.Priority,
                        LastModifiedBy=s.LastModifiedBy
                    }));
                }
                else
                {
                    result.AddRange(data.Select(s => new NewsSearchEntity
                    {
                        Id = s.PNewsID                        
                    }));
                }
            }

            return result;
        }

        public static List<NewsCategoryEntity> GetListCateRoyalty()
        {
            return NewsRoyaltiesExDal.GetListCateRoyalty();
        }
    }
}
