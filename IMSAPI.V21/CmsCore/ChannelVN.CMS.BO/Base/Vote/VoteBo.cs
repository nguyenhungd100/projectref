﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Vote;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.Entity.ErrorCode;
using System.Linq;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BO.Base.Vote
{
    public class VoteBo
    {
        public static ErrorMapping.ErrorCodes Insert(VoteEntity vote, int zoneId, string zoneListId, ref int voteId)
        {
            try
            {
                if (null == vote)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return VoteDal.Insert(vote, zoneId, zoneListId, ref voteId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VoteDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes InsertAnswers(VoteAnswersEntity voteAnswers, ref int voteAnswersId)
        {
            return VoteDal.InsertAnswers(voteAnswers, ref voteAnswersId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes Update(VoteEntity vote, int zoneId, string zoneListId)
        {
            return VoteDal.Update(vote, zoneId, zoneListId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes UpdateAnswers(VoteAnswersEntity voteAnswers)
        {
            return VoteDal.UpdateAnswers(voteAnswers) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }


        public static ErrorMapping.ErrorCodes UpdatePriorityAnswers(string listAnswersId)
        {
            return VoteDal.UpdatePriorityVoteAnswers(listAnswersId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static VoteDetailEntity GetInfo(int id)
        {

            var voteDetailEntity = new VoteDetailEntity();
            voteDetailEntity.VoteInfo = (id < 0 ? null : VoteDal.GetById(id));
            if (id > 0)
            {
                voteDetailEntity.ListZoneInVote = VoteInZoneDal.GetVoteInZoneByVoteId(id);
                voteDetailEntity.ListAnswers = VoteDal.GetListAnswersByVoteId(id);
                try
                {
                    //Nếu có thông tin ở DB ngoài thì lấy, không thì lấy ở DB trong
                    //voteDetailEntity.ListAnswers = VoteDal.GetListAnswersByVoteIdExt(id);
                }
                catch (Exception ex)
                {
                    voteDetailEntity.ListAnswers = VoteDal.GetListAnswersByVoteId(id);
                }
            }
            else
            {
                voteDetailEntity.ListAnswers = null;
            }

            return voteDetailEntity;
        }

        public static VoteEntity GetVoteByNewsId(long id)
        {
            var vote = BoCached.Base.Vote.VoteDalFactory.GetVoteByNewsId(id);
            if (vote == null)
            {
                vote = VoteDal.GetByNewsId(id);
                if (vote != null)
                {
                    //add vote to redis
                    BoCached.Base.Vote.VoteDalFactory.AddVoteByNewsId(id,vote);
                }
            }
            return vote;
        }

        public static List<VoteEntity> GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            var list = new List<VoteEntity>();
            try
            {
                //var data = BoSearch.Base.Vote.VoteDalFactory.SearchVote(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
                //if(data!=null && data.Count > 0)
                //{
                //    list = BoCached.Base.Vote.VoteDalFactory.GetVoteByListId(data);
                //}
                //if(list==null || (null!=list && list.Count <= 0))
                //{
                //    list= VoteDal.GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
                //}

                list = VoteDal.GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
                return list;
            }
            catch
            {
                return list;
            }            
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsByVoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = VoteDal.InitESAllVote(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VoteDal.InitESAllVote(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new VoteSearchEntity
                {
                    Id = s.Id,
                    Title = s.Title,
                    ZoneIds = VoteInZoneDal.GetVoteInZoneByVoteId(s.Id).Select(se => se.ZoneId.ToString()).ToArray(),
                    CreatedDate = s.CreatedDate
                }).ToList();
                BoSearch.Base.Vote.VoteDalFactory.InitAllVote(list);
            }, action);
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByVoteAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = VoteDal.InitESAllVote(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VoteDal.InitESAllVote(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.Vote.VoteDalFactory.InitAllVote(data);
            }, action);
        }
        public static List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            var voteAnswers = VoteDal.GetListAnswersByVoteId(voteId);
            //try
            //{
            //    var extVoteAnswers = VoteDal.GetListAnswersByVoteIdExt(voteId);
            //    for (var i = 0; i < voteAnswers.Count; i++)
            //    {
            //        var ext = extVoteAnswers.Find(it => Utility.ConvertToInt(it.Value) == voteAnswers[i].Id);
            //        if (ext != null)
            //        {
            //            voteAnswers[i].VoteRate = ext.VoteRate;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            //}
            return voteAnswers;
        }

        public static VoteAnswersEntity GetAnswersDetail(int id)
        {
            return VoteDal.GetAnswersDetail(id);
        }

        public static ErrorMapping.ErrorCodes DeleteAnswers(int id)
        {
            return VoteDal.DeleteAnswers(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            return VoteDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes InsertVoteInNews(int voteId, long newsId)
        {
            try
            {
                return VoteDal.InsertVoteInNews(voteId, newsId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VoteDal.InsertVoteInNews:{0}", ex.Message));
            }

        }
    }
}
