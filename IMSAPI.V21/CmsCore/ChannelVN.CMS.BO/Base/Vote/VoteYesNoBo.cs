﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.DAL.Base.Vote;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Vote
{
    public class VoteYesNoBo
    {
        #region VoteYesNoGroup

        public static List<VoteYesNoGroupEntity> GetAllVoteYesNoGroup()
        {
            return VoteYesNoGroupDal.GetAll();
        }
        public static VoteYesNoGroupEntity GetVoteYesNoGroupByVoteYesNoGroupId(int voteYesNoGroupId)
        {
            return VoteYesNoGroupDal.GetById(voteYesNoGroupId);
        }

        public static ErrorMapping.ErrorCodes UpdateVoteYesNoGroupPriority(string sortedListIds)
        {
            return VoteYesNoGroupDal.UpdatePriority(sortedListIds)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes InsertVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup, ref int voteYesNoId)
        {
            return VoteYesNoGroupDal.Insert(voteYesNoGroup, ref voteYesNoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }
        public static ErrorMapping.ErrorCodes UpdateVoteYesNoGroup(VoteYesNoGroupEntity voteYesNoGroup)
        {
            var currentVoteGroup = VoteYesNoGroupDal.GetById(voteYesNoGroup.Id);
            if (currentVoteGroup == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            currentVoteGroup.GroupName = voteYesNoGroup.GroupName;
            currentVoteGroup.Status = voteYesNoGroup.Status;
            return VoteYesNoGroupDal.Update(currentVoteGroup)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }
        public static ErrorMapping.ErrorCodes DeleteVoteYesNoGroup(int voteYesNoGroupId)
        {
            return VoteYesNoGroupDal.Delete(voteYesNoGroupId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }

        #endregion

        #region VoteYesNo

        public static List<VoteYesNoEntity> SearchVoteYesNo(int voteYesNoGroupId, string keyword, int isFocus, EnumVoteYesNoStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VoteYesNoDal.Search(voteYesNoGroupId, keyword, isFocus, (int)status, pageIndex, pageSize, ref totalRow);
        }
        public static VoteYesNoEntity GetVoteYesNoByVoteYesNoId(int voteYesNoId)
        {
            return VoteYesNoDal.GetById(voteYesNoId);
        }

        public static ErrorMapping.ErrorCodes UpdateVoteYesNoPriority(string sortedListIds)
        {
            return VoteYesNoDal.UpdatePriority(sortedListIds)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes InsertVoteYesNo(VoteYesNoEntity voteYesNo, ref int NewsId)
        {
            voteYesNo.CreatedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            return VoteYesNoDal.Insert(voteYesNo, ref NewsId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }
        public static ErrorMapping.ErrorCodes UpdateVoteYesNo(VoteYesNoEntity voteYesNo)
        {
            var currentVote = VoteYesNoDal.GetById(voteYesNo.Id);
            if (currentVote == null)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            currentVote.Title = voteYesNo.Title;
            currentVote.Avatar = voteYesNo.Avatar;
            currentVote.YesAnswer = voteYesNo.YesAnswer;
            currentVote.NoAnswer = voteYesNo.NoAnswer;
            currentVote.Source = voteYesNo.Source;
            currentVote.Author = voteYesNo.Author;
            currentVote.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            currentVote.Status = voteYesNo.Status;
            return VoteYesNoDal.Update(currentVote)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }
        public static ErrorMapping.ErrorCodes DeleteVoteYesNo(int voteYesNoId)
        {
            return VoteYesNoDal.Delete(voteYesNoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError; 
        }

        #endregion
    }
}
