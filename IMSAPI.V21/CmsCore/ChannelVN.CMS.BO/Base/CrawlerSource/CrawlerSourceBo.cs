﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.CrawlerSource;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.CrawlerSource;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.AutoDiscussion;

namespace ChannelVN.CMS.BO.Base.CrawlerSource
{
    public class CrawlerSourceBo
    {
        public static ErrorMapping.ErrorCodes InsertCrawlerSource(CrawlerSourceTopicWrapperEntity news)
        {
            try
            {
                var inserted = CrawlerSourceDal.InsertCrawlerSource(news);
                if (inserted)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static List<CrawlerSourceEntity> GetSourceByTopic(string userName, int topicId)
        {
            try
            {
                return CrawlerSourceDal.GetSourceByTopic(userName, topicId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<CrawlerSourceEntity> GetSourceByUser(string userName)
        {
            try
            {
                return CrawlerSourceDal.GetSourceByUser(userName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<CrawlerSourceTopicEntity> GetTopicByCrawlerSource(int crawlerSourceId)
        {
            try
            {
                return CrawlerSourceDal.GetTopicByCrawlerSource(crawlerSourceId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

    }
}
