﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Base.Account;

namespace ChannelVN.CMS.BO.Base.Security
{
    public class PermissionBo
    {
        #region Business

        public static ErrorMapping.ErrorCodes GrantPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            var returnData = zoneId <= 0 ? ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound : UpdateUserPermission(username, permissionId, zoneId);
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(username);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes GrantPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity { UserId = userId, PermissionId = permissionId, ZoneId = zoneId };
            var returnData = UpdateUserPermission(userPermission);
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userId);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes GrantListPermission(string username, List<UserPermissionEntity> userPermissions)
        {
            var updateSuccess = true;
            try
            {
                RemoveAllUserPermissionByUserName(username);

                var permissionCount = userPermissions.Count;
                for (var i = 0; i < permissionCount; i++)
                {
                    var userPermission = userPermissions[i];
                    updateSuccess = updateSuccess &&
                                    (UpdateUserPermission(username, userPermission.PermissionId,
                                                                       userPermission.ZoneId) == ErrorMapping.ErrorCodes.Success);
                }
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(username);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return (updateSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError);
        }

        public static ErrorMapping.ErrorCodes GrantListPermission(int userId, List<UserPermissionEntity> userPermissions)
        {
            var updateSuccess = true;

            RemoveAllUserPermissionByUserId(userId);

            var permissionCount = userPermissions.Count;
            for (var i = 0; i < permissionCount; i++)
            {
                var userPermission = userPermissions[i];
                userPermission.UserId = userId;
                updateSuccess = updateSuccess &&
                                (UpdateUserPermission(userPermission) == ErrorMapping.ErrorCodes.Success);
            }
            if (updateSuccess)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userId);
            }
            return (updateSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError);
        }

        public static ErrorMapping.ErrorCodes IsUserHasPermissionInZone(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            var allZoneGranted = ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(username, permissionId);
            if (allZoneGranted.Exists(item => item.Id == zoneId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }

            var user = UserDal.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }
            // full quyền trên tất cả các chuyên mục
            if (user.IsFullPermission && user.IsFullZone)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            var permission = GetPermissionById(permissionId);
            // không full quyền trên tất cả các chuyên mục
            if (!user.IsFullPermission && !user.IsFullZone)
            {
                if (permission != null)
                {
                    return permission.IsGrantByCategory
                               ? CheckUserPermission(username, permissionId, zoneId)
                               : CheckUserPermission(username, permissionId);
                }
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (!permission.IsGrantByCategory && user.IsFullPermission) return ErrorMapping.ErrorCodes.Success;

            // Lấy danh sách quyền theo user
            var userPermissions = GetListByUserId(user.Id, true);
            int count = userPermissions.Count;
            // Nếu full quyền thì check chuyên mục xem có nằm trong danh sách quyền của user hay không
            if (user.IsFullPermission)
            {
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].ZoneId == zoneId)
                    {
                        return ErrorMapping.ErrorCodes.Success;
                    }
                }
            }
            else // Nếu full chuyên mục thì check quyền xem có nằm trong danh sách quyền của user hay không
            {
                for (var i = 0; i < count; i++)
                {
                    if (userPermissions[i].PermissionId == permissionId)
                    {
                        return ErrorMapping.ErrorCodes.Success;
                    }
                }
            }
            // Không có quyền thỏa mãn
            return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
        }

        public static ErrorMapping.ErrorCodes IsUserInGroupPermission(string username, int groupPermissionId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (groupPermissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            return CheckUserInGroupPermission(username, groupPermissionId);
        }

        #endregion

        public static ErrorMapping.ErrorCodes UpdateUserPermission(UserPermissionEntity userPermission)
        {
            var existsUser = UserDal.GetUserById(userPermission.UserId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var existsPermission = PermissionDal.GetPermissionById(userPermission.PermissionId);
            if (null == existsPermission)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            var existsZone = ZoneDal.GetZoneById(userPermission.ZoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }

            var returnData = UserPermissionDal.UpdateUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userPermission.UserId);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes UpdateUserPermission(string username, int permissionId, int zoneId)
        {
            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var existsPermission = PermissionDal.GetPermissionById(permissionId);
            if (null == existsPermission)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            var existsZone = ZoneDal.GetZoneById(zoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity
                                     {
                                         UserId = existsUser.Id,
                                         PermissionId = permissionId,
                                         ZoneId = zoneId
                                     };

            var returnData = UserPermissionDal.UpdateUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveUserPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity
                                     {
                                         UserId = userId,
                                         PermissionId = permissionId,
                                         ZoneId = zoneId
                                     };

            var existsUser = UserDal.GetUserById(userPermission.UserId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserPermissionDal.RemoveUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveUserPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }

            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var userPermission = new UserPermissionEntity
            {
                UserId = existsUser.Id,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            var returnData= UserPermissionDal.RemoveUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveAllUserPermissionByUserId(int userId)
        {
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserPermissionDal.RemoveAllUserPermission(existsUser.Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveAllUserPermissionByUserName(string username)
        {
            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserPermissionDal.RemoveAllUserPermission(existsUser.Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(UserPermissionEntity userPermission)
        {
            var existsUser = UserDal.GetUserById(userPermission.UserId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return UserPermissionDal.CheckUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId, int zoneId)
        {
            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            var userPermission = new UserPermissionEntity
            {
                UserId = existsUser.Id,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            return UserPermissionDal.CheckUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId)
        {
            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            var userPermission = GetListByUsernameAndPermissionId(username, permissionId);

            return userPermission.Count > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserInGroupPermission(string username, int groupId)
        {
            var existsUser = UserDal.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }

            return GroupPermissionDal.CheckUserInGroupPermission(existsUser.Id, groupId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static PermissionEntity GetPermissionById(int permissionId)
        {
            var per=BoCached.Base.Security.PermissionDalFactory.GetPermissionById(permissionId);
            if (per == null)
            {
                per = PermissionDal.GetPermissionById(permissionId);
                if (per != null)
                {
                    BoCached.Base.Security.PermissionDalFactory.AddPermission(per);
                }
            }
            return per;
        }
        public static List<PermissionEntity> GetListByGroupId(int groupPermissionId)
        {
            return PermissionDal.GetListPermissionByGroupId(groupPermissionId);
        }
        public static List<PermissionEntity> GetListByTemplateId(int permissionTemplateId)
        {
            return PermissionDal.GetListPermissionByTemplateId(permissionTemplateId);
        }

        public static List<PermissionEntity> GetPermissionByUsername(string username)
        {
            string filterInListPermissionId = Convert.ToInt32(EnumPermission.ArticleReporter) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleEditor) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleAdmin);

            return PermissionDal.GetListPermissionByUsername(username, filterInListPermissionId);
        }

        public static List<UserPermissionEntity> GetPermissionByUsername2(string username)
        {
            var user= UserDataBo.GetUserByUsername(username);
            var userId = 0;
            if (user!=null){
                userId = user.Id;
            }
            return GetListByUserId(userId, false);
        }

        public static List<UserPermissionEntity> GetListByUserId(int userId, bool isGetChildZone)
        {
            try
            {
                var data = BoCached.Base.Security.PermissionDalFactory.GetListByUserId(userId, isGetChildZone);
                if (data == null || (data != null && data.Count() <= 0))
                {
                    data = UserPermissionDal.GetListUserPermissionByUserId(userId, isGetChildZone);
                    if (data != null && data.Count() > 0)
                    {
                        // Add to REDIS
                        BoCached.Base.Security.PermissionDalFactory.AddListUserPermissionByUserId(userId, isGetChildZone, data);
                    }
                }
                return data;                
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<UserPermissionEntity>();
            }
        }
        public static List<UserPermissionEntity> GetListByUserName(string username)
        {
            return UserPermissionDal.GetListUserPermissionByUserName(username);
        }
        public static List<UserPermissionEntity> GetListByUserIdAndZoneId(int userId, int zoneId)
        {
            return UserPermissionDal.GetListUserPermissionByUserIdAndZoneId(userId, zoneId);
        }
        public static List<UserPermissionEntity> GetListByUsernameAndZoneId(string username, int zoneId)
        {
            return UserPermissionDal.GetListUserPermissionByUsernameAndZoneId(username, zoneId);
        }
        public static List<UserPermissionEntity> GetListByUserIdAndPermissionId(int userId, int permissionId)
        {
            return UserPermissionDal.GetListUserPermissionByUserIdAndPermissionId(userId, permissionId);
        }
        public static List<UserPermissionEntity> GetListByUsernameAndPermissionId(string username, int permissionId)
        {
            return UserPermissionDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);
        }

        public static List<PermissionTemplateEntity> GetAllPermissionTemplate()
        {
            return PermissionTemplateDal.GetAllPermissionTemplate();
        }
        public static List<GroupPermissionEntity> GetAllPermissionGroup()
        {            
            return GroupPermissionDal.GetAllGroupPermission();
        }

        public static List<GroupPermissionDetailEntity> GetAllPermissionGroupDetail()
        {
            var groupPermissionDetails = new List<GroupPermissionDetailEntity>();
            try {
                groupPermissionDetails = BoCached.Base.Security.PermissionDalFactory.GetAllPermissionGroupDetail();

                if (groupPermissionDetails == null || (groupPermissionDetails != null && groupPermissionDetails.Count() <= 0))
                {
                    groupPermissionDetails = new List<GroupPermissionDetailEntity>();

                    var groupPermissions = GetAllPermissionGroup();

                    groupPermissionDetails.AddRange(
                        groupPermissions.Select(groupPermission => new GroupPermissionDetailEntity
                        {
                            Id = groupPermission.Id,
                            Name = groupPermission.Name,                            
                            PermissionList = PermissionDal.GetListPermissionByGroupId(groupPermission.Id)
                        }));
                    if (groupPermissionDetails != null && groupPermissionDetails.Count() > 0)
                    {
                        // Add to REDIS
                        BoCached.Base.Security.PermissionDalFactory.AddAllPermissionGroupDetail(groupPermissionDetails);
                    }
                }

                return groupPermissionDetails;
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return groupPermissionDetails;
            }
        }

        #region PermissionTemplate
        public static List<PermissionTemplateEntity> PermissionTemplateGetAll()
        {
            return PermissionTemplateDal.GetAllPermissionTemplate();
        }
        public static ErrorMapping.ErrorCodes PermissionTemplateUpdate(PermissionTemplateEntity template)
        {
            return PermissionTemplateDal.UpdatePermissionTemplate(template) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes PermissionTemplateDelete(int templateId)
        {
            return PermissionTemplateDal.DeletePermissionTemplate(templateId) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<PermissionTemplateDetailEntity> GetListPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            return PermissionTemplateDal.GetListPermissionTemplateDetailByTemplateId(templateId, isGetChildZone);
        }

        public static List<PermissionTemplateDetailEntity> GetPermisstionTemplateDetailById(int templateId)
        {
            return PermissionTemplateDal.GetPermisstionTemplateDetailById(templateId);
        }

        public static ErrorMapping.ErrorCodes PermissionTemplateDetailUpdate(int templateId, List<PermissionTemplateDetailEntity> permissionTemplates)
        {
            var updateSuccess = true;

            RemoveAllPermissionTemplateDetailByPermissionTemplateId(templateId);

            var permissionCount = permissionTemplates.Count;
            for (var i = 0; i < permissionCount; i++)
            {
                var permissionTemplate = permissionTemplates[i];
                //permissionTemplate.TemplateId = templateId;
                updateSuccess = updateSuccess && (PermissionTemplateDetailUpdate(permissionTemplate) == ErrorMapping.ErrorCodes.Success);
            }
            return (updateSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError);
        }
        public static ErrorMapping.ErrorCodes PermissionTemplateDetailUpdate(PermissionTemplateDetailEntity permissionTemplate)
        {
           
            //var existsPermission = PermissionDal.GetPermissionById(permissionTemplate.PermissionId);
            //if (null == existsPermission)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            //}

            //var existsZone = ZoneDal.GetZoneById(permissionTemplate.ZoneId);
            //if (null == existsZone)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            //}

            return PermissionTemplateDal.PermissionTemplateDetailUpdate(permissionTemplate) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes RemoveAllPermissionTemplateDetailByPermissionTemplateId(int templateId)
        {          
            return PermissionTemplateDal.DeleteAllPermissionTemplateDetail(templateId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static PermissionTemplateWithPermissionDetailEntity GetPermissionTemplateWithPermissionDetailByTemplateId(int templateId, bool isGetChildZone)
        {            
            var templateWithPermissionDetailEntity = new PermissionTemplateWithPermissionDetailEntity
            {
                AllGroupPermission = PermissionBo.GetAllPermissionGroupDetail(),
                AllParentZone = ZoneBo.GetListParentZoneActivedByParentId(),
                PermissionTemplateDetailList = PermissionBo.GetListPermissionDetailByTemplateId(templateId, isGetChildZone)
            };
            return templateWithPermissionDetailEntity;
        }
        public static PermissionTemplateEntity GetPermissionTemplateById(int templateId)
        {
            return PermissionTemplateDal.GetPermissionTemplateById(templateId);
        }
        public static ErrorMapping.ErrorCodes PermissionTemplateUpdate(int templateId, string templateName)
        {
            return PermissionTemplateDal.PermissionTemplateUpdate(templateId, templateName) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes PermissionTemplateInsert(string templateName, ref int id)
        {
            return PermissionTemplateDal.PermissionTemplateInsert(templateName, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

    }
}
