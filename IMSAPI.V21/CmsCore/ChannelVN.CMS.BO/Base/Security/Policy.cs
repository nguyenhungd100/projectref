﻿using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Security
{
    public class Policy
    {
        public static ErrorMapping.ErrorCodes ValidAccount(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }
            if (user.Password != Crypton.Encrypt(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            UserBo.AddnewSmsCodeForUser(user.Id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes ValidAccountWithMd5EncryptPassword(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }
            if (Crypton.Md5Encrypt(Crypton.Decrypt(user.Password)).ToLower() != password.ToLower())
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            UserBo.AddnewSmsCodeForUser(user.Id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes ValidSmsCode(string username, string smsCode)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(smsCode))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode;
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            var userSms = UserBo.GetUserSmsCode(user.Id);
            if (null == userSms || userSms.SmsCode != smsCode)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode;
            }
            UserBo.RemoveInUsingState(user.Id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes ChangePassword(string username, string oldPassword, string newPassword, string accountNameLogin)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (string.IsNullOrEmpty(oldPassword))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }

            if (oldPassword != newPassword)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch;
            }

            var user = UserBo.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            return user.Status != (int)UserStatus.Actived ? ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked : UserBo.ChangePassword(user.Id, oldPassword, newPassword, accountNameLogin);
        }
        public static ErrorMapping.ErrorCodes ResetPassword(string encryptUserId, string newPassword)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }

            return UserBo.ResetPassword(userId, newPassword);
        }
        public static ErrorMapping.ErrorCodes ChangeStatus(string username, UserStatus userStatus)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (userStatus == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            var user = UserBo.GetUserByUsername(username);
            var returnData = (null == user ? ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername : UserBo.ChangeStatus(user.Id, userStatus));

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(user.Id);
            }
            return returnData;
        }
    }
}
