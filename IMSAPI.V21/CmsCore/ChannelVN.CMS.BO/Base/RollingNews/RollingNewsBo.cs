﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.BO.Base.RollingNews
{
    public class RollingNewsBo
    {
        #region RollingNews

        //public static List<RollingNewsEntity> SearchRollingNews(string keyword, EnumRollingNewsStatus status, int pageIndex, int pageSize,ref int totalRow)
        //{
        //    return RollingNewsDal.Search(keyword, (int)status, pageIndex, pageSize, ref totalRow);
        //}
        public static List<NodeJs_RollingNewsEntity> SearchRollingNews(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            //call nodejs
            return RollingNewsNodeJsServices.RollingNews_Search(fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        //public static List<RollingNewsForSuggestionEntity> SearchRollingNewsForSuggestion(string keyword, EnumRollingNewsStatus status, int top)
        //{
        //    return RollingNewsDal.SearchForSuggestion(keyword, (int)status, top);
        //}
        public static NodeJs_RollingNewsEntity GetRollingNewsByRollingNewsId(string rollingNewsId)
        {
            //return RollingNewsDal.GetById(rollingNewsId);

            //goi nodejs
            return RollingNewsNodeJsServices.RollingNews_GetById(rollingNewsId);
        }
        //public static ErrorMapping.ErrorCodes InsertRollingNews(RollingNewsEntity rollingNews)
        //{
        //    rollingNews.CreatedBy = WcfMessageHeader.Current.ClientUsername;
        //    return RollingNewsDal.Insert(rollingNews)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}
        public static ErrorMapping.ErrorCodes InsertRollingNewsV2(NodeJs_RollingNews rollingNews, ref string Id)
        {
            //rollingNews.CreatedBy = WcfMessageHeader.Current.ClientUsername;
            //return RollingNewsDal.InsertV2(rollingNews, ref Id)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;

            //goi nodejs
            var response = RollingNewsNodeJsServices.RollingNews_Insert(rollingNews);
            Id = response.Data;
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateRollingNews(NodeJs_RollingNews rollingNews)
        {
            rollingNews.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;

            if (RollingNewsNodeJsServices.RollingNews_Update(rollingNews).Success)
            {
                //try
                //{
                //    var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace,"RedisPublishDb"));

                //    var statusDataKeyFormat =
                //        ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace,"RedisKeyFormatForCmsRollingNewsStatus");
                //    var statusDataKey = string.Format(statusDataKeyFormat, rollingNews.Id);

                //    var redisClient = new RedisStackClient(dbNumber);
                //    redisClient.Remove(statusDataKey);
                //    redisClient.Add(statusDataKey, (int)rollingNews.Status);
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                //}
                //// Start Save Rolling News Info
                //try
                //{
                //    var dbNumber = Utility.ConvertToInt( ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));

                //    var statusDataKeyFormat =
                //        ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsInfo");
                //    var statusDataKey = string.Format(statusDataKeyFormat, rollingNews.Id);
                //    Logger.WriteLog(Logger.LogType.Trace, string.Format("Key {0}, db {1}, info {2}", statusDataKey, dbNumber, NewtonJson.Serialize(rollingNews)));
                //    var redisClient = new RedisStackClient(dbNumber);
                //    redisClient.Remove(statusDataKey);
                //    redisClient.Add<NodeJs_RollingNews>(statusDataKey, rollingNews, DateTime.Now.AddMonths(1).TimeOfDay);
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        //public static ErrorMapping.ErrorCodes DeleteRollingNews(int rollingNewsId)
        //{
        //    return RollingNewsDal.Delete(rollingNewsId)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}
        //public static ErrorMapping.ErrorCodes ChangeStatusRollingNews(int rollingNewsId, EnumRollingNewsStatus status)
        //{
        //    if (RollingNewsDal.ChangeStatus(rollingNewsId, (int)status, WcfMessageHeader.Current.ClientUsername))
        //    {
        //        var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));

        //        var statusDataKeyFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsStatus");
        //        var statusDataKey = string.Format(statusDataKeyFormat, rollingNewsId);

        //        var redisClient = new RedisStackClient(dbNumber);

        //        redisClient.Remove(statusDataKey);
        //        redisClient.Add(statusDataKey, (int)status);

        //        return ErrorMapping.ErrorCodes.Success;
        //    }
        //    else
        //    {
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}

        //public static ErrorMapping.ErrorCodes PublishRollingIntoNewsContentOnly(int rollingNewsId)
        //{
        //    try
        //    {
        //        var rollingNewsEventHtmlFormatForAllContent = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RollingNewsEventHtmlFormat");
        //        if (string.IsNullOrEmpty(rollingNewsEventHtmlFormatForAllContent))
        //        {
        //            rollingNewsEventHtmlFormatForAllContent = "<div class=\"livenews-item\" rel=\"{0}\"><i class=\"ico-type icon-{1}\"></i><p class=\"published-date\">{2}</p><p class=\"note\">{3}</p><div class=\"inner clearfix\"><div class=\"news-content\">{4}</div></div></div>";
        //        }
        //        var allRollingNewsEventContent = "";
        //        var rollingNewsEvents = GetRollingNewsEventByRollingNewsId(rollingNewsId,
        //                                                                           EnumRollingNewsEventType.AllType,
        //                                                                           EnumRollingNewsEventStatus.Published);
        //        if (rollingNewsEvents != null && rollingNewsEvents.Count > 0)
        //        {
        //            foreach (var rollingNewsEvent in rollingNewsEvents)
        //            {
        //                EnumRollingNewsEventType eventType;
        //                if (!Enum.TryParse(rollingNewsEvent.EventType.ToString(), out eventType))
        //                {
        //                    eventType = EnumRollingNewsEventType.BreakingNews;
        //                }

        //                allRollingNewsEventContent += string.Format(rollingNewsEventHtmlFormatForAllContent,
        //                    rollingNewsEvent.Id,
        //                    eventType.ToString(),
        //                    rollingNewsEvent.EventTime.ToString("HH:mm - dd/MM/yyyy"),
        //                    rollingNewsEvent.EventNote,
        //                    rollingNewsEvent.EventContent);
        //            }

        //            var newsUseRollingNews = NewsDal.GetNewsUseRollingNews(rollingNewsId);

        //            foreach (var news in newsUseRollingNews)
        //            {
        //                NewsDal.PublishNewsContentOnly(news.Id, allRollingNewsEventContent);
        //            }
        //        }
        //        return ErrorMapping.ErrorCodes.Success;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error,
        //                        "PublishRollingNewsContentOnly(" + rollingNewsId + ")" + ex);
        //    }
        //    return ErrorMapping.ErrorCodes.BusinessError;
        //}

        #endregion

        #region RollingNewsAuthor

        //public static List<RollingNewsAuthorEntity> GetRollingNewsAuthorByRollingNewsId(int rollingNewsId)
        //{
        //    return RollingNewsAuthorDal.GetByRollingNewsId(rollingNewsId);
        //}
        //public static RollingNewsAuthorEntity GetRollingNewsAuthorByRollingNewsAuthorId(int rollingNewsAuthorId)
        //{
        //    return RollingNewsAuthorDal.GetById(rollingNewsAuthorId);
        //}
        //public static ErrorMapping.ErrorCodes InsertRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        //{
        //    return RollingNewsAuthorDal.Insert(rollingNewsAuthor)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}
        //public static ErrorMapping.ErrorCodes UpdateRollingNewsAuthor(RollingNewsAuthorEntity rollingNewsAuthor)
        //{
        //    return RollingNewsAuthorDal.Update(rollingNewsAuthor)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}
        //public static ErrorMapping.ErrorCodes DeleteRollingNewsAuthor(int rollingNewsAuthorId)
        //{
        //    return RollingNewsAuthorDal.Delete(rollingNewsAuthorId)
        //               ? ErrorMapping.ErrorCodes.Success
        //               : ErrorMapping.ErrorCodes.BusinessError;
        //}

        #endregion

        #region RollingNewsEvent

        public static List<NodeJs_RollingNewsEventEntity> GetRollingNewsEventByRollingNewsId(string rollingNewsId)
        {
            return RollingNewsNodeJsServices.RollingNewsItem_GetListByRollingNewsId(rollingNewsId);
        }
        public static NodeJs_RollingNewsEventEntity GetRollingNewsEventByRollingNewsEventId(string rollingNewsEventId)
        {
            return RollingNewsNodeJsServices.RollingNewsItem_GetById(rollingNewsEventId);
        }
        public static ErrorMapping.ErrorCodes InsertRollingNewsEvent(NodeJs_RollingNewsEventEntity rollingNewsEvent, ref string Id)
        {
            rollingNewsEvent.CreatedBy = WcfMessageHeader.Current.ClientUsername;
            var response = RollingNewsNodeJsServices.RollingNews_Event_Insert(rollingNewsEvent);
            Id = response.Data;
            if (response.Success)
            {
                //if (rollingNewsEvent.Status == (int)EnumRollingNewsEventStatus.Published)
                //{
                //    UpdateRollingNewsEventIntoRedis(rollingNewsEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        //public static ErrorMapping.ErrorCodes UpdateRollingNewsEventV2(List<RollingNewsEventEntity> rollingNewsEvent, int rollingNewsId)
        //{
        //    try
        //    {
        //        var rollingNews = GetRollingNewsByRollingNewsId(rollingNewsId);
        //        if (rollingNews != null)
        //        {
        //            int success = 0;
        //            bool isPublish = false;
        //            string Ids = "";
        //            //RollingNewsEventDal.DeleteByRollingNewsId(rollingNewsId);
        //            foreach (var item in rollingNewsEvent)
        //            {
        //                item.CreatedBy = WcfMessageHeader.Current.ClientUsername;
        //                item.RollingNewsId = rollingNewsId;
        //                int Id = 0;
        //                if (item.Id > 0)
        //                {
        //                    RollingNewsBo.UpdateRollingNewsEvent(item);
        //                    Ids += ";" + item.Id;
        //                    success++;
        //                }
        //               else if (RollingNewsEventDal.InsertV2(item, ref Id))
        //                {
        //                    Ids += ";" + Id;
        //                    success++;
        //                }
        //                if (item.Status == (int)EnumRollingNewsEventStatus.Published && isPublish == false) isPublish = true;
        //            }
        //            if (Ids != "")
        //            {
        //                Ids = Ids.Substring(1);
        //                RollingNewsEventDal.DeleteByIds(rollingNewsId, Ids);
        //            }
        //            if (success > 0 & isPublish)
        //            {
        //                UpdateRollingNewsEventIntoRedis(rollingNewsId);
        //            }
        //            return ErrorMapping.ErrorCodes.Success;
        //        }
        //        else
        //            return ErrorMapping.ErrorCodes.UnknowError;

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //}
        public static ErrorMapping.ErrorCodes UpdateRollingNewsEvent(NodeJs_RollingNewsEventEntity rollingNewsEvent)
        {
            rollingNewsEvent.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
            var success = RollingNewsNodeJsServices.RollingNews_Event_Update(rollingNewsEvent).Success;
            if (success)
            {
                //var currentEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEvent.Id);
                //if (currentEvent != null && currentEvent.Status == (int)EnumRollingNewsEventStatus.Published)
                //{
                //    UpdateRollingNewsEventIntoRedis(currentEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteRollingNewsEvent(string rollingNewsEventId)
        {
            //var rollingNewsEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);

            var success = RollingNewsNodeJsServices.RollingNews_Event_Delete(rollingNewsEventId).Success;
            if (success)
            {
                //if (rollingNewsEvent != null && rollingNewsEvent.Status == (int)EnumRollingNewsEventStatus.Published)
                //{
                //    UpdateRollingNewsEventIntoRedis(rollingNewsEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteRollingNewsEventByRollingNewsId(string rollingNewstId)
        {
            var success = RollingNewsNodeJsServices.RollingNews_Event_Delete_By_RollingNewsId(rollingNewstId).Success;
            if (success)
            {
                //UpdateRollingNewsEventIntoRedis(rollingNewstId);
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteRollingNewsEventByListIds(string ids)
        {
            var success = RollingNewsNodeJsServices.RollingNews_Event_Delete_By_ListIds(ids).Success;
            if (success)
            {
                //UpdateRollingNewsEventIntoRedis(rollingNewstId);
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes PublishRollingNewsEvent(string id, string rollingnews_id, string lastModifiedBy)
        {
            var success = RollingNewsNodeJsServices.RollingNews_Event_Publish(id, rollingnews_id, lastModifiedBy).Success;
            if (success)
            {
                //var rollingNewsEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);
                //if (rollingNewsEvent != null)
                //{
                //    UpdateRollingNewsEventIntoRedis(rollingNewsEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UnpublishRollingNewsEvent(string id, string rollingnews_id, string lastModifiedBy)
        {
            var success = RollingNewsNodeJsServices.RollingNews_Event_UnPublish(id, rollingnews_id, lastModifiedBy).Success;
            if (success)
            {
                //var rollingNewsEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);
                //if (rollingNewsEvent != null)
                //{
                //    UpdateRollingNewsEventIntoRedis(rollingNewsEvent.RollingNewsId);
                //}
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        //public static ErrorMapping.ErrorCodes UpdateIsFocusForRollingNewsEvent(int rollingNewsEventId, bool isFocus)
        //{
        //    var success = RollingNewsEventDal.UpdateIsFocus(rollingNewsEventId, isFocus, WcfMessageHeader.Current.ClientUsername);
        //    if (success)
        //    {
        //        //var rollingNewsEvent = GetRollingNewsEventByRollingNewsEventId(rollingNewsEventId);
        //        //if (rollingNewsEvent != null &&
        //        //    rollingNewsEvent.Status == (int)EnumRollingNewsEventStatus.Published)
        //        //{
        //        //    UpdateRollingNewsEventIntoRedis(rollingNewsEvent.RollingNewsId);
        //        //}
        //        return ErrorMapping.ErrorCodes.Success;
        //    }
        //    return ErrorMapping.ErrorCodes.BusinessError;
        //}

        //public static ErrorMapping.ErrorCodes UpdateRollingNewsEventIntoRedis(int rollingNewsId)
        //{
        //    try
        //    {
        //        //var rollingNewsEventHtmlFormatForAllContent = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RollingNewsEventHtmlFormat");
        //        //if (string.IsNullOrEmpty(rollingNewsEventHtmlFormatForAllContent))
        //        //{
        //        //    rollingNewsEventHtmlFormatForAllContent = "<div class=\"livenews-item\" rel=\"{0}\"><i class=\"ico-type icon-{1}\"></i><p class=\"published-date\">{2}</p><p class=\"note\">{3}</p><div class=\"inner clearfix\"><div class=\"news-content\">{4}</div></div></div>";
        //        //}
        //        //var allRollingNewsEventContent = "";
        //        var rollingNewsEvents = GetRollingNewsEventByRollingNewsId(rollingNewsId,
        //                                                                           EnumRollingNewsEventType.AllType,
        //                                                                           EnumRollingNewsEventStatus.Published);
        //        if (rollingNewsEvents != null && rollingNewsEvents.Count > 0)
        //        {
        //            var allData = new List<RollingNewsEventInfo>();
        //            var focusData = new List<RollingNewsEventInfo>();

        //            foreach (var rollingNewsEvent in rollingNewsEvents)
        //            {
        //                var eventInfo = new RollingNewsEventInfo
        //                {
        //                    Id = rollingNewsEvent.Id,
        //                    IsFocus = rollingNewsEvent.IsFocus,
        //                    EventNote = rollingNewsEvent.EventNote,
        //                    EventContent = rollingNewsEvent.EventContent,
        //                    EventType = rollingNewsEvent.EventType,
        //                    EventTime = rollingNewsEvent.EventTime,
        //                    MobileContent = rollingNewsEvent.MobileContent,
        //                    ShortContent = rollingNewsEvent.ShortContent,
        //                    Images = rollingNewsEvent.Images,
        //                    ImageCount = rollingNewsEvent.ImageCount
        //                };
        //                if (eventInfo.IsFocus)
        //                {
        //                    focusData.Add(eventInfo);
        //                }
        //                allData.Add(eventInfo);

        //                EnumRollingNewsEventType eventType;
        //                if (!Enum.TryParse(rollingNewsEvent.EventType.ToString(), out eventType))
        //                {
        //                    eventType = EnumRollingNewsEventType.BreakingNews;
        //                }

        //                //allRollingNewsEventContent += string.Format(rollingNewsEventHtmlFormatForAllContent,
        //                //    rollingNewsEvent.Id,
        //                //    eventType.ToString(),
        //                //    rollingNewsEvent.EventTime.ToString("HH:mm - dd/MM/yyyy"),
        //                //    rollingNewsEvent.EventNote,
        //                //    rollingNewsEvent.EventContent);
        //            }

        //            var dbNumber = Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb"));

        //            var allDataKeyFormat = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsData");
        //            var allDataKey = string.Format(allDataKeyFormat, rollingNewsId);
        //            var redisClient = new RedisStackClient(dbNumber);
        //            redisClient.Remove(allDataKey);
        //            redisClient.Add<List<RollingNewsEventInfo>>(allDataKey, allData, DateTime.Now.AddMonths(1).TimeOfDay);

        //            //var newsUseRollingNews = NewsDal.GetNewsUseRollingNews(rollingNewsId);

        //            //foreach (var news in newsUseRollingNews)
        //            //{
        //            //    NewsDal.PublishNewsContentOnly(news.Id, allRollingNewsEventContent);
        //            //}


        //            //var focusDataKeyFormat = ChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisKeyFormatForCmsRollingNewsHotData");
        //            //var focusDataKey = string.Format(focusDataKeyFormat, rollingNewsId);
        //            //RedisHelper.Remove(dbNumber, focusDataKey);
        //            //RedisHelper.Add(dbNumber, focusDataKey, focusData);
        //        }
        //        return ErrorMapping.ErrorCodes.Success;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error,
        //                        "UpdateRollingNewsEventIntoRedis(" + rollingNewsId + ")" + ex);
        //    }
        //    return ErrorMapping.ErrorCodes.BusinessError;
        //}

        #endregion
    }
}
