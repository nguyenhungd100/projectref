﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.TemplateEmbed;
using ChannelVN.CMS.Entity.Base.TemplateEmbed;

namespace ChannelVN.CMS.BO.Base.TemplateEmbed
{
    public class TemplateEmbedBo
    {
        public static List<TemplateEmbedEntity> SearchTemplateEmbed(string keyword, int channelId, int typeId, EnumTemplateEmbedStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return TemplateEmbedDal.SearchTemplateEmbed(keyword, channelId, typeId, (int)status, pageIndex, pageSize, ref totalRow);
        }
    }
}
