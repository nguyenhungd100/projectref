﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BoCached.CacheObjectV2;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.BoCached.Entity.Security;
using System.Threading.Tasks;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.BO.Base.Account
{
    public class UserDataBo
    {
        /// <summary>
        /// Expired on 5 minutes
        /// </summary>
        private const int SmsExpired = 5;

        #region USER
        public static ErrorMapping.ErrorCodes AddNew(UserEntity user, string accountName, ref int newUserId, ref string newEncryptUserId)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            // Check in REDIS & Mssql
            var existsUser = GetUserByUsername(user.UserName);
            if (null != existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUsernameExists;
            }

            user.Password = Crypton.Encrypt(user.Password);
            try
            {
                if (UserDal.AddnewUser(user, ref newUserId))
                {
                    newEncryptUserId = CryptonForId.EncryptId(newUserId);

                    // Add to REDIS
                    user.Id = newUserId;
                    user.CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);

                    BoCached.Base.Account.UserDalFactory.AddUser(user);

                    // Add to ES                    
                    BoSearch.Base.Account.UserDalFactory.AddUser(user);

                    //log add user
                    ActivityBo.LogAddUser(accountName, user.UserName);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes Update(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }
            try
            {
                user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

                var existsUser = GetById(user.Id);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }
                // Nếu không update password thì lấy password cũ
                if (string.IsNullOrEmpty(user.Password))
                {
                    user.Password = existsUser.Password;
                }
                var accountLogin = GetUserByUsername(accountName);
                if (accountLogin == null)
                    return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

                if (existsUser.IsSystem && !existsUser.IsSystem)
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                var returnData = UserDal.UpdateUserById(user)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    // Change in REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateProfile(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }
            user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

            var existsUser = GetById(user.Id);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            // Nếu không update thì lấy cũ
            if (string.IsNullOrEmpty(user.Password))
            {
                user.Password = existsUser.Password;
            }
            if (string.IsNullOrEmpty(user.FullName))
            {
                user.FullName = existsUser.FullName;
            }
            //cho phép update email và số đt
            //if (string.IsNullOrEmpty(user.Email))
            //{
            //    user.Email = existsUser.Email;
            //}
            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    user.Mobile = existsUser.Mobile;
            //}
            if (string.IsNullOrEmpty(user.Description))
            {
                user.Description = existsUser.Description;
            }

            var accountLogin = GetUserByUsername(accountName);
            if (accountLogin == null)
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

            var existsAccount = UserDataCached.GetUserById(user.Id);
            if (existsAccount.IsSystem && !accountLogin.IsSystem)
                return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

            existsUser.Address = user.Address;
            existsUser.Avatar = user.Avatar;
            if (user.Birthday > DbCommon.MinDateTime) existsUser.Birthday = user.Birthday;
            existsUser.FullName = user.FullName;
            existsUser.Mobile = user.Mobile;
            existsUser.ModifiedDate = DateTime.Now;
            existsUser.Password = user.Password;
            existsUser.Status = user.Status;
            existsUser.Description = user.Description;
            existsUser.Email = user.Email;
            existsUser.StaffCode = user.StaffCode;
            existsUser.TelegramId = user.TelegramId;
            existsUser.DepartmentId = user.DepartmentId;
            try
            {
                var returnData = UserDal.UpdateUserById(existsUser)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    // Change in REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
                    // Change in ES
                    BoSearch.Base.Account.UserDalFactory.UpdateUser(existsUser);

                    //log update user profile
                    ActivityBo.LogUpdateProfile(accountName, existsUser.UserName);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes Delete(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.DeleteUserById(userId)
                ? ErrorMapping.ErrorCodes.Success
                : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                // Change in REDIS
                BoCached.Base.Account.UserDalFactory.DeleteUserById(userId);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes UpdateAvatar(string userName, string avatar)
        {
            //return UserDal.UpdateUserAvatar(userName, avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            var returnData = UserDal.UpdateUserAvatar(userName, avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                // Change in REDIS
                var user = GetUserByUsername(userName);
                if (null != user)
                {
                    user.Avatar = avatar;
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes UpdateTelegramId(string userName, long telegramId)
        {            
            var returnData = UserDal.UpdateTelegramId(userName, telegramId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                // Change in REDIS
                var user = GetUserByUsername(userName);
                if (null != user)
                {
                    if(telegramId<=0)
                        user.TelegramId = null;
                    else
                        user.TelegramId = telegramId;

                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes ChangeStatus(int userId, UserStatus status)
        {
            //var existsUser = UserDataCached.GetUserById(userId);

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserStatusByById(userId, (int)status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                ////Xóa cached
                //UserDataCached userCached = new UserDataCached();
                //userCached.RemoveCachedByPrefix(existsUser.UserName);
                //PermissionDataCached perCached = new PermissionDataCached();
                //perCached.RemoveCachedByPrefix(existsUser.UserName);

                // Change in REDIS
                existsUser.Status = (int)status;
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes ChangePassword(int userId, string oldPassword, string newPassword, string accountNameLogin)
        {
            try
            {
                //var existsUser = UserDataCached.GetUserById(userId);

                var existsUser = GetById(userId);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }

                oldPassword = Crypton.Encrypt(oldPassword);
                if (existsUser.Password != oldPassword)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
                }

                var accountLogin = GetUserByUsername(accountNameLogin);
                // Nếu account đăng nhập ko phải account hệ thống
                // và Account được reset pass là account hệ thống
                // Thì ko được sửa pass
                if (accountLogin == null || (!accountLogin.IsSystem && existsUser.IsSystem))
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                var returnData = UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword))
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    //UserDataCached userCached = new UserDataCached();
                    //userCached.RemoveCachedByPrefix(existsUser.UserName);

                    // Change in REDIS
                    existsUser.Password = Crypton.Encrypt(newPassword);
                    BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes ResetPassword(int userId, string newPassword)
        {
            //var existsUser = UserDataCached.GetUserById(userId);

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword)) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //UserDataCached userCached = new UserDataCached();
                //userCached.RemoveCachedByPrefix(existsUser.UserName);

                // Change in REDIS
                existsUser.Password = Crypton.Encrypt(newPassword);
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes ResetPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            if (string.IsNullOrEmpty(oldPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
            if (string.IsNullOrEmpty(cfNewPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidRetypePassword;

            //var existsUser = UserDataCached.GetUserByUsername(accountName);

            var existsUser = GetUserByUsername(accountName);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            newPassword = Crypton.Encrypt(newPassword);
            oldPassword = Crypton.Encrypt(oldPassword);

            if (existsUser.Password != oldPassword)
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;

            var returnData = UserDal.UpdateUserPasswordByById(existsUser.Id, newPassword) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //UserDataCached userCached = new UserDataCached();
                //userCached.RemoveCachedByPrefix(existsUser.UserName);

                // Change in REDIS
                existsUser.Password = newPassword;
                existsUser.LastChangePass = DateTime.Now;
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
            }
            return returnData;
        }
        public static UserEntity GetById(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            return GetById(userId);
        }
        public static UserEntity GetById(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }
            // Check in REDIS
            var user = BoCached.Base.Account.UserDalFactory.GetUserById(userId);

            if (null == user)
            {
                user = UserDal.GetUserById(userId);
                if (null != user)
                {
                    // Add to REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
            }
            else
            {
                user.EncryptId = CryptonForId.EncryptId(user.Id);
                //user.Id = 0;                
            }
            return user;
        }
        public static UserEntity GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }
                // Check in REDIS
                var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(username.ToLower());

                if (null == user)
                {
                    user = UserDal.GetUserByUsername(username);
                    if (null != user)
                    {
                        // Add to REDIS
                        BoCached.Base.Account.UserDalFactory.AddUser(user);
                    }
                }
                else
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    //user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserCachedEntity GetUserCachedByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }
                // Check in REDIS
                var userInfo = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(username.ToLower());

                if (null == userInfo)
                {
                    userInfo = new UserCachedEntity();
                    userInfo.User = UserDal.GetUserByUsername(username);
                    if (null != userInfo.User)
                    {
                        // Add to REDIS
                        BoCached.Base.Account.UserDalFactory.AddUser(userInfo.User);
                        // Add to Es
                        BoSearch.Base.Account.UserDalFactory.AddUser(userInfo.User);
                    }
                }
                //else
                //{
                if (userInfo!=null && null != userInfo.User)
                {
                    userInfo.User.EncryptId = CryptonForId.EncryptId(userInfo.User.Id);
                    //user.Id = 0;

                    //if (userInfo.PermissionList.Count == 0)
                    //{
                        userInfo.PermissionList = PermissionBo.GetListByUserId(userInfo.User.Id, true);
                    //}
                }
                //}
                return userInfo;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static UserEntity GetUserByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    return null;
                }
                var user = UserDal.GetUserByEmail(email);
                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion
        public static UserWithPermissionEntity GetUserWithPermissionByUserId(int userId)
        {
            try
            {
                var user = UserDataCached.GetUserById(userId);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                    {
                        User = user,
                        UserPermissions = PermissionDataCached.GetListUserPermissionByUserName(user.UserName) //UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                    };
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static UserWithPermissionEntity GetUserWithPermissionByUserName(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }

                var user = UserDataCached.GetUserByUsername(username);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                    {
                        User = user,
                        UserPermissions = PermissionDataCached.GetListUserPermissionByUserName(user.UserName)// UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                    };
                    if (null != userWithPermission.User)
                    {
                        userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                        userWithPermission.User.Id = 0;
                    }
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static ErrorMapping.ErrorCodes AddnewSmsCodeForUser(int userId)
        {
            var existsUser = UserDataCached.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (existsUser.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
            }

            var userSms = new UserSmsEntity
            {
                UserId = userId,
                SmsCode = Utility.GenerateRandomString(4),
                ExpiredDate = DateTime.Now.AddMinutes(SmsExpired)
            };

            return UserSmsDal.AddnewSmsCode(userSms) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes RemoveInUsingState(int userId)
        {
            var existsUser = UserDataCached.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (existsUser.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
            }

            return UserSmsDal.RemoveInUsing(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static UserSmsEntity GetUserSmsCode(int userId)
        {
            return UserSmsDal.GetUserSmsByUserId(userId);
        }
        public static ErrorMapping.ErrorCodes UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            if (UserDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey))
            {
                BoCached.Base.Account.UserDalFactory.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        #region Business
        public static UserStandardEntity GetUserStandardByUserId(string encryptUserId)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }

            var user = GetById(encryptUserId);

            if (null == user)
            {
                return null;
            }
            return new UserStandardEntity
            {
                Id = 0,
                EncryptId = encryptUserId,
                UserName = user.UserName,
                FullName = user.FullName,
                Avatar = user.Avatar,
                Email = user.Email,
                Mobile = user.Mobile,
                IsFullPermission = user.IsFullPermission,
                IsFullZone = user.IsFullZone,
                Status = user.Status
            };
        }
        public static UserWithPermissionEntity GetUserWithPermissionByUserId(string encryptUserId)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);

                if (userId <= 0)
                {
                    return null;
                }

                var userWithPermission = GetUserWithPermissionByUserId(userId);
                if (null != userWithPermission && null != userWithPermission.User)
                {
                    userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                    userWithPermission.User.Id = 0;
                }
                return userWithPermission;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion

        #region Permission
        public static ErrorMapping.ErrorCodes GrantPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            var returnData = zoneId <= 0 ? ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound : UpdateUserPermission(username, permissionId, zoneId);
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(username);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(username);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes GrantPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity { UserId = userId, PermissionId = permissionId, ZoneId = zoneId };
            var returnData = UpdateUserPermission(userPermission);
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                string Username = UserDataCached.GetUserById(userId).UserName;
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(Username);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(Username);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes GrantListPermission(string username, List<UserPermissionEntity> userPermissions)
        {
            var updateSuccess = true;
            try
            {
                RemoveAllUserPermissionByUserName(username);

                var permissionCount = userPermissions.Count;
                for (var i = 0; i < permissionCount; i++)
                {
                    var userPermission = userPermissions[i];
                    updateSuccess = updateSuccess &&
                                    (UpdateUserPermission(username, userPermission.PermissionId,
                                                                       userPermission.ZoneId) == ErrorMapping.ErrorCodes.Success);
                }

                //Xóa cached
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(username);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(username);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return (updateSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError);
        }
        public static ErrorMapping.ErrorCodes GrantListPermission(int userId, List<UserPermissionEntity> userPermissions)
        {            
            var updateSuccess = true;

            if (userPermissions!=null && userPermissions.Count>0)// && RemoveAllUserPermissionByUserId(userId) == ErrorMapping.ErrorCodes.Success)
            {
                updateSuccess = UpdateAllUserPermission(userId, userPermissions);
                if (updateSuccess)
                {
                    try {
                        //tao key uid=>listPerChild
                        var listPerChild = UserPermissionDal.GetListUserPermissionByUserId(userId, true);
                        if (listPerChild != null && listPerChild.Count > 0)
                        {
                            // Add to REDIS
                            BoCached.Base.Security.PermissionDalFactory.AddListUserPermissionByUserId(userId, true, listPerChild);
                        }
                        else
                        {
                            //update redis
                            //BoCached.Base.Account.UserDalFactory.AddPermissionListByUserId(userId, userPermissions);
                            BoCached.Base.Security.PermissionDalFactory.AddListUserPermissionByUserId(userId, true, userPermissions);
                        }
                    }
                    catch(Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error,ex.Message);
                        //update redis
                        BoCached.Base.Security.PermissionDalFactory.AddListUserPermissionByUserId(userId, true, userPermissions);
                    }
                }
            }

            //Logger.WriteLog(Logger.LogType.Trace, "Start: "+ userId+ " " + DateTime.Now.ToString("HH:mm:ss"));
            //TaskUpdateUserPermission(() =>
            //{                
            //    foreach (var userPermission in userPermissions)
            //    {                
            //        userPermission.UserId = userId;
            //        var errorCode = UpdateUserPermission(userPermission);
            //        if(errorCode != ErrorMapping.ErrorCodes.Success)
            //        {
            //            Logger.WriteLog(Logger.LogType.Trace, "TaskUpdateUserPermission: errorCode => " + ErrorMapping.Current[errorCode] + " userPermission: " + NewtonJson.Serialize(userPermission));
            //        }
            //        updateSuccess = updateSuccess && (errorCode == ErrorMapping.ErrorCodes.Success);
            //    }
            //    Logger.WriteLog(Logger.LogType.Trace, "End: "+ userId + " " + DateTime.Now.ToString("HH:mm:ss"));
            //});

            return (updateSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError);
        }

        private static void TaskUpdateUserPermission(System.Action action)
        {
            var task = Task.Run(()=> {
                try {
                    action();
                }
                catch { }
            });
            try {
                task.Wait(TimeSpan.FromSeconds(5));
            }
            catch { }
        }

        public static ErrorMapping.ErrorCodes IsUserHasPermissionInZone(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            var allZoneGranted = ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(username, permissionId);
            if (allZoneGranted.Exists(item => item.Id == zoneId))
            {
                return ErrorMapping.ErrorCodes.Success;
            }

            var user = UserBo.GetUserByUsername(username); //UserDataCached.GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }
            // full quyền trên tất cả các chuyên mục
            if (user.IsFullPermission && user.IsFullZone)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            
            var permission = PermissionBo.GetPermissionById(permissionId);
            // không full quyền trên tất cả các chuyên mục
            if (!user.IsFullPermission && !user.IsFullZone)
            {
                if (permission != null)
                {
                    return permission.IsGrantByCategory
                               ? CheckUserPermission(username, permissionId, zoneId)
                               : CheckUserPermission(username, permissionId);
                }
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (!permission.IsGrantByCategory && user.IsFullPermission) return ErrorMapping.ErrorCodes.Success;

            // Lấy danh sách quyền theo user
            var userPermissions = PermissionBo.GetListByUserId(user.Id, true); //PermissionDataCached.GetListUserPermissionByUserName(username);//GetListByUserId(user.Id, true);

            // Nếu full quyền thì check chuyên mục xem có nằm trong danh sách quyền của user hay không
            if (user.IsFullPermission)
            {                
                if (userPermissions.Where(k => k.ZoneId == zoneId).Count() > 0)
                    return ErrorMapping.ErrorCodes.Success;
            }
            else // Nếu full chuyên mục thì check quyền xem có nằm trong danh sách quyền của user hay không
            {                
                if (userPermissions.Where(k => k.PermissionId == permissionId).Count() > 0)
                    return ErrorMapping.ErrorCodes.Success;
            }
            // Không có quyền thỏa mãn
            return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
        }
        public static ErrorMapping.ErrorCodes IsUserInGroupPermission(string username, int groupPermissionId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            if (groupPermissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.ValidAccountNotHavePermission;
            }

            return CheckUserInGroupPermission(username, groupPermissionId);
        }
        public static bool UpdateAllUserPermission(int userId, List<UserPermissionEntity> userPermission)
        {
            var pageSize = 1000;
            var total = userPermission.Count;
            if(total<= pageSize)
            {
                return UserPermissionDal.UpdateAllUserPermission2(userId, userPermission);
            }

            var page = total / pageSize;
            if (total % pageSize > 0)
                page++;

            var result = false;
            UserPermissionDal.UpdateAllUserPermissionDelete(userId);
            for (int k = 0; k < page; k++)
            {
                var userPermissionPage = userPermission.Skip(k * pageSize).Take(pageSize).ToList();

                result = UserPermissionDal.UpdateAllUserPermission(userId, userPermissionPage);
            }

            return result;
        }
        public static ErrorMapping.ErrorCodes UpdateUserPermission(UserPermissionEntity userPermission)
        {
            //bo check user
            //var existsUser = BoCached.Base.Account.UserDalFactory.GetUserById(userPermission.UserId);
            //if (null == existsUser)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            //}

            //var existsPermission = BoCached.Base.Security.PermissionDalFactory.GetPermissionById(userPermission.PermissionId);
            var existsPermission = PermissionDal.GetPermissionById(userPermission.PermissionId);
            if (null == existsPermission)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            //Bỏ check exist zone vì có chung với zonevideo
            //var existsZone = ZoneDal.GetZoneById(userPermission.ZoneId);//var existsZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(userPermission.ZoneId);
            //if (null == existsZone)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            //}

            var returnData = UserPermissionDal.UpdateUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            return returnData;
        }
        public static ErrorMapping.ErrorCodes UpdateUserPermission(string username, int permissionId, int zoneId)
        {
            var existsUser = UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var existsPermission = PermissionDal.GetPermissionById(permissionId);
            if (null == existsPermission)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            var existsZone = ZoneDal.GetZoneById(zoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity
            {
                UserId = existsUser.Id,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            var returnData = UserPermissionDal.UpdateUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Xóa cached
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(username);
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(username);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveUserPermission(int userId, int permissionId, int zoneId)
        {
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }
            var userPermission = new UserPermissionEntity
            {
                UserId = userId,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            var existsUser = UserDataCached.GetUserById(userPermission.UserId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserPermissionDal.RemoveUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Xóa cached
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(existsUser.UserName);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(existsUser.UserName);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveUserPermission(string username, int permissionId, int zoneId)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (permissionId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountZoneNotFound;
            }

            var existsUser = UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var userPermission = new UserPermissionEntity
            {
                UserId = existsUser.Id,
                PermissionId = permissionId,
                ZoneId = zoneId
            };

            var returnData = UserPermissionDal.RemoveUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Xóa cached
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(username);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(username);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveAllUserPermissionByUserId(int userId)
        {
            //var existsUser = BoCached.Base.Account.UserDalFactory.GetUserById(userId);
            //if (null == existsUser)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            //}
            var returnData = UserPermissionDal.RemoveAllUserPermission(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Xóa cached
                BoCached.Base.Account.UserDalFactory.RemovePermissionListByUserId(userId);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes RemoveAllUserPermissionByUserName(string username)
        {
            var existsUser = UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserPermissionDal.RemoveAllUserPermission(existsUser.Id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Xóa cached
                UserDataCached userCached = new UserDataCached();
                userCached.RemoveCachedByPrefix(username);
                PermissionDataCached cached = new PermissionDataCached();
                cached.RemoveCachedByPrefix(username);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(UserPermissionEntity userPermission)
        {
            var existsUser = UserDataCached.GetUserById(userPermission.UserId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            //return UserPermissionDal.CheckUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            return PermissionDataCached.GetListUserPermissionByUserName(existsUser.UserName).Where(k => k.PermissionId == userPermission.PermissionId & k.ZoneId == userPermission.ZoneId).ToList().Count > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId, int zoneId)
        {
            var existsUser = UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }

            //return UserPermissionDal.CheckUserPermission(userPermission) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            //return PermissionDataCached.GetListUserPermissionByUserName(existsUser.UserName).Where(k => k.PermissionId == permissionId & k.ZoneId == zoneId).ToList().Count > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            
            //chinhnb
            return PermissionBo.GetListByUserId(existsUser.Id, true).Where(k => k.PermissionId == permissionId & k.ZoneId == zoneId).ToList().Count > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId)
        {
            var existsUser = UserBo.GetUserByUsername(username); //UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            //chinhnb
            var userPermission = PermissionBo.GetListByUserId(existsUser.Id, true).Where(k => k.PermissionId == permissionId).ToList(); //PermissionDataCached.GetListUserPermissionByUserName(username).Where(k => k.PermissionId == permissionId).ToList();
            return userPermission.Count > 0 ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes CheckUserInGroupPermission(string username, int groupId)
        {
            var existsUser = UserDataCached.GetUserByUsername(username);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            if (existsUser.IsFullPermission)
            {
                return ErrorMapping.ErrorCodes.Success;
            }

            return GroupPermissionDal.CheckUserInGroupPermission(existsUser.Id, groupId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<PermissionEntity> GetPermissionByUsername(string username)
        {
            string filterInListPermissionId = Convert.ToInt32(EnumPermission.ArticleReporter) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleEditor) + "," +
                                              Convert.ToInt32(EnumPermission.ArticleAdmin);
            return PermissionDal.GetListPermissionByUsername(username, filterInListPermissionId);
        }
        public static List<UserPermissionEntity> GetListByUserId(int userId, bool isGetChildZone)
        {
            if (isGetChildZone)
                return PermissionDataCached.GetListUserPermissionByUserName(UserDataCached.GetUserById(userId).UserName);
            else
                return UserPermissionDal.GetListUserPermissionByUserId(userId, isGetChildZone);
        }
        public static List<UserPermissionEntity> GetListByUserName(string username)
        {
            return PermissionDataCached.GetListUserPermissionByUserName(username);
        }
        public static List<UserPermissionEntity> GetListByUserIdAndZoneId(int userId, int zoneId)
        {
            //return UserPermissionDal.GetListUserPermissionByUserIdAndZoneId(userId, zoneId);
            string username = UserDataCached.GetUserById(userId).UserName;
            return PermissionDataCached.GetListUserPermissionByUserName(username).Where(k => k.ZoneId == zoneId).ToList();
        }
        public static List<UserPermissionEntity> GetPermisionByUsernameAndZoneId(string username, int zoneId)
        {
            return PermissionDataCached.GetListUserPermissionByUserName(username).Where(k => k.ZoneId == zoneId).ToList();
        }
        public static List<UserPermissionEntity> GetListByUserIdAndPermissionId(int userId, int permissionId)
        {
            //return UserPermissionDal.GetListUserPermissionByUserIdAndPermissionId(userId, permissionId);
            string username = UserDataCached.GetUserById(userId).UserName;
            return PermissionDataCached.GetListUserPermissionByUserName(username).Where(k => k.PermissionId == permissionId).ToList();
        }
        public static List<UserPermissionEntity> GetListByUsernameAndPermissionId(string username, int permissionId)
        {
            //return UserPermissionDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);
            return PermissionDataCached.GetListUserPermissionByUserName(username).Where(k => k.PermissionId == permissionId).ToList();
        }
        #endregion

        #region Policy
        public static ErrorMapping.ErrorCodes ValidAccount(string username, string password, ref UserCachedEntity userInfo)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            //var user = GetUserByUsername(username);
            userInfo = GetUserCachedByUsername(username);

            if (userInfo!=null && null == userInfo.User)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }
            if (userInfo.User.Password != Crypton.Encrypt(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            if (userInfo.User.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            try {
                UserBo.AddnewSmsCodeForUser(userInfo.User.Id);
            }
            catch(Exception ex) {
                Logger.WriteLog(Logger.LogType.Debug, "AddnewSmsCodeForUser =>" + ex.Message);
            }

            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes CheckUser(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            var user = GetUserByUsername(username);

            if (null != user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUsernameExists;
            }

            return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
        }
        public static ErrorMapping.ErrorCodes ValidAccountWithMd5EncryptPassword(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(password))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            //var user = UserDataCached.GetUserByUsername(username);

            var user = GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }
            if (Crypton.Md5Encrypt(Crypton.Decrypt(user.Password)).ToLower() != password.ToLower())
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidPassword;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            UserBo.AddnewSmsCodeForUser(user.Id);
            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes ValidSmsCode(string username, string smsCode)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(smsCode))
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode;
            }

            //var user = UserDataCached.GetUserByUsername(username);

            var user = GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidUsername;
            }

            if (user.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.ValidAccountUserLocked;
            }

            var userSms = UserBo.GetUserSmsCode(user.Id);
            if (null == userSms || userSms.SmsCode != smsCode)
            {
                return ErrorMapping.ErrorCodes.ValidAccountInvalidSmsCode;
            }

            UserBo.RemoveInUsingState(user.Id);

            return ErrorMapping.ErrorCodes.Success;
        }
        public static ErrorMapping.ErrorCodes ChangePassword(string username, string oldPassword, string newPassword, string accountNameLogin)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (string.IsNullOrEmpty(oldPassword))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }

            if (oldPassword != newPassword)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountOldAndNewPasswordNotMatch;
            }

            //var user = UserDataBo.GetUserByUsername(username);

            var user = GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            var returnData = user.Status != (int)UserStatus.Actived
                ? ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked
                : UserBo.ChangePassword(user.Id, oldPassword, newPassword, accountNameLogin);
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //UserDataCached userCached = new UserDataCached();
                //userCached.RemoveCachedByPrefix(username);

                //Change in REDIS
                user.Password = newPassword;
                BoCached.Base.Account.UserDalFactory.AddUser(user);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes ResetPassword(string encryptUserId, string newPassword, string accountName)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (string.IsNullOrEmpty(newPassword))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }

            var returnData = UserBo.ResetPassword(userId, newPassword, accountName);
            //if (returnData == ErrorMapping.ErrorCodes.Success)
            //{
            //    UserDataCached userCached = new UserDataCached();
            //    userCached.RemoveCachedByPrefix(UserDataCached.GetUserById(userId).UserName);
            //}
            return returnData;
        }
        public static ErrorMapping.ErrorCodes ResetOTP(string encryptUserId, string accountName)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);
            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserBo.ResetOTP(userId);

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Change in REDIS      
                var user = BoCached.Base.Account.UserDalFactory.GetUserById(userId);                
                BoCached.Base.Account.UserDalFactory.UpdateOtpSecretKeyForUsername(user.UserName, "");

                //update permission
                ActivityBo.LogResetOtp(accountName, user.UserName);
            }

            return returnData;
        }
        public static ErrorMapping.ErrorCodes ChangeStatus(string username, UserStatus userStatus, string accountName)
        {
            if (string.IsNullOrEmpty(username))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (userStatus == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            var user = GetUserByUsername(username);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            var returnData = UserDal.UpdateUserStatusByById(user.Id, (int)userStatus)
                ? ErrorMapping.ErrorCodes.Success
                : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                user.Status = (int)userStatus;
                // Change in REDIS
                BoCached.Base.Account.UserDalFactory.AddUser(user);
                // Change in ES
                BoSearch.Base.Account.UserDalFactory.UpdateUser(user);

                //log change satus
                ActivityBo.LogUpdateStatusUser(accountName, username, (int)userStatus);
            }
            return returnData;
        }
        #endregion
    }
}
