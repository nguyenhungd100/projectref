﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Account
{
    public class UserPenNameBo
    {
        public static ErrorMapping.ErrorCodes EditPenName(UserPenNameEntity penName)
        {
            return UserPenNameDal.EditPenName(penName) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeletePenNameById(int id)
        {
            return UserPenNameDal.DeletePenNameById(id) == true ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize,
                                                            ref int totalRow)
        {
            return UserPenNameDal.SearchPenName(email, username, pageIndex, pageSize, ref totalRow);
        }
        public static UserPenNameEntity GetByVietId(long vietId)
        {
            return UserPenNameDal.GetPenNameByVietId(vietId);
        }
    }
}
