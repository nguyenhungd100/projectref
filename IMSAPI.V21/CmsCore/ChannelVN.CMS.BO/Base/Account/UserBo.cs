﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.SocialNetwork.BO;

namespace ChannelVN.CMS.BO.Base.Account
{
    public class UserBo
    {
        /// <summary>
        /// Expired on 5 minutes
        /// </summary>
        private const int SmsExpired = 5;

        public static ErrorMapping.ErrorCodes AddNew(UserEntity user, ref int newUserId, ref string newEncryptUserId)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidUsername;
            }

            if (string.IsNullOrEmpty(user.Password))
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            }
            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            //var existsUser = UserDal.GetUserByUsername(user.UserName);
            // Check in REDIS & Mssql
            var existsUser = GetUserByUsername(user.UserName);

            if (null != existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUsernameExists;
            }

            //existsUser = UserDal.GetUserByEmail(user.Email);
            //if (null != existsUser)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;
            //}

            user.Password = Crypton.Encrypt(user.Password);
            try
            {
                if (UserDal.AddnewUser(user, ref newUserId))
                {
                    newEncryptUserId = CryptonForId.EncryptId(newUserId);

                    // Add to REDIS
                    user.Id = newUserId;
                    BoCached.Base.Account.UserDalFactory.AddUser(user);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidMobile;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            try
            {
                user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

                //var existsUser = UserDal.GetUserById(user.Id);
                var existsUser = GetById(user.Id);

                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }
                // Nếu không update password thì lấy password cũ
                if (string.IsNullOrEmpty(user.Password))
                {
                    user.Password = existsUser.Password;
                }

                //var accountLogin = GetUserByUsername(accountName);
                //if (accountLogin == null)
                //    return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

                //var existsAccount = GetById(user.Id);
                //if (existsAccount.IsSystem && !accountLogin.IsSystem)
                //    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                ////existsUser = UserDal.GetUserByEmail(user.Email);
                ////if (null != existsUser && existsUser.Id != user.Id)
                ////{
                ////    return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;
                ////}
                //var returnData = UserDal.UpdateUserById(user) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                //if (returnData == ErrorMapping.ErrorCodes.Success)
                //{
                //    CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(user.Id);
                //    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(user.Id);
                //}
                //return returnData;

                if (existsUser.IsSystem && !existsUser.IsSystem)
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

                var returnData = UserDal.UpdateUserById(user) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    // Change in REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateProfile(UserEntity user, string accountName)
        {
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            //if (!Utility.IsValidEmail(user.Email))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidEmail;
            //}

            //if (string.IsNullOrEmpty(user.Mobile))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountInvalidMobile;
            //}

            UserStatus status;
            if (!Enum.TryParse(user.Status.ToString(), out status))
            {
                status = UserStatus.Unknow;
            }
            if (status == UserStatus.Unknow)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidStatus;
            }

            user.Id = CryptonForId.DecryptIdToInt(user.EncryptId);

            //var existsUser = UserDal.GetUserById(user.Id);

            var existsUser = GetById(user.Id);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            // Nếu không update password thì lấy password cũ
            if (string.IsNullOrEmpty(user.Password))
            {
                user.Password = existsUser.Password;
            }

            var accountLogin = GetUserByUsername(accountName);
            if (accountLogin == null)
                return ErrorMapping.ErrorCodes.UpdateAccountPermissionNotFound;

            var existsAccount = GetById(user.Id);
            if (existsAccount.IsSystem && !accountLogin.IsSystem)
                return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;

            //var checkUserEmail = UserDal.GetUserByEmail(user.Email);
            //if (null != checkUserEmail && checkUserEmail.Id != user.Id)
            //{
            //    if (existsAccount.Email!=user.Email)
            //    {
            //        return ErrorMapping.ErrorCodes.UpdateAccountEmailExists;   
            //    }                
            //}

            existsUser.Address = user.Address;
            existsUser.Avatar = user.Avatar;
            if (user.Birthday > DbCommon.MinDateTime) existsUser.Birthday = user.Birthday;
            existsUser.FullName = user.FullName;
            existsUser.Mobile = user.Mobile;
            existsUser.ModifiedDate = DateTime.Now;
            existsUser.Password = user.Password;
            //existsUser.Status = existsUser.Status;
            existsUser.Status = user.Status;
            existsAccount.Description = user.Description;
            existsAccount.Email = user.Email;

            try
            {
                var returnData = UserDal.UpdateUserById(existsUser)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    //CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(user.Id);

                    // Change in REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }
            var existsUser = UserDal.GetUserById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.DeleteUserById(userId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes UpdateAvatar(string userName, string avatar)
        {
            //return UserDal.UpdateUserAvatar(userName, avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            var returnData = UserDal.UpdateUserAvatar(userName, avatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                // Change in REDIS
                var user = GetUserByUsername(userName);
                if (null != user)
                {
                    user.Avatar = avatar;
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes ChangeStatus(int userId, UserStatus status)
        {
            //var existsUser = UserDal.GetUserById(userId);
            //if (null == existsUser)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            //}

            //var returnData = UserDal.UpdateUserStatusByById(userId, (int)status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            //if (returnData == ErrorMapping.ErrorCodes.Success)
            //{
            //    CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);
            //    CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(existsUser.Id);
            //}
            //return returnData;

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserStatusByById(userId, (int)status) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                // Change in REDIS
                existsUser.Status = (int)status;
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes ChangePassword(int userId, string oldPassword, string newPassword, string accountNameLogin)
        {
            try
            {
                //var existsUser = UserDal.GetUserById(userId);

                var existsUser = GetById(userId);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }

                oldPassword = Crypton.Encrypt(oldPassword);
                if (existsUser.Password != oldPassword)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
                }

                var accountLogin = GetUserByUsername(accountNameLogin);
                // Nếu account đăng nhập ko phải account hệ thống
                // và Account được reset pass là account hệ thống
                // Thì ko được sửa pass
                if (accountLogin == null || (!accountLogin.IsSystem && existsUser.IsSystem))
                    return ErrorMapping.ErrorCodes.UpdateAccountCantNotEditSystem;
                
                var returnData = UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword))
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    //CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);

                    // Change in REDIS
                    existsUser.Password = Crypton.Encrypt(newPassword);
                    BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ResetPassword(int userId, string newPassword, string accountName="")
        {
            //var existsUser = UserDal.GetUserById(userId);

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserPasswordByById(userId, Crypton.Encrypt(newPassword)) 
                ? ErrorMapping.ErrorCodes.Success 
                : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);

                // Change in REDIS
                existsUser.Password = Crypton.Encrypt(newPassword);
                existsUser.LastChangePass = DateTime.Now;
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);

                //log reset password                
                ActivityBo.LogResetPassword(accountName, existsUser.UserName);
            }
            return returnData;
        }

        public static ErrorMapping.ErrorCodes ResetOTP(int userId)
        {
            //var existsUser = UserDal.GetUserById(userId);

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            var returnData = UserDal.UpdateUserOTPByById(userId)
                ? ErrorMapping.ErrorCodes.Success
                : ErrorMapping.ErrorCodes.UnknowError;
           
            return returnData;
        }

        public static ErrorMapping.ErrorCodes ResetPassword(string accountName, string newPassword, string cfNewPassword, string oldPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidPassword;
            if (string.IsNullOrEmpty(oldPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;
            if (string.IsNullOrEmpty(cfNewPassword))
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidRetypePassword;

            //var existsUser = UserDal.GetUserByUsername(accountName);

            var existsUser = GetUserByUsername(accountName);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            newPassword = Crypton.Encrypt(newPassword);
            oldPassword = Crypton.Encrypt(oldPassword);

            if (existsUser.Password != oldPassword)
                return ErrorMapping.ErrorCodes.UpdateAccountInvalidOldPassword;

            var returnData = UserDal.UpdateUserPasswordByById(existsUser.Id, newPassword)
                ? ErrorMapping.ErrorCodes.Success
                : ErrorMapping.ErrorCodes.UnknowError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //CacheObjectBase.GetInstance<UserCached>().RemoveAllCachedByGroup(existsUser.Id);

                // Change in REDIS
                existsUser.Password = newPassword;
                BoCached.Base.Account.UserDalFactory.AddUser(existsUser);
            }
            return returnData;
        }

        public static UserEntity GetById(string encryptUserId)
        {
            var userId = CryptonForId.DecryptIdToInt(encryptUserId);

            //if (userId <= 0)
            //{
            //    return null;
            //}
            //var user = UserDal.GetUserById(userId);
            //if (null != user)
            //{
            //    user.EncryptId = CryptonForId.EncryptId(user.Id);
            //    user.Id = 0;
            //}
            //return user;

            return GetById(userId);
        }

        public static UserEntity GetById(int userId)
        {
            if (userId <= 0)
            {
                return null;
            }
            //var user = UserDal.GetUserById(userId);

            // Check in REDIS
            var user = BoCached.Base.Account.UserDalFactory.GetUserById(userId);

            if (null == user)
            {
                user = UserDal.GetUserById(userId);
                if (null != user)
                {
                    // Add to REDIS
                    BoCached.Base.Account.UserDalFactory.AddUser(user);
                }
            }

            if (null != user)
            {
                user.EncryptId = CryptonForId.EncryptId(user.Id);
                //user.Id = 0;
            }
            return user;
        }

        public static UserEntity GetUserByUsername(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }                
                // Check in REDIS
                var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(username);
                
                if (null == user)
                {
                    user = UserDal.GetUserByUsername(username);
                    if (null != user)
                    {
                        // Add to REDIS
                        BoCached.Base.Account.UserDalFactory.AddUser(user);
                    }
                }

                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    //user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserEntity GetUserByEmail(string email)
        {
            try
            {
                if (string.IsNullOrEmpty(email))
                {
                    return null;
                }
                var user = UserDal.GetUserByEmail(email);
                if (null != user)
                {
                    user.EncryptId = CryptonForId.EncryptId(user.Id);
                    //user.Id = 0;
                }
                return user;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(int userId)
        {
            try
            {
                var user = UserDal.GetUserById(userId);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                                                 {
                                                     User = user,
                                                     UserPermissions = UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                                                 };
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserName(string username)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return null;
                }

                var user = UserDal.GetUserByUsername(username);
                if (null == user)
                {
                    return null;
                }
                else
                {
                    var userWithPermission = new UserWithPermissionEntity
                                                 {
                                                     User = user,
                                                     UserPermissions = UserPermissionDal.GetListUserPermissionByUserId(user.Id, true)
                                                 };
                    if (null != userWithPermission.User)
                    {
                        userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                        userWithPermission.User.Id = 0;
                    }
                    return userWithPermission;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<UserStandardEntity> SearchUser(string keyword,
                                                            UserStatus status,
                                                            UserSortExpression sortOrder,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
         {
            var userWithSimpleFields = new List<UserStandardEntity>();
            var users = new List<UserEntity>();

            var usersSearch = BoSearch.Base.Account.UserDalFactory.SearchUser(keyword, (int)status, (int)sortOrder, pageIndex, pageSize, ref totalRow);
            if(null != usersSearch && usersSearch.Count > 0)
            {
                users = BoCached.Base.Account.UserDalFactory.GetUserByListId(usersSearch);
            }

            if (users == null || (users != null && users.Count <= 0))
            {
                users = UserDal.SearchUser(keyword, (int)status, (int)sortOrder, pageIndex, pageSize, ref totalRow);

                if (null != users && users.Count > 0)
                {
                    //index es
                    BoSearch.Base.Account.UserDalFactory.InitUsersByStatus(users);
                    //init redis
                    BoCached.Base.Account.UserDalFactory.InitUsersByStatus(users);
                }
            }
            if (null != users && users.Count > 0)
            {                
                userWithSimpleFields = users.Select(s => new UserStandardEntity
                {
                    Id = s.Id,
                    UserName = s.UserName,
                    FullName = s.FullName,
                    Avatar = s.Avatar,
                    Email = s.Email,
                    Mobile = s.Mobile,
                    IsFullPermission = s.IsFullPermission,
                    IsFullZone = s.IsFullZone,
                    Status = s.Status,
                    StaffCode=s.StaffCode
                }).ToList();                
            }
            return userWithSimpleFields;
        }

        public static List<UserStandardEntity> SearchUser2(string keyword,
                                                            UserStatus status,                                                           
                                                            int start,
                                                            int rows, ref int totalRow)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();
            var users = new List<UserEntity>();

            var usersSearch = BoSearch.Base.Account.UserDalFactory.SearchUser2(keyword, (int)status, start, rows, ref totalRow);
            if (null != usersSearch && usersSearch.Count > 0)
            {
                users = BoCached.Base.Account.UserDalFactory.GetUserByListId(usersSearch);
            }            

            if (null != users && users.Count > 0)
            {
                userWithSimpleFields = users.Select(s => new UserStandardEntity
                {
                    Id = s.Id,
                    UserName = s.UserName,
                    FullName = s.FullName,
                    Avatar = s.Avatar,
                    Email = s.Email,
                    Mobile = s.Mobile,
                    IsFullPermission = s.IsFullPermission,
                    IsFullZone = s.IsFullZone,
                    Status = s.Status,
                    IsRole=s.IsRole,
                    StaffCode=s.StaffCode
                }).ToList();
            }
            return userWithSimpleFields;
        }

        /// <summary>
        /// Lấy danh sách user với quyền và zoneId theo userName
        /// check permission và zoneId của userName để lấy ra danh sách user tương ứng.
        /// Logic xử lý trong store
        /// </summary>
        /// <param name="permissionId"></param>
        /// <param name="zoneId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static List<UserStandardEntity> GetUserWithFullPermissionAndZoneId(EnumPermission permissionId, long newsId, string userName)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users = BoCached.Base.Account.UserDalFactory.GetUserWithFullPermissionAndZoneId((int)permissionId, newsId, userName);
            if (users == null)
            {
                users = UserDal.GetUserWithFullPermissionAndZoneId((int)permissionId, newsId, userName);
            }
            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount,
                        StaffCode= users[i].StaffCode
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionIdAndZoneList(EnumPermission permissionId, string zoneIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users=BoCached.Base.Account.UserDalFactory.GetUserByPermissionIdAndZoneList((int)permissionId, zoneIds);
            if (users == null)
            {
                users = UserDal.GetUserByPermissionIdAndZoneList((int)permissionId, zoneIds);
            }            

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount,
                        StaffCode = users[i].StaffCode
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetUserByPermissionListAndZoneList(string zoneIds, params EnumPermission[] permissionIds)
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var listPermissionId = permissionIds.Aggregate("",
                                                           (current, permissionId) =>
                                                           current + (";" + (int)permissionId));

            if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

            var users = UserDal.GetUserByPermissionListAndZoneList(listPermissionId, zoneIds);

            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount,
                        StaffCode = users[i].StaffCode
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }

        public static List<UserStandardEntity> GetUserFullPermisstion()
        {
            var userWithSimpleFields = new List<UserStandardEntity>();

            var users = BoCached.Base.Account.UserDalFactory.GetUserFullPermisstion();
            if (users == null)
            {
                users = UserDal.GetUserFullPermission();
            }
            if (null != users && users.Count > 0)
            {
                var count = users.Count;

                for (var i = 0; i < count; i++)
                {
                    var userInfo = new UserStandardEntity
                    {
                        Id = users[i].Id,
                        UserName = users[i].UserName,
                        FullName = users[i].FullName,
                        Avatar = users[i].Avatar,
                        Email = users[i].Email,
                        Mobile = users[i].Mobile,
                        IsFullPermission = users[i].IsFullPermission,
                        IsFullZone = users[i].IsFullZone,
                        Status = users[i].Status,
                        PermissionCount = users[i].PermissionCount,
                        StaffCode = users[i].StaffCode
                    };
                    userWithSimpleFields.Add(userInfo);
                }
            }
            return userWithSimpleFields;
        }
        public static List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(EnumPermission permissionId, int zoneId)
        {
            return UserDal.GetNormalUserByPermissionIdAndZoneId((int)permissionId, zoneId);
        }

        public static ErrorMapping.ErrorCodes AddnewSmsCodeForUser(int userId)
        {
            try
            {
                //var existsUser = UserDal.GetUserById(userId);

                var existsUser = GetById(userId);
                if (null == existsUser)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
                }

                if (existsUser.Status != (int)UserStatus.Actived)
                {
                    return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
                }

                var userSms = new UserSmsEntity
                {
                    UserId = userId,
                    SmsCode = Utility.GenerateRandomString(4),
                    ExpiredDate = DateTime.Now.AddMinutes(SmsExpired)
                };

                var returnData = UserSmsDal.AddnewSmsCode(userSms)
                    ? ErrorMapping.ErrorCodes.Success
                    : ErrorMapping.ErrorCodes.UnknowError;

                if (returnData == ErrorMapping.ErrorCodes.Success)
                {
                    //UserDataCached userCached = new UserDataCached();
                    //userCached.RemoveCachedByPrefix(username);

                    //Add in REDIS
                    BoCached.Base.Account.UserSmsDalFactory.AddnewSmsCode(userSms);
                }

                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "AddnewSmsCodeForUser =>" + ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes RemoveInUsingState(int userId)
        {
            //var existsUser = UserDal.GetUserById(userId);

            var existsUser = GetById(userId);
            if (null == existsUser)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserNotFound;
            }

            if (existsUser.Status != (int)UserStatus.Actived)
            {
                return ErrorMapping.ErrorCodes.UpdateAccountUserHasBeenLocked;
            }

            var returnData = UserSmsDal.RemoveInUsing(userId) 
                ? ErrorMapping.ErrorCodes.Success 
                : ErrorMapping.ErrorCodes.UnknowError;

            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Change in REDIS
                BoCached.Base.Account.UserSmsDalFactory.RemoveInUsing(userId);
            }
            return returnData;
        }

        public static UserSmsEntity GetUserSmsCode(int userId)
        {
            //return UserSmsDal.GetUserSmsByUserId(userId);

            var userSms = BoCached.Base.Account.UserSmsDalFactory.GetUserSmsByUserId(userId);
            if(null == userSms)
            {
                userSms = UserSmsDal.GetUserSmsByUserId(userId);
                if(null != userSms)
                {
                    BoCached.Base.Account.UserSmsDalFactory.AddnewSmsCode(userSms);
                }
            }
            return userSms;
        }

        public static string GetOtpSecretKeyByUsername(string username)
        {
            //return UserDal.GetOtpSecretKeyByUsername(username);

            var returnData = BoCached.Base.Account.UserDalFactory.GetOtpSecretKeyByUsername(username);
            if (string.IsNullOrEmpty(returnData))
            {
                returnData = UserDal.GetOtpSecretKeyByUsername(username);

                //Change in REDIS
                BoCached.Base.Account.UserDalFactory.UpdateOtpSecretKeyForUsername(username, returnData);
            }
            return returnData;
        }        

        public static List<UserSearchAutocompleteEntity> UserSearchAutocomplete(int ZoneId, string Keyword, bool isShowTKTS)
        {
            try
            {
                return UserDal.UserSearchAutocomplete(ZoneId, Keyword, isShowTKTS);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<UserSearchAutocompleteEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            var returnData = UserDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey)
                ? ErrorMapping.ErrorCodes.Success
                : ErrorMapping.ErrorCodes.BusinessError;
            if (returnData == ErrorMapping.ErrorCodes.Success)
            {
                //Add in REDIS
                BoCached.Base.Account.UserDalFactory.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            }
            return returnData;
        }
        public static ErrorMapping.ErrorCodes UserPermission_Insert(int userID, int tempId)
        {
            if (UserDal.UserPermission_Insert(userID, tempId))
            {
                CacheObjectBase.GetInstance<PermissionCached>().RemoveAllCachedByGroup(userID);
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        #region Business

        public static UserStandardEntity GetUserStandardByUserId(string encryptUserId)
        {
            int userId = CryptonForId.DecryptIdToInt(encryptUserId);

            if (userId <= 0)
            {
                return null;
            }

            var user = GetById(encryptUserId);

            if (null == user)
            {
                return null;
            }
            return new UserStandardEntity
            {
                Id = 0,
                EncryptId = encryptUserId,
                UserName = user.UserName,
                FullName = user.FullName,
                Avatar = user.Avatar,
                Email = user.Email,
                Mobile = user.Mobile,
                IsFullPermission = user.IsFullPermission,
                IsFullZone = user.IsFullZone,
                Status = user.Status,
                StaffCode = user.StaffCode
            };
        }

        public static UserWithPermissionEntity GetUserWithPermissionByUserId(string encryptUserId)
        {
            try
            {
                var userId = CryptonForId.DecryptIdToInt(encryptUserId);

                if (userId <= 0)
                {
                    return null;
                }

                var userWithPermission = GetUserWithPermissionByUserId(userId);
                if (null != userWithPermission && null != userWithPermission.User)
                {
                    userWithPermission.User.EncryptId = CryptonForId.EncryptId(userWithPermission.User.Id);
                    userWithPermission.User.Id = 0;
                }
                return userWithPermission;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static UserWithPermissionDetailEntity GetUserWithPermissionDetailByUserId(string encryptUserId, bool isGetChildZone)
        {
            try
            {
                int userId = CryptonForId.DecryptIdToInt(encryptUserId);

                var userWithPermissionDetail = new UserWithPermissionDetailEntity
                {
                    AllGroupPermission = PermissionBo.GetAllPermissionGroupDetail(),
                    AllParentZone = ZoneBo.GetListParentZoneActivedByParentId(),
                    AllParentZoneVideo = ZoneVideoBo.GetListByParentId(0,1),
                    User = GetById(userId),
                    UserPermissionList = PermissionBo.GetListByUserId(userId, isGetChildZone)
                };

                //check status group permission
                if (userWithPermissionDetail != null)
                {
                    var data=userWithPermissionDetail.AllGroupPermission.Select(s => new GroupPermissionDetailEntity
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Status = CheckStatusGroupPermission(userId, s.PermissionList, userWithPermissionDetail.User, userWithPermissionDetail.UserPermissionList),
                        PermissionList = s.PermissionList
                    }).ToList();
                    userWithPermissionDetail.AllGroupPermission=data;
                }

                return userWithPermissionDetail;
            }
            catch
            {
                return new UserWithPermissionDetailEntity();
            }
        }
        private static bool CheckStatusGroupPermission(int userId, List<PermissionEntity> permissionList,UserEntity user, List<UserPermissionEntity> userPermissionList)
        {
            try
            {
                if(user!=null && user.IsFullPermission)
                {
                    return true;
                }
                if (userPermissionList.Count(u => u.UserId == userId && (permissionList.Count(p=>p.Id==u.PermissionId)>0)) > 0)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static List<UserStandardEntity> Search(string keyword,
                                                           UserStatus status,
                                                           UserSortExpression sortOrder,
                                                           int pageIndex,
                                                           int pageSize,
                                                           ref int totalRow)
        {
            var userStandards = SearchUser(keyword, status, sortOrder, pageIndex, pageSize, ref totalRow);

            if (null != userStandards && userStandards.Count > 0)
            {
                int count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> Search2(string keyword,
                                                           UserStatus status,                                                           
                                                           int start,
                                                           int rows, ref int totalRow)
        {
            var userStandards = SearchUser2(keyword, status, start, rows,ref totalRow);

            if (null != userStandards && userStandards.Count > 0)
            {
                int count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitESAllUser(string name, DateTime startDate, DateTime endDate, int pageSize,string action=null)
        {                                    
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = UserDal.InitESAllUser();
                //index es
                BoSearch.Base.Account.UserDalFactory.InitUsersByStatus(data);                
            },action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllUser(string name, DateTime startDate, DateTime endDate, int pageSize,string action=null)
        {
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = UserDal.InitESAllUser();                
                //init redis
                BoCached.Base.Account.UserDalFactory.InitUsersByStatus(data);
            }, action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllUserPermission(string id, string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {                
                if (!string.IsNullOrEmpty(id))
                {
                    var listPer = UserPermissionDal.GetListUserPermissionByUserId(Utility.ConvertToInt(id), true);
                    if (listPer != null && listPer.Count() > 0)
                    {
                        BoCached.Base.Security.PermissionDalFactory.InitUserPermission(Utility.ConvertToInt(id), listPer);
                    }
                }
                else {
                    var data = UserDal.InitESAllUser();
                    //init redis userpermission
                    foreach (var item in data)
                    {
                        var listPer = UserPermissionDal.GetListUserPermissionByUserId(item.Id, true);
                        if (listPer != null && listPer.Count() > 0)
                        {
                            BoCached.Base.Security.PermissionDalFactory.InitUserPermission(item.Id, listPer);
                        }
                    }
                }
            }, action);
        }

        public static List<UserStandardEntity> GetUserListByPermissionListAndZoneList(string zoneIds, params EnumPermission[] permissionIds)
        {
            var userStandards = GetUserByPermissionListAndZoneList(zoneIds, permissionIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        public static List<UserStandardEntity> GetListUserFullPermisstion()
        {
            var userStandards = GetUserFullPermisstion();

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        public static List<UserStandardEntity> GetUserListByPermissionIdAndZoneList(EnumPermission permissionId, string zoneIds)
        {
            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }

        public static List<UserStandardEntity> GetUserWhoCanManageThisNews(EnumPermission permissionId, long newsId)
        {
            var zones = ZoneBo.GetZoneByNewsId(newsId);
            var zoneIds = zones.Aggregate("", (current, zone) => current + ("," + zone.Id));
            if (!string.IsNullOrEmpty(zoneIds)) zoneIds = zoneIds.Substring(1);

            var userStandards = GetUserByPermissionIdAndZoneList(permissionId, zoneIds);

            if (null != userStandards && userStandards.Count > 0)
            {
                var count = userStandards.Count;
                for (var i = 0; i < count; i++)
                {
                    userStandards[i].EncryptId = CryptonForId.EncryptId(userStandards[i].Id);
                    userStandards[i].Id = 0;
                }
            }
            return userStandards;
        }
        #endregion
    }
}
