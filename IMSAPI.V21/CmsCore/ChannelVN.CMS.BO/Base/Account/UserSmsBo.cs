﻿using System;
using ChannelVN.CMS.DAL.Base.Account;

namespace ChannelVN.CMS.BO.Base.Account
{
    public class UserSmsBo
    {
        public static void Insert(string userName, string smsCode, DateTime expiredDate)
        {
            try
            {
                var user = UserDal.GetUserByUsername(userName);
                if (user != null)
                    UserSmsDal.Insert(user.Id, smsCode, expiredDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.Insert:{0}", ex.Message));
            }
        }

        public static int CheckValidateCode(string userName, string smsCode)
        {
            try
            {
                return UserSmsDal.CheckValidateCode(userName, smsCode);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.CheckValidateCode:{0}", ex.Message));
            }
        }

        public static void GetSmsCode(string userName, ref string smsCode, ref string phoneNumber)
        {
            try
            {
                UserSmsDal.GetSmsCode(userName, ref smsCode, ref phoneNumber);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserSmsDal.GetSmsCode:{0}", ex.Message));
            }
        }
    }
}
