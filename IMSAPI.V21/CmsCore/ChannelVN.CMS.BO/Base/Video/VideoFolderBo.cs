﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoFolderBo
    {
        #region Update

        public static ErrorMapping.ErrorCodes InsertVideoFolder(VideoFolderEntity videoFolder, ref int newVideoFolderId)
        {
            try
            {
                if (null == videoFolder)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoFolderDal.Insert(videoFolder, ref newVideoFolderId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateVideoFolder(VideoFolderEntity videoFolder)
        {
            try
            {
                if (null == videoFolder)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoFolderDal.Update(videoFolder) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteVideoFolder(int videoFolderId)
        {
            try
            {
                return VideoFolderDal.Delete(videoFolderId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static VideoFolderEntity GetVideoFolderByVideoFolderId(int videoFolderId)
        {
            try
            {
                return VideoFolderDal.GetById(videoFolderId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoFolderEntity> GetVideoFolderByParentId(int parentId, string createdBy)
        {
            try
            {
                return VideoFolderDal.GetByParentId(parentId, createdBy);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoFolderEntity> VideoFolderSearch(int parentId, string name, string createdBy, int status)
        {
            try
            {
                return VideoFolderDal.Search(parentId, name, createdBy, status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<VideoFolderEntity> GetAllFolderWithTreeView(string createdBy)
        {
            try
            {
                var listFolder = VideoFolderDal.Search(-1, "", createdBy, 1);
                return BindFolderToTreeview(listFolder, "--");
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<VideoFolderEntity>();
            }

        }
        public static List<VideoFolderEntity> BindFolderToTreeview(List<VideoFolderEntity> allFolder, string prefix)
        {
            var allFolderWithSimpleField = new List<VideoFolderEntity>();
            var count = allFolder.Count;
            foreach (var folder in allFolder)
            {
                allFolderWithSimpleField.Add(new VideoFolderEntity
                {
                    Id = folder.Id,
                    ParentId = folder.ParentId,
                    Name = folder.Name,
                    Status = folder.Status,
                    CreatedBy = folder.CreatedBy
                });
            }

            allFolderWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.CreatedDate);
            var outputFolder = new List<VideoFolderEntity>();
            BindFolderToTreeview(allFolderWithSimpleField.ToList(), 0, prefix, ref outputFolder);
            return outputFolder;
        }

        private static void BindFolderToTreeview(List<VideoFolderEntity> allFolder, int currentParentId, string prefix, ref List<VideoFolderEntity> outputFolder)
        {
            if (allFolder == null || allFolder.Count <= 0) return;
            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allFolder.Count;
            for (var i = 0; i < count; i++)
            {
                if (allFolder[i].ParentId == currentParentId)
                {
                    allFolder[i].Name = currentPrefix + " " + allFolder[i].Name;
                    outputFolder.Add(allFolder[i]);
                    BindFolderToTreeview(allFolder, allFolder[i].Id, currentPrefix + prefix, ref outputFolder);
                }
            }
        }
        #endregion
    }
}
