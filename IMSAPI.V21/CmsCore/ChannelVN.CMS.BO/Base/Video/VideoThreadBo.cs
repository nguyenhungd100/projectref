﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoThreadBo
    {
        public static List<VideoThreadEntity> SearchVideoThread(string keyword, bool isHotThread, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoThreadDal.Search(keyword, isHotThread, status, pageIndex, pageSize, ref totalRow);
        }

        public static VideoThreadDetailEntity GetVideoThreadById(int id)
        {
            var videoThread = VideoThreadDal.GetById(id);
            if (videoThread != null)
            {
                return new VideoThreadDetailEntity()
                           {
                               VideoThread = videoThread,
                               Videos = VideoDal.GetAllVideoInVideoThread(id)
                           };
            }
            return null;
        }

        public static ErrorMapping.ErrorCodes InsertVideoThread(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId)
        {
            return VideoThreadDal.Insert(videoThread, videoIdList, ref videoThreadId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateVideoThread(VideoThreadEntity videoThread, string videoIdList)
        {
            return VideoThreadDal.Update(videoThread, videoIdList)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteVideoThread(int videoThreadId)
        {
            return VideoThreadDal.Delete(videoThreadId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateVideoInVideoThread(int videoThreadId, string videoIdList)
        {
            return VideoThreadDal.UpdateVideoInVideoThread(videoThreadId, videoIdList)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
    }
}
