﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BO.Base.Security;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class PlaylistBo
    {
        #region Sets        
        public static ErrorMapping.ErrorCodes Save(PlaylistEntity playlistEntity,string zoneIdList, ref int playlistId)
        {
            try
            {
                if (null == playlistEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (playlistEntity.Id > 0)
                {
                    playlistId = playlistEntity.Id;
                    return Update(playlistEntity, zoneIdList, ref playlistId);
                }
                return Insert(playlistEntity, zoneIdList, ref playlistId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistBo.Save:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Insert(PlaylistEntity playlistEntity,string zoneIdList, ref int playlistId)
        {
            try
            {
                if (null == playlistEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(playlistEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoPlaylistMustHaveName;
                }

                var success = PlaylistDal.Insert(playlistEntity, zoneIdList, ref playlistId);
                if (success)
                {
                    playlistEntity.Id = playlistId;
                    //insert redis
                    BoCached.Base.PlayList.PlayListDalFactory.AddPlayList(playlistEntity);
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var listTemp = new List<string>();
                        listTemp.Add(playlistEntity.ZoneId.ToString());
                        listTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(listTemp.Distinct());
                    }
                    else if (playlistEntity.ZoneId > 0)
                    {
                        var listTemp = new List<string>();
                        listTemp.Add(playlistEntity.ZoneId.ToString());
                        zoneids.AddRange(listTemp.Distinct());
                    }
                    //insert es                                    
                    var playListES = new PlayListSearchEntity
                    {
                        Id = playlistEntity.Id,
                        ZoneId= playlistEntity.ZoneId,
                        ZoneIds = zoneids.ToArray(),
                        Name = playlistEntity.Name,
                        Avatar = playlistEntity.Avatar,                        
                        Status = playlistEntity.Status,
                        CreatedBy= playlistEntity.CreatedBy,
                        CreatedDate = playlistEntity.CreatedDate,
                        EditedBy = playlistEntity.EditedBy,
                        EditedDate = playlistEntity.EditedDate,
                        DistributionDate = playlistEntity.DistributionDate,
                        Mode= playlistEntity.Mode,
                        VideoCount = playlistEntity.VideoCount,
                        FollowCount= playlistEntity.FollowCount,
                        ZoneName= playlistEntity.ZoneName,
                        VideoCountNotPublish= 0
                    };
                    BoSearch.Base.PlayList.PlayListDalFactory.AddPlayList(playListES);

                    ActivityBo.LogAddNewPlayList(playlistEntity.Id, playlistEntity.CreatedBy, playlistEntity.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Update(PlaylistEntity playlistEntity, string zoneIdList, ref int playlistId)
        {
            try
            {
                if (null == playlistEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(playlistEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoPlaylistMustHaveName;
                }

                var playListDB = GetById(playlistEntity.Id);
                if(playListDB!=null && playListDB.PlaylistInfo == null)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                playlistEntity.Status = playListDB.PlaylistInfo.Status;

                var success = PlaylistDal.Update(playlistEntity, zoneIdList);
                if (success)
                {
                    //insert redis
                    BoCached.Base.PlayList.PlayListDalFactory.AddPlayList(playlistEntity);

                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(playlistEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (playlistEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(playlistEntity.ZoneId.ToString());                        
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    //insert es                    
                    var playListES = new PlayListSearchEntity
                    {
                        Id = playlistEntity.Id,
                        ZoneIds = zoneids.ToArray(),
                        Name = playlistEntity.Name,
                        Avatar = playlistEntity.Avatar,
                        Status = playlistEntity.Status,
                        EditedBy = playlistEntity.EditedBy,
                        EditedDate = playlistEntity.EditedDate,
                        LastModifiedBy = playlistEntity.LastModifiedBy,
                        LastModifiedDate = playlistEntity.LastModifiedDate,
                        PublishedBy = playlistEntity.PublishedBy,
                        PublishedDate = playlistEntity.PublishedDate,
                        DistributionDate = playlistEntity.DistributionDate,
                        Mode = playlistEntity.Mode,
                        VideoCount = playlistEntity.VideoCount,
                        ZoneId=playlistEntity.ZoneId,
                        ZoneName=playlistEntity.ZoneName                        
                    };
                    BoSearch.Base.PlayList.PlayListDalFactory.UpdatePlayList(playListES);

                    //ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes RemoveVideoOutOfPlaylist(int playlistId, string videoIdList)
        {
            try
            {
                if (playlistId <= 0 || string.IsNullOrEmpty(videoIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result= PlaylistDal.RemoveVideoOutOfPlaylist(playlistId, videoIdList)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (result== ErrorMapping.ErrorCodes.Success)
                {
                    var hashName = "VideoInPlaylistSearch:" + playlistId;
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.RemoveVideoOutOfPlaylist(hashName);                    
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.RemoveVideoOutOfPlaylist:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes AddVideo(int videoId, int playlistId, int priority)
        {
            try
            {
                if (videoId <= 0 || playlistId <= 0 || priority <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return PlaylistDal.AddVideo(videoId, playlistId, priority)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.AddVideo:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes AddVideoList(string addVideoIds, string deleteVideoIds, int playlistId, string playlistName="")
        {
            try
            {               
                if (playlistId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var rs =  PlaylistDal.AddVideoList(addVideoIds, deleteVideoIds, playlistId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if (rs == ErrorMapping.ErrorCodes.Success) {
                    FuncTaskAsyn(() => {
                                                
                        var listAll = new List<string>();
                        var a = addVideoIds.Split(';').ToList();
                        var b = deleteVideoIds.Split(';').ToList();
                            
                        listAll.AddRange(a);
                        listAll.AddRange(b);
                        var listDistinct = listAll.Distinct().ToList();
                        BoCached.Base.Video.VideoDalFactory.DeletePlayListInVideo(listDistinct);

                        //update videoinplaylist ->es
                        BoSearch.Base.Video.VideoDalFactory.UpdateVideoInPlayList(playlistId, a, b, playlistName);        
                                        
                    });                                 
                }

                return rs;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.AddVideoList:{0}", ex.Message));
            }
        }

        private static void FuncTaskAsyn(System.Action func)
        {
            var task= Task.Run(() =>
            {
                try
                {
                    func();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, "AddVideoList => FuncTaskAsyn() error => " + ex.Message);
                }
            });

            try
            {
                if (task.Wait(TimeSpan.FromSeconds(3)))
                {

                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
        }

        public static ErrorMapping.ErrorCodes DeleteById(int playlistId)
        {
            try
            {
                if (playlistId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return PlaylistDal.DeleteById(playlistId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.DeleteById:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes DeleteByIdList(string playlistIdList)
        {
            try
            {
                if (string.IsNullOrEmpty(playlistIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result= PlaylistDal.DeleteByIdList(playlistIdList)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                if(result== ErrorMapping.ErrorCodes.Success)
                {
                    if (string.IsNullOrEmpty(playlistIdList))
                        return result;
                    var listP = new List<string>();
                    listP.AddRange(playlistIdList.Split(','));
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.Delete(listP);
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.Delete(listP);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.DeleteByIdList:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes PublishPlaylist(int playlistId, string userDoAction)
        {
            try
            {
                if (0 >= playlistId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                //check playlist không có video xuất bản -> không đc xuất bản playlist: check trong sql
                //if (!CheckPublishPlayList(playlistId))
                //{
                //    return ErrorMapping.ErrorCodes.PlayListNotPublish;
                //}
                var result = PlaylistDal.Publish(playlistId, userDoAction) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if(result== ErrorMapping.ErrorCodes.Success)
                {
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.Publish(playlistId, userDoAction);
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.Publish(playlistId, userDoAction);                    
                }
                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                if (ex.Message.Contains("17007"))
                {
                    return ErrorMapping.ErrorCodes.PlayListNotPublish;
                }
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UnpublishPlaylist(int playlistId)
        {
            try
            {
                if (0 >= playlistId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = PlaylistDal.Unpublish(playlistId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.UnPublish(playlistId, "");
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.UnPublish(playlistId, "");
                }

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SendPlayList(int playListId, string username)
        {
            try
            {
                if (0 >= playListId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                
                var playList = PlaylistDal.GetById(playListId);
                if (playList == null)
                {
                    return ErrorMapping.ErrorCodes.PlayListNotAvailable;
                }
                if (playList.Status != (int)EnumPlayListStatus.Temporary && playList.Status != (int)EnumPlayListStatus.ReturnForEditor)
                {
                    return ErrorMapping.ErrorCodes.PlayListNotAllowToSend;
                }
                if (!(playList.CreatedBy.Equals(username) || playList.EditedBy.Equals(username)))
                {
                    return ErrorMapping.ErrorCodes.PlayListNotAllowToSend;
                }                

                var success = PlaylistDal.Send(playListId, username);
                if (success)
                {
                    //bo trang thay cho bien tap -> cho xuat ban: WaitForPublish
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.Send(playListId, (int)EnumPlayListStatus.WaitForPublished, username);
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.ChangeStatus(playListId, (int)EnumPlayListStatus.WaitForPublished, username);

                    ActivityBo.LogSendVideoToWaitToPublish(playList.Id, username, playList.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes ReturnPlayList(int playListId, string username)
        {
            try
            {
                if (0 >= playListId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                
                var playList = PlaylistDal.GetById(playListId);
                if (playList == null)
                {
                    return ErrorMapping.ErrorCodes.PlayListNotAvailable;
                }
                if (playList.Status != (int)EnumPlayListStatus.WaitForPublished && playList.Status != (int)EnumPlayListStatus.Unpublished)
                {
                    return ErrorMapping.ErrorCodes.PlayListNotAllowToReturn;
                }
                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToReturn;
                }

                var success = PlaylistDal.Return(playListId);
                if (success)
                {
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.Return(playListId, username);
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.ChangeStatus(playListId, (int)EnumPlayListStatus.ReturnForEditor, username);

                    ActivityBo.LogReturnVideo(playList.Id, WcfMessageHeader.Current.ClientUsername, playList.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes MoveTrash(int playlistId, string username)
        {
            try
            {
                if (0 >= playlistId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result = PlaylistDal.MoveTrash(playlistId, username) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    //redis
                    BoCached.Base.PlayList.PlayListDalFactory.MoveTrash(playlistId, username);
                    //es
                    BoSearch.Base.PlayList.PlayListDalFactory.MoveTrash(playlistId, username);
                }

                return result;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes ChangeMode(int playlistId, EnumPlayListMode mode)
        {
            if (mode != EnumPlayListMode.AllMode)
            {
                var resurl= PlaylistDal.ChangeMode(playlistId, (int)mode) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                if(resurl== ErrorMapping.ErrorCodes.Success)
                {
                    var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    BoCached.Base.PlayList.PlayListDalFactory.ChangeMode(accountName, playlistId, (int)mode);
                    BoSearch.Base.PlayList.PlayListDalFactory.ChangeMode(accountName, playlistId, (int)mode);
                }
                return resurl;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateVideoPriorotyInPlayList(int playlistId, List<VideoPriorityEntity> listVideoPriority)
        {
            if (listVideoPriority!=null && listVideoPriority.Count>0)
            {
                var resurl = false;
                foreach (var item in listVideoPriority)
                {
                    resurl = PlaylistDal.UpdateVideoPriorotyInPlayList(playlistId, item);
                }
                if(resurl)
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            
            return ErrorMapping.ErrorCodes.InvalidRequest;
        }

        #endregion

        #region Gets

        public static PlaylistEntity GetPlayListById(int playlistId)
        {
            try
            {                
                var playlist = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListById(playlistId);
                if (playlist == null || (playlist != null && playlist.Id <= 0))
                {
                    playlist = PlaylistDal.GetById(playlistId);
                    if (playlist != null)
                    {
                        //add redis
                        BoCached.Base.PlayList.PlayListDalFactory.AddPlayList(playlist);
                    }
                }                

                return playlist;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetById:{0}", ex.Message));
            }
        }
        public static PlayListCachedEntity GetById(int playlistId)
        {
            try
            {
                var playListWithZone = new PlayListCachedEntity();
                var playlist = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListById(playlistId);
                if (playlist == null || (playlist!=null && playlist.Id<=0))
                {
                    playlist = PlaylistDal.GetById(playlistId);
                    if (playlist != null)
                    {
                        //add redis
                        BoCached.Base.PlayList.PlayListDalFactory.AddPlayList(playlist);
                    }
                }

                playListWithZone.PlaylistInfo = playlist;
                playListWithZone.ZoneVideoList = ZoneVideoBo.GetListZoneInPlayList(playlistId);

                return playListWithZone;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetById:{0}", ex.Message));
            }
        }

        public static List<PlaylistEntity> GetListByVideoId(int videoId)
        {
            try
            {
                var playList = BoCached.Base.Video.VideoDalFactory.GetPlayListByVideoId(videoId);
                if(playList==null || (playList!=null && playList.Count() <= 0))
                {
                    playList= PlaylistDal.GetListByVideoId(videoId);
                    if(playList != null && playList.Count() > 0)
                    {
                        //add redis
                        BoCached.Base.Video.VideoDalFactory.AddPlayListByVideoId(videoId, playList);
                    }
                }
                return playList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetListByVideoId:{0}", ex.Message));
            }
        }
        public static List<PlaylistEntity> GetListByVideoChannelId(int videoChannelId)
        {
            try
            {
                var playList = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetPlayListByVideoChannelId(videoChannelId);
                if (playList == null || (playList != null && playList.Count() <= 0))
                {
                    playList = PlaylistDal.GetListByVideoChannelId(videoChannelId);
                    if (playList != null && playList.Count() > 0)
                    {
                        //add redis
                        BoCached.Base.VideoChannel.VideoChannelDalFactory.AddPlayListByVideoChannelId(videoChannelId, playList);
                    }
                }
                return playList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetListByVideoId:{0}", ex.Message));
            }
        }

        public static List<PlaylistEntity> GetListPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            try
            {
                return PlaylistDal.GetListPaging(pageIndex, pageSize, status, keyword, sortOder, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetListPaging:{0}", ex.Message));
            }
        }

        public static List<PlaylistEntity> GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            try
            {
                return PlaylistDal.GetMyPlaylist(username, pageIndex, pageSize, status, keyword, sortOder, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }

        public static List<PlayListSearchEntity> SearchPlayList(string username, int zoneId, EnumPlayListStatus status, string keyword, EnumPlayListMode mode, EnumPlayListSort sortOder, DateTime createdDateFrom, DateTime createdDateTo, DateTime distributionDateFrom, DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var playlists = BoSearch.Base.PlayList.PlayListDalFactory.SearchPlayList(username, zoneId, status, keyword, mode, sortOder, createdDateFrom, createdDateTo, distributionDateFrom, distributionDateTo, pageIndex, pageSize, ref totalRow);

                var playlistPublish = playlists.Where(n => n.Status == (int)EnumPlayListStatus.Published).Select(p => p.Id).ToList();
                if (playlistPublish.Count > 0) {
                    var str = string.Join(",", playlistPublish.ToArray());
                    var dataCount = VideoBo.GetVideoNotPublishCount(str);
                    if (dataCount != null && dataCount.Count > 0)
                    {
                        foreach (var item in playlists) {
                            var lst = dataCount.SingleOrDefault(n => n.PlaylistId == item.Id);
                            if (lst != null)
                                item.VideoCountNotPublish = lst.VideoNotPublishCount;
                            else
                                item.VideoCountNotPublish = 0;
                        }
                    }
                    else {
                        foreach (var item in playlists)
                        {
                            item.VideoCountNotPublish = 0;
                        }
                        return playlists;
                    }
                }
                else {
                    foreach (var item in playlists) {
                        item.VideoCountNotPublish = 0;
                    }                    
                }

                return playlists;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }

        public static List<CountStatus> CountPlaylist(string username, string listStatus)
        {
            try
            {
                var list = listStatus.Split(',').ToList();
                return BoSearch.Base.PlayList.PlayListDalFactory.CountPlaylist(username, list);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }

        public static List<PlaylistCountFollowEntity> CountVideoFollow(string playListIds)
        {
            try
            {                
                return PlaylistDal.CountVideoFollow(playListIds);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetMyPlaylist:{0}", ex.Message));
            }
        }

        public static List<PlaylistCounterEntity> GetPlaylistCounter()
        {
            var int1 = new List<int>();
            var int2 = new List<int>();
            var result = PlaylistDal.GetPlaylistCounter();
            var values = Enum.GetValues(typeof(EnumPlayListStatus));
            foreach (var value in values)
            {
                int1.Add((int)value);
                int2.Add(Utility.ConvertToInt(value));
            }
            foreach (var value in values.Cast<object>().Where(value => result.Find(item => item.Status == (int)value) == null))
            {
                result.Add(new PlaylistCounterEntity
                {
                    Status = (int)value,
                    Count = 0
                });
            }
            return result;
        }

        public static List<PlaylistWithZoneEntity> GetLastDaysHavePlaylistWithZone(DateTime fromDate, DateTime toDate, int group, int team, int mode)
        {
            try
            {
                List<PlaylistWithZoneEntity> lst = new List<PlaylistWithZoneEntity>();
                var playlistWithZone = new PlaylistWithZoneEntity();
                var lstPlaylist = PlaylistDal.GetLastDaysHavePlaylist(fromDate, toDate, group, team, mode);
                foreach (var playlistEntity in lstPlaylist)
                {
                    playlistWithZone = new PlaylistWithZoneEntity();
                    playlistWithZone.Playlist = playlistEntity;
                    playlistWithZone.PlaylistInZone = PlaylistDal.GetZoneByPlaylist(playlistEntity.Id);
                    lst.Add(playlistWithZone);
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetLastDaysHavePlaylist:{0}", ex.Message));
            }
        }

        public static List<PlaylistWithZoneEntity> GetTopDayHaveVideo(int top)
        {
            try
            {
                List<PlaylistWithZoneEntity> lst = new List<PlaylistWithZoneEntity>();
                var playlistWithZone = new PlaylistWithZoneEntity();
                var lstPlaylist = PlaylistDal.GetTopDayHaveVideo(top);
                foreach (var playlistEntity in lstPlaylist)
                {
                    playlistWithZone = new PlaylistWithZoneEntity();
                    playlistWithZone.Playlist = playlistEntity;
                    playlistWithZone.PlaylistInZone = PlaylistDal.GetZoneByPlaylist(playlistEntity.Id);
                    lst.Add(playlistWithZone);
                }
                return lst;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.GetLastDaysHavePlaylist:{0}", ex.Message));
            }
        }

        public static List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId)
        {
            return PlaylistDal.GetZoneByPlaylist(playlistId);
        }
        public static ErrorMapping.ErrorCodes Playlist_Update_DistributionDate(string playlistIdList)
        {
            try
            {
                if (string.IsNullOrEmpty(playlistIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return PlaylistDal.Playlist_Update_DistributionDate(playlistIdList)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.Playlist_Update_DistributionDate:{0}", ex.Message));
            }
        }
        public static PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate)
        {
            try
            {
                return PlaylistDal.PlaylistGroup_Current(fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("PlaylistDal.PlaylistGroup_Current:{0}", ex.Message));
            }
        }
        #endregion

        private static bool CurrentClientUserHasPermision(EnumPermission permission)
        {
            var result = BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(WcfMessageHeader.Current.ClientUsername, (int)permission);
            if (result)
            {
                return true;
            }
            return PermissionBo.CheckUserPermission(WcfMessageHeader.Current.ClientUsername, (int)permission) ==
                   ErrorMapping.ErrorCodes.Success;
        }

        public static List<PlaylistEntity> InitEsRedisByListPlayListId(string playListIdList)
        {
            try
            {
                return PlaylistDal.GetListByIdList(playListIdList);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllPlayList(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = PlaylistDal.InitRedisAllPlayList(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = PlaylistDal.InitRedisAllPlayList(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.PlayList.PlayListDalFactory.InitAllPlayList(data);
            }, action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsAllPlayList(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = PlaylistDal.InitRedisAllPlayList(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = PlaylistDal.InitRedisAllPlayList(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new PlayListSearchEntity
                {
                    Id = s.Id,     
                    ZoneId=s.ZoneId,
                    Name=s.Name,
                    EditedDate=s.EditedDate,
                    FollowCount=s.FollowCount,
                    Mode=s.Mode,
                    PublishedDate=s.PublishedDate,
                    VideoCount=s.VideoCount,
                    VideoCountNotPublish=0,               
                    ZoneIds = ZoneVideoDal.GetListZoneRelationByPlayListId(s.Id).Select(z => z.Id.ToString()).ToArray(),                    
                    CreatedBy = s.CreatedBy,
                    LastModifiedBy = s.LastModifiedBy,
                    EditedBy = s.EditedBy,
                    PublishedBy = s.PublishedBy,                   
                    CreatedDate = s.CreatedDate,
                    DistributionDate = s.DistributionDate,
                    LastModifiedDate = s.LastModifiedDate,                    
                    Status = s.Status,                    
                    Avatar = s.Avatar,                    
                    ZoneName = s.ZoneName
                }).ToList();
                BoSearch.Base.PlayList.PlayListDalFactory.InitAllPlayList(list);
            }, action);
        }
    }
}
