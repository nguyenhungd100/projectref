﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoEPLBo
    {
        public static List<VideoEPLEntity> VideoEPLByZone(int zoneId, DateTime dateFrom, DateTime dateTo)
        {
            var listVideo = new List<VideoEPLEntity>();
            try
            {
                return VideoEPLDal.VideoEPLByZone(zoneId, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return listVideo;

        }
        public static List<VideoEPLZoneToZoneEntity> VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo)
        {
            var listVideo = new List<VideoEPLZoneToZoneEntity>();
            try
            {
                return VideoEPLDal.VideoEPLZoneAndZone(zoneId1, zoneId2, dateFrom, dateTo);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return listVideo;

        }
        public static List<EPLPlayListEntity> EPLPlayListGetAll()
        {
            var listVideo = new List<EPLPlayListEntity>();
            try
            {
                return VideoEPLDal.EPLPlayListGetAll();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return listVideo;

        }

        public static List<EPLPlayListEntity> EPLPlayListGetByUser(string userName)
        {
            var listVideo = new List<EPLPlayListEntity>();
            try
            {
                return VideoEPLDal.EPLPlayListGetByUser(userName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return listVideo;

        }
        public static ErrorMapping.ErrorCodes Insert(string userName, int videoId)
        {
            try
            {
                var returnvalue = VideoEPLDal.Insert(userName, videoId);
                return returnvalue
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int userId, int videoId)
        {
            try
            {
                var returnvalue = VideoEPLDal.Delete(userId, videoId);
                return returnvalue
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

    }
}
