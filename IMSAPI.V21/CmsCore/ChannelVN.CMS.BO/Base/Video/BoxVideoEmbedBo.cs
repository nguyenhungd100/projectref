﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class BoxVideoEmbedBo
    {
        #region BoxVideoEmbed
        public static List<BoxVideoEmbedEntity> GetListVideoEmbed(int zoneId, int type)
        {
            var VideoEmbed = new List<BoxVideoEmbedEntity>();
            try
            {

                return BoxVideoEmbedDal.GetListVideoEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static ErrorMapping.ErrorCodes Insert(BoxVideoEmbedEntity VideoEmbebBox)
        {
            try
            {
                if (null != VideoEmbebBox)
                {
                    return BoxVideoEmbedDal.Insert(VideoEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listVideoId, int zoneId, int type)
        {
            try
            {
                if (null != listVideoId)
                {
                    BoxVideoEmbedDal.Update(listVideoId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedDal.Update:{0}", ex.Message));
            }

        }
        public static ErrorMapping.ErrorCodes UpdateEpl(List<BoxVideoEmbedEplEntity> listEmbed)
        {
            try
            {
                foreach (var item in listEmbed)
                {
                    BoxVideoEmbedDal.UpdateEpl(item);
                    
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedDal.UpdateEpl:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(long VideoId, int zoneId, int type)
        {
            try
            {
                if (VideoId > 0 && zoneId > 0)
                {
                    BoxVideoEmbedDal.Delete(VideoId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedDal.Delete:{0}", ex.Message));
            }

        }
        #endregion

        #region BoxPlayEmbed
        public static List<BoxPlayEmbedEntity> GetListPlayEmbed(int zoneId, int type)
        {
            var VideoEmbed = new List<BoxPlayEmbedEntity>();
            try
            {
                var typeId = (int)VideoPositionType.VideoBoxMuc;
                return BoxVideoEmbedDal.GetListPlayEmbed(zoneId, type, typeId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }       

        public static ErrorMapping.ErrorCodes UpdatePlay(string listObject, int zoneId, int type, string name, int displayStyle, int priority)
        {
            try
            {
                var listUpdate = new List<BoxPlayEmbedEntity>();

                if (!string.IsNullOrEmpty(listObject))
                {
                    listUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BoxPlayEmbedEntity>>(listObject);
                }
                int typeId= (int)VideoPositionType.VideoBoxMuc;
                if (listUpdate.Count <= 0)
                {
                    DeletePlay(zoneId, type, typeId);
                }
                else
                {
                    DeletePlay(zoneId, type, typeId);
                    foreach (var item in listUpdate)
                    {
                        item.TypeId = typeId;
                        item.DisplayStyle = displayStyle;
                        item.Priority = priority;
                        BoxVideoEmbedDal.InsertPlay(item);
                    }                    
                }                    

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxPlayEmbedDal.UpdatePlay:{0}", ex.Message));
            }

        }
        
        public static ErrorMapping.ErrorCodes DeletePlay(int zoneId, int type, int typeId)
        {
            try
            {                    
                return BoxVideoEmbedDal.DeletePlay(zoneId, type, typeId) ?ErrorMapping.ErrorCodes.Success: ErrorMapping.ErrorCodes.UpdateZoneNotAllow;                
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxPlayEmbedDal.DeletePlay:{0}", ex.Message));
            }

        }
        #endregion

        #region BoxVideoHighLightEmbed
        public static List<BoxVideoHighlightEmbedEntity> GetVideoHighLightEmbed()
        {
            var VideoEmbed = new List<BoxVideoHighlightEmbedEntity>();
            try
            {
                var typeId = (int)VideoPositionType.VideoHighLight;
                return BoxVideoEmbedDal.GetVideoHighLightEmbed(typeId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static ErrorMapping.ErrorCodes UpdateVideoHighlight(string listObject)
        {
            try
            {
                var listUpdate = new List<BoxVideoHighlightEmbedEntity>();

                if (!string.IsNullOrEmpty(listObject))
                {
                    listUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BoxVideoHighlightEmbedEntity>>(listObject);
                }
                int typeId = (int)VideoPositionType.VideoHighLight;
                if (listUpdate.Count <= 0)
                {
                    DeleteVideoHighLight(typeId);
                }
                else
                {
                    DeleteVideoHighLight(typeId);
                    foreach (var item in listUpdate)
                    {
                        item.TypeId = typeId;
                        BoxVideoEmbedDal.InsertVideoHighLight(item);
                    }
                }

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxPlayEmbedDal.UpdatePlay:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes DeleteVideoHighLight(int typeId)
        {
            try
            {
                return BoxVideoEmbedDal.DeleteVideoHighLight(typeId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxDeleteVideoHighLightDal.DeleteVideoHighLight:{0}", ex.Message));
            }

        }
        #endregion

        #region BoxProgramHotHomeEmbed
        public static List<BoxProgramHotHomeEmbedEntity> GetProgramHotHomeEmbed(int zoneId)
        {
            var VideoEmbed = new List<BoxProgramHotHomeEmbedEntity>();
            try
            {
                var typeId = (int)VideoPositionType.VideoProgramHotHome;
                if (zoneId > 0)
                {
                    typeId = (int)VideoPositionType.VideoProgramHotMuc;
                }
                
                return BoxVideoEmbedDal.GetProgramHotHomeEmbed(typeId,zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static ErrorMapping.ErrorCodes UpdateProgramHotHome(string name, int zoneId, string listObject)
        {
            try
            {
                var listUpdate = new List<BoxProgramHotHomeEmbedEntity>();

                if (!string.IsNullOrEmpty(listObject))
                {
                    listUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<List<BoxProgramHotHomeEmbedEntity>>(listObject);
                }
                int typeId = (int)VideoPositionType.VideoProgramHotHome;
                if (zoneId > 0)
                {
                    typeId = (int)VideoPositionType.VideoProgramHotMuc;
                }
                if (listUpdate.Count <= 0)
                {
                    DeleteProgramHotHome(typeId, zoneId);
                }
                else
                {
                    DeleteProgramHotHome(typeId, zoneId);
                    foreach (var item in listUpdate)
                    {
                        item.TypeId = typeId;
                        BoxVideoEmbedDal.InsertProgramHotHome(item);
                    }
                }

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxProgramHotHomeEmbedDal.UpdateProgramHotHome:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes DeleteProgramHotHome(int typeId, int zoneId)
        {
            try
            {
                return BoxVideoEmbedDal.DeleteProgramHotHome(typeId, zoneId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxDeleteProgramHotHomeDal.DeleteProgramHotHome:{0}", ex.Message));
            }

        }
        #endregion
    }
}
