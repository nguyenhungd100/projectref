﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.DAL.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoChannelBo
    {
        public static ErrorMapping.ErrorCodes SaveVideoChannel(VideoChannelEntity videoChannelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList, ref int videoChannelId)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (videoChannelEntity.Id > 0)
                {
                    videoChannelId = videoChannelEntity.Id;
                    return Update(videoChannelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList);
                }

                return Insert(videoChannelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList, ref videoChannelId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Insert(VideoChannelEntity videoChannelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList, ref int videoChannelId)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoChannelEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoChannelTitleCouldNotBeEmpty;
                }

                var success = VideoChannelDal.Insert(videoChannelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList, ref videoChannelId);
                if (success)
                {
                    videoChannelEntity.Id = videoChannelId;
                    //insert redis
                    BoCached.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannel(videoChannelEntity);
                    //insert es      
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var listTemp = new List<string>();
                        listTemp.Add(videoChannelEntity.ZoneId.ToString());
                        listTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(listTemp.Distinct());
                    }
                    var labelids = new List<string>();
                    if (!string.IsNullOrEmpty(labelIdList))
                    {
                        var listTemp= new List<string>();
                        listTemp.Add(videoChannelEntity.LabelId.ToString());
                        listTemp.AddRange(labelIdList.Split(';'));
                        labelids.AddRange(listTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }
                    var videoIds = new List<string>();
                    var videoChannelES = new VideoChannelSearchEntity
                    {
                        Id = videoChannelEntity.Id,
                        ZoneId= videoChannelEntity.ZoneId,
                        PublisherId =videoChannelEntity.PublisherId,
                        Name = videoChannelEntity.Name,
                        Avatar = videoChannelEntity.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        Labelds= labelids.ToArray(),
                        PlayListIds= playListIds.ToArray(),
                        VideoIds= videoIds.ToArray(),
                        Status = videoChannelEntity.Status,
                        CreatedDate=videoChannelEntity.CreatedDate,
                        FollowCount= videoChannelEntity.FollowCount,
                        VideoCount= videoChannelEntity.VideoCount,
                        ZoneName= videoChannelEntity.ZoneName,
                        Mode = videoChannelEntity.Mode
                    };
                    BoSearch.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannel(videoChannelES);

                    //ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Update(VideoChannelEntity videoChannelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoChannelEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoChannelTitleCouldNotBeEmpty;
                }

                var videoChannel = GetVideoChannelDetail(videoChannelEntity.Id, videoChannelEntity.CreatedBy);
                if (videoChannel == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                videoChannel.Id = videoChannelEntity.Id;
                videoChannel.ZoneId = videoChannelEntity.ZoneId;
                videoChannel.PublisherId = videoChannelEntity.PublisherId;
                videoChannel.Name = videoChannelEntity.Name;
                videoChannel.Avatar = videoChannelEntity.Avatar;
                videoChannel.Description = videoChannelEntity.Description;
                videoChannel.Priority = videoChannelEntity.Priority;
                videoChannel.IntroClip = videoChannelEntity.IntroClip;
                videoChannel.ChannelRelation = videoChannelEntity.ChannelRelation;
                videoChannel.Status = videoChannelEntity.Status;
                videoChannel.Url = videoChannelEntity.Url;

                videoChannel.CreatedBy = videoChannel.CreatedBy;
                videoChannel.CreatedDate = videoChannel.CreatedDate;

                videoChannel.LastModifiedBy = videoChannelEntity.LastModifiedBy;
                videoChannel.LastModifiedDate = videoChannelEntity.LastModifiedDate;

                videoChannel.FollowCount = videoChannel.FollowCount;
                videoChannel.Rank = videoChannel.Rank;
                videoChannel.VideoCount = videoChannel.VideoCount;

                videoChannel.ZoneId = videoChannelEntity.ZoneId;
                videoChannel.LabelId = videoChannelEntity.LabelId;
                videoChannel.ZoneName = videoChannelEntity.ZoneName;

                videoChannel.MetaJson = videoChannelEntity.MetaJson;
                videoChannel.Mode = videoChannelEntity.Mode;
                videoChannel.MetaAvatar = videoChannelEntity.MetaAvatar;

                var success = VideoChannelDal.Update(videoChannel, zoneIdList, labelIdList, playlistIdList, videoIdList);
                if (success)
                {                    
                    //insert redis
                    BoCached.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannel(videoChannel);
                    //insert es      
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        zoneids.Add(videoChannelEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneIdList.Split(';'));
                        zoneids.Distinct();
                    }
                    var labelids = new List<string>();
                    if (!string.IsNullOrEmpty(labelIdList))
                    {
                        labelids.Add(videoChannelEntity.LabelId.ToString());
                        labelids.AddRange(labelIdList.Split(';'));
                        labelids.Distinct();
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }
                    var videoIds = new List<string>();
                    var videoChannelES = new VideoChannelSearchEntity
                    {
                        Id = videoChannel.Id,
                        ZoneId=videoChannel.ZoneId,
                        PublisherId = videoChannel.PublisherId,
                        Name = videoChannel.Name,
                        Avatar = videoChannel.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        Labelds = labelids.ToArray(),
                        PlayListIds = playListIds.ToArray(),
                        VideoIds = videoIds.ToArray(),
                        Status = videoChannel.Status,
                        CreatedDate = videoChannel.CreatedDate,
                        FollowCount = videoChannel.FollowCount,
                        VideoCount = videoChannel.VideoCount,
                        ZoneName= videoChannel.ZoneName,
                        Mode= videoChannel.Mode
                    };
                    BoSearch.Base.VideoChannel.VideoChannelDalFactory.UpdateVideoChannel(videoChannelES);

                    //ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteChannel(int channelId)
        {
            var result= VideoChannelDal.Delete(channelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
            if(result== ErrorMapping.ErrorCodes.Success)
            {
                BoCached.Base.VideoChannel.VideoChannelDalFactory.Delete(channelId);
                BoSearch.Base.VideoChannel.VideoChannelDalFactory.Delete(channelId);
            }
            return result;
        }

        public static List<VideoChannelSearchEntity> SearchVideoChannel(string keyword, string zoneIds, string labelIds, string playlistIds, string videoIds, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                return BoSearch.Base.VideoChannel.VideoChannelDalFactory.SearchVideoChannel(keyword, zoneIds, labelIds, playlistIds, videoIds, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            }
            catch
            {
                return new List<VideoChannelSearchEntity>();
            }
        }

        public static VideoChannelEntity GetVideoChannelDetail(int id, string currentUsername)
        {
            try
            {
                var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelById(id);
                if (videoChannel == null)
                {
                    videoChannel = VideoChannelDal.GetById(id);
                    if (videoChannel != null)
                    {
                        //add redis
                        BoCached.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannel(videoChannel);
                    }
                }
                return videoChannel;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoChannelCachedEntity GetVideoChannelDetailEdit(int id, string currentUsername)
        {
            try
            {
                var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelById(id);
                if (videoChannel == null)
                {
                    videoChannel = VideoChannelDal.GetById(id);
                    if (videoChannel != null)
                    {
                        //add redis
                        BoCached.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannel(videoChannel);
                    }
                }

                var videoChannelDetail = new VideoChannelCachedEntity();
                videoChannelDetail.VideoChannelInfo = videoChannel;
                videoChannelDetail.PlayList = PlaylistBo.GetListByVideoChannelId(id);
                videoChannelDetail.ZoneVideoList = ZoneVideoBo.GetListZoneInVideoChannel(id);

                return videoChannelDetail;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoChannelEntity> GetListVideoChannelByVideoId(int videoId)
        {
            try
            {
                var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByVideoId(videoId);
                if (videoChannel == null || (videoChannel != null && videoChannel.Count() <= 0))
                {
                    videoChannel = VideoChannelDal.GetVideoChannelByVideoId(videoId);
                    if (videoChannel != null && videoChannel.Count() > 0)
                    {
                        //add redis
                        BoCached.Base.VideoChannel.VideoChannelDalFactory.AddVideoChannelListByVideoId(videoId, videoChannel);
                    }
                }
                return videoChannel;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoChannelDal.GetVideoChannelByVideoId:{0}", ex.Message));
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsAllVideoChannel(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = VideoChannelDal.InitAllVideoChannel(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VideoChannelDal.InitAllVideoChannel(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new VideoChannelSearchEntity
                {
                    Id = s.Id,
                    ZoneId=s.ZoneId,
                    PublisherId=s.PublisherId,
                    Name = s.Name,
                    Avatar = s.Avatar,
                    Labelds=new string[] {},
                    PlayListIds=new string[] { },
                    VideoIds= new string[] { },                    
                    FollowCount = s.FollowCount,                                        
                    VideoCount = s.VideoCount,                    
                    ZoneIds = ZoneVideoDal.GetListZoneRelationByChannelId(s.Id).Select(z => z.Id.ToString()).ToArray(),                    
                    CreatedDate = s.CreatedDate,                    
                    Status = s.Status                                     
                }).ToList();
                BoSearch.Base.VideoChannel.VideoChannelDalFactory.InitAllVideoChannel(list);
            }, action);
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllVideoChannel(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = VideoChannelDal.InitAllVideoChannel(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VideoChannelDal.InitAllVideoChannel(page, pageSize, startDate, endDate, ref totalRows);

                BoCached.Base.VideoChannel.VideoChannelDalFactory.InitAllVideoChannel(data);
            }, action);
        }
    }
}
