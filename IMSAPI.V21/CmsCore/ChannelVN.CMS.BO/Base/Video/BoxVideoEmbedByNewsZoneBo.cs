﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class BoxVideoEmbedByNewsZoneBo
    {
        public static List<BoxVideoEmbedByNewsZoneEntity> GetListVideoEmbed(int zoneId, int type)
        {
            var VideoEmbed = new List<BoxVideoEmbedByNewsZoneEntity>();
            try
            {

                return BoxVideoEmbedByNewsZoneDal.GetListVideoEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static ErrorMapping.ErrorCodes Insert(BoxVideoEmbedByNewsZoneEntity VideoEmbebBox)
        {
            try
            {
                if (null != VideoEmbebBox)
                {
                    return BoxVideoEmbedByNewsZoneDal.Insert(VideoEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedByNewsZoneDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listVideoId, int zoneId, int type)
        {
            try
            {
                if (null != listVideoId)
                {
                    BoxVideoEmbedByNewsZoneDal.Update(listVideoId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedByNewsZoneDal.Update:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(long VideoId, int zoneId, int type)
        {
            try
            {
                if (VideoId > 0 && zoneId > 0)
                {
                    BoxVideoEmbedByNewsZoneDal.Delete(VideoId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoEmbedByNewsZoneDal.Delete:{0}", ex.Message));
            }

        }
    }
}
