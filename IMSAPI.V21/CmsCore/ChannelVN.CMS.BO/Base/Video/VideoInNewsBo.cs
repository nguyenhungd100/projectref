﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoInNewsBo
    {
        public static ErrorMapping.ErrorCodes UpdateVideoInNews(string videoInNews, long newsId)
        {
            try
            {
                if (VideoInNewsDal.Insert(videoInNews, newsId))
                {
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

    }
}
