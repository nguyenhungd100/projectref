﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class BoxVideoProgramEmbedBo
    {
        public static List<ZoneVideoEntity> SearchProgramEmbed(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            var VideoEmbed = new List<ZoneVideoEntity>();
            try
            {
                return BoxVideoProgramEmbedDal.SearchProgramEmbed(zoneId, keyword, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }
        public static List<BoxVideoProgramEmbedEntity> GetListProgramEmbed(int zoneId, int type)
        {
            var VideoEmbed = new List<BoxVideoProgramEmbedEntity>();
            try
            {
                return BoxVideoProgramEmbedDal.GetListVideoProgramEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static ErrorMapping.ErrorCodes Update(string listVideoId, int zoneId, int type)
        {
            try
            {
                if (null != listVideoId)
                {
                    BoxVideoProgramEmbedDal.Update(listVideoId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxVideoProgramEmbedDal.Update:{0}", ex.Message));
            }

        }
    }
}
