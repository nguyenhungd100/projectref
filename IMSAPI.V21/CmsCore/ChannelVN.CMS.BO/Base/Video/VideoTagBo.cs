﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoTagBo
    {
        public static ErrorMapping.ErrorCodes InsertVideoTag(VideoTagEntity videoTag, ref int videoTagId)
        {
            if (string.IsNullOrEmpty(videoTag.Name))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }
            videoTag.UnsignName = Utility.UnicodeToKoDau(videoTag.Name);
            videoTag.Url = Utility.UnicodeToKoDauAndGach(videoTag.Name);
            return VideoTagDal.Insert(videoTag, ref videoTagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoTag(VideoTagEntity videoTag)
        {
            if (string.IsNullOrEmpty(videoTag.Name))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }
            videoTag.UnsignName = Utility.UnicodeToKoDau(videoTag.Name);
            videoTag.Url = Utility.UnicodeToKoDauAndGach(videoTag.Name);
            return VideoTagDal.Update(videoTag)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoTag(string videoTagName, ref int videoTagId)
        {
            if (string.IsNullOrEmpty(videoTagName))
            {
                return ErrorMapping.ErrorCodes.VideoTagMustHaveName;
            }
            var tag = new VideoTagEntity()
                          {
                              Name = videoTagName,
                              UnsignName = Utility.UnicodeToKoDau(videoTagName),
                              Url = Utility.UnicodeToKoDauAndGach(videoTagName),
                              CreatedBy = WcfMessageHeader.Current.ClientUsername,
                              Description = videoTagName,
                              IsHotTag = false,
                              Status = (int)EnumVideoTagStatus.Actived
                          };
            return VideoTagDal.UpdateByName(tag, ref videoTagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes UpdateVideoTagMode(int videoTagId, bool isHotTag)
        {
            return VideoTagDal.UpdateMode(videoTagId, isHotTag)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<VideoTagEntity> SearchVideoTag(string keyword, int parentId, EnumVideoTagMode tagMode, EnumVideoTagStatus status, EnumVideoTagSort orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow)
        {
            return VideoTagDal.Search(keyword, parentId, (int)tagMode, (int)status, (int)orderBy, getTagHasVideoOnly, pageIndex,
                                      pageSize, ref totalRow);
        }
        public static List<VideoTagEntity> GetVideoTagByZoneVideoId(int zoneVideoId)
        {
            return VideoTagDal.GetByZoneVideoId(zoneVideoId);
        }
        public static VideoTagEntity GetVideoTagById(int id)
        {
            return VideoTagDal.GetById(id);
        }

        public static VideoTagEntity GetVideoTagByName(string name)
        {
            return VideoTagDal.GetByName(name);
        }

        public static List<VideoTagEntity> GetVideoTagByIds(string tagIds)
        {
            var data = BoCached.Base.Video.VideoTagDalFactory.GetVideoTagByIds(tagIds);
            if(data==null || (data!=null && data.Count() <= 0))
            {
                data= VideoTagDal.GetByIds(tagIds);
            }

            return data;
        }
    }
}
