﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoLabelBo
    {        
        public static ErrorMapping.ErrorCodes InsertVideoLabel(VideoLabelEntity videoLabel, ref int videoLabelId)
        {
            if (string.IsNullOrEmpty(videoLabel.Name))
            {
                return ErrorMapping.ErrorCodes.VideoLabelMustHaveName;
            }
            return VideoLabelDal.Insert(videoLabel, ref videoLabelId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static ErrorMapping.ErrorCodes UpdateVideoLabel(VideoLabelEntity videoLabel)
        {
            if (string.IsNullOrEmpty(videoLabel.Name))
            {
                return ErrorMapping.ErrorCodes.VideoLabelMustHaveName;
            }
            return VideoLabelDal.Update(videoLabel)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }        

        public static ErrorMapping.ErrorCodes DeleteVideoLabel(int zoneVideoId)
        {
            return VideoLabelDal.Delete(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static VideoLabelEntity GetVideoLabelById(int labelId)
        {            
            return VideoLabelDal.GetById(labelId);            
        }

        public static List<VideoLabelEntity> GetListByParentId(int parentId, int isSystem)
        {
            return VideoLabelDal.GetListByParentId(parentId, isSystem);
        }

        public static ErrorMapping.ErrorCodes GetVideoLabelIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {
            return VideoLabelDal.GetVideoLabelIdByNewsZoneId(newsZoneId, ref zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static ErrorMapping.ErrorCodes MoveVideoLabelUp(int zoneVideoId)
        {
            return VideoLabelDal.MoveUp(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static ErrorMapping.ErrorCodes MoveVideoLabelDown(int zoneVideoId)
        {
            return VideoLabelDal.MoveDown(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }
    }
}
