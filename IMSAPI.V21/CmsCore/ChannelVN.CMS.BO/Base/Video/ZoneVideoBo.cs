﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class ZoneVideoBo
    {
        public static List<ZoneVideoEntity> GetListByParentId(int parentId, int status)
        {
            try
            {
                //var data= BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoByParentId(parentId, status);
                //if (data == null || (data != null && data.Count() <= 0))
                //{
                //    data = ZoneVideoDal.GetListByParentId(parentId, status);
                //    if(data!=null && data.Count > 0)
                //    {
                //        BoCached.Base.ZoneVideo.ZoneVideoDalFactory.InitAllZoneVideo(data);
                //    }
                //}
                return ZoneVideoDal.GetListByParentId(parentId, status);            
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetListByParentId:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> GetPrivateZoneVideo(string username, int parentZoneId, int status)
        {
            try
            {
                var zones = GetListZoneVideoActivedByUsernameAndPermissionIds(username, (int)EnumPermission.ApproveVideo, (int)EnumPermission.VideoManager);
                if (parentZoneId != -1)
                {
                    zones = zones.Where(s => s.ParentId == parentZoneId && s.Status==status).ToList();
                }
                return zones;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneVideoEntity>();
            }
        }
        public static List<ZoneVideoEntity> GetListZoneVideoActivedByUsernameAndPermissionIds(string username, params int[] permissionIds)
        {
            try
            {
                var listPermissionId = permissionIds.Aggregate("", (current, permissionId) => current + (";" + permissionId));
                if (!string.IsNullOrEmpty(listPermissionId)) listPermissionId = listPermissionId.Remove(0, 1);

                var allZone = BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoActivedByUsernameAndPermissionIds(username, listPermissionId);
                if (allZone == null || (allZone != null && allZone.Count() <= 0))
                {
                    allZone = ZoneVideoDal.GetZoneVideoActivedByUsernameAndPermissionIds(username, listPermissionId);                    
                }

                return allZone;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneVideoEntity>();
            }
        }

        public static List<ZoneVideoEntity> GetListZoneRelation(int videoId)
        {
            try
            {
                var data = BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoByVideoId(videoId);
                if(data==null || (data!=null && data.Count() <= 0))
                {
                    data= ZoneVideoDal.GetListZoneRelation(videoId);
                    if (data != null)
                    {
                        //add to redis
                        BoCached.Base.Video.VideoDalFactory.AddListZoneRelationByVideoId(videoId,data);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetListZoneRelation:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> GetListZoneInPlayList(int playListId)
        {
            try
            {
                var data = BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoByPlayListId(playListId);
                if (data == null || (data != null && data.Count() <= 0))
                {
                    data = ZoneVideoDal.GetListZoneRelationByPlayListId(playListId);
                    if (data != null)
                    {
                        //add to redis
                        BoCached.Base.PlayList.PlayListDalFactory.AddListZoneRelationByPlayListId(playListId, data);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetListZoneRelation:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> GetListZoneInVideoChannel(int channelId)
        {
            try
            {
                var data = BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoByChannelId(channelId);
                if (data == null || (data != null && data.Count() <= 0))
                {
                    data = ZoneVideoDal.GetListZoneRelationByChannelId(channelId);
                    if (data != null)
                    {
                        //add to redis
                        BoCached.Base.PlayList.PlayListDalFactory.AddListZoneRelationByChannelId(channelId, data);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetListZoneRelation:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            try
            {
                return ZoneVideoDal.GetActiveZoneInDay(zoneIds, viewDate,mode);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetActiveZoneInDay:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            try
            {
                return ZoneVideoDal.GetActiveZoneInDayV2(zoneIds, viewDate, mode);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetActiveZoneInDay:{0}", ex.Message));
            }
        }

        public static ZoneVideoEntity GetZoneVideoByZoneVideoId(int zoneVideoId)
        {
            try
            {
                var zoneVideo = BoCached.Base.ZoneVideo.ZoneVideoDalFactory.GetZoneVideoById(zoneVideoId);
                if (zoneVideo == null) {
                    zoneVideo= ZoneVideoDal.GetById(zoneVideoId);
                    if (zoneVideo != null)
                    {
                        //add redis
                        BoCached.Base.ZoneVideo.ZoneVideoDalFactory.AddZoneVideo(zoneVideo);
                    }
                }

                return zoneVideo;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetZoneVideoByZoneVideoId:{0}", ex.Message));
            }
        }

        public static ZoneVideoDetailEntity GetZoneVideoDetailByZoneVideoId(int zoneVideoId)
        {
            try
            {
                var zoneVideo = ZoneVideoDal.GetById(zoneVideoId);
                if (zoneVideo != null)
                {
                    return new ZoneVideoDetailEntity()
                    {
                        ZoneVideo = zoneVideo,
                        VideoTags = VideoTagDal.GetByZoneVideoId(zoneVideoId)
                    };
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetZoneVideoByZoneVideoId:{0}", ex.Message));
            }
        }

        public static List<ZoneVideoEntity> BindAllOfZoneVideoToTreeviewFullDepth(List<ZoneVideoEntity> allZones, string prefix)
        {
            var allZonesWithSimpleField = new List<ZoneVideoEntity>();
            var count = allZones.Count;
            foreach (var zone in allZones)
            {
                if (zone.Status == 1)
                {
                    allZonesWithSimpleField.Add(new ZoneVideoEntity
                    {
                        Id = zone.Id,
                        ParentId = zone.ParentId,
                        Name = zone.Name,
                        Url = zone.Url,
                        Order = zone.Order,
                        Status = zone.Status,
                        Invisibled = zone.Invisibled,
                        CatId = zone.CatId,
                        DisplayStyle = zone.DisplayStyle,
                        ShowOnHome = zone.ShowOnHome
                    });
                }
            }

            allZonesWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.Order);
            var outputZones = new List<ZoneVideoEntity>();
            BindAllOfZoneVideoToTreeviewFullDepth(allZonesWithSimpleField.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        private static void BindAllOfZoneVideoToTreeviewFullDepth(List<ZoneVideoEntity> allZones, int currentParentId, string prefix, ref List<ZoneVideoEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentId == currentParentId)
                {
                    allZones[i].Name = currentPrefix + " " + allZones[i].Name;
                    outputZones.Add(allZones[i]);
                    BindAllOfZoneVideoToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }

        public static List<ZoneVideoEntity> BuildTree(List<ZoneVideoEntity> zoneList, string prefix)
        {
            if (zoneList == null || zoneList.Count <= 0) return new List<ZoneVideoEntity>();

            var zoneListOut = new List<ZoneVideoEntity>();
            foreach (var zone in zoneList)
            {
                if (zone.ParentId != 0) continue;
                var item = zone;
                zoneListOut.Add(new ZoneVideoEntity()
                                    {
                                        Id = item.Id,
                                        Name = item.Name,
                                        ParentId = item.ParentId,
                                        Url = item.Url,
                                        Order = item.Order,
                                        CatId = item.CatId,
                                        CreatedDate = item.CreatedDate,
                                        DisplayStyle = item.DisplayStyle,
                                        Invisibled = item.Invisibled,
                                        ModifiedDate = item.ModifiedDate,
                                        NumberOfChild = item.NumberOfChild,
                                        ShowOnHome = item.ShowOnHome,
                                        Status = item.Status
                                    });
                zoneListOut.AddRange(
                    zoneList.Where(subZone => subZone.ParentId == item.Id).Select(
                        subZone => new ZoneVideoEntity()
                                       {
                                           Id = subZone.Id,
                                           Name = prefix + subZone.Name,
                                           ParentId = subZone.ParentId,
                                           Url = subZone.Url,
                                           Order = subZone.Order,
                                           CatId = subZone.CatId,
                                           CreatedDate = subZone.CreatedDate,
                                           DisplayStyle = subZone.DisplayStyle,
                                           Invisibled = subZone.Invisibled,
                                           ModifiedDate = subZone.ModifiedDate,
                                           NumberOfChild = subZone.NumberOfChild,
                                           ShowOnHome = subZone.ShowOnHome,
                                           Status = subZone.Status
                                       }));
            }
            return zoneListOut;
        }

        public static ErrorMapping.ErrorCodes InsertVideoZone(ZoneVideoEntity zoneVideo, string videoTagId, ref int zoneVideoId)
        {
            if (string.IsNullOrEmpty(zoneVideo.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result= ZoneVideoDal.Insert(zoneVideo, videoTagId, ref zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
            if(result == ErrorMapping.ErrorCodes.Success)
            {
                zoneVideo.Id = zoneVideoId;
                BoCached.Base.ZoneVideo.ZoneVideoDalFactory.AddZoneVideo(zoneVideo);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes InsertVideoZoneInitDB(ZoneVideoEntity zoneVideo, string videoTagId)
        {
            if (string.IsNullOrEmpty(zoneVideo.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = ZoneVideoDal.InsertInitDB(zoneVideo, videoTagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
            if (result == ErrorMapping.ErrorCodes.Success)
            {               
                BoCached.Base.ZoneVideo.ZoneVideoDalFactory.AddZoneVideo(zoneVideo);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateVideoZone(ZoneVideoEntity zoneVideo, string videoTagId)
        {
            if (string.IsNullOrEmpty(zoneVideo.Name))
            {
                return ErrorMapping.ErrorCodes.ZoneVideoMustHaveName;
            }
            var result = ZoneVideoDal.Update(zoneVideo, videoTagId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                BoCached.Base.ZoneVideo.ZoneVideoDalFactory.AddZoneVideo(zoneVideo);
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes MoveVideoZoneUp(int zoneVideoId)
        {
            return ZoneVideoDal.MoveUp(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static ErrorMapping.ErrorCodes MoveVideoZoneDown(int zoneVideoId)
        {
            return ZoneVideoDal.MoveDown(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static ErrorMapping.ErrorCodes DeleteVideoZone(int zoneVideoId)
        {
            return ZoneVideoDal.Delete(zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static List<ZoneVideoEntity> GetListByParentIdExternalCms(int parentId, int status)
        {
            try
            {
                return ZoneVideoDal.GetListByParentIdFromExternal(parentId, status);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetListByParentId:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {
            return ZoneVideoDal.GetZoneVideoIdByNewsZoneId(newsZoneId, ref zoneVideoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.VideoNotAvailable;
        }

        public static List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            try
            {
                return ZoneVideoDal.GetNotifyZoneInDay(viewDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.GetNotifyZoneInDay:{0}", ex.Message));
            }
        }
        public static ErrorMapping.ErrorCodes UpdateAvatarZoneVideo(string ListAvatar)
        {
            try
            {
                return ZoneVideoDal.UpdateAvatarZoneVideo(ListAvatar) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.VideoNotAvailable; ;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("ZoneVideoDal.UpdateAvatarZoneVideo:{0}", ex.Message));
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllZoneVideo(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = ZoneVideoDal.GetListByParentId(-1,-1);
                //init redis
                BoCached.Base.ZoneVideo.ZoneVideoDalFactory.InitAllZoneVideo(data);
            }, action);
        }
    }
}
