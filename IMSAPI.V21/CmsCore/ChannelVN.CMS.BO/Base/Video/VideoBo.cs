﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Video;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BoSearch.Entity;
using System.Threading.Tasks;
using ChannelVN.CMS.BO.Video;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.BO.Base.Video
{
    public class VideoBo
    {
        #region Update
        public static ErrorMapping.ErrorCodes UpdateConvertedMode(string listVideoId)
        {
            try
            {
                if (listVideoId.EndsWith(",")) listVideoId = listVideoId.Remove(listVideoId.Length - 1, 1);
                if (listVideoId.StartsWith(",")) listVideoId = listVideoId.Remove(0, 1);
                return VideoDal.UpdateConvertedMode(listVideoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Insert(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                //videoEntity.Status = (int)EnumVideoStatus.Temporary;

                var success = VideoDal.Insert(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);
                if (success)
                {
                    if (!string.IsNullOrEmpty(videoEntity.FileName))
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes InsertV3(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }

                var success = VideoDal.InsertV3(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                if (success)
                {
                    videoEntity.Id = videoId;
                    if (!string.IsNullOrEmpty(videoEntity.Url))
                        videoEntity.Url = videoEntity.Url.Replace("{VideoId}", videoId.ToString());
                    if (!string.IsNullOrEmpty(videoEntity.OriginalUrl))
                        videoEntity.OriginalUrl = videoEntity.OriginalUrl.Replace("{VideoId}", videoId.ToString());

                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(videoEntity);
                    //inser es
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }
                    var channelListIds = new List<string>();
                    if (!string.IsNullOrEmpty(channelIdList))
                    {
                        channelListIds.AddRange(channelIdList.Split(';'));
                    }
                    var listType = new List<string>();
                    //listType.Add(videoEntity.Type.ToString());

                    var videoES = new VideoSearchEntity
                    {
                        Id = videoEntity.Id,
                        ZoneId = videoEntity.ZoneId,
                        Name = videoEntity.Name,
                        Avatar = videoEntity.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        PlaylistIds = playListIds.ToArray(),
                        CreatedBy = videoEntity.CreatedBy,
                        CreatedDate = videoEntity.CreatedDate,
                        DistributionDate = videoEntity.DistributionDate,
                        Mode = videoEntity.Mode,
                        Status = videoEntity.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = videoEntity.KeyVideo,
                        ZoneName = videoEntity.ZoneName,
                        HtmlCode = videoEntity.HtmlCode,
                        ChannelIds = channelListIds.ToArray(),
                        ParentId = videoEntity.ParentId,
                        Type = videoEntity.Type,
                        ListType = listType.ToArray(),
                        Duration = videoEntity.Duration,
                        OriginalId = videoEntity.OriginalId,
                        Description = videoEntity.Description,
                        Url = videoEntity.Url,
                        FileName = videoEntity.FileName,
                        Namespace = videoEntity.Namespace
                    };
                    BoSearch.Base.Video.VideoDalFactory.AddVideo(videoES);

                    //xóa cache playlist in video
                    FuncTaskAsyn(playListIds);

                    if (!string.IsNullOrEmpty(videoEntity.FileName))
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name + " => filename: " + videoEntity.FileName);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes InsertV4(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }

                var success = VideoDal.InsertV4(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                if (success)
                {
                    videoEntity.Id = videoId;
                    if (!string.IsNullOrEmpty(videoEntity.Url))
                        videoEntity.Url = videoEntity.Url.Replace("{VideoId}", videoId.ToString());
                    if (!string.IsNullOrEmpty(videoEntity.OriginalUrl))
                        videoEntity.OriginalUrl = videoEntity.OriginalUrl.Replace("{VideoId}", videoId.ToString());

                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(videoEntity);
                    //inser es
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }
                    var channelListIds = new List<string>();
                    if (!string.IsNullOrEmpty(channelIdList))
                    {
                        channelListIds.AddRange(channelIdList.Split(';'));
                    }
                    var listType = new List<string>();
                    //listType.Add(videoEntity.Type.ToString());

                    var videoES = new VideoSearchEntity
                    {
                        Id = videoEntity.Id,
                        ZoneId = videoEntity.ZoneId,
                        Name = videoEntity.Name,
                        Avatar = videoEntity.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        PlaylistIds = playListIds.ToArray(),
                        CreatedBy = videoEntity.CreatedBy,
                        CreatedDate = videoEntity.CreatedDate,
                        DistributionDate = videoEntity.DistributionDate,
                        Mode = videoEntity.Mode,
                        Status = videoEntity.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = videoEntity.KeyVideo,
                        ZoneName = videoEntity.ZoneName,
                        HtmlCode = videoEntity.HtmlCode,
                        ChannelIds = channelListIds.ToArray(),
                        ParentId = videoEntity.ParentId,
                        Type = videoEntity.Type,
                        ListType = listType.ToArray(),
                        Duration = videoEntity.Duration,
                        OriginalId = videoEntity.OriginalId,
                        Description = videoEntity.Description,
                        Url = videoEntity.Url,
                        FileName = videoEntity.FileName,
                        Namespace = videoEntity.Namespace,
                        IsOnHome = videoEntity.IsOnHome,
                        IsAMP = videoEntity.IsAMP,
                        SourceId = videoEntity.SourceId
                    };
                    BoSearch.Base.Video.VideoDalFactory.AddVideo(videoES);

                    //xóa cache playlist in video
                    FuncTaskAsyn(playListIds);

                    if (!string.IsNullOrEmpty(videoEntity.FileName))
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name + " => filename: " + videoEntity.FileName);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes InsertVideoInitDB(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }

                var success = VideoDal.InsertVideoInitDB(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
                if (success)
                {
                    //videoEntity.Id = videoId;
                    if (!string.IsNullOrEmpty(videoEntity.Url))
                        videoEntity.Url = videoEntity.Url.Replace("{VideoId}", videoEntity.Id.ToString());
                    //if (!string.IsNullOrEmpty(videoEntity.OriginalUrl))
                    //    videoEntity.OriginalUrl = videoEntity.OriginalUrl.Replace("{VideoId}", videoId.ToString());

                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(videoEntity);
                    //inser es
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }
                    var channelListIds = new List<string>();
                    if (!string.IsNullOrEmpty(channelIdList))
                    {
                        channelListIds.AddRange(channelIdList.Split(';'));
                    }
                    var listType = new List<string>();
                    //listType.Add(videoEntity.Type.ToString());

                    var videoES = new VideoSearchEntity
                    {
                        Id = videoEntity.Id,
                        ZoneId = videoEntity.ZoneId,
                        Name = videoEntity.Name,
                        Avatar = videoEntity.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        PlaylistIds = playListIds.ToArray(),
                        CreatedBy = videoEntity.CreatedBy,
                        CreatedDate = videoEntity.CreatedDate,
                        DistributionDate = videoEntity.DistributionDate,
                        Mode = videoEntity.Mode,
                        Status = videoEntity.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = videoEntity.KeyVideo,
                        ZoneName = videoEntity.ZoneName,
                        HtmlCode = videoEntity.HtmlCode,
                        ChannelIds = channelListIds.ToArray(),
                        ParentId = videoEntity.ParentId,
                        Type = videoEntity.Type,
                        ListType = listType.ToArray(),
                        Duration = videoEntity.Duration,
                        OriginalId = videoEntity.OriginalId,
                        Description = videoEntity.Description,
                        Url = videoEntity.Url,
                        FileName = videoEntity.FileName,
                        Namespace = videoEntity.Namespace
                    };
                    BoSearch.Base.Video.VideoDalFactory.AddVideo(videoES);

                    //xóa cache playlist in video
                    //FuncTaskAsyn(playListIds);

                    //if (!string.IsNullOrEmpty(videoEntity.FileName))
                    //{
                    //    ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name + " => filename: " + videoEntity.FileName);
                    //}
                    //ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes InsertVideoInfo(VideoEntity videoEntity, ref int videoId)
        {
            try
            {
                var success = VideoDal.InsertVideoInfo(videoEntity, ref videoId);
                if (success)
                {
                    videoEntity.Id = videoId;
                    if (!string.IsNullOrEmpty(videoEntity.Url))
                        videoEntity.Url = videoEntity.Url.Replace("{VideoId}", videoId.ToString());
                    if (!string.IsNullOrEmpty(videoEntity.OriginalUrl))
                        videoEntity.OriginalUrl = videoEntity.OriginalUrl.Replace("{VideoId}", videoId.ToString());

                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(videoEntity);
                    //inser es
                    var zoneids = new List<string>();
                    if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }

                    var listType = new List<string>();

                    var videoES = new VideoSearchEntity
                    {
                        Id = videoEntity.Id,
                        ZoneId = videoEntity.ZoneId,
                        Name = videoEntity.Name,
                        Avatar = videoEntity.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        CreatedBy = videoEntity.CreatedBy,
                        CreatedDate = videoEntity.CreatedDate,
                        Status = videoEntity.Status,
                        KeyVideo = videoEntity.KeyVideo,
                        ZoneName = videoEntity.ZoneName,
                        HtmlCode = videoEntity.HtmlCode,
                        Duration = videoEntity.Duration,
                        OriginalId = videoEntity.OriginalId,
                        Description = videoEntity.Description,
                        Url = videoEntity.Url,
                        FileName = videoEntity.FileName,
                        PlaylistIds = new string[] { },
                        ChannelIds = new string[] { }
                    };
                    BoSearch.Base.Video.VideoDalFactory.AddVideo(videoES);

                    if (videoEntity.Id > 0)
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name + " => filename: " + videoEntity.FileName);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes InsertV2(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var success = VideoDal.InsertV2(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);
                if (success)
                {
                    if (!string.IsNullOrEmpty(videoEntity.FileName))
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Update(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var zoneName = "";
                var video = VideoDal.GetById(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                    {
                        // Nếu là quyền upload => chỉ được sửa video do mình upload
                        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                        {
                            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                        }
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                    }
                }

                var isUploadNewVideo = (video.FileName != videoEntity.FileName);
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.Mode = videoEntity.Mode;
                video.DistributionDate = videoEntity.DistributionDate == DateTime.MinValue ? DateTime.Now : videoEntity.DistributionDate;
                video.VideoRelation = videoEntity.VideoRelation;
                video.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                if (video.DistributionDate > DateTime.MinValue)
                {
                    video.DistributionDate = videoEntity.DistributionDate;
                }

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }
                // Update video status
                if (videoEntity.Status != (int)EnumVideoStatus.AllStatus)
                    video.Status = videoEntity.Status;

                if (video.Status == (int)EnumVideoStatus.Published && string.IsNullOrEmpty(video.PublishBy))
                {
                    video.PublishDate = videoEntity.DistributionDate;
                    video.PublishBy = WcfMessageHeader.Current.ClientUsername;
                }

                if (video.Status == (int)EnumVideoStatus.Return)
                {
                    video.Status = (int)EnumVideoStatus.WaitForEdit;
                }

                var success = VideoDal.Update(video, tagIdList, tagNameList, zoneIdList, playlistIdList);
                if (success)
                {
                    if (isUploadNewVideo)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                    }
                    ActivityBo.LogAddNewVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateV4(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var zoneName = videoEntity.ZoneName;
                var video = GetById(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                    {
                        // Nếu là quyền upload => chỉ được sửa video do mình upload
                        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                        {
                            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                        }
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                    }
                }

                var isUploadNewVideo = (video.FileName != videoEntity.FileName);
                var fileNameOld = video.FileName;
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.OriginalUrl = videoEntity.OriginalUrl;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.Mode = videoEntity.Mode;
                video.DistributionDate = videoEntity.DistributionDate == DateTime.MinValue ? DateTime.Now : videoEntity.DistributionDate;
                video.VideoRelation = videoEntity.VideoRelation;
                video.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                video.AvatarShareFacebook = videoEntity.AvatarShareFacebook;
                video.OriginalId = videoEntity.OriginalId;
                video.Author = videoEntity.Author;
                video.ZoneName = videoEntity.ZoneName;
                video.ParentId = videoEntity.ParentId;
                video.HashId = videoEntity.HashId;
                video.Type = videoEntity.Type;
                video.TrailerUrl = videoEntity.TrailerUrl;
                video.MetaAvatar = videoEntity.MetaAvatar;
                video.Location = videoEntity.Location;
                video.PolicyContentId = videoEntity.PolicyContentId;
                video.DisplayStyle = videoEntity.DisplayStyle;
                video.Namespace = videoEntity.Namespace;

                if (video.DistributionDate > DateTime.MinValue)
                {
                    video.DistributionDate = videoEntity.DistributionDate;
                }

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }
                // Update video status
                if (videoEntity.Status != (int)EnumVideoStatus.AllStatus)
                    video.Status = videoEntity.Status;

                if (video.Status == (int)EnumVideoStatus.Published && string.IsNullOrEmpty(video.PublishBy))
                {
                    video.PublishDate = videoEntity.DistributionDate;
                    video.PublishBy = WcfMessageHeader.Current.ClientUsername;
                }

                //bỏ trạng thái chờ biên tập
                //if (video.Status == (int)EnumVideoStatus.Return)
                //{
                //    video.Status = (int)EnumVideoStatus.WaitForEdit;
                //}
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }

                var success = VideoDal.UpdateV4(video, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
                if (success)
                {
                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(video);
                    //inser es
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }

                    var channelListIds = new List<string>();
                    if (!string.IsNullOrEmpty(channelIdList))
                    {
                        channelListIds.AddRange(channelIdList.Split(';'));
                    }

                    var videoES = new VideoSearchEntity
                    {
                        Id = video.Id,
                        ZoneId = video.ZoneId,
                        Name = video.Name,
                        Avatar = video.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        PlaylistIds = playListIds.ToArray(),
                        LastModifiedBy = video.LastModifiedBy,
                        LastModifiedDate = video.LastModifiedDate,
                        DistributionDate = video.DistributionDate,
                        Mode = video.Mode,
                        Status = video.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = video.KeyVideo,
                        ZoneName = video.ZoneName,
                        HtmlCode = video.HtmlCode,
                        ChannelIds = channelListIds.ToArray(),
                        ParentId = video.ParentId,
                        Type = video.Type,
                        Duration = video.Duration,
                        Description = video.Description,
                        Url = video.Url,
                        FileName = video.FileName
                    };
                    BoSearch.Base.Video.VideoDalFactory.UpdateVideo(videoES);

                    //xóa cache playlist in video
                    FuncTaskAsyn(playListIds);

                    if (isUploadNewVideo)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name + " => filename: " + video.FileName);
                    }
                    ActivityBo.LogUpdateVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name, "filenameOld: " + fileNameOld);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateVideoInfo(VideoEntity videoEntity)
        {
            try
            {
                var zoneName = videoEntity.ZoneName;
                var video = GetById(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                // Check update status
                if (video.Status == (int)EnumVideoStatus.Published)
                    return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;

                //if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                //{
                //    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                //    {
                //        // Nếu là quyền upload => chỉ được sửa video do mình upload
                //        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                //        {
                //            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                //        }
                //    }
                //    else
                //    {
                //        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                //    }
                //}

                var fileNameOld = video.FileName;
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.OriginalUrl = videoEntity.OriginalUrl;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.LastModifiedBy = videoEntity.LastModifiedBy;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                video.OriginalId = videoEntity.OriginalId;
                video.Author = videoEntity.Author;
                video.ZoneName = videoEntity.ZoneName;
                video.Location = videoEntity.Location;
                video.PolicyContentId = videoEntity.PolicyContentId;

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }

                var success = VideoDal.UpdateVideoInfo(video);
                if (success)
                {
                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(video);
                    //inser es
                    var zoneids = new List<string>();
                    if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }

                    var videoES = new VideoSearchEntity
                    {
                        Id = video.Id,
                        ZoneId = video.ZoneId,
                        Name = video.Name,
                        Avatar = video.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        LastModifiedBy = video.LastModifiedBy,
                        LastModifiedDate = video.LastModifiedDate,
                        Status = video.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = video.KeyVideo,
                        ZoneName = video.ZoneName,
                        HtmlCode = video.HtmlCode,
                        Duration = video.Duration,
                        Description = video.Description,
                        Url = video.Url,
                        FileName = video.FileName
                    };
                    BoSearch.Base.Video.VideoDalFactory.UpdateVideo(videoES);

                    if (video.Id > 0)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name + " => filename: " + video.FileName);
                    }
                    ActivityBo.LogUpdateVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name, "filenameOld: " + fileNameOld);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        private static async void FuncTaskAsyn(List<string> playListIds)
        {
            await Task.Run(() =>
            {
                try
                {
                    //xoa cache video in playlist
                    foreach (var playlistId in playListIds)
                    {
                        var hashName = "VideoInPlaylistSearch:" + playlistId;
                        BoCached.Base.Video.VideoDalFactory.DeleteDataSearchVideoInPlaylist(hashName);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "UpdateVideo => RemoveCachedPlayListInVideoTask() error => " + ex);
                }
            }).ContinueWith(c =>
            {
                //todo
            });
        }

        public static ErrorMapping.ErrorCodes UpdateWithoutPermission(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var zoneName = "";
                var video = VideoDal.GetById(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                //if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                //{
                //    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                //    {
                //        // Nếu là quyền upload => chỉ được sửa video do mình upload
                //        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                //        {
                //            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                //        }
                //    }
                //    else
                //    {
                //        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                //    }
                //}

                var isUploadNewVideo = (video.FileName != videoEntity.FileName);
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.Mode = videoEntity.Mode;
                video.DistributionDate = videoEntity.DistributionDate == DateTime.MinValue ? DateTime.Now : videoEntity.DistributionDate;
                video.VideoRelation = videoEntity.VideoRelation;
                video.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                if (video.DistributionDate > DateTime.MinValue)
                {
                    video.DistributionDate = videoEntity.DistributionDate;
                }

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }
                // Update video status
                if (videoEntity.Status != (int)EnumVideoStatus.AllStatus)
                    video.Status = videoEntity.Status;

                if (video.Status == (int)EnumVideoStatus.Published && string.IsNullOrEmpty(video.PublishBy))
                {
                    video.PublishDate = videoEntity.DistributionDate;
                    video.PublishBy = WcfMessageHeader.Current.ClientUsername;
                }

                if (video.Status == (int)EnumVideoStatus.Return)
                {
                    video.Status = (int)EnumVideoStatus.WaitForEdit;
                }

                var success = VideoDal.Update(video, tagIdList, tagNameList, zoneIdList, playlistIdList);
                if (success)
                {
                    if (isUploadNewVideo)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                    }
                    ActivityBo.LogAddNewVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateV2(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var zoneName = "";
                var video = VideoDal.GetByIdV2(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                    {
                        // Nếu là quyền upload => chỉ được sửa video do mình upload
                        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                        {
                            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                        }
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                    }
                }

                var isUploadNewVideo = (video.FileName != videoEntity.FileName);
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.VideoFolderId = videoEntity.VideoFolderId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.Mode = videoEntity.Mode;
                video.DistributionDate = videoEntity.DistributionDate == DateTime.MinValue ? DateTime.Now : videoEntity.DistributionDate;
                video.VideoRelation = videoEntity.VideoRelation;
                video.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                if (video.DistributionDate > DateTime.MinValue)
                {
                    video.DistributionDate = videoEntity.DistributionDate;
                }

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }
                // Update video status
                if (videoEntity.Status != (int)EnumVideoStatus.AllStatus)
                    video.Status = videoEntity.Status;

                if (video.Status == (int)EnumVideoStatus.Published && string.IsNullOrEmpty(video.PublishBy))
                {
                    video.PublishDate = videoEntity.DistributionDate;
                    video.PublishBy = WcfMessageHeader.Current.ClientUsername;
                }

                if (video.Status == (int)EnumVideoStatus.Return)
                {
                    video.Status = (int)EnumVideoStatus.WaitForEdit;
                }

                var success = VideoDal.UpdateV2(video, tagIdList, tagNameList, zoneIdList, playlistIdList);
                if (success)
                {
                    if (isUploadNewVideo)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                    }
                    ActivityBo.LogAddNewVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes Save(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return Update(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
                }
                //ELSE
                return Insert(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveV4(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                var zoneVideoUrl = "video";
                if (videoEntity.ZoneId > 0)
                {
                    var videoZone = ZoneVideoBo.GetZoneVideoByZoneVideoId(videoEntity.ZoneId);
                    if (videoZone != null)
                    {
                        zoneVideoUrl = videoZone.Url;
                        videoEntity.ZoneName = videoEntity.ZoneName ?? videoZone.Name;
                    }
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                    videoEntity.OriginalUrl = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return UpdateV4(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
                }
                //ELSE
                return InsertV3(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveV5(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                var zoneVideoUrl = "video";
                if (videoEntity.ZoneId > 0)
                {
                    var videoZone = ZoneVideoBo.GetZoneVideoByZoneVideoId(videoEntity.ZoneId);
                    if (videoZone != null)
                    {
                        zoneVideoUrl = videoZone.Url;
                        videoEntity.ZoneName = videoEntity.ZoneName ?? videoZone.Name;
                    }
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                    videoEntity.OriginalUrl = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return UpdateV5(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
                }
                //ELSE
                return InsertV4(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateV5(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                var zoneName = videoEntity.ZoneName;
                var video = GetById(videoEntity.Id, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    if (CurrentClientUserHasPermision(EnumPermission.VideoManager))
                    {
                        // Nếu là quyền upload => chỉ được sửa video do mình upload
                        if (!video.CreatedBy.Equals(WcfMessageHeader.Current.ClientUsername))
                        {
                            return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                        }
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.VideoNotAllowToUpdate;
                    }
                }

                var isUploadNewVideo = (video.FileName != videoEntity.FileName);
                var fileNameOld = video.FileName;
                video.FileName = videoEntity.FileName;
                video.Name = videoEntity.Name;
                video.ZoneId = videoEntity.ZoneId;
                video.UnsignName = videoEntity.UnsignName;
                video.Url = videoEntity.Url;
                video.OriginalUrl = videoEntity.OriginalUrl;
                video.Source = videoEntity.Source;
                video.Description = videoEntity.Description;
                video.AllowAd = videoEntity.AllowAd;
                video.Mode = videoEntity.Mode;
                video.DistributionDate = videoEntity.DistributionDate == DateTime.MinValue ? DateTime.Now : videoEntity.DistributionDate;
                video.VideoRelation = videoEntity.VideoRelation;
                video.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;
                video.LastModifiedDate = DateTime.Now;
                video.IsRemoveLogo = videoEntity.IsRemoveLogo;
                video.Tags = videoEntity.Tags;
                video.AvatarShareFacebook = videoEntity.AvatarShareFacebook;
                video.OriginalId = videoEntity.OriginalId;
                video.Author = videoEntity.Author;
                video.ZoneName = videoEntity.ZoneName;
                video.ParentId = videoEntity.ParentId;
                video.HashId = videoEntity.HashId;
                video.Type = videoEntity.Type;
                video.TrailerUrl = videoEntity.TrailerUrl;
                video.MetaAvatar = videoEntity.MetaAvatar;
                video.Location = videoEntity.Location;
                video.PolicyContentId = videoEntity.PolicyContentId;
                video.DisplayStyle = videoEntity.DisplayStyle;
                video.Namespace = videoEntity.Namespace;
                video.IsOnHome = videoEntity.IsOnHome;
                video.IsAMP = videoEntity.IsAMP;
                video.SourceId = videoEntity.SourceId;

                if (video.DistributionDate > DateTime.MinValue)
                {
                    video.DistributionDate = videoEntity.DistributionDate;
                }

                // Nếu có thông tin fie video => update thông tin file video
                if (!string.IsNullOrEmpty(videoEntity.KeyVideo))
                {
                    video.Capacity = videoEntity.Capacity;
                    video.Duration = videoEntity.Duration;
                    video.FileName = videoEntity.FileName;
                    video.HtmlCode = videoEntity.HtmlCode;
                    video.KeyVideo = videoEntity.KeyVideo;
                    video.Pname = videoEntity.Pname;
                    video.Size = videoEntity.Size;
                    video.Avatar = videoEntity.Avatar;
                }
                // Update video status
                if (videoEntity.Status != (int)EnumVideoStatus.AllStatus)
                    video.Status = videoEntity.Status;

                if (video.Status == (int)EnumVideoStatus.Published && string.IsNullOrEmpty(video.PublishBy))
                {
                    video.PublishDate = videoEntity.DistributionDate;
                    video.PublishBy = WcfMessageHeader.Current.ClientUsername;
                }

                //bỏ trạng thái chờ biên tập
                //if (video.Status == (int)EnumVideoStatus.Return)
                //{
                //    video.Status = (int)EnumVideoStatus.WaitForEdit;
                //}
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }

                var success = VideoDal.UpdateV5(video, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
                if (success)
                {
                    //inser redis
                    BoCached.Base.Video.VideoDalFactory.AddVideo(video);
                    //inser es
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(zoneIdList))
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneidsTemp.AddRange(zoneIdList.Split(';'));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    else if (videoEntity.ZoneId > 0)
                    {
                        var zoneidsTemp = new List<string>();
                        zoneidsTemp.Add(videoEntity.ZoneId.ToString());
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var playListIds = new List<string>();
                    if (!string.IsNullOrEmpty(playlistIdList))
                    {
                        playListIds.AddRange(playlistIdList.Split(';'));
                    }

                    var channelListIds = new List<string>();
                    if (!string.IsNullOrEmpty(channelIdList))
                    {
                        channelListIds.AddRange(channelIdList.Split(';'));
                    }

                    var videoES = new VideoSearchEntity
                    {
                        Id = video.Id,
                        ZoneId = video.ZoneId,
                        Name = video.Name,
                        Avatar = video.Avatar,
                        ZoneIds = zoneids.ToArray(),
                        PlaylistIds = playListIds.ToArray(),
                        LastModifiedBy = video.LastModifiedBy,
                        LastModifiedDate = video.LastModifiedDate,
                        DistributionDate = video.DistributionDate,
                        Mode = video.Mode,
                        Status = video.Status,
                        IsVideoYoutube = 0,
                        KeyVideo = video.KeyVideo,
                        ZoneName = video.ZoneName,
                        HtmlCode = video.HtmlCode,
                        ChannelIds = channelListIds.ToArray(),
                        ParentId = video.ParentId,
                        Type = video.Type,
                        Duration = video.Duration,
                        Description = video.Description,
                        Url = video.Url,
                        FileName = video.FileName,
                        IsOnHome = video.IsOnHome,
                        IsAMP = video.IsAMP,
                        SourceId = video.SourceId
                    };
                    BoSearch.Base.Video.VideoDalFactory.UpdateVideo(videoES);

                    //xóa cache playlist in video
                    FuncTaskAsyn(playListIds);

                    if (isUploadNewVideo)
                    {
                        ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name + " => filename: " + video.FileName);
                    }
                    ActivityBo.LogUpdateVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name, "filenameOld: " + fileNameOld);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveVideoInitDB(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                var zoneVideoUrl = "video";
                if (videoEntity.ZoneId > 0)
                {
                    var videoZone = ZoneVideoBo.GetZoneVideoByZoneVideoId(videoEntity.ZoneId);
                    if (videoZone != null)
                    {
                        zoneVideoUrl = videoZone.Url;
                        videoEntity.ZoneName = videoEntity.ZoneName ?? videoZone.Name;
                    }
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        //videoEntity.OriginalUrl = string.Format(urlFormat, zoneVideoUrl,
                        //    Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        //    videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        //videoEntity.OriginalUrl = string.Format(urlFormat,
                        //    Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        //    videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                    //videoEntity.OriginalUrl = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                return InsertVideoInitDB(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveVideoInfo(VideoEntity videoEntity, ref int videoId)
        {
            try
            {
                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoBo.GetZoneVideoByZoneVideoId(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                        videoEntity.OriginalUrl = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                    videoEntity.OriginalUrl = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    videoId = videoEntity.Id;
                    return UpdateVideoInfo(videoEntity);
                }

                return InsertVideoInfo(videoEntity, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveWithoutPermission(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return UpdateWithoutPermission(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
                }
                //ELSE
                return Insert(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        // Thêm VideoInFolder
        public static ErrorMapping.ErrorCodes SaveV3(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;
                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                if (BoConstants.UseTagVideoFormat)
                {
                    var tagVideos = VideoTagBo.GetVideoTagByIds(tagIdList);
                    var tagItems = "";
                    foreach (var videoTag in tagVideos)
                    {
                        tagItems += ", " + string.Format(tagFormat, videoTag.Id, videoTag.Url, videoTag.Name);
                    }
                    if (!string.IsNullOrEmpty(tagItems))
                    {
                        tagItems = tagItems.Remove(0, 1);
                    }
                    videoEntity.Tags = tagItems;
                }
                else
                {
                    videoEntity.Tags = tagNameList;
                }

                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return UpdateV2(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
                }
                //ELSE
                return InsertV2(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SaveName(VideoEntity videoEntity)
        {
            try
            {
                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }
                videoEntity.UnsignName = Utility.UnicodeToKoDau(videoEntity.Name);
                if (videoEntity.Id > 0)
                {
                    return VideoDal.UpdateName(videoEntity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
            return ErrorMapping.ErrorCodes.Exception;
        }
        public static ErrorMapping.ErrorCodes SaveNameAndDescription(VideoEntity videoEntity)
        {
            try
            {
                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }
                videoEntity.UnsignName = Utility.UnicodeToKoDau(videoEntity.Name);
                if (videoEntity.Id > 0)
                {
                    return VideoDal.UpdateNameAndDescription(videoEntity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
            return ErrorMapping.ErrorCodes.Exception;
        }
        public static ErrorMapping.ErrorCodes SaveV2(VideoEntity videoEntity, string tagIds, string zoneIdList, string playlistIdList, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var tagFormat = BoConstants.VideoUrlFormatTag;// AppSettings.GetString(BoConstants.VIDEO_FORMAT_TAG);
                var tagIdList = string.Empty;
                var tagLinkList = string.Empty;
                var tagNameList = string.Empty;
                var tagList = VideoTagBo.GetVideoTagByIds(tagIds);
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagIdList += ";" + tag.Id;
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagNameList += ";" + tag.Name;

                    }

                    if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagNameList != string.Empty) tagNameList = tagNameList.Remove(0, 1);
                }
                videoEntity.Tags = tagNameList;

                if (BoConstants.IsBuildLinkVideo)
                {
                    var zoneVideoUrl = "video";
                    if (videoEntity.ZoneId > 0)
                    {
                        var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                        if (videoZone != null)
                        {
                            zoneVideoUrl = videoZone.Url;
                        }
                    }
                    var urlFormat = BoConstants.VideoUrlFormat;
                    if (urlFormat.IndexOf("{2}") > 0)
                    {
                        videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                    else
                    {
                        videoEntity.Url = string.Format(urlFormat,
                            Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                            videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                    }
                }
                else
                {
                    videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
                }

                if (videoEntity.Id > 0)
                {
                    return Update(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
                }
                //ELSE
                return Insert(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateMode(int mode, int id, string userDoAction)
        {
            try
            {

                var success = VideoDal.UpdateMode(mode, id, userDoAction);
                if (success)
                {
                    //redis
                    BoCached.Base.Video.VideoDalFactory.UpdateMode(id, mode, WcfMessageHeader.Current.ClientUsername);
                    //es
                    BoSearch.Base.Video.VideoDalFactory.UpdateMode(id, mode, WcfMessageHeader.Current.ClientUsername);

                    var zoneName = "";
                    var video = GetById(id, ref zoneName);
                    ActivityBo.LogChangeVideoMode(id, userDoAction, video.Name, video.Mode);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateStatusVideoByHashId(string hashId, string keyVideo, string userDoAction)
        {
            try
            {

                var success = VideoDal.UpdateStatusVideoByHashId(hashId, keyVideo, userDoAction);
                if (success)
                {
                    var zoneName = "";
                    var video = GetVideoByHashId(hashId, ref zoneName);
                    if (video != null)
                    {
                        //redis
                        BoCached.Base.Video.VideoDalFactory.UpdateStatusVideoByHashId(video.Id, keyVideo, WcfMessageHeader.Current.ClientUsername);
                        //es
                        BoSearch.Base.Video.VideoDalFactory.UpdateStatusVideoByHashId(video.Id, keyVideo, WcfMessageHeader.Current.ClientUsername);

                        ActivityBo.LogChangeVideoMode(video.Id, userDoAction, video.Name, video.Mode);
                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Debug, "UpdateStatusVideoByHashId: -> video null: ko update BoCached and BoSearch");
                    }
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                if (ex.Message.Contains("ERROR#0984639770"))
                {
                    return ErrorMapping.ErrorCodes.ExceptionError0984639770;
                }
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateListVideoViews(string listKeyVideo, string listViewVideo)
        {
            try
            {
                return VideoDal.UpdateListVideoViews(listKeyVideo, listViewVideo)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateViews(string keyVideo, int views)
        {
            try
            {
                return VideoDal.UpdateViews(keyVideo, views)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes SendVideo(int videoId, string username)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var zoneName = "";
                var video = VideoDal.GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.Temporary && video.Status != (int)EnumVideoStatus.Return && video.Status != (int)EnumVideoStatus.CloneVideo)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToSend;
                }
                if (!(video.CreatedBy.Equals(username) || video.EditedBy.Equals(username)))
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToSend;
                }
                //if (string.IsNullOrEmpty(video.KeyVideo))
                //{
                //    return ErrorMapping.ErrorCodes.VideoMustHaveFileToSend;
                //}

                var success = VideoDal.Send(videoId, username);
                if (success)
                {
                    //bo trang thay cho bien tap -> cho xuat ban: WaitForPublish
                    //redis
                    BoCached.Base.Video.VideoDalFactory.Send(videoId, (int)EnumVideoStatus.WaitForPublish, username);
                    //es
                    BoSearch.Base.Video.VideoDalFactory.ChangeStatus(videoId, (int)EnumVideoStatus.WaitForPublish, username);

                    ActivityBo.LogSendVideoToWaitToPublish(video.Id, username, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes PublishVideo(int videoId, string userDoAction)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                }
                var zoneName = "";
                var video = VideoDal.GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.WaitForPublish &&
                    video.Status != (int)EnumVideoStatus.WaitForEdit &&
                    video.Status != (int)EnumVideoStatus.Temporary &&
                    video.Status != (int)EnumVideoStatus.DoneVideo &&
                    video.Status != (int)EnumVideoStatus.Return &&
                    video.Status != (int)EnumVideoStatus.Unpublished)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                }
                //if (string.IsNullOrEmpty(video.KeyVideo))
                //{
                //    return ErrorMapping.ErrorCodes.VideoMustHaveFileToPublish;
                //}

                var success = VideoDal.Publish(videoId, userDoAction);
                if (success)
                {
                    //redis
                    BoCached.Base.Video.VideoDalFactory.Publish(videoId, userDoAction);
                    //es
                    BoSearch.Base.Video.VideoDalFactory.ChangeStatus(videoId, 1, userDoAction);

                    ActivityBo.LogPublishVideo(video.Id, userDoAction, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes PublishVideoInNews(int videoId, string userDoAction, DateTime distributionDate)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                //if (!CurrentClientUserHasPermision(EnumPermission.ArticleAdmin))
                //{
                //    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                //}
                var zoneName = "";
                var video = VideoDal.GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.WaitForPublish &&
                    video.Status != (int)EnumVideoStatus.WaitForEdit &&
                    video.Status != (int)EnumVideoStatus.Temporary &&
                    video.Status != (int)EnumVideoStatus.Return &&
                    video.Status != (int)EnumVideoStatus.Unpublished)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                }
                if (string.IsNullOrEmpty(video.KeyVideo))
                {
                    return ErrorMapping.ErrorCodes.VideoMustHaveFileToPublish;
                }

                var success = VideoDal.PublishInNews(videoId, userDoAction, distributionDate);
                if (success)
                {
                    ActivityBo.LogPublishVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UnpublishVideo(int videoId, string username)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var zoneName = "";
                var video = VideoDal.GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.Published)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToUnpublish;
                }
                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToUnpublish;
                }

                var success = VideoDal.Unpublish(videoId);
                if (success)
                {
                    //unpublish redis
                    BoCached.Base.Video.VideoDalFactory.Unpublish(videoId);
                    //unpublish es
                    BoSearch.Base.Video.VideoDalFactory.Unpublish(videoId);

                    //log
                    ActivityBo.LogUnpublishVideo(video.Id, username, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes ReturnVideo(int videoId, string username)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var zoneName = "";
                var video = VideoDal.GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.WaitForPublish && video.Status != (int)EnumVideoStatus.Unpublished)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToReturn;
                }
                if (!CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToReturn;
                }

                var success = VideoDal.Return(videoId);
                if (success)
                {
                    //redis
                    BoCached.Base.Video.VideoDalFactory.Return(videoId, username);
                    //es
                    BoSearch.Base.Video.VideoDalFactory.ChangeStatus(videoId, 4, username);

                    ActivityBo.LogReturnVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes AddVideoFileToVideo(VideoEntity videoEntity, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return VideoDal.AddVideoFileToVideo(videoEntity, ref videoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.AddVideoFileToVideo:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsIdForList(string idList, long newsId)
        {
            try
            {
                if (string.IsNullOrEmpty(idList) || newsId <= 0)
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                return VideoDal.UpdateNewsIdForList(idList, newsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(":{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateAvatar(string avatar, int id, string userDoAction)
        {
            try
            {

                return VideoDal.UpdateAvatar(avatar, id, userDoAction) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteById(int videoId)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var zoneName = "";
                var video = GetById(videoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (video.Status != (int)EnumVideoStatus.Temporary &&
                    video.Status != (int)EnumVideoStatus.CloneVideo &&
                    video.Status != (int)EnumVideoStatus.Unpublished &&
                    video.Status != (int)EnumVideoStatus.WaitForPublish &&
                    video.Status != (int)EnumVideoStatus.Return)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToDelete;
                }
                var success = VideoDal.DeleteById(videoId);
                if (success)
                {
                    //xóa video es
                    BoSearch.Base.Video.VideoDalFactory.DeleteVideo(videoId);
                    //xóa video redis
                    BoCached.Base.Video.VideoDalFactory.DeleteVideo(videoId);

                    ActivityBo.LogDeleteVideo(videoId, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteByIdList(string videoIdList)
        {
            try
            {
                if (string.IsNullOrEmpty(videoIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.DeleteByIdList(videoIdList) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes ChangeStatus(int videoId)
        {
            try
            {
                if (0 >= videoId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.ChangeStatus(videoId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes ChangeStatusByList(string videoIdList)
        {
            try
            {
                if (string.IsNullOrEmpty(videoIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.ChangeStatusByList(videoIdList) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes RemoveVideoFromPlaylist(int videoId, int playlistId)
        {
            try
            {
                if (videoId <= 0 || playlistId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.RemoveVideoFromPlaylist(videoId, playlistId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes SyncNewsAndVideo(string videoIdList, long newsId)
        {
            try
            {
                if (newsId <= 0 || string.IsNullOrEmpty(videoIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.SyncNewsAndVideo(videoIdList, newsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes SyncNewsAndVideoWithStatus(string videoIdList, long newsId, EnumVideoStatus status, int excludeStatus)
        {
            try
            {
                if (newsId <= 0 || string.IsNullOrEmpty(videoIdList))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return VideoDal.SyncNewsAndVideoWithStatus(videoIdList, newsId, (int)status, excludeStatus) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }


        public static ErrorMapping.ErrorCodes UpdateKeyVideoByFileName(string keyvideo, string fileName)
        {
            try
            {
                return VideoDal.UpdateKeyVideoByFileName(keyvideo, fileName) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        #endregion

        #region Get

        public static List<VideoKeyEntity> GetListVideoWaitConvert(int top)
        {
            try
            {
                return VideoDal.GetListVideoWaitConvert(top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoEntity GetById(int videoId, ref string zoneName)
        {
            try
            {
                var video = BoCached.Base.Video.VideoDalFactory.GetVideoById(videoId, ref zoneName);
                if (video == null)
                {
                    video = VideoDal.GetById(videoId, ref zoneName);
                    if (video != null)
                    {
                        //add redis
                        BoCached.Base.Video.VideoDalFactory.AddVideo(video);
                    }
                }
                return video;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoEntity GetVideoByHashId(string hashId, ref string zoneName)
        {
            try
            {
                return VideoDal.GetVideoByHashId(hashId, ref zoneName); ;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<CountStatus> CountVideo(string username, string listStatus)
        {
            try
            {
                if (string.IsNullOrEmpty(listStatus))
                {
                    return null;
                }
                var list = listStatus.Split(',').ToList();

                return BoSearch.Base.Video.VideoDalFactory.CountVideo(username, list);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<CountStatus> CountVideoSingle(string username, string listStatus)
        {
            try
            {
                if (string.IsNullOrEmpty(listStatus))
                {
                    return null;
                }
                var list = listStatus.Split(',').ToList();

                return BoSearch.Base.Video.VideoDalFactory.CountVideoSingle(username, list);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static VideoEntity GetByVideoExternalId(string videoExternalId, ref string zoneName)
        {
            try
            {
                return VideoDal.GetByVideoExternalId(videoExternalId, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static VideoEntity GetByIdV2(int videoId, ref string zoneName)
        {
            try
            {
                return VideoDal.GetByIdV2(videoId, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static VideoEntity GetByFileName(string fileName)
        {
            try
            {
                return VideoDal.GetByFileName(fileName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static VideoEntity GetByKeyvideo(string keyvideo, ref string zoneName)
        {
            try
            {
                //var data=BoSearch.Base.Video.VideoDalFactory.
                return VideoDal.GetByKeyvideo(keyvideo, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<VideoRelationEntity> GetListRelationByIdList(string videoIdList)
        {
            try
            {
                var data = new List<VideoRelationEntity>();
                if (string.IsNullOrEmpty(videoIdList))
                {
                    return data;
                }
                var listId = videoIdList.Split(';').ToList();
                var list = BoCached.Base.Video.VideoDalFactory.GetVideoByListId(listId);
                if (list == null || (list != null && list.Count() <= 0))
                {
                    list = VideoDal.GetListByIdList(string.Join(",", listId));
                }

                data.AddRange(list.Select(s => new VideoRelationEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    Avatar = s.Avatar,
                    FileName = s.FileName,
                    KeyVideo = s.KeyVideo,
                    Author = s.Author,
                    DistributionDate = s.DistributionDate,
                    MetaAvatar = s.Avatar,
                    Status = s.Status,
                    Url = s.Url
                }));
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoEntity> GetListByIdList(string videoIdList)
        {
            try
            {
                var data = new List<VideoEntity>();
                if (string.IsNullOrEmpty(videoIdList))
                {
                    return data;
                }
                var listId = videoIdList.Split(';').ToList();
                data = BoCached.Base.Video.VideoDalFactory.GetVideoByListId(listId);
                if (data == null || (data != null && data.Count() <= 0))
                {
                    data = VideoDal.GetListByIdList(videoIdList);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoByAuthorEntity> GetListAuthorInVideo(long videoId)
        {
            try
            {
                var list = VideoDal.GetListAuthorInVideo(videoId);
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<VideoByAuthorEntity>();
            }
        }
        public static List<VideoEntity> GetInitVideoByListVideoId(string videoIdList)
        {
            try
            {
                return VideoDal.GetListByIdList(videoIdList);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoEntity> GetListPagingByZone(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            try
            {
                return VideoDal.GetListPagingByZone(pageIndex, pageSize, zoneId, playlistId, status, keyword, sortOder, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoNotPublishCountInPlaylist> GetVideoNotPublishCount(string playlistIds)
        {
            try
            {
                return VideoDal.GetVideoNotPublishCount(playlistIds);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoEntity> GetListPagingInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            try
            {
                return VideoDal.GetListPagingInPlaylist(pageIndex, pageSize, playlistId, status, keyword, sortOder, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<VideoEntity> GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int sortOrder, int cateId, int playlistId, ref int totalRow)
        {
            try
            {
                return VideoDal.GetListMyVideo(username, pageIndex, pageSize, keyword, status, sortOrder, cateId, playlistId,
                                               ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetListMyVideo:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> GetAllVideoInPlaylist(int playlistId)
        {
            try
            {
                return VideoDal.GetAllVideoInPlaylist(playlistId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetAllVideoInPlaylist:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> GetAllVideoInPlaylistByMode(int playlistId, int mode)
        {
            try
            {
                return VideoDal.GetAllVideoInPlaylistByMode(playlistId, mode);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetAllVideoInPlaylistByMode:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow, ref int maxPriority)
        {
            try
            {
                return VideoDal.GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow, ref maxPriority);

                //var hashName = "VideoInPlaylistSearch:" + playlistId;
                //var key = pageIndex + ":" + pageSize;
                ////get cache
                //var valueData = BoCached.Base.Video.VideoDalFactory.GetDataSearchVideoInPlaylist(hashName, key);
                //if (!string.IsNullOrEmpty(valueData))
                //{
                //    var data = NewtonJson.Deserialize<VideoInPlaylistSearch>(valueData);
                //    totalRow = data.TotalRow;
                //    return data.Videos;
                //}
                //else
                //{
                //    //ko co lay tu db     
                //    var totalRows = 0;
                //    var dataSearch = VideoDal.GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow);
                //    totalRows = totalRow;
                //    if (dataSearch.Count > 0)
                //    {
                //        var value = new VideoInPlaylistSearch()
                //        {
                //            TotalRow = totalRows,
                //            Videos = dataSearch
                //        };
                //        string valueJson = NewtonJson.Serialize(value);
                //        BoCached.Base.Video.VideoDalFactory.AddDataSearchVideoInPlaylist(hashName, key, valueJson);
                //    }
                //    return dataSearch;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetVideoInPlaylist:{0}", ex.Message));
            }
        }

        public static VideoEntity GetVideoNewByPlayListId(int playlistId)
        {
            try
            {
                return VideoDal.GetVideoNewByPlayListId(playlistId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetVideoNewByPlayListId:{0}", ex.Message));
            }
        }
        public static List<VideoSearchEntity> ListVideoByStatus(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.ListVideoByStatus(username, status, order, mode, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var playListIds = new List<string>();
                    var channelIds = new List<string>();
                    var viewVideo = new List<string>();
                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace.ToLower();
                    foreach (var item in data)
                    {
                        playListIds.AddRange(item.PlaylistIds);
                        channelIds.AddRange(item.ChannelIds);
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }
                    var playListDistinctIds = playListIds.Distinct().ToList();
                    var channelDistinctIds = channelIds.Distinct().ToList();

                    var playList = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListByListId(playListDistinctIds);
                    var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByListId(channelDistinctIds);

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    //lấy playlistname và channelname
                    foreach (var item in data)
                    {
                        item.PlaylistName = MappingPlayListName(item.PlaylistIds, playList).ToArray();//PlaylistBo.GetListByVideoId(item.Id).Select(s=>s.Name).ToArray();
                        item.ChannelName = MappingVideoChannelName(item.ChannelIds, videoChannel).ToArray();//VideoChannelBo.GetListVideoChannelByVideoId(item.Id).Select(s => s.Name).ToArray(); ;
                        item.Views = MappingViewVideo(nameSpace, item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }

        public static List<VideoSearchEntity> ListVideoByStatusSingle(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.ListVideoByStatusSingle(username, status, order, mode, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var playListIds = new List<string>();
                    var channelIds = new List<string>();
                    var viewVideo = new List<string>();
                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace.ToLower();
                    foreach (var item in data)
                    {
                        playListIds.AddRange(item.PlaylistIds);
                        channelIds.AddRange(item.ChannelIds);
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }
                    var playListDistinctIds = playListIds.Distinct().ToList();
                    var channelDistinctIds = channelIds.Distinct().ToList();

                    var playList = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListByListId(playListDistinctIds);
                    var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByListId(channelDistinctIds);

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    //lấy playlistname và channelname
                    foreach (var item in data)
                    {
                        item.PlaylistName = MappingPlayListName(item.PlaylistIds, playList).ToArray();//PlaylistBo.GetListByVideoId(item.Id).Select(s=>s.Name).ToArray();
                        item.ChannelName = MappingVideoChannelName(item.ChannelIds, videoChannel).ToArray();//VideoChannelBo.GetListVideoChannelByVideoId(item.Id).Select(s => s.Name).ToArray(); ;
                        item.Views = MappingViewVideo(nameSpace, item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }
        public static List<VideoSearchEntity> ListMyVideoSingle(string username, int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.ListMyVideoSingle(username, zoneVideoId, keyword, fromDate, toDate, order, mode, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var playListIds = new List<string>();
                    var channelIds = new List<string>();
                    var viewVideo = new List<string>();
                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace.ToLower();
                    foreach (var item in data)
                    {
                        playListIds.AddRange(item.PlaylistIds);
                        channelIds.AddRange(item.ChannelIds);
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }
                    var playListDistinctIds = playListIds.Distinct().ToList();
                    var channelDistinctIds = channelIds.Distinct().ToList();

                    var playList = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListByListId(playListDistinctIds);
                    var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByListId(channelDistinctIds);

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    //lấy playlistname và channelname
                    foreach (var item in data)
                    {
                        item.PlaylistName = MappingPlayListName(item.PlaylistIds, playList).ToArray();//PlaylistBo.GetListByVideoId(item.Id).Select(s=>s.Name).ToArray();
                        item.ChannelName = MappingVideoChannelName(item.ChannelIds, videoChannel).ToArray();//VideoChannelBo.GetListVideoChannelByVideoId(item.Id).Select(s => s.Name).ToArray(); ;
                        item.Views = MappingViewVideo(nameSpace, item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }

        private static List<string> MappingPlayListName(string[] playlistIds, List<PlayListCachedEntity> playList)
        {
            var list = new List<string>();
            try
            {
                foreach (var item in playlistIds)
                {
                    if (playList != null && playList.Count > 0)
                    {
                        var result = playList.Where(s => s.PlaylistInfo.Id.ToString() == item.ToString()).Select(s => s.PlaylistInfo.Name).FirstOrDefault();
                        list.Add(result);
                    }
                }

                return list;
            }
            catch
            {
                return list;
            }
        }

        private static List<string> MappingVideoChannelName(string[] playlistIds, List<VideoChannelCachedEntity> channel)
        {
            var list = new List<string>();
            try
            {
                foreach (var item in playlistIds)
                {
                    if (channel != null && channel.Count > 0)
                    {
                        var result = channel.Where(s => s.VideoChannelInfo.Id.ToString() == item.ToString()).Select(s => s.VideoChannelInfo.Name).FirstOrDefault();
                        list.Add(result);
                    }
                }
                return list;
            }
            catch
            {
                return list;
            }
        }

        private static int MappingViewVideo(string nameSpace, string fileName, ViewVideo listView)
        {
            int view = 0;
            try
            {
                if (listView != null && listView.videos != null && listView.videos.Count > 0)
                {
                    var key = CMS.Common.Crypton.Md5Encrypt(nameSpace + "/" + fileName);
                    foreach (var obj in listView.videos)
                    {
                        if (obj != null && obj.key == key)
                        {
                            view = obj.play;
                            break;
                        }
                    }
                }
                return view;
            }
            catch
            {
                return view;
            }
        }

        public static List<VideoSearchEntity> ListVideoByParentId(int parentId, int status, ref int totalRow)
        {
            try
            {
                return BoSearch.Base.Video.VideoDalFactory.ListVideoByParentId(parentId, status, ref totalRow);
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }
        public static List<VideoSearchEntity> Search(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.SearchVideo(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var playListIds = new List<string>();
                    var channelIds = new List<string>();
                    var viewVideo = new List<string>();
                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace.ToLower();
                    foreach (var item in data)
                    {
                        playListIds.AddRange(item.PlaylistIds);
                        channelIds.AddRange(item.ChannelIds);
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }
                    var playListDistinctIds = playListIds.Distinct().ToList();
                    var channelDistinctIds = channelIds.Distinct().ToList();

                    var playList = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListByListId(playListDistinctIds);
                    var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByListId(channelDistinctIds);

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    //lấy playlistname và channelname
                    foreach (var item in data)
                    {
                        item.PlaylistName = MappingPlayListName(item.PlaylistIds, playList).ToArray();//PlaylistBo.GetListByVideoId(item.Id).Select(s=>s.Name).ToArray();
                        item.ChannelName = MappingVideoChannelName(item.ChannelIds, videoChannel).ToArray();//VideoChannelBo.GetListVideoChannelByVideoId(item.Id).Select(s => s.Name).ToArray(); ;
                        item.Views = MappingViewVideo(nameSpace, item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<VideoSearchEntity>();
            }
        }

        public static List<VideoSearchEntity> ExportVideoPublish(string zoneIds, string createdBy, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var viewVideo = new List<string>();
                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace;
                    if (nameSpace == "TTVH")
                    {
                        nameSpace = "thethaovanhoa";
                    }
                    foreach (var item in data)
                    {
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    foreach (var item in data)
                    {
                        item.Url = CmsChannelConfiguration.GetAppSetting("FRONT_END_URL_VIDEO") + item.Url.Replace("/video/", "/");
                        item.Views = MappingViewVideo(nameSpace.ToLower(), item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }

        public static List<VideoSearchEntity> SearchVideoSingle(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoDalFactory.SearchVideoSingle(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var playListIds = new List<string>();
                    var channelIds = new List<string>();
                    var viewVideo = new List<string>();

                    var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace;
                    if (nameSpace == "TTVH")
                    {
                        nameSpace = "thethaovanhoa";
                    }
                    foreach (var item in data)
                    {
                        if (item.PlaylistIds != null)
                            playListIds.AddRange(item.PlaylistIds);
                        if (item.ChannelIds != null)
                            channelIds.AddRange(item.ChannelIds);
                        if (string.IsNullOrEmpty(item.Namespace))
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + item.FileName));
                        else
                            viewVideo.Add(CMS.Common.Crypton.Md5Encrypt(item.FileName));
                    }
                    var playListDistinctIds = playListIds.Distinct().ToList();
                    var channelDistinctIds = channelIds.Distinct().ToList();

                    var playList = BoCached.Base.PlayList.PlayListDalFactory.GetPlayListByListId(playListDistinctIds);
                    var videoChannel = BoCached.Base.VideoChannel.VideoChannelDalFactory.GetVideoChannelByListId(channelDistinctIds);

                    var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");
                    var listView = new ViewVideo();
                    if (isUseAdtechData == "1")
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));
                    else if (isUseAdtechData == "2")
                        listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", viewVideo), DateTime.MinValue, DateTime.Now, nameSpace);
                    else
                        listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", viewVideo));

                    //lấy playlistname và channelname
                    foreach (var item in data)
                    {
                        item.PlaylistName = MappingPlayListName(item.PlaylistIds, playList).ToArray();//PlaylistBo.GetListByVideoId(item.Id).Select(s=>s.Name).ToArray();
                        item.ChannelName = MappingVideoChannelName(item.ChannelIds, videoChannel).ToArray();//VideoChannelBo.GetListVideoChannelByVideoId(item.Id).Select(s => s.Name).ToArray(); ;                        
                        item.Views = MappingViewVideo(nameSpace.ToLower(), item.FileName, listView);
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SearchVideoSingle => " + ex);
                return new List<VideoSearchEntity>();
            }
        }

        public static List<BoSearch.Entity.Video> SearchVTV(int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data = BoSearch.Base.Video.VideoVTVDalFactory.SearchVideoVTV(zoneVideoId, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.IsClone = 0;
                        if (CheckCloneVideoVtv(item.Id))
                        {
                            item.IsClone = 1;
                        }
                    }
                }

                return data;
            }
            catch (Exception)
            {
                return new List<BoSearch.Entity.Video>();
            }
        }

        private static bool CheckCloneVideoVtv(long id)
        {
            try
            {
                return BoSearch.Base.Video.VideoDalFactory.CheckCloneVideoVtv(id);
            }
            catch
            {
                return false;
            }
        }

        public static List<VideoSearchEntity> SearchVideoKeyByZone(DateTime fromDate, DateTime toDate, string username, List<string> zoneVideoId, int playlistId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return BoSearch.Base.Video.VideoDalFactory.SearchVideoKeyByZone(fromDate, toDate, username, zoneVideoId, playlistId, keyword, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception)
            {
                return new List<VideoSearchEntity>();
            }
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsByVideoAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, startPage, startDate, endDate, () =>
            {
                var data = VideoDal.InitESAllVideo(startPage, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VideoDal.InitESAllVideo(page, pageSize, startDate, endDate, ref totalRows);

                var ids = data.Select(c => c.Id);

                //var zoneIds = ZoneVideoDal.GetListZoneRelation(s.Id).Select(z => z.Id.ToString()).ToArray();
                //var playlistIds = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray();
                //var playlistName = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray();
                //var channelIds = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray();
                //var channelName = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray();

                //try
                //{
                //    var list1 = data.Select(s => new VideoSearchEntity
                //    {
                //        Id = s.Id,
                //        ZoneId = s.ZoneId,
                //        Name = s.Name,
                //        ZoneIds = ZoneVideoDal.GetListZoneRelation(s.Id).Select(z => z.Id.ToString()).ToArray(),
                //        PlaylistIds = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                //        PlaylistName = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                //        ChannelIds = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                //        ChannelName = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                //        CreatedBy = s.CreatedBy,
                //        EditedBy = s.EditedBy,
                //        PublishBy = s.PublishBy,
                //        PublishDate = s.PublishDate,
                //        LastModifiedBy = s.LastModifiedBy,
                //        LastModifiedDate = s.LastModifiedDate,
                //        CreatedDate = s.CreatedDate,
                //        DistributionDate = s.DistributionDate,
                //        EditedDate = s.EditedDate,
                //        Mode = s.Mode,
                //        Status = s.Status,
                //        Views = s.Views,
                //        ZoneName = s.ZoneName,
                //        Avatar = s.Avatar,
                //        HtmlCode = s.HtmlCode,
                //        KeyVideo = s.KeyVideo,
                //        ParentId = s.ParentId < 0 ? 0 : s.ParentId,
                //        Type = s.Type < 0 ? 0 : s.Type,
                //        Duration = s.Duration,
                //        ListType = VideoDal.GetListVideoByParentId(s.Id).Select(z => z.Type.ToString()).ToArray(),
                //        Description = s.Description,
                //        Url = s.Url,
                //        FileName = s.FileName,
                //        Namespace = s.Namespace ?? string.Empty,
                //        IsOnHome = s.IsOnHome,
                //        IsAMP = s.IsAMP,
                //        SourceId = s.SourceId
                //    }).ToList();
                //}
                //catch (Exception ex)
                //{
                //    throw;
                //}
                var list = data.Select(s => new VideoSearchEntity
                {
                    Id = s.Id,
                    ZoneId = s.ZoneId,
                    Name = s.Name,
                    ZoneIds = ZoneVideoDal.GetListZoneRelation(s.Id).Select(z => z.Id.ToString()).ToArray(),
                    PlaylistIds = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                    PlaylistName = PlaylistDal.GetListByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                    ChannelIds = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                    ChannelName = VideoChannelDal.GetVideoChannelByVideoId(s.Id).Select(z => z.Name.ToString()).ToArray(),
                    CreatedBy = s.CreatedBy,
                    EditedBy = s.EditedBy,
                    PublishBy = s.PublishBy,
                    PublishDate = s.PublishDate,
                    LastModifiedBy = s.LastModifiedBy,
                    LastModifiedDate = s.LastModifiedDate,
                    CreatedDate = s.CreatedDate,
                    DistributionDate = s.DistributionDate,
                    EditedDate = s.EditedDate,
                    Mode = s.Mode,
                    Status = s.Status,
                    Views = s.Views,
                    ZoneName = s.ZoneName,
                    Avatar = s.Avatar,
                    HtmlCode = s.HtmlCode,
                    KeyVideo = s.KeyVideo,
                    ParentId = s.ParentId < 0 ? 0 : s.ParentId,
                    Type = s.Type < 0 ? 0 : s.Type,
                    Duration = s.Duration,
                    ListType = VideoDal.GetListVideoByParentId(s.Id).Select(z => z.Type.ToString()).ToArray(),
                    Description = s.Description,
                    Url = s.Url,
                    FileName = s.FileName,
                    Namespace = s.Namespace ?? string.Empty,
                    IsOnHome = s.IsOnHome,
                    IsAMP = s.IsAMP,
                    SourceId = s.SourceId
                }).ToList();
                BoSearch.Base.Video.VideoDalFactory.InitAllVideo(list);
            }, action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByVideoAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = VideoDal.InitESAllVideo(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = VideoDal.InitESAllVideo(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.Video.VideoDalFactory.InitAllVideo(data);
            }, action);
        }
        public static List<VideoEntity> SearchForList(string username, int zoneVideoId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchForList(username, zoneVideoId, keyword, status, order, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.SearchForList:{0}", ex.Message));
            }
        }
        public static List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchDistribution(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchDistributionV2(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.SearchDistributionV2:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> SearchV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchV2(username, videoFolderId, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.SearchV2:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> SearchAd(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchAd(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, allowAd, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> SearchExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchExcludeBomb(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static List<VideoExportEntity> SearchExport(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchExport(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> SearchWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchWithExcludeZones(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, excludeZones, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Get VideoTag List for Video Editing
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public static List<VideoTagEntity> GetVideoTagListByVideo(int videoId)
        {
            try
            {
                var dataTag = BoCached.Base.Video.VideoDalFactory.GetVideoTagByVideoId(videoId);
                if (dataTag == null || (dataTag != null && dataTag.Count <= 0))
                {
                    dataTag = VideoTagDal.GetByVideoId(videoId);
                    if (dataTag != null && dataTag.Count > 0)
                    {
                        //add redis
                        BoCached.Base.Video.VideoDalFactory.AddVideoTagByVideoId(videoId, dataTag);
                    }
                }
                return dataTag;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetVideoTagListByVideo:{0}", ex.Message));
            }
        }

        public static List<VideoInNewsEntity> GetVideoInNewsList(int videoId)
        {
            try
            {
                return VideoDal.GetVideoInNewsList(videoId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetVideoInNewsList:{0}", ex.Message));
            }
        }

        public static List<VideoCounterEntity> GetVideoCounter()
        {
            var result = new List<VideoCounterEntity>();
            try
            {
                var username = WcfMessageHeader.Current.ClientUsername;
                if (CurrentClientUserHasPermision(EnumPermission.ApproveVideo))
                {
                    username = "";
                }
                result = VideoDal.GetVideoCounter(username, WcfMessageHeader.Current.ClientUsername);
                var values = Enum.GetValues(typeof(EnumVideoStatus));
                foreach (var value in values.Cast<object>().Where(value => result.Find(item => item.Status == (int)value) == null))
                {
                    result.Add(new VideoCounterEntity
                    {
                        Status = (int)value,
                        Count = 0
                    });
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return result;
        }

        public static List<string> GetLastPublishedForUpdateView(DateTime fromDate)
        {
            try
            {
                return VideoDal.GetLastPublishedForUpdateView(fromDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetLastPublishedForUpdateView:{0}", ex.Message));
            }
        }

        public static List<VideoExportEntity> GetRoyalties(DateTime startDate, DateTime endDate,
                                                                int pageIndex,
                                                                int pageSize)
        {
            try
            {
                return VideoDal.SelectRoyalties(startDate, endDate, pageIndex, pageSize);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<VideoExportEntity> GetRoyaltiesByZone(DateTime startDate, DateTime endDate,
                                                               int pageIndex,
                                                               int pageSize, string zones)
        {
            try
            {
                return VideoDal.SelectRoyaltiesByZone(startDate, endDate, pageIndex, pageSize, zones);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.SelectRoyaltiesByZone:{0}", ex.Message));
            }
        }

        public static int CountRoyalties(DateTime startDate, DateTime endDate)
        {
            try
            {
                return VideoDal.CountRoyalties(startDate, endDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }

        public static int CountRoyaltiesByZone(DateTime startDate, DateTime endDate, string zones)
        {
            try
            {
                return VideoDal.CountRoyaltiesByZone(startDate, endDate, zones);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }


        public static List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.GetListSharing(keyword, zoneIds, dateFrom, dateTo, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetListSharing:{0}", ex.Message));
            }
        }
        public static List<VideoEntity> GetListVideoByHour(string zoneVideoIds, int Hour)
        {
            try
            {
                return VideoDal.GetListVideoByHour(zoneVideoIds, Hour);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetListVideoByHour:{0}", ex.Message));
            }
        }

        #endregion

        #region VideoFromYoutube

        public static ErrorMapping.ErrorCodes InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                var existsVideoFromYoutube = VideoFromYoutubeDal.GetBySourceVideoUrl(videoFromYoutube.SourceVideoUrl);
                if (existsVideoFromYoutube != null) return ErrorMapping.ErrorCodes.YoutubeVideoUrlExists;

                if (VideoFromYoutubeDal.InsertVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id))
                {
                    var newVideo = VideoDal.GetByVideoFromYoutubeId(id);
                    if (newVideo != null)
                        ActivityBo.LogAddNewVideoFromYoutube(newVideo.Id, WcfMessageHeader.Current.ClientUsername,
                                                             newVideo.Name, videoFromYoutube.SourceVideoUrl);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList)
        {
            try
            {
                if (VideoFromYoutubeDal.UpdateVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList))
                {
                    var newVideo = VideoDal.GetByVideoFromYoutubeId(videoFromYoutube.Id);
                    if (newVideo != null)
                        ActivityBo.LogAddNewVideoFromYoutube(newVideo.Id, WcfMessageHeader.Current.ClientUsername,
                                                             newVideo.Name, videoFromYoutube.SourceVideoUrl);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes SaveVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            try
            {
                if (videoFromYoutube.Id > 0)
                {
                    if (VideoFromYoutubeDal.UpdateVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList))
                    {
                        id = videoFromYoutube.Id;
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }
                else
                {
                    var existsVideoFromYoutube = VideoFromYoutubeDal.GetBySourceVideoUrl(videoFromYoutube.SourceVideoUrl);
                    if (existsVideoFromYoutube == null || (existsVideoFromYoutube != null && existsVideoFromYoutube.AccountName != videoFromYoutube.AccountName))
                    {
                        if (VideoFromYoutubeDal.InsertVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList,
                                                                       playlistIdList, ref id))
                            return ErrorMapping.ErrorCodes.Success;
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.YoutubeVideoUrlExists;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, EnumVideoFromYoutubeStatus status, string htmlCode, string keyVideo, string pname, string fileName, string duration, string size, int capacity)
        {
            try
            {
                if (VideoFromYoutubeDal.UpdateVideoFromYoutubeStatus(id, avatar, filePath, (int)status, htmlCode, keyVideo, pname, fileName, duration, size, capacity))
                    return ErrorMapping.ErrorCodes.Success;
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteVideoFromYoutube(int id)
        {
            try
            {
                if (VideoFromYoutubeDal.DeleteVideoFromYoutube(id))
                {
                    var newVideo = VideoDal.GetByVideoFromYoutubeId(id);
                    if (newVideo != null)
                        ActivityBo.LogDeleteVideoFromYoutube(newVideo.Id, WcfMessageHeader.Current.ClientUsername,
                                                             newVideo.Name);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes SendVideoFromYoutube(int id)
        {
            try
            {
                var videoFromYoutube = VideoFromYoutubeDal.GetVideoFromYoutubeById(id);
                if (videoFromYoutube == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (videoFromYoutube.Status != (int)EnumVideoFromYoutubeStatus.UploadSuccess)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                }
                var zoneName = "";
                var video = VideoDal.GetById(videoFromYoutube.VideoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (string.IsNullOrEmpty(video.KeyVideo) || string.IsNullOrEmpty(video.FileName))
                {
                    return ErrorMapping.ErrorCodes.VideoMustHaveFileToSend;
                }
                if (VideoFromYoutubeDal.SendVideoFromYoutube(id))
                {
                    ActivityBo.LogSendVideoFromYoutube(video.Id, WcfMessageHeader.Current.ClientUsername,
                                                         video.Name);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes PublishVideoFromYoutube(int id)
        {
            try
            {
                var videoFromYoutube = VideoFromYoutubeDal.GetVideoFromYoutubeById(id);
                if (videoFromYoutube == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (videoFromYoutube.Status != (int)EnumVideoFromYoutubeStatus.UploadSuccess)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAllowToPublish;
                }
                var zoneName = "";
                var video = VideoDal.GetById(videoFromYoutube.VideoId, ref zoneName);
                if (video == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }
                if (string.IsNullOrEmpty(video.KeyVideo) || string.IsNullOrEmpty(video.FileName))
                {
                    return ErrorMapping.ErrorCodes.VideoMustHaveFileToSend;
                }
                if (VideoFromYoutubeDal.PublishVideoFromYoutube(id, WcfMessageHeader.Current.ClientUsername))
                {
                    ActivityBo.LogPublishVideoFromYoutube(video.Id, WcfMessageHeader.Current.ClientUsername,
                                                         video.Name);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            try
            {
                return VideoFromYoutubeDal.GetVideoFromYoutubeById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static VideoFromYoutubeForEditEntity GetVideoFromYoutubeForEdit(int id)
        {
            try
            {
                var videoFromYoutubeDetail = new VideoFromYoutubeForEditEntity();
                var videoFromYoutube = VideoFromYoutubeDal.GetVideoFromYoutubeById(id);
                if (videoFromYoutube != null)
                {
                    videoFromYoutubeDetail.VideoFromYoutubeInfo = videoFromYoutube;
                    var videoId = videoFromYoutube.VideoId;
                    var zoneName = "";
                    var video = VideoDal.GetById(videoId, ref zoneName);
                    videoFromYoutubeDetail.VideoInfo = video;
                    videoFromYoutubeDetail.TagList = GetVideoTagListByVideo(videoId);
                    videoFromYoutubeDetail.VideoRelation = GetListRelationByIdList(video.VideoRelation);
                    videoFromYoutubeDetail.Playlist = PlaylistBo.GetListByVideoId(videoId);
                    videoFromYoutubeDetail.ZoneVideoList = ZoneVideoBo.GetListZoneRelation(videoId);
                }
                return videoFromYoutubeDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<VideoFromYoutubeEntity> GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.GetListVideoFromYoutube(accountName, status, order, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<VideoFromYoutubeDetailEntity> SearchVideoFromYoutube(string username, string keyword, EnumVideoFromYoutubeStatus status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            var listVideo = new List<VideoFromYoutubeDetailEntity>();
            try
            {
                var videoSearch = BoSearch.Base.Video.VideoYoutubeDalFactory.SearchVideoYoutube(username, keyword, (int)status, zoneVideoId, playlistId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                if (null != videoSearch && videoSearch.Count > 0)
                {
                    listVideo = BoCached.Base.Video.VideoYoutubeDalFactory.GetVideoYoutubeByListId(videoSearch);
                }
                if (listVideo == null || (listVideo != null && listVideo.Count() <= 0))
                {
                    //db sql
                    listVideo = VideoFromYoutubeDal.Search(username, keyword, (int)status, zoneVideoId, playlistId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                }

                return listVideo;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, string.Format("VideoYoutubeDal.Search:{0}", ex.Message));
                return listVideo;
            }
        }


        public static List<VideoFromYoutubeEntity> GetUploadingVideoFromYoutube()
        {
            return VideoFromYoutubeDal.GetUploadingVideo();
        }
        #endregion

        private static bool CurrentClientUserHasPermision(EnumPermission permission)
        {
            var result = BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(WcfMessageHeader.Current.ClientUsername, (int)permission);
            if (result)
            {
                return true;
            }
            return PermissionBo.CheckUserPermission(WcfMessageHeader.Current.ClientUsername, (int)permission) ==
                   ErrorMapping.ErrorCodes.Success;
        }

        public static List<VideoEntity> SearchExternalVideo(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchExternalVideo(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }

        #region VideoExtension

        public static VideoExtensionEntity GetVideoExtensionValue(long videoId, EnumVideoExtensionType type)
        {
            return VideoExtensionDal.GetValue(videoId, (int)type);
        }

        public static int GetVideoExtensionMaxValue(long videoId, int type)
        {
            return VideoExtensionDal.GetMaxValue(videoId, (int)type);
        }
        public static ErrorMapping.ErrorCodes SetVideoExtensionValue(long videoId, EnumVideoExtensionType type, string value)
        {
            return VideoExtensionDal.SetValue(videoId, (int)type, value)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes DeleteVideoExtensionByVideoId(long videoId)
        {
            return VideoExtensionDal.DeleteByVideoId(videoId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        public static List<VideoExtensionEntity> GetVideoExtensionByVideoId(long videoId)
        {
            var videoEx = BoCached.Base.Video.VideoDalFactory.GetVideoExtensionByVideoId((int)videoId);
            if (videoEx == null || (videoEx != null && videoEx.Count() <= 0))
            {
                videoEx = VideoExtensionDal.GetByVideoId(videoId);
                if (videoEx != null && videoEx.Count() > 0)
                {
                    //add redis
                    BoCached.Base.Video.VideoDalFactory.AddVideoExtensionByVideoId((int)videoId, videoEx);
                }
            }
            return videoEx;
        }
        public static List<VideoExtensionEntity> GetVideoExtensionByListVideoId(string listVideoId)
        {
            return VideoExtensionDal.GetByListVideoId(listVideoId);
        }
        public static List<VideoExtensionEntity> GetVideoExtensionByTypeAndVaue(EnumVideoExtensionType type, string value)
        {
            return VideoExtensionDal.GetByTypeAndVaue((int)type, value);
        }

        #endregion

        public static List<BoxVideoEmbedEntity> GetExternalListVideoEmbed(int zoneId, int type)
        {
            var VideoEmbed = new List<BoxVideoEmbedEntity>();
            try
            {

                return VideoDal.GetExternalListVideoEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return VideoEmbed;

        }

        public static VideoEntity GetExternalById(int videoId, ref string zoneName)
        {
            try
            {

                return VideoDal.GetExternalById(videoId, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return new VideoEntity();
        }

        #region VideoDistribution
        public static List<VideoEntity> SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchVideoDistribution(zoneVideoIds, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static VideoEntity VideoDistributionGetById(int videoId, ref string zoneName)
        {
            try
            {
                return VideoDal.VideoDistributionGetById(videoId, ref zoneName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        #endregion

        public static List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId)
        {
            try
            {
                return VideoDal.GetPlaylistGroupByParentId(playlistId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetPlaylistGroupByParentId:{0}", ex.Message));
            }
        }

        public static List<PlaylistGroupEntity> BindAllOfPlaylistGroupToTreeviewFullDepth(List<PlaylistGroupEntity> allZonesWithSimpleField, string prefix)
        {
            allZonesWithSimpleField.OrderBy(item => item.ParentId).ThenBy(item => item.GroupName);
            var outputZones = new List<PlaylistGroupEntity>();
            BindAllOfPlaylistGroupToTreeviewFullDepth(allZonesWithSimpleField.ToList(), 0, prefix, ref outputZones);
            return outputZones;
        }

        private static void BindAllOfPlaylistGroupToTreeviewFullDepth(List<PlaylistGroupEntity> allZones, int currentParentId, string prefix, ref List<PlaylistGroupEntity> outputZones)
        {
            if (allZones == null || allZones.Count <= 0) return;

            var currentPrefix = prefix;
            if (currentParentId <= 0)
            {
                currentPrefix = "";
            }
            var count = allZones.Count;
            for (var i = 0; i < count; i++)
            {
                if (allZones[i].ParentId == currentParentId)
                {
                    allZones[i].GroupName = currentPrefix + " " + allZones[i].GroupName;
                    outputZones.Add(allZones[i]);
                    BindAllOfPlaylistGroupToTreeviewFullDepth(allZones, allZones[i].Id, currentPrefix + prefix, ref outputZones);
                }
            }
        }

        public static PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId)
        {
            try
            {
                return VideoDal.GetPlaylistGroupByPlaylistId(playlistId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup)
        {
            try
            {
                var success = VideoDal.InsertPlaylistInVideoGroup(playlistGroup);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static List<VideoEntity> SearchByPlaylistGroup(int group, int zone)
        {
            try
            {
                return VideoDal.SearchByPlaylistGroup(group, zone);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }

        #region Video EPL
        public static ErrorMapping.ErrorCodes Insert_By_Feed(VideoEntity videoEntity, string tagIdList, string zoneList, int MapPlayListTime, ref int videoId)
        {
            try
            {
                if (null == videoEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.VideoTitleCouldNotBeEmpty;
                }

                //videoEntity.Status = (int)EnumVideoStatus.Temporary;

                var success = VideoDal.Insert_By_Feed(videoEntity, tagIdList, zoneList, MapPlayListTime, ref videoId);
                if (success)
                {
                    try
                    {
                        if (string.IsNullOrEmpty(videoEntity.Tags))
                        {
                            //keyword =='' insert vào recut
                            VideoDal.Video_Insesert_ZoneRecut(videoId);
                        }
                        else
                        {
                            var zonevideo = VideoDal.ZoneVideo_GetAll();
                            var ZoneDisplayStyle1 = zonevideo.Where(k => k.DisplayStyle == 1).ToList();
                            var arrKeyword = videoEntity.Tags.Split(',');
                            string ListZoneId = "";
                            foreach (var key in arrKeyword)
                            {
                                if (string.IsNullOrEmpty(key)) continue;
                                var zone = ZoneDisplayStyle1.Where(k => k.ListNewsZoneId.Contains(key.Trim())).FirstOrDefault();
                                if (zone != null)
                                    if (zone.Id > 0)
                                        ListZoneId += ";" + zone.Id;
                            }
                            //không map được DisplayStyle1 chuyển sang DisplayStyle2
                            if (string.IsNullOrEmpty(ListZoneId))
                            {
                                var ZoneDisplayStyle2 = zonevideo.Where(k => k.DisplayStyle == 2).ToList();
                                bool isMap = false;
                                foreach (var key in arrKeyword)
                                {
                                    if (string.IsNullOrEmpty(key)) continue;
                                    var zone = ZoneDisplayStyle2.Where(k => k.ListNewsZoneId.Contains(key.Trim())).FirstOrDefault();
                                    if (zone != null)
                                        if (zone.Id > 0)
                                        {
                                            VideoDal.Video_UpdateZonePrimary(videoId, zone.Id);
                                            isMap = true;
                                            break;
                                        }
                                }
                                if (!isMap)
                                    VideoDal.Video_UpdateZonePrimary(videoId, 22);
                            }
                            else
                            {
                                //Nếu map được DisplayStyle1 ==> map playlist
                                ListZoneId = ListZoneId.Substring(1);
                                if (VideoDal.Video_InsertZone(videoId, ListZoneId))
                                {
                                    var arrZone = ListZoneId.Split(';');
                                    var PlayListId = 0;
                                    if (arrZone.Count() >= 2)
                                    {
                                        VideoDal.Playlist_GetIdByZoneId(Utility.ConvertToInt(arrZone[0]), Utility.ConvertToInt(arrZone[1]), videoEntity.DistributionDate.AddDays(MapPlayListTime), videoEntity.DistributionDate, videoId, ref PlayListId);
                                        if (PlayListId == 0)
                                        {
                                            VideoDal.Video_UpdateZonePrimary(videoId, 21);
                                        }
                                    }
                                    else
                                    {
                                        var ZoneDisplayStyle2 = zonevideo.Where(k => k.DisplayStyle == 2).ToList();
                                        bool isMap = false;
                                        foreach (var key in arrKeyword)
                                        {
                                            if (string.IsNullOrEmpty(key)) continue;
                                            var zone = ZoneDisplayStyle2.Where(k => k.ListNewsZoneId.Contains(key.Trim())).FirstOrDefault();
                                            if (zone != null)
                                                if (zone.Id > 0)
                                                {
                                                    VideoDal.Video_UpdateZonePrimary(videoId, zone.Id);
                                                    isMap = true;
                                                    break;
                                                }
                                        }
                                        if (!isMap)
                                            VideoDal.Video_UpdateZonePrimary(videoId, 22);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Video_Insert_By_Feed:Mapping Zone =>" + ex.Message);

                    }


                    if (!string.IsNullOrEmpty(videoEntity.FileName))
                    {
                        ActivityBo.LogUploadVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                    }
                    ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes Video_Insesert_ZoneRecut(int videoId)
        {
            try
            {
                var success = VideoDal.Video_Insesert_ZoneRecut(videoId);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes Video_UpdateZonePrimary(int videoId, int ZoneId)
        {
            try
            {
                var success = VideoDal.Video_UpdateZonePrimary(videoId, ZoneId);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes Video_InsertZone(int videoId, string ZoneIds)
        {
            try
            {
                var success = VideoDal.Video_InsertZone(videoId, ZoneIds);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlayListId)
        {
            try
            {
                var success = VideoDal.Playlist_GetIdByZoneId(zoneId1, zoneId2, DateFrom, DateTo, VideoId, ref PlayListId);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static List<ZoneVideoEntity> ZoneVideo_GetAll()
        {
            try
            {
                return VideoDal.ZoneVideo_GetAll();


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneVideoEntity>();
            }
        }
        #endregion

        #region VideoApi
        public static Boolean GetVideoApiById(int videoId)
        {
            try
            {
                return VideoDal.GetVideoApiById(videoId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetVideoApiById:{0}", ex.Message));
            }
        }
        public static Boolean InsertVideoApi(VideoEntity videoEntity)
        {
            if (BoConstants.IsBuildLinkVideo)
            {
                var zoneVideoUrl = "video";
                if (videoEntity.ZoneId > 0)
                {
                    var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                    if (videoZone != null)
                    {
                        zoneVideoUrl = videoZone.Url;
                    }
                }
                var urlFormat = BoConstants.VideoUrlFormat;
                if (urlFormat.IndexOf("{2}") > 0)
                {
                    videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                        Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                }
                else
                {
                    videoEntity.Url = string.Format(urlFormat,
                        Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                }
            }
            else
            {
                videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
            }
            try
            {
                return VideoDal.InsertVideoApi(videoEntity);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.InsertVideoApi:{0}", ex.Message));
            }
        }
        public static List<VideoApiEntity> GetListVideoApiById(string listVideoId)
        {
            try
            {
                return VideoDal.GetListVideoApiById(listVideoId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.GetListVideoApiById:{0}", ex.Message));
            }
        }
        public static Boolean Kenh14InsertVideoApi(VideoEntity videoEntity)
        {

            if (BoConstants.IsBuildLinkVideo)
            {
                var zoneVideoUrl = "video";
                if (videoEntity.ZoneId > 0)
                {
                    var videoZone = ZoneVideoDal.GetById(videoEntity.ZoneId);
                    if (videoZone != null)
                    {
                        zoneVideoUrl = videoZone.Url;
                    }
                }
                var urlFormat = BoConstants.VideoUrlFormat;
                if (urlFormat.IndexOf("{2}") > 0)
                {
                    videoEntity.Url = string.Format(urlFormat, zoneVideoUrl,
                        Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                }
                else
                {
                    videoEntity.Url = string.Format(urlFormat,
                        Utility.UnicodeToKoDauAndGach(videoEntity.Name),
                        videoEntity.Id > 0 ? videoEntity.Id.ToString() : "{VideoId}");
                }
            }
            else
            {
                videoEntity.Url = Utility.UnicodeToKoDauAndGach(videoEntity.Name);
            }
            try
            {
                return VideoDal.Kenh14EPLInsertVideoApi(videoEntity);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.sp_Kenh14EPL_InsertVideoApi:{0}", ex.Message));
            }
        }
        public static Boolean Kenh14EPLChangerPublishVideoApi(string keyVideo, int status)
        {
            try
            {
                return VideoDal.Kenh14EPLChangerPublishVideoApi(keyVideo, status);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.sp_Kenh14EPL_ChangerPublish_VideoApi:{0}", ex.Message));
            }
        }
        public static Boolean Kenh14EPLChangerUpdateVideoApi(VideoEntity videoEntity)
        {
            try
            {
                return VideoDal.Kenh14EPLChangerUpdateVideoApi(videoEntity);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.sp_Kenh14EPL_UpdateVideoApi:{0}", ex.Message));
            }
        }
        #endregion

        #region MP3
        public static List<Mp3Entity> SearchMp3(string username, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return VideoDal.SearchMp3(username, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("VideoDal.Search:{0}", ex.Message));
            }
        }
        public static Mp3Entity GetMp3ById(int id)
        {
            try
            {
                return VideoDal.GetMp3ById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static ErrorMapping.ErrorCodes SaveMp3(Mp3Entity mp3Entity, ref int id)
        {
            try
            {
                if (null == mp3Entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                if (mp3Entity.Id > 0)
                {
                    return UpdateMp3(mp3Entity);
                }
                return InsertMp3(mp3Entity, ref id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes InsertMp3(Mp3Entity mp3Entity, ref int id)
        {
            try
            {
                if (null == mp3Entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var success = VideoDal.InsertMp3(mp3Entity, ref id);

                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateMp3(Mp3Entity mp3Entity)
        {
            try
            {
                if (null == mp3Entity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var success = VideoDal.UpdateMp3(mp3Entity);
                return success
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        #endregion
    }
}
