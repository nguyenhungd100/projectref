﻿using ChannelVN.CMS.DAL.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Statistic;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BO.Base.Statistic
{
    public class PageViewStatisticBo
    {
        public static bool PageViewByZone_UpdateByDay(PageViewByZoneEntity viewEntity)
        {
            return PageViewStatisticDal.PageViewByZone_UpdateByDay(viewEntity);
        }

        public static DateTime GetLastestRun()
        {
            return PageViewStatisticDal.GetLastestRun();
        }

        public static bool PageViewAllSite_UpdateByDay(PageViewAllSiteEntity objPageViewUpdate)
        {
            return PageViewStatisticDal.PageViewAllSite_UpdateByDay(objPageViewUpdate);
        }

        public static List<PageViewAllSiteEntity> PageViewAllSite_GetData(DateTime fromDate, DateTime toDate,int channelId)
        {
            return PageViewStatisticDal.PageViewAllSite_GetData(fromDate, toDate, channelId);
        }

        public static List<PageViewByZoneEntity> PageViewByZone_GetData(DateTime fromDate, DateTime toDate,string zoneIds,int channelId)
        {
            return PageViewStatisticDal.PageViewByZone_GetData(fromDate, toDate, zoneIds, channelId);
        }

        public static ChannelEntity GetChannel_ByName(string channelName)
        {
            return PageViewStatisticDal.GetChannel_ByName(channelName);
        }
    }
}
