﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Thread;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BO.Base.Thread
{
    public class ThreadBo
    {
        #region Get
        public virtual ThreadDetailEntity GetThreadByThreadId(long threadId)
        {
            ThreadDetailEntity threadDetail = null;
            var thread = ThreadDal.GetThreadByThreadId(threadId);
            if (null != thread)
            {
                threadDetail = new ThreadDetailEntity
                {
                    Id = thread.Id,
                    Name = thread.Name,
                    UnsignName = thread.UnsignName,
                    Title = thread.Title,
                    Description = thread.Description,
                    Url = thread.Url,
                    Avatar = thread.Avatar,
                    HomeAvatar = thread.HomeAvatar,
                    SpecialAvatar = thread.SpecialAvatar,
                    IsHot = thread.IsHot,
                    IsOnHome = thread.IsOnHome,
                    CreatedDate = thread.CreatedDate,
                    ModifiedDate = thread.ModifiedDate,
                    CreatedBy = thread.CreatedBy,
                    EditedBy = thread.EditedBy,
                    MetaKeyword = thread.MetaKeyword,
                    MetaContent = thread.MetaContent,
                    TemplateId = thread.TemplateId,
                    ThreadInZone = ThreadInZoneDal.GetThreadInZoneByThreadId(thread.Id),
                    ThreadRelation = ThreadDal.GetRelationThread(thread.Id),
                    Invisibled = thread.Invisibled,
                    NewsCoverId = thread.NewsCoverId
                };
            }
            return threadDetail;
        }
        public virtual List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            try
            {
                //tam thoi chưa lấy từ redis
                //var list = BoCached.Base.Thread.ThreadDalFactory.GetThreadByNewsId(newsId);
                //if (list == null)
                //{
                //    list= ThreadDal.GetThreadByNewsId(newsId);
                //    if(list!=null && list.Count() > 0)
                //    {
                //        //add to redis
                //        BoCached.Base.Thread.ThreadDalFactory.AddThreadByNewsId(newsId,list);
                //    }
                //}
                return ThreadDal.GetThreadByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<ThreadEntity>();
            }
        }
        public virtual List<ThreadEntity> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            var threads= new List<ThreadEntity>();
            try
            {
                //var ids = BoSearch.Base.Thread.ThreadDalFactory.SearchThread(keyword, zoneId, (int)orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
                //if (ids!=null && ids.Count > 0)
                //{
                //    threads = BoCached.Base.Thread.ThreadDalFactory.GetThreadByListId(ids);
                //}
                //if (threads == null || (threads != null && threads.Count() <= 0))
                //{
                //    threads= ThreadDal.SearchThread(keyword, zoneId, (int)orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
                //}
                threads = ThreadDal.SearchThread(keyword, zoneId, (int)orderBy, isHotThread, pageIndex, pageSize, ref totalRow);

                return threads;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return threads;
            }            
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitEsByThreadAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = ThreadDal.InitESAllThread(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = ThreadDal.InitESAllThread(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new ThreadSearchEntity
                {
                    Id = s.Id,
                    Name = s.Name,
                    ZoneIds = ThreadInZoneDal.GetThreadInZoneByThreadId(s.Id).Select(se=>se.ZoneId.ToString()).ToArray(),
                    CreatedDate = s.CreatedDate,
                    IsHot=s.IsHot
                }).ToList();
                BoSearch.Base.Thread.ThreadDalFactory.InitAllThread(list);
            }, action);
        }

        public BoCached.Entity.Init.LogInitAsyncEntity InitRedisByThreadAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = ThreadDal.InitESAllThread(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = ThreadDal.InitESAllThread(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.Thread.ThreadDalFactory.InitAllThread(data);
            }, action);
        }

        public virtual List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {
            return ThreadDal.SearchThreadForSuggestion(top, zoneId, keyword, (int)orderBy);
        }
        #endregion

        #region Update
        public virtual ErrorMapping.ErrorCodes Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref int newThreadId)
        {
            if (string.IsNullOrEmpty(thread.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateThreadInvalidThreadName;
            }
            if (thread.CreatedDate == DateTime.MinValue) thread.CreatedDate = DateTime.Now;
            return ThreadDal.Insert(thread, zoneId, zoneIdList, relationThread, ref newThreadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            if (string.IsNullOrEmpty(thread.Name))
            {
                return ErrorMapping.ErrorCodes.UpdateThreadThreadNotFound;
            }
            var currentThread = ThreadDal.GetThreadByThreadId(thread.Id);
            if (currentThread == null)
            {
                return ErrorMapping.ErrorCodes.UpdateThreadThreadNotFound;
            }
            currentThread.Name = thread.Name;
            currentThread.UnsignName = thread.UnsignName;
            currentThread.Title = thread.Title;
            currentThread.Description = thread.Description;
            currentThread.Url = thread.Url;
            currentThread.Avatar = thread.Avatar;
            currentThread.HomeAvatar = thread.HomeAvatar;
            currentThread.SpecialAvatar = thread.SpecialAvatar;
            currentThread.IsHot = thread.IsHot;
            currentThread.IsOnHome = thread.IsOnHome;
            currentThread.CreatedDate = thread.CreatedDate == DateTime.MinValue
                                            ? currentThread.CreatedDate
                                            : thread.CreatedDate;
            currentThread.ModifiedDate = DateTime.Now;
            currentThread.EditedBy = WcfMessageHeader.Current.ClientUsername;
            currentThread.MetaKeyword = thread.MetaKeyword;
            currentThread.MetaContent = thread.MetaContent;
            currentThread.TemplateId = thread.TemplateId;
            currentThread.Invisibled = thread.Invisibled;
            currentThread.NewsCoverId = thread.NewsCoverId;

            return ThreadDal.Update(thread, zoneId, zoneIdList, relationThread) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes DeleteById(long threadId)
        {
            return ThreadDal.DeleteById(threadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateThreadHot(long threadId, bool isHotThread)
        {
            return ThreadDal.UpdateThreadHot(threadId, isHotThread) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateIsInvisibled(long threadId, bool invisibled)
        {
            return ThreadDal.UpdateIsInvisibled(threadId, invisibled) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public virtual ErrorMapping.ErrorCodes UpdateThreadNews(long threadId, string deleteNewsId, string addNewsId)
        {
            var result= ThreadNewsDal.UpdateNews(threadId, deleteNewsId, addNewsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if(result== ErrorMapping.ErrorCodes.Success && !string.IsNullOrEmpty(deleteNewsId))
            {
                //update threadid in news
                BoCached.Base.News.NewsDalFactory.UpdateThreadIdInNewsByNewsId(threadId, deleteNewsId);
            }
            return result;
        }
        public static ErrorMapping.ErrorCodes MultiUpdateThreadNews(string listThreadId, long newsId, int threadId)
        {
            var data = ThreadNewsDal.MultiUpdateThreadNews(listThreadId, newsId, threadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (data == ErrorMapping.ErrorCodes.Success)
            {
                //BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(newsId, 0);
                //xoa cached threadnews
                //var arrayId = listThreadId.Split(new char[] { ';',',' }).ToList();
                //if (arrayId != null && arrayId.Count > 0)
                //{
                //    foreach (var id in arrayId)
                //    {
                //        var hashName = "ThreadNews:" + id;
                //        BoCached.Base.Topic.TopicDalFactory.DeleteDataSearchNewsInTopic(hashName);
                //    }
                //}
            }
            return data;
        }
        public virtual ErrorMapping.ErrorCodes UpdateThreadNewsRelated(long newsId, string relatedThreads)
        {
            return ThreadNewsDal.UpdateThreadNews(newsId, relatedThreads) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion
    }
}
