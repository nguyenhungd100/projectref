﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsPublishBo
    {
        /// <summary>
        /// Lấy danh sách tin bài theo Date và zoneId
        /// </summary>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public static List<NewsPublishEntity> GetListDataByDate(int zoneIds, DateTime fromDate,DateTime toDate)
        {
            var newsList = new List<NewsPublishEntity>();
            try
            {
                newsList = NewsPublishDal.GetListNewsPublishByDate(zoneIds, fromDate, toDate);
            }
            catch (Exception ex)
            {
            }
            return newsList;
        }

        public static List<NewsPublishEntity> GetLastestNewsByHour(int hour, int zoneId)
        {
            var newsList = new List<NewsPublishEntity>();
            try
            {
                newsList = NewsPublishDal.GetLastestNewsByHour(hour, zoneId);
            }
            catch (Exception ex)
            {
            }
            return newsList;
        }

        public static NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId) {
            return NewsPublishDal.GetByNewsIdAndZoneId(newsId, zoneId);
        }

        public static NewsPublishEntity GetNewsPublishByNewsId(long newsId)
        {
            return NewsPublishDal.GetNewsPublishByNewsId(newsId);
        }

        public static List<NewsPublishEntity>  GetListNewsByListNewsId(string listNewsId)
        {
            return NewsPublishDal.GetListNewsByListNewsId(listNewsId);
        }

        #region New methods for optimization
        public static List<NewsPublishForSearchEntity> SearchNewsPublishedByKeyword(int zoneId, string keyword, int DisplayPosition, int Priority, bool IsShowNewsBomd, int pageIndex, int pageSize, bool useFullDatabase)
        {
            return NewsPublishDal.SearchByKeyword(zoneId, keyword, DisplayPosition, Priority, IsShowNewsBomd, pageIndex, pageSize, useFullDatabase);
        }
        public static List<NewsPublishForSearchEntity> SearchNewsPublishedByKeywordES(string keyword, int zoneId, int displayPosition, int priority, bool showBomb, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var list = new List<NewsPublishForSearchEntity>();
                var data = BoSearch.Base.News.NewsDalFactory.SearchNewsForNewsPositionES(keyword, zoneId, displayPosition, priority, showBomb, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var result = BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(data.Select(s => s.Id.ToString()).ToList());
                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            foreach (var item2 in result)
                            {
                                try
                                {
                                    if (item2.NewsInfo != null && item.Id == item2.NewsInfo.Id)
                                    {
                                        list.Add(new NewsPublishForSearchEntity
                                        {
                                            NewsId = item2.NewsInfo.Id,
                                            Title = item2.NewsInfo.Title,
                                            Avatar = item2.NewsInfo.Avatar,                                            
                                            Sapo = item2.NewsInfo.Sapo,
                                            Url = item2.NewsInfo.Url,                                            
                                            DistributionDate = item2.NewsInfo.DistributionDate,
                                            EncryptId = item2.NewsInfo.EncryptId,
                                            Type = item2.NewsInfo.Type,
                                            ZoneId    = item2.NewsInfo.ZoneId,
                                            OriginalId= item2.NewsInfo.OriginalId,
                                            Priority= item2.NewsInfo.Priority,
                                            Source= item2.NewsInfo.Source
                                        });
                                        break;
                                    }
                                    else if (result.LastIndexOf(item2) == (result.Count - 1))
                                    {
                                        list.Add(new NewsPublishForSearchEntity
                                        {
                                            NewsId = item.Id,
                                            Title = item.Title,
                                            Avatar = item.Avatar,                                            
                                            Sapo = item.Sapo == null ? "" : item.Sapo,
                                            Url = item.Url == null ? "" : item.Url,                                            
                                            DistributionDate = item.DistributionDate,
                                            EncryptId = item.EncryptId,
                                            Type = item.Type,
                                            ZoneId=0,
                                            OriginalId=0,
                                            Priority=item.Priority,
                                            Source=""
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        list = data.Select(s => new NewsPublishForSearchEntity
                        {
                            NewsId = s.Id,
                            Title = s.Title,
                            Avatar = s.Avatar,                            
                            Sapo = s.Sapo == null ? "" : s.Sapo,
                            Url = s.Url == null ? "" : s.Url,                            
                            DistributionDate = s.DistributionDate,
                            EncryptId = s.EncryptId,
                            Type = s.Type,
                            ZoneId = 0,
                            OriginalId = 0,
                            Priority = s.Priority,
                            Source = ""
                        }).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return new List<NewsPublishForSearchEntity>();
            }
        }

        public static List<NewsPublishForSearchEntity> SearchNewsPublishedByKeywordESInDateNow(string keyword, int zoneId, int displayPosition, int priority, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var list = new List<NewsPublishForSearchEntity>();
                var data = BoSearch.Base.News.NewsDalFactory.SearchNewsForNewsPositionESInDateNow(keyword, zoneId, displayPosition, priority, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var result = BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(data.Select(s => s.Id.ToString()).ToList());
                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            foreach (var item2 in result)
                            {
                                try
                                {
                                    if (item2.NewsInfo != null && item.Id == item2.NewsInfo.Id)
                                    {
                                        list.Add(new NewsPublishForSearchEntity
                                        {
                                            NewsId = item2.NewsInfo.Id,
                                            Title = item2.NewsInfo.Title,
                                            Avatar = item2.NewsInfo.Avatar,
                                            Sapo = item2.NewsInfo.Sapo,
                                            Url = item2.NewsInfo.Url,
                                            DistributionDate = item2.NewsInfo.DistributionDate,
                                            EncryptId = item2.NewsInfo.EncryptId,
                                            Type = item2.NewsInfo.Type,
                                            ZoneId = item2.NewsInfo.ZoneId,
                                            OriginalId = item2.NewsInfo.OriginalId,
                                            Priority = item2.NewsInfo.Priority,
                                            Source = item2.NewsInfo.Source
                                        });
                                        break;
                                    }
                                    else if (result.LastIndexOf(item2) == (result.Count - 1))
                                    {
                                        list.Add(new NewsPublishForSearchEntity
                                        {
                                            NewsId = item.Id,
                                            Title = item.Title,
                                            Avatar = item.Avatar,
                                            Sapo = item.Sapo == null ? "" : item.Sapo,
                                            Url = item.Url == null ? "" : item.Url,
                                            DistributionDate = item.DistributionDate,
                                            EncryptId = item.EncryptId,
                                            Type = item.Type,
                                            ZoneId = 0,
                                            OriginalId = 0,
                                            Priority = item.Priority,
                                            Source = ""
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                                }
                            }
                        }

                    }
                    else
                    {
                        list = data.Select(s => new NewsPublishForSearchEntity
                        {
                            NewsId = s.Id,
                            Title = s.Title,
                            Avatar = s.Avatar,
                            Sapo = s.Sapo == null ? "" : s.Sapo,
                            Url = s.Url == null ? "" : s.Url,
                            DistributionDate = s.DistributionDate,
                            EncryptId = s.EncryptId,
                            Type = s.Type,
                            ZoneId = 0,
                            OriginalId = 0,
                            Priority = s.Priority,
                            Source = ""
                        }).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return new List<NewsPublishForSearchEntity>();
            }
        }
        #endregion
    }
}
