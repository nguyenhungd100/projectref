﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsChildBo
    {
        #region News Child Version
        public static NewsChildVersionEntity GetByNewsIdAndOrder(long parentNewsId, int order, string userDoAction)
        {
            return NewsChildVersionDal.GetByNewsIdAndOrder(parentNewsId, order, userDoAction);
        }
        public static NewsChildVersionEntity UpdateNewsChildVersion(long parentNewsId, int order, string body, string userDoAction)
        {
            var newsVersionId = 0L;
            if (NewsChildVersionDal.Update(parentNewsId, ref order, body, ref newsVersionId, userDoAction))
            {
                return new NewsChildVersionEntity { NewsParentId = parentNewsId, Order = order, Body = body, NewsVersionId = newsVersionId };
            }
            return null;
        }
        public static ErrorMapping.ErrorCodes UpdateNewsChildVersion(NewsChildVersionEntity entity)
        {
            try
            {
                return NewsChildVersionDal.Update(entity) ? ErrorMapping.ErrorCodes.Success
                                  : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch { return ErrorMapping.ErrorCodes.BusinessError; }
        }
        public static ErrorMapping.ErrorCodes RemoveNewsChild(long parentNewsId, int order, string userDoAction)
        {
            return NewsChildVersionDal.Remove(parentNewsId, order, userDoAction)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }


        public static ErrorMapping.ErrorCodes InsertNewsChildVersion(NewsChildVersionEntity entity)
        {
            try
            {
                return NewsChildDal.InsertNewsChildVersion(entity)
                              ? ErrorMapping.ErrorCodes.Success
                              : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }

        public static List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildDal.GetNewsChildVersionByNewsId(newsId, isGetContent);
        }

        public static NewsChildVersionEntity GetNewsChildVersionByNewsAndOrderId(long newsVersionId, int order)
        {
            return NewsChildDal.GetNewsChildVersionByNewsAndOrderId(newsVersionId, order);
        }

        public static ErrorMapping.ErrorCodes DeleteNewsChildVersion(long versionId, int order)
        {
            return NewsChildVersionDal.Remove(versionId, order)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion

        #region NewsChild
        public static List<NewsChildEntity> GetByNewsId(long newsId, bool isGetContent)
        {
            try
            {
                var list = BoCached.Base.NewsChild.NewsChildDalFactory.GetNewsChildByNewsId(newsId, isGetContent);
                if (list == null)
                {
                    list = NewsChildDal.GetByNewsId(newsId, isGetContent);
                    if (list != null && list.Count() > 0)
                    {
                        //add to redis
                        BoCached.Base.NewsChild.NewsChildDalFactory.AddNewsChildByNewsId(newsId, list);
                    }
                }
                return list;
            }
            catch { return null; }
        }

        public static ErrorMapping.ErrorCodes InsertNewsChild(NewsChildEntity entity)
        {
            try
            {
                //Kiểm tra thông tin bài viết
                var NewsInfo = NewsDal.GetNewsById(entity.NewsParentId);
                if (NewsInfo != null && !string.IsNullOrEmpty(NewsInfo.Title))
                {
                    //Nếu tin bài đã được xuất bản
                    if (NewsInfo.Status == (int)NewsStatus.Published)
                    {
                        var newsChildPublish = new NewsChildPublishEntity
                        {
                            NewsParentId = entity.NewsParentId,
                            Order = entity.Order,
                            Body = entity.Body
                        };
                        NewsChildPublishDal.InsertNewsChildPublish(newsChildPublish);
                    }
                    return NewsChildDal.InsertNewsChild(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                }
                else
                {
                    return NewsChildDal.InsertNewsChild(entity)
                              ? ErrorMapping.ErrorCodes.Success
                              : ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            catch
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsChild(NewsChildEntity entity)
        {
            try
            {
                //Kiểm tra thông tin bài viết
                var NewsInfo = NewsDal.GetNewsById(entity.NewsParentId);
                if (NewsInfo != null && !string.IsNullOrEmpty(NewsInfo.Title))
                {
                    //Nếu tin bài đã được xuất bản
                    if (NewsInfo.Status == (int)NewsStatus.Published)
                    {
                        var newsChildPublish = new NewsChildPublishEntity
                        {
                            NewsParentId = entity.NewsParentId,
                            Order = entity.Order,
                            Body = entity.Body
                        };
                        NewsChildPublishDal.UpdateNewsChildPublish(newsChildPublish);
                    }
                    return NewsChildDal.UpdateNewsChild(entity)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
                }
                else
                {
                    return NewsChildDal.UpdateNewsChild(entity)
                              ? ErrorMapping.ErrorCodes.Success
                              : ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            catch
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteNewsChild(long parentNewsId, int order)
        {
            try
            {

                return NewsChildDal.DeleteNewsChild(parentNewsId, order)
                          ? ErrorMapping.ErrorCodes.Success
                          : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<NewsChildEntity> GetNewsChildByNewsId(long newsId, bool isGetContent = false)
        {
            return NewsChildDal.GetByNewsId(newsId, isGetContent);
        }

        public static NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order)
        {
            return NewsChildDal.GetByNewsAndOrderId(parentNewsId, order);
        }
        #endregion
    }
}
