﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsConfigBo
    {
        public static NewsConfigEntity GetByConfigName(string configName)
        {
            return NewsConfigDal.GetByConfigName(configName);
        }
        public static NewsConfigEntity GetNewsConfigByTypeInitValue(int type, string value)
        {
            return NewsConfigDal.GetNewsConfigByTypeInitValue(type, value);
        }
        public static List<NewsConfigEntity> GetAll(int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsConfigDal.GetAll(pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsConfigEntity> GetListConfigForStaticHtmlTemplate(string groupConfigKey, EnumStaticHtmlTemplateType type)
        {
            var data = BoCached.Base.NewsConfig.NewsConfigDalFactory.GetListConfigByGroupKey(groupConfigKey, (int)type);
            if(data==null || (data!=null && data.Count <= 0))
            {
                data= NewsConfigDal.GetListConfigByGroupKey(groupConfigKey, (int)type);
            }
            return data;
        }
        public static List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey)
        {
            return NewsConfigDal.GetListConfigByGroupKey(groupConfigKey, 0);
        }
        public static ErrorMapping.ErrorCodes SetValue(string configName, string configValue)
        {
            var result= NewsConfigDal.SetValue(configName, configValue);
            if (result)
            {
                BoCached.Base.NewsConfig.NewsConfigDalFactory.SetValue(configName, configValue);
            }
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes SetValue(string configName, string configValue, int configType)
        {
            var result = NewsConfigDal.SetValue(configName, configValue, configType);            
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes SetValue(string configName, string configValue, int configType, string configLabel, string configInitValue)
        {
            var result = NewsConfigDal.SetValueAndLabel(configName, configLabel, configValue, configType, configInitValue);
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes SetValue(NewsConfigEntity obj)
        {
            var result = NewsConfigDal.SetValue(obj);
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes SetValueForStaticHtmlTemplate(string configName, string configLabel, string configValue, EnumStaticHtmlTemplateType configType)
        {
            var data = NewsConfigDal.SetValueAndLabel(configName, configLabel, configValue, (int)configType);            
            return data ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes DeleteNewsConfigByConfigName(string configName)
        {
            var result = NewsConfigDal.DeleteNewsConfigByConfigName(configName);
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
