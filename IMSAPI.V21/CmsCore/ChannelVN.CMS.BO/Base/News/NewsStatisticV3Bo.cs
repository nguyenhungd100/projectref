﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsStatisticV3Bo
    {
        public static List<StatisticV3ViewCountEntity> ListPageViewAll(Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListPageViewAll({0}, {1})", fromDate, toDate));
                return StatisticV3Dal.PageViewAll(fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3ViewCountEntity>();
            }
        }

        public static List<StatisticV3CategoryEntity> ListCategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListCategoryViewCount({0}, {1}, {2})", cateId, fromDate, toDate));
                return StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3CategoryEntity>();
            }
        }

        public static List<StatisticV3ViewCountEntity> ListPageViewAll(ChannelId channel, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListPageViewAll({0}, {1}, channelId: {2})", fromDate, toDate, channel));
                if (channel == ChannelId.AutoPro)
                {
                    return DAL.External.AutoPro.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.CafeBiz)
                {
                    return DAL.External.CafeBiz.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.CafeF)
                {
                    return DAL.External.CafeF.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.GameK)
                {
                    return DAL.External.GameK.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.GenK)
                {
                    return DAL.External.GenK.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.GiaDinh)
                {
                    return DAL.External.GiaDinhNet.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.Nld)
                {
                    return DAL.External.NguoiLaoDong.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.Soha)
                {
                    return DAL.External.SohaNews.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.VTV)
                {
                    return DAL.External.VTV.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.DanTri)
                {
                    return DAL.External.DanTri.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.Kenh14)
                {
                    return DAL.External.Kenh14.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                if (channel == ChannelId.VTVEnglish)
                {
                    return DAL.External.VTVEnglish.StatisticV3Dal.PageViewAll(fromDate, toDate);
                }
                return channel == ChannelId.Skds ? DAL.External.Skds.StatisticV3Dal.PageViewAll(fromDate, toDate) : new List<StatisticV3ViewCountEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3ViewCountEntity>();
            }
        }

        public static List<StatisticV3CategoryEntity> ListCategoryViewCount(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListCategoryViewCount({0}, {1}, {2})", cateId, fromDate, toDate));
                if (channel == ChannelId.AutoPro)
                {
                    return DAL.External.AutoPro.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.CafeF)
                {
                    return DAL.External.CafeF.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.CafeBiz)
                {
                    return DAL.External.CafeBiz.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.GameK)
                {
                    return DAL.External.GameK.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.GenK)
                {
                    return DAL.External.GenK.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.GiaDinh)
                {
                    return DAL.External.GiaDinhNet.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.Nld)
                {
                    return DAL.External.NguoiLaoDong.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.Soha)
                {
                    return DAL.External.SohaNews.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.VTV)
                {
                    return DAL.External.VTV.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.DanTri)
                {
                    return DAL.External.DanTri.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.Kenh14)
                {
                    return DAL.External.Kenh14.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.VTVEnglish)
                {
                    return DAL.External.VTVEnglish.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate);
                }
                return channel == ChannelId.Skds ? DAL.External.Skds.StatisticV3Dal.CategoryViewCount(cateId, fromDate, toDate) : new List<StatisticV3CategoryEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3CategoryEntity>();
            }
        }

        public static List<StatisticV3CategoryEntity> ListCategoryViewCountSummary(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            try
            {
                if (channel == ChannelId.VTV)
                {
                    return DAL.External.VTV.StatisticV3Dal.CategoryViewCountSummary(cateId, fromDate, toDate);
                }
                return new List<StatisticV3CategoryEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3CategoryEntity>();
            }
        }

        public static List<StatisticV3ViewCountEntity> ListPageViewAllMobile(ChannelId channel, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListPageViewAllMobile({0}, {1}, channelId: {2})", fromDate, toDate, channel));
                if (channel == ChannelId.DanTri)
                {
                    return DAL.External.DanTri.StatisticV3Dal.PageViewAllMobile(fromDate, toDate);
                }
                if (channel == ChannelId.GiaDinh)
                {
                    return DAL.External.GiaDinhNet.StatisticV3Dal.PageViewAllMobile(fromDate, toDate);
                }
                if (channel == ChannelId.Kenh14)
                {
                    return DAL.External.Kenh14.StatisticV3Dal.PageViewAllMobile(fromDate, toDate);
                }
                if (channel == ChannelId.Soha)
                {
                    return DAL.External.SohaNews.StatisticV3Dal.PageViewAllMobile(fromDate, toDate);
                }
                if (channel == ChannelId.GameK)
                {
                    return DAL.External.GameK.StatisticV3Dal.PageViewAllMobile(fromDate, toDate);
                }
                return new List<StatisticV3ViewCountEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3ViewCountEntity>();
            }
        }

        public static List<StatisticV3CategoryEntity> ListCategoryViewCountMobile(ChannelId channel, string cateId, Int64 fromDate, Int64 toDate)
        {
            try
            {
                Logger.WriteLog(Logger.LogType.Debug, string.Format("ListCategoryViewCountMobile({0}, {1}, {2})", cateId, fromDate, toDate));
                if (channel == ChannelId.DanTri)
                {
                    return DAL.External.DanTri.StatisticV3Dal.CategoryViewCountMobile(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.Kenh14)
                {
                    return DAL.External.Kenh14.StatisticV3Dal.CategoryViewCountMobile(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.Soha)
                {
                    return DAL.External.SohaNews.StatisticV3Dal.CategoryViewCountMobile(cateId, fromDate, toDate);
                }
                if (channel == ChannelId.GameK)
                {
                    return DAL.External.GameK.StatisticV3Dal.CategoryViewCountMobile(cateId, fromDate, toDate);
                }
                return channel == ChannelId.GiaDinh ? DAL.External.GiaDinhNet.StatisticV3Dal.CategoryViewCountMobile(cateId, fromDate, toDate) : new List<StatisticV3CategoryEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<StatisticV3CategoryEntity>();
            }
        }
    }
}
