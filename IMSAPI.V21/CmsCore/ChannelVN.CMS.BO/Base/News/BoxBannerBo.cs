﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class BoxBannerBo
    {
        public static ErrorMapping.ErrorCodes InsertBoxBanner(BoxBannerEntity boxBanner, string zoneIdList, ref int id)
        {
            try
            {
                if (null == boxBanner)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxBannerDal.Insert(boxBanner, zoneIdList, ref id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxBannerBo.InsertBoxBanner:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxBanner(BoxBannerEntity boxBanner, string zoneIdList)
        {
            try
            {
                if (null == boxBanner || boxBanner.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxBannerDal.Update(boxBanner, zoneIdList)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxBannerBo.UpdateBoxBanner:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxBannerPriority(string listOfBoxBannerId)
        {
            try
            {
                if (string.IsNullOrEmpty(listOfBoxBannerId))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxBannerDal.UpdateSortOrder(listOfBoxBannerId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxBannerBo.UpdateBoxBannerPriority:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes DeleteBoxBanner(int boxBannerId)
        {
            try
            {
                if (boxBannerId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxBannerDal.Delete(boxBannerId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxBannerBo.DeleteBoxBanner:{0}", ex.Message));
            }
        }

        public static BoxBannerEntity GetBoxBannerById(int id)
        {
            var data= BoxBannerDal.GetById(id);
            if (data != null)
            {
                data.ListInZone= BoxBannerDal.GetListZoneIdById(id);
            }
            return data;
        }

        public static List<BoxBannerEntity> GetListBoxBannerByZoneIdAndPosition(int zoneId, int position, EnumBoxBannerStatus status, EnumBoxBannerType type)
        {
            return BoxBannerDal.GetListByZoneIdAndPosition(zoneId, position, (int)status, (int)type);
        }
        public static List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId)
        {
            return BoxBannerDal.GetListZoneByBoxBannerId(boxBannerId);
        }
    }
}
