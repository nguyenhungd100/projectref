﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class BoxLivePageEmbedBo
    {
        public static ErrorMapping.ErrorCodes InsertBoxLivePageEmbed(BoxLivePageEmbedEntity item, ref int id)
        {
            try
            {
                if (null == item)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.Insert(item, ref id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.InsertBoxLivePageEmbed:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxLivePageEmbed(BoxLivePageEmbedEntity item)
        {
            try
            {
                if (null == item || item.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.Update(item)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.UpdateBoxLivePageEmbed:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxLivePageEmbedPriority(string listOfBoxLivePageEmbedId)
        {
            try
            {
                if (string.IsNullOrEmpty(listOfBoxLivePageEmbedId))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.UpdateSortOrder(listOfBoxLivePageEmbedId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.UpdateBoxLivePageEmbedPriority:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes DeleteBoxLivePageEmbed(int boxBannerId)
        {
            try
            {
                if (boxBannerId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.Delete(boxBannerId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.DeleteBoxLivePageEmbed:{0}", ex.Message));
            }
        }

        public static BoxLivePageEmbedEntity GetBoxLivePageEmbedById(int id)
        {
            var data= BoxLivePageEmbedDal.GetById(id);            
            return data;
        }

        public static List<BoxLivePageEmbedEntity> GetListBoxLivePageEmbed(string pageName, string pageUrl, int position, int status, int type, int templateId)
        {
            return BoxLivePageEmbedDal.GetListBoxLivePageEmbed(pageName, pageUrl, position, status, type, templateId);
        }

        #region BoxInboundTemplate
        public static ErrorMapping.ErrorCodes InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            try
            {
                if (null == item)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var data = BoxLivePageEmbedDal.InsertBoxInboundTemplate(item, ref id);
                if (data == false && id <= 0)
                    return ErrorMapping.ErrorCodes.DuplicateError;

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.InsertBoxInboundTemplate:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            try
            {
                if (null == item || item.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.UpdateBoxInboundTemplate(item)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.UpdateBoxInboundTemplate:{0}", ex.Message));
            }
        }        

        public static ErrorMapping.ErrorCodes DeleteBoxInboundTemplate(int boxId)
        {
            try
            {
                if (boxId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxLivePageEmbedDal.DeleteBoxInboundTemplate(boxId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxLivePageEmbedBo.DeleteBoxInboundTemplate:{0}", ex.Message));
            }
        }

        public static BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            var data = BoxLivePageEmbedDal.GetBoxInboundTemplateById(id);
            return data;
        }

        public static List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxLivePageEmbedDal.GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);
        }
        #endregion
    }
}
