﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.DAL.Base.News;

namespace ChannelVN.CMS.BO.Base.News
{
    public class EmbedGroupBo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Insert(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                if (null == embedGroup)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return EmbedGroupDal.Insert(embedGroup, listOfZoneId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmbedGroupDal.Insert:{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Update(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            try
            {
                if (null == embedGroup)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return EmbedGroupDal.Update(embedGroup, listOfZoneId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmbedGroupDal.Update:{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listOfEmbedGroupId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateSortOrder(string listOfEmbedGroupId)
        {
            try
            {
                if (null == listOfEmbedGroupId)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return EmbedGroupDal.UpdateSortOrder(listOfEmbedGroupId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmbedGroupDal.UpdateSortOrder:{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<EmbedGroupEntity> GetAll()
        {
            try
            {
                var embedGroup = EmbedGroupDal.GetAll();
                var embedGroupCount = embedGroup.Count();
                for (int i = 0; i < embedGroupCount; i++)
                {
                    var listZoneId = GetById(embedGroup[i].Id);
                    string name = "";
                    var zoneCount = listZoneId.ListOfZones.Count();
                    for (int j = 0; j < zoneCount; j++)
                    {
                        name += ZoneBo.GetZoneById(listZoneId.ListOfZones[j].ZoneId).Name + " - ";
                    }
                    if (!string.IsNullOrEmpty(name))
                        name = name.Trim().Substring(0, name.Trim().Length - 1);
                    embedGroup[i].Name = name;
                }
                return embedGroup;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmbedGroupDal.GetAll:{0}", ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static EmbedGroupWithDetailEntity GetById(int id)
        {
            var embedGroupWithDetail = new EmbedGroupWithDetailEntity();
            embedGroupWithDetail.EmbedGroup = EmbedGroupDal.GetById(id);
            var listOfZones = EmbedGroupDal.GetListZoneById(id);
            var lisOfZoneWithDetail = new List<ZoneInEmbedGroupWithDetailEntity>();
            for (int i = 0; i < listOfZones.Count(); i++)
            {
                var name = ZoneBo.GetZoneById(listOfZones[i].ZoneId).Name;
                lisOfZoneWithDetail.Add(new ZoneInEmbedGroupWithDetailEntity
                                            {
                                                Name = name,
                                                SortOrder = listOfZones[i].SortOrder,
                                                ZoneId = listOfZones[i].ZoneId
                                            });
            }
            embedGroupWithDetail.ListOfZones = lisOfZoneWithDetail;
            return embedGroupWithDetail;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {               
                return EmbedGroupDal.Delete(id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("EmbedGroupDal.Delete:{0}", ex.Message));
            }
        }
    }
}
