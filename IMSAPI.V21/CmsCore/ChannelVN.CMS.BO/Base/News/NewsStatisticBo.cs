﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.Statistic;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsStatisticBo
    {
        public static List<NewsStatisticEntity> GetStatisticStatusNewsByAccount(string account, int status, DateTime startDate, DateTime endDate)
        {
            return NewsStatisticDal.GetStatisticStatusNewsByAccount(account, status, startDate, endDate);

        }

        public static List<NewsStatisticEntity> GetStatisticStatusNewsByAccountAdmin(string createdBy, int status, DateTime startDate, DateTime endDate)
        {
            return NewsStatisticDal.GetStatisticStatusNewsByAccountAdmin(createdBy, status, startDate, endDate);
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction(string zoneId, DateTime startDate, DateTime endDate)
        {
            try {
                var rsCache = BoCached.Base.Statistic.StatisticDalFactory.GetStaticNewsTotalProducttionCache(zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                if (rsCache == null || (rsCache != null && rsCache.Count <= 0))
                {                    
                    var data = NewsStatisticDal.GetStatisticNewsTotalProduction(zoneId, startDate, endDate);
                    if (data != null && data.Count > 0)
                    {
                        BoCached.Base.Statistic.StatisticDalFactory.AddStaticNewsTotalProducttionCache(data, zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                    }
                    return data;
                }
                else {
                    return rsCache;
                }
            }
            catch
            {
                return new List<NewsStatisticTotalProduction>();
            }
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction2(string zoneId, DateTime startDate, DateTime endDate)
        {
            try
            {
                //var rsCache = BoCached.Base.Statistic.StatisticDalFactory.GetStaticNewsTotalProducttionCache(zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                //if (rsCache == null || (rsCache != null && rsCache.Count <= 0))
                //{
                    //thong ke tu es
                    var data = BoSearch.Base.News.NewsDalFactory.GetStatisticNewsTotalProduction(zoneId, startDate, endDate);
                    //var data = NewsStatisticDal.GetStatisticNewsTotalProduction(zoneId, startDate, endDate);
                    //if (data != null && data.Count > 0)
                    //{
                    //    BoCached.Base.Statistic.StatisticDalFactory.AddStaticNewsTotalProducttionCache(data, zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                    //}
                    return data;
                //}
                //else {
                //    return rsCache;
                //}
            }
            catch
            {
                return new List<NewsStatisticTotalProduction>();
            }
        }

        public static List<ViewCountByUserEntity> GetUserViewCount(DateTime startDate, DateTime endDate)
        {
            return NewsStatisticDal.GetUserViewCount(startDate, endDate);
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsComment(string zoneId, DateTime startDate, DateTime endDate)
        {
            List<NewsStatisticTotalProduction> listData = NewsStatisticDal.GetStatisticNewsComment(zoneId, startDate, endDate);
            if (zoneId.Length <= 0)
            {
                Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(listData));
                List<NewsStatisticTotalProduction> externalListData = NewsStatisticDal.GetStatisticNewsCommentFromExternalDB(zoneId, startDate, endDate);
                Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(externalListData));
                DateTime d1, d2;
                var count = listData.Count;
                for (var i = 0; i < count; i++)
                {
                    foreach (var item in externalListData)
                    {
                        if (listData[i].ValuesDate == item.ValuesDate)
                        {
                            listData[i].ValuesDataCreated = item.ValuesDataCreated;
                            break;
                        }
                    }
                }
            }
            else
            {
                foreach (var e in listData)
                {
                    e.ValuesDataCreated = 0;
                }
            }
            Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(listData));
            return listData;
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProductionUser(string zoneId, DateTime startDate, DateTime endDate, string userName)
        {
            return NewsStatisticDal.GetStatisticNewsTotalProductionUser(zoneId, startDate, endDate, userName);
        }

        public static List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsForExportEntity>();
            try
            {
                newsList = NewsStatisticDal.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        public static List<NewsStatisticSourceEntity> GetStatisticNewsBySourceId(int sourceId, DateTime startDate, DateTime endDate)
        {
            return NewsStatisticDal.GetStatisticNewsBySourceId(sourceId, startDate, endDate);

        }

        public static DepartmentViewCountEntity GetDepartmentView(int departmentId, DateTime fromDate, DateTime toDate)
        {
            return NewsStatisticDal.GetDepartmentView(departmentId, fromDate, toDate);

        }
    }
}
