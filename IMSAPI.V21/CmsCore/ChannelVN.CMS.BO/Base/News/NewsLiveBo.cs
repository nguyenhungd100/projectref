﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsLiveBo
    {
        #region Sets

        private static bool ExistsAuthor(long newsId, string accountName)
        {
            var newsLiveAuthorList = NewsLiveDal.GetAuthorList(newsId);
            var user = UserDal.GetUserByUsername(accountName);
            if (null == user || user.Id <= 0)
                return false;
            if (user.IsFullPermission)
                return true;
            if (newsLiveAuthorList == null || newsLiveAuthorList.Count <= 0)
                return false;
            return newsLiveAuthorList.Any(newsLiveAuthorEntity => newsLiveAuthorEntity.AuthorName == accountName);
        }

        public static ErrorMapping.ErrorCodes Insert(NewsLiveEntity newsLiveEntity, ref long newsLiveId)
        {
            try
            {
                if (null == newsLiveEntity)
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                var accountName = !string.IsNullOrEmpty(newsLiveEntity.PublishedBy)
                                  ? newsLiveEntity.PublishedBy
                                  : !string.IsNullOrEmpty(newsLiveEntity.EditedBy)
                                        ? newsLiveEntity.EditedBy
                                        : newsLiveEntity.CreatedBy;
                if (!ExistsAuthor(newsLiveEntity.NewsId, accountName))
                    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;

                var settings = NewsLiveDal.GetSettings(newsLiveEntity.NewsId);
                if (settings == null || settings.IsFinished)
                    return ErrorMapping.ErrorCodes.NewsLiveIsFinished;

                if (NewsLiveDal.Insert(newsLiveEntity, ref newsLiveId))
                    return ErrorMapping.ErrorCodes.Success;
                //else
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.Insert:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Update(NewsLiveEntity newsLiveEntity)
        {
            try
            {
                if (null == newsLiveEntity)
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;

                var accountName = !string.IsNullOrEmpty(newsLiveEntity.PublishedBy)
                                      ? newsLiveEntity.PublishedBy
                                      : newsLiveEntity.EditedBy;
                var user = UserDal.GetUserByUsername(accountName);
                if (null == user)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
                }
                if (!(user.IsFullPermission || (GroupPermissionDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin))))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
                }

                newsLiveEntity.PublishedDateNumber = Utility.SetPublishedDate(newsLiveEntity.PublishedDate);

                if (NewsLiveDal.Update(newsLiveEntity))
                    return ErrorMapping.ErrorCodes.Success;
                //else
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.Update:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Delete(long id, string publishedBy)
        {
            try
            {
                var user = UserDal.GetUserByUsername(publishedBy);
                if (null == user)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotAllowDelete;
                }
                if (!(user.IsFullPermission || (GroupPermissionDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin))))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotAllowDelete;
                }
                return NewsLiveDal.Delete(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.Delete:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Publish(long id, string publishedBy)
        {
            var user = UserDal.GetUserByUsername(publishedBy);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsLiveNotAllowPublish;
            }
            //if (!(user.IsFullPermission || (GroupPermissionDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin))))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotAllowPublish;
            //}
            try
            {
                return NewsLiveDal.ChangeStatus(id, (int)NewsLiveStatus.Published, publishedBy,
                                                string.Empty) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.ChangeStatus:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Rollback(long id, string editedBy)
        {
            var user = UserDal.GetUserByUsername(editedBy);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
            }
            //if (!(user.IsFullPermission || (GroupPermissionDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin))))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
            //}
            try
            {
                return NewsLiveDal.ChangeStatus(id, (int)NewsLiveStatus.Created, string.Empty, editedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.ChangeStatus:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes Trash(long id, string editedBy)
        {
            var user = UserDal.GetUserByUsername(editedBy);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
            }
            //if (!(user.IsFullPermission || (GroupPermissionDal.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin))))
            //{
            //    return ErrorMapping.ErrorCodes.UpdateNewsLiveNotPermission;
            //}
            try
            {
                return NewsLiveDal.ChangeStatus(id, (int)NewsLiveStatus.Deleted, string.Empty, editedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.ChangeStatus:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes ChangeStatus(long id, int status, string publishedBy, string editedBy)
        {
            try
            {
                return NewsLiveDal.ChangeStatus(id, status, publishedBy, editedBy) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.ChangeStatus:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateContent(long id, string content)
        {
            try
            {
                return NewsLiveDal.UpdateContent(id, content) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateSettings(NewsLiveSettingsEntity newsLiveSettings)
        {
            if (null == newsLiveSettings)
                return ErrorMapping.ErrorCodes.InvalidRequest;
            try
            {
                return NewsLiveDal.UpdateSettings(newsLiveSettings) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes AddAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity)
        {
            try
            {
                return NewsLiveDal.AddAuthor(newsLiveAuthorEntity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteAuthor(long newsId, int authorId)
        {
            try
            {
                return NewsLiveDal.DeleteAuthor(newsId, authorId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        #endregion

        #region Gets

        public static NewsLiveForList GetAll(long newsId, NewsLiveStatus status, int order)
        {
            try
            {
                var newsLiveEntitiesList = NewsLiveDal.GetAll(newsId, (int)status, order);
                var newsLiveSettings = NewsLiveDal.GetSettings(newsId);
                var newsLiveAuthorList = NewsLiveDal.GetAuthorList(newsId);
                return new NewsLiveForList
                {
                    NewsLiveEntitiesList = newsLiveEntitiesList,
                    NewsLiveSettings = newsLiveSettings,
                    NewsAuthorList = newsLiveAuthorList,
                    TotalRow = newsLiveEntitiesList.Count
                };
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.GetAll:{0}", ex.Message));
            }
        }

        public static List<NewsLiveEntity> GetPaging(long newsId, NewsLiveStatus status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var newsLiveEntitiesList = NewsLiveDal.GetPaging(newsId, (int)status, order, pageIndex, pageSize,
                                                                 ref totalRow);
                return newsLiveEntitiesList;
                //var newsLiveSettings = NewsLiveDal.GetSettings(newsId);

                //return new NewsLiveForList()
                //           {
                //               NewsLiveEntitiesList = newsLiveEntitiesList,
                //               NewsLiveSettings = newsLiveSettings,
                //               TotalRow = totalRow
                //           };
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsLiveDal.GetPaging:{0}", ex.Message));
            }
        }

        public static NewsLiveEntity GetById(long id)
        {
            try
            {
                return NewsLiveDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsLiveEntity();
            }
        }

        public static NewsLiveDetail GetDetails(long newsLiveId)
        {
            try
            {
                var newsLiveEntity = NewsLiveDal.GetById(newsLiveId);
                if (null == newsLiveEntity)
                    return new NewsLiveDetail();
                var newsLiveAuthorList = NewsLiveDal.GetAuthorList(newsLiveEntity.NewsId);
                var newsLiveSettings = NewsLiveDal.GetSettings(newsLiveEntity.NewsId);
                return new NewsLiveDetail
                           {
                               NewsLive = newsLiveEntity,
                               NewsAuthorList = newsLiveAuthorList,
                               NewsSettings = newsLiveSettings
                           };
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsLiveDetail();
            }
        }

        public static List<NewsLiveAuthorEntity> GetAuthorList(long newsId)
        {
            try
            {
                return NewsLiveDal.GetAuthorList(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsLiveAuthorEntity>();
            }
        }

        public static NewsLiveSettingsEntity GetSettings(long newsId)
        {
            try
            {
                return NewsLiveDal.GetSettings(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsLiveSettingsEntity();
            }
        }

        #endregion
    }
}
