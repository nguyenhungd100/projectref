﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsLogExternalActionBo
    {
        public static List<NewsLogExternalActionEntity> GetActionLogByNewsIdAndActionType(long newsId, EnumNewsLogExternalAction actionType)
        {
            return NewsLogExternalActionDal.GetByNewsIdAndActionType(newsId, (int)actionType);
        }
        public static ErrorMapping.ErrorCodes InsertActionLog(NewsLogExternalActionEntity newsLogExternalAction)
        {
            if (newsLogExternalAction.NewsId <= 0) return ErrorMapping.ErrorCodes.BusinessError;

            newsLogExternalAction.ProcessedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            if (NewsLogExternalActionDal.Insert(newsLogExternalAction))
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
