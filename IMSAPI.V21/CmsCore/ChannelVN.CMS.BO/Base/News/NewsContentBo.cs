﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsContentBo
    {
        public static NewsContentEntity GetNewsContentByNewsId(long newsId)
        {
            var newContent = BoCached.Base.NewsContent.NewsContentDalFactory.GetNewsContentByNewsId(newsId);
            if (newContent == null)
            {
                newContent = NewsContentDal.GetByNewsId(newsId);
                if (newContent != null)
                {
                    BoCached.Base.NewsContent.NewsContentDalFactory.AddNewsContentByNewsId(newsId, newContent);
                }
            }
            return newContent;
        }
    }
}
