﻿using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsChildVersionBo
    {
        public static List<NewsChildVersionEntity> GetByNewsId(long newsId,bool isGetContent)
        {
            try
            {
                var list = BoCached.Base.NewsChildVersion.NewsChildVersionDalFactory.GetNewsChildVersionByNewsId(newsId, isGetContent);
                if (list == null)
                {
                    list= NewsChildVersionDal.GetByNewsId(newsId, isGetContent);
                    if (list!=null && list.Count()>0) {
                        //add to redis
                        BoCached.Base.NewsChildVersion.NewsChildVersionDalFactory.AddNewsChildVersionByNewsId(newsId,list);
                    }
                }
                return list;
            }
            catch { return null; }
        }
    }
}
