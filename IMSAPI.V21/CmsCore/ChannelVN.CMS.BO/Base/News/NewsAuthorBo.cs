﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsAuthorBo
    {
        public static ErrorMapping.ErrorCodes Insert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId)
        {
            try
            {
                var inserted = NewsAuthorDal.Insert(newsAuthorEntity, ref newsAuthorId);
                if (inserted)
                {
                    newsAuthorEntity.Id = newsAuthorId;
                    //add redis
                    BoCached.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsAuthor(newsAuthorEntity);
                    //add es
                    BoSearch.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsAuthor(newsAuthorEntity);
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.ValidAuthorNameExist;
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes AssignListAuthorIntoNews(List<NewsByAuthorEntity> newsByAuthors)
        {
            try
            {
                var inserted = newsByAuthors.Aggregate(true, (current, newsByAuthor) => current && NewsAuthorDal.InsertToNewsByAuthor(newsByAuthor));
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes InsertToNewsByAuthor(NewsByAuthorEntity newsByAuthorEntity)
        {
            try
            {
                var inserted = NewsAuthorDal.InsertToNewsByAuthor(newsByAuthorEntity);
                if (inserted)
                {
                    //init redis
                    BoCached.Base.NewsAuthor.NewsAuthorDalFactory.InsertToNewsByAuthor(newsByAuthorEntity);                    
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Update(NewsAuthorEntity newsAuthorEntity)
        {
            try
            {
                var inserted = NewsAuthorDal.Update(newsAuthorEntity);
                if (inserted)
                {
                    BoCached.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsAuthor(newsAuthorEntity);
                    BoSearch.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsAuthor(newsAuthorEntity);
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = NewsAuthorDal.Delete(id);
                if (inserted)
                {
                    BoCached.Base.NewsAuthor.NewsAuthorDalFactory.DeleteNewsAuthorById(id);
                    BoSearch.Base.NewsAuthor.NewsAuthorDalFactory.DeleteNewsAuthor(id);
                }
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static NewsAuthorEntity GetById(int id)
        {
            try
            {
                var author = BoCached.Base.NewsAuthor.NewsAuthorDalFactory.GetNewsAuthorById(id);
                if (author == null)
                {
                    author= NewsAuthorDal.GetById(id);
                    if (author != null)
                    {
                        //add to redis
                        BoCached.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsAuthor(author);
                    }
                }
                return author;
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsAuthorEntity();
            }
        }

        public static List<NewsAuthorEntity> GetListByUserName(string username)
        {
            try
            {
                var data = BoCached.Base.NewsAuthor.NewsAuthorDalFactory.GetNewsAuthorByUsername(username);
                if(data==null || (data!=null && data.Count() < 0))
                {
                    data=NewsAuthorDal.GetListByUserName(username);                    
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsAuthorEntity>();
            }
        }

        public static NewsAuthorEntity GetByUserData(string userData, int authorType)
        {
            try
            {
                return NewsAuthorDal.GetByUserData(userData, authorType);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsAuthorEntity();
            }
        }

        public static List<NewsAuthorEntity> GetAll()
        {
            try
            {
                return NewsAuthorDal.GetAll();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsAuthorEntity>();
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisAllNewsAuthor(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = NewsAuthorDal.GetAll();
                //init redis
                BoCached.Base.NewsAuthor.NewsAuthorDalFactory.InitAllNewsAuthor(data);
            }, action);            
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitESAllNewsAuthor(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () => 1, (page) => {
                var data = NewsAuthorDal.GetAll();
                //init es
                BoSearch.Base.NewsAuthor.NewsAuthorDalFactory.InitAllNewsAuthor(data);
            }, action);            
        }

        public static List<NewsAuthorEntity> Search(string keyword)
        {
            try
            {
                var list =new List<NewsAuthorEntity>();
                var data=BoSearch.Base.NewsAuthor.NewsAuthorDalFactory.SearchNewsAuthor(keyword);
                if(data!=null && data.Count() > 0)
                {
                    list = BoCached.Base.NewsAuthor.NewsAuthorDalFactory.GetNewsAuthorByListId(data);
                    if (list == null || (list != null && list.Count() <= 0))
                    {
                        list = NewsAuthorDal.Search(keyword);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsAuthorEntity>();
            }
        }

        public static List<NewsByAuthorEntity> GetListInNews(long newsId)
        {
            try
            {
                //var list = BoCached.Base.NewsAuthor.NewsAuthorDalFactory.GetNewsByAuthorByNewsId(newsId);
                //if (list == null)
                //{
                //    list= NewsAuthorDal.GetListInNews(newsId);
                //    if(list!=null && list.Count() > 0)
                //    {
                //        BoCached.Base.NewsAuthor.NewsAuthorDalFactory.AddNewsByAuthorByNewsId(newsId, list);
                //    }
                //}

                var list = NewsAuthorDal.GetListInNews(newsId);
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsByAuthorEntity>();
            }
        }

        public static NewsAuthorEntity AutoCompleted(NewsAuthorEntity newsAuthorEntity)
        {
            var authorId = 0;
            try
            {
                return NewsAuthorDal.Insert(newsAuthorEntity, ref authorId) ? NewsAuthorDal.GetById(authorId) : new NewsAuthorEntity();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
