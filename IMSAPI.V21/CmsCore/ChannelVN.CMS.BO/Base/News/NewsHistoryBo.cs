﻿using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;
using System;
using ChannelVN.SocialNetwork.Entity;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsHistoryBo
    {
        public static bool InsertNews(long newsId, int newsStatus, string createdBy)
        {
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                var existsNews = NewsDal.GetNewsById(newsId);
                ActivityBo.NewsHistory(newsId, createdBy, newsStatus, existsNews == null ? "" : existsNews.Title, "Viết bài", "Viết bài");                
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                return NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = createdBy,
                    ReceivedBy = "",
                    ActionName = "Viết bài",
                    Description = "Viết bài"
                });
            }
            return true;
        }

        public static bool UpdateNews(long newsId, int newsStatus, string modifiedBy,string newsTitle)
        {
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                //var existsNews = NewsDal.GetNewsById(newsId);
                ActivityBo.NewsHistory(newsId, modifiedBy, newsStatus, newsTitle, "Sửa bài", "Sửa bài");
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                return NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = modifiedBy,
                    ReceivedBy = "",
                    ActionName = "Sửa bài",
                    Description = "Sửa bài"
                });
            }
            return true;
        }

        public static bool UpdateCheckError(long newsId, int newsStatus, string modifiedBy, string newsTitle)
        {
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                //var existsNews = NewsDal.GetNewsById(newsId);
                ActivityBo.NewsHistory(newsId, modifiedBy, newsStatus, newsTitle, "Soát lỗi", "Soát lỗi");
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                return NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = modifiedBy,
                    ReceivedBy = "",
                    ActionName = "Soát lỗi",
                    Description = "Soát lỗi"
                });
            }
            return true;
        }

        public static bool UpdateNewsIsOnHome(long newsId,bool isOnHome, string modifiedBy)
        {
            var existsNews = NewsDal.GetNewsById(newsId);
            var newsStatus = existsNews.Status;
            var messageLog = isOnHome ? "Set trạng thái hiển thị lên trang chủ" : "Set trạng thái không hiển thị lên trang chủ";
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {                
                ActivityBo.NewsHistory(newsId, modifiedBy, newsStatus, existsNews == null ? "" : existsNews.Title, messageLog, messageLog);
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                return NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = modifiedBy,
                    ReceivedBy = "",
                    ActionName = messageLog,
                    Description = messageLog
                });
            }
            return true;
        }

        public static bool UpdateNewsIsFocus(long newsId, bool isFocus, string modifiedBy)
        {
            var existsNews = NewsDal.GetNewsById(newsId);
            var newsStatus = existsNews.Status;
            var messageLog = isFocus ? "Set trạng thái hiển thị tiêu điểm trong danh sách" : "Set trạng thái không hiển thị tiêu điểm trong danh sách";
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                ActivityBo.NewsHistory(newsId, modifiedBy, newsStatus, existsNews == null ? "" : existsNews.Title, messageLog, messageLog);
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                return NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = modifiedBy,
                    ReceivedBy = "",
                    ActionName = messageLog,
                    Description = messageLog
                });
            }
            return true;
        }
        public static void InsertNewsPr(long newsId, int newsStatus, string createdBy)
        {
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                var existsNews = NewsDal.GetNewsById(newsId);
                ActivityBo.NewsHistory(newsId, createdBy, newsStatus, existsNews == null ? "" : existsNews.Title, "Viết bài", "Viết bài");
            }
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                NewsHistoryDal.InsertNewsHistory(new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newsStatus,
                    SentBy = createdBy,
                    ReceivedBy = "",
                    ActionName = "Viết bài",
                    Description = "Viết bài"
                });
            }
        }        

        public static void ChangeStatusFlow(long newsId, int currentStatus, int newStatus, string sentBy, string receivedBy, string newsTitle = "")
        {
            var actionName = "";

            #region current status
            var currentStatusMessage = "";
            switch (currentStatus)
            {
                case (int)NewsStatus.Temporary:
                    currentStatusMessage = "Lưu tạm";
                    break;
                case (int)NewsStatus.WaitForEdit:
                    currentStatusMessage = "Chờ biên tập";
                    break;
                case (int)NewsStatus.ReceivedForEdit:
                    currentStatusMessage = "Nhận biên tập";
                    break;
                case (int)NewsStatus.ReturnedToReporter:
                    currentStatusMessage = "Trả về cho Phóng viên";
                    break;
                case (int)NewsStatus.WaitForPublish:
                    currentStatusMessage = "Chờ duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReceivedForPublish:
                    currentStatusMessage = "Nhận duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReturnedToEditor:
                    currentStatusMessage = "Trả về cho Biên tập viên";
                    break;
                case (int)NewsStatus.Published:
                    currentStatusMessage = "Đã xuất bản";
                    break;
                case (int)NewsStatus.Unpublished:
                    currentStatusMessage = "Gỡ xuất bản";
                    break;
                case (int)NewsStatus.MovedToTrash:
                    currentStatusMessage = "Xóa tạm (lưu thùng rác)";
                    break;
                case (int)NewsStatus.ReturnToCooperator:
                    currentStatusMessage = "Trả về CTV";
                    break;
                case (int)NewsStatus.WaitForEditorialBoard:
                    currentStatusMessage = "Đợi tổng biên tập duyệt";
                    break;
                case (int)NewsStatus.ReceivedForEditorialBoard:
                    currentStatusMessage = "Tổng biên tập nhật duyệt";
                    break;
                case (int)NewsStatus.ReturnedToEditorialSecretary:
                    currentStatusMessage = "Ban biên tập trả thư ký";
                    break;
                default:
                    currentStatusMessage = "Không xác định";
                    break;
            }
            #endregion

            #region new status
            var newStatusMessage = "";
            switch (newStatus)
            {
                case (int)NewsStatus.Temporary:
                    newStatusMessage = "Lưu tạm";
                    actionName = "Lưu bài viết";
                    break;
                case (int)NewsStatus.WaitForEdit:
                    newStatusMessage = "Chờ biên tập";
                    actionName = "Chờ biên tập";
                    break;
                case (int)NewsStatus.ReceivedForEdit:
                    newStatusMessage = "Nhận biên tập";
                    actionName = "Nhận biên tập";
                    break;
                case (int)NewsStatus.ReturnedToReporter:
                    newStatusMessage = "Trả về cho Phóng viên";
                    actionName = "Trả về cho Phóng viên";
                    break;
                case (int)NewsStatus.WaitForPublish:
                    newStatusMessage = "Chờ duyệt xuất bản";
                    actionName = "Chờ duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReceivedForPublish:
                    newStatusMessage = "Nhận duyệt xuất bản";
                    actionName = "Nhận duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReturnedToEditor:
                    newStatusMessage = "Trả về cho Biên tập viên";
                    actionName = "Trả về cho Biên tập viên";
                    break;
                case (int)NewsStatus.Published:
                    newStatusMessage = "Xuất bản";
                    actionName = "Xuất bản";
                    break;
                case (int)NewsStatus.Unpublished:
                    newStatusMessage = "Gỡ xuất bản";
                    actionName = "Gỡ xuất bản";
                    break;
                case (int)NewsStatus.MovedToTrash:
                    newStatusMessage = "Xóa tạm (lưu thùng rác)";
                    actionName = "Xóa (Lưu thùng rác)";
                    break;
                case (int)NewsStatus.ReturnToCooperator:
                    newStatusMessage = "Trả lại CTV";
                    actionName = "Trả lại cho CTV";
                    break;

                case (int)NewsStatus.WaitForEditorialBoard:
                    newStatusMessage = "Đợi tổng biên tập duyệt";
                    actionName = "Đợi tổng biên tập duyệt";
                    break;
                case (int)NewsStatus.ReceivedForEditorialBoard:
                    newStatusMessage = "Tổng biên tập nhật duyệt";
                    actionName = "Tổng biên tập nhật duyệt";
                    break;
                case (int)NewsStatus.ReturnedToEditorialSecretary:
                    newStatusMessage = "Ban biên tập trả thư ký";
                    actionName = "Ban biên tập trả thư ký";
                    break;
                default:
                    newStatusMessage = "Không xác định";
                    actionName = "Không xác định";
                    break;
            }
            #endregion

            var actionDescription = string.Format("Chuyển trạng thái từ [{0}] sang [{1}]", currentStatusMessage, newStatusMessage);
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                // var existsNews = NewsDal.GetNewsById(newsId);
                string modifiedBy = sentBy;
                if (string.IsNullOrEmpty(modifiedBy)) modifiedBy = receivedBy;
                ActivityBo.NewsHistory(newsId, modifiedBy, newStatus, newsTitle, actionName, actionDescription);
            }
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable) {
                var newsHistory = new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newStatus,
                    SentBy = sentBy,
                    ReceivedBy = receivedBy,
                    ActionName = actionName,
                    Description = actionDescription
                };
                var result=NewsHistoryDal.InsertNewsHistory(newsHistory);                
                if (result)
                {
                    //add to redis
                    BoCached.Base.NewsHistory.NewsHistoryDalFactory.AddNewsHistory(newsHistory);
                }
            }
        }

        public static void ChangeStatusFlowWithActionName(long newsId,string actionName, int currentStatus, int newStatus, string sentBy, string receivedBy, string newsTitle = "")
        {            
            #region current status
            var currentStatusMessage = "";
            switch (currentStatus)
            {
                case (int)NewsStatus.Temporary:
                    currentStatusMessage = "Lưu tạm";
                    break;
                case (int)NewsStatus.WaitForEdit:
                    currentStatusMessage = "Chờ biên tập";
                    break;
                case (int)NewsStatus.ReceivedForEdit:
                    currentStatusMessage = "Nhận biên tập";
                    break;
                case (int)NewsStatus.ReturnedToReporter:
                    currentStatusMessage = "Trả về cho Phóng viên";
                    break;
                case (int)NewsStatus.WaitForPublish:
                    currentStatusMessage = "Chờ duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReceivedForPublish:
                    currentStatusMessage = "Nhận duyệt xuất bản";
                    break;
                case (int)NewsStatus.ReturnedToEditor:
                    currentStatusMessage = "Trả về cho Biên tập viên";
                    break;
                case (int)NewsStatus.Published:
                    currentStatusMessage = "Đã xuất bản";
                    break;
                case (int)NewsStatus.Unpublished:
                    currentStatusMessage = "Gỡ xuất bản";
                    break;
                case (int)NewsStatus.MovedToTrash:
                    currentStatusMessage = "Xóa tạm (lưu thùng rác)";
                    break;
                case (int)NewsStatus.ReturnToCooperator:
                    currentStatusMessage = "Trả về CTV";
                    break;
                case (int)NewsStatus.WaitForEditorialBoard:
                    currentStatusMessage = "Đợi tổng biên tập duyệt";
                    break;
                case (int)NewsStatus.ReceivedForEditorialBoard:
                    currentStatusMessage = "Tổng biên tập nhật duyệt";
                    break;
                case (int)NewsStatus.ReturnedToEditorialSecretary:
                    currentStatusMessage = "Ban biên tập trả thư ký";
                    break;
                case (int)NewsStatus.ReturnedToMyEditor:
                    currentStatusMessage = "Bài trả lại tôi";
                    break;
                default:
                    currentStatusMessage = "Không xác định";
                    break;
            }
            #endregion

            #region new status
            var newStatusMessage = "";
            switch (newStatus)
            {
                case (int)NewsStatus.Temporary:
                    newStatusMessage = "Lưu tạm";                   
                    break;
                case (int)NewsStatus.WaitForEdit:
                    newStatusMessage = "Chờ biên tập";                  
                    break;
                case (int)NewsStatus.ReceivedForEdit:
                    newStatusMessage = "Nhận biên tập";                   
                    break;
                case (int)NewsStatus.ReturnedToReporter:
                    newStatusMessage = "Trả về cho Phóng viên";                    
                    break;
                case (int)NewsStatus.WaitForPublish:
                    newStatusMessage = "Chờ duyệt xuất bản";                  
                    break;
                case (int)NewsStatus.ReceivedForPublish:
                    newStatusMessage = "Nhận duyệt xuất bản";                   
                    break;
                case (int)NewsStatus.ReturnedToEditor:
                    newStatusMessage = "Trả về cho Biên tập viên";                  
                    break;
                case (int)NewsStatus.Published:
                    newStatusMessage = "Xuất bản";                    
                    break;
                case (int)NewsStatus.Unpublished:
                    newStatusMessage = "Gỡ xuất bản";                    
                    break;
                case (int)NewsStatus.MovedToTrash:
                    newStatusMessage = "Xóa tạm (lưu thùng rác)";                    
                    break;
                case (int)NewsStatus.ReturnToCooperator:
                    newStatusMessage = "Trả lại CTV";                    
                    break;
                case (int)NewsStatus.WaitForEditorialBoard:
                    newStatusMessage = "Đợi tổng biên tập duyệt";                    
                    break;
                case (int)NewsStatus.ReceivedForEditorialBoard:
                    newStatusMessage = "Tổng biên tập nhật duyệt";                    
                    break;
                case (int)NewsStatus.ReturnedToEditorialSecretary:
                    newStatusMessage = "Ban biên tập trả thư ký";                    
                    break;
                case (int)NewsStatus.ReturnedToMyEditor:
                    currentStatusMessage = "Bài trả lại tôi";
                    break;
                default:
                    newStatusMessage = "Không xác định";
                    break;
            }
            #endregion

            var actionDescription = string.Format("Chuyển trạng thái từ [{0}] sang [{1}]", currentStatusMessage, newStatusMessage);
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            if (ApiNodeJsEnable)
            {
                // var existsNews = NewsDal.GetNewsById(newsId);
                string modifiedBy = sentBy;
                if (string.IsNullOrEmpty(modifiedBy)) modifiedBy = receivedBy;
                ActivityBo.NewsHistory(newsId, modifiedBy, newStatus, newsTitle, actionName, actionDescription);
            }
            if (HistoryNodeJs_Enable || !ApiNodeJsEnable)
            {
                var newsHistory = new NewsHistoryEntity()
                {
                    NewsId = newsId,
                    Status = newStatus,
                    SentBy = sentBy,
                    ReceivedBy = receivedBy,
                    ActionName = actionName,
                    Description = actionDescription
                };
                var result = NewsHistoryDal.InsertNewsHistory(newsHistory);
                if (result)
                {
                    //add to redis
                    BoCached.Base.NewsHistory.NewsHistoryDalFactory.AddNewsHistory(newsHistory);
                }
            }
        }

        public virtual List<NewsHistoryEntity> GetHistoryByNewsid(long newsId, DateTime from, DateTime to)
        {
            var ApiNodeJsEnable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "LogActionNodeJs_Enable"));
            var HistoryNodeJs_Enable = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "HistoryNodeJs_Enable"));
            if (!HistoryNodeJs_Enable & ApiNodeJsEnable)
            {
                int totalRows = 0;
                var data = ActivityBo.Search("", 1, 100, newsId, -1, (int)EnumActivityType.NewsHistory,"", from, to, ref totalRows);
                if (data == null || data.Count == 0)
                    return new List<NewsHistoryEntity>();
                else
                {
                    var history = new List<NewsHistoryEntity>();
                    foreach (var item in data)
                    {
                        history.Add(new NewsHistoryEntity
                        {
                            NewsId = item.ApplicationId,
                            ActionName = item.DestinationName,
                            CreatedDate = item.CreatedDate,
                            ReceivedBy = item.SourceName,
                            SentBy = item.SourceName,
                            Status = item.ActionTypeDetail,
                            Description = item.ActionText
                        });
                    }
                    return history;
                }
            }
            else {
                var data = BoCached.Base.NewsHistory.NewsHistoryDalFactory.GetByNewsId(newsId);
                if(data==null || (data!=null && data.Count <= 0))
                {
                    data= NewsHistoryDal.GetByNewsId(newsId);
                }
                return data;
            }
        }

        public virtual List<ActivityEntity> SearchLogs(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, string sourceId, DateTime dateFrom, DateTime dateTo, ref int totalRows) {
            return ActivityBo.Search("", pageIndex, pageSize, applicationId, actionTypeDetail, type, sourceId, DateTime.MinValue, DateTime.MinValue, ref totalRows);

        }
    }
}
