﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsExtensionBo
    {
        public static bool SetValue(long newsId, int type, string value)
        {
            try {
                var data = NewsExtensionDal.SetValue(newsId, type, value);
                if (data)
                {
                    //add redis newsExtension
                    BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtension(new NewsExtensionEntity
                    {
                        NewsId = newsId,
                        Type = type,
                        Value = value
                    });
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewsExtensionBo.SetValue: error => " + ex);
                return false;
            }
        }

        public static bool SendExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            var data = NewsExtensionDal.SendExpertInNewsExtension(newsExtension);
            if (data)
            {
                //add redis newsExtension
                BoCached.Base.NewsExtension.NewsExtensionDalFactory.InitAllNewsExtension(newsExtension);
            }
            return data;
        }
        public static bool ReSendExpertInNewsExtension(int expertId, int expireDate, string note)
        {
            var data = NewsExtensionDal.ReSendExpertInNewsExtension(expertId, expireDate, note);
            if (data)
            {
                //add redis newsExtension
                //BoCached.Base.NewsExtension.NewsExtensionDalFactory.InitAllNewsExtension(newsExtension);
            }
            return data;
        }
        public static bool ConfirmExpertInNewsExtension(NewsExtensionEntity newsExtension)
        {
            var data = NewsExtensionDal.ConfirmExpertInNewsExtension(newsExtension);
            if (data)
            {
                //add redis newsExtension
                BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtension(newsExtension);
            }
            return data;
        }

        public static bool ReturnExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {            
            var data = NewsExtensionDal.ReturnExpertInNewsExtension(newsExtension);
            if (data)
            {
                //add redis newsExtension
                BoCached.Base.NewsExtension.NewsExtensionDalFactory.InitAllNewsExtension(newsExtension);                
            }
            return data;
        }

        public static List<NewsSimpleExpertEntity> ListNewsByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionDal.ListNewsByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public static List<NewsSimpleExpertEntity> ListNewsNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionDal.ListNewsNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public static List<NewsSimpleExpertEntity> ListNewsAllByExpert(int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsExtensionDal.ListNewsAllByExpert(expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }

        public static bool DeleteByNewsId(long newsId)
        {
            return NewsExtensionDal.DeleteByNewsId(newsId);                       
        }

        public static List<NewsExtensionEntity> GetByNewsId(long newsId)
        {
            try
            {
                var list = BoCached.Base.NewsExtension.NewsExtensionDalFactory.GetNewsExtensionByNewsId(newsId);
                if (list == null || (list !=null && list.Count < 0))
                {
                    list= NewsExtensionDal.GetByNewsId(newsId);
                    if (list!=null && list.Count > 0) {
                        //add to redis
                        BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtensionByNewsId(newsId,list);
                    }
                }
                return list;
            }
            catch { return null; }
        }

        public static bool SetListCachedByNewsId(long newsId, List<NewsExtensionEntity> list)
        {
            try
            {
                return BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtensionByNewsId(newsId, list);
            }
            catch { return false; }
        }

        public static List<NewsExtensionEntity> ListNewsExtensionByIds(string ids, int type)
        {
            var listExtension = new List<NewsExtensionEntity>();
            var newsIds = new List<string>();
            newsIds.AddRange(ids.Split(','));

            if (newsIds.Count > 0)
            {
                if (type > 0)
                {
                    var listKey = newsIds.Select(s => { s = s + "_" + type; return s; }).ToList();
                    return BoCached.Base.NewsExtension.NewsExtensionDalFactory.GetNewsExtensionByListKey(listKey);
                }
                else
                {
                    //all newsextension
                    return NewsExtensionDal.GetNewsExtensionByListIds(string.Join(",", newsIds));
                }
            }
            return listExtension;
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsExtensionAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = NewsExtensionDal.InitESAllNewsExtension(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = NewsExtensionDal.InitESAllNewsExtension(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.NewsExtension.NewsExtensionDalFactory.InitAllNewsExtension(data);
            }, action);
        }
    }
}
