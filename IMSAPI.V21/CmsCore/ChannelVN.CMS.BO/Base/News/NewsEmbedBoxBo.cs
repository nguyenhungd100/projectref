﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsEmbedBoxBo
    {

        public static List<NewsEmbedBoxListEntity> GetListNewsEmbedBox(int type, int status)
        {
            var newsEmbedBox = new List<NewsEmbedBoxListEntity>();
            try
            {

                return NewsEmbedBoxDal.GetListNewsEmbedBox(type, status);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static ErrorMapping.ErrorCodes Insert(NewsEmbebBoxEntity newsEmbebBox)
        {
            try
            {
                if (null != newsEmbebBox)
                {
                    return NewsEmbedBoxDal.Insert(newsEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbebBoxDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listNewsId, int type)
        {
            try
            {
                if (null != listNewsId && type > 0)
                {
                    NewsEmbedBoxDal.Update(listNewsId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbebBoxDal.Update:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes UpdateStatus(string listOfBoxNewsId, int type, int status)
        {
            try
            {
                if (null != listOfBoxNewsId && status > 0)
                {
                    NewsEmbedBoxDal.UpdateStatus(listOfBoxNewsId, type, status);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbebBoxDal.UpdateStatus:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(long newsId, int type)
        {
            try
            {
                if (newsId > 0 && type > 0)
                {
                    NewsEmbedBoxDal.Delete(newsId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbebBoxDal.Delete:{0}", ex.Message));
            }

        }
    }
}
