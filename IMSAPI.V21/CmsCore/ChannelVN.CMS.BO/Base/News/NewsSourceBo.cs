﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsSourceBo
    {
        public virtual ErrorMapping.ErrorCodes Insert(NewsSourceEntity newsSourceEntity, ref int newsSourceId)
        {
            try
            {
                var inserted = NewsSourceDal.Insert(newsSourceEntity, ref newsSourceId);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes Update(NewsSourceEntity newsSourceEntity)
        {
            try
            {
                var inserted = NewsSourceDal.Update(newsSourceEntity);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual ErrorMapping.ErrorCodes Delete(int id)
        {
            try
            {
                var inserted = NewsSourceDal.Delete(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public virtual NewsSourceEntity GetById(int id)
        {
            try
            {
                return NewsSourceDal.GetById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsSourceEntity();
            }
        }
        public virtual List<NewsSourceEntity> GetAll(string keyword)
        {
            try
            {
                return NewsSourceDal.GetAll(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsSourceEntity>();
            }
        }
        public virtual List<NewsBySourceEntity> GetListInNews(long newsId)
        {
            try
            {
                //var list = BoCached.Base.NewsSource.NewsSourceDalFactory.GetNewsSourceByNewsId(newsId);
                //if (list == null)
                //{
                //    list= NewsSourceDal.GetListInNews(newsId);
                //    if (list != null && list.Count()>0)
                //    {
                //        BoCached.Base.NewsSource.NewsSourceDalFactory.AddNewsSourceByNewsId(newsId, list);
                //    }
                //}

                var list = NewsSourceDal.GetListInNews(newsId);
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsBySourceEntity>();
            }
        }
    }
}
