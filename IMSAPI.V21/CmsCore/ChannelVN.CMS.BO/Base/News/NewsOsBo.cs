﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsOsBo
    {
        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:55
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static NewsOsEntity GetNewsByNewsId(long id)
        {
            try
            {
                return NewsOsDal.GetDetailById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsOsEntity();
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:59
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="catId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<ListNewsOsEntity> GetListNews(string keyword, string catId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listNewsOs = new List<ListNewsOsEntity>();
                var list = NewsOsDal.GetListNews(keyword, catId, pageIndex, pageSize, ref totalRow);

                if (null != list && list.Count > 0)
                {
                    listNewsOs.AddRange(
                        list.Select(data => new ListNewsOsEntity()
                                                {
                                                    News_ID = data.News_ID.ToString(),
                                                    News_Image = data.News_Image,
                                                    News_Author = data.News_Author,
                                                    News_CreateDate = data.News_CreateDate,
                                                    News_Title = data.News_Title,
                                                    WordCount = data.WordCount

                                                }));
                }
                return listNewsOs;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ListNewsOsEntity>();
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:59
        /// </summary>
        /// <param name="id"></param>
        /// <param name="content"></param>
        /// <param name="avatar"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static long ReceiverNews(long id, string content, string avatar, string username)
        {
            try
            {
                // Đọc dữ liệu từ hệ thống ngoài
                var data = GetNewsByNewsId(id);
                var news = new NewsEntity()
                               {
                                   Avatar = avatar,
                                   AvatarDesc = data.News_ImageNote,
                                   Title = data.News_Title,
                                   SubTitle = data.News_Subtitle,
                                   Sapo = data.News_InitialContent,
                                   Body = content,
                                   CreatedBy = username,
                                   CreatedDate = DateTime.Now,
                                   NewsRelation = data.News_Relation,
                                   LastModifiedBy = username,
                                   LastModifiedDate = DateTime.Now,
                                   WordCount = data.WordCount,
                                   ViewCount = 0,
                                   ThreadId = 0,
                                   Source = data.News_Source,
                                   IsFocus = false,
                                   Type = (int)NewsType.EXT,
                                   NewsType = (int)NewsType.EXT,
                                   Note = "Bài nhận từ " + data.News_Source
                               };

                // Lưu dữ liệu vào bảng News
                long newNewsId = 0;
                var encryptNewsId = string.Empty;
                NewsBo.InsertNews(news, data.Cat_ID, string.Empty, string.Empty, string.Empty, string.Empty, username,
                                  ref newNewsId, ref encryptNewsId, null, 0,0, new List<NewsExtensionEntity>());
                news.Id = newNewsId;
                NewsBo.UpdateVersion(news, username);
                NewsBo.ReleaseFirstVersion(newNewsId, username);
                // Update lại trạng thái đã nhận cho biên tập viên
                UpdateStatusNewsOs(id);
                // Trả ra giá trị vừa lưu 

                return newNewsId;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0L;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:59
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateStatusNewsOs(long id)
        {
            try
            {
                var inserted = NewsOsDal.UpdateStatusNewsOs(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:59
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteById(long id)
        {
            try
            {
                var inserted = NewsOsDal.DeleteById(id);
                return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: ThanhTN
        /// LastModifiedDate: 2013-01-24 15:59
        /// </summary>
        /// <param name="listNewsId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes DeleteByListId(string listNewsId)
        {
            try
            {
                var ids = listNewsId.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var newsId in ids.Select(Utility.ConvertToLong).Where(newsId => newsId > 0))
                {
                    NewsOsDal.DeleteById(newsId);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
    }
}
