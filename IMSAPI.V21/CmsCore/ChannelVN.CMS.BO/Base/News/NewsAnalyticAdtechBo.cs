﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsAnalyticAdtechBo
    {
        public static ErrorMapping.ErrorCodes ResetAdtechViewCount()
        {
            try
            {
                return NewsAnalyticAdtechDal.ResetViewCount()
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("ResetAdtechViewCount() => {0}", ex));
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateAdtechViewCount(long newsId, int viewCount)
        {
            try
            {
                return NewsAnalyticAdtechDal.UpdateViewCount(newsId, viewCount)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("UpdateAdtechViewCount({0}, {1}) => {2}", newsId, viewCount, ex));
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes BatchUpdateAdtechViewCount(string listNewsId, string listViewCount)
        {
            try
            {
                return NewsAnalyticAdtechDal.BatchUpdateViewCount(listNewsId, listViewCount)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("BatchUpdateAdtechViewCount(\"{0}\", \"{1}\") => {2}", listNewsId, listViewCount, ex));
            }
            return ErrorMapping.ErrorCodes.Exception;
        }
        public static List<NewsWithAnalyticEntity> GetTopMostReadInHourByAdtechViewCount(int top)
        {
            try
            {
                return NewsAnalyticAdtechDal.GetTopMostReadInHour(top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("GetTopMostReadInHourByAdtechViewCount({0}) => {1}", top, ex));
                return new List<NewsWithAnalyticEntity>();
            }
        }
        public static List<NewsAnalyticEntity> GetLastUpdateAdtechViewCount(DateTime lastUpdateViewCount)
        {
            try
            {
                return NewsAnalyticAdtechDal.GetLastUpdateViewCount(lastUpdateViewCount);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("GetLastUpdateAdtechViewCount({0}) => {1}", lastUpdateViewCount, ex));
                return new List<NewsAnalyticEntity>();
            }
        }
        public static List<NewsAnalyticEntity> GetTopLastestAdtechViewCount(int top)
        {
            try
            {
                return NewsAnalyticAdtechDal.GetTopLastestViewCount(top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, string.Format("GetTopLastestAdtechViewCount({0}) => {1}", top, ex));
                return new List<NewsAnalyticEntity>();
            }
        }
    }
}
