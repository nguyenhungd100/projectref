﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsInZoneBo
    {
        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            return NewsInZoneDal.GetNewsInZoneByNewsId(newsId);
        }
        public static ErrorMapping.ErrorCodes InsertNewsInZone(NewsInZoneEntity newsInZone)
        {
            var data = NewsInZoneDal.Insert(newsInZone)
                        ? ErrorMapping.ErrorCodes.Success
                        : ErrorMapping.ErrorCodes.BusinessError;
            return data;
        }
    }
}
