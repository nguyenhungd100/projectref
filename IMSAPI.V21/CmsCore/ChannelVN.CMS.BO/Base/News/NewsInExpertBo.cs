﻿using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsInExpertBo
    {        
        public static ErrorMapping.ErrorCodes SendNewsInExpert(List<NewsInExpertEntity> newsExpert)
        {
            var result = NewsInExpertDal.SendNewsInExpert(newsExpert);
            if (result)
            {
                var obj = newsExpert.FirstOrDefault();
                ActivityBo.LogExpertAction(1, string.Join(",", newsExpert.Select(s=>s.NewsId)), obj.Note, obj.AccountName);

                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReSendNewsInExpert(string accountName, int expertId, int expireDate, string note, long newsId)
        {
            var result = NewsInExpertDal.ReSendNewsInExpert(expertId, expireDate, note, newsId);
            if (result)
            {                
                ActivityBo.LogExpertAction(2, newsId.ToString(), note + " expertId: "+ expertId, accountName);

                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;            
        }
        public static ErrorMapping.ErrorCodes RecoverNewsInExpert(string accountName, string listNewsId)
        {
            var result = NewsInExpertDal.RecoverNewsInExpert(listNewsId);
            if (result)
            {
                ActivityBo.LogExpertAction(3, listNewsId, "", accountName);

                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ConfirmNewsInExpert(NewsInExpertEntity newsExpert)
        {
            var result = NewsInExpertDal.ConfirmNewsInExpert(newsExpert);
            if (result)
            {                
                ActivityBo.LogExpertAction(4, newsExpert.NewsId.ToString(), newsExpert.Note, newsExpert.AccountName+"_expertid_"+ newsExpert.ExpertId);

                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;            
        }
        public static ErrorMapping.ErrorCodes ReturnNewsInExpert(NewsInExpertEntity newsExpert)
        {            
            var result = NewsInExpertDal.ReturnNewsInExpert(newsExpert);
            if (result)
            {
                ActivityBo.LogExpertAction(5, newsExpert.NewsId.ToString(), newsExpert.Note, newsExpert.AccountName + "_expertid_" + newsExpert.ExpertId);

                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;           
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertDal.ListNewsInExpertByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertDal.ListNewsInExpertNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertAllByExpert(int expertId, int topicId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsInExpertDal.ListNewsInExpertAllByExpert(expertId, topicId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsInExpertEntity> ListNewsExpertByIds(string ids)
        {
            return NewsInExpertDal.GetNewsExpertByIds(ids);
        }
    }
}
