﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsAnalyticBo
    {
        public static List<NewsAnalyticEntity> GetLastUpdateViewCount(DateTime lastUpdateViewCount)
        {
            try
            {
                return NewsAnalyticDal.GetLastUpdateViewCount(lastUpdateViewCount);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticEntity>();
            }
        }
        public static List<NewsAnalyticEntity> GetTopLastestNews(int top)
        {
            try
            {
                return NewsAnalyticDal.GetTopLastestNews(top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticEntity>();
            }
        }

        public static List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top)
        {
            try
            {
                return NewsAnalyticDal.GetTopHighestViewCountByZone(zone, hour, top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticV3Entity>();
            }
        }

        public static List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top)
        {
            try
            {
                return NewsAnalyticDal.GetTopHighestCommentCountByZone(zone, hour, top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticV3Entity>();
            }
        }
    }
}
