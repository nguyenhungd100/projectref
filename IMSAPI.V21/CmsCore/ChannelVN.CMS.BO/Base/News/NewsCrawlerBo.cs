﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsCrawlerBo
    {
        public static NewsCrawlerEntity GetNewsByNewsId(int id)
        {
            return NewsCrawlerDal.GetDetailById(id);
        }

        public static List<NewsCrawlerEntity> GetListNews(int catId, int newsAngentId,int hot, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsCrawlerDal.GetListNewsCrawler(catId, newsAngentId, hot, pageIndex, pageSize, ref totalRow);

        }



        //Nhận bài từ crawler vào hệ thống
        public static long ReceiverNews(int id, string content, string avatar, string zoneId, string username)
        {
            // Đọc dữ liệu từ hệ thống ngoài
            var data = GetNewsByNewsId(id);
            var news = new NewsEntity()
            {
                Avatar = avatar,
                AvatarDesc = "",
                Title = data.Title,
                Sapo = data.Sapo,
                Body = content,
                CreatedBy = username,
                CreatedDate = DateTime.Now,
                NewsRelation = "",
                LastModifiedBy = username,
                LastModifiedDate = DateTime.Now,
                DistributionDate = DateTime.Now,
                WordCount = 0,
                ViewCount = 0,
                ThreadId = 0,
                IsFocus = false,
                Type = (int)NewsType.EXT,
                NewsType = (int)NewsType.EXT
            };

            // Lưu dữ liệu vào bảng News
            long newNewsId = 0; 
            var encryptNewsId = string.Empty;
            NewsBo.InsertNews(news, ConvertZoneId(data.CatID), string.Empty, string.Empty, string.Empty, string.Empty, username, ref newNewsId, ref encryptNewsId, null, 0,0, new List<NewsExtensionEntity>());
            if (newNewsId>0)
            {
                // Update lại trạng thái đã nhận cho biên tập viên
                UpdateIsMoveCms(id);
            }
            // Trả ra giá trị vừa lưu 
            if (newNewsId <= 0) return 0;
            //var dataNews = NewsBo.GetNewsForEditByNewsId(newNewsId, username);
            return newNewsId;
        }

        public static ErrorMapping.ErrorCodes InsertSite(string siteLink, string siteName) 
        {
            var inserted = NewsCrawlerDal.InsertSite(siteLink,siteName);
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes InvisibleSite(int id)  
        {
            var inserted = NewsCrawlerDal.InvisibleSite(id);
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateIsMoveCms(int id)
        {
            var inserted = NewsCrawlerDal.UpdateIsMoveCms(id);
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes DeleteById(int id)
        {
            var inserted = NewsCrawlerDal.DeleteById(id);
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes DeleteByListId(string listNewsId)
        {
            try
            {
                var ids = listNewsId.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var newsId in ids.Select(id => Utility.ConvertToInt(id)).Where(newsId => newsId > 0))
                {
                    NewsCrawlerDal.DeleteById(newsId);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<CategoryCrawlerEntity>GetListCategoryCrawler()
        {
            return NewsCrawlerDal.GetListCategoryCrawler();

        }
        public static List<SiteCrawlerEntity> GetListSiteCrawler()
        {
            return NewsCrawlerDal.GetListSiteCrawler();

        } 


      private static int ConvertZoneId (int id) 
      {

            var zoneId = 0;
            switch (id) {
                // Thoi trang         
                case 11:
                    zoneId = 10017;
                    break;
                // Thể thao   
                case 6:
                    zoneId = 10002;
                    break;
                // Xã hội   
                case 1:
                    zoneId = 10009;
                    break;
                // Giải trí    
                case 7:
                    zoneId = 10001;
                    break;
                // Chuyện lạ  
                case 10:
                    zoneId = 10010;
                    break;
                // Tình yêu giới tính  
                case 12:
                    zoneId = 10040;
                    break;
                // Pháp luật  
                case 3:
                    zoneId = 10015;
                    break;
                // Sức khỏe   
                case 8:
                    zoneId = 10014;
                    break;
                // Kinh doanh  
                case 4:
                    zoneId = 10019;
                    break;
                // Công nghệ  
                case 5:
                    zoneId = 10020;
                    break;
                 // Thế giới
                case  2 :
                    zoneId = 10030;
                    break;
                    // Đẹp
                case  9:
                    zoneId = 10017;
                    break;

            }
            return zoneId;
        }
    }
}
