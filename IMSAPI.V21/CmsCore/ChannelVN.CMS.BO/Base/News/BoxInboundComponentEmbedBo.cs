﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class BoxInboundComponentEmbedBo
    {
        public static ErrorMapping.ErrorCodes InsertBoxInboundComponentEmbed(BoxInboundComponentEmbedEntity item, ref int id)
        {
            try
            {
                if (null == item)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.Insert(item, ref id)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.InsertBoxInboundComponentEmbed:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxInboundComponentEmbed(BoxInboundComponentEmbedEntity item)
        {
            try
            {
                if (null == item || item.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.Update(item)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.UpdateBoxInboundComponentEmbed:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxInboundComponentEmbedPriority(string listOfBoxInboundComponentEmbedId)
        {
            try
            {
                if (string.IsNullOrEmpty(listOfBoxInboundComponentEmbedId))
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.UpdateSortOrder(listOfBoxInboundComponentEmbedId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.UpdateBoxInboundComponentEmbedPriority:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes DeleteBoxInboundComponentEmbed(int boxBannerId)
        {
            try
            {
                if (boxBannerId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.Delete(boxBannerId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.DeleteBoxInboundComponentEmbed:{0}", ex.Message));
            }
        }

        public static BoxInboundComponentEmbedEntity GetBoxInboundComponentEmbedById(int id)
        {
            var data= BoxInboundComponentEmbedDal.GetById(id);            
            return data;
        }

        public static List<BoxInboundComponentEmbedEntity> GetListBoxInboundComponentEmbed(long newsId, int position, int status, int type)
        {
            return BoxInboundComponentEmbedDal.GetListBoxInboundComponentEmbed(newsId, position, status, type);
        }

        #region BoxInboundTemplate
        public static ErrorMapping.ErrorCodes InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            try
            {
                if (null == item)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var data = BoxInboundComponentEmbedDal.InsertBoxInboundTemplate(item, ref id);
                if (data == false && id <= 0)
                    return ErrorMapping.ErrorCodes.DuplicateError;

                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.InsertBoxInboundTemplate:{0}", ex.Message));
            }
        }

        public static ErrorMapping.ErrorCodes UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            try
            {
                if (null == item || item.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.UpdateBoxInboundTemplate(item)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.UpdateBoxInboundTemplate:{0}", ex.Message));
            }
        }        

        public static ErrorMapping.ErrorCodes DeleteBoxInboundTemplate(int boxId)
        {
            try
            {
                if (boxId <= 0)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                return BoxInboundComponentEmbedDal.DeleteBoxInboundTemplate(boxId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxInboundComponentEmbedBo.DeleteBoxInboundTemplate:{0}", ex.Message));
            }
        }

        public static BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            var data = BoxInboundComponentEmbedDal.GetBoxInboundTemplateById(id);
            return data;
        }

        public static List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return BoxInboundComponentEmbedDal.GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);
        }
        #endregion
    }
}
