﻿using System;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.WcfExtensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsPrBo
    {
        public static NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            try
            {
                //var list = BoCached.Base.NewsPr.NewsPrDalFactory.GetNewsPrByNewsId(newsId);
                //if (list == null)
                //{
                //    list = NewsPrDal.GetNewsPrByNewsId(newsId);
                //    if(list!=null)
                //    {
                //        BoCached.Base.NewsPr.NewsPrDalFactory.AddNewsPrByNewsId(newsId,list);
                //    }
                //}
                //return list;

                return NewsPrDal.GetNewsPrByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }
        public static NewsPrEntity GetNewsPrByContentId(long contentId)
        {
            try
            {
                return NewsPrDal.GetNewsPrByContentId(contentId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, ref long newsId)
        {
            try
            {
                if (newsPrInfo.ContentId == 0 || newsPrInfo.DistributionId == 0)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }

                var currentNewsPr = NewsPrDal.GetNewsPrByContentId(newsPrInfo.ContentId);
                if (currentNewsPr != null)
                {
                    newsPrInfo.Id = currentNewsPr.Id;
                    newsPrInfo.NewsId = currentNewsPr.NewsId;
                    var currentNews = NewsDal.GetNewsById(currentNewsPr.NewsId);
                    if (currentNews != null)
                    {
                        currentNews.ZoneId = newsInfo.ZoneId;
                        currentNews.Title = newsInfo.Title;
                        currentNews.SubTitle = newsInfo.SubTitle;
                        currentNews.Sapo = newsInfo.Sapo;
                        currentNews.Body = newsInfo.Body;
                        currentNews.Avatar = newsInfo.Avatar;                        
                        currentNews.Avatar5 = newsInfo.Avatar5;
                        currentNews.AvatarDesc = newsInfo.AvatarDesc;
                        currentNews.Author = newsInfo.Author;
                        currentNews.Source = newsInfo.Source;
                        currentNews.IsFocus = newsInfo.IsFocus;
                        currentNews.CreatedBy = newsInfo.CreatedBy;
                        currentNews.EditedBy = newsInfo.EditedBy;
                        currentNews.PublishedBy = newsInfo.PublishedBy;
                        currentNews.ApprovedBy = newsInfo.ApprovedBy;
                        currentNews.ApprovedDate = newsInfo.ApprovedDate;
                        currentNews.WordCount = newsInfo.WordCount;
                        currentNews.Url = newsInfo.Url;
                        currentNews.IsPr = newsInfo.IsPr;
                        currentNews.AdStore = newsInfo.AdStore;
                        currentNews.AdStoreUrl = newsInfo.AdStoreUrl;
                        currentNews.PrBookingNumber = newsInfo.PrBookingNumber;
                        currentNews.PrPosition = newsInfo.PrPosition;
                        currentNews.LocationType = newsInfo.LocationType;
                        currentNews.NewsRelation = newsInfo.NewsRelation;
                        currentNews.ZoneId = newsInfo.ZoneId;
                        currentNews.TagItem = newsInfo.TagItem;
                        currentNews.DisplayStyle = newsInfo.DisplayStyle;
                        currentNews.Price = newsInfo.Price;
                        newsInfo = currentNews;
                    }
                }

                if (newsInfo.Id <= 0)
                {
                    newsInfo.Id = NewsBo.SetPrimaryId();
                    newsPrInfo.NewsId = newsInfo.Id;
                }
                var primaryZone = ZoneDal.GetZoneById(newsInfo.ZoneId);

                newsInfo.Url = NewsBo.BuildLinkUrl(newsInfo.Id, newsInfo.Type, primaryZone.ShortUrl, newsInfo.Title, newsInfo.ZoneId);

                if (newsInfo.AdStore)
                {
                    newsInfo.AdStoreUrl = NewsBo.BuildLinkUrlAdStore(newsInfo.Id, primaryZone.ShortUrl, primaryZone.Id, newsInfo.Title);
                }

                if (NewsPrDal.RecieveNewsPrIntoCms(newsInfo, newsPrInfo, listNewsRelationId))
                {
                    newsId = newsInfo.Id;
                    try
                    {
                        //add es redis bai pr
                        BoSearch.Base.News.NewsDalFactory.AddNews(newsInfo);
                        BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(newsInfo.Id, newsInfo.CreatedBy, newsInfo.Title, newsInfo.Status);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes RecieveNewsPrV2IntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, List<NewsExtensionEntity> newsExtensions, ref long newsId)
        {
            try
            {
                if (newsPrInfo.ContentId == 0 || newsPrInfo.DistributionId < 0)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }

                var currentNewsPr = NewsPrDal.GetNewsPrByContentId(newsPrInfo.ContentId);
                if (currentNewsPr != null)
                {
                    newsPrInfo.Id = currentNewsPr.Id;
                    newsPrInfo.NewsId = currentNewsPr.NewsId;
                    var currentNews = NewsDal.GetNewsById(currentNewsPr.NewsId);
                    if (currentNews != null)
                    {
                        currentNews.ZoneId = newsInfo.ZoneId;
                        currentNews.Title = newsInfo.Title;
                        currentNews.SubTitle = newsInfo.SubTitle;
                        currentNews.Sapo = newsInfo.Sapo;
                        currentNews.Body = newsInfo.Body;
                        currentNews.Avatar = newsInfo.Avatar;
                        currentNews.Avatar = newsInfo.Avatar2;
                        currentNews.Avatar = newsInfo.Avatar3;
                        currentNews.Avatar = newsInfo.Avatar4;
                        currentNews.Avatar5 = newsInfo.Avatar5;
                        currentNews.AvatarDesc = newsInfo.AvatarDesc;
                        currentNews.Author = newsInfo.Author;
                        currentNews.Source = newsInfo.Source;
                        currentNews.IsFocus = newsInfo.IsFocus;
                        currentNews.CreatedBy = newsInfo.CreatedBy;
                        currentNews.EditedBy = newsInfo.EditedBy;
                        currentNews.PublishedBy = newsInfo.PublishedBy;
                        currentNews.ApprovedBy = newsInfo.ApprovedBy;
                        currentNews.ApprovedDate = newsInfo.ApprovedDate;
                        currentNews.WordCount = newsInfo.WordCount;
                        currentNews.Url = newsInfo.Url;
                        currentNews.IsPr = newsInfo.IsPr;
                        currentNews.AdStore = newsInfo.AdStore;
                        currentNews.AdStoreUrl = newsInfo.AdStoreUrl;
                        currentNews.PrBookingNumber = newsInfo.PrBookingNumber;
                        currentNews.PrPosition = newsInfo.PrPosition;
                        currentNews.LocationType = newsInfo.LocationType;
                        currentNews.NewsRelation = newsInfo.NewsRelation;
                        currentNews.ZoneId = newsInfo.ZoneId;
                        currentNews.TagItem = newsInfo.TagItem;
                        currentNews.DisplayStyle = newsInfo.DisplayStyle;
                        currentNews.Price = newsInfo.Price;
                        newsInfo = currentNews;
                    }
                }

                if (newsInfo.Id <= 0)
                {
                    newsInfo.Id = NewsBo.SetPrimaryId();
                    newsPrInfo.NewsId = newsInfo.Id;
                }
                var primaryZone = ZoneDal.GetZoneById(newsInfo.ZoneId);

                newsInfo.Url = NewsBo.BuildLinkUrl(newsInfo.Id, newsInfo.Type, primaryZone.ShortUrl, newsInfo.Title, newsInfo.ZoneId);

                if (newsInfo.AdStore)
                {
                    newsInfo.AdStoreUrl = NewsBo.BuildLinkUrlAdStore(newsInfo.Id, primaryZone.ShortUrl, primaryZone.Id, newsInfo.Title);
                }

                if (NewsPrDal.RecieveNewsPrV2IntoCms(newsInfo, newsPrInfo, listNewsRelationId))
                {
                    newsId = newsInfo.Id;
                    try
                    {
                        //add es redis bai pr
                        BoSearch.Base.News.NewsDalFactory.AddNews(newsInfo);
                        BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);

                        //save newsextension
                        TaskInsertNewsExtensionSuccessAsync(newsInfo.Id, newsExtensions);

                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(newsInfo.Id, newsInfo.CreatedBy, newsInfo.Title, newsInfo.Status);
                        
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        private static void TaskInsertNewsExtensionSuccessAsync(long newNewsId, List<NewsExtensionEntity> newsExtensions)
        {            
            var task = Task.Run(() =>
            {
                try
                {                    
                    foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                    {
                        newsExtension.NewsId = newNewsId;                        
                        NewsExtensionBo.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                    }                    
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "InsertNews => TaskInsertNewsSuccessAsync() error => " + ex);
                }

            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsPrInfo(NewsPrEntity newsPrInfo)
        {
            try
            {
                if (newsPrInfo.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
                return NewsPrDal.UpdateNewsPrInfo(newsPrInfo)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsPrV2Info(NewsPrEntity newsPrInfo, string message)
        {
            try
            {
                var result = NewsPrDal.UpdateNewsPrV2Info(newsPrInfo);
                if (result)
                {
                    //log update NewsPR
                    ActivityBo.LogUpdatePrStatus(WcfMessageHeader.Current.ClientUsername, message);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateNewsPrV2Info_2(NewsPrEntity newsPrInfo, string message)
        {
            try
            {
                var result = NewsPrDal.UpdateNewsPrV2Info_2(newsPrInfo);
                if (result)
                {
                    //log update NewsPR
                    ActivityBo.LogUpdatePrStatus(WcfMessageHeader.Current.ClientUsername, message);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static NewsPrEntity GetNewsPrByBookingId(long bookingId)
        {
            try
            {
                return NewsPrDal.GetNewsPrByBookingId(bookingId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return null;
            }
        }

        public static ErrorMapping.ErrorCodes RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId, ref long newsId)
        {
            try
            {
                var currentNewsPr = NewsPrDal.GetNewsPrByBookingId(newsPrInfo.BookingId);
                if (currentNewsPr.NewsId > 0)
                {
                    newsPrInfo.Id = currentNewsPr.Id;

                    var currentNews = NewsDal.GetNewsById(currentNewsPr.NewsId);
                    if (currentNews != null)
                    {
                        currentNews.ZoneId = newsInfo.ZoneId;
                        currentNews.Title = newsInfo.Title;
                        currentNews.SubTitle = newsInfo.SubTitle;
                        currentNews.Sapo = newsInfo.Sapo;
                        currentNews.Body = newsInfo.Body;
                        currentNews.Avatar = newsInfo.Avatar;
                        currentNews.AvatarDesc = newsInfo.AvatarDesc;
                        currentNews.Author = newsInfo.Author;
                        currentNews.Source = newsInfo.Source;
                        currentNews.IsFocus = newsInfo.IsFocus;
                        currentNews.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                        currentNews.WordCount = newsInfo.WordCount;
                        currentNews.Url = newsInfo.Url;
                        currentNews.IsPr = newsInfo.IsPr;
                        currentNews.AdStore = newsInfo.AdStore;
                        currentNews.AdStoreUrl = newsInfo.AdStoreUrl;
                        currentNews.PrBookingNumber = newsInfo.PrBookingNumber;
                        currentNews.PrPosition = newsInfo.PrPosition;
                        currentNews.LocationType = newsInfo.LocationType;
                        currentNews.NewsRelation = newsInfo.NewsRelation;
                        currentNews.ZoneId = newsInfo.ZoneId;

                        newsInfo = currentNews;
                    }
                }

                if (newsInfo.Id <= 0)
                {
                    newsInfo.Id = NewsBo.SetPrimaryId();
                    newsPrInfo.NewsId = newsInfo.Id;
                }
                //var primaryZone = ZoneDal.GetZoneById(newsInfo.ZoneId);

                //newsInfo.Url = NewsBo.BuildLinkUrl(newsInfo.Id, newsInfo.Type, primaryZone.ShortUrl, newsInfo.Title);

                //if (newsInfo.AdStore)
                //{
                //    newsInfo.AdStoreUrl = NewsBo.BuildLinkUrlAdStore(newsInfo.Id, primaryZone.ShortUrl, primaryZone.Id, newsInfo.Title);
                //}

                if (NewsPrDal.RecieveNewsPrRetailIntoCms(newsInfo, newsPrInfo, listNewsRelationId))
                {
                    newsId = newsInfo.Id;
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static List<NewsPrCurrentDateEntity> GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows)
        {
            try
            {
                return NewsPrDal.GetListNewsPrCurrentDate(Date, UnPublish, PageIndex, PageSize, ref TotalRows);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsPrCurrentDateEntity>();
            }
        }

        #region ViewPlus
        public static List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize,int bidType, ref int TotalRow)
        {
            try
            {
                return NewsPrDal.ViewPlus_GetList(ZoneId, DateFrom, DateTo, PageIndex, PageSize, bidType ,ref TotalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsViewPlusListEntity>();
            }
        }
        public static ErrorMapping.ErrorCodes ViewPlus_Insert(ViewPlusEntity viewPlus)
        {
            try
            {
                if (viewPlus.NewsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
                return NewsPrDal.ViewPlus_Insert(viewPlus)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes ViewPlus_Delete(string Ids)
        {
            try
            {
                if (string.IsNullOrEmpty(Ids))
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
                return NewsPrDal.ViewPlus_Delete(Ids)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        #endregion

        #region ZoneViewPlus
        public static List<ZoneViewPlusEntity> GetAllZoneViewPlus_by_TypeId_ZoneId(int typeId, int zoneId)
        {
            try
            {
                return NewsPrDal.GetAllZoneViewPlus_by_TypeId_ZoneId(typeId, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<ZoneViewPlusEntity>();
            }
}
        #endregion
    }
}
