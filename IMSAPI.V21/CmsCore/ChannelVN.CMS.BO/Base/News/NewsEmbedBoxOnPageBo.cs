﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsEmbedBoxOnPageBo
    {
        public static List<NewsEmbedBoxOnPageListEntity> GetListNewsEmbedBoxOnPage(int zoneId, int type)
        {
            var newsEmbedBox = new List<NewsEmbedBoxOnPageListEntity>();
            try
            {

                return NewsEmbedBoxOnPageDal.GetListNewsEmbedBoxOnPage(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static ErrorMapping.ErrorCodes Insert(NewsEmbedBoxOnPageEntity newsEmbebBox)
        {
            try
            {
                if (null != newsEmbebBox)
                {
                    return NewsEmbedBoxOnPageDal.Insert(newsEmbebBox) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.Insert:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Update(string listNewsId, int zoneId, int type)
        {
            try
            {
                if (null != listNewsId)
                {
                    NewsEmbedBoxOnPageDal.Update(listNewsId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.Update:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes Delete(long newsId, int zoneId,int type)
        {
            try
            {
                if (newsId > 0 && zoneId > 0)
                {
                    NewsEmbedBoxOnPageDal.Delete(newsId, zoneId,type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsEmbedBoxOnPageDal.Delete:{0}", ex.Message));
            }

        }

    }
}
