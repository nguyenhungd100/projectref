﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsAnalyticV2Bo
    {
        public static List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCountId)
        {
            try
            {
                return NewsAnalyticV2Dal.GetLastUpdateViewCount(lastUpdateViewCountId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticV2Entity>();
            }
        }
        public static List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord)
        {
            try
            {
                return NewsAnalyticV2Dal.GetAllViewFromDateToNow(fromDate, maxRecord);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticV2Entity>();
            }
        }
        public static List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId)
        {
            try
            {
                return NewsAnalyticV2Dal.GetTopMostReadInHour(top, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return new List<NewsAnalyticV2Entity>();
            }
        }
    }
}
