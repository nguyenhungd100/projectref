﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.BO.Base.Account;
using ChannelVN.CMS.BO.Base.Photo;
using ChannelVN.CMS.BO.Base.Security;
using ChannelVN.CMS.BO.Base.Tag;
using ChannelVN.CMS.BO.Base.Topic;
using ChannelVN.CMS.BO.Base.Vote;
using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BoCached.CacheObjects;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.DAL.Base.Tag;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.Expert.Bo;
using ChannelVN.SocialNetwork.BO;
using ChannelVN.CMS.BO.Base.Video;
using ChannelVN.CMS.BO.Base.NewsPosition;
using ChannelVN.CMS.BO.Base.AutoDiscussion;
using ChannelVN.WcfExtensions;
using static ChannelVN.CMS.Entity.ErrorCode.ErrorMapping;
using ChannelVN.BrandContent.Bo;
using ChannelVN.CMS.BoSearch.Entity;
using System.Threading.Tasks;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Net;
using System.IO;
using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Entity.Base.Nodejs;
using ChannelVN.Kenh14.Bo;
using ChannelVN.Kenh14.Entity;
using ChannelVN.CMS.BO.Base.Views;
using System.Globalization;
using System.Web;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.MessageQueue;
using ChannelVN.CMS.MessageQueue.Constants;
using System.Diagnostics;
using ChannelVN.CMS.BoCached.Entity.Init;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsBo
    {
        // Auto increment NewsId by DateTime format 'Gets from Constants.NEWS_FORMAT_ID)'
        public static long SetPrimaryId()
        {
            return long.Parse(DateTime.Now.ToString(BoConstants.NEWS_FORMAT_ID));
        }

        #region NewsExtension

        public static NewsExtensionEntity GetNewsExtensionValue(long newsId, EnumNewsExtensionType type)
        {
            return NewsExtensionDal.GetValue(newsId, (int)type);
        }

        public static int GetNewsExtensionMaxValue(long newsId, int type)
        {
            return NewsExtensionDal.GetMaxValue(newsId, (int)type);
        }
        public static ErrorMapping.ErrorCodes SetNewsExtensionValue(long newsId, EnumNewsExtensionType type, string value)
        {
            //var result = NewsExtensionDal.SetValue(newsId, (int)type, value);
            var result = NewsExtensionBo.SetValue(newsId, (int)type, value);
            if (result)
            {                
                return ErrorMapping.ErrorCodes.Success;
            }                       
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes SendExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            var result = NewsExtensionBo.SendExpertInNewsExtension(newsExtension);
            if (result)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReSendExpertInNewsExtension(int expertId, int expireDate, string note)
        {
            var result = NewsExtensionBo.ReSendExpertInNewsExtension(expertId, expireDate, note);
            if (result)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ConfirmExpertInNewsExtension(NewsExtensionEntity newsExtension)
        {
            var result = NewsExtensionBo.ConfirmExpertInNewsExtension(newsExtension);
            if (result)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }
        public static ErrorMapping.ErrorCodes ReturnExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            var result = NewsExtensionBo.ReturnExpertInNewsExtension(newsExtension);
            if (result)
            {
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            return NewsExtensionDal.GetByNewsId(newsId);
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByListNewsId(string listNewsId)
        {
            return NewsExtensionDal.GetByListNewsId(listNewsId);
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType type, string value)
        {
            return NewsExtensionDal.GetByTypeAndVaue((int)type, value);
        }

        #endregion        

        #region Tag news

        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsId(long newsId)
        {

            var listTagNews = BoCached.Base.Tag.TagDalFactory.GetTagNewsByNewsId(newsId);
            if (listTagNews == null || (listTagNews != null && listTagNews.Count == 0))
            {
                listTagNews = TagNewsDal.GetTagNewsByNewsId(newsId);
                if (listTagNews != null)
                {
                    BoCached.Base.Tag.TagDalFactory.AddTagNewsByNewsId(newsId, listTagNews);
                }
            }
            return listTagNews;
        }

        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByListOfTagId(string tagIds)
        {
            var listTagNews = TagNewsDal.GetTagNewsWithTagInfoByListOfTagId(tagIds);
            return listTagNews.Select(tagNews => new TagNewsWithTagInfoEntity
            {
                TagId = tagNews.TagId,
                NewsId = tagNews.NewsId,
                TagMode = tagNews.TagMode,
                Priority = tagNews.Priority,
                TagProperty = tagNews.TagProperty,
                Name = "",
                Url = ""
            }).ToList();
        }

        #endregion

        #region Update news

        private static readonly IDictionary<string, List<string>> DictsNewsTitleUpdating = new Dictionary<string, List<string>>();

        #region Check news title updating

        private static bool IsUpdatingThisNews(string newsTitle)
        {
            if (string.IsNullOrEmpty(newsTitle)) return false;

            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

            return (DictsNewsTitleUpdating.ContainsKey(accountName) && DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle));
        }
        private static void UpdatingThisNews(string newsTitle)
        {
            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                if (!DictsNewsTitleUpdating[accountName].Exists(item => item == newsTitle))
                {
                    DictsNewsTitleUpdating[accountName].Add(newsTitle);
                }
            }
            else
            {
                DictsNewsTitleUpdating.Add(accountName, new List<string> { newsTitle });
            }
        }
        private static void NewsUpdated(string newsTitle)
        {
            var accountName = WcfMessageHeader.Current.ClientUsername;
            if (DictsNewsTitleUpdating.ContainsKey(accountName))
            {
                var currentList = DictsNewsTitleUpdating[accountName];
                if (currentList != null)
                {
                    var index = currentList.FindIndex(item => item == newsTitle);
                    while (index >= 0)
                    {
                        currentList.RemoveAt(index);
                        index = currentList.FindIndex(item => item == newsTitle);
                    }
                }
                DictsNewsTitleUpdating[accountName] = currentList;
            }
        }

        #endregion

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:00 
        /// </summary>
        /// <param name="news">NewsEntity</param>
        /// <param name="zoneId">int</param>
        /// <param name="zoneIdList">string</param>
        /// <param name="tagIdList">string</param>
        /// <param name="tagIdListForPrimary">string</param>
        /// <param name="newsRelationIdList">string</param>
        /// <param name="usernameForUpdateAction">string</param>
        /// <param name="newNewsId">ref long</param>
        /// <param name="newEncryptNewsId">ref string</param>
        /// <param name="authorList">List of author info</param>
        /// <returns></returns>
        public static ErrorCodes InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
                                            string usernameForUpdateAction, ref long newNewsId,
                                            ref string newEncryptNewsId, List<string> authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions, string action = "")
        {
            try
            {
                var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
                var RelationFormatJson = bool.Parse(BoConstants.RelationFormatJson);
                if (null == authorList || authorList.Count != 3)
                {
                    authorList = new List<string> { "", "", "" };
                }
                if (null == news || string.IsNullOrEmpty(news.Title))
                {
                    return ErrorCodes.UpdateNewsInvalidTitle;
                }

                #region Updating News

                if (IsUpdatingThisNews(news.Title))
                {                    
                    return ErrorCodes.UpdateNewsTitleIsExisted;
                }
                UpdatingThisNews(news.Title);

                #endregion

                if (zoneId <= 0 && action != "crawler_news")
                {
                    return ErrorCodes.UpdateNewsInvalidPrimaryZone;
                }

                newNewsId = SetPrimaryId();
                if (action == "notnewid")
                {
                    newNewsId = news.Id;                    
                }                
                news.Id = newNewsId;

                //var existsNews = NewsDal.GetNewsForValidateById(news.Id);
                var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsForValidateById(news.Id);
                if (null != existsNews)
                {
                    return ErrorCodes.UpdateNewsDuplicateId;
                }

                var primaryZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(zoneId);
                if (null == primaryZone)
                {
                    primaryZone = new Entity.Base.Zone.ZoneEntity();
                    if (action != "crawler_news")
                    {
                        primaryZone = ZoneDal.GetZoneById(zoneId);
                        if (null == primaryZone)
                        {
                            return ErrorCodes.UpdateNewsInvalidPrimaryZone;
                        }
                        else
                        {
                            //add zone redis
                            BoCached.Base.Zone.ZoneDalFactory.AddZone(primaryZone);
                        }
                    }
                }

                #region //tạm comment lại NewsRelation
                var newsRelationList = "";
                if (null != newsRelationIdList && newsRelationIdList.Any())
                {
                    try
                    {
                        var relatedNews = BoCached.Base.News.NewsDalFactory.GetRelatedNewsByNewsIds(newsRelationIdList);
                        if (relatedNews == null || (null != relatedNews && relatedNews.Count == 0))
                        {
                            relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                        }
                        if (null != relatedNews && relatedNews.Count > 0)
                        {
                            if (RelationFormatJson)
                                newsRelationList = NewtonJson.Serialize(relatedNews);
                            else
                            {
                                foreach (var itemRelation in relatedNews)
                                {
                                    newsRelationList += "," + itemRelation.NewsId;
                                }
                                if (newsRelationList != string.Empty) newsRelationList = newsRelationList.Remove(0, 1);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
                news.NewsRelation = newsRelationList;
                #endregion

                var tagLinkList = "";

                if (news.TagSubTitleId > 0)
                {
                    // Chống trùng tag
                    if (string.Format(";{0};", tagIdList).IndexOf(string.Format(";{0};", news.TagSubTitleId)) == -1)
                    {
                        tagIdList += (!string.IsNullOrEmpty(tagIdList) ? ";" : "") + news.TagSubTitleId;
                    }
                }
                var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);

                var tagFormat = BoConstants.NewsUrlFormatForTag;
                var tagItems = "";
                var TagJSON = "";
                if (tagList != null && tagList.Count > 0)
                {
                    var lstTag = new List<TagJson>();
                    // Lọc phát nữa cho chắc
                    tagList = tagList.Distinct().ToList();
                    foreach (var tag in tagList)
                    {
                        if (TagFormatJson)
                        {
                            var tagTmp = new TagJson
                            {
                                Id = tag.Id,
                                Name = tag.Name,
                                Url = tag.Url
                            };
                            lstTag.Add(tagTmp);
                        }
                        else
                        {
                            tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        }
                        tagItems += ";" + tag.Name;
                    }
                    //build tag to json                    
                    if (TagFormatJson)
                    {
                        TagJSON = NewtonJson.Serialize(lstTag);
                    }
                    else
                    {
                        if (tagLinkList != string.Empty) TagJSON = tagLinkList.Remove(0, 1);
                    }
                    if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                }

                news.TagPrimary = tagIdListForPrimary;
                news.Tag = TagJSON;//tagLinkList;
                news.TagItem = tagItems;

                /* Build link theo primary zone */
                if (news.Type != (int)NewsType.FakeNewsUrl)
                {
                    news.Url = BuildLinkUrl(newNewsId, news.Type, primaryZone.ShortUrl, news.Title, zoneId);
                }

                if (news.AdStore)
                {
                    news.AdStoreUrl = BuildLinkUrlAdStore(newNewsId, primaryZone.ShortUrl, primaryZone.Id, news.Title);
                }

                if (news.NewsType == (int)NewsType.EXT)
                {
                    //tam comment nhay buoc
                    if (!string.IsNullOrEmpty(action) && action!= "notnewid")
                    {
                        //// Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"                    
                        if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin, zoneId))
                        {
                            news.Status = (int)NewsStatus.ReceivedForPublish;
                            news.ApprovedBy = usernameForUpdateAction;
                            news.EditedBy = usernameForUpdateAction;
                        }
                        // Là biet tap vien => Lưu ở trạng thái "Nhận biên tập"
                        else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleEditor, zoneId))
                        {
                            news.Status = (int)NewsStatus.ReceivedForEdit;
                            news.EditedBy = usernameForUpdateAction;
                        }
                        // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
                        else
                        {
                            news.Status = (int)NewsStatus.Temporary;
                        }
                    }
                    else
                    {
                        news.Status = (int)NewsStatus.Temporary;
                    }
                }
                else
                {
                    //action !=null cho phep bai vao trang thai tho user quyen
                    if (!string.IsNullOrEmpty(action) && action!= "notnewid")
                    {
                        if (action == "crawler_news")
                        {
                            news.Status = (int)NewsStatus.CrawlerNews;
                            news.CreatedBy = usernameForUpdateAction;
                        }
                        else {
                            var user = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(usernameForUpdateAction);
                            // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"                    
                            if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin, zoneId))
                            {
                                // Là ban bien tap=> Lưu ở trạng thái "Nhận duyet"                    
                                if (user != null && user.User != null && (user.User.IsRole == (int)IsRole.EditorialBoard || user.User.IsRole == (int)IsRole.Secretary))
                                {
                                    news.Status = (int)NewsStatus.ReceivedForEditorialBoard;
                                    news.PublishedBy = usernameForUpdateAction;
                                    news.CreatedBy = usernameForUpdateAction;
                                }
                                else
                                {
                                    news.Status = (int)NewsStatus.ReceivedForPublish;
                                    news.ApprovedBy = usernameForUpdateAction;
                                    news.CreatedBy = usernameForUpdateAction;
                                }
                            }
                            // Là Bien tap vien => Lưu ở trạng thái "Nhận biên tập"                    
                            else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleEditor, zoneId))
                            {
                                news.Status = (int)NewsStatus.ReceivedForEdit;
                                news.EditedBy = usernameForUpdateAction;
                                news.CreatedBy = usernameForUpdateAction;
                            }
                            // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
                            else
                            {
                                news.Status = (int)NewsStatus.Temporary;
                                news.CreatedBy = usernameForUpdateAction;
                            }
                        }
                    }
                    else
                    {
                        news.Status = (int)NewsStatus.Temporary;
                        news.CreatedBy = usernameForUpdateAction;
                    }
                }

                var inserted = NewsDal.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary,
                                                  newsRelationIdList, authorList[0], authorList[1], authorList[2],
                                                  sourceId);
                if (!inserted)
                {
                    newNewsId = 0;
                }
                else
                {
                    //cut 19072018
                    news.ZoneId = zoneId;
                    var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                    updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;
                    news.ListZoneId = updateZoneIds;
                    //get zonename
                    var oZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(zoneId);
                    if (oZone != null) { news.ZoneName = oZone.Name; }

                    //insert news redis                    
                    if (!BoCached.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, news);
                    }
                    //insert news es                    
                    if (!BoSearch.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.ES, news);
                    }
                    //end

                    TaskInsertNewsSuccessAsync(newNewsId, news, zoneId, usernameForUpdateAction, zoneIdList, newsExtensions);

                    newEncryptNewsId = CryptonForId.EncryptId(newNewsId);
                    return ErrorCodes.Success;
                }
                //NewsUpdated(news.Title);

                return ErrorCodes.UnknowError;
            }
            catch(Exception ex)
            {
                NewsUpdated(news.Title);
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorCodes.UnknowError;
            }
        }       

        private static void TaskInsertNewsSuccessAsync(long newNewsId, NewsEntity news, int zoneId, string usernameForUpdateAction, string zoneIdList, List<NewsExtensionEntity> newsExtensions)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                try
                {
                    foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                    {
                        newsExtension.NewsId = newNewsId;
                        //NewsExtensionDal.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                        NewsExtensionBo.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                    }

                    //cut 19072018
                    //end

                    //update list extension to redis
                    //BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtensionByNewsId(newNewsId, newsExtensions);

                    var flagNewsNotify = NewsNotificationDal.UpdateNotification(newNewsId, usernameForUpdateAction,
                                                               (int)NewsStatus.ReceivedForEdit, false,
                                                               "[" + usernameForUpdateAction + "] vừa viết bài");
                    if (flagNewsNotify)
                    {
                        //add newsnotify redis
                        BoCached.Base.News.NewsDalFactory.UpdateNotification(new NewsNotificationEntity
                        {
                            NewsId = newNewsId,
                            UserName = usernameForUpdateAction,
                            NewsStatus = (int)NewsStatus.ReceivedForEdit,
                            IsWarning = false,
                            Message = "[" + usernameForUpdateAction + "] vừa viết bài"
                        });
                    }

                    var flagNewsHistory = NewsHistoryBo.InsertNews(newNewsId, news.Status, usernameForUpdateAction);
                    if (flagNewsHistory)
                    {
                        //add newshistory redis
                        BoCached.Base.News.NewsDalFactory.InsertNewsHistory(new NewsHistoryEntity
                        {
                            NewsId = newNewsId,
                            Status = news.Status,
                            SentBy = usernameForUpdateAction,
                            ReceivedBy = "",
                            ActionName = "Viết bài",
                            Description = "Viết bài"
                        });
                    }
                    var errocode = UpdateFirstVersion(news);
                    if (errocode == ErrorMapping.ErrorCodes.Success)
                    {
                        //add NewsVersion redis
                        //BoCached.Base.NewsVersion.NewsVersionDalFactory.UpdateFirstVersion(news);

                        //nodejs
                        UpdateFirstVersion(news);
                    }
                    ReleaseFirstVersion(newNewsId, usernameForUpdateAction);

                    // Log hành động sửa bài viết                    
                    ActivityBo.LogAddNews(newNewsId, usernameForUpdateAction, news.Title, news.Status);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "InsertNews => TaskInsertNewsSuccessAsync() error => " + ex);
                }

            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }

        public static ErrorCodes CrawlerNews(NewsEntity news)
        {
            try
            {     
                var newNewsId = SetPrimaryId();
                news.Id = newNewsId;
                
                var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsForValidateById(news.Id);
                if (null != existsNews)
                {
                    return ErrorCodes.UpdateNewsDuplicateId;
                }
                                
                var inserted = NewsDal.InsertNews(news, 0, "", "", "","", "", "", "",0);
                if (!inserted)
                {
                    newNewsId = 0;
                }
                else
                {
                    //insert news redis
                    if (!BoCached.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, news);
                    }
                    //insert news es
                    if (!BoSearch.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.ES, news);
                    }
                    
                    return ErrorCodes.Success;
                }
                
                return ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {               
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return ErrorCodes.UnknowError;
            }
        }

        public static ErrorCodes AppBotSaveNews(NewsEntity news, string action, ref long id)
        {
            try
            {
                id = 0L;
                var primaryZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(news.ZoneId);
                var ZoneShortUrl = "";
                if (primaryZone != null)
                {
                    ZoneShortUrl = primaryZone.ShortUrl;
                    news.ZoneName = primaryZone.Name;
                }                               

                var keyId = news.ParentNewsId + "_" + news.OriginalId;
                var existsNewsParent = BoCached.Base.News.NewsDalFactory.GetNewsParentIdAndOriginalId(keyId);

                if (null != existsNewsParent && existsNewsParent.NewsInfo != null && existsNewsParent.NewsInfo.Id > 0 && (news.Status == (int)NewsStatus.Published || action == "publish"))
                {
                    var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(existsNewsParent.NewsInfo.Id);
                    if (null == existsNews)
                    {
                        return ErrorCodes.UpdateNewsNewsNotFound;
                    }

                    //fix nếu tin xb bắn từ toquoc or bvhttdl thì chỉ update lại body 
                    if (news.OriginalId == 19 || news.OriginalId == 20) {
                        existsNews.Body = news.Body;
                    }
                    else {
                        existsNews.ZoneId = news.ZoneId;
                        existsNews.Title = news.Title;
                        existsNews.SubTitle = news.SubTitle;
                        existsNews.Sapo = news.Sapo;
                        existsNews.Body = news.Body;
                        existsNews.InitSapo = news.InitSapo;
                        existsNews.Avatar = news.Avatar;
                        existsNews.Author = news.Author;
                        existsNews.Source = news.Source;
                        existsNews.SourceURL = news.SourceURL;
                        existsNews.Note = news.Note;
                        existsNews.DistributionDate = DateTime.SpecifyKind(news.DistributionDate, DateTimeKind.Unspecified);
                        existsNews.ZoneName = news.ZoneName;
                        existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, ZoneShortUrl, existsNews.Title, existsNews.ZoneId);
                        existsNews.LastModifiedBy = news.LastModifiedBy;
                        existsNews.LastModifiedDate = DateTime.SpecifyKind(news.LastModifiedDate, DateTimeKind.Unspecified);
                        existsNews.ListZoneId = news.ListZoneId;
                    }
                    //tin sang to quoc đã sửa IsSensitiveNews(Nhạy cảm) thì ko update
                    var newsExtention = NewsExtensionDal.GetByNewsId(existsNews.Id);
                    if (newsExtention!=null && newsExtention.Exists(s=>s.Type==(int)EnumNewsExtensionType.IsSensitiveNews && Utility.ConvertToInt(s.Value,0)>0))
                    {
                        //ko làm gì
                        Logger.WriteLog(Logger.LogType.Trace, "AppBotSaveNews ko lam gi voi IsSensitiveNews>0 => keyId: " + keyId + " Url: " + existsNews.Url + " news.Avatar: " + news.Avatar + " DistributionDate: " + news.DistributionDate.ToString("dd/MM/yyyy HH:mm"));
                    }
                    else {                        
                        //update
                        if (NewsDal.UpdateNews(existsNews, existsNews.ZoneId, existsNews.ListZoneId, "", "", "", "", "", "", 0))
                        {
                            id = existsNews.Id;

                            var updateZoneIds = (existsNews.ZoneId > 0 ? existsNews.ZoneId.ToString() : "");
                            updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + existsNews.ListZoneId;
                            existsNews.ListZoneId = updateZoneIds;

                            //update news redis
                            if(!BoCached.Base.News.NewsDalFactory.AddNews(existsNews, true))
                            {
                                QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, existsNews);
                            }
                            //update news es
                            if (!BoSearch.Base.News.NewsDalFactory.UpdateNews(existsNews))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS, TopicName.ES, existsNews);
                            }

                            /*
                            republish lại để update newspublish, newscontent và cache monitor 
                            publish bài
                                */
                            if (NewsDal.ChangeStatusToPublished(existsNews.Id, 0, 0,
                                                        0, 0, existsNews.PublishedBy,
                                                        Utility.SetPublishedDate(news.DistributionDate), existsNews.Body))
                            {
                                //publish redis
                                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, 0, 0,
                                                                0, 0, existsNews.PublishedBy,
                                                                Utility.SetPublishedDate(news.DistributionDate), existsNews.Body))
                                {
                                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.REDIS, new NewsEntity { Id = existsNews.Id, PublishedBy = existsNews.PublishedBy, DistributionDate = existsNews.DistributionDate, Body = existsNews.Body, Status = (int)NewsStatus.Published });
                                }
                                //update es
                                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, news.PublishedBy, (int)NewsStatus.Published))
                                {
                                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.ES, new NewsEntity { Id = existsNews.Id, PublishedBy = news.PublishedBy, Status = (int)NewsStatus.Published });
                                }
                            }
                            try
                            {
                                BoFactory.GetInstance<NewsPositionBo>().UpdateNewsIntoAutoUpdatePosition(existsNews, existsNews.ZoneId, updateZoneIds);
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "AppBotSaveNews => UpdateNewsIntoAutoUpdatePosition(" + NewtonJson.Serialize(existsNews) + ") error => " + ex);
                            }

                            // Log hành động sửa bài viết                    
                            ActivityBo.LogUpdateNews(existsNews.Id, existsNews.LastModifiedBy, existsNews.Title, existsNews.Status);
                        }

                        Logger.WriteLog(Logger.LogType.Trace, "AppBotSaveNews update publish => keyId: " + keyId + " Url: " + existsNews.Url + " news.Avatar: " + news.Avatar + " DistributionDate: " + news.DistributionDate.ToString("dd/MM/yyyy HH:mm"));
                    }

                    //log bai bi update nguoc cho skhn
                    if (!string.IsNullOrEmpty(news.SentBy) && news.SentBy.ToLower().Equals("suckhoehangngay")) {
                        var keyQueueId = "appbot_publish";
                        var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyQueueId, action, news);
                    }                    

                    return ErrorCodes.Success;
                }
                else if (null != existsNewsParent && existsNewsParent.NewsInfo != null && existsNewsParent.NewsInfo.Id > 0 && existsNewsParent.NewsInfo.Status == (int)NewsStatus.WaitForPublish && action == "waitpublish")
                {
                    Logger.WriteLog(Logger.LogType.Trace, "AppBotSaveNews update waitpublish => keyId: " + keyId + " bài đã có ở chờ xb không tạo mới nữa.");

                    return ErrorCodes.Success;
                }
                else if (null != existsNewsParent && existsNewsParent.NewsInfo != null && existsNewsParent.NewsInfo.Id > 0 && existsNewsParent.NewsInfo.Status == (int)NewsStatus.WaitForEdit && action == "waiteditor")
                {
                    Logger.WriteLog(Logger.LogType.Trace, "AppBotSaveNews update waiteditor => keyId: " + keyId + " bài đã có ở chờ bt không tạo mới nữa.");

                    return ErrorCodes.Success;
                }

                //buil url
                news.Url = BuildLinkUrl(news.Id, news.Type, ZoneShortUrl, news.Title, news.ZoneId);
                news.CreatedDate= DateTime.SpecifyKind(news.CreatedDate, DateTimeKind.Unspecified);
                news.DistributionDate = DateTime.SpecifyKind(news.DistributionDate, DateTimeKind.Unspecified);
                news.LastModifiedDate = DateTime.SpecifyKind(news.LastModifiedDate, DateTimeKind.Unspecified);

                var inserted = NewsDal.InsertNewsForAppBotPublish(news, news.ZoneId, news.ListZoneId, "", "", "", "", "", "", 0, action);
                if (inserted)                
                {
                    var updateZoneIds = (news.ZoneId > 0 ? news.ZoneId.ToString() : "");
                    updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + news.ListZoneId;
                    news.ListZoneId = updateZoneIds;

                    //insert news redis
                    if (!BoCached.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, news);
                    }
                    //insert news es
                    if (!BoSearch.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.ES, news);
                    }

                    if (news.Status == (int)NewsStatus.Published) {
                        /* publish bài
                                */
                        if (NewsDal.ChangeStatusToPublished(news.Id, 0, 0,
                                                        0, 0, news.PublishedBy,
                                                        Utility.SetPublishedDate(news.DistributionDate), news.Body))
                        {
                            //publish redis
                            if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, 0, 0,
                                                            0, 0, news.PublishedBy,
                                                            Utility.SetPublishedDate(news.DistributionDate), news.Body))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.REDIS, new NewsEntity { Id = news.Id, PublishedBy = news.PublishedBy, DistributionDate = news.DistributionDate, Body = news.Body, Status = (int)NewsStatus.Published });
                            }
                            //update es
                            if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, news.CreatedBy, (int)NewsStatus.Published))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.ES, new NewsEntity { Id = news.Id, PublishedBy = news.CreatedBy, Status = (int)NewsStatus.Published });
                            }
                        }
                    }

                    //tao key newsparrenid and originalId -> check trùng tin
                    BoCached.Base.News.NewsDalFactory.AddNewsParentIdAndOriginalId(news);

                    // Log hành động sửa bài viết                    
                    ActivityBo.LogAddNews(news.Id, news.CreatedBy, news.Title, news.Status);

                    //Logger.WriteLog(Logger.LogType.Trace, "AppBotSaveNews insert publish => keyId: " + keyId + " Url: " + news.Url + " news.Avatar: " + news.Avatar + " DistributionDate: " + news.DistributionDate.ToString("dd/MM/yyyy HH:mm"));

                    return ErrorCodes.Success;
                }

                return ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                string errorData = " NewsId: " + news.ParentNewsId + " OriginalId: " + news.OriginalId;
                Logger.WriteLog(Logger.LogType.Error, ex.ToString() + errorData);
                return ErrorCodes.UnknowError;
            }
        }

        public static NewsEntity GetNewsIdByParentNewsIdForAppbotPublish(long parentId)
        {
            NewsEntity returnValue = NewsDal.GetNewsIdByParentNewsIdForAppbotPublish(parentId);
            return returnValue;
        }

        public static ErrorCodes AppCrawlerInitNewsDB(NewsEntity news)
        {
            try
            {
                //xu ly tag
                if (!string.IsNullOrEmpty(news.Tag))
                {
                    var dataTag = news.Tag.Split(';').ToList();
                    var TagBo = new TagBo();                    
                    var taglistid = new List<string>();
                    foreach (var item in dataTag)
                    {
                        var currentTag = TagBo.GetTagByTagName(item);
                        if (currentTag == null)
                        {
                            var tagNew = new TagEntity
                            {
                                Name = item,
                                Description = item,
                                ParentId = 0,
                                Url = Utility.ConvertTextToLink(item),
                                ZoneId = 0
                            };
                            long newTagId = 0;
                            var result = TagBo.InsertTag(tagNew, 0, "",ref newTagId);
                            if (result == ErrorCodes.Success)
                            {
                                taglistid.Add(newTagId.ToString());
                            }
                        }
                        else
                        {
                            taglistid.Add(currentTag.Id.ToString());
                        }
                    }
                    news.TagItem = string.Join(";", dataTag);
                    news.Tag = string.Join(";", taglistid);
                }

                var primaryZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(news.ZoneId);
                var ZoneShortUrl = "";
                if (primaryZone != null)
                {
                    ZoneShortUrl = primaryZone.ShortUrl;
                    news.ZoneName = primaryZone.Name;
                }

                DateTime distributionDate;
                if (DateTime.TryParse(news.DistributionDate.ToString(), CultureInfo.InvariantCulture, DateTimeStyles.None, out distributionDate))
                    news.DistributionDate = distributionDate;

                //buil url
                news.Url = BuildLinkUrl(news.Id, news.Type, ZoneShortUrl, news.Title, news.ZoneId);

                var inserted = NewsDal.InsertNews(news, news.ZoneId, news.ListZoneId, news.Tag, "", news.NewsRelation, "", "", "", 0);
                if (inserted)
                {
                    //insert news redis
                    if (!BoCached.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, news);
                    }
                    //insert news es
                    if (!BoSearch.Base.News.NewsDalFactory.AddNews(news))
                    {
                        QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.ES, news);
                    }
                    
                    /* publish bài
                            */
                    if (NewsDal.ChangeStatusToPublished(news.Id, 0, 0,
                                                    0, 0, news.CreatedBy,
                                                    Utility.SetPublishedDate(news.DistributionDate), news.Body))
                    {
                        //publish redis
                        if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, 0, 0, 0, 0, news.CreatedBy, Utility.SetPublishedDate(news.DistributionDate), news.Body))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.REDIS, new NewsEntity { Id = news.Id, PublishedBy = news.CreatedBy, DistributionDate = news.DistributionDate, Body = news.Body, Status = (int)NewsStatus.Published });
                        }
                        //update es
                        if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(news.Id, news.CreatedBy, (int)NewsStatus.Published))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.ES, new NewsEntity { Id = news.Id, PublishedBy = news.CreatedBy, Status = (int)NewsStatus.Published });
                        }
                    }                   

                    return ErrorCodes.Success;
                }

                return ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString() + " NewsId = " + news.Id);
                return ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteNews(long newsId, string userName)
        {
            var errorCode = ErrorCodes.BusinessError;
            var news = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (news == null)
            {
                news = NewsDal.GetNewsById(newsId);
            }
            // chỉ xóa bài từ thùng rác
            if (news.Status == 9)
                if (NewsDal.DeleteNews(newsId))
                {
                    errorCode = ErrorCodes.Success;
                    ActivityBo.LogDeleteNews(newsId, userName, "[DB] " + news.Title, news.Status);
                }
            return errorCode;
        }

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:06 
        /// </summary>
        /// <param name="news"></param>
        /// <param name="zoneId"></param>
        /// <param name="zoneIdList"></param>
        /// <param name="tagIdList"></param>
        /// <param name="tagIdListForPrimary"></param>
        /// <param name="newsRelationIdList"></param>
        /// <param name="usernameForUpdateAction"></param>
        /// <param name="isRebuildLink"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
                                            string usernameForUpdateAction, bool isRebuildLink, ref int newsStatus, List<string> authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
            var RelationFormatJson = bool.Parse(BoConstants.RelationFormatJson);
            if (null == authorList || authorList.Count != 3)
            {
                authorList = new List<string> { "", "", "" };
            }
            
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
            }
            if (null == news || news.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            //var existsNews = NewsDal.GetNewsById(news.Id);
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(news.Id);
            if (null == existsNews)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }
            
            var oldTitle = existsNews.Title;
            var newTitle = news.Title;

            if (existsNews.Status == (int)NewsStatus.Published && oldTitle != newTitle)
            {
                //check trung title khi xb
                var existTitle = BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", newTitle, (int)NewsStatus.Published)));
                if (existTitle != null && !string.IsNullOrEmpty(newTitle))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                }
            }

            var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(usernameForUpdateAction);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
            }

            /* Chỉ được quyền edit khi tin ở một trong các trạng thái:
             *  - "Lưu tạm", "Trả lại phóng viên" và người cập nhật là người gửi
             *  - "Chờ biên tập", "Nhận biên tập", "Trả lại biên tập viên" và người cập nhật là người đã nhận biên tập tin này
             *  - "Chờ xuất bản", "Nhận xuất bản", "Đã xuất bản", "Bị gỡ xuống" và người cập nhạt là người đã nhận xuất bản tin này
             *  - "Xóa tạm" và người cập nhật là người cập nhật cuối
             */
            //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_2_" + existsNews.Status);
            if (!(((existsNews.Status == (int)NewsStatus.Temporary ||
                    existsNews.Status == (int)NewsStatus.ReturnedToReporter)
                   && existsNews.CreatedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.WaitForEdit ||
                    existsNews.Status == (int)NewsStatus.ReceivedForEdit ||
                    existsNews.Status == (int)NewsStatus.ReturnedToEditor)
                   && existsNews.EditedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.WaitForPublish ||
                    existsNews.Status == (int)NewsStatus.ReceivedForPublish ||
                    existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished)
                   && (existsNews.ApprovedBy != null && existsNews.ApprovedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase))) ||
                  ((existsNews.Status == (int)NewsStatus.MovedToTrash)
                   &&
                   existsNews.LastModifiedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                  ((existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished)
                   &&
                   (((existsNews.ApprovedBy != null && existsNews.ApprovedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                     user.IsFullPermission))) ||
                     (existsNews.Status == (int)NewsStatus.ReturnToCooperator)))
            {
                // Nếu bài đã xuất bản thì check thêm trường hợp người sửa là thư ký có quyền xử lý chuyên mục
                if (existsNews.Status == (int)NewsStatus.Published ||
                    existsNews.Status == (int)NewsStatus.Unpublished ||
                    existsNews.Status == (int)NewsStatus.ReturnToCooperator)
                {
                    // Lấy quyền của người nhận                    
                    var userPermissionsForUpdater = BoCached.Base.Security.PermissionDalFactory.GetListByUserName(usernameForUpdateAction);
                    //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_3_" + NewtonJson.Serialize(userPermissionsForUpdater));
                    // Người xử lý không có quyền thư ký trong chuyên mục của bài này => không được phép sửa
                    if (!(user.IsFullPermission || (BoCached.Base.Security.PermissionDalFactory.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin) && user.IsFullZone) || IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user)))
                    {
                        //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_4_UpdateNewsNotAllowEdit");
                        return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
                    }
                }
                else if (existsNews.Status == (int)NewsStatus.ReceivedForEditorialBoard || existsNews.Status == (int)NewsStatus.ReturnedToMyEditor || existsNews.Status == (int)NewsStatus.ReceivedForPublish)
                {
                    //cho phep ban bien tap tiep tuc sua
                    //cho phep ban bien tap tiep tuc sua bai tra lai toi
                }                
                else
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
                }
            }

            #region tạm comment NewsRelation
            var newsRelationList = "";
            try
            {
                var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                if (null != relatedNews && relatedNews.Count > 0)
                {
                    if (!RelationFormatJson)
                    {
                        foreach (var itemRelation in relatedNews)
                        {
                            newsRelationList += "," + itemRelation.NewsId;
                        }
                        if (!string.IsNullOrEmpty(newsRelationList)) newsRelationList = newsRelationList.Remove(0, 1);
                    }
                    else
                    {
                        newsRelationList = NewtonJson.Serialize(relatedNews);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            news.NewsRelation = newsRelationList;
            #endregion

            if (news.TagSubTitleId > 0)
            {
                // Chống trùng tag
                if (string.Format(";{0};", tagIdList).IndexOf(string.Format(";{0};", news.TagSubTitleId)) == -1)
                {
                    tagIdList += (!string.IsNullOrEmpty(tagIdList) ? ";" : "") + news.TagSubTitleId;
                }
            }
            var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
            //Convert tag về chuỗi json
            var tagLinkList = "";

            var tagFormat = BoConstants.NewsUrlFormatForTag;
            var tagItems = "";
            string TagJSON = "";
            if (tagList != null && tagList.Count > 0)
            {
                var lstTag = new List<TagJson>();
                // Lọc phát nữa cho chắc
                tagList = tagList.Distinct().ToList();
                foreach (var tag in tagList)
                {
                    if (TagFormatJson)
                    {
                        var tagTmp = new TagJson
                        {
                            Id = tag.Id,
                            Name = tag.Name,
                            Url = tag.Url
                        };
                        if (lstTag.FirstOrDefault(x => x.Id == tagTmp.Id) != null)
                        {
                            continue;
                        }
                        lstTag.Add(tagTmp);
                    }
                    else
                    {
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                    }
                    tagItems += ";" + tag.Name;
                }
                //build tag to json
                if (TagFormatJson)
                {
                    TagJSON = NewtonJson.Serialize(lstTag);
                }
                else
                {
                    if (tagLinkList != string.Empty) TagJSON = tagLinkList.Remove(0, 1);
                }
                if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
            }
            news.TagPrimary = tagIdListForPrimary;
            news.Tag = TagJSON;//tagLinkList;         

            existsNews.Author = news.Author;
            existsNews.Avatar = news.Avatar;
            existsNews.AvatarDesc = news.AvatarDesc;
            existsNews.Avatar2 = news.Avatar2;
            existsNews.Avatar3 = news.Avatar3;
            existsNews.Avatar4 = news.Avatar4;
            existsNews.Avatar5 = news.Avatar5;
            existsNews.DistributionDate = news.DistributionDate;
            existsNews.Sapo = news.Sapo;
            existsNews.Title = news.Title;
            existsNews.Body = news.Body;
            existsNews.NewsRelation = news.NewsRelation;
            existsNews.SubTitle = news.SubTitle;
            existsNews.LastModifiedBy = news.LastModifiedBy;
            existsNews.LastModifiedDate = news.LastModifiedDate;
            existsNews.WordCount = news.WordCount;
            existsNews.Source = news.Source;
            existsNews.Tag = news.Tag;
            existsNews.TagPrimary = news.TagPrimary;
            existsNews.Note = news.Note;
            existsNews.Type = news.Type;
            existsNews.NewsType = news.NewsType;
            existsNews.OriginalId = news.OriginalId;
            existsNews.TagItem = tagItems;
            existsNews.InitSapo = news.InitSapo;

            //quangnv added on 07/01/2013
            existsNews.AdStore = news.AdStore;
            existsNews.TagSubTitleId = news.TagSubTitleId;

            existsNews.IsFocus = news.IsFocus;
            existsNews.IsOnHome = news.IsOnHome;
            existsNews.IsOnMobile = news.IsOnMobile;
            existsNews.ThreadId = news.ThreadId;
            existsNews.AvatarCustom = news.AvatarCustom;
            existsNews.DisplayInSlide = news.DisplayInSlide;
            existsNews.DisplayPosition = news.DisplayPosition;
            existsNews.DisplayStyle = news.DisplayStyle;

            existsNews.IsActivePenName = news.IsActivePenName;
            existsNews.IsShowPenNameCTV = news.IsShowPenNameCTV;
            existsNews.PenName = news.PenName;
            existsNews.CmsAccountVietId = news.CmsAccountVietId;

            existsNews.InterviewId = news.InterviewId;
            existsNews.RollingNewsId = news.RollingNewsId;

            existsNews.Priority = news.Priority;
            existsNews.LocationType = news.LocationType;
            existsNews.ExpiredDate = news.ExpiredDate;
            existsNews.SourceURL = news.SourceURL;

            existsNews.NewsCategory = news.NewsCategory;
            existsNews.NoteRoyalties = news.NoteRoyalties;            
            existsNews.IsPr = news.IsPr;

            existsNews.ShortTitle = news.ShortTitle;
            existsNews.ParentNewsId = news.ParentNewsId;
            existsNews.IsProd = news.IsProd;            

            // Neu la thu ky thi moi cho sua cac thong tin nay
            if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin, zoneId))
            {
                existsNews.Price = news.Price;
                existsNews.BonusPrice = news.BonusPrice;

                // VTV - BTV chọn được thể loại bài

                existsNews.TemplateName = news.TemplateName;
                existsNews.TemplateConfig = news.TemplateConfig;

            }
            existsNews.IsBreakingNews = news.IsBreakingNews;
            existsNews.PegaBreakingNews = news.PegaBreakingNews;

            /* Build link theo primary zone */
            var primaryZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(zoneId);
            if (null == primaryZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }
            if (existsNews.Status == (int)NewsStatus.Published)
            {
                if (isRebuildLink)
                {
                    if (existsNews.Type != (int)NewsType.FakeNewsUrl)
                    {
                        existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title,
                              zoneId);
                    }
                }
            }
            else
            {
                if (existsNews.Type != (int)NewsType.FakeNewsUrl)
                {
                    existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title,
                        zoneId);
                }
            }

            existsNews.AdStore = news.AdStore;

            if (existsNews.AdStore)
            {
                existsNews.AdStoreUrl = BuildLinkUrlAdStore(existsNews.Id, primaryZone.ShortUrl, primaryZone.Id, news.Title);
            }

            //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_5_Status_" + existsNews.Status + "_User_" + usernameForUpdateAction);
            // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"
            if ((existsNews.Status == (int)NewsStatus.WaitForPublish ||
                 existsNews.Status == (int)NewsStatus.Unpublished ||
                 existsNews.Status == (int)NewsStatus.ReturnToCooperator) &&
                BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin, zoneId))
            {
                //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_5_PublishedBy_" + usernameForUpdateAction);
                existsNews.Status = (int)NewsStatus.ReceivedForPublish;
                existsNews.ApprovedBy = usernameForUpdateAction;
            }
            // Là biên tập viên => Lưu ở trạng thái "Nhận biên tập"
            else if ((existsNews.Status == (int)NewsStatus.WaitForEdit ||
                      existsNews.Status == (int)NewsStatus.ReturnedToEditor ||
                      existsNews.Status == (int)NewsStatus.ReturnToCooperator) &&
                     BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction,
                                                      (int)EnumPermission.ArticleEditor, zoneId))
            {
                //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_5_EditedBy_" + usernameForUpdateAction);
                existsNews.Status = (int)NewsStatus.ReceivedForEdit;
                existsNews.EditedBy = usernameForUpdateAction;
            }
            // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
            else if (existsNews.Status == (int)NewsStatus.ReturnedToReporter ||
                     existsNews.Status == (int)NewsStatus.ReturnToCooperator)
            {
                //Logger.WriteLog(Logger.LogType.Trace, "UpdateNews_5_CreatedBy_" + usernameForUpdateAction);
                existsNews.Status = (int)NewsStatus.Temporary;
                existsNews.CreatedBy = usernameForUpdateAction;
            }

            if (NewsDal.UpdateNews(existsNews, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, authorList[0], authorList[1], authorList[2], sourceId))
            {
                //cut->obj existsNews bi thay doi ngay xb                
                existsNews.ZoneId = zoneId;
                var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;
                existsNews.ListZoneId = updateZoneIds;
                //get zonename
                var oZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(zoneId);
                if (oZone != null) { existsNews.ZoneName = oZone.Name; }

                //fix ngay xb null
                if (existsNews.DistributionDate <= DateTime.MinValue) { existsNews.DistributionDate = DateTime.Now; }

                //update news redis
                if (!BoCached.Base.News.NewsDalFactory.AddNews(existsNews, true))
                {
                    QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, existsNews);
                }
                //update news es
                if (!BoSearch.Base.News.NewsDalFactory.UpdateNews(existsNews))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS, TopicName.ES, existsNews);
                }
                //end

                if (oldTitle != newTitle)
                {
                    NewsUpdated(oldTitle);
                    UpdatingThisNews(newTitle);
                }
                newsStatus = existsNews.Status;

                TaskUpdateNewsSuccessAsync(existsNews, zoneId, zoneIdList, usernameForUpdateAction, publishedContent, newsExtensions);

                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.UnknowError;
        }

        private static void TaskUpdateNewsSuccessAsync(NewsEntity existsNews, int zoneId, string zoneIdList, string usernameForUpdateAction, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                try
                {                    
                    //NewsExtensionDal.DeleteByNewsId(existsNews.Id);
                    NewsExtensionBo.DeleteByNewsId(existsNews.Id);
                    foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                    {
                        newsExtension.NewsId = existsNews.Id;
                        //NewsExtensionDal.SetValue(existsNews.Id, newsExtension.Type, newsExtension.Value);
                        NewsExtensionBo.SetValue(existsNews.Id, newsExtension.Type, newsExtension.Value);
                    }                    

                    //cut->obj existsNews bi thay doi ngay xb                    
                    var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                    updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;                                  
                    //end

                    //update list extension to redis
                    //BoCached.Base.NewsExtension.NewsExtensionDalFactory.AddNewsExtensionByNewsId(existsNews.Id, newsExtensions);

                    //update tagnews redis
                    var listTagNews = TagNewsDal.GetTagNewsByNewsId(existsNews.Id);
                    if (listTagNews != null)
                    {
                        BoCached.Base.Tag.TagDalFactory.AddTagNewsByNewsId(existsNews.Id, listTagNews);
                    }

                    //NewsHistoryBo.UpdateNews(news.Id, news.Status, usernameForUpdateAction);

                    UpdateVersion(existsNews, usernameForUpdateAction);
                    ReleaseVersion(existsNews.Id, usernameForUpdateAction);

                    var flagNewsHistory = NewsHistoryBo.UpdateNews(existsNews.Id, existsNews.Status, usernameForUpdateAction, existsNews.Title);

                    // Nếu bài đã published thì republish lại để update newspublish, newscontent và cache monitor
                    if (existsNews.Status == (int)NewsStatus.Published)
                    {
                        /* publish bài
                            */
                        if (NewsDal.ChangeStatusToPublished(existsNews.Id, 0, 0,
                                                        0, 0, usernameForUpdateAction,
                                                        Utility.SetPublishedDate(existsNews.DistributionDate), publishedContent))
                        {
                            //publish redis
                            if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(existsNews.Id, 0, 0,
                                                            0, 0, usernameForUpdateAction,
                                                            Utility.SetPublishedDate(existsNews.DistributionDate), publishedContent))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.REDIS, new NewsEntity { Id = existsNews.Id, PublishedBy = usernameForUpdateAction, DistributionDate = existsNews.DistributionDate, Body = publishedContent, Status = (int)NewsStatus.Published });
                            }
                            //update es
                            if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(existsNews.Id, usernameForUpdateAction, (int)NewsStatus.Published))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.ES, new NewsEntity { Id = existsNews.Id, PublishedBy = usernameForUpdateAction, Status = (int)NewsStatus.Published });
                            }
                        }
                        try
                        {
                            BoFactory.GetInstance<NewsPositionBo>().UpdateNewsIntoAutoUpdatePosition(existsNews, zoneId, updateZoneIds);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Fatal, "UpdateNews => UpdateNewsIntoAutoUpdatePosition(" + NewtonJson.Serialize(existsNews) + ") error => " + ex);
                        }
                    }
                    // Log hành động sửa bài viết                    
                    ActivityBo.LogUpdateNews(existsNews.Id, usernameForUpdateAction, existsNews.Title, existsNews.Status);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, "UpdateNews => TaskUpdateNewsSuccessAsync() error => " + ex);
                }
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:09 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="avatar"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateNewsAvatar(long newsId, string avatar, int position)
        {
            var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (newsInfo == null)
            {
                newsInfo = NewsDal.GetNewsById(newsId);
                if (null == newsInfo || newsInfo.Id <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
            }
            switch (position)
            {
                //case 0:
                //case 1:
                default:
                    newsInfo.Avatar = avatar;
                    break;
                case 2:
                    newsInfo.Avatar2 = avatar;
                    break;
                case 3:
                    newsInfo.Avatar3 = avatar;
                    break;
                case 4:
                    newsInfo.Avatar4 = avatar;
                    break;
                case 5:
                    newsInfo.Avatar5 = avatar;
                    break;
                case 6:
                    newsInfo.AvatarCustom = avatar;
                    break;
            }
            var result = false;
            try
            {
                result = NewsDal.UpdateNewsAvatar(newsInfo.Id, newsInfo.Avatar, newsInfo.Avatar2, newsInfo.Avatar3,
                                                  newsInfo.Avatar4, newsInfo.Avatar5, newsInfo.AvatarCustom);
                if (result)
                {
                    //update es
                    BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsAvatar(newsInfo.Id, newsInfo.Avatar,"");
                    //update redis
                    BoCached.Base.News.NewsDalFactory.UpdateNewsAvatar(newsInfo.Id, newsInfo.Avatar, newsInfo.Avatar2, newsInfo.Avatar3,
                                                  newsInfo.Avatar4, newsInfo.Avatar5, newsInfo.AvatarCustom);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.Message);
            }
            return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        /// <summary>
        /// Updated: NANIA
        /// LastModifiedDate: 2013-01-24 12:08 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="usernameForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Delete(long newsId, string usernameForUpdateAction)
        {
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete;
            }
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            var existsNews = NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            /* Chỉ được quyền xóa khi tin ở trạng thái "Xóa tạm (thùng rác)" và người xóa là người cập nhật cuối (người xóa tạm) 
             */
            if (
                !((existsNews.Status == (int)NewsStatus.MovedToTrash &&
                   existsNews.LastModifiedBy.Equals(usernameForUpdateAction, StringComparison.CurrentCultureIgnoreCase))))
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNotAllowDelete;
            }

            if (NewsDal.DeleteNews(newsId))
            {
                // Log hành động sửa bài viết
                ActivityBo.LogDeleteNews(existsNews.Id, existsNews.LastModifiedBy, existsNews.Title, existsNews.Status);
                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateOnlyTitle(long newsId, string newsTitle, string accountName)
        {
            try
            {                          
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(newsId);
                    if (newsInfo == null)
                    {
                        return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                    }
                }
                if (newsInfo != null)
                {
                    if (newsInfo.Status == (int)NewsStatus.Published && newsInfo.Title != newsTitle)
                    {
                        //check trung title khi xb
                        var existTitle = BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, (int)NewsStatus.Published)));
                        if (existTitle != null && !string.IsNullOrEmpty(newsTitle))
                        {
                            return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                        }
                    }

                    // Get Zone Primary to build URL for news
                    var zonePrimary = BoCached.Base.Zone.ZoneDalFactory.GetPrimaryZoneByNewsId(newsId);
                    if (zonePrimary == null)
                    {
                        zonePrimary = ZoneDal.GetPrimaryZoneByNewsId(newsId);
                    }
                    var newsUrl = BuildLinkUrl(newsId, newsInfo.Type, zonePrimary.ShortUrl, newsTitle, newsInfo.ZoneId);
                    // Update Title for this news
                    if (NewsDal.UpdateOnlyNewsTitle(newsId, newsTitle, newsUrl, accountName))
                    {
                        //update title redis
                        if(!BoCached.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsId, newsTitle, newsUrl, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ONLYTITLE,TopicName.REDIS, new NewsEntity { Id=newsId,Title=newsTitle,Url=newsUrl,LastModifiedBy=accountName });
                        }
                        //update title es
                        if(!BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsId, newsTitle, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ONLYTITLE, TopicName.ES, new NewsEntity { Id = newsId, Title = newsTitle, Url = newsUrl, LastModifiedBy = accountName });
                        }

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }

                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateDetail(long newsId, string newsTitle, string sapo, string body, string accountName)
        {
            try
            {
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(newsId);
                    if (newsInfo == null)
                    {
                        return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                    }
                }
                if (newsInfo != null)
                {
                    if (newsInfo.Status == (int)NewsStatus.Published && newsInfo.Title != newsTitle)
                    {
                        //check trung title khi xb
                        var existTitle = BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, (int)NewsStatus.Published)));
                        if (existTitle != null && !string.IsNullOrEmpty(newsTitle))
                        {
                            return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                        }
                    }

                    if (string.IsNullOrEmpty(newsTitle)) { newsTitle = newsInfo.Title; }
                    if (string.IsNullOrEmpty(sapo)) { newsTitle = newsInfo.Sapo; }
                    if (string.IsNullOrEmpty(body)) { newsTitle = newsInfo.Body; }

                    // Get Zone Primary to build URL for news
                    var zonePrimary = ZoneDal.GetPrimaryZoneByNewsId(newsId);
                    var newsUrl = BuildLinkUrl(newsId, newsInfo.Type, zonePrimary.ShortUrl, newsTitle, newsInfo.ZoneId);
                    // Update Title for this news
                    if (NewsDal.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName))
                    {
                        //update to redis
                        if(!BoCached.Base.News.NewsDalFactory.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_DETAIL, TopicName.REDIS, new NewsEntity { Id = newsId, Title = newsTitle, Sapo = sapo, Body = body, Url = newsUrl, LastModifiedBy = accountName });
                        }
                        //update title es
                        if(!BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsId, newsTitle, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ONLYTITLE, TopicName.REDIS, new NewsEntity { Id = newsId, Title = newsTitle, LastModifiedBy = accountName });
                        }

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }

                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateNewsByErrorCheck(NewsEntity news, string tagIdList, string accountName)
        {
            try
            {                
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(news.Id);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(news.Id);
                    if (newsInfo == null)
                    {
                        return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                    }
                }
                if (newsInfo != null)
                {
                    if (newsInfo.Status == (int)NewsStatus.Published && newsInfo.Title != news.Title)
                    {
                        //check trung title khi xb
                        var existTitle = BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", news.Title, (int)NewsStatus.Published)));
                        if (existTitle != null && !string.IsNullOrEmpty(news.Title))
                        {
                            return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                        }
                    }

                    if (string.IsNullOrEmpty(news.Title)) { news.Title = newsInfo.Title; }
                    if (string.IsNullOrEmpty(news.SubTitle)) { news.SubTitle = newsInfo.SubTitle; }
                    if (string.IsNullOrEmpty(news.ShortTitle)) { news.ShortTitle = newsInfo.ShortTitle; }
                    if (string.IsNullOrEmpty(news.Sapo)) { news.Sapo = newsInfo.Sapo; }
                    if (string.IsNullOrEmpty(news.Body)) { news.Body = newsInfo.Body; }
                    if (string.IsNullOrEmpty(news.AvatarDesc)) { news.AvatarDesc = newsInfo.AvatarDesc; }
                    if (string.IsNullOrEmpty(news.Note)) { news.Note = newsInfo.Note; }
                    if (string.IsNullOrEmpty(news.Avatar)) { news.Avatar2 = newsInfo.Avatar; }
                    if (string.IsNullOrEmpty(news.Avatar2)) { news.Avatar2 = newsInfo.Avatar2; }
                    if (string.IsNullOrEmpty(news.Avatar3)) { news.Avatar3 = newsInfo.Avatar3; }
                    if (string.IsNullOrEmpty(news.Avatar4)) { news.Avatar4 = newsInfo.Avatar4; }
                    if (string.IsNullOrEmpty(news.Avatar5)) { news.Avatar5 = newsInfo.Avatar5; }
                    if (news.TagSubTitleId<0) { news.TagSubTitleId = newsInfo.TagSubTitleId; }

                    var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
                    if (newsInfo.TagSubTitleId > 0)
                    {
                        // Chống trùng tag
                        if (string.Format(";{0};", tagIdList).IndexOf(string.Format(";{0};", newsInfo.TagSubTitleId)) == -1)
                        {
                            tagIdList += (!string.IsNullOrEmpty(tagIdList) ? ";" : "") + newsInfo.TagSubTitleId;
                        }
                    }
                    var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
                    //Convert tag về chuỗi json
                    var tagLinkList = "";

                    var tagFormat = BoConstants.NewsUrlFormatForTag;
                    var tagItems = "";
                    string TagJSON = "";
                    if (tagList != null && tagList.Count > 0)
                    {
                        var lstTag = new List<TagJson>();
                        // Lọc phát nữa cho chắc
                        tagList = tagList.Distinct().ToList();
                        foreach (var tag in tagList)
                        {
                            if (TagFormatJson)
                            {
                                var tagTmp = new TagJson
                                {
                                    Id = tag.Id,
                                    Name = tag.Name,
                                    Url = tag.Url
                                };
                                if (lstTag.FirstOrDefault(x => x.Id == tagTmp.Id) != null)
                                {
                                    continue;
                                }
                                lstTag.Add(tagTmp);
                            }
                            else
                            {
                                tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                            }
                            tagItems += ";" + tag.Name;
                        }
                        //build tag to json
                        if (TagFormatJson)
                        {
                            TagJSON = NewtonJson.Serialize(lstTag);
                        }
                        else
                        {
                            if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                        }
                        if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                    }                    
                    newsInfo.Tag = TagJSON;

                    // Get Zone Primary to build URL for news
                    var zonePrimary = ZoneDal.GetPrimaryZoneByNewsId(news.Id);
                    var newsUrl = BuildLinkUrl(news.Id, newsInfo.Type, zonePrimary.ShortUrl, news.Title, newsInfo.ZoneId);
                    // Update Title for this news
                    if (NewsDal.UpdateNewsByErrorCheck(news, newsUrl, newsInfo.Tag, accountName))
                    {
                        //update to redis
                        if(!BoCached.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(news, newsUrl, newsInfo.Tag, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_CHECKERROR,TopicName.REDIS,news);
                        }
                        //update title es
                        if(!BoSearch.Base.News.NewsDalFactory.UpdateNewsByErrorCheck(news.Id, news.Title, news.Sapo, news.Avatar, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_CHECKERROR, TopicName.ES, new NewsEntity { Id=news.Id,Title=news.Title,Sapo=news.Sapo, Avatar=news.Avatar, LastModifiedBy=accountName});
                        }

                        TaskErrorCheckSuccessAsync(newsInfo, news, accountName);

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }

                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        private static void TaskErrorCheckSuccessAsync(NewsEntity newsDb, NewsEntity newsCheck, string usernameForUpdateAction)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                try
                {
                    if(newsDb!=null && newsCheck != null)
                    {
                        newsDb.Title = newsCheck.Title;
                        newsDb.SubTitle = newsCheck.SubTitle;
                        newsDb.ShortTitle = newsCheck.ShortTitle;
                        newsDb.Sapo = newsCheck.Sapo;
                        newsDb.Body = newsCheck.Body;
                        newsDb.AvatarDesc = newsCheck.AvatarDesc;
                        newsDb.Note = newsCheck.Note;
                        newsDb.Avatar2 = newsCheck.Avatar2;
                        newsDb.Avatar3 = newsCheck.Avatar3;
                        newsDb.Avatar4 = newsCheck.Avatar4;
                        newsDb.Avatar5 = newsCheck.Avatar5;
                        newsDb.TagSubTitleId = newsCheck.TagSubTitleId;
                    }

                    UpdateVersion(newsDb, usernameForUpdateAction);
                    ReleaseVersion(newsDb.Id, usernameForUpdateAction);

                    NewsHistoryBo.UpdateCheckError(newsDb.Id, newsDb.Status, usernameForUpdateAction, newsDb.Title);

                    // Log hành động soát lỗi bài viết                    
                    ActivityBo.LogUpdateNewsByErrorCheck(newsDb.Id, usernameForUpdateAction, newsDb.Title, newsDb.Status);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "UpdateNewsByErrorCheck => TaskErrorCheckSuccessAsync() error => " + ex);
                }
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }

        public static ErrorMapping.ErrorCodes UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            try
            {                
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(newsId);
                    if (newsInfo == null)
                    {
                        return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                    }
                }
                if (newsInfo != null)
                {
                    if (newsInfo.Status == (int)NewsStatus.Published && newsInfo.Title != newsTitle)
                    {
                        //check trung title khi xb
                        var existTitle = BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, (int)NewsStatus.Published)));
                        if (existTitle != null && !string.IsNullOrEmpty(newsTitle))
                        {
                            return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                        }
                    }

                    if (string.IsNullOrEmpty(newsTitle)) { newsTitle = newsInfo.Title; }
                    if (string.IsNullOrEmpty(avatar)) { newsTitle = newsInfo.Avatar; }
                    if (string.IsNullOrEmpty(avatar2)) { newsTitle = newsInfo.Avatar2; }
                    if (string.IsNullOrEmpty(avatar3)) { newsTitle = newsInfo.Avatar3; }
                    if (string.IsNullOrEmpty(avatar4)) { newsTitle = newsInfo.Avatar4; }
                    if (string.IsNullOrEmpty(avatar5)) { newsTitle = newsInfo.Avatar5; }

                    if (NewsDal.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName))
                    {
                        //update to redis
                        if(!BoCached.Base.News.NewsDalFactory.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_DETAILSIMPLE,TopicName.REDIS,new NewsEntity { Id=newsId,Title=newsTitle,Avatar=avatar, Avatar2 = avatar2 , Avatar3 = avatar3 , Avatar4 = avatar4 , Avatar5 = avatar5, LastModifiedBy = accountName });
                        }
                        //update title es
                        if(!BoSearch.Base.News.NewsDalFactory.UpdateOnlyNewsTitle(newsId, newsTitle, accountName))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ONLYTITLE, TopicName.REDIS, new NewsEntity { Id = newsId, Title = newsTitle, LastModifiedBy = accountName });
                        }

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }

                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            try
            {
                var newsInfo = NewsDal.GetNewsById(newsId);
                if (newsInfo == null) return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                else
                {
                    if (NewsDal.UpdateDisplayPosition(newsId, displayPosition, accountName))
                    {
                        BoSearch.Base.News.NewsDalFactory.UpdateDisplayPosition(newsId, displayPosition, accountName);
                        BoCached.Base.News.NewsDalFactory.UpdateDisplayPosition(newsId, displayPosition, accountName);

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes UpdatePriority(long newsId, int priority, string accountName)
        {
            try
            {
                var newsInfo = NewsDal.GetNewsById(newsId);
                if (newsInfo == null) return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                else
                {
                    if (NewsDal.UpdatePriority(newsId, priority, accountName))
                    {
                        BoSearch.Base.News.NewsDalFactory.UpdatePriority(newsId, priority, accountName);
                        BoCached.Base.News.NewsDalFactory.UpdatePriority(newsId, priority, accountName);

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                        return ErrorMapping.ErrorCodes.UnknowError;
                }
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static bool RemoveNewsPosition(int configPosition, int zoneId, int typeId, string accountName)
        {
            try
            {                
                return NewsDal.RemoveNewsPosition(configPosition, zoneId, typeId, accountName);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        #endregion

        #region Status flow

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <param name="receiver"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Send(long newsId, string userForUpdateAction, bool isSendOver, string receiver = "")
        {
            //test
            //IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter, existsNews.ListZoneId, user);

            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {                
                return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);//NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }

            // Trạng thái hiện tại không được phép gửi
            if (existsNews.Status != (int)NewsStatus.Temporary &&
                existsNews.Status != (int)NewsStatus.ReceivedForEdit &&
                existsNews.Status != (int)NewsStatus.ReturnedToReporter &&
                existsNews.Status != (int)NewsStatus.ReceivedForPublish &&
                existsNews.Status != (int)NewsStatus.MovedToTrash && 
                existsNews.Status != (int)NewsStatus.ReturnedToMyEditor)
            {
                Logger.WriteLog(Logger.LogType.Debug, "Status News Error 3: " + newsId + "[" + existsNews.Status + "]");
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }

            // Kiểm tra người xử lý
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction);//UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                var userDb = UserDal.GetUserByUsername(userForUpdateAction);
                if (userDb == null)
                {
                    return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                }
                userCached = new BoCached.Entity.Security.UserCachedEntity();
                userCached.User = userDb;
                userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
            }
            var user = userCached.User;            
            var userPermissionsForUpdater = userCached.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);

            if (string.IsNullOrEmpty(existsNews.ListZoneId))
            {
                //loc lai zoneId cho bai cu
                if (existsNews.ZoneId <= 0)
                {
                    existsNews = NewsDal.GetNewsById(newsId);
                    if (existsNews != null && !string.IsNullOrEmpty(existsNews.ListZoneId))
                    {
                        var zoneIds = existsNews.ListZoneId.Split(',');
                        if (zoneIds.Count() > 0)
                            existsNews.ZoneId = Utility.ConvertToInt(zoneIds[0]);
                        existsNews.ListZoneId = string.Join(";", zoneIds);
                    }
                }
                else
                    existsNews.ListZoneId = existsNews.ZoneId.ToString();
            }

            // Trạng thái tin hiện tại là Lưu tạm (Temporary) và người gửi là phóng viên
            if ((existsNews.Status == (int)NewsStatus.Temporary || existsNews.Status == (int)NewsStatus.ReturnedToReporter) &&
                IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter, existsNews.ListZoneId, user))
            {
                // Người gửi là người viết bài? => Gửi lên
                if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    //check gửi vượt cấp
                    if (isSendOver && user.IsSendOver)
                    {
                        //gửi vượt cấp                        
                        return FuncSendOverWaitForPublish(newsId, existsNews, userForUpdateAction, receiver);
                    }
                    else
                    {
                        //gửi bình thường                 
                        return FuncWaitForEdit(newsId, existsNews, userForUpdateAction, receiver);
                    }
                }
            }
            // Trạng thái tin hiện tại là Nhận biên tập (ReceivedForEdit) hoặc (trạng thái hiện tại là Lưu tạm và người gửi là Biên tập viên)
            if (existsNews.Status == (int)NewsStatus.ReceivedForEdit ||
                ((existsNews.Status == (int)NewsStatus.Temporary || existsNews.Status == (int)NewsStatus.ReturnedToReporter) &&
                 IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor, existsNews.ListZoneId, user)))
            {
                // Người gửi là người biên tập? => Gửi đi
                if ((existsNews.EditedBy != null && existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                    (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase) &&
                     existsNews.Status == (int)NewsStatus.Temporary))
                {
                    return FuncWaitForPublish(newsId, existsNews, userForUpdateAction, receiver);
                }
            }
            
            // Trạng thái tin hiện tại là Nhận xuất bản (ReceivedForPublish) hoặc (trạng thái hiện tại là Lưu tạm và người gửi là thư ký)
            if (existsNews.Status == (int)NewsStatus.ReceivedForPublish || 
                ((existsNews.Status == (int)NewsStatus.Temporary || existsNews.Status == (int)NewsStatus.ReturnedToReporter) && IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user)))
            {               
                // Người gửi là người nhận xuất bản? => Gửi đi
                if ((existsNews.ApprovedBy != null && existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                    (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase) &&
                     existsNews.Status == (int)NewsStatus.Temporary))
                {
                    return FuncWaitForEditorialBoard(newsId, existsNews, userForUpdateAction, receiver);
                }
            }

            // Đang là trạng thái xóa tạm
            if (existsNews.Status == (int)NewsStatus.MovedToTrash)
            {
                // 
                if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor,
                                         existsNews.ListZoneId, user) ||
                    IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                         existsNews.ListZoneId, user))
                {
                    if (NewsDal.ChangeStatusToWaitForPublish(newsId, existsNews.EditedBy))
                    {
                        //update redis
                        if (!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                        }
                        //update es
                        if (!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                        }

                        NewTaskRunAction(() =>
                        {                            
                            //topic chờ xuất bản
                            int topicId = 0;
                            DiscussionV2Bo.AutoTopicWaitPublish(newsId, existsNews.Title, userForUpdateAction, ref topicId);
                            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                                   (int)NewsStatus.WaitForPublish, false,
                                                                   "[" + userForUpdateAction +
                                                                   "] trả lại bài về kho chung chờ duyệt xuất bản");
                            if (existsNews.CreatedBy != existsNews.EditedBy)
                            {
                                NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                                       (int)NewsStatus.WaitForPublish, false,
                                                                       "[" + userForUpdateAction +
                                                                       "] trả lại bài về kho chung chờ duyệt xuất bản");
                            }
                            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.WaitForPublish, userForUpdateAction,"", existsNews.Title);
                            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài về kho chung chờ duyệt xuất bản", existsNews.Status, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);
                        });

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
                else
                {
                    if (NewsDal.ChangeStatusToWaitForEdit(newsId))
                    {
                        //update redis
                        if (!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEdit });
                        }
                        //update es
                        if (!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEdit });
                        }

                        NewTaskRunAction(() =>
                        {                            
                            //topic chờ biên tập
                            int topicId = 0;
                            DiscussionV2Bo.AutoTopicWaitEditor(newsId, existsNews.Title, userForUpdateAction, ref topicId);
                            NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                                   (int)NewsStatus.WaitForEdit, false,
                                                                   "[" + userForUpdateAction + "] vừa");
                            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.WaitForEdit, userForUpdateAction,"", existsNews.Title);
                            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài về nhận biên tập", existsNews.Status, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);
                        });

                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
            }

            // Đang là trạng thái bài trả lại tôi
            if (existsNews.Status == (int)NewsStatus.ReturnedToMyEditor)
            {
                if(IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user))
                {
                    Logger.WriteLog(Logger.LogType.Trace, "chinhnb2_" + NewtonJson.Serialize(userPermissionsForUpdater) + "_" + (int)EnumPermission.ArticleAdmin + "_" + existsNews.ListZoneId + "_" + NewtonJson.Serialize(user));
                    return FuncWaitForEditorialBoard(newsId, existsNews, userForUpdateAction, receiver);
                }
                if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor, existsNews.ListZoneId, user))
                {
                    return FuncWaitForPublish(newsId, existsNews, userForUpdateAction, receiver);
                }
                if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter, existsNews.ListZoneId, user))
                {
                    //check gửi vượt cấp
                    if (isSendOver && user.IsSendOver)
                    {
                        //gửi vượt cấp                        
                        return FuncSendOverWaitForPublish(newsId, existsNews, userForUpdateAction, receiver);
                    }
                    else
                    {
                        //gửi bình thường                 
                        return FuncWaitForEdit(newsId, existsNews, userForUpdateAction, receiver);
                    }
                }
            }

            var flg = ((existsNews.Status == (int)NewsStatus.Temporary || existsNews.Status == (int)NewsStatus.ReturnedToReporter) && IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleReporter, existsNews.ListZoneId, user));
            Logger.WriteLog(Logger.LogType.Trace, "chinhnb2_status_" + existsNews.Status + "_flg_" + flg);

            Logger.WriteLog(Logger.LogType.Trace, "chinhnb2_" + NewtonJson.Serialize(userPermissionsForUpdater) + "_" + (int)EnumPermission.ArticleReporter + "_" + existsNews.ListZoneId + "_" + NewtonJson.Serialize(user));

            // Không được phép gửi
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
        }

        #region func for action send
        private static ErrorMapping.ErrorCodes FuncWaitForEdit(long newsId, NewsEntity existsNews, string userForUpdateAction, string receiver)
        {
            // Gửi lên thư mục chung để chờ biên tập
            if (string.IsNullOrEmpty(receiver) ||
                        userForUpdateAction.Equals(receiver, StringComparison.CurrentCultureIgnoreCase))
            {
                if (NewsDal.ChangeStatusToWaitForEdit(newsId))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEdit });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2,TopicName.REDIS,new NewsEntity{ Id=newsId, LastModifiedBy= userForUpdateAction, Status= (int)NewsStatus.WaitForEdit});
                    }

                    NewTaskRunAction(() =>
                    {
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.WaitForEdit, userForUpdateAction,"", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài", existsNews.Status, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);

                        // Log hành động gửi bài viết
                        ActivityBo.LogSendNews(existsNews.Id, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);

                        //topic waitEditor
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicWaitEditor(newsId, existsNews.Title, existsNews.CreatedBy, ref topicId);                        
                    });

                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            var userCachedReceiver = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(receiver);
            // Không tìm thấy người nhận
            if (null == userCachedReceiver)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            // Gửi đích danh cho 1 người biên tập viên => Lấy quyền của người nhận
            var userPermissionsForReceiver = userCachedReceiver.PermissionList; //UserPermissionDal.GetListUserPermissionByUserName(receiver);
            var userForReceiver = userCachedReceiver.User; //UserBo.GetUserByUsername(receiver);
            // Người nhận không có quyền biên tập trong chuyên mục của bài này => không được phép gửi đích danh
            if (!IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleEditor, existsNews.ListZoneId, userForReceiver))
            {
                //Logger.WriteLog(Logger.LogType.Debug, "Send directly to someone: " + newsId + "[" + existsNews.Status + "]");
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            if (NewsDal.ChangeStatusToRecievedForEdit(newsId, receiver))
            {
                //update es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForEdit))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy = receiver, Status = (int)NewsStatus.ReceivedForEdit });
                }
                //update redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForEdit))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy = receiver, Status = (int)NewsStatus.ReceivedForEdit });
                }

                NewTaskRunAction(() =>
                {
                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                            (int)NewsStatus.ReceivedForEdit, false,
                                                            "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                            "] biên tập");
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.ReceivedForEdit, userForUpdateAction, receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài cho [" + receiver + "] biên tập", existsNews.Status, (int)NewsStatus.ReceivedForEdit, userForUpdateAction, receiver, existsNews.Title);
                    // Log hành động gửi bài viết
                    ActivityBo.LogSendNews(existsNews.Id, existsNews.Status, userForUpdateAction, "", existsNews.Title);
                    //Topic Nhập về biên tập
                    int topicId = 0;
                    DiscussionV2Bo.AutoTopicEditor(newsId, existsNews.Title, receiver, ref topicId);                    
                });

                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }
        private static ErrorMapping.ErrorCodes FuncWaitForPublish(long newsId, NewsEntity existsNews, string userForUpdateAction, string receiver)
        {
            // Gửi lên thư mục chung để chờ duyệt xuất bản
            if (string.IsNullOrEmpty(receiver) ||
                        userForUpdateAction.Equals(receiver, StringComparison.CurrentCultureIgnoreCase))
            {
                if (NewsDal.ChangeStatusToWaitForPublish(newsId, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.WaitForPublish, false,
                                                               "[" + userForUpdateAction +
                                                               "] gửi lên chờ duyệt xuất bản");
                       
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi lên", existsNews.Status, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);

                        // Log hành động gửi bài viết
                        ActivityBo.LogSendNews(existsNews.Id, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);

                        //Topic gửi lên chờ duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction + "] gửi lên chờ duyệt xuất bản";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                        
                    });

                    // Kết thúc log
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            var userCachedReceiver = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(receiver);
            // Không tìm thấy người nhận
            if (null == userCachedReceiver)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            // Gửi đích danh cho 1 người thư ký tòa soạn => Lấy quyền của người nhận
            var userPermissionsForReceiver = userCachedReceiver.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(receiver);
            var userForReceiver = userCachedReceiver.User;//UserBo.GetUserByUsername(receiver);
            // Người nhận không có quyền thư ký trong chuyên mục của bài này => không được phép gửi đích danh
            if (
                !IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleAdmin,
                                 existsNews.ListZoneId, userForReceiver))
            {
                Logger.WriteLog(Logger.LogType.Debug, "send directly to somebody: " + newsId + "[" + existsNews.Status + "]");
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            if (NewsDal.ChangeStatusToRecievedForPublish(newsId, userForUpdateAction, receiver))
            {
                //update es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForPublish))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, ApprovedBy = receiver, Status = (int)NewsStatus.ReceivedForPublish });
                }
                //update redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForPublish))
                {                    
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, ApprovedBy = receiver, Status = (int)NewsStatus.ReceivedForPublish });
                }

                NewTaskRunAction(() =>
                {
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish,userForUpdateAction, receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi sang nhờ duyệt xuất bản", existsNews.Status, (int)NewsStatus.ReceivedForPublish, userForUpdateAction, receiver, existsNews.Title);
                    // Log hành động gửi bài viết
                    ActivityBo.LogSendNews(existsNews.Id, existsNews.Status, userForUpdateAction, receiver, existsNews.Title);

                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                           (int)NewsStatus.ReceivedForPublish, false,
                                                           "[" + userForUpdateAction +
                                                           "] gửi sang nhờ duyệt xuất bản");
                    NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                           (int)NewsStatus.ReceivedForPublish, false,
                                                           "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] duyệt xuất bản");
                    //topic gửi lên chờ xuất bản
                    int topicId = 0;
                    string msg = "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] duyệt xuất bản";
                    DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                   
                });

                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        private static ErrorMapping.ErrorCodes FuncWaitForEditorialBoard(long newsId, NewsEntity existsNews, string userForUpdateAction, string receiver)
        {
            // Gửi lên thư mục chung để chờ tổng biên tập xuất bản
            if (string.IsNullOrEmpty(receiver) ||
                        userForUpdateAction.Equals(receiver, StringComparison.CurrentCultureIgnoreCase))
            {
                if (NewsDal.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction, (int)NewsStatus.WaitForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_14, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEditorialBoard });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction, (int)NewsStatus.WaitForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_14, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEditorialBoard });
                    }

                    NewTaskRunAction(() =>
                    {
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.WaitForEditorialBoard, userForUpdateAction, "", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài chờ tổng biên tập duyệt xuất bản", existsNews.Status, (int)NewsStatus.WaitForEditorialBoard, userForUpdateAction, "", existsNews.Title);

                        // Log hành động gửi bài viết
                        ActivityBo.LogSendNews(existsNews.Id, (int)NewsStatus.WaitForEditorialBoard, userForUpdateAction, "", existsNews.Title);

                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy, (int)NewsStatus.WaitForEditorialBoard, false, "[" + userForUpdateAction + "] gửi lên chờ tổng biên tập duyệt xuất bản");
                        
                        //Topic gửi lên chờ tổng biên tập duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction +
                                                               "] gửi lên chờ tổng biên tập duyệt xuất bản";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                        
                    });

                    // Kết thúc log
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            var userCachedReceiver = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(receiver);
            // Không tìm thấy người nhận
            if (null == userCachedReceiver)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            // Gửi đích danh cho 1 người tổng biên tập => Lấy quyền của người nhận
            var userPermissionsForReceiver = userCachedReceiver.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(receiver);
            var userForReceiver = userCachedReceiver.User;//UserBo.GetUserByUsername(receiver);
            // Người nhận không có quyền tổng biên tập trong chuyên mục của bài này => không được phép gửi đích danh
            if (
                !IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleAdmin,
                                 existsNews.ListZoneId, userForReceiver) && (userForReceiver.IsRole == (int)IsRole.EditorialBoard || userForReceiver.IsRole == (int)IsRole.Secretary))
            {
                //Logger.WriteLog(Logger.LogType.Debug, "send directly to somebody: " + newsId + "[" + existsNews.Status + "]");
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            if (NewsDal.ChangeStatusToRecievedForEditorialBoard(newsId, userForUpdateAction, receiver))
            {
                //update es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForEditorialBoard))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_16, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, PublishedBy=receiver, Status = (int)NewsStatus.ReceivedForEditorialBoard });
                }
                //update redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsId, userForUpdateAction, receiver, (int)NewsStatus.ReceivedForEditorialBoard))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_16, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, PublishedBy = receiver, Status = (int)NewsStatus.ReceivedForEditorialBoard });
                }

                NewTaskRunAction(() =>
                {
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.ReceivedForEditorialBoard, userForUpdateAction, receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi bài cho [" + receiver + "] tổng biên tập duyệt xuất bản", existsNews.Status, (int)NewsStatus.ReceivedForEditorialBoard, userForUpdateAction, receiver, existsNews.Title);
                    // Log hành động gửi bài viết
                    ActivityBo.LogSendNews(existsNews.Id, existsNews.Status, userForUpdateAction, receiver, existsNews.Title);

                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                           (int)NewsStatus.ReceivedForEditorialBoard, false,
                                                           "[" + userForUpdateAction +
                                                           "] gửi sang nhờ tổng biên tập duyệt xuất bản");
                    NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                           (int)NewsStatus.ReceivedForEditorialBoard, false,
                                                           "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] tổng biên tập duyệt xuất bản");
                    //topic gửi lên chờ tổng biên tập xuất bản
                    int topicId = 0;
                    string msg = "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] tổng biên tập duyệt xuất bản";
                    DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                    
                });

                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        private static ErrorMapping.ErrorCodes FuncSendOverWaitForPublish(long newsId, NewsEntity existsNews, string userForUpdateAction, string receiver)
        {
            // Gửi lên thư mục chung để chờ duyệt xuất bản
            if (string.IsNullOrEmpty(receiver) ||
                        userForUpdateAction.Equals(receiver, StringComparison.CurrentCultureIgnoreCase))
            {
                if (NewsDal.ChangeStatusToWaitForPublish(newsId, ""))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, "", (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = "", Status = (int)NewsStatus.WaitForPublish });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, "", (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = "", Status = (int)NewsStatus.WaitForPublish });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.WaitForPublish, false,
                                                               "[" + userForUpdateAction +
                                                               "] gửi lên chờ duyệt xuất bản");
                        
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi vượt cấp", existsNews.Status, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);

                        // Log hành động gửi bài viết
                        ActivityBo.LogSendNews(existsNews.Id, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);

                        //Topic gửi lên chờ duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction + "] gửi lên chờ duyệt xuất bản";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                        
                    });

                    // Kết thúc log
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            var userCachedReceiver = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(receiver);
            // Không tìm thấy người nhận
            if (null == userCachedReceiver)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            // Gửi đích danh cho 1 người thư ký tòa soạn => Lấy quyền của người nhận
            var userPermissionsForReceiver = userCachedReceiver.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(receiver);
            var userForReceiver = userCachedReceiver.User;//UserBo.GetUserByUsername(receiver);
            // Người nhận không có quyền thư ký trong chuyên mục của bài này => không được phép gửi đích danh
            if (
                !IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleAdmin,
                                 existsNews.ListZoneId, userForReceiver))
            {
                Logger.WriteLog(Logger.LogType.Debug, "send directly to somebody: " + newsId + "[" + existsNews.Status + "]");
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToSend;
            }
            if (NewsDal.ChangeStatusToRecievedForPublish(newsId, "", receiver))
            {
                //update es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, "", receiver, (int)NewsStatus.ReceivedForPublish))
                {                    
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = "", ApprovedBy= receiver, Status = (int)NewsStatus.ReceivedForPublish });
                }
                //update redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, "", receiver, (int)NewsStatus.ReceivedForPublish))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = "", ApprovedBy = receiver, Status = (int)NewsStatus.ReceivedForPublish });                    
                }

                NewTaskRunAction(() =>
                {
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish,userForUpdateAction, receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gửi sang nhờ duyệt xuất bản", existsNews.Status, (int)NewsStatus.ReceivedForPublish, userForUpdateAction, receiver, existsNews.Title);
                    // Log hành động gửi bài viết
                    ActivityBo.LogSendNews(existsNews.Id, existsNews.Status, userForUpdateAction, receiver, existsNews.Title);

                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                           (int)NewsStatus.ReceivedForPublish, false,
                                                           "[" + userForUpdateAction +
                                                           "] gửi sang nhờ duyệt xuất bản");
                    NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                           (int)NewsStatus.ReceivedForPublish, false,
                                                           "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] duyệt xuất bản");
                    //topic gửi lên chờ xuất bản
                    int topicId = 0;
                    string msg = "[" + userForUpdateAction + "] gửi cho [" + receiver +
                                                           "] duyệt xuất bản";
                    DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);                    
                });

                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        private static void NewTaskRunAction(System.Action action)
        {
            var context = HttpContext.Current;
            var task = Task.Run(() =>
            {
                HttpContext.Current = context;
                try
                {
                    action();
                }
                catch { }                
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }
        #endregion

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Receive(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if(existsNews==null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }            
            // Kiểm tra người xử lý
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction); //UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                var userDb= UserDal.GetUserByUsername(userForUpdateAction);                               
                if(userDb == null)
                {
                    return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                }                    
                userCached = new BoCached.Entity.Security.UserCachedEntity();
                userCached.User = userDb;
                userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
            }
            var user = userCached.User;
            // Lấy quyền của người nhận
            var userPermissionsForUpdater = userCached.PermissionList; //UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);

            if (existsNews.ListZoneId == "" || existsNews.ListZoneId == null)
            {
                //loc lai zoneId cho bai cu
                if (existsNews.ZoneId <= 0)
                {
                    existsNews = NewsDal.GetNewsById(newsId);
                    if (existsNews != null && !string.IsNullOrEmpty(existsNews.ListZoneId))
                    {
                        var zoneIds = existsNews.ListZoneId.Split(',');
                        if (zoneIds.Count() > 0)
                            existsNews.ZoneId = Utility.ConvertToInt(zoneIds[0]);
                        existsNews.ListZoneId = string.Join(";", zoneIds);                        
                    }
                }else
                    existsNews.ListZoneId = existsNews.ZoneId.ToString();
            }

            // Trạng thái hiện tại là Chờ biên tập (WaitForEdit)
            if (existsNews.Status == (int)NewsStatus.WaitForEdit || existsNews.Status == (int)NewsStatus.ReturnedToEditor)
            {
                // Người nhận không có quyền biên tập trong chuyên mục của bài này => không được phép nhận biên tập
                if (!(IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor, existsNews.ListZoneId, user)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
                }

                // Người nhận biên tập có quyền biên tập => Nhận biên tập
                if (NewsDal.ChangeStatusToRecievedForEdit(newsId, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsId, existsNews.LastModifiedBy, userForUpdateAction, (int)NewsStatus.ReceivedForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = existsNews.LastModifiedBy, EditedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForEdit });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEdit(newsId, existsNews.LastModifiedBy, userForUpdateAction, (int)NewsStatus.ReceivedForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = existsNews.LastModifiedBy, EditedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForEdit });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.ReceivedForEdit, false,
                                                               "[" + userForUpdateAction + "] nhận biên tập");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận bài", existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        //Topic Nhận về biên tập
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicEditor(newsId, existsNews.Title, userForUpdateAction, ref topicId);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title, existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }

            // Trạng thái hiện tại là Chờ duyệt xuất bản (WaitForPublish)
            if (existsNews.Status == (int)NewsStatus.WaitForPublish || existsNews.Status == (int)NewsStatus.ReturnedToEditorialSecretary || existsNews.Status == (int)NewsStatus.Unpublished)
            {
                // Người nhận không có quyền thư ký trong chuyên mục của bài này => không được phép duyệt xuất bản
                if (
                    !IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                     existsNews.ListZoneId, user))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
                }
                // Người nhận có quyền thư ký chuyên mục => nhận bài để duyệt xuất bản
                if (NewsDal.ChangeStatusToRecievedForPublish(newsId, existsNews.EditedBy, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, existsNews.EditedBy, userForUpdateAction, (int)NewsStatus.ReceivedForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = existsNews.EditedBy, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForPublish });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRecievedForPublish(newsId, existsNews.EditedBy, userForUpdateAction, (int)NewsStatus.ReceivedForPublish))
                    {                        
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = existsNews.EditedBy, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForPublish });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.ReceivedForPublish, false,
                                                               "[" + userForUpdateAction + "] nhận duyệt xuất bản");
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                               (int)NewsStatus.ReceivedForPublish, false,
                                                               "[" + userForUpdateAction + "] nhận duyệt xuất bản");
                        //Topic Nhận về duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction + "] nhận duyệt xuất bản";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);

                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận bài", existsNews.Status, (int)NewsStatus.ReceivedForPublish, "", userForUpdateAction, existsNews.Title);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title, existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }

            // Trạng thái hiện tại là Chờ tổng biên tập duyệt (WaitForEditorialBoard)
            if (existsNews.Status == (int)NewsStatus.WaitForEditorialBoard)
            {
                // Người nhận không có quyền thư ký trong chuyên mục của bài này => không được phép duyệt xuất bản
                if (!IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                     existsNews.ListZoneId, user) && (user.IsRole != (int)IsRole.EditorialBoard || user.IsRole == (int)IsRole.Secretary))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
                }
                // Người nhận có quyền thư ký chuyên mục => nhận bài để duyệt xuất bản
                if (NewsDal.ChangeStatusToRecievedForEditorialBoard(newsId, existsNews.ApprovedBy, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsId, existsNews.ApprovedBy, userForUpdateAction, (int)NewsStatus.ReceivedForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_16, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, PublishedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForEditorialBoard });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReceivedForEditorialBoard(newsId, existsNews.ApprovedBy, userForUpdateAction, (int)NewsStatus.ReceivedForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_16, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, PublishedBy = userForUpdateAction, Status = (int)NewsStatus.ReceivedForEditorialBoard });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.ReceivedForEditorialBoard, false,
                                                               "[" + userForUpdateAction + "] nhận duyệt xuất bản");
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.PublishedBy,
                                                               (int)NewsStatus.ReceivedForEditorialBoard, false,
                                                               "[" + userForUpdateAction + "] nhận duyệt xuất bản");
                        //Topic Nhận về duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction + "] nhận duyệt xuất bản";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg, ref topicId);

                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEditorialBoard, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận bài", existsNews.Status, (int)NewsStatus.ReceivedForEditorialBoard, "", userForUpdateAction, existsNews.Title);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title, existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            
            // Không có quyền nhận
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Release(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép nhả bài đã nhận
            if (existsNews.Status != (int)NewsStatus.ReceivedForEdit &&
                existsNews.Status != (int)NewsStatus.ReceivedForPublish &&
                existsNews.Status != (int)NewsStatus.ReceivedForEditorialBoard)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
            }
            // Kiểm tra người xử lý
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction);//UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                var userDb = UserDal.GetUserByUsername(userForUpdateAction);
                if (userDb == null)
                {
                    return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                }
                userCached = new BoCached.Entity.Security.UserCachedEntity();
                userCached.User = userDb;
                userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
            }
            // Không phải là người nhận bài này => Không được nhả bài 
            //if (
            //    !((existsNews.Status == (int)NewsStatus.ReceivedForEdit &&
            //       (!string.IsNullOrEmpty(existsNews.EditedBy) && existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))) ||
            //      (existsNews.Status == (int)NewsStatus.ReceivedForPublish &&
            //       (!string.IsNullOrEmpty(existsNews.ApprovedBy) && existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))) ||
            //       (existsNews.Status == (int)NewsStatus.ReceivedForEditorialBoard &&
            //       (!string.IsNullOrEmpty(existsNews.PublishedBy) && existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)))))
            //{
            //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
            //}
            // Nhả bài đã nhận
            if (existsNews.Status == (int)NewsStatus.ReceivedForEdit)
            {
                // Không phải là người nhận bài này => Không được nhả bài 
                if (!(!string.IsNullOrEmpty(existsNews.EditedBy) && existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
                }

                if (NewsDal.ChangeStatusToWaitForEdit(newsId))
                {
                    //update redis
                    if (!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEdit });
                    }
                    //update es
                    if (!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_2, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEdit });
                    }

                    NewTaskRunAction(() =>
                    {                        
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.WaitForEdit, false,
                                                               "[" + userForUpdateAction +
                                                               "] trả lại bài về kho chung chờ biên tập");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhả bài", existsNews.Status, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);
                    });

                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            //Nhả nhận xuất bản
            if (existsNews.Status == (int)NewsStatus.ReceivedForPublish)
            {
                // Không phải là người nhận bài này => Không được nhả bài 
                if (!(!string.IsNullOrEmpty(existsNews.ApprovedBy) && existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
                }

                //tam bo case nay => nhan xb => nha ra => cho xb
                //var user = userCached.User;
                //var userPermissionsForUpdater = userCached.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);                
                // nếu người xử lý là thư ký và nhận bài trực tiếp từ phóng viên thì nhả về kho chờ biên tập
                //if (string.IsNullOrEmpty(existsNews.EditedBy) && IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user))
                //{
                //    if (NewsDal.ChangeStatusToWaitForEdit(newsId))
                //    {
                //        NewTaskRunAction(() =>
                //        {
                //            //update redis
                //            BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit);
                //            //update es
                //            BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEdit(newsId, userForUpdateAction, (int)NewsStatus.WaitForEdit);

                //            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                //                                                   (int)NewsStatus.WaitForEdit, false,
                //                                                   "[" + userForUpdateAction +
                //                                                   "] trả lại bài về kho chung chờ biên tập");
                //            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);
                //            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhả bài", existsNews.Status, (int)NewsStatus.WaitForEdit, userForUpdateAction, "", existsNews.Title);
                //        });

                //        return ErrorMapping.ErrorCodes.Success;
                //    }
                //    else
                //    {
                //        return ErrorMapping.ErrorCodes.BusinessError;
                //    }
                //}
                //else // Ngược lại thì nhả về kho chờ xuất bản
                //{
                if (NewsDal.ChangeStatusToWaitForPublish(newsId, userForUpdateAction))
                {
                    //update redis
                    if (!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                    }
                    //update es
                    if (!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForPublish(newsId, userForUpdateAction, (int)NewsStatus.WaitForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_5, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForPublish });
                    }

                    NewTaskRunAction(() =>
                        {
                            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                                   (int)NewsStatus.WaitForPublish, false,
                                                                   "[" + userForUpdateAction +
                                                                   "] trả lại bài về kho chung chờ duyệt xuất bản");
                            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                                   (int)NewsStatus.WaitForPublish, false,
                                                                   "[" + userForUpdateAction +
                                                                   "] trả lại bài về kho chung chờ duyệt xuất bản");
                            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.WaitForPublish, userForUpdateAction,"", existsNews.Title);
                            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhả bài", existsNews.Status, (int)NewsStatus.WaitForPublish, userForUpdateAction, "", existsNews.Title);
                        });

                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
                //}
            }
            //Nhả nhận duyệt
            if (existsNews.Status == (int)NewsStatus.ReceivedForEditorialBoard)
            {
                // Không phải là người nhận bài này => Không được nhả bài 
                if (!(!string.IsNullOrEmpty(existsNews.PublishedBy) && existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
                }

                if (NewsDal.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction))
                {
                    //update redis
                    if (!BoCached.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction, (int)NewsStatus.WaitForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_14, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEditorialBoard });
                    }
                    //update es
                    if (!BoSearch.Base.News.NewsDalFactory.ChangeStatusToWaitForEditorialBoard(newsId, userForUpdateAction, (int)NewsStatus.WaitForEditorialBoard))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_14, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = userForUpdateAction, Status = (int)NewsStatus.WaitForEditorialBoard });
                    }

                    NewTaskRunAction(() =>
                    {                        
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.WaitForEditorialBoard, false,
                                                               "[" + userForUpdateAction +
                                                               "] trả lại bài về kho chung chờ duyệt");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.WaitForEditorialBoard, userForUpdateAction, "", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhả bài", existsNews.Status, (int)NewsStatus.WaitForEditorialBoard, userForUpdateAction, "", existsNews.Title);
                    });

                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }

            //không dc phép nhả bài
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToRelease;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="newsPositionTypeForHome"></param>
        /// <param name="newsPositionOrderForHome"></param>
        /// <param name="newsPositionTypeForList"></param>
        /// <param name="newsPositionOrderForList"></param>
        /// <param name="userForUpdateAction"></param>
        /// <param name="newsUrl"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Publish(long newsId, int newsPositionTypeForHome, int newsPositionOrderForHome,
                                         int newsPositionTypeForList, int newsPositionOrderForList,
                                         string userForUpdateAction, ref string newsUrl, string publishedContent)
        {
            try
            {

                if (newsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
                }
                if (string.IsNullOrEmpty(userForUpdateAction))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToPublish;
                }
                // Kiểm tra tin cần chuyển trạng thái                
                var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                if (null == existsNews)
                {
                    existsNews = NewsDal.GetNewsById(newsId);
                    if (existsNews == null)
                        return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
                }
                //check trung title khi xb
                var existTitle= BoCached.Base.News.NewsDalFactory.GetTitleNewsByKey(Utility.GenerateSHA256String(string.Format("{0}_{1}", existsNews?.Title, (int)NewsStatus.Published)));
                if (existTitle != null)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsTitleIsExisted;
                }

                // Trạng thái hiện tại không được phép xuất bản -> bo chỉ cần có quyền xuất bản là đc ko cần check status
                //if (existsNews.Status != (int)NewsStatus.Temporary &&
                //    existsNews.Status != (int)NewsStatus.ReceivedForEdit &&
                //    existsNews.Status != (int)NewsStatus.ReceivedForPublish &&
                //    existsNews.Status != (int)NewsStatus.ReceivedForEditorialBoard &&
                //    existsNews.Status != (int)NewsStatus.Published &&
                //    existsNews.Status != (int)NewsStatus.Unpublished)
                //{
                //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToPublish;
                //}
                // Kiểm tra người xử lý                
                var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction);
                // Không tìm thấy người xử lý
                if (null == userCached)
                {
                    var userDb = UserDal.GetUserByUsername(userForUpdateAction);
                    if (userDb == null)
                    {
                        return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                    }
                    userCached = new BoCached.Entity.Security.UserCachedEntity();
                    userCached.User = userDb;
                    userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
                }
                var user = userCached.User;
                // Lấy quyền của người nhận                
                var userPermissionsForUpdater = userCached.PermissionList; //BoCached.Base.Security.PermissionDalFactory.GetListByUserName(userForUpdateAction);
                // Người xử lý không có quyền thư ký trong chuyên mục của bài này => không được phép xuất bản
                if (!(user.IsFullPermission ||
                      (BoCached.Base.Security.PermissionDalFactory.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin) &&
                       user.IsFullZone) || IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user))
                      //&& (user.IsRole == (int)IsRole.EditorialBoard || user.IsRole == (int)IsRole.Secretary)
                      )
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToPublish;
                }

                if (string.IsNullOrEmpty(existsNews.Title))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle;
                }
                //if (string.IsNullOrEmpty(existsNews.Sapo) && existsNews.NewsType != (int)NewsType.SlidePhoto)
                //{
                //    return ErrorMapping.ErrorCodes.UpdateNewsInvalidSapo;
                //}
                if (string.IsNullOrEmpty(existsNews.Body) && existsNews.Type != (int)NewsType.SlidePhoto && existsNews.Type != (int)NewsType.BigStory)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidBody;
                }
                //if (string.IsNullOrEmpty(existsNews.Avatar))
                //{
                //    return ErrorMapping.ErrorCodes.UpdateNewsInvalidAvatar;
                //}
                //if (string.IsNullOrEmpty(existsNews.TagItem))
                //{
                //    return ErrorMapping.ErrorCodes.UpdateNewsInvalidTag;
                //}
                newsUrl = existsNews.Url;

                /* publish bài
                 */
                if (NewsDal.ChangeStatusToPublished(newsId, newsPositionTypeForHome, newsPositionOrderForHome,
                                                    newsPositionTypeForList, newsPositionOrderForList, userForUpdateAction,
                                                    Utility.SetPublishedDate(existsNews.DistributionDate), publishedContent))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToPublished(newsId, userForUpdateAction, (int)NewsStatus.Published))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8,TopicName.ES, new NewsEntity { Id = newsId, PublishedBy = userForUpdateAction, Status = (int)NewsStatus.Published });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToPublished(newsId, newsPositionTypeForHome, newsPositionOrderForHome,
                                                newsPositionTypeForList, newsPositionOrderForList, userForUpdateAction,
                                                Utility.SetPublishedDate(existsNews.DistributionDate), publishedContent))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_8, TopicName.REDIS, new NewsEntity { Id = newsId, PublishedBy = userForUpdateAction,DistributionDate= existsNews.DistributionDate, Body = publishedContent, Status = (int)NewsStatus.Published });
                    }

                    NewTaskRunAction(() =>
                    {
                        try
                        {
                            var primaryZoneId = 0;
                            var allZoneIdIncludePrimary = "";
                            var newsInZone = BoCached.Base.News.NewsDalFactory.GetNewsInZoneByNewsId(newsId);
                            if (newsInZone != null && newsInZone.Count > 0)
                            {
                                foreach (var zone in newsInZone)
                                {
                                    if (zone.IsPrimary)
                                    {
                                        primaryZoneId = zone.ZoneId;
                                    }
                                    allZoneIdIncludePrimary += "," + zone.ZoneId;
                                }
                                if (!string.IsNullOrEmpty(allZoneIdIncludePrimary))
                                    allZoneIdIncludePrimary = allZoneIdIncludePrimary.Remove(0, 1);
                            }
                            BoFactory.GetInstance<NewsPositionBo>().UpdateNewsIntoAutoUpdatePosition(existsNews, primaryZoneId, allZoneIdIncludePrimary);

                            Logger.WriteLog(Logger.LogType.Debug,"NewsPosition => " + newsPositionTypeForHome + "-" + newsPositionOrderForHome + "-" + newsPositionTypeForList + "-" + newsPositionOrderForList);
                            if (newsPositionTypeForHome > 0 && newsPositionOrderForHome > 0)
                            {
                                var data = new NewsPositionScheduleEntity()
                                {
                                    NewsId = existsNews.Id,
                                    Title = existsNews.Title,
                                    Avatar = existsNews.Avatar,
                                    PositionTypeId = newsPositionTypeForHome,
                                    ZoneId = 0,
                                    Order = newsPositionOrderForHome,
                                    ScheduleDate = existsNews.DistributionDate,
                                    //existsNews.DistributionDate > DateTime.Now
                                    //    ? existsNews.DistributionDate
                                    //    : DateTime.Now,
                                    ProcessedDate = DateTime.Now,
                                    CreatedBy = userForUpdateAction,
                                    CreatedDate = DateTime.Now,
                                    LastModifiedBy = userForUpdateAction,
                                    LastModifiedDate = DateTime.Now,
                                    Status = 1
                                };
                                var a = BoFactory.GetInstance<NewsPositionBo>()
                                    .InserNewsPositionSchedule(data);
                                Logger.WriteLog(Logger.LogType.Debug, "InserNewsPositionScheduleForHome[" + NewtonJson.Serialize(data) + "] => " + NewtonJson.Serialize(a));
                            }
                            if (newsPositionTypeForList > 0 && newsPositionOrderForList > 0)
                            {
                                var data = new NewsPositionScheduleEntity()
                                {
                                    NewsId = existsNews.Id,
                                    Title = existsNews.Title,
                                    Avatar = existsNews.Avatar,
                                    PositionTypeId = newsPositionTypeForList,
                                    ZoneId = primaryZoneId,
                                    Order = newsPositionOrderForList,
                                    ScheduleDate =
                                        existsNews.DistributionDate > DateTime.Now
                                            ? existsNews.DistributionDate
                                            : DateTime.Now,
                                    ProcessedDate = DateTime.Now,
                                    CreatedBy = userForUpdateAction,
                                    CreatedDate = DateTime.Now,
                                    LastModifiedBy = userForUpdateAction,
                                    LastModifiedDate = DateTime.Now,
                                    Status = 1
                                };
                                var a = BoFactory.GetInstance<NewsPositionBo>()
                                    .InserNewsPositionSchedule(data);
                                Logger.WriteLog(Logger.LogType.Debug, "InserNewsPositionScheduleForHome[" + NewtonJson.Serialize(data) + "] => " + NewtonJson.Serialize(a));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Logger.LogType.Fatal, "Publish => UpdateNewsIntoAutoUpdatePosition(" + NewtonJson.Serialize(existsNews) + ") error => " + ex);
                        }
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.Published, false,
                                                               "[" + userForUpdateAction + "] đã xuất bản bài");
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                               (int)NewsStatus.Published, false,
                                                               "[" + userForUpdateAction + "] đã xuất bản bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.Published, userForUpdateAction, "", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Xuất bản bài", existsNews.Status, (int)NewsStatus.Published, userForUpdateAction, "", existsNews.Title);
                        try
                        {
                            //topic xuất bản
                            int topicId = 0;
                            DiscussionV2Bo.AutoTopicPublish(newsId, existsNews.Title, userForUpdateAction, ref topicId);
                        }
                        catch (Exception ex3)
                        {
                            Logger.WriteLog(Logger.LogType.Fatal, ex3.ToString());
                        }
                        ////NewsChildPublish
                        var listNewsChildPublish = NewsChildPublishDal.GetNewsChildPublishByNewsId(existsNews.Id);
                        if (listNewsChildPublish == null || (listNewsChildPublish != null && listNewsChildPublish.Count == 0))
                        {
                            var listNewsChild = NewsChildDal.GetByNewsId(existsNews.Id, false);
                            if (listNewsChild != null && listNewsChild.Count > 0)
                            {
                                foreach (var item in listNewsChild)
                                {
                                    var newsChildPublish = new NewsChildPublishEntity
                                    {
                                        NewsParentId = item.NewsParentId,
                                        Order = item.Order,
                                        Title = item.Title,
                                        Body = item.Body
                                    };
                                    NewsChildPublishDal.InsertNewsChildPublish(newsChildPublish);
                                }
                            }
                        }
                        // Log hành động xuất bản bản
                        ActivityBo.LogPublishNews(newsId, userForUpdateAction, existsNews.Title, existsNews.Status);

                        //CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(newsId.ToString());
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Unpublish(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish;
            }
            // Kiểm tra tin cần chuyển trạng thái                
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép gỡ xuất bản
            if (existsNews.Status != (int)NewsStatus.Published)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish;
            }
            // Kiểm tra người xử lý            
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                var userDb = UserDal.GetUserByUsername(userForUpdateAction);
                if (userDb == null)
                {
                    return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                }
                userCached = new BoCached.Entity.Security.UserCachedEntity();
                userCached.User = userDb;
                userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
            }
            // Không phải là người xuất bản bài này => Không được gỡ bài
            //if (!existsNews.PublishedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
            //{
            //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish;
            //}
            var user = userCached.User;
            // Lấy quyền của người xử lý            
            var userPermissionsForUpdater = userCached.PermissionList;//BoCached.Base.Security.PermissionDalFactory.GetListByUserName(userForUpdateAction);
            // Người xử lý không có quyền thư ký trong chuyên mục của bài này => không được phép gỡ xuất bản
            if (!(user.IsFullPermission || (BoCached.Base.Security.PermissionDalFactory.CheckUserInGroupPermission(user.Id, (int)EnumPermission.ArticleAdmin) && user.IsFullZone) 
                || IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user) 
                //&& (user.IsRole != (int)IsRole.EditorialBoard || user.IsRole != (int)IsRole.Secretary)
                )                  
            )
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToUnpublish;
            }
            try
            {
                var listPosition = BoFactory.GetInstance<NewsPositionBo>().GetAllPositionHasNewsId(newsId);
                List<long> positionIds = listPosition.Select(n => n.Id).ToList();
                // Gỡ xuất bản
                if (NewsDal.ChangeStatusToUnpublished(newsId, userForUpdateAction))
                {
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToUnPublished(newsId, userForUpdateAction, (int)NewsStatus.Unpublished))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_10, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Unpublished });
                    }
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToUnPublished(newsId, userForUpdateAction, (int)NewsStatus.Unpublished))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_10, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Unpublished });
                    }

                    try
                    {
                        BoFactory.GetInstance<NewsPositionBo>().RemoveNewsFromPosition(newsId, positionIds);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "RemoveNewsFromPosition(" + newsId + ") error => " + ex);
                    }

                    NewTaskRunAction(() =>
                    {
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.Unpublished,userForUpdateAction, "", existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Gỡ bài", existsNews.Status, (int)NewsStatus.Unpublished, userForUpdateAction, "", existsNews.Title);
                        try
                        {
                            // Log hành động gỡ bài viết
                            ActivityBo.LogUnPublishNews(newsId, userForUpdateAction, existsNews.Title, existsNews.Status);
                            // Đóng log;
                        }
                        catch (Exception ex)
                        {
                            // Lỗi cũng phải cho ra ngoài
                            // Để còn đồng bộ với mobile
                        }

                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,
                                                               (int)NewsStatus.Unpublished, false,
                                                               "[" + userForUpdateAction + "] đã gỡ xuất bản bài");
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                               (int)NewsStatus.Unpublished, false,
                                                               "[" + userForUpdateAction + "] đã gỡ xuất bản bài");
                        //topic & send discussion unpublish
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicUnPublish(newsId, existsNews.Title, userForUpdateAction, ref topicId);                        
                    });
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            catch (Exception exception)
            {
                Logger.WriteLog(Logger.LogType.Trace, exception.ToString());
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReturnToCooperator(long newsId, string userForUpdateAction, string note)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
            }
            // Kiểm tra tin cần chuyển trạng thái                
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép trả lại
            if (existsNews.Status != (int)NewsStatus.Temporary &&
                existsNews.Status != (int)NewsStatus.ReceivedForEdit &&
                existsNews.Status != (int)NewsStatus.ReceivedForPublish &&
                existsNews.Status != (int)NewsStatus.Unpublished)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
            }
            // Nếu không phải người này nhận bài về thì không được quyền trả lại Cộng tác viên
            if (existsNews.CreatedBy != userForUpdateAction)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
            }
            // Kiểm tra người xử lý
            var user = UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
            }
            if (NewsDal.ChangeStatusToReturnToCooperator(newsId, note))
            {
                //update redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnToCooperator))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_11, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction,Note=note, Status = (int)NewsStatus.ReturnToCooperator });
                }
                //update es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnToCooperator(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnToCooperator))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_11, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnToCooperator });
                }

                //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnToCooperator, "",userForUpdateAction, existsNews.Title);
                NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả về cộng tác viên", existsNews.Status, (int)NewsStatus.ReturnToCooperator, "", userForUpdateAction, existsNews.Title);
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            // Không có quyền trả lại
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <param name="receiver"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Return(long newsId, string userForUpdateAction, bool isSendOver, string receiver = "", string note = "")
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);//NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            //check neu khong nhap note lay note cu
            if (string.IsNullOrEmpty(note?.Trim()))
            {
                note = existsNews.Note;
            }
            // Trạng thái hiện tại không được phép trả lại
            if (existsNews.Status == (int)NewsStatus.Temporary ||
                existsNews.Status == (int)NewsStatus.ReturnedToReporter ||
                existsNews.Status == (int)NewsStatus.Published ||
                existsNews.Status == (int)NewsStatus.MovedToTrash ||
                existsNews.Status == (int)NewsStatus.ReturnToCooperator)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
            }
            // Kiểm tra người xử lý
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction);//UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                var userDb = UserDal.GetUserByUsername(userForUpdateAction);
                if (userDb == null)
                {
                    return ErrorMapping.ErrorCodes.KhongTimThayNguoiXuLy;
                }
                userCached = new BoCached.Entity.Security.UserCachedEntity();
                userCached.User = userDb;
                userCached.PermissionList = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);
            }
            var user = userCached.User;
            var userPermissionsForUpdater = userCached.PermissionList;//UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);

            // Trạng thái tin hiện tại là nhận biên tập (ReceivedForEdit)
            if (existsNews.Status == (int)NewsStatus.ReceivedForEdit || existsNews.Status == (int)NewsStatus.WaitForEdit || existsNews.Status == (int)NewsStatus.ReturnedToEditor)
            {
                if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturnYourself;
                }
                else
                {                    
                    if (NewsDal.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note))
                    {
                        //update es
                        if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_4, TopicName.ES, new NewsEntity { Id = newsId, ReturnedBy = userForUpdateAction, Note=note, Status = (int)NewsStatus.ReturnedToReporter });
                        }
                        //update redis
                        if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_4, TopicName.REDIS, new NewsEntity { Id = newsId, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToReporter });
                        }

                        NewTaskRunAction(() =>
                        {
                            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy, (int)NewsStatus.ReturnedToReporter, false, "[" + userForUpdateAction + "] đã trả lại bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToReporter,userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToReporter, userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                        //auto topic
                        int topicId = 0;
                            DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả về cho phóng viên", ref topicId);
                        // Log hành động trả bài
                        ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.CreatedBy, existsNews.Title, (int)NewsStatus.ReturnedToReporter);
                        });
                        // Đóng log;
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }                    
                }                             
            }
            // Trạng thái tin hiện tại là Nhận duyệt xuất bản (ReceivedForPublish) hoặc bị gỡ xuống (Unpublished)
            if (existsNews.Status == (int)NewsStatus.ReceivedForPublish || existsNews.Status == (int)NewsStatus.WaitForPublish ||
                existsNews.Status == (int)NewsStatus.Unpublished || existsNews.Status == (int)NewsStatus.ReturnedToEditorialSecretary)
            {
                // Người trả lại là người nhận duyệt xuất bản? => Trả về cho biên tập viên                
                if (string.IsNullOrEmpty(receiver))
                {
                    //if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturnYourself;
                    //}
                    //else
                    //{                                               
                        if (existsNews.EditedBy == existsNews.ApprovedBy && existsNews.CreatedBy != existsNews.EditedBy &&
                            IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user))
                        {
                        //if (NewsDal.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note))
                        //{
                        //    //update redis
                        //    BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter);
                        //    //update es
                        //    BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter);

                        //    NewTaskRunAction(() =>
                        //        {
                        //            NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy,(int)NewsStatus.ReturnedToReporter,false,"[" + userForUpdateAction + "] đã trả lại bài");
                        //            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToReporter,userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                        //            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToReporter, userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                        //            //auto topic
                        //            int topicId = 0;
                        //            DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả bài về kho chờ biên tập", ref topicId);
                        //            // Log hành động trả bài
                        //            ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.LastReceiver, existsNews.Title, (int)NewsStatus.ReturnedToReporter);
                        //        });
                        //    // Đóng log;
                        //    return ErrorMapping.ErrorCodes.Success;
                        //}
                        //else
                        //{
                        //    return ErrorMapping.ErrorCodes.BusinessError;
                        //}

                            //trả bài về kho chờ biên tập
                            if (NewsDal.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note))
                            {
                                //update redis
                                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditor))
                                {
                                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_7, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy=existsNews.EditedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditor });
                                }
                                //update es
                                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditor))
                                {
                                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_7, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = existsNews.EditedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditor });
                                }

                                NewTaskRunAction(() =>
                                {
                                    NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                                           (int)NewsStatus.ReturnedToEditor,
                                                                           false,
                                                                           "[" + userForUpdateAction +
                                                                           "] đã trả lại bài");
                                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditor,userForUpdateAction, existsNews.EditedBy, existsNews.Title);
                                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToEditor, userForUpdateAction, existsNews.EditedBy, existsNews.Title);
                                    //auto topic
                                    int topicId = 0;
                                    DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả bài về kho chờ biên tập", ref topicId);
                                    // Log hành động trả bài
                                    ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.LastReceiver, existsNews.Title, (int)NewsStatus.ReturnedToEditor);
                                });
                                // Đóng log;
                                return ErrorMapping.ErrorCodes.Success;
                            }
                            else
                            {
                                return ErrorMapping.ErrorCodes.BusinessError;
                            }
                        }
                        else
                        {
                            if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user) && (user.IsRole == (int)IsRole.EditorialBoard || user.IsRole == (int)IsRole.Secretary))
                            {
                                if (NewsDal.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note))
                                {
                                    //update redis
                                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditorialSecretary))
                                    {
                                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditorialSecretary });
                                    }
                                    //update es
                                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditorialSecretary))
                                    {
                                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditorialSecretary });
                                    }

                                    NewTaskRunAction(() =>
                                    {
                                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.ApprovedBy, (int)NewsStatus.ReturnedToEditorialSecretary, false, "[" + userForUpdateAction + "] đã trả lại bài");
                                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditorialSecretary,userForUpdateAction, existsNews.ApprovedBy, existsNews.Title);
                                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToEditorialSecretary, userForUpdateAction, existsNews.ApprovedBy, existsNews.Title);
                                        //auto topic
                                        int topicId = 0;
                                        DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả bài về kho chờ biên tập", ref topicId);
                                        // Log hành động trả bài
                                        ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.LastReceiver, existsNews.Title, (int)NewsStatus.ReturnedToEditorialSecretary);
                                    });
                                    // Đóng log;
                                    return ErrorMapping.ErrorCodes.Success;
                                }
                                else
                                {
                                    return ErrorMapping.ErrorCodes.BusinessError;
                                }
                            }
                            else {
                                //trả bài về kho chờ biên tập
                                if (NewsDal.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note))
                                {
                                    //update redis
                                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditor))
                                    {
                                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_7, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = existsNews.EditedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditor });
                                    }
                                    //update es
                                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, existsNews.EditedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditor))
                                    {
                                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_7, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = existsNews.EditedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditor });
                                    }

                                    NewTaskRunAction(() =>
                                    {
                                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                                               (int)NewsStatus.ReturnedToEditor,
                                                                               false,
                                                                               "[" + userForUpdateAction +
                                                                               "] đã trả lại bài");
                                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditor,userForUpdateAction, existsNews.EditedBy, existsNews.Title);
                                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToEditor, userForUpdateAction, existsNews.EditedBy, existsNews.Title);
                                        //auto topic
                                        int topicId = 0;
                                        DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả bài về kho chờ biên tập", ref topicId);
                                        // Log hành động trả bài
                                        ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.LastReceiver, existsNews.Title, (int)NewsStatus.ReturnedToEditor);
                                    });
                                    // Đóng log;
                                    return ErrorMapping.ErrorCodes.Success;
                                }
                                else
                                {
                                    return ErrorMapping.ErrorCodes.BusinessError;
                                }
                            }
                        }
                    //}
                }
                //tra vuot cap cho pv
                if (isSendOver && !string.IsNullOrEmpty(receiver))
                {
                    if (NewsDal.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note))
                    {
                        //update redis
                        if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_4, TopicName.REDIS, new NewsEntity { Id = newsId, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToReporter });
                        }
                        //update es
                        if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToReporter(newsId, userForUpdateAction, note, (int)NewsStatus.ReturnedToReporter))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_4, TopicName.ES, new NewsEntity { Id = newsId, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToReporter });
                        }

                        NewTaskRunAction(() =>
                        {
                            NewsNotificationDal.UpdateNotification(existsNews.Id, userForUpdateAction,(int)NewsStatus.ReturnedToReporter, false,"[" + userForUpdateAction + "] đã trả lại bài");
                            //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToReporter, userForUpdateAction,receiver, existsNews.Title);
                            NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToReporter, userForUpdateAction, receiver, existsNews.Title);
                            // Log hành động trả bài
                            ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, receiver, existsNews.Title, (int)NewsStatus.ReturnedToReporter);
                        });
                        // Đóng log;
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
                // Trả về đích danh cho 1 người biên tập viên => Lấy quyền của người nhận
                var userPermissionsForReceiver = UserPermissionDal.GetListUserPermissionByUserName(receiver);
                var userForReceiver = UserBo.GetUserByUsername(receiver);
                // Người nhận không có quyền biên tập trong chuyên mục của bài này => không được phép gửi đích danh
                if (!IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleEditor, existsNews.ListZoneId, userForReceiver))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
                }
                if (NewsDal.ChangeStatusToReturnedToMyEditor(newsId, receiver, userForUpdateAction, note))
                {
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, receiver, userForUpdateAction, note, (int)NewsStatus.ReturnedToMyEditor))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_20, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = receiver, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToMyEditor });
                    }
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditor(newsId, receiver, userForUpdateAction, note, (int)NewsStatus.ReturnedToMyEditor))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_20, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = receiver, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToMyEditor });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,(int)NewsStatus.ReturnedToMyEditor, false, "[" + userForUpdateAction + "] đã trả lại bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditor, userForUpdateAction,receiver, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToMyEditor, userForUpdateAction, receiver, existsNews.Title);
                        // Log hành động trả bài
                        ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.LastReceiver, existsNews.Title, (int)NewsStatus.ReturnedToMyEditor);
                    });
                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }                
            }
            // Trạng thái tin hiện tại là tổng biên tập nhận duyệt (ReceivedForEditorialBoard)
            if (existsNews.Status == (int)NewsStatus.ReceivedForEditorialBoard || existsNews.Status == (int)NewsStatus.WaitForEditorialBoard)
            {
                if (string.IsNullOrEmpty(receiver))
                {
                    // Người trả lại là tổng biên tập nhận duyệt bài này? => Trả về cho thư ký                
                    //if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturnYourself;
                    //}
                    //else
                    //{
                        if (NewsDal.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note))
                        {
                            //update redis
                            if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditorialSecretary))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditorialSecretary });
                            }
                            //update es
                            if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, existsNews.ApprovedBy, userForUpdateAction, note, (int)NewsStatus.ReturnedToEditorialSecretary))
                            {
                                QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = existsNews.ApprovedBy, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToEditorialSecretary });
                            }
                            NewTaskRunAction(() =>
                            {
                                NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.ApprovedBy, (int)NewsStatus.ReturnedToEditorialSecretary, false, "[" + userForUpdateAction + "] đã trả lại bài");
                                //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditorialSecretary,userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                                NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToEditorialSecretary, userForUpdateAction, existsNews.ApprovedBy, existsNews.Title);
                                //auto topic
                                int topicId = 0;
                                DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả về cho thư ký", ref topicId);
                                // Log hành động trả bài
                                ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.ApprovedBy, existsNews.Title, (int)NewsStatus.ReturnedToEditorialSecretary);
                            });
                            // Đóng log;
                            return ErrorMapping.ErrorCodes.Success;
                        }
                        else
                        {
                            return ErrorMapping.ErrorCodes.BusinessError;
                        }
                    //}
                }
                //bbt Trả về đích danh cho 1 người tk => Lấy quyền của người nhận
                
                //var userPermissionsForReceiver = UserPermissionDal.GetListUserPermissionByUserName(receiver);
                //var userForReceiver = UserBo.GetUserByUsername(receiver);
                // Người nhận không có quyền tk trong chuyên mục của bài này => không được phép gửi đích danh
                //if (!IsHasPermission(userPermissionsForReceiver, EnumPermission.ArticleAdmin, existsNews.ListZoneId, userForReceiver))
                //{
                //    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
                //}

                if (NewsDal.ChangeStatusToReturnedToMyEditorialSecretary(newsId, receiver, userForUpdateAction, note))
                {
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, receiver, userForUpdateAction, note, (int)NewsStatus.ReturnedToMyEditor))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = receiver, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToMyEditor });
                    }
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToReturnedToEditorialSecretary(newsId, receiver, userForUpdateAction, note, (int)NewsStatus.ReturnedToMyEditor))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_19, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = receiver, ReturnedBy = userForUpdateAction, Note = note, Status = (int)NewsStatus.ReturnedToMyEditor });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, receiver, (int)NewsStatus.ReturnedToMyEditor, false, "[" + userForUpdateAction + "] đã trả lại bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReturnedToEditorialSecretary,userForUpdateAction, existsNews.CreatedBy, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Trả lại bài", existsNews.Status, (int)NewsStatus.ReturnedToMyEditor, userForUpdateAction, receiver, existsNews.Title);
                        //auto topic
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicReturn(newsId, existsNews.Title, userForUpdateAction, " đã trả về cho thư ký", ref topicId);
                        // Log hành động trả bài
                        ActivityBo.LogReturnNews(existsNews.Id, userForUpdateAction, existsNews.CreatedBy, existsNews.Title, (int)NewsStatus.ReturnedToMyEditor);
                    });
                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            // Không có quyền trả lại
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReturn;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes GetBack(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToGetBack;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép rút lại
            if (existsNews.Status != (int)NewsStatus.WaitForEdit &&
                existsNews.Status != (int)NewsStatus.WaitForPublish &&
                existsNews.Status != (int)NewsStatus.WaitForEditorialBoard)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToGetBack;
            }
            // Kiểm tra người xử lý
            var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(userForUpdateAction); //UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToGetBack;
            }
            // Trạng thái tin hiện tại là Chờ biên tập (WaitForEdit)
            if (existsNews.Status == (int)NewsStatus.WaitForEdit)
            {
                // Người rút về là người viết bài này? => Rút về
                if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (NewsDal.ChangeStatusToRecieveFromReturnStore(newsId))
                    {
                        //update redis
                        if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsId, (int)NewsStatus.Temporary))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.REDIS, new NewsEntity { Id = newsId, Status = (int)NewsStatus.Temporary });
                        }
                        //update es
                        if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsId, (int)NewsStatus.Temporary))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.ES, new NewsEntity { Id = newsId, Status = (int)NewsStatus.Temporary });
                        }
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Rút bài về", existsNews.Status, (int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
            }
            // Trạng thái tin hiện tại là Chờ duyệt xuất bản (WaitForPublish)
            if (existsNews.Status == (int)NewsStatus.WaitForPublish)
            {
                // Người rút về là người nhận biên tập bài này? => Rút về
                if (existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (NewsDal.ChangeStatusToRecieveFromReturnStore(newsId))
                    {
                        //update redis
                        if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsId, (int)NewsStatus.ReceivedForEdit))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.REDIS, new NewsEntity { Id = newsId, Status = (int)NewsStatus.ReceivedForEdit });
                        }
                        //update es
                        if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRecieveFromReturnStore(newsId, (int)NewsStatus.ReceivedForEdit))
                        {
                            QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.ES, new NewsEntity { Id = newsId, Status = (int)NewsStatus.ReceivedForEdit });
                        }

                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEdit, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Rút bài về", existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
            }
            // Không có quyền rút về
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToGetBack;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReceiveFromReturnStore(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
            }
            // Kiểm tra tin cần chuyển trạng thái                
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép nhận lại bài bị trả
            if (existsNews.Status != (int)NewsStatus.ReturnedToEditor &&
                existsNews.Status != (int)NewsStatus.ReturnedToReporter)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
            }
            // Kiểm tra người xử lý
            var user = UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
            }
            if (existsNews.Status == (int)NewsStatus.ReturnedToReporter)
            {
                // Người nhận về là người viết bài? => Nhận về
                if (existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (NewsDal.ChangeStatusToRecieveFromReturnStore(newsId))
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.EditedBy,
                                                               (int)NewsStatus.Temporary, false,
                                                               "[" + userForUpdateAction + "] đã nhận lại lại bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận lại bài", existsNews.Status, (int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
            }
            if (existsNews.Status == (int)NewsStatus.ReturnedToEditor)
            {
                // Người nhận về là người biên tập bài? => Nhận về
                if (existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (NewsDal.ChangeStatusToRecieveFromReturnStore(newsId))
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.PublishedBy,
                                                               (int)NewsStatus.ReceivedForEdit, false,
                                                               "[" + userForUpdateAction + "] đã nhận lại lại bài");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEdit, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận lại bài", existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        return ErrorMapping.ErrorCodes.Success;
                    }
                    else
                    {
                        return ErrorMapping.ErrorCodes.BusinessError;
                    }
                }
            }
            // Không được phép nhận về
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <param name="listZoneId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReceiveFromCooperatorStore(long newsId, string userForUpdateAction, string listZoneId)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator;
            }
            // Kiểm tra tin cần chuyển trạng thái                
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép nhận lại bài bị trả
            if (existsNews.Status != (int)NewsStatus.ReturnToCooperator)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator;
            }
            // Kiểm tra người xử lý
            var user = UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator;
            }

            // Lấy quyền của người nhận
            var userPermissionsForUpdater = UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);

            // Người nhận không có quyền thư ký trong chuyên mục của bài này => không được phép duyệt xuất bản
            if (
                !(IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                                 listZoneId, user) ||
                IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor,
                                 listZoneId, user)))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
            }
            if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin,
                listZoneId, user))
            {
                // Người nhận có quyền thư ký chuyên mục => nhận bài để duyệt xuất bản
                if (NewsDal.ChangeStatusToRecievedFromCooperatorStore(newsId, (int)NewsStatus.ReceivedForPublish, userForUpdateAction))
                {
                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, userForUpdateAction,
                        (int)NewsStatus.ReceivedForPublish, false,
                        "[" + userForUpdateAction + "] nhận duyệt xuất bản từ Cộng tác viên");
                        //Topic Nhận về duyệt xuất bản
                        int topicId = 0;
                        string msg = "[" + userForUpdateAction + "] nhận duyệt xuất bản từ Cộng tác viên";
                        DiscussionV2Bo.AutoTopicWaitApproval(newsId, existsNews.Title, userForUpdateAction, msg,
                            ref topicId);

                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận duyệt xuất bản từ Cộng tác viên", existsNews.Status, (int)NewsStatus.ReceivedForPublish, "", userForUpdateAction, existsNews.Title);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title,
                            existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            else if (IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleEditor,
                listZoneId, user))
            {
                // Người nhận biên tập có quyền biên tập => Nhận biên tập
                if (NewsDal.ChangeStatusToRecievedFromCooperatorStore(newsId, (int)NewsStatus.ReceivedForPublish, userForUpdateAction))
                {
                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, userForUpdateAction,
                                                           (int)NewsStatus.ReceivedForEdit, false,
                                                           "[" + userForUpdateAction + "] nhận biên tập từ Cộng tác viên");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEdit, "",userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Nhận duyệt xuất bản từ Cộng tác viên", existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        //Topic Nhận về biên tập
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicEditor(newsId, existsNews.Title, userForUpdateAction, ref topicId);
                        // Log hành động nhận bài viết
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title,
                                                  existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            // Không được phép nhận về
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromCooperator;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes MoveToTrash(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
            }
            // Kiểm tra tin cần chuyển trạng thái            
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép xóa tạm (xóa vào thùng rác)
            if (existsNews.Status != (int)NewsStatus.Temporary &&
                existsNews.Status != (int)NewsStatus.ReceivedForEdit &&
                existsNews.Status != (int)NewsStatus.ReturnedToReporter &&
                existsNews.Status != (int)NewsStatus.ReceivedForPublish &&
                existsNews.Status != (int)NewsStatus.ReturnedToEditor &&
                existsNews.Status != (int)NewsStatus.Unpublished &&
                existsNews.Status != (int)NewsStatus.ReturnedToEditorialSecretary &&
                existsNews.Status != (int)NewsStatus.ReceivedForEditorialBoard &&
                existsNews.Status != (int)NewsStatus.CrawlerNews &&
                existsNews.Status != (int)NewsStatus.CrawlerByVccorp &&
                existsNews.Status != (int)NewsStatus.ReturnedToMyEditor)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
            }
            // Kiểm tra người xử lý
            var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
            }
            if (existsNews.Status == (int)NewsStatus.Temporary ||
                existsNews.Status == (int)NewsStatus.ReturnedToReporter)
            {
                if (!existsNews.CreatedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
                }
            }
            if (existsNews.Status == (int)NewsStatus.ReceivedForEdit)
            {
                if (!existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
                }
            }
            if (existsNews.Status == (int)NewsStatus.ReceivedForPublish ||
                existsNews.Status == (int)NewsStatus.Unpublished ||
                existsNews.Status == (int)NewsStatus.ReturnedToEditorialSecretary)
            {
                // Lấy quyền của người nhận
                var userPermissionsForUpdater = BoCached.Base.Security.PermissionDalFactory.GetListByUserName(userForUpdateAction);
                if (!((existsNews.ApprovedBy != null && existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase)) ||
                    IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, existsNews.ListZoneId, user)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
                }
            }
            if (existsNews.Status == (int)NewsStatus.CrawlerNews || existsNews.Status == (int)NewsStatus.CrawlerByVccorp)
            {
                // Lấy quyền của người xoa
                var userPermissionsForUpdater = BoCached.Base.Security.PermissionDalFactory.GetListByUserName(userForUpdateAction);
                if (!(IsHasPermission(userPermissionsForUpdater, EnumPermission.ArticleAdmin, user) || IsHasPermission(userPermissionsForUpdater, EnumPermission.ManageCrawlerNews, user)))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
                }
            }

            if (NewsDal.ChangeStatusToMovedToTrash(newsId, userForUpdateAction))
            {
                //change redis
                if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(newsId, userForUpdateAction))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_9, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.MovedToTrash });
                }
                //change es
                if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToMovedToTrash(newsId, userForUpdateAction))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_9, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.MovedToTrash });
                }

                //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.MovedToTrash, userForUpdateAction,userForUpdateAction, existsNews.Title);
                NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Xóa bài", existsNews.Status, (int)NewsStatus.MovedToTrash, userForUpdateAction, userForUpdateAction, existsNews.Title);

                // Log hành động nhận bài viết
                ActivityBo.LogDeleteNews(existsNews.Id, userForUpdateAction, existsNews.Title, existsNews.Status);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReceiveFromTrash(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            // Trạng thái hiện tại không được phép phục hồi tin đã xóa
            if (existsNews.Status != (int)NewsStatus.MovedToTrash)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash;
            }
            // Kiểm tra người xử lý
            var user = BoCached.Base.Account.UserDalFactory.GetUserByUsername(userForUpdateAction);//UserDal.GetUserByUsername(userForUpdateAction);
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash;
            }
            if (!existsNews.LastModifiedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromTrash;
            }

            if (NewsDal.ChangeStatusToRestoreFromTrash(newsId, existsNews.LastModifiedBy))
            {
                if (existsNews.ApprovedBy != null && existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    //change redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.ReceivedForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy =existsNews.EditedBy, ApprovedBy=existsNews.ApprovedBy, Status = (int)NewsStatus.ReceivedForPublish });
                    }
                    //change es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.ReceivedForPublish))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_6, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy = existsNews.EditedBy, ApprovedBy = existsNews.ApprovedBy, Status = (int)NewsStatus.ReceivedForPublish });
                    }

                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish, "",userForUpdateAction, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Phục hồi bài", existsNews.Status, (int)NewsStatus.ReceivedForPublish, "", userForUpdateAction, existsNews.Title);
                }
                else if (existsNews.EditedBy != null && existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    //change redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.ReceivedForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy= existsNews.EditedBy, Status = (int)NewsStatus.ReceivedForEdit });
                    }
                    //change es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.ReceivedForEdit))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_3, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, EditedBy = existsNews.EditedBy, Status = (int)NewsStatus.ReceivedForEdit });
                    }

                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Phục hồi bài", existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                }
                else
                {
                    //change redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.Temporary))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Temporary });
                    }
                    //change es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToRestoreFromTrash(newsId, (int)NewsStatus.Temporary))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Temporary });
                    }

                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Phục hồi bài", existsNews.Status, (int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ReceiveCrawlerNews(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }

            // Kiểm tra người xử lý
            var userCached = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(userForUpdateAction); //UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == userCached)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
            }
            var user = userCached.User;
            // Lấy quyền của người nhận
            var userPermissionsForUpdater = userCached.PermissionList; //UserPermissionDal.GetListUserPermissionByUserName(userForUpdateAction);

            if (existsNews.ListZoneId == "" || existsNews.ListZoneId == null)
            {
                existsNews.ListZoneId = existsNews.ZoneId.ToString();
            }

            // Trạng thái hiện tại là tin crawler (CrawlerNews) and Vccorp
            if (existsNews.Status == (int)NewsStatus.CrawlerNews || existsNews.Status == (int)NewsStatus.CrawlerByVccorp)
            {
                if (NewsDal.ChangeStatusToCrawlerForEdit(newsId, userForUpdateAction))
                {
                    //update es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusToCrawlerForEdit(newsId, userForUpdateAction, (int)NewsStatus.Temporary))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.ES, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Temporary });
                    }
                    //update redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusToCrawlerForEdit(newsId, userForUpdateAction, (int)NewsStatus.Temporary))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_STATUS_1, TopicName.REDIS, new NewsEntity { Id = newsId, LastModifiedBy = userForUpdateAction, Status = (int)NewsStatus.Temporary });
                    }

                    NewTaskRunAction(() =>
                    {
                        NewsNotificationDal.UpdateNotification(existsNews.Id, existsNews.CreatedBy, (int)NewsStatus.Temporary, false, "[" + userForUpdateAction + "] lấy tin crawler");
                        //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status, (int)NewsStatus.ReceivedForEdit, "", userForUpdateAction, existsNews.Title);
                        NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Lấy tin crawler", existsNews.Status, (int)NewsStatus.Temporary, "", userForUpdateAction, existsNews.Title);
                        //Topic Temporary
                        int topicId = 0;
                        DiscussionV2Bo.AutoTopicEditor(newsId, existsNews.Title, userForUpdateAction, ref topicId);

                        //upload avatar
                        if (!string.IsNullOrEmpty(existsNews.Avatar) && existsNews.Avatar.StartsWith("http"))
                        {
                            PhotoNodeJsServices.UploadAvatar2(newsId, existsNews.Avatar, userForUpdateAction);
                        }

                        // Log hành động Temporary
                        ActivityBo.LogReceiveNews(existsNews.Id, userForUpdateAction, existsNews.Title, existsNews.Status);
                    });

                    // Đóng log;
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }

            // Không có quyền lấy
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:13
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="userForUpdateAction"></param>
        /// <param name="receiver"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes Forward(long newsId, string userForUpdateAction, string receiver = "")
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
            }
            // Kiểm tra tin cần chuyển trạng thái
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId); //NewsDal.GetNewsForValidateById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            // Trạng thái "Nhận biên tập" => kiểm tra người nhận chuyển tiếp có phải là biên tập viên chuyên mục này không?
            if (existsNews.Status == (int)NewsStatus.ReceivedForEdit)
            {
                // Người cập nhật không phải là người biên tập bài này bài => không được phép gửi bài này cho biên tập viên khác
                if (!existsNews.EditedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
                }

                // Người cập nhật là người biên tập bài này => kiểm tra người nhận bài có quyền biên tập
                var userPermissionsForReciever = BoCached.Base.Security.PermissionDalFactory.GetListByUserName(receiver);//UserPermissionDal.GetListUserPermissionByUserName(receiver);
                var userForReceiver = BoCached.Base.Account.UserDalFactory.GetUserByUsername(receiver);//UserBo.GetUserByUsername(receiver);
                // Người nhận không có quyền biên tập => không được phép chuyển bài cho người này biên tập
                if (
                    !IsHasPermission(userPermissionsForReciever, EnumPermission.ArticleEditor,
                                     existsNews.ListZoneId, userForReceiver))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
                }

                // Người nhận biên tập có quyền biên tập => gửi bài cho người biên tập
                if (NewsDal.ChangeStatusForwardToOtherEditor(newsId, receiver))
                {
                    //change redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(newsId, receiver))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_FORWARD_TO_EDITOR, TopicName.REDIS, new NewsEntity { Id = newsId, EditedBy = receiver });
                    }
                    //change es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherEditor(newsId, receiver))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_FORWARD_TO_EDITOR, TopicName.ES, new NewsEntity { Id = newsId, EditedBy = receiver });
                    }

                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                           (int)NewsStatus.ReceivedForEdit, false,
                                                           "[" + userForUpdateAction + "] đã gửi sang nhờ biên tập");
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForEdit, userForUpdateAction,receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Chuyển bài", existsNews.Status, (int)NewsStatus.ReceivedForEdit, userForUpdateAction, receiver, existsNews.Title);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            // Người xử lý nhận bài biên tập từ danh sách bài chờ biên tập => kiểm tra người xử lý có quyền biên tập?
            else if (existsNews.Status == (int)NewsStatus.ReceivedForPublish)
            {
                // Người cập nhật không phải là người nhận xuất bản bài này bài => không được phép gửi bài này cho thư ký khác
                if (existsNews.ApprovedBy != null && !existsNews.ApprovedBy.Equals(userForUpdateAction, StringComparison.CurrentCultureIgnoreCase))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
                }

                // Người cập nhật là người nhận xuất bản bài này => kiểm tra người nhận bài có quyền xuất bản
                var userPermissionsForReciever = BoCached.Base.Security.PermissionDalFactory.GetListByUserName(receiver);//UserPermissionDal.GetListUserPermissionByUserName(receiver);
                var userForReceiver = BoCached.Base.Account.UserDalFactory.GetUserByUsername(receiver);//UserBo.GetUserByUsername(receiver);
                // Người nhận không có quyền xuất bản => không được phép chuyển bài cho người này xuất bản
                if (
                    !IsHasPermission(userPermissionsForReciever, EnumPermission.ArticleAdmin,
                                     existsNews.ListZoneId, userForReceiver))
                {
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
                }

                // Người nhận xuất bản có quyền xuất bản => gửi bài cho người xuất bản
                if (NewsDal.ChangeStatusForwardToOtherSecretary(newsId, receiver))
                {
                    //change redis
                    if(!BoCached.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(newsId, receiver))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_FORWARD_TO_SECRETARY, TopicName.REDIS, new NewsEntity { Id = newsId, ApprovedBy = receiver });
                    }
                    //change es
                    if(!BoSearch.Base.News.NewsDalFactory.ChangeStatusForwardToOtherSecretary(newsId, receiver))
                    {
                        QueueManager.AddToQueue(ActionName.UPDATE_NEWS_FORWARD_TO_SECRETARY, TopicName.ES, new NewsEntity { Id = newsId, ApprovedBy = receiver });
                    }

                    NewsNotificationDal.UpdateNotification(existsNews.Id, receiver,
                                                           (int)NewsStatus.ReceivedForPublish, false,
                                                           "[" + userForUpdateAction +
                                                           "] đã gửi sang nhờ duyệt xuất bản");
                    //NewsHistoryBo.ChangeStatusFlow(existsNews.Id, existsNews.Status,(int)NewsStatus.ReceivedForPublish, userForUpdateAction,receiver, existsNews.Title);
                    NewsHistoryBo.ChangeStatusFlowWithActionName(existsNews.Id, "Chuyển bài", existsNews.Status, (int)NewsStatus.ReceivedForPublish, userForUpdateAction, receiver, existsNews.Title);
                    return ErrorMapping.ErrorCodes.Success;
                }
                else
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            else // Các trạng thái khác không được phép chuyển sang trạng thái này
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToForward;
            }
        }

        public static ErrorCodes RemoveNews(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorCodes.ChangeNewsStatusNotAllowToMoveToTrash;
            }

            // Kiểm tra tin cần chuyển trạng thái            
            var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
            if (null == existsNews)
            {
                existsNews = NewsDal.GetNewsById(newsId);
                if (existsNews == null)
                    return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }

            if (NewsDal.DeleteNews(newsId))
            {
                //change redis
                BoCached.Base.News.NewsDalFactory.DeleteNewsByNewsIdRedis(newsId.ToString());
                //change es
                BoSearch.Base.News.NewsDalFactory.DeleteNews(newsId);
                
                //NewsHistoryBo.ChangeStatusFlowWithActionName(newsId, "Xóa bài từ app bot", existsNews.Status, 9999, userForUpdateAction, userForUpdateAction, existsNews.Title);

                // Log hành động xóa bài viết từ app bot
                ActivityBo.LogDeleteNews(newsId, userForUpdateAction, existsNews.Title, existsNews.Status);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        #endregion

        #region Get news

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:23
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="currentUsername"></param>
        /// <returns></returns>
        public virtual NewsDetailForEditEntity GetNewsForEditByNewsId(long newsId, string currentUsername)
        {
            string newsVersionId = "";
            var newsDetail = new NewsDetailForEditEntity();
            try
            {
                newsDetail.NewsInfo = (newsId < 0 ? null : GetNewsByNewsId(newsId, currentUsername, ref newsVersionId));
                if (newsDetail.NewsInfo!=null)
                {
                    //lay bai tam su (parentid)
                    newsDetail.ParentNews = getParentNews(newsDetail.NewsInfo.ParentNewsId);                    
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, ex.Message);
            }
            
            var isAllowExpert = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Expert");            
            if (isAllowExpert==1) {
                try {
                    var expertnews = ExpertBo.GetExpertByNewsId(newsId);
                    if (expertnews != null)
                    {
                        newsDetail.ExpertId = expertnews.Id;
                        newsDetail.ExpertQuote = expertnews.Quote;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowVote = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Vote");            
            if (isAllowVote == 1)
            {
                try {
                    var vote = VoteBo.GetVoteByNewsId(newsId);
                    newsDetail.VoteInfo = vote;
                }
                catch(Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowTopic = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Topic");            
            if (isAllowTopic == 1)
            {
                try
                {
                    var topic = TopicBo.GetTopicByNewsId(newsId);
                    if (topic != null && topic.Count > 0)
                    {
                        newsDetail.TopicInNews = topic.Select(s=>new TopicInNews {
                            Id=s.Id,
                            TopicName=s.TopicName,
                            Logo=s.Logo,
                            ParentId=s.ParentId,
                            ParentName=s.ParentName,
                            IsPrimary=s.IsPrimary,
                            DisplayName=s.DisplayName
                        }).ToList();

                        newsDetail.TopicId = topic.FirstOrDefault(s=>s.IsPrimary=true).Id;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowBrandContent = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_BrandContent");            
            if (isAllowBrandContent == 1)
            {
                try { 
                    var brandContent = BrandContentBo.BrandInNews_GetByNewsId(newsId);
                    var brandNews = brandContent.FirstOrDefault(x => x.NewsId == newsId);                    
                    if (brandNews == null)
                    {
                        brandNews = new BrandContent.Entity.BrandContentEntity();
                    }
                    var returnContent = new List<BrandContent.Entity.BrandContentEntity>();
                    returnContent.Add(brandNews);
                    newsDetail.BrandContent = returnContent;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            try
            {
                newsDetail.NewsPr = NewsPrBo.GetNewsPrByNewsId(newsId);

                var threadNews = BoFactory.GetInstance<Thread.ThreadBo>().GetThreadByNewsId(newsId);
                //newsDetail.NewsInfo.ThreadId = 0;
                if (threadNews != null && threadNews.Count > 0)
                {
                    newsDetail.ThreadNewsInfo = threadNews.Select(s => new ThreadInNewsEntity
                    {
                        Id=s.Id,
                        Name=s.Name,
                        Avatar=s.Avatar,
                        Description=s.Description,
                        Url=s.Url,
                        Invisibled=s.Invisibled,
                        IsPrimary=s.IsPrimary
                    }).ToList();
                    //newsDetail.NewsInfo.ThreadId = threadNews.FirstOrDefault(s => s.IsPrimary = true).Id;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            var isAllowSticker = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Sticker");            
            if (isAllowSticker == 1)
            {
                try
                {
                    var sticker = StickerBo.GetListStickerByNewsId(newsId);
                    var stickerNews = sticker.FirstOrDefault(x => x.NewsId == newsId);                    
                    if (stickerNews == null)
                    {
                        stickerNews = new StickerEntity();
                    }
                    var returnContent = new List<StickerEntity>();
                    returnContent.Add(stickerNews);
                    newsDetail.Sticker = returnContent;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            if (null != newsDetail.NewsInfo)
            {
                // Zone
                if (newsId > 0)
                {
                    try
                    {
                        //newsDetail.NewsInZone = GetNewsInZoneByNewsId(newsId);
                        newsDetail.NewsRelation = GetRelatedNewsByNewsId(newsId);
                        newsDetail.TagInNews = GetTagNewsWithTagInfoByNewsId(newsId);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        //newsDetail.NewsInZone = GetNewsInZoneByZoneIdList(newsId, newsDetail.NewsInfo.ListZoneId);
                        newsDetail.NewsRelation = GetRelatedNewsByNewsIds(newsDetail.NewsInfo.NewsRelation);
                        newsDetail.TagInNews = GetTagNewsWithTagInfo(newsId, newsDetail.NewsInfo.TagPrimary, newsDetail.NewsInfo.Tag);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, ex.Message);
                    }
                }
                try {
                    newsDetail.NewsBySource = BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId);
                    newsDetail.NewsByAuthor = NewsAuthorBo.GetListInNews(newsId);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Debug, ex.Message);
                }
            }
            newsDetail.ListVersion = GetListVersionByNewsId((newsId < 0 ? 0 : newsId), currentUsername);            
            newsDetail.NewsExtensions = NewsExtensionBo.GetByNewsId(newsId);
            return newsDetail;
        }

        private NewsParentEntity getParentNews(long parentNewsId)
        {
            NewsParentEntity data = null;
            try
            {
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(parentNewsId);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(parentNewsId);                    
                }
                if (newsInfo != null)
                {
                    data = new NewsParentEntity
                    {
                        Id = newsInfo.Id,
                        Title = newsInfo.Title,
                        Avatar = newsInfo.Avatar
                    };                    
                }
            }
            catch { }
            return data;
        }

        public virtual NewsDetailForEditEntity GetNewsForEditByNewsIdCDData(long newsId, string currentUsername)
        {            
            var newsDetail = new NewsDetailForEditEntity();
            try
            {
                newsDetail.NewsInfo = (newsId < 0 ? null : NewsDal.GetNewsById(newsId));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneActivedByUsernameAndPermissionIds(currentUsername, (int)EnumPermission.ArticleReporter, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleAdmin), "--");
            
            var isAllowExpert = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Expert");            
            if (isAllowExpert == 1)
            {
                try
                {
                    var expertnews = ExpertBo.GetExpertByNewsId(newsId);
                    if (expertnews != null)
                    {
                        newsDetail.ExpertId = expertnews.Id;
                        newsDetail.ExpertQuote = expertnews.Quote;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowVote = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Vote");            
            if (isAllowVote == 1)
            {
                try
                {
                    var vote = VoteBo.GetVoteByNewsId(newsId);
                    newsDetail.VoteInfo = vote;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowTopic = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Topic");            
            if (isAllowTopic == 1)
            {
                try
                {
                    var topic = TopicBo.GetTopicByNewsId(newsId);
                    if (topic != null && topic.Count > 0)
                    {
                        newsDetail.TopicInNews = topic.Select(s => new TopicInNews
                        {
                            Id = s.Id,
                            TopicName = s.TopicName,
                            Logo = s.Logo,
                            ParentId = s.ParentId,
                            ParentName = s.ParentName,
                            IsPrimary=s.IsPrimary,
                            DisplayName=s.DisplayName
                        }).ToList();

                        newsDetail.TopicId = topic.FirstOrDefault(s => s.IsPrimary = true).Id;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isAllowBrandContent = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_BrandContent");            
            if (isAllowBrandContent == 1)
            {
                try
                {
                    var brandContent = BrandContentBo.BrandInNews_GetByNewsId(newsId);
                    var brandNews = brandContent.FirstOrDefault(x => x.NewsId == newsId);                    
                    if (brandNews == null)
                    {
                        brandNews = new BrandContent.Entity.BrandContentEntity();
                    }
                    var returnContent = new List<BrandContent.Entity.BrandContentEntity>();
                    returnContent.Add(brandNews);
                    newsDetail.BrandContent = returnContent;                    
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            try
            {
                newsDetail.NewsPr = NewsPrBo.GetNewsPrByNewsId(newsId);

                //newsDetail.NewsInfo.ThreadId = 0;
                var threadNews = BoFactory.GetInstance<Thread.ThreadBo>().GetThreadByNewsId(newsId);
                if (threadNews != null && threadNews.Count > 0)
                {
                    newsDetail.ThreadNewsInfo = threadNews.Select(s => new ThreadInNewsEntity
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Avatar = s.Avatar,
                        Description = s.Description,
                        Url = s.Url,
                        Invisibled = s.Invisibled,
                        IsPrimary = s.IsPrimary
                    }).ToList();
                    //newsDetail.NewsInfo.ThreadId = threadNews.FirstOrDefault(s => s.IsPrimary = true).Id;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            var isAllowSticker = CmsChannelConfiguration.GetAppSettingInInt32("Is_Allow_Sticker");            
            if (isAllowSticker == 1)
            {
                try
                {
                    var sticker = StickerBo.GetListStickerByNewsId(newsId);
                    var stickerNews = sticker.FirstOrDefault(x => x.NewsId == newsId);                    
                    if (stickerNews == null)
                    {
                        stickerNews = new StickerEntity();
                    }
                    var returnContent = new List<StickerEntity>();
                    returnContent.Add(stickerNews);
                    newsDetail.Sticker = returnContent;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            
            if (null != newsDetail.NewsInfo)
            {
                // Zone
                if (newsId > 0)
                {
                    try
                    {
                        newsDetail.NewsInZone = GetNewsInZoneByNewsId(newsId);
                        newsDetail.NewsRelation = GetRelatedNewsByNewsId(newsId);
                        newsDetail.TagInNews = GetTagNewsWithTagInfoByNewsId(newsId);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }
                else
                {
                    try
                    {
                        newsDetail.NewsInZone = GetNewsInZoneByZoneIdList(newsId, newsDetail.NewsInfo.ListZoneId);
                        newsDetail.NewsRelation = GetRelatedNewsByNewsIds(newsDetail.NewsInfo.NewsRelation);
                        newsDetail.TagInNews = GetTagNewsWithTagInfo(newsId, newsDetail.NewsInfo.TagPrimary, newsDetail.NewsInfo.Tag);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Debug, ex.Message);
                    }
                }
                newsDetail.NewsBySource = BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId);
                newsDetail.NewsByAuthor = NewsAuthorBo.GetListInNews(newsId);
            }
            newsDetail.ListVersion = GetListVersionByNewsId((newsId < 0 ? 0 : newsId), currentUsername);            
            var newsContent = NewsContentBo.GetNewsContentByNewsId(newsId);
            if (newsContent != null)
            {
                newsDetail.NewsContentWithTemplate = newsContent.Body;
            }
            else
            {
                newsDetail.NewsContentWithTemplate = newsDetail.NewsInfo != null ? newsDetail.NewsInfo.Body : "";
            }
            newsDetail.NewsExtensions = NewsExtensionBo.GetByNewsId(newsId);
            return newsDetail;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:23
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="zoneIdList"></param>
        /// <returns></returns>
        public static List<NewsInZoneEntity> GetNewsInZoneByZoneIdList(long newsId, string zoneIdList)
        {
            var newsInZones = new List<NewsInZoneEntity>();
            if (!string.IsNullOrEmpty(zoneIdList))
            {
                var zoneIds = zoneIdList.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                var isPrimary = true;
                foreach (var zoneId in zoneIds)
                {
                    newsInZones.Add(new NewsInZoneEntity()
                    {
                        NewsId = newsId,
                        ZoneId = Utility.ConvertToInt(zoneId),
                        IsPrimary = isPrimary
                    });
                    isPrimary = false;
                }
            }
            return newsInZones;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 12:23
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="tagIdListForPrimary"></param>
        /// <param name="tagIdList"></param>
        /// <returns></returns>
        public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfo(long newsId, string tagIdListForPrimary,
                                                                            string tagIdList)
        {
            var tagNews = new List<TagNewsWithTagInfoEntity>();

            var tagIdsForPrimary = (!string.IsNullOrEmpty(tagIdListForPrimary)
                                        ? tagIdListForPrimary.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                                        : new string[] { });
            var index = 0;
            foreach (var tagIdForPrimary in tagIdsForPrimary)
            {
                tagNews.Add(new TagNewsWithTagInfoEntity()
                {
                    NewsId = newsId,
                    TagId = Utility.ConvertToInt(tagIdForPrimary),
                    Priority = index,
                    TagMode = (int)EnumTagModes.TagForNewsSubtitle,
                    TagProperty = ""
                });
                index++;
            }

            var tagIds = (!string.IsNullOrEmpty(tagIdList)
                              ? tagIdList.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries)
                              : new string[] { });
            index = 0;
            foreach (var tagId in tagIds)
            {
                tagNews.Add(new TagNewsWithTagInfoEntity()
                {
                    NewsId = newsId,
                    TagId = Utility.ConvertToInt(tagId),
                    Priority = index,
                    TagMode = (int)EnumTagModes.TagForNews,
                    TagProperty = ""
                });
                index++;
            }
            return tagNews;
        }

        public virtual NewsEntity GetNewsByNewsId(long id)
        {
            return NewsDal.GetNewsById(id);
        }

        public virtual NewsEntity GetNewsByNewsIdForInit(long id)
        {
            return NewsDal.GetNewsByNewsIdForInit(id);
        }

        public static List<NewsWithAnalyticEntity> GetNewsByListNewsId(NewsStatus status, string listNewsId)
        {
            var data = new List<NewsWithAnalyticEntity>();
            var listIds = listNewsId.Split(';').ToList();
            var res = BoCached.Base.News.NewsDalFactory.GetNewsByListId(listIds);
            data.AddRange(res.Select(s=>new NewsWithAnalyticEntity {
                NewsId=s.Id,
                Title=s.Title,
                Url=s.Url,
                Avatar=s.Avatar,
                Sapo=s.Sapo,
                ZoneId=s.ZoneId,
                ZoneName=s.ZoneName,
                CreatedBy=s.CreatedBy,
                DistributionDate=s.DistributionDate,
                Avatar2=s.Avatar2,
                Avatar3 = s.Avatar3,
                Avatar4 = s.Avatar4,
                Avatar5 = s.Avatar5
            }).ToList());

            if (data==null || (data!=null && data.Count <= 0) || (data != null && data.Count < listIds.Count))
            {
                data= NewsDal.GetNewsByListNewsId((int)status, listNewsId);
            }
            return data;
        }
        public static List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId)
        {
            try
            {
                return NewsDal.GetAllNewsByListNewsId(listNewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsWithExpertEntity>();
            }
        }
        public static List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId)
        {
            try
            {
                return NewsDal.GetAllNewsForExportByListNewsId(listNewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsForExportEntity>();
            }
        }
        public static List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            return NewsDal.GetNewsDistributionDateByListNewsId(listNewsId);
        }

        public static NewsEntity GetNewsInfoById(long id)
        {
            try
            {
                if (id > 0)
                {
                    return NewsDal.GetNewsInfoById(id);
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static int GetNewsTypeByNewsId(long id)
        {
            try
            {
                if (id > 0)
                {
                    return NewsDal.GetNewsTypeByNewsId(id);
                }
                return (int)NewsType.Normal;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return (int)NewsType.Normal;
            }
        }
        public static NewsEntity GetNewsByNewsId(long id, string lastModifiedBy, ref string newsVesionId)
        {
            newsVesionId = "";
            if (id > 0)
            {
                var newsInfo = BoCached.Base.News.NewsDalFactory.GetNewsById(id);
                if (newsInfo == null)
                {
                    newsInfo = NewsDal.GetNewsById(id);
                    if (newsInfo != null)
                    {                                                
                        BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);
                    }
                }
                //loc lai zoneId cho bai cu
                if (newsInfo.ZoneId <= 0)
                {
                    newsInfo = NewsDal.GetNewsById(id);
                    if (newsInfo != null && !string.IsNullOrEmpty(newsInfo.ListZoneId))
                    {
                        var zoneIds = newsInfo.ListZoneId.Split(',');
                        if(zoneIds.Count()>0)
                            newsInfo.ZoneId = Utility.ConvertToInt(zoneIds[0]);
                        newsInfo.ListZoneId = string.Join(";", zoneIds);
                        var oZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(newsInfo.ZoneId);
                        if (oZone != null) { newsInfo.ZoneName = oZone.Name; }

                        BoCached.Base.News.NewsDalFactory.AddNews(newsInfo);
                    }
                }
                return newsInfo;
            }
            else
            {
                //var lastestVersion = BoCached.Base.News.NewsDalFactory.GetLastestVersionByNewsId(id, lastModifiedBy);
                //if (lastestVersion == null)
                //{
                //    lastestVersion = NewsVersionDal.GetLastestVersionByNewsId(id, lastModifiedBy);
                //}

                //get tu nodejs
                var lastestVersion = GetLastestVersionByNewsId(id, lastModifiedBy);

                if (null != lastestVersion)
                {
                    //BoCached.Base.NewsVersion.NewsVersionDalFactory.AddNewsVersion(lastestVersion);

                    newsVesionId = lastestVersion.Id;

                    var news = new NewsEntity();
                    news.Title = lastestVersion.Title;
                    news.SubTitle = lastestVersion.SubTitle;
                    news.Sapo = lastestVersion.Sapo;
                    news.Body = lastestVersion.Body;
                    news.Avatar = lastestVersion.Avatar;
                    news.AvatarDesc = lastestVersion.AvatarDesc;
                    news.Avatar2 = lastestVersion.Avatar2;
                    news.Avatar3 = lastestVersion.Avatar3;
                    news.Avatar4 = lastestVersion.Avatar4;
                    news.Avatar5 = lastestVersion.Avatar5;
                    news.Author = lastestVersion.Author;
                    news.NewsRelation = lastestVersion.NewsRelation;
                    news.Source = lastestVersion.Source;
                    news.IsFocus = lastestVersion.IsFocus;
                    news.Type = lastestVersion.Type;
                    news.ThreadId = lastestVersion.ThreadId;
                    news.CreatedDate = lastestVersion.CreatedDate;
                    news.LastModifiedDate = lastestVersion.LastModifiedDate;
                    news.DistributionDate = lastestVersion.DistributionDate;
                    news.CreatedBy = lastestVersion.CreatedBy;
                    news.LastModifiedBy = lastestVersion.LastModifiedBy;
                    news.PublishedBy = lastestVersion.PublishedBy;
                    news.EditedBy = lastestVersion.EditedBy;
                    news.LastReceiver = lastestVersion.LastReceiver;
                    news.WordCount = lastestVersion.WordCount;
                    news.ViewCount = lastestVersion.ViewCount;
                    news.Priority = lastestVersion.Priority;
                    news.ListZoneId = lastestVersion.ListZoneId;
                    news.Tag = lastestVersion.Tag;
                    news.Note = lastestVersion.Note;
                    news.DisplayInSlide = lastestVersion.DisplayInSlide;
                    news.DisplayPosition = lastestVersion.DisplayPosition;
                    news.DisplayStyle = lastestVersion.DisplayStyle;
                    news.AvatarCustom = lastestVersion.AvatarCustom;
                    news.IsOnHome = lastestVersion.IsOnHome;
                    news.OriginalId = lastestVersion.OriginalId;
                    // Extension fields
                    news.Price = lastestVersion.Price;
                    news.TagItem = lastestVersion.TagItem;
                    news.Url = lastestVersion.Url;
                    news.NewsCategory = lastestVersion.NewsCategory;
                    news.NoteRoyalties = lastestVersion.NoteRoyalties;
                    news.InitSapo = lastestVersion.InitSapo;
                    return news;
                }
            }
            return null;
        }

        public static List<NewsSearchEntity> ListNewsByStatus(string username, int typeOfCheck, string zoneIds,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status, int type,
                                                        int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.ListNewsByStatus(username, typeOfCheck, zoneIds, (int)filterFieldForUsername, (int)sortOrder, (int)status, type, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsByMultiStatus(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, string listStatus, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            try
            {
                if (string.IsNullOrEmpty(listStatus))
                {
                    return null;
                }
                
                var currentUserName = username;

                var statusZoneIds = new Dictionary<string, dynamic>();
                var user = UserDataBo.GetUserByUsername(username);
                if (user == null)
                {
                    return null;
                }
                var isRole = user.IsRole;
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);//PermissionBo.GetPermissionByUsername2(username);
                var listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();
                
                var list = listStatus.Split(',').ToList();
                list = list.OrderBy(n => Convert.ToInt32(n)).ToList();
                foreach (var status in list)
                {
                    var filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    username = currentUserName;

                    //check quyền trả list tin                        
                    var newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                    switch (newsStatus)
                    {
                        #region //bài của tôi
                        case NewsStatus.Unknow:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        case NewsStatus.All:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        #endregion
                        #region //chỉ quyền phong viên mới thấy list này
                        case NewsStatus.Temporary:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        #endregion
                        #region //chỉ quyền biên tập viên mới thấy list này
                        case NewsStatus.WaitForEdit:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        case NewsStatus.ReceivedForEdit:
                            filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                            break;
                        case NewsStatus.ReturnedToEditor:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            else
                                username = "KhongCoQuyen";
                            break;
                        #endregion
                        #region //chỉ quyền thư ký mới thấy list này
                        case NewsStatus.WaitForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        case NewsStatus.ReceivedForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        case NewsStatus.ReturnedToEditorialSecretary:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;
                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        #endregion
                        #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                        case NewsStatus.Published:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            else {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            }
                            break;
                        case NewsStatus.WaitForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    username = string.Empty;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    username = string.Empty;
                            }
                            if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                            {
                                zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                
                            }
                            break;
                        case NewsStatus.ReceivedForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            break;
                        case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            break;
                        #endregion
                        case NewsStatus.MovedToTrash:
                            filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                            break;
                        case NewsStatus.ReturnToCooperator:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        #region //List trả lại đích danh BTV hoặc TK
                        case NewsStatus.ReturnedToMyEditor:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                            }
                            else
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                            break;
                        #endregion
                        #region //List crawler
                        case NewsStatus.CrawlerNews:
                        case NewsStatus.CrawlerByVccorp:
                            username = string.Empty;
                            zoneIds = "";
                            break;
                            #endregion
                    };

                    statusZoneIds[status] = new { zoneIds, filterFieldForUsername, username };
                }

                return BoSearch.Base.News.NewsDalFactory.ListNewsByMultiStatus(keyword, author, fromDate, toDate, list, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsByMultiStatusSubsite(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, string listStatus, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            try
            {
                if (string.IsNullOrEmpty(listStatus))
                {
                    return null;
                }

                var currentUserName = username;

                var statusZoneIds = new Dictionary<string, dynamic>();
                var user = UserDataBo.GetUserByUsername(username);
                if (user == null)
                {
                    return null;
                }
                var isRole = user.IsRole;
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);//PermissionBo.GetPermissionByUsername2(username);
                var listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();

                var list = listStatus.Split(',').ToList();
                list = list.OrderBy(n => Convert.ToInt32(n)).ToList();
                foreach (var status in list)
                {
                    var filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    username = currentUserName;

                    //check quyền trả list tin                        
                    var newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                    switch (newsStatus)
                    {
                        #region //bài của tôi
                        case NewsStatus.Unknow:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        case NewsStatus.All:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        #endregion
                        #region //chỉ quyền phong viên mới thấy list này
                        case NewsStatus.Temporary:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        #endregion
                        #region //chỉ quyền biên tập viên mới thấy list này
                        case NewsStatus.WaitForEdit:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        case NewsStatus.ReceivedForEdit:
                            filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                            break;
                        case NewsStatus.ReturnedToEditor:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            else
                                username = "KhongCoQuyen";
                            break;
                        #endregion
                        #region //chỉ quyền thư ký mới thấy list này
                        case NewsStatus.WaitForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        case NewsStatus.ReceivedForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        case NewsStatus.ReturnedToEditorialSecretary:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;
                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        #endregion
                        #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                        case NewsStatus.Published:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            else
                            {
                                username = currentUserName;
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            }
                            break;
                        case NewsStatus.WaitForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    username = string.Empty;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    username = string.Empty;
                            }
                            if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                            {
                                zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                            }
                            break;
                        case NewsStatus.ReceivedForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            break;
                        case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                username = string.Empty;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            break;
                        #endregion
                        case NewsStatus.MovedToTrash:
                            filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                            break;
                        case NewsStatus.ReturnToCooperator:
                            filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                            break;
                        #region //List trả lại đích danh BTV hoặc TK
                        case NewsStatus.ReturnedToMyEditor:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                            {
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;

                                if (string.IsNullOrEmpty(zoneIds) && null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());
                                }
                            }
                            else
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                            break;
                        #endregion
                        #region //List crawler
                        case NewsStatus.CrawlerNews:
                        case NewsStatus.CrawlerByVccorp:
                            username = string.Empty;
                            zoneIds = "";
                            break;
                            #endregion
                    };

                    var zoneTotalChecks = new List<int>();
                    var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

                    foreach (var zoneParentId in zoneParentIds)
                    {
                        zoneTotalChecks.Add(zoneParentId);
                        var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                        var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                        zoneTotalChecks.AddRange(zoneChildrenIds);
                    }
                    zoneIds = string.Join(",", zoneTotalChecks?.Distinct());
                    statusZoneIds[status] = new { zoneIds, filterFieldForUsername, username };
                }

                return BoSearch.Base.News.NewsDalFactory.ListNewsByMultiStatusSubsite(keyword, author, fromDate, toDate, list, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsProcessing(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            try
            {                
                var currentUserName = username;

                var statusZoneIds = new Dictionary<string, dynamic>();
                var user = UserDataBo.GetUserByUsername(username);
                if (user == null)
                {
                    return null;
                }
                var isRole = user.IsRole;
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);//PermissionBo.GetPermissionByUsername2(username);
                var listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();

                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }

                //nhan bt=3, nhan xb=6, nhan duyet=16
                const string listStatus = "3,6,16";
                var list = listStatus.Split(',').ToList();
                list = list.OrderBy(n => Convert.ToInt32(n)).ToList();
                foreach (var status in list)
                {
                    var filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    username = currentUserName;

                    //check quyền trả list tin                        
                    var newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                    switch (newsStatus)
                    {                        
                        #region //chỉ quyền biên tập viên mới thấy list này                        
                        case NewsStatus.ReceivedForEdit:
                            filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                            break;                        
                        #endregion
                        #region //chỉ quyền thư ký mới thấy list này                        
                        case NewsStatus.ReceivedForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                            break;                        
                        #endregion
                        #region //chỉ quyền thu ky va ban biên tập mới thấy list này                        
                        case NewsStatus.ReceivedForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            break;
                        #endregion                        
                    };

                    statusZoneIds[status] = new { zoneIds, filterFieldForUsername, username };
                }

                return BoSearch.Base.News.NewsDalFactory.ListNewsProcessing(keyword, author, fromDate, toDate, list, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsProcessingSubsite(string keyword, string author, string zoneIds, DateTime fromDate, DateTime toDate, string username, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            try
            {
                var currentUserName = username;

                var statusZoneIds = new Dictionary<string, dynamic>();
                var user = UserDataBo.GetUserByUsername(username);
                if (user == null)
                {
                    return null;
                }
                var isRole = user.IsRole;
                var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);//PermissionBo.GetPermissionByUsername2(username);
                var listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();

                if (string.IsNullOrEmpty(zoneIds))
                {
                    if (user.IsFullZone)
                    {
                        zoneIds = "";
                    }
                    else
                    {
                        if (null != listUserPermission && listUserPermission.Count > 0)
                        {
                            zoneIds = string.Join(",", listUserPermission.Select(s => s.ZoneId).Distinct().ToArray());
                        }
                    }
                }

                //nhan bt=3, nhan xb=6, nhan duyet=16
                const string listStatus = "3,6,16";
                var list = listStatus.Split(',').ToList();
                list = list.OrderBy(n => Convert.ToInt32(n)).ToList();
                foreach (var status in list)
                {
                    var filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                    username = currentUserName;

                    //check quyền trả list tin                        
                    var newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                    switch (newsStatus)
                    {
                        #region //chỉ quyền biên tập viên mới thấy list này                        
                        case NewsStatus.ReceivedForEdit:
                            filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                            break;
                        #endregion
                        #region //chỉ quyền thư ký mới thấy list này                        
                        case NewsStatus.ReceivedForPublish:
                            if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                            break;
                        #endregion
                        #region //chỉ quyền thu ky va ban biên tập mới thấy list này                        
                        case NewsStatus.ReceivedForEditorialBoard:
                            if (!user.IsFullPermission)
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            else
                            {
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                    filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                            }
                            break;
                            #endregion
                    };

                    var zoneTotalChecks = new List<int>();
                    var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();
                    foreach (var zoneParentId in zoneParentIds)
                    {
                        zoneTotalChecks.Add(zoneParentId);
                        var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                        var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                        zoneTotalChecks.AddRange(zoneChildrenIds);
                    }
                    zoneIds = string.Join(",", zoneTotalChecks?.Distinct());
                    statusZoneIds[status] = new { zoneIds, filterFieldForUsername, username };
                }

                return BoSearch.Base.News.NewsDalFactory.ListNewsProcessing(keyword, author, fromDate, toDate, list, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsIsPr(string keyword, string username, string zoneIds, NewsStatus newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.ListNewsIsPr(keyword, username, zoneIds, (int)newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsIsPrSubsite(string keyword, string username, string zoneIds, NewsStatus newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var zoneTotalChecks = new List<int>();
                var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

                foreach (var zoneParentId in zoneParentIds)
                {
                    zoneTotalChecks.Add(zoneParentId);
                    var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                    var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                    zoneTotalChecks.AddRange(zoneChildrenIds);
                }

                return BoSearch.Base.News.NewsDalFactory.ListNewsIsPrSubsite(keyword, username, zoneIds, zoneTotalChecks, (int)newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsPublishByDate(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var lstElastic = BoSearch.Base.News.NewsDalFactory.ListNewsPublishByDate(fromDate, toDate, pageIndex, pageSize, ref totalRow);
                //var lstNewsId = new List<string>();
                //if (lstElastic != null && lstElastic.Count > 0) lstNewsId = lstElastic.Select(a => a.Id.ToString()).ToList();
                //var lstCache = BoCached.Base.News.NewsDalFactory.GetNewsByListId(lstNewsId);
                //Logger.WriteLog(Logger.LogType.Debug, "lstElastic: " + lstElastic.Count + ", lstCache: " + lstCache.Count);
                //NewsSearchEntity entity = null;
                //for (var i = 0; i < lstCache.Count; i++)
                //{

                //    Logger.WriteLog(Logger.LogType.Debug, ", NewsId: " + lstCache[i].Id);
                    
                //}
                //for (var i = 0; i < lstElastic.Count; i++)
                //{
                //    entity = lstElastic[i];
                //    Logger.WriteLog(Logger.LogType.Debug, "Id: " + entity.Id + ", ZoneId: " + lstCache.FirstOrDefault(a => a.Id == entity.Id).ZoneId);
                //    if (i == 0) Logger.WriteLog(Logger.LogType.Debug, NewtonJson.Serialize(lstCache.FirstOrDefault(a => a.Id == entity.Id)));
                //    entity.ZoneId = lstCache.FirstOrDefault(a => a.Id == entity.Id).ZoneId;
                //}
                //for (var i = 0; i < lstElastic.Count; i++)
                //{
                //    entity = lstElastic[i];
                //    Logger.WriteLog(Logger.LogType.Debug, "ZoneId: " + entity.ZoneId );
                //}
                return lstElastic;
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }
        public static List<NewsRoyaltiesV2OutEntity> ListNewsPublishForRoyalties(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var data= NewsDal.ListNewsPublishForRoyalties(fromDate, toDate, pageIndex, pageSize, ref totalRow);
                return data.Select(s => new NewsRoyaltiesV2OutEntity {
                    id = s.Id,
                    author = s.Author,
                    created_by = s.CreatedBy,
                    created_date = s.CreatedDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    distribution_date= s.DistributionDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    is_pr=s.IsPr,
                    is_prod=s.OriginalId>0?1:0,
                    last_modified_date= s.LastModifiedDate.ToString("yyyy-MM-dd HH:mm:ss"),
                    original_url=s.OriginalUrl,
                    origin_id=s.OriginalId,
                    source=s.Source,
                    thread_id=s.ThreadId,
                    title=s.Title,
                    type=s.Type,
                    url=s.Url,
                    wordCount=s.WordCount,
                    zone_id=s.ZoneId,
                    status=s.Status
                }).ToList();
            }
            catch
            {
                return new List<NewsRoyaltiesV2OutEntity>();
            }
        }

        public static List<NewsSearchEntity> ListNewsPublish(string accountName, DateTime fromDate, DateTime toDate, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.ListNewsPublish(accountName, fromDate, toDate, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> SearchNewsForSeo(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.SearchNewsForSeo(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchViewEntity> ExportNewsPublish(List<NewsSearchEntity> data, int orderby, long viewFrom, long viewTo, int pageIndex, int pageSize, ref int totalRowsView)
        {
            try
            {
                //check id<100000 -> crc32
                var ids = data.Select(n => n.Id < 100000 ? Utility.Crc32(n.Url).ToString() : n.EncryptId).ToList();
                
                var dataViews = ViewsBo.SortViews(pageIndex, pageSize, string.Join(";", ids), "desc", orderby, viewFrom, viewTo, ref totalRowsView);
                var dataResult = new List<NewsSearchViewEntity>();

                if (orderby == 0)
                {
                    foreach (var item in dataViews)
                    {
                        var lst = data.SingleOrDefault(n => n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString());
                        if (lst != null)
                        {
                            dataResult.Add(new NewsSearchViewEntity()
                            {
                                Id = lst.Id,
                                EncryptId = lst.EncryptId,
                                Title = lst.Title,
                                ZoneIds = lst.ZoneIds,
                                Type = lst.Type,
                                CreatedBy = lst.CreatedBy,
                                LastModifiedBy = lst.LastModifiedBy,
                                EditedBy = lst.EditedBy,
                                PublishedBy = lst.PublishedBy,
                                LastReceiver = lst.LastReceiver,
                                ApprovedBy = lst.ApprovedBy,
                                ReturnedBy = lst.ReturnedBy,
                                CreatedDate = lst.CreatedDate,
                                DistributionDate = lst.DistributionDate,
                                LastModifiedDate = lst.LastModifiedDate,
                                ApprovedDate = lst.ApprovedDate,
                                ViewCount = Convert.ToInt32(item.TotalViews),
                                ViewPcCount = Convert.ToInt32(item.ViewPc),
                                ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                Status = lst.Status,
                                IsOnHome = lst.IsOnHome,
                                Avatar = lst.Avatar,
                                ViewRoyalties = lst.ViewRoyalties,
                                ZoneName = lst.ZoneName,
                                Note = lst.Note,
                                Author = lst.Author,
                                ErrorCheckedBy = lst.ErrorCheckedBy,
                                ErrorCheckedDate = lst.ErrorCheckedDate,
                                SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                WordCount = lst.WordCount
                            });
                        }
                    }
                }
                else if (orderby == 1)
                {
                    foreach (var lst in data)
                    {
                        var item = dataViews.SingleOrDefault(n => lst.EncryptId == n.NewsId.ToString() || Utility.Crc32(lst.Url).ToString() == n.NewsId.ToString());
                        if (item != null)
                        {
                            dataResult.Add(new NewsSearchViewEntity()
                            {
                                Id = lst.Id,
                                EncryptId = lst.EncryptId,
                                Title = lst.Title,
                                ZoneIds = lst.ZoneIds,
                                Type = lst.Type,
                                CreatedBy = lst.CreatedBy,
                                LastModifiedBy = lst.LastModifiedBy,
                                EditedBy = lst.EditedBy,
                                PublishedBy = lst.PublishedBy,
                                LastReceiver = lst.LastReceiver,
                                ApprovedBy = lst.ApprovedBy,
                                ReturnedBy = lst.ReturnedBy,
                                CreatedDate = lst.CreatedDate,
                                DistributionDate = lst.DistributionDate,
                                LastModifiedDate = lst.LastModifiedDate,
                                ApprovedDate = lst.ApprovedDate,
                                ViewCount = Convert.ToInt32(item.TotalViews),
                                ViewPcCount = Convert.ToInt32(item.ViewPc),
                                ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                Status = lst.Status,
                                IsOnHome = lst.IsOnHome,
                                Avatar = lst.Avatar,
                                ViewRoyalties = lst.ViewRoyalties,
                                ZoneName = lst.ZoneName,
                                Note = lst.Note,
                                Author = lst.Author,
                                ErrorCheckedBy = lst.ErrorCheckedBy,
                                ErrorCheckedDate = lst.ErrorCheckedDate,
                                SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                WordCount = lst.WordCount
                            });
                        }
                    }
                }
                else
                {
                    foreach (var item in dataViews)
                    {
                        var lst = data.SingleOrDefault(n => n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString());
                        if (lst != null)
                        {
                            dataResult.Add(new NewsSearchViewEntity()
                            {
                                Id = lst.Id,
                                EncryptId = lst.EncryptId,
                                Title = lst.Title,
                                ZoneIds = lst.ZoneIds,
                                Type = lst.Type,
                                CreatedBy = lst.CreatedBy,
                                LastModifiedBy = lst.LastModifiedBy,
                                EditedBy = lst.EditedBy,
                                PublishedBy = lst.PublishedBy,
                                LastReceiver = lst.LastReceiver,
                                ApprovedBy = lst.ApprovedBy,
                                ReturnedBy = lst.ReturnedBy,
                                CreatedDate = lst.CreatedDate,
                                DistributionDate = lst.DistributionDate,
                                LastModifiedDate = lst.LastModifiedDate,
                                ApprovedDate = lst.ApprovedDate,
                                ViewCount = Convert.ToInt32(item.TotalViews),
                                ViewPcCount = Convert.ToInt32(item.ViewPc),
                                ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                Status = lst.Status,
                                IsOnHome = lst.IsOnHome,
                                Avatar = lst.Avatar,
                                ViewRoyalties = lst.ViewRoyalties,
                                ZoneName = lst.ZoneName,
                                Note = lst.Note,
                                Author = lst.Author,
                                ErrorCheckedBy = lst.ErrorCheckedBy,
                                ErrorCheckedDate = lst.ErrorCheckedDate,
                                SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                WordCount = lst.WordCount
                            });
                        }
                    }
                }

                return dataResult;
            }
            catch
            {
                return new List<NewsSearchViewEntity>();
            }
        }

        public static List<NewsSearchEntity> ListMyNews(string username, string zoneIds,
                                                        List<int> filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status, int type,
                                                        int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.ListMyNews(username, zoneIds, filterFieldForUsername, (int)sortOrder, (int)status, type, pageIndex, pageSize, ref totalRow);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:11
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="username"></param>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="filterFieldForUsername"></param>
        /// <param name="sortOrder"></param>
        /// <param name="status"></param>
        /// <param name="newsType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsSearchEntity> SearchNews(string keyword, int typeOfCheck, string author, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow, string createdBy="")
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.SearchNews(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        /// <summary>
        /// Edited: nguyenhung
        /// LastModifiedDate: 2019-12-03 12:02
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="username"></param>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="filterFieldForUsername"></param>
        /// <param name="sortOrder"></param>
        /// <param name="status"></param>
        /// <param name="newsType"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsSearchEntity> SearchNewsByZoneParent(string keyword, int typeOfCheck, string author, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow, string createdBy = "")
        {
            try
            {
                var zoneTotalChecks = new List<int>();
                var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

                foreach (var zoneParentId in zoneParentIds)
                {
                    zoneTotalChecks.Add(zoneParentId);
                    var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                    var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                    zoneTotalChecks.AddRange(zoneChildrenIds);
                }                             
                return BoSearch.Base.News.NewsDalFactory.SearchNewsByParent(keyword, typeOfCheck, author, username, zoneIds, zoneTotalChecks, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> SearchNewsSeoReviewStatus(string keyword, int reviewStatus, string author, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow, string createdBy = "")
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.SearchNewsSeoReviewStatus(keyword, reviewStatus, author, username, zoneIds, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> SearchNewsSeoReviewStatusSubsite(string keyword, int reviewStatus, string author, string username, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate,
                                                       NewsFilterFieldForUsername filterFieldForUsername,
                                                       NewsSortExpression sortOrder,
                                                       NewsStatus status,
                                                       NewsType newsType, int pageIndex, int pageSize,
                                                       ref int totalRow, string createdBy = "")
        {
            try
            {
                var zoneTotalChecks = new List<int>();
                var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

                foreach (var zoneParentId in zoneParentIds)
                {
                    zoneTotalChecks.Add(zoneParentId);
                    var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                    var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                    zoneTotalChecks.AddRange(zoneChildrenIds);
                }
                return BoSearch.Base.News.NewsDalFactory.SearchNewsSeoReviewStatusSubsite(keyword, reviewStatus, author, username, zoneIds,zoneTotalChecks, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> SearchNewsPublishES(string keyword, int typeOfCheck, string author, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow, string createdBy = "")
        {
            try
            {
                return BoSearch.Base.News.NewsDalFactory.SearchNewsPublishES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsSearchEntity> SearchNewsPublishSubsiteES(string keyword, int typeOfCheck, string author, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow, string createdBy = "")
        {
            try
            {
                var zoneTotalChecks = new List<int>();
                var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

                foreach (var zoneParentId in zoneParentIds)
                {
                    zoneTotalChecks.Add(zoneParentId);
                    var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                    var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                    zoneTotalChecks.AddRange(zoneChildrenIds);
                }

                return BoSearch.Base.News.NewsDalFactory.SearchNewsPublishSubsiteES(keyword, typeOfCheck, author, username, zoneIds, zoneTotalChecks, fromDate, toDate, (int)filterFieldForUsername, (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            catch
            {
                return new List<NewsSearchEntity>();
            }
        }

        public static List<NewsInListEntity> SearchNewsForNewsPositionES(string keyword, int zoneId, int displayPosition, int priority, bool showBomb, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var list = new List<NewsInListEntity>();
                var data= BoSearch.Base.News.NewsDalFactory.SearchNewsForNewsPositionES(keyword, zoneId, displayPosition, priority, showBomb, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var result = BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(data.Select(s => s.Id.ToString()).ToList());
                    if (result != null && result.Count > 0)
                    {
                        foreach(var item in data)
                        {
                            foreach (var item2 in result) {
                                try {
                                    if (item2.NewsInfo != null && item.Id == item2.NewsInfo.Id)
                                    {
                                        list.Add(new NewsInListEntity
                                        {
                                            Id = item2.NewsInfo.Id,
                                            Title = item2.NewsInfo.Title,
                                            Avatar = item2.NewsInfo.Avatar,
                                            Avatar2 = item2.NewsInfo.Avatar2,
                                            Avatar3 = item2.NewsInfo.Avatar3,
                                            Avatar4 = item2.NewsInfo.Avatar4,
                                            Avatar5 = item2.NewsInfo.Avatar5,
                                            Sapo = item2.NewsInfo.Sapo,
                                            Url = item2.NewsInfo.Url,
                                            DisplayPosition = item2.NewsInfo.DisplayPosition,
                                            DistributionDate = item2.NewsInfo.DistributionDate,
                                            EncryptId = item2.NewsInfo.EncryptId,
                                            Type = item2.NewsInfo.Type,
                                            Author = item2.NewsInfo.Author,
                                            Priority= item.Priority
                                        });
                                        break;
                                    }
                                    else if (result.LastIndexOf(item2) == (result.Count - 1))
                                    {
                                        list.Add(new NewsInListEntity
                                        {
                                            Id = item.Id,
                                            Title = item.Title,
                                            Avatar = item.Avatar,
                                            Avatar2 = item.Avatar,
                                            Avatar3 = item.Avatar,
                                            Avatar4 = item.Avatar,
                                            Avatar5 = item.Avatar,
                                            Sapo = item.Sapo == null ? "" : item.Sapo,
                                            Url = item.Url == null ? "" : item.Url,
                                            DisplayPosition = item.DisplayPosition,
                                            DistributionDate = item.DistributionDate,
                                            EncryptId = item.EncryptId,
                                            Type = item.Type,
                                            Author = item.Author,
                                            Priority = item.Priority
                                        });
                                    }
                                }
                                catch(Exception ex) {
                                    //Logger.WriteLog(Logger.LogType.Error, ex.Message);
                                }
                            }
                        }
                        //list = result.Select(s => new NewsInListEntity
                        //{
                        //    Id = s.NewsInfo.Id,
                        //    Title = s.NewsInfo.Title,
                        //    Avatar = s.NewsInfo.Avatar,
                        //    Avatar2 = s.NewsInfo.Avatar2,
                        //    Avatar3 = s.NewsInfo.Avatar3,
                        //    Avatar4 = s.NewsInfo.Avatar4,
                        //    Avatar5 = s.NewsInfo.Avatar5,
                        //    Sapo = s.NewsInfo.Sapo,
                        //    Url = s.NewsInfo.Url,
                        //    DisplayPosition = s.NewsInfo.DisplayPosition,
                        //    DistributionDate = s.NewsInfo.DistributionDate,
                        //    EncryptId = s.NewsInfo.EncryptId,
                        //    Type = s.NewsInfo.Type,
                        //    Author = s.NewsInfo.Author
                        //}).ToList();
                    }
                    else
                    {
                        list = data.Select(s => new NewsInListEntity
                        {
                            Id = s.Id,
                            Title = s.Title,
                            Avatar = s.Avatar,
                            Avatar2 = s.Avatar,
                            Avatar3 = s.Avatar,
                            Avatar4 = s.Avatar,
                            Avatar5 = s.Avatar,
                            Sapo = s.Sapo == null ? "" : s.Sapo,
                            Url = s.Url == null ? "" : s.Url,
                            DisplayPosition = s.DisplayPosition,
                            DistributionDate = s.DistributionDate,
                            EncryptId = s.EncryptId,
                            Type = s.Type,
                            Author = s.Author,
                            Priority = s.Priority
                        }).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return new List<NewsInListEntity>();
            }
        }

        public static List<NewsInListEntity> SearchNewsForNewsPositionESInDateNow(string keyword, int zoneId, int displayPosition, int priority, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var list = new List<NewsInListEntity>();
                var data = BoSearch.Base.News.NewsDalFactory.SearchNewsForNewsPositionESInDateNow(keyword, zoneId, displayPosition, priority, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var result = BoCached.Base.News.NewsDalFactory.GetNewsCachedByListId(data.Select(s => s.Id.ToString()).ToList());
                    if (result != null && result.Count > 0)
                    {
                        foreach (var item in data)
                        {
                            foreach (var item2 in result)
                            {
                                try
                                {
                                    if (item2.NewsInfo != null && item.Id == item2.NewsInfo.Id)
                                    {
                                        list.Add(new NewsInListEntity
                                        {
                                            Id = item2.NewsInfo.Id,
                                            Title = item2.NewsInfo.Title,
                                            Avatar = item2.NewsInfo.Avatar,
                                            Avatar2 = item2.NewsInfo.Avatar2,
                                            Avatar3 = item2.NewsInfo.Avatar3,
                                            Avatar4 = item2.NewsInfo.Avatar4,
                                            Avatar5 = item2.NewsInfo.Avatar5,
                                            Sapo = item2.NewsInfo.Sapo,
                                            Url = item2.NewsInfo.Url,
                                            DisplayPosition = item2.NewsInfo.DisplayPosition,
                                            DistributionDate = item2.NewsInfo.DistributionDate,
                                            EncryptId = item2.NewsInfo.EncryptId,
                                            Type = item2.NewsInfo.Type,
                                            Author = item2.NewsInfo.Author,
                                            Priority = item.Priority
                                        });
                                        break;
                                    }
                                    else if (result.LastIndexOf(item2) == (result.Count - 1))
                                    {
                                        list.Add(new NewsInListEntity
                                        {
                                            Id = item.Id,
                                            Title = item.Title,
                                            Avatar = item.Avatar,
                                            Avatar2 = item.Avatar,
                                            Avatar3 = item.Avatar,
                                            Avatar4 = item.Avatar,
                                            Avatar5 = item.Avatar,
                                            Sapo = item.Sapo == null ? "" : item.Sapo,
                                            Url = item.Url == null ? "" : item.Url,
                                            DisplayPosition = item.DisplayPosition,
                                            DistributionDate = item.DistributionDate,
                                            EncryptId = item.EncryptId,
                                            Type = item.Type,
                                            Author = item.Author,
                                            Priority = item.Priority
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //Logger.WriteLog(Logger.LogType.Error, ex.Message);
                                }
                            }
                        }
                        //list = result.Select(s => new NewsInListEntity
                        //{
                        //    Id = s.NewsInfo.Id,
                        //    Title = s.NewsInfo.Title,
                        //    Avatar = s.NewsInfo.Avatar,
                        //    Avatar2 = s.NewsInfo.Avatar2,
                        //    Avatar3 = s.NewsInfo.Avatar3,
                        //    Avatar4 = s.NewsInfo.Avatar4,
                        //    Avatar5 = s.NewsInfo.Avatar5,
                        //    Sapo = s.NewsInfo.Sapo,
                        //    Url = s.NewsInfo.Url,
                        //    DisplayPosition = s.NewsInfo.DisplayPosition,
                        //    DistributionDate = s.NewsInfo.DistributionDate,
                        //    EncryptId = s.NewsInfo.EncryptId,
                        //    Type = s.NewsInfo.Type,
                        //    Author = s.NewsInfo.Author
                        //}).ToList();
                    }
                    else
                    {
                        list = data.Select(s => new NewsInListEntity
                        {
                            Id = s.Id,
                            Title = s.Title,
                            Avatar = s.Avatar,
                            Avatar2 = s.Avatar,
                            Avatar3 = s.Avatar,
                            Avatar4 = s.Avatar,
                            Avatar5 = s.Avatar,
                            Sapo = s.Sapo == null ? "" : s.Sapo,
                            Url = s.Url == null ? "" : s.Url,
                            DisplayPosition = s.DisplayPosition,
                            DistributionDate = s.DistributionDate,
                            EncryptId = s.EncryptId,
                            Type = s.Type,
                            Author = s.Author,
                            Priority = s.Priority
                        }).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return new List<NewsInListEntity>();
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsByNewsAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            var totalRows = 0;

            return BoCached.Common.Queue.InitSyncData(name, startPage, startDate, endDate, () =>
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var data = NewsDal.InitESAllNews(1, pageSize, startDate, endDate, ref totalRows, zoneid);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                Logger.WriteLog(Logger.LogType.Info,"Time Init News On Es: " + elapsedTime +"(get data on SQL)");
                return totalPages;
            }, (page) =>
            {
                var data = NewsDal.InitESAllNews(page, pageSize, startDate, endDate, ref totalRows, zoneid);
                var list = data.Select(s => new NewsSearchEntity
                {
                    Id = s.Id,
                    EncryptId = s.Id.ToString(),
                    Title = s.Title,
                    ZoneIds = ZoneDal.GetZoneByNewsId(s.Id).Select(z => z.Id.ToString()).ToArray(),
                    Type = s.Type,
                    CreatedBy = s.CreatedBy,
                    LastModifiedBy = s.LastModifiedBy,
                    EditedBy = s.EditedBy,
                    PublishedBy = s.PublishedBy,
                    LastReceiver = s.LastReceiver,
                    ApprovedBy = s.ApprovedBy,
                    ReturnedBy = s.ReturnedBy,
                    CreatedDate = s.CreatedDate,
                    DistributionDate = s.DistributionDate,
                    LastModifiedDate = s.LastModifiedDate,
                    ApprovedDate = s.ApprovedDate,
                    ViewCount = s.ViewCount,
                    Status = s.Status,
                    IsOnHome = s.IsOnHome,
                    Avatar = s.Avatar,
                    ViewRoyalties = 0,
                    ZoneName = s.ZoneName,
                    Note = s.Note,
                    Author = s.Author,
                    ErrorCheckedBy = s.ErrorCheckedBy,
                    ErrorCheckedDate = s.ErrorCheckedDate,
                    SensitiveCheckedBy = s.SensitiveCheckedBy,
                    SensitiveCheckedDate = s.SensitiveCheckedDate,
                    WordCount = s.WordCount,
                    Sapo = s.Sapo,
                    Url =s.Url,
                    DisplayPosition=s.DisplayPosition,
                    Priority=s.Priority,
                    Source=s.Source,
                    IsFocus=s.IsFocus,
                    NewsType=s.NewsType,
                    IsProd=s.IsProd,
                    IsPr=s.IsPr,
                    OriginalId=s.OriginalId,
                    ZoneId = s.ZoneId
                }).ToList();
                BoSearch.Base.News.NewsDalFactory.InitAllNews(list);
            }, action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null, int zoneid = 0)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, startPage, startDate, endDate, () =>
            {
                var data = NewsDal.InitRedisAllNews(1, pageSize, startDate, endDate, ref totalRows, zoneid);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = NewsDal.InitRedisAllNews(page, pageSize, startDate, endDate, ref totalRows, zoneid);
                BoCached.Base.News.NewsDalFactory.InitAllNews(data);
            }, action);
        }
        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByNewsCheckDuplicateAsync(string name, int startPage, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, startPage, startDate, endDate, () =>
            {
                var data = NewsDal.InitRedisAllNewsPublish(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = NewsDal.InitRedisAllNewsPublish(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.News.NewsDalFactory.InitAllNewsCheckDuplicate(data);
            }, action);
        }

        public static dynamic InitRedisToSql(string username, DateTime startDate, DateTime endDate)
        {
            try
            {
                var data = NewsDal.GetNewsPublishByDate(startDate, endDate);
                if (data != null && data.Count > 0)
                {
                    var list = BoCached.Base.News.NewsDalFactory.GetNewsByListId(data.Select(s => s.Id.ToString()).ToList());

                    if (list != null && list.Count > 0)
                    {
                        var count = 0;
                        foreach (var item in list)
                        {
                            var publishedBy = item.PublishedBy;
                            var approvedBy = item.ApprovedBy;
                            if (string.IsNullOrEmpty(item.PublishedBy))
                            {
                                publishedBy = "";
                            }
                            if (string.IsNullOrEmpty(item.ApprovedBy))
                            {
                                approvedBy = "";
                            }
                            if (NewsDal.UpdateNewsByPublishBy(item.Id, publishedBy, approvedBy))
                                count++;
                        }
                        return new { done = count };
                    }

                    return new { list = false };
                }
                return new { data = false };
            }
            catch (Exception ex)
            {
                return new { data = false };
            }
        }

        public static List<NewsInListEntity> SearchNewsTemp(string keyword, string username, string zoneIds,
                                                      DateTime fromDate,
                                                      DateTime toDate,
                                                      NewsFilterFieldForUsername filterFieldForUsername,
                                                      NewsSortExpression sortOrder,
                                                      NewsStatus status,
                                                      NewsType newsType, int pageIndex, int pageSize,
                                                      ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsTemp(keyword, username, zoneIds, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static List<NewsInListEntity> SearchNewsByNewsId(long NewsId)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsByNewsId(NewsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static List<NewsInListEntity> SearchNewsPublishedForSecretary(string keyword, string username, int zoneId,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsPublishedForSecretary(keyword, username, zoneId, fromDate, toDate,
                                                      pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsInListEntity> SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsTemp_PublishedForSecretary(keyword, username, zoneId, fromDate, toDate,
                                                      pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsInListEntity> SearchNewsPr(string keyword, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate,
                                                        NewsFilterFieldForUsername filterFieldForUsername,
                                                        NewsSortExpression sortOrder,
                                                        NewsStatus status,
                                                        NewsType newsType, int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsPr(keyword, username, zoneIds, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsInListEntity> SearchNewsByUsername(string keyword, string username, string zoneIds,
                                                        DateTime fromDate,
                                                        DateTime toDate, int pageIndex, int pageSize,
                                                        ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsInListEntity> SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                return NewsDal.SearchNewsTemp_ByUsername(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize,
                                                      ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        public static List<NewsSearchEntity> SearchNewsByType(string keyword, string author, NewsType type, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate, int pageIndex, int pageSize,
                                                       ref int totalRow, string createdBy)
        {
            return BoSearch.Base.News.NewsDalFactory.SearchNewsByType(keyword, author, (int)type, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public static List<NewsSearchEntity> SearchNewsByTypeSubsite(string keyword, string author, NewsType type, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate, int pageIndex, int pageSize,
                                                       ref int totalRow, string createdBy)
        {
            var zoneTotalChecks = new List<int>();
            var zoneParentIds = zoneIds.Split(',').Select(Int32.Parse).ToList();

            foreach (var zoneParentId in zoneParentIds)
            {
                zoneTotalChecks.Add(zoneParentId);
                var zoneChildrens = ZoneDal.GetZoneActiveByParentId(zoneParentId);
                var zoneChildrenIds = zoneChildrens?.Select(c => c.Id)?.ToList();
                zoneTotalChecks.AddRange(zoneChildrenIds);
            }

            return BoSearch.Base.News.NewsDalFactory.SearchNewsByTypeSubsite(keyword, author, (int)type, zoneIds, zoneTotalChecks, fromDate, toDate, pageIndex, pageSize, ref totalRow, createdBy);
        }

        public static List<NewsSearchEntity> GetListMagazineByType(string keyword, int type, int magazineType,
                                                       int pageIndex, int pageSize,
                                                       ref int totalRow)
        {
            var data = BoSearch.Base.News.NewsDalFactory.GetListMagazineByType(keyword, type, magazineType, pageIndex, pageSize, ref totalRow);
            return data;
        }

        public static List<NewsInListEntity> SearchNewsByType2(string keyword, NewsType type, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                var newsSearch = BoSearch.Base.News.NewsDalFactory.SearchNewsByType2(keyword, (int)type, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                if (null != newsSearch && newsSearch.Count > 0)
                {
                    newsList = BoCached.Base.News.NewsDalFactory.GetNewsByListId(newsSearch);
                }
                if (null == newsList || (newsList != null && newsList.Count() <= 0))
                {
                    //db sql
                    newsList = NewsDal.SearchNewsByType(keyword, (int)type, zoneIds, fromDate, toDate, pageIndex, pageSize,
                                                          ref totalRow);
                }

                return newsList;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return newsList;
            }
        }

        public static List<CountStatus> CountNews(string username, string listStatus, int type)
        {
            try
            {
                if (string.IsNullOrEmpty(listStatus))
                {
                    return null;
                }

                var list = listStatus.Split(',').ToList();
                list = list.OrderBy(n => Convert.ToInt32(n)).ToList();

                var hashName = string.Format("{0}:{1}:{2}", username, string.Join("-", list.ToArray()), type);

                var currentUserName = username;
                var data = BoCached.Base.News.NewsDalFactory.CountStatus(hashName);

                if (data == null || (data != null && data.Count() <= 0))
                {
                    var statusZoneIds = new Dictionary<string, dynamic>();
                    var user = UserDataBo.GetUserByUsername(username);
                    if (user == null)
                    {
                        return null;
                    }
                    var isRole = user.IsRole;
                    var listUserPermission = PermissionBo.GetListByUserId(user.Id, true);//PermissionBo.GetPermissionByUsername2(username);
                    var listCurrentRole = listUserPermission.Select(s => s.PermissionId).Distinct().ToList();

                    foreach (var status in list)
                    {
                        var filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                        var zoneIds = string.Empty;//"1,2,3,4,5,31,33,47,61,79,88,114,118,129,132,135,136,140,141,142,146,149,150,152,181,189,192,194,86";
                        username = currentUserName;

                        //check quyền trả list tin                        
                        var newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                        switch (newsStatus)
                        {
                            #region //bài của tôi
                            case NewsStatus.Unknow:
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                break;
                            case NewsStatus.All:
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                break;
                            #endregion
                            #region //chỉ quyền phong viên mới thấy list này
                            case NewsStatus.Temporary:
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                break;
                            case NewsStatus.ReturnedToReporter://chỉ quyền phong viên mới thấy list này
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleReporter))
                                {
                                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleReporter).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            #endregion
                            #region //chỉ quyền biên tập viên mới thấy list này
                            case NewsStatus.WaitForEdit:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                                {
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            case NewsStatus.ReceivedForEdit:
                                filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                                break;
                            case NewsStatus.ReturnedToEditor:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                                {
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                else
                                    username = "KhongCoQuyen";
                                break;
                            #endregion
                            #region //chỉ quyền thư ký mới thấy list này
                            case NewsStatus.WaitForPublish:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            case NewsStatus.ReceivedForPublish:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {
                                    filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            case NewsStatus.ReturnedToEditorialSecretary:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            #endregion
                            #region //chỉ quyền thu ky va ban biên tập mới thấy list này
                            case NewsStatus.Published:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                else {
                                    username = currentUserName;
                                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                }
                                break;
                            case NewsStatus.WaitForEditorialBoard:
                                if (!user.IsFullPermission)
                                {
                                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                        username = string.Empty;
                                }
                                else
                                {
                                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                        username = string.Empty;
                                }
                                if (null != listUserPermission && listUserPermission.Count > 0)
                                {
                                    zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                    
                                }
                                break;
                            case NewsStatus.ReceivedForEditorialBoard:
                                if (!user.IsFullPermission)
                                {
                                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin) && (isRole == (int)IsRole.EditorialBoard || isRole == (int)IsRole.Secretary))
                                        filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                                }
                                else
                                {
                                    if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                        filterFieldForUsername = NewsFilterFieldForUsername.PublishedBy;
                                }
                                break;
                            case NewsStatus.Unpublished://chỉ quyền thư ký và ban biên tập mới thấy list này
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {                                    
                                    username = string.Empty;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                break;
                            #endregion
                            case NewsStatus.MovedToTrash:
                                filterFieldForUsername = NewsFilterFieldForUsername.LastModifiedBy;
                                break;
                            case NewsStatus.ReturnToCooperator:
                                filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;
                                break;
                            #region //List trả lại đích danh BTV hoặc TK
                            case NewsStatus.ReturnedToMyEditor:
                                if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleEditor))
                                {
                                    filterFieldForUsername = NewsFilterFieldForUsername.EditedBy;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleEditor).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                else if (listCurrentRole.Exists(e => e == (int)EnumPermission.ArticleAdmin))
                                {
                                    filterFieldForUsername = NewsFilterFieldForUsername.ApprovedBy;
                                    if (null != listUserPermission && listUserPermission.Count > 0)
                                    {
                                        zoneIds = string.Join(",", listUserPermission.Where(w => w.PermissionId == (int)EnumPermission.ArticleAdmin).Select(s => s.ZoneId).Distinct().ToArray());                                        
                                    }
                                }
                                else
                                    filterFieldForUsername = NewsFilterFieldForUsername.CreatedBy;

                                break;
                            #endregion
                            #region //List crawler
                            case NewsStatus.CrawlerNews:
                            case NewsStatus.CrawlerByVccorp:
                                username = string.Empty;
                                zoneIds = "";
                                break;
                                #endregion
                        };

                        statusZoneIds[status] = new { zoneIds, filterFieldForUsername, username };
                    }

                    data = BoSearch.Base.News.NewsDalFactory.CountNews(list, statusZoneIds, type);
                    //update data redis
                    BoCached.Base.News.NewsDalFactory.AddCountStatus(hashName, data);
                }
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<NewsInListEntity> SearchNewsByStatus(string keyword, string userDoAction, string userForFilter, NewsFilterFieldForUsername userFieldForFilter,
                                                                    string zoneIds, DateTime fromDate, DateTime toDate, NewsType newsType,
                                                                    NewsStatus status, bool isGetTotalRow, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsDal.SearchNewsByStatus(keyword, userDoAction, userForFilter, (int)userFieldForFilter, zoneIds, fromDate, toDate,
                                                  (int)newsType, (int)status, isGetTotalRow, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:13
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="keyword"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsPublishForObjectBoxEntity> SearchNewsForNewsRelation(int zoneId, string keyword,
                                                                                    int pageIndex, int pageSize,
                                                                                    ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            var list = new List<NewsPublishForObjectBoxEntity>();
            try
            {
                var data = BoSearch.Base.News.NewsDalFactory.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
                if (data != null && data.Count > 0)
                {
                    list = data.Select(s => new NewsPublishForObjectBoxEntity
                    {
                        NewsId = s.Id,
                        EncryptId = s.EncryptId,
                        Title = s.Title,
                        Sapo = s.Sapo,
                        Avatar = s.Avatar,
                        //Avatar2 = s.Avatar2,
                        //Avatar3 = s.Avatar3,
                        //Avatar4 = s.Avatar4,
                        //Avatar5 = s.Avatar5,
                        DistributionDate = s.DistributionDate,
                        Url = s.Url,
                        //ZoneId = s.ZoneId,
                        //DisplayStyle = s.DisplayStyle
                    }).ToList();
                    //list = BoCached.Base.News.NewsDalFactory.GetNewsPublishByListId(data.Select(s=>s.Id.ToString()).ToList());
                }
                if (list == null || (list != null && list.Count <= 0))
                {
                    //list = NewsPublishDal.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow);
                }
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return list;
            }
        }
        public static List<NewsPublishForObjectBoxEntity> SearchNewsPublishTemp_ForNewsRelation(int zoneId, string keyword,
                                                                                    int pageIndex, int pageSize,
                                                                                    ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            var list = new List<NewsPublishForObjectBoxEntity>();
            try
            {
                var data = BoSearch.Base.News.NewsDalFactory.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
                if (data != null && data.Count > 0)
                {
                    list = data.Select(s => new NewsPublishForObjectBoxEntity
                    {
                        NewsId = s.Id,
                        EncryptId = s.EncryptId,
                        Title = s.Title,
                        Sapo = s.Sapo,
                        Avatar = s.Avatar,
                        //Avatar2 = s.Avatar2,
                        //Avatar3 = s.Avatar3,
                        //Avatar4 = s.Avatar4,
                        //Avatar5 = s.Avatar5,
                        DistributionDate = s.DistributionDate,
                        Url = s.Url,
                        //ZoneId = s.ZoneId,
                        //DisplayStyle = s.DisplayStyle,
                        ZoneName=s.ZoneName
                    }).ToList();
                    //list = BoCached.Base.News.NewsDalFactory.GetNewsPublishByListId(data.Select(s=>s.Id.ToString()).ToList());
                }
                if (list == null || (list != null && list.Count <= 0))
                {
                    //list = NewsPublishDal.SearchNewsPublishTemp_ForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow);
                    //tam coment => ko co es thi thoi ko vao db
                    //list = NewsPublishDal.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow);
                }
                return list;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return list;
            }
        }

        /// <summary>
        /// NgocNH - 7/1/2015
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="tagId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsPublishForObjectBoxEntity> SearchNewsForNewsTagRelation(int zoneId, int tagId,
                                                                                    int pageIndex, int pageSize,
                                                                                    ref int totalRow)
        {
            try
            {
                return NewsPublishDal.SearchNewsPublishForNewsTagRelation(zoneId, tagId, pageIndex, pageSize,
                                                                       ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:14
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="displayPosition"></param>
        /// <param name="distributedDateFrom"></param>
        /// <param name="distributedDateTo"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <param name="excludeNewsIds"></param>
        /// <returns></returns>
        public static List<NewsPublishForSearchEntity> SearchNewsPublished(int zoneId, string keyword, int type,
                                                                           int displayPosition,
                                                                           DateTime distributedDateFrom,
                                                                           DateTime distributedDateTo, int pageIndex,
                                                                           int pageSize, ref int totalRow,
                                                                           string excludeNewsIds)
        {
            try
            {
                var relatedNews = NewsPublishDal.SearchNewsPublished(zoneId, keyword, type, displayPosition,
                                                                     distributedDateFrom, distributedDateTo, pageIndex,
                                                                     pageSize, ref totalRow);
                if (null != relatedNews)
                {
                    if (!string.IsNullOrEmpty(excludeNewsIds))
                    {
                        excludeNewsIds = ";" + excludeNewsIds + ";";
                    }
                    else
                    {
                        excludeNewsIds = "";
                    }
                    foreach (var item in relatedNews)
                    {
                        var newsId = ";" + item.NewsId + ";";
                        if (excludeNewsIds.IndexOf(newsId) >= 0)
                        {
                            relatedNews.Remove(item);
                        }
                    }
                }
                return relatedNews;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }


        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:16
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="keyword"></param>
        /// <param name="type"></param>
        /// <param name="displayPosition"></param>
        /// <param name="distributedDateFrom"></param>
        /// <param name="distributedDateTo"></param>
        /// <param name="excludeNewsIds"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <returns></returns>
        public static List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type,
                                                                      int displayPosition, int priority,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<NewsInListEntity> SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type,
                                                                      int displayPosition,
                                                                      DateTime distributedDateFrom,
                                                                      DateTime distributedDateTo, string excludeNewsIds, int newsType,
            string excludePositionTypes,
                                                                      int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsWhichPublishedForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom,
                                                        distributedDateTo, excludeNewsIds, newsType, excludePositionTypes, pageIndex, pageSize,
                                                        ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:17
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsId(long newsId)
        {
            try
            {
                //var relateNews = BoCached.Base.News.NewsDalFactory.GetRelatedNewsByNewsId(newsId);
                //if (relateNews == null || (relateNews!=null && relateNews.Count<=0))
                //{
                //    relateNews = NewsPublishDal.GetRelatedNewsByNewsId(newsId);
                //    if (relateNews != null)
                //    {
                //        //add to redis
                //        BoCached.Base.News.NewsDalFactory.AddRelatedNewsByNewsId(newsId, relateNews);
                //    }
                //}

                var relateNews = NewsPublishDal.GetRelatedNewsByNewsId(newsId);

                return relateNews;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsIds(string newsIds)
        {
            var list = BoCached.Base.News.NewsDalFactory.GetRelatedNewsByNewsIds(newsIds);
            if (list == null)
            {
                list = NewsPublishDal.GetRelatedNewsByNewsIds(newsIds);
            }
            return list;
        }

        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            var newsInZone = BoCached.Base.News.NewsDalFactory.GetNewsInZoneByNewsId(newsId);
            if (newsInZone == null)
            {
                newsInZone = NewsInZoneDal.GetNewsInZoneByNewsId(newsId);
                if (newsInZone != null)
                {
                    //add to redis
                    BoCached.Base.News.NewsDalFactory.AddNewsInZoneByNewsId(newsId, newsInZone);
                }
            }
            return NewsInZoneDal.GetNewsInZoneByNewsId(newsId);
        }

        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByTypeNewsId(long newsId, int type)
        {
            try
            {
                return NewsPublishDal.GetRelatedNewsByTypeNewsId(newsId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:32
        /// </summary>
        /// <param name="listNewsId"></param>
        /// <returns></returns>
        public virtual List<NewsForOtherAPIEntity> GetNewsForOtherApi(string listNewsId)
        {
            try
            {
                var listNews = NewsDal.GetNewsForOtherAPI(listNewsId);
                if (listNews != null && listNews.Count > 0)
                    foreach (var news in listNews)
                    {
                        news.Url = BuildLinkUrl(news.Id, news.Type, news.ZoneShortUrl, news.Title, news.ZoneId);
                    }
                return listNews;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public virtual List<NewsForSuggestionEntity> GetNewsUseInterviewByListInterviewId(string listInterviewId)
        {
            return NewsDal.GetNewsUseInterviewByListInterviewId(listInterviewId);
        }

        public static List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        {
            return NewsDal.GetTopMostNewsInHour(top, zoneId);
        }

        public static List<NewsWithAnalyticEntity> GetHotNews(int top, int day)
        {
            var news = new List<NewsWithAnalyticEntity>();
            var newsSearch = BoSearch.Base.News.NewsDalFactory.GetHotNews(top, day);
            if (newsSearch.Count > 0)
            {
                try
                {
                    var listIds = newsSearch.Select(n => n.Id).ToList();
                    var url = CmsChannelConfiguration.GetAppSetting("UrlGetViewCount");
                    var secretKey = CmsChannelConfiguration.GetAppSetting("UrlGetViewCountSecretKey");
                                        
                    var newsIds = "[" + string.Join(",", listIds) + "]";
                    url = string.Format(url, newsIds);

                    WebRequest request = WebRequest.Create(url);
                    request.Headers.Add("cms_api_key", secretKey ?? "5e80e09f5155411e90153db772ce084b");
                    WebResponse response = request.GetResponse();
                    Stream data = response.GetResponseStream();
                    string html = string.Empty;
                    using (StreamReader sr = new StreamReader(data))
                    {
                        html = sr.ReadToEnd();
                    }

                    var dataViewCountDynamic = NewtonJson.Deserialize<dynamic>(html);
                    var dataViewCount = new List<NewsWithAnalyticEntity>();
                    for (int i = 0; i < dataViewCountDynamic.Count; i++)
                    {
                        dataViewCount.Add(new NewsWithAnalyticEntity()
                        {
                            NewsId = dataViewCountDynamic[i].item_id,
                            ViewCount = dataViewCountDynamic[i].total_view.view_pc + dataViewCountDynamic[i].total_view.view_mob,
                            ViewCountDaily = dataViewCountDynamic[i].view_current_date.view_pc + dataViewCountDynamic[i].view_current_date.view_mob,
                            ViewCountHourly = dataViewCountDynamic[i].view_current_hourly.view_pc + dataViewCountDynamic[i].view_current_hourly.view_mob
                        });
                    }

                    var lst = dataViewCount.OrderByDescending(n => n.ViewCountHourly).Take(top).ToList();

                    //foreach (var item in lst)
                    //{
                    //    var newsItem = newsSearch.SingleOrDefault(n => n.Id == item.NewsId);
                    //    if (newsItem != null)
                    //    {
                    //        item.Title = newsItem.Title;
                    //        item.Avatar = newsItem.Avatar;
                    //        item.DistributionDate = newsItem.DistributionDate;
                    //        news.Add(item);
                    //    }
                    //}

                    foreach (var item in newsSearch)
                    {
                        var newsItem = lst.SingleOrDefault(n => n.NewsId == item.Id);
                        if (newsItem != null)
                        {
                            newsItem.Title = item.Title;
                            newsItem.Avatar = item.Avatar;
                            newsItem.DistributionDate = item.DistributionDate;
                            news.Add(newsItem);
                        }
                    }
                }
                catch (Exception err)
                {
                    return news;
                }
            }
            return news;

            //return NewsDal.GetHotNews(top);
        }

        public static List<NewsWithAnalyticEntity> GetHotNews2(int top, int day)
        {
            //cache
            var news = BoCached.Base.Statistic.StatisticDalFactory.GetHotNews(top, day); //new List<NewsWithAnalyticEntity>();
            if (news != null && news.Count > 0)
                return news;

            news = new List<NewsWithAnalyticEntity>();
            var newsSearch = BoSearch.Base.News.NewsDalFactory.GetHotNews(5000, day);
            if (newsSearch.Count > 0)
            {
                try
                {
                    var listIds = newsSearch.Select(n => n.Id).ToList();                    

                    var url = CmsChannelConfiguration.GetAppSetting("UrlGetViewCount");
                    var secretKey = CmsChannelConfiguration.GetAppSetting("UrlGetViewCountSecretKey");

                    var total = listIds.Count;
                    var page = total / 50;
                    if (total % 50 > 0)
                        page++;

                    var dataViewCount = new List<NewsWithAnalyticEntity>();
                    var newsIds = "";
                    if (total <= 50)
                    {
                        newsIds = "[" + string.Join(",", listIds) + "]";

                        url = string.Format(url, newsIds);
                        WebRequest request = WebRequest.Create(url);
                        request.Headers.Add("cms_api_key", secretKey ?? "5e80e09f5155411e90153db772ce084b");
                        WebResponse response = request.GetResponse();
                        Stream data = response.GetResponseStream();
                        string html = string.Empty;
                        using (StreamReader sr = new StreamReader(data))
                        {
                            html = sr.ReadToEnd();
                        }

                        var dataViewCountDynamic = NewtonJson.Deserialize<dynamic>(html);

                        for (int i = 0; i < dataViewCountDynamic.Count; i++)
                        {
                            dataViewCount.Add(new NewsWithAnalyticEntity()
                            {
                                NewsId = dataViewCountDynamic[i].item_id,
                                ViewCount = dataViewCountDynamic[i].total_view.view_pc + dataViewCountDynamic[i].total_view.view_mob,
                                ViewCountDaily = dataViewCountDynamic[i].view_current_date.view_pc + dataViewCountDynamic[i].view_current_date.view_mob,
                                ViewCountHourly = dataViewCountDynamic[i].view_current_hourly.view_pc + dataViewCountDynamic[i].view_current_hourly.view_mob
                            });
                        }
                    }
                    else
                    {
                        for (int k = 0; k < page; k++)
                        {
                            newsIds = "[" + string.Join(",", listIds.Skip(k * 50).Take(50).ToList()) + "]";

                            var path = string.Format(url, newsIds);
                            WebRequest request = WebRequest.Create(path);
                            request.Headers.Add("cms_api_key", secretKey ?? "5e80e09f5155411e90153db772ce084b");
                            WebResponse response = request.GetResponse();
                            Stream data = response.GetResponseStream();
                            string html = string.Empty;
                            using (StreamReader sr = new StreamReader(data))
                            {
                                html = sr.ReadToEnd();
                            }

                            var dataViewCountDynamic = NewtonJson.Deserialize<dynamic>(html);

                            for (int i = 0; i < dataViewCountDynamic.Count; i++)
                            {
                                dataViewCount.Add(new NewsWithAnalyticEntity()
                                {
                                    NewsId = dataViewCountDynamic[i].item_id,
                                    ViewCount = dataViewCountDynamic[i].total_view.view_pc + dataViewCountDynamic[i].total_view.view_mob,
                                    ViewCountDaily = dataViewCountDynamic[i].view_current_date.view_pc + dataViewCountDynamic[i].view_current_date.view_mob,
                                    ViewCountHourly = dataViewCountDynamic[i].view_current_hourly.view_pc + dataViewCountDynamic[i].view_current_hourly.view_mob
                                });
                            }
                        }
                    }

                    var lst = dataViewCount.OrderByDescending(n => n.ViewCount).Take(top).ToList();

                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            var newsItem = newsSearch.SingleOrDefault(n => n.Id == item.NewsId);
                            if (newsItem != null)
                            {
                                item.Title = newsItem.Title;
                                item.Avatar = newsItem.Avatar;
                                item.DistributionDate = newsItem.DistributionDate;
                                news.Add(item);
                            }
                        }
                    }
                    else {
                        newsSearch = newsSearch.Take(top).ToList();
                        foreach (var item in newsSearch)
                        {
                            var newsItem = lst.SingleOrDefault(n => n.NewsId == item.Id);
                            if (newsItem != null)
                            {
                                newsItem.Title = item.Title;
                                newsItem.Avatar = item.Avatar;
                                newsItem.DistributionDate = item.DistributionDate;
                                news.Add(newsItem);
                            }
                            else
                            {
                                news.Add(new NewsWithAnalyticEntity {
                                    Title = item.Title,
                                    Avatar = item.Avatar,
                                    DistributionDate = item.DistributionDate
                                });
                            }                           
                        }
                    }
                }
                catch
                {
                    return news;
                }
            }
            //cache
            BoCached.Base.Statistic.StatisticDalFactory.AddHotNews(top, day, news);

            return news;            
        }

        public static List<NewsEntity> SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status,
                                                       int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsByTag(tagIds, tagNames, searchBytag, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTag:{0}", ex.Message));
            }
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsDal.SearchNewsPublishExcludeNewsInTag(zoneId, keyword, tagId, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsDal.SearchNewsPublishExcludeNewsInMc(zoneId, keyword, mcId, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            return NewsDal.SearchNewsPublishExcludeNewsInThread(zoneId, keyword, threadId, pageIndex, pageSize, ref totalRow);
        }
        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            //var data = new List<NewsInListEntity>();
            //var list = BoSearch.Base.News.NewsDalFactory.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
            //if(list == null || (list != null && list.Count<0))
            //    data = NewsDal.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
            //else
            //    data = list.Select(s => new NewsInListEntity
            //    {
            //        Id=s.Id,
            //        EncryptId=s.EncryptId,
            //        Avatar=s.Avatar,
            //        Title=s.Title, 
            //        Sapo=s.Sapo,
            //        Url=s.Url,
            //        CreatedBy=s.CreatedBy,
            //        CreatedDate=s.CreatedDate,
            //        DistributionDate=s.DistributionDate,
            //        Author=s.Author,
            //        DisplayPosition=s.DisplayPosition                
            //    }).ToList();

            return NewsDal.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
        }

        /// <summary>
        /// NANIA bổ sung hàm SearchNewsByFocusKeyword
        /// Tìm các bài có sử dụng từ khóa Focus này
        /// Created By: 22/03/2013
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            try
            {
                return NewsDal.SearchNewsByFocusKeyword(keyword, top);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> CheckNewsByKeyword(string keyword, int page, int num, ref int total)
        {
            try
            {
                return NewsDal.CheckNewsByKeyword(keyword, page, num, ref total);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByFocusKeyword:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> SearchNewsByTagId(long tagId)
        {
            try
            {
                return NewsDal.SearchNewsByTagId(tagId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTagId:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsByTagIdWithPaging(tagId, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTagIdWithPaging:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsByMcIdWithPaging(mcId, type, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTagIdWithPaging:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return NewsDal.SearchNewsByThreadIdWithPaging(threadId, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByThreadIdWithPaging:{0}", ex.Message));
            }
        }
        public static List<NewsEntity> SearchNewsByTopicIdWithPaging(string keyword, long topicId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                //if(!string.IsNullOrEmpty(keyword))
                return NewsDal.SearchNewsByTopicIdWithPaging(keyword, topicId, status, pageIndex, pageSize, ref totalRow);

                //var hashName = "NewsInTopic:" + topicId;
                //var key = pageIndex + ":" + pageSize;
                ////get cache
                //var valueData = BoCached.Base.Topic.TopicDalFactory.GetDataSearchNewsInTopic(hashName, key);
                //if (!string.IsNullOrEmpty(valueData))
                //{
                //    var data = NewtonJson.Deserialize<NewsInTopicSearch>(valueData);
                //    totalRow = data.TotalRow;
                //    return data.News;
                //}
                //else {
                //    //ko co lay tu db                    
                //    var dataSearch = NewsDal.SearchNewsByTopicIdWithPaging("",topicId, pageIndex, pageSize, ref totalRow);
                //    if (dataSearch.Count > 0)
                //    {
                //        var value = new NewsInTopicSearch()
                //        {
                //            TotalRow = totalRow,
                //            News = dataSearch
                //        };
                //        string valueJson = NewtonJson.Serialize(value);
                //        BoCached.Base.Topic.TopicDalFactory.AddDataSearchNewsInTopic(hashName, key, valueJson);
                //    }
                //    return dataSearch;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTopicIdWithPaging:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> SearchNewsPublishByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var hashName = "NewsPublishInTopic:" + topicId;
                var key = pageIndex + ":" + pageSize;
                //get cache
                var valueData = BoCached.Base.Topic.TopicDalFactory.GetDataSearchNewsInTopic(hashName, key);
                if (!string.IsNullOrEmpty(valueData))
                {
                    var data = NewtonJson.Deserialize<NewsInTopicSearch>(valueData);
                    totalRow = data.TotalRow;
                    return data.News;
                }
                else {
                    //ko co lay tu db     
                    var totalRows = 0;
                    var dataSearch = NewsDal.SearchNewsPublishByTopicIdWithPaging(topicId, pageIndex, pageSize, ref totalRow);
                    totalRows = totalRow;
                    if (dataSearch.Count > 0)
                    {
                        var value = new NewsInTopicSearch()
                        {
                            TotalRow = totalRows,
                            News = dataSearch
                        };
                        string valueJson = NewtonJson.Serialize(value);
                        BoCached.Base.Topic.TopicDalFactory.AddDataSearchNewsInTopic(hashName, key, valueJson);
                    }
                    return dataSearch;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchNewsByTopicIdWithPaging:{0}", ex.Message));
            }
        }

        public static List<NewsEntity> SearchHotNewsByTopicId(long topicId)
        {
            try
            {
                return NewsDal.SearchHotNewsByTopicId(topicId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SearchHotNewsByTopicId:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:35
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="zoneIds"></param>
        /// <param name="role"></param>
        /// <param name="commonStatus"></param>
        /// <param name="isByZone"></param>
        /// <returns></returns>
        public static List<NewsCounterEntity> CounterNewsByStatus(string accountName, int role, string commonStatus)
        {
            try
            {
                var userInfo = UserBo.GetUserByUsername(accountName);
                var isByZone = !userInfo.IsFullZone;

                var zoneIds = string.Empty;
                if (isByZone)
                {
                    var zoneList = ZoneBo.GetListZoneByUsername(accountName);
                    if (zoneList != null && zoneList.Count > 0)
                    {
                        zoneIds = zoneList.Aggregate(zoneIds, (current, zoneEntity) => current + ("," + zoneEntity.Id));
                        if (!string.IsNullOrEmpty(zoneIds)) zoneIds = zoneIds.Remove(0, 1);
                    }
                }
                return NewsDal.CounterNewsByStatus(accountName, zoneIds, role, commonStatus, isByZone);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: ThanhTN
        /// LastModifiedDate: 2013-08-07 14:35
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="zoneId"></param>
        /// <returns></returns>
        public static List<NewsCounterEntity> GetNewsCounterByStatus(string accountName, int zoneId)
        {
            var counters = NewsDal.GetCounter(accountName, zoneId);
            foreach (var status in Enum.GetValues(typeof(NewsStatus)))
            {
                if (counters.FindIndex(item => item.Status == (int)status) < 0)
                {
                    counters.Add(new NewsCounterEntity
                    {
                        Status = (int)status,
                        TotalRow = 0
                    });
                }
            }
            return counters;
        }

        /// <summary>
        /// Get dánh sách các bài viết để tính nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public static List<NewsForRoyaltiesEntity> GetRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                return NewsDal.SelectRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate)
        {
            var counters = NewsDal.GetZoneCounter(zoneId, fromDate, endDate);
            return counters;
        }

        public static List<NewsForRoyaltiesEntity> GetBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                return NewsDal.SelectBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetPRRoyalties(string zoneId, DateTime startDate, DateTime endDate,
                                                                string creator, string author, int pageIndex,
                                                                int pageSize, int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                return NewsDal.SelectPRRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyalties:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate,
                                                               string creator, int author, int pageIndex, int pageSize, ref int totalRows,
                                                               int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                return NewsDal.SelectRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRows, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyaltiesV2:{0}", ex.Message));
            }
        }

        public static List<NewsForRoyaltiesEntity> GetRoyaltiesPRV2(string zoneId, DateTime startDate, DateTime endDate,
                                                              string creator, int author, int pageIndex, int pageSize, ref int totalRows,
                                                              int order, string NewsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            try
            {
                return NewsDal.SelectPRRoyaltiesV2(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, ref totalRows, order, NewsCategories, (int)newsType, viewCountFrom, viewCountTo);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectRoyaltiesV2:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Count tổng số bản ghi
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <returns></returns>
        public static int CountRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                return NewsDal.CountRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }

        public static int CountBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                return NewsDal.CountBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountRoyalties:{0}", ex.Message));
            }
        }

        public static int CountPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                         string author, string NewsCategories, NewsType newsType, int viewCountFrom, int viewCountTo, int originalId = 0)
        {
            try
            {
                return NewsDal.CountPRRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountPRRoyalties:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Get tổng tien nhuan but
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <returns></returns>
        public static decimal GetTotalPrice(string zoneId, DateTime startDate, DateTime endDate, string creator,
                                            string author, string newsCategories, NewsType newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            try
            {
                return NewsDal.SelectTotalPrice(zoneId, startDate, endDate, creator, author, newsCategories, (int)newsType, viewCountFrom, viewCountTo, originalId);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.SelectTotalPrice:{0}", ex.Message));
            }
        }

        /// <summary>
        /// Set giá tiền cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Giá tiền</param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SetPrice(long id, decimal price)
        {
            var isSuccess = NewsDal.UpdatePrice(id, price);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bonusPrice"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SetBonusPrice(long id, decimal bonusPrice)
        {
            var isSuccess = NewsDal.UpdateBonusPrice(id, bonusPrice);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isOnHome"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateIsOnHome(long id, bool isOnHome, string accountName)
        {
            //var isSuccess = NewsDal.UpdateIsOnHome(id, isOnHome);
            //return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (NewsDal.UpdateIsOnHome(id, isOnHome))
            {
                //update title redis
                if(!BoCached.Base.News.NewsDalFactory.UpdateNewsIsOnHome(id, isOnHome, accountName))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ISONHOME, TopicName.REDIS, new NewsEntity { Id=id,IsOnHome=isOnHome,LastModifiedBy=accountName });
                }
                //update title es
                if(!BoSearch.Base.News.NewsDalFactory.UpdateNewsIsOnHome(id, isOnHome, accountName))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ISONHOME, TopicName.ES, new NewsEntity { Id = id, IsOnHome = isOnHome, LastModifiedBy = accountName });
                }

                var flagNewsHistory = NewsHistoryBo.UpdateNewsIsOnHome(id, isOnHome, accountName);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
                return ErrorMapping.ErrorCodes.UnknowError;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateIsFocus(long id, bool isFocus, string accountName)
        {
            //var isSuccess = NewsDal.UpdateIsFocus(id, isFocus);
            //return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (NewsDal.UpdateIsFocus(id, isFocus))
            {
                //update title redis
                if(!BoCached.Base.News.NewsDalFactory.UpdateNewsIsFocus(id, isFocus, accountName))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ISFOCUS, TopicName.REDIS, new NewsEntity { Id = id, IsFocus = isFocus, LastModifiedBy = accountName });
                }
                //update title es
                if(!BoSearch.Base.News.NewsDalFactory.UpdateNewsIsFocus(id, isFocus, accountName))
                {
                    QueueManager.AddToQueue(ActionName.UPDATE_NEWS_ISFOCUS, TopicName.ES, new NewsEntity { Id = id, IsFocus = isFocus, LastModifiedBy = accountName });
                }

                var flagNewsHistory = NewsHistoryBo.UpdateNewsIsFocus(id, isFocus, accountName);

                return ErrorMapping.ErrorCodes.Success;
            }
            else
                return ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes UpdateSeoReviewStatus(long id, string accountName)
        {
            var reviewed = (int)SEOMetaNewsReviewStatusEnum.Reviewed;    
            if (NewsDal.UpdateSeoReviewStatus(id, reviewed, accountName))
            {
                //update redis
                BoCached.Base.News.NewsDalFactory.UpdateSeoReviewStatus(id, reviewed, accountName);
                //update es
                BoSearch.Base.News.NewsDalFactory.UpdateNewsSeoReviewStatus(id, reviewed, accountName);

                //logaction
                ActivityBo.LogActivity(0, accountName, accountName,"","","Update Reviewed Seo", accountName + " đã reviewed seo cho bài viết " + id.ToString(), SocialNetwork.Entity.EnumActivityType.News);
                
                return ErrorMapping.ErrorCodes.Success;
            }
            else
                return ErrorMapping.ErrorCodes.UnknowError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="displayInSlide"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateDisplayInSlide(long id, int displayInSlide)
        {
            var isSuccess = NewsDal.UpdateDisplayInSlide(id, displayInSlide);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        /// <summary>
        /// Set ghi chú khi chấm nhuận bút cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="note">Ghi chú</param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SetNoteRoyalties(long id, string note)
        {
            var isSuccess = NewsDal.UpdateNoteRoyalties(id, note);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes SetNewsCategory(long id, int newsCategory)
        {
            var isSuccess = NewsDal.UpdateNewsCategory(id, newsCategory);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static NewsForValidateEntity GetNewsForValidateById(long id)
        {
            return NewsDal.GetNewsForValidateById(id);
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        {
            return NewsDal.GetNewsInfoForCachedById(id);
        }

        public static List<NewsUnPublishEntity> SearchNewsUnPublish(string keyword,
                                                                int ZoneId,
                                                                DateTime fromDate,
                                                                DateTime toDate,
                                                                int pageIndex,
                                                                int pageSize,
                                                                ref int totalRow)
        {
            var newsList = new List<NewsUnPublishEntity>();
            try
            {
                return NewsDal.SearchNewsUnPublish(keyword, ZoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsDashBoardUserEntity>();
            try
            {
                return NewsDal.GetDataDashBoardUser(username, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public static List<long> GetIds(string username, string zoneIds, int top)
        {
            var newsList = new List<long>();
            try
            {
                var data = BoSearch.Base.News.NewsDalFactory.ListNewsPublishByUserName(username, zoneIds, top);
                if(data==null || (data!=null && data.Count <= 0))
                {
                    data=NewsDal.GetIds(username, zoneIds, top);
                }
                return data;
                //return NewsDal.GetIds(username, zoneIds, top);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        #endregion

        #region News notification

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:51
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="username"></param>
        /// <param name="newsStatus"></param>
        /// <param name="isWarning"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SetNotification(long newsId, string username, NewsStatus newsStatus,
                                                 bool isWarning, string message)
        {
            try
            {
                if (newsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                if (string.IsNullOrEmpty(username))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }

                var existsNews = NewsDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }

                var user = UserDal.GetUserByUsername(username);
                if (null != user)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }

                return NewsNotificationDal.UpdateNotification(newsId, username, (int)newsStatus, isWarning, message)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:53
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReadNotification(long newsId, string username)
        {
            try
            {
                if (newsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                if (string.IsNullOrEmpty(username))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }

                var existsNews = NewsDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }

                var user = UserDal.GetUserByUsername(username);
                if (null == user)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }

                return NewsNotificationDal.ReadNotification(newsId, username)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:54
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="username"></param>
        /// <param name="labelId"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes SetLabelForNews(long newsId, string username, int labelId)
        {
            try
            {
                if (newsId <= 0)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                if (string.IsNullOrEmpty(username))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }
                if (string.IsNullOrEmpty(username))
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidLabel;
                }

                var existsNews = NewsDal.GetNewsForValidateById(newsId);
                if (null == existsNews)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }

                var user = UserDal.GetUserByUsername(username);
                if (null != user)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidUser;
                }

                var label = NewsLabelDal.GetNewsLabelById(labelId);
                if (null != label)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNotifictionInvalidLabel;
                }

                return NewsNotificationDal.UpdateLabel(newsId, username, labelId)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:55
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="username"></param>
        /// <param name="zoneIds"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="filterFieldForUsername"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<NewsInListWithNotifyEntity> SearchNewsWithNotification(string keyword, string username,
                                                                                  string zoneIds, DateTime fromDate,
                                                                                  DateTime toDate,
                                                                                  NewsFilterFieldForUsername
                                                                                      filterFieldForUsername,
                                                                                  NewsSortExpression sortOrder,
                                                                                  int pageIndex, int pageSize,
                                                                                  ref int totalRow,
                                                                                  params NewsStatus[] status)
        {
            try
            {
                var listStatus = status.Aggregate("",
                                                  (current, newsStatus) =>
                                                  current + ("," + Convert.ToInt32(newsStatus).ToString()));
                if (null != listStatus && string.IsNullOrEmpty(listStatus)) listStatus = listStatus.Substring(1);

                return NewsNotificationDal.SearchNewsWithNotification(keyword, username, zoneIds, fromDate, toDate,
                                                                      (int)filterFieldForUsername, (int)sortOrder,
                                                                      listStatus, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:55
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="username"></param>
        /// <param name="zoneId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRow"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static List<NewsInListWithNotifyEntity> SearchNewsWithNotificationV2(string keyword, string username,
                                                                                  int zoneId, DateTime fromDate,
                                                                                  DateTime toDate,
                                                                                  NewsSortExpression sortOrder,
                                                                                  int pageIndex, int pageSize,
                                                                                  ref int totalRow,
                                                                                  params NewsStatus[] status)
        {
            try
            {
                var listStatus = status.Aggregate("",
                                                  (current, newsStatus) =>
                                                  current + ("," + Convert.ToInt32(newsStatus).ToString()));
                if (null != listStatus && string.IsNullOrEmpty(listStatus)) listStatus = listStatus.Substring(1);

                return NewsNotificationDal.SearchNewsWithNotificationV2(keyword, username, zoneId, fromDate, toDate, (int)sortOrder,
                                                                      listStatus, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:55
        /// </summary>
        /// <param name="listNewsId"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static List<NewsNotificationEntity> GetNewsNotificationByNewsId(string listNewsId, string username)
        {
            try
            {
                return NewsNotificationDal.GetNewsNotificationByListNewsId(listNewsId, username);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:56
        /// </summary>
        /// <param name="username"></param>
        /// <param name="getFromDate"></param>
        /// <param name="unreadCount"></param>
        /// <returns></returns>
        public static List<NewsNotificationEntity> GetUnReadNewsNotificationByUsername(string username,
                                                                                       DateTime getFromDate,
                                                                                       ref int unreadCount)
        {
            try
            {
                return NewsNotificationDal.GetUnReadNewsNotificationByUsername(username, getFromDate, ref unreadCount);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #endregion

        #region News version

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:56
        /// </summary>
        /// <param name="news"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateVersion(NewsEntity news, string lastModifiedBy)
        {
            try
            {
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.AutoSaveNewsVersion(news, -1, news.ListZoneId, news.Tag, "", news.NewsRelation, lastModifiedBy);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

                //var result = NewsVersionDal.UpdateVersion(news, news.ListZoneId, lastModifiedBy)
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;
                //if (result == ErrorMapping.ErrorCodes.Success)
                //{
                //    //update redis
                //    BoCached.Base.NewsVersion.NewsVersionDalFactory.UpdateFirstVersion(news);
                //}
                //return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UpdateVersion AutoSaveNewsVersion=>" + ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:57
        /// </summary>
        /// <param name="news"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateFirstVersion(NewsEntity news)
        {
            try
            {
                news.Id = 0;
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.AutoSaveNewsVersion(news, -1, news.ListZoneId, news.Tag, "", news.NewsRelation, news.CreatedBy);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                //return NewsVersionDal.UpdateVersion(news, news.ListZoneId, news.CreatedBy)
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 14:57
        /// </summary>
        /// <param name="news"></param>
        /// <param name="zoneId"></param>
        /// <param name="zoneIdList"></param>
        /// <param name="tagIdList"></param>
        /// <param name="tagIdListForPrimary"></param>
        /// <param name="newsRelationIdList"></param>
        /// <param name="currentUsername"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes AutoSaveNewsVersion(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername)
        {
            //goi tu nodejs
            var response = NewsVersionNodeJsServices.AutoSaveNewsVersion(news, zoneId, zoneIdList, tagIdList, tagIdListForSubtitle, newsRelationIdList, currentUsername);
            return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

            //var existsNews = NewsDal.GetNewsById(news.Id);
            //if (null == existsNews)
            //{
            //    existsNews = new NewsEntity();
            //}
            //var newsRelationList = "";
            //try
            //{
            //    var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
            //    if (null != relatedNews && relatedNews.Count > 0)
            //    {
            //        newsRelationList = NewtonJson.Serialize(relatedNews);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
            //}
            //existsNews.NewsRelation = newsRelationList;

            //var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
            //var tagListForPrimary = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdListForPrimary);

            //var tagLinkList = "";
            //var tagPrimaryLinkList = "";

            //var tagPrimaryFormat = BoConstants.NewsUrlFormatForTag;
            //var tagFormat = BoConstants.NewsUrlFormatForTag;
            //var tagItems = "";
            //if (tagList != null && tagList.Count > 0)
            //{
            //    foreach (var tag in tagList)
            //    {
            //        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
            //        tagItems += ";" + tag.Name;

            //    }
            //    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
            //    if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
            //}
            //if (tagListForPrimary != null && tagListForPrimary.Count > 0)
            //{
            //    foreach (var tag in tagListForPrimary)
            //    {
            //        tagPrimaryLinkList += "," + string.Format(tagPrimaryFormat, tag.Url, tag.Id, tag.Name);

            //    }
            //    if (tagPrimaryLinkList != string.Empty) tagPrimaryLinkList = tagPrimaryLinkList.Remove(0, 1);
            //}
            //existsNews.TagPrimary = tagPrimaryLinkList;
            //existsNews.Tag = tagLinkList;

            //existsNews.Author = news.Author;
            //existsNews.Avatar = news.Avatar;
            //existsNews.AvatarDesc = news.AvatarDesc;
            //existsNews.Avatar2 = news.Avatar2;
            //existsNews.Avatar3 = news.Avatar3;
            //existsNews.Avatar4 = news.Avatar4;
            //existsNews.Avatar5 = news.Avatar5;
            //existsNews.DistributionDate = news.DistributionDate;
            //existsNews.Sapo = news.Sapo;
            //existsNews.Title = news.Title;
            //existsNews.SubTitle = news.SubTitle;
            //existsNews.Body = news.Body;
            //existsNews.LastModifiedBy = news.LastModifiedBy;
            //existsNews.LastModifiedDate = news.LastModifiedDate;
            //existsNews.WordCount = news.WordCount;
            //existsNews.Source = news.Source;
            //existsNews.IsFocus = news.IsFocus;

            //existsNews.Type = news.Type;
            //existsNews.IsOnHome = news.IsOnHome;
            //existsNews.NewsType = news.Type;

            //existsNews.AvatarCustom = news.AvatarCustom;
            //existsNews.DisplayInSlide = news.DisplayInSlide;
            //existsNews.DisplayPosition = news.DisplayPosition;
            //existsNews.DisplayStyle = news.DisplayStyle;
            //existsNews.Price = news.Price;
            //existsNews.NoteRoyalties = news.NoteRoyalties;
            //existsNews.TagItem = tagItems;
            //existsNews.CreatedBy = currentUsername;
            //existsNews.NewsCategory = news.NewsCategory;

            ///* Build link theo primary zone */
            //var primaryZone = ZoneDal.GetZoneById(zoneId);
            //if (null == primaryZone)
            //{
            //    //return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            //    existsNews.Url = string.Empty;
            //}
            //else
            //{
            //    if (news.Type != (int)NewsType.FakeNewsUrl)
            //        existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title, zoneId);
            //}
            //var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
            //updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;

            //return NewsVersionDal.UpdateVersion(existsNews, updateZoneIds, currentUsername)
            //           ? ErrorMapping.ErrorCodes.Success
            //           : ErrorMapping.ErrorCodes.BusinessError;
        }

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:00
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReleaseVersion(long newsId, string lastModifiedBy)
        {
            try
            {
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.ReleaseNewsVersion(newsId, lastModifiedBy);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

                //return NewsVersionDal.ReleaseVersion(newsId, lastModifiedBy)
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ReleaseVersion =>" + ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        //public static ErrorMapping.ErrorCodes CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId)
        //{
        //    try
        //    {
        //        return NewsVersionDal.CreateVersionForExistsNews(newsId, createdBy, ref newsVersionId)
        //                   ? ErrorMapping.ErrorCodes.Success
        //                   : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.UnknowError;
        //    }
        //}

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:03
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes ReleaseFirstVersion(long newsId, string lastModifiedBy)
        {
            try
            {
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.ReleaseFirstNewsVersion(newsId, lastModifiedBy);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

                //return NewsVersionDal.ReleaseFirstVersion(newsId, lastModifiedBy)
                //           ? ErrorMapping.ErrorCodes.Success
                //           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        //public static ErrorMapping.ErrorCodes RemoveEditingVersion(long newsId, string lastModifiedBy)
        //{
        //    try
        //    {
        //        return NewsVersionDal.RemoveEditingVersion(newsId, lastModifiedBy)
        //                   ? ErrorMapping.ErrorCodes.Success
        //                   : ErrorMapping.ErrorCodes.BusinessError;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return ErrorMapping.ErrorCodes.UnknowError;
        //    }
        //}

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:00
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> GetVersionByNewsId(long newsId)
        {
            try
            {
                //var versions = NewsVersionDal.GetByNewsId(newsId);

                //return versions.Select(version => new NewsVersionWithSimpleFieldsEntity()
                //{
                //    Id = version.Id,
                //    NewsId = version.NewsId,
                //    Version = version.Version,
                //    IsEditing = version.IsEditing,
                //    CreatedDateVersion = version.CreatedDateVersion
                //}).ToList();

                //goi tu nodejs
                return NewsVersionNodeJsServices.ListVersionByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
            }
        }

        //public static List<NewsVersionEntity> GetByNewsId(long newsId)
        //{
        //    try
        //    {
        //        return NewsVersionDal.GetByNewsId(newsId);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        return new List<NewsVersionEntity>();
        //    }
        //}

        /// <summary>
        /// Edited: NANIA
        /// LastModifiedDate: 2013-01-24 15:00
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="lastModifiedBy"></param>
        /// <returns></returns>
        public static List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> GetListVersionByNewsId(long newsId, string lastModifiedBy)
        {
            var list = new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
            try
            {
                //list = BoCached.Base.NewsVersion.NewsVersionDalFactory.GetNewsVersionByNewsId(newsId);
                //if (list == null)
                //{
                //    list= NewsVersionDal.GetListVersionByNewsId(newsId, lastModifiedBy);
                //    if (list != null && list.Count()>0)
                //    {
                //        BoCached.Base.NewsVersion.NewsVersionDalFactory.AddNewsVersionByNewsId(newsId, list);
                //    }
                //}
                //return list;

                //goi tu nodejs
                return NewsVersionNodeJsServices.GetNewsVersionByNewsIdForUser(newsId, lastModifiedBy);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return list;
            }
        }

        public static NewsVersionNodeJsEntity GetLastestVersionByNewsId(long newsId, string lastModifiedBy)
        {
            try
            {
                //return NewsVersionDal.GetLastestVersionByNewsId(newsId, lastModifiedBy);

                //goi tu nodejs
                return NewsVersionNodeJsServices.GetLastestVersionByNewsId(newsId, lastModifiedBy);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        public static NewsVersionNodeJsEntity GetById(string id)
        {
            try
            {
                //return NewsVersionDal.GetById(id);

                //goi tu nodejs

                return NewsVersionNodeJsServices.GetVersionById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsVersionNodeJsEntity();
            }
        }        

        public static ErrorMapping.ErrorCodes RemoveNewsVersionByNewsId(long newsId, string lastModifiedBy)
        {
            try
            {
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.RemoveNewsVersion(newsId, lastModifiedBy);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        public static ErrorMapping.ErrorCodes RemoveNewsVersionById(string versionId, string accountName)
        {
            try
            {
                //goi tu nodejs
                var response = NewsVersionNodeJsServices.RemoveNewsVersionById(versionId, accountName);
                return response.Success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.UnknowError;
            }
        }

        #endregion

        #region private members

        public static bool IsHasPermission(List<UserPermissionEntity> userPermissions,
                                            EnumPermission permissionToCheck, string listZoneId, UserEntity user)
        {
            //fix
            //userPermissions = NewtonJson.Deserialize<List<UserPermissionEntity>>("[{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":3},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":22},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":25},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":26},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":28},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":30},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":31},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":32},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":33},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":34},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":35},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":36},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":101},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":103},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":104},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":105},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":106},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":107},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":108},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":109},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":117},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":118},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":119},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":120},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":121},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":122},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":123},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":137},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":138},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":139},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":140},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":143},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":145},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":148},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":149},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":150},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":151},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":152},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":153},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":154},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":155},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":156},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":157},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":158},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":159},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":160},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":161},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":162},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":163},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":164},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":165},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":166},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":167},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":168},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":169},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":170},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":171},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":172},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":173},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":174},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":175},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":176},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":177},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":178},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":179},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":180},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":181},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":189},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":190},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":191},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":192},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":205},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":206},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":207},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":208},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":209},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":210},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":502},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":503},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":504},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":505},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":506},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":507},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":508},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":509},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":510},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":511},{\"UserId\":579,\"PermissionId\":1,\"ZoneId\":512},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":3},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":22},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":25},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":26},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":28},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":30},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":31},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":32},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":33},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":34},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":35},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":36},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":101},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":103},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":104},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":105},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":106},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":107},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":108},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":109},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":117},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":118},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":119},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":120},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":121},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":122},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":123},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":137},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":138},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":139},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":140},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":143},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":145},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":148},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":149},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":150},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":151},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":152},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":153},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":154},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":155},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":156},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":157},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":158},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":159},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":160},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":161},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":162},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":163},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":164},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":165},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":166},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":167},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":168},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":169},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":170},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":171},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":172},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":173},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":174},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":175},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":176},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":177},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":178},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":179},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":180},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":181},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":189},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":190},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":191},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":192},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":205},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":206},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":207},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":208},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":209},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":210},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":502},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":503},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":504},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":505},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":506},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":507},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":508},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":509},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":510},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":511},{\"UserId\":579,\"PermissionId\":2,\"ZoneId\":512},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":32},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":105},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":158},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":159},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":160},{\"UserId\":579,\"PermissionId\":3,\"ZoneId\":503}]");
            //permissionToCheck = EnumPermission.ArticleAdmin;
            //listZoneId = "3";
            //user = new UserEntity {
            //    IsFullPermission = false,
            //    IsFullZone = false            
            //};

            try
            {
                if (user.IsFullPermission && user.IsFullZone) return true;

                var step = listZoneId.IndexOf(",") >= 0 ? "," : (listZoneId.IndexOf(";") > 0 ? ";" : "|");
                if (user.IsFullPermission && userPermissions.Count != 0)
                {
                    listZoneId = step + listZoneId + step;
                    var count = userPermissions.Count;
                    for (var i = 0; i < count; i++)
                    {
                        if (listZoneId.IndexOf(step + userPermissions[i].ZoneId + step) >= 0)
                        {
                            return true;
                        }
                    }
                }

                if (user.IsFullZone && userPermissions.Count != 0)
                {
                    listZoneId = step + listZoneId + step;
                    var count = userPermissions.Count;
                    for (var i = 0; i < count; i++)
                    {
                        if (userPermissions[i].PermissionId == (int)permissionToCheck)
                        {
                            return true;
                        }
                    }
                }

                if (userPermissions.Count != 0)
                {
                    listZoneId = step + listZoneId + step;
                    var count = userPermissions.Count;
                    for (var i = 0; i < count; i++)
                    {
                        if (userPermissions[i].PermissionId == (int)permissionToCheck &&
                            listZoneId.IndexOf(step + userPermissions[i].ZoneId + step) >= 0)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsHasPermission(List<UserPermissionEntity> userPermissions, EnumPermission permissionToCheck, UserEntity user)
        {
            try
            {
                if (user.IsFullPermission) return true;
                
                if (userPermissions!=null && userPermissions.Count != 0)
                {                                        
                    foreach (var item in userPermissions)
                    {
                        if (item.PermissionId == (int)permissionToCheck)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        //public static string BuildLinkUrl(long newsId, string zoneUrl, string newsTitle)
        //{
        //    try
        //    {
        //        var formatUrl = AppSettings.GetString(BoConstants.NEWS_FORMAT_URL);
        //        var titleUnsignAndSlash = Utility.ConvertTextToLink(newsTitle, "-");
        //        return titleUnsignAndSlash != "" ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, newsId) : string.Empty;
        //    }
        //    catch
        //    {
        //        return string.Empty;
        //    }
        //}

        public static string BuildLinkUrl(long newsId, int newsType, string zoneUrl, string newsTitle, int zoneId)
        {
            try
            {
                string formatUrl;
                switch (newsType)
                {
                    case (int)NewsType.Video:
                        formatUrl = BoConstants.NewsUrlFormatForVideoNews;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL_VIDEO);
                        break;
                    case (int)NewsType.Image:
                        formatUrl = BoConstants.NewsUrlFormatForPhotoNews;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL_PHOTO);
                        break;
                    case (int)NewsType.SlidePhoto:
                        formatUrl = BoConstants.NewsUrlFormatForSlidePhotoNews;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL_SLIDE_PHOTO);
                        break;
                    case (int)NewsType.BigStory:
                        formatUrl = BoConstants.NewsUrlFormatForBigStoryNews;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL_SLIDE_PHOTO);
                        break;
                    default:
                        formatUrl = BoConstants.NewsUrlFormat;// AppSettings.GetString(BoConstants.NEWS_FORMAT_URL);
                        break;
                }
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                if (string.IsNullOrEmpty(titleUnsignAndSlash))
                {
                    titleUnsignAndSlash = "news";
                }
                if (formatUrl.IndexOf("ca{3}") > 0)
                {
                    return string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, newsId, zoneId);
                }
                else if (formatUrl.IndexOf("{2}") > 0)
                {
                    return string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, newsId);
                }
                else
                {
                    return string.Format(formatUrl, titleUnsignAndSlash, newsId);
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string BuildLinkUrlAdStore(long newsId, string zoneUrl, int zoneId, string newsTitle, string channelNamespace = "")
        {
            try
            {
                string formatUrl = string.IsNullOrEmpty(channelNamespace)
                                    ? BoConstants.NewsUrlFormatForAdStore :
                                      BoConstants.NewsUrlFormatForAdStoreByNamespace(channelNamespace);
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId, newsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string BuildLinkUrlAdStore(long newsId, string zoneUrl, int zoneId, string newsTitle)
        {
            try
            {
                string formatUrl = BoConstants.NewsUrlFormatForAdStore;
                var titleUnsignAndSlash = Utility.UnicodeToKoDauAndGach(newsTitle);
                return titleUnsignAndSlash != ""
                           ? string.Format(formatUrl, zoneUrl, titleUnsignAndSlash, zoneId, newsId)
                           : string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion

        #region UpdateTagAuto for News

        public static ErrorMapping.ErrorCodes UpdateTagAutoByNewsId(long newsId, string tagIdList)
        {
            try
            {

                var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
                var tagLinkList = "";
                var tagFormat = BoConstants.NewsUrlFormatForTag;
                var tagItems = "";
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagItems += ";" + tag.Name;

                    }
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                }

                return NewsDal.UpdateTagAutoByNewsId(newsId, tagLinkList, tagItems)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateTagAutoForNews(long newsId, string tagAutoIdList)
        {
            try
            {

                var tagIdListUpdated = "";

                var data = NewsDal.UpdateTagAutoForNews(newsId, tagAutoIdList, ref tagIdListUpdated)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;

                // Lưu giá trị link

                var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdListUpdated);
                var tagLinkList = "";
                var tagFormat = BoConstants.NewsUrlFormatForTag;
                var tagItems = "";
                if (tagList != null && tagList.Count > 0)
                {
                    foreach (var tag in tagList)
                    {
                        tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                        tagItems += ";" + tag.Name;

                    }
                    if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                    if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
                }

                var dataUpdateTagLinkTagItem = NewsDal.UpdateTagUrlForNews(newsId, tagLinkList, tagItems) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;

                return data;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region News Recommendation
        //public static List<NewsInListEntity> RecommendByActiveUsers(int zoneId, int totalRow)
        //{
        //    var newsList = new List<NewsInListEntity>();
        //    try
        //    {
        //        Dictionary<long, long> listActiveNews = RedisHelper.GetActiveNews(totalRow);
        //        string listNewsId = String.Join(",", listActiveNews.Select(item => item.Key).ToArray());
        //        List<NewsInListEntity> fullNewsList = NewsDal.GetNewsByIds(listNewsId, zoneId);

        //        newsList.AddRange(fullNewsList.Select(news => new NewsInListEntity
        //        {
        //            Id = news.Id,
        //            Title = news.Title,
        //            Sapo = news.Sapo,
        //            Avatar = news.Avatar,
        //            Avatar2 = news.Avatar2,
        //            Avatar3 = news.Avatar3,
        //            Avatar4 = news.Avatar4,
        //            Avatar5 = news.Avatar5,
        //            AvatarCustom = news.AvatarCustom,
        //            DistributionDate = news.DistributionDate,
        //            OriginalId = news.OriginalId,
        //            Source = news.Source,
        //            ActiveUsers = listActiveNews[news.Id],
        //            ViewCount = news.ViewCount
        //        }));
        //        newsList.Sort((news1, news2) => news2.ActiveUsers.CompareTo(news1.ActiveUsers));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //    return newsList;
        //}
        #endregion

        #region NewsUsePhoto
        /// <summary>
        /// tupa added 2013/04/15
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="photoPublishedIds"></param>
        /// <returns></returns>
        public virtual ErrorMapping.ErrorCodes UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds, string photoPublishedDescriptions)
        {
            try
            {
                for (int i = 0; i < photoPublishedIds.Split(';').Length; i++)
                {
                    if (photoPublishedIds.Split(';')[i] != "")
                    {
                        long id = Utility.ConvertToLong(photoPublishedIds.Split(';')[i]);
                        var photoPublished = PhotoBo.GetPhotoPublishedByPhotoPublishedId(id);

                        List<string> desList = new List<string>();
                        foreach (var s in photoPublishedDescriptions.Split(';'))
                        {
                            desList.Add(s);
                        }
                        for (int j = photoPublishedIds.Split(';').Length; j < photoPublishedIds.Split(';').Length; j++)
                        {
                            desList.Add("");
                        }
                        photoPublishedDescriptions = String.Join(";", desList);
                        photoPublished.Name = photoPublishedDescriptions.Split(';')[i];
                        PhotoBo.UpdatePhotoPublished(photoPublished);
                    }
                }
                if (NewsUsePhotoDal.UpdatePhotoPublishedInNews(newsId, photoPublishedIds))
                    return ErrorMapping.ErrorCodes.Success;
                else
                    return ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        /// <summary>
        ///  tupa added 2013/04/15
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual List<NewsUsePhotoEntity> GetAllNewsUsePhotoByNewsId(long newsId)
        {
            var listNewsusePhotos = new List<NewsUsePhotoEntity>();
            try
            {
                listNewsusePhotos = NewsUsePhotoDal.GetAllNewsUsePhotoByNewsId(newsId);
                foreach (var newsUsePhotoEntity in listNewsusePhotos)
                {
                    newsUsePhotoEntity.PhotoPublished =
                        PhotoBo.GetPhotoPublishedByPhotoPublishedId(newsUsePhotoEntity.PhotoPublishedId);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return listNewsusePhotos;
        }
        /// <summary>
        /// tupa added 2013/04/16
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual int CountPhotoPublishedByNewsId(long newsId)
        {
            int count = 0;
            try
            {
                count = NewsUsePhotoDal.CountPhotoPublishedByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return count;
        }
        #endregion

        #region NewsUseVideo
        public virtual ErrorMapping.ErrorCodes UpdateVideoInNews(long newsId, string videoIds, string videoDescriptions)
        {
            ErrorMapping.ErrorCodes code = ErrorMapping.ErrorCodes.Success;

            BoFactory.GetInstance<NewsBo>().DeleteVideoByNewsId(newsId);
            for (int i = 0; i < videoIds.Split(';').Length; i++)
            {
                var videoId = Utility.ConvertToLong(videoIds.Split(';')[i]);
                NewsUseVideoEntity videoEntity = new NewsUseVideoEntity();
                videoEntity.VideoId = videoId;
                videoEntity.NewsId = newsId;
                videoEntity.Description = videoDescriptions.Split(';')[i];
                string zoneName = "";
                var video = VideoBo.GetById(Utility.ConvertToInt(videoId), ref zoneName);
                videoEntity.AllowAd = video.AllowAd;
                videoEntity.Avatar = video.Avatar;
                videoEntity.Capacity = video.Capacity;
                videoEntity.DistributionDate = video.PublishDate;
                videoEntity.Duration = video.Duration;
                videoEntity.FileName = video.FileName;
                videoEntity.HtmlCode = video.HtmlCode;
                videoEntity.KeyVideo = video.KeyVideo;
                videoEntity.Mode = video.Mode;
                videoEntity.Name = video.Name;
                videoEntity.Pname = video.Pname;
                videoEntity.Size = video.Size;
                videoEntity.UnsignName = video.UnsignName;
                videoEntity.Url = video.Url;
                videoEntity.Views = video.Views;
                try
                {
                    if (NewsUseVideoDal.Update(videoEntity))
                        code = ErrorMapping.ErrorCodes.Success;
                    else
                        code = ErrorMapping.ErrorCodes.UnknowError;
                }
                catch (Exception ex)
                {
                    return ErrorMapping.ErrorCodes.BusinessError;
                }
            }
            return code;
        }
        /// <summary>
        /// tupa added 2013/04/16
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual List<NewsUseVideoEntity> GetAllNewsUseVideoByNewsId(long newsId)
        {
            var newsUseVideoEntities = new List<NewsUseVideoEntity>();
            try
            {
                newsUseVideoEntities = NewsUseVideoDal.GetAllNewsUseVideoByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsUseVideoEntities;
        }
        /// <summary>
        /// tupa added 2013/04/16
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual int CountVideoByNewsId(long newsId)
        {
            int count = 0;
            try
            {
                count = NewsUseVideoDal.CountVideoByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return count;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public virtual int DeleteVideoByNewsId(long newsId)
        {
            int count = 0;
            try
            {
                count = NewsUseVideoDal.DeleteVideoByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return count;
        }
        #endregion

        #region NewsWaitRepublish

        public static ErrorMapping.ErrorCodes InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
                                            string usernameForUpdateAction, ref long newNewsId,
                                            ref string newEncryptNewsId, List<string> authorList, int newsChildOrder, int sourceId, List<NewsExtensionEntity> newsExtensions)
        {
            if (null == authorList || authorList.Count != 3)
            {
                authorList = new List<string> { "", "", "" };
            }
            if (null == news)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidTitle;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            newNewsId = news.Id;

            //var existsNews = NewsDal.GetNewsForValidateById(news.Id);
            //if (null != existsNews)
            //{
            //    return ErrorMapping.ErrorCodes.UpdateNewsDuplicateId;
            //}
            var existsZone = ZoneDal.GetZoneById(zoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            var newsRelationList = "";
            if (null != newsRelationIdList && newsRelationIdList.Any())
            {
                try
                {
                    var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                    if (null != relatedNews && relatedNews.Count > 0)
                    {
                        newsRelationList = NewtonJson.Serialize(relatedNews);
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }
            news.NewsRelation = newsRelationList;

            var tagLinkList = "";
            var tagPrimaryLinkList = "";

            var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
            var tagListForPrimary = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdListForPrimary);

            var tagPrimaryFormat = BoConstants.NewsUrlFormatForTag;
            var tagFormat = BoConstants.NewsUrlFormatForTag;
            var tagItems = "";
            if (tagList != null && tagList.Count > 0)
            {
                foreach (var tag in tagList)
                {
                    tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                    tagItems += ";" + tag.Name;
                }
                //if (tagIdList != string.Empty) tagIdList = tagIdList.Remove(0, 1);
                if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
            }
            if (tagListForPrimary != null && tagListForPrimary.Count > 0)
            {
                tagPrimaryLinkList = tagListForPrimary.Aggregate(tagPrimaryLinkList,
                                                                 (current, tag) => current + (";" + tag.Name));
                if (tagPrimaryLinkList != string.Empty) tagPrimaryLinkList = tagPrimaryLinkList.Remove(0, 1);
            }

            news.TagPrimary = tagPrimaryLinkList;
            news.Tag = tagLinkList;
            news.TagItem = tagItems;

            /* Build link theo primary zone */
            var primaryZone = ZoneDal.GetZoneById(zoneId);
            if (null == primaryZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }
            news.Url = BuildLinkUrl(newNewsId, news.Type, primaryZone.ShortUrl, news.Title, zoneId);

            var exitsNews = NewsDal.GetNewsForValidateById(news.Id);
            news.Status = (int)NewsWaitRepublishStatus.Wating;
            news.EditedBy = usernameForUpdateAction;
            news.CreatedBy = exitsNews.CreatedBy;
            news.PublishedBy = exitsNews.PublishedBy;
            news.LastModifiedBy = usernameForUpdateAction;

            var inserted = NewsWaitRepublishDal.InsertNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary,
                                              newsRelationIdList, authorList[0], authorList[1], authorList[2], sourceId);
            if (!inserted)
            {
                newNewsId = 0;
            }
            else
            {
                foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                {
                    //NewsExtensionDal.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                    NewsExtensionBo.SetValue(newNewsId, newsExtension.Type, newsExtension.Value);
                }

                NewsHistoryBo.InsertNews(news.Id, news.Status, usernameForUpdateAction);

                var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;

                news.ListZoneId = updateZoneIds;
                UpdateFirstVersion(news);
                ReleaseFirstVersion(newNewsId, usernameForUpdateAction);
            }
            if (inserted)
            {
                newEncryptNewsId = CryptonForId.EncryptId(newNewsId);
                return ErrorMapping.ErrorCodes.Success;
            }

            return ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                            string tagIdListForPrimary, string newsRelationIdList,
                                            string usernameForUpdateAction, bool isRebuildLink, ref int newsStatus, List<string> authorList, int newsChildOrder, int sourceId, string publishedContent, List<NewsExtensionEntity> newsExtensions)
        {
            if (null == authorList || authorList.Count != 3)
            {
                authorList = new List<string> { "", "", "" };
            }
            if (string.IsNullOrEmpty(usernameForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNotAllowEdit;
            }
            if (null == news || news.Id <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            if (zoneId <= 0)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            var existsNews = NewsDal.GetNewsById(news.Id);
            if (null == existsNews)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
            }

            var user = UserBo.GetUserByUsername(usernameForUpdateAction);
            if (null == user) return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;


            var existsZone = ZoneDal.GetZoneById(zoneId);
            if (null == existsZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }

            var newsRelationList = "";
            try
            {
                var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(newsRelationIdList);
                if (null != relatedNews && relatedNews.Count > 0)
                {
                    newsRelationList = NewtonJson.Serialize(relatedNews);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            news.NewsRelation = newsRelationList;

            var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdList);
            var tagListForPrimary = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(tagIdListForPrimary);

            var tagLinkList = "";
            var tagPrimaryLinkList = "";

            var tagPrimaryFormat = BoConstants.NewsUrlFormatForTag;
            var tagFormat = BoConstants.NewsUrlFormatForTag;
            var tagItems = "";
            if (tagList != null && tagList.Count > 0)
            {
                foreach (var tag in tagList)
                {
                    tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                    tagItems += ";" + tag.Name;

                }
                if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);
            }
            if (tagListForPrimary != null && tagListForPrimary.Count > 0)
            {
                tagPrimaryLinkList = tagListForPrimary.Aggregate(tagPrimaryLinkList,
                                                                 (current, tag) =>
                                                                 current +
                                                                 ("," +
                                                                  string.Format(tagPrimaryFormat, tag.Url, tag.Id,
                                                                                tag.Name)));
                if (tagPrimaryLinkList != string.Empty) tagPrimaryLinkList = tagPrimaryLinkList.Remove(0, 1);
            }
            news.TagPrimary = tagPrimaryLinkList;
            news.Tag = tagLinkList;

            existsNews.NewsRelation = news.NewsRelation;

            existsNews.Author = news.Author;

            existsNews.Avatar = news.Avatar;
            existsNews.AvatarDesc = news.AvatarDesc;
            existsNews.Avatar2 = news.Avatar2;
            existsNews.Avatar3 = news.Avatar3;
            existsNews.Avatar4 = news.Avatar4;
            existsNews.Avatar5 = news.Avatar5;
            existsNews.DistributionDate = news.DistributionDate;
            existsNews.Sapo = news.Sapo;
            existsNews.Title = news.Title;
            existsNews.Body = news.Body;
            existsNews.NewsRelation = news.NewsRelation;
            existsNews.SubTitle = news.SubTitle;
            existsNews.LastModifiedBy = news.LastModifiedBy;
            existsNews.LastModifiedDate = news.LastModifiedDate;
            existsNews.WordCount = news.WordCount;
            existsNews.Source = news.Source;
            existsNews.Tag = news.Tag;
            existsNews.Note = news.Note;
            existsNews.Type = news.Type;
            existsNews.NewsType = news.NewsType;
            existsNews.OriginalId = news.OriginalId;
            existsNews.TagItem = tagItems;
            existsNews.InitSapo = news.InitSapo;

            existsNews.ThreadId = news.ThreadId;

            // Neu la thu ky thi moi cho sua cac thong tin nay
            if (
                PermissionBo.CheckUserPermission(WcfExtensions.WcfMessageHeader.Current.ClientUsername,
                                                 (int)EnumPermission.ArticleAdmin, zoneId) ==
                ErrorMapping.ErrorCodes.Success)
            {
                existsNews.IsFocus = news.IsFocus;
                existsNews.IsOnHome = news.IsOnHome;
                existsNews.AvatarCustom = news.AvatarCustom;
                existsNews.DisplayInSlide = news.DisplayInSlide;
                existsNews.DisplayPosition = news.DisplayPosition;
                existsNews.DisplayStyle = news.DisplayStyle;
                existsNews.Price = news.Price;
                existsNews.NewsCategory = news.NewsCategory;
                existsNews.NoteRoyalties = news.NoteRoyalties;

                existsNews.TemplateName = news.TemplateName;
                existsNews.TemplateConfig = news.TemplateConfig;

                existsNews.IsBreakingNews = news.IsBreakingNews;
                existsNews.PegaBreakingNews = news.PegaBreakingNews;
            }

            /* Build link theo primary zone */
            var primaryZone = ZoneDal.GetZoneById(zoneId);
            if (null == primaryZone)
            {
                return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
            }
            if (existsNews.Status == (int)NewsStatus.Published)
            {
                if (isRebuildLink)
                    if (news.Type != (int)NewsType.FakeNewsUrl)
                        existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title, zoneId);
            }
            else
            {
                if (news.Type != (int)NewsType.FakeNewsUrl)
                    existsNews.Url = BuildLinkUrl(existsNews.Id, existsNews.Type, primaryZone.ShortUrl, existsNews.Title, zoneId);
            }

            existsNews.Status = (int)NewsWaitRepublishStatus.Wating;
            existsNews.CreatedBy = usernameForUpdateAction;

            if (NewsWaitRepublishDal.UpdateNewsWaitRepublish(existsNews, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, authorList[0], authorList[1], authorList[2], sourceId))
            {
                //NewsExtensionDal.DeleteByNewsId(existsNews.Id);
                NewsExtensionBo.DeleteByNewsId(existsNews.Id);
                foreach (var newsExtension in newsExtensions.Where(newsExtension => !string.IsNullOrEmpty(newsExtension.Value)))
                {
                    //NewsExtensionDal.SetValue(existsNews.Id, newsExtension.Type, newsExtension.Value);
                    NewsExtensionBo.SetValue(existsNews.Id, newsExtension.Type, newsExtension.Value);
                }

                //NewsHistoryBo.UpdateNews(news.Id, news.Status, usernameForUpdateAction);

                var updateZoneIds = (zoneId > 0 ? zoneId.ToString() : "");
                updateZoneIds += (!string.IsNullOrEmpty(updateZoneIds) ? ";" : "") + zoneIdList;

                news.ListZoneId = updateZoneIds;
                UpdateVersion(existsNews, usernameForUpdateAction);
                ReleaseVersion(news.Id, usernameForUpdateAction);

                newsStatus = existsNews.Status;

                // Log hành động sửa bài viết
                ActivityBo.LogUpdateNews(existsNews.Id, usernameForUpdateAction, existsNews.Title, existsNews.Status);
                // Kết thúc log
                return ErrorMapping.ErrorCodes.Success;
            }
            return ErrorMapping.ErrorCodes.UnknowError;
        }


        public static ErrorMapping.ErrorCodes ChangeNewsWaitRepublishStatusToProcessed(long newsId, string userForUpdateAction)
        {
            if (newsId <= 0)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNewsNotFound;
            }
            if (string.IsNullOrEmpty(userForUpdateAction))
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
            }

            var user = UserDal.GetUserByUsername(userForUpdateAction);
            // Không tìm thấy người xử lý
            if (null == user)
            {
                return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceiveFromReturnStore;
            }


            if (NewsWaitRepublishDal.ChangeStatusToProcessed(newsId, userForUpdateAction))
            {
                NewsNotificationDal.UpdateNotification(newsId, userForUpdateAction,
                                                       (int)NewsStatus.ReceivedForEdit, false,
                                                       "[" + userForUpdateAction + "] đã duyệt bài");
                return ErrorMapping.ErrorCodes.Success;
            }
            else
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
            return ErrorMapping.ErrorCodes.ChangeNewsStatusNotAllowToReceive;
        }

        public static List<NewsInListEntity> SearchNewsWaitRepublish(string keyword, string username, string zoneIds,
                                                       DateTime fromDate,
                                                       DateTime toDate,
                                                       NewsFilterFieldForUsername filterFieldForUsername,
                                                       NewsSortExpression sortOrder,
                                                       NewsWaitRepublishStatus status,
                                                       NewsType newsType, int pageIndex, int pageSize,
                                                       ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                var fullNewsList = NewsWaitRepublishDal.SearchNewsWaitRepublish(keyword, username, zoneIds, fromDate, toDate,
                                                      (int)filterFieldForUsername,
                                                      (int)sortOrder, (int)status, (int)newsType, pageIndex, pageSize,
                                                      ref totalRow);

                newsList.AddRange(fullNewsList.Select(news => new NewsInListEntity
                {
                    Id = news.Id,
                    Title = news.Title,
                    Sapo = news.Sapo,
                    Avatar = news.Avatar,
                    Avatar2 = news.Avatar2,
                    Avatar3 = news.Avatar3,
                    Avatar4 = news.Avatar4,
                    Avatar5 = news.Avatar5,
                    AvatarCustom = news.AvatarCustom,
                    CreatedDate = news.CreatedDate,
                    EditedDate = news.LastModifiedDate,
                    PublishedDate = news.LastModifiedDate,
                    DistributionDate = news.DistributionDate,
                    LastModifiedDate = news.LastModifiedDate,
                    CreatedBy = news.CreatedBy,
                    EditedBy = news.EditedBy,
                    PublishedBy = news.PublishedBy,
                    Status = news.Status,
                    Type = news.Type,
                    Note = news.Note,
                    ViewCount = news.ViewCount,
                    DisplayPosition = news.DisplayPosition,
                    Price = news.Price.HasValue?news.Price.Value:0,
                    OriginalId = news.OriginalId,
                    Source = news.Source,
                    Author = news.Author,
                    IsOnHome = news.IsOnHome,
                    WordCount = news.WordCount
                }));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }
        public virtual NewsDetailForEditEntity GetNewsByNewsWaitRepublishId(long newsId, string currentUsername)
        {
            string newsVersionId = "";
            var newsDetail = new NewsDetailForEditEntity();
            newsDetail.AllZone = ZoneBo.BindAllOfZoneToTreeviewFullDepth(ZoneBo.GetListZoneByUsername(currentUsername), "--");
            newsDetail.NewsInfo = (newsId < 0 ? null : GetNewsByNewsWaitRepublishId(newsId, currentUsername, ref newsVersionId));
            if (null != newsDetail.NewsInfo)
            {
                // Zone
                if (newsId > 0)
                {
                    newsDetail.NewsInZone = GetNewsInZoneByNewsId(newsId);
                    newsDetail.NewsRelation = GetRelatedNewsByNewsId(newsId);
                    newsDetail.TagInNews = GetTagNewsWithTagInfoByNewsId(newsId);
                }
                else
                {
                    newsDetail.NewsInZone = ChannelVN.CMS.BO.Base.News.NewsBo.GetNewsInZoneByZoneIdList(newsId, newsDetail.NewsInfo.ListZoneId);
                    newsDetail.NewsRelation = GetRelatedNewsByNewsIds(newsDetail.NewsInfo.NewsRelation);
                    newsDetail.TagInNews = GetTagNewsWithTagInfo(newsId, newsDetail.NewsInfo.TagPrimary,
                                                                 newsDetail.NewsInfo.Tag);
                }

                newsDetail.NewsBySource = BoFactory.GetInstance<NewsSourceBo>().GetListInNews(newsId);
                newsDetail.NewsByAuthor = NewsAuthorBo.GetListInNews(newsId);
                //newsDetail.NewsByAuthor = new List<NewsByAuthorEntity>();
            }
            newsDetail.ListVersion = GetListVersionByNewsId((newsId < 0 ? 0 : newsId), currentUsername);
            //if (newsVersionId > 0)
            //{
            //    var newsChildVersions = NewsChildVersionDal.GetByNewsId(newsVersionId, false);
            //    newsDetail.ListNewsChild = newsChildVersions.Select(newsChildVersion => new NewsChildEntity
            //    {
            //        NewsParentId =
            //            newsChildVersion.
            //            NewsParentId,
            //        Order =
            //            newsChildVersion.
            //            Order,
            //        Body =
            //            newsChildVersion.
            //            Body
            //    }).ToList();
            //}
            //else
            //{
            //    newsDetail.ListNewsChild = NewsChildDal.GetByNewsId(newsId, false);
            //}
            var newsContent = NewsContentDal.GetByNewsId(newsId);
            if (newsContent != null)
            {
                newsDetail.NewsContentWithTemplate = newsContent.Body;
            }
            else
            {
                newsDetail.NewsContentWithTemplate = newsDetail.NewsInfo != null ? newsDetail.NewsInfo.Body : "";
            }
            newsDetail.NewsExtensions = NewsExtensionDal.GetByNewsId(newsId);
            return newsDetail;
        }

        public static NewsEntity GetNewsByNewsWaitRepublishId(long id)
        {
            return NewsWaitRepublishDal.GetNewsWaitRepublishById(id);
        }

        public static NewsEntity GetNewsByNewsWaitRepublishId(long id, string lastModifiedBy, ref string newsVesionId)
        {
            newsVesionId = "";
            if (id > 0)
            {
                return NewsWaitRepublishDal.GetNewsWaitRepublishById(id);
            }
            else
            {
                //var lastestVersion = NewsVersionDal.GetLastestVersionByNewsId(id, lastModifiedBy);
                //get tu nodejs
                var lastestVersion = GetLastestVersionByNewsId(id, lastModifiedBy);
                if (null != lastestVersion)
                {
                    newsVesionId = lastestVersion.Id;

                    var news = new NewsEntity();
                    news.Title = lastestVersion.Title;
                    news.SubTitle = lastestVersion.SubTitle;
                    news.Sapo = lastestVersion.Sapo;
                    news.Body = lastestVersion.Body;
                    news.Avatar = lastestVersion.Avatar;
                    news.AvatarDesc = lastestVersion.AvatarDesc;
                    news.Avatar2 = lastestVersion.Avatar2;
                    news.Avatar3 = lastestVersion.Avatar3;
                    news.Avatar4 = lastestVersion.Avatar4;
                    news.Avatar5 = lastestVersion.Avatar5;
                    news.Author = lastestVersion.Author;
                    news.NewsRelation = lastestVersion.NewsRelation;
                    news.Source = lastestVersion.Source;
                    news.IsFocus = lastestVersion.IsFocus;
                    news.Type = lastestVersion.Type;
                    news.ThreadId = lastestVersion.ThreadId;
                    news.CreatedDate = lastestVersion.CreatedDate;
                    news.LastModifiedDate = lastestVersion.LastModifiedDate;
                    news.DistributionDate = lastestVersion.DistributionDate;
                    news.CreatedBy = lastestVersion.CreatedBy;
                    news.LastModifiedBy = lastestVersion.LastModifiedBy;
                    news.PublishedBy = lastestVersion.PublishedBy;
                    news.EditedBy = lastestVersion.EditedBy;
                    news.LastReceiver = lastestVersion.LastReceiver;
                    news.WordCount = lastestVersion.WordCount;
                    news.ViewCount = lastestVersion.ViewCount;
                    news.Priority = lastestVersion.Priority;
                    news.ListZoneId = lastestVersion.ListZoneId;
                    news.Tag = lastestVersion.Tag;
                    news.Note = lastestVersion.Note;
                    news.DisplayInSlide = lastestVersion.DisplayInSlide;
                    news.DisplayPosition = lastestVersion.DisplayPosition;
                    news.DisplayStyle = lastestVersion.DisplayStyle;
                    news.AvatarCustom = lastestVersion.AvatarCustom;
                    news.IsOnHome = lastestVersion.IsOnHome;
                    news.OriginalId = lastestVersion.OriginalId;
                    // Extension fields
                    news.Price = lastestVersion.Price;
                    news.TagItem = lastestVersion.TagItem;
                    news.Url = lastestVersion.Url;
                    news.NewsCategory = lastestVersion.NewsCategory;
                    news.NoteRoyalties = lastestVersion.NoteRoyalties;
                    news.InitSapo = lastestVersion.InitSapo;
                    return news;
                }
            }
            return null;
        }

        public static int NewsWaitRepublishCountDown(string username, string zoneIds,
                                                       NewsFilterFieldForUsername filterFieldForUsername,
                                                       NewsType newsType)
        {
            int totalRow = 0;
            try
            {
                totalRow = NewsWaitRepublishDal.NewsWaitRepublishCountDown(username, zoneIds, (int)filterFieldForUsername, (int)newsType);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return totalRow;
        }
        #endregion

        public static List<NewsForExportEntity> StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsForExportEntity>();
            try
            {
                newsList = NewsDal.StatisticsExportNewsData(zoneIds, sourceIds, orderBy, fromDate, toDate);

                //newsList.AddRange(fullNewsList.Select(news => new NewsInListEntity
                //{
                //    Id = news.Id,
                //    Title = news.Title,
                //    CreatedDate = news.CreatedDate,
                //    PublishedDate = news.LastModifiedDate,
                //    DistributionDate = news.DistributionDate,
                //    CreatedBy = news.CreatedBy,
                //    PublishedBy = news.PublishedBy,
                //    ViewCount = news.ViewCount,
                //    Source = news.Source,
                //    Url = news.Url,
                //    ZoneName = news.ZoneName
                //}));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        public static List<NewsForExportEntity> StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsForExportEntity>();
            try
            {
                newsList = NewsDal.StatisticsExportHotNewsData(zoneIds, top, fromDate, toDate);

                //newsList.AddRange(fullNewsList.Select(news => new NewsInListEntity
                //{
                //    Id = news.Id,
                //    Title = news.Title,
                //    CreatedDate = news.CreatedDate,
                //    PublishedDate = news.LastModifiedDate,
                //    DistributionDate = news.DistributionDate,
                //    CreatedBy = news.CreatedBy,
                //    PublishedBy = news.PublishedBy,
                //    ViewCount = news.ViewCount,
                //    Source = news.Source,
                //    Url = news.Url,
                //    ZoneName = news.ZoneName
                //}));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        public static List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsForExportEntity>();
            try
            {
                newsList = NewsDal.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate);

                //newsList.AddRange(fullNewsList.Select(news => new NewsInListEntity
                //{
                //    Id = news.Id,
                //    Title = news.Title,
                //    CreatedDate = news.CreatedDate,
                //    PublishedDate = news.LastModifiedDate,
                //    DistributionDate = news.DistributionDate,
                //    CreatedBy = news.CreatedBy,
                //    PublishedBy = news.PublishedBy,
                //    ViewCount = news.ViewCount,
                //    Source = news.Source,
                //    Url = news.Url,
                //    ZoneName = news.ZoneName
                //}));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        public static List<NewsForExportEntity> GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            var newsList = new List<NewsForExportEntity>();
            try
            {
                newsList = NewsDal.GetListNewsByDistributionDate(zoneIds, fromDate, toDate);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        #region NewsStats By OriginalId

        public static int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            return NewsDal.GetViewCountByOriginalId(zoneIds, fromDate, toDate, originalId);
        }
        public static List<NewsInListEntity> SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                newsList = NewsDal.SearchNewsByOriginalId(zoneIds, fromDate, toDate, originalId, pageIndex, pageSize,
                                                          ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsList;
        }

        #endregion

        #region Update like count
        public static ErrorMapping.ErrorCodes UpdateLikeCount(long newsId, int likeCount)
        {
            var data = NewsDal.UpdateLikeCount(newsId, likeCount)
                         ? ErrorMapping.ErrorCodes.Success
                         : ErrorMapping.ErrorCodes.BusinessError;
            return data;
        }
        public static List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            return NewsDal.SearchNewsByDateRange(startDate, endDate, status);
        }
        #endregion

        public static int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return NewsDal.CountNewsBySource(sourceId, fromDate, toDate);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.CountNewsBySource:{0}", ex.Message));
            }
        }


        public virtual NewsEntity GetNewsByTitle(string title)
        {
            return NewsDal.GetNewsByTitle(title);
        }

        public static ErrorMapping.ErrorCodes UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount)
        {
            var isSuccess = NewsDal.UpdatePhotoVideoCount(newsId, photoCount, videoCount);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static ErrorMapping.ErrorCodes UpdateNewsRelationJson(long id, List<NewsPublishForNewsRelationEntity> newsRelation)
        {
            var newsRelationList = "";
            if (null != newsRelation && newsRelation.Any())
            {
                try
                {
                    var relatedNews = NewsPublishDal.GetRelatedNewsByNewsIds(string.Join(";", newsRelation.Where(x => x.NewsRelationType == 1).Select(x => x.NewsId).ToArray()));
                    if (null != relatedNews && relatedNews.Count > 0)
                    {
                        foreach (var newsPublishForNewsRelationEntity in relatedNews)
                        {
                            newsPublishForNewsRelationEntity.NewsRelationType = 1;
                        }
                    }
                    var relatedNewsSpecial = NewsPublishDal.GetRelatedNewsByNewsIds(string.Join(";", newsRelation.Where(x => x.NewsRelationType == 2).Select(x => x.NewsId).ToArray()));
                    if (null != relatedNewsSpecial && relatedNewsSpecial.Count > 0)
                    {
                        foreach (var newsPublishForNewsRelationEntity in relatedNewsSpecial)
                        {
                            newsPublishForNewsRelationEntity.NewsRelationType = 2;
                        }
                    }
                    if (null == relatedNews)
                    {
                        relatedNews = new List<NewsPublishForNewsRelationEntity>();
                    }
                    if (null != relatedNewsSpecial && relatedNewsSpecial.Count > 0)
                    {
                        relatedNews.AddRange(relatedNewsSpecial);
                    }
                    newsRelationList = NewtonJson.Serialize(relatedNews);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                }
            }

            var isSuccess = NewsDal.UpdateNewsRelationJson(id, newsRelationList);
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }

        public static List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword)
        {
            var newsList = new List<NewsInListEntity>();
            try
            {
                newsList = NewsDal.SearchNewsPublishedByKeyword(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        public static List<NewsAppBotEntity> GetListNewsNotIbtempByZoneId(int zoneId, int page, int num, ref int total)
        {
            try
            {
                return NewsDal.GetListNewsNotIbtempByZoneId(zoneId, page, num, ref total);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.GetListNewsNotIbtempByZoneId:{0}", ex.Message));
            }
        }

        public static List<NewsAppBotEntity> GetListNewsNotIbBoundByZoneId(int zoneId, int page, int num, ref int total)
        {
            try
            {
                return NewsDal.GetListNewsNotIbBoundByZoneId(zoneId, page, num, ref total);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("NewsDal.GetListNewsNotIbBoundByZoneId:{0}", ex.Message));
            }
        }

        public static List<NewsFullLogEntity> GetNewsFullLog(string keyword)
        {
            var newsList = new List<NewsFullLogEntity>();
            try
            {
                newsList = NewsDal.GetNewsFullLog(keyword);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }

            return newsList;
        }

        #region News Top Hot
        public static ErrorMapping.ErrorCodes NewsHot_Delete(string NewsIds, int type)
        {
            try
            {
                return NewsDal.NewsHot_Delete(NewsIds, type) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_UpdateType()
        {
            try
            {
                return NewsDal.NewsHot_UpdateType() ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_CheckExists()
        {
            try
            {
                return NewsDal.NewsHot_CheckExists() ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_Insert(NewsHotEntity entity)
        {
            try
            {
                return NewsDal.NewsHot_Insert(entity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_Insert2(NewsHotEntity entity)
        {
            try
            {
                return NewsDal.NewsHot_Insert2(entity) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_Lock(int newsHotId)
        {
            try
            {
                return NewsDal.NewsHot_Lock(newsHotId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static ErrorMapping.ErrorCodes NewsHot_Unlock(int newsHotId)
        {
            try
            {
                return NewsDal.NewsHot_Unlock(newsHotId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }
        public static bool NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId, NewsPositionEntity newsPositionEntity)
        {
            try
            {
                var returnData = NewsDal.NewsHot_ChangeNewsInLockPosition(newsHotId, newsId);
                if (returnData)
                {
                    ActivityBo.LogSetNewsPositionOnHomeStream(newsPositionEntity.NewsId, WcfMessageHeader.Current.ClientUsername + ": ChangeNewsInLockPosition", newsPositionEntity.Title, newsPositionEntity.Type, newsPositionEntity.Order, newsPositionEntity.Locked);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool NewsHot_AddNewsInLockPosition(NewsPositionEntity addItem)
        {
            try
            {
                var returnData = NewsDal.NewsHot_AddNewsInLockPosition(addItem);
                if (returnData)
                {
                    ActivityBo.LogSetNewsPositionOnHomeStream(addItem.NewsId, WcfMessageHeader.Current.ClientUsername + ": AddNewsInLockPosition", addItem.Title, addItem.Type, addItem.Order, addItem.Locked);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static bool NewsHot_DeleteNewsInLockPosition(NewsPositionEntity addItem)
        {
            try
            {
                var returnData = NewsDal.NewsHot_DeleteNewsInLockPosition(addItem);
                if (returnData)
                {
                    ActivityBo.LogSetNewsPositionOnHomeStream(addItem.NewsId, WcfMessageHeader.Current.ClientUsername + ": DeleteNewsInLockPosition", addItem.Title, addItem.Type, addItem.Order, addItem.Locked);
                }
                return returnData;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public static long NewsHot_UnpublishOrBomdNews(long newsId)
        {
            try
            {
                return NewsDal.NewsHot_UnpublishOrBomdNews(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return 0;
            }
        }
        public static List<NewsHotEntity> NewsHot_GetByType(int type)
        {
            try
            {
                return NewsDal.NewsHot_GetByType(type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsHotEntity>();
            }
        }
        public static List<NewsHotEntity> NewsHot_GetAllForRedis()
        {
            try
            {
                return NewsDal.NewsHot_GetAllForRedis();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsHotEntity>();
            }
        }
        public static List<NewsHotCafeFEntity> NewsHot_GetAllForRedisByCafeF()
        {
            try
            {
                return NewsDal.NewsHot_GetAllForRedisByCafeF();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsHotCafeFEntity>();
            }
        }
        #endregion


        #region Seoer Update news
        public static ErrorMapping.ErrorCodes Seoer_Update(SeoerNewsEntity news)
        {
            try
            {
                var existsNews = NewsDal.GetNewsById(news.NewsId);
                if (existsNews == null)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsNewsNotFound;
                }
                news.ReviewerName = "Seo_" + news.ReviewerName;

                string TagItem = "";
                if (!string.IsNullOrEmpty(news.Tag))
                {
                    var arrTag = news.Tag.Split(';').ToList().Distinct();
                    foreach (var item in arrTag)
                    {
                        if (!string.IsNullOrEmpty(item))
                            TagItem += ";" + item;
                    }
                    if (TagItem != "")
                        TagItem = TagItem.Substring(1);
                }
                string listTagId = "";
                string TagJSON = "";
                if (TagItem != "")
                {
                    int PrimaryZoneId = 0;
                    string OrtherZoneId = "";

                    var zone = GetNewsInZoneByNewsId(news.NewsId);
                    var ZoneP = zone.FirstOrDefault(k => k.IsPrimary == true);
                    if (ZoneP != null)
                        PrimaryZoneId = ZoneP.ZoneId;

                    var ortherZone = zone.Where(k => k.IsPrimary == false);
                    foreach (var item in ortherZone)
                    {
                        OrtherZoneId += ";" + item.ZoneId;
                    }
                    if (OrtherZoneId != "")
                        OrtherZoneId = OrtherZoneId.Substring(1);
                    TagBo.Seoer_Insert_Tag(TagItem, news.ReviewerName, PrimaryZoneId, OrtherZoneId, ref listTagId);

                    var tagList = BoFactory.GetInstance<TagBo>().GetTagByListOfTagId(listTagId);
                    var TagFormatJson = bool.Parse(BoConstants.TagFormatJson);
                    //Convert tag về chuỗi json
                    var tagLinkList = "";

                    var tagFormat = BoConstants.NewsUrlFormatForTag;
                    var tagItems = "";

                    if (tagList != null && tagList.Count > 0)
                    {
                        var lstTag = new List<TagJson>();
                        tagList = tagList.Distinct().ToList();
                        foreach (var tag in tagList)
                        {
                            //add tag es redis                                               
                            BoSearch.Base.Tag.TagDalFactory.AddTag(tag);
                            BoCached.Base.Tag.TagDalFactory.AddTag(tag);

                            if (TagFormatJson)
                            {
                                var tagTmp = new TagJson
                                {
                                    Id = tag.Id,
                                    Name = tag.Name,
                                    Url = tag.Url
                                };
                                if (lstTag.FirstOrDefault(x => x.Id == tagTmp.Id) != null)
                                {
                                    continue;
                                }
                                lstTag.Add(tagTmp);
                            }
                            else
                            {
                                tagLinkList += "," + string.Format(tagFormat, tag.Url, tag.Id, tag.Name);
                            }
                            tagItems += ";" + tag.Name;
                        }

                        if (TagFormatJson)
                        {
                            TagJSON = NewtonJson.Serialize(lstTag);
                        }
                        else
                        {
                            if (tagLinkList != string.Empty) tagLinkList = tagLinkList.Remove(0, 1);
                        }
                        if (tagItems != string.Empty) tagItems = tagItems.Remove(0, 1);

                    }
                    news.Tag = tagItems;
                    existsNews.TagItem = tagItems;
                    existsNews.Tag = TagJSON;
                }
                else
                {
                    news.Tag = existsNews.TagItem;
                    TagJSON = existsNews.Tag;
                }
                existsNews.Body = news.Body;

                var res = NewsDal.Seoer_UpdateNews(news, listTagId, TagJSON);
                if (res)
                {
                    //update body redis
                    BoCached.Base.News.NewsDalFactory.UpdateOnlyNewsBody(news.NewsId,news.Body,news.ReviewerName,news.ReviewStatus);

                    //update ReviewStatus ES
                    BoSearch.Base.News.NewsDalFactory.UpdateNewsSeoReviewStatus(news.NewsId, news.ReviewStatus, news.ReviewerName);

                    // Log hành động sửa bài viết
                    UpdateVersion(existsNews, news.ReviewerName);
                    ReleaseVersion(news.NewsId, news.ReviewerName);

                    ActivityBo.LogUpdateNews(existsNews.Id, news.ReviewerName, existsNews.Title, existsNews.Status);

                    CacheObjectBase.GetInstance<NewsCached>().RemoveAllCachedByGroup(news.NewsId.ToString());

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
        #endregion

        #region Chuyen gia sua noi dung
        public static ErrorMapping.ErrorCodes Expert_UpdateBody(ExpertNewsEntity news)
        {
            try
            {
                var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(news.NewsId);
                if (existsNews == null)
                {
                    existsNews = NewsDal.GetNewsById(news.NewsId);
                    if (existsNews != null)
                    {
                        BoCached.Base.News.NewsDalFactory.AddNews(existsNews);
                    }
                }                
                news.ExpertName = "expert_" + news.ExpertName;                
                existsNews.Body = news.Body;
                                 
                // updaye version tam cho expert                    
                var response = NewsVersionNodeJsServices.AutoSaveNewsVersion(existsNews, -1, existsNews.ListZoneId, existsNews.Tag, "", existsNews.NewsRelation, news.ExpertName);
                //luu tru vesion vao newsextension
                if(response.Success)
                    SetNewsExtensionValue(existsNews.Id,EnumNewsExtensionType.LastVersionNewsOfExpert, response.Data);
                    

                ActivityBo.LogUpdateVesionNewsOfExpert(existsNews.Id, news.ExpertName, existsNews.Title, existsNews.Status, response.Data);

                return ErrorMapping.ErrorCodes.Success;                               
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }

        public static ErrorMapping.ErrorCodes Confirm_UpdateBody(ExpertNewsEntity news)
        {
            try
            {
                var existsNews = BoCached.Base.News.NewsDalFactory.GetNewsById(news.NewsId);
                if (existsNews == null)
                {
                    existsNews = NewsDal.GetNewsById(news.NewsId);
                    if (existsNews != null)
                    {
                        BoCached.Base.News.NewsDalFactory.AddNews(existsNews);
                    }
                }
                news.ExpertName = "expert_" + news.ExpertName;
                existsNews.Body = news.Body;

                var res = NewsDal.Expert_UpdateNews(news);
                if (res)
                {
                    //update body redis
                    BoCached.Base.News.NewsDalFactory.UpdateOnlyNewsBody(news.NewsId, news.Body, news.ExpertName);

                    //release version cho expert                    
                    var response = NewsVersionNodeJsServices.ReleaseNewsVersion(news.NewsId, news.ExpertName);

                    if (response.Success)
                        SetNewsExtensionValue(existsNews.Id, EnumNewsExtensionType.LastVersionNewsOfExpert, "");

                    // Log hành động 
                    ActivityBo.LogUpdateBodyNewsExpert(existsNews.Id, news.AccountName, existsNews.Title, existsNews.Status, news.ExpertName);

                    return ErrorMapping.ErrorCodes.Success;
                }

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                return ErrorMapping.ErrorCodes.BusinessError;
            }

        }
        #endregion

        public static ErrorMapping.ErrorCodes RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId)
        {
            var isSuccess = false;            
            var currentNews = NewsDal.GetNewsById(newsInfo.Id);
            if (currentNews != null)
            {                
                currentNews.ZoneId = newsInfo.ZoneId;
                currentNews.Title = newsInfo.Title;
                currentNews.SubTitle = newsInfo.SubTitle;
                currentNews.Sapo = newsInfo.Sapo;
                currentNews.Body = newsInfo.Body;
                currentNews.Avatar = newsInfo.Avatar;
                currentNews.AvatarDesc = newsInfo.AvatarDesc;
                currentNews.Author = newsInfo.Author;
                currentNews.Source = newsInfo.Source;
                currentNews.IsFocus = newsInfo.IsFocus;
                currentNews.CreatedBy = newsInfo.CreatedBy;
                currentNews.WordCount = newsInfo.WordCount;
                currentNews.Url = newsInfo.Url;
                currentNews.IsPr = newsInfo.IsPr;
                currentNews.AdStore = newsInfo.AdStore;
                currentNews.AdStoreUrl = newsInfo.AdStoreUrl;
                currentNews.NewsRelation = newsInfo.NewsRelation;
                currentNews.ParentNewsId = newsInfo.ParentNewsId;
                newsInfo = currentNews;
            }

            var usernameForUpdateAction = newsInfo.CreatedBy;
            var user = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(usernameForUpdateAction);
            // Là thư ký tòa soạn => Lưu ở trạng thái "Nhận xuất bản"                    
            if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleAdmin))
            {
                // Là ban bien tap=> Lưu ở trạng thái "Nhận duyet"                    
                if (user != null && user.User != null && (user.User.IsRole == (int)IsRole.EditorialBoard || user.User.IsRole == (int)IsRole.Secretary))
                {
                    newsInfo.Status = (int)NewsStatus.ReceivedForEditorialBoard;
                    newsInfo.PublishedBy = usernameForUpdateAction;
                    newsInfo.CreatedBy = usernameForUpdateAction;
                }
                else
                {
                    newsInfo.Status = (int)NewsStatus.ReceivedForPublish;
                    newsInfo.ApprovedBy = usernameForUpdateAction;
                    newsInfo.CreatedBy = usernameForUpdateAction;
                }
            }
            // Là Bien tap vien => Lưu ở trạng thái "Nhận biên tập"                    
            else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(usernameForUpdateAction, (int)EnumPermission.ArticleEditor))
            {
                newsInfo.Status = (int)NewsStatus.ReceivedForEdit;
                newsInfo.EditedBy = usernameForUpdateAction;
                newsInfo.CreatedBy = usernameForUpdateAction;
            }
            // Là phóng viên => Lưu ở trạng thái "Lưu tạm"
            else
            {
                newsInfo.Status = (int)NewsStatus.Temporary;
                newsInfo.CreatedBy = usernameForUpdateAction;
            }

            if (newsInfo.Id <= 0)
            {
                newsInfo.Id = SetPrimaryId();
            }

            var primaryZone = BoCached.Base.Zone.ZoneDalFactory.GetZoneById(newsInfo.ZoneId);
            if (null == primaryZone)
            {
                primaryZone = ZoneDal.GetZoneById(newsInfo.ZoneId);
                if (null == primaryZone)
                {
                    return ErrorMapping.ErrorCodes.UpdateNewsInvalidPrimaryZone;
                }
                else
                {
                    //add zone redis
                    BoCached.Base.Zone.ZoneDalFactory.AddZone(primaryZone);
                }
            }

            newsInfo.Url = BuildLinkUrl(newsInfo.Id, newsInfo.Type, primaryZone.ShortUrl, newsInfo.Title, newsInfo.ZoneId);

            if (newsInfo.AdStore)
            {
                newsInfo.AdStoreUrl = BuildLinkUrlAdStore(newsInfo.Id, primaryZone.ShortUrl, primaryZone.Id, newsInfo.Title);
            }
            isSuccess = NewsDal.RecieveFeedbackNewsIntoCms(newsInfo, ref newsId);
            if (isSuccess)
            {                
                newsInfo.ListZoneId = newsInfo.ZoneId.ToString();
                //insert news redis
                if (!BoCached.Base.News.NewsDalFactory.AddNews(newsInfo))
                {
                    QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.REDIS, newsInfo);
                }
                //insert news es
                if (!BoSearch.Base.News.NewsDalFactory.AddNews(newsInfo))
                {
                    QueueManager.AddToQueue(ActionName.ADD_NEWS, TopicName.ES, newsInfo);
                }
            }
            return isSuccess ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
    }
}
