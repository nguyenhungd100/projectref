﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.News
{
    public class NewsGroupBo
    {
        #region News group

        /// <summary>
        /// Add: ThanhTN
        /// LastModifiedDate: 2013-01-31, 16:45
        /// </summary>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostRead(groupNewsId, withinDays, rootZoneId, excludeLastDays)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// Add: ThanhTN
        /// LastModifiedDate: 2013-01-31, 16:45
        /// </summary>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostReadDaily(groupNewsId, rootZoneId, excludeLastDays)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// Add: ThanhTN
        /// LastModifiedDate: 2013-01-31, 16:45
        /// </summary>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostReadHourly(groupNewsId, rootZoneId, excludeLastDays)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// Add: ThanhTN
        /// LastModifiedDate: 2013-12-14, 12:01
        /// </summary>
        /// <returns></returns>
        public static ErrorMapping.ErrorCodes UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostComment(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        /// <summary>
        /// Ngocnh 11/12/2014 15:20
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static NewsGroupEntity NewsGroupGetById(int id)
        {
            try
            {
                return NewsGroupDal.GetNewsGroupById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new NewsGroupEntity();
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays, int zoneId, int itemCount)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostReadByZone(groupNewsId, withinDays, excludeLastDays, zoneId, itemCount)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostReadV2(groupNewsId, withinDays, excludeLastDays, itemCount)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays, int topNews)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostCommentByZone(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays, topNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays, int topNews)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostCommentV2(groupNewsId, withinDays, objectType, excludeLastDays, topNews)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostReadV3(groupNewsId, zoneId, withinHours, totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<NewsGroupDetailEntity>();
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList)
        {
            try
            {
                return NewsGroupDal.UpdateTopMostLikeShare(groupNewsId, zoneId, lstNewsId, likeShareCountList)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount)
        {
            try
            {
                return NewsGroupDal.UpdateTopCommentCountInDay(groupNewsId, lstNewsId, lstCommentCount)
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion
    }
}
