﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.StreamItem;
using ChannelVN.CMS.Entity.Base.StreamItem;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BO.Base.StreamItem
{
    public class StreamItemBo
    {
        #region StreamItem
        public static ErrorMapping.ErrorCodes InsertStreamItem(StreamItemEntity streamItem, ref long id)
        {            
            var result = StreamItemDal.InsertStreamItem(streamItem, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es
                streamItem.Id = id;
                BoSearch.Base.StreamItem.StreamItemDalFactory.AddStreamItem(streamItem);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.AddStreamItem(streamItem);
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateStreamItem(StreamItemEntity streamItem)
        {            
            var result = StreamItemDal.UpdateStreamItem(streamItem) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //ess                
                BoSearch.Base.StreamItem.StreamItemDalFactory.UpdateStreamItem(streamItem);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.AddStreamItem(streamItem);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes DeleteStreamItemById(long id)
        {
            var result = StreamItemDal.DeleteStreamItemById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                BoSearch.Base.StreamItem.StreamItemDalFactory.DeleteStreamItemById(id);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.DeleteStreamItemById(id);
            }

            return result;
        }

        public static StreamItemEntity GetStreamItemById(long id)
        {
            var data = BoCached.Base.StreamItem.StreamItemDalFactory.GetStreamItemById(id);
            if (data == null)
            {
                data = StreamItemDal.GetStreamItemById(id);
                if (data != null)
                {
                    BoCached.Base.StreamItem.StreamItemDalFactory.AddStreamItem(data);
                }
            }
            return data; //StreamItemDal.GetStreamItemById(id);
        }

        public static StreamItemDetailEntity GetNewsForStreamItemById(long newsId)
        {
            //var data = BoCached.Base.StreamItem.StreamItemDalFactory.GetNewsForStreamItemById(newsId);
            //if (data == null)
            //{
            //    data = StreamItemDal.GetNewsForStreamItemById(newsId);
            //    if (data != null)
            //    {
            //        BoCached.Base.StreamItem.StreamItemDalFactory.AddNewsForStreamItemById(data);
            //    }
            //}
            return StreamItemDal.GetNewsForStreamItemById(newsId);
        }       

        public static List<StreamItemEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            var streamItems= new List<StreamItemEntity>();
            try
            {
                var data = BoSearch.Base.StreamItem.StreamItemDalFactory.SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);

                if (data == null || (data != null && data.Count <= 0))
                {
                    streamItems = StreamItemDal.SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);                    
                }
                streamItems = data.Select(s => new StreamItemEntity
                {
                    Id=s.Id,
                    Order = s.Order,
                    TypeId = s.TypeId,
                    TemplateId = s.TemplateId,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate,
                    CreatedBy = s.CreatedBy,
                    PublishedDate = s.PublishedDate,
                    PublishedBy = s.PublishedBy,
                    DataJson=s.DataJson,
                    Mode=s.Mode,
                    Title=s.Title
                }).ToList();

                return streamItems;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return streamItems;
            }             
        }

        public static long CountStreamItem()
        {            
            try
            {                
                return BoSearch.Base.StreamItem.StreamItemDalFactory.CountStreamItem();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return 0;
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitEsByStreamItemAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = StreamItemDal.InitESAllStreamItem(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = StreamItemDal.InitESAllStreamItem(page, pageSize, startDate, endDate, ref totalRows);
                var list = data.Select(s => new StreamItemSearchEntity
                {
                    Id = s.Id,
                    Order = s.Order,
                    TypeId = s.TypeId,
                    TemplateId = s.TemplateId,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate,
                    CreatedBy = s.CreatedBy,
                    PublishedDate = s.PublishedDate,
                    PublishedBy = s.PublishedBy,
                    DataJson = s.DataJson
                }).ToList();
                BoSearch.Base.StreamItem.StreamItemDalFactory.InitAllStreamItem(list);
            }, action);
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByStreamItemAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = StreamItemDal.InitESAllStreamItem(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = StreamItemDal.InitESAllStreamItem(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.StreamItem.StreamItemDalFactory.InitAllStreamItem(data);
            }, action);
        }

        #endregion

        #region StreamItemMobile
        public static ErrorMapping.ErrorCodes InsertStreamItemMobile(StreamItemMobileEntity streamItem, ref long id)
        {
            var result = StreamItemDal.InsertStreamItemMobile(streamItem, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es
                streamItem.Id = id;
                BoSearch.Base.StreamItem.StreamItemDalFactory.AddStreamItemMobile(streamItem);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.AddStreamItemMobile(streamItem);
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            var result = StreamItemDal.UpdateStreamItemMobile(streamItem) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //ess                
                BoSearch.Base.StreamItem.StreamItemDalFactory.UpdateStreamItemMobile(streamItem);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.AddStreamItemMobile(streamItem);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes DeleteStreamItemMobileById(long id)
        {
            var result = StreamItemDal.DeleteStreamItemMobileById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                BoSearch.Base.StreamItem.StreamItemDalFactory.DeleteStreamItemMobileById(id);

                //redis
                BoCached.Base.StreamItem.StreamItemDalFactory.DeleteStreamItemMobileById(id);
            }

            return result;
        }

        public static StreamItemMobileEntity GetStreamItemMobileById(long id)
        {
            return StreamItemDal.GetStreamItemMobileById(id);
        }

        public static List<StreamItemMobileEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            var streamItems = new List<StreamItemMobileEntity>();
            try
            {
                var data = BoSearch.Base.StreamItem.StreamItemDalFactory.SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);

                if (data == null || (data != null && data.Count <= 0))
                {
                    streamItems = StreamItemDal.SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
                }
                streamItems = data.Select(s => new StreamItemMobileEntity
                {
                    Id = s.Id,
                    Order = s.Order,
                    TypeId = s.TypeId,
                    TemplateId = s.TemplateId,
                    Status = s.Status,
                    CreatedDate = s.CreatedDate,
                    CreatedBy = s.CreatedBy,
                    PublishedDate = s.PublishedDate,
                    PublishedBy = s.PublishedBy,
                    DataJson = s.DataJson
                }).ToList();

                return streamItems;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return streamItems;
            }
        }

        #endregion
    }
}
