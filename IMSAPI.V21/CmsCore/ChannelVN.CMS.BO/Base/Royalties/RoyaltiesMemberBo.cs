﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesMemberBo
    {
        #region Get

        public static RoyaltiesMemberEntity GetRoyaltiesMemberById(int id)
        {
            return RoyaltiesMemberDal.GetById(id);
        }

        public static List<RoyaltiesMemberEntity> SearchRoyaltiesMember(int royaltiesRoleId, string keyword, EnumRoyaltiesMemberStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            return RoyaltiesMemberDal.Search(royaltiesRoleId, keyword, (int)status, pageIndex, pageSize, ref totalRow);
        }

        #endregion

        #region Set

        public static ErrorMapping.ErrorCodes InsertRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember)
        {
            return RoyaltiesMemberDal.Insert(royaltiesMember)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesMember(RoyaltiesMemberEntity royaltiesMember)
        {
            return RoyaltiesMemberDal.Update(royaltiesMember)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteRoyaltiesMember(int royaltiesMemberId)
        {
            return RoyaltiesMemberDal.Delete(royaltiesMemberId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
