﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesRoleBo
    {
        #region Get

        public static RoyaltiesRoleEntity GetRoyaltiesRoleById(int id)
        {
            return RoyaltiesRoleDal.GetById(id);
        }

        public static List<RoyaltiesRoleEntity> GetAllRoyaltiesRole()
        {
            return RoyaltiesRoleDal.GetAll();
        }

        #endregion

        #region Set

        public static ErrorMapping.ErrorCodes InsertRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole)
        {
            return RoyaltiesRoleDal.Insert(royaltiesRole)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesRole(RoyaltiesRoleEntity royaltiesRole)
        {
            return RoyaltiesRoleDal.Update(royaltiesRole)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteRoyaltiesRole(int royaltiesRoleId)
        {
            return RoyaltiesRoleDal.Delete(royaltiesRoleId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
