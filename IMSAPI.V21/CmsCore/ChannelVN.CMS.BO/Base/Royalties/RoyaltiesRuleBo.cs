﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesRuleBo
    {
        #region Get

        public static List<RoyaltiesRuleEntity> GetRoyaltiesRuleByZoneId(int zoneId)
        {
            return RoyaltiesRuleDal.GetByZoneId(zoneId);
        }

        public static RoyaltiesRuleEntity GetByRoyaltiesCategoryIdAndRoyaltiesRate(bool isManager,
                                                                                   int royaltiesCategoryId,
                                                                                   int royaltiesRate)
        {
            return RoyaltiesRuleDal.GetByRoyaltiesCategoryIdAndRoyaltiesRate(isManager,
                                                                             royaltiesCategoryId,
                                                                             royaltiesRate);
        }

        #endregion

        #region Set

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesRuleValues(List<RoyaltiesRuleEntity> listRoyaltiesRule)
        {
            try
            {
                foreach (var royaltiesRule in listRoyaltiesRule)
                {
                    RoyaltiesRuleDal.UpdateValue(royaltiesRule.RoyaltiesCategoryId, royaltiesRule.IsManager,
                                                 royaltiesRule.RoyaltiesRate, royaltiesRule.RoyaltiesValue);
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception ex)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        #endregion
    }
}
