﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesBo
    {
        #region Get

        public static RoyaltiesEntity GetRoyaltiesById(long id)
        {
            return RoyaltiesDal.GetById(id);
        }

        public static List<RoyaltiesEntity> SearchRoyalties(EnumRoyaltiesCategoryType type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            if (fromDate > DbCommon.MinDateTime)
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            if (toDate > DbCommon.MinDateTime)
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

            var listRoyalties = RoyaltiesDal.Search((int)type, zoneId, royaltiesMemberId, royaltiesCategoryId, royaltiesRoleId,
                                                    keyword,
                                                    royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            var listRoyaltiesId = listRoyalties.Aggregate("", (current, royalty) => current + (";" + royalty.Id));
            if (!string.IsNullOrEmpty(listRoyaltiesId)) listRoyaltiesId = listRoyaltiesId.Remove(0, 1);

            var listOldRoyaltiesHistory = RoyaltiesHistoryDal.GetOldRoyaltiesHistoryByListRoyaltiesId(listRoyaltiesId);
            foreach (var royalty in listRoyalties)
            {
                var oldRoyalties = listOldRoyaltiesHistory.Find(item => item.RoyaltiesId == royalty.Id);
                if (oldRoyalties != null)
                {
                    royalty.RoyaltiesOldTotalValue = oldRoyalties.RoyaltiesTotalValue;
                }
            }
            return listRoyalties;
        }

        public static List<RoyaltiesGroupByUserEntity> SearchRoyaltiesGroupByUser(EnumRoyaltiesCategoryType type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate)
        {
            if (fromDate > DbCommon.MinDateTime)
                fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            if (toDate > DbCommon.MinDateTime)
                toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 59);

            return RoyaltiesDal.SearchGroupByUser((int)type, zoneId, royaltiesMemberId, royaltiesCategoryId,
                                                  royaltiesRoleId,
                                                  keyword,
                                                  royaltiesRate, fromDate, toDate);
        }

        public static List<RoyaltiesHistoryEntity> GetRoyaltiesHistoryByRoyaltiesId(long royaltiesId)
        {
            return RoyaltiesHistoryDal.GetByRoyaltiesId(royaltiesId);
        }
        public static List<RoyaltiesEntity> GetRoyaltiesByNewsId(long newsId)
        {
            return RoyaltiesDal.GetByNewsId(newsId);
        }

        #endregion

        #region Set

        private static RoyaltiesEntity CalculateRoyaltiesValue(int zoneId, RoyaltiesEntity royalties)
        {
            var royaltiesRules = RoyaltiesRuleDal.GetByZoneId(zoneId);

            if (royaltiesRules != null && royaltiesRules.Count > 0)
            {
                #region For article

                var royaltiesForArticle =
                    royaltiesRules.Find(
                        item =>
                        item.IsManager = royalties.IsManager &&
                                         item.RoyaltiesCategoryId == royalties.RoyaltiesCategoryId &&
                                         item.RoyaltiesRate == royalties.RoyaltiesRate);
                if (royaltiesForArticle != null) royalties.RoyaltiesValue = royaltiesForArticle.RoyaltiesValue;

                #endregion

                #region For clip

                royalties.ClipRoyaltiesValue = royalties.SelfProduceClipRoyaltiesValue;

                if (royalties.CountClipClassA > 0)
                {
                    var royaltiesForClipA = royaltiesRules.Find(
                        item =>
                        item.IsManager = royalties.IsManager &&
                                         item.RoyaltiesCategoryType == (int)EnumRoyaltiesCategoryType.ForClip &&
                                         item.RoyaltiesRate == 1);
                    if (royaltiesForClipA != null)
                        royalties.ClipRoyaltiesValue += royalties.CountClipClassA * royaltiesForClipA.RoyaltiesValue;
                }

                if (royalties.CountClipClassB > 0)
                {
                    var royaltiesForClipB = royaltiesRules.Find(
                        item =>
                        item.IsManager = royalties.IsManager &&
                                         item.RoyaltiesCategoryType == (int)EnumRoyaltiesCategoryType.ForClip &&
                                         item.RoyaltiesRate == 2);
                    if (royaltiesForClipB != null)
                        royalties.ClipRoyaltiesValue += royalties.CountClipClassB * royaltiesForClipB.RoyaltiesValue;
                }

                #endregion

                #region For photo

                royalties.PhotoRoyaltiesValue = 0;

                if (royalties.CountPhotoClassA > 0)
                {
                    var royaltiesForPhotoA = royaltiesRules.Find(
                        item =>
                        item.IsManager = royalties.IsManager &&
                                         item.RoyaltiesCategoryType == (int)EnumRoyaltiesCategoryType.ForPhoto &&
                                         item.RoyaltiesRate == 1);
                    if (royaltiesForPhotoA != null)
                        royalties.PhotoRoyaltiesValue += royalties.CountPhotoClassA * royaltiesForPhotoA.RoyaltiesValue;
                }

                if (royalties.CountPhotoClassB > 0)
                {
                    var royaltiesForPhotoB = royaltiesRules.Find(
                        item =>
                        item.IsManager = royalties.IsManager &&
                                         item.RoyaltiesCategoryType == (int)EnumRoyaltiesCategoryType.ForPhoto &&
                                         item.RoyaltiesRate == 2);
                    if (royaltiesForPhotoB != null)
                        royalties.PhotoRoyaltiesValue += royalties.CountPhotoClassB * royaltiesForPhotoB.RoyaltiesValue;
                }

                #endregion

                royalties.RoyaltiesTotalValue = royalties.RoyaltiesValue +
                                                royalties.ClipRoyaltiesValue +
                                                royalties.PhotoRoyaltiesValue;
            }
            return royalties;
        }

        public static ErrorMapping.ErrorCodes InsertRoyalties(RoyaltiesEntity royalties)
        {
            var royaltiesCategory = RoyaltiesCategoryDal.GetById(royalties.RoyaltiesCategoryId);

            if (royaltiesCategory != null)
            {
                var zoneId = royaltiesCategory.ZoneId;
                royalties.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                var newRoyaltiesId = 0L;

                var result = RoyaltiesDal.Insert(CalculateRoyaltiesValue(zoneId, royalties), ref newRoyaltiesId);
                if (result && newRoyaltiesId > 0)
                {
                    RoyaltiesHistoryDal.Insert(new RoyaltiesHistoryEntity
                        {
                            RoyaltiesId = newRoyaltiesId,
                            RoyaltiesCategoryId = royalties.RoyaltiesCategoryId,
                            RoyaltiesRate = royalties.RoyaltiesRate,
                            RoyaltiesValue = royalties.RoyaltiesValue,
                            RoyaltiesBonus = royalties.RoyaltiesBonus,
                            RoyaltiesPenalty = royalties.RoyaltiesPenalty,
                            RoyaltiesTotalValue = royalties.RoyaltiesTotalValue,
                            CountPhotoClassA = royalties.CountPhotoClassA,
                            CountPhotoClassB = royalties.CountPhotoClassB,
                            PhotoRoyaltiesValue = royalties.PhotoRoyaltiesValue,
                            CountClipClassA = royalties.CountClipClassA,
                            CountClipClassB = royalties.CountClipClassB,
                            ClipRoyaltiesValue = royalties.ClipRoyaltiesValue,
                            SelfProduceClipRoyaltiesValue = royalties.SelfProduceClipRoyaltiesValue,
                            Note = royalties.Note,
                            LastModifiedBy = royalties.CreatedBy
                        });
                }
                return result
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyalties(RoyaltiesEntity royalties)
        {
            var royaltiesCategory = RoyaltiesCategoryDal.GetById(royalties.RoyaltiesCategoryId);

            if (royaltiesCategory != null)
            {
                var zoneId = royaltiesCategory.ZoneId;

                var currentRoyalties = RoyaltiesDal.GetById(royalties.Id);
                if (currentRoyalties == null) return ErrorMapping.ErrorCodes.BusinessError;

                currentRoyalties.RoyaltiesCategoryId = royalties.RoyaltiesCategoryId;
                currentRoyalties.RoyaltiesRate = royalties.RoyaltiesRate;
                currentRoyalties.RoyaltiesValue = royalties.RoyaltiesValue;
                currentRoyalties.RoyaltiesBonus = royalties.RoyaltiesBonus;
                currentRoyalties.RoyaltiesPenalty = royalties.RoyaltiesPenalty;
                currentRoyalties.RoyaltiesTotalValue = royalties.RoyaltiesTotalValue;
                currentRoyalties.CountPhotoClassA = royalties.CountPhotoClassA;
                currentRoyalties.CountPhotoClassB = royalties.CountPhotoClassB;
                currentRoyalties.PhotoRoyaltiesValue = royalties.PhotoRoyaltiesValue;
                currentRoyalties.CountClipClassA = royalties.CountClipClassA;
                currentRoyalties.CountClipClassB = royalties.CountClipClassB;
                currentRoyalties.ClipRoyaltiesValue = royalties.ClipRoyaltiesValue;
                currentRoyalties.SelfProduceClipRoyaltiesValue = royalties.SelfProduceClipRoyaltiesValue;
                currentRoyalties.Note = royalties.Note;
                currentRoyalties.LastModifiedBy = royalties.LastModifiedBy;

                var result = RoyaltiesDal.Update(CalculateRoyaltiesValue(zoneId, currentRoyalties));
                if (result)
                {
                    RoyaltiesHistoryDal.Insert(new RoyaltiesHistoryEntity
                        {
                            RoyaltiesId = currentRoyalties.Id,
                            RoyaltiesCategoryId = currentRoyalties.RoyaltiesCategoryId,
                            RoyaltiesRate = currentRoyalties.RoyaltiesRate,
                            RoyaltiesValue = currentRoyalties.RoyaltiesValue,
                            RoyaltiesBonus = currentRoyalties.RoyaltiesBonus,
                            RoyaltiesPenalty = currentRoyalties.RoyaltiesPenalty,
                            RoyaltiesTotalValue = currentRoyalties.RoyaltiesTotalValue,
                            CountPhotoClassA = currentRoyalties.CountPhotoClassA,
                            CountPhotoClassB = currentRoyalties.CountPhotoClassB,
                            PhotoRoyaltiesValue = currentRoyalties.PhotoRoyaltiesValue,
                            CountClipClassA = currentRoyalties.CountClipClassA,
                            CountClipClassB = currentRoyalties.CountClipClassB,
                            ClipRoyaltiesValue = currentRoyalties.ClipRoyaltiesValue,
                            SelfProduceClipRoyaltiesValue = currentRoyalties.SelfProduceClipRoyaltiesValue,
                            Note = currentRoyalties.Note,
                            LastModifiedBy = currentRoyalties.LastModifiedBy
                        });
                }
                return result
                           ? ErrorMapping.ErrorCodes.Success
                           : ErrorMapping.ErrorCodes.BusinessError;
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes DeleteRoyalties(int royaltiesId)
        {
            return RoyaltiesDal.Delete(royaltiesId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateListRoyalties(long newsId, List<RoyaltiesEntity> listRoyalties)
        {
            try
            {
                foreach (var royalty in listRoyalties)
                {
                    royalty.NewsId = newsId;
                    royalty.VideoId = 0;
                    royalty.PhotoId = 0;
                    royalty.CreatedBy = WcfMessageHeader.Current.ClientUsername;
                    royalty.LastModifiedBy = WcfMessageHeader.Current.ClientUsername;

                    if (royalty.Id > 0)
                    {
                        UpdateRoyalties(royalty);
                    }
                    else
                    {
                        InsertRoyalties(royalty);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
