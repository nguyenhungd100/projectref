﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesQuotaBo
    {
        #region Get

        public static List<RoyaltiesQuotaEntity> Search(string keyword,
                                                          UserStatus status,
                                                          UserSortExpression sortOrder,
                                                          int pageIndex,
                                                          int pageSize,
                                                          ref int totalRow)
        {
            var userStandards = RoyaltiesQuotaDal.Search(keyword, status, sortOrder, pageIndex, pageSize, ref totalRow);

            return userStandards;
        }

        #endregion

        #region Set
        public static ErrorMapping.ErrorCodes UpdateQuota(string lstId, string lstPost, string lstView)
        {
            var result = RoyaltiesQuotaDal.Update(lstId, lstPost, lstView);
            return result
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }
        #endregion
    }
}
