﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.Royalties
{
    public class RoyaltiesCategoryBo
    {
        #region Get

        public static RoyaltiesCategoryEntity GetRoyaltiesCategoryById(int id)
        {
            return RoyaltiesCategoryDal.GetById(id);
        }

        public static List<RoyaltiesCategoryEntity> GetAllRoyaltiesCategory()
        {
            return RoyaltiesCategoryDal.GetAll();
        }

        public static List<RoyaltiesCategoryEntity> GetByRoyaltiesCategoryType(int zoneId, EnumRoyaltiesCategoryType type)
        {
            return RoyaltiesCategoryDal.GetByRoyaltiesCategoryType(zoneId, (int)type);
        }

        #endregion

        #region Set

        public static ErrorMapping.ErrorCodes InsertRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory)
        {
            return RoyaltiesCategoryDal.Insert(royaltiesCategory)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesCategory(RoyaltiesCategoryEntity royaltiesCategory)
        {
            return RoyaltiesCategoryDal.Update(royaltiesCategory)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        public static ErrorMapping.ErrorCodes UpdateRoyaltiesCategoryForClipAndPhoto(int zoneId)
        {
            try
            {
                var currentListCategory = RoyaltiesCategoryDal.GetByRoyaltiesCategoryType(zoneId, -1);
                if (!currentListCategory.Exists(item => item.Type == (int)EnumRoyaltiesCategoryType.ForClip))
                {
                    RoyaltiesCategoryDal.Insert(new RoyaltiesCategoryEntity
                    {
                        Name = "Nhuận clip",
                        ZoneId = zoneId,
                        Type = (int)EnumRoyaltiesCategoryType.ForClip
                    });
                }
                if (!currentListCategory.Exists(item => item.Type == (int)EnumRoyaltiesCategoryType.ForPhoto))
                {
                    RoyaltiesCategoryDal.Insert(new RoyaltiesCategoryEntity
                    {
                        Name = "Nhuận ảnh",
                        ZoneId = zoneId,
                        Type = (int)EnumRoyaltiesCategoryType.ForPhoto
                    });
                }
                return ErrorMapping.ErrorCodes.Success;
            }
            catch (Exception)
            {
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes DeleteRoyaltiesCategory(int royaltiesCategoryId)
        {
            return RoyaltiesCategoryDal.Delete(royaltiesCategoryId)
                       ? ErrorMapping.ErrorCodes.Success
                       : ErrorMapping.ErrorCodes.BusinessError;
        }

        #endregion
    }
}
