﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.FileUpload;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.FileUpload
{
    public class FileUploadBo
    {
        #region Update

        public static ErrorMapping.ErrorCodes InsertFileUpload(FileUploadEntity fileUpload, ref int newFileUploadId)
        {
            try
            {
                if (null == fileUpload)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                fileUpload.UploadedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return FileUploadDal.InsertFileUpload(fileUpload, ref newFileUploadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UpdateFileUpload(FileUploadEntity fileUpload)
        {
            try
            {
                if (null == fileUpload)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                fileUpload.LastModifiedBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                return FileUploadDal.UpdateFileUpload(fileUpload) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes DeleteFileUpload(int fileUploadId)
        {
            try
            {
                return FileUploadDal.DeleteFileUpload(fileUploadId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region Get

        public static FileUploadEntity GetFileUploadById(int id)
        {
            try
            {
                return FileUploadDal.GetFileUploadById(id);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }
        public static List<FileUploadEntity> SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                return FileUploadDal.SearchFileUpload(keyword, zoneId, ext, uploadedBy, status, pageIndex, pageSize, ref totalRow);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FileUploadEntity>();
            }
        }
        public static List<FileUploadExtEntity> GetAllExt()
        {
            try
            {
                return FileUploadDal.GetAllExt();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new List<FileUploadExtEntity>();
            }
        }
        #endregion
    }
}
