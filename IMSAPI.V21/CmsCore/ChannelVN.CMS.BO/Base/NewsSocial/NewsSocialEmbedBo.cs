﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsSocial;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsSocial
{
    public class NewsSocialEmbedBo
    {
        #region NewsSocialEmbed
        public static ErrorMapping.ErrorCodes InsertNewsSocialEmbed(NewsSocialEmbedEntity streamItem, ref long id)
        {            
            var result = NewsSocialEmbedDal.InsertNewsSocialEmbed(streamItem, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es
                //streamItem.Id = id;
                //BoSearch.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.AddNewsSocialEmbed(streamItem);

                //redis
                //BoCached.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.AddNewsSocialEmbed(streamItem);
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateNewsSocialEmbed(NewsSocialEmbedEntity streamItem)
        {            
            var result = NewsSocialEmbedDal.UpdateNewsSocialEmbed(streamItem) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //ess                
                //BoSearch.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.UpdateNewsSocialEmbed(streamItem);

                //redis
                //BoCached.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.AddNewsSocialEmbed(streamItem);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes DeleteNewsSocialEmbedById(long id)
        {
            var result = NewsSocialEmbedDal.DeleteNewsSocialEmbedById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                //BoSearch.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.DeleteNewsSocialEmbedById(id);

                //redis
                //BoCached.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.DeleteNewsSocialEmbedById(id);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes OnToggleStatus(long id)
        {
            var result = NewsSocialEmbedDal.OnToggleStatus(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                //BoSearch.Base.NewsSocialEmbed.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);

                //redis
                //BoCached.Base.NewsSocialEmbed.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);
            }

            return result;
        }

        public static NewsSocialEmbedEntity GetNewsSocialEmbedById(long id)
        {            
            return NewsSocialEmbedDal.GetNewsSocialEmbedById(id);
        }

        public static NewsSocialEmbedEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            return NewsSocialEmbedDal.GetNewsSocialEmbedByRawId(rawId);
        }

        public static List<NewsSocialEmbedEntity> SearchNewsSocialEmbed(int status, int pageIndex, int pageSize, ref int totalRow)
        {
            var streamItems= new List<NewsSocialEmbedEntity>();
            try
            {
                //var data = BoSearch.Base.NewsSocialEmbed.NewsSocialEmbedDalFactory.SearchNewsSocialEmbed(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);

                //if (data == null || (data != null && data.Count <= 0))
                //{
                    streamItems = NewsSocialEmbedDal.SearchNewsSocialEmbed(status, pageIndex, pageSize, ref totalRow);                    
                //}
                //streamItems = data.Select(s => new NewsSocialEmbedEntity
                //{
                //    Id=s.Id,
                //    Order = s.Order,
                //    TypeId = s.TypeId,
                //    TemplateId = s.TemplateId,
                //    Status = s.Status,
                //    CreatedDate = s.CreatedDate,
                //    CreatedBy = s.CreatedBy,
                //    PublishedDate = s.PublishedDate,
                //    PublishedBy = s.PublishedBy,
                //    DataJson=s.DataJson
                //}).ToList();

                return streamItems;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return streamItems;
            }             
        }

        #endregion        
    }
}
