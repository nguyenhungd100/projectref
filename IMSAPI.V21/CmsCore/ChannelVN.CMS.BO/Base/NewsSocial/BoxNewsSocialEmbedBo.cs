﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsSocial;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsSocial
{
    public class BoxNewsSocialEmbedBo
    {
        #region BoxNewsSocialEmbed

        public static List<BoxNewsSocialEmbedEntity> GetListBoxNewsSocialEmbed(int type, int zoneId)
        {
            var newsEmbedBox = new List<BoxNewsSocialEmbedEntity>();
            try
            {
                return BoxNewsSocialEmbedDal.GetListBoxNewsSocialEmbed(type, zoneId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return newsEmbedBox;

        }

        public static ErrorMapping.ErrorCodes Update(string listNewsId, int type, int zoneId)
        {
            try
            {
                if (null != listNewsId)
                {
                    BoxNewsSocialEmbedDal.Update(listNewsId, type, zoneId);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("BoxNewsSocialEmbedDal.Update:{0}", ex.Message));
            }

        }

        #endregion        
    }
}
