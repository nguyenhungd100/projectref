﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.NewsSocial;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BO.Base.NewsSocial
{
    public class NewsSocialContentBo
    {
        #region NewsSocialContent
        public static ErrorMapping.ErrorCodes InsertNewsSocialContent(NewsSocialContentEntity streamItem, ref long id)
        {            
            var result = NewsSocialContentDal.InsertNewsSocialContent(streamItem, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es
                //streamItem.Id = id;
                //BoSearch.Base.NewsSocialContent.NewsSocialContentDalFactory.AddNewsSocialContent(streamItem);

                //redis
                //BoCached.Base.NewsSocialContent.NewsSocialContentDalFactory.AddNewsSocialContent(streamItem);
            }
            return result;
        }

        public static ErrorMapping.ErrorCodes UpdateNewsSocialContent(NewsSocialContentEntity streamItem)
        {            
            var result = NewsSocialContentDal.UpdateNewsSocialContent(streamItem) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //ess                
                //BoSearch.Base.NewsSocialContent.NewsSocialContentDalFactory.UpdateNewsSocialContent(streamItem);

                //redis
                //BoCached.Base.NewsSocialContent.NewsSocialContentDalFactory.AddNewsSocialContent(streamItem);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes DeleteNewsSocialContentById(long id)
        {
            var result = NewsSocialContentDal.DeleteNewsSocialContentById(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                //BoSearch.Base.NewsSocialContent.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);

                //redis
                //BoCached.Base.NewsSocialContent.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);
            }

            return result;
        }

        public static ErrorMapping.ErrorCodes OnToggleStatus(long id)
        {
            var result = NewsSocialContentDal.OnToggleStatus(id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (result == ErrorMapping.ErrorCodes.Success)
            {
                //es             
                //BoSearch.Base.NewsSocialContent.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);

                //redis
                //BoCached.Base.NewsSocialContent.NewsSocialContentDalFactory.DeleteNewsSocialContentById(id);
            }

            return result;
        }

        public static NewsSocialContentEntity GetNewsSocialContentById(long id)
        {            
            return NewsSocialContentDal.GetNewsSocialContentById(id);
        }

        public static NewsSocialContentEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            return NewsSocialContentDal.GetNewsSocialEmbedByRawId(rawId);
        }

        public static List<NewsSocialContentEntity> SearchNewsSocialContent(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            var streamItems= new List<NewsSocialContentEntity>();
            try
            {
                //var data = BoSearch.Base.NewsSocialContent.NewsSocialContentDalFactory.SearchNewsSocialContent(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);

                //if (data == null || (data != null && data.Count <= 0))
                //{
                    streamItems = NewsSocialContentDal.SearchNewsSocialContent(keyword, status, pageIndex, pageSize, ref totalRow);                    
                //}
                //streamItems = data.Select(s => new NewsSocialContentEntity
                //{
                //    Id=s.Id,
                //    Order = s.Order,
                //    TypeId = s.TypeId,
                //    TemplateId = s.TemplateId,
                //    Status = s.Status,
                //    CreatedDate = s.CreatedDate,
                //    CreatedBy = s.CreatedBy,
                //    PublishedDate = s.PublishedDate,
                //    PublishedBy = s.PublishedBy,
                //    DataJson=s.DataJson
                //}).ToList();

                return streamItems;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return streamItems;
            }             
        }

        #endregion        
    }
}
