﻿using System;
using ChannelVN.CMS.DAL.Base.NewsSlave;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.BO.Base.NewsSlave
{
    public class NewsSlaveBo
    {
        #region NewsSlave
        
        public static ErrorMapping.ErrorCodes DeleteNews(long newsId,string connectSlave)
        {
            try
            {              
                if(NewsSlaveDal.DeleteNews(newsId, connectSlave))
                    return ErrorMapping.ErrorCodes.Success;

                return ErrorMapping.ErrorCodes.BusinessError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, string.Format("NewsSlaveDal.Update:{0}", ex.Message));
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        #endregion        
    }
}
