﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Topic;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.BO.Common;
using System.Dynamic;
using ChannelVN.SEO.DAL.SEOMetaTopic;

namespace ChannelVN.CMS.BO.Base.Topic
{
    public class TopicBo
    {
        #region Get
        public static TopicEntity GetTopicByName(string nameTopic)
        {
            return TopicDal.GetTopicByName(nameTopic);            
        }
        public static TopicDetailEntity GetTopicByTopicId(long TopicId)
        {
            TopicDetailEntity TopicDetail = null;
            var Topic = BoCached.Base.Topic.TopicDalFactory.GetTopicByTopicId(TopicId);
            if (Topic == null)
            {
                Topic = TopicDal.GetTopicByTopicId(TopicId);
                if (null != Topic)
                {
                    BoCached.Base.Topic.TopicDalFactory.AddTopic(Topic);
                }
            }
            if (null != Topic)
            {
                var patentTopic = TopicDal.GetTopicByTopicId(Topic.ParentId);
                TopicDetail = new TopicDetailEntity
                {
                    Id = Topic.Id,
                    TopicName = Topic.TopicName,
                    Logo = Topic.Logo,
                    Cover = Topic.Cover,
                    IsActive = Topic.IsActive,
                    IsIconActive = Topic.IsIconActive,
                    TopicInZone = TopicInZoneDal.GetTopicInZoneByTopicId(Topic.Id),
                    ListTag = TopicDal.GetTagsByTopicId(Topic.Id),
                    Description = Topic.Description,
                    LogoFancyClose = Topic.LogoFancyClose,
                    LogoTopicName = Topic.LogoTopicName,
                    LogoSubMenu = Topic.LogoSubMenu,
                    DefaultViewMode = Topic.DefaultViewMode,
                    GuideToSendMail = Topic.GuideToSendMail,
                    TopicEmail = Topic.TopicEmail,
                    IsTopToolbar = Topic.IsTopToolbar,
                    ParentId=Topic.ParentId,
                    ParentName= patentTopic != null? patentTopic.TopicName:"",
                    RelationTopic =Topic.RelationTopic,
                    ListRelationTopic = TopicDal.GetTopicRelationByTopicId(Topic.Id),
                    DisplayName=Topic.DisplayName,
                    Priority=Topic.Priority,
                    ZoneId=Topic.ZoneId,
                    ZoneIdList=Topic.ZoneIdList,
                    SeoMetaTopic= SEOMetaTopicDal.GetSeoMetaTopicByTopicId(Topic.Id),
                    CreatedBy=Topic.CreatedBy,
                    CreatedDate=Topic.CreatedDate,
                    ModifiedBy=Topic.ModifiedBy,
                    ModifiedDate=Topic.ModifiedDate,
                    DisplayUrl=Topic.DisplayUrl,
                    NewsCount=Topic.NewsCount,
                    TagInString=Topic.TagInString,
                    IsAmp=Topic.IsAmp
                };
            }
            return TopicDetail;
        }
        public static List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            try
            {
                //var list = BoCached.Base.Topic.TopicDalFactory.GetTopicByNewsId(newsId);
                //if (list == null || (list != null && list.Count == 0))
                //{
                //    list = TopicDal.GetTopicByNewsId(newsId);
                //    if (list != null && list.Count > 0)
                //    {
                //        var topic = list.FirstOrDefault(s => s.IsPrimary = true);
                //        if (topic != null)
                //        {                           
                //            //add topic to redis
                //            BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(newsId, topic.Id);
                //        }
                //    }
                //}

                return TopicDal.GetTopicByNewsId(newsId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<TopicEntity>();
            }
        }
        //app bot
        public static TopicDataAppBotEntity GetListTopicAndTopicParrentByNewsId(long newsId)
        {
            var dataOjc = new TopicDataAppBotEntity();
            try
            {                
                var data = TopicDal.GetTopicByNewsId(newsId);
                if(data!=null && data.Count > 0)
                {
                    if (data.Count == 1)
                    {
                        var item = data[0];
                        dataOjc.CurrentTopic = item;
                        dataOjc.ListTopic = TopicDal.GetTopicByParentId(item.ParentId <= 0 ? item.Id : item.ParentId);
                    }
                    else {
                        TopicEntity primaryItem = data.Where(t => t.IsPrimary == true).FirstOrDefault();
                        if(primaryItem == null)
                        {
                            primaryItem = data[0];
                        }

                        dataOjc.CurrentTopic = primaryItem;
                        if (primaryItem.ParentId == 0)
                        {
                            var listTopic = TopicDal.GetTopicByParentId(primaryItem.Id);
                            dataOjc.ListTopic = listTopic;
                        }
                        else
                        {
                            var topicParent = TopicDal.GetTopicByTopicId(primaryItem.ParentId);
                            if (topicParent != null)
                            {
                                var listTopic = TopicDal.GetTopicByParentId(topicParent.Id);
                                dataOjc.ListTopic = listTopic;
                            }
                        }
                        
                    }
                }

                if(dataOjc.ListTopic == null)
                {
                    dataOjc.ListTopic = new List<TopicEntity>();
                }

                return dataOjc;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return dataOjc;
            }
        }
        public static List<TopicEntity> SearchTopic(string keyword, int zoneId, int orderBy, int isHotTopic, int isActive, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {           
                var data= TopicDal.SearchTopic(keyword, zoneId, orderBy, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);
                
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TopicEntity>();
        }

        public static List<TopicParent> ListParentTopicById(int topicId)
        {
            try
            {                
                return TopicDal.ListParentTopicById(topicId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return new List<TopicParent>();
        }

        public static ErrorMapping.ErrorCodes ToggleTopicActive(int id)
        {
            try
            {                
                if (TopicDal.ToggleTopicActive(id))
                {
                    BoCached.Base.Topic.TopicDalFactory.ToggleTopicActive(id);
                }
                return ErrorMapping.ErrorCodes.Success;               
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ToggleTopicIconActive(int id)
        {
            try
            {                
                if (TopicDal.ToggleTopicIconActive(id))
                {
                    BoCached.Base.Topic.TopicDalFactory.ToggleTopicIconActive(id);
                }
                return ErrorMapping.ErrorCodes.Success;                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static ErrorMapping.ErrorCodes ToggleTopicIsToolbar(int id)
        {
            try
            {                
                if (TopicDal.ToggleTopicIsToolbar(id))
                {
                    BoCached.Base.Topic.TopicDalFactory.ToggleTopicIsToolbar(id);
                }
                return ErrorMapping.ErrorCodes.Success;                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.BusinessError;
            }
        }

        public static BoCached.Entity.Init.LogInitAsyncEntity InitRedisByTopicAsync(string name, DateTime startDate, DateTime endDate, int pageSize, string action = null)
        {
            var totalRows = 0;
            return BoCached.Common.Queue.InitSyncData(name, 1, startDate, endDate, () =>
            {
                var data = TopicDal.InitESAllTopic(1, pageSize, startDate, endDate, ref totalRows);
                var totalPages = (int)Math.Ceiling((double)totalRows / pageSize);
                return totalPages;
            }, (page) =>
            {
                var data = TopicDal.InitESAllTopic(page, pageSize, startDate, endDate, ref totalRows);
                BoCached.Base.Topic.TopicDalFactory.InitAllTopic(data);
            }, action);
        }
        #endregion

        #region Update
        public static ErrorMapping.ErrorCodes Insert(TopicEntity Topic, int zoneId, string zoneIdList, ref int newTopicId)
        {
            if (string.IsNullOrEmpty(Topic.TopicName))
            {
                return ErrorMapping.ErrorCodes.UnknowError;
            }
            
            var inserted = TopicDal.Insert(Topic, zoneId, zoneIdList, ref newTopicId);
            if (inserted)
            {
                Topic.Id = newTopicId;                
                Topic.DisplayUrl = Topic.DisplayUrl.Replace("{TopicId}", newTopicId.ToString());
                //add redis
                BoCached.Base.Topic.TopicDalFactory.AddTopic(Topic);
                
            }
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.ValidAuthorNameExist;

            //return TopicDal.Insert(Topic, zoneId, ref newTopicId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes Update(TopicEntity Topic, int zoneId, string zoneIdList)
        {

            var inserted = TopicDal.Update(Topic, zoneId, zoneIdList);
            if (inserted)
            {
                BoCached.Base.Topic.TopicDalFactory.AddTopic(Topic);
            }
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            //if (string.IsNullOrEmpty(Topic.TopicName))
            //{
            //    return ErrorMapping.ErrorCodes.UnknowError;
            //}
            //var currentTopic = TopicDal.GetTopicByTopicId(Topic.Id);
            //if (currentTopic == null)
            //{
            //    return ErrorMapping.ErrorCodes.UnknowError;
            //}
            //currentTopic.TopicName = Topic.TopicName;
            //currentTopic.Logo = Topic.Logo;
            //currentTopic.Cover = Topic.Cover;
            //currentTopic.IsActive = Topic.IsActive;
            //currentTopic.IsIconActive = Topic.IsIconActive;

            //return TopicDal.Update(Topic, zoneId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes DeleteById(long TopicId)
        {
            //return TopicDal.DeleteById(TopicId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            var inserted = TopicDal.DeleteById(TopicId);
            if (inserted)
            {
                BoCached.Base.Topic.TopicDalFactory.DeleteTopicById(TopicId);
            }
            return inserted ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateTopicHot(long TopicId, bool isHotTopic)
        {
            return TopicDal.UpdateTopicHot(TopicId, isHotTopic) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateTopicNews(int TopicId, string deleteNewsId, string addNewsId)
        {
            var data = TopicNewsDal.UpdateNews(TopicId, deleteNewsId, addNewsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (data == ErrorMapping.ErrorCodes.Success)
            {
                if (!string.IsNullOrEmpty(deleteNewsId)) {
                    var arrDelIds = deleteNewsId.Split(';');
                    foreach (var item in arrDelIds) {
                        BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(Utility.ConvertToLong(item), 0);
                    }
                }

                if (!string.IsNullOrEmpty(addNewsId))
                {
                    var arrAddIds = addNewsId.Split(';');
                    foreach (var item in arrAddIds)
                    {
                        BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(Utility.ConvertToLong(item), TopicId);
                    }
                }

                var hashName = "NewsInTopic:" + TopicId;
                BoCached.Base.Topic.TopicDalFactory.DeleteDataSearchNewsInTopic(hashName);

                //var newsId = 0L;
                //long.TryParse(string.IsNullOrEmpty(deleteNewsId) ? addNewsId : deleteNewsId, out newsId);
                //var list = TopicDal.GetTopicByNewsId(newsId);
                //if (list != null)
                //{
                //    //add topic to redis
                //    BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(newsId, list.FirstOrDefault());
                //}
            }
            return data;
        }

        public static ErrorMapping.ErrorCodes MultiUpdateTopicNews(string listTopicId, long newsId, int topicId)
        {
            var data = TopicNewsDal.MultiUpdateTopicNews(listTopicId, newsId, topicId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            if (data == ErrorMapping.ErrorCodes.Success)
            {
                //BoCached.Base.Topic.TopicDalFactory.AddTopicByNewsId(newsId, 0);
                //xoa cached newsintopic
                var arrayId = listTopicId.Split(';').ToList();
                if(arrayId!=null && arrayId.Count > 0)
                {
                    foreach(var id in arrayId)
                    {
                        var hashName = "NewsInTopic:" + id;
                        BoCached.Base.Topic.TopicDalFactory.DeleteDataSearchNewsInTopic(hashName);
                    }
                }                
            }
            return data;
        }

        public static ErrorMapping.ErrorCodes UpdateTopicNewsHot(long topicId, string addNewsAvatar, string addNewsId)
        {
            return TopicNewsDal.UpdateNewsHot(topicId, addNewsAvatar, addNewsId) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        public static ErrorMapping.ErrorCodes UpdateTopicNewsRelated(long newsId, string relatedTopics)
        {
            return TopicNewsDal.UpdateTopicNews(newsId, relatedTopics) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
        }
        #endregion

        #region BoxTopicEmbed

        public static List<BoxTopicEmbedEntity> ListBoxTopicEmbed(int topicId, int type)
        {
            var box = new List<BoxTopicEmbedEntity>();
            try
            {

                return TopicDal.ListBoxTopicEmbed(topicId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return box;

        }

        public static ErrorMapping.ErrorCodes InsertBoxTopicEmbed(BoxTopicEmbedEntity box, ref int id)
        {
            try
            {
                if (null != box)
                {
                    return TopicDal.InsertBoxTopicEmbed(box, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.InsertBoxTopicEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes UpdateBoxTopicEmbed(BoxTopicEmbedEntity box)
        {
            try
            {
                if (null != box)
                {
                    var result = TopicDal.UpdateBoxTopicEmbed(box);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.UpdateBoxTopicEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes DeleteBoxTopicEmbed(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = TopicDal.DeleteBoxTopicEmbed(id);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.DeleteBoxTopicEmbed:{0}", ex.Message));
            }

        }

        #endregion

        #region BoxTopicOnPageEmbed

        public static List<BoxTopicOnPageEmbedEntity> ListBoxTopicOnPageEmbed(int zoneId, int type)
        {
            var box = new List<BoxTopicOnPageEmbedEntity>();
            try
            {

                return TopicDal.ListBoxTopicOnPageEmbed(zoneId, type);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return box;

        }

        public static ErrorMapping.ErrorCodes UpdateBoxTopicOnPageEmbed(string listTopicId, int zoneId, int type)
        {
            try
            {
                if (null != listTopicId)
                {
                    TopicDal.UpdateBoxTopicOnPageEmbed(listTopicId, zoneId, type);
                    return ErrorMapping.ErrorCodes.Success;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.UpdateBoxTopicOnPageEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes InsertBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box, ref int id)
        {
            try
            {
                if (null != box)
                {
                    return TopicDal.InsertBoxTopicOnPageEmbed(box, ref id) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.InsertBoxTopicOnPageEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes UpdateBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box)
        {
            try
            {
                if (null != box)
                {
                    var result = TopicDal.UpdateBoxTopicOnPageEmbed(box);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.UpdateBoxTopicOnPageEmbed:{0}", ex.Message));
            }

        }

        public static ErrorMapping.ErrorCodes DeleteBoxTopicOnPageEmbed(int id)
        {
            try
            {
                if (id > 0)
                {
                    var result = TopicDal.DeleteBoxTopicOnPageEmbed(id);
                    return result ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                }
                return ErrorMapping.ErrorCodes.UpdateZoneNotAllow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("TopicDal.DeleteBoxTopicOnPageEmbed:{0}", ex.Message));
            }

        }

        #endregion
    }
}
