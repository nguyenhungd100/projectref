﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.EndUser;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.DAL.Base.EndUser;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BO.Base.EndUser
{
    public class EndUserBo
    {
        public static ErrorMapping.ErrorCodes SaveEndUser(EndUserEntity videoChannelEntity, ref int videoChannelId)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (videoChannelEntity.Id > 0)
                {
                    videoChannelId = videoChannelEntity.Id;
                    return Update(videoChannelEntity);
                }

                return Insert(videoChannelEntity, ref videoChannelId);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Insert(EndUserEntity videoChannelEntity, ref int videoChannelId)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoChannelEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.EndUserTitleCouldNotBeEmpty;
                }

                var success = EndUserDal.Insert(videoChannelEntity, ref videoChannelId);
                if (success)
                {
                    videoChannelEntity.Id = videoChannelId;
                    //insert redis
                    BoCached.Base.EndUser.EndUserDalFactory.AddEndUser(videoChannelEntity);
                    //insert es                          
                    var videoChannelES = new EndUserSearchEntity
                    {
                        Id = videoChannelEntity.Id,    
                        Email= videoChannelEntity.Email,
                        Name = videoChannelEntity.Name,
                        VietId= videoChannelEntity.VietId,
                        Avatar = videoChannelEntity.Avatar,                        
                        Status = videoChannelEntity.Status,
                        CreatedDate=videoChannelEntity.CreatedDate,
                        Sex= videoChannelEntity.Sex
                    };
                    BoSearch.Base.EndUser.EndUserDalFactory.AddEndUser(videoChannelES);

                    //ActivityBo.LogAddNewVideo(videoId, videoEntity.CreatedBy, videoEntity.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes Update(EndUserEntity videoChannelEntity)
        {
            try
            {
                if (null == videoChannelEntity)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                if (string.IsNullOrEmpty(videoChannelEntity.Name))
                {
                    return ErrorMapping.ErrorCodes.EndUserTitleCouldNotBeEmpty;
                }

                var videoChannel = GetEndUserDetail(videoChannelEntity.Id, videoChannelEntity.CreatedBy);
                if (videoChannel == null)
                {
                    return ErrorMapping.ErrorCodes.VideoNotAvailable;
                }

                videoChannel.Id = videoChannelEntity.Id;
                videoChannel.Email = videoChannelEntity.Email;
                videoChannel.Password = videoChannelEntity.Password;                
                videoChannel.Name = videoChannelEntity.Name;
                videoChannel.VietId = videoChannelEntity.VietId;
                videoChannel.FacebookId = videoChannelEntity.FacebookId;
                videoChannel.GoogleId = videoChannelEntity.GoogleId;
                videoChannel.Phone = videoChannelEntity.Phone;
                videoChannel.Avatar = videoChannelEntity.Avatar;
                videoChannel.Status = videoChannelEntity.Status;
                videoChannel.Sex = videoChannelEntity.Sex;
                videoChannel.Birthday = videoChannelEntity.Birthday;
                videoChannel.Address = videoChannelEntity.Address;  
                              
                videoChannel.CreatedBy = videoChannel.CreatedBy;
                videoChannel.CreatedDate = videoChannel.CreatedDate;
                videoChannel.LastModifiedBy = videoChannelEntity.LastModifiedBy;
                videoChannel.LastModifiedDate = videoChannelEntity.LastModifiedDate;                

                var success = EndUserDal.Update(videoChannel);
                if (success)
                {                    
                    //insert redis
                    BoCached.Base.EndUser.EndUserDalFactory.AddEndUser(videoChannelEntity);
                    //insert es                         
                    var videoChannelES = new EndUserSearchEntity
                    {
                        Id = videoChannelEntity.Id,     
                        Email=videoChannelEntity.Email,                   
                        Name = videoChannelEntity.Name,
                        VietId=videoChannelEntity.VietId,
                        Avatar = videoChannelEntity.Avatar,                        
                        Status = videoChannelEntity.Status,
                        CreatedDate = videoChannelEntity.CreatedDate,
                        Sex=videoChannelEntity.Sex                        
                    };
                    BoSearch.Base.EndUser.EndUserDalFactory.UpdateEndUser(videoChannelES);

                    //ActivityBo.LogUploadVideo(video.Id, WcfMessageHeader.Current.ClientUsername, video.Name);
                }
                return success ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static List<EndUserSearchEntity> SearchEndUser(string keyword, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                return BoSearch.Base.EndUser.EndUserDalFactory.SearchEndUser(keyword, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            }
            catch
            {
                return new List<EndUserSearchEntity>();
            }
        }

        public static EndUserEntity GetEndUserDetail(int id, string currentUsername)
        {
            try
            {
                var videoChannel = BoCached.Base.EndUser.EndUserDalFactory.GetEndUserById(id);
                if (videoChannel == null)
                {
                    videoChannel = EndUserDal.GetById(id);
                    if (videoChannel != null)
                    {
                        //add redis
                        BoCached.Base.EndUser.EndUserDalFactory.AddEndUser(videoChannel);
                    }
                }
                return videoChannel;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return null;
            }
        }

        #region follow

        public static ErrorMapping.ErrorCodes UserFollowPlayList(PlayListFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                var result= EndUserDal.UserFollowPlayList(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (result== ErrorMapping.ErrorCodes.Success)
                {
                    BoSearch.Base.PlayList.PlayListDalFactory.UpdateFollowVideoForPlaylist(userFollow.PlayListId,"follow");
                }                
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserFollowZoneVideo(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UserFollowChannel(VideoChannelFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserFollowChannel(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserFollowLabel(VideoLabelFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserFollowLabel(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserFollowPublisher(PublisherFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserFollowPublisher(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region unfollow

        public static ErrorMapping.ErrorCodes UserUnFollowPlayList(PlayListFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }
                var result= EndUserDal.UserUnFollowPlayList(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
                if (result == ErrorMapping.ErrorCodes.Success)
                {
                    BoSearch.Base.PlayList.PlayListDalFactory.UpdateFollowVideoForPlaylist(userFollow.PlayListId, "unfollow");
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserUnFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserUnFollowZoneVideo(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        public static ErrorMapping.ErrorCodes UserUnFollowChannel(VideoChannelFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserUnFollowChannel(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserUnFollowLabel(VideoLabelFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserUnFollowLabel(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }
        public static ErrorMapping.ErrorCodes UserUnFollowPublisher(PublisherFollowEntity userFollow)
        {
            try
            {
                if (null == userFollow)
                {
                    return ErrorMapping.ErrorCodes.InvalidRequest;
                }

                return EndUserDal.UserUnFollowPublisher(userFollow) ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.UnknowError;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return ErrorMapping.ErrorCodes.Exception;
            }
        }

        #endregion

        #region thống kê

        public static List<UserStatisticEntity> GetStatisticStatus(int status, DateTime startDate, DateTime endDate)
        {
            try
            {
                return EndUserDal.GetStatisticStatus(status, startDate, endDate);
            }
            catch
            {
                return new List<UserStatisticEntity>();
            }
        }

        public static object StatictisEndUser(string username, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return BoSearch.Base.EndUser.EndUserDalFactory.StatictisEndUser(username, fromDate, toDate);
            }
            catch
            {
                return new { };
            }
        }

        public static List<PlayListCountEntity> StatictisFollowPlaylist(string username, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return EndUserDal.StatictisFollowPlaylist(username, fromDate, toDate);
            }
            catch
            {
                return new List<PlayListCountEntity>();
            }
        }

        public static List<VideoChannelCountEntity> StatictisFollowVideoChannel(string username, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return EndUserDal.StatictisFollowVideoChannel(username, fromDate, toDate);
            }
            catch
            {
                return new List<VideoChannelCountEntity>();
            }
        }

        #endregion
    }
}
