﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Text;

namespace ChannelVN.CMS.BO.Nodejs
{

    public class VideoApiNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("VideoApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("VideoApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("VideoApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }


        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        public static NodeJs_VideoApiEntity GetVideoInfo(string path)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);

                sb.AppendFormat("&path={0}", path);                
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-file-info";

                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<NodeJs_VideoApiEntity>(strData);

                if (ParseObject == null || (ParseObject != null && ParseObject.Code == 500))
                {
                    Logger.WriteLog(Logger.LogType.Error, "GetVideoInfo=>" + ParseObject.Message);
                    return null;
                }
                return ParseObject;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetVideoInfo=>" + ex.Message);
                return null;
            }
        }
    }

    #region Entity

    public class NodeJs_VideoApiEntity
    {
        [JsonProperty("qualities")]
        public string Qualities { get; set; }

        [JsonProperty("upload_time")]
        public string UploadTime { get; set; }

        [JsonProperty("thumb_url")]
        public string ThumbUrl { get; set; }

        [JsonProperty("video_size")]
        public string VideoSize { get; set; }


        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("video_bitrate")]
        public int VideoBitrate { get; set; }

        [JsonProperty("filename")]
        public string FileName { get; set; }

        [JsonProperty("upload_by")]
        public string UploadBy { get; set; }


        [JsonProperty("audio_frequency")]
        public int AudioFrequency { get; set; }

        [JsonProperty("converted")]
        public bool Converted { get; set; }

        [JsonProperty("audio_bitrate")]
        public int AudioBitrate { get; set; }

        [JsonProperty("fid")]
        public string Fid { get; set; }


        [JsonProperty("md5")]
        public string Md5 { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("video_codec")]
        public string VideoCodec { get; set; }

        [JsonProperty("bitrate")]
        public int Bitrate { get; set; }

        [JsonProperty("video_framerate")]
        public float VideoFramerate { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        //error
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

    }
    #endregion
}
