﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Nodejs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class NewsVersionNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("NewsVersionNodeJsApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("NewsVersionNodeJsApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("NewsVersionNodeJsApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }
        public static List<NewsVersionNodeJsEntity> GetListVersionByNewsId(long NewsId)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", NewsId);
                client.PostData = sb.ToString();
                client.ActionName = "getByNewsId";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListNewsVersion>(strData);

                if (ParseObject == null) ParseObject = new ResponseListNewsVersion();
                Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(ParseObject.data));
                return ParseObject.data == null ? new List<NewsVersionNodeJsEntity>() : ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetListVersionByNewsId=>" + ex.Message);
                return new List<NewsVersionNodeJsEntity>();
            }
        }
        public static List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> ListVersionByNewsId(long NewsId)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", NewsId);
                client.PostData = sb.ToString();
                client.ActionName = "getByNewsId";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListNewsVersionSample>(strData);

                if (ParseObject == null) ParseObject = new ResponseListNewsVersionSample();
                //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(ParseObject.message));
                var MessageObj = ParseObject.data == null ? new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>() : ParseObject.data;
                return ConvertListEntity<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>(MessageObj.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ListVersionByNewsId=>" + ex.Message);
                return new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
            }
        }
        public static NewsVersionNodeJsEntity GetVersionById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "getById";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseNewsVersion>(strData);

                if (ParseObject == null) ParseObject = new ResponseNewsVersion();
                //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(ParseObject.data));
                return ParseObject.data == null ? null : ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetVersionById=>" + ex.Message);
                return null;
            }
        }
        public static List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> GetNewsVersionByNewsIdForUser(long newsId, string lastModifiedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", newsId < 0 ? 0 : newsId);
                sb.AppendFormat("&created_version_by={0}", lastModifiedBy);
                client.PostData = sb.ToString();
                client.ActionName = "getByNewsIdAndCreateVersionBy";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListNewsVersionSample>(strData);

                if (ParseObject == null) ParseObject = new ResponseListNewsVersionSample();
                //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(ParseObject.message));
                var MessageObj = ParseObject.data == null ? new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>() : ParseObject.data;
                return ConvertListEntity<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>(MessageObj.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetListVersionByNewsId=>" + ex.Message);
                return new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
            }
        }
        public static NewsVersionNodeJsEntity GetLastestVersionByNewsId(long newsId, string lastModifiedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", newsId < 0 ? 0 : newsId);
                if(newsId > 0)
                    sb.AppendFormat("&created_version_by={0}", lastModifiedBy);
                client.PostData = sb.ToString();
                client.ActionName = "getByNewsIdAndCreateVersionBy";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListNewsVersion>(strData);

                if (ParseObject == null) ParseObject = new ResponseListNewsVersion();
                //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(ParseObject.data));
                var MessageObj = ParseObject.data == null ? new List<NewsVersionNodeJsEntity>() : ParseObject.data;
                if (MessageObj.Count > 0)
                {
                    return MessageObj.OrderByDescending(k => k.CreatedDateVersion).Take(1).FirstOrDefault();
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetLastestVersionByNewsId=>" + ex.Message);
                return null;
            }
        }
        public static WcfActionResponse AutoSaveNewsVersion(NewsEntity obj, int zoneId, string zoneIdList, string tagIdList, string tagIdListForSubtitle, string newsRelationIdList, string currentUsername)
        {
            try
            {
                if (string.IsNullOrEmpty(zoneIdList))
                    zoneIdList = zoneId.ToString();
                else if (zoneId > 0)
                    zoneIdList = zoneId + ";" + zoneIdList;

                //Logger.WriteLog(Logger.LogType.Trace, "AutoSaveNewsVersion=>" + NewtonJson.Serialize(obj));
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&created_date_version={0}", DateTime.Now);
                sb.AppendFormat("&is_editing={0}", 1);
                sb.AppendFormat("&created_version_by={0}", currentUsername);
                sb.AppendFormat("&news_id={0}", obj.Id);
                sb.AppendFormat("&list_zone_id={0}", zoneIdList);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&sub_title={0}", obj.SubTitle);
                sb.AppendFormat("&sapo={0}", obj.Sapo);
                sb.AppendFormat("&body={0}", HttpUtility.UrlEncode(obj.Body));
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&avatar_desc={0}", obj.AvatarDesc);
                sb.AppendFormat("&avatar2={0}", obj.Avatar2);
                sb.AppendFormat("&avatar3={0}", obj.Avatar3);
                sb.AppendFormat("&avatar4={0}", obj.Avatar4);
                sb.AppendFormat("&avatar5={0}", obj.Avatar5);
                sb.AppendFormat("&author={0}", obj.Author);
                sb.AppendFormat("&news_relation={0}", newsRelationIdList);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&source={0}", obj.Source);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus ? 1 : 0);
                sb.AppendFormat("&type={0}", obj.Type);
                sb.AppendFormat("&news_type={0}", obj.NewsType);
                sb.AppendFormat("&thread_id={0}", obj.ThreadId);
                sb.AppendFormat("&distribution_date={0}", obj.DistributionDate);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&published_by={0}", obj.PublishedBy);
                sb.AppendFormat("&edited_by={0}", obj.EditedBy);
                sb.AppendFormat("&last_receiver={0}", obj.LastReceiver);
                sb.AppendFormat("&word_count={0}", obj.WordCount);
                sb.AppendFormat("&view_count={0}", obj.ViewCount);
                sb.AppendFormat("&priority={0}", obj.Priority);
                sb.AppendFormat("&tag={0}", tagIdList);
                sb.AppendFormat("&note={0}", obj.Note);
                sb.AppendFormat("&display_style={0}", obj.DisplayStyle);
                sb.AppendFormat("&display_position={0}", obj.DisplayPosition);
                sb.AppendFormat("&display_in_slide={0}", obj.DisplayInSlide);
                sb.AppendFormat("&avatar_custom={0}", obj.AvatarCustom);
                sb.AppendFormat("&is_on_home={0}", obj.IsOnHome ? 1 : 0);
                sb.AppendFormat("&original_id={0}", obj.OriginalId);
                sb.AppendFormat("&price={0}", obj.Price);
                sb.AppendFormat("&url={0}", obj.Url);
                sb.AppendFormat("&note_royalties={0}", obj.NoteRoyalties);
                sb.AppendFormat("&tag_item={0}", tagIdList);
                sb.AppendFormat("&news_category={0}", obj.NewsCategory);
                sb.AppendFormat("&init_sapo={0}", obj.InitSapo);
                sb.AppendFormat("&short_title={0}", obj.ShortTitle);
                sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&zone_id={0}", obj.ZoneId);
                client.PostData = sb.ToString();
                //Logger.WriteLog(Logger.LogType.Trace, "AutoSaveNewsVersion:PostData=>" + client.PostData);
                client.ActionName = "save";

                var res = client.MakeRequest();
                if (string.IsNullOrEmpty(res))
                {
                    return new WcfActionResponse();
                }

                Logger.WriteLog(Logger.LogType.Trace, "AutoSaveNewsVersion:Res=>" + res);

                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(res);

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "AutoSaveNewsVersion=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        
        public static WcfActionResponse RemoveNewsVersion(long NewsId, string lastModifiedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", NewsId);
                sb.AppendFormat("&created_version_by={0}", lastModifiedBy);
                client.PostData = sb.ToString();
                client.ActionName = "deleteByNewsId";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<NewsVersionActionResponse>(strData);

                if (ParseObject == null) ParseObject = new NewsVersionActionResponse();
                return ConvertWcfResponse(ParseObject);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RemoveNewsVersion=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RemoveNewsVersionById(string versionId, string accountName)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", versionId);
                
                client.PostData = sb.ToString();
                client.ActionName = "deleteByVersionId";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<NewsVersionActionResponse>(strData);

                Logger.WriteLog(Logger.LogType.Trace, string.Format("RemoveNewsVersionById => accountName: {0}, versionId: {1}, strData: {2}", accountName, versionId, strData));

                if (ParseObject == null) ParseObject = new NewsVersionActionResponse();
                return ConvertWcfResponse(ParseObject);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RemoveNewsVersionById => " + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse ReleaseNewsVersion(long NewsId, string lastModifiedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", NewsId);
                sb.AppendFormat("&modified_by={0}", lastModifiedBy);
                client.PostData = sb.ToString();
                client.ActionName = "releaseVersion";

                Logger.WriteLog(Logger.LogType.Trace, "ReleaseNewsVersion PostData=>" + sb.ToString());

                var strData = client.MakeRequest();

                Logger.WriteLog(Logger.LogType.Trace, "ReleaseNewsVersion strData=>" + strData);

                var ParseObject = JsonConvert.DeserializeObject<NewsVersionActionResponse>(strData);

                if (ParseObject == null) ParseObject = new NewsVersionActionResponse();
                return ConvertWcfResponse(ParseObject);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ReleaseNewsVersion=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse ReleaseFirstNewsVersion(long NewsId, string lastModifiedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&news_id={0}", NewsId);
                sb.AppendFormat("&modified_by={0}", lastModifiedBy);
                client.PostData = sb.ToString();
                client.ActionName = "releaseFirstVersion";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<NewsVersionActionResponse>(strData);

                if (ParseObject == null) ParseObject = new NewsVersionActionResponse();
                return ConvertWcfResponse(ParseObject);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ReleaseFirstNewsVersion=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse ConvertWcfResponse(NewsVersionActionResponse response)
        {
            if (response == null)
                response = new NewsVersionActionResponse();
            return new WcfActionResponse
            {
                Message = response.message,
                Success = response.success,
                ErrorCode = response.code,
            };
        }
        public static T ConvertEntity<T>(object entityObject)
        {
            if (entityObject != null)
            {
                var type = typeof(T);
                var typeProperties = type.GetProperties();

                var typeInput = entityObject.GetType();
                var typeInputProperties = typeInput.GetProperties();

                var newObject = Activator.CreateInstance(typeof(T));

                foreach (var property in typeProperties)
                {
                    var inputPropertyInfo = typeInputProperties.FirstOrDefault(info => info.Name.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase));
                    if (inputPropertyInfo != null)
                    {
                        try
                        {
                            property.SetValue(newObject, inputPropertyInfo.GetValue(entityObject));
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                return (T)newObject;
            }
            return default(T);
        }
        private static List<T> ConvertListEntity<T>(object[] listEntityObject)
        {
            var listNewObject = new List<T>();

            foreach (var entityObject in listEntityObject)
            {
                listNewObject.Add(ConvertEntity<T>(entityObject));
            }
            return listNewObject;
        }
    }

    #region Entity
    //[DataContract]
    //public class NewsVersionActionResponse
    //{
    //    [DataMember]
    //    public bool success { get; set; }
    //    [DataMember]
    //    public int code { get; set; }
    //    [DataMember]
    //    public string message { get; set; }
    //    public NewsVersionActionResponse()
    //    {
    //        success = false;
    //        message = "";
    //        code = 0;
    //    }
    //}
    //public class error
    //{
    //    public string message { get; set; }
    //    public int code { get; set; }
    //}
    //[DataContract]
    //public class ResponseListNewsVersion
    //{
    //    [DataMember]
    //    public bool success { get; set; }
    //    [DataMember]
    //    public int code { get; set; }
    //    [DataMember]
    //    public string message { get; set; }
    //    [DataMember]
    //    public List<NewsVersionNodeJsEntity> data { get; set; }
    //    public ResponseListNewsVersion()
    //    {
    //        success = false;
    //        message = "";
    //        code = 0;
    //        data = new List<NewsVersionNodeJsEntity>();
    //    }
    //}
    //[DataContract]
    //public class ResponseListNewsVersionSample
    //{
    //    [DataMember]
    //    public bool success { get; set; }
    //    [DataMember]
    //    public int code { get; set; }
    //    [DataMember]
    //    public string message { get; set; }
    //    [DataMember]
    //    public List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity> data { get; set; }
    //    public ResponseListNewsVersionSample()
    //    {
    //        success = false;
    //        message = "";
    //        code = 0;
    //        data = new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
    //    }
    //}
    //[DataContract]
    //public class ResponseNewsVersion
    //{
    //    [DataMember]
    //    public bool success { get; set; }
    //    [DataMember]
    //    public int code { get; set; }
    //    [DataMember]
    //    public string message { get; set; }
    //    [DataMember]
    //    public NewsVersionNodeJsEntity data { get; set; }
    //    public ResponseNewsVersion()
    //    {
    //        success = false;
    //        message = "";
    //        code = 0;
    //        data = new NewsVersionNodeJsEntity();
    //    }
    //}

    //[DataContract]
    //public class NewsVersionNodeJsEntity : EntityBase
    //{
    //    [DataMember]
    //    public string Id { get; set; }
    //    [JsonProperty(PropertyName = "version")]
    //    public int Version { get; set; }
    //    [JsonProperty(PropertyName = "created_date_version")]
    //    public DateTime CreatedDateVersion { get; set; }
    //    [JsonProperty(PropertyName = "is_editing")]
    //    public string _IsEditing { get; set; }
    //    public bool IsEditing { get { return _IsEditing == "1" ? true : false; } set { } }

    //    [JsonProperty(PropertyName = "news_id")]
    //    public long NewsId { get; set; }
    //    [DataMember]
    //    public string EncryptId { get { return CryptonForId.EncryptId(NewsId); } set { } }
    //    [DataMember]
    //    public string Title { get; set; }
    //    [JsonProperty("sub_title")]
    //    public string SubTitle { get; set; }
    //    [DataMember]
    //    public string Sapo { get; set; }
    //    [DataMember]
    //    public string Body { get; set; }
    //    [DataMember]
    //    public string Avatar { get; set; }
    //    [JsonProperty("avatar_desc")]
    //    public string AvatarDesc { get; set; }
    //    [DataMember]
    //    public string Avatar2 { get; set; }
    //    [DataMember]
    //    public string Avatar3 { get; set; }
    //    [DataMember]
    //    public string Avatar4 { get; set; }
    //    [DataMember]
    //    public string Avatar5 { get; set; }
    //    [DataMember]
    //    public string Author { get; set; }
    //    [JsonProperty("news_relation")]
    //    public string NewsRelation { get; set; }
    //    [JsonProperty(PropertyName = "status")]
    //    public int Status { get; set; }
    //    [DataMember]
    //    public string Source { get; set; }
    //    [JsonProperty(PropertyName = "is_focus")]
    //    public string _IsFocus { get; set; }
    //    public bool IsFocus { get { return _IsFocus == "1" ? true : false; } set { } }
    //    [JsonProperty(PropertyName = "type")]
    //    public int Type { get; set; }
    //    [JsonProperty(PropertyName = "thread_id")]
    //    public int ThreadId { get; set; }
    //    [JsonProperty("created_date")]
    //    public DateTime CreatedDate { get; set; }
    //    [JsonProperty("modified_date")]
    //    public DateTime LastModifiedDate { get; set; }
    //    [JsonProperty("distribution_date")]
    //    public string _DistributionDate { get; set; }
    //    public DateTime DistributionDate { get { return Utility.ConvertToDateTime(_DistributionDate); } set { } }
    //    [JsonProperty("created_by")]
    //    public string CreatedBy { get; set; }
    //    [JsonProperty("modified_by")]
    //    public string LastModifiedBy { get; set; }
    //    [JsonProperty("published_by")]
    //    public string PublishedBy { get; set; }
    //    [JsonProperty("edited_by")]
    //    public string EditedBy { get; set; }
    //    [JsonProperty("last_receiver")]
    //    public string LastReceiver { get; set; }
    //    [JsonProperty(PropertyName = "word_count")]
    //    public int WordCount { get; set; }
    //    [JsonProperty(PropertyName = "view_count")]
    //    public int ViewCount { get; set; }
    //    [JsonProperty(PropertyName = "priority")]
    //    public int Priority { get; set; }
    //    [JsonProperty("list_zone_id")]
    //    public string ListZoneId { get; set; }
    //    [DataMember]
    //    public string Tag { get; set; }
    //    [DataMember]
    //    public string Note { get; set; }

    //    [JsonProperty(PropertyName = "display_style")]
    //    public int DisplayStyle { get; set; }
    //    [JsonProperty(PropertyName = "display_position")]
    //    public int DisplayPosition { get; set; }
    //    [JsonProperty(PropertyName = "display_in_slide")]
    //    public int DisplayInSlide { get; set; }
    //    [JsonProperty("avatar_custom")]
    //    public string AvatarCustom { get; set; }
    //    [JsonProperty(PropertyName = "original_id")]
    //    public int OriginalId { get; set; }
    //    [JsonProperty(PropertyName = "is_on_home")]
    //    public string _IsOnHome { get; set; }
    //    public bool IsOnHome { get { return _IsOnHome == "1" ? true : false; } set { } }
    //    [DataMember]
    //    public decimal Price { get; set; }

    //    [DataMember]
    //    public string Url { get; set; }
    //    [JsonProperty("note_royalties")]
    //    public string NoteRoyalties { get; set; }
    //    [JsonProperty("tag_item")]
    //    public string TagItem { get; set; }

    //    [JsonProperty(PropertyName = "news_category")]
    //    public int NewsCategory { get; set; }
    //    [JsonProperty("init_sapo")]
    //    public string InitSapo { get; set; }
    //    [JsonProperty("short_title")]
    //    public string ShortTitle { get; set; }
    //    [JsonProperty("interview_id")]
    //    public string InterviewId { get; set; }
    //    [DataMember]
    //    public int ZoneId { get; set; }
    //}
    //[DataContract]
    //public class NewsVersionWithSimpleFieldsNodejsForParseJsonEntity : EntityBase
    //{
    //    [DataMember]
    //    public string Id { get; set; }
    //    [JsonProperty(PropertyName = "version")]
    //    public int Version { get; set; }
    //    [JsonProperty(PropertyName = "created_date_version")]
    //    public DateTime CreatedDateVersion { get; set; }
    //    [JsonProperty(PropertyName = "is_editing")]
    //    public string _IsEditing { get; set; }
    //    public bool IsEditing { get { return _IsEditing == "1" ? true : false; } set { } }

    //    [JsonProperty(PropertyName = "news_id")]
    //    public long NewsId { get; set; }
    //}
    //public class NewsVersionWithSimpleFieldsNodejsEntity : EntityBase
    //{
    //    [DataMember]
    //    public string Id { get; set; }
    //    [DataMember]
    //    public int Version { get; set; }
    //    [DataMember]
    //    public DateTime CreatedDateVersion { get; set; }
    //    [DataMember]
    //    public bool IsEditing { get; set; }
    //    [DataMember]
    //    public long NewsId { get; set; }
    //}
    #endregion
}
