﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{

    public class RollingNewsNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("RollingNewsApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("RollingNewsApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("RollingNewsApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }


        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        #region RollingNews
        public static List<NodeJs_RollingNewsEntity> RollingNews_Search(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);

                var fDate = fromDate.ToString(DateTimeFormat);
                var tDate = toDate.ToString(DateTimeFormat);
                if (fDate != "0001/01/01 00:00")
                    sb.AppendFormat("&created_date_from={0}", fDate);
                if (tDate != "0001/01/01 00:00")
                    sb.AppendFormat("&created_date_to={0}", tDate);
                //sb.AppendFormat("&created_date_from={0}", fromDate.ToString(DateTimeFormat));
                //sb.AppendFormat("&created_date_to={0}", toDate.ToString(DateTimeFormat));
                sb.AppendFormat("&page_index={0}", pageIndex);
                sb.AppendFormat("&page_size={0}", pageSize);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "search";
                var strData = client.MakeRequest();
                strData = strData.Replace("is_show_rollingnews_label", "show_label");
                var ParseObject = JsonConvert.DeserializeObject<ResponseListRollingNews>(strData);

                if (ParseObject == null) ParseObject = new ResponseListRollingNews();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Trace, "RollingNews_Search=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_RollingNewsEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Search=>" + ex.Message);
                return new List<NodeJs_RollingNewsEntity>();
            }
        }
        public static NodeJs_RollingNewsEntity RollingNews_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-id";
                var strData = client.MakeRequest();
                strData = strData.Replace("is_show_rollingnews_label", "show_label");
                var ParseObject = JsonConvert.DeserializeObject<ResponseRollingNews>(strData);

                if (ParseObject == null) ParseObject = new ResponseRollingNews();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Trace, "RollingNews_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_GetById=>" + ex.Message);
                return null;
            }
        }
        public static WcfActionResponse RollingNews_Insert(NodeJs_RollingNews obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&show_time={0}", obj.ShowTime ? 1 : 0);
                sb.AppendFormat("&show_author={0}", obj.ShowAuthor ? 1 : 0);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&start_date={0}", obj.StartDate);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&show_label={0}", obj.IsShowRollingNewsLabel ? 1 : 0);
                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Update(NodeJs_RollingNews obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&show_time={0}", obj.ShowTime ? 1 : 0);
                sb.AppendFormat("&show_author={0}", obj.ShowAuthor ? 1 : 0);
                sb.AppendFormat("&modified_by={0}", obj.LastModifiedBy);
                sb.AppendFormat("&start_date={0}", obj.StartDate);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&show_label={0}", obj.IsShowRollingNewsLabel ? 1 : 0);
                client.PostData = sb.ToString();
                client.ActionName = "update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        #endregion

        #region RollingNews_Event
        public static NodeJs_RollingNewsEventEntity RollingNewsItem_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "event/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseRollingNewsItem>(strData);

                if (ParseObject == null) ParseObject = new ResponseRollingNewsItem();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "RollingNewsItem_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNewsItem_GetById=>" + ex.Message);
                return new NodeJs_RollingNewsEventEntity();
            }
        }
        public static List<NodeJs_RollingNewsEventEntity> RollingNewsItem_GetListByRollingNewsId(string RollingNewsId)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&rollingnews_id={0}", RollingNewsId);
                client.PostData = sb.ToString();
                client.ActionName = "event/get-by-rollingnews-id";
                var strData = client.MakeRequest();
                //Logger.WriteLog(Logger.LogType.Debug, strData);
                var ParseObject = JsonConvert.DeserializeObject<ResponseListRollingNewsItem>(strData);

                if (ParseObject == null) ParseObject = new ResponseListRollingNewsItem();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "RollingNewsItem_GetListByRollingNewsId=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_RollingNewsEventEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNewsItem_GetListByRollingNewsId=>" + ex.Message);
                return new List<NodeJs_RollingNewsEventEntity>();
            }
        }
        public static WcfActionResponse RollingNews_Event_Insert(NodeJs_RollingNewsEventEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&rollingnews_id={0}", obj.RollingNewsId);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&event_content={0}", HttpUtility.UrlEncode(obj.EventContent));
                sb.AppendFormat("&event_note={0}", obj.EventNote);
                sb.AppendFormat("&event_time={0}", obj.EventTime);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                //sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                //sb.AppendFormat("&published_by={0}", obj.PublishedBy);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                client.PostData = sb.ToString();
                client.ActionName = "event/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_Update(NodeJs_RollingNewsEventEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&rollingnews_id={0}", obj.RollingNewsId);
                sb.AppendFormat("&event_type={0}", obj.EventType);
                sb.AppendFormat("&event_content={0}", HttpUtility.UrlEncode(obj.EventContent));
                sb.AppendFormat("&event_note={0}", obj.EventNote);
                sb.AppendFormat("&event_time={0}", obj.EventTime);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&modified_by={0}", obj.LastModifiedBy);
                //sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                //sb.AppendFormat("&published_by={0}", obj.PublishedBy);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus);
                client.PostData = sb.ToString();
                client.ActionName = "event/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_Publish(string Id, string RollingNewsId, string PublishedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&rollingnews_id={0}", RollingNewsId);
                sb.AppendFormat("&status={0}", 1);
                sb.AppendFormat("&published_date={0}", DateTime.Now.ToString(DateTimeFormat));
                sb.AppendFormat("&published_by={0}", PublishedBy);
                client.PostData = sb.ToString();
                client.ActionName = "event/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_UnPublish(string Id, string RollingNewsId, string modified_by)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&rollingnews_id={0}", RollingNewsId);
                sb.AppendFormat("&status={0}", 2);
                sb.AppendFormat("&modified_by={0}", modified_by);
                client.PostData = sb.ToString();
                client.ActionName = "event/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_Delete(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "event/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());
                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Delete=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_Delete_By_RollingNewsId(string RollingNewsId)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&rollingnews_id={0}", RollingNewsId);
                client.PostData = sb.ToString();
                client.ActionName = "event/remove-by-rollingnews-id";                
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.NodejsActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponseByObj(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Delete_By_RollingNewsId=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse RollingNews_Event_Delete_By_ListIds(string Ids)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&ids={0}", Ids);
                client.PostData = sb.ToString();
                client.ActionName = "event/remove-by-list-id";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.NodejsActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponseByObj(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "RollingNews_Event_Delete_By_ListIds=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        #endregion
    }

    #region Entity
    public enum RollingNewsIsFocus : int
    {
        All = -1,
        IsFocus = 1,
        NotIsFocus = 0,
    }

    public enum RollingNewsStatus : int
    {

        All = -1,
        Temporary = 0,
        Published = 1,
        Unpublished = 2,
    }

    [DataContract]
    public class ResponseListRollingNews
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_RollingNewsEntity> Data { get; set; }
        public ResponseListRollingNews()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_RollingNewsEntity>();
        }
    }
    [DataContract]
    public class ResponseRollingNews
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_RollingNewsEntity Data { get; set; }
        public ResponseRollingNews()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    [DataContract]
    public class ResponseRollingNewsItem
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_RollingNewsEventEntity Data { get; set; }
        public ResponseRollingNewsItem()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    [DataContract]
    public class ResponseListRollingNewsItem
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_RollingNewsEventEntity> Data { get; set; }
        public ResponseListRollingNewsItem()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_RollingNewsEventEntity>();
        }
    }

    [DataContract]
    public class NodeJs_RollingNewsEventEntity
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("rollingnews_id")]
        public string RollingNewsId { get; set; }
        [JsonProperty("event_type")]
        public int EventType { get; set; }
        [JsonProperty("event_content")]
        public string EventContent { get; set; }
        [JsonProperty("event_note")]
        public string EventNote { get; set; }
        [JsonProperty("event_time")]
        public DateTime EventTime { get; set; }
        [JsonProperty("is_focus")]
        //public int _IsFocus { get; set; }
        //public bool IsFocus { get { return _IsFocus == 1 ? true : false; } set { } }
        public int IsFocus { get; set; }
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("published_by")]
        public string PublishedBy { get; set; }
        [JsonProperty("published_date")]
        public DateTime PublishedDate { get; set; }
        [JsonProperty("modified_by")]
        public string LastModifiedBy { get; set; }
        [JsonProperty("modified_date")]
        public DateTime LastModifiedDate { get; set; }
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("show_time")]
        public string MobileContent { get; set; }
        public string ShortContent { get; set; }
        public string Images { get; set; }
        public int ImageCount { get; set; }
    }
    public class RollingNewsItemMappingEntity
    {
        public string Id { get; set; }
        public string RollingNewsId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
        public int IsHighlight { get; set; }
        public string TabId { get; set; }
        public int IsFocus { get; set; }
        public string Avatar { get; set; }
        public int IsHot { get; set; }
        public bool IsRemove { get; set; }
    }
    public class NodeJs_RollingNewsEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [DataMember]
        [JsonProperty("show_time")]
        //public string _ShowTime { get; set; }
        //public bool ShowTime { get { return _ShowTime == "1" ? true : false; } set { } }
        public int ShowTime { get; set; }

        [DataMember]
        [JsonProperty("show_author")]
        //public string _ShowAuthor { get; set; }
        //public bool ShowAuthor { get { return _ShowAuthor == "1" ? true : false; } set { } }
        public int ShowAuthor { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("description")]
        public string PublishedContent { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("modified_by")]
        public string LastModifiedBy { get; set; }

        [JsonProperty("modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [DataMember]
        [JsonProperty("show_label")]
        //public string _IsShowRollingNewsLabel { get; set; }
        //public bool IsShowRollingNewsLabel { get { return _IsShowRollingNewsLabel == "1" ? true : false; } set { } }
        public int IsShowRollingNewsLabel { get; set; }

        //[DataMember]
        //[JsonProperty("news_id")]
        //public long NewsId { get; set; }

        //[DataMember]
        //[JsonProperty("news_title")]
        //public string NewsTitle { get; set; }
    }
    public class NodeJs_RollingNews
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool ShowTime { get; set; }
        public bool ShowAuthor { get; set; }
        public DateTime StartDate { get; set; }
        public string PublishedContent { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public int Status { get; set; }
        public bool IsShowRollingNewsLabel { get; set; }

    }
    #endregion
}
