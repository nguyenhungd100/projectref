﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Nodejs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{

    public class NewsStoresNodeJsServices
    {
        private const string DateTimeFormat = "yyyy-MM-dd HH:mm";

        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("NewsStoresApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = "",//CmsChannelConfiguration.GetAppSetting("NewsStoresApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("NewsStoresApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }        

        public static NodeJs_ChannelsEntity ListChannel()
        {
            try
            {
                var allow = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting("NewsStoresApiEnabled"));
                if (allow)
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();                    
                    
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    client.PostData = sb.ToString();
                    client.ActionName = "channels";

                    var strData = client.MakeRequest();
                    var ParseObject = JsonConvert.DeserializeObject<NodeJs_ChannelsEntity>(strData);

                    if (ParseObject == null || (ParseObject != null && ParseObject.Code == 500))
                    {
                        Logger.WriteLog(Logger.LogType.Error, "ListChannel=>" + ParseObject.Message);
                        return null;
                    }
                    return ParseObject;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ListChannel=>" + ex.Message);
                return null;
            }
        }

        public static NodeJs_NewsResponseEntity SearchNewsStore(string keyword, int channelId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize)
        {
            try
            {
                var allow = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting("NewsStoresApiEnabled"));
                if (allow)
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("key_word={0}", keyword);
                    if(channelId>0)
                        sb.AppendFormat("&channel_id={0}", channelId);
                    if(fromDate!=DateTime.MinValue)
                        sb.AppendFormat("&from_date={0}", fromDate.ToString(DateTimeFormat));
                    if (toDate != DateTime.MinValue)
                        sb.AppendFormat("&to_date={0}", toDate.ToString(DateTimeFormat));
                    sb.AppendFormat("&page_index={0}", pageIndex);
                    sb.AppendFormat("&page_size={0}", pageSize);

                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    client.PostData = sb.ToString();
                    client.ActionName = "news/by-keyword";

                    var strData = client.MakeRequest();
                    var ParseObject = JsonConvert.DeserializeObject<NodeJs_NewsResponseEntity>(strData);

                    if (ParseObject == null || (ParseObject != null && ParseObject.Code == 500))
                    {
                        Logger.WriteLog(Logger.LogType.Error, "SearchNewsStore=>" + ParseObject.Message);
                        return null;
                    }
                    return ParseObject;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SearchNewsStore=>" + ex.Message);
                return null;
            }
        }
    }

    #region Entity

    public class NodeJs_NewsResponseEntity
    {        
        public int TotalRows { get; set; }
       
        public List<NodeJs_NewsStoresEntity> News { get; set; }

        //error
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class NodeJs_NewsStoresEntity
    {        
        public int Id { get; set; }
       
        public string Author { get; set; }
        
        public string Title { get; set; }
        
        public string Sapo { get; set; }
        
        public string Url { get; set; }
        
        public string Image { get; set; }
                
        public string PublishDate { get; set; }
        
        public string DESC { get; set; }
        
        public int ContentType { get; set; }
                
        public bool ZoneId { get; set; }
        
        public string ZoneName { get; set; }
        
        public List<NodeJs_TagsEntity> Tags { get; set; }
        
        public string TopicName { get; set; }
        
        public string NewsId { get; set; }
        
        public string RowNum { get; set; }

        public string ChannelName { get; set; }
    }

    public class NodeJs_TagsEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
    }

    public class NodeJs_ChannelsEntity
    {        
        public List<NodeJs_ChannelEntity> Channels { get; set; }

        //error
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class NodeJs_ChannelEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }        
    }
    #endregion
}
