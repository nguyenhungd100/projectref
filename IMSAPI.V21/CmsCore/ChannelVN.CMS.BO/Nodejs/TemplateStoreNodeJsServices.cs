﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class TemplateStoreNodeJsServices
    {
        const string DateTimeFormat = "yyyy-MM-dd HH:mm";

        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("TemplateStoreApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("TemplateStoreApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("TemplateStoreApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }

        public static ResponseTemplateStore<NodeJs_TemplateStoreCategoryEntity> Categories()
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);

                    //client.PostData = sb.ToString();
                    client.ActionName = "categories?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStore<NodeJs_TemplateStoreCategoryEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStore<NodeJs_TemplateStoreCategoryEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "Categories=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStore<NodeJs_TemplateStoreCategoryEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Categories=>" + ex.Message);
                return new ResponseTemplateStore<NodeJs_TemplateStoreCategoryEntity>();
            }
        }

        public static ResponseTemplateStore<NodeJs_TemplateStoreTypeEntity> Types(string category_id="-1")
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    sb.AppendFormat("&category_id={0}", category_id);

                    //client.PostData = sb.ToString();
                    client.ActionName = "types?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStore<NodeJs_TemplateStoreTypeEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStore<NodeJs_TemplateStoreTypeEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "Types=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStore<NodeJs_TemplateStoreTypeEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Types=>" + ex.Message);
                return new ResponseTemplateStore<NodeJs_TemplateStoreTypeEntity>();
            }
        }

        public static ResponseTemplateStore<NodeJs_TemplateStorePageEntity> Pages(string keyword, string category_id, int type_id, DateTime from, DateTime to, int page, int size)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    sb.AppendFormat("&keyword={0}", keyword);
                    sb.AppendFormat("&category_id={0}", category_id);
                    if (type_id > -1)
                        sb.AppendFormat("&type_id={0}", type_id);

                    var fDate = from.ToString(DateTimeFormat);
                    var tDate = to.ToString(DateTimeFormat);
                    if (fDate != "0001-01-01 00:00")
                        sb.AppendFormat("&from={0}", fDate);
                    if (tDate != "0001-01-01 00:00")
                        sb.AppendFormat("&to={0}", tDate);

                    sb.AppendFormat("&page={0}", page);
                    sb.AppendFormat("&size={0}", size);

                    //client.PostData = sb.ToString();
                    client.ActionName = "pages?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStore<NodeJs_TemplateStorePageEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStore<NodeJs_TemplateStorePageEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "Pages=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStore<NodeJs_TemplateStorePageEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Pages=>" + ex.Message);
                return new ResponseTemplateStore<NodeJs_TemplateStorePageEntity>();
            }
        }
        public static ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity> GetPageById(string id)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    //sb.AppendFormat("&id={0}", id);                    

                    //client.PostData = sb.ToString();
                    client.ActionName = string.Format("page/{0}?{1}", id, sb.ToString());
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "GetPageById=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetPageById=>" + ex.Message);
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
        }

        public static ResponseTemplateStore<NodeJs_TemplateStoreElementEntity> Elements(string keyword, string category_id, int type_id, DateTime from, DateTime to, int page, int size)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    sb.AppendFormat("&keyword={0}", keyword);
                    sb.AppendFormat("&category_id={0}", category_id);
                    if (type_id > -1)
                        sb.AppendFormat("&type_id={0}", type_id);

                    var fDate = from.ToString(DateTimeFormat);
                    var tDate = to.ToString(DateTimeFormat);
                    if (fDate != "0001-01-01 00:00")
                        sb.AppendFormat("&from={0}", fDate);
                    if (tDate != "0001-01-01 00:00")
                        sb.AppendFormat("&to={0}", tDate);

                    sb.AppendFormat("&page={0}", page);
                    sb.AppendFormat("&size={0}", size);

                    //client.PostData = sb.ToString();
                    client.ActionName = "elements?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "Elements=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Elements=>" + ex.Message);
                return new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
            }
        }
        public static ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity> GetElementById(string id)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    //sb.AppendFormat("&id={0}", id);                    

                    //client.PostData = sb.ToString();
                    client.ActionName = string.Format("element/{0}?{1}",id,sb.ToString());
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "GetElementById=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetElementById=>" + ex.Message);
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
        }

        public static ResponseTemplateStore<NodeJs_TemplateStoreElementEntity> Popups(string keyword, string category_id, int type_id, DateTime from, DateTime to, int page, int size)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    sb.AppendFormat("&keyword={0}", keyword);
                    sb.AppendFormat("&category_id={0}", category_id);
                    if(type_id>-1)
                        sb.AppendFormat("&type_id={0}", type_id);

                    var fDate = from.ToString(DateTimeFormat);
                    var tDate = to.ToString(DateTimeFormat);
                    if (fDate != "0001-01-01 00:00")
                        sb.AppendFormat("&from={0}", fDate);
                    if (tDate != "0001-01-01 00:00")
                        sb.AppendFormat("&to={0}", tDate);

                    sb.AppendFormat("&page={0}", page);
                    sb.AppendFormat("&size={0}", size);

                    //client.PostData = sb.ToString();
                    client.ActionName = "popups?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "Popups=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Popups=>" + ex.Message);
                return new ResponseTemplateStore<NodeJs_TemplateStoreElementEntity>();
            }
        }
        public static ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity> GetPopupById(string id)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("TemplateStoreEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    sb.AppendFormat("channel_id={0}", client.Channel_Id);
                    sb.AppendFormat("&secret_key={0}", client.SecretKey);
                    //sb.AppendFormat("&id={0}", id);                    

                    //client.PostData = sb.ToString();
                    client.ActionName = string.Format("popup/{0}?{1}", id, sb.ToString());
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>>(strData);

                    if (ParseObject == null) ParseObject = new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
                    if (!ParseObject.Success)
                        Logger.WriteLog(Logger.LogType.Trace, "GetPopupById=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetPopupById=>" + ex.Message);
                return new ResponseTemplateStoreDetail<NodeJs_TemplateStoreDetailEntity>();
            }
        }
    }      

    public class ResponseTemplateStore<T>
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public List<T> Data { get; set; }
        public ResponseTemplateStore()
        {
            Success = false;
            ErrorCode = 0;
            Message = "";
            Data = new List<T>();
        }
    }

    public class ResponseTemplateStoreDetail<T>
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public ResponseTemplateStoreDetail()
        {
            Success = false;
            ErrorCode = 0;
            Message = "";
            Data = default(T);
        }
    }

    public class NodeJs_TemplateStoreCategoryEntity
    {        
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }
        
        [JsonProperty("order")]        
        public int Order { get; set; }
        
        [JsonProperty("created_date")]        
        public string CreatedDate { get; set; }

        [JsonProperty("pid")]
        public string PId { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }        
    }

    public class NodeJs_TemplateStoreTypeEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }                

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }        

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("order")]
        public int Order { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }
    }

    public class NodeJs_TemplateStoreElementEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type_id")]
        public string TypeId { get; set; }

        [JsonProperty("type_name")]
        public string TypeName { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }
    }

    public class NodeJs_TemplateStorePageEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_date")]
        public string CreatedDate { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("type_id")]
        public string TypeId { get; set; }

        [JsonProperty("type_name")]
        public string TypeName { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        [JsonProperty("category_name")]
        public string CategoryName { get; set; }
    }

    public class NodeJs_TemplateStoreDetailEntity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        //[JsonProperty("created_by")]
        //public string CreatedBy { get; set; }

        //[JsonProperty("created_date")]
        //public string CreatedDate { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        //[JsonProperty("title")]
        //public string Title { get; set; }

        [JsonProperty("type_id")]
        public string TypeId { get; set; }

        //[JsonProperty("type_name")]
        //public string TypeName { get; set; }

        [JsonProperty("category_id")]
        public string CategoryId { get; set; }

        //[JsonProperty("category_name")]
        //public string CategoryName { get; set; }

        [JsonProperty("temp_content")]
        public string TempContent { get; set; }

        [JsonProperty("temp_css")]
        public string TempCss { get; set; }

        [JsonProperty("temp_css_mobile")]
        public string TempCssMobile { get; set; }
    }
}
