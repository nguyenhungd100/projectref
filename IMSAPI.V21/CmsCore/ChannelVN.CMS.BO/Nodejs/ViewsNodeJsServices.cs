﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Nodejs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{

    public class ViewsNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("ViewsApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("ViewsApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("ViewsApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }


        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        public static List<NodeJs_ViewsEntity> SortViews(int pageIndex, int pageSize, string newsIds, string sort, int skipSort, long viewFrom, long viewTo, ref int totalRow)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);

                sb.AppendFormat("&page={0}", pageIndex);
                sb.AppendFormat("&size={0}", pageSize);
                sb.AppendFormat("&news_ids={0}", newsIds);
                sb.AppendFormat("&sort={0}", sort);
                sb.AppendFormat("&skip_sort={0}", skipSort);
                sb.AppendFormat("&view_from={0}", viewFrom);
                sb.AppendFormat("&view_to={0}", viewTo);
                sb.AppendFormat("&with_total_row={0}", 1);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "sort-views";

                var strData = client.MakeRequest();

                if (strData.Contains("\"news_id\":\"\""))
                {
                    strData = strData.Replace("\"news_id\":\"\"", "\"news_id\":0");

                }
                var ParseObject = JsonConvert.DeserializeObject<ResponseListViews>(strData);

                ParseObject.Data.data = ParseObject.Data.data.Where(c => c.NewsId > 0).ToList();

                if (ParseObject == null) ParseObject = new ResponseListViews();
                totalRow = ParseObject.Data.total_row;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Debug, "SortViews=>" + ParseObject.Message);

                return ParseObject.Data.data == null ? new List<NodeJs_ViewsEntity>() : ParseObject.Data.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SortViews=>" + ex.Message);
                return new List<NodeJs_ViewsEntity>();
            }
        }
    }

    #region Entity   

    [DataContract]
    public class ResponseListViews
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public ResponseListViewsData Data { get; set; }
        public ResponseListViews()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new ResponseListViewsData();
        }
    }

    public class ResponseListViewsData
    {
        [DataMember]
        public int total_row { get; set; }
        [DataMember]
        public List<NodeJs_ViewsEntity> data { get; set; }
    }

    public class NodeJs_ViewsEntity
    {
        [JsonProperty("news_id")]
        public long NewsId { get; set; }

        [JsonProperty("total_view")]
        public long TotalViews { get; set; }

        [JsonProperty("view_pc")]
        public long ViewPc { get; set; }

        [JsonProperty("view_mob")]
        public long ViewMobile { get; set; }

    }
    #endregion
}
