﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class NewsRoyaltyNodeJsServices
    {
        const string DateTimeFormat = "yyyy-MM-dd HH:mm";

        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("NewsRoyaltyApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = "",//CmsChannelConfiguration.GetAppSetting("NewsRoyaltyApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("NewsRoyaltyApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }

        public static ResponseNewsRoyalty NewsRoyalty_Search(int zoneId, string createdBy, string penName, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                if (CmsChannelConfiguration.GetAppSettingInBoolean("NewsRoyaltyEnabled"))
                {
                    var client = GetRestClient();
                    var sb = new StringBuilder();
                    var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                    string sort_order = "DESC";
                    //sb.AppendFormat("channel_id={0}", client.Channel_Id);

                    var fDate = fromDate.ToString(DateTimeFormat);
                    var tDate = toDate.ToString(DateTimeFormat);
                    if (fDate != "0001-01-01 00:00")
                        sb.AppendFormat("&from_date={0}", fDate);
                    if (tDate != "0001-01-01 00:00")
                        sb.AppendFormat("&to_date={0}", tDate);
                    sb.AppendFormat("&page_number={0}", pageIndex);
                    sb.AppendFormat("&page_size={0}", pageSize);
                    if(zoneId>0)
                        sb.AppendFormat("&zone={0}", zoneId);

                    sb.AppendFormat("&createdby={0}", createdBy);
                    sb.AppendFormat("&penname={0}", penName);
                    if(nameSpace.Equals("AFamily") || nameSpace.Equals("Sport5"))
                    {
                        sort_order = "ASC";
                    }
                    sb.AppendFormat("&sort_order={0}", sort_order);

                    //client.PostData = sb.ToString();
                    client.ActionName = "?" + sb.ToString();
                    client.Token = client.SecretKey;
                    client.Method = NodeJsRestClient.HttpMethod.GET;

                    var strData = client.MakeRequest();

                    var ParseObject = JsonConvert.DeserializeObject<ResponseNewsRoyalty>(strData);

                    if (ParseObject == null) ParseObject = new ResponseNewsRoyalty();
                    if (ParseObject.Status != 1)
                        Logger.WriteLog(Logger.LogType.Trace, "NewsRoyalty_Search=>" + ParseObject.Message);

                    return ParseObject;
                }
                return new ResponseNewsRoyalty();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewsRoyalty_Search=>" + ex.Message);
                return new ResponseNewsRoyalty();
            }
        }
    }
    
    public class ResponseNewsRoyalty
    {
        [JsonProperty("status")]
        public int Status { get; set; }
        [JsonProperty("message")]
        public ResponseListNewsRoyalty Message { get; set; }
        public ResponseNewsRoyalty()
        {
            Status = 0;
            Message = new ResponseListNewsRoyalty();
        }
    }

    public class ResponseListNewsRoyalty
    {
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }        
        [JsonProperty("data")]
        public List<NodeJs_NewsRoyaltyEntity> Data { get; set; }
        public ResponseListNewsRoyalty()
        {
            TotalCount = 0;            
            Data = new List<NodeJs_NewsRoyaltyEntity>();
        }
    }

    public class NodeJs_NewsRoyaltyEntity
    {
        [JsonProperty("STT")]
        public string STT { get; set; }

        [JsonProperty("chuyen_muc")]
        public string chuyen_muc { get; set; }

        [JsonProperty("news_id")]
        public string NewsId { get; set; }

        [JsonProperty("ngay_dang")]
        public DateTime ngay_dang { get; set; }

        [JsonProperty("tieu_de")]
        public string tieu_de { get; set; }
        
        [JsonProperty("url")]        
        public string url { get; set; }
        
        [JsonProperty("zone")]        
        public int zone { get; set; }

        [JsonProperty("word_count")]
        public int word_count { get; set; }

        [JsonProperty("viewed_count")]
        public int viewed_count { get; set; }

        [JsonProperty("viewed_mobile_count")]
        public int viewed_mobile_count { get; set; }

        [JsonProperty("created_by")]
        public string created_by { get; set; }

        [JsonProperty("trang_thai_gui_nhuan")]
        public int trang_thai_gui_nhuan { get; set; }

        [JsonProperty("pen_name")]
        public string pen_name { get; set; }

        [JsonProperty("mns")]
        public string mns { get; set; }

        [JsonProperty("loai_bai")]
        public int loai_bai { get; set; }

        [JsonProperty("text_dong_gop")]
        public int text_dong_gop { get; set; }

        [JsonProperty("loai_cong_viec")]
        public string loai_cong_viec { get; set; }

        [JsonProperty("so_luong_anh")]
        public int so_luong_anh { get; set; }

        [JsonProperty("so_luong_clip_suu_tam")]
        public int so_luong_clip_suu_tam { get; set; }

        [JsonProperty("so_luong_clip_san_xuat")]
        public int so_luong_clip_san_xuat { get; set; }

        [JsonProperty("url_clip_san_xuat")]
        public string url_clip_san_xuat { get; set; }

        [JsonProperty("url_clip_suu_tam")]
        public string url_clip_suu_tam { get; set; }

        [JsonProperty("nguon")]
        public bool nguon { get; set; }

        [JsonProperty("ghi_chu")]
        public string ghi_chu { get; set; }
    }
}
