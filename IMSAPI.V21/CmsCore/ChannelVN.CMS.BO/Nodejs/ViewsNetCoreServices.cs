﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.BO.Video;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Statistic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class ViewsNetCoreServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("ViewsNetCoreApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                SecretKey = CmsChannelConfiguration.GetAppSetting("ViewsNetCoreSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }


        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        public static List<StatisticV3ViewCountEntity> PageViewAllSiteGetData(int type, DateTime dateFrom, DateTime dateTo, string channelName)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();

                sb.AppendFormat("Channel={0}", channelName);
                sb.AppendFormat("&Type={0}", type);
                sb.AppendFormat("&FromDate={0}", dateFrom.ToString("yyyy-MM-dd"));
                sb.AppendFormat("&ToDate={0}", dateTo.ToString("yyyy-MM-dd"));
                sb.AppendFormat("&SecretKey={0}", client.SecretKey??"");

                client.PostData = sb.ToString();
                client.ActionName = "view/get_all";
                client.Timeout = 7 * 1000;

                //Logger.WriteLog(Logger.LogType.Trace, "PageViewAllSiteGetData=> " + client.PostData);

                var strData = client.MakeRequest();

                //Logger.WriteLog(Logger.LogType.Trace, "PageViewAllSiteGetData strData=> " + strData);

                var ParseObject = JsonConvert.DeserializeObject<ResponseDataViewsAll>(strData);

                if (ParseObject == null || (ParseObject != null && !ParseObject.success))
                {
                    Logger.WriteLog(Logger.LogType.Error, "PageViewAllSiteGetData strData=> " + strData);
                    return new List<StatisticV3ViewCountEntity>();
                }                                    
                return ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "PageViewAllSiteGetData=>" + ex.Message);
                return new List<StatisticV3ViewCountEntity>();
            }
        }

        public static List<StatisticV3CategoryEntity> PageViewByZoneGetData(int type, DateTime dateFrom, DateTime dateTo, string zoneIds, string channelName)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();

                sb.AppendFormat("Channel={0}", channelName);
                sb.AppendFormat("&Zones={0}", zoneIds);
                sb.AppendFormat("&Type={0}", type);
                sb.AppendFormat("&FromDate={0}", dateFrom.ToString("yyyy-MM-dd"));
                sb.AppendFormat("&ToDate={0}", dateTo.ToString("yyyy-MM-dd"));
                sb.AppendFormat("&SecretKey={0}", client.SecretKey ?? "");

                client.PostData = sb.ToString();
                client.ActionName = "view/get_by_list_zone";

                //Logger.WriteLog(Logger.LogType.Trace, "PageViewByZoneGetData=> " + client.PostData);

                var strData = client.MakeRequest();

                //Logger.WriteLog(Logger.LogType.Trace, "PageViewByZoneGetData strData=> " + strData);

                var ParseObject = JsonConvert.DeserializeObject<ResponseDataViewsZone>(strData);

                if (ParseObject == null || (ParseObject != null && !ParseObject.success))
                {
                    Logger.WriteLog(Logger.LogType.Error, "PageViewByZoneGetData strData=> " + strData);
                    return new List<StatisticV3CategoryEntity>();
                }

                return ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "PageViewByZoneGetData=>" + ex.Message);
                return new List<StatisticV3CategoryEntity>();
            }
        }

        public static ViewVideo GetViewVideo(string keys, DateTime dtFromDate, DateTime dtToDate, string channelName)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();

                sb.AppendFormat("channel={0}", channelName);
                sb.AppendFormat("&keys={0}", keys);
                //sb.AppendFormat("&FromDate={0}", dateFrom.ToString("yyyy-MM-dd"));
                //sb.AppendFormat("&ToDate={0}", dateTo.ToString("yyyy-MM-dd"));
                sb.AppendFormat("&SecretKey={0}", client.SecretKey ?? "");

                client.PostData = sb.ToString();
                client.ActionName = "videoview/get_all";
                client.Timeout = 5*1000;

                //Logger.WriteLog(Logger.LogType.Trace, "GetViewVideo=> " + client.PostData);

                var strData = client.MakeRequest();

                //Logger.WriteLog(Logger.LogType.Trace, "PageViewAllSiteGetData strData=> " + strData);

                var ParseObject = JsonConvert.DeserializeObject<ViewVideo>(strData);

                if (ParseObject == null) return new ViewVideo();

                return ParseObject;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "GetViewVideo=>" + ex.Message);
                return new ViewVideo();
            }
        }
    }

    [DataContract]
    public class ResponseDataViewsAll
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int errorCode { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<StatisticV3ViewCountEntity> data { get; set; }
        public ResponseDataViewsAll()
        {
            success = false;
            errorCode = 0;
            message = "";
            data =null;
        }
    }

    [DataContract]
    public class ResponseDataViewsZone
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int errorCode { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<StatisticV3CategoryEntity> data { get; set; }
        public ResponseDataViewsZone()
        {
            success = false;
            errorCode = 0;
            message = "";
            data = null;
        }
    }
}
