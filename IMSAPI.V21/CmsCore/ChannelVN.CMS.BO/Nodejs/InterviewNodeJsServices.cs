﻿using ChannelVN.CMS.BO.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class InterviewNodeJsServices
    {
        private static NodeJsRestClient GetRestClient()
        {
            return new NodeJsRestClient
            {
                EndPoint = CmsChannelConfiguration.GetAppSetting("InterviewApiUrl"),
                Method = NodeJsRestClient.HttpMethod.POST,
                ContentType = "application/x-www-form-urlencoded; charset=utf-8",
                Channel_Id = CmsChannelConfiguration.GetAppSetting("InterviewApiChannel"),
                SecretKey = CmsChannelConfiguration.GetAppSetting("InterviewApiSecretKey"),
                PostData = "",
                ActionName = ""
            };
        }

        const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        #region Interview
        public static List<NodeJs_InterviewEntity> Interview_Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&keyword={0}", keyword);
                sb.AppendFormat("&page_index={0}", pageIndex);
                sb.AppendFormat("&page_size={0}", pageSize);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "search";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListInterview>(strData);

                if (ParseObject == null) ParseObject = new ResponseListInterview();

                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "Interview_Search=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_InterviewEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Search=>" + ex.Message);
                return new List<NodeJs_InterviewEntity>();
            }
        }
        public static List<NodeJs_InterviewEntity2> ListInterviewByIds(string ids)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&ids={0}", ids);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-list-ids";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListInterviewByIds>(strData);

                if (ParseObject == null) ParseObject = new ResponseListInterviewByIds();

                if (!ParseObject.success)
                    Logger.WriteLog(Logger.LogType.Error, "ListInterviewByIds=>" + ParseObject.message);
                return ParseObject.data == null ? new List<NodeJs_InterviewEntity2>() : ParseObject.data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ListInterviewByIds=>" + ex.Message);
                return new List<NodeJs_InterviewEntity2>();
            }
        }
        public static NodeJs_InterviewEntity Interview_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                client.PostData = sb.ToString();
                client.ActionName = "get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseInterview>(strData);

                if (ParseObject == null) ParseObject = new ResponseInterview();
                //totalRow = ParseObject.Data.total_count;
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "Interview_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_GetById=>" + ex.Message);
                return null;
            }
        }
        public static WcfActionResponse Interview_Insert(NodeJs_Interview obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&unsign_title={0}", obj.UnsignTitle);
                sb.AppendFormat("&start_date={0}", obj.StartDate);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus ? 1 : 0);
                sb.AppendFormat("&is_actived={0}", obj.IsActived ? 1 : 0);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&published_date={0}", obj.PublisedDate);
                sb.AppendFormat("&published_by={0}", obj.PublishedBy);
                client.PostData = sb.ToString();
                client.ActionName = "add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Update(NodeJs_Interview obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&title={0}", obj.Title);
                sb.AppendFormat("&unsign_title={0}", obj.UnsignTitle);
                sb.AppendFormat("&start_date={0}", obj.StartDate);
                sb.AppendFormat("&is_focus={0}", obj.IsFocus ? 1 : 0);
                sb.AppendFormat("&is_actived={0}", obj.IsActived ? 1 : 0);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&published_date={0}", obj.PublisedDate);
                sb.AppendFormat("&published_by={0}", obj.PublishedBy);
                client.PostData = sb.ToString();
                client.ActionName = "update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_InterviewEntity Interview_GetInterview(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);                
                client.PostData = sb.ToString();
                client.ActionName = "interview/get-interview-with-question";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseInterview>(strData);

                if (ParseObject == null) ParseObject = new ResponseInterview();                
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "Interview_GetInterview=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_GetInterview=>" + ex.Message);
                return null;
            }
        }
        #endregion

        #region Interview_Question
        public static NodeJs_InterviewQuestionEntity InterviewQuestion_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "question/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseInterviewQuestion>(strData);

                if (ParseObject == null) ParseObject = new ResponseInterviewQuestion();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewQuestion_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewQuestion_GetById=>" + ex.Message);
                return new NodeJs_InterviewQuestionEntity();
            }
        }
        public static List<NodeJs_InterviewQuestionEntity> InterviewQuestion_Search(string InterviewId, EnumNodeJsInterviewQuestionStatus status)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&interview_id={0}", InterviewId);
                if(status > 0)
                    sb.AppendFormat("&status={0}", (int)status);
                client.PostData = sb.ToString();
                client.ActionName = "question/search";
                var strData = client.MakeRequest();
                //Logger.WriteLog(Logger.LogType.Debug, strData);
                var ParseObject = JsonConvert.DeserializeObject<ResponseListInterviewQuestion>(strData);

                if (ParseObject == null) ParseObject = new ResponseListInterviewQuestion();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewQuestion_GetListByInterviewId=>" + ParseObject.Message);
                return ParseObject.Data == null ? new List<NodeJs_InterviewQuestionEntity>() : ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewQuestion_GetListByInterviewId=>" + ex.Message);
                return new List<NodeJs_InterviewQuestionEntity>();
            }
        }
        public static WcfActionResponse Interview_Question_Insert(NodeJs_InterviewQuestionEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&priority={0}", obj.Priority);
                sb.AppendFormat("&type={0}", obj.Type);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&process_user={0}", obj.ProcessUser);
                sb.AppendFormat("&content={0}", HttpUtility.UrlEncode(obj.Content));
                sb.AppendFormat("&fullname={0}", obj.FullName);
                sb.AppendFormat("&sex={0}", obj.Sex);
                sb.AppendFormat("&age={0}", obj.Age);
                sb.AppendFormat("&email={0}", obj.Email);
                sb.AppendFormat("&job={0}", obj.Job);
                sb.AppendFormat("&address={0}", obj.Address);
                sb.AppendFormat("&mobile={0}", obj.Mobile);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&received_date={0}", obj.ReceivedDate);
                sb.AppendFormat("&received_by={0}", obj.ReceivedBy);
                sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                sb.AppendFormat("&published_by={0}", obj.PublishedBy);

                client.PostData = sb.ToString();
                client.ActionName = "question/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_Update(NodeJs_InterviewQuestionEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();

                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", obj.Id);
                sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&priority={0}", obj.Priority);
                sb.AppendFormat("&type={0}", obj.Type);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&process_user={0}", obj.ProcessUser);
                sb.AppendFormat("&content={0}", HttpUtility.UrlEncode(obj.Content));
                sb.AppendFormat("&fullname={0}", obj.FullName);
                sb.AppendFormat("&sex={0}", obj.Sex);
                sb.AppendFormat("&age={0}", obj.Age);
                sb.AppendFormat("&email={0}", obj.Email);
                sb.AppendFormat("&job={0}", obj.Job);
                sb.AppendFormat("&address={0}", obj.Address);
                sb.AppendFormat("&mobile={0}", obj.Mobile);
                sb.AppendFormat("&received_date={0}", obj.ReceivedDate);
                sb.AppendFormat("&receive_by={0}", obj.ReceivedBy);
                sb.AppendFormat("&published_date={0}", obj.PublishedDate);
                sb.AppendFormat("&published_by={0}", obj.PublishedBy);

                client.PostData = sb.ToString();
                client.ActionName = "question/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_Publish(string Id, string InterviewId, string PublishedBy)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&interview_id={0}", InterviewId);
                sb.AppendFormat("&status={0}", (int)EnumNodeJsInterviewQuestionStatus.Published);
                sb.AppendFormat("&published_date={0}", DateTime.Now.ToString(DateTimeFormat));
                sb.AppendFormat("&published_by={0}", PublishedBy);
                client.PostData = sb.ToString();
                client.ActionName = "question/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_UnPublish(string Id, string InterviewId, string modified_by)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&interview_id={0}", InterviewId);
                sb.AppendFormat("&status={0}", (int)EnumNodeJsInterviewQuestionStatus.UnPublished);
                sb.AppendFormat("&modified_by={0}", modified_by);
                client.PostData = sb.ToString();
                client.ActionName = "question/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_ReturnWaitForAnswer(string Id, string InterviewId, string modified_by)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&interview_id={0}", InterviewId);
                sb.AppendFormat("&status={0}", (int)EnumNodeJsInterviewQuestionStatus.WaitForAnswer);
                sb.AppendFormat("&modified_by={0}", modified_by);
                client.PostData = sb.ToString();
                client.ActionName = "question/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_ReturnWaitForAnswer=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_UpdatePrioriry(string Id, int Priority, string modified_by)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                sb.AppendFormat("&priority={0}", Priority);                
                sb.AppendFormat("&modified_by={0}", modified_by);
                client.PostData = sb.ToString();
                client.ActionName = "question/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_ReturnWaitForAnswer=>" + ex.Message);
                return new WcfActionResponse();
            }
        }
        public static WcfActionResponse Interview_Question_Delete(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "question/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());
                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Delete=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        //public static WcfActionResponse Interview_Question_Delete(string Id)
        //{
        //    try
        //    {
        //        var client = GetRestClient();
        //        var sb = new StringBuilder();
        //        sb.AppendFormat("channel_id={0}", client.Channel_Id);
        //        sb.AppendFormat("&secret_key={0}", client.SecretKey);
        //        sb.AppendFormat("&id={0}", Id);
        //        client.PostData = sb.ToString();
        //        client.ActionName = "question/remove";
        //        var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());
        //        return NodeJsRestClient.ConvertWcfResponse(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Delete=>" + ex.Message);
        //        return new WcfActionResponse();
        //    }
        //}
        //public static WcfActionResponse Interview_Question_Delete_By_InterviewId(string InterviewId)
        //{
        //    try
        //    {
        //        var client = GetRestClient();
        //        var sb = new StringBuilder();
        //        sb.AppendFormat("channel_id={0}", client.Channel_Id);
        //        sb.AppendFormat("&secret_key={0}", client.SecretKey);
        //        sb.AppendFormat("&interview_id={0}", InterviewId);
        //        client.PostData = sb.ToString();
        //        client.ActionName = "question/remove-by-interview-id";
        //        var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

        //        return NodeJsRestClient.ConvertWcfResponse(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, "Interview_Question_Delete_By_InterviewId=>" + ex.Message);
        //        return new WcfActionResponse();
        //    }
        //}


        #endregion

        #region InterviewAnswer

        public static WcfActionResponse Interview_Answer_Insert(NodeJs_InterviewAnswerEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                //sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&question_id={0}", obj.QuestionId);
                sb.AppendFormat("&guest_id={0}", obj.GuestId);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&answer_content={0}", obj.AnswerContent);
                sb.AppendFormat("&answer_date={0}", obj.AnswerDate);
                sb.AppendFormat("&answer_by={0}", obj.AnswerBy);

                client.PostData = sb.ToString();
                client.ActionName = "answer/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Answer_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse Interview_Answer_Update(NodeJs_InterviewAnswerEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                //sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&question_id={0}", obj.QuestionId);
                sb.AppendFormat("&guest_id={0}", obj.GuestId);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&answer_content={0}", obj.AnswerContent);
                sb.AppendFormat("&answer_date={0}", obj.AnswerDate);
                sb.AppendFormat("&answer_by={0}", obj.AnswerBy);
                sb.AppendFormat("&id={0}", obj.Id);

                client.PostData = sb.ToString();
                client.ActionName = "answer/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Answer_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_InterviewAnswerEntity InterviewAnswer_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "answer/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseInterviewAnswer>(strData);

                if (ParseObject == null) ParseObject = new ResponseInterviewAnswer();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewAnswer_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewAnswer_GetById=>" + ex.Message);
                return new NodeJs_InterviewAnswerEntity();
            }
        }

        public static List<NodeJs_InterviewAnswerEntity> InterviewAnswer_GetByQuestionId(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&question_id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "answer/get-by-question-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListInterviewAnswer>(strData);

                if (ParseObject == null) ParseObject = new ResponseListInterviewAnswer();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewAnswer_GetByQuestionId=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewAnswer_GetByQuestionId=>" + ex.Message);
                return new List<NodeJs_InterviewAnswerEntity>();
            }
        }

        public static WcfActionResponse Interview_Answer_Delete(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "answer/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());
                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Answer_Delete=>" + ex.Message);
                return new WcfActionResponse();
            }
        }


        #endregion  

        #region InterviewGuest

        public static WcfActionResponse Interview_Guest_Insert(NodeJs_InterviewGuestEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&online_username={0}", obj.OnlineUserName);
                sb.AppendFormat("&fullname={0}", obj.FullName);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&role={0}", obj.Role);
                sb.AppendFormat("&desc={0}", obj.Desc);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);

                client.PostData = sb.ToString();
                client.ActionName = "guest/add";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Guest_Insert=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse Interview_Guest_Update(NodeJs_InterviewGuestEntity obj)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&interview_id={0}", obj.InterviewId);
                sb.AppendFormat("&online_username={0}", obj.OnlineUserName);
                sb.AppendFormat("&fullname={0}", obj.FullName);
                sb.AppendFormat("&avatar={0}", obj.Avatar);
                sb.AppendFormat("&role={0}", obj.Role);
                sb.AppendFormat("&desc={0}", obj.Desc);
                sb.AppendFormat("&status={0}", obj.Status);
                sb.AppendFormat("&created_by={0}", obj.CreatedBy);
                sb.AppendFormat("&id={0}", obj.Id);

                client.PostData = sb.ToString();
                client.ActionName = "guest/update";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Guest_Update=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static WcfActionResponse Interview_Guest_Delete(string id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", id);

                client.PostData = sb.ToString();
                client.ActionName = "guest/remove";
                var data = JsonConvert.DeserializeObject<NodeJsRestClient.ApiActionResponse>(client.MakeRequest());

                return NodeJsRestClient.ConvertWcfResponse(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "Interview_Guest_Delete=>" + ex.Message);
                return new WcfActionResponse();
            }
        }

        public static NodeJs_InterviewGuestEntity InterviewGuest_GetById(string Id)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&id={0}", Id);
                client.PostData = sb.ToString();
                client.ActionName = "guest/get-by-id";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseInterviewGuest>(strData);

                if (ParseObject == null) ParseObject = new ResponseInterviewGuest();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewGuest_GetById=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewGuest_GetById=>" + ex.Message);
                return new NodeJs_InterviewGuestEntity();
            }
        }

        public static List<NodeJs_InterviewGuestEntity> InterviewGuest_GetByInterviewId(string interviewId, int status)
        {
            try
            {
                var client = GetRestClient();
                var sb = new StringBuilder();
                sb.AppendFormat("channel_id={0}", client.Channel_Id);
                sb.AppendFormat("&secret_key={0}", client.SecretKey);
                sb.AppendFormat("&interview_id={0}", interviewId);
                sb.AppendFormat("&status={0}", status);
                client.PostData = sb.ToString();
                client.ActionName = "guest/search";
                var strData = client.MakeRequest();
                var ParseObject = JsonConvert.DeserializeObject<ResponseListInterviewGuest>(strData);

                if (ParseObject == null) ParseObject = new ResponseListInterviewGuest();
                if (!ParseObject.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InterviewGuest_GetByQuestionId=>" + ParseObject.Message);
                return ParseObject.Data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InterviewGuest_GetByQuestionId=>" + ex.Message);
                return new List<NodeJs_InterviewGuestEntity>();
            }
        }

        #endregion
    }


    #region Entity
    public enum EnumInterviewIsFocus : int
    {
        All = -1,
        IsFocus = 1,
        NotIsFocus = 0,
    }

    public enum EnumInterviewStatus : int
    {

        All = -1,
        Temporary = 0,
        Published = 1,
        Unpublished = 2,
    }

    public enum EnumNodeJsInterviewQuestionStatus : int
    {

        All = -1,

        WaitForCollect = 0,

        WaitForDistribute = 1,

        WaitForAnswer = 2,

        WaitForPublish = 3,

        Published = 4,

        UnPublished = 5,

        WaitForEdit = 6,
    }

    [DataContract]
    public class ResponseListInterview
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_InterviewEntity> Data { get; set; }
        public ResponseListInterview()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_InterviewEntity>();
        }
    }
    [DataContract]
    public class ResponseListInterviewByIds
    {
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public List<NodeJs_InterviewEntity2> data { get; set; }
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public int code { get; set; }
        [DataMember]
        public string content { get; set; }
    }
    [DataContract]
    public class ResponseInterview
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_InterviewEntity Data { get; set; }
        public ResponseInterview()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }    
    [DataContract]
    public class ResponseInterviewQuestion
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_InterviewQuestionEntity Data { get; set; }
        public ResponseInterviewQuestion()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }
    [DataContract]
    public class ResponseListInterviewQuestion
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_InterviewQuestionEntity> Data { get; set; }
        public ResponseListInterviewQuestion()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_InterviewQuestionEntity>();
        }
    }

    [DataContract]
    public class ResponseInterviewAnswer
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_InterviewAnswerEntity Data { get; set; }
        public ResponseInterviewAnswer()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = null;
        }
    }

    [DataContract]
    public class ResponseListInterviewAnswer
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_InterviewAnswerEntity> Data { get; set; }
        public ResponseListInterviewAnswer()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_InterviewAnswerEntity>();
        }
    }

    [DataContract]
    public class ResponseInterviewGuest
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public NodeJs_InterviewGuestEntity Data { get; set; }
        public ResponseInterviewGuest()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new NodeJs_InterviewGuestEntity();
        }
    }


    [DataContract]
    public class ResponseListInterviewGuest
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public int Code { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public List<NodeJs_InterviewGuestEntity> Data { get; set; }
        public ResponseListInterviewGuest()
        {
            Success = false;
            Code = 0;
            Message = "";
            Data = new List<NodeJs_InterviewGuestEntity>();
        }
    }

    [DataContract]
    public class NodeJs_InterviewQuestionEntity
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("interview_id")]
        public string InterviewId { get; set; }

        [DataMember]
        [JsonProperty("priority")]
        public int Priority { get; set; }

        [DataMember]
        [JsonProperty("type")]
        public int Type { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("process_user")]
        public string ProcessUser { get; set; }
        [DataMember]
        [JsonProperty("content")]
        public string Content { get; set; }
        [DataMember]
        [JsonProperty("fullName")]
        public string FullName { get; set; }
        [DataMember]
        [JsonProperty("sex")]
        public int Sex { get; set; }
        [DataMember]
        [JsonProperty("age")]
        public int Age { get; set; }
        [DataMember]
        [JsonProperty("email")]
        public string Email { get; set; }
        [DataMember]
        [JsonProperty("job")]
        public string Job { get; set; }
        [DataMember]
        [JsonProperty("address")]
        public string Address { get; set; }
        [DataMember]
        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("received_date")]
        public DateTime ReceivedDate { get; set; }

        [JsonProperty("received_by")]
        public string ReceivedBy { get; set; }

        [JsonProperty("published_date")]
        public DateTime PublishedDate { get; set; }

        [JsonProperty("published_by")]
        public string PublishedBy { get; set; }

        [JsonProperty("modified_date")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty("modified_by")]
        public string ModifiedBy { get; set; }

        [JsonProperty("channel_id")]
        public string ChannelId { get; set; }

        [JsonProperty("answers")]
        public List<NodeJs_InterviewAnswerEntity> Answers { get; set; }
    }

    public class InterviewQuestionMappingEntity
    {
        public string Id { get; set; }
        public string InterviewId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Body { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int Status { get; set; }
        public int IsHighlight { get; set; }
        public string TabId { get; set; }
        public int IsFocus { get; set; }
        public string Avatar { get; set; }
        public int IsHot { get; set; }
        public bool IsRemove { get; set; }
    }

    public class NodeJs_InterviewEntity
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }
        [DataMember]
        [JsonProperty("title")]
        public string Title { get; set; }
        [DataMember]
        [JsonProperty("unsign_title")]
        public string UnsignTitle { get; set; }

        [JsonProperty("is_focus")]
        //public string _isFocus { get; set; }
        //public bool IsFocus { get { return _isFocus == "1" ? true : false; } set { _isFocus = value ? "1" : "0"; } }
        public int IsFocus { get; set; }
        [JsonProperty("is_actived")]
        //public string _isActived { get; set; }
        //public bool IsActived { get { return _isActived == "1" ? true : false; } set { } }
        public int IsActived { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("published_date")]
        public DateTime PublishedDate { get; set; }

        [JsonProperty("published_by")]
        public string PublishedBy { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("questions")]
        public List<NodeJs_InterviewQuestionEntity> Questions { get; set; }

        //[DataMember]
        //public long NewsId { get; set; }
        //[DataMember]
        //public string NewsTitle { get; set; }
    }

    public class NodeJs_InterviewEntity2
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }
        [DataMember]
        [JsonProperty("title")]
        public string Title { get; set; }
        [DataMember]
        [JsonProperty("unsign_title")]
        public string UnsignTitle { get; set; }

        [JsonProperty("is_focus")]
        //public string _isFocus { get; set; }
        //public bool IsFocus { get { return _isFocus == "1" ? true : false; } set { _isFocus = value ? "1" : "0"; } }
        public int IsFocus { get; set; }
        [JsonProperty("is_actived")]
        //public string _isActived { get; set; }
        //public bool IsActived { get { return _isActived == "1" ? true : false; } set { } }
        public int IsActived { get; set; }

        [JsonProperty("start_date")]
        public DateTime StartDate { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty("published_date")]
        public DateTime PublishedDate { get; set; }

        [JsonProperty("published_by")]
        public string PublishedBy { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("Questions")]
        public List<NodeJs_InterviewQuestionCountEntity> Questions { get; set; }

        [DataMember]
        [JsonProperty("NewsId")]
        public string NewsId { get; set; }

    }
    public class NodeJs_InterviewQuestionCountEntity
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }
    }

        public class NodeJs_Interview
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string UnsignTitle { get; set; }
        public bool IsFocus { get; set; }
        public bool IsActived { get; set; }
        public DateTime StartDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PublishedBy { get; set; }
        public DateTime PublisedDate { get; set; }
        public int Status { get; set; }

    }

    [DataContract]
    public class NodeJs_InterviewAnswerEntity
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }

        //[JsonProperty("interview_id")]
        //public string InterviewId { get; set; }

        [JsonProperty("question_id")]
        public string QuestionId { get; set; }

        [JsonProperty("guests_id")]
        public string GuestId { get; set; }

        [JsonProperty("answers_content")]
        public string AnswerContent { get; set; }

        [JsonProperty("answers_date")]
        public DateTime AnswerDate { get; set; }

        [JsonProperty("answers_by")]
        public string AnswerBy { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public int Status { get; set; }
    }

    [DataContract]
    public class NodeJs_InterviewGuestEntity
    {
        [DataMember]
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("interview_id")]
        public string InterviewId { get; set; }

        [JsonProperty("online_username")]
        public string OnlineUserName { get; set; }

        [DataMember]
        [JsonProperty("fullname")]
        public string FullName { get; set; }

        [DataMember]
        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [DataMember]
        [JsonProperty("role")]
        public int Role { get; set; }

        [DataMember]
        [JsonProperty("desc")]
        public string Desc { get; set; }

        [DataMember]
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }
    }
    #endregion
}
