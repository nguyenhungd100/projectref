﻿using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ChannelVN.CMS.BO.Nodejs
{
    public class PhotoNodeJsServices
    {
        public const string FILE_MANAGER_UPLOAD_EXPIRED_TIME = "FILE_MANAGER_UPLOAD_EXPIRED_TIME";
        public const string FILE_MANAGER_UPLOAD_MAXLENGTH = "FILE_MANAGER_UPLOAD_MAXLENGTH";
        public const string FILE_MANAGER_UPLOAD_NAMESPACE = "FILE_MANAGER_UPLOAD_NAMESPACE";
        public const string FILE_MANAGER_SECRET_KEY = "FILE_MANAGER_SECRET_KEY";
        public const string FILE_MANAGER_HTTPUPLOAD = "FILE_MANAGER_HTTPUPLOAD";
        public const string FILE_MANAGER_INCLUDING_PREFIX = "FILE_MANAGER_INCLUDING_PREFIX";
        public const string FILE_MANAGER_PREFIX = "FILE_MANAGER_PREFIX";

        private static readonly int UploadExpiredTime = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_EXPIRED_TIME);
        private static readonly string UploadNamespace = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_UPLOAD_NAMESPACE);
        private static readonly string SecretKey = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_SECRET_KEY);
        private static readonly string HttpUpload = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_HTTPUPLOAD);
        private static readonly bool IsIncludingPrefix = CmsChannelConfiguration.GetAppSettingInBoolean(FILE_MANAGER_INCLUDING_PREFIX);
        private static readonly string Prefix = CmsChannelConfiguration.GetAppSetting(FILE_MANAGER_PREFIX);

        //private static NodeJsRestClient GetRestClient()
        //{
        //    return new NodeJsRestClient
        //    {
        //        EndPoint = CmsChannelConfiguration.GetAppSetting("PhotoApiUrl"),
        //        Method = NodeJsRestClient.HttpMethod.POST,
        //        ContentType = "application/x-www-form-urlencoded; charset=utf-8",
        //        Channel_Id = CmsChannelConfiguration.GetAppSetting("PhotoApiChannel"),
        //        SecretKey = CmsChannelConfiguration.GetAppSetting("PhotoApiSecretKey"),
        //        PostData = "",
        //        ActionName = ""
        //    };
        //}


        //const string DateTimeFormat = "yyyy/MM/dd HH:mm";

        //public static bool UploadAvatar(long newsId,string avatar)
        //{
        //    var client = GetRestClient();
        //    var sb = new StringBuilder();
        //    sb.AppendFormat("channel_id={0}", client.Channel_Id);

        //    sb.AppendFormat("&news_id={0}", newsId);            
        //    sb.AppendFormat("&avatar={0}", avatar);
        //    sb.AppendFormat("&secret_key={0}", client.SecretKey);
        //    client.PostData = sb.ToString();
        //    client.ActionName = "upload";

        //    var strData = client.MakeRequest();
        //    var ParseObject = JsonConvert.DeserializeObject<ResponseListViews>(strData);

        //    if (ParseObject == null) ParseObject = new ResponseListViews();

        //    if (!ParseObject.Success)
        //        Logger.WriteLog(Logger.LogType.Error, "UploadAvatar=>" + ParseObject.Message);

        //    NewsBo.UpdateNewsAvatar(newsId, ParseObject.Data.ToString(),0);

        //    return true;
        //}

        public static bool UploadAvatar2(long newsId, string filePath, string username)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                return false;
            }

            var fileNameUpload = DateTime.Now.Month + "/" + Path.GetFileName(filePath);

            var policyData = CreatePolicyForUpload(username, filePath, fileNameUpload, false);
            var policy = policyData[1];
            var signature = policyData[2];
            var path = policyData[3];
            var result = false;
            try
            {
                result = UploadFile(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(filePath));
                if (result)
                {
                    //update avatar
                    NewsBo.UpdateNewsAvatar(newsId, path, 0);
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UploadAvatar2(path: " + path + " fileName: " + fileNameUpload + ") => " + ex.ToString());
                return result;
            }
        }

        private static string Serialize(string fileName)
        {
            using (FileStream reader = new FileStream(fileName, FileMode.Open))
            {
                byte[] buffer = new byte[reader.Length];
                reader.Read(buffer, 0, (int)reader.Length);
                return Convert.ToBase64String(buffer);
            }
        }

        public static bool UploadFile(string currentPolicy, string currentSignature, string fileName, string base64Data)
        {
            var query = "";
            try
            {
                query = "policy=" + HttpContext.Current.Server.UrlEncode(currentPolicy) +
                "&signature=" + HttpContext.Current.Server.UrlEncode(currentSignature) +
                "&source=" + HttpContext.Current.Server.UrlEncode(base64Data);
                var requestParamsInBytes = Encoding.UTF8.GetBytes(query);

                var httpRequest = (HttpWebRequest)WebRequest.Create(HttpUpload);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = requestParamsInBytes.Length;
                httpRequest.Method = "POST";
                httpRequest.KeepAlive = true;
                httpRequest.Timeout = 5 * 1000;
                httpRequest.MaximumResponseHeadersLength = int.MaxValue;

                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    streamWriter.Write(requestParamsInBytes, 0, requestParamsInBytes.Length);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK)
                {
                    httpResponse.Close();
                    httpResponse.Dispose();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UploadFile(" + query + ") => " + ex.ToString());
                return false;
            }
        }

        private static dynamic getDirectoryFile(string domain, string fileUrl)
        {
            var filename = string.Empty;
            var ext = string.Empty;
            var directory = string.Empty;

            fileUrl = fileUrl.Split('?')[0];

            var st = new Stack<string>(fileUrl.Split(new string[] { "/" }, StringSplitOptions.None));
            filename = st.Pop();
            st = new Stack<string>(fileUrl.Split(new string[] { "." }, StringSplitOptions.None));
            ext = st.Pop();
            directory = fileUrl.Replace(domain, "");
            directory = directory.Replace('/' + filename, "");
            return new
            {
                directory,
                filename,
                ext
            };
        }

        public static string[] getPolicyForUpload(string directory, string filename, bool overwrite)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_MAXLENGTH);
                string policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"redirect\": \"\",";
                policy += "\"directory\": \"" + directory + "\",";
                policy += "\"filename\": \"" + filename + "\",";
                policy += "\"overwrite\": \"" + (overwrite ? "1" : "0") + "\",";
                policy += "\"fix_orientation\":1,";
                policy += "\"content-length-range\": \"10-" + maxRequest + "\"";
                policy += "}";

                var signature = CreateSignature(SecretKey, policy);
                return new[] { strRandom, Base64.Encode(policy), signature, policy };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string CreateSignature(string secretKey, string policy)
        {
            var encoding = new ASCIIEncoding();
            var policyBytes = encoding.GetBytes(policy);
            var base64Policy = Convert.ToBase64String(policyBytes);

            var secretKeyBytes = encoding.GetBytes(secretKey);
            var hmacsha1 = new HMACSHA256(secretKeyBytes);

            var base64PolicyBytes = encoding.GetBytes(base64Policy);
            var signatureBytes = hmacsha1.ComputeHash(base64PolicyBytes);

            return Convert.ToBase64String(signatureBytes);
        }

        public static string[] CreatePolicyForUpload(string userName, string path, string filename, bool overwrite)
        {
            try
            {
                var addMin = UploadExpiredTime > 0 ? UploadExpiredTime : 25; // minutes
                var strRandom = Guid.NewGuid().ToString("N").Substring(0, 5);
                var maxRequest = CmsChannelConfiguration.GetAppSettingInInt32(FILE_MANAGER_UPLOAD_MAXLENGTH);

                Logger.WriteLog(Logger.LogType.Warning, "====== Create policy: " + filename);
                string policy = "{";
                policy += "\"expires\": \"" + DateTime.Now.AddMinutes(addMin).ToString("r") + "\",";
                policy += "\"namespace\": \"" + UploadNamespace + "\",";
                policy += "\"redirect\": \"\",";
                policy += "\"directory\": \"" + (IsIncludingPrefix ? Prefix + "/" + path : path) + "\",";
                policy += "\"filename\": \"" + filename + "\",";
                policy += "\"overwrite\": \"" + (overwrite ? "1" : "0") + "\",";
                policy += "\"fix_orientation\":1,";
                policy += "\"content-length-range\": \"10-" + maxRequest + "\"";
                policy += "}";
                //Logger.WriteLog(Logger.LogType.Warning, "Policy: " + policy);
                var signature = CreateSignature(SecretKey, policy);
                //Logger.WriteLog(Logger.LogType.Warning, "Signature: " + signature);
                //Logger.WriteLog(Logger.LogType.Warning, "====== End of policy: " + filename);

                return new[] { strRandom, Base64.Encode(policy), signature, path, policy };

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
