﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.GameK.Auction;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.DAL.External.GameK
{
    public class ReverseAuctionDal
    {
        public static bool AddNewAuction(ReverseAuctionEntity auction, ref int auctionId)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.ReverseAuctionMainDal.AddNewAuction(auction, ref auctionId);
            }
            return returnValue;
        }

        public static bool UpdateAuctionById(ReverseAuctionEntity auction)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.ReverseAuctionMainDal.UpdateAuctionById(auction);
            }
            return returnValue;
        }

        public static bool DeleteUserById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.DeleteUserById(id);
            }
            return returnValue;
        }

        public static ReverseAuctionEntity GetById(long id)
        {
            ReverseAuctionEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.ReverseAuctionMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<ReverseAuctionEntity> Search(string keyword, int status, long newsId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ReverseAuctionEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.ReverseAuctionMainDal.Search(keyword, status, newsId, pageIndex, pageSize,
                    ref totalRow);
            }
            return returnValue;
        }

    }
}
