﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.GameK
{
    public class NewsDal : Base.News.NewsDal
    {
        public static bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GameKNewsMainDal.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, newsRelationSpecialId, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GameKNewsMainDal.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, newsRelationSpecialId, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }
    }
}
