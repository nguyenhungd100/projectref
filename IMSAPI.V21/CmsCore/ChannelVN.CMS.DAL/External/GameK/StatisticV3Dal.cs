﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.GameK
{
    public class StatisticV3Dal : Base.Statistic.StatisticV3Dal
    {
        public static List<StatisticV3ViewCountEntity> PageViewAll(Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3ViewCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.PageViewAll(fromDate, toDate);
            }
            return returnValue;
        }

        public static List<StatisticV3CategoryEntity> CategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3CategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.CategoryViewCount(cateId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<StatisticV3CategoryEntity> CategoryViewCountMobile(string cateId, Int64 fromDate, Int64 toDate)
        {
            //List<StatisticV3CategoryEntity> returnValue;
            //using (var db = new CmsMainDb())
            //{
            //    returnValue = db.StatisticV3MainDal.CategoryViewCountMobile(cateId, fromDate, toDate);
            //}
            //return returnValue;

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                     {
                                         new SqlParameter("@CateId", cateId),
                                         new SqlParameter("@FromDate", fromDate),
                                         new SqlParameter("@ToDate", toDate)
                                     };
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.Statistic),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_MobileGameK_CategoryDaily_By_Day",
                                                    parameters);

                var list = new List<StatisticV3CategoryEntity>();
                while (reader.Read())
                {
                    var single = new StatisticV3CategoryEntity();
                    EntityBase.SetObjectValue(reader, ref single);
                    list.Add(single);
                }
                reader.Close();
                reader.Dispose();
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_MobileSoha_CategoryDaily_By_Day:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }

        public static List<StatisticV3ViewCountEntity> PageViewAllMobile(Int64 fromDate, Int64 toDate)
        {
            //List<StatisticV3CategoryEntity> returnValue;
            //using (var db = new CmsMainDb())
            //{
            //    returnValue = db.StatisticV3MainDal.PageViewAllMobile(fromDate, toDate);
            //}
            //return returnValue;

            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                     {
                                         new SqlParameter("@FromDate", fromDate),
                                         new SqlParameter("@ToDate", toDate)
                                     };
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.Statistic),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_MobileGameK_ViewCount_By_Day",
                                                    parameters);

                var list = new List<StatisticV3ViewCountEntity>();
                while (reader.Read())
                {
                    var single = new StatisticV3ViewCountEntity();
                    EntityBase.SetObjectValue(reader, ref single);
                    list.Add(single);
                }
                reader.Close();
                reader.Dispose();
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_MobileSoha_ViewCount_By_Day:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }
    }
}
