﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.GenK.Expert;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.GenK
{
    public class ExpertDal
    {
        public static bool Insert(ExpertEntity expertEntity, ref int newsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.Insert(expertEntity, ref newsAuthorId);
            }
            return returnValue;
        }

        public static ExpertEntity GetById(int id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Update(ExpertEntity expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.Update(expertEntity);
            }
            return returnValue;
        }

        public static List<ExpertEntity> Search(string keyword)
        {
            List<ExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.Search(keyword);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.Delete(id);
            }
            return returnValue;
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.GetExpertByNewsId(id);
            }
            return returnValue;
        }

        public static bool InsertExpertToNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.InsertExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool UpdateExpertInNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.UpdateExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool DeleteExpertNewsId(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.DeleteExpertInNews(id);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertGenkMainDal.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
