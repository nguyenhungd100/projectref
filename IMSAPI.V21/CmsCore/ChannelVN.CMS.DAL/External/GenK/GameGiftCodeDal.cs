﻿using System;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.GenK.GiftCode;
using System.Data;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.GenK
{
    public class GameGiftCodeDal
    {

        #region Set methods

        public static bool InsertGiftCode(long newsId, string giftCode, string tags)
        {
            bool retVal = false;
            using (var db = new ExternalCmsDb())
            {
                retVal = db.GameGiftCodeDal.InsertGiftCode(newsId, giftCode, tags);
            }
            return retVal;
        }
        #endregion

        public static GameGiftCodeEntity GetGiftCodeByNewsId(long newsId)
        {
            GameGiftCodeEntity retVal = null;
            using (var db = new ExternalCmsDb())
            {
                retVal = db.GameGiftCodeDal.GetGiftCodeByNewsId(newsId);
            }
            return retVal;
        }
    }
}
