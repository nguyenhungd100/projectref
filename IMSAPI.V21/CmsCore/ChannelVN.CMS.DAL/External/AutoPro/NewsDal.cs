﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.AutoPro
{
    public class NewsDal : Base.News.NewsDal
    {
        public static bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoProMainDal.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, newsRelationSpecialId, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AutoProMainDal.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, newsRelationSpecialId, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }
    }
}
