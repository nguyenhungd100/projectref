﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.GiaDinhNet
{
    public class NewsDal : Base.News.NewsDal
    {
        //Magazine News
        public static List<NewsEntity> SearchNewsByMagazineIdWithPaging(int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GiaDinhNetNewsMainDal.SearchNewsByMagazineIdWithPaging(magazineId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInMagazine(int zoneId, string keyword, int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GiaDinhNetNewsMainDal.SearchNewsPublishExcludeNewsInMagazine(zoneId, keyword, magazineId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
