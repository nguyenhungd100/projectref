﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.Kenh14
{
    public class NewsDal : Base.News.NewsDal
    {
        public static bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.Kenh14NewsMainDal.InsertNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.Kenh14NewsMainDal.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateSpecialNewsRelation(long newsId, int zoneId, string newsRelationSpecialId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.Kenh14NewsMainDal.UpdateSpecialNewsRelation(newsId, zoneId, newsRelationSpecialId);
            }
            return returnValue;
        }

        public static List<NewsWithAnalyticEntity> GetNewsByListNewsId(string listNewsId)
        {
            var query = @"SELECT N.News_Id AS NewsId, C.Cat_Name AS ZoneName, N.News_Title AS Title, N.News_InitialContent AS Sapo, 
                        N.News_Image AS Avatar, N.News_Image AS Avatar2, N.News_Image AS Avatar3, N.News_Image AS Avatar4, N.News_Image AS Avatar5, 
                        N.News_Author AS CreatedBy, N.News_Approver AS PublishedBy, N.News_PublishDate AS DistributionDate, CONVERT(int, N.ViewCount) AS ViewCount, Url
                        FROM News AS N INNER JOIN Category AS C ON N.Cat_Id = C.Cat_Id
                        WHERE (N.News_Status = 3) AND (PATINDEX('%;' + CONVERT(varchar(50), N.News_Id) + ';%', '" + listNewsId + @"') > 0)";
            SqlDataReader reader = null;
            try
            {
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.Kenh14OldVersionDb),
                                                    CommandType.Text,
                                                    query);

                var newsList = new List<NewsWithAnalyticEntity>();
                while (reader.Read())
                {
                    var news = new NewsWithAnalyticEntity();
                    EntityBase.SetObjectValue(reader, ref news);
                    newsList.Add(news);
                }
                reader.Close();
                reader.Dispose();
                return newsList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_News_GetNewsByListNewsId:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }
    }
}
