﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.Entity.External.Kenh14.FacebookImageUpload;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.Kenh14
{
    public class FacebookImageUploadDal
    {
        public static List<FacebookImageUploadEntity> GetByNewsId(long newsId, int pageIndex, int pageSize, int status, ref int totalRow)
        {
            List<FacebookImageUploadEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.FacebookImageUploadMainDal.GetById(newsId, pageIndex, pageSize, status, ref totalRow);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.FacebookImageUploadMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

    }
}
