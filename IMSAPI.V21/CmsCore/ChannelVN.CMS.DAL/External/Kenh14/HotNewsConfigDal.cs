﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.Kenh14
{
    public class HotNewsConfigDal
    {
        public static bool Update(HotNewsConfigEntity hotNewsConfigEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.HotNewsConfigMainDal.Update(hotNewsConfigEntity);
            }
            return returnValue;
        }

        public static bool Delete(int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.HotNewsConfigMainDal.Delete(zoneId);
            }
            return returnValue;
        }
        public static List<HotNewsConfigEntity> GetList()
        {
            List<HotNewsConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.HotNewsConfigMainDal.GetList();

            }
            return returnValue;
        }
    }
}
