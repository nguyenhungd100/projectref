﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.Kenh14
{
    public class BoxTVShowDal
    {
        public static bool Update(BoxTVShowEntity boxTVShowEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTVShowMainDal.Update(boxTVShowEntity);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTVShowMainDal.Delete(id);
            }
            return returnValue;
        }
        public static BoxTVShowEntity GetById(int id)
        {
            BoxTVShowEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTVShowMainDal.GetById(id);

            }
            return returnValue;
        }

        public static List<BoxTVShowEntity> GetList()
        {
            List<BoxTVShowEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTVShowMainDal.GetList();

            }
            return returnValue;
        }
    }
}
