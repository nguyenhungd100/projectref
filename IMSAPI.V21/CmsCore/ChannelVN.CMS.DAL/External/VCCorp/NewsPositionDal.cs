﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.VCCorp
{
    public class NewsPositionDal : Base.NewsPosition.NewsPositionDal
    {
        public static bool SaveLinkPosition(int type, int position, int zoneId, string title, string avatar, string url)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VCCorpNewsPositionMainDal.SaveLinkPosition(type, position, zoneId, title, avatar, url);
            }
            return returnValue;
        }
    }
}
