﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.VTV
{
    public class StatisticV3Dal : Base.Statistic.StatisticV3Dal
    {
        public static List<StatisticV3ViewCountEntity> PageViewAll(Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3ViewCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.PageViewAll(fromDate, toDate);
            }
            return returnValue;
        }
        public static List<StatisticV3CategoryEntity> CategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3CategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.CategoryViewCount(cateId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<StatisticV3CategoryEntity> CategoryViewCountSummary(string cateId, Int64 fromDate, Int64 toDate)
        {
            SqlDataReader reader = null;
            try
            {
                var parameters = new[]
                                     {
                                         new SqlParameter("@CateId", cateId),
                                         new SqlParameter("@FromDate", fromDate),
                                         new SqlParameter("@ToDate", toDate)
                                     };
                reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.Statistic),
                                                    CommandType.StoredProcedure,
                                                    DbCommon.DatabaseSchema + "CMS_VTV_Category_View",
                                                    parameters);

                var list = new List<StatisticV3CategoryEntity>();
                while (reader.Read())
                {
                    var single = new StatisticV3CategoryEntity();
                    EntityBase.SetObjectValue(reader, ref single);
                    list.Add(single);
                }
                reader.Close();
                reader.Dispose();
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_VTV_Category_View:{0}", ex.Message));
            }
            finally
            {
                if (null != reader && !reader.IsClosed)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }
        }
    }
}
