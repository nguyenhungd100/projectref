﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.NguoiLaoDong
{
    public class ExternalVideoEmbedDal
    {
        public static bool Insert(ExternalVideoEmbedEntity externalVideoEmbed)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExternalVideoEmbedMainDal.Insert(externalVideoEmbed);
            }
            return returnValue;
        }
        public static bool Delete(int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExternalVideoEmbedMainDal.Delete(zoneId, type);
            }
            return returnValue;
        }
        public static List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type)
        {
            List<ExternalVideoEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExternalVideoEmbedMainDal.GetListByZoneType(zoneId, type);
            }
            return returnValue;
        }
    }
}
