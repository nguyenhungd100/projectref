﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.NguoiLaoDong
{
    public class NewsPositionDal : Base.NewsPosition.NewsPositionDal
    {
        public static bool RemoveNewsDisplayInSlide(string newsIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NguoiLaoDongNewsPositionMainDal.RemoveNewsDisplayInSlide(newsIdList);
            }
            return returnValue;
        }
    }
}
