﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.NewsGallery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeBiz
{
    public class NewsGalleryDal 
    {
        public static bool SaveNewsGallery(NewsGalleryEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.SaveNewsGallery(info, ref id);
            }
            return returnValue;
        }

        public static bool UpdateNewsGallery(NewsGalleryEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.UpdateNewsGallery(info);
            }
            return returnValue;
        }

        public static bool DeleteNewsGallery(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.DeleteNewsGallery(id);
            }
            return returnValue;
        }

        public static NewsGalleryEntity SelectNewsGalleryById(int id)
        {
            NewsGalleryEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.SelectNewsGalleryById(id);
            }
            return returnValue;
        }

        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId)
        {
            List<NewsGalleryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.SelectNewsGalleryByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows)
        {
            List<NewsGalleryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryMainDal.SelectNewsGalleryByNewsId(newsId, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }
    }
}
