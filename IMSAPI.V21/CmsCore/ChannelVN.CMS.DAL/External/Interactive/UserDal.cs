﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.Interactive.Security;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.External.Interactive
{
    public class UserDal
    {
        public static bool AddnewUser(UserEntity user, ref int newUserId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.AddnewUser(user, ref newUserId);
            }
            return returnValue;
        }

        public static bool UpdateUserById(UserEntity user)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateUserById(user);
            }
            return returnValue;
        }

        public static bool DeleteUserById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.DeleteUserById(id);
            }
            return returnValue;
        }
        
        public static bool UpdateUserAvatar(string accountName, string avatar)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateUserAvatar(accountName, avatar);
            }
            return returnValue;
        }

        public static bool UpdateUserStatusByById(int id, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateUserStatusByById(id, status);
            }
            return returnValue;
        }
        public static bool UpdateUserPasswordByById(int id, string password)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateUserPasswordByById(id, password);
            }
            return returnValue;
        }

        public static UserEntity GetUserById(int id)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserById(id);
            }
            return returnValue;
        }
        public static UserEntity GetUserByUsername(string username)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserByUsername(username);
            }
            return returnValue;
        }
        public static UserEntity GetUserByEmail(string email)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserByEmail(email);
            }
            return returnValue;
        }
        public static UserEntity GetUserByUserNameAndActiveCode(string userName, string activeCode)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserByUserNameAndActiveCode(userName, activeCode);
            }
            return returnValue;

        }

        public static List<UserEntity> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.SearchUser(keyword, status, sortOrder, pageIndex, pageSize,
                    ref totalRow);
            }
            return returnValue;
        }
        public static List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserByPermissionIdAndZoneList(permissionId, zoneIds);
            }
            return returnValue;
        }
        public static List<UserEntity> GetUserByPermissionListAndZoneList(string permissionIds, string zoneIds)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserByPermissionListAndZoneList(permissionIds, zoneIds);
            }
            return returnValue;
        }

        public static List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(int permissionId, int zoneId)
        {
            List<UserStandardEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetNormalUserByPermissionIdAndZoneId(permissionId, zoneId);
            }
            return returnValue;
        }

        #region lấy danh sách user theo permissionId và ZoneId
        public static List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetUserWithFullPermissionAndZoneId(permissionId, newsId, userName);
            }
            return returnValue;
        }
        #endregion

        public static string GetOtpSecretKeyByUsername(string username)
        {
            string returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.GetOtpSecretKeyByUsername(username);
            }
            return returnValue;
        }
        public static bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            }
            return returnValue;
        }
        public static bool UpdateActiveCodeForUser(string username, string activeCode)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InteractiveUserMainDal.UpdateActiveCodeForUser(username, activeCode);
            }
            return returnValue;
        }
    }
}
