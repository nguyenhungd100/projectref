﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class FeedBackToExpertArticleDal
    {
        public static FeedBackToExpertArticleEntity GetById(int id)
        {
            FeedBackToExpertArticleEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.FeedBackToExpertArticleMainDal.GetById(id);
            }
            return returnValue;

        }

        public static List<FeedBackToExpertArticleEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FeedBackToExpertArticleEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.FeedBackToExpertArticleMainDal.Search(keyword, status, pageIndex,pageSize,ref totalRow);
            }
            return returnValue;
        }

        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.FeedBackToExpertArticleMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }
    }
}
