﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class ExpertDal
    {
        public static bool Insert(ExpertEntity expertEntity, ref int newsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Insert(expertEntity, ref newsAuthorId);
            }
            return returnValue;
        }

        public static ExpertEntity GetById(int id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Update(ExpertEntity expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Update(expertEntity);
            }
            return returnValue;
        }

        public static List<ExpertEntity> Search(string keyword)
        {
            List<ExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Search(keyword);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.Delete(id);
            }
            return returnValue;
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.GetExpertByNewsId(id);
            }
            return returnValue;
        }

        public static bool InsertExpertToNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.InsertExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool UpdateExpertInNews(ExpertInNews expertEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.UpdateExpertToNews(expertEntity);
            }
            return returnValue;
        }

        public static bool DeleteExpertNewsId(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.DeleteExpertInNews(id);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ExpertMainDal.SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
