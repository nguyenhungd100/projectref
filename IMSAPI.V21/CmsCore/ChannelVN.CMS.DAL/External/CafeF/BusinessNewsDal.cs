﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class BusinessNewsDal
    {
        public static BusinessNewsCrawlerDetailEntity GetById(long id)
        {
            BusinessNewsCrawlerDetailEntity returnValue;
            using (var db = new CafeFBusinessNewsCrawlerDb())
            {
                returnValue = db.BusinessNewsMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool UpdateStatus(long id, int status)
        {
            bool returnValue;
            using (var db = new CafeFBusinessNewsCrawlerDb())
            {
                returnValue = db.BusinessNewsMainDal.UpdateStatus(id, status);
            }
            return returnValue;
        }

        public static List<BusinessNewsCrawlerSourceEntity> GetAll()
        {
            List<BusinessNewsCrawlerSourceEntity> returnValue;
            using (var db = new CafeFBusinessNewsCrawlerDb())
            {
                returnValue = db.BusinessNewsMainDal.GetAll();
            }
            return returnValue;
        }

        public static List<BusinessNewsCrawlerDetailEntity> SearchNews(int source, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BusinessNewsCrawlerDetailEntity> returnValue;
            using (var db = new CafeFBusinessNewsCrawlerDb())
            {
                returnValue = db.BusinessNewsMainDal.SearchNews(source, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        //public static bool Delete(int id)
        //{
        //    bool returnValue;
        //    using (var db = new CmsMainDb())
        //    {
        //        returnValue = db.ExpertMainDal.Delete(id);
        //    }
        //    return returnValue;
        //}
    }
}
