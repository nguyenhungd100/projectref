﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.BoxExpertNewsEmbed;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class BoxExpertNewsEmbedDal
    {
        public static List<BoxExpertNewsEmbedListEntity> GetListBoxExpertNewsEmbed(int zoneId, int type)
        {
            List<BoxExpertNewsEmbedListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxExpertNewsEmbedMainDal.GetListNewsEmbedBoxOnPage(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxExpertNewsEmbedEntity newsEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxExpertNewsEmbedMainDal.Insert(newsEmbebBox);
            }
            return returnValue;
        }

        public static void Update(string listNewsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxExpertNewsEmbedMainDal.Update(listNewsId, zoneId, type);
            }
        }

        public static void Delete(long newsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxExpertNewsEmbedMainDal.Delete(newsId, zoneId, type);
            }
        }
    }
}
