﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.CafeF.NewsGallery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class NewsGalleryDal 
    {
        public static bool SaveNewsGallery(NewsGalleryEntity info, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.SaveNewsGallery(info, ref id);
            }
            return returnValue;
        }

        public static bool UpdateNewsGallery(NewsGalleryEntity info)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.UpdateNewsGallery(info);
            }
            return returnValue;
        }

        public static bool DeleteNewsGallery(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.DeleteNewsGallery(id);
            }
            return returnValue;
        }

        public static NewsGalleryEntity SelectNewsGalleryById(int id)
        {
            NewsGalleryEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.SelectNewsGalleryById(id);
            }
            return returnValue;
        }

        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId)
        {
            List<NewsGalleryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.SelectNewsGalleryByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows)
        {
            List<NewsGalleryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGalleryCafeFMainDal.SelectNewsGalleryByNewsId(newsId, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }
    }
}
