﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class StatisticV3Dal : Base.Statistic.StatisticV3Dal
    {
        public static List<StatisticV3ViewCountEntity> PageViewAll(Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3ViewCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.PageViewAll(fromDate, toDate);
            }
            return returnValue;
        }

        public static List<StatisticV3CategoryEntity> CategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            List<StatisticV3CategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StatisticV3MainDal.CategoryViewCount(cateId, fromDate, toDate);
            }
            return returnValue;
        }
    }
}
