﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.CafeF
{
    public class EmbedAlbumDal
    {
        public static bool Update(EmbedAlbumEntity embedAlbum)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumCafeFMainDal.Update(embedAlbum);

            }
            return returnValue;
        }
        public static bool Insert(EmbedAlbumEntity embedAlbum, ref int newEmbedAlbumId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumCafeFMainDal.Insert(embedAlbum, ref newEmbedAlbumId);

            }
            return returnValue;
        }
    }
}
