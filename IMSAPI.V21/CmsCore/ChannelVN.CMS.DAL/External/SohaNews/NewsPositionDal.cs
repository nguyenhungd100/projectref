﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.SohaNews
{
    public class NewsPositionDal : Base.NewsPosition.NewsPositionDal
    {
        public static bool SaveLinkPosition(int type, int position, int zoneId, string title, string avatar, string url)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SohaNewsNewsPositionMainDal.SaveLinkPosition(type, position, zoneId, title, avatar, url);
            }
            return returnValue;
        }
    }
}
