﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.SohaNews
{
    public class NewsPositionForZoneSportDal
    {
        public static List<NewsPositionForZoneSportEntity> GetListByTypeAndZoneId(int typeId, int zoneId, string listOfOrder = "")
        {
            List<NewsPositionForZoneSportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SohaNewsNewsPositionForZoneSportMainDal.GetListByTypeAndZoneId(typeId, zoneId, listOfOrder);
            }
            return returnValue;
        }

        public static bool SaveNewsPosition(int typeId, int zoneId, int order, long newsId, DateTime expiredLock, string avatar, int avatarIndex, bool checkNewsExists = true)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SohaNewsNewsPositionForZoneSportMainDal.SaveNewsPosition(typeId, zoneId, order, newsId, expiredLock, avatar, avatarIndex, checkNewsExists);
            }
            return returnValue;
        }

        public static bool UpdateForBomd(int typeId, int zoneId, int order, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SohaNewsNewsPositionForZoneSportMainDal.UpdateForBomd(typeId, zoneId, order, newsId);
            }
            return returnValue;
        }

        public static bool UpdateForUnlockedPosition(int typeId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.SohaNewsNewsPositionForZoneSportMainDal.UpdateForUnlockedPosition(typeId, zoneId);
            }
            return returnValue;
        }
    }
}
