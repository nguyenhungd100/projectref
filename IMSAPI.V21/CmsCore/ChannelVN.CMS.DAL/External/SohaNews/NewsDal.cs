﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.External.SohaNews
{
    public class NewsDal
    {
        public static bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSohaMainDal.InsertNews(news, zoneId, zoneIdList, tagIdList,
                                   tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSohaMainDal.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary,
                    newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote,
                    sourceId);
            }
            return returnValue;
        }
    }
}
