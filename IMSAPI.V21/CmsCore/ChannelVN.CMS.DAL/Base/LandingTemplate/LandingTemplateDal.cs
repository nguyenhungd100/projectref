﻿using ChannelVN.CMS.Entity.Base.LandingTemplate;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.LandingTemplate
{
    public class LandingTemplateDal
    {
        #region LandingTemplate
        public static List<LandingTemplateEntity> Search(string keyword, int categoryId, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<LandingTemplateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.Search(keyword, categoryId, status, type, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
        public static LandingTemplateEntity GetById(int id)
        {
            LandingTemplateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.GetById(id);

            }
            return returnValue;
        }
        public static bool Insert(LandingTemplateEntity obj, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.Insert(obj, ref id);

            }
            return returnValue;
        }
        public static bool Update(LandingTemplateEntity obj)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.Update(obj);

            }
            return returnValue;
        }
        public static bool UpdateStatus(int id, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.UpdateStatus(id, status);

            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.Delete(id);

            }
            return returnValue;

        }
        #endregion

        #region LandingTemplateCategory
        public static List<LandingTemplateCategoryEntity> GetAllCategory()
        {
            List<LandingTemplateCategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.GetAllCategory();

            }
            return returnValue;
        }
        public static LandingTemplateCategoryEntity GetCategoryById(int id)
        {
            LandingTemplateCategoryEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.GetCategoryById(id);

            }
            return returnValue;
        }
        public static bool InsertCategory(LandingTemplateCategoryEntity obj, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.InsertCategory(obj, ref id);

            }
            return returnValue;
        }
        public static bool UpdateCategory(LandingTemplateCategoryEntity obj)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.UpdateCategory(obj);

            }
            return returnValue;
        }        
        public static bool DeleteCategory(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LandingTemplateMainDal.DeleteCategory(id);

            }
            return returnValue;

        }
        #endregion
    }
}
