﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsNotify
{
    public class NewsNotifyDal
    {
        public static bool InsertNews(NewsNotifyEntity news)
        {
            bool returnValue;
            using (var db = new AppLogNotifyDb())
            {
                returnValue = db.NewsNotifyMainDal.InsertNews(news);
            }
            return returnValue;
        }
        public static bool Notify_InsertNews(NewsNotifyEntity news)
        {
            bool returnValue;
            using (var db = new AppLogNotifyDb())
            {
                returnValue = db.NewsNotifyMainDal.Notify_InsertNews(news);
            }
            return returnValue;
        }
        public static bool InsertAppNews(NewsAppEntity news)
        {
            bool returnValue;
            using (var db = new AppLogNotifyDb())
            {
                returnValue = db.NewsNotifyMainDal.InsertNewsApp(news);
            }
            return returnValue;
        }
    }
}
