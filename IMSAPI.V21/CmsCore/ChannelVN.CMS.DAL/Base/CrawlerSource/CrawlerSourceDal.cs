﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.CrawlerSource;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.CrawlerSource
{
    public class CrawlerSourceDal
    {
        public static bool InsertCrawlerSource(CrawlerSourceTopicWrapperEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CrawlerSourceMainDal.InsertCrawlerSource(news);
            }
            return returnValue;
        }

        public static List<CrawlerSourceEntity> GetSourceByTopic(string userName, int topicId)
        {
            List<CrawlerSourceEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CrawlerSourceMainDal.GetSourceByTopic(userName, topicId);
            }
            return returnValue;
        }

        public static List<CrawlerSourceEntity> GetSourceByUser(string userName)
        {
            List<CrawlerSourceEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CrawlerSourceMainDal.GetSource(userName);
            }
            return returnValue;
        }

        public static List<CrawlerSourceTopicEntity> GetTopicByCrawlerSource(int crawlerSource)
        {
            List<CrawlerSourceTopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.CrawlerSourceMainDal.GetTopicByCrawlerSource(crawlerSource);
            }
            return returnValue;
        }

    }
}
