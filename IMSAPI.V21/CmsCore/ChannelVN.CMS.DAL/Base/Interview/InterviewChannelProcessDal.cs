﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewChannelProcessDal
    {
        #region Update

        public static bool Update(int interviewChannelId, string listProcessInterviewChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelProcessMainDal.Update(interviewChannelId, listProcessInterviewChannelId);
            }
            return returnValue;
        }

        #endregion
    }
}
