﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewV3AnswersDal
    {
        #region  GET
        public static List<InterviewV3AnswersEntity> InterviewV3AnswersGetListByQuestionId(int questionId)
        {
            List<InterviewV3AnswersEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersGetListByQuestionId(questionId);
            }
            return returnValue;
        }

        public static List<InterviewV3AnswersCustomEntity> InterviewV3AnswersGetCustomListByQuestionId(int questionId)
        {
            List<InterviewV3AnswersCustomEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersGetCustomListByQuestionId(questionId);
            }
            return returnValue;
        }

        public static InterviewV3AnswersEntity InterviewV3AnswersGetById(int id)
        {
            InterviewV3AnswersEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersGetById(id);
            }
            return returnValue;
        }
        #endregion

        #region UPDATE

        public static bool InterviewV3AnswersInsert(InterviewV3AnswersEntity interviewV3Answers,out int answerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersInsert(interviewV3Answers, out answerId);
            }
            return returnValue;
        }
        public static bool InterviewV3AnswersUpdate(InterviewV3AnswersEntity interviewV3Answers)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersUpdate(interviewV3Answers);
            }
            return returnValue;
        }
        public static bool InterviewV3AnswersUpdateAnswerContent(int Id,string content,string updateBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersUpdateAnswerContent(Id, content, updateBy);
            }
            return returnValue;
        }
        public static bool InterviewV3AnswersUpdateAnswerStatus(int Id, bool status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersUpdateStatus(Id, status);
            }
            return returnValue;
        }
        public static bool InterviewV3AnswersDelete(int answersId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3AnswersDal.InterviewV3AnswersDelete(answersId);
            }
            return returnValue;
        }

        #endregion
    }
}
