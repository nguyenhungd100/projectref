﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewQuestionDal
    {
        #region Get

        public static InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            InterviewQuestionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.GetInterviewQuestionByInterviewQuestionId(interviewQuestionId);
            }
            return returnValue;
        }
        public static List<InterviewQuestionWithSimpleFieldEntity> SearchInterviewQuestion(int interviewId, string keyword, int type, string username, int fieldFilterForUsername, int status, bool invertStatus, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InterviewQuestionWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.SearchInterviewQuestion(interviewId, keyword, type, username, fieldFilterForUsername, status, invertStatus, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<InterviewQuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            List<InterviewQuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.GetAllPublishedQuestionByInterviewId(interviewId);
            }
            return returnValue;
        }
        public static List<QuestionCountByChannelEntity> GetQuestionCountByChannel(int interviewId)
        {
            List<QuestionCountByChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.GetQuestionCountByChannel(interviewId);
            }
            return returnValue;
        }

        #endregion

        #region Working flow

        public static bool ReceivedQuestion(int interviewQuestionId, string receivedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.ReceivedQuestion(interviewQuestionId, receivedBy);
            }
            return returnValue;
        }

        public static bool DistributeQuestion(long interviewQuestionId, int interviewChannelId, string distributedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.DistributeQuestion(interviewQuestionId, interviewChannelId, distributedBy);
            }
            return returnValue;
        }

        public static bool ForwardQuestion(int interviewQuestionId, int interviewChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.ForwardQuestion(interviewQuestionId, interviewChannelId);
            }
            return returnValue;
        }

        public static bool SendAnswerForQuestion(int interviewQuestionId, string answerContent, string answerBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.SendAnswerForQuestion(interviewQuestionId, answerContent, answerBy);
            }
            return returnValue;
        }

        public static bool ReturnQuestion(int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.ReturnQuestion(interviewQuestionId);
            }
            return returnValue;
        }

        public static bool SendWaitForPublish(int interviewQuestionId, string editedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.SendWaitForPublish(interviewQuestionId, editedBy);
            }
            return returnValue;
        }

        public static bool PublishQuestion(long interviewQuestionId, string publishedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.PublishQuestion(interviewQuestionId, publishedBy);
            }
            return returnValue;
        }

        public static bool UnpublishQuestion(int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.UnpublishQuestion(interviewQuestionId);
            }
            return returnValue;
        }

        #endregion

        #region Update

        public static bool Insert(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.Insert(interviewQuestion, ref interviewQuestionId);
            }
            return returnValue;
        }
        public static bool InsertExternalQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.InsertExternalQuestion(interviewQuestion, ref interviewQuestionId);
            }
            return returnValue;
        }
        public static bool InsertExtensionData(int interviewId, string data, string userDoAction, ref int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.InsertExtensionData(interviewId, data, userDoAction, ref interviewQuestionId);
            }
            return returnValue;
        }
        public static bool Update(InterviewQuestionEntity interviewQuestion)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.Update(interviewQuestion);
            }
            return returnValue;
        }

        public static bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.UpdatePriority(interviewId, listOfInterviewQuestionId);
            }
            return returnValue;
        }
        public static bool DeleteQuestion(int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewQuestionMainDal.DeleteQuestion(interviewQuestionId);
            }
            return returnValue;
        }

        #endregion
    }
}
