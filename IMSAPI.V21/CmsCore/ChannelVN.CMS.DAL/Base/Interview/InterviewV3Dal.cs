﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewV3Dal
    {
        #region GET
        public static InterviewV3Entity InterviewV3GetById(int interviewId)
        {
            InterviewV3Entity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3MainDal.InterviewV3GetById(interviewId);
            }
            return returnValue;
        }
        #endregion

        #region UPDATE
        public static bool InterviewV3Insert(InterviewV3Entity interview, out int interviewId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3MainDal.InterviewV3Insert(interview, out interviewId);
            }
            return returnValue;
        }

        public static bool InterviewV3Update(InterviewV3Entity interview)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3MainDal.InterviewV3Update(interview);
            }
            return returnValue;
        }

        public static bool InterviewV3UpdateStatus(int interviewId,int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3MainDal.InterviewV3UpdateStatus(interviewId, status);
            }
            return returnValue;
        }
        #endregion
    }
}
