﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewChannelDal
    {
        public static InterviewChannelEntity GetInterviewChannelByInterviewChannelId(int interviewChannelId)
        {
            InterviewChannelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetInterviewChannelByInterviewChannelId(interviewChannelId);
            }
            return returnValue;
        }
        public static List<InterviewChannelEntity> GetByListOfInterviewId(string listOfInterviewId)
        {
            List<InterviewChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetByListOfInterviewId(listOfInterviewId);
            }
            return returnValue;
        }
        public static List<InterviewChannelEntity> GetChannelDetailByInterviewId(int interviewId, string username)
        {
            List<InterviewChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetChannelDetailByInterviewId(interviewId, username);
            }
            return returnValue;
        }
        public static List<InterviewChannelEntity> GetByInterviewIdAndUsername(int interviewId, string username)
        {
            List<InterviewChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetByInterviewIdAndUsername(interviewId, username);
            }
            return returnValue;
        }
        public static List<InterviewChannelEntity> GetUserOnChannelRole(int interviewId, int channelRole, string username)
        {
            List<InterviewChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetUserOnChannelRole(interviewId, channelRole, username);
            }
            return returnValue;
        }
        public static List<InterviewChannelEntity> GetInterviewChannelProcess(int interviewChannelId)
        {
            List<InterviewChannelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.GetInterviewChannelProcess(interviewChannelId);
            }
            return returnValue;
        }

        public static bool Insert(InterviewChannelEntity interviewChannel, ref int interviewChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.Insert(interviewChannel, ref interviewChannelId);
            }
            return returnValue;
        }
        public static bool Update(InterviewChannelEntity interviewChannel)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.Update(interviewChannel);
            }
            return returnValue;
        }
        public static bool Remove(int interviewChannelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewChannelMainDal.Remove(interviewChannelId);
            }
            return returnValue;
        }
    }
}
