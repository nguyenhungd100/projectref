﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewDal
    {
        #region Get

        public static InterviewEntity GetInterviewByInterviewId(int interviewId)
        {
            InterviewEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.GetInterviewByInterviewId(interviewId);
            }
            return returnValue;
        }
        public static InterviewWithSimpleFieldEntity GetInterviewShortInfoByInterviewId(int interviewId)
        {
            InterviewWithSimpleFieldEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.GetInterviewShortInfoByInterviewId(interviewId);
            }
            return returnValue;
        }
        public static List<InterviewWithSimpleFieldEntity> SearchInterview(string keyword, int isFocus, int isActived, int status, DateTime startDateFrom, DateTime startDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InterviewWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.SearchInterview(keyword, isFocus, isActived, status, startDateFrom, startDateTo, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<InterviewForSuggestionEntity> SearchInterviewForSuggestion(int top, string keyword, int isFocus, int isActived)
        {
            List<InterviewForSuggestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.SearchInterviewForSuggestion(top, keyword, isFocus, isActived);
            }
            return returnValue;
        }
        public static InterviewPublishedEntity GetInterviewPublishedByInterviewId(int interviewId)
        {
            InterviewPublishedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.GetInterviewPublishedByInterviewId(interviewId);
            }
            return returnValue;
        }

        #endregion

        #region Update

        public static bool Insert(InterviewEntity interview, ref int interviewId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.Insert(interview, ref interviewId);
            }
            return returnValue;
        }
        public static bool Update(InterviewEntity interview)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.Update(interview);
            }
            return returnValue;
        }
        public static bool DeleteById(long interviewId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.DeleteById(interviewId);
            }
            return returnValue;
        }
        public static bool UpdateInterviewFocus(long interviewId, bool isFocus)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.UpdateInterviewFocus(interviewId, isFocus);
            }
            return returnValue;
        }
        public static bool UpdateInterviewActive(long interviewId, bool isActived)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.UpdateInterviewActive(interviewId, isActived);
            }
            return returnValue;
        }
        public static bool PublishInterviewContent(long interviewId, string ascQuestionContent, string descQuestionContent, string publishedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewMainDal.PublishInterviewContent(interviewId, ascQuestionContent, descQuestionContent, publishedBy);
            }
            return returnValue;
        }

        #endregion
    }
}
