﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewV3GuestsDal
    {
        #region GET
        public static InterviewV3GuestsEntity InterviewV3GuestsGetById(int id)
        {
            InterviewV3GuestsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsGetById(id);
            }
            return returnValue;
        }

        public static InterviewV3GuestsEntity InterviewV3GuestsGetInterviewIdAndUserName(int interviewId, string userName)
        {
            InterviewV3GuestsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsGetInterviewIdAndUserName(interviewId, userName);
            }
            return returnValue;
        }

        public static List<InterviewV3GuestsEntity> InterviewV3GuestsGetByInterviewId(int interviewId,bool status)
        {
            List<InterviewV3GuestsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsGetByInterviewId(interviewId, status);
            }
            return returnValue;
        }
        #endregion

        #region UPDATE
        public static bool InterviewV3GuestsInsert(InterviewV3GuestsEntity interviewV3Guests,out int guestId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsInsert(interviewV3Guests, out guestId);
            }
            return returnValue;
        }

        public static bool InterviewV3GuestsUpdateInfo(InterviewV3GuestsEntity interviewV3Guests)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsUpdateInfo(interviewV3Guests);
            }
            return returnValue;
        }

        public static bool InterviewV3GuestsDelete (int guestId, int interviewId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsDelete(guestId, interviewId);
            }
            return returnValue;
        }

        public static bool InterviewV3GuestsUpdateStatus(int guestId, bool status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3GuestsDal.InterviewV3GuestsUpdateStatus(guestId, status);
            }
            return returnValue;
        }
        #endregion
    }
}

