﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.Base.Interview
{
    public class InterviewV3QuestionDal
    {
        #region  GET
        public static List<InterviewV3QuestionEntity> SearchInterviewV3Question(int interviewId, string keyword, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<InterviewV3QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.SearchInterviewV3Question(interviewId, keyword, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByInterviewId(int interviewId)
        {
            List<InterviewV3QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionGetListByInterviewId(interviewId);
            }
            return returnValue;
        }

        public static List<InterviewV3QuestionDisplayListEntity> InterviewV3QuestionSimpleListByInterviewId(int interviewId)
        {
            List<InterviewV3QuestionDisplayListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionSimpleListByInterviewId(interviewId);
            }
            return returnValue;
        }

        public static List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByStatus(int interviewId, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy)
        {
            List<InterviewV3QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionGetListByStatus(interviewId, status, orderBy);
            }
            return returnValue;
        }

        public static InterviewV3QuestionEntity InterviewV3QuestionGetById(int id)
        {
            InterviewV3QuestionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionGetById(id);
            }
            return returnValue;
        }
        public static bool InsertExtensionData(int interviewId, string data, string userDoAction, ref int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InsertExtensionData(interviewId, data, userDoAction, ref interviewQuestionId);
            }
            return returnValue;
        }
        public static bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.UpdatePriority(interviewId, listOfInterviewQuestionId);
            }
            return returnValue;
        }
        public static List<InterviewV3QuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            List<InterviewV3QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.GetAllPublishedQuestionByInterviewId(interviewId);
            }
            return returnValue;
        }
        #endregion

        #region UPDATE
        public static bool InterviewV3QuestionInsert(InterviewV3QuestionEntity interviewV3Question,out int questionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionInsert(interviewV3Question, out questionId);
            }
            return returnValue;
        }

        public static bool InterviewV3QuestionUpdate(InterviewV3QuestionEntity interviewV3Question)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionUpdate(interviewV3Question);
            }
            return returnValue;
        }

        public static bool InterviewV3QuestionUpdatePriority(int questionId,int priority)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionUpdatePriority(questionId, priority);
            }
            return returnValue;
        }

        public static bool InterviewV3QuestionUpdateQuestionContent(int questionId, string questionContent,string updateBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionUpdateQuestionContent(questionId, questionContent, updateBy);
            }
            return returnValue;
        }

        public static bool InterviewV3QuestionUpdateStatus(int questionId, EnumInterviewV3QuestionStatus status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionUpdateQuestionStatus(questionId, status);
            }
            return returnValue;
        }

        public static bool InterviewV3QuestionReceiveQuestion(int questionId, string processUser, EnumInterviewV3QuestionStatus status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.InterviewV3QuestionReceiveQuestion(questionId, processUser, status);
            }
            return returnValue;
        }

        public static bool DeleteQuestion(int interviewQuestionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.InterviewV3QuestionDal.DeleteQuestion(interviewQuestionId);
            }
            return returnValue;
        }
        #endregion
    }
}

