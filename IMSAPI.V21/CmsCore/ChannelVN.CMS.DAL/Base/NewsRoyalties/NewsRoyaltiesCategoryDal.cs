﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsRoyalties
{
    public class NewsRoyaltiesCategoryDal
    {
        #region Get

        public static List<NewsRoyaltiesCategoryEntity> GetAll()
        {
            List<NewsRoyaltiesCategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesCategoryMainDal.GetAll();
            }
            return returnValue;
        }

        #endregion
    }
}
