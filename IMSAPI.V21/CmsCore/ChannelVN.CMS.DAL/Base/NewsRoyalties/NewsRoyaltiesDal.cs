﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsRoyalties
{
    public class NewsRoyaltiesDal
    {
        #region Get

        public static List<NewsRoyaltiesEntity> GetByNewsId(long newsId)
        {
            List<NewsRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }

        public static NewsRoyaltiesEntity GetById(long id)
        {
            NewsRoyaltiesEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<NewsRoyaltiesEntity> Search(string userName, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.Search(userName, royaltiesCategoryId, royaltiesCategoryId, keyword, royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion

        public static bool Insert(NewsRoyaltiesEntity royalties, ref long newRoyaltiesId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.Insert(royalties, ref newRoyaltiesId);
            }
            return returnValue;
        }

        public static bool Update(NewsRoyaltiesEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.Update(royalties);
            }
            return returnValue;
        }

        public static bool UpdateValue(NewsRoyaltiesEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.UpdateValue(royalties);
            }
            return returnValue;
        }

        public static bool UpdateMediaNumber(NewsRoyaltiesEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.UpdateMediaNumber(royalties);
            }
            return returnValue;
        }

        public static bool UpdateLevelId(NewsRoyaltiesEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.UpdateLevelId(royalties);
            }
            return returnValue;
        }

        public static bool Delete(int royaltiesId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.Delete(royaltiesId);
            }
            return returnValue;
        }

        public static bool DeleteByNewsId(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesMainDal.DeleteByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
