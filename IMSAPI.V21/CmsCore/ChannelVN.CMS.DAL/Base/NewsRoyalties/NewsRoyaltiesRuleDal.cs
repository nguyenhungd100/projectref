﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsRoyalties
{
    public class NewsRoyaltiesRuleDal
    {
        #region Get

        public static List<NewsRoyaltiesRuleEntity> GetListRoleLevel()
        {
            List<NewsRoyaltiesRuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.GetListRoleLevel();
            }
            return returnValue;
        }

        public static NewsRoyaltiesRuleEntity GetValueByRoleLevelCategoryId(int roleId, int levelId, int categoryId)
        {
            NewsRoyaltiesRuleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.GetValueByRoleLevelCategoryId(roleId, levelId, categoryId);
            }
            return returnValue;
        }

        public static NewsRoyaltiesRuleEntity GetValueByRoleLevelMediaId(int roleId, int levelId, int mediaId)
        {
            NewsRoyaltiesRuleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.GetValueByRoleLevelMediaId(roleId, levelId, mediaId);
            }
            return returnValue;
        }

        public static List<NewsRoyaltiesRuleEntity> GetAll()
        {
            List<NewsRoyaltiesRuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.GetAll();
            }
            return returnValue;
        }

        #endregion

        public static bool UpdateValueByRoleLevelCategoryId(NewsRoyaltiesRuleEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.UpdateValueByRoleLevelCategoryId(royalties);
            }
            return returnValue;
        }

        public static bool UpdateValueByRoleLevelMediaId(NewsRoyaltiesRuleEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesRuleMainDal.UpdateValueByRoleLevelMediaId(royalties);
            }
            return returnValue;
        }
    }
}
