﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsRoyalties
{
    public class NewsRoyaltiesLevelDal
    {
        #region Get

        public static List<NewsRoyaltiesLevelEntity> GetAll()
        {
            List<NewsRoyaltiesLevelEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsRoyaltiesLevelMainDal.GetAll();
            }
            return returnValue;
        }

        #endregion
    }
}
