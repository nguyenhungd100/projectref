﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsPosition
{
    public class NewsPositionTypeDal
    {
        public static List<NewsPositionTypeEntity> GetAll()
        {
            List<NewsPositionTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionTypeMainDal.GetAll();
            }
            return returnValue;
        }
    }
}
