﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsPosition
{
    public class NewsPositionScheduleDal
    {
        public static bool Insert(NewsPositionScheduleEntity newsPosition)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.Insert(newsPosition);
            }
            return returnValue;
        }
        public static bool Update(NewsPositionScheduleEntity newsPosition)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.Update(newsPosition);
            }
            return returnValue;
        }
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.Delete(id);
            }
            return returnValue;
        }
        public static List<NewsPositionScheduleEntity> GetListScheduleByPositionTypeId(int positionTypeId)
        {
            List<NewsPositionScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.GetListScheduleByPositionTypeId(positionTypeId);
            }
            return returnValue;
        }
        public static List<NewsPositionEntity> GetListScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.GetListScheduleByPositionTypeIdAndOrder(typeId, order, zoneId);
            }
            return returnValue;
        }
        public static List<NewsPositionScheduleEntity> GetUnprocessItem(int maxProcess)
        {
            List<NewsPositionScheduleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.GetUnprocessItem(maxProcess);
            }
            return returnValue;
        }
        public static bool UpdateProcessedState(string listProcessIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionScheduleMainDal.UpdateProcessedState(listProcessIds);
            }
            return returnValue;
        }
    }
}
