﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsPosition
{
    public class NewsPositionDal
    {
        public static List<NewsPositionEntity> GetListByTypeAndZoneId(int typeId, int zoneId, string listOfOrder = "")
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetListByTypeAndZoneId(typeId, zoneId, listOfOrder);
            }
            return returnValue;
        }
        public static List<NewsPositionEntity> GetListNewsPositionAllBom(int typeId, int zoneId)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetListNewsPositionAllBom(typeId, zoneId);
            }
            return returnValue;
        }
        public static List<NewsPositionEntity> GetListByTypeAndZoneIdWithOrderByDistritbutionDate(int typeId, int zoneId, int top)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetListByTypeAndZoneIdWithOrderByDistritbutionDate(typeId, zoneId, top);
            }
            return returnValue;
        }
        public static bool LockPosition(long id, int lockForZoneId, DateTime expiredLock)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.LockPosition(id, lockForZoneId, expiredLock);
            }
            return returnValue;
        }
        public static bool UnlockPosition(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UnlockPosition(id);
            }
            return returnValue;
        }

        public static NewsPositionEntity GetNewsPositionById(long id)
        {
            NewsPositionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetNewsPositionById(id);
            }
            return returnValue;
        }
        public static bool SaveLinkPosition(int typeId, int position, int zoneId, long newsId, int type, string title, string avatar, string url, string sapo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.SaveLinkPosition(typeId, position, zoneId, newsId, type, title, avatar, url, sapo);
            }
            return returnValue;
        }

        public static bool SaveNewsPosition(int typeId, int zoneId, int order, long newsId, DateTime expiredLock, string avatar, int avatarIndex, bool checkNewsExists = true)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.SaveNewsPosition(typeId, zoneId, order, newsId, expiredLock, avatar, avatarIndex, checkNewsExists);
            }
            return returnValue;
        }

        public static bool SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId, string listScheduleDate, string listRemovePosition)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.SaveNewsPositionSchedule(typeId, zoneId, order, listNewsId, listScheduleDate, listRemovePosition);
            }
            return returnValue;
        }

        public static bool UpdateForUnlockedPosition(int typeId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateForUnlockedPosition(typeId, zoneId);
            }
            return returnValue;
        }
        public static bool UpdateExpiredPosition()
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateExpiredPosition();
            }
            return returnValue;
        }
        public static bool UpdatePositionOrder(int typeId, int zoneId, string listPositionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdatePositionOrder(typeId, zoneId, listPositionId);
            }
            return returnValue;
        }
        public static bool UpdatePositionOrderByPositionId(long positionId, int positionOrder)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdatePositionOrderByPositionId(positionId, positionOrder);
            }
            return returnValue;
        }
        public static bool UpdateForBomd(int typeId, int zoneId, int order, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateForBomd(typeId, zoneId, order, newsId);
            }
            return returnValue;
        }

        public static List<NewsPositionForCheckAutoUpdateEntity> GetListNewsByListNewsId(int typeId, int zoneId, string listNewsId)
        {
            List<NewsPositionForCheckAutoUpdateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetListNewsByListNewsId(typeId, zoneId, listNewsId);
            }
            return returnValue;
        }

        public static List<NewsPositionForCheckAutoUpdateEntity> GetListNewsByListNewsIdAndTypeId(int typeId, string listNewsId)
        {
            List<NewsPositionForCheckAutoUpdateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetListNewsByListNewsIdAndTypeId(typeId, listNewsId);
            }
            return returnValue;
        }
        public static bool UpdateNewsPositionForAutoUpdate(long newsPositionId, NewsPublishEntity newsPublish)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateNewsPositionForAutoUpdate(newsPositionId, newsPublish);
            }
            return returnValue;
        }
        public static bool UpdateNewsInfoIntoNewsPosition(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateNewsInfoIntoNewsPosition(newsId);
            }
            return returnValue;
        }
        public static bool UpdateDistributionDate(long newsId, DateTime distributionDate)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateDistributionDate(newsId, distributionDate);
            }
            return returnValue;
        }
        public static List<NewsPositionTypeEntity> GetAllNewsPositionType()
        {
            List<NewsPositionTypeEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetAllNewsPositionType();
            }
            return returnValue;
        }
        public static NewsPositionTypeEntity GetNewsPositionTypeById(int typeId)
        {
            NewsPositionTypeEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetNewsPositionTypeById(typeId);
            }
            return returnValue;
        }
        public static NewsPositionEntity GetNewsInfoInPosition(int typeId, int zoneId, long newsId)
        {
            NewsPositionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetNewsInfoInPosition(typeId, zoneId, newsId);
            }
            return returnValue;
        }
        public static int GetCountNewsInPositionByDistributionDate(int typeId, int zoneId, DateTime distributionDate)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetCountNewsInPositionByDistributionDate(typeId, zoneId, distributionDate);
            }
            return returnValue;
        }
        public static NewsPositionEntity GetOldestPosition(int typeId, int zoneId)
        {
            NewsPositionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetOldestPosition(typeId, zoneId);
            }
            return returnValue;
        }
        public static NewsPositionEntity GetOldestPositionIncludeNewsId(int typeId, int zoneId, long newsId)
        {
            NewsPositionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetOldestPositionIncludeNewsId(typeId, zoneId, newsId);
            }
            return returnValue;
        }

        /// <summary>
        /// Update news into Auto update position in NewsPosition table
        /// </summary>
        /// <param name="typeId">TypeId</param>
        /// <param name="listZoneId">ListZoneId in NewsPosition</param>
        /// <param name="newsId">NewsId for update</param>
        /// <param name="zoneIdForNews">ZoneId for this news (ZoneIdForNews!=0 => Use this ZoneId; else use Zone in each ListZoneId)</param>
        /// <returns></returns>
        public static bool UpdateNewsIntoAutoUpdatePosition(int typeId, string listZoneId, long newsId, int zoneIdForNews)
        {
            listZoneId = listZoneId.Replace(';', ',');

            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateNewsIntoAutoUpdatePosition(typeId, listZoneId, newsId, zoneIdForNews);
            }
            return returnValue;
        }
        /// <summary>
        /// Update news into manual update position
        /// </summary>
        /// <param name="listTypeId">List NewsPosition TypeId (ngan cach boi , )</param>
        /// <param name="newsId">NewsId</param>
        /// <returns></returns>
        public static bool UpdateNewsIntoManualUpdatePosition(string listTypeId, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UpdateNewsIntoManualUpdatePosition(listTypeId, newsId);
            }
            return returnValue;
        }
        /// <summary>
        /// Remove news from auto update position in NewsPosition table
        /// </summary>
        /// <param name="newsId">NewsId for update</param>
        /// <returns></returns>
        public static bool RemoveNewsFromAutoUpdatePosition(int typeId, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.RemoveNewsFromAutoUpdatePosition(typeId, newsId);
            }
            return returnValue;
        }
        public static List<NewsPositionEntity> GetAllPositionHasNewsId(long newsId)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetAllPositionHasNewsId(newsId);
            }
            return returnValue;
        }

        public static List<NewsPositionEntity> GetAllPositionHasIds(string ids)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetAllPositionHasIds(ids);
            }
            return returnValue;
        }

        public static bool LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.LockPositionByType(newsPositionId, lockTypeId, order, expiredLock);
            }
            return returnValue;
        }
        public static bool LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.LockPositionByNewsId(newsId, zoneId, lockTypeId, order, expiredLock);
            }
            return returnValue;
        }
        public static bool UnlockPositionByType(int lockTypeId, int order)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.UnlockPositionByType(lockTypeId, order);
            }
            return returnValue;
        }

        #region NewsPositionSchedule
        public static List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId)
        {
            List<NewsPositionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionMainDal.GetScheduleByTypeAndOrder(typeId, order, zoneId);
            }
            return returnValue;
        }
        #endregion
    }
}
