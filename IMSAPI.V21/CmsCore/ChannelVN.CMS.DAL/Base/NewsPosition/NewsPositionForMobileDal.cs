﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsPosition
{
    public class NewsPositionForMobileDal
    {
        public static List<NewsPositionForMobileEntity> GetListByTypeAndZoneId(int typeId, int zoneId)
        {
            List<NewsPositionForMobileEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionForMobileMainDal.GetListByTypeAndZoneId(typeId, zoneId);
            }
            return returnValue;
        }

        public static bool SaveNewsPosition(int typeId, int zoneId, int order, long newsId, DateTime expiredLock, string avatar, int avatarIndex, bool checkNewsExists = true)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPositionForMobileMainDal.SaveNewsPosition(typeId, zoneId, order, newsId, expiredLock, avatar, avatarIndex, checkNewsExists);
            }
            return returnValue;
        }
    }
}
