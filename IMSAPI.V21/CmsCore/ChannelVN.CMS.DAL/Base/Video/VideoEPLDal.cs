﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoEPLDal
    {
        public static List<VideoEPLEntity> VideoEPLByZone(int zoneId, DateTime dateFrom, DateTime dateTo)
        {
            List<VideoEPLEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.VideoEPLByZone(zoneId, dateFrom, dateTo);
            }
            return returnValue;
        }
        public static List<VideoEPLZoneToZoneEntity> VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo)
        {
            List<VideoEPLZoneToZoneEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.VideoEPLZoneAndZone(zoneId1, zoneId2, dateFrom, dateTo);
            }
            return returnValue;
        }
        public static List<EPLPlayListEntity> EPLPlayListGetAll()
        {
            List<EPLPlayListEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.EPLPlayListGetAll();
            }
            return returnValue;
        }

        public static List<EPLPlayListEntity> EPLPlayListGetByUser(string userName)
        {
            List<EPLPlayListEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.EPLPlayListGetByUser(userName);
            }
            return returnValue;
        }

        public static bool Insert(string userName, int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.Insert(userName, videoId);
            }
            return returnValue;
        }

        public static bool Delete(int userId, int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoEPLMainDal.Delete(userId, videoId);
            }
            return returnValue;
        }
    }
}
