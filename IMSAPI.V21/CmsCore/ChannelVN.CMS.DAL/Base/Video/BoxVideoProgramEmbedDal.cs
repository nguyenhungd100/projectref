﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class BoxVideoProgramEmbedDal
    {
        public static List<ZoneVideoEntity> SearchProgramEmbed(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoProgramEmbedMainDal.SearchProgramEmbed(zoneId, keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<BoxVideoProgramEmbedEntity> GetListVideoProgramEmbed(int zoneId, int type)
        {
            List<BoxVideoProgramEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoProgramEmbedMainDal.GetListVideoProgramEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static bool Update(string listVideoId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoProgramEmbedMainDal.Update(listVideoId, zoneId, type);
            }
            return returnValue;
        }
        
    }
}
