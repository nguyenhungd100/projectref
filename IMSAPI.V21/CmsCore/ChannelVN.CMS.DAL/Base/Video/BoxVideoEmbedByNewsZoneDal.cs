﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class BoxVideoEmbedByNewsZoneDal
    {
        public static List<BoxVideoEmbedByNewsZoneEntity> GetListVideoEmbed(int zoneId, int type)
        {
            List<BoxVideoEmbedByNewsZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedByNewsZoneMainDal.GetListVideoEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxVideoEmbedByNewsZoneEntity VideoEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedByNewsZoneMainDal.Insert(VideoEmbebBox);
            }
            return returnValue;
        }

        public static bool Update(string listVideoId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedByNewsZoneMainDal.Update(listVideoId, zoneId, type);
            }
            return returnValue;
        }

        public static bool Delete(long VideoId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedByNewsZoneMainDal.Delete(VideoId, zoneId, type);
            }
            return returnValue;
        }
    }
}
