using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class ZoneVideoDal
    {
        public static List<ZoneVideoEntity> GetListByParentId(int parentId, int status)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetListByParentId(parentId, status);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetZoneVideoActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetZoneVideoActivedByUsernameAndPermissionIds(username, permissionIds);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetListZoneRelation(int videoId)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetListZoneRelation(videoId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetListZoneRelationByPlayListId(int playListId)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetListZoneRelationByPlayListId(playListId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetListZoneRelationByChannelId(int channelId)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetListZoneRelationByChannelId(channelId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetActiveZoneInDay(zoneIds,viewDate,mode);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetActiveZoneInDayV2(zoneIds, viewDate, mode);
            }
            return returnValue;
        }

        public static List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            List<VideoModeNotifyEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetNotifyZoneInDay(viewDate);
            }
            return returnValue;
        }

        public static ZoneVideoEntity GetById(int id)
        {
            ZoneVideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Insert(ZoneVideoEntity zoneVideo, string listVideoTagId, ref int zoneVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.Insert(zoneVideo, listVideoTagId, ref zoneVideoId);
            }
            return returnValue;
        }

        public static bool InsertInitDB(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.InsertInitDB(zoneVideo, listVideoTagId);
            }
            return returnValue;
        }

        public static bool Update(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.Update(zoneVideo, listVideoTagId);
            }
            return returnValue;
        }

        public static bool MoveUp(int zoneVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.MoveUp(zoneVideoId);
            }
            return returnValue;
        }

        public static bool MoveDown(int zoneVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.MoveDown(zoneVideoId);
            }
            return returnValue;
        }

        public static bool Delete(int zoneVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.Delete(zoneVideoId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetListByParentIdFromExternal(int parentId, int status)
        {
            List<ZoneVideoEntity> returnValue;
            using (var db = new ExternalCmsVideoDb())
            {
                returnValue = db.ZoneVideoExternalMainDal.GetListByParentId(parentId, status);
            }
            return returnValue;
        }

        public static bool GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.GetZoneVideoIdByNewsZoneId(newsZoneId, ref zoneVideoId);
            }
            return returnValue;
        }

        public static bool UpdateAvatarZoneVideo(string listAvatar)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.ZoneVideoMainDal.UpdateAvatarZoneVideo(listAvatar);
            }
            return returnValue;
        }
    }
}
