﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoDal
    {
        #region Sets
        public static bool UpdateConvertedMode(string listVideoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateConvertedMode(listVideoId);
            }
            return returnValue;
        }

        public static bool Insert(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Insert(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);
            }
            return returnValue;
        }

        public static bool InsertV3(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertV3(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
            }
            return returnValue;
        }

        public static bool InsertV4(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertV4(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
            }
            return returnValue;
        }

        public static bool InsertVideoInitDB(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertVideoInitDB(videoEntity, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList, ref videoId);
            }
            return returnValue;
        }

        public static bool InsertVideoInfo(VideoEntity videoEntity, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertVideoInfo(videoEntity, ref videoId);
            }
            return returnValue;
        }

        public static bool InsertV2(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertV2(videoEntity, tagIdList, zoneIdList, playlistIdList, ref videoId);
            }
            return returnValue;
        }
      
        public static bool Update(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Update(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
            }
            return returnValue;
        }

        public static bool UpdateV4(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateV4(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
            }
            return returnValue;
        }

        public static bool UpdateV5(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateV5(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList, channelIdList, authorList);
            }
            return returnValue;
        }

        public static bool UpdateVideoInfo(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateVideoInfo(videoEntity);
            }
            return returnValue;
        }

        public static bool UpdateV2(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateV2(videoEntity, tagIdList, tagNameList, zoneIdList, playlistIdList);
            }
            return returnValue;
        }

        public static bool UpdateName(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateName(videoEntity);
            }
            return returnValue;
        }

        public static bool UpdateNameAndDescription(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateNameAndDescription(videoEntity);
            }
            return returnValue;
        }

        public static bool UpdateMode(int mode, int id, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateMode(mode, id, userDoAction);
            }
            return returnValue;
        }

        public static bool UpdateStatusVideoByHashId(string hashId,string keyVideo, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateStatusVideoByHashId(hashId, keyVideo, userDoAction);
            }
            return returnValue;
        }

        public static bool UpdateListVideoViews(string listKeyVideo, string listViewVideo)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateListVideoViews(listKeyVideo, listViewVideo);
            }
            return returnValue;
        }
        public static bool UpdateViews(string keyVideo, int views)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateViews(keyVideo, views);
            }
            return returnValue;
        }

        public static bool Send(int videoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Send(videoId, userDoAction);
            }
            return returnValue;
        }

        public static bool Return(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Return(videoId);
            }
            return returnValue;
        }

        public static bool Publish(int videoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Publish(videoId, userDoAction);
            }
            return returnValue;
        }

        public static bool PublishInNews(int videoId, string userDoAction, DateTime distributionDate)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.PublishInNews(videoId, userDoAction, distributionDate);
            }
            return returnValue;
        }

        public static bool Unpublish(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Unpublish(videoId);
            }
            return returnValue;
        }

        public static bool UpdateAvatar(string avatar, int id, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateAvatar(avatar, id, userDoAction);
            }
            return returnValue;
        }       

        public static bool AddVideoFileToVideo(VideoEntity videoEntity, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.AddVideoFileToVideo(videoEntity, ref videoId);
            }
            return returnValue;
        }

        public static bool UpdateNewsIdForList(string idList, long newsId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateNewsIdForList(idList, newsId);
            }
            return returnValue;
        }

        public static bool DeleteById(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.DeleteById(videoId);
            }
            return returnValue;
        }

        public static bool DeleteByIdList(string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.DeleteByIdList(videoIdList);
            }
            return returnValue;
        }

        public static bool ChangeStatus(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.ChangeStatus(videoId);
            }
            return returnValue;
        }

        public static bool ChangeStatusByList(string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.ChangeStatusByList(videoIdList);
            }
            return returnValue;
        }

        public static bool RemoveVideoFromPlaylist(int videoId, int playlistId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.RemoveVideoFromPlaylist(videoId, playlistId);
            }
            return returnValue;
        }

        public static bool SyncNewsAndVideo(string videoIdList, long newsId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SyncNewsAndVideo(videoIdList, newsId);
            }
            return returnValue;
        }
        public static bool SyncNewsAndVideoWithStatus(string videoIdList, long newsId, int status, int excludeStatus)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SyncNewsAndVideoWithStatus(videoIdList, newsId, status, excludeStatus);
            }
            return returnValue;
        }

        public static bool UpdateKeyVideoByFileName(string keyvideo, string fileName)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateKeyVideoByFileName(keyvideo, fileName);
            }
            return returnValue;
        }

        #endregion

        #region Gets
        public static List<VideoKeyEntity> GetListVideoWaitConvert(int top)
        {
            List<VideoKeyEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoWaitConvert(top);
            }
            return returnValue;
        }

        public static VideoEntity GetById(int videoId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetById(videoId, ref zoneName);
            }
            return returnValue;
        }

        public static VideoEntity GetVideoByHashId(string hashId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoByHashId(hashId, ref zoneName);
            }
            return returnValue;
        }

        public static VideoEntity GetByVideoExternalId(string videoExternalId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetByVideoExternalId(videoExternalId, ref zoneName);
            }
            return returnValue;
        }

        public static VideoEntity GetByIdV2(int videoId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetByIdV2(videoId, ref zoneName);
            }
            return returnValue;
        }

        public static VideoEntity GetByFileName(string fileName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetByFileName(fileName);
            }
            return returnValue;
        }
        public static VideoEntity GetByKeyvideo(string keyvideo, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetByKeyvideo(keyvideo, ref zoneName);
            }
            return returnValue;
        }

        public static VideoEntity GetByVideoFromYoutubeId(int videoFromYoutubeId)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetByVideoFromYoutubeId(videoFromYoutubeId);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListByIdList(string videoIdList)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListByIdList(videoIdList);
            }
            return returnValue;
        }

        public static List<VideoByAuthorEntity> GetListAuthorInVideo(long videoId)
        {
            List<VideoByAuthorEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListAuthorInVideo(videoId);
            }
            return returnValue;
        }

        public static List<string> GetLastPublishedForUpdateView(DateTime fromDate)
        {
            List<string> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetLastPublishedForUpdateView(fromDate);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetAllVideoInPlaylist(int playlistId)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetAllVideoInPlaylist(playlistId);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetAllVideoInPlaylistByMode(int playlistId, int mode)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetAllVideoInPlaylistByMode(playlistId, mode);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow, ref int maxPriority)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow, ref maxPriority);
            }
            return returnValue;
        }

        public static VideoEntity GetVideoNewByPlayListId(int playlistId)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoNewByPlayListId(playlistId);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetAllVideoInVideoThread(int videoThreadId)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetAllVideoInVideoThread(videoThreadId);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListPagingByZone(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListPagingByZone(pageIndex, pageSize, zoneId, playlistId, status, keyword, sortOder, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoNotPublishCountInPlaylist> GetVideoNotPublishCount(string playlistIds)
        {
            List<VideoNotPublishCountInPlaylist> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoNotPublishCount(playlistIds);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListPagingInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListPagingInPlaylist(pageIndex, pageSize, playlistId, status, keyword, sortOder, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int order, int zoneVideoId, int playlistId, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListMyVideo(username, pageIndex, pageSize, keyword, status, order, zoneVideoId, playlistId, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> Search(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Search(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoEntity> InitESAllVideo(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InitESAllVideo(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoEntity> SearchForList(string username, int zoneVideoId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchForList(username, zoneVideoId, keyword, status, order, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoDistributionEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchDistribution(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoDistributionEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchDistributionV2(keyword, viewDate, mode, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoEntity> SearchV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchV2(username, videoFolderId, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> SearchAd(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchAd(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, allowAd, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> SearchExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchExcludeBomb(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoExportEntity> SearchExport(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoExportEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchExport(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> SearchExternalVideo(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new ExternalCmsVideoDb())
            {
                returnValue = db.VideoMainDal.Search(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoInNewsEntity> GetVideoInNewsList(int videoId)
        {
            List<VideoInNewsEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoInNewsList(videoId);
            }
            return returnValue;
        }

        public static List<VideoCounterEntity> GetVideoCounter(string username, string userDoAction)
        {
            List<VideoCounterEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoCounter(username, userDoAction);
            }
            return returnValue;
        }

        public static List<VideoExportEntity> SelectRoyalties(DateTime startDate, DateTime endDate, int pageIndex, int pageSize)
        {
            List<VideoExportEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SelectRoyalties(startDate, endDate, pageIndex, pageSize);
            }
            return returnValue;
        }

        public static List<VideoExportEntity> SelectRoyaltiesByZone(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string zones)
        {
            List<VideoExportEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SelectRoyaltiesByZone(startDate, endDate, pageIndex, pageSize, zones);
            }
            return returnValue;
        }

        public static int CountRoyalties(DateTime startDate, DateTime endDate)
        {
            int returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.CountRoyalties(startDate, endDate);
            }
            return returnValue;
        }

        public static int CountRoyaltiesByZone(DateTime startDate, DateTime endDate, string zones)
        {
            int returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.CountRoyaltiesByZone(startDate, endDate, zones);
            }
            return returnValue;
        }

        public static List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSharingEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListSharing(keyword, zoneIds, dateFrom, dateTo, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListVideoByHour(string zoneVideoIds, int Hour)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoByHour(zoneVideoIds, Hour);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetListVideoByParentId(int videoId)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoByParentId(videoId);
            }
            return returnValue;
        }
        #endregion

        #region VideoFromYoubute

        public static bool InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, ref int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertVideoFromYoutube(videoFromYoutube, ref id);
            }
            return returnValue;
        }

        public static bool UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateVideoFromYoutube(videoFromYoutube);
            }
            return returnValue;
        }

        public static bool DeleteVideoFromYoutube(int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.DeleteVideoFromYoutube(id);
            }
            return returnValue;
        }

        public static VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            VideoFromYoutubeEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoFromYoutubeById(id);
            }
            return returnValue;
        }

        public static List<VideoFromYoutubeEntity> GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoFromYoutubeEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoFromYoutube(accountName, status, order, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion

        public static List<VideoEntity> SearchWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchWithExcludeZones(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, excludeZones, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<BoxVideoEmbedEntity> GetExternalListVideoEmbed(int zoneId, int type)
        {
            List<BoxVideoEmbedEntity> returnValue;
            using (var db = new ExternalCmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static VideoEntity GetExternalById(int videoId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new ExternalCmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetById(videoId, ref zoneName);
            }
            return returnValue;
        }

        #region Video Distribution
        public static List<VideoEntity> SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDistributionDb())
            {
                returnValue = db.VideoMainDal.SearchVideoDistribution(zoneVideoIds, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static VideoEntity VideoDistributionGetById(int videoId, ref string zoneName)
        {
            VideoEntity returnValue;
            using (var db = new CmsVideoDistributionDb())
            {
                returnValue = db.VideoMainDal.GetById(videoId, ref zoneName);
            }
            return returnValue;
        }
        #endregion

        public static List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId)
        {
            List<PlaylistGroupEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetPlaylistGroupByParentId(playlistId);
            }
            return returnValue;
        }

        public static PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId)
        {
            PlaylistGroupEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetPlaylistGroupByPlaylistId(playlistId);
            }
            return returnValue;
        }

        public static bool InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertPlaylistInVideoGroup(playlistGroup);
            }
            return returnValue;
        }

        public static List<VideoEntity> SearchByPlaylistGroup(int group, int zone)
        {
            List<VideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchByPlaylistGroup(group, zone);
            }
            return returnValue;
        }

        #region Video EPL
        public static bool Insert_By_Feed(VideoEntity videoEntity, string tagIdList, string zoneList, int MapPlayListTime, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Insert_By_Feed(videoEntity, tagIdList, zoneList, MapPlayListTime, ref videoId);
            }
            return returnValue;
        }
        public static bool Video_Insesert_ZoneRecut(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Video_Insesert_ZoneRecut(videoId);
            }
            return returnValue;
        }
        public static bool Video_UpdateZonePrimary(int videoId, int ZoneId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Video_UpdateZonePrimary(videoId, ZoneId);
            }
            return returnValue;
        }
        public static bool Video_InsertZone(int videoId, string ZoneIds)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Video_InsertZone(videoId, ZoneIds);
            }
            return returnValue;
        }
        public static bool Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlayListId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Playlist_GetIdByZoneId(zoneId1, zoneId2, DateFrom, DateTo, VideoId, ref PlayListId);
            }
            return returnValue;
        }
        public static List<ZoneVideoEntity> ZoneVideo_GetAll()
        {
            List<ZoneVideoEntity> returnValue = new List<ZoneVideoEntity>();
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.ZoneVideo_GetAll();
            }
            return returnValue;
        }
        #endregion

        #region VideoApi
        public static bool GetVideoApiById(int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetVideoApiById(videoId);
            }
            return returnValue;
        }
        public static bool InsertVideoApi(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertVideoApiById(videoEntity);
            }
            return returnValue;
        }
        public static List<VideoApiEntity> GetListVideoApiById(string listVideoId)
        {
            List<VideoApiEntity> returnValue = new List<VideoApiEntity>();
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetListVideoApiById(listVideoId);
            }
            return returnValue;
        }
        #endregion

        #region Kenh14EPLVideoApi
        public static Boolean Kenh14EPLInsertVideoApi(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Kenh14EPLInsertVideoApi(videoEntity);
            }
            return returnValue;
        }
        public static Boolean Kenh14EPLChangerPublishVideoApi(string keyVideo, int status)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Kenh14EPLChangerPublishVideoApi(keyVideo, status);
            }
            return returnValue;
        }
        public static Boolean Kenh14EPLChangerUpdateVideoApi(VideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.Kenh14EPLUpdateVideoApi(videoEntity);
            }
            return returnValue;
        }
        #endregion

        #region Mp3

        public static List<Mp3Entity> SearchMp3(string username, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<Mp3Entity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.SearchMp3(username, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static Mp3Entity GetMp3ById(int id)
        {
            Mp3Entity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.GetMp3ById(id);
            }
            return returnValue;
        }
        public static bool InsertMp3(Mp3Entity mp3Entity, ref int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.InsertMp3(mp3Entity, ref id);
            }
            return returnValue;
        }
        public static bool UpdateMp3(Mp3Entity mp3Entity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoMainDal.UpdateMp3(mp3Entity);
            }
            return returnValue;
        }      
        #endregion
    }
}
