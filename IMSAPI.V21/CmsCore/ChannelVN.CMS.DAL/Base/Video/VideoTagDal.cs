﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoTagDal
    {
        public static bool Insert(VideoTagEntity videoTag, ref int videoTagId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.Insert(videoTag, ref videoTagId);
            }
            return returnValue;
        }
        public static bool UpdateByName(VideoTagEntity videoTag, ref int videoTagId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.UpdateByName(videoTag, ref videoTagId);
            }
            return returnValue;
        }
        public static bool Update(VideoTagEntity videoTag)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.Update(videoTag);
            }
            return returnValue;
        }
        public static bool UpdateMode(int videoTagId, bool isHotTag)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.UpdateMode(videoTagId, isHotTag);
            }
            return returnValue;
        }
        public static List<VideoTagEntity> Search(string keyword, int parentId, int isHotTag, int status, int orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.Search(keyword, parentId, isHotTag, status, orderBy, getTagHasVideoOnly, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static VideoTagEntity GetById(int id)
        {
            VideoTagEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.GetById(id);
            }
            return returnValue;
        }

        public static VideoTagEntity GetByName(string name)
        {
            VideoTagEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.GetByName(name);
            }
            return returnValue;
        }

        /// <summary>
        /// Get VideoTag List for Video Editing
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public static List<VideoTagEntity> GetByVideoId(int videoId)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.GetByVideoId(videoId);
            }
            return returnValue;
        }

        /// <summary>
        /// Get VideoTag List for zonevideo Editing
        /// </summary>
        /// <param name="zoneVideoId"></param>
        /// <returns></returns>
        public static List<VideoTagEntity> GetByZoneVideoId(int zoneVideoId)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.GetByZoneVideoId(zoneVideoId);
            }
            return returnValue;
        }

        public static List<VideoTagEntity> GetByIds(string tagIds)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoTagMainDal.GetByIds(tagIds);
            }
            return returnValue;
        }
    }
}
