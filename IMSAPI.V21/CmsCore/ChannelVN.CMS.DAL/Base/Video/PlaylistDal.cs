﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class PlaylistDal
    {
        #region Update
        
        public static bool Insert(PlaylistEntity playlistEntity, string zoneIdList, ref int playlistId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Insert(playlistEntity, zoneIdList, ref playlistId);
            }
            return returnValue;
        }

        public static bool Update(PlaylistEntity playlistEntity, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Update(playlistEntity, zoneIdList);
            }
            return returnValue;
        }        

        public static bool RemoveVideoOutOfPlaylist(int playlistId, string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.RemoveVideoOutOfPlaylist(playlistId, videoIdList);
            }
            return returnValue;
        }

        public static bool ChangeStatus(int playlistId, int status)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.ChangeStatus(playlistId, status);
            }
            return returnValue;
        }

        public static bool ChangeMode(int playlistId, int mode)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.ChangeMode(playlistId, mode);
            }
            return returnValue;
        }

        public static bool UpdateVideoPriorotyInPlayList(int playlistId, VideoPriorityEntity item)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.UpdateVideoPriorotyInPlayList(playlistId, item);
            }
            return returnValue;
        }

        public static bool ChangeOrder(int playlistId, int videoId, int priority)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.ChangeOrder(playlistId, videoId, priority);
            }
            return returnValue;
        }

        public static bool ChangeOrderByIdList(int playlistId, string playlistIdList, int priority)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.ChangeOrderByIdList(playlistId, playlistIdList, priority);
            }
            return returnValue;
        }

        public static bool AddVideo(int videoId, int playlistId, int priority)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.AddVideo(videoId, playlistId, priority);
            }
            return returnValue;
        }

        public static bool AddVideoList(string addVideoIds, string deleteVideoIds, int playlistId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.AddVideoList(addVideoIds, deleteVideoIds, playlistId);
            }
            return returnValue;
        }

        public static bool DeleteById(int playlistId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.DeleteById(playlistId);
            }
            return returnValue;
        }

        public static bool DeleteByIdList(string playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.DeleteByIdList(playlistIdList);
            }
            return returnValue;
        }

        public static bool Publish(int playlistId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Publish(playlistId, userDoAction);
            }
            return returnValue;
        }

        public static bool Unpublish(int playlistId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Unpublish(playlistId);
            }
            return returnValue;
        }

        public static bool Send(int playListId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Send(playListId, userDoAction);
            }
            return returnValue;
        }

        public static bool Return(int playListId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Return(playListId);
            }
            return returnValue;
        }
        public static bool MoveTrash(int playlistId, string username)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.MoveTrash(playlistId, username);
            }
            return returnValue;
        }
        #endregion

        #region Get

        public static PlaylistEntity GetById(int playlistId)
        {
            PlaylistEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetById(playlistId);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetListByVideoId(int videoId)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetListByVideoId(videoId);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetListByVideoChannelId(int videoChannelId)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetListByVideoChannelId(videoChannelId);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetListPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetListPaging(pageIndex, pageSize, status, keyword, sortOder, ref totalRow);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetMyPlaylist(username, pageIndex, pageSize, status, keyword, sortOder, ref totalRow);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> Search(string username, int zoneVideoId, int status, string keyword, int mode, int sortOder, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Search(username, zoneVideoId, status, keyword, mode, sortOder, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<PlaylistCountFollowEntity> CountVideoFollow(string playListIds)
        {
            List<PlaylistCountFollowEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.CountVideoFollow(playListIds);
            }
            return returnValue;
        }

        public static List<PlaylistCounterEntity> GetPlaylistCounter()
        {
            List<PlaylistCounterEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetPlaylistCounter();
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetLastDaysHavePlaylist(DateTime fromDate, DateTime toDate, int group, int team, int mode)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetLastDaysHavePlaylist(fromDate, toDate, group, team, mode);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetTopDayHaveVideo(int top)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetTopDayHaveVideo(top);
            }
            return returnValue;
        }

        public static List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId)
        {
            List<PlaylistInZoneEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetZoneByPlaylist(playlistId);
            }
            return returnValue;
        }
        public static bool Playlist_Update_DistributionDate(string playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.Playlist_Update_DistributionDate(playlistIdList);
            }
            return returnValue;
        }
        public static PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate)
        {
            PlaylistGroupEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.PlaylistGroup_Current(fromDate, toDate);
            }
            return returnValue;
        }
        #endregion

        public static List<PlaylistEntity> InitRedisAllPlayList(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.InitRedisAllPlayList(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetListByIdList(string playListIdList)
        {
            List<PlaylistEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.PlaylistMainDal.GetListByIdList(playListIdList);
            }
            return returnValue;
        }
    }
}
