﻿using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoChannelDal
    {
        public static bool Insert(VideoChannelEntity channelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.Insert(channelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList, ref videoId);
            }
            return returnValue;
        }

        public static bool Update(VideoChannelEntity channelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.Update(channelEntity, zoneIdList, labelIdList, playlistIdList, videoIdList);
            }
            return returnValue;
        }

        public static bool Delete(int channelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.Delete(channelId);
            }
            return returnValue;
        }

        public static VideoChannelEntity GetById(int id)
        {
            VideoChannelEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<VideoChannelEntity> GetVideoChannelByVideoId(int videoId)
        {
            List<VideoChannelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.GetVideoChannelByVideoId(videoId);
            }
            return returnValue;
        }

        public static List<VideoChannelEntity> InitAllVideoChannel(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<VideoChannelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoChannelMainDal.InitAllVideoChannel(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
    }
}
