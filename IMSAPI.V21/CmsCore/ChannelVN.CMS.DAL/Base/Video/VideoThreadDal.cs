﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoThreadDal
    {
        public static List<VideoThreadEntity> Search(string keyword, bool isHotThread, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoThreadEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.Search(keyword, isHotThread, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static VideoThreadEntity GetById(int id)
        {
            VideoThreadEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Insert(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.Insert(videoThread, videoIdList, ref videoThreadId);
            }
            return returnValue;
        }

        public static bool Update(VideoThreadEntity videoThread, string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.Update(videoThread, videoIdList);
            }
            return returnValue;
        }

        public static bool Delete(int videoThreadId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.Delete(videoThreadId);
            }
            return returnValue;
        }

        public static bool UpdateVideoInVideoThread(int videoThreadId, string videoIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoThreadMainDal.UpdateVideoInVideoThread(videoThreadId, videoIdList);
            }
            return returnValue;
        }
    }
}
