﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class BoxVideoEmbedDal
    {
        #region BoxVideoEmbed
        public static List<BoxVideoEmbedEntity> GetListVideoEmbed(int zoneId, int type)
        {
            List<BoxVideoEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.GetListVideoEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxVideoEmbedEntity VideoEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.Insert(VideoEmbebBox);
            }
            return returnValue;
        }

        public static bool Update(string listVideoId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.Update(listVideoId, zoneId, type);
            }
            return returnValue;
        }
        public static bool UpdateEpl(BoxVideoEmbedEplEntity embed)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.UpdateEpl(embed);
            }
            return returnValue;
        }
        public static bool Delete(long VideoId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.Delete(VideoId, zoneId, type);
            }
            return returnValue;
        }
        #endregion

        #region BoxPlayEmbed
        public static List<BoxPlayEmbedEntity> GetListPlayEmbed(int zoneId, int type, int typeId)
        {
            List<BoxPlayEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.GetListPlayEmbed(zoneId, type, typeId);
            }
            return returnValue;
        }

        public static bool InsertPlay(BoxPlayEmbedEntity VideoEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.InsertPlay(VideoEmbebBox);
            }
            return returnValue;
        }
        
        public static bool DeletePlay(int zoneId, int type, int typeId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.DeletePlay(zoneId, type, typeId);
            }
            return returnValue;
        }
        #endregion

        #region BoxVideoHighLightEmbed
        public static List<BoxVideoHighlightEmbedEntity> GetVideoHighLightEmbed(int typeId)
        {
            List<BoxVideoHighlightEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.GetVideoHighLightEmbed(typeId);
            }
            return returnValue;
        }

        public static bool InsertVideoHighLight(BoxVideoHighlightEmbedEntity VideoEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.InsertVideoHighLight(VideoEmbebBox);
            }
            return returnValue;
        }

        public static bool DeleteVideoHighLight(int typeId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.DeleteVideoHighLight(typeId);
            }
            return returnValue;
        }
        #endregion

        #region BoxVideoHighLightEmbed
        public static List<BoxProgramHotHomeEmbedEntity> GetProgramHotHomeEmbed(int typeId, int zoneId)
        {
            List<BoxProgramHotHomeEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.GetProgramHotHomeEmbed(typeId, zoneId);
            }
            return returnValue;
        }

        public static bool InsertProgramHotHome(BoxProgramHotHomeEmbedEntity VideoEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.InsertProgramHotHome(VideoEmbebBox);
            }
            return returnValue;
        }

        public static bool DeleteProgramHotHome(int typeId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxVideoEmbedMainDal.DeleteProgramHotHome(typeId, zoneId);
            }
            return returnValue;
        }
        #endregion
    }
}
