using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoLabelDal
    {
        public static List<VideoLabelEntity> GetListByParentId(int parentId, int isSystem)
        {
            List<VideoLabelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetListByParentId(parentId, isSystem);
            }
            return returnValue;
        }

        public static List<VideoLabelEntity> GetListZoneRelation(int videoId)
        {
            List<VideoLabelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetListZoneRelation(videoId);
            }
            return returnValue;
        }

        public static List<VideoLabelEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            List<VideoLabelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetActiveZoneInDay(zoneIds,viewDate,mode);
            }
            return returnValue;
        }

        public static List<VideoLabelEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            List<VideoLabelEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetActiveZoneInDayV2(zoneIds, viewDate, mode);
            }
            return returnValue;
        }

        public static List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            List<VideoModeNotifyEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetNotifyZoneInDay(viewDate);
            }
            return returnValue;
        }

        public static VideoLabelEntity GetById(int id)
        {
            VideoLabelEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetById(id);
            }
            return returnValue;
        }

        public static bool Insert(VideoLabelEntity videoLabel, ref int videoLabelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.Insert(videoLabel, ref videoLabelId);
            }
            return returnValue;
        }

        public static bool Update(VideoLabelEntity videoLabel)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.Update(videoLabel);
            }
            return returnValue;
        }

        public static bool MoveUp(int videoLabelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.MoveUp(videoLabelId);
            }
            return returnValue;
        }

        public static bool MoveDown(int videoLabelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.MoveDown(videoLabelId);
            }
            return returnValue;
        }

        public static bool Delete(int videoLabelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.Delete(videoLabelId);
            }
            return returnValue;
        }        

        public static bool GetVideoLabelIdByNewsZoneId(string newsZoneId, ref int videoLabelId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.GetVideoLabelIdByNewsZoneId(newsZoneId, ref videoLabelId);
            }
            return returnValue;
        }

        public static bool UpdateAvatarVideoLabel(string listAvatar)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoLabelMainDal.UpdateAvatarVideoLabel(listAvatar);
            }
            return returnValue;
        }
    }
}
