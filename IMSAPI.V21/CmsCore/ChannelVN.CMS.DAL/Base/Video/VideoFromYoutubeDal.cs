﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoFromYoutubeDal
    {
        public static bool InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.InsertVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList, ref id);
            }
            return returnValue;
        }

        public static bool UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.UpdateVideoFromYoutube(videoFromYoutube, video, tagIdList, zoneIdList, playlistIdList);
            }
            return returnValue;
        }
        public static bool UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, int status, string htmlCode, string keyVideo, string pname, string fileName, string duration, string size, int capacity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.UpdateVideoFromYoutubeStatus(id, avatar, filePath, status, htmlCode, keyVideo, pname, fileName, duration, size, capacity);
            }
            return returnValue;
        }

        public static bool DeleteVideoFromYoutube(int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.DeleteVideoFromYoutube(id);
            }
            return returnValue;
        }

        public static bool PublishVideoFromYoutube(int id, string publishedBy)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.PublishVideoFromYoutube(id, publishedBy);
            }
            return returnValue;
        }
        public static bool SendVideoFromYoutube(int id)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.SendVideoFromYoutube(id);
            }
            return returnValue;
        }

        public static VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            VideoFromYoutubeEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.GetVideoFromYoutubeById(id);
            }
            return returnValue;
        }
        public static VideoFromYoutubeEntity GetBySourceVideoUrl(string sourceVideoUrl)
        {
            VideoFromYoutubeEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.GetBySourceVideoUrl(sourceVideoUrl);
            }
            return returnValue;
        }
        public static List<VideoFromYoutubeDetailEntity> Search(string username, string keyword, int status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoFromYoutubeDetailEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.Search(username, keyword, status, zoneVideoId, playlistId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoFromYoutubeEntity> GetUploadingVideo()
        {
            List<VideoFromYoutubeEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFromYoutubeMainDal.GetUploadingVideo();
            }
            return returnValue;
        }
    }
}
