﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoFolderDal
    {
        #region Update

        public static bool Insert(VideoFolderEntity videoFolder, ref int newVideoFolderId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.Insert(videoFolder, ref newVideoFolderId);

            }
            return returnValue;
        }

        public static bool Update(VideoFolderEntity videoFolder)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.Update(videoFolder);

            }
            return returnValue;
        }

        public static bool Delete(int videoFolderId)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.Delete(videoFolderId);

            }
            return returnValue;
        }

        #endregion

        #region Get

        public static VideoFolderEntity GetById(int videoFolderId)
        {
            VideoFolderEntity returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.GetById(videoFolderId);

            }
            return returnValue;
        }

        public static List<VideoFolderEntity> Search(int parentId, string name, string createdBy, int status)
        {
            List<VideoFolderEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.Search(parentId, name, createdBy,status);

            }
            return returnValue;
        }


        public static List<VideoFolderEntity> GetByParentId(int parentId, string createdBy)
        {
            List<VideoFolderEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.VideoFolderMainDal.GetByParentId(parentId, createdBy);

            }
            return returnValue;
        }

        #endregion
    }
}
