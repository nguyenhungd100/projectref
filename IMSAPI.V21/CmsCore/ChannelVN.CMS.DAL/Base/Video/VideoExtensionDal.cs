﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Video
{
    public class VideoExtensionDal
    {
        public static VideoExtensionEntity GetValue(long videoId, int type)
        {
            VideoExtensionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.GetValue(videoId, type);
            }
            return returnValue;
        }
        public static int GetMaxValue(long videoId, int type)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.GetMaxValue(videoId, type);
            }
            return returnValue;
        }
        public static bool SetValue(long videoId, int type, string value)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.SetValue(videoId, type, value);
            }
            return returnValue;
        }
        public static bool DeleteByVideoId(long videoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.DeleteByVideoId(videoId);
            }
            return returnValue;
        }
        public static List<VideoExtensionEntity> GetByVideoId(long videoId)
        {
            List<VideoExtensionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.GetByVideoId(videoId);
            }
            return returnValue;
        }
        public static List<VideoExtensionEntity> GetByListVideoId(string listVideoId)
        {
            List<VideoExtensionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.GetByListVideoId(listVideoId);
            }
            return returnValue;
        }
        public static List<VideoExtensionEntity> GetByTypeAndVaue(int type, string value)
        {
            List<VideoExtensionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VideoExtensionMainDal.GetByTypeAndVaue(type, value);
            }
            return returnValue;
        }
    }
}
