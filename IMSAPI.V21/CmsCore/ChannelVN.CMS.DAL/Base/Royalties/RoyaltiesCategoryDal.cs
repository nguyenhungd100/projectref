﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesCategoryDal
    {
        #region Get

        public static RoyaltiesCategoryEntity GetById(int id)
        {
            RoyaltiesCategoryEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.GetById(id);

            }
            return returnValue;
        }

        public static List<RoyaltiesCategoryEntity> GetAll()
        {
            List<RoyaltiesCategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.GetAll();

            }
            return returnValue;
        }

        public static List<RoyaltiesCategoryEntity> GetByRoyaltiesCategoryType(int zoneId, int type)
        {
            List<RoyaltiesCategoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.GetByRoyaltiesCategoryType(zoneId, type);

            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Insert(RoyaltiesCategoryEntity royaltiesCategory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.Insert(royaltiesCategory);

            }
            return returnValue;
        }

        public static bool Update(RoyaltiesCategoryEntity royaltiesCategory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.Update(royaltiesCategory);

            }
            return returnValue;
        }

        public static bool Delete(int royaltiesCategoryId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesCategoryMainDal.Delete(royaltiesCategoryId);

            }
            return returnValue;
        }

        #endregion
    }
}
