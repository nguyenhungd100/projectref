﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesQuotaDal
    {
        #region Get

        public static List<RoyaltiesQuotaEntity> Search(string keyword,
                                                            UserStatus status,
                                                            UserSortExpression sortOrder,
                                                            int pageIndex,
                                                            int pageSize,
                                                            ref int totalRow)
        {
            List<RoyaltiesQuotaEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesQuotaMainDal.SearchUser(keyword, (int)status, (int)sortOrder, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Update(string lstId, string lstPost, string lstView)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesQuotaMainDal.Update(lstId, lstPost, lstView);
            }
            return returnValue;
        }

        #endregion
    }
}
