﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesDal
    {
        #region Get

        public static RoyaltiesEntity GetById(long id)
        {
            RoyaltiesEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.GetById(id);

            }
            return returnValue;
        }

        public static List<RoyaltiesEntity> Search(int type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<RoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.Search(type, zoneId, royaltiesMemberId, royaltiesCategoryId, royaltiesRoleId, keyword, royaltiesRate, fromDate, toDate, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }

        public static List<RoyaltiesEntity> GetByNewsId(long newsId)
        {
            List<RoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.GetByNewsId(newsId);

            }
            return returnValue;
        }

        public static List<RoyaltiesGroupByUserEntity> SearchGroupByUser(int type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate)
        {
            List<RoyaltiesGroupByUserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.SearchGroupByUser(type, zoneId, royaltiesMemberId, royaltiesCategoryId, royaltiesRoleId, keyword, royaltiesRate, fromDate, toDate);

            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Insert(RoyaltiesEntity royalties, ref long newRoyaltiesId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.Insert(royalties, ref newRoyaltiesId);

            }
            return returnValue;
        }

        public static bool Update(RoyaltiesEntity royalties)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.Update(royalties);

            }
            return returnValue;
        }

        public static bool Delete(int royaltiesId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.Delete(royaltiesId);

            }
            return returnValue;
        }

        public static bool DeleteByNewsId(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMainDal.DeleteByNewsId(newsId);

            }
            return returnValue;
        }

        #endregion
    }
}
