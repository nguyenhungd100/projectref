﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesMemberDal
    {
        #region Get

        public static RoyaltiesMemberEntity GetById(int id)
        {
            RoyaltiesMemberEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMemberMainDal.GetById(id);

            }
            return returnValue;
        }

        public static List<RoyaltiesMemberEntity> Search(int royaltiesRoleId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<RoyaltiesMemberEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMemberMainDal.Search(royaltiesRoleId, keyword, status, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Insert(RoyaltiesMemberEntity royaltiesMember)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMemberMainDal.Insert(royaltiesMember);

            }
            return returnValue;
        }

        public static bool Update(RoyaltiesMemberEntity royaltiesMember)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMemberMainDal.Update(royaltiesMember);

            }
            return returnValue;
        }

        public static bool Delete(int royaltiesMemberId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesMemberMainDal.Delete(royaltiesMemberId);

            }
            return returnValue;
        }

        #endregion
    }
}
