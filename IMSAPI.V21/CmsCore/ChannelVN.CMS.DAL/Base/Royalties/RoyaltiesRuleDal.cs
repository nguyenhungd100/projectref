﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesRuleDal
    {
        #region Get

        public static List<RoyaltiesRuleEntity> GetByZoneId(int zoneId)
        {
            List<RoyaltiesRuleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRuleMainDal.GetByZoneId(zoneId);
            }
            return returnValue;
        }
        public static RoyaltiesRuleEntity GetByRoyaltiesCategoryIdAndRoyaltiesRate(bool isManager, int royaltiesCategoryId, int royaltiesRate)
        {
            RoyaltiesRuleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRuleMainDal.GetByRoyaltiesCategoryIdAndRoyaltiesRate(isManager, royaltiesCategoryId, royaltiesRate);
            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool UpdateValue(int royaltiesCategoryId, bool isManager, int royaltiesRate, int royaltiesValue)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRuleMainDal.UpdateValue(royaltiesCategoryId, isManager, royaltiesRate, royaltiesValue);

            }
            return returnValue;
        }

        #endregion
    }
}
