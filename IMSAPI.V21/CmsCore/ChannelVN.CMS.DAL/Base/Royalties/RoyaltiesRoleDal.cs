﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesRoleDal
    {
        #region Get
        
        public static RoyaltiesRoleEntity GetById(int id)
        {
            RoyaltiesRoleEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRoleMainDal.GetById(id);

            }
            return returnValue;
        }

        public static List<RoyaltiesRoleEntity> GetAll()
        {
            List<RoyaltiesRoleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRoleMainDal.GetAll();

            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Insert(RoyaltiesRoleEntity royaltiesRole)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRoleMainDal.Insert(royaltiesRole);

            }
            return returnValue;
        }

        public static bool Update(RoyaltiesRoleEntity royaltiesRole)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRoleMainDal.Update(royaltiesRole);

            }
            return returnValue;
        }

        public static bool Delete(int royaltiesRoleId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesRoleMainDal.Delete(royaltiesRoleId);

            }
            return returnValue;
        }

        #endregion
    }
}
