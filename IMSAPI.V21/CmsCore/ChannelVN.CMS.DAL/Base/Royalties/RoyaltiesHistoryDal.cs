﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Royalties
{
    public class RoyaltiesHistoryDal
    {

        #region Get

        public static List<RoyaltiesHistoryEntity> GetByRoyaltiesId(long royaltiesId)
        {
            List<RoyaltiesHistoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesHistoryMainDal.GetByRoyaltiesId(royaltiesId);

            }
            return returnValue;
        }

        public static List<RoyaltiesHistoryEntity> GetOldRoyaltiesHistoryByListRoyaltiesId(string listRoyaltiesId)
        {
            List<RoyaltiesHistoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesHistoryMainDal.GetOldRoyaltiesHistoryByListRoyaltiesId(listRoyaltiesId);

            }
            return returnValue;
        }

        #endregion

        #region Set

        public static bool Insert(RoyaltiesHistoryEntity royaltiesHistory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RoyaltiesHistoryMainDal.Insert(royaltiesHistory);

            }
            return returnValue;
        }

        #endregion
    }
}
