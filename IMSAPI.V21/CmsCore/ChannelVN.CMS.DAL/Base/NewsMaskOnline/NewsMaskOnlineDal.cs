﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsMaskOnline
{
    public class NewsMaskOnlineDal
    {
        public static List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                    int SourceID, int status, int pageIndex, int pageSize, int Mode = -1)
        {
            List<NewsMaskOnlineEntity> returnValue;
            using (var db = new MaskOnlineDb())
            {
                returnValue = db.NewsMaskOnlineDal.ListNewsMaskOnline(keyword, fromDate, toDate, SourceID, status, pageIndex, pageSize, Mode);
            }
            return returnValue;
        }
    }
}
