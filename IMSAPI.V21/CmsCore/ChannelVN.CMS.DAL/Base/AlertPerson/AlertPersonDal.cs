﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.AlertPerson
{
    public class AlertPersonDal
    {
        #region Get methods
        public static AlertPersonEntity GetAlertPersonById(int personId)
        {
            AlertPersonEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlertPersonMainDal.GetAlertPersonById(personId);
            }
            return returnValue;
        }
        public static List<AlertPersonEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AlertPersonEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlertPersonMainDal.SearchAlertPerson(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion

        #region Update methods

        public static bool Insert(AlertPersonEntity tag, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlertPersonMainDal.Insert(tag, ref tagId);
            }
            return returnValue;
        }
        public static bool Update(AlertPersonEntity tag)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlertPersonMainDal.Update(tag);
            }
            return returnValue;
        }
        public static bool DeleteById(int tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AlertPersonMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        #endregion
    }
}
