﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Statistic
{
    public class NewsStatisticDal
    {

        public static List<NewsStatisticEntity> GetStatisticStatusNewsByAccount(string account, int status, DateTime startDate, DateTime endDate)
        {
            List<NewsStatisticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticStatusNewsByAccount(account, status, startDate, endDate);
            }
            return returnValue;
        }
        public static List<NewsStatisticEntity> GetStatisticStatusNewsByAccountAdmin(string createdBy, int status, DateTime startDate, DateTime endDate)
        {
            List<NewsStatisticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticStatusNewsByAccountAdmin(createdBy, status, startDate, endDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction(string zoneId, DateTime fromDate, DateTime toDate)
        {
            List<NewsStatisticTotalProduction> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticNewsTotalProduction(zoneId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<ViewCountByUserEntity> GetUserViewCount(DateTime fromDate, DateTime toDate)
        {
            List<ViewCountByUserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetUserViewCount(fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsComment(string zoneId, DateTime fromDate, DateTime toDate)
        {
            List<NewsStatisticTotalProduction> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticNewsComment(zoneId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsCommentFromExternalDB(string zoneId, DateTime fromDate, DateTime toDate)
        {
            List<NewsStatisticTotalProduction> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsStatisticExternalMainDal.GetStatisticNewsCommentFromExternalDB(zoneId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProductionUser(string zoneId, DateTime fromDate, DateTime toDate, string userName)
        {
            List<NewsStatisticTotalProduction> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticNewsTotalProductionUser(zoneId, fromDate, toDate, userName);
            }
            return returnValue;
        }

        public static List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsStatisticExternalMainDal.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticSourceEntity> GetStatisticNewsBySourceId(int sourceId, DateTime startDate, DateTime endDate)
        {
            List<NewsStatisticSourceEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetStatisticNewsBySourceId(sourceId, startDate, endDate);
            }
            return returnValue;
        }

        public static DepartmentViewCountEntity GetDepartmentView(int departmentId, DateTime fromDate, DateTime toDate)
        {
            DepartmentViewCountEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsStatisticMainDal.GetDepartmentView(departmentId, fromDate, toDate);
            }
            return returnValue;
        }

    }
}
