﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.DAL.Base.Statistic
{
    public class PageViewStatisticDal
    {
        public static bool PageViewByZone_UpdateByDay(PageViewByZoneEntity viewEntity)
        {
            bool returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.PageViewByZone_UpdateByDay(viewEntity);
            }
            return returnValue;
        }

        public static DateTime GetLastestRun()
        {
            DateTime returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.GetLastestRun();
            }
            return returnValue;
        }

        public static bool PageViewAllSite_UpdateByDay(PageViewAllSiteEntity objPageViewUpdate)
        {
            bool returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.PageViewAllSite_UpdateByDay(objPageViewUpdate);
            }
            return returnValue;
        }

        public static List<PageViewAllSiteEntity> PageViewAllSite_GetData(DateTime fromDate, DateTime toDate,int channelId)
        {
            List<PageViewAllSiteEntity> returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.PageViewAllSite_GetData(fromDate, toDate, channelId);
            }
            return returnValue;
        }

        public static List<PageViewByZoneEntity> PageViewByZone_GetData(DateTime fromDate, DateTime toDate,string zoneIds,int channelId)
        {
            List<PageViewByZoneEntity> returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.PageViewByZone_GetData(fromDate, toDate,zoneIds, channelId);
            }
            return returnValue;
        }

        public static ChannelEntity GetChannel_ByName(string channelName)
        {
            ChannelEntity returnValue;
            using (var db = new CmsPageViewStatisticDb())
            {
                returnValue = db.NewsSohaMainDal.GetChannel_ByName(channelName);
            }
            return returnValue;
        }
    }
}
