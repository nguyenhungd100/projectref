﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Topic
{
    public class TopicInZoneDal
    {
        public static List<TopicInZoneEntity> GetTopicInZoneByTopicId(long tagId)
        {
            List<TopicInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicInZoneMainDal.GetTopicInZoneByTopicId(tagId);
            }
            return returnValue;
        }
    }
}
