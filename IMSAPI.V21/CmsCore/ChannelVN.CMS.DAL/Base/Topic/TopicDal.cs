﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.MainDal.Databases;
using System;

namespace ChannelVN.CMS.DAL.Base.Topic
{
    public class TopicDal
    {
        #region Get
        public static TopicEntity GetTopicByName(string nameTopic)
        {
            TopicEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTopicByName(nameTopic);
            }
            return returnValue;
        }

        public static TopicEntity GetTopicByTopicId(long TopicId)
        {
            TopicEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTopicByTopicId(TopicId);
            }
            return returnValue;
        }
        public static List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            List<TopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTopicByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<TagWithSimpleFieldEntity> GetTagsByTopicId(int topicId)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTagsByTopicId(topicId);
            }
            return returnValue;
        }
        public static List<TopicEntity> GetTopicByParentId(int topicId)
        {
            List<TopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTopicByParentId(topicId);
            }
            return returnValue;
        }
        public static List<TopicRelation> GetTopicRelationByTopicId(int topicId)
        {
            List<TopicRelation> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.GetTopicRelationByTopicId(topicId);
            }
            return returnValue;
        }
        public static List<TopicEntity> SearchTopic(string keyword, int zoneId, int orderBy, int isHotTopic, int isActive, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.SearchTopic(keyword, zoneId, orderBy, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<TopicParent> ListParentTopicById(int topicId)
        {
            List<TopicParent> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ListParentTopicById(topicId);
            }
            return returnValue;
        }
        public static bool ToggleTopicActive(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ToggleTopicActive(id);
            }
            return returnValue;
        }

        public static bool ToggleTopicIconActive(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ToggleTopicIconActive(id);
            }
            return returnValue;
        }

        public static bool ToggleTopicIsToolbar(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ToggleTopicIsToolbar(id);
            }
            return returnValue;
        }
        public static List<TopicEntity> InitESAllTopic(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<TopicEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.InitESAllTopic(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        #endregion

        #region Update
        public static bool Insert(TopicEntity Topic, int zoneId, string zoneIdList, ref int TopicId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.Insert(Topic, zoneId, zoneIdList, ref TopicId);
            }
            return returnValue;
        }
        public static bool Update(TopicEntity Topic, int zoneId, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.Update(Topic, zoneId, zoneIdList);
            }
            return returnValue;
        }
        public static bool DeleteById(long TopicId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.DeleteById(TopicId);
            }
            return returnValue;
        }
        public static bool UpdateTopicHot(long TopicId, bool isHotTopic)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.UpdateTopicHot(TopicId, isHotTopic);
            }
            return returnValue;
        }

        #endregion

        #region BoxTopicEmbed

        public static List<BoxTopicEmbedEntity> ListBoxTopicEmbed(int zoneId, int type)
        {
            List<BoxTopicEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ListBoxTopicEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static bool InsertBoxTopicEmbed(BoxTopicEmbedEntity box, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.InsertBoxTopicEmbed(box, ref id);
            }
            return returnValue;
        }

        public static bool UpdateBoxTopicEmbed(BoxTopicEmbedEntity box)
        {
            using (var db = new CmsMainDb())
            {
                return db.TopicMainDal.UpdateBoxTopicEmbed(box);
            }
        }

        public static bool DeleteBoxTopicEmbed(int id)
        {
            using (var db = new CmsMainDb())
            {
                return db.TopicMainDal.DeleteBoxTopicEmbed(id);
            }
        }

        #endregion

        #region BoxTopicOnPageEmbed

        public static List<BoxTopicOnPageEmbedEntity> ListBoxTopicOnPageEmbed(int topicId, int type)
        {
            List<BoxTopicOnPageEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.ListBoxTopicOnPageEmbed(topicId, type);
            }
            return returnValue;
        }

        public static bool UpdateBoxTopicOnPageEmbed(string listTopicId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.UpdateBoxTopicOnPageEmbed(listTopicId, zoneId, type);
            }
            return returnValue;
        }

        public static bool InsertBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicMainDal.InsertBoxTopicOnPageEmbed(box, ref id);
            }
            return returnValue;
        }

        public static bool UpdateBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box)
        {
            using (var db = new CmsMainDb())
            {
                return db.TopicMainDal.UpdateBoxTopicOnPageEmbed(box);
            }
        }

        public static bool DeleteBoxTopicOnPageEmbed(int id)
        {
            using (var db = new CmsMainDb())
            {
                return db.TopicMainDal.DeleteBoxTopicOnPageEmbed(id);
            }
        }

        #endregion
    }
}
