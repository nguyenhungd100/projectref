﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Topic
{
    public class TopicNewsDal
    {
        public static bool UpdateNews(int TopicId, string deleteNewsId, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicNewsMainDal.UpdateNews(TopicId, deleteNewsId, addNewsId);
            }
            return returnValue;
        }
        public static bool MultiUpdateTopicNews(string listTopicId, long newsId, int topicId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicNewsMainDal.MultiUpdateTopicNews(listTopicId, newsId, topicId);
            }
            return returnValue;
        }
        public static bool UpdateNewsHot(long topicId, string addNewsAvatar, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicNewsMainDal.UpdateNewsHot(topicId, addNewsAvatar, addNewsId);
            }
            return returnValue;
        }
        public static bool UpdateTopicNews(long newsId, string relatedTopics)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TopicNewsMainDal.UpdateTopicNews(newsId, relatedTopics);
            }
            return returnValue;
        }
    }
}
