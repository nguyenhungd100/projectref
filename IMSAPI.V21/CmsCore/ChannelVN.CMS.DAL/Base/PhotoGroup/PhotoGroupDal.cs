﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.DAL.Base.PhotoGroup
{
    public class PhotoGroupDal
    {
        #region Update
        public static bool InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity photo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.InsertPhotoGroupDetailInPhotoTag(photo);

            }
            return returnValue;
        }
        public static bool InsertPhotoGroupDetail(PhotoGroupDetailEntity photo, ref int newPhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.InsertPhotoGroupDetail(photo, ref newPhotoId);

            }
            return returnValue;
        }
        public static bool UpdatePhotoGroupDetail(PhotoGroupDetailEntity photo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.UpdatePhotoGroupDetail(photo);

            }
            return returnValue;
        }
        public static bool UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.UpdatePhotoGroupDetailStatus(id, status);

            }
            return returnValue;
        }
        public static bool DeletePhotoGroupDetail(int photoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.DeletePhotoGroupDetail(photoId);

            }
            return returnValue;
        }
        public static bool DeletePhotoGroupDetailInPhotoTag(int photoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.DeletePhotoGroupDetailInPhotoTag(photoId);

            }
            return returnValue;
        }

        #endregion

        #region Get
        public static List<PhotoGroupDetailInZoneEntity> PhotoGroupDetailInZone_GetAll(int photoGroupId)
        {
            List<PhotoGroupDetailInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.PhotoGroupDetailInZone_GetAll(photoGroupId);

            }
            return returnValue;
        }
        public static PhotoGroupDetailEntity GetPhotoGroupDetailById(int photoId)
        {
            PhotoGroupDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.GetPhotoGroupDetailById(photoId);

            }
            return returnValue;
        }
        public static List<PhotoGroupDetailEntity> SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int photoGroupId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoGroupDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.SearchPhotoGroupDetail(keyword, zoneId, fromDate, toDate, createdBy, status, isHot, photoGroupId, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
        public static List<PhotoGroupDetailCountEntity> CountPhotoGroupDetail(string username)
        {
            List<PhotoGroupDetailCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.CountPhotoGroupDetail(username);

            }
            return returnValue;
        }

        public static List<PhotoGroupEntity> GetAllPhotoGroup(ref int totalRow)
        {
            List<PhotoGroupEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoGroupMainDal.GetAllPhotoGroup(ref totalRow);

            }
            return returnValue;
        }

        #endregion
    }
}
