﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsPrDal
    {
        public static NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            NewsPrEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.GetNewsPrByNewsId(newsId);
            }
            return returnValue;
        }

        public static NewsPrEntity GetNewsPrByContentId(long contentId)
        {
            NewsPrEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.GetNewsPrByContentId(contentId);
            }
            return returnValue;
        }

        public static bool RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.RecieveNewsPrIntoCms(newsInfo, newsPrInfo, listNewsRelationId);
            }
            return returnValue;
        }

        public static bool RecieveNewsPrV2IntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.RecieveNewsPrV2IntoCms(newsInfo, newsPrInfo, listNewsRelationId);
            }
            return returnValue;
        }

        public static bool UpdateNewsPrInfo(NewsPrEntity newsPrInfo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.UpdateNewsPrInfo(newsPrInfo);
            }
            return returnValue;
        }

        public static bool UpdateNewsPrV2Info(NewsPrEntity newsPrInfo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.UpdateNewsPrV2Info(newsPrInfo);
            }
            return returnValue;
        }

        public static bool UpdateNewsPrV2Info_2(NewsPrEntity newsPrInfo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.UpdateNewsPrV2Info_2(newsPrInfo);
            }
            return returnValue;
        }

        public static NewsPrEntity GetNewsPrByBookingId(long bookingId)
        {
            NewsPrEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.GetNewsPrByBookingId(bookingId);
            }
            return returnValue;
        }

        public static bool RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.RecieveNewsPrRetailIntoCms(newsInfo, newsPrInfo, listNewsRelationId);
            }
            return returnValue;
        }

        public static List<NewsPrCurrentDateEntity> GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows)
        {
            List<NewsPrCurrentDateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.GetListNewsPrCurrentDate(Date, UnPublish, PageIndex, PageSize, ref TotalRows);
            }
            return returnValue;
        }
        
        #region ViewPlus
        public static List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize,int bidType, ref int TotalRow)
        {
            List<NewsViewPlusListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.ViewPlus_GetList(ZoneId, DateFrom, DateTo, PageIndex, PageSize, bidType, ref TotalRow);
            }
            return returnValue;
        }
        public static bool ViewPlus_Insert(ViewPlusEntity viewPlus)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.ViewPlus_Insert(viewPlus);
            }
            return returnValue;
        }
        public static bool ViewPlus_Delete(string Ids)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPrMainDal.ViewPlus_Delete(Ids);
            }
            return returnValue;
        }

        #endregion

        #region zoneviewplus
        public static List<ZoneViewPlusEntity> GetAllZoneViewPlus_by_TypeId_ZoneId(int typeId, int zoneId)
        {
            List<ZoneViewPlusEntity> lst = null;
            using (var db = new CmsMainDb())
            {
                lst = db.NewsPrMainDal.GetAllZoneViewPlus_by_TypeId_ZoneId(typeId, zoneId);
            }
            return lst;
        }
        #endregion
    }
}
