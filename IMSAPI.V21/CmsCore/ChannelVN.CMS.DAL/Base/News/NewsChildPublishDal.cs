﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsChildPublishDal
    {
        public static bool InsertNewsChildPublish(NewsChildPublishEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildPublishMainDal.InsertNewsChildPublish(entity);
            }
            return returnValue;
        }
        public static bool DeleteNewsChildPublish(long parentNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildPublishMainDal.DeleteNewsChildPublish(parentNewsId);
            }
            return returnValue;
        }
        public static bool UpdateNewsChildPublish(NewsChildPublishEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildPublishMainDal.UpdateNewsChildPublish(entity);
            }
            return returnValue;
        }
        public static List<NewsChildPublishEntity> GetNewsChildPublishByNewsId(long newsParentId) {
            List<NewsChildPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildPublishMainDal.GetNewsChildPublishByNewsId(newsParentId);
            }
            return returnValue;
        }
    }
}
