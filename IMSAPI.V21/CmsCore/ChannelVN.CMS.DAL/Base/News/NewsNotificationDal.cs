﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsNotificationDal
    {
        public static bool UpdateNotification(long newsId, string username, int newsStatus, bool isWarning, string message)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.UpdateNotification(newsId, username, newsStatus, isWarning, message);
            }
            return returnValue;
        }
        public static bool ReadNotification(long newsId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.ReadNotification(newsId, username);
            }
            return returnValue;
        }
        public static bool UpdateLabel(long newsId, string username, int labelId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.UpdateLabel(newsId, username, labelId);
            }
            return returnValue;
        }
        public static List<NewsInListWithNotifyEntity> SearchNewsWithNotification(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, string listStatus, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListWithNotifyEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.SearchNewsWithNotification(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, listStatus, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListWithNotifyEntity> SearchNewsWithNotificationV2(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int sortOrder, string listStatus, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListWithNotifyEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.SearchNewsWithNotificationV2(keyword, username, zoneId, fromDate, toDate, sortOrder, listStatus, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsNotificationEntity> GetNewsNotificationByListNewsId(string listNewsId, string username)
        {
            List<NewsNotificationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.GetNewsNotificationByListNewsId(listNewsId, username);
            }
            return returnValue;
        }
        public static List<NewsNotificationEntity> GetUnReadNewsNotificationByUsername(string username, DateTime getFromDate, ref int unreadCount)
        {
            List<NewsNotificationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsNotificationMainDal.GetUnReadNewsNotificationByUsername(username, getFromDate, ref unreadCount);
            }
            return returnValue;
        }
    }
}
