﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using System.Data;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsSourceDal
    {
        /// <summary>
        /// Insert NewsSource 
        /// Last update: 06/05/2013
        /// </summary>
        /// <param name="newsSourceEntity"></param>
        /// <param name="newsSourceId"></param>
        /// <returns></returns>
        public static bool Insert(NewsSourceEntity newsSourceEntity, ref int newsSourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.Insert(newsSourceEntity, ref newsSourceId);
            }
            return returnValue;
        }
        /// <summary>
        /// NewsSource Update
        /// </summary>
        /// <param name="newsSourceEntity"></param>
        /// <returns></returns>
        public static bool Update(NewsSourceEntity newsSourceEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.Update(newsSourceEntity);
            }
            return returnValue;
        }
        /// <summary>
        /// News Source Delete By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.Delete(id);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static bool DeleteAllNewsBySource(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.DeleteAllNewsBySource(newsId);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static NewsSourceEntity GetById(int id)
        {
            NewsSourceEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.GetById(id);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<NewsSourceEntity> GetAll(string keyword)
        {
            List<NewsSourceEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.GetAll(keyword);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsBySourceEntity> GetListInNews(long newsId)
        {
            List<NewsBySourceEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSourceMainDal.GetListInNews(newsId);
            }
            return returnValue;
        }
    }
}
