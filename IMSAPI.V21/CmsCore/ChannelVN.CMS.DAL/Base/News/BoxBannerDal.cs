﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class BoxBannerDal
    {
        public static bool Insert(BoxBannerEntity boxBanner, string zoneIdList, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.Insert(boxBanner, zoneIdList, ref id);
            }
            return returnValue;
        }

        public static bool Update(BoxBannerEntity boxBanner, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.Update(boxBanner, zoneIdList);
            }
            return returnValue;
        }

        public static bool UpdateSortOrder(string listOfBoxBannerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.UpdateSortOrder(listOfBoxBannerId);
            }
            return returnValue;
        }

        public static bool Delete(int boxBannerId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.Delete(boxBannerId);
            }
            return returnValue;
        }

        public static BoxBannerEntity GetById(int id)
        {
            BoxBannerEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<BoxBannerInZoneEntity> GetListZoneIdById(int id)
        {
            List<BoxBannerInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.GetListZoneIdById(id);
            }
            return returnValue;
        }

        public static List<BoxBannerEntity> GetListByZoneIdAndPosition(int zoneId, int position, int status, int type)
        {
            List<BoxBannerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.GetListByZoneIdAndPosition(zoneId, position, status, type);
            }
            return returnValue;
        }

        public static List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId)
        {
            List<BoxBannerZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxBannerMainDal.GetListZoneByBoxBannerId(boxBannerId);
            }
            return returnValue;
        }
    }
}
