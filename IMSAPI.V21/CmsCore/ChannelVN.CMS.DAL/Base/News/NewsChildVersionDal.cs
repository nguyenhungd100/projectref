﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsChildVersionDal
    {
        public static List<NewsChildVersionEntity> GetByNewsId(long parentNewsVersionId, bool isGetContent)
        {
            List<NewsChildVersionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.GetByNewsId(parentNewsVersionId, isGetContent);
            }
            return returnValue;
        }
        public static NewsChildVersionEntity GetByNewsIdAndOrder(long parentNewsId, int order, string userDoAction)
        {
            NewsChildVersionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.GetByNewsIdAndOrder(parentNewsId, order, userDoAction);
            }
            return returnValue;
        }
        public static bool Remove(long parentNewsId, int order, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.Remove(parentNewsId, order, userDoAction);
            }
            return returnValue;
        }
        public static bool Remove(long newsVersionId, int order)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.Remove(newsVersionId, order);
            }
            return returnValue;
        }

        public static bool Update(long parentNewsId, ref int order, string body, ref long newsVersionId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.Update(parentNewsId, ref order, body, ref newsVersionId, userDoAction);
            }
            return returnValue;
        }
        public static bool Update(NewsChildVersionEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildVersionMainDal.Update(entity);
            }
            return returnValue;
        }
    }
}
