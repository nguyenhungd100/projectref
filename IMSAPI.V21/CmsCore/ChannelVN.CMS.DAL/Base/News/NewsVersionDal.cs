﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsVersionDal
    {
        public static bool UpdateVersion(NewsEntity news, string listOfZoneId, string createBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.UpdateVersion(news, listOfZoneId, createBy);
            }
            return returnValue;
        }
        public static bool ReleaseVersion(long newsId, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.ReleaseVersion(newsId, lastModifiedBy);
            }
            return returnValue;
        }
        public static bool ReleaseFirstVersion(long newsId, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.ReleaseFirstVersion(newsId, lastModifiedBy);
            }
            return returnValue;
        }
        public static bool RemoveEditingVersion(long newsId, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.RemoveEditingVersion(newsId, lastModifiedBy);
            }
            return returnValue;
        }
        public static bool CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.CreateVersionForExistsNews(newsId, createdBy, ref newsVersionId);
            }
            return returnValue;
        }

        public static List<NewsVersionEntity> GetByNewsId(long newsId)
        {
            List<NewsVersionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<NewsVersionWithSimpleFieldsEntity> GetListVersionByNewsId(long newsId, string lastModifiedBy)
        {
            List<NewsVersionWithSimpleFieldsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.GetListVersionByNewsId(newsId, lastModifiedBy);
            }
            return returnValue;
        }
        public static NewsVersionEntity GetById(long id)
        {
            NewsVersionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.GetById(id);
            }
            return returnValue;
        }
        public static NewsVersionEntity GetLastestVersionByNewsId(long newsId, string lastModifiedBy)
        {
            NewsVersionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.GetLastestVersionByNewsId(newsId, lastModifiedBy);
            }
            return returnValue;
        }
        public static NewsVersionEntity GetNewsVersionByNewsIdAndCreatedBy(long newsId, string createdBy)
        {
            NewsVersionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsVersionMainDal.GetNewsVersionByNewsIdAndCreatedBy(newsId, createdBy);
            }
            return returnValue;
        }
    }
}
