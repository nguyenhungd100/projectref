﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsInExpertDal
    {        
        public static bool SendNewsInExpert(List<NewsInExpertEntity> newsExpert)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.SendNewsInExpert(newsExpert);
            }
            return returnValues;
        }
        public static bool ReSendNewsInExpert(int expertId, int expireDate, string note, long newsId)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ReSendNewsInExpert(expertId, expireDate, note, newsId);
            }
            return returnValues;
        }
        public static bool RecoverNewsInExpert(string listNewsId)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.RecoverNewsInExpert(listNewsId);
            }
            return returnValues;
        }
        public static bool ConfirmNewsInExpert(NewsInExpertEntity newsExpert)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ConfirmNewsInExpert(newsExpert);
            }
            return returnValues;
        }
        public static bool ReturnNewsInExpert(NewsInExpertEntity newsExpert)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ReturnNewsInExpert(newsExpert);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ListNewsInExpertByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ListNewsInExpertNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsInExpertAllByExpert(int expertId, int topicId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.ListNewsInExpertAllByExpert(expertId, topicId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsInExpertEntity> GetNewsExpertByIds(string ids)
        {
            List<NewsInExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsInExpertMainDal.GetNewsExpertByIds(ids);
            }
            return returnValues;
        }
    }
}
