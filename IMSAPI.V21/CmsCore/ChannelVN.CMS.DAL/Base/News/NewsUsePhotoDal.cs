﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.DAL.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsUsePhotoDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="photoPublishedIds"></param>
        /// <returns></returns>
        public static bool UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsUsePhotoMainDal.UpdatePhotoPublishedInNews(newsId, photoPublishedIds);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsUsePhotoEntity> GetAllNewsUsePhotoByNewsId(long newsId)
        {
            List<NewsUsePhotoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsUsePhotoMainDal.GetAllNewsUsePhotoByNewsId(newsId);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static int CountPhotoPublishedByNewsId(long newsId)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsUsePhotoMainDal.CountPhotoPublishedByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
