﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsOsDal
    {

        public static NewsOsEntity GetDetailById(long id) 
        {
            NewsOsEntity returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsOsMainDal.GetDetailById(id);
            }
            return returnValue;
        }

        public static List<NewsOsEntity> GetListNews(string keyWork, string catId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsOsEntity> returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsOsMainDal.GetListNews(keyWork, catId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static  bool UpdateStatusNewsOs(long newsId)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsOsMainDal.UpdateStatusNewsOs(newsId);
            }
            return returnValue;
        }

        public static bool DeleteById(long newsId)
        {
            bool returnValue;
            using (var db = new ExternalCmsDb())
            {
                returnValue = db.NewsOsMainDal.DeleteById(newsId);
            }
            return returnValue;
        }
    }
}
