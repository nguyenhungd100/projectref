﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsUseVideoDal
    {
        public static bool Update(NewsUseVideoEntity videoEntity)
        {
            bool returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.NewsUseVideoMainDal.Update(videoEntity);
            }
            return returnValue;
        }
        
        public static List<NewsUseVideoEntity> GetAllNewsUseVideoByNewsId(long newsId)
        {
            List<NewsUseVideoEntity> returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.NewsUseVideoMainDal.GetAllNewsUseVideoByNewsId(newsId);
            }
            return returnValue;
        }
        
        public static int CountVideoByNewsId(long newsId)
        {
            int returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.NewsUseVideoMainDal.CountVideoByNewsId(newsId);
            }
            return returnValue;
        }
        public static int DeleteVideoByNewsId(long newsId)
        {
            int returnValue;
            using (var db = new CmsVideoDb())
            {
                returnValue = db.NewsUseVideoMainDal.DeleteVideoByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
