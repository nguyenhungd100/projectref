﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsPublishDal
    {
        public static bool InsertNews(long newsId, int zoneId, string url)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.InsertNews(newsId, zoneId, url);
            }
            return returnValue;
        }

        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsIds(string newsIds)
        {
            List<NewsPublishForNewsRelationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetRelatedNewsByNewsIds(newsIds);
            }
            return returnValue;
        }
        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsId(long newsId)
        {
            List<NewsPublishForNewsRelationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetRelatedNewsByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<NewsPublishForObjectBoxEntity> SearchNewsPublishTemp_ForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForObjectBoxEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.SearchNewsPublishTemp_ForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsPublishForObjectBoxEntity> SearchNewsPublishForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForObjectBoxEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsPublishForObjectBoxEntity> SearchNewsPublishForNewsTagRelation(int zoneId, int tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForObjectBoxEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.SearchNewsPublishForNewsTagRelation(zoneId, tagId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsPublishForSearchEntity> SearchNewsPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForSearchEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.SearchNewsPublished(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        //Lấy danh sách  tin bài  trong khoảng thời gian và chuyên mục 
        public static List<NewsPublishEntity> GetListNewsPublishByDate(int zoneId, DateTime distributedDateFrom, DateTime distributedDateTo)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetListNewsPublishByDate(zoneId, distributedDateFrom, distributedDateTo);
            }
            return returnValue;
        }

        public static List<NewsPublishEntity> GetLastestNewsByHour(int hour, int zoneId)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetLastestNewsByHour(hour, zoneId);
            }
            return returnValue;
        }

        public static List<NewsPublishEntity> GetByNewsIdAndZoneId(int zoneId, long newsId, string listExcludePositionTypeId, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetByNewsIdAndZoneId(zoneId, newsId, listExcludePositionTypeId, isOnHome, displayInSlide, isBreakingNews, displayPosition, isFocus);
            }
            return returnValue;
        }

        public static NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId)
        {
            NewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetByNewsIdAndZoneId(newsId, zoneId);
            }
            return returnValue;
        }

        public static NewsPublishEntity GetLastestNewsExcludeListPositionType(long currentNewsId, int zoneId, string listExcludePositionTypeId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            NewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetLastestNewsExcludeListPositionType(currentNewsId, zoneId, listExcludePositionTypeId, includeChildZone, isOnHome, displayInSlide, isBreakingNews, displayPosition, isFocus);
            }
            return returnValue;
        }

        public static NewsPublishEntity GetLastestNewsByZoneId(int zoneId, long excludeNewsId)
        {
            NewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetLastestNewsByZoneId(zoneId, excludeNewsId);
            }
            return returnValue;
        }

        public static NewsPublishEntity GetLastestNewsByZoneIdExcludeNewsInPosition(int zoneId, long excludeNewsId, int excludeNewsInPositionId)
        {
            NewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetLastestNewsByZoneIdExcludeNewsInPosition(zoneId, excludeNewsId, excludeNewsInPositionId);
            }
            return returnValue;
        }
        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByTypeNewsId(long newsId, int type)
        {
            List<NewsPublishForNewsRelationEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetRelatedNewsByTypeNewsId(newsId, type);
            }
            return returnValue;
        }

        public static NewsPublishEntity GetNewsPublishByNewsId(long newsId)
        {
            NewsPublishEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<NewsPublishEntity> GetListNewsByListNewsId(string listNewsId)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.GetListNewsByListNewsId(listNewsId);
            }
            return returnValue;
        }
        #region New methods for optimization
        public static List<NewsPublishForSearchEntity> SearchByKeyword(int zoneId, string keyword, int DisplayPosition, int Priority, bool IsShowNewsBomd, int pageIndex, int pageSize, bool useFullDatabase)
        {
            List<NewsPublishForSearchEntity> returnValue = new List<NewsPublishForSearchEntity>();
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsPublishMainDal.SearchByKeyword(zoneId, keyword, DisplayPosition, Priority, IsShowNewsBomd, pageIndex, pageSize, useFullDatabase);
            }
            return returnValue;
        }
        #endregion
    }
}
