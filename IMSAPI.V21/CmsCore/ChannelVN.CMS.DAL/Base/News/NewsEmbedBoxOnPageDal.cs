﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsEmbedBoxOnPageDal
    {
        public static List<NewsEmbedBoxOnPageListEntity> GetListNewsEmbedBoxOnPage(int zoneId, int type)
        {
            List<NewsEmbedBoxOnPageListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsEmbedBoxOnPageMainDal.GetListNewsEmbedBoxOnPage(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(NewsEmbedBoxOnPageEntity newsEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsEmbedBoxOnPageMainDal.Insert(newsEmbebBox);
            }
            return returnValue;
        }

        public static void Update(string listNewsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsEmbedBoxOnPageMainDal.Update(listNewsId, zoneId, type);
            }
        }

        public static void Delete(long newsId, int zoneId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsEmbedBoxOnPageMainDal.Delete(newsId, zoneId, type);
            }
        }
    }
}
