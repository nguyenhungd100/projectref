﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsExtensionDal
    {
        public static NewsExtensionEntity GetValue(long newsId, int type)
        {
            NewsExtensionEntity returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetValue(newsId, type);
            }
            return returnValues;
        }
        public static int GetMaxValue(long newsId, int type)
        {
            int returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetMaxValue(newsId, type);
            }
            return returnValues;
        }
        public static bool SetValue(long newsId, int type, string value)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.SetValue(newsId, type, value);
            }
            return returnValues;
        }
        public static bool SendExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.SendExpertInNewsExtension(newsExtension);
            }
            return returnValues;
        }
        public static bool ReSendExpertInNewsExtension(int expertId, int expireDate, string note)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ReSendExpertInNewsExtension(expertId, expireDate, note);
            }
            return returnValues;
        }
        public static bool ConfirmExpertInNewsExtension(NewsExtensionEntity newsExtension)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ConfirmExpertInNewsExtension(newsExtension);
            }
            return returnValues;
        }
        public static bool ReturnExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ReturnExpertInNewsExtension(newsExtension);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ListNewsByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ListNewsNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsSimpleExpertEntity> ListNewsAllByExpert(int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSimpleExpertEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.ListNewsAllByExpert(expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValues;
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByListIds(string ids)
        {
            List<NewsExtensionEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetNewsExtensionByListIds(ids);
            }
            return returnValues;
        }
        public static bool DeleteByNewsId(long newsId)
        {
            bool returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.DeleteByNewsId(newsId);
            }
            return returnValues;
        }
        public static List<NewsExtensionEntity> GetByNewsId(long newsId)
        {
            List<NewsExtensionEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetByNewsId(newsId);
            }
            return returnValues;
        }
        public static List<NewsExtensionEntity> GetByListNewsId(string listNewsId)
        {
            List<NewsExtensionEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetByListNewsId(listNewsId);
            }
            return returnValues;
        }
        public static List<NewsExtensionEntity> GetByTypeAndVaue(int type, string value)
        {
            List<NewsExtensionEntity> returnValues;
            using (var db = new CmsMainDb())
            {
                returnValues = db.NewsExtensionMainDal.GetByTypeAndVaue(type, value);
            }
            return returnValues;
        }

        public static List<NewsExtensionEntity> InitESAllNewsExtension(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<NewsExtensionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsExtensionMainDal.InitESAllNewsExtension(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
    }
}
