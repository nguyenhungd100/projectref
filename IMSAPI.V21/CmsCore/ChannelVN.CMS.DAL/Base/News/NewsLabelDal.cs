﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsLabelDal
    {
        public static NewsLabelEntity GetNewsLabelById(int id)
        {
            NewsLabelEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLabelMainDal.GetNewsLabelById(id);
            }
            return returnValue;
        }
    }
}
