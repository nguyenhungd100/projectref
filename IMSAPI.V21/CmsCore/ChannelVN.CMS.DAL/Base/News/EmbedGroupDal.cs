﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class EmbedGroupDal
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public static bool Insert(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.Insert(embedGroup, listOfZoneId);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public static bool Update(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.Update(embedGroup, listOfZoneId);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listOfEmbedGroupId"></param>
        /// <returns></returns>
        public static bool UpdateSortOrder(string listOfEmbedGroupId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.UpdateSortOrder(listOfEmbedGroupId);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<EmbedGroupEntity> GetAll()
        {
            List<EmbedGroupEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.GetAll();
            }
            return returnValue;
        }
        public static EmbedGroupEntity GetById(int id)
        {
            EmbedGroupEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.GetById(id);
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<ZoneInEmbedGroupEntity> GetListZoneById(int id)
        {
            List<ZoneInEmbedGroupEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.GetListZoneById(id);
            }
            return returnValue;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedGroupMainDal.Delete(id);
            }
            return returnValue;
        }
    }
}
