﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsGroupDal
    {
        public static NewsGroupEntity GetNewsGroupById(int id)
        {
            NewsGroupEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.GetNewsGroupById(id);
            }
            return returnValue;
        }
        public static bool UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostRead(groupNewsId, withinDays, rootZoneId, excludeLastDays);
            }
            return returnValue;
        }
        public static bool UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays, int zoneId, int itemCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostReadByZone(groupNewsId, withinDays, excludeLastDays, zoneId, itemCount);
            }
            return returnValue;
        }
        public static bool UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostReadV2(groupNewsId, withinDays, excludeLastDays, itemCount);
            }
            return returnValue;
        }
        public static bool UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostComment(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays);
            }
            return returnValue;
        }

        public static bool UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostReadDaily(groupNewsId, rootZoneId, excludeLastDays);
            }
            return returnValue;
        }

        public static bool UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostReadHourly(groupNewsId, rootZoneId, excludeLastDays);
            }
            return returnValue;
        }

        public static bool UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays, int topNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostCommentByZone(groupNewsId, withinDays, rootZoneId, objectType, excludeLastDays, topNews);
            }
            return returnValue;
        }

        public static bool UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays, int topNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostCommentV2(groupNewsId, withinDays, objectType, excludeLastDays, topNews);
            }
            return returnValue;
        }
        public static List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow)
        {
            List<NewsGroupDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostReadV3(groupNewsId, zoneId, withinHours, totalRow);
            }
            return returnValue;
        }
        public static bool UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopMostLikeShare(groupNewsId, zoneId, lstNewsId, likeShareCountList);
            }
            return returnValue;
        }
        public static bool UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsGroupMainDal.UpdateTopCommentCountInDay(groupNewsId, lstNewsId, lstCommentCount);
            }
            return returnValue;
        }
    }
}
