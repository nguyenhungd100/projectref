﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsConfigDal
    {
        public static NewsConfigEntity GetByConfigName(string configName)
        {
            NewsConfigEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.GetByConfigName(configName);
            }
            return returnValue;
        }
        public static NewsConfigEntity GetNewsConfigByTypeInitValue(int type, string value)
        {
            NewsConfigEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.GetNewsConfigByTypeInitValue(type, value);
            }
            return returnValue;
        }

        public static List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey, int type)
        {
            List<NewsConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.GetListConfigByGroupKey(groupConfigKey, type);
            }
            return returnValue;
        }
        public static List<NewsConfigEntity> GetAll(int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsConfigEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.GetAll(pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static bool SetValue(NewsConfigEntity obj)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.SetValue(obj);
            }
            return returnValue;
        }
        public static bool SetValue(string configName, string configValue)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.SetValue(configName, configValue);
            }
            return returnValue;
        }
        public static bool SetValue(string configName, string configValue, int configType)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.SetValue(configName, configValue, configType);
            }
            return returnValue;
        }
        public static bool SetValueAndLabel(string configName, string configLabel, string configValue)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.SetValueAndLabel(configName, configLabel, configValue);
            }
            return returnValue;
        }
        public static bool SetValueAndLabel(string configName, string configLabel, string configValue, int configType, string configInitValue="")
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.SetValueAndLabel(configName, configLabel, configValue, configType, configInitValue);
            }
            return returnValue;
        }
        public static bool DeleteNewsConfigByConfigName(string configName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsConfigMainDal.DeleteNewsConfigByConfigName(configName);
            }
            return returnValue;
        }
    }
}
