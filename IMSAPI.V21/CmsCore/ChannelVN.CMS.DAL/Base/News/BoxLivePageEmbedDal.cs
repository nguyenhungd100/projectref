﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class BoxLivePageEmbedDal
    {
        public static bool Insert(BoxLivePageEmbedEntity item, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.Insert(item, ref id);
            }
            return returnValue;
        }

        public static bool Update(BoxLivePageEmbedEntity item)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.Update(item);
            }
            return returnValue;
        }

        public static bool UpdateSortOrder(string listOfBoxLivePageEmbedId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.UpdateSortOrder(listOfBoxLivePageEmbedId);
            }
            return returnValue;
        }

        public static bool Delete(int boxId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.Delete(boxId);
            }
            return returnValue;
        }

        public static BoxLivePageEmbedEntity GetById(int id)
        {
            BoxLivePageEmbedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.GetById(id);
            }
            return returnValue;
        }
        
        public static List<BoxLivePageEmbedEntity> GetListBoxLivePageEmbed(string pageName, string pageUrl, int position, int status, int type, int templateId)
        {
            List<BoxLivePageEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.GetListBoxLivePageEmbed(pageName, pageUrl, position, status, type, templateId);
            }
            return returnValue;
        }

        #region BoxInboundTemplate
        public static bool InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.InsertBoxInboundTemplate(item, ref id);
            }
            return returnValue;
        }

        public static bool UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.UpdateBoxInboundTemplate(item);
            }
            return returnValue;
        }        

        public static bool DeleteBoxInboundTemplate(int boxId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.DeleteBoxInboundTemplate(boxId);
            }
            return returnValue;
        }

        public static BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            BoxInboundTemplateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.GetBoxInboundTemplateById(id);
            }
            return returnValue;
        }

        public static List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BoxInboundTemplateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxLivePageEmbedMainDal.GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion

    }
}
