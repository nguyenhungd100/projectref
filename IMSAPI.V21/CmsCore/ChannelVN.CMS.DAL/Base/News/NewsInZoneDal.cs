﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsInZoneDal
    {
        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            List<NewsInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsInZoneMainDal.GetNewsInZoneByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<NewsInZoneEntity> GetNewsInZoneByListNewsId(string listNewsId)
        {
            List<NewsInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsInZoneMainDal.GetNewsInZoneByListNewsId(listNewsId);
            }
            return returnValue;
        }

        public static bool Insert(NewsInZoneEntity newsInZone)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsInZoneMainDal.Insert(newsInZone);
            }
            return returnValue;
        }
    }
}
