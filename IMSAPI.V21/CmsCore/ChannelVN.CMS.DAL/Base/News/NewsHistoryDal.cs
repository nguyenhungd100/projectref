﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsHistoryDal
    {
        public static bool InsertNewsHistory(NewsHistoryEntity newsHistory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsHistoryMainDal.InsertNewsHistory(newsHistory);
            }
            return returnValue;
        }
        public static List<NewsHistoryEntity> GetByNewsId(long newsId)
        {
            List<NewsHistoryEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsHistoryMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
