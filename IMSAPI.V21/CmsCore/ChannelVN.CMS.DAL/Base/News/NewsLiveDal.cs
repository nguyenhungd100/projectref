﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsLiveDal
    {
        #region Sets

        public static bool Insert(NewsLiveEntity newsLiveEntity, ref long newsLiveId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.Insert(newsLiveEntity, ref newsLiveId);
            }
            return returnValue;
        }

        public static bool Update(NewsLiveEntity newsLiveEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.Update(newsLiveEntity);
            }
            return returnValue;
        }

        public static bool Delete(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.Delete(id);
            }
            return returnValue;
        }

        public static bool ChangeStatus(long id, int status, string publishedBy, string editedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.ChangeStatus(id, status, publishedBy, editedBy);
            }
            return returnValue;
        }

        public static bool UpdateContent(long id, string content)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.UpdateContent(id, content);
            }
            return returnValue;
        }

        public static bool UpdateSettings(NewsLiveSettingsEntity newsLiveSettings)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.UpdateSettings(newsLiveSettings);
            }
            return returnValue;
        }

        public static bool AddAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.AddAuthor(newsLiveAuthorEntity);
            }
            return returnValue;
        }

        public static bool DeleteAuthor(long newsId, int authorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.DeleteAuthor(newsId, authorId);
            }
            return returnValue;
        }

        #endregion

        #region Gets

        public static List<NewsLiveEntity> GetAll(long newsId, int status, int order)
        {
            List<NewsLiveEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.GetAll(newsId, status, order);
            }
            return returnValue;
        }

        public static List<NewsLiveEntity> GetPaging(long newsId, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsLiveEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.GetPaging(newsId, status, order, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static NewsLiveEntity GetById(long id)
        {
            NewsLiveEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<NewsLiveAuthorEntity> GetAuthorList(long newsId)
        {
            List<NewsLiveAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.GetAuthorList(newsId);
            }
            return returnValue;
        }

        public static NewsLiveSettingsEntity GetSettings(long newsId)
        {
            NewsLiveSettingsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLiveMainDal.GetSettings(newsId);
            }
            return returnValue;
        }

        #endregion
    }
}
