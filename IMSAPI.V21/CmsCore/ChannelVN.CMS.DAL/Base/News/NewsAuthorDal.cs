﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsAuthorDal
    {
        public static bool Insert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.Insert(newsAuthorEntity, ref newsAuthorId);
            }
            return returnValue;
        }

        /// <summary>
        /// Insert into NewsByAuthor table
        /// </summary>
        /// <param name="newsByAuthorEntity"></param>
        /// <returns></returns>
        public static bool InsertToNewsByAuthor(NewsByAuthorEntity newsByAuthorEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.InsertToNewsByAuthor(newsByAuthorEntity);
            }
            return returnValue;
        }

        public static bool Update(NewsAuthorEntity newsAuthorEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.Update(newsAuthorEntity);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.Delete(id);
            }
            return returnValue;
        }

        /// <summary>
        /// Delete All rows in NewsByAuthor table
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static bool DeleteAllNewsByAuthor(long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.DeleteAllNewsByAuthor(newsId);
            }
            return returnValue;
        }

        public static NewsAuthorEntity GetById(int id)
        {
            NewsAuthorEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<NewsAuthorEntity> GetListByUserName(string username)
        {
            List<NewsAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.GetListByUserName(username);
            }
            return returnValue;
        }

        public static NewsAuthorEntity GetByUserData(string userData, int authorType)
        {
            NewsAuthorEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.GetByUserData(userData, authorType);
            }
            return returnValue;
        }

        public static List<NewsAuthorEntity> GetAll()
        {
            List<NewsAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.GetAll();
            }
            return returnValue;
        }

        public static List<NewsAuthorEntity> Search(string keyword)
        {
            List<NewsAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.Search(keyword);
            }
            return returnValue;
        }

        /// <summary>
        /// From NewsByAuthorEntity
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public static List<NewsByAuthorEntity> GetListInNews(long newsId)
        {
            List<NewsByAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAuthorMainDal.GetListInNews(newsId);
            }
            return returnValue;
        }
    }
}
