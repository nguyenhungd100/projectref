﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsAnalyticAdtechDal
    {
        public static bool ResetViewCount()
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.ResetViewCount();
            }
            return returnValue;
        }
        public static bool UpdateViewCount(long newsId, int viewCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.UpdateViewCount(newsId, viewCount);
            }
            return returnValue;
        }
        public static bool BatchUpdateViewCount(string listNewsId, string listViewCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.BatchUpdateViewCount(listNewsId, listViewCount);
            }
            return returnValue;
        }
        public static List<NewsWithAnalyticEntity> GetTopMostReadInHour(int top)
        {
            List<NewsWithAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.GetTopMostReadInHour(top);
            }
            return returnValue;
        }

        public static List<NewsAnalyticEntity> GetLastUpdateViewCount(DateTime lastUpdateViewCount)
        {
            List<NewsAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.GetLastUpdateViewCount(lastUpdateViewCount);
            }
            return returnValue;
        }
        public static List<NewsAnalyticEntity> GetTopLastestViewCount(int top)
        {
            List<NewsAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticAdtechMainDal.GetTopLastestViewCount(top);
            }
            return returnValue;
        }
    }
}
