﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsWaitRepublishDal
    {
        public static bool InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                       string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.InsertNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.UpdateNewsWaitRepublish(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool ChangeStatusToProcessed(long id, string lastModidiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.ChangeStatusToProcessed(id, lastModidiedBy);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsWaitRepublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.SearchNewsWaitRepublish(keyword, username, zoneIds, fromDate, toDate, filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static NewsEntity GetNewsWaitRepublishById(long id)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.GetNewsWaitRepublishById(id);
            }
            return returnValue;
        }
        public static int NewsWaitRepublishCountDown(string username, string zoneIds, int filterFieldForUsername, int type)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsWaitRepublishMainDal.NewsWaitRepublishCountDown(username, zoneIds, filterFieldForUsername, type);
            }
            return returnValue;
        }
    }
}
