﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsLogExternalActionDal
    {
        public static List<NewsLogExternalActionEntity> GetByNewsIdAndActionType(long newsId, int actionType)
        {
            List<NewsLogExternalActionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLogExternalActionMainDal.GetByNewsIdAndActionType(newsId, actionType);
            }
            return returnValue;
        }

        public static bool Insert(NewsLogExternalActionEntity newsLogExternalAction)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsLogExternalActionMainDal.Insert(newsLogExternalAction);
            }
            return returnValue;
        }
    }
}
