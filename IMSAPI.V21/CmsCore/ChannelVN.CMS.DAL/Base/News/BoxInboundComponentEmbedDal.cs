﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class BoxInboundComponentEmbedDal
    {
        public static bool Insert(BoxInboundComponentEmbedEntity item, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.Insert(item, ref id);
            }
            return returnValue;
        }

        public static bool Update(BoxInboundComponentEmbedEntity item)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.Update(item);
            }
            return returnValue;
        }

        public static bool UpdateSortOrder(string listOfBoxInboundComponentEmbedId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.UpdateSortOrder(listOfBoxInboundComponentEmbedId);
            }
            return returnValue;
        }

        public static bool Delete(int boxId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.Delete(boxId);
            }
            return returnValue;
        }

        public static BoxInboundComponentEmbedEntity GetById(int id)
        {
            BoxInboundComponentEmbedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.GetById(id);
            }
            return returnValue;
        }
        
        public static List<BoxInboundComponentEmbedEntity> GetListBoxInboundComponentEmbed(long newsId, int position, int status, int type)
        {
            List<BoxInboundComponentEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.GetListBoxInboundComponentEmbed(newsId, position, status, type);
            }
            return returnValue;
        }

        #region BoxInboundTemplate
        public static bool InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.InsertBoxInboundTemplate(item, ref id);
            }
            return returnValue;
        }

        public static bool UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.UpdateBoxInboundTemplate(item);
            }
            return returnValue;
        }        

        public static bool DeleteBoxInboundTemplate(int boxId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.DeleteBoxInboundTemplate(boxId);
            }
            return returnValue;
        }

        public static BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            BoxInboundTemplateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.GetBoxInboundTemplateById(id);
            }
            return returnValue;
        }

        public static List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<BoxInboundTemplateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxInboundComponentEmbedMainDal.GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion

    }
}
