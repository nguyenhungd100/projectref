﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsDal
    {
        public static bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.InsertNews(news, zoneId, zoneIdList, tagIdList,
                                   tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId);
            }
            return returnValue;
        }

        public static bool InsertNewsForAppBotPublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId, string action="")
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.InsertNewsForAppBotPublish(news, zoneId, zoneIdList, tagIdList,
                                   tagIdListForPrimary, newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote, sourceId, action);
            }
            return returnValue;
        }

        public static NewsEntity GetNewsIdByParentNewsIdForAppbotPublish(long parentId)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsIdByParentNewsIdForAppbotPublish(parentId);
            }
            return returnValue;
        }

        public static bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNews(news, zoneId, zoneIdList, tagIdList, tagIdListForPrimary,
                    newsRelationIdList, listOfAuthorId, listOfAuthorName, listOfAuthorNote,
                    sourceId);
            }
            return returnValue;
        }

        public static bool UpdateNewsAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNewsAvatar(newsId, avatar, avatar2, avatar3, avatar4, avatar5, avatarCustom);
            }
            return returnValue;
        }

        public static bool DeleteNews(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.DeletePhysical(id);
            }
            return returnValue;
        }

        public static bool ChangeStatusToWaitForEdit(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToWaitForEdit(id);
            }
            return returnValue;
        }

        public static bool ChangeStatusToRecievedForEdit(long id, string editedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRecievedForEdit(id, editedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToWaitForPublish(long id, string editedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToWaitForPublish(id, editedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToWaitForEditorialBoard(long id, string approvedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToWaitForEditorialBoard(id, approvedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToRecievedForPublish(long id, string editedBy, string approvedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRecievedForPublish(id, editedBy, approvedBy);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRecievedForEditorialBoard(long id, string approvedBy, string publishedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRecievedForEditorialBoard(id, approvedBy, publishedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToPublished(long id, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string publishedBy, long publishedDate, string publishedContent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToPublished(id, newsPositionTypeForHome,
                    newsPositionOrderForHome, newsPositionTypeForList, newsPositionOrderForList, publishedBy,
                    publishedDate, publishedContent);
            }
            return returnValue;
        }
        
        public static bool ChangeStatusToUnpublished(long id, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToUnpublished(id, lastModifiedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToMovedToTrash(long id, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToMovedToTrash(id, lastModifiedBy);
            }
            return returnValue;
        }        

        public static bool ChangeStatusToRestoreFromTrash(long id, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRestoreFromTrash(id, lastModifiedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToCrawlerForEdit(long id, string createdBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToCrawlerForEdit(id, createdBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnToCooperator(long id, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnToCooperator(id, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnedToReporter(long id, string username, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnedToReporter(id, username, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnedToEditorialSecretary(long id, string approvedBy, string username, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnedToEditorialSecretary(id, approvedBy, username, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnedToMyEditorialSecretary(long id, string approvedBy, string username, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnedToMyEditorialSecretary(id, approvedBy, username, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnedToEditor(long id, string receiver, string username, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnedToEditor(id, receiver, username, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnedToMyEditor(long id, string receiver, string username, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToReturnedToMyEditor(id, receiver, username, note);
            }
            return returnValue;
        }

        public static bool ChangeStatusToRecieveFromReturnStore(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRecieveFromReturnStore(id);
            }
            return returnValue;
        }

        public static bool ChangeStatusToRecievedFromCooperatorStore(long id, int recievedWithStatus, string recievedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusToRecievedFromCooperatorStore(id, recievedWithStatus, recievedBy);
            }
            return returnValue;
        }

        public static bool ChangeStatusForwardToOtherEditor(long id, string receiver)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusForwardToOtherEditor(id, receiver);
            }
            return returnValue;
        }

        public static bool ChangeStatusForwardToOtherSecretary(long id, string receiver)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ChangeStatusForwardToOtherSecretary(id, receiver);
            }
            return returnValue;
        }

        public static NewsForValidateEntity GetNewsForValidateById(long id)
        {
            NewsForValidateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsForValidateById(id);
            }
            return returnValue;
        }

        public static NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        {
            NewsInfoForCachedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsInfoForCachedById(id);
            }
            return returnValue;
        }

        public static NewsEntity GetNewsById(long id)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsById(id);
            }
            return returnValue;
        }

        public static NewsEntity GetNewsByNewsIdForInit(long id)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsByNewsIdForInit(id);
            }
            return returnValue;
        }
        public static int GetNewsTypeByNewsId(long id)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsTypeByNewsId(id);
            }
            return returnValue;
        }
        public static List<NewsForOtherAPIEntity> GetNewsForOtherAPI(string listNewsId)
        {
            List<NewsForOtherAPIEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsForOtherAPI(listNewsId);
            }
            return returnValue;
        }

        public static List<NewsWithAnalyticEntity> GetNewsByListNewsId(int status, string listNewsId)
        {
            List<NewsWithAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsByListNewsId(status, listNewsId);
            }
            return returnValue;
        }
        public static List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId)
        {
            List<NewsWithExpertEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetAllNewsByListNewsId(listNewsId);
            }
            return returnValue;
        }
        public static List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetAllNewsForExportByListNewsId(listNewsId);
            }
            return returnValue;
        }

        public static List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            List<NewsWithDistributionDateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsDistributionDateByListNewsId(listNewsId);
            }
            return returnValue;
        }

        public static List<NewsForSuggestionEntity> GetNewsUseInterviewByListInterviewId(string listInterviewId)
        {
            List<NewsForSuggestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsUseInterviewByListInterviewId(listInterviewId);
            }
            return returnValue;
        }

        public static List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        {
            List<NewsWithAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetTopMostNewsInHour(top, zoneId);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNews(keyword, username, zoneIds, fromDate, toDate,
                    filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsRoyaltiesV2Entity> ListNewsPublishForRoyalties(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsRoyaltiesV2Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.ListNewsPublishForRoyalties(fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsEntity> InitRedisAllNews(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow, int zoneid)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.InitRedisAllNews(pageIndex, pageSize, fromDate, toDate, ref totalRow, zoneid);
            }
            return returnValue;
        }
        public static List<NewsEntity> InitRedisAllNewsPublish(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.InitRedisAllNewsPublish(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsEntity> InitESAllNews(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow, int zoneid)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.InitESAllNews(pageIndex, pageSize, fromDate, toDate, ref totalRow, zoneid);
            }
            return returnValue;
        }        

        public static List<NewsEntity> GetNewsPublishByDate(DateTime fromDate, DateTime toDate)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsPublishByDate(fromDate, toDate);
            }
            return returnValue;
        }

        public static bool UpdateNewsByPublishBy(long newdId, string publishedBy, string approvedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNewsByPublishBy(newdId, publishedBy, approvedBy);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsTemp(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsTemp(keyword, username, zoneIds, fromDate, toDate,
                    filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsByNewsId(long NewsId)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByNewsId(NewsId);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsPublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishedForSecretary(keyword, username, zoneId, fromDate, toDate,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsTemp_PublishedForSecretary(keyword, username, zoneId, fromDate, toDate,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPr(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPr(keyword, username, zoneIds, fromDate, toDate,
                    filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByUsername(keyword, username, zoneIds, fromDate, toDate,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsTemp_ByUsername(keyword, username, zoneIds, fromDate, toDate,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsByType(string keyword, int type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByType(keyword, type, zoneIds, fromDate, toDate,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="top"></param>
        /// <param name="typeId"></param>
        /// <param name="zoneId"></param>
        /// <param name="listExcludePositionTypeId"></param>
        /// <param name="includeListNewsId"></param>
        /// <param name="includeChildZone"></param>
        /// <param name="isOnHome"></param>
        /// <param name="displayInSlide"></param>
        /// <param name="isBreakingNews"></param>
        /// <param name="displayPosition"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public static List<NewsPublishEntity> GetTopLastestNewsForAutoUpdateNewsPosition(int top, int typeId, int zoneId, string listExcludePositionTypeId, string includeListNewsId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetTopLastestNewsForAutoUpdateNewsPosition(top, typeId, zoneId, listExcludePositionTypeId, includeListNewsId, includeChildZone, isOnHome, displayInSlide, isBreakingNews, displayPosition, isFocus);
            }
            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="top"></param>
        /// <param name="typeId"></param>
        /// <param name="zoneId"></param>
        /// <param name="includeChildZone"></param>
        /// <param name="isOnHome"></param>
        /// <param name="displayInSlide"></param>
        /// <param name="isBreakingNews"></param>
        /// <param name="displayPosition"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public static List<NewsPublishEntity> GetTopLastestNewsForUpdateNewsPosition(int top, int typeId, int zoneId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            List<NewsPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetTopLastestNewsForUpdateNewsPosition(top, typeId, zoneId,
                    includeChildZone, isOnHome, displayInSlide, isBreakingNews, displayPosition, isFocus);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsByStatus(string keyword, string usernameDoAction, string usernameUpdateNews, int filterFieldForUsernameUpdateNews, string zoneIds, DateTime fromDate, DateTime toDate, int type, int status, bool isGetTotalRow, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByStatus(keyword, usernameDoAction, usernameUpdateNews,
                    filterFieldForUsernameUpdateNews, zoneIds, fromDate, toDate, type, status, isGetTotalRow,
                    pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsEntity> SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByTag(tagIds, tagNames, searchBytag, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishExcludeNewsInTag(zoneId, keyword, tagId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishExcludeNewsInMc(zoneId, keyword, mcId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishExcludeNewsInThread(zoneId, keyword, threadId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        /// <summary>
        /// NANIA bổ sung hàm SearchNewsByFocusKeyword
        /// Tìm các bài có sử dụng từ khóa Focus này
        /// Created By: 22/03/2013
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public static List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByFocusKeyword(keyword, top);
            }
            return returnValue;
        }

        public static List<NewsEntity> CheckNewsByKeyword(string keyword, int page, int num, ref int total)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CheckNewsByKeyword(keyword, page, num, ref total);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, int priority, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, string excludePositionTypes, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsWhichPublishedForNewsPosition(zoneId, keyword, type,
                    displayPosition, distributedDateFrom, distributedDateTo, excludeNewsIds, newstype,
                    excludePositionTypes, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsEntity> SearchNewsByTagId(long tagId)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByTagId(tagId);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByTagIdWithPaging(tagId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByMcIdWithPaging(mcId, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByThreadIdWithPaging(threadId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsByTopicIdWithPaging(string keyword, long topicId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByTopicIdWithPaging(keyword, topicId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsEntity> SearchNewsPublishByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishByTopicIdWithPaging(topicId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsEntity> SearchHotNewsByTopicId(long topicId)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchHotNewsByTopicId(topicId);
            }
            return returnValue;
        }

        public static List<NewsCounterEntity> CounterNewsByStatus(string accountName, string zoneIds, int role, string commonStatus, bool isByZone)
        {
            List<NewsCounterEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CounterNewsByStatus(accountName, zoneIds, role, commonStatus, isByZone);
            }
            return returnValue;
        }

        public static List<NewsCounterEntity> GetCounter(string accountName, int zoneId)
        {
            List<NewsCounterEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetCounter(accountName, zoneId);
            }
            return returnValue;
        }

        public static CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate)
        {
            CategoryCounterEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetZoneCounter(zoneId, fromDate, endDate);
            }
            return returnValue;
        }

        /// <summary>
        /// Select dánh sách bài viết để tính nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public static List<NewsForRoyaltiesEntity> SelectRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            List<NewsForRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }

        public static List<NewsForRoyaltiesEntity> SelectBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            List<NewsForRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }

        public static List<NewsForRoyaltiesEntity> SelectPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            List<NewsForRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectPRRoyalties(zoneId, startDate, endDate, creator, author, pageIndex, pageSize, order, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }
        /// <summary>
        /// Thống kê nhuận bút dành cho Cafebiz
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="creator"></param>
        /// <param name="author"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="NewsCategories"></param>
        /// <param name="newsType"></param>
        /// <param name="viewCountFrom"></param>
        /// <param name="viewCountTo"></param>
        /// <returns></returns>
        public static List<NewsForRoyaltiesEntity> SelectRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                        int pageIndex, int pageSize, ref int totalRows,
                                                                        int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = -1)
        {
            List<NewsForRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectRoyaltiesV2(zoneId, startDate, endDate, creator, author,
                    pageIndex, pageSize, ref totalRows, order, NewsCategories, newsType, viewCountFrom, viewCountTo);
            }
            return returnValue;
        }

        public static List<NewsForRoyaltiesEntity> SelectPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                        int pageIndex, int pageSize, ref int totalRows,
                                                                        int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = -1)
        {
            List<NewsForRoyaltiesEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectPRRoyaltiesV2(zoneId, startDate, endDate, creator, author,
                    pageIndex, pageSize, ref totalRows, order, NewsCategories, newsType, viewCountFrom, viewCountTo);
            }
            return returnValue;
        }

        /// <summary>
        /// Count tổng số nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public static int CountRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CountRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }

        public static int CountBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CountBusinessPrRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;

        }
        public static int CountPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CountPRRoyalties(zoneId, startDate, endDate, creator, author, NewsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }
        /// <summary>
        /// Get tong tien nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public static decimal SelectTotalPrice(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            decimal returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SelectTotalPrice(zoneId, startDate, endDate, creator, author,
                    newsCategories, newsType, viewCountFrom, viewCountTo, originalId);
            }
            return returnValue;
        }

        /// <summary>
        /// Update giá tiền cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Giá tiền cho bài viết</param>
        /// <returns></returns>
        public static bool UpdatePrice(long id, decimal price)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdatePrice(id, price);
            }
            return returnValue;
        }
        /// <summary>
        /// Update tiền bonus cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Tiền bonus cho bài viết</param>
        /// <returns></returns>
        public static bool UpdateBonusPrice(long id, decimal bonusPrice)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateBonusPrice(id, bonusPrice);
            }
            return returnValue;
        }
        public static bool UpdateNewsCategory(long id, int newsCategory)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNewsCategory(id, newsCategory);
            }
            return returnValue;
        }
        public static bool UpdateIsOnHome(long id, bool isOnHome)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateIsOnHome(id, isOnHome);
            }
            return returnValue;
        }
        public static bool UpdateIsFocus(long id, bool isFocus)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateIsFocus(id, isFocus);
            }
            return returnValue;
        }

        public static bool UpdateSeoReviewStatus(long id, int reviewStatus, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateSeoReviewStatus(id, reviewStatus, accountName);
            }
            return returnValue;
        }

        public static bool UpdateDisplayInSlide(long id, int displayInSlide)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateDisplayInSlide(id, displayInSlide);
            }
            return returnValue;
        }
        /// <summary>
        /// Update ghi chú về chấm nhuận bút cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="note">Ghi chú cho bài viết</param>
        /// <returns></returns>
        public static bool UpdateNoteRoyalties(long id, string note)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNoteRoyalties(id, note);
            }
            return returnValue;
        }

        public static bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateOnlyNewsTitle(newsId, newsTitle, newsUrl, accountName);
            }
            return returnValue;
        }

        public static bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName);
            }
            return returnValue;
        }

        public static bool UpdateNewsByErrorCheck(NewsEntity news, string newsUrl, string tag, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNewsByErrorCheck(news, newsUrl, tag, accountName);
            }
            return returnValue;
        }

        public static bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName);
            }
            return returnValue;
        }

        public static bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateDisplayPosition(newsId, displayPosition, accountName);
            }
            return returnValue;
        }
        public static bool UpdatePriority(long newsId, int priority, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdatePriority(newsId, priority, accountName);
            }
            return returnValue;
        }

        public static bool RemoveNewsPosition(int configPosition, int zoneId, int typeId, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.RemoveNewsPosition(configPosition, zoneId, typeId, accountName);
            }
            return returnValue;
        }

        // Update lại tag trong winservice 

        public static bool UpdateTagAutoByNewsId(long newsId, string linkTag, string tagItems)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateTagAutoByNewsId(newsId, linkTag, tagItems);
            }
            return returnValue;
        }


        // Update lại tag mới strong bảng Tag 
        public static bool UpdateTagAutoForNews(long newsId, string tagListId, ref string tagIdListUpdated)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateTagAutoForNews(newsId, tagListId, ref tagIdListUpdated);
            }
            return returnValue;
        }
        public static bool UpdateTagUrlForNews(long newsId, string tagLink, string tagItems)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateTagUrlForNews(newsId, tagLink, tagItems);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> GetNewsByIds(string listNewsId, int zoneId)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsByIds(listNewsId, zoneId);
            }
            return returnValue;
        }

        public static List<NewsForExportEntity> StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.StatisticsExportNewsData(zoneIds, sourceIds, orderBy, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsForExportEntity> StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.StatisticsExportHotNewsData(zoneIds, top, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.StatisticsExportNewsHasManyCommentsData(zoneIds, top, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsForExportEntity> GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            List<NewsForExportEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetListNewsByDistributionDate(zoneIds, fromDate, toDate);
            }
            return returnValue;
        }
        public static bool PublishNewsContentOnly(long newsId, string body)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.PublishNewsContentOnly(newsId, body);
            }
            return returnValue;
        }

        public static bool UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdatePhotoVideoCount(newsId, photoCount, videoCount);
            }
            return returnValue;
        }

        public static List<NewsForSuggestionEntity> GetNewsUseRollingNews(int rollingNewsId)
        {
            List<NewsForSuggestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsUseRollingNews(rollingNewsId);
            }
            return returnValue;
        }

        #region NewsStats By OriginalId

        public static int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetViewCountByOriginalId(zoneIds, fromDate, toDate, originalId);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByOriginalId(zoneIds, fromDate, toDate, originalId, pageIndex,
                    pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.CountNewsBySource(sourceId, fromDate, toDate);
            }
            return returnValue;
        }
        #endregion

        #region Update like count

        public static bool UpdateLikeCount(long newsId, int likeCount)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateLikeCount(newsId, likeCount);
            }
            return returnValue;
        }
        public static List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            List<NewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsByDateRange(startDate, endDate, status);
            }
            return returnValue;
        }

        #endregion

        public static NewsEntity GetNewsByTitle(string title)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsByTitle(title);
            }
            return returnValue;
        }

        public static bool UpdateNewsRelationJson(long id, string newsRelation)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.UpdateNewsRelationJson(id, newsRelation);
            }
            return returnValue;
        }

        public static List<NewsUnPublishEntity> SearchNewsUnPublish(string keyword, int ZoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsUnPublishEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsUnPublish(keyword, ZoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword)
        {
            List<NewsInListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.SearchNewsPublishedByKeyword(keyword);
            }
            return returnValue;
        }

        public static List<NewsAppBotEntity> GetListNewsNotIbtempByZoneId(int zoneId, int page, int num, ref int total)
        {
            List<NewsAppBotEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetListNewsNotIbtempByZoneId(zoneId, page, num, ref total);
            }
            return returnValue;
        }

        public static List<NewsAppBotEntity> GetListNewsNotIbBoundByZoneId(int zoneId, int page, int num, ref int total)
        {
            List<NewsAppBotEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetListNewsNotIbBoundByZoneId(zoneId, page, num, ref total);
            }
            return returnValue;
        }

        public static List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate)
        {
            List<NewsDashBoardUserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetDataDashBoardUser(username, fromDate, toDate);
            }
            return returnValue;
        }
        public static List<long> GetIds(string username, string zoneIds, int top)
        {
            List<long> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetIds(username, zoneIds, top);
            }
            return returnValue;
        }

        #region Get News log
        public static NewsEntity GetNewsInfoById(long id)
        {
            NewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.GetNewsInfoById(id);
            }
            return returnValue;
        }
        public static List<NewsFullLogEntity> GetNewsFullLog(string keyword)
        {
            List<NewsFullLogEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.News_GetFullLog(keyword);
            }
            return returnValue;
        }
        #endregion

        #region News Top Hot
        public static bool NewsHot_Insert(NewsHotEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_Insert(entity);
            }
            return returnValue;
        }
        public static bool NewsHot_Insert2(NewsHotEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_Insert2(entity);
            }
            return returnValue;
        }
        public static bool NewsHot_Delete(string NewsIds, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_Delete(NewsIds, type);
            }
            return returnValue;
        }
        public static bool NewsHot_UpdateType()
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_UpdateType();
            }
            return returnValue;
        }
        public static bool NewsHot_CheckExists()
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_CheckExists();
            }
            return returnValue;
        }
        public static bool NewsHot_Lock(int newsHotId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_Lock(newsHotId);
            }
            return returnValue;
        }
        public static bool NewsHot_Unlock(int newsHotId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_Unlock(newsHotId);
            }
            return returnValue;
        }
        public static bool NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_ChangeNewsInLockPosition(newsHotId, newsId);
            }
            return returnValue;
        }
        public static bool NewsHot_AddNewsInLockPosition(NewsPositionEntity addItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_AddNewsInLockPosition(addItem);
            }
            return returnValue;
        }
        public static bool NewsHot_DeleteNewsInLockPosition(NewsPositionEntity addItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_DeleteNewsInLockPosition(addItem);
            }
            return returnValue;
        }
        public static long NewsHot_UnpublishOrBomdNews(long newsId)
        {
            long returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_UnpublishOrBomdNews(newsId);
            }
            return returnValue;
        }
        public static List<NewsHotEntity> NewsHot_GetByType(int type)
        {
            List<NewsHotEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_GetByType(type);
            }
            return returnValue;
        }
        public static List<NewsHotEntity> NewsHot_GetAllForRedis()
        {
            List<NewsHotEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_GetAllForRedis();
            }
            return returnValue;
        }
        public static List<NewsHotCafeFEntity> NewsHot_GetAllForRedisByCafeF()
        {
            List<NewsHotCafeFEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.NewsHot_GetAllForRedisByCafeF();
            }
            return returnValue;
        }
        #endregion

        #region Seoer Update News
        public static bool Seoer_UpdateNews(SeoerNewsEntity entity, string TagIdList, string TagJSON)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Seoer_UpdateNews(entity, TagIdList, TagJSON);
            }
            return returnValue;
        }
        #endregion

        #region Expert Update News
        public static bool Expert_UpdateNews(ExpertNewsEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.Expert_UpdateNews(entity);
            }
            return returnValue;
        }
        #endregion

        public static bool RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMainDal.RecieveFeedbackNewsIntoCms(newsInfo, ref newsId);
            }
            return returnValue;
        }
    }
}
