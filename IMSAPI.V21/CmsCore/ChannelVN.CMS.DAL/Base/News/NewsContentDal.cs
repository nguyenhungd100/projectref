﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsContentDal
    {
        public static bool InsertNews(long newsId, string tags)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsContentMainDal.InsertNews(newsId, tags);
            }
            return returnValue;
        }
        public static NewsContentEntity GetByNewsId(long newsId)
        {
            NewsContentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsContentMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
