﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsEmbedBoxDal
    {
        public static List<NewsEmbedBoxListEntity> GetListNewsEmbedBox(int type, int status)
        {
            List<NewsEmbedBoxListEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsEmbedBoxMainDal.GetListNewsEmbedBox(type, status);
            }
            return returnValue;
        }

        public static bool Insert(NewsEmbebBoxEntity newsEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsEmbedBoxMainDal.Insert(newsEmbebBox);
            }
            return returnValue;
        }

        public static void Update(string listNewsId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsEmbedBoxMainDal.Update(listNewsId, type);
            }
        }
        public static void UpdateStatus(string listOfBoxNewsId, int type, int status)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsEmbedBoxMainDal.UpdateStatus(listOfBoxNewsId, type, status);
            }
        }
        public static void Delete(long newsId, int type)
        {
            using (var db = new CmsMainDb())
            {
                db.NewsEmbedBoxMainDal.Delete(newsId, type);
            }
        }
    }
}
