﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsChildDal
    {
        public static List<NewsChildEntity> GetByNewsId(long parentNewsId, bool isGetContent)
        {
            List<NewsChildEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.GetByNewsId(parentNewsId, isGetContent);
            }
            return returnValue;
        }

        public static NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order)
        {
            NewsChildEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.GetByNewsAndOrderId(parentNewsId, order);
            }
            return returnValue;
        }

        public static bool InsertNewsChild(NewsChildEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.InsertNewsChild(entity);
            }
            return returnValue;
        }

        public static bool UpdateNewsChild(NewsChildEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.UpdateNewsChild(entity);
            }
            return returnValue;
        }

        public static bool DeleteNewsChild(long parentNewsId, int order)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.DeleteNewsChild(parentNewsId, order);
            }
            return returnValue;
        }

        #region News Child Version
        public static bool InsertNewsChildVersion(NewsChildVersionEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.InsertNewsChildVersion(entity);
            }
            return returnValue;
        }

        public static bool UpdateNewsChildVersion(NewsChildVersionEntity entity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.UpdateNewsChildVersion(entity);
            }
            return returnValue;
        }

        public static List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false)
        {
            List<NewsChildVersionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.GetNewsChildVersionByNewsId(newsId, isGetContent);
            }
            return returnValue;
        }

        public static NewsChildVersionEntity GetNewsChildVersionByNewsAndOrderId(long newsVersionId, int order)
        {
            NewsChildVersionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsChildMainDal.GetNewsChildVersionByNewsAndOrderId(newsVersionId, order);
            }
            return returnValue;
        }

        #endregion

    }
}
