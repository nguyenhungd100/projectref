﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsAnalyticDal
    {
        public static List<NewsAnalyticEntity> GetLastUpdateViewCount(DateTime lastUpdateViewCount)
        {
            List<NewsAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticMainDal.GetLastUpdateViewCount(lastUpdateViewCount);
            }
            return returnValue;
        }
        public static List<NewsAnalyticEntity> GetTopLastestNews(int top)
        {
            List<NewsAnalyticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticMainDal.GetTopLastestNews(top);
            }
            return returnValue;
        }

        public static List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top)
        {
            List<NewsAnalyticV3Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticMainDal.GetTopHighestViewCountByZone(zone, hour, top);
            }
            return returnValue;
        }

        public static List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top)
        {
            List<NewsAnalyticV3Entity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsAnalyticMainDal.GetTopHighestCommentCountByZone(zone, hour, top);
            }
            return returnValue;
        }
    }
}
