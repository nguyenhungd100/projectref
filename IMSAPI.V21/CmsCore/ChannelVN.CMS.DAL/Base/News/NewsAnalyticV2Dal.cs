﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsAnalyticV2Dal
    {
        public static List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCountId)
        {
            List<NewsAnalyticV2Entity> returnValue;
            using (var db = new CmsAnalyticDb())
            {
                returnValue = db.NewsAnalyticV2MainDal.GetLastUpdateViewCount(lastUpdateViewCountId);
            }
            return returnValue;
        }
        public static List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord)
        {
            List<NewsAnalyticV2Entity> returnValue;
            using (var db = new CmsAnalyticDb())
            {
                returnValue = db.NewsAnalyticV2MainDal.GetAllViewFromDateToNow(fromDate, maxRecord);
            }
            return returnValue;
        }
        public static List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId)
        {
            List<NewsAnalyticV2Entity> returnValue;
            using (var db = new CmsAnalyticDb())
            {
                returnValue = db.NewsAnalyticV2MainDal.GetTopMostReadInHour(top, zoneId);
            }
            return returnValue;
        }
    }
}
