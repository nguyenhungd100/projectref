﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.News
{
    public class NewsCrawlerDal
    {
        // Lấy chi tiết một tin crawler
        public static NewsCrawlerEntity GetDetailById(int id)
        {
            NewsCrawlerEntity returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.GetDetailById(id);
            }
            return returnValue;
        }

        public static List<NewsCrawlerEntity> GetListNewsCrawler(int category, int siteId, int hot, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsCrawlerEntity> returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.GetListNewsCrawler(category, siteId, hot, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool CheckExist(string siteLink)
        {
            bool returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.CheckExist(siteLink);
            }
            return returnValue;
        }

        public static bool InsertSite(string siteLink, string siteName)
        {
            bool returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.InsertSite(siteLink, siteName);
            }
            return returnValue;
        }

        public static bool InvisibleSite(int id)
        {
            bool returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.InvisibleSite(id);
            }
            return returnValue;
        }

        public static bool UpdateIsMoveCms(int newsId)
        {
            bool returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.UpdateIsMoveCms(newsId);
            }
            return returnValue;
        }

        public static bool DeleteById(int newsId)
        {
            bool returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.DeleteById(newsId);
            }
            return returnValue;
        }

        public static List<CategoryCrawlerEntity> GetListCategoryCrawler()
        {
            List<CategoryCrawlerEntity> returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.GetListCategoryCrawler();
            }
            return returnValue;
        }

        public static List<SiteCrawlerEntity> GetListSiteCrawler()
        {
            List<SiteCrawlerEntity> returnValue;
            using (var db = new CmsCrawlerDb())
            {
                returnValue = db.NewsCrawlerMainDal.GetListSiteCrawler();
            }
            return returnValue;
        }
    }
}
