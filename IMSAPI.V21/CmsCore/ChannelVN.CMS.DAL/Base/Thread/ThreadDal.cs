﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Thread
{
    public class ThreadDal
    {
        #region Get
        public static ThreadEntity GetThreadByThreadId(long threadId)
        {
            ThreadEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.GetThreadByThreadId(threadId);
            }
            return returnValue;
        }
        public static List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            List<ThreadEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.GetThreadByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<ThreadEntity> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            List<ThreadEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<ThreadEntity> InitESAllThread(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<ThreadEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.InitESAllThread(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        public static List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {
            List<ThreadWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.SearchThreadForSuggestion(top, zoneId, keyword, orderBy);
            }
            return returnValue;
        }
        public static List<ThreadWithSimpleFieldEntity> GetRelationThread(long threadId)
        {
            List<ThreadWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.GetRelationThread(threadId);
            }
            return returnValue;
        }
        #endregion

        #region Update
        public static bool Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref int threadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.Insert(thread, zoneId, zoneIdList, relationThread, ref threadId);
            }
            return returnValue;
        }
        public static bool Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.Update(thread, zoneId, zoneIdList, relationThread);
            }
            return returnValue;
        }
        public static bool DeleteById(long threadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.DeleteById(threadId);
            }
            return returnValue;
        }
        public static bool UpdateThreadHot(long threadId, bool isHotThread)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.UpdateThreadHot(threadId, isHotThread);
            }
            return returnValue;
        }
        public static bool UpdateIsInvisibled(long threadId, bool invisibled)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadMainDal.UpdateIsInvisibled(threadId, invisibled);
            }
            return returnValue;
        }

        #endregion
    }
}
