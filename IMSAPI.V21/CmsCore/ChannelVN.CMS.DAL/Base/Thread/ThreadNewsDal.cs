﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Thread
{
    public class ThreadNewsDal
    {
        public static bool UpdateNews(long threadId, string deleteNewsId, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadNewsMainDal.UpdateNews(threadId, deleteNewsId, addNewsId);
            }
            return returnValue;
        }
        public static bool UpdateThreadNews(long newsId, string relatedThreads)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadNewsMainDal.UpdateThreadNews(newsId, relatedThreads);
            }
            return returnValue;
        }
        public static bool MultiUpdateThreadNews(string listThreadId, long newsId, int threadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadNewsMainDal.MultiUpdateThreadNews(listThreadId, newsId, threadId);
            }
            return returnValue;
        }
    }
}
