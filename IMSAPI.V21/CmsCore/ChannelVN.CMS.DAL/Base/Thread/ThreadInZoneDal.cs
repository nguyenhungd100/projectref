﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Thread
{
    public class ThreadInZoneDal
    {
        public static List<ThreadInZoneEntity> GetThreadInZoneByThreadId(long tagId)
        {
            List<ThreadInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ThreadInZoneMainDal.GetThreadInZoneByThreadId(tagId);
            }
            return returnValue;
        }
    }
}
