﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Zone
{
    public class ZoneDefaultTagDal
    {
        public static List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {
            List<ZoneDefaultTagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneDefaultTagMainDal.GetAllDefaultTagForZone();
            }
            return returnValue;
        }
    }
}
