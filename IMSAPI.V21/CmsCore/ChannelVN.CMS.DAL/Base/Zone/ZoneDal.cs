﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Zone
{
    public class ZoneDal
    {
        #region Gets

        public static ZoneEntity GetZoneById(int id)
        {
            ZoneEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneById(id);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneActiveByParentId(int parentId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneActiveByParentId(parentId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByParentId(int parentId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByParentId(parentId);
            }
            return returnValue;
        }
        public static List<ZoneWithSimpleFieldEntity> GetZoneActivedByParentId(int parentId)
        {
            List<ZoneWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneActivedByParentId(parentId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUserId(int userId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUserId(userId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUsername(string username)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUsername(username);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUserIdAndPermissionId(int userId, int permissionId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUserIdAndPermissionId(userId, permissionId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUsernameAndPermissionId(string username, int permissionId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUsernameAndPermissionId(username, permissionId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUsernameAndPermissionIds(string username, string permissionIds)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUsernameAndPermissionIds(username, permissionIds);
            }
            return returnValue;
        }
        public static List<ZoneWithSimpleFieldEntity> GetZoneActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            List<ZoneWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneActivedByUsernameAndPermissionIds(username, permissionIds);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByUserId(int userId, string keyword)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByUserId(userId, keyword);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetZoneByKeyword(keyword);
            }
            return returnValue;
        }
        public static ZoneEntity GetPrimaryZoneByNewsId(long newsId)
        {
            ZoneEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetPrimaryZoneByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<ZoneEntity> GetGroupNewsZoneByParentId(int id)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetGroupNewsZoneByParentId(id);
            }
            return returnValue;
        }
        public static int GetMaxIdZone()
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.GetMaxIdZone();
            }
            return returnValue;
        }

        #endregion

        #region Sets

        public static bool Insert(ZoneEntity zoneEntity)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue=db.ZoneMainDal.Insert(zoneEntity);
            }
            return returnValue;
        }

        public static bool InsertInitDB(ZoneEntity zoneEntity)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.InsertInitDB(zoneEntity);
            }
            return returnValue;
        }

        public static bool Update(ZoneEntity zoneEntity)
        {
            bool returnValue=false;
            using (var db = new CmsMainDb())
            {
                returnValue=db.ZoneMainDal.Update(zoneEntity);
            }
            return returnValue;
        }

        public static void MoveUp(int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                db.ZoneMainDal.MoveUp(zoneId);
            }
        }

        public static void MoveDown(int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                db.ZoneMainDal.MoveDown(zoneId);
            }
        }

        public static bool ToggleZoneInvisibled(int zoneId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.ToggleZoneInvisibled(zoneId);
            }
            return returnValue;
        }

        public static bool ToggleZoneStatus(int zoneId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.ToggleZoneStatus(zoneId);
            }
            return returnValue;
        }

        public static bool ToggleZoneAllowComment(int zoneId)
        {
            bool returnValue = false;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZoneMainDal.ToggleZoneAllowComment(zoneId);
            }
            return returnValue;
        }

        #endregion
    }
}
