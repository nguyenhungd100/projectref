﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.FileUpload
{
    public class FileUploadDal
    {
        #region Update

        public static bool InsertFileUpload(FileUploadEntity fileUpload, ref int newFileUploadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.InsertFileUpload(fileUpload, ref newFileUploadId);
            }
            return returnValue;
        }
        public static bool UpdateFileUpload(FileUploadEntity fileUpload)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.UpdateFileUpload(fileUpload);
            }
            return returnValue;
        }
        public static bool DeleteFileUpload(int fileUploadId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.DeleteFileUpload(fileUploadId);
            }
            return returnValue;
        }

        #endregion

        #region Get

        public static FileUploadEntity GetFileUploadById(int fileUploadId)
        {
            FileUploadEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.GetFileUploadById(fileUploadId);
            }
            return returnValue;
        }
        public static List<FileUploadEntity> SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<FileUploadEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.SearchFileUpload(keyword, zoneId, ext, uploadedBy, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<FileUploadExtEntity> GetAllExt()
        {
            List<FileUploadExtEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.FileUploadMainDal.GetAllExt();
            }
            return returnValue;
        }

        #endregion
    }
}
