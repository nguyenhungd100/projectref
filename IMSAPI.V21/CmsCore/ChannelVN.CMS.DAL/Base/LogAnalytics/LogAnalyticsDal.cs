﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.LogAnalytics;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.LogAnalytics
{
    public class LogAnalyticsDal
    {
        public static bool Update(LogAnalyticsEntity log)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.LogAnalyticsMainDal.Update(log);
            }
            return returnValue;
        }

    }
}
