﻿using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.NewsWarning
{
    public class ContentWarningDal
    {
        public static bool ContentWarningLog_Insert(long newsId, string Account)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.ContentWarningLog_Insert(newsId, Account);
            }
            return returnValue;
        }
        public static bool Update(ContentWarningEntity contentWarning)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.Update(contentWarning);
            }
            return returnValue;
        }

        public static List<ContentWarningEntity> GetListByLastModifiedDate(DateTime fromDate, DateTime toDate)
        {
            List<ContentWarningEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.GetListByLastModifiedDate(fromDate, toDate);
            }
            return returnValue;
        }

        public static ContentWarningEntity GetByNewsId(long newsId)
        {
            ContentWarningEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.GetByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<ContentWarningEntity> GetByListNewsId(string listNewsId)
        {
            List<ContentWarningEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.GetByListNewsId(listNewsId);
            }
            return returnValue;
        }
        public static List<ContentWarningLogEntity> ContentWarningLog_GetByListNewsId(string listNewsId)
        {
            List<ContentWarningLogEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.ContentWarningLog_GetByListNewsId(listNewsId);
            }
            return returnValue;
        }
        public static List<ContentWarningLogEntity> ContentWarningLog_GetByNewsId(long NewsId)
        {
            List<ContentWarningLogEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ContentWarningMainDal.ContentWarningLog_GetByNewsId(NewsId);
            }
            return returnValue;
        }
    }
}
