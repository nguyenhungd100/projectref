﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx;

namespace ChannelVN.CMS.DAL.Base.NewsRoyaltiesEx
{
    public class NewsRoyaltiesExDal
    {        
        public static List<NewsRoyaltiesExEntity> SearchNewsRoyalty(string accountName, string zoneIds, string catIds, DateTime fromDate, DateTime toDate, int pageIndex, int pagaSize, ref int totalRows)
        {
            List<NewsRoyaltiesExEntity> returnValue;
            using (var db = new NewsRoyaltiesExDb())
            {
                returnValue = db.NewsRoyaltiesExMainDal.SearchNewsRoyalty(accountName, zoneIds, catIds, fromDate, toDate, pageIndex, pagaSize, ref totalRows);
            }
            return returnValue;
        }

        public static List<NewsCategoryEntity> GetListCateRoyalty()
        {
            List<NewsCategoryEntity> returnValue;
            using (var db = new NewsRoyaltiesExDb())
            {
                returnValue = db.NewsRoyaltiesExMainDal.GetListCateRoyalty();
            }
            return returnValue;
        }
    }
}
