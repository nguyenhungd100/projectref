﻿using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.MediaAlbum
{
    public class MediaAlbumDal
    {
        public static List<MediaAlbumEntity> Search(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<MediaAlbumEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MediaAlbumMainDal.Search(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
        public static MediaAlbumEntity GetById(long MediaAlbumId)
        {
            MediaAlbumEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MediaAlbumMainDal.GetById(MediaAlbumId);

            }
            return returnValue;
        }

        public static bool Insert(MediaAlbumEntity MediaAlbum, ref int newMediaAlbumId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MediaAlbumMainDal.Insert(MediaAlbum, ref newMediaAlbumId);

            }
            return returnValue;
        }
        public static bool Update(MediaAlbumEntity MediaAlbum)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MediaAlbumMainDal.Update(MediaAlbum);

            }
            return returnValue;
        }
        public static bool Delete(int MediaAlbumId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.MediaAlbumMainDal.Delete(MediaAlbumId);

            }
            return returnValue;

        }
    }
}
