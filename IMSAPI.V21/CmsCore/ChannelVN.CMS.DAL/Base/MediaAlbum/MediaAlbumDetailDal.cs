﻿using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.MediaAlbum
{
    public class MediaAlbumDetailDal
    {
        #region Update

        public static bool InsertMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumDetail, string tagIdList, ref int newPhotoId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.InsertMediaAlbumDetail(MediaAlbumDetail, tagIdList, ref newPhotoId);
            }
            return retVal;
        }
        public static bool UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumDetail, string tagIdList)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.UpdateMediaAlbumDetail(MediaAlbumDetail, tagIdList);
            }
            return retVal;
        }
        public static bool UpdateMediaAlbumDetailStatus(int id, EnumMediaAlbumDetailStatus status)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.UpdateMediaAlbumDetailStatus(id, status);
            }
            return retVal;
        }
        public static bool UpdateMediaAlbumDetailViewCount(int id, int viewCount)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.UpdateMediaAlbumDetailViewCount(id, viewCount);
            }
            return retVal;
        }
        public static bool DeleteMediaAlbumDetail(long MediaAlbumDetailId)
        {
            bool retVal = false;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.DeleteMediaAlbumDetail(MediaAlbumDetailId);
            }
            return retVal;
        }

        #endregion

        #region Get

        public static MediaAlbumDetailEntity GetMediaAlbumDetailById(int MediaAlbumDetailId)
        {
            MediaAlbumDetailEntity retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.GetMediaAlbumDetailById(MediaAlbumDetailId);
            }
            return retVal;
        }
        public static List<MediaAlbumDetailEntity> GetMediaAlbumDetailByAlbumId(int MediaAlbumId)
        {
            List<MediaAlbumDetailEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.GetMediaAlbumDetailByAlbumId(MediaAlbumId);
            }
            return retVal;
        }
        public static List<MediaAlbumDetailEntity> SearchMediaAlbumDetail(string keyword, int zoneId, EnumMediaAlbumDetailMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumMediaAlbumDetailStatus status, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            List<MediaAlbumDetailEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.SearchMediaAlbumDetail(keyword, zoneId, mediaType,
                                                        fromDate, toDate, createdBy, author,
                                                        status, isHot, pageIndex, pageSize, ref totalRow);
            }
            return retVal;
        }
        public static List<MediaAlbumDetailCountEntity> CountMediaAlbumDetail(string username, int zoneId)
        {
            List<MediaAlbumDetailCountEntity> retVal;
            using (var db = new CmsMainDb())
            {
                retVal = db.MediaAlbumDetailMainDal.CountMediaAlbumDetail(username, zoneId);
            }
            return retVal;
        }

        #endregion
    }
}
