﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.RollingNews
{
    public class RollingNewsEventDal
    {
        public static List<RollingNewsEventEntity> GetByRollingNewsId(int rollingNewsId, int eventType, int status)
        {
            List<RollingNewsEventEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.GetByRollingNewsId(rollingNewsId, eventType, status);

            }
            return returnValue;
        }
        public static RollingNewsEventEntity GetById(int rollingNewsEventId)
        {
            RollingNewsEventEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.GetById(rollingNewsEventId);

            }
            return returnValue;
        }
        public static bool Insert(RollingNewsEventEntity rollingNewsEvent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.Insert(rollingNewsEvent);

            }
            return returnValue;
        }
        public static bool InsertV2(RollingNewsEventEntity rollingNewsEvent, ref int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.InsertV2(rollingNewsEvent, ref Id);

            }
            return returnValue;
        }
        public static bool Update(RollingNewsEventEntity rollingNewsEvent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.Update(rollingNewsEvent);

            }
            return returnValue;
        }
        public static bool DeleteByRollingNewsId(int rollingNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.DeleteByRollingNewsId(rollingNewsId);

            }
            return returnValue;
        }
        public static bool DeleteByIds(int rollingNewsId, string Ids)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.DeleteByIds(rollingNewsId, Ids);

            }
            return returnValue;
        }
        public static bool Delete(int rollingNewsEventId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.Delete(rollingNewsEventId);

            }
            return returnValue;
        }
        public static bool Publish(int rollingNewsEventId, string publishedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.Publish(rollingNewsEventId, publishedBy);

            }
            return returnValue;
        }
        public static bool Unpublish(int rollingNewsEventId, string unpublishedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.Unpublish(rollingNewsEventId, unpublishedBy);

            }
            return returnValue;
        }
        public static bool UpdateIsFocus(int rollingNewsEventId, bool isFocus, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsEventMainDal.UpdateIsFocus(rollingNewsEventId, isFocus, lastModifiedBy);

            }
            return returnValue;
        }
    }
}
