﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.RollingNews
{
    public class RollingNewsAuthorDal
    {
        public static List<RollingNewsAuthorEntity> GetByRollingNewsId(int rollingNewsId)
        {
            List<RollingNewsAuthorEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsAuthorMainDal.GetByRollingNewsId(rollingNewsId);

            }
            return returnValue;
        }
        public static RollingNewsAuthorEntity GetById(int rollingNewsAuthorId)
        {
            RollingNewsAuthorEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsAuthorMainDal.GetById(rollingNewsAuthorId);

            }
            return returnValue;
        }
        public static bool Insert(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsAuthorMainDal.Insert(rollingNewsAuthor);

            }
            return returnValue;
        }
        public static bool Update(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsAuthorMainDal.Update(rollingNewsAuthor);

            }
            return returnValue;
        }
        public static bool Delete(int rollingNewsAuthorId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsAuthorMainDal.Delete(rollingNewsAuthorId);

            }
            return returnValue;
        }
    }
}
