﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.RollingNews
{
    public class RollingNewsDal
    {
        public static List<RollingNewsEntity> Search(string keyword, int status, int pageIndex, int pageSize,
                                                     ref int totalRow)
        {
            List<RollingNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.Search(keyword, status, pageIndex, pageSize,
                                                 ref totalRow);

            }
            return returnValue;
        }
        public static List<RollingNewsForSuggestionEntity> SearchForSuggestion(string keyword, int status, int top)
        {
            List<RollingNewsForSuggestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.SearchForSuggestion(keyword, status, top);

            }
            return returnValue;
        }
        public static RollingNewsEntity GetById(int rollingNewsId)
        {
            RollingNewsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.GetById(rollingNewsId);

            }
            return returnValue;
        }
        public static bool Insert(RollingNewsEntity rollingNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.Insert(rollingNews);

            }
            return returnValue;
        }
        public static bool InsertV2(RollingNewsEntity rollingNews, ref int Id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.InsertV2(rollingNews, ref Id);

            }
            return returnValue;
        }
        public static bool Update(RollingNewsEntity rollingNews)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.Update(rollingNews);

            }
            return returnValue;
        }
        public static bool Delete(int rollingNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.Delete(rollingNewsId);

            }
            return returnValue;
        }
        public static bool ChangeStatus(int rollingNewsId, int status, string lastModifiedBy)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.RollingNewsMainDal.ChangeStatus(rollingNewsId, status, lastModifiedBy);

            }
            return returnValue;
        }
    }
}
