﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsCacheMonitor
{
    public class NewsCachedMonitorDal
    {
        public static SqlDataReader GetChangedRecordForNews()
        {
            SqlDataReader returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCachedMonitorMainDal.GetChangedRecordForNews();
            }
            return returnValue;
        }
        public static SqlDataReader GetChangedRecordForTag()
        {
            SqlDataReader returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCachedMonitorMainDal.GetChangedRecordForTag();
            }
            return returnValue;
        }

        public static bool UpdateProcessItemStateForNews(string listOfProcessIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCachedMonitorMainDal.UpdateProcessItemStateForNews(listOfProcessIds);
            }
            return returnValue;
        }
        public static bool UpdateProcessItemStateForTag(string listOfProcessIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsCachedMonitorMainDal.UpdateProcessItemStateForTag(listOfProcessIds);
            }
            return returnValue;
        }
    }
}
