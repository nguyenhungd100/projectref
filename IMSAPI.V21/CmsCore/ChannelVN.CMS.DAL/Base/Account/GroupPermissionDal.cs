﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class GroupPermissionDal
    {
        public static List<GroupPermissionEntity> GetAllGroupPermission()
        {
            List<GroupPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GroupPermissionMainDal.GetAllGroupPermission();
            }
            return returnValue;
        }
        public static bool CheckUserInGroupPermission(int userId, int groupId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GroupPermissionMainDal.CheckUserInGroupPermission(userId, groupId);
            }
            return returnValue;
        }
    }
}
