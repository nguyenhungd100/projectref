﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class UserSmsDal
    {
        #region Sets

        public static bool AddnewSmsCode(UserSmsEntity userSms)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.AddnewSmsCode(userSms);
            }
            return returnValue;
        }

        public static bool Insert(int userId, string smsCode, DateTime expiredDate)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.Insert(userId, smsCode, expiredDate);
            }
            return returnValue;
        }

        public static bool RemoveInUsing(int userId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.RemoveInUsing(userId);
            }
            return returnValue;
        }

        #endregion

        #region Gets

        public static UserSmsEntity GetUserSmsByUserId(int userId)
        {
            UserSmsEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.GetUserSmsByUserId(userId);
            }
            return returnValue;
        }

        public static int CheckValidateCode(string userName, string smsCode)
        {
            int returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.CheckValidateCode(userName, smsCode);
            }
            return returnValue;
        }

        public static bool GetSmsCode(string userName, ref string smsCode, ref string phoneNumber)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserSmsMainDal.GetSmsCode(userName, ref smsCode, ref phoneNumber);
            }
            return returnValue;
        }

        #endregion

    }
}
