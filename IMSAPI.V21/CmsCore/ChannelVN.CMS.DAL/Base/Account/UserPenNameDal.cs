﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class UserPenNameDal
    {
        public static bool EditPenName(UserPenNameEntity penName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPenNameMainDal.EditPenName(penName);
            }
            return returnValue;
        }
        public static bool DeletePenNameById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPenNameMainDal.DeletePenNameById(id);
            }
            return returnValue;
        }
        public static List<UserPenNameEntity> SearchPenName(string email, string username, int pageIndex, int pageSize, ref int totalRow)
        {
            List<UserPenNameEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPenNameMainDal.SearchPenName(email, username, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static UserPenNameEntity GetPenNameByVietId(long vietId)
        {
            UserPenNameEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPenNameMainDal.GetPenNameByVietId(vietId);
            }
            return returnValue;
        }
    }
}
