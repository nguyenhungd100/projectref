﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class PermissionDal
    {
        public static List<PermissionEntity> GetListPermissionByGroupId(int groupPermissionId)
        {
            List<PermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionMainDal.GetListPermissionByGroupId(groupPermissionId);
            }
            return returnValue;
        }
        public static List<PermissionEntity> GetListPermissionByTemplateId(int permissionTemplateId)
        {
            List<PermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionMainDal.GetListPermissionByTemplateId(permissionTemplateId);
            }
            return returnValue;
        }

        public static List<PermissionEntity> GetListPermissionByUsername(string username, string filterInListPermissionId)
        {
            List<PermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionMainDal.GetListPermissionByUsername(username, filterInListPermissionId);
            }
            return returnValue;
        }
        public static PermissionEntity GetPermissionById(int id)
        {
            PermissionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionMainDal.GetPermissionById(id);
            }
            return returnValue;
        }
    }
}
