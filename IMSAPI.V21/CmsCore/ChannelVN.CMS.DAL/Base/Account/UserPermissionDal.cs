﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class UserPermissionDal
    {
        public static bool UpdateUserPermission(UserPermissionEntity userPermission)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.UpdateUserPermission(userPermission);
            }
            return returnValue;
        }
        public static bool UpdateAllUserPermission(int userId, List<UserPermissionEntity> userPermission)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.UpdateAllUserPermission(userId, userPermission);
            }
            return returnValue;
        }
        public static bool UpdateAllUserPermission2(int userId, List<UserPermissionEntity> userPermission)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.UpdateAllUserPermission2(userId, userPermission);
            }
            return returnValue;
        }
        public static bool UpdateAllUserPermissionDelete(int userId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.UpdateAllUserPermissionDelete(userId);
            }
            return returnValue;
        }
        public static bool RemoveUserPermission(UserPermissionEntity userPermission)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.RemoveUserPermission(userPermission);
            }
            return returnValue;
        }
        public static bool RemoveAllUserPermission(int userId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.RemoveAllUserPermission(userId);
            }
            return returnValue;
        }
        public static bool CheckUserPermission(UserPermissionEntity userPermission)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.CheckUserPermission(userPermission);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUserId(int userId, bool isGetChildZone)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUserId(userId, isGetChildZone);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUserName(string username)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUserName(username);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUserIdAndZoneId(int userId, int zoneId)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUserIdAndZoneId(userId, zoneId);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUsernameAndZoneId(string username, int zoneId)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUsernameAndZoneId(username, zoneId);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUserIdAndPermissionId(int userId, int permissionId)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUserIdAndPermissionId(userId, permissionId);
            }
            return returnValue;
        }
        public static List<UserPermissionEntity> GetListUserPermissionByUsernameAndPermissionId(string username, int permissionId)
        {
            List<UserPermissionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserPermissionMainDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);
            }
            return returnValue;
        }
    }
}
