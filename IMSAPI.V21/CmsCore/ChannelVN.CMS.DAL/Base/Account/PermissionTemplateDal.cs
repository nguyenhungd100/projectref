﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class PermissionTemplateDal
    {
        public static List<PermissionTemplateEntity> GetAllPermissionTemplate()
        {
            List<PermissionTemplateEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.GetAllPermissionTemplate();
            }
            return returnValue;
        }
        public static bool UpdatePermissionTemplate(PermissionTemplateEntity pm)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.UpdatePermissionTemplate(pm);
            }
            return returnValue;
        }
        public static bool DeletePermissionTemplate(int templateId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.DeletePermissionTemplate(templateId);
            }
            return returnValue;
        }

        public static bool PermissionTemplateDetailUpdate(int templateId, int permissionId, string zoneIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.PermissionTemplateDetailUpdate(templateId, permissionId, zoneIds);
            }
            return returnValue;
        }
        public static bool DeleteAllPermissionTemplateDetail(int templateId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.RemoveAllPermissionTemplateDetail(templateId);
            }
            return returnValue;
        }
        public static bool DeletePermissionTemplateDetail(PermissionTemplateDetailEntity pt)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.RemovePermissionTemplate_Detail(pt);
            }
            return returnValue;
        }
        public static PermissionTemplateEntity GetPermissionTemplateById(int templateId)
        {
            PermissionTemplateEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.GetPermissionTemplateById(templateId);
            }
            return returnValue;
        }
        public static bool PermissionTemplateUpdate(int templateId, string templateName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.PermissionTemplateUpdate(templateId, templateName);
            }
            return returnValue;
        }
        public static bool PermissionTemplateInsert( string templateName, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.PermissionTemplateInsert(templateName, ref id);
            }
            return returnValue;
        }
        public static List<PermissionTemplateDetailEntity> GetListPermissionTemplateDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            List<PermissionTemplateDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.GetListPermissionTemplateDetailByTemplateId(templateId, isGetChildZone);
            }
            return returnValue;
        }
        public static List<PermissionTemplateDetailEntity> GetPermisstionTemplateDetailById(int templateId)
        {
            List<PermissionTemplateDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.GetPermisstionTemplateDetailById(templateId);
            }
            return returnValue;
        }
        public static bool PermissionTemplateDetailUpdate(PermissionTemplateDetailEntity permissionTemplateDetail)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PermissionTemplateMainDal.PermissionTemplateDetailUpdate(permissionTemplateDetail);
            }
            return returnValue;
        }
    }
}
