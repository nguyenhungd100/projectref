﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.DAL.Base.Account
{
    public class UserDal
    {
        public static bool AddnewUser(UserEntity user, ref int newUserId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.AddnewUser(user, ref newUserId);
            }
            return returnValue;
        }

        public static bool UpdateUserById(UserEntity user)
        {            
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateUserById(user);
            }
            return returnValue;            
        }

        public static bool DeleteUserById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.DeleteUserById(id);
            }
            return returnValue;
        }



        public static bool UpdateUserAvatar(string accountName, string avatar)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateUserAvatar(accountName, avatar);
            }
            return returnValue;
        }

        public static bool UpdateTelegramId(string accountName, long telegramId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateTelegramId(accountName, telegramId);
            }
            return returnValue;
        }

        public static bool UpdateUserStatusByById(int id, int status)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateUserStatusByById(id, status);
            }
            return returnValue;
        }
        public static bool UpdateUserPasswordByById(int id, string password)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateUserPasswordByById(id, password);
            }
            return returnValue;
        }

        public static bool UpdateUserOTPByById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateUserOTPByById(id);
            }
            return returnValue;
        }


        public static UserEntity GetUserById(int id)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserById(id);
            }
            return returnValue;
        }
        public static UserEntity GetUserByUsername(string username)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserByUsername(username);
            }
            return returnValue;
        }
        public static UserEntity GetUserByEmail(string email)
        {
            UserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserByEmail(email);
            }
            return returnValue;
        }

        public static List<UserEntity> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.SearchUser(keyword, status, sortOrder, pageIndex, pageSize,
                    ref totalRow);
            }
            return returnValue;
        }
        public static List<UserEntity> InitESAllUser()
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.InitESAllUser();
            }
            return returnValue;
        }
        public static List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserByPermissionIdAndZoneList(permissionId, zoneIds);
            }
            return returnValue;
        }
        public static List<UserEntity> GetUserByPermissionListAndZoneList(string permissionIds, string zoneIds)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserByPermissionListAndZoneList(permissionIds, zoneIds);
            }
            return returnValue;
        }

        public static List<UserEntity> GetUserFullPermission()
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserFullPermission();
            }
            return returnValue;
        }

        public static List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(int permissionId, int zoneId)
        {
            List<UserStandardEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetNormalUserByPermissionIdAndZoneId(permissionId, zoneId);
            }
            return returnValue;
        }

        #region lấy danh sách user theo permissionId và ZoneId
        public static List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            List<UserEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetUserWithFullPermissionAndZoneId(permissionId, newsId, userName);
            }
            return returnValue;
        }
        #endregion

        public static string GetOtpSecretKeyByUsername(string username)
        {
            string returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.GetOtpSecretKeyByUsername(username);
            }
            return returnValue;
        }

        public static List<UserSearchAutocompleteEntity> UserSearchAutocomplete(int ZoneId, string Keyword, bool isShowTKTS)
        {
            List<UserSearchAutocompleteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UserSearchAutocomplete(ZoneId, Keyword, isShowTKTS);
            }
            return returnValue;
        }
        public static bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            }
            return returnValue;
        }
        public static bool UserPermission_Insert(int userId, int tempId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.UserMainDal.UserPermission_Insert(userId, tempId);
            }
            return returnValue;
        }
    }
}
