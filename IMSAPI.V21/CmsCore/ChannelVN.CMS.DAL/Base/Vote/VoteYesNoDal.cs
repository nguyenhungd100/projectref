﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Vote
{
    public class VoteYesNoDal
    {

        public static List<VoteYesNoEntity> Search(int voteYesNoGroupId, string keyword, int isFocus, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VoteYesNoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.Search(voteYesNoGroupId, keyword, isFocus, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static VoteYesNoEntity GetById(int voteYesNoId)
        {
            VoteYesNoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.GetById(voteYesNoId);
            }
            return returnValue;
        }

        public static bool UpdatePriority(string sortedListIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.UpdatePriority(sortedListIds);
            }
            return returnValue;
        }
        public static bool Insert(VoteYesNoEntity voteYesNo, ref int NewsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.Insert(voteYesNo, ref NewsId);
            }
            return returnValue;
        }
        public static bool Update(VoteYesNoEntity voteYesNo)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.Update(voteYesNo);
            }
            return returnValue;
        }
        public static bool Delete(int voteYesNoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoMainDal.Delete(voteYesNoId);
            }
            return returnValue;
        }
    }
}
