﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Vote
{
   public  class VoteInZoneDal
    {
       public static List<VoteInZoneEntity> GetVoteInZoneByVoteId(long voteId)
       {
            List<VoteInZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteInZoneMainDal.GetVoteInZoneByVoteId(voteId);
            }
            return returnValue;
        }
    }
}
