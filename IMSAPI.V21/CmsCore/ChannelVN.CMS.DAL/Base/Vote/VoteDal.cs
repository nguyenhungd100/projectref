﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.DAL.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Vote
{
    public class VoteDal
    {
        #region Gets
        public static VoteEntity GetById(int id)
        {
            VoteEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<VoteEntity> GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            List<VoteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
            }
            return returnValue;
        }
        public static List<VoteEntity> InitESAllVote(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<VoteEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.InitESAllVote(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        public static List<VoteAnswersEntity> GetListAnswersByVoteIdExt(int voteId)
        {
            List<VoteAnswersEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetListAnswersByVoteId(voteId);
            }
            return returnValue;
        }
        public static List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            List<VoteAnswersEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetListAnswersByVoteId(voteId);
            }
            return returnValue;
        }
        public static VoteAnswersEntity GetAnswersDetail(int id)
        {
            VoteAnswersEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetAnswersDetail(id);
            }
            return returnValue;
        }
        public static VoteEntity GetByNewsId(long id)
        {
            VoteEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.GetByNewsId(id);
            }
            return returnValue;
        }
        #endregion

        #region Sets

        public static bool Insert(VoteEntity vote, int zoneId, string zoneIdList, ref int voteId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.Insert(vote, zoneId, zoneIdList, ref voteId);
            }
            return returnValue;
        }

        public static bool Update(VoteEntity vote, int zoneId, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.Update(vote, zoneId, zoneIdList);
            }
            return returnValue;
        }

        public static bool InsertAnswers(VoteAnswersEntity voteAnswers, ref int voteAnswersId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.InsertAnswers(voteAnswers, ref voteAnswersId);
            }
            return returnValue;
        }

        public static bool UpdateAnswers(VoteAnswersEntity voteAnswers)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.UpdateAnswers(voteAnswers);
            }
            return returnValue;
        }

        public static bool UpdatePriorityVoteAnswers(string listVoteAnswersId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.UpdatePriorityVoteAnswers(listVoteAnswersId);
            }
            return returnValue;
        }


        public static bool DeleteAnswers(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.DeleteAnswers(id);
            }
            return returnValue;
        }

        public static bool Delete(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.Delete(id);
            }
            return returnValue;
        }

        public static bool InsertVoteInNews(int voteId, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteMainDal.InsertVoteInNews(voteId, newsId);
            }
            return returnValue;
        }

        #endregion
    }
}
