﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Vote
{
    public class VoteYesNoGroupDal
    {
        public static List<VoteYesNoGroupEntity> GetAll()
        {
            List<VoteYesNoGroupEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.GetAll();
            }
            return returnValue;
        }
        public static VoteYesNoGroupEntity GetById(int voteYesNoGroupId)
        {
            VoteYesNoGroupEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.GetById(voteYesNoGroupId);
            }
            return returnValue;
        }

        public static bool UpdatePriority(string sortedListIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.UpdatePriority(sortedListIds);
            }
            return returnValue;
        }
        public static bool Insert(VoteYesNoGroupEntity voteYesNoGroup, ref int voteYesNoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.Insert(voteYesNoGroup, ref voteYesNoId);
            }
            return returnValue;
        }
        public static bool Update(VoteYesNoGroupEntity voteYesNoGroup)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.Update(voteYesNoGroup);
            }
            return returnValue;
        }
        public static bool Delete(int voteYesNoGroupId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.VoteYesNoGroupMainDal.Delete(voteYesNoGroupId);
            }
            return returnValue;
        }
    }
}
