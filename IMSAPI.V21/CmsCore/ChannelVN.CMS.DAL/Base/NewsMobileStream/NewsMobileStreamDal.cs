﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsMobileStream
{

    public class NewsMobileStreamDal
    {
        /// <summary>
        /// CREATE: TƯỚNG CƯƠNG
        /// LastModifiedDate: 2015-10-28
        /// </summary>
        /// <param name="zoneId"></param>
        /// <returns>GET LIST NEWS MOBILE STREAM</returns>
        public static List<NewsMobileStreamEntity> GetList(int zoneId)
        {
            List<NewsMobileStreamEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMobileStreamMainDal.GetList(zoneId);
            }
            return returnValue;
        }

        /// <summary>
        /// CREATE: TƯỚNG CƯƠNG
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="newsId"></param>
        /// <param name="status"></param>
        /// <param name="isHighlight"></param>
        /// <returns>UPDATE NEWS MOBILE STREAM</returns>
        public static bool SaveNewsMobileStream(int zoneId, long newsId, int status, bool isHighlight)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMobileStreamMainDal.SaveNewsMobileStream(zoneId, newsId, status, isHighlight);
            }
            return returnValue;
        }

        public static bool NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine, int newsPositionTypeIdOnMobileFocus)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsMobileStreamMainDal.NewsMobileStream_RemoveTopLessView(topRemove, numberOfNewsOnTimeLine, newsPositionTypeIdOnMobileFocus);
            }
            return returnValue;
        }
    }
}
