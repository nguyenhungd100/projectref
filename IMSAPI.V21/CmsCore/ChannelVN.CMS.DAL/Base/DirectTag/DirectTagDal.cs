﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.DAL.Base.DirectTag
{
    public class DirectTagDal
    {
        public static bool InsertDirectTag(ref int tagId, string tagName, string tagLink, string quoteFormat, int tagType)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.InsertDirectTag(ref tagId, tagName, tagLink, quoteFormat, tagType);
            }
            return returnValue;
        }

        public static bool UpdateDirectTag(int tagId, string tagName, string tagLink, string quoteFormat, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.UpdateDirectTag(tagId, tagName, tagLink, quoteFormat, type);
            }
            return returnValue;
        }

        public static bool DeleteDirectTag(string tagIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.DeleteDirectTag(tagIds);
            }
            return returnValue;
        }

        public static bool InsertDirectTagNewsMulti(string tagIds, string newsIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.InsertDirectTagNewsMulti(tagIds, newsIds);
            }
            return returnValue;
        }

        public static bool InsertDirectTagNewsMultiByType(string tagIds, string newsIds, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.InsertDirectTagNewsMultiByType(tagIds, newsIds, type);
            }
            return returnValue;
        }

        public static bool DeleteDirectTagNewsFromNews(int tagId, Int64 newsId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.DeleteDirectTagNewsFromNews(tagId, newsId, type);
            }
            return returnValue;
        }

        public static List<DirectTagEntity> SelectAll(int pageIndex, int pageSize, ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.DirectTagMainDal.SelectAll(pageIndex, pageSize, ref totalRow);
            }
        }

        public static List<DirectTagEntity> SelectAllByType(int pageIndex, int pageSize, int type, ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.DirectTagMainDal.SelectAllByType(pageIndex, pageSize, type, ref totalRow);
            }
        }

        public static List<DirectTagEntity> SearchDirectTag(string keyword, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            using (var db = new CmsMainDb())
            {
                return db.DirectTagMainDal.SearchDirectTag(keyword, type, pageIndex, pageSize, ref totalRow);
            }
        }

        public static DirectTagEntity IsTagAvailable(string tagName)
        {
            using (var db = new CmsMainDb())
            {
                return db.DirectTagMainDal.IsTagAvailable(tagName);
            }
        }

        public static List<DirectTagEntity> SelectDirectTagByNewsId(Int64 newsId)
        {
            using (var db = new CmsMainDb())
            {
                return db.DirectTagMainDal.SelectDirectTagByNewsId(newsId);
            }
        }

        public static List<NewsPublishForSearchEntity> SearchNewsPublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForSearchEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.SearchNewsPublish(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsPublishForSearchEntity> SearchNewsPublish_DirectTag_LessThan_3Tag(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsPublishForSearchEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.DirectTagMainDal.SearchNewsPublish_DirectTag_LessThan_3Tag(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
    }
}
