﻿using ChannelVN.CMS.Entity.Base.EndUser;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.EndUser
{
    public class EndUserDal
    {
        public static bool Insert(EndUserEntity channelEntity, ref int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.Insert(channelEntity, ref videoId);
            }
            return returnValue;
        }

        public static bool Update(EndUserEntity channelEntity)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.Update(channelEntity);
            }
            return returnValue;
        }

        public static EndUserEntity GetById(int id)
        {
            EndUserEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.GetById(id);
            }
            return returnValue;
        }

        #region follow
        public static bool UserFollowPlayList(PlayListFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserFollowPlayList(userFollow);
            }
            return returnValue;
        }
        public static bool UserFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserFollowZoneVideo(userFollow);
            }
            return returnValue;
        }
        public static bool UserFollowChannel(VideoChannelFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserFollowChannel(userFollow);
            }
            return returnValue;
        }
        public static bool UserFollowLabel(VideoLabelFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserFollowLabel(userFollow);
            }
            return returnValue;
        }
        public static bool UserFollowPublisher(PublisherFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserFollowPublisher(userFollow);
            }
            return returnValue;
        }
        #endregion

        #region unfollow
        public static bool UserUnFollowPlayList(PlayListFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserUnFollowPlayList(userFollow);
            }
            return returnValue;
        }
        public static bool UserUnFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserUnFollowZoneVideo(userFollow);
            }
            return returnValue;
        }
        public static bool UserUnFollowChannel(VideoChannelFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserUnFollowChannel(userFollow);
            }
            return returnValue;
        }
        public static bool UserUnFollowLabel(VideoLabelFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserUnFollowLabel(userFollow);
            }
            return returnValue;
        }
        public static bool UserUnFollowPublisher(PublisherFollowEntity userFollow)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.UserUnFollowPublisher(userFollow);
            }
            return returnValue;
        }
        #endregion

        #region thống kê
        public static List<UserStatisticEntity> GetStatisticStatus(int status, DateTime startDate, DateTime endDate)
        {
            List<UserStatisticEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.GetStatisticStatus(status, startDate, endDate);
            }
            return returnValue;
        }

        public static List<PlayListCountEntity> StatictisFollowPlaylist(string username, DateTime from, DateTime to)
        {
            List<PlayListCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.StatictisFollowPlaylist(username, from, to);
            }
            return returnValue;
        }

        public static List<VideoChannelCountEntity> StatictisFollowVideoChannel(string username, DateTime from, DateTime to)
        {
            List<VideoChannelCountEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EndUserMainDal.StatictisFollowVideoChannel(username, from, to);
            }
            return returnValue;
        }

        #endregion
    }
}
