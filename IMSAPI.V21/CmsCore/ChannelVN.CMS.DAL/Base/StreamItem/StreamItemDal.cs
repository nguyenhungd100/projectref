﻿using ChannelVN.CMS.Entity.Base.StreamItem;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.StreamItem
{
    public class StreamItemDal
    {
        #region StreamItem
        public static bool InsertStreamItem(StreamItemEntity streamItem, ref long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.InsertStreamItem(streamItem, ref id);
            }
            return returnValue;
        }

        public static bool UpdateStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.UpdateStreamItem(streamItem);
            }
            return returnValue;
        }
        public static bool DeleteStreamItemById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.DeleteStreamItemById(id);
            }
            return returnValue;
        }

        public static StreamItemEntity GetStreamItemById(long id)
        {
            StreamItemEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.GetStreamItemById(id);
            }
            return returnValue;
        }

        public static StreamItemDetailEntity GetNewsForStreamItemById(long newsId)
        {
            StreamItemDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.GetNewsForStreamItemById(newsId);
            }
            return returnValue;
        }

        public static List<StreamItemEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<StreamItemEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<StreamItemEntity> InitESAllStreamItem(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<StreamItemEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.InitESAllStreamItem(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
        #endregion

        #region StreamItemMobile
        public static bool InsertStreamItemMobile(StreamItemMobileEntity streamItem, ref long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.InsertStreamItemMobile(streamItem, ref id);
            }
            return returnValue;
        }

        public static bool UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.UpdateStreamItemMobile(streamItem);
            }
            return returnValue;
        }
        public static bool DeleteStreamItemMobileById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.DeleteStreamItemMobileById(id);
            }
            return returnValue;
        }

        public static StreamItemMobileEntity GetStreamItemMobileById(long id)
        {
            StreamItemMobileEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.GetStreamItemMobileById(id);
            }
            return returnValue;
        }

        public static List<StreamItemMobileEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<StreamItemMobileEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.StreamItemMainDal.SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion
    }
}
