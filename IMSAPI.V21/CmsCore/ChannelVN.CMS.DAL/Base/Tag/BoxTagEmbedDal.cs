﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class BoxTagEmbedDal
    {
        #region Tag
        public static List<BoxTagEmbedEntity> GetListTagEmbed(int zoneId, int type)
        {
            List<BoxTagEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.GetListTagEmbed(zoneId, type);
            }
            return returnValue;
        }
        public static bool Update(string listTagId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.Update(listTagId, zoneId, type);
            }
            return returnValue;
        }
        public static bool UpdateExternalTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.UpdateExternalTag(listTagId, zoneId, type, listTitle, listUrl);
            }
            return returnValue;
        }
        #endregion

        #region VideoTag
        public static List<BoxTagEmbedEntity> GetListVideoTagEmbed(int zoneId, int type)
        {
            List<BoxTagEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.GetListVideoTagEmbed(zoneId, type);
            }
            return returnValue;
        }
        public static bool UpdateVideoTag(string listTagId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.UpdateVideoTag(listTagId, zoneId, type);
            }
            return returnValue;
        }
        public static bool UpdateExternalVideoTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxTagEmbedMainDal.UpdateExternalVideoTag(listTagId, zoneId, type, listTitle, listUrl);
            }
            return returnValue;
        }
        #endregion
    }
}
