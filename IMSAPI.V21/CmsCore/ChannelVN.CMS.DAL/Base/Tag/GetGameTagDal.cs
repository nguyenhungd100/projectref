﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class GateGameTagDal
    {
        #region Get methods
        public static GateGameTagEntity GetGateGameTagByTagId(long tagId)
        {
            GateGameTagEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GateGameTagMainDal.GetGateGameTagByTagId(tagId);
            }
            return returnValue;
        }
        #endregion

        public static bool Insert(GateGameTagEntity gateGameTag, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.GateGameTagMainDal.Insert(gateGameTag, ref tagId);
            }
            return returnValue;
        }
    }
}
