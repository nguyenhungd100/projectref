﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagScoreDal
    {
        #region Insert/Update/List methods

        public static List<TagScoreEntity> List(long TagId)
        {
            List<TagScoreEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagScoreMainDal.List(TagId);
            }
            return returnValue;
        }

        public static bool Insert(TagScoreEntity tagscore)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagScoreMainDal.Insert(tagscore);
            }
            return returnValue;
        }

        public static bool Delete(long TagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagScoreMainDal.Delete(TagId);
            }
            return returnValue;
        }

        #endregion
    }
}
