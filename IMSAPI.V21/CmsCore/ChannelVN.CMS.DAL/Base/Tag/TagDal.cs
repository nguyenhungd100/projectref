﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagDal
    {
        #region Get methods
        public static List<TagEntity> GetTagByListOfId(string listTagId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByListOfId(listTagId);
            }
            return returnValue;
        }
        public static List<TagEntity> GetTagByParentTagId(long parentTagId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByParentTagId(parentTagId);
            }
            return returnValue;
        }
        public static TagEntity GetTagByTagId(long tagId)
        {
            TagEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByTagId(tagId);
            }
            return returnValue;
        }
        public static TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            TagEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByTagName(name, getTagHasNewsOnly);
            }
            return returnValue;
        }
        public static TagEntity GetTagByTagNameOrUrl(string name, string url)
        {
            TagEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByTagNameOrUrl(name, url);
            }
            return returnValue;
        }
        public static List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly);
            }
            return returnValue;
        }

        public static List<CountTagNewsEntity> CountTagNewsByListTagId(string listTagId)
        {
            List<CountTagNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.CountTagNewsByListTagId(listTagId);
            }
            return returnValue;
        }

        public static List<TagEntity> InitESAllTag(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.InitESAllTag(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }

        public static List<TagEntity> Tag_SearchInBoxEmbed(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Tag_SearchInBoxEmbed(keyword, isHotTag, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<TagEntity> Tag_SearchInBoxEmbed_Approved(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Tag_SearchInBoxEmbed_Approved(keyword, isHotTag, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<TagEntity> SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchAllByKeyword(keyword, zoneId, isThread);
            }
            return returnValue;
        }
        public static List<TagWithSimpleFieldEntity> SearchTagForSuggestion(int top, int zoneId, string keyword, int isHot, int orderBy, bool getTagHasNewsOnly)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.SearchTagForSuggestion(top, zoneId, keyword, isHot, orderBy, getTagHasNewsOnly);
            }
            return returnValue;
        }

        public static List<TagWithSimpleFieldEntity> GetAllCleanTag(string startByKeyword, int minWordCount, int maxWordCount)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetAllCleanTag(startByKeyword, minWordCount, maxWordCount);
            }
            return returnValue;
        }

        public static List<TagWithSimpleFieldEntity> Tag_GetRelation(long TagId)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetRelationTag(TagId);
            }
            return returnValue;
        }
        public static bool Update_RelationTag(long TagId, string TagRelationIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Update_RelationTag(TagId, TagRelationIds);
            }
            return returnValue;
        }
        #endregion

        #region Update methods
        public static bool Insert(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Insert(tag, zoneId, zoneIdList, ref tagId);
            }
            return returnValue;
        }

        public static bool Insert(TagEntity tag, ref long tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Insert(tag, ref tagId);
            }
            return returnValue;
        }

        //public static bool InsertNew(TagEntity tag, ref long tagId) 
        //{
        //    try
        //    {
        //        tagId = 0;
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@ParentID", tag.ParentId),
        //                                 new SqlParameter("@Name", tag.Name),
        //                                 new SqlParameter("@Description", tag.Description),
        //                                 new SqlParameter("@Url", tag.Url),
        //                                 new SqlParameter("@Invisibled", tag.Invisibled),
        //                                 new SqlParameter("@IsHotTag", tag.IsHotTag),
        //                                 new SqlParameter("@Type", tag.Type),
        //                                 new SqlParameter("@CreatedBy", tag.CreatedBy),
        //                                 new SqlParameter("@UnsignName", tag.UnsignName),
        //                                 new SqlParameter("@IsThread", tag.IsThread),
        //                                 new SqlParameter("@Avatar", tag.Avatar),
        //                                 new SqlParameter("@Priority", tag.Priority),
        //                                 new SqlParameter("@Id", 0) {Direction = ParameterDirection.Output}
        //                             };
        //        SqlHelperParameterCache.CacheParameterSet(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
        //                                                    DbCommon.DatabaseSchema + "CMS_Tag_Insert",
        //                                                    parameters);

        //        int numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
        //                                                    CommandType.StoredProcedure,
        //                                                    DbCommon.DatabaseSchema + "CMS_Tag_Insert",
        //                                                    parameters);
        //        return numberOfRow > 0;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_Tag_Insert:{0}", ex.Message));
        //    }
        //}

        public static bool Update(TagEntity tag, int zoneId, string zoneIdList)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Update(tag, zoneId, zoneIdList);
            }
            return returnValue;
        }
        public static bool UpdateViewCountByUrl(int viewCount, string url)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdateViewCountByTag(viewCount, url);
            }
            return returnValue;
        }
        public static bool UpdateListViewCountByUrl(string value)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdateListViewCountByTag(value);
            }
            return returnValue;
        }
        public static bool DeleteById(long tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.DeleteById(tagId);
            }
            return returnValue;
        }
        public static bool DeleteTagNewsById(string tagName, long newsId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.DeleteTagNewsById(tagName, newsId);
            }
            return returnValue;
        }

        //public static bool UpdateTagForNews(long newsId, string tagIds)
        //{
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId),
        //                                 new SqlParameter("@TagIDs", tagIds)
        //                             };
        //        var numberOfRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
        //                                                    CommandType.StoredProcedure,
        //                                                    DbCommon.DatabaseSchema + "CMS_TagNews_UpdateTagForNews",
        //                                                    parameters);
        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(DbCommon.DatabaseSchema + "CMS_TagNews_UpdateTagForNews:{0}", ex.Message));
        //    }
        //}

        /// <summary>
        /// Thêm mới thông tin tag vào bảng tagNews
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="tagId"></param>
        /// <param name="tagMode"></param>
        /// <returns></returns>
        public static bool AddNews(string newsIds, long tagId, int tagMode = 1)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.AddNews(newsIds, tagId, tagMode);
            }
            return returnValue;
        }

        public static bool UpdatePriority(long tagId, long priority)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdatePriority(tagId, priority);
            }
            return returnValue;
        }

        public static bool UpdateTagHot(long tagId, bool isHotTag)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdateTagHot(tagId, isHotTag);
            }
            return returnValue;
        }
        public static bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdateTagInvisibled(tagId, invisibled);
            }
            return returnValue;
        }        

        /// <summary>
        /// Cập nhật tag cho bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static bool UpdateTagNews(long newsId, string tagName)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.UpdateTagNews(newsId, tagName);
            }
            return returnValue;
        }

        #endregion

        public static List<TagWithSimpleFieldEntity> GetTagByListOfTagName(string listTagName)
        {
            List<TagWithSimpleFieldEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetTagByListOfTagName(listTagName);
            }
            return returnValue;
        }

        public static List<TagEntity> GetListTagByNewsId(long newsId)
        {
            List<TagEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.GetListTagByNewsId(newsId);
            }
            return returnValue;
        }

        public static bool RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.RecieveFromCloud(tag, zoneId, zoneIdList, ref tagId);
            }
            return returnValue;
        }

        #region Seoer_Insert_Tag
        public static bool Seoer_Insert_Tag(string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.Seoer_Insert_Tag(TagNames, CreatedBy, PrimaryZoneId, OtherZoneId, ref Ids);
            }
            return returnValue;
        }
        #endregion

        #region BoxTagSeoEmbed

        public static List<BoxTagSeoEmbedEntity> ListBoxTagSeoEmbed(long tagId, int type)
        {
            List<BoxTagSeoEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.ListBoxTagSeoEmbed(tagId, type);
            }
            return returnValue;
        }

        public static bool InsertBoxTagSeoEmbed(BoxTagSeoEmbedEntity box, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagMainDal.InsertBoxTagSeoEmbed(box, ref id);
            }
            return returnValue;
        }

        public static bool UpdateBoxTagSeoEmbed(BoxTagSeoEmbedEntity box)
        {
            using (var db = new CmsMainDb())
            {
                return db.TagMainDal.UpdateBoxTagSeoEmbed(box);
            }
        }

        public static bool DeleteBoxTagSeoEmbed(int id)
        {
            using (var db = new CmsMainDb())
            {
                return db.TagMainDal.DeleteBoxTagSeoEmbed(id);
            }
        }

        #endregion
    }
}
