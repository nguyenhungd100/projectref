﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class BoxThreadEmbedDal
    {
        public static List<BoxThreadEmbedEntity> GetListThreadEmbed(int zoneId, int type)
        {
            List<BoxThreadEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxThreadEmbedMainDal.GetListThreadEmbed(zoneId, type);
            }
            return returnValue;
        }

        public static bool Insert(BoxThreadEmbedEntity ThreadEmbebBox)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxThreadEmbedMainDal.Insert(ThreadEmbebBox);
            }
            return returnValue;
        }

        public static bool Update(string listThreadId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxThreadEmbedMainDal.Update(listThreadId, zoneId, type);
            }
            return returnValue;
        }

        public static bool Delete(long ThreadId, int zoneId, int type)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxThreadEmbedMainDal.Delete(ThreadId, zoneId, type);
            }
            return returnValue;
        }
    }
}
