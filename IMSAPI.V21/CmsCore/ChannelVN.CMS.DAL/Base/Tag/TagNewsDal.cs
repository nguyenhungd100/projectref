﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagNewsDal
    {
        public static List<TagNewsWithTagInfoEntity> GetTagNewsByNewsId(long newsId)
        {
            List<TagNewsWithTagInfoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagNewsMainDal.GetTagNewsByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<TagNewsEntity> GetTagNewsWithTagInfoByListOfTagId(string tagIds)
        {
            List<TagNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagNewsMainDal.GetTagNewsWithTagInfoByListOfTagId(tagIds);
            }
            return returnValue;
        }
        public static List<TagNewsEntity> GetTagNewsByNewsIdAndMode(long newsId, Int16 tagMode)
        {
            List<TagNewsEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagNewsMainDal.GetTagNewsByNewsIdAndMode(newsId, tagMode);
            }
            return returnValue;
        }

        //public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsId(long newsId)
        //{
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId)
        //                             };
        //        reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
        //                                         CommandType.StoredProcedure,
        //                                         DbCommon.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsId",
        //                                         parameters);

        //        var tagNewss = new List<TagNewsWithTagInfoEntity>();
        //        while (reader.Read())
        //        {
        //            var tagNews = new TagNewsWithTagInfoEntity();
        //            EntityBase.SetObjectValue(reader, ref tagNews);
        //            tagNewss.Add(tagNews);
        //        }
        //        return tagNewss;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format(DbCommon.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsId:{0}", ex.Message));
        //    }
        //    finally
        //    {
        //        if (null != reader && !reader.IsClosed) reader.Close();
        //    }
        //}

        //public static List<TagNewsWithTagInfoEntity> GetTagNewsWithTagInfoByNewsIdAndMode(long newsId, Int16 tagMode)
        //{
        //    SqlDataReader reader = null;
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId),
        //                                 new SqlParameter("@TagMode", tagMode)
        //                             };
        //        reader = SqlHelper.ExecuteReader(DbCommon.GetConnectionString(DbCommon.Connection.CmsMainDb),
        //                                         CommandType.StoredProcedure,
        //                                         DbCommon.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsIdAndTagMode",
        //                                         parameters);

        //        var tagNewss = new List<TagNewsWithTagInfoEntity>();
        //        while (reader.Read())
        //        {
        //            var tagNews = new TagNewsWithTagInfoEntity();
        //            EntityBase.SetObjectValue(reader, ref tagNews);
        //            tagNewss.Add(tagNews);
        //        }
        //        return tagNewss;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(String.Format(DbCommon.DatabaseSchema + "CMS_TagNews_GetWithTagInfoByNewsIdAndTagMode:{0}", ex.Message));
        //    }
        //    finally
        //    {
        //        if (null != reader && !reader.IsClosed) reader.Close();
        //    }
        //}

        public static bool UpdateNews(long tagId, string deleteNewsId, string addNewsId, int tagMode = 1)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagNewsMainDal.UpdateNews(tagId, deleteNewsId, addNewsId, tagMode);
            }
            return returnValue;
        }
    }
}
