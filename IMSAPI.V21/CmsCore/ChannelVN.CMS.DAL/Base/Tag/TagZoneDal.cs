﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagZoneDal
    {
        public static List<TagZoneEntity> GetTagNewsByTagId(long tagId)
        {
            List<TagZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagZoneMainDal.GetTagNewsByTagId(tagId);
            }
            return returnValue;
        }
    }
}
