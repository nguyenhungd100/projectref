﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagAutoDal
    {
        public static List<TagAutoEntity> GetAutoTagList()
        {
            List<TagAutoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagAutoMainDal.GetAutoTagList();
            }
            return returnValue;
        }

        public static bool UpdateAutoTagIsProcess(long newsId) 
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.TagAutoMainDal.UpdateAutoTagIsProcess(newsId);
            }
            return returnValue;
        }
    }
}
