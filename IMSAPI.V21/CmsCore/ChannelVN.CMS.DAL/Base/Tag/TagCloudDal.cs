﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Tag
{
    public class TagCloudDal
    {
        public static List<QueueUpdateTagChannelEntity> GetUnProcess(int channelId)
        {
            List<QueueUpdateTagChannelEntity> returnValue;
            using (var db = new TagCloudCmsDb())
            {
                returnValue = db.TagCloudMainDal.GetUnProcess(channelId);
            }
            return returnValue;
        }

        public static bool UpdateIsProcess(long newsId)
        {
            bool returnValue;
            using (var db = new TagCloudCmsDb())
            {
                returnValue = db.TagCloudMainDal.UpdateIsProcessed(newsId);
            }
            return returnValue;
        }
    }
}
