﻿using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.DAL.Base.Photo
{   
    public class AlbumByAuthorDal
    {
        public static bool SaveAlbumByAuthor(AlbumByAuthorEntity albumByAuthor)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumByAuthorDal.SaveAlbumByAuthor(albumByAuthor);

            }
            return returnValue;
        }
    }
}
