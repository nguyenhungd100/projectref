﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class EmbedAlbumDal
    {
        public static List<EmbedAlbumEntity> Search(string keyword, int type, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<EmbedAlbumEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumMainDal.Search(keyword, type, zoneId, status, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
        public static EmbedAlbumEntity GetById(long embedAlbumId)
        {
            EmbedAlbumEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumMainDal.GetById(embedAlbumId);

            }
            return returnValue;
        }
        public static bool Insert(EmbedAlbumEntity embedAlbum, ref int newEmbedAlbumId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumMainDal.Insert(embedAlbum, ref newEmbedAlbumId);

            }
            return returnValue;
        }
        public static bool Update(EmbedAlbumEntity embedAlbum)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumMainDal.Update(embedAlbum);

            }
            return returnValue;
        }
        public static bool Delete(int embedAlbumId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumMainDal.Delete(embedAlbumId);

            }
            return returnValue;
        }
    }
}
