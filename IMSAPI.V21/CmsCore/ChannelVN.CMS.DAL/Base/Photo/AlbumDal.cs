﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class AlbumDal
    {
        #region Update

        public static bool Insert(AlbumEntity album, string tagIdList, string eventIdList, ref int newAlbumId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Insert(album, tagIdList, eventIdList, ref newAlbumId);

            }
            return returnValue;
        }

        public static bool Update(AlbumEntity album, string tagIdList, string eventIdList)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Update(album, tagIdList, eventIdList);

            }
            return returnValue;
        }

        public static bool Delete(int albumId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Delete(albumId);

            }
            return returnValue;
        }

        public static bool Send(int albumId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Send(albumId, userDoAction);
            }
            return returnValue;
        }
        public static bool Publish(int albumId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Publish(albumId, userDoAction);
            }
            return returnValue;
        }
        public static bool UnPublish(int albumId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.UnPublish(albumId, userDoAction);
            }
            return returnValue;
        }
        public static bool Return(int albumId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Return(albumId, userDoAction);
            }
            return returnValue;
        }

        public static bool AddPhotoInAlbum(string addPhotoIds, string deletePhotoIds, int albumId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.AddPhotoInAlbum(addPhotoIds, deletePhotoIds, albumId);
            }
            return returnValue;
        }

        public static bool UpdateThumbImageByPhotoId(int albumId, long photoId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.UpdateThumbImageByPhotoId(albumId, photoId);

            }
            return returnValue;
        }

        public static AlbumPublishedEntity Publish(int albumId, long newsId, int zoneId, string distributedBy)
        {
            AlbumPublishedEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Publish(albumId, newsId, zoneId, distributedBy);

            }
            return returnValue;
        }

        #endregion

        #region Get

        public static AlbumEntity GetById(long albumId)
        {
            AlbumEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.GetById(albumId);

            }
            return returnValue;
        }

        public static List<AlbumEntity> Search(string keyword, int zoneId,int eventId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<AlbumEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.Search(keyword, zoneId, eventId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }

        public static List<CountAlbumStatus> CountAlbum(string username, List<string> listStatus, string userAction)
        {
            List<CountAlbumStatus> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.CountAlbum(username, listStatus, userAction);

            }
            return returnValue;
        }

        public static List<AlbumEntity> InitRedisAllPhotoAlbum(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<AlbumEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumMainDal.InitRedisAllPhotoAlbum(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }

        #endregion
    }
}
