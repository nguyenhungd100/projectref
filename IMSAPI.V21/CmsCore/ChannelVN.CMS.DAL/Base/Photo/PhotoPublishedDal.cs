﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoPublishedDal
    {
        #region Update

        public static bool Update(PhotoPublishedEntity photoPublished)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.Update(photoPublished);

            }
            return returnValue;
        }

        public static bool UpdateAlbumId(PhotoPublishedUpdateEntity photoPublished)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.UpdateAlbumId(photoPublished);

            }
            return returnValue;
        }

        public static bool UpdatePhotoInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.UpdatePhotoInUsed(listPhotoPublishedIdInUsed, newsId, albumId, createdBy);

            }
            return returnValue;
        }

        //public static bool UpdatePhotoInUsedWhenInsertNews(string listPhotoPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    try
        //    {
        //        var sqlParameters = new[]
        //                                {
        //                                    new SqlParameter("@ListPhotoPublishedIdInUsed", listPhotoPublishedIdInUsed),
        //                                    new SqlParameter("@NewsId", newsId),
        //                                    new SqlParameter("@CreatedBy", createdBy)
        //                                };
        //        var numberRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsPhotoDb),
        //                                                  CommandType.StoredProcedure,
        //                                                  DbCommon.DatabaseSchema + "PhotoCms_PhotoPublished_UpdatePhotoInUsedWhenInsert",
        //                                                  sqlParameters);
        //        return numberRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(DbCommon.DatabaseSchema + "PhotoCms_PhotoPublished_UpdatePhotoInUsedWhenInsert:{0}", ex.Message));
        //    }
        //}

        public static bool UpdatePriority(string listPhotoPublishedId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.UpdatePriority(listPhotoPublishedId);

            }
            return returnValue;
        }

        public static bool Delete(string listPhotoPublishedId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.Delete(listPhotoPublishedId);

            }
            return returnValue;
        }

        #endregion

        #region Get

        public static PhotoPublishedEntity GetById(long photoPublishedId)
        {
            PhotoPublishedEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.GetById(photoPublishedId);

            }
            return returnValue;
        }

        public static List<PhotoPublishedEntity> GetByPhotoId(long photoId)
        {
            List<PhotoPublishedEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.GetByPhotoId(photoId);

            }
            return returnValue;
        }

        public static List<PhotoPublishedEntity> SearchInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoPublishedEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoPublishedMainDal.SearchInAlbum(keyword, albumId, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }

        #endregion
    }
}
