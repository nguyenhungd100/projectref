﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoDal
    {
        #region Update

        public static bool Insert(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Insert(photo, tagIdList, ref newPhotoId);

            }
            return returnValue;
        }

        public static bool InsertV2(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.InsertV2(photo, tagIdList, ref newPhotoId);

            }
            return returnValue;
        }

        public static bool Update(PhotoEntity photo, string tagIdList)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Update(photo, tagIdList);

            }
            return returnValue;
        }

        public static bool UpdateV2(PhotoEntity photo, string tagIdList)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.UpdateV2(photo, tagIdList);

            }
            return returnValue;
        }     

        public static bool Delete(long photoId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Delete(photoId);

            }
            return returnValue;
        }

        public static bool DeletePhotoInAlbum(string listPhotoId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.DeletePhotoInAlbum(listPhotoId);

            }
            return returnValue;
        }

        public static PhotoPublishedEntity Publish(long photoId, long newsId, int zoneId, int albumId, string distributedBy)
        {
            PhotoPublishedEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Publish(photoId, newsId, zoneId, albumId, distributedBy);

            }
            return returnValue;
        }

        public static bool Send(long photoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Send(photoId, userDoAction);
            }
            return returnValue;
        }
        public static bool PublishV2(long photoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.PublishV2(photoId, userDoAction);
            }
            return returnValue;
        }
        public static bool UnPublish(long photoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.UnPublish(photoId, userDoAction);
            }
            return returnValue;
        }
        public static bool Return(long photoId, string userDoAction)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Return(photoId, userDoAction);
            }
            return returnValue;
        }        

        #endregion

        #region Get

        public static PhotoEntity GetById(long photoId)
        {
            PhotoEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.GetById(photoId);

            }
            return returnValue;
        }
        public static PhotoEntity GetByIdV2(long photoId)
        {
            PhotoEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.GetByIdV2(photoId);

            }
            return returnValue;
        }

        public static PhotoEntity GetByImageUrl(string imageUrl)
        {
            PhotoEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.GetByImageUrl(imageUrl);

            }
            return returnValue;
        }

        public static List<PhotoEntity> Search(string keyword, int photoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.Search(keyword, photoLabelId, zoneId, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
      
        public static List<PhotoEntity> SearchV2(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.SearchV2(keyword, zoneId, albumId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }
        public static List<PhotoEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);

            }
            return returnValue;
        }

        public static List<CountAlbumStatus> CountPhoto(string username, List<string> listStatus, string userAction)
        {
            List<CountAlbumStatus> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.CountPhoto(username, listStatus, userAction);

            }
            return returnValue;
        }

        #endregion

        public static List<PhotoEntity> InitRedisAllPhoto(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            List<PhotoEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoMainDal.InitRedisAllPhoto(pageIndex, pageSize, fromDate, toDate, ref totalRow);
            }
            return returnValue;
        }
    }
}
