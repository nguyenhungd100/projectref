﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoTagDal
    {
        public static List<PhotoTagsEntity> GetByPhotoId(long photoId)
        {
            List<PhotoTagsEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.GetByPhotoId(photoId);

            }
            return returnValue;
        }

        public static bool Insert(PhotoTagEntity tag, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.Insert(tag, ref  tagId);
            }
            return returnValue;
        }

        public static bool Update(PhotoTagEntity tag)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.Update(tag);
            }
            return returnValue;
        }
        public static bool UpdateByName(PhotoTagEntity tag, ref int tagId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.UpdateByName(tag, ref tagId);
            }
            return returnValue;
        }
        public static PhotoTagEntity GetTagByTagName(string name)
        {
            PhotoTagEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.GetTagByTagName(name);
            }
            return returnValue;
        }

        public static List<PhotoTagEntity> SearchTag(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoTagEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.SearchTag(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<PhotoTagEntity> SearchTag(string keyword)
        {
            List<PhotoTagEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.SearchTag(keyword);
            }
            return returnValue;
        }

        public static PhotoTagEntity GetById(int id)
        {
            PhotoTagEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.GetById(id);
            }
            return returnValue;
        }

        public static List<PhotoTagEntity> GetByAlbumId(int id)
        {
            List<PhotoTagEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoTagMainDal.GetByAlbumId(id);
            }
            return returnValue;
        }
    }
}
