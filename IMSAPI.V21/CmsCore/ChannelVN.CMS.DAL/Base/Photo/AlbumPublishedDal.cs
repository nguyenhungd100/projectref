﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class AlbumPublishedDal
    {
        #region Update

        public static bool Update(AlbumPublishedEntity albumPublished)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumPublishedMainDal.Update(albumPublished);

            }
            return returnValue;
        }

        public static bool UpdateAlbumInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumPublishedMainDal.UpdateAlbumInUsed(listAlbumPublishedIdInUsed, newsId, createdBy);

            }
            return returnValue;
        }

        //public static bool UpdateAlbumInUsedWhenInsertNews(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    try
        //    {
        //        var sqlParameters = new[]
        //                                {
        //                                    new SqlParameter("@ListAlbumPublishedIdInUsed", listAlbumPublishedIdInUsed),
        //                                    new SqlParameter("@NewsId", newsId),
        //                                    new SqlParameter("@CreatedBy", createdBy)
        //                                };
        //        var numberRow = SqlHelper.ExecuteNonQuery(DbCommon.GetConnectionString(DbCommon.Connection.CmsPhotoDb),
        //                                                  CommandType.StoredProcedure,
        //                                                  DbCommon.DatabaseSchema + "PhotoCms_AlbumPublished_UpdateAlbumInUsedWhenInsert",
        //                                                  sqlParameters);
        //        return numberRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(DbCommon.DatabaseSchema + "PhotoCms_AlbumPublished_UpdateAlbumInUsedWhenInsert:{0}", ex.Message));
        //    }
        //}

        #endregion

        #region Get

        public static AlbumPublishedEntity GetById(long albumPublishedId)
        {
            AlbumPublishedEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumPublishedMainDal.GetById(albumPublishedId);

            }
            return returnValue;
        }

        public static List<AlbumPublishedEntity> GetByAlbumId(long photoId)
        {
            List<AlbumPublishedEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.AlbumPublishedMainDal.GetByAlbumId(photoId);

            }
            return returnValue;
        }

        #endregion
    }
}
