﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class EmbedAlbumDetailDal
    {
        public static List<EmbedAlbumDetailEntity> GetByEmbedAlbumId(int embedAlbumId, int topPhoto)
        {
            List<EmbedAlbumDetailEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.GetByEmbedAlbumId(embedAlbumId, topPhoto);

            }
            return returnValue;
        }
        public static EmbedAlbumDetailEntity GetById(long embedAlbumId)
        {
            EmbedAlbumDetailEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.GetById(embedAlbumId);

            }
            return returnValue;
        }

        public static bool Insert(EmbedAlbumDetailEntity embedAlbum, ref int newEmbedAlbumDetailId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.Insert(embedAlbum, ref newEmbedAlbumDetailId);

            }
            return returnValue;
        }
        public static bool Update(EmbedAlbumDetailEntity embedAlbum)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.Update(embedAlbum);

            }
            return returnValue;
        }
        public static bool DeleteByEmbedAlbumIdExcudeListIds(int embedAlbumId, string excludeListIds)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.EmbedAlbumDetailMainDal.DeleteByEmbedAlbumIdExcudeListIds(embedAlbumId, excludeListIds);

            }
            return returnValue;
        }
    }
}
