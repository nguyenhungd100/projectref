﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoFolderDal
    {
        #region Update

        public static bool Insert(PhotoFolderEntity photoFolder, ref int newPhotoFolderId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.Insert(photoFolder, ref newPhotoFolderId);

            }
            return returnValue;

        }

        public static bool Update(PhotoFolderEntity photoFolder)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.Update(photoFolder);

            }
            return returnValue;
        }

        public static bool Delete(int photoFolderId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.Delete(photoFolderId);

            }
            return returnValue;
        }

        #endregion

        #region Get

        public static PhotoFolderEntity GetById(int photoFolderId)
        {
            PhotoFolderEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.GetById(photoFolderId);

            }
            return returnValue;
        }

        public static List<PhotoFolderEntity> Search(int parentId, string name, string createdBy, int status)
        {
            List<PhotoFolderEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.Search(parentId, name, createdBy,status);

            }
            return returnValue;
        }

        public static List<PhotoFolderEntity> GetByParentId(int parentId, string createdBy)
        {
            List<PhotoFolderEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoFolderMainDal.GetByParentId(parentId, createdBy);

            }
            return returnValue;
        }

        #endregion
    }
}
