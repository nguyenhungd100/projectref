﻿using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoByAuthorDal
    {
        public static bool SavePhotoByAuthor(PhotoByAuthorEntity photoByAuthor, ref int photoByAuthorId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoByAuthorDal.SavePhotoByAuthor(photoByAuthor, ref photoByAuthorId);

            }
            return returnValue;
        }       
    }
}
