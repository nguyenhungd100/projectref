﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Photo;

namespace ChannelVN.CMS.DAL.Base.Zone
{
    public class ZonePhotoDal
    {
        #region Gets
        public static List<ZonePhotoEntity> GetZonePhotoActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            List<ZonePhotoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.GetZonePhotoActivedByUsernameAndPermissionIds(username, permissionIds);
            }
            return returnValue;
        }

        public static bool Insert(ZonePhotoEntity zonePhoto, ref int zonePhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.Insert(zonePhoto, ref zonePhotoId);
            }
            return returnValue;
        }        
        public static bool Update(ZonePhotoEntity zonePhoto)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.Update(zonePhoto);
            }
            return returnValue;
        }
        public static List<ZonePhotoEntity> GetListByParentId(int parentId, int status)
        {
            List<ZonePhotoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.GetListByParentId(parentId, status);
            }
            return returnValue;
        }
        public static ZonePhotoEntity GetById(int id)
        {
            ZonePhotoEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.GetById(id);
            }
            return returnValue;
        }
        public static bool MoveUp(int zonePhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.MoveUp(zonePhotoId);
            }
            return returnValue;
        }

        public static bool MoveDown(int zonePhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.MoveDown(zonePhotoId);
            }
            return returnValue;
        }

        public static bool Delete(int zonePhotoId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.Delete(zonePhotoId);
            }
            return returnValue;
        }

        public static List<ZoneEntity> GetZoneByParentId(int parentId)
        {
            List<ZoneEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.GetZoneByParentId(parentId);
            }
            return returnValue;
        }
        public static List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id)
        {
            List<PhotoGroupDetailInZonePhotoEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.ZonePhotoMainDal.GetZonePhotoByPhotoId(id);
            }
            return returnValue;
        }

        #endregion
    }
}
