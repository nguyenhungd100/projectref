﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Photo;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoEventDal
    {
        public static List<PhotoEventEntity> SearchPhotoEvent(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoEventEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.SearchPhotoEvent(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static PhotoEventEntity GetById(int id)
        {
            PhotoEventEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.GetById(id);
            }
            return returnValue;
        }
        public static List<PhotoEventSimpleEntity> GetByAlbumId(int id)
        {
            List<PhotoEventSimpleEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.GetByAlbumId(id);
            }
            return returnValue;
        }
        public static bool Insert(PhotoEventEntity photoEvent, ref int photoEventId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.Insert(photoEvent, ref photoEventId);
            }
            return returnValue;
        }        
        public static bool Update(PhotoEventEntity photoEvent)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.Update(photoEvent);
            }
            return returnValue;
        }
        
        public static bool Delete(int photoEventId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.Delete(photoEventId);
            }
            return returnValue;
        }

        public static List<PhotoEventEntity> GetPhotoEventActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            List<PhotoEventEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.PhotoEventMainDal.GetPhotoEventActivedByUsernameAndPermissionIds(username, permissionIds);
            }
            return returnValue;
        }

    }
}
