﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.Photo
{
    public class PhotoLabelDal
    {
        #region Update

        public static bool Insert(PhotoLabelEntity photoLabel, ref int newPhotoLabelId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoLabelMainDal.Insert(photoLabel, ref newPhotoLabelId);

            }
            return returnValue;
        }

        public static bool Update(PhotoLabelEntity photoLabel)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoLabelMainDal.Update(photoLabel);

            }
            return returnValue;
        }

        public static bool Delete(int photoLabelId)
        {
            bool returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoLabelMainDal.Delete(photoLabelId);

            }
            return returnValue;
        }

        #endregion

        #region Get

        public static PhotoLabelEntity GetById(int photoLabelId)
        {
            PhotoLabelEntity returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoLabelMainDal.GetById(photoLabelId);

            }
            return returnValue;
        }

        public static List<PhotoLabelEntity> GetByParentId(int parentId, string createdBy)
        {
            List<PhotoLabelEntity> returnValue;
            using (var db = new CmsPhotoDb())
            {
                returnValue = db.PhotoLabelMainDal.GetByParentId(parentId, createdBy);

            }
            return returnValue;
        }

        #endregion
    }
}
