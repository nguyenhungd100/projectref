﻿using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.NewsSocial
{
    public class BoxNewsSocialEmbedDal
    {
        #region BoxNewsSocialEmbed        

        public static List<BoxNewsSocialEmbedEntity> GetListBoxNewsSocialEmbed(int type, int zoneId)
        {
            List<BoxNewsSocialEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.BoxNewsSocialEmbedMainDal.GetListBoxNewsSocialEmbed(type, zoneId);
            }
            return returnValue;
        }

        public static void Update(string listNewsId, int type, int zoneId)
        {
            using (var db = new CmsMainDb())
            {
                db.BoxNewsSocialEmbedMainDal.Update(listNewsId, type, zoneId);
            }
        }
        #endregion        
    }
}
