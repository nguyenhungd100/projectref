﻿using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.NewsSocial
{
    public class NewsSocialEmbedDal
    {
        #region NewsSocialEmbed
        public static bool InsertNewsSocialEmbed(NewsSocialEmbedEntity streamItem, ref long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.InsertNewsSocialEmbed(streamItem, ref id);
            }
            return returnValue;
        }

        public static bool UpdateNewsSocialEmbed(NewsSocialEmbedEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.UpdateNewsSocialEmbed(streamItem);
            }
            return returnValue;
        }

        public static bool DeleteNewsSocialEmbedById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.DeleteNewsSocialEmbedById(id);
            }
            return returnValue;
        }

        public static bool OnToggleStatus(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.OnToggleStatus(id);
            }
            return returnValue;
        }

        public static NewsSocialEmbedEntity GetNewsSocialEmbedById(long id)
        {
            NewsSocialEmbedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.GetNewsSocialEmbedById(id);
            }
            return returnValue;
        }

        public static NewsSocialEmbedEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            NewsSocialEmbedEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.GetNewsSocialEmbedByRawId(rawId);
            }
            return returnValue;
        }

        public static List<NewsSocialEmbedEntity> SearchNewsSocialEmbed(int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSocialEmbedEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialEmbedMainDal.SearchNewsSocialEmbed(status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion        
    }
}
