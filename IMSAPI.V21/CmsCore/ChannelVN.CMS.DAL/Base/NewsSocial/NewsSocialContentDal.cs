﻿using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.DAL.Base.NewsSocial
{
    public class NewsSocialContentDal
    {
        #region NewsSocialContent
        public static bool InsertNewsSocialContent(NewsSocialContentEntity streamItem, ref long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.InsertNewsSocialContent(streamItem, ref id);
            }
            return returnValue;
        }

        public static bool UpdateNewsSocialContent(NewsSocialContentEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.UpdateNewsSocialContent(streamItem);
            }
            return returnValue;
        }

        public static bool DeleteNewsSocialContentById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.DeleteNewsSocialContentById(id);
            }
            return returnValue;
        }

        public static bool OnToggleStatus(long id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.OnToggleStatus(id);
            }
            return returnValue;
        }

        public static NewsSocialContentEntity GetNewsSocialContentById(long id)
        {
            NewsSocialContentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.GetNewsSocialContentById(id);
            }
            return returnValue;
        }

        public static NewsSocialContentEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            NewsSocialContentEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.GetNewsSocialEmbedByRawId(rawId);
            }
            return returnValue;
        }

        public static List<NewsSocialContentEntity> SearchNewsSocialContent(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSocialContentEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.NewsSocialContentMainDal.SearchNewsSocialContent(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion        
    }
}
