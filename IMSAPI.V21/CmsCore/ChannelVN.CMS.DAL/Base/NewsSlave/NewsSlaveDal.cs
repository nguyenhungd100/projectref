﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.NewsSlave
{
    public class NewsSlaveDal
    {
        #region NewsSlave                
        public static bool DeleteNews(long newsId, string connectSlave)
        {
            using (var db = new SlaveCmsDb(connectSlave))
            {
                return db.NewsSlaveMainDal.DeleteNews(newsId);
            }
        }
        #endregion        
    }
}
