﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.DAL.Base.AuthenticateLog
{
    public class AuthenticateLogDal
    {
        public static bool Insert(AuthenticateLogEntity user, ref long newLogId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.AuthenticateLogMainDal.Insert(user, ref newLogId);
            }
            return returnValue;
        }
    }
}
