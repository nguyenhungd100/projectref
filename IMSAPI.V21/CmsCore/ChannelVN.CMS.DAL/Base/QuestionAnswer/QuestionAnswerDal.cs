﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Common;
using ChannelVN.CMS.Entity.Base.GroupQuestion;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.QuestionAnswer
{
    public class QuestionAnswerDal
    {
        public static bool InsertQuestion(QuestionEntity news, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.InsertQuestion(news, ref id);
            }
            return returnValue;
        }

        public static bool InsertQuestionInEvent(int questionId, int eventId)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.InsertQuestionInEvent(questionId, eventId);
            }
            return returnValue;
        }

        public static bool InsertAnswer(AnswerEntity news, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.InsertAnswer(news, ref id);
            }
            return returnValue;
        }
        public static bool InsertGroupQuestion(GroupQuestionEntity news, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.InsertGroupQuestion(news, ref id);
            }
            return returnValue;
        }
        public static bool InsertEventQuestion(EventQuestionEntity news, ref int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.InsertEventQuestion(news, ref id);
            }
            return returnValue;
        }
        public static bool UpdateGroupQuestion(GroupQuestionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.UpdateGroupQuestion(news);
            }
            return returnValue;
        }
        public static bool UpdateEventQuestion(EventQuestionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.UpdateEventQuestion(news);
            }
            return returnValue;
        }
        public static bool DeleteGroupQuestion(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.DeleteGroupQuestion(id);
            }
            return returnValue;
        }

        public static bool DeleteEventQuestion(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.DeleteEventQuestion(id);
            }
            return returnValue;
        }

        public static bool UpdateQuestion(QuestionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.UpdateQuestion(news);
            }
            return returnValue;
        }
        public static bool UpdateAnswer(AnswerEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.UpdateAnswer(news);
            }
            return returnValue;
        }
        public static bool DeleteQuestion(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.DeleteQuestion(id);
            }
            return returnValue;
        }
        public static bool DeleteAnswer(int id)
        {
            bool returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.DeleteAnswer(id);
            }
            return returnValue;
        }
        public static List<GroupQuestionEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<GroupQuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.Search(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<EventQuestionEntity> SearchEvent(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            List<EventQuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.SearchEvent(keyword, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<QuestionEntity> GetListQuestionByGroup(string keyword, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetListQuestionByGroup(keyword, groupId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<QuestionEntity> GetListQuestionByGroupEvent(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetListQuestionByGroupEvent(keyword, groupId, eventId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<QuestionEntity> GetListQuestionBefore(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuestionEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetListQuestionBefore(keyword, groupId, eventId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<AnswerEntity> GetListAnswer(int questionId)
        {
            List<AnswerEntity> returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetListAnswer(questionId);
            }
            return returnValue;
        }
        public static QuestionEntity GetQuestionById(int questionId)
        {
            QuestionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetQuestionById(questionId);
            }
            return returnValue;
        }
        public static GroupQuestionEntity GetGroupQuestionById(int questionId)
        {
            GroupQuestionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetGroupQuestionById(questionId);
            }
            return returnValue;
        }
        public static EventQuestionEntity GetEventQuestionById(int questionId)
        {
            EventQuestionEntity returnValue;
            using (var db = new CmsMainDb())
            {
                returnValue = db.QuestionAnswerMainDal.GetEventQuestionById(questionId);
            }
            return returnValue;
        }
    }
}
