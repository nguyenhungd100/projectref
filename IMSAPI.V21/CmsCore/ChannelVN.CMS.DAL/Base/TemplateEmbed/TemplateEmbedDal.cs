﻿using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.TemplateEmbed;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.DAL.Base.TemplateEmbed
{
    public class TemplateEmbedDal
    {                
        public static List<TemplateEmbedEntity> SearchTemplateEmbed(string keyword, int channelId, int typeId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<TemplateEmbedEntity> returnValue;
            using (var db = new TemplateEmbedCmsDb())
            {
                returnValue = db.TemplateEmbedMainDal.SearchTemplateEmbed(keyword, channelId, typeId, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }                        
    }
}
