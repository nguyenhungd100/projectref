﻿using System;
using System.Web;
using System.Web.Caching;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.DAL.Common
{
    public class DbCommon
    {
        private const string _CONNECTION_DECRYPT_KEY = "nfvsMof35XnUdQEWuxgAZta";

        private static string GetConnectionName(Connection connection)
        {
            switch (connection)
            {
                case Connection.CmsMainDb:
                    return "CmsMainDb";
                case Connection.CmsAnalyticDb:
                    return "CmsAnalyticDb";
                case Connection.ExternalCmsDb:
                    return "ExternalCmsDb";
                case Connection.CmsCrawlerDb:
                    return "CmsCrawlerDb";
                case Connection.CmsVideoDb:
                    return "CmsVideoDb";
                case Connection.CmsPhotoDb:
                    return "CmsPhotoDb";
                case Connection.Kenh14OldVersionDb:
                    return "Kenh14OldVersionDb";
                case Connection.MaskOnlineDb:
                    return "MaskOnlineDb";
                case Connection.Statistic:
                    return "CmsSystemStatisticsDb";
                case Connection.ExternalCmsVideoDb:
                    return "ExternalCmsVideoDb";
                default:
                    return "";
            }
        }

        public enum Connection
        {
            CmsMainDb = 1,
            ExternalCmsDb = 2,
            CmsCrawlerDb = 3,
            CmsVideoDb = 4,
            CmsPhotoDb = 5,
            CmsAnalyticDb = 6,
            Kenh14OldVersionDb = 7,
            MaskOnlineDb = 8,
            Statistic = 9,
            ExternalCmsVideoDb = 10

        }
        public static DateTime MinDateTime = new DateTime(1980, 1, 1);
        public static string DatabaseSchema = "[dbo].";
        public static string GetConnectionString(Connection connection)
        {
            return ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace, GetConnectionName(connection),
                                                            _CONNECTION_DECRYPT_KEY);
        }

        public static bool IsUseMainDal
        {
            get
            {
                return Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "IsUseMainDal"));
            }
           
        }
    }
}
