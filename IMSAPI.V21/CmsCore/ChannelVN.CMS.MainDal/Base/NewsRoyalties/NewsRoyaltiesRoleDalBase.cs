﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyalties
{
    public abstract class NewsRoyaltiesRoleDalBase
    {
        #region Get

        public List<NewsRoyaltiesRoleEntity> GetAll()
        {
            const string commandText = "CMS_NewsRoyaltiesRole_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<NewsRoyaltiesRoleEntity> data = _db.GetList<NewsRoyaltiesRoleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

         #region Core members

        private readonly CmsMainDb _db;

        protected NewsRoyaltiesRoleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
