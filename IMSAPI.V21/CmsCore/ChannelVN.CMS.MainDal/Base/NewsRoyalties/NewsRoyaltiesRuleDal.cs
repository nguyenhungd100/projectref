﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyalties
{
    public class NewsRoyaltiesRuleDal: NewsRoyaltiesRuleDalBase
    {
        internal NewsRoyaltiesRuleDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
