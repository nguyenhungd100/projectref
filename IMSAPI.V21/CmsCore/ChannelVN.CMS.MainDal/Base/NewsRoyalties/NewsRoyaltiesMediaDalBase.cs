﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyalties
{
    public abstract class NewsRoyaltiesMediaDalBase
    {
        #region Get

        public List<NewsRoyaltiesMediaEntity> GetAll()
        {
            const string commandText = "CMS_NewsRoyaltiesMedia_GetAllMedia";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<NewsRoyaltiesMediaEntity> data = _db.GetList<NewsRoyaltiesMediaEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsRoyaltiesMediaDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
