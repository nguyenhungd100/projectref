﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyalties
{
    public class NewsRoyaltiesRuleDalBase
    {
        #region Get

        public List<NewsRoyaltiesRuleEntity> GetListRoleLevel()
        {
            const string commandText = "CMS_NewsRoyaltiesRule_GetListRoleLevel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<NewsRoyaltiesRuleEntity> data = _db.GetList<NewsRoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsRoyaltiesRuleEntity GetValueByRoleLevelCategoryId(int roleId, int levelId, int categoryId)
        {
            const string commandText = "CMS_NewsRoyaltiesRule_GetValueByRoleLevelCategoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "roleId", roleId);
                _db.AddParameter(cmd, "levelId", levelId);
                _db.AddParameter(cmd, "categoryId", categoryId);
                NewsRoyaltiesRuleEntity data = _db.Get<NewsRoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsRoyaltiesRuleEntity GetValueByRoleLevelMediaId(int roleId, int levelId, int mediaId)
        {
            const string commandText = "CMS_NewsRoyaltiesRule_GetValueByRoleLevelMediaId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "roleId", roleId);
                _db.AddParameter(cmd, "levelId", levelId);
                _db.AddParameter(cmd, "mediaId", mediaId);
                NewsRoyaltiesRuleEntity data = _db.Get<NewsRoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsRoyaltiesRuleEntity> GetAll()
        {
            const string commandText = "CMS_NewsRoyaltiesRule_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<NewsRoyaltiesRuleEntity> data = _db.GetList<NewsRoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        public bool UpdateValueByRoleLevelCategoryId(NewsRoyaltiesRuleEntity royalties)
        {
            const string commandText = "CMS_NewsRoyaltiesRule_UpdateValueByRoleLevelCategoryId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoleId", royalties.RoleId);
                _db.AddParameter(cmd, "LevelId", royalties.LevelId);
                _db.AddParameter(cmd, "CategoryId", royalties.CategoryId);
                _db.AddParameter(cmd, "Value", royalties.Value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateValueByRoleLevelMediaId(NewsRoyaltiesRuleEntity royalties)
        {
            const string commandText = "CMS_NewsRoyaltiesRule_UpdateValueByRoleLevelMediaId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoleId", royalties.RoleId);
                _db.AddParameter(cmd, "LevelId", royalties.LevelId);
                _db.AddParameter(cmd, "MediaId", royalties.MediaId);
                _db.AddParameter(cmd, "Value", royalties.Value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsRoyaltiesRuleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
