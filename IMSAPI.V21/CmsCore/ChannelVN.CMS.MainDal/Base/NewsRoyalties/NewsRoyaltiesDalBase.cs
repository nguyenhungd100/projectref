﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsRoyalties;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyalties
{
    public abstract class NewsRoyaltiesDalBase
    {
        #region Get

        public List<NewsRoyaltiesEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsRoyalties_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                List<NewsRoyaltiesEntity> data = _db.GetList<NewsRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsRoyaltiesEntity GetById(long id)
        {
            const string commandText = "CMS_NewsRoyalties_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "id", id);
                NewsRoyaltiesEntity data = _db.Get<NewsRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsRoyaltiesEntity> Search(string userName, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "CMS_NewsRoyalties_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "userName", userName);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesRoleId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesRate);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsRoyaltiesEntity> data = _db.GetList<NewsRoyaltiesEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        public bool Insert(NewsRoyaltiesEntity royalties, ref long newRoyaltiesId)
        {
            const string commandText = "CMS_NewsRoyalties_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newRoyaltiesId, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", royalties.NewsId);
                _db.AddParameter(cmd, "Username", royalties.Username);
                _db.AddParameter(cmd, "PenName", royalties.PenName);
                _db.AddParameter(cmd, "RoleId", royalties.RoleId);
                _db.AddParameter(cmd, "LevelId", royalties.LevelId);
                _db.AddParameter(cmd, "CategoryId", royalties.CategoryId);
                _db.AddParameter(cmd, "MediaId", royalties.MediaId);
                _db.AddParameter(cmd, "NumberOfMedia", royalties.NumberOfMedia);
                _db.AddParameter(cmd, "Value", royalties.Value);
                _db.AddParameter(cmd, "Note", royalties.Note);
                _db.AddParameter(cmd, "CreatedBy", royalties.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);
                _db.AddParameter(cmd, "Status", 1);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newRoyaltiesId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(NewsRoyaltiesEntity royalties)
        {
            const string commandText = "CMS_NewsRoyalties_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.Id);
                _db.AddParameter(cmd, "NewsId", royalties.NewsId);
                _db.AddParameter(cmd, "Username", royalties.Username);
                _db.AddParameter(cmd, "PenName", royalties.PenName);
                _db.AddParameter(cmd, "RoleId", royalties.RoleId);
                _db.AddParameter(cmd, "LevelId", royalties.LevelId);
                _db.AddParameter(cmd, "CategoryId", royalties.CategoryId);
                _db.AddParameter(cmd, "MediaId", royalties.MediaId);
                _db.AddParameter(cmd, "NumberOfMedia", royalties.NumberOfMedia);
                _db.AddParameter(cmd, "Value", royalties.Value);
                _db.AddParameter(cmd, "Note", royalties.Note);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateValue(NewsRoyaltiesEntity royalties)
        {
            const string commandText = "CMS_NewsRoyalties_UpdateValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.Id);
                _db.AddParameter(cmd, "Value", royalties.Value);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMediaNumber(NewsRoyaltiesEntity royalties)
        {
            const string commandText = "CMS_NewsRoyalties_UpdateMediaNumber";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.Id);
                _db.AddParameter(cmd, "NumberOfMedia", royalties.NumberOfMedia);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateLevelId(NewsRoyaltiesEntity royalties)
        {
            const string commandText = "CMS_NewsRoyalties_UpdateLevelId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.NewsId);
                _db.AddParameter(cmd, "LevelId", royalties.LevelId);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int royaltiesId)
        {
            const string commandText = "CMS_NewsRoyalties_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteByNewsId(long newsId)
        {
            const string commandText = "CMS_Royalties_DeleteByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsRoyaltiesDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
