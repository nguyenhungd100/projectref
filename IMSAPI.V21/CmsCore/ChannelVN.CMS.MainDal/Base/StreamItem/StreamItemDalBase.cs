﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.StreamItem;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.MainDal.Common;

namespace ChannelVN.CMS.MainDal.Base.StreamItem
{
    public abstract class StreamItemDalBase
    {
        #region StreamItem      
        public bool InsertStreamItem(StreamItemEntity streamItem, ref long id)
        {
            const string commandText = "CMS_StreamItem_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Order", streamItem.Order);
                _db.AddParameter(cmd, "TypeId", streamItem.TypeId);
                _db.AddParameter(cmd, "TemplateId", streamItem.TemplateId);
                _db.AddParameter(cmd, "Status", streamItem.Status);
                _db.AddParameter(cmd, "CreatedDate", streamItem.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", streamItem.CreatedBy);
                _db.AddParameter(cmd, "PublishedDate", streamItem.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", streamItem.PublishedBy);
                _db.AddParameter(cmd, "DataJson", streamItem.DataJson);
                _db.AddParameter(cmd, "Mode", streamItem.Mode);
                _db.AddParameter(cmd, "Title", streamItem.Title);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStreamItem(StreamItemEntity streamItem)
        {

            const string commandText = "CMS_StreamItem_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id);
                _db.AddParameter(cmd, "Order", streamItem.Order);
                _db.AddParameter(cmd, "TypeId", streamItem.TypeId);
                _db.AddParameter(cmd, "TemplateId", streamItem.TemplateId);
                _db.AddParameter(cmd, "Status", streamItem.Status);                                
                _db.AddParameter(cmd, "DataJson", streamItem.DataJson);
                _db.AddParameter(cmd, "Mode", streamItem.Mode);
                _db.AddParameter(cmd, "Title", streamItem.Title);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteStreamItemById(long id)
        {
            const string commandText = "CMS_StreamItem_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public StreamItemEntity GetStreamItemById(long id)
        {
            const string commandText = "CMS_StreamItem_GetById";
            try
            {
                StreamItemEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<StreamItemEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public StreamItemDetailEntity GetNewsForStreamItemById(long newsId)
        {
            const string commandText = "CMS_News_GetNewsForStreamItemById";
            try
            {
                StreamItemDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId);
                data = _db.Get<StreamItemDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StreamItemEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_StreamItem_Search";
            try
            {
                List<StreamItemEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "TemplateId", templateId);                
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                
                data = _db.GetList<StreamItemEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StreamItemEntity> InitESAllStreamItem(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {           
            const string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM StreamItem T where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT T.*,
	                                        ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                        FROM StreamItem T                                         
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))                                        
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<StreamItemEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<StreamItemEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region StreamItemMobile      
        public bool InsertStreamItemMobile(StreamItemMobileEntity streamItem, ref long id)
        {
            const string commandText = "CMS_StreamItemMobile_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Order", streamItem.Order);
                _db.AddParameter(cmd, "TypeId", streamItem.TypeId);
                _db.AddParameter(cmd, "TemplateId", streamItem.TemplateId);
                _db.AddParameter(cmd, "Status", streamItem.Status);
                _db.AddParameter(cmd, "CreatedDate", streamItem.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", streamItem.CreatedBy);
                _db.AddParameter(cmd, "PublishedDate", streamItem.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", streamItem.PublishedBy);
                _db.AddParameter(cmd, "DataJson", streamItem.DataJson);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {

            const string commandText = "CMS_StreamItemMobile_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id);
                _db.AddParameter(cmd, "Order", streamItem.Order);
                _db.AddParameter(cmd, "TypeId", streamItem.TypeId);
                _db.AddParameter(cmd, "TemplateId", streamItem.TemplateId);
                _db.AddParameter(cmd, "Status", streamItem.Status);
                _db.AddParameter(cmd, "DataJson", streamItem.DataJson);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteStreamItemMobileById(long id)
        {
            const string commandText = "CMS_StreamItemMobile_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public StreamItemMobileEntity GetStreamItemMobileById(long id)
        {
            const string commandText = "CMS_StreamItemMobile_GetById";
            try
            {
                StreamItemMobileEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<StreamItemMobileEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StreamItemMobileEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_StreamItemMobile_Search";
            try
            {
                List<StreamItemMobileEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "TemplateId", templateId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<StreamItemMobileEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected StreamItemDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion        
    }
}
