﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsMaskOnline
{
    public class NewsMaskOnlineDal: NewsMaskOnlineDalBase
    {
        internal NewsMaskOnlineDal(MaskOnlineDb db)
            : base(db)
        {
        }
    }
}
