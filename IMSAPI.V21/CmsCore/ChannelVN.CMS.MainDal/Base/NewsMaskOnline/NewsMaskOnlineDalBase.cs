﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsMaskOnline
{
    public abstract class NewsMaskOnlineDalBase
    {
        public List<NewsMaskOnlineEntity> ListNewsMaskOnline(string keyword, DateTime fromDate, DateTime toDate,
                                                    int SourceID, int status, int pageIndex, int pageSize, int Mode = -1)
        {
            const string commandText = "CMS_Mask_NewsPublished_GetList";
            try
            {
                List<NewsMaskOnlineEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyWord", keyword);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "From", DBNull.Value);
                else
                    _db.AddParameter(cmd, "From", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "To", DBNull.Value);
                else
                    _db.AddParameter(cmd, "To", toDate);
                _db.AddParameter(cmd, "Mode", Mode);
                _db.AddParameter(cmd, "SourceID", SourceID);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsMaskOnlineEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly MaskOnlineDb _db;

        protected NewsMaskOnlineDalBase(MaskOnlineDb db)
        {
            _db = db;
        }

        protected MaskOnlineDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
