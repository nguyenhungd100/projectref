﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.PhotoGroup
{
    public class PhotoGroupDal : PhotoGroupDalBase
    {
        internal PhotoGroupDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
