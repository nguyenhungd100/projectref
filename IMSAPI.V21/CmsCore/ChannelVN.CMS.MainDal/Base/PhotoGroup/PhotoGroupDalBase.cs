﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.PhotoGroup;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.MainDal.Base.PhotoGroup
{
    public abstract class PhotoGroupDalBase
    {
        #region Update
        public bool InsertPhotoGroupDetailInPhotoTag(PhotoGroupDetailInPhotoTagEntity photo)
        {
            const string commandText = "CMS_PhotoGroupDetailInPhotoTag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoGroupDetailId", photo.PhotoGroupDetailId);
                _db.AddParameter(cmd, "PhotoTagId", photo.PhotoTagId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool InsertPhotoGroupDetail(PhotoGroupDetailEntity photo, ref int newPhotoId)
        {
            const string commandText = "CMS_PhotoGroupDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photo.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Title", photo.Title);
                _db.AddParameter(cmd, "ImageUrl", photo.ImageUrl);
                _db.AddParameter(cmd, "Avatar1", photo.Avatar1);
                _db.AddParameter(cmd, "Avatar2", photo.Avatar2);
                _db.AddParameter(cmd, "Avatar3", photo.Avatar3);
                _db.AddParameter(cmd, "Avatar4", photo.Avatar4);
                _db.AddParameter(cmd, "Avatar5", photo.Avatar5);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Description", photo.Description);
                _db.AddParameter(cmd, "RelatedLink", photo.RelatedLink);
                _db.AddParameter(cmd, "PhotoGroupId", photo.PhotoGroupId);
                _db.AddParameter(cmd, "DistributionDate", photo.DistributionDate);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "IsHot", photo.IsHot);
                _db.AddParameter(cmd, "CreatedBy", photo.CreatedBy);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);
                _db.AddParameter(cmd, "Tags", photo.Tags);
                _db.AddParameter(cmd, "ZoneIdList", photo.SecondZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                newPhotoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdatePhotoGroupDetail(PhotoGroupDetailEntity photo)
        {
            const string commandText = "CMS_PhotoGroupDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photo.Id);
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Title", photo.Title);
                _db.AddParameter(cmd, "ImageUrl", photo.ImageUrl);
                _db.AddParameter(cmd, "Avatar1", photo.Avatar1);
                _db.AddParameter(cmd, "Avatar2", photo.Avatar2);
                _db.AddParameter(cmd, "Avatar3", photo.Avatar3);
                _db.AddParameter(cmd, "Avatar4", photo.Avatar4);
                _db.AddParameter(cmd, "Avatar5", photo.Avatar5);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Description", photo.Description);
                _db.AddParameter(cmd, "RelatedLink", photo.RelatedLink);
                _db.AddParameter(cmd, "PhotoGroupId", photo.PhotoGroupId);
                _db.AddParameter(cmd, "DistributionDate", photo.DistributionDate);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "IsHot", photo.IsHot);
                _db.AddParameter(cmd, "ModifiedBy", photo.ModifiedBy);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);
                _db.AddParameter(cmd, "Tags", photo.Tags);
                _db.AddParameter(cmd, "ZoneIdList", photo.SecondZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePhotoGroupDetailStatus(int id, EnumPhotoGroupDetailStatus status)
        {
            const string commandText = "CMS_PhotoGroupDetail_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", (int)status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePhotoGroupDetail(int photoId)
        {
            const string commandText = "CMS_PhotoGroupDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePhotoGroupDetailInPhotoTag(int photoId)
        {
            const string commandText = "CMS_PhotoGroupDetailInPhotoTag_DeleteByGroupPhotoDetail";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoGroupDetailId", photoId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public PhotoGroupDetailEntity GetPhotoGroupDetailById(int photoId)
        {

            const string commandText = "CMS_PhotoGroupDetail_GetById";
            try
            {
                PhotoGroupDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);
                data = _db.Get<PhotoGroupDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoGroupDetailInZoneEntity> PhotoGroupDetailInZone_GetAll(int photogroupId)
        {

            const string commandText = "CMS_PhotoGroupDetailInZone_GetByPhotoGroupDetailId";
            try
            {
                List<PhotoGroupDetailInZoneEntity> data = new List<PhotoGroupDetailInZoneEntity>();
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photogroupId);
                data = _db.GetList<PhotoGroupDetailInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoGroupDetailEntity> SearchPhotoGroupDetail(string keyword, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, EnumPhotoGroupDetailStatus status, int isHot, int photoGroupId, int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "CMS_PhotoGroupDetail_Search";
            try
            {
                List<PhotoGroupDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "IsHot", isHot);
                _db.AddParameter(cmd, "PhotoGroupId", photoGroupId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoGroupDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoGroupDetailCountEntity> CountPhotoGroupDetail(string username)
        {

            const string commandText = "CMS_PhotoGroupDetail_GetPhotoCount";
            try
            {
                List<PhotoGroupDetailCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                data = _db.GetList<PhotoGroupDetailCountEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<PhotoGroupEntity> GetAllPhotoGroup(ref int totalRow)
        {

            const string commandText = "CMS_PhotoGroup_GetAll";
            try
            {
                List<PhotoGroupEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PhotoGroupEntity>(cmd);
                totalRow = data.Count;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected PhotoGroupDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
