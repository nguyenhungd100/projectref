﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.LogAnalytics;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.LogAnalytics
{
    public class LogAnalyticsDal : LogAnalyticsDalBase
    {
        internal LogAnalyticsDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
