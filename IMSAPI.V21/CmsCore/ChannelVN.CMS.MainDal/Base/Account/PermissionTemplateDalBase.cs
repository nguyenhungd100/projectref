﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.MainDal.Base.Account
{
    public abstract class PermissionTemplateDalBase
    {
        public List<PermissionTemplateEntity> GetAllPermissionTemplate()
        {
            const string commandText = "CMS_PermissionTemplate_GetAll";
            try
            {
                List<PermissionTemplateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PermissionTemplateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePermissionTemplate(PermissionTemplateEntity pm)
        {
            const string commandText = "CMS_PermissionTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", pm.Id);
                _db.AddParameter(cmd, "TemplateName", pm.TemplateName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePermissionTemplate(int templateId)
        {
            const string commandText = "CMS_PermissionTemplate_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemovePermissionTemplate_Detail(PermissionTemplateDetailEntity pd)
        {
            const string commandText = "CMS_PermissionTemplateDetail_Remove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", pd.TemplateId);
                _db.AddParameter(cmd, "PermissionId", pd.PermissionId);
                _db.AddParameter(cmd, "ZoneId", pd.ZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemoveAllPermissionTemplateDetail(int templateId)
        {
            const string commandText = "CMS_PermissionTemplateDetail_RemoveAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PermissionTemplateDetailEntity> GetListPermissionTemplateDetailByTemplateId(int templateId, bool isGetChildZone)
        {
            const string commandText = "CMS_PermissionTemplateDetail_GetListByPermissionTemplate";
            try
            {
                List<PermissionTemplateDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                //_db.AddParameter(cmd, "IsGetChildZone", isGetChildZone);
                data = _db.GetList<PermissionTemplateDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PermissionTemplateDetailEntity> GetPermisstionTemplateDetailById(int templateId)
        {
            const string commandText = "CMS_PermissionTemplateDetail_ById";
            try
            {
                List<PermissionTemplateDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);               
                data = _db.GetList<PermissionTemplateDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PermissionTemplateEntity GetPermissionTemplateById(int templateId)
        {
            const string commandText = "CMS_GetPermissionTemplateById";
            try
            {
                PermissionTemplateEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", templateId);
                data = _db.Get<PermissionTemplateEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PermissionTemplateDetailUpdate(int TemplateId, int PermissionId, string ZoneIds)
        {
            const string commandText = "CMS_PermissionTemplateDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", TemplateId);
                _db.AddParameter(cmd, "PermissionId", PermissionId);
                _db.AddParameter(cmd, "ZoneId", ZoneIds);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PermissionTemplateDetailUpdate(PermissionTemplateDetailEntity pt)
        {
            const string commandText = "CMS_PermissionTemplateDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", pt.TemplateId);
                _db.AddParameter(cmd, "PermissionId", pt.PermissionId);
                _db.AddParameter(cmd, "ZoneId", pt.ZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PermissionTemplateInsert(string TemplateName, ref int id)
        {
            const string commandText = "CMS_PermissionTemplate_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "TemplateName", TemplateName);
                var numberOfRow = cmd.ExecuteNonQuery();
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PermissionTemplateUpdate(int Id,string TemplateName)
        {
            const string commandText = "CMS_PermissionTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TemplateId", Id);
                _db.AddParameter(cmd, "TemplateName", TemplateName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected PermissionTemplateDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
