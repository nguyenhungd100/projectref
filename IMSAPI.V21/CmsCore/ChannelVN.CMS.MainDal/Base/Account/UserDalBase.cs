﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Account
{
    public abstract class UserDalBase
    {
        public bool AddnewUser(UserEntity user, ref int newUserId)
        {
            const string commandText = "CMS_User_Addnew";
            try
            {
                newUserId = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", user.UserName);
                _db.AddParameter(cmd, "Password", user.Password);
                _db.AddParameter(cmd, "FullName", user.FullName);
                _db.AddParameter(cmd, "Avatar", user.Avatar);
                _db.AddParameter(cmd, "Email", user.Email);
                _db.AddParameter(cmd, "Mobile", user.Mobile);
                _db.AddParameter(cmd, "IsFullPermission", user.IsFullPermission);
                _db.AddParameter(cmd, "IsFullZone", user.IsFullZone);
                _db.AddParameter(cmd, "Status", user.Status);
                _db.AddParameter(cmd, "Address", user.Address);
                _db.AddParameter(cmd, "BirthDay", user.Birthday);
                _db.AddParameter(cmd, "Description", user.Description);
                _db.AddParameter(cmd, "StaffCode", user.StaffCode);
                _db.AddParameter(cmd, "TelegramId", user.TelegramId);
                _db.AddParameter(cmd, "DepartmentId", user.DepartmentId);

                var numberOfRow = cmd.ExecuteNonQuery();

                newUserId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0 && newUserId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserById(UserEntity user)
        {
            const string commandText = "CMS_User_UpdateById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", user.Id);
                _db.AddParameter(cmd, "FullName", user.FullName);
                _db.AddParameter(cmd, "Avatar", user.Avatar);
                _db.AddParameter(cmd, "Email", user.Email);
                _db.AddParameter(cmd, "Mobile", user.Mobile);
                _db.AddParameter(cmd, "IsFullPermission", user.IsFullPermission);
                _db.AddParameter(cmd, "IsFullZone", user.IsFullZone);
                _db.AddParameter(cmd, "Status", user.Status);
                _db.AddParameter(cmd, "Address", user.Address);
                _db.AddParameter(cmd, "BirthDay", user.Birthday);
                _db.AddParameter(cmd, "Description", user.Description);
                _db.AddParameter(cmd, "IsRole", user.IsRole);
                _db.AddParameter(cmd, "IsSendOver", user.IsSendOver);
                _db.AddParameter(cmd, "StaffCode", user.StaffCode);
                _db.AddParameter(cmd, "TelegramId", user.TelegramId);
                _db.AddParameter(cmd, "DepartmentId", user.DepartmentId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteUserById(int id)
        {
            const string commandText = "CMS_User_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserAvatar(string accountName, string avatar)
        {
            const string commandText = "CMS_User_UpdateAvatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                _db.AddParameter(cmd, "Avatar", avatar);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTelegramId(string accountName, long telegramId)
        {
            const string commandText = "CMS_User_UpdateTelegramId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", accountName);
                if(telegramId<=0)
                    _db.AddParameter(cmd, "TelegramId", DBNull.Value);
                else
                    _db.AddParameter(cmd, "TelegramId", telegramId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserStatusByById(int id, int status)
        {
            const string commandText = "CMS_User_UpdateStatusByUserId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateUserPasswordByById(int id, string password)
        {
            const string commandText = "CMS_User_UpdatePasswordByUserId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Password", password);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateUserOTPByById(int id)
        {
            const string commandText = "CMS_User_ResetOTPByUserId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);                
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public UserEntity GetUserById(int id)
        {
            const string commandText = "CMS_User_GetUserById";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserByUsername(string username)
        {
            const string commandText = "CMS_User_GetUserByUsername";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public UserEntity GetUserByEmail(string email)
        {
            const string commandText = "CMS_User_GetUserByEmail";
            try
            {
                UserEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Email", email);
                data = _db.Get<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserEntity> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_User_SeachUser";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                data = _db.GetList<UserEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserEntity> InitESAllUser()
        {
            const string commandText = "CMS_User_InitAllUser";
            //const string commandText = @"SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		          //                              U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
            //                                    U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem, U.IsRole, U.IsSendOver
	           //                             FROM [User] AS U LEFT OUTER JOIN
            //                                    UserProfile AS P ON U.Id = P.UserId	";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText,true);            
                data = _db.GetList<UserEntity>(cmd);                

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            const string commandText = "CMS_User_GetUserByPermissionIdAndZoneList";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserEntity> GetUserByPermissionListAndZoneList(string permissionIds, string zoneIds)
        {
            const string commandText = "CMS_User_GetUserByPermissionListAndZoneList";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserEntity> GetUserFullPermission()
        {
            const string commandText = "CMS_User_GetUserFullPermission";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<UserStandardEntity> GetNormalUserByPermissionIdAndZoneId(int permissionId, int zoneId)
        {
            const string commandText = "CMS_User_GetNormalUserByPermissionIdAndZoneId";
            try
            {
                List<UserStandardEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<UserStandardEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region lấy danh sách user theo permissionId và ZoneId
        public List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            const string commandText = "CMS_User_GetListUserByPermissionAndZoneId";
            try
            {
                List<UserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<UserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public string GetOtpSecretKeyByUsername(string username)
        {
            const string commandText = "CMS_User_GetOtpSecretKeyByUsername";
            try
            {
                var data = "";
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                data = Utility.ConvertToString(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<UserSearchAutocompleteEntity> UserSearchAutocomplete(int ZoneId, string Keyword, bool isShowTKTS)
        {
            const string commandText = "CMS_User_SearchAutocomplete";
            try
            {
                List<UserSearchAutocompleteEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                _db.AddParameter(cmd, "Keyword", Keyword);
                _db.AddParameter(cmd, "IsShowTKTS", isShowTKTS);
                data = _db.GetList<UserSearchAutocompleteEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            const string commandText = "CMS_User_UpdateOtpSecretKeyForUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "OtpSecretKey", otpSecretKey);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UserPermission_Insert(int userId, int tempId)
        {
            const string commandText = "CMS_UserPermission_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "TemplateID", tempId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;
        
        protected UserDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
