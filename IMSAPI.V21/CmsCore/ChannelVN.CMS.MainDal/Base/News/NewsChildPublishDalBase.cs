﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsChildPublishDalBase
    {
        public bool InsertNewsChildPublish(NewsChildPublishEntity entity)
        {
            const string commandText = "CMS_NewsChildPublish_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteNewsChildPublish(long parentNewsId)
        {
            const string commandText = "CMS_NewsChildPublish_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsChildPublish(NewsChildPublishEntity entity)
        {
            const string commandText = "CMS_NewsChildPublish_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsChildPublishEntity> GetNewsChildPublishByNewsId(long newsParentId)
        {
            const string commandText = "CMS_NewsChildPublish_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", newsParentId);
                List<NewsChildPublishEntity> data = _db.GetList<NewsChildPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsChildPublishDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
