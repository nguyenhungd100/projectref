﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public class BoxInboundComponentEmbedDal : BoxInboundComponentEmbedDalBase
    {
        internal BoxInboundComponentEmbedDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
