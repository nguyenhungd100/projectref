﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsHistoryDalBase
    {
        public bool InsertNewsHistory(NewsHistoryEntity newsHistory)
        {
            const string commandText = "CMS_NewsHistory_InsertNewsHistory";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsHistory.NewsId);
                _db.AddParameter(cmd, "ActionName", newsHistory.ActionName);
                _db.AddParameter(cmd, "SentBy", newsHistory.SentBy);
                _db.AddParameter(cmd, "ReceivedBy", newsHistory.ReceivedBy);
                _db.AddParameter(cmd, "Status", newsHistory.Status);
                _db.AddParameter(cmd, "Description", newsHistory.Description);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsHistoryEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsHistory_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsHistoryEntity> data = _db.GetList<NewsHistoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsHistoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
