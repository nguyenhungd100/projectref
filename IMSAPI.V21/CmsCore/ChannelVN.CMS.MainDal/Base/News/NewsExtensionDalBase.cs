﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsExtensionDalBase
    {
        public NewsExtensionEntity GetValue(long newsId, int type)
        {
            const string commandText = "CMS_NewsExtension_GetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                NewsExtensionEntity data = _db.Get<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetMaxValue(long newsId, int type)
        {
            const string commandText = "CMS_NewsExtension_GetMaxValue";
            try
            {
                var maxValue = 1;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MaxValue", maxValue, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.ExecuteReader(cmd);
                maxValue = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return maxValue;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(long newsId, int type, string value)
        {
            const string commandText = "CMS_NewsExtension_SetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SendExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            var commandDelete = new StringBuilder("");
            try
            {
                for (int i = 0; i < newsExtension.Count; i++)
                {
                    commandDelete.AppendLine(string.Format(" DELETE from NewsExtension where NewsId={0} AND Type = {1}", newsExtension[i].NewsId, newsExtension[i].Type));
                }
                commandText.AppendLine(commandDelete.ToString());

                commandText.AppendLine(string.Format(" INSERT INTO [dbo].[NewsExtension]([NewsId],[Type],[Value])"));
                for (int i = 0; i < newsExtension.Count; i++)
                {
                    if (i == (newsExtension.Count - 1))
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, N'{2}' WHERE NOT EXISTS (select NewsId from NewsExtension where NewsId={0} AND Type = {1})", newsExtension[i].NewsId, newsExtension[i].Type, newsExtension[i].Value));
                    else {
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, N'{2}' WHERE NOT EXISTS (select NewsId from NewsExtension where NewsId={0} AND Type = {1})", newsExtension[i].NewsId, newsExtension[i].Type, newsExtension[i].Value));
                        commandText.AppendLine(" UNION ALL");
                    }
                }
                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow >= 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public bool ReSendExpertInNewsExtension(int expertId, int expireDate, string note)
        {
            const string commandText = "CMS_NewsExtension_ReSendExpert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ExpertId", expertId);
                _db.AddParameter(cmd, "RemindDate", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssZ"));
                _db.AddParameter(cmd, "ExpireDate", expireDate);
                _db.AddParameter(cmd, "Note", note);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ConfirmExpertInNewsExtension(NewsExtensionEntity newsExtension)
        {
            const string commandText = "CMS_NewsExtension_SetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsExtension.NewsId);
                _db.AddParameter(cmd, "Type", newsExtension.Type);
                _db.AddParameter(cmd, "Value", newsExtension.Value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReturnExpertInNewsExtension(List<NewsExtensionEntity> newsExtension)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            var commandDelete= new StringBuilder("");
            try
            {
                for (int i = 0; i < newsExtension.Count; i++)
                {
                    commandDelete.AppendLine(string.Format(" DELETE from NewsExtension where NewsId={0} AND Type = {1}", newsExtension[i].NewsId, newsExtension[i].Type));
                }

                commandText.AppendLine(commandDelete.ToString());

                commandText.AppendLine(string.Format(" INSERT INTO [dbo].[NewsExtension]([NewsId],[Type],[Value])"));
                for (int i = 0; i < newsExtension.Count; i++)
                {
                    if (i == (newsExtension.Count - 1))
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, N'{2}' WHERE NOT EXISTS (select NewsId from NewsExtension where NewsId={0} AND Type = {1})", newsExtension[i].NewsId, newsExtension[i].Type, newsExtension[i].Value));
                    else {
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, N'{2}' WHERE NOT EXISTS (select NewsId from NewsExtension where NewsId={0} AND Type = {1})", newsExtension[i].NewsId, newsExtension[i].Type, newsExtension[i].Value));
                        commandText.AppendLine(" UNION ALL");
                    }
                }
                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow >= 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExtension_ListNewsByExpertId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ExpertId", expertId);
                _db.AddParameter(cmd, "ExpertStatus", expertStatus);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if(fromDate!=DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExtension_ListNewsNotByExpert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicId", topicId);                
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                if (fromDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Account", account);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsAllByExpert(int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExtension_ListNewsAllByExpert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ExpertStatus", expertStatus);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetNewsExtensionByListIds(string ids)
        {
            const string commandText = "CMS_NewsExtension_GetNewsExtensionByListIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                
                _db.AddParameter(cmd, "NewsIds", ids);
                
                var data = _db.GetList<NewsExtensionEntity>(cmd);                

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsExtension_DeleteByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsExtension_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsExtensionEntity> data = _db.GetList<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_NewsExtension_GetByListNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                List<NewsExtensionEntity> data = _db.GetList<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsExtensionEntity> GetByTypeAndVaue(int type, string value)
        {
            const string commandText = "CMS_NewsExtension_GetByTypeAndValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                List<NewsExtensionEntity> data = _db.GetList<NewsExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsExtensionEntity> InitESAllNewsExtension(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM NewsExtension

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT T.*,
	                                        ROW_NUMBER() OVER(ORDER BY T.NewsId DESC) AS RowNumber 
                                        FROM NewsExtension T
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<NewsExtensionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsExtensionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsExtensionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
