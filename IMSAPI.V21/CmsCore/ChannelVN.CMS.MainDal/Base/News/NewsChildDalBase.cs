﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsChildDalBase
    {
        public List<NewsChildEntity> GetByNewsId(long parentNewsId, bool isGetContent)
        {
            const string commandText = "CMS_NewsChild_GetByNewsId";
            var isNewsChildPublish = Utility.ConvertToBoolean(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "NewsChild"));
            if (isNewsChildPublish == false)
                return null;
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                _db.AddParameter(cmd, "IsGetContent", isGetContent);
                List<NewsChildEntity> data = _db.GetList<NewsChildEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsChildEntity GetByNewsAndOrderId(long parentNewsId, int order)
        {
            const string commandText = "CMS_NewsChild_GetByNewsAndOrderId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                _db.AddParameter(cmd, "Order", order);
                NewsChildEntity data = _db.Get<NewsChildEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertNewsChild(NewsChildEntity entity)
        {
            const string commandText = "CMS_NewsChild_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsChild(NewsChildEntity entity)
        {
            const string commandText = "CMS_NewsChild_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNewsChild(long parentNewsId, int order)
        {
            const string commandText = "CMS_NewsChild_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                _db.AddParameter(cmd, "Order", order);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region News Child Version
        public bool InsertNewsChildVersion(NewsChildVersionEntity entity)
        {
            const string commandText = "CMS_NewsChildVersion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", entity.NewsVersionId);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsChildVersion(NewsChildVersionEntity entity)
        {
            const string commandText = "CMS_NewsChildVersion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", entity.NewsVersionId);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId, bool isGetContent = false)
        {
            const string commandText = "CMS_NewsChildVersion_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", newsId);
                _db.AddParameter(cmd, "IsGetContent", isGetContent);
                List<NewsChildVersionEntity> data = _db.GetList<NewsChildVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsChildVersionEntity GetNewsChildVersionByNewsAndOrderId(long newsVersionId, int order)
        {
            const string commandText = "CMS_NewsChildVersion_GetByNewsAndOrderId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", newsVersionId);
                _db.AddParameter(cmd, "Order", order);
                NewsChildVersionEntity data = _db.Get<NewsChildVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsChildDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
