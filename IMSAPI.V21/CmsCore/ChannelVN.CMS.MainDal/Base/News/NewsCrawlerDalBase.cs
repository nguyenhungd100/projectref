﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsCrawlerDalBase
    {
        // Lấy chi tiết một tin crawler
        public NewsCrawlerEntity GetDetailById(int id)
        {
            string commandText = "SELECT * FROM BookmarkAuto WHERE ID=" + id;
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                NewsCrawlerEntity data = _db.Get<NewsCrawlerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCrawlerEntity> GetListNewsCrawler(int category, int siteId, int hot, int pageIndex, int pageSize, ref int totalRow)
        {
            var parameters = string.Empty;

            if (hot == -1)
            {
                if (category == 0 && siteId == 0)
                {
                    parameters = "SELECT TOP " + pageSize +
                                 " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND ISNULL(BA.SohaStatus, 0)=0  AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                }
                else
                {
                    if (category != 0 && siteId == 0)
                    {
                        parameters = "SELECT TOP " + pageSize +
                                     " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                     category +
                                     " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";

                    }
                    else
                    {
                        if (category == 0 && siteId != 0)
                        {
                            parameters = "SELECT TOP " + pageSize +
                                         " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.Source=" +
                                         siteId +
                                         " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                        }
                        else
                        {
                            if (category != 0 && siteId != 0)
                            {
                                parameters = "SELECT TOP " + pageSize +
                                             " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                             category + " AND BA.Source=" + siteId +
                                             " AND  ISNULL(BA.SohaStatus, 0)=0 AND  BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                            }
                        }
                    }
                }
            }

            else
            {
                if (hot == 0)
                {
                    if (category == 0 && siteId == 0)
                    {
                        parameters = "SELECT TOP " + pageSize +
                                     " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth < 100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                    }
                    else
                    {
                        if (category != 0 && siteId == 0)
                        {
                            parameters = "SELECT TOP " + pageSize +
                                         " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                         category +
                                         " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth < 100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";

                        }
                        else
                        {
                            if (category == 0 && siteId != 0)
                            {
                                parameters = "SELECT TOP " + pageSize +
                                             " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.Source=" +
                                             siteId +
                                             " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth < 100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                            }
                            else
                            {
                                if (category != 0 && siteId != 0)
                                {
                                    parameters = "SELECT TOP " + pageSize +
                                                 " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                                 category + " AND BA.Source=" + siteId +
                                                 " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth < 100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (category == 0 && siteId == 0)
                    {
                        parameters = "SELECT TOP " + pageSize +
                                     " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth >100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                    }
                    else
                    {
                        if (category != 0 && siteId == 0)
                        {
                            parameters = "SELECT TOP " + pageSize +
                                         " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                         category +
                                         " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth >100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";

                        }
                        else
                        {
                            if (category == 0 && siteId != 0)
                            {
                                parameters = "SELECT TOP " + pageSize +
                                             " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.Source=" +
                                             siteId +
                                             " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth >100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                            }
                            else
                            {
                                if (category != 0 && siteId != 0)
                                {
                                    parameters = "SELECT TOP " + pageSize +
                                                 " ID,Source,CatID,NewsURL,Title,Sapo,Content,ContentProcess,CreateDate,Avatar,SiteName FROM BookmarkAuto AS BA INNER JOIN Site as S ON BA.Source =S.SiteID WHERE S.SourceSoHa =1 AND BA.CatID=" +
                                                 category + " AND BA.Source=" + siteId +
                                                 " AND  ISNULL(BA.SohaStatus, 0)=0 AND BA.AvatarWidth >100 AND BA.CodeStatus =1  ORDER BY BA.CreateDate DESC";
                                }
                            }
                        }
                    }
                }
            }

            string commandText = parameters;
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<NewsCrawlerEntity> data = _db.GetList<NewsCrawlerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool CheckExist(string siteLink)
        {
            string commandText = "SELECT Count(SiteID) FROM Site WHERE SiteLink=N'" + siteLink + "'";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }



        public bool InsertSite(string siteLink, string siteName)
        {
            string commandText = string.Empty;
            try
            {
                if (CheckExist(siteLink))
                {
                    commandText = "UPDATE Site SET SourceSoHa =1 WHERE  SiteLink =N'" + siteLink + "'";
                }
                else
                {
                    commandText = "INSERT INTO Site (SourceSoHa,SiteLink,SiteName,Status) VALUES (1,N'" + siteLink + "',N'" + siteName + "', 0)";
                }
                var cmd = _db.CreateCommand(commandText, false);
                var data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InvisibleSite(int id)
        {
            string commandText = "UPDATE Site SET SourceSoHa = NULL WHERE  SiteID =" + id;
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                var data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateIsMoveCms(int newsId)
        {
            string commandText = "UPDATE BookmarkAuto SET SohaStatus =1 WHERE ID =" + newsId;
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                var data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int newsId)
        {
            string commandText = "UPDATE BookmarkAuto SET SohaStatus =1 WHERE ID =" + newsId;
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                var data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CategoryCrawlerEntity> GetListCategoryCrawler()
        {
            const string commandText = "SELECT * FROM CategoryAuto";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<CategoryCrawlerEntity> data = _db.GetList<CategoryCrawlerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<SiteCrawlerEntity> GetListSiteCrawler()
        {
            const string commandText = "SELECT * FROM Site WHERE SourceSoHa =1";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<SiteCrawlerEntity> data = _db.GetList<SiteCrawlerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsCrawlerDb _db;

        protected NewsCrawlerDalBase(CmsCrawlerDb db)
        {
            _db = db;
        }

        protected CmsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
