﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsNotificationDalBase
    {
        public bool UpdateNotification(long newsId, string username, int newsStatus, bool isWarning, string message)
        {
            const string commandText = "CMS_NewsNotification_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "NewsStatus", newsStatus);
                _db.AddParameter(cmd, "IsWarning", isWarning);
                _db.AddParameter(cmd, "Message", message);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ReadNotification(long newsId, string username)
        {
            const string commandText = "CMS_NewsNotification_ReadNotification";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "UserName", username);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateLabel(long newsId, string username, int labelId)
        {
            const string commandText = "CMS_NewsNotification_UpdateLabel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "LabelId", labelId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListWithNotifyEntity> SearchNewsWithNotification(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, string listStatus, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchWithNotification";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "ListStatus", listStatus);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsInListWithNotifyEntity> data = _db.GetList<NewsInListWithNotifyEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                return null;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListWithNotifyEntity> SearchNewsWithNotificationV2(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int sortOrder, string listStatus, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchWithNotificationV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "ListStatus", listStatus);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsInListWithNotifyEntity> data = _db.GetList<NewsInListWithNotifyEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                return null;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsNotificationEntity> GetNewsNotificationByListNewsId(string listNewsId, string username)
        {
            const string commandText = "CMS_NewsNotification_GetByListNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", listNewsId);
                _db.AddParameter(cmd, "Username", username);
                List<NewsNotificationEntity> data = _db.GetList<NewsNotificationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                return null;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsNotificationEntity> GetUnReadNewsNotificationByUsername(string username, DateTime getFromDate, ref int unreadCount)
        {
            const string commandText = "CMS_NewsNotification_GetUnReadNotificationByUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UnReadCount", unreadCount, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                if (getFromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "GetFromDate", DBNull.Value);
                else _db.AddParameter(cmd, "GetFromDate", getFromDate);
                List<NewsNotificationEntity> data = _db.GetList<NewsNotificationEntity>(cmd);
                unreadCount = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                return null;
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsNotificationDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
