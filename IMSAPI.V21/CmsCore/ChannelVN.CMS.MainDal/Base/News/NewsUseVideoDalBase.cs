﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.MainDal.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsUseVideoDalBase
    {
        public bool Update(NewsUseVideoEntity videoEntity)
        {
            const string commandText = "CMS_NewsUseVideo_UpdateVideoInNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "VideoId", videoEntity.VideoId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "Priority", videoEntity.Priority);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsUseVideoEntity> GetAllNewsUseVideoByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsUseVideo_GetAllByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsUseVideoEntity> data = _db.GetList<NewsUseVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountVideoByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsUseVideo_CountVideoByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                SqlDataReader data = (SqlDataReader)_db.ExecuteReader(cmd);
                if (data.Read())
                {
                    return data.GetInt32(0);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int DeleteVideoByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsUseVideo_DeleteVideoByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                int data = _db.ExecuteNonQuery(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsVideoDb _db;

        protected NewsUseVideoDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
