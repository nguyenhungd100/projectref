﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using System.Data;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsSourceDalBase
    {
        /// <summary>
        /// Insert NewsSource 
        /// Last update: 06/05/2013
        /// </summary>
        /// <param name="newsSourceEntity"></param>
        /// <param name="newsSourceId"></param>
        /// <returns></returns>
        public bool Insert(NewsSourceEntity newsSourceEntity, ref int newsSourceId)
        {
            const string commandText = "CMS_NewsSource_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", newsSourceEntity.Name);
                _db.AddParameter(cmd, "SourceType", newsSourceEntity.SourceType);
                _db.AddParameter(cmd, "UnsignName", newsSourceEntity.UnsignName);
                _db.AddParameter(cmd, "Avatar", newsSourceEntity.Avatar);
                _db.AddParameter(cmd, "Url", newsSourceEntity.Url);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newsSourceId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// NewsSource Update
        /// </summary>
        /// <param name="newsSourceEntity"></param>
        /// <returns></returns>
        public bool Update(NewsSourceEntity newsSourceEntity)
        {
            const string commandText = "CMS_NewsSource_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", newsSourceEntity.Name);
                _db.AddParameter(cmd, "SourceType", newsSourceEntity.SourceType);
                _db.AddParameter(cmd, "UnsignName", newsSourceEntity.UnsignName);
                _db.AddParameter(cmd, "Avatar", newsSourceEntity.Avatar);
                _db.AddParameter(cmd, "Url", newsSourceEntity.Url);
                _db.AddParameter(cmd, "Id", newsSourceEntity.Id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// News Source Delete By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            const string commandText = "CMS_NewsSource_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public bool DeleteAllNewsBySource(long newsId)
        {
            const string commandText = "CMS_NewsBySource_DeleteAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public NewsSourceEntity GetById(int id)
        {
            const string commandText = "CMS_NewsSource_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                NewsSourceEntity data = _db.Get<NewsSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<NewsSourceEntity> GetAll(string keyword)
        {
            const string commandText = "CMS_NewsSource_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                List<NewsSourceEntity> data = _db.GetList<NewsSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<NewsBySourceEntity> GetListInNews(long newsId)
        {
            const string commandText = "CMS_NewsSource_GetListInNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsBySourceEntity> data = _db.GetList<NewsBySourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsSourceDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
