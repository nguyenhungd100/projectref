﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsLiveDalBase
    {
        #region Sets

        public bool Insert(NewsLiveEntity newsLiveEntity, ref long newsLiveId)
        {
            const string commandText = "CMS_NewsLive_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsLiveEntity.NewsId);
                _db.AddParameter(cmd, "Title", newsLiveEntity.Title);
                _db.AddParameter(cmd, "Content", newsLiveEntity.Content);
                _db.AddParameter(cmd, "CreatedBy", newsLiveEntity.CreatedBy);
                _db.AddParameter(cmd, "PublishedBy", newsLiveEntity.PublishedBy);
                _db.AddParameter(cmd, "EditedBy", newsLiveEntity.EditedBy);
                _db.AddParameter(cmd, "Status", newsLiveEntity.Status);
                _db.AddParameter(cmd, "Priority", newsLiveEntity.Priority);
                _db.AddParameter(cmd, "PublishedDateNumber", newsLiveEntity.PublishedDateNumber);
                _db.AddParameter(cmd, "Avatar", newsLiveEntity.Avatar);
                _db.AddParameter(cmd, "EventTime", newsLiveEntity.EventTime);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newsLiveId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(NewsLiveEntity newsLiveEntity)
        {
            const string commandText = "CMS_NewsLive_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsLiveEntity.Id);
                _db.AddParameter(cmd, "NewsId", newsLiveEntity.NewsId);
                _db.AddParameter(cmd, "Title", newsLiveEntity.Title);
                _db.AddParameter(cmd, "Content", newsLiveEntity.Content);
                _db.AddParameter(cmd, "CreatedBy", newsLiveEntity.CreatedBy);
                _db.AddParameter(cmd, "PublishedBy", newsLiveEntity.PublishedBy);
                _db.AddParameter(cmd, "EditedBy", newsLiveEntity.EditedBy);
                _db.AddParameter(cmd, "Status", newsLiveEntity.Status);
                _db.AddParameter(cmd, "Priority", newsLiveEntity.Priority);
                _db.AddParameter(cmd, "PublishedDateNumber", newsLiveEntity.PublishedDateNumber);
                _db.AddParameter(cmd, "Avatar", newsLiveEntity.Avatar);
                _db.AddParameter(cmd, "EventTime", newsLiveEntity.EventTime);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long id)
        {
            const string commandText = "CMS_NewsLive_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatus(long id, int status, string publishedBy, string editedBy)
        {
            const string commandText = "CMS_NewsLive_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                _db.AddParameter(cmd, "EditedBy", editedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateContent(long id, string content)
        {
            const string commandText = "CMS_NewsLive_UpdateContent";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Content", content);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateSettings(NewsLiveSettingsEntity newsLiveSettings)
        {
            const string commandText = "CMS_NewsLiveSettings_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsLiveSettings.NewsId);
                _db.AddParameter(cmd, "IsFinished", newsLiveSettings.IsFinished);
                _db.AddParameter(cmd, "ShowTime", newsLiveSettings.ShowTime);
                _db.AddParameter(cmd, "ShowAuthor", newsLiveSettings.ShowAuthor);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool AddAuthor(NewsLiveAuthorEntity newsLiveAuthorEntity)
        {
            const string commandText = "CMS_NewsLive_AddAuthor";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsLiveAuthorEntity.NewsId);
                _db.AddParameter(cmd, "AuthorName", newsLiveAuthorEntity.AuthorName);
                _db.AddParameter(cmd, "Note", newsLiveAuthorEntity.Note);
                _db.AddParameter(cmd, "AuthorId", newsLiveAuthorEntity.AuthorId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteAuthor(long newsId, int authorId)
        {
            const string commandText = "CMS_NewsLive_DeleteAuthor";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "AuthorId", authorId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Gets

        public List<NewsLiveEntity> GetAll(long newsId, int status, int order)
        {
            const string commandText = "CMS_NewsLive_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                List<NewsLiveEntity> data = _db.GetList<NewsLiveEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsLiveEntity> GetPaging(long newsId, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsLive_GetPaging";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsLiveEntity> data = _db.GetList<NewsLiveEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsLiveEntity GetById(long id)
        {
            const string commandText = "CMS_NewsLive_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                NewsLiveEntity data = _db.Get<NewsLiveEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsLiveAuthorEntity> GetAuthorList(long newsId)
        {
            const string commandText = "CMS_NewsLive_GetAuthorList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsLiveAuthorEntity> data = _db.GetList<NewsLiveAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsLiveSettingsEntity GetSettings(long newsId)
        {
            const string commandText = "CMS_NewsLiveSettings_GetDetail";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                NewsLiveSettingsEntity data = _db.Get<NewsLiveSettingsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsLiveDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
