﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsChildVersionDalBase
    {
        public List<NewsChildVersionEntity> GetByNewsId(long parentNewsVersionId, bool isGetContent)
        {
            const string commandText = "CMS_NewsChildVersion_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", parentNewsVersionId);
                _db.AddParameter(cmd, "IsGetContent", isGetContent);
                List<NewsChildVersionEntity> data = _db.GetList<NewsChildVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsChildVersionEntity GetByNewsIdAndOrder(long parentNewsId, int order, string userDoAction)
        {
            const string commandText = "CMS_NewsChildVersion_GetByNewsIdAndOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                NewsChildVersionEntity data = _db.Get<NewsChildVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Remove(long parentNewsId, int order, string userDoAction)
        {
            const string commandText = "CMS_NewsChildVersion_Remove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentNewsId", parentNewsId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Remove(long newsVersionId, int order)
        {
            const string commandText = "CMS_NewsChildVersion_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", newsVersionId);
                _db.AddParameter(cmd, "Order", order);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(long parentNewsId, ref int order, string body, ref long newsVersionId, string userDoAction)
        {
            const string commandText = "CMS_NewsChildVersion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Order", order, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsVersionId", newsVersionId, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsParentId", parentNewsId);
                _db.AddParameter(cmd, "Body", body);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                order = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                newsVersionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(NewsChildVersionEntity entity)
        {
            const string commandText = "CMS_NewsChildVersion_UpdateV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsParentId", entity.NewsParentId);
                _db.AddParameter(cmd, "Title", entity.Title);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "Order", entity.Order);
                _db.AddParameter(cmd, "NewsVersionId", entity.NewsVersionId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected NewsChildVersionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
