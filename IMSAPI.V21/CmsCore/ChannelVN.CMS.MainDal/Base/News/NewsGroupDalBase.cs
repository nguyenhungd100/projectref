﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;
using System.Collections.Generic;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsGroupDalBase
    {
        public NewsGroupEntity GetNewsGroupById(int id)
        {
            const string commandText = "CMS_GroupNews_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", id);
                NewsGroupEntity data = _db.Get<NewsGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostRead(int groupNewsId, int withinDays, int rootZoneId, int excludeLastDays)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostRead";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "RootZoneId", rootZoneId);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostReadByZone(int groupNewsId, int withinDays, int excludeLastDays, int zoneId, int itemCount)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostReadByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                _db.AddParameter(cmd, "TopNews", itemCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostReadV2(int groupNewsId, int withinDays, int excludeLastDays, int itemCount)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostReadV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                _db.AddParameter(cmd, "TopNews", itemCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostComment(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "RootZoneId", rootZoneId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostReadDaily(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostReadDaily";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "RootZoneId", rootZoneId);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostReadHourly(int groupNewsId, int rootZoneId, int excludeLastDays)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostReadHourly";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "RootZoneId", rootZoneId);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostCommentByZone(int groupNewsId, int withinDays, int rootZoneId, int objectType, int excludeLastDays, int topNews)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostCommentByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "ZoneId", rootZoneId);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                _db.AddParameter(cmd, "TopNews", topNews);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostCommentV2(int groupNewsId, int withinDays, int objectType, int excludeLastDays, int topNews)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostCommentV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "WithinDays", withinDays);
                _db.AddParameter(cmd, "ObjectType", objectType);
                _db.AddParameter(cmd, "ExcludeLastDays", excludeLastDays);
                _db.AddParameter(cmd, "TopNews", topNews);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsGroupDetailEntity> UpdateTopMostReadV3(int groupNewsId, int zoneId, int withinHours, int totalRow)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostReadV3";
            try
            {
                List<NewsGroupDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "WithinHours", withinHours);
                _db.AddParameter(cmd, "TotalRow", totalRow);
                data = _db.GetList<NewsGroupDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopMostLikeShare(int groupNewsId, int zoneId, string lstNewsId, string likeShareCountList)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopMostLikeShare";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "AddNewsIds", lstNewsId);
                _db.AddParameter(cmd, "LikeShareCountList", likeShareCountList);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopCommentCountInDay(int groupNewsId, string lstNewsId, string lstCommentCount)
        {
            const string commandText = "CMS_GroupNewsDetail_UpdateTopCommentCountInDay";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GroupNewsId", groupNewsId);
                _db.AddParameter(cmd, "AddNewsIds", lstNewsId);
                _db.AddParameter(cmd, "AddCommentCounts", lstCommentCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsGroupDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
