﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public class NewsChildDal : NewsChildDalBase
    {
        internal NewsChildDal(CmsMainDb db)
            : base(db)
        {
        }
    }}
