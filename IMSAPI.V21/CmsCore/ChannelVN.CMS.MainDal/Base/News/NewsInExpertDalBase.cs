﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsInExpertDalBase
    {  
        public bool SendNewsInExpert(List<NewsInExpertEntity> newsExpert)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            var commandDelete = new StringBuilder("");
            try
            {
                for (int i = 0; i < newsExpert.Count; i++)
                {
                    commandDelete.AppendLine(string.Format(" DELETE from NewsExpert where NewsId={0} AND Status<>1", newsExpert[i].NewsId, newsExpert[i].ExpertId));
                }
                commandText.AppendLine(commandDelete.ToString());

                commandText.AppendLine(string.Format(" INSERT INTO [dbo].[NewsExpert]([NewsId],[ExpertId],[Status],[SendDate],[Note])"));
               
                for (int i = 0; i < newsExpert.Count; i++)
                {
                    if (i == (newsExpert.Count - 1))
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, {2}, '{3}', N'{4}' WHERE NOT EXISTS (select NewsId from NewsExpert where NewsId={0})", newsExpert[i].NewsId, newsExpert[i].ExpertId, newsExpert[i].Status, newsExpert[i].SendDate, newsExpert[i].Note));
                    else {
                        commandText.AppendLine(string.Format(" SELECT {0}, {1}, {2}, '{3}', N'{4}' WHERE NOT EXISTS (select NewsId from NewsExpert where NewsId={0})", newsExpert[i].NewsId, newsExpert[i].ExpertId, newsExpert[i].Status, newsExpert[i].SendDate, newsExpert[i].Note));
                        commandText.AppendLine(" UNION ALL");
                    }
                }
                
                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public bool ReSendNewsInExpert(int expertId, int expireDate, string note="", long newsId=0)
        {            
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            try
            {
                if (newsId > 0)
                    commandText.AppendLine(string.Format(" UPDATE NewsExpert set SendDate='{0}', Note=N'{1}' WHERE ExpertId={2} and Status=0  and SendDate <= DATEADD(hh, DateDiff(hh, 0, GETDATE()) - {3}, GETDATE()) and NewsId={4}", DateTime.Now, note, expertId, expireDate, newsId));
                else
                    commandText.AppendLine(string.Format(" UPDATE NewsExpert set SendDate='{0}', Note=N'{1}' WHERE ExpertId={2} and Status=0  and SendDate <= DATEADD(hh, DateDiff(hh, 0, GETDATE()) - {3}, GETDATE())", DateTime.Now, note, expertId, expireDate));

                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public bool RecoverNewsInExpert(string listNewsId)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            try
            {
                commandText.AppendLine(" DELETE NewsExpert WHERE NewsId in("+ listNewsId + ") AND Status = 0");

                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public bool ConfirmNewsInExpert(NewsInExpertEntity newsExpert)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            try
            {
                commandText.AppendLine(string.Format(" UPDATE NewsExpert SET [Status]={0}, [ConfirmDate]='{1}' WHERE NewsId={2} AND ExpertId = {3}", newsExpert.Status, newsExpert.ConfirmDate, newsExpert.NewsId, newsExpert.ExpertId));
                
                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public bool ReturnNewsInExpert(NewsInExpertEntity newsExpert)
        {
            var commandText = new StringBuilder("BEGIN TRANSACTION");
            try
            {
                commandText.AppendLine(string.Format(" UPDATE [dbo].[NewsExpert] SET [Status]={0}, [ReturnDate]='{1}', [Note]=N'{2}' WHERE NewsId={3} AND ExpertId = {4}", newsExpert.Status, newsExpert.ReturnDate, newsExpert.Note, newsExpert.NewsId, newsExpert.ExpertId));

                commandText.AppendLine(" COMMIT TRANSACTION");

                var cmd = _db.CreateCommand(commandText.ToString(), false);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}: {1}", "Exception", ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertByExpertId(int expertId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExpert_ListNewsByExpertId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ExpertId", expertId);
                _db.AddParameter(cmd, "ExpertStatus", expertStatus);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if(fromDate!=DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertNotByExpert(string account, int topicId, int zoneId, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExpert_ListNewsNotByExpert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicId", topicId);                
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                if (fromDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Account", account);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsSimpleExpertEntity> ListNewsInExpertAllByExpert(int expertId, int topicId, int expertStatus, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsExpert_ListNewsAllByExpert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ExpertId", expertId);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "ExpertStatus", expertStatus);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "FromDate", fromDate);
                else
                    _db.AddParameter(cmd, "FromDate", "");
                if (toDate != DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", "");
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsSimpleExpertEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInExpertEntity> GetNewsExpertByIds(string ids)
        {
            const string commandText = "CMS_NewsExpert_GetNewsExpertByListIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "NewsIds", ids);

                var data = _db.GetList<NewsInExpertEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }            
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsInExpertDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
