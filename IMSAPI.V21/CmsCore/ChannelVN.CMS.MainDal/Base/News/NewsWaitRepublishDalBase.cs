﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsWaitRepublishDalBase
    {
        public bool InsertNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                       string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_NewsWaitRepublish_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsWaitRepublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_NewsWaitRepublish_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);

                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToProcessed(long id, string lastModidiedBy)
        {
            const string commandText = "CMS_NewsWaitRepublish_ChangeStatusToProcessed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModidiedBy", lastModidiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsWaitRepublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsWaitRepublish_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsEntity> data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetNewsWaitRepublishById(long id)
        {
            const string commandText = "CMS_NewsWaitRepublish_GetNewsById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                NewsEntity data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int NewsWaitRepublishCountDown(string username, string zoneIds, int filterFieldForUsername, int type)
        {
            const string commandText = "CMS_NewsWaitRepublish_CountDown";
            try
            {
                int total = 0;
                var cmd = _db.CreateCommand(commandText, true);
                 _db.AddParameter(cmd, "Username", username);
                                         _db.AddParameter(cmd, "ZoneIds", zoneIds);
                                         _db.AddParameter(cmd, "Type", type);
                                         _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "TotalRow", total,ParameterDirection.Output);
                _db.ExecuteReader(cmd);
                total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return total;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsWaitRepublishDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
