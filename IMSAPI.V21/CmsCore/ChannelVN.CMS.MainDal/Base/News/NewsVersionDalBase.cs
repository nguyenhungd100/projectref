﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsVersionDalBase
    {
        public bool UpdateVersion(NewsEntity news, string listOfZoneId, string createBy)
        {
            const string commandText = "CMS_NewsVersion_UpdateVersion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", news.Id);
                _db.AddParameter(cmd, "ListZoneId", listOfZoneId);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDate", DateTime.Now);
                else _db.AddParameter(cmd, "CreatedDate", news.CreatedDate);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                else _db.AddParameter(cmd, "LastModifiedDate", news.LastModifiedDate);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "LastReceiver", news.LastReceiver);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "CreateVersionBy", createBy);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "ShortTitle", news.ShortTitle);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ReleaseVersion(long newsId, string lastModifiedBy)
        {
            const string commandText = "CMS_NewsVersion_ReleaseVersion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ReleaseFirstVersion(long newsId, string lastModifiedBy)
        {
            const string commandText = "CMS_NewsVersion_ReleaseFirstVersion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RemoveEditingVersion(long newsId, string lastModifiedBy)
        {
            const string commandText = "CMS_NewsVersion_RemoveEditingVersion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool CreateVersionForExistsNews(long newsId, string createdBy, ref long newsVersionId)
        {
            const string commandText = "CMS_NewsVersion_CreateVersionForExistsNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsVersionId", newsVersionId, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newsVersionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsVersionEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsVersion_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsVersionEntity> data = _db.GetList<NewsVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsVersionWithSimpleFieldsEntity> GetListVersionByNewsId(long newsId, string lastModifiedBy)
        {
            const string commandText = "CMS_NewsVersion_GetListVersionByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                List<NewsVersionWithSimpleFieldsEntity> data = _db.GetList<NewsVersionWithSimpleFieldsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsVersionEntity GetById(long id)
        {
            const string commandText = "CMS_NewsVersion_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                NewsVersionEntity data = _db.Get<NewsVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsVersionEntity GetLastestVersionByNewsId(long newsId, string lastModifiedBy)
        {
            const string commandText = "CMS_NewsVersion_GetLastestVersionByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                NewsVersionEntity data = _db.Get<NewsVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "CMS_NewsVersion_GetLastestVersionByNewsId: " + ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsVersionEntity GetNewsVersionByNewsIdAndCreatedBy(long newsId, string createdBy)
        {
            const string commandText = "CMS_NewsVersion_GetNewsVersionByNewsIdAndCreatedBy";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                _db.AddParameter(cmd, "createdBy", createdBy);
                NewsVersionEntity data = _db.Get<NewsVersionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "CMS_NewsVersion_GetNewsVersionByNewsIdAndCreatedBy: " + ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsVersionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
