﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsUsePhotoDalBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="photoPublishedIds"></param>
        /// <returns></returns>
        public bool UpdatePhotoPublishedInNews(long newsId, string photoPublishedIds)
        {
            const string commandText = "CMS_NewsUsePhoto_UpdatePhotoPublishedInNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "PhotoPublishedIds", photoPublishedIds);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<NewsUsePhotoEntity> GetAllNewsUsePhotoByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsUsePhoto_GetAllByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsUsePhotoEntity> data = _db.GetList<NewsUsePhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public int CountPhotoPublishedByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsUsePhoto_CountPhotoPublishedByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                SqlDataReader data = (SqlDataReader)_db.ExecuteReader(cmd);
                if (data.Read())
                {
                    return data.GetInt32(0);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsUsePhotoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
