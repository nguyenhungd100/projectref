﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsEmbedBoxDalBase
    {
        public List<NewsEmbedBoxListEntity> GetListNewsEmbedBox(int type, int status)
        {
            const string commandText = "CMS_BoxNews_GetListByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                _db.AddParameter(cmd, "Status", Convert.ToInt32(status));
                List<NewsEmbedBoxListEntity> data = _db.GetList<NewsEmbedBoxListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(NewsEmbebBoxEntity newsEmbebBox)
        {
            const string commandText = "CMS_BoxNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsEmbebBox.News_ID);
                _db.AddParameter(cmd, "ThuTu", newsEmbebBox.ThuTu);
                _db.AddParameter(cmd, "Type", newsEmbebBox.Type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Update(string listNewsId, int type)
        {
            const string commandText = "CMS_BoxNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listNewsId);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public void UpdateStatus(string listOfBoxNewsId, int type, int status)
        {
            const string commandText = "CMS_BoxNews_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfBoxNewsId", listOfBoxNewsId);
                _db.AddParameter(cmd, "Type", Convert.ToInt32(type));
                _db.AddParameter(cmd, "Status", Convert.ToInt32(status));
                var data = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public void Delete(long newsId, int type)
        {
            const string commandText = "CMS_BoxNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsEmbedBoxDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
