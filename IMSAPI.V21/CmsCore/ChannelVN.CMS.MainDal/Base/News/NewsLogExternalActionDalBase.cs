﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsLogExternalActionDalBase
    {
        public List<NewsLogExternalActionEntity> GetByNewsIdAndActionType(long newsId, int actionType)
        {
            const string commandText = "CMS_NewsLogExternalAction_GetByNewsIdAndActionType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ActionType", actionType);
                List<NewsLogExternalActionEntity> data = _db.GetList<NewsLogExternalActionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(NewsLogExternalActionEntity newsLogExternalAction)
        {
            const string commandText = "CMS_NewsLogExternalAction_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsLogExternalAction.NewsId);
                _db.AddParameter(cmd, "ExternalActionType", newsLogExternalAction.ExternalActionType);
                _db.AddParameter(cmd, "ProcessedBy", newsLogExternalAction.ProcessedBy);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsLogExternalActionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
