﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsPrDalBase
    {
        public NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsPr_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                NewsPrEntity data = _db.Get<NewsPrEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsPrCurrentDateEntity> GetListNewsPrCurrentDate(DateTime Date, bool UnPublish, int PageIndex, int PageSize, ref int TotalRows)
        {
            const string commandText = "CMS_NewsPrCurrentDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", TotalRows);
                _db.AddParameter(cmd, "Date", Date < Constants.MinDateTime ? DateTime.Now : Date);
                _db.AddParameter(cmd, "UnPublish", UnPublish);
                _db.AddParameter(cmd, "PageIndex", PageIndex);
                _db.AddParameter(cmd, "PageSize", PageSize);
                var data = _db.GetList<NewsPrCurrentDateEntity>(cmd);
                TotalRows =  Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsPrEntity GetNewsPrByContentId(long contentId)
        {
            const string commandText = "CMS_NewsPr_GetByContentId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ContentId", contentId);
                NewsPrEntity data = _db.Get<NewsPrEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RecieveNewsPrIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            const string commandText = "CMS_NewsPr_RecieveNewsPrIntoCms";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPrInfo.NewsId);
                _db.AddParameter(cmd, "ContentId", newsPrInfo.ContentId);
                _db.AddParameter(cmd, "DistributionId", newsPrInfo.DistributionId);
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);
                _db.AddParameter(cmd, "IsFocus", newsPrInfo.IsFocus);
                _db.AddParameter(cmd, "Duration", newsPrInfo.Duration);
                if (newsPrInfo.DistributionDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", newsPrInfo.DistributionDate);
                else _db.AddParameter(cmd, "DistributionDate", DateTime.Now);
                if (newsPrInfo.PublishDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "PublishDate", newsPrInfo.PublishDate);
                else _db.AddParameter(cmd, "PublishDate", DateTime.Now);
                _db.AddParameter(cmd, "ExpireDate", newsPrInfo.ExpireDate);                

                _db.AddParameter(cmd, "CreatedBy", newsInfo.CreatedBy);
                _db.AddParameter(cmd, "ZoneId", newsInfo.ZoneId);
                _db.AddParameter(cmd, "Title", newsInfo.Title);
                _db.AddParameter(cmd, "SubTitle", newsInfo.SubTitle);
                _db.AddParameter(cmd, "Sapo", newsInfo.Sapo);
                _db.AddParameter(cmd, "Body", newsInfo.Body);
                _db.AddParameter(cmd, "Avatar", newsInfo.Avatar);                
                _db.AddParameter(cmd, "Avatar5", newsInfo.Avatar5);
                _db.AddParameter(cmd, "AvatarDesc", newsInfo.AvatarDesc);
                _db.AddParameter(cmd, "Source", newsInfo.Source);
                _db.AddParameter(cmd, "Author", newsInfo.Author);
                _db.AddParameter(cmd, "WordCount", newsInfo.WordCount);
                _db.AddParameter(cmd, "IsPr", newsInfo.IsPr);
                _db.AddParameter(cmd, "PrPosition", newsInfo.PrPosition);
                _db.AddParameter(cmd, "AdStore", newsInfo.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", newsInfo.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", newsInfo.PrBookingNumber);
                _db.AddParameter(cmd, "Url", newsInfo.Url);
                _db.AddParameter(cmd, "LocationType", newsInfo.LocationType);
                _db.AddParameter(cmd, "Status", newsInfo.Status);
                _db.AddParameter(cmd, "NewsRelation", newsInfo.NewsRelation);

                _db.AddParameter(cmd, "ListNewsRelationId", listNewsRelationId);
                _db.AddParameter(cmd, "TagIdList", newsInfo.TagItem);
                _db.AddParameter(cmd, "BookingId", newsPrInfo.BookingId);
                _db.AddParameter(cmd, "DisplayStyle", newsInfo.DisplayStyle);
                _db.AddParameter(cmd, "Price", newsInfo.Price);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RecieveNewsPrV2IntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            const string commandText = "CMS_NewsPr_RecieveNewsPrV2IntoCms";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPrInfo.NewsId);
                _db.AddParameter(cmd, "ContentId", newsPrInfo.ContentId);
                _db.AddParameter(cmd, "DistributionId", newsPrInfo.DistributionId);
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);
                _db.AddParameter(cmd, "IsFocus", newsPrInfo.IsFocus);
                _db.AddParameter(cmd, "Duration", newsPrInfo.Duration);

                if (newsPrInfo.DistributionDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", newsPrInfo.DistributionDate);
                else _db.AddParameter(cmd, "DistributionDate", DateTime.Now);
                if (newsPrInfo.PublishDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "PublishDate", newsPrInfo.PublishDate);
                else _db.AddParameter(cmd, "PublishDate", DateTime.Now);
                if (newsPrInfo.ExpireDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpireDate", newsPrInfo.ExpireDate);
                else _db.AddParameter(cmd, "ExpireDate", newsPrInfo.ExpireDate);

                _db.AddParameter(cmd, "CreatedBy", newsInfo.CreatedBy);
                _db.AddParameter(cmd, "ZoneId", newsInfo.ZoneId);
                _db.AddParameter(cmd, "Title", newsInfo.Title);
                _db.AddParameter(cmd, "SubTitle", newsInfo.SubTitle);
                _db.AddParameter(cmd, "Sapo", newsInfo.Sapo);
                _db.AddParameter(cmd, "Body", newsInfo.Body);
                _db.AddParameter(cmd, "Avatar", newsInfo.Avatar);
                _db.AddParameter(cmd, "Avatar2", newsInfo.Avatar2);
                _db.AddParameter(cmd, "Avatar3", newsInfo.Avatar3);
                _db.AddParameter(cmd, "Avatar4", newsInfo.Avatar4);
                _db.AddParameter(cmd, "Avatar5", newsInfo.Avatar5);
                _db.AddParameter(cmd, "AvatarDesc", newsInfo.AvatarDesc);
                _db.AddParameter(cmd, "Source", newsInfo.Source);
                _db.AddParameter(cmd, "Author", newsInfo.Author);
                _db.AddParameter(cmd, "WordCount", newsInfo.WordCount);
                _db.AddParameter(cmd, "IsPr", newsInfo.IsPr);
                _db.AddParameter(cmd, "PrPosition", newsInfo.PrPosition);
                _db.AddParameter(cmd, "AdStore", newsInfo.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", newsInfo.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", newsInfo.PrBookingNumber);
                _db.AddParameter(cmd, "Url", newsInfo.Url);
                _db.AddParameter(cmd, "LocationType", newsInfo.LocationType);
                _db.AddParameter(cmd, "Status", newsInfo.Status);
                _db.AddParameter(cmd, "NewsRelation", newsInfo.NewsRelation);

                _db.AddParameter(cmd, "ListNewsRelationId", listNewsRelationId);
                _db.AddParameter(cmd, "TagIdList", newsInfo.TagItem);
                _db.AddParameter(cmd, "BookingId", newsPrInfo.BookingId);
                _db.AddParameter(cmd, "DisplayStyle", newsInfo.DisplayStyle);
                _db.AddParameter(cmd, "Price", newsInfo.Price);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsPrInfo(NewsPrEntity newsPrInfo)
        {
            const string commandText = "CMS_NewsPr_UpdateNewsPrInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsPrInfo.Id);
                _db.AddParameter(cmd, "DistributionDate", newsPrInfo.DistributionDate);
                _db.AddParameter(cmd, "PublishDate", newsPrInfo.PublishDate);
                _db.AddParameter(cmd, "ExpireDate", newsPrInfo.ExpireDate);
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);
                _db.AddParameter(cmd, "IsFocus", newsPrInfo.IsFocus);
                _db.AddParameter(cmd, "Duration", newsPrInfo.Duration);
                _db.AddParameter(cmd, "ViewPlusBannerId", newsPrInfo.ViewPlusBannerId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsPrV2Info(NewsPrEntity newsPrInfo)
        {
            const string commandText = "CMS_NewsPr_UpdateNewsPrV2Info";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPrInfo.NewsId);
                _db.AddParameter(cmd, "DistributionDate", newsPrInfo.DistributionDate);                               
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);               
                _db.AddParameter(cmd, "ZoneId", newsPrInfo.ZoneId);                
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsPrV2Info_2(NewsPrEntity newsPrInfo)
        {
            const string commandText = "CMS_NewsPr_UpdateNewsPrV2Info_2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPrInfo.NewsId);
                _db.AddParameter(cmd, "DistributionDate", newsPrInfo.DistributionDate);
                _db.AddParameter(cmd, "PublishDate", newsPrInfo.PublishDate);
                _db.AddParameter(cmd, "ExpireDate", newsPrInfo.ExpireDate);
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);
                _db.AddParameter(cmd, "ZoneId", newsPrInfo.ZoneId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPrEntity GetNewsPrByBookingId(long bookingId)
        {
            const string commandText = "CMS_NewsPr_GetByBookingId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", bookingId);
                NewsPrEntity data = _db.Get<NewsPrEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RecieveNewsPrRetailIntoCms(NewsEntity newsInfo, NewsPrEntity newsPrInfo, string listNewsRelationId)
        {
            const string commandText = "CMS_NewsPr_RecieveNewsPrRetailIntoCms";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPrInfo.NewsId);
                _db.AddParameter(cmd, "ContentId", newsPrInfo.ContentId);
                _db.AddParameter(cmd, "DistributionId", newsPrInfo.DistributionId);
                _db.AddParameter(cmd, "Mode", newsPrInfo.Mode);
                _db.AddParameter(cmd, "IsFocus", newsPrInfo.IsFocus);
                _db.AddParameter(cmd, "Duration", newsPrInfo.Duration);
                _db.AddParameter(cmd, "CreatedBy", newsInfo.CreatedBy);
                _db.AddParameter(cmd, "BookingId", newsPrInfo.BookingId);
                _db.AddParameter(cmd, "Title", newsInfo.Title);
                _db.AddParameter(cmd, "SubTitle", newsInfo.SubTitle);
                _db.AddParameter(cmd, "Sapo", newsInfo.Sapo);
                _db.AddParameter(cmd, "Body", newsInfo.Body);
                _db.AddParameter(cmd, "Avatar", newsInfo.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", newsInfo.AvatarDesc);
                _db.AddParameter(cmd, "Source", newsInfo.Source);
                _db.AddParameter(cmd, "Author", newsInfo.Author);
                _db.AddParameter(cmd, "WordCount", newsInfo.WordCount);
                _db.AddParameter(cmd, "IsPr", newsInfo.IsPr);
                _db.AddParameter(cmd, "PrPosition", newsInfo.PrPosition);
                _db.AddParameter(cmd, "AdStore", newsInfo.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", newsInfo.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", newsInfo.PrBookingNumber);
                _db.AddParameter(cmd, "Url", newsInfo.Url);
                _db.AddParameter(cmd, "LocationType", newsInfo.LocationType);
                _db.AddParameter(cmd, "Status", newsInfo.Status);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region ViewPlus
        public bool ViewPlus_Insert(ViewPlusEntity viewPlus)
        {
            const string commandText = "CMS_ViewPlus_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", viewPlus.NewsId);
                _db.AddParameter(cmd, "BannerId", viewPlus.BannerId);
                _db.AddParameter(cmd, "BidType", viewPlus.BidType);
                _db.AddParameter(cmd, "Budget", viewPlus.Budget);
                _db.AddParameter(cmd, "Message", viewPlus.Message);
                _db.AddParameter(cmd, "ReturnMessage", viewPlus.ReturnMessage);
                _db.AddParameter(cmd, "StartTime", viewPlus.StartTime);
                _db.AddParameter(cmd, "EndTime", viewPlus.EndTime);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ViewPlus_Delete(string Ids)
        {
            const string commandText = "CMS_ViewPlus_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", Ids);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsViewPlusListEntity> ViewPlus_GetList(int ZoneId, DateTime DateFrom, DateTime DateTo, int PageIndex, int PageSize, int bidType, ref int TotalRow)
        {
            const string commandText = "CMS_ViewPlus_GetList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", TotalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                if(DateFrom < Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom",  DBNull.Value);
                else
                    _db.AddParameter(cmd, "DateFrom", DateFrom);
                if (DateTo < Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DateTo", DateTo);
                _db.AddParameter(cmd, "PageIndex", PageIndex);
                _db.AddParameter(cmd, "PageSize", PageSize);
                _db.AddParameter(cmd, "BidType", bidType);
                var data = _db.GetList<NewsViewPlusListEntity>(cmd);
                TotalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region zoneviewplus
        public List<ZoneViewPlusEntity> GetAllZoneViewPlus_by_TypeId_ZoneId(int typeId, int zoneId)
        {
            const string commandText = "CMS_ZoneViewPlus_GetAllData_by_TypeId_ZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                var data = _db.GetList<ZoneViewPlusEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPrDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
