﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public class NewsLogExternalActionDal : NewsLogExternalActionDalBase
    {
        internal NewsLogExternalActionDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
