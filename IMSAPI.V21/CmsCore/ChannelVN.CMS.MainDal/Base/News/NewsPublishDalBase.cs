﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsPublishDalBase
    {
        public bool InsertNews(long newsId, int zoneId, string url)
        {
            const string commandText = "CMS_NewsPublish_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Url", url);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsIds(string newsIds)
        {
            const string commandText = "CMS_NewsPublish_GetRelatedNewsByNewsIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", newsIds);
                List<NewsPublishForNewsRelationEntity> data = _db.GetList<NewsPublishForNewsRelationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsPublish_GetNewsRelationByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                var data = _db.GetList<NewsPublishForNewsRelationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForObjectBoxEntity> SearchNewsPublishForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_SearchForNewsRelation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<NewsPublishForObjectBoxEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForObjectBoxEntity> SearchNewsPublishForNewsTagRelation(int zoneId, int tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_SearchForNewsTagRelation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TagId", tagId);
                List<NewsPublishForObjectBoxEntity> data = _db.GetList<NewsPublishForObjectBoxEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForObjectBoxEntity> SearchNewsPublishTemp_ForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublishTemp_SearchForNewsRelation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsPublishForObjectBoxEntity> data = _db.GetList<NewsPublishForObjectBoxEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForSearchEntity> SearchNewsPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_SearchNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsPublishForSearchEntity> data = _db.GetList<NewsPublishForSearchEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //Lấy danh sách  tin bài  trong khoảng thời gian và chuyên mục 
        public List<NewsPublishEntity> GetListNewsPublishByDate(int zoneId, DateTime distributedDateFrom, DateTime distributedDateTo)
        {
            const string commandText = "SYNCV2_CMS_NewsPublish_GetListByDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                List<NewsPublishEntity> data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsPublishEntity> GetLastestNewsByHour(int hour, int zoneId)
        {
            const string commandText = "CMS_NewsPublish_GetLastestNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "HourToGet", hour);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                List<NewsPublishEntity> data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishEntity> GetByNewsIdAndZoneId(int zoneId, long newsId, string listExcludePositionTypeId, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            const string commandText = "CMS_NewsPublish_GetByNewsIdAndZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ExcludePositionTypeId", listExcludePositionTypeId);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                _db.AddParameter(cmd, "IsBreakingNews", isBreakingNews);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                List<NewsPublishEntity> data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public NewsPublishEntity GetByNewsIdAndZoneId(long newsId, int zoneId)
        {
            const string commandText = "CMS_NewsPublish_GetNewsPublishByNewsIdAndZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                NewsPublishEntity data = _db.Get<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPublishEntity GetLastestNewsExcludeListPositionType(long currentNewsId, int zoneId, string listExcludePositionTypeId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            const string commandText = "CMS_NewsPublish_GetLastestNewsExcludeListPositionType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CurrentNewsId", currentNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListExcludePositionTypeId", listExcludePositionTypeId);
                _db.AddParameter(cmd, "IncludeChildZone", includeChildZone);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                _db.AddParameter(cmd, "IsBreakingNews", isBreakingNews);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                NewsPublishEntity data = _db.Get<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPublishEntity GetLastestNewsByZoneId(int zoneId, long excludeNewsId)
        {
            const string commandText = "CMS_NewsPublish_GetLastestNewsByZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ExcludeNewsId", excludeNewsId);
                NewsPublishEntity data = _db.Get<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPublishEntity GetLastestNewsByZoneIdExcludeNewsInPosition(int zoneId, long excludeNewsId, int excludeNewsInPositionId)
        {
            const string commandText = "CMS_NewsPublish_GetLastestNewsByZoneIdExcludeNewsInPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ExcludeNewsId", excludeNewsId);
                _db.AddParameter(cmd, "ExcludeNewsInPositionId", excludeNewsInPositionId);
                var data = _db.Get<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForNewsRelationEntity> GetRelatedNewsByTypeNewsId(long newsId, int type)
        {
            const string commandText = "CMS_NewsPublish_GetNewsRelationByTypeNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Type", type);
                List<NewsPublishForNewsRelationEntity> data = _db.GetList<NewsPublishForNewsRelationEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPublishEntity GetByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsPublish_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                NewsPublishEntity data = _db.Get<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishEntity> GetListNewsByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_NewsPublish_GetByListNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                List<NewsPublishEntity> data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region New methods for optimization
        public List<NewsPublishForSearchEntity> SearchByKeyword(int zoneId, string keyword, int DisplayPosition, int Priority, bool IsShowNewsBomd, int pageIndex, int pageSize, bool useFullDatabase)
        {
            const string commandTextFull = "CMS_NewsPublish_SearchByKeyword";
            const string commandText = "CMS_NewsPublishTemp_SearchByKeyword";
            try
            {
                var cmd = _db.CreateCommand(useFullDatabase ? commandTextFull : commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "DisplayPosition", DisplayPosition);
                _db.AddParameter(cmd, "Priority", Priority);
                _db.AddParameter(cmd, "IsShowNewsBomd", IsShowNewsBomd);
                cmd.CommandTimeout = 100;
                List<NewsPublishForSearchEntity> data = _db.GetList<NewsPublishForSearchEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion


        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPublishDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
