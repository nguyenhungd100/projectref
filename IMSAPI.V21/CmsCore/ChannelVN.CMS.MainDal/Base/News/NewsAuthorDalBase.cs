﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsAuthorDalBase
    {
        public bool Insert(NewsAuthorEntity newsAuthorEntity, ref int newsAuthorId)
        {
            const string commandText = "CMS_NewsAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserData", newsAuthorEntity.UserData);
                _db.AddParameter(cmd, "AuthorName", newsAuthorEntity.AuthorName);
                _db.AddParameter(cmd, "AuthorType", newsAuthorEntity.AuthorType);
                _db.AddParameter(cmd, "OriginalName", newsAuthorEntity.OriginalName);
                _db.AddParameter(cmd, "UnsignName", newsAuthorEntity.UnsignName);
                _db.AddParameter(cmd, "Avatar", newsAuthorEntity.Avatar);
                _db.AddParameter(cmd, "NewsRoyaltiesRoleId", newsAuthorEntity.NewsRoyaltiesRoleId);                
                _db.AddParameter(cmd, "AuthorTitle", newsAuthorEntity.AuthorTitle);
                _db.AddParameter(cmd, "Description", newsAuthorEntity.Description);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                newsAuthorId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Insert into NewsByAuthor table
        /// </summary>
        /// <param name="newsByAuthorEntity"></param>
        /// <returns></returns>
        public bool InsertToNewsByAuthor(NewsByAuthorEntity newsByAuthorEntity)
        {
            const string commandText = "CMS_NewsByAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsByAuthorEntity.NewsId);
                _db.AddParameter(cmd, "AuthorId", newsByAuthorEntity.AuthorId);
                _db.AddParameter(cmd, "AuthorName", newsByAuthorEntity.AuthorName);
                _db.AddParameter(cmd, "Note", newsByAuthorEntity.Note);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(NewsAuthorEntity newsAuthorEntity)
        {
            const string commandText = "CMS_NewsAuthor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserData", newsAuthorEntity.UserData);
                _db.AddParameter(cmd, "AuthorName", newsAuthorEntity.AuthorName);
                _db.AddParameter(cmd, "AuthorType", newsAuthorEntity.AuthorType);
                _db.AddParameter(cmd, "OriginalName", newsAuthorEntity.OriginalName);
                _db.AddParameter(cmd, "UnsignName", newsAuthorEntity.UnsignName);
                _db.AddParameter(cmd, "Avatar", newsAuthorEntity.Avatar);
                _db.AddParameter(cmd, "NewsRoyaltiesRoleId", newsAuthorEntity.NewsRoyaltiesRoleId);
                _db.AddParameter(cmd, "Id", newsAuthorEntity.Id);
                _db.AddParameter(cmd, "AuthorTitle", newsAuthorEntity.AuthorTitle);
                _db.AddParameter(cmd, "Description", newsAuthorEntity.Description);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_NewsAuthor_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Delete All rows in NewsByAuthor table
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public bool DeleteAllNewsByAuthor(long newsId)
        {
            const string commandText = "CMS_NewsByAuthor_DeleteAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsAuthorEntity GetById(int id)
        {
            const string commandText = "CMS_NewsAuthor_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                NewsAuthorEntity data = _db.Get<NewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAuthorEntity> GetListByUserName(string username)
        {
            const string commandText = "CMS_NewsAuthor_GetListByUserName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", username);
                List<NewsAuthorEntity> data = _db.GetList<NewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsAuthorEntity GetByUserData(string userData, int authorType)
        {
            const string commandText = "CMS_NewsAuthor_GetByUserData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserData", userData);
                _db.AddParameter(cmd, "AuthorType", authorType);
                NewsAuthorEntity data = _db.Get<NewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAuthorEntity> GetAll()
        {
            const string commandText = "CMS_NewsAuthor_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<NewsAuthorEntity> data = _db.GetList<NewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAuthorEntity> Search(string keyword)
        {
            const string commandText = "CMS_NewsAuthor_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                List<NewsAuthorEntity> data = _db.GetList<NewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// From NewsByAuthorEntity
        /// </summary>
        /// <param name="newsId"></param>
        /// <returns></returns>
        public List<NewsByAuthorEntity> GetListInNews(long newsId)
        {
            const string commandText = "CMS_NewsAuthor_GetListInNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                List<NewsByAuthorEntity> data = _db.GetList<NewsByAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsAuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
