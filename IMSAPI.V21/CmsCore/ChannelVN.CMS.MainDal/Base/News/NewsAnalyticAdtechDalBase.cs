﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsAnalyticAdtechDalBase
    {
        public bool ResetViewCount()
        {
            const string commandText = "CMS_NewsAnalyticAdtech_ResetViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateViewCount(long newsId, int viewCount)
        {
            const string commandText = "CMS_NewsAnalyticAdtech_UpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ViewCount", viewCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool BatchUpdateViewCount(string listNewsId, string listViewCount)
        {
            const string commandText = "CMS_NewsAnalyticAdtech_BatchUpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                _db.AddParameter(cmd, "ListViewCount", listViewCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithAnalyticEntity> GetTopMostReadInHour(int top)
        {
            const string commandText = "CMS_NewsAnalyticAdtech_GetTopMostReadInHour";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                List<NewsWithAnalyticEntity> data = _db.GetList<NewsWithAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAnalyticEntity> GetLastUpdateViewCount(DateTime lastUpdateViewCount)
        {
            const string commandText = "CMS_NewsAnalyticAdtech_GetLastUpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LastUpdateViewCount", lastUpdateViewCount);
                List<NewsAnalyticEntity> data = _db.GetList<NewsAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAnalyticEntity> GetTopLastestViewCount(int top)
        {
            const string commandText = "CMS_NewsAnalyticAdtech_GetTopLastestNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                List<NewsAnalyticEntity> data = _db.GetList<NewsAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsAnalyticAdtechDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
