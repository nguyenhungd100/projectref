﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public class NewsGroupDal : NewsGroupDalBase
    {
        internal NewsGroupDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
