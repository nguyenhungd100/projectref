﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public class NewsChildPublishDal : NewsChildPublishDalBase
    {
        internal NewsChildPublishDal(CmsMainDb db)
            : base(db)
        {
        }
    }}
