﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class EmbedGroupDalBase
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public bool Insert(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            const string commandText = "CMS_EmbedGroup_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
               _db.AddParameter(cmd, "Id", 0);
                                         _db.AddParameter(cmd, "IsVisible", embedGroup.IsVisible);
                                         _db.AddParameter(cmd, "TemplateId", embedGroup.TemplateId);                                         
                                         _db.AddParameter(cmd, "ZoneIdList",listOfZoneId);
                bool data = _db.ExecuteNonQuery(cmd)>0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="embedGroup"></param>
        /// <param name="listOfZoneId"></param>
        /// <returns></returns>
        public bool Update(EmbedGroupEntity embedGroup, string listOfZoneId)
        {
            const string commandText = "CMS_EmbedGroup_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                 _db.AddParameter(cmd, "Id", embedGroup.Id);
                                         _db.AddParameter(cmd, "IsVisible", embedGroup.IsVisible);
                                         _db.AddParameter(cmd, "TemplateId", embedGroup.TemplateId);                                         
                                         _db.AddParameter(cmd, "ZoneIdList",listOfZoneId) ;
                bool data = _db.ExecuteNonQuery(cmd)>0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listOfEmbedGroupId"></param>
        /// <returns></returns>
        public bool UpdateSortOrder(string listOfEmbedGroupId)
        {
            const string commandText = "CMS_EmbedGroup_UpdateSortOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                 _db.AddParameter(cmd, "ListOfEmbedGroupId", listOfEmbedGroupId)   ;
                bool data = _db.ExecuteNonQuery(cmd)>0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<EmbedGroupEntity> GetAll()
        {
            const string commandText = "CMS_EmbedGroup_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<EmbedGroupEntity> data = _db.GetList<EmbedGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public EmbedGroupEntity GetById(int id)
        {
            const string commandText = "CMS_EmbedGroup_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                EmbedGroupEntity data = _db.Get<EmbedGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<ZoneInEmbedGroupEntity> GetListZoneById(int id)
        {
            const string commandText = "CMS_EmbedGroup_GetListZoneById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                List<ZoneInEmbedGroupEntity> data = _db.GetList<ZoneInEmbedGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            const string commandText = "CMS_EmbedGroup_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id)  ;
                bool data = _db.ExecuteNonQuery(cmd)>0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected EmbedGroupDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
