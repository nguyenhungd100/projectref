﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class BoxBannerDalBase
    {
        public bool Insert(BoxBannerEntity boxBanner, string zoneIdList, ref int id)
        {
            const string commandText = "CMS_BoxBanner_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxBanner.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", boxBanner.ZoneId);
                _db.AddParameter(cmd, "Position", boxBanner.Position);
                _db.AddParameter(cmd, "Priority", boxBanner.Priority);
                _db.AddParameter(cmd, "Avatar", boxBanner.Avatar);
                _db.AddParameter(cmd, "Title", boxBanner.Title);
                _db.AddParameter(cmd, "Sapo", boxBanner.Sapo);
                _db.AddParameter(cmd, "Url", boxBanner.Url);
                _db.AddParameter(cmd, "Status", boxBanner.Status);
                _db.AddParameter(cmd, "DisplayStyle", boxBanner.DisplayStyle);
                _db.AddParameter(cmd, "Type", boxBanner.Type);
                _db.AddParameter(cmd, "ObjectId", boxBanner.ObjectId);
                _db.AddParameter(cmd, "CreatedDate", boxBanner.CreatedDate);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BoxBannerEntity boxBanner, string zoneIdList)
        {
            const string commandText = "CMS_BoxBanner_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxBanner.Id);
                _db.AddParameter(cmd, "ZoneId", boxBanner.ZoneId);
                _db.AddParameter(cmd, "Position", boxBanner.Position);
                _db.AddParameter(cmd, "Priority", boxBanner.Priority);
                _db.AddParameter(cmd, "Avatar", boxBanner.Avatar);
                _db.AddParameter(cmd, "Title", boxBanner.Title);
                _db.AddParameter(cmd, "Sapo", boxBanner.Sapo);
                _db.AddParameter(cmd, "Url", boxBanner.Url);
                _db.AddParameter(cmd, "Status", boxBanner.Status);
                _db.AddParameter(cmd, "DisplayStyle", boxBanner.DisplayStyle);
                _db.AddParameter(cmd, "Type", boxBanner.Type);
                _db.AddParameter(cmd, "ObjectId", boxBanner.ObjectId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateSortOrder(string listOfBoxBannerId)
        {
            const string commandText = "CMS_BoxBanner_UpdateSortOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfBoxBannerId", listOfBoxBannerId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int boxBannerId)
        {
            const string commandText = "CMS_BoxBanner_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxBannerId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BoxBannerEntity GetById(int id)
        {const string commandText = "CMS_BoxBanner_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                 _db.AddParameter(cmd, "Id", id);
                BoxBannerEntity data = _db.Get<BoxBannerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxBannerInZoneEntity> GetListZoneIdById(int id)
        {
            const string commandText = "CMS_BoxBanner_GetListZoneIdById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
               
                List<BoxBannerInZoneEntity> data = _db.GetList<BoxBannerInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxBannerEntity> GetListByZoneIdAndPosition(int zoneId, int position, int status, int type)
        {const string commandText = "CMS_BoxBanner_GetListByZoneIdAndPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                                            _db.AddParameter(cmd, "Position", position);
                                            _db.AddParameter(cmd, "Status", status);
                                            _db.AddParameter(cmd, "Type", type);
                List<BoxBannerEntity> data = _db.GetList<BoxBannerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxBannerZoneEntity> GetListZoneByBoxBannerId(int boxBannerId)
        {const string commandText = "CMS_BoxBanner_GetZoneByBoxBannerId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "BoxBannerId", boxBannerId)    ;
                List<BoxBannerZoneEntity> data = _db.GetList<BoxBannerZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxBannerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
