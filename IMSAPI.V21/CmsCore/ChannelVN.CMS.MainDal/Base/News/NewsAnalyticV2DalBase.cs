﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsAnalyticV2DalBase
    {
        public List<NewsAnalyticV2Entity> GetLastUpdateViewCount(long lastUpdateViewCountId)
        {
            string commandText = @"SELECT NewsId, SUM([Count]) AS ViewCount, MAX(Id) AS LastUpdateViewCountId
                                FROM [PageViews]
                                WHERE (Id > " + lastUpdateViewCountId + @") AND (NewsId > 0)
                                GROUP BY NewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<NewsAnalyticV2Entity> data = _db.GetList<NewsAnalyticV2Entity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsAnalyticV2Entity> GetAllViewFromDateToNow(DateTime fromDate, int maxRecord)
        {
            string commandText = @"SELECT TOP(" + maxRecord + @") NewsId, SUM(ViewCount) AS ViewCount, MAX(LastUpdateViewCountId) AS LastUpdateViewCountId
                            FROM (
	                            SELECT NewsId, SUM([Count]) AS ViewCount, MAX(Id) AS LastUpdateViewCountId
	                            FROM [PageViewReports] 
	                            WHERE (ViewDate >= '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + @"') AND (NewsId > 0)
	                            GROUP BY NewsId 
	                            UNION
	                            SELECT NewsId, SUM([Count]) AS ViewCount, MAX(Id) AS LastUpdateViewCountId
	                            FROM [PageViews] 
	                            WHERE (NewsId > 0)
	                            GROUP BY NewsId 
                            ) AS DATA
                            GROUP BY NewsId
                            ORDER BY NewsId DESC";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<NewsAnalyticV2Entity> data = _db.GetList<NewsAnalyticV2Entity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsAnalyticV2Entity> GetTopMostReadInHour(int top, int zoneId)
        {
            string commandText = @"SELECT TOP(" + top + @") NewsId, SUM([Count]) AS ViewCount
                            FROM [PageViews]
                            WHERE (NewsId > 0) AND (DATEDIFF(hour, ViewDate, GETDATE()) = 0) " + (zoneId > 0 ? " AND CatId=" + zoneId : "") + @"
                            GROUP BY NewsId
                            ORDER BY SUM([Count]) DESC";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                List<NewsAnalyticV2Entity> data = _db.GetList<NewsAnalyticV2Entity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsAnalyticDb _db;

        protected NewsAnalyticV2DalBase(CmsAnalyticDb db)
        {
            _db = db;
        }

        protected CmsAnalyticDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
