﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.NewsPosition;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsDalBase
    {
        public bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_News_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "RollingNewsId", news.RollingNewsId);
                _db.AddParameter(cmd, "InterviewId", news.InterviewId);

                //quangnv added on 07/01/2013
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);

                //fox added on 14/05/2014
                _db.AddParameter(cmd, "LocationType", news.LocationType);

                //thanhtn added on 03/06/2014
                if (news.ExpiredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredDate", DBNull.Value);
                else _db.AddParameter(cmd, "ExpiredDate", news.ExpiredDate);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);
                //fox added 4/9/2014
                _db.AddParameter(cmd, "BonusPrice", news.BonusPrice);
                //thanhtn 29/07/2015
                _db.AddParameter(cmd, "LastReceiver", news.LastReceiver);
                //chinhnb 09/03/2018
                _db.AddParameter(cmd, "ShortTitle", news.ShortTitle);
                _db.AddParameter(cmd, "ParentNewsId", news.ParentNewsId);
                _db.AddParameter(cmd, "IsProd", news.IsProd);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertNewsForAppBotPublish(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                      string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId, string action)
        {
            const string commandText = "CMS_News_InsertForAppBotPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "RollingNewsId", news.RollingNewsId);
                _db.AddParameter(cmd, "InterviewId", news.InterviewId);

                //quangnv added on 07/01/2013
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);

                //fox added on 14/05/2014
                _db.AddParameter(cmd, "LocationType", news.LocationType);

                //thanhtn added on 03/06/2014
                if (news.ExpiredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredDate", DBNull.Value);
                else _db.AddParameter(cmd, "ExpiredDate", news.ExpiredDate);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);
                //fox added 4/9/2014
                _db.AddParameter(cmd, "BonusPrice", news.BonusPrice);
                //thanhtn 29/07/2015
                _db.AddParameter(cmd, "LastReceiver", news.LastReceiver);
                //chinhnb 09/03/2018
                _db.AddParameter(cmd, "ShortTitle", news.ShortTitle);
                _db.AddParameter(cmd, "ParentNewsId", news.ParentNewsId);
                _db.AddParameter(cmd, "IsProd", news.IsProd);

                _db.AddParameter(cmd, "Action", action);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetNewsIdByParentNewsIdForAppbotPublish(long parentId)
        {
            const string commandText = "CMS_News_GetNewsIdByParentIdForAppBotPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentNewsId", parentId);
                return _db.Get<NewsEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_News_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "RollingNewsId", news.RollingNewsId);
                _db.AddParameter(cmd, "InterviewId", news.InterviewId);
                //quangnv added on 07/01/2013
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);
                //fox added on 14/05/2014
                _db.AddParameter(cmd, "LocationType", news.LocationType);
                //thanhtn added on 03/06/2014
                if (news.ExpiredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredDate", DBNull.Value);
                else _db.AddParameter(cmd, "ExpiredDate", news.ExpiredDate);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);
                //fox added 4/9/2014
                _db.AddParameter(cmd, "BonusPrice", news.BonusPrice);
                //chinhnb 09/03/2018
                _db.AddParameter(cmd, "ShortTitle", news.ShortTitle);
                ////chinhnb 12/12/2017 ApprovedBy
                _db.AddParameter(cmd, "ApprovedBy", news.ApprovedBy);
                _db.AddParameter(cmd, "ParentNewsId", news.ParentNewsId);
                _db.AddParameter(cmd, "IsProd", news.IsProd);

                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UpdateNews_db_error_" + ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            const string commandText = "CMS_News_UpdateAvatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Avatar", avatar);
                _db.AddParameter(cmd, "Avatar2", avatar2);
                _db.AddParameter(cmd, "Avatar3", avatar3);
                _db.AddParameter(cmd, "Avatar4", avatar4);
                _db.AddParameter(cmd, "Avatar5", avatar5);
                _db.AddParameter(cmd, "AvatarCustom", avatarCustom);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNews(long id)
        {
            const string commandText = "CMS_News_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToWaitForEdit(long id)
        {
            const string commandText = "CMS_News_ChangeStatusToWaitForEdit";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRecievedForEdit(long id, string editedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRecievedForEdit";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "EditedBy", editedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToWaitForPublish(long id, string editedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToWaitForPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "EditedBy", editedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToWaitForEditorialBoard(long id, string approvedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToWaitForEditorialBoard";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRecievedForPublish(long id, string editedBy, string approvedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRecievedForPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "EditedBy", editedBy);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToRecievedForEditorialBoard(long id, string approvedBy, string publishedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRecievedForEditorialBoard";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToPublished(long id, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string publishedBy, long publishedDate, string publishedContent)
        {
            const string commandText = "CMS_News_ChangeStatusToPublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                _db.AddParameter(cmd, "NewsPositionTypeForHome", newsPositionTypeForHome);
                _db.AddParameter(cmd, "NewsPositionOrderForHome", newsPositionOrderForHome);
                _db.AddParameter(cmd, "NewsPositionTypeForList", newsPositionTypeForList);
                _db.AddParameter(cmd, "NewsPositionOrderForList", newsPositionOrderForList);
                _db.AddParameter(cmd, "PublishedDate", publishedDate);
                if (string.IsNullOrEmpty(publishedContent))
                {
                    _db.AddParameter(cmd, "PublishedContent", DBNull.Value);
                }
                else
                {
                    _db.AddParameter(cmd, "PublishedContent", publishedContent);
                }
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeletePhysical(long newsId)
        {
            const string commandText = "CMS_News_DeleteNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToUnpublished(long id, string lastModifiedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToUnpublished";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToMovedToTrash(long id, string lastModifiedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToMovedToTrash";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRestoreFromTrash(long id, string lastModifidedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRestoreFromTrash";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LastModidiedBy", lastModifidedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToCrawlerForEdit(long id, string createdBy)
        {
            const string commandText = "CMS_News_ChangeStatusToCrawlerForEditt";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToReturnToCooperator(long id, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnToCooperator";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToReturnedToReporter(long id, string username, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnedToReporter";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ReturnedBy", username);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToReturnedToEditorialSecretary(long id, string approvedBy, string username, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnedToEditorialSecretary";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToReturnedToMyEditorialSecretary(long id, string approvedBy, string username, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnedToMyEditorialSecretary";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusToReturnedToEditor(long id, string receiver, string username, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnedToEditor";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Receiver", receiver);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToReturnedToMyEditor(long id, string receiver, string username, string note)
        {
            const string commandText = "CMS_News_ChangeStatusToReturnedToMyEditor";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Receiver", receiver);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Note", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRecieveFromReturnStore(long id)
        {
            const string commandText = "CMS_News_ChangeStatusToRecieveFromReturnStore";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusToRecievedFromCooperatorStore(long id, int recievedWithStatus, string recievedBy)
        {
            const string commandText = "CMS_News_ChangeStatusToRecievedFromCooperatorStore";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "RecievedWithStatus", recievedWithStatus);
                _db.AddParameter(cmd, "RecievedBy", recievedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusForwardToOtherEditor(long id, string receiver)
        {
            const string commandText = "CMS_News_ChangeStatusForwardToOtherEditor";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Receiver", receiver);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatusForwardToOtherSecretary(long id, string receiver)
        {
            const string commandText = "CMS_News_ChangeStatusForwardToOtherSecretary";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Receiver", receiver);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            const string commandText = "CMS_News_GetNewsForValidateByNewsId";
            try
            {
                NewsForValidateEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsForValidateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        {
            const string commandText = "CMS_News_GetNewsInfoForCachedByNewsId";
            try
            {
                NewsInfoForCachedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsInfoForCachedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetNewsById(long id)
        {
            const string commandText = "CMS_News_GetNewsById";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "CMS_News_GetNewsById: " + ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsEntity GetNewsByNewsIdForInit(long id)
        {
            const string commandText = @"SELECT  N.*,dbo.CMS_fGetListOfZoneIdByNewsId(@Id) AS ListZoneId, NZ.ZoneId, Z.Name as ZoneName
                                        FROM News N 
                                        inner join NewsInzone NZ on N.Id=NZ.NewsId and NZ.IsPrimary=1
                                        inner join Zone Z on Z.Id=NZ.ZoneId and NZ.IsPrimary=1
                                        WHERE N.Id = @Id";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "CMS_News_GetNewsById: " + ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetNewsTypeByNewsId(long id)
        {
            const string commandText = "CMS_News_GetNewsTypeById";
            try
            {
                int data = (int)NewsType.Normal;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<int>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsForOtherAPIEntity> GetNewsForOtherAPI(string listNewsId)
        {
            const string commandText = "CMS_News_GetNewsForOtherAPIByListId";
            try
            {
                List<NewsForOtherAPIEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsForOtherAPIEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithAnalyticEntity> GetNewsByListNewsId(int status, string listNewsId)
        {
            const string commandText = "CMS_News_GetNewsByListNewsId";
            try
            {
                List<NewsWithAnalyticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsWithAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsWithExpertEntity> GetAllNewsByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_News_GetAllNewsByListNewsId";
            try
            {
                List<NewsWithExpertEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsWithExpertEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsForExportEntity> GetAllNewsForExportByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_News_GetAllNewsForExportByListNewsId";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_News_GetNewsDistributionDateByListNewsId";
            try
            {
                List<NewsWithDistributionDateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsWithDistributionDateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForSuggestionEntity> GetNewsUseInterviewByListInterviewId(string listInterviewId)
        {
            const string commandText = "CMS_News_GetNewsUseInterviewByListInterviewId";
            try
            {
                List<NewsForSuggestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListInterviewId", listInterviewId);
                data = _db.GetList<NewsForSuggestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        {
            const string commandText = "CMS_NewsAnalytic_GetTopMostNewsInHour";
            try
            {
                List<NewsWithAnalyticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<NewsWithAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNews(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsRoyaltiesV2Entity> ListNewsPublishForRoyalties(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM News n where n.Status in(8,10) AND (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.LastModifiedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.LastModifiedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.Author,n.Status,
                                        n.Source,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.WordCount,
                                        n.OriginalId,n.Url,
                                        n.OriginalUrl,n.IsPr, n.IsProd, nz.ZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n 
                                        left join NewsInZone nz WITH(nolock) on nz.NewsId=n.Id and nz.isprimary=1
                                        left join Zone z on z.Id=nz.ZoneId
                                        where n.Status in(8,10) AND (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.LastModifiedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.LastModifiedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<NewsRoyaltiesV2Entity> data = null;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsRoyaltiesV2Entity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, string.Format("ListNewsPublishForRoyalties => pageIndex: {0}, ex.Message: {1}", pageIndex, ex.Message));
                return new List<NewsRoyaltiesV2Entity>();
            }
        }
        public List<NewsEntity> InitRedisAllNews(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow, int zoneid)
        {
            //const string commandText = "CMS_News_InitRedisAllNews";
            string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(0) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                        n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                        n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                        n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                        n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                        n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                        n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, isnull(n.IsProd,0) as IsProd, dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n                                         
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

            var commandTextWithZone = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(0) FROM News n inner join NewsInZone nz on n.Id=nz.NewsId and nz.ZoneId=@ZoneCheckId      
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                        n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                        n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                        n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                        n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                        n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                        n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, isnull(n.IsProd,0) as IsProd, dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n inner join NewsInZone nz on n.Id=nz.NewsId and nz.ZoneId=@ZoneCheckId                                          
                                         where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(zoneid > 0 ? commandTextWithZone : commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (zoneid > 0)
                {
                    _db.AddParameter(cmd, "ZoneCheckId", zoneid);
                }

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                Logger.WriteLog(Logger.LogType.Trace, string.Format("Finish SQL INIT TotalRow = {0}", totalRow));
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, string.Format("InitRedisAllNews => pageIndex: {0}, ex.Message: {1}", pageIndex, ex.Message));
                return new List<NewsEntity>();
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> InitRedisAllNewsPublish(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_News_InitRedisAllNews";
            const string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM News n where n.Status=8 AND (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                        n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                        n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                        n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                        n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                        n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                        n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, n.IsProd, --dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n 
                                        where n.Status=8 AND (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, string.Format("InitRedisAllNewsPublish => pageIndex: {0}, ex.Message: {1}", pageIndex, ex.Message));
                return new List<NewsEntity>();
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> InitESAllNews(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow, int zoneid)
        {
            //string commandText = "CMS_News_InitESAllNews";
            string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                        n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                        n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                        n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                        n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                        n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                        n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, z.Name as ZoneName, n.IsProd, nz.ZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n 
                                        left join NewsInZone nz WITH(nolock) on nz.NewsId=n.Id and nz.isprimary=1
                                        left join Zone z on z.Id=nz.ZoneId
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";

            string commantextWithZone = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM News n inner join NewsInZone nz2 on n.Id=nz2.NewsId and nz2.ZoneId=@ZoneCheckId   
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
                                        n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
                                        n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
                                        n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
                                        n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
                                        n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.ApprovedBy,
                                        n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, z.Name as ZoneName, n.IsProd, nz.ZoneId,
                                        ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
                                        FROM News n 
                                        left join NewsInZone nz WITH(nolock) on nz.NewsId=n.Id and nz.isprimary=1
                                        left join Zone z on z.Id=nz.ZoneId
                                        inner join NewsInZone nz2 on n.Id=nz2.NewsId and nz2.ZoneId=@ZoneCheckId      
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(zoneid> 0? commantextWithZone : commandText, false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (zoneid > 0)
                {
                    _db.AddParameter(cmd, "ZoneCheckId", zoneid);
                }
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                Logger.WriteLog(Logger.LogType.Trace, string.Format("TotalRow = {0}", totalRow));

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Trace, string.Format("InitESAllNews => pageIndex: {0}, ex.Message: {1}", pageIndex, ex.Message));
                return new List<NewsEntity>();
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> GetNewsPublishByDate(DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetNewsPublishByDate";

            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsByPublishBy(long newsId, string publishedBy, string approvedBy)
        {
            const string commandText = "CMS_News_UpdateNewsByPublishBy";

            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                _db.AddParameter(cmd, "ApprovedBy", approvedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsTemp(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsTemp_Search";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsByNewsId(long NewsId)
        {
            const string commandText = "CMS_News_SearchByNewsId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsId);
                data = _db.GetList<NewsInListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsPublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishedForSecretary";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsTemp_PublishedForSecretary(string keyword, string username, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsTemp_SearchNewsPublishedForSecretary";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsPr(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search_Pr";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "FilterFieldForUsername", filterFieldForUsername);
                _db.AddParameter(cmd, "SortOrder", sortOrder);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByUsername";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsTemp_ByUsername(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsTemp_SearchByUsername";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsByType(string keyword, int type, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByType";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="top"></param>
        /// <param name="typeId"></param>
        /// <param name="zoneId"></param>
        /// <param name="listExcludePositionTypeId"></param>
        /// <param name="includeListNewsId"></param>
        /// <param name="includeChildZone"></param>
        /// <param name="isOnHome"></param>
        /// <param name="displayInSlide"></param>
        /// <param name="isBreakingNews"></param>
        /// <param name="displayPosition"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public List<NewsPublishEntity> GetTopLastestNewsForAutoUpdateNewsPosition(int top, int typeId, int zoneId, string listExcludePositionTypeId, string includeListNewsId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            const string commandText = "CMS_News_GetTopLastestNewsForAutoUpdateNewsPosition";
            try
            {
                List<NewsPublishEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListExcludePositionTypeId", listExcludePositionTypeId);
                _db.AddParameter(cmd, "IncludeListNewsId", includeListNewsId);
                _db.AddParameter(cmd, "IncludeChildZone", includeChildZone);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                _db.AddParameter(cmd, "IsBreakingNews", isBreakingNews);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="top"></param>
        /// <param name="typeId"></param>
        /// <param name="zoneId"></param>
        /// <param name="includeChildZone"></param>
        /// <param name="isOnHome"></param>
        /// <param name="displayInSlide"></param>
        /// <param name="isBreakingNews"></param>
        /// <param name="displayPosition"></param>
        /// <param name="isFocus"></param>
        /// <returns></returns>
        public List<NewsPublishEntity> GetTopLastestNewsForUpdateNewsPosition(int top, int typeId, int zoneId, bool includeChildZone, int isOnHome, int displayInSlide, int isBreakingNews, int displayPosition, int isFocus)
        {
            const string commandText = "CMS_News_GetTopLastestNewsForUpdateNewsPosition";
            try
            {
                List<NewsPublishEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "IncludeChildZone", includeChildZone);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                _db.AddParameter(cmd, "IsBreakingNews", isBreakingNews);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                data = _db.GetList<NewsPublishEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsByStatus(string keyword, string usernameDoAction, string usernameUpdateNews, int filterFieldForUsernameUpdateNews, string zoneIds, DateTime fromDate, DateTime toDate, int type, int status, bool isGetTotalRow, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByStatus";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "UsernameDoAction", usernameDoAction);
                _db.AddParameter(cmd, "UsernameUpdateNews", usernameUpdateNews);
                _db.AddParameter(cmd, "FilterFieldForUsernameUpdateNews", filterFieldForUsernameUpdateNews);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsGetTotalRow", isGetTotalRow);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsByTag(string tagIds, string tagNames, bool searchBytag, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchByTag";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagIds", tagIds);
                _db.AddParameter(cmd, "TagNames", tagNames);
                _db.AddParameter(cmd, "SearchByTag", searchBytag);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTag(int zoneId, string keyword, long tagId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInTag";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInMc(int zoneId, string keyword, int mcId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInMc";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "McId", mcId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInThread(int zoneId, string keyword, long threadId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInThread";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ThreadId", threadId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInTopic";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// NANIA bổ sung hàm SearchNewsByFocusKeyword
        /// Tìm các bài có sử dụng từ khóa Focus này
        /// Created By: 22/03/2013
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public List<NewsEntity> SearchNewsByFocusKeyword(string keyword, int top)
        {
            const string commandText = "CMS_News_SearchByFocusKeyword";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> CheckNewsByKeyword(string keyword, int page, int num, ref int total)
        {
            //const string commandText = "CMS_News_CheckNewsByKeyword";
            var commandText = @"IF @Keyword <> ''
	                            BEGIN
		                            SELECT @TotalRow = COUNT(*) FROM News n where Title LIKE '%' + @Keyword + '%'		
                                    SELECT  *
                                    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                                                        * ,
                                                        ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                                                FROM      News
                                                WHERE     Title LIKE '%' + @Keyword + '%' --or Id=@Keyword
                                            ) baseTable
                                    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                                                    AND     ( @PageIndex * @PageSize )
	                            END";
            var newsId = 0L;
            long.TryParse(keyword, out newsId);
            if (newsId > 0)
            {
                commandText = @"SELECT @TotalRow = COUNT(*) FROM News n where Title LIKE '%' + @Keyword + '%'		
                                SELECT  *
                                FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                                                    * ,
                                                    ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                                            FROM      News
                                            WHERE     Id=@Keyword
                                        ) baseTable
                                WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                                                AND     ( @PageIndex * @PageSize )";
            }
            try
            {
                List<NewsEntity> data = null;
                //var cmd = _db.CreateCommand(commandText, true);
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "TotalRow", total, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", page);
                _db.AddParameter(cmd, "PageSize", num);

                data = _db.GetList<NewsEntity>(cmd);
                total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, int priority, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "IMS2_News_SearchNewsWhichPublished";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "Priority", priority);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsForNewsPosition";
            //const string commandText = "IMS2_News_SearchNewsForNewsPosition";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsInListEntity> SearchNewsWhichPublishedForNewsPosition(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, string excludePositionTypes, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsWhichPublishedForNewsPosition";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "ExcludePositionTypes", excludePositionTypes);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsByTagId(long tagId)
        {
            const string commandText = "CMS_News_GetNewsByTagId";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByTagIdWithPaging(long tagId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByTagIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByMcIdWithPaging(int mcId, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByMcIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "McId", mcId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByThreadIdWithPaging(long threadId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByThreadIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ThreadId", threadId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByTopicIdWithPaging(string keyword, long topicId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_GetNewsByTopicIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchNewsPublishByTopicIdWithPaging(long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_GetNewsByTopicIdWithPaging";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsEntity> SearchHotNewsByTopicId(long topicId)
        {
            const string commandText = "CMS_Topic_GetHotNews";
            try
            {
                List<NewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", topicId);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCounterEntity> CounterNewsByStatus(string accountName, string zoneIds, int role, string commonStatus, bool isByZone)
        {
            const string commandText = "CMS_News_CounterInList";
            try
            {
                List<NewsCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Role", role);
                _db.AddParameter(cmd, "CommonStatus", commonStatus);
                _db.AddParameter(cmd, "IsByZone", isByZone);
                data = _db.GetList<NewsCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCounterEntity> GetCounter(string accountName, int zoneId)
        {
            const string commandText = "CMS_News_GetCounter";
            try
            {
                List<NewsCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public CategoryCounterEntity GetZoneCounter(int zoneId, DateTime fromDate, DateTime endDate)
        {
            const string commandText = "CMS_NewsPublish_GetCountNewsPublishByZone";
            try
            {
                CategoryCounterEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                data = _db.Get<CategoryCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Select dánh sách bài viết để tính nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="order">Order</param>
        /// <returns></returns>
        public List<NewsForRoyaltiesEntity> SelectRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectRoyalties";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForRoyaltiesEntity> SelectBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectRoyaltiesBusinessPr";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForRoyaltiesEntity> SelectPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, int pageIndex, int pageSize, int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectPRRoyalties";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Thống kê nhuận bút dành cho Cafebiz
        /// </summary>
        /// <param name="zoneId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="creator"></param>
        /// <param name="author"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="order"></param>
        /// <param name="NewsCategories"></param>
        /// <param name="newsType"></param>
        /// <param name="viewCountFrom"></param>
        /// <param name="viewCountTo"></param>
        /// <returns></returns>
        public List<NewsForRoyaltiesEntity> SelectRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                        int pageIndex, int pageSize, ref int totalRows,
                                                                        int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            const string commandText = "CMS_News_SelectRoyaltiesV2";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<NewsForRoyaltiesEntity> SelectPRRoyaltiesV2(string zoneId, DateTime startDate, DateTime endDate, string creator, int author,
                                                                       int pageIndex, int pageSize, ref int totalRows,
                                                                       int order, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0)
        {
            const string commandText = "CMS_News_SelectPRRoyaltiesV2";
            try
            {
                List<NewsForRoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", NewsCategories);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = _db.GetList<NewsForRoyaltiesEntity>(cmd);

                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Count tổng số nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public int CountRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountBusinessPrRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountBusinessPrRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountPRRoyalties(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string NewsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_CountPRRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategories", NewsCategories);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Get tong tien nhuận bút
        /// </summary>
        /// <param name="zoneId">Chuyên mục</param>
        /// <param name="startDate">Ngày bắt đầu</param>
        /// <param name="endDate">Ngày kết thúc</param>
        /// <param name="creator">Người tạo bài viết</param>
        /// <param name="author">Tác giả của bài viết</param>
        public decimal SelectTotalPrice(string zoneId, DateTime startDate, DateTime endDate, string creator, string author, string newsCategories, int newsType, int viewCountFrom = 0, int viewCountTo = 0, int originalId = 0)
        {
            const string commandText = "CMS_News_SelectTotalPrice";
            try
            {
                decimal data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "CreatedBy", creator);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "NewsCategory", newsCategories);
                _db.AddParameter(cmd, "NewsType", newsType);
                _db.AddParameter(cmd, "OriginalId", originalId);
                _db.AddParameter(cmd, "ViewCountFrom", viewCountFrom);
                _db.AddParameter(cmd, "ViewCountTo", viewCountTo);
                data = Utility.ConvertToDecimal(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Update giá tiền cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Giá tiền cho bài viết</param>
        /// <returns></returns>
        public bool UpdatePrice(long id, decimal price)
        {
            const string commandText = "CMS_News_UpdatePrice";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Price", price);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Update tiền bonus cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="price">Tiền bonus cho bài viết</param>
        /// <returns></returns>
        public bool UpdateBonusPrice(long id, decimal bonusPrice)
        {
            const string commandText = "CMS_News_UpdateBonusPrice";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "BonusPrice", bonusPrice);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsCategory(long id, int newsCategory)
        {
            const string commandText = "CMS_News_UpdateNewsCategory";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "NewsCategory", newsCategory);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsOnHome(long id, bool isOnHome)
        {
            const string commandText = "CMS_News_UpdateIsOnHome";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "IsOnHome", isOnHome);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsFocus(long id, bool isFocus)
        {
            const string commandText = "CMS_News_UpdateIsFocus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateSeoReviewStatus(long id, int reviewStatus, string accountName)
        {
            const string commandText = "CMS_SeoMetaNews_UpdateReviewStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ReviewStatus", reviewStatus);
                _db.AddParameter(cmd, "ReviewedBy", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateDisplayInSlide(long id, int displayInSlide)
        {
            const string commandText = "CMS_News_UpdateDisplayInSlide";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "DisplayInSlide", displayInSlide);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Update ghi chú về chấm nhuận bút cho bài viết
        /// </summary>
        /// <param name="id">Id bài viết</param>
        /// <param name="note">Ghi chú cho bài viết</param>
        /// <returns></returns>
        public bool UpdateNoteRoyalties(long id, string note)
        {
            const string commandText = "CMS_News_UpdateNoteRoyalties";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "NoteRoyalties", note);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            const string commandText = "CMS_News_UpdateOnlyTitle";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "NewsTitle", newsTitle);
                _db.AddParameter(cmd, "NewsUrl", newsUrl);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            const string commandText = "CMS_News_UpdateDetail";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "NewsTitle", newsTitle);
                _db.AddParameter(cmd, "NewsSapo", sapo);
                _db.AddParameter(cmd, "NewsBody", body);
                _db.AddParameter(cmd, "NewsUrl", newsUrl);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateNewsByErrorCheck(NewsEntity news, string newsUrl, string tag, string accountName)
        {
            const string commandText = "CMS_News_UpdateNewsByErrorCheck";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", news.Id);
                _db.AddParameter(cmd, "NewsTitle", news.Title);
                _db.AddParameter(cmd, "NewsSubTitle", news.SubTitle);
                _db.AddParameter(cmd, "NewsSapo", news.Sapo);
                _db.AddParameter(cmd, "NewsBody", news.Body);
                _db.AddParameter(cmd, "NewsUrl", newsUrl);
                _db.AddParameter(cmd, "NewsAvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "NewsNote", news.Note);
                _db.AddParameter(cmd, "NewsTag", tag);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);
                _db.AddParameter(cmd, "ShortTitle", news.ShortTitle);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            const string commandText = "CMS_News_UpdateDetailSimple";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "NewsTitle", newsTitle);
                _db.AddParameter(cmd, "avatar", avatar);
                _db.AddParameter(cmd, "avatar2", avatar2);
                _db.AddParameter(cmd, "avatar3", avatar3);
                _db.AddParameter(cmd, "avatar4", avatar4);
                _db.AddParameter(cmd, "avatar5", avatar5);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            const string commandText = "CMS_News_UpdateDisplayPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(long id, int priority, string accountName)
        {
            const string commandText = "CMS_News_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                _db.AddParameter(cmd, "Priority", priority);
                _db.AddParameter(cmd, "AccountName", accountName);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool RemoveNewsPosition(int configPosition, int zoneId, int typeId, string accountName)
        {
            const string commandText = "CMS_NewsPosition_RemoveNewsPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ConfigPosition", configPosition);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        // Update lại tag trong winservice 

        public bool UpdateTagAutoByNewsId(long newsId, string linkTag, string tagItems)
        {
            const string commandText = "CMS_News_UpdateTagById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Tag", linkTag);
                _db.AddParameter(cmd, "TagItems", tagItems);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        // Update lại tag mới strong bảng Tag 
        public bool UpdateTagAutoForNews(long newsId, string tagListId, ref string tagIdListUpdated)
        {
            const string commandText = "CMS_TagNews_UpdateTagAutoForNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "TagIdList", tagListId);
                tagIdListUpdated = Utility.ConvertToString(_db.GetFirtDataRecord(cmd));

                return !string.IsNullOrEmpty(tagIdListUpdated);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTagUrlForNews(long newsId, string tagLink, string tagItems)
        {
            const string commandText = "CMS_News_UpdateTagUrl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "TagLink", tagLink);
                _db.AddParameter(cmd, "TagItems", tagItems);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> GetNewsByIds(string listNewsId, int zoneId)
        {
            const string commandText = "CMS_News_GetByListId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListId", listNewsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsInListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportNewsData(string zoneIds, string sourceIds, int orderBy, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportNews";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Sources", sourceIds);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "DateFrom", fromDate);
                _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportHotNewsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportHotNews";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportNewsHasManyComments";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> GetListNewsByDistributionDate(string zoneIds, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetByDistributionDate";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishNewsContentOnly(long newsId, string body)
        {
            const string commandText = "CMS_News_PublishNewsContentOnly";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Body", body);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePhotoVideoCount(long newsId, int photoCount, int videoCount)
        {
            const string commandText = "CMS_News_UpdatePhotoVideoCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "VideoCount", videoCount);
                _db.AddParameter(cmd, "Photocount", photoCount);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForSuggestionEntity> GetNewsUseRollingNews(int rollingNewsId)
        {
            const string commandText = "CMS_News_GetNewsUseRollingNews";
            try
            {
                List<NewsForSuggestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);
                data = _db.GetList<NewsForSuggestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region NewsStats By OriginalId

        public int GetViewCountByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId)
        {
            const string commandText = "CMS_News_GetViewCount_By_OriginalId";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "OriginalId", originalId);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsByOriginalId(string zoneIds, DateTime fromDate, DateTime toDate, int originalId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_Search_By_OriginalId";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "OriginalId", originalId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Update like count

        public bool UpdateLikeCount(long newsId, int likeCount)
        {
            const string commandText = "CMS_News_UpdateLikeCount";
            try
            {
                bool data = false;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "LikeCount", likeCount);
                data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsEntity> SearchNewsByDateRange(DateTime startDate, DateTime endDate, int status)
        {
            const string commandText = "CMS_News_SearchNewsByDateRange";
            try
            {
                List<NewsEntity> data;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DateFrom", startDate);
                _db.AddParameter(cmd, "DateTo", endDate);
                _db.AddParameter(cmd, "status", status);
                data = _db.GetList<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        public NewsEntity GetNewsByTitle(string title)
        {
            const string commandText = "CMS_NewsPublish_GetByNewsTitle";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsTitle", title);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountNewsBySource(int sourceId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_CountBySource";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SourceId", sourceId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsRelationJson(long id, string newsRelation)
        {
            const string commandText = "CMS_News_UpdateNewsRelationJson";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "NewsRelation", newsRelation);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsUnPublishEntity> SearchNewsUnPublish(string keyword, int ZoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsUnPublish_Search";
            try
            {
                List<NewsUnPublishEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", ZoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsUnPublishEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishedByKeyword(string keyword)
        {
            const string commandText = "CMS_News_GetNewsPublishByKeyword";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                data = _db.GetList<NewsInListEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAppBotEntity> GetListNewsNotIbtempByZoneId(int zoneId, int page, int num, ref int total)
        {
            const string commandText = "CMS_NewsPublish_GetListNewsNotIbtempByZoneId";
            try
            {
                List<NewsAppBotEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", total, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", page);
                _db.AddParameter(cmd, "PageSize", num);
                data = _db.GetList<NewsAppBotEntity>(cmd);

                total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAppBotEntity> GetListNewsNotIbBoundByZoneId(int zoneId, int page, int num, ref int total)
        {
            const string commandText = "CMS_NewsPublish_GetListNewsNotIbBoundByZoneId";
            try
            {
                List<NewsAppBotEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", total, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", page);
                _db.AddParameter(cmd, "PageSize", num);
                data = _db.GetList<NewsAppBotEntity>(cmd);

                total = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsDashBoardUserEntity> GetDataDashBoardUser(string username, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetDataDashBoardUser";
            try
            {
                List<NewsDashBoardUserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsDashBoardUserEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<long> GetIds(string username, string zoneIds, int top)
        {
            const string commandText = "CMS_News_GetIds";
            try
            {
                List<long> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetListGenericType<long>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region GET NEW LOG
        public NewsEntity GetNewsInfoById(long id)
        {
            const string commandText = "CMS_NewsInfo_GetById";
            try
            {
                NewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsFullLogEntity> News_GetFullLog(string keyword)
        {
            const string commandText = "CMS_News_GetFullLog";
            try
            {
                List<NewsFullLogEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", keyword);
                data = _db.GetList<NewsFullLogEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion  

        #region News Top Hot
        public bool NewsHot_Delete(string NewsIds, int type)
        {
            const string commandText = "CMS_NewsHot_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", NewsIds);
                _db.AddParameter(cmd, "Type", type);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_UpdateType()
        {
            const string commandText = "CMS_NewsHot_UpdateType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_CheckExists()
        {
            const string commandText = "CMS_NewsHot_CheckExists";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool NewsHot_Insert(NewsHotEntity entity)
        {
            const string commandText = "CMS_NewsHot_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.newsId);
                _db.AddParameter(cmd, "Type", entity.Type);
                _db.AddParameter(cmd, "Title", entity.title);
                _db.AddParameter(cmd, "Url", entity.url);
                _db.AddParameter(cmd, "PictureUrl", entity.pictureUrl);
                _db.AddParameter(cmd, "PredictColor", entity.predictColor);
                _db.AddParameter(cmd, "PredictDesc", entity.predictDesc);
                _db.AddParameter(cmd, "PublishDate", entity.publishDate);
                _db.AddParameter(cmd, "Rank", entity.rank);
                _db.AddParameter(cmd, "Sapo", entity.sapo);
                _db.AddParameter(cmd, "CategoryHref", entity.categoryHref);
                _db.AddParameter(cmd, "CategoryName", entity.categoryName);
                _db.AddParameter(cmd, "LastModifiedDate", entity.LastModifiedDate);
                _db.AddParameter(cmd, "SortOrder", entity.SortOrder);
                _db.AddParameter(cmd, "sticked", entity.sticked);
                _db.AddParameter(cmd, "inCover", entity.inCover);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_Insert2(NewsHotEntity entity)
        {
            const string commandText = "CMS_NewsHot_Insert2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.newsId);
                _db.AddParameter(cmd, "Type", entity.Type);
                _db.AddParameter(cmd, "Title", entity.title);
                _db.AddParameter(cmd, "Url", entity.url);
                _db.AddParameter(cmd, "PictureUrl", entity.pictureUrl);
                _db.AddParameter(cmd, "PredictColor", entity.predictColor);
                _db.AddParameter(cmd, "PredictDesc", entity.predictDesc);
                _db.AddParameter(cmd, "PublishDate", entity.publishDate);
                _db.AddParameter(cmd, "Rank", entity.rank);
                _db.AddParameter(cmd, "Sapo", entity.sapo);
                _db.AddParameter(cmd, "CategoryHref", entity.categoryHref);
                _db.AddParameter(cmd, "CategoryName", entity.categoryName);
                _db.AddParameter(cmd, "LastModifiedDate", entity.LastModifiedDate);
                _db.AddParameter(cmd, "SortOrder", entity.SortOrder);
                _db.AddParameter(cmd, "sticked", entity.sticked);
                _db.AddParameter(cmd, "inCover", entity.inCover);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_Lock(int newsHotId)
        {
            const string commandText = "CMS_NewsHot_Lock";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsHotId", newsHotId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_Unlock(int newsHotId)
        {
            const string commandText = "CMS_NewsHot_Unlock";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsHotId", newsHotId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_ChangeNewsInLockPosition(int newsHotId, long newsId)
        {
            const string commandText = "CMS_NewsHot_ChangeNewsInLockPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsHotId", newsHotId);
                _db.AddParameter(cmd, "NewsId", newsId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
                //return _db.Get<NewsHotEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_AddNewsInLockPosition(NewsPositionEntity addItem)
        {
            const string commandText = "CMS_NewsHot_AddNewsInLockPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", addItem.NewsId);
                _db.AddParameter(cmd, "SortOrder", addItem.Order);
                _db.AddParameter(cmd, "StreamType", addItem.ZoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool NewsHot_DeleteNewsInLockPosition(NewsPositionEntity addItem)
        {
            const string commandText = "CMS_NewsHot_DeleteNewsInLockPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", addItem.NewsId);
                _db.AddParameter(cmd, "SortOrder", addItem.Order);
                _db.AddParameter(cmd, "StreamType", addItem.ZoneId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public long NewsHot_UnpublishOrBomdNews(long newsId)
        {
            const string commandText = "CMS_NewsHot_UnpublishOrBomdNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                return _db.Get<long>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsHotEntity> NewsHot_GetByType(int type)
        {
            const string commandText = "CMS_NewsHot_GetbyType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                return _db.GetList<NewsHotEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsHotEntity> NewsHot_GetAllForRedis()
        {
            const string commandText = "CMS_NewsHot_GetAllForRedis";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<NewsHotEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsHotCafeFEntity> NewsHot_GetAllForRedisByCafeF()
        {
            const string commandText = "CMS_NewsHotCafeF_GetAllForRedis";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<NewsHotCafeFEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        #region Seoer Update News
        public bool Seoer_UpdateNews(SeoerNewsEntity entity, string TagIdList, string TagJSON)
        {
            const string commandText = "CMS_News_Expert_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "MetaTitle", entity.MetaTitle);
                _db.AddParameter(cmd, "MetaKeyword", entity.MetaKeyword);
                _db.AddParameter(cmd, "MetaDescription", entity.MetaDescription);
                _db.AddParameter(cmd, "TagItem", entity.Tag);
                _db.AddParameter(cmd, "TagIdList", TagIdList);
                _db.AddParameter(cmd, "Tag", TagJSON);
                _db.AddParameter(cmd, "ReviewStatus", entity.ReviewStatus);
                _db.AddParameter(cmd, "ModifiedBy", entity.ReviewerName);

                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Expert Update News
        public bool Expert_UpdateNews(ExpertNewsEntity entity)
        {
            const string commandText = "CMS_News_ExpertUpdate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", entity.NewsId);
                _db.AddParameter(cmd, "Body", entity.Body);
                _db.AddParameter(cmd, "ModifiedBy", entity.ExpertName);

                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public bool RecieveFeedbackNewsIntoCms(NewsEntity newsInfo, ref long newsId)
        {
            const string commandText = "CMS_News_RecieveFeedbackNewsIntoCms";
            try
            {
                newsId = newsInfo.Id;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsInfo.Id);
                _db.AddParameter(cmd, "IsFocus", newsInfo.IsFocus);
                if (newsInfo.DistributionDate > Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", newsInfo.DistributionDate);
                else _db.AddParameter(cmd, "DistributionDate", DateTime.Now);
                _db.AddParameter(cmd, "CreatedBy", newsInfo.CreatedBy);
                _db.AddParameter(cmd, "ZoneId", newsInfo.ZoneId);
                _db.AddParameter(cmd, "Title", newsInfo.Title);
                _db.AddParameter(cmd, "SubTitle", newsInfo.SubTitle);
                _db.AddParameter(cmd, "Sapo", newsInfo.Sapo);
                _db.AddParameter(cmd, "Body", newsInfo.Body);
                _db.AddParameter(cmd, "Avatar", newsInfo.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", newsInfo.AvatarDesc);
                _db.AddParameter(cmd, "Source", newsInfo.Source);
                _db.AddParameter(cmd, "Author", newsInfo.Author);
                _db.AddParameter(cmd, "WordCount", newsInfo.WordCount);
                _db.AddParameter(cmd, "IsPr", newsInfo.IsPr);
                _db.AddParameter(cmd, "AdStore", newsInfo.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", newsInfo.AdStoreUrl);
                _db.AddParameter(cmd, "Url", newsInfo.Url);
                _db.AddParameter(cmd, "LocationType", newsInfo.LocationType);
                _db.AddParameter(cmd, "Status", newsInfo.Status);
                _db.AddParameter(cmd, "NewsRelation", newsInfo.NewsRelation);
                _db.AddParameter(cmd, "ParentNewsId", newsInfo.ParentNewsId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
    }
}
