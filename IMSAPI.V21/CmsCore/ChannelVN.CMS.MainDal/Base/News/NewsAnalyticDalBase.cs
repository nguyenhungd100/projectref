﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsAnalyticDalBase
    {
        public List<NewsAnalyticEntity> GetLastUpdateViewCount(DateTime lastUpdateViewCount)
        {
            const string commandText = "CMS_NewsAnalytic_GetLastUpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LastUpdateViewCount", lastUpdateViewCount);
                List<NewsAnalyticEntity> data = _db.GetList<NewsAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAnalyticEntity> GetTopLastestNews(int top)
        {
            const string commandText = "CMS_NewsAnalytic_GetTopLastestNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                List<NewsAnalyticEntity> data = _db.GetList<NewsAnalyticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAnalyticV3Entity> GetTopHighestViewCountByZone(int zone, int hour, int top)
        {
            const string commandText = "CMS_News_GetHighestViewCountByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zone);
                _db.AddParameter(cmd, "Hour", hour);
                List<NewsAnalyticV3Entity> data = _db.GetList<NewsAnalyticV3Entity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsAnalyticV3Entity> GetTopHighestCommentCountByZone(int zone, int hour, int top)
        {
            const string commandText = "CMS_News_GetHighestCommentCountByZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zone);
                _db.AddParameter(cmd, "Hour", hour);
                List<NewsAnalyticV3Entity> data = _db.GetList<NewsAnalyticV3Entity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsAnalyticDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
