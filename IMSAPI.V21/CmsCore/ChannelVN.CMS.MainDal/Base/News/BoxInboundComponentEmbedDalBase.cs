﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class BoxInboundComponentEmbedDalBase
    {
        public bool Insert(BoxInboundComponentEmbedEntity item, ref int id)
        {
            const string commandText = "CMS_BoxInboundComponentEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", item.Id, ParameterDirection.Output);                
                _db.AddParameter(cmd, "Position", item.Position);
                _db.AddParameter(cmd, "Type", item.Type);
                _db.AddParameter(cmd, "Priority", item.Priority);
                _db.AddParameter(cmd, "Avatar", item.Avatar);
                _db.AddParameter(cmd, "Title", item.Title);                               
                _db.AddParameter(cmd, "DisplayStyle", item.DisplayStyle);
                _db.AddParameter(cmd, "Status", item.Status);
                _db.AddParameter(cmd, "NewsId", item.NewsId);
                //_db.AddParameter(cmd, "LastModifiedDate", item.LastModifiedDate);
                _db.AddParameter(cmd, "Embed", item.Embed);
                _db.AddParameter(cmd, "DataJson", item.DataJson);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(BoxInboundComponentEmbedEntity item)
        {
            const string commandText = "CMS_BoxInboundComponentEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", item.Id);
                _db.AddParameter(cmd, "Position", item.Position);
                _db.AddParameter(cmd, "Type", item.Type);
                _db.AddParameter(cmd, "Priority", item.Priority);
                _db.AddParameter(cmd, "Avatar", item.Avatar);
                _db.AddParameter(cmd, "Title", item.Title);
                _db.AddParameter(cmd, "DisplayStyle", item.DisplayStyle);
                _db.AddParameter(cmd, "Status", item.Status);
                _db.AddParameter(cmd, "NewsId", item.NewsId);
                //_db.AddParameter(cmd, "LastModifiedDate", item.LastModifiedDate);
                _db.AddParameter(cmd, "Embed", item.Embed);
                _db.AddParameter(cmd, "DataJson", item.DataJson);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateSortOrder(string listOfBoxInboundComponentEmbedId)
        {
            const string commandText = "CMS_BoxInboundComponentEmbed_UpdateSortOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfBoxInboundComponentEmbedId", listOfBoxInboundComponentEmbedId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int boxId)
        {
            const string commandText = "CMS_BoxInboundComponentEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BoxInboundComponentEmbedEntity GetById(int id)
        {const string commandText = "CMS_BoxInboundComponentEmbed_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                 _db.AddParameter(cmd, "Id", id);
                BoxInboundComponentEmbedEntity data = _db.Get<BoxInboundComponentEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        

        public List<BoxInboundComponentEmbedEntity> GetListBoxInboundComponentEmbed(long newsId, int position, int status, int type)
        {const string commandText = "CMS_BoxInboundComponentEmbed_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Position", position);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.GetList<BoxInboundComponentEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region BoxInboundTemplate
        public bool InsertBoxInboundTemplate(BoxInboundTemplateEntity item, ref int id)
        {
            const string commandText = "CMS_BoxInboundComponentTemplate_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", item.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "TypeId", item.TypeId);
                _db.AddParameter(cmd, "Title", item.Title);
                _db.AddParameter(cmd, "Description", item.Description);
                _db.AddParameter(cmd, "CreatedBy", item.CreatedBy);
                //_db.AddParameter(cmd, "CreatedDate", item.CreatedDate);
                _db.AddParameter(cmd, "ModifiedBy", item.ModifiedBy);
                //_db.AddParameter(cmd, "ModifiedDate", item.ModifiedDate);
                _db.AddParameter(cmd, "ListZoneId", item.ListZoneId);
                _db.AddParameter(cmd, "ListTopicId", item.ListTopicId);
                _db.AddParameter(cmd, "ListThreadId", item.ListThreadId);
                _db.AddParameter(cmd, "DataJson", item.DataJson);
                _db.AddParameter(cmd, "DataEmbed", item.DataEmbed);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateBoxInboundTemplate(BoxInboundTemplateEntity item)
        {
            const string commandText = "CMS_BoxInboundComponentTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", item.Id);
                _db.AddParameter(cmd, "TypeId", item.TypeId);
                _db.AddParameter(cmd, "Title", item.Title);
                _db.AddParameter(cmd, "Description", item.Description);
                //_db.AddParameter(cmd, "CreatedBy", item.CreatedBy);
                //_db.AddParameter(cmd, "CreatedDate", item.CreatedDate);                
                _db.AddParameter(cmd, "ModifiedBy", item.ModifiedBy);
                //_db.AddParameter(cmd, "ModifiedDate", item.ModifiedDate);
                _db.AddParameter(cmd, "ListZoneId", item.ListZoneId);
                _db.AddParameter(cmd, "ListTopicId", item.ListTopicId);
                _db.AddParameter(cmd, "ListThreadId", item.ListThreadId);
                _db.AddParameter(cmd, "DataJson", item.DataJson);
                _db.AddParameter(cmd, "DataEmbed", item.DataEmbed);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }       

        public bool DeleteBoxInboundTemplate(int boxId)
        {
            const string commandText = "CMS_BoxInboundComponentTemplate_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public BoxInboundTemplateEntity GetBoxInboundTemplateById(int id)
        {
            const string commandText = "CMS_BoxInboundComponentTemplate_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var data = _db.Get<BoxInboundTemplateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxInboundTemplateEntity> GetListBoxInboundTemplate(string typeId, string title, string listZoneId, string listTopicId, string listThreadId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_BoxInboundComponentTemplate_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "ListZoneId", listZoneId);
                _db.AddParameter(cmd, "ListTopicId", listTopicId);
                _db.AddParameter(cmd, "ListThreadId", listThreadId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                var data = _db.GetList<BoxInboundTemplateEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxInboundComponentEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
