﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.News
{
    public abstract class NewsConfigDalBase
    {
        public NewsConfigEntity GetByConfigName(string configName)
        {
            const string commandText = "CMS_NewsConfig_GetByConfigName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                NewsConfigEntity data = _db.Get<NewsConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public NewsConfigEntity GetNewsConfigByTypeInitValue(int type, string value)
        {
            const string commandText = "CMS_NewsConfig_GetByTypeInitValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigValueType", type);
                _db.AddParameter(cmd, "ConfigInitValue", value);

                NewsConfigEntity data = _db.Get<NewsConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey, int type)
        {
            const string commandText = "CMS_NewsConfig_GetByGroupConfigKey";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigGroupKey", groupConfigKey);
                _db.AddParameter(cmd, "ConfigType", type);
                List<NewsConfigEntity> data = _db.GetList<NewsConfigEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsConfigEntity> GetAll(int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsConfig_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);                
                
                List<NewsConfigEntity> data = _db.GetList<NewsConfigEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(NewsConfigEntity obj)
        {
            const string commandText = "CMS_NewsConfig_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", obj.ConfigName);
                _db.AddParameter(cmd, "ConfigValue", obj.ConfigValue);
                _db.AddParameter(cmd, "ConfigLabel", obj.ConfigLabel);
                _db.AddParameter(cmd, "ConfigType", obj.ConfigValueType);
                _db.AddParameter(cmd, "ConfigInitValue", obj.ConfigInitValue);
                _db.AddParameter(cmd, "ConfigGroupKey", obj.ConfigGroupKey);
                _db.AddParameter(cmd, "ConfigGroup", obj.ConfigGroup);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(string configName, string configValue)
        {
            const string commandText = "CMS_NewsConfig_SetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(string configName, string configValue, int configType)
        {
            const string commandText = "CMS_NewsConfig_SetValueByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigType", configType);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValueAndLabel(string configName, string configLabel, string configValue)
        {
            const string commandText = "CMS_NewsConfig_SetValueAndLabel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigLabel", configLabel);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValueAndLabel(string configName, string configLabel, string configValue, int configType, string configInitValue)
        {
            const string commandText = "CMS_NewsConfig_SetValueAndLabelByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);
                _db.AddParameter(cmd, "ConfigValue", configValue);
                _db.AddParameter(cmd, "ConfigLabel", configLabel);
                _db.AddParameter(cmd, "ConfigType", configType);
                _db.AddParameter(cmd, "ConfigInitValue", configInitValue);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNewsConfigByConfigName(string configName)
        {
            const string commandText = "CMS_NewsConfig_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ConfigName", configName);                
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsConfigDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
