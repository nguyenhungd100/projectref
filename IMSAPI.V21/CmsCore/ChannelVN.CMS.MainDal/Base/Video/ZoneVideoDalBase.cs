using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class ZoneVideoDalBase
    {
        public List<ZoneVideoEntity> GetListByParentId(int parentId, int status)
        {
            const string commandText = "VideoCms_ZoneVideo_GetListByParentId";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetZoneVideoActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "VideoCms_ZoneVideo_GetActivedByUsernameAndPermissionIds";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetListZoneRelation(int videoId)
        {
            const string commandText = "VideoCms_ZoneVideo_GetListRelationVideo";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetListZoneRelationByPlayListId(int playListId)
        {
            const string commandText = "VideoCms_ZoneVideo_GetListRelationByPlayListId";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlayListId", playListId);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetListZoneRelationByChannelId(int channelId)
        {
            const string commandText = "VideoCms_ZoneVideo_GetListZoneRelationByChannelId";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", channelId);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            const string commandText = "CMS_ZoneVideo_GetActiveZoneInDay";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "zoneIds", zoneIds);
                _db.AddParameter(cmd, "viewDate", viewDate);
                _db.AddParameter(cmd, "mode", mode);

                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ZoneVideoEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            const string commandText = "CMS_ZoneVideo_GetActiveZoneInDayV2";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "zoneIds", zoneIds);
                _db.AddParameter(cmd, "viewDate", viewDate);
                _db.AddParameter(cmd, "mode", mode);

                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            const string commandText = "CMS_Video_GetNewestVideoForNotify";
            try
            {
                List<VideoModeNotifyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "viewDate", viewDate);

                data = _db.GetList<VideoModeNotifyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ZoneVideoEntity GetById(int id)
        {
            const string commandText = "VideoCms_ZoneVideo_GetById";
            try
            {
                ZoneVideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(ZoneVideoEntity zoneVideo, string listVideoTagId, ref int zoneVideoId)
        {

            const string commandText = "VideoCms_ZoneVideo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideo.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", zoneVideo.Name);
                _db.AddParameter(cmd, "Url", zoneVideo.Url);
                _db.AddParameter(cmd, "Order", zoneVideo.Order);
                _db.AddParameter(cmd, "ParentId", zoneVideo.ParentId);

                if (zoneVideo.CreatedDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "CreatedDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "CreatedDate", zoneVideo.CreatedDate);
                _db.AddParameter(cmd, "Status", zoneVideo.Status);
                _db.AddParameter(cmd, "DisplayStyle", zoneVideo.DisplayStyle);
                _db.AddParameter(cmd, "ShowOnHome", zoneVideo.ShowOnHome);
                _db.AddParameter(cmd, "ListVideoTagId", listVideoTagId);
                _db.AddParameter(cmd, "Invisibled", zoneVideo.Invisibled);
                _db.AddParameter(cmd, "Avatar", zoneVideo.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneVideo.AvatarCover);
                _db.AddParameter(cmd, "ZoneRelation", zoneVideo.ZoneRelation);
                _db.AddParameter(cmd, "Logo", zoneVideo.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneVideo.MetaAvatar);

                _db.AddParameter(cmd, "CatId", zoneVideo.CatId);
                _db.AddParameter(cmd, "Mode", zoneVideo.Mode);
                _db.AddParameter(cmd, "Description", zoneVideo.Description);                

                var numberOfRow = cmd.ExecuteNonQuery();

                zoneVideoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertInitDB(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {

            const string commandText = "VideoCms_ZoneVideo_InsertInitDB";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideo.Id);
                _db.AddParameter(cmd, "Name", zoneVideo.Name);
                _db.AddParameter(cmd, "Url", zoneVideo.Url);
                _db.AddParameter(cmd, "Order", zoneVideo.Order);
                _db.AddParameter(cmd, "ParentId", zoneVideo.ParentId);

                if (zoneVideo.CreatedDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "CreatedDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "CreatedDate", zoneVideo.CreatedDate);
                _db.AddParameter(cmd, "Status", zoneVideo.Status);
                _db.AddParameter(cmd, "DisplayStyle", zoneVideo.DisplayStyle);
                _db.AddParameter(cmd, "ShowOnHome", zoneVideo.ShowOnHome);
                _db.AddParameter(cmd, "ListVideoTagId", listVideoTagId);
                _db.AddParameter(cmd, "Invisibled", zoneVideo.Invisibled);
                _db.AddParameter(cmd, "Avatar", zoneVideo.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneVideo.AvatarCover);
                _db.AddParameter(cmd, "ZoneRelation", zoneVideo.ZoneRelation);
                _db.AddParameter(cmd, "Logo", zoneVideo.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneVideo.MetaAvatar);

                _db.AddParameter(cmd, "CatId", zoneVideo.CatId);
                _db.AddParameter(cmd, "Mode", zoneVideo.Mode);
                _db.AddParameter(cmd, "Description", zoneVideo.Description);

                var numberOfRow = cmd.ExecuteNonQuery();                

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(ZoneVideoEntity zoneVideo, string listVideoTagId)
        {

            const string commandText = "VideoCms_ZoneVideo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideo.Id);
                _db.AddParameter(cmd, "Name", zoneVideo.Name);
                _db.AddParameter(cmd, "Url", zoneVideo.Url);
                _db.AddParameter(cmd, "Order", zoneVideo.Order);
                _db.AddParameter(cmd, "ParentId", zoneVideo.ParentId);

                if (zoneVideo.ModifiedDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "ModifiedDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "ModifiedDate", zoneVideo.ModifiedDate);

                _db.AddParameter(cmd, "DisplayStyle", zoneVideo.DisplayStyle);
                _db.AddParameter(cmd, "ShowOnHome", zoneVideo.ShowOnHome);
                _db.AddParameter(cmd, "Status", zoneVideo.Status);
                _db.AddParameter(cmd, "ListVideoTagId", listVideoTagId);
                _db.AddParameter(cmd, "Invisibled", zoneVideo.Invisibled);
                _db.AddParameter(cmd, "Avatar", zoneVideo.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneVideo.AvatarCover);
                _db.AddParameter(cmd, "ZoneRelation", zoneVideo.ZoneRelation);
                _db.AddParameter(cmd, "Logo", zoneVideo.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneVideo.MetaAvatar);

                _db.AddParameter(cmd, "CatId", zoneVideo.CatId);
                _db.AddParameter(cmd, "Mode", zoneVideo.Mode);
                _db.AddParameter(cmd, "Description", zoneVideo.Description);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MoveUp(int zoneVideoId)
        {

            const string commandText = "VideoCms_ZoneVideo_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MoveDown(int zoneVideoId)
        {
            const string commandText = "VideoCms_ZoneVideo_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int zoneVideoId)
        {
            const string commandText = "VideoCms_ZoneVideo_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public bool GetZoneVideoIdByNewsZoneId(string newsZoneId, ref int zoneVideoId)
        {

            const string commandText = "CMS_ZoneVideo_GetByNewsZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneVideoId, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsZoneId", newsZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                zoneVideoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAvatarZoneVideo(string listAvatar)
        {
            const string commandText = "Cms_ZoneVideo_Update_Avatars";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Avatars", listAvatar);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsVideoDb _db;

        protected ZoneVideoDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
