﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoFolderDalBase
    {
        #region Update

        public bool Insert(VideoFolderEntity videoFolder, ref int newVideoFolderId)
        {

            const string commandText = "VideoCms_VideoFolder_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoFolder.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", videoFolder.ParentId);
                _db.AddParameter(cmd, "Name", videoFolder.Name);
                _db.AddParameter(cmd, "CreatedBy", videoFolder.CreatedBy);
                _db.AddParameter(cmd, "Status", videoFolder.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                newVideoFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(VideoFolderEntity videoFolder)
        {
            const string commandText = "VideoCms_VideoFolder_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", videoFolder.ParentId);
                _db.AddParameter(cmd, "Name", videoFolder.Name);
                _db.AddParameter(cmd, "Status", videoFolder.Status);
                _db.AddParameter(cmd, "Id", videoFolder.Id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int videoFolderId)
        {
            const string commandText = "VideoCms_VideoFolder_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoFolderId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public VideoFolderEntity GetById(int videoFolderId)
        {
            const string commandText = "VideoCms_VideoFolder_GetById";
            try
            {
                VideoFolderEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoFolderId);
                data = _db.Get<VideoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoFolderEntity> GetByParentId(int parentId, string createdBy)
        {
            const string commandText = "VideoCms_VideoFolder_GetByParentId";
            try
            {
                List<VideoFolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                data = _db.GetList<VideoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<VideoFolderEntity> Search(int parentId, string name, string createdBy, int status)
        {
            const string commandText = "VideoCms_VideoFolder_Search";
            try
            {
                List<VideoFolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<VideoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoFolderDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
