﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoTagDalBase
    {
        public bool Insert(VideoTagEntity videoTag, ref int videoTagId)
        {
            const string commandText = "VideoCms_VideoTag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoTag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", videoTag.ParentId);
                _db.AddParameter(cmd, "Name", videoTag.Name);
                _db.AddParameter(cmd, "UnsignName", videoTag.UnsignName);
                _db.AddParameter(cmd, "Avatar", videoTag.Avatar);
                _db.AddParameter(cmd, "Description", videoTag.Description);
                _db.AddParameter(cmd, "Url", videoTag.Url);
                _db.AddParameter(cmd, "IsHotTag", videoTag.IsHotTag);
                _db.AddParameter(cmd, "CreatedBy", videoTag.CreatedBy);
                _db.AddParameter(cmd, "Status", videoTag.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoTagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateByName(VideoTagEntity videoTag, ref int videoTagId)
        {
            const string commandText = "VideoCms_VideoTag_UpdateByName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoTag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", videoTag.Name);
                _db.AddParameter(cmd, "UnsignName", videoTag.UnsignName);
                _db.AddParameter(cmd, "Url", videoTag.Url);
                _db.AddParameter(cmd, "CreatedBy", videoTag.CreatedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoTagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoTagId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(VideoTagEntity videoTag)
        {
            const string commandText = "VideoCms_VideoTag_Update";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoTag.Id);
                _db.AddParameter(cmd, "ParentId", videoTag.ParentId);
                _db.AddParameter(cmd, "Avatar", videoTag.Avatar);
                _db.AddParameter(cmd, "Description", videoTag.Description);
                _db.AddParameter(cmd, "IsHotTag", videoTag.IsHotTag);
                _db.AddParameter(cmd, "Status", videoTag.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateMode(int videoTagId, bool isHotTag)
        {
            const string commandText = "VideoCms_VideoTag_ChangeMode";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoTagId);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoTagEntity> Search(string keyword, int parentId, int isHotTag, int status, int orderBy, bool getTagHasVideoOnly, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_VideoTag_Search";
            try
            {
                List<VideoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "GetTagHasVideoOnly", getTagHasVideoOnly);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoTagEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public VideoTagEntity GetById(int id)
        {
            const string commandText = "VideoCms_VideoTag_GetById";
            try
            {
                VideoTagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VideoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public VideoTagEntity GetByName(string name)
        {
            const string commandText = "VideoCms_VideoTag_GetByName";
            try
            {
                VideoTagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                data = _db.Get<VideoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        /// <summary>
        /// Get VideoTag List for Video Editing
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public List<VideoTagEntity> GetByVideoId(int videoId)
        {
            const string commandText = "VideoCms_VideoTag_GetListByVideoId";
            try
            {
                List<VideoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<VideoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Get VideoTag List for zonevideo Editing
        /// </summary>
        /// <param name="zoneVideoId"></param>
        /// <returns></returns>
        public List<VideoTagEntity> GetByZoneVideoId(int zoneVideoId)
        {
            const string commandText = "VideoCms_VideoTag_GetListByZoneVideoId";
            try
            {
                List<VideoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                data = _db.GetList<VideoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public List<VideoTagEntity> GetByIds(string tagIds)
        {
            const string commandText = "VideoCms_VideoTag_GetByIds";
            try
            {
                List<VideoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", tagIds);
                data = _db.GetList<VideoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoTagDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
