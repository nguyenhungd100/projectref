﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoExtensionDalBase
    {
        public VideoExtensionEntity GetValue(long videoId, int type)
        {
            const string commandText = "CMS_VideoExtension_GetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "Type", type);
                VideoExtensionEntity data = _db.Get<VideoExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public int GetMaxValue(long videoId, int type)
        {
            const string commandText = "CMS_VideoExtension_GetMaxValue";
            try
            {
                var maxValue = 1;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MaxValue", maxValue, ParameterDirection.Output);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "Type", type);
                var data = _db.ExecuteReader(cmd);
                maxValue = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return maxValue;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SetValue(long videoId, int type, string value)
        {
            const string commandText = "CMS_VideoExtension_SetValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByVideoId(long videoId)
        {
            const string commandText = "CMS_VideoExtension_DeleteByVideoId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoExtensionEntity> GetByListVideoId(string listVideoId)
        {
            const string commandText = "CMS_VideoExtension_GetByListVideoId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListVideoId", listVideoId);
                List<VideoExtensionEntity> data = _db.GetList<VideoExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoExtensionEntity> GetByVideoId(long videoId)
        {
            const string commandText = "CMS_VideoExtension_GetByVideoId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                List<VideoExtensionEntity> data = _db.GetList<VideoExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoExtensionEntity> GetByTypeAndVaue(int type, string value)
        {
            const string commandText = "CMS_VideoExtension_GetByTypeAndValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Value", value);
                List<VideoExtensionEntity> data = _db.GetList<VideoExtensionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VideoExtensionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
