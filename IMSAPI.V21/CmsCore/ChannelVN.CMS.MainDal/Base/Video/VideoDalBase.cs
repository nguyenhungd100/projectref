﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoDalBase
    {
        #region Sets

        public bool UpdateConvertedMode(string listVideoId)
        {
            const string commandText = "VideoCms_Video_UpdateConvertedMode";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListVideoId", listVideoId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            const string commandText = "VideoCms_Video_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                //CuongNP - Thêm nguồn video
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertV3(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            const string commandText = "VideoCms_Video_InsertV3";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "ChannelIdList", channelIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "AvatarShareFacebook", videoEntity.AvatarShareFacebook);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "HashId", videoEntity.HashId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrailerUrl", videoEntity.TrailerUrl);
                _db.AddParameter(cmd, "MetaAvatar", videoEntity.MetaAvatar);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);
                _db.AddParameter(cmd, "DisplayStyle", videoEntity.DisplayStyle);
                // INSERT INTO VideoByAuthor                              
                _db.AddParameter(cmd, "AuthorIdList", authorList[0]);
                _db.AddParameter(cmd, "AuthorNameList", authorList[1]);
                _db.AddParameter(cmd, "AuthorNoteList", authorList[2]);
                //share video
                _db.AddParameter(cmd, "Namespace", videoEntity.Namespace);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public bool InsertV4(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            string commandText = "VideoCms_Video_InsertV3";
            try
            {
                if (WcfMessageHeader.Current.Namespace == "TTVH")
                {
                    commandText = "VideoCms_Video_InsertV4";
                }

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "ChannelIdList", channelIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "AvatarShareFacebook", videoEntity.AvatarShareFacebook);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "HashId", videoEntity.HashId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrailerUrl", videoEntity.TrailerUrl);
                _db.AddParameter(cmd, "MetaAvatar", videoEntity.MetaAvatar);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);
                _db.AddParameter(cmd, "DisplayStyle", videoEntity.DisplayStyle);
                // INSERT INTO VideoByAuthor                              
                _db.AddParameter(cmd, "AuthorIdList", authorList[0]);
                _db.AddParameter(cmd, "AuthorNameList", authorList[1]);
                _db.AddParameter(cmd, "AuthorNoteList", authorList[2]);
                //share video
                _db.AddParameter(cmd, "Namespace", videoEntity.Namespace);               

                if (WcfMessageHeader.Current.Namespace == "TTVH")
                {
                    _db.AddParameter(cmd, "IsOnHome", videoEntity.IsOnHome);
                    _db.AddParameter(cmd, "IsAMP", videoEntity.IsAMP);
                    _db.AddParameter(cmd, "SourceId", videoEntity.SourceId);
                }

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public bool InsertVideoInitDB(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList, ref int videoId)
        {
            const string commandText = "VideoCms_Video_InsertVideoInitDB";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "ChannelIdList", channelIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "AvatarShareFacebook", videoEntity.AvatarShareFacebook);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "HashId", videoEntity.HashId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrailerUrl", videoEntity.TrailerUrl);
                _db.AddParameter(cmd, "MetaAvatar", videoEntity.MetaAvatar);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);
                _db.AddParameter(cmd, "DisplayStyle", videoEntity.DisplayStyle);
                // INSERT INTO VideoByAuthor                              
                _db.AddParameter(cmd, "AuthorIdList", authorList[0]);
                _db.AddParameter(cmd, "AuthorNameList", authorList[1]);
                _db.AddParameter(cmd, "AuthorNoteList", authorList[2]);
                //share video
                _db.AddParameter(cmd, "Namespace", videoEntity.Namespace);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public bool InsertVideoInfo(VideoEntity videoEntity, ref int videoId)
        {
            const string commandText = "VideoCms_Video_InsertVideoInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public bool InsertV2(VideoEntity videoEntity, string tagIdList, string zoneIdList, string playlistIdList, ref int videoId)
        {
            const string commandText = "VideoCms_Video_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "VideoFolderId", videoEntity.VideoFolderId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                //CuongNP - Thêm nguồn video
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }


        public bool Update(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            const string commandText = "VideoCms_Video_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                //CuongNP - Thêm nguồn video
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagNameList", tagNameList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);

                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateV4(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            const string commandText = "VideoCms_Video_UpdateV4";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagNameList", tagNameList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "ChannelIdList", channelIdList);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "AvatarShareFacebook", videoEntity.AvatarShareFacebook);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "HashId", videoEntity.HashId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrailerUrl", videoEntity.TrailerUrl);
                _db.AddParameter(cmd, "MetaAvatar", videoEntity.MetaAvatar);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);
                _db.AddParameter(cmd, "DisplayStyle", videoEntity.DisplayStyle);
                // INSERT INTO VideoByAuthor                 
                _db.AddParameter(cmd, "AuthorIdList", authorList[0]);
                _db.AddParameter(cmd, "AuthorNameList", authorList[1]);
                _db.AddParameter(cmd, "AuthorNoteList", authorList[2]);
                //share video
                _db.AddParameter(cmd, "Namespace", videoEntity.Namespace);

                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateV5(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList, string channelIdList, List<string> authorList)
        {
            string commandText = "VideoCms_Video_UpdateV4";

            try
            {
                if (WcfMessageHeader.Current.Namespace == "TTVH")
                {
                    commandText = "VideoCms_Video_UpdateV5";
                }

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagNameList", tagNameList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "ChannelIdList", channelIdList);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "AvatarShareFacebook", videoEntity.AvatarShareFacebook);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "HashId", videoEntity.HashId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrailerUrl", videoEntity.TrailerUrl);
                _db.AddParameter(cmd, "MetaAvatar", videoEntity.MetaAvatar);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);
                _db.AddParameter(cmd, "DisplayStyle", videoEntity.DisplayStyle);
                // INSERT INTO VideoByAuthor                 
                _db.AddParameter(cmd, "AuthorIdList", authorList[0]);
                _db.AddParameter(cmd, "AuthorNameList", authorList[1]);
                _db.AddParameter(cmd, "AuthorNoteList", authorList[2]);
                //share video
                _db.AddParameter(cmd, "Namespace", videoEntity.Namespace);

                _db.AddParameter(cmd, "Id", videoEntity.Id);

                if (WcfMessageHeader.Current.Namespace == "TTVH")
                {
                    _db.AddParameter(cmd, "IsOnHome", videoEntity.IsOnHome);
                    _db.AddParameter(cmd, "IsAMP", videoEntity.IsAMP);
                    _db.AddParameter(cmd, "SourceId", videoEntity.SourceId);
                }

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateVideoInfo(VideoEntity videoEntity)
        {
            const string commandText = "VideoCms_Video_UpdateVideoInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "OriginalUrl", videoEntity.OriginalUrl);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                _db.AddParameter(cmd, "Author", videoEntity.Author);
                _db.AddParameter(cmd, "Location", videoEntity.Location);
                _db.AddParameter(cmd, "PolicyContentId", videoEntity.PolicyContentId);

                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateV2(VideoEntity videoEntity, string tagIdList, string tagNameList, string zoneIdList, string playlistIdList)
        {
            const string commandText = "VideoCms_Video_UpdateV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "VideoFolderId", videoEntity.VideoFolderId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagNameList", tagNameList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                _db.AddParameter(cmd, "ParentId", videoEntity.ParentId);
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                _db.AddParameter(cmd, "TrainerUrl", videoEntity.TrailerUrl);

                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateName(VideoEntity videoEntity)
        {
            const string commandText = "CMS_Video_UpdateName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "EditedBy", videoEntity.EditedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNameAndDescription(VideoEntity videoEntity)
        {
            const string commandText = "CMS_Video_UpdateNameDescription";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "EditedBy", videoEntity.EditedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                _db.AddParameter(cmd, "Id", videoEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMode(int mode, int id, string userDoAction)
        {
            const string commandText = "VideoCms_Video_UpdateMode";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatusVideoByHashId(string hashId, string keyVideo, string userDoAction)
        {
            const string commandText = "VideoCms_Video_UpdateStatusVideoByHashId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "HashId", hashId);
                _db.AddParameter(cmd, "KeyVideo", keyVideo);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateListVideoViews(string listKeyVideo, string listViewVideo)
        {
            const string commandText = "VideoCms_Video_UpdateListViews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListKeyVideo", listKeyVideo);
                _db.AddParameter(cmd, "ListViewVideo", listViewVideo);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateViews(string keyVideo, int views)
        {
            const string commandText = "VideoCms_Video_UpdateViews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", keyVideo);
                _db.AddParameter(cmd, "Views", views);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Send(int videoId, string userDoAction)
        {
            const string commandText = "VideoCms_Video_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Return(int videoId)
        {
            const string commandText = "VideoCms_Video_Return";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Publish(int videoId, string userDoAction)
        {
            const string commandText = "VideoCms_Video_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishInNews(int videoId, string userDoAction, DateTime distributionDate)
        {
            const string commandText = "VideoCms_Video_Publish_In_News";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                if (distributionDate == DateTime.MinValue)
                {
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                }
                else
                {
                    _db.AddParameter(cmd, "DistributionDate", distributionDate);
                }
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Unpublish(int videoId)
        {
            const string commandText = "VideoCms_Video_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateAvatar(string avatar, int id, string userDoAction)
        {
            const string commandText = "VideoCms_Video_UpdateAvatar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Avatar", avatar);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }



        public bool AddVideoFileToVideo(VideoEntity videoEntity, ref int videoId)
        {
            const string commandText = "VideoCms_Video_AddFileToVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);

                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", videoEntity.LastModifiedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);

                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);



                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateNewsIdForList(string idList, long newsId)
        {

            const string commandText = "VideoCms_Video_UpdateNewsIdForList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", idList);
                _db.AddParameter(cmd, "NewsId", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int videoId)
        {
            const string commandText = "VideoCms_Video_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteByIdList(string videoIdList)
        {
            const string commandText = "VideoCms_Video_DeleteList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", videoIdList);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeStatus(int videoId, int status, string userName)
        {
            const string commandText = "VideoCms_Video_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "UserName", userName);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool ChangeStatus(int videoId)
        {
            const string commandText = "VideoCms_Video_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool ChangeStatusByList(string videoIdList, int status, string userName)
        {
            const string commandText = "VideoCms_Video_ChangeStatusList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", videoIdList);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "UserName", userName);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatusByList(string videoIdList)
        {
            const string commandText = "VideoCms_Video_ChangeStatusList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", videoIdList);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RemoveVideoFromPlaylist(int videoId, int playlistId)
        {
            const string commandText = "VideoCms_Video_RemoveVideoFromPlaylist";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SyncNewsAndVideo(string videoIdList, long newsId)
        {
            const string commandText = "VideoCms_UpdateNewsForVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SyncNewsAndVideoWithStatus(string videoIdList, long newsId, int status, int excludeStatus)
        {
            const string commandText = "VideoCms_UpdateNewsForVideoWithStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ExcludeStatus", excludeStatus);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateKeyVideoByFileName(string keyvideo, string fileName)
        {
            const string commandText = "CMS_Video_UpdateKeyVideoByNameFileName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", keyvideo);
                _db.AddParameter(cmd, "FileName", fileName);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Gets

        public List<VideoKeyEntity> GetListVideoWaitConvert(int top)
        {
            const string commandText = "VideoCms_Video_GetListVideoWaitConvert";
            try
            {
                List<VideoKeyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<VideoKeyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoEntity GetById(int videoId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetById";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoEntity GetVideoByHashId(string hashId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetVideoByHashId";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "HashId", hashId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoNotPublishCountInPlaylist> GetVideoNotPublishCount(string playlistIds)
        {

            const string commandText = "CMS_Video_CountVideoNotPublishByListPlaylist";
            try
            {
                List<VideoNotPublishCountInPlaylist> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylitIds", playlistIds);
                data = _db.GetList<VideoNotPublishCountInPlaylist>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoEntity GetByVideoExternalId(string videoExternalId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetByVideoExternalId";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoExternalId", videoExternalId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoEntity GetByIdV2(int videoId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetByIdV2";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoEntity GetByFileName(string fileName)
        {
            const string commandText = "VideoCms_Video_GetByFileName";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FileName", fileName);
                data = _db.Get<VideoEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoEntity GetByKeyvideo(string keyvideo, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetByKeyvideo";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", keyvideo);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoEntity GetByVideoFromYoutubeId(int videoFromYoutubeId)
        {
            const string commandText = "VideoCms_Video_GetByVideoFromYoutubeId";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoFromYoutubeId", videoFromYoutubeId);
                data = _db.Get<VideoEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetListByIdList(string videoIdList)
        {

            const string commandText = "VideoCms_Video_GetListByIdList";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", videoIdList);
                data = _db.GetList<VideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoByAuthorEntity> GetListAuthorInVideo(long videoId)
        {
            const string commandText = "CMS_VideoAuthor_GetListAuthorInVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                var data = _db.GetList<VideoByAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<string> GetLastPublishedForUpdateView(DateTime fromDate)
        {
            const string commandText = "VideoCms_Video_GetLastPublishedForUpdateView";
            try
            {
                List<string> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                data = _db.GetList<string>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetAllVideoInPlaylist(int playlistId)
        {
            const string commandText = "VideoCms_Video_GetAllInPlaylist";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                data = _db.GetList<VideoEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetAllVideoInPlaylistByMode(int playlistId, int mode)
        {
            const string commandText = "VideoCms_Video_GetAllInPlaylistByMode";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Mode", mode);
                data = _db.GetList<VideoEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetVideoInPlaylist(int playlistId, int pageIndex, int pageSize, ref int totalRow, ref int maxPriority)
        {

            const string commandText = "VideoCms_Video_GetVideoInPlaylist";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "MaxPriority", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                maxPriority = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 1));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoEntity GetVideoNewByPlayListId(int playlistId)
        {

            const string commandText = "VideoCms_Video_GetVideoNewByPlayListId";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                data = _db.Get<VideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetAllVideoInVideoThread(int videoThreadId)
        {
            const string commandText = "VideoCms_Video_GetAllVideoInVideoThread";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                data = _db.GetList<VideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetListPagingByZone(int pageIndex, int pageSize, int zoneId, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_GetListPagingByZone";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneVideoId", zoneId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Order", sortOder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetListPagingInPlaylist(int pageIndex, int pageSize, int playlistId, int status, string keyword, int sortOder, ref int totalRow)
        {

            const string commandText = "VideoCms_Video_GetListPagingInPlaylist";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Order", sortOder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<VideoEntity> GetListMyVideo(string username, int pageIndex, int pageSize, string keyword, int status, int order, int zoneVideoId, int playlistId, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_GetMyList";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> Search(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_Search";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> InitESAllVideo(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_InitESAllVideo";
            //const string commandText = @"
            //                            DECLARE @UpperBand int, @LowerBand int

            //                            SELECT @totalRow = COUNT(*) FROM Video v where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (v.CreatedDate BETWEEN @DateFrom AND @DateTo))

            //                            SET @LowerBand  = (@pageIndex - 1) * @PageSize
            //                            SET @UpperBand  = (@pageIndex * @PageSize)
            //                            SELECT * FROM (
            //                            SELECT v.*,ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber 
            //                            FROM Video v                                         
            //                            where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (v.CreatedDate BETWEEN @DateFrom AND @DateTo))
            //                            ) AS temp
            //                            WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<VideoEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchForList(string username, int zoneVideoId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_Search_For_List";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneId", zoneVideoId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", order);
                if (fromDate == DateTime.MinValue)
                {
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                }
                else
                    _db.AddParameter(cmd, "FromDate", fromDate);
                if (toDate == DateTime.MinValue)
                {
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                }
                else
                    _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_VideoDistribution_Search";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneVideoIds", zoneVideoIds);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoDistributionEntity> SearchDistribution(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ZoneVideo_GetZoneHaveVideoByDate";
            try
            {
                List<VideoDistributionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "createdDate", viewDate);
                _db.AddParameter(cmd, "ZoneVideo", zoneIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "mode", mode);
                data = _db.GetList<VideoDistributionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoDistributionEntity> SearchDistributionV2(string keyword, DateTime viewDate, int mode, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ZoneVideo_GetZoneHaveVideoByDateV2";
            try
            {
                List<VideoDistributionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "keyword", keyword);
                _db.AddParameter(cmd, "createdDate", viewDate);
                _db.AddParameter(cmd, "ZoneVideo", zoneIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "mode", mode);
                data = _db.GetList<VideoDistributionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchV2(string username, int videoFolderId, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_SearchV2";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "VideoFolderId", videoFolderId);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchAd(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, bool allowAd, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_Search_AllowAd";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "AllowAd", allowAd);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchExcludeBomb(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_Search_Exclude_Bomb";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoExportEntity> SearchExport(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_Search_Export";
            try
            {
                List<VideoExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoExportEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoInNewsEntity> GetVideoInNewsList(int videoId)
        {
            const string commandText = "VideoCms_GetNewsIdListByVideoId";
            try
            {
                List<VideoInNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<VideoInNewsEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoCounterEntity> GetVideoCounter(string username, string userDoAction)
        {
            const string commandText = "VideoCms_Video_GetVideoCounter";
            try
            {
                List<VideoCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                data = _db.GetList<VideoCounterEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoExportEntity> SelectRoyalties(DateTime startDate, DateTime endDate, int pageIndex, int pageSize)
        {
            const string commandText = "CMS_Video_SelectRoyalties";
            try
            {
                List<VideoExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoExportEntity> SelectRoyaltiesByZone(DateTime startDate, DateTime endDate, int pageIndex, int pageSize, string zones)
        {
            const string commandText = "CMS_Video_SelectRoyaltiesByZone";
            try
            {
                List<VideoExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "StartDate", startDate);
                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Zone", zones);
                data = _db.GetList<VideoExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountRoyalties(DateTime startDate, DateTime endDate)
        {
            const string commandText = "CMS_Video_CountRoyalties";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int CountRoyaltiesByZone(DateTime startDate, DateTime endDate, string zones)
        {
            const string commandText = "CMS_Video_CountRoyaltiesByZone";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StartDate", startDate);
                _db.AddParameter(cmd, "EndDate", endDate);
                _db.AddParameter(cmd, "Zone", zones);
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<VideoSharingEntity> GetListSharing(string keyword, string zoneIds, DateTime dateFrom, DateTime dateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Video_ListSharing";
            try
            {
                List<VideoSharingEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);

                if (dateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DateFrom", dateFrom);

                if (dateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DateTo", dateTo);

                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoSharingEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetListVideoByHour(string zoneVideoIds, int Hour)
        {
            const string commandText = "VideoCms_Video_GetByHour";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "HourToGet", Hour);
                _db.AddParameter(cmd, "ZoneVideoIds", zoneVideoIds);
                data = _db.GetList<VideoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> GetListVideoByParentId(int videoId)
        {
            const string commandText = "VideoCms_Video_GetListVideoByParentId";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<VideoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region VideoFromYoubute

        public bool InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, ref int id)
        {
            const string commandText = "CMS_VideoFromYoutube_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoFromYoutube.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "AccountName", videoFromYoutube.AccountName);
                _db.AddParameter(cmd, "SourceVideoUrl", videoFromYoutube.SourceVideoUrl);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube)
        {
            const string commandText = "CMS_VideoFromYoutube_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoFromYoutube.Id);
                _db.AddParameter(cmd, "VideoId", videoFromYoutube.VideoId);
                _db.AddParameter(cmd, "FilePath", videoFromYoutube.FilePath);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteVideoFromYoutube(int id)
        {

            const string commandText = "CMS_VideoFromYoutube_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            const string commandText = "CMS_VideoFromYoutube_GetById";
            try
            {
                VideoFromYoutubeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VideoFromYoutubeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoFromYoutubeEntity> GetListVideoFromYoutube(string accountName, int status, int order, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VideoFromYoutube_GetList";
            try
            {
                List<VideoFromYoutubeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "AccountName", accountName);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<VideoFromYoutubeEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> SearchWithExcludeZones(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, string excludeZones, int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "VideoCms_Video_SearchWithExcludeZones";

            object objFromDate;
            if (fromDate > DateTime.MinValue) objFromDate = fromDate;
            else objFromDate = DBNull.Value;

            object objToDate;
            if (toDate > DateTime.MinValue) objToDate = toDate;
            else objToDate = DBNull.Value;

            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", objFromDate);
                _db.AddParameter(cmd, "ToDate", objToDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "ExcludeZones", excludeZones);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion

        public List<PlaylistGroupEntity> GetPlaylistGroupByParentId(int playlistId)
        {
            const string commandText = "VideoCms_PlaylistGroup_GetByParent";
            try
            {
                List<PlaylistGroupEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                data = _db.GetList<PlaylistGroupEntity>(cmd);


                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public PlaylistGroupEntity GetPlaylistGroupByPlaylistId(int playlistId)
        {

            const string commandText = "VideoCms_PlaylistGroup_GetByPlaylistId";
            try
            {
                PlaylistGroupEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                data = _db.Get<PlaylistGroupEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertPlaylistInVideoGroup(PlaylistInPlaylistGroupEntity playlistGroup)
        {
            const string commandText = "VideoCms_PlaylistInPlaylistGroup_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistGroup.PlaylistId);
                _db.AddParameter(cmd, "PlaylistGroupId", playlistGroup.PlaylistGroupId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoEntity> SearchByPlaylistGroup(int group, int zone)
        {
            const string commandText = "CMS_Video_GetByPlaylistGroup";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Group", group);
                _db.AddParameter(cmd, "Zone", zone);
                data = _db.GetList<VideoEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Video EPL
        public bool Insert_By_Feed(VideoEntity videoEntity, string tagIdList, string zoneList, int MapPlayListTime, ref int videoId)
        {
            const string commandText = "VideoCms_Video_InsertByFeed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "Pname", videoEntity.Pname);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "NewsId", videoEntity.NewsId);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "Tags", videoEntity.Tags);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                //CuongNP - Thêm nguồn video
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                if (videoEntity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoEntity.DistributionDate);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneNames", zoneList);
                _db.AddParameter(cmd, "VideoRelationIdList", videoEntity.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", "");
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Size", videoEntity.Size);
                _db.AddParameter(cmd, "Capacity", videoEntity.Capacity);
                _db.AddParameter(cmd, "AllowAd", videoEntity.AllowAd);
                _db.AddParameter(cmd, "IsRemoveLogo", videoEntity.IsRemoveLogo);
                // _db.AddParameter(cmd, "MapPlayListTime", MapPlayListTime);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public bool Video_Insesert_ZoneRecut(int videoId)
        {
            const string commandText = "CMS_Video_ZoneRecut";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Video_UpdateZonePrimary(int videoId, int zoneId)
        {
            const string commandText = "CMS_Video_UpdateZonePrimary";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Video_InsertZone(int videoId, string ZoneIdList)
        {
            const string commandText = "CMS_Video_InsertZone";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "ZoneIdList", ZoneIdList);
                var numberOfRow = cmd.ExecuteNonQuery();
                return videoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Playlist_GetIdByZoneId(int zoneId1, int zoneId2, DateTime DateFrom, DateTime DateTo, int VideoId, ref int PlaylistId)
        {
            const string commandText = "Playlist_GetByZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", PlaylistId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Zone1", zoneId1);
                _db.AddParameter(cmd, "Zone2", zoneId2);
                _db.AddParameter(cmd, "DateFrom", DateFrom);
                _db.AddParameter(cmd, "DateTo", DateTo);
                _db.AddParameter(cmd, "VideoId", VideoId);
                var number = _db.ExecuteNonQuery(cmd);
                PlaylistId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneVideoEntity> ZoneVideo_GetAll()
        {
            const string commandText = "ZoneVideo_GetAll";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                return _db.GetList<ZoneVideoEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Video Api
        public Boolean GetVideoApiById(int videoId)
        {
            const string commandText = "sp_VideoApiSelectByVideoId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "videoId", videoId);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public Boolean InsertVideoApiById(VideoEntity videoEntity)
        {
            const string commandText = "sp_InsertVideoApi";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                // Insert videoId vao cot Pname
                _db.AddParameter(cmd, "videoId", videoEntity.Pname);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "CreatedDate", videoEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "Url", videoEntity.Url);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoApiEntity> GetListVideoApiById(string listVideoId)
        {
            const string commandText = "sp_SelectListVideoApiInlucde";
            try
            {
                List<VideoApiEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "listVideoId", listVideoId);
                data = _db.GetList<VideoApiEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Kenh14VideoApi
        public Boolean Kenh14EPLInsertVideoApi(VideoEntity videoEntity)
        {
            const string commandText = "sp_Kenh14EPL_InsertVideoApi";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                // insert obj data_trailer  to column Htmlcode
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "CreatedDate", videoEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                // Insert type_square to column type
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                // Url check sau
                //_db.AddParameter(cmd, "Url", videoEntity.Url);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public Boolean Kenh14EPLChangerPublishVideoApi(string keyVideo, int status)
        {
            const string commandText = "sp_Kenh14EPL_ChangerPublish_VideoApi";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", keyVideo);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public Boolean Kenh14EPLUpdateVideoApi(VideoEntity videoEntity)
        {
            const string commandText = "sp_Kenh14EPL_UpdateVideoApi";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "KeyVideo", videoEntity.KeyVideo);
                _db.AddParameter(cmd, "ZoneId", videoEntity.ZoneId);
                _db.AddParameter(cmd, "Name", videoEntity.Name);
                _db.AddParameter(cmd, "UnsignName", videoEntity.UnsignName);
                _db.AddParameter(cmd, "Description", videoEntity.Description);
                // update obj data_trailer  to column Htmlcode
                _db.AddParameter(cmd, "HtmlCode", videoEntity.HtmlCode);
                _db.AddParameter(cmd, "Status", videoEntity.Status);
                _db.AddParameter(cmd, "Avatar", videoEntity.Avatar);
                _db.AddParameter(cmd, "Views", videoEntity.Views);
                _db.AddParameter(cmd, "Mode", videoEntity.Mode);
                _db.AddParameter(cmd, "CreatedDate", videoEntity.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoEntity.CreatedBy);
                _db.AddParameter(cmd, "Duration", videoEntity.Duration);
                _db.AddParameter(cmd, "Source", videoEntity.Source);
                _db.AddParameter(cmd, "FileName", videoEntity.FileName);
                _db.AddParameter(cmd, "OriginalId", videoEntity.OriginalId);
                // update type_square to column type
                _db.AddParameter(cmd, "Type", videoEntity.Type);
                var numberOfRow = _db.GetFirtDataRecord(cmd);
                return Utility.ConvertToInt(numberOfRow) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Mp3

        public List<Mp3Entity> SearchMp3(string username, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Mp3_Search";
            try
            {
                List<Mp3Entity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "CreatedBy", username);
                _db.AddParameter(cmd, "Title", keyword);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<Mp3Entity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public Mp3Entity GetMp3ById(int id)
        {

            const string commandText = "VideoCms_Mp3_GetById";
            try
            {
                Mp3Entity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<Mp3Entity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertMp3(Mp3Entity mp3Entity, ref int id)
        {
            const string commandText = "VideoCms_Mp3_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", mp3Entity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", mp3Entity.Name);
                _db.AddParameter(cmd, "UnsignName", mp3Entity.UnsignName);
                _db.AddParameter(cmd, "Description", mp3Entity.Description);
                _db.AddParameter(cmd, "HtmlCode", mp3Entity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", mp3Entity.Avatar);
                _db.AddParameter(cmd, "KeyVideo", mp3Entity.KeyVideo);
                _db.AddParameter(cmd, "Status", mp3Entity.Status);
                _db.AddParameter(cmd, "FileName", mp3Entity.FileName);
                _db.AddParameter(cmd, "CreatedBy", mp3Entity.CreatedBy);
                _db.AddParameter(cmd, "Url", mp3Entity.Url);
                if (mp3Entity.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", mp3Entity.DistributionDate);
                _db.AddParameter(cmd, "Duration", mp3Entity.Duration);
                _db.AddParameter(cmd, "Size", mp3Entity.Size);
                _db.AddParameter(cmd, "Capacity", mp3Entity.Capacity);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return id > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateMp3(Mp3Entity mp3Entity)
        {
            const string commandText = "VideoCms_Mp3_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", mp3Entity.Id);
                _db.AddParameter(cmd, "Title", mp3Entity.Name);
                _db.AddParameter(cmd, "Sapo", mp3Entity.Description);
                _db.AddParameter(cmd, "CreatedBy", mp3Entity.LastModifiedBy);
                _db.AddParameter(cmd, "HtmlCode", mp3Entity.HtmlCode);
                _db.AddParameter(cmd, "Avatar", mp3Entity.Avatar);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        #endregion
    }
}
