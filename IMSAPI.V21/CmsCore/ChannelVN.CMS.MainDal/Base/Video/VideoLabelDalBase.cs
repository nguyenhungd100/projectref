using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoLabelDalBase
    {
        public List<VideoLabelEntity> GetListByParentId(int parentId, int isSystem)
        {
            const string commandText = "VideoCms_VideoLabel_GetListByParentId";
            try
            {
                List<VideoLabelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "IsSystem", isSystem);
                data = _db.GetList<VideoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoLabelEntity> GetListZoneRelation(int videoId)
        {
            const string commandText = "VideoCms_VideoLabel_GetListRelationVideo";
            try
            {
                List<VideoLabelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<VideoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoLabelEntity> GetActiveZoneInDay(string zoneIds, DateTime viewDate, int mode)
        {
            const string commandText = "CMS_VideoLabel_GetActiveZoneInDay";
            try
            {
                List<VideoLabelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "zoneIds", zoneIds);
                _db.AddParameter(cmd, "viewDate", viewDate);
                _db.AddParameter(cmd, "mode", mode);

                data = _db.GetList<VideoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoLabelEntity> GetActiveZoneInDayV2(string zoneIds, DateTime viewDate, int mode)
        {
            const string commandText = "CMS_VideoLabel_GetActiveZoneInDayV2";
            try
            {
                List<VideoLabelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "zoneIds", zoneIds);
                _db.AddParameter(cmd, "viewDate", viewDate);
                _db.AddParameter(cmd, "mode", mode);

                data = _db.GetList<VideoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoModeNotifyEntity> GetNotifyZoneInDay(DateTime viewDate)
        {
            const string commandText = "CMS_Video_GetNewestVideoForNotify";
            try
            {
                List<VideoModeNotifyEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "viewDate", viewDate);

                data = _db.GetList<VideoModeNotifyEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoLabelEntity GetById(int id)
        {
            const string commandText = "VideoCms_VideoLabel_GetById";
            try
            {
                VideoLabelEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VideoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(VideoLabelEntity videoLabel, ref int videoLabelId)
        {

            const string commandText = "VideoCms_VideoLabel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabel.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", videoLabel.ParentId);
                _db.AddParameter(cmd, "Name", videoLabel.Name);
                _db.AddParameter(cmd, "Url", videoLabel.Url);
                _db.AddParameter(cmd, "Avatar", videoLabel.Avatar);
                _db.AddParameter(cmd, "Priority", videoLabel.Priority);
                _db.AddParameter(cmd, "IsSystem", videoLabel.IsSystem);
                _db.AddParameter(cmd, "CreatedDate", videoLabel.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoLabel.CreatedBy);
                _db.AddParameter(cmd, "ModifiedDate", videoLabel.ModifiedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                videoLabelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(VideoLabelEntity videoLabel)
        {

            const string commandText = "VideoCms_VideoLabel_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabel.Id);                
                _db.AddParameter(cmd, "ParentId", videoLabel.ParentId);
                _db.AddParameter(cmd, "Name", videoLabel.Name);
                _db.AddParameter(cmd, "Url", videoLabel.Url);
                _db.AddParameter(cmd, "Avatar", videoLabel.Avatar);
                _db.AddParameter(cmd, "Priority", videoLabel.Priority);
                _db.AddParameter(cmd, "IsSystem", videoLabel.IsSystem);
                _db.AddParameter(cmd, "CreatedDate", videoLabel.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoLabel.CreatedBy);
                _db.AddParameter(cmd, "ModifiedDate", videoLabel.ModifiedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MoveUp(int videoLabelId)
        {

            const string commandText = "VideoCms_VideoLabel_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabelId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MoveDown(int videoLabelId)
        {
            const string commandText = "VideoCms_VideoLabel_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabelId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int videoLabelId)
        {
            const string commandText = "VideoCms_VideoLabel_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabelId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public bool GetVideoLabelIdByNewsZoneId(string newsZoneId, ref int videoLabelId)
        {

            const string commandText = "CMS_VideoLabel_GetByNewsZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoLabelId, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsZoneId", newsZoneId);
                var numberOfRow = cmd.ExecuteNonQuery();
                videoLabelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAvatarVideoLabel(string listAvatar)
        {
            const string commandText = "Cms_VideoLabel_Update_Avatars";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Avatars", listAvatar);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoLabelDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
