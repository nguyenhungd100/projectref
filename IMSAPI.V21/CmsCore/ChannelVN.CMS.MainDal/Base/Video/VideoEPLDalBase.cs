﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoEPLDalBase
    {
        public List<VideoEPLEntity> VideoEPLByZone(int zoneId, DateTime dateFrom, DateTime dateTo)
        {

            const string commandText = "CMS_VideoEPL_Search_By_Zone";
            try
            {
                List<VideoEPLEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (dateFrom < Constants.MinDateTime)
                    _db.AddParameter(cmd, "dateFrom", DBNull.Value);
                else
                    _db.AddParameter(cmd, "dateFrom", dateFrom);
                if (dateTo < Constants.MinDateTime)
                    _db.AddParameter(cmd, "dateTo", DBNull.Value);
                else
                    _db.AddParameter(cmd, "dateTo", dateTo);
                data = _db.GetList<VideoEPLEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<VideoEPLZoneToZoneEntity> VideoEPLZoneAndZone(int zoneId1, int zoneId2, DateTime dateFrom, DateTime dateTo)
        {

            const string commandText = "CMS_VideoEPL_Search_Zone_And_Zone";
            try
            {
                List<VideoEPLZoneToZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId1", zoneId1);
                _db.AddParameter(cmd, "ZoneId2", zoneId2);
                if (dateFrom < Constants.MinDateTime)
                    _db.AddParameter(cmd, "dateFrom", DBNull.Value);
                else
                    _db.AddParameter(cmd, "dateFrom", dateFrom);
                if (dateTo < Constants.MinDateTime)
                    _db.AddParameter(cmd, "dateTo", DBNull.Value);
                else
                    _db.AddParameter(cmd, "dateTo", dateTo);
                data = _db.GetList<VideoEPLZoneToZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<EPLPlayListEntity> EPLPlayListGetAll()
        {

            const string commandText = "CMS_EPLPlaylist_GetAll";
            try
            {
                List<EPLPlayListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<EPLPlayListEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<EPLPlayListEntity> EPLPlayListGetByUser(string userName)
        {

            const string commandText = "CMS_EPLPlaylist_GetByUser";
            try
            {
                List<EPLPlayListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserName", userName);
                data = _db.GetList<EPLPlayListEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(string userName, int videoId)
        {
            const string commandText = "CMS_EPLPlaylist_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "UserName", userName);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int userId, int videoId)
        {
            const string commandText = "CMS_EPLPlaylist_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "UserId", userId);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoEPLDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
