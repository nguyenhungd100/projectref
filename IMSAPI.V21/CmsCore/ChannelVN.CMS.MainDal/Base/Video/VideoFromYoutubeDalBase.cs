﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoFromYoutubeDalBase
    {
        public bool InsertVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList, ref int id)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", video.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "AccountName", videoFromYoutube.AccountName);
                _db.AddParameter(cmd, "SourceVideoUrl", videoFromYoutube.SourceVideoUrl);
                _db.AddParameter(cmd, "ZoneId", video.ZoneId);

                _db.AddParameter(cmd, "Name", video.Name);
                _db.AddParameter(cmd, "UnsignName", video.UnsignName);
                _db.AddParameter(cmd, "Description", video.Description);
                _db.AddParameter(cmd, "Avatar", video.Avatar);
                _db.AddParameter(cmd, "Mode", video.Mode);
                _db.AddParameter(cmd, "Tags", video.Tags);
                _db.AddParameter(cmd, "Url", video.Url);

                if (video.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", video.DistributionDate);

                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", video.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);

                _db.AddParameter(cmd, "AllowAd", video.AllowAd);



                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateVideoFromYoutube(VideoFromYoutubeEntity videoFromYoutube, VideoEntity video, string tagIdList, string zoneIdList, string playlistIdList)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ZoneId", video.ZoneId);

                _db.AddParameter(cmd, "Name", video.Name);
                _db.AddParameter(cmd, "UnsignName", video.UnsignName);
                _db.AddParameter(cmd, "Description", video.Description);
                _db.AddParameter(cmd, "Avatar", video.Avatar);
                _db.AddParameter(cmd, "Mode", video.Mode);
                _db.AddParameter(cmd, "Tags", video.Tags);
                _db.AddParameter(cmd, "LastModifiedBy", video.LastModifiedBy);

                if (video.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", video.DistributionDate);

                _db.AddParameter(cmd, "Url", video.Url);

                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "VideoRelationIdList", video.VideoRelation);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);

                _db.AddParameter(cmd, "AllowAd", video.AllowAd);


                _db.AddParameter(cmd, "Id", video.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateVideoFromYoutubeStatus(int id, string avatar, string filePath, int status, string htmlCode, string keyVideo, string pname, string fileName, string duration, string size, int capacity)
        {
            const string commandText = "VideoCms_VideoFromYoutube_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", id);

                _db.AddParameter(cmd, "FilePath", filePath);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Avatar", avatar);
                _db.AddParameter(cmd, "HtmlCode", htmlCode);
                _db.AddParameter(cmd, "KeyVideo", keyVideo);
                _db.AddParameter(cmd, "Pname", pname);
                _db.AddParameter(cmd, "FileName", fileName);
                _db.AddParameter(cmd, "Duration", duration);

                _db.AddParameter(cmd, "Size", size);
                _db.AddParameter(cmd, "Capacity", capacity);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteVideoFromYoutube(int id)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                cmd.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishVideoFromYoutube(int id, string publishedBy)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SendVideoFromYoutube(int id)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoFromYoutubeEntity GetVideoFromYoutubeById(int id)
        {
            const string commandText = "VideoCms_VideoFromYoutube_GetById";
            try
            {
                VideoFromYoutubeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VideoFromYoutubeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VideoFromYoutubeEntity GetBySourceVideoUrl(string sourceVideoUrl)
        {
            const string commandText = "VideoCms_VideoFromYoutube_GetBySourceVideoUrl";
            try
            {
                VideoFromYoutubeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SourceVideoUrl", sourceVideoUrl);
                data = _db.Get<VideoFromYoutubeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoFromYoutubeDetailEntity> Search(string username, string keyword, int status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_VideoFromYoutube_Search";
            try
            {
                List<VideoFromYoutubeDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoFromYoutubeDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoFromYoutubeEntity> GetUploadingVideo()
        {
            const string commandText = "VideoCms_VideoFromYoutube_GetUploading";
            try
            {
                List<VideoFromYoutubeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<VideoFromYoutubeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoFromYoutubeDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
