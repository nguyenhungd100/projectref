﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class BoxVideoEmbedByNewsZoneDalBase
    {
        public List<BoxVideoEmbedByNewsZoneEntity> GetListVideoEmbed(int zoneId, int type)
        {

            const string commandText = "CMS_BoxVideoEmbedByNewsZone_GetListByZone";
            try
            {
                List<BoxVideoEmbedByNewsZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxVideoEmbedByNewsZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(BoxVideoEmbedByNewsZoneEntity VideoEmbebBox)
        {
            const string commandText = "CMS_BoxVideoEmbedByNewsZone_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", VideoEmbebBox.ZoneId);
                _db.AddParameter(cmd, "VideoId", VideoEmbebBox.VideoId);
                _db.AddParameter(cmd, "SortOrder", VideoEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", VideoEmbebBox.Type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(string listVideoId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxVideoEmbedByNewsZone_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listVideoId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Delete(long VideoId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxVideoEmbedByNewsZone_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoID", VideoId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxVideoEmbedByNewsZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
