﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class BoxVideoEmbedDalBase
    {
        #region BoxVideoEmbed
        public List<BoxVideoEmbedEntity> GetListVideoEmbed(int zoneId, int type)
        {

            const string commandText = "CMS_BoxVideoEmbed_GetListByZone";
            try
            {
                List<BoxVideoEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxVideoEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(BoxVideoEmbedEntity VideoEmbebBox)
        {
            const string commandText = "CMS_BoxVideoEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", VideoEmbebBox.ZoneId);
                _db.AddParameter(cmd, "VideoId", VideoEmbebBox.VideoId);
                _db.AddParameter(cmd, "SortOrder", VideoEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", VideoEmbebBox.Type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(string listVideoId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxVideoEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listVideoId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateEpl(BoxVideoEmbedEplEntity embed)
        {
            const string commandText = "CMS_BoxVideoEmbed_UpdateEmbedEpl";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Avatar", embed.Avatar);
                _db.AddParameter(cmd, "Embed", embed.Embed);
                _db.AddParameter(cmd, "Name", embed.Name);
                _db.AddParameter(cmd, "SortOrder", embed.SortOrder);
                _db.AddParameter(cmd, "Type", embed.Type);
                _db.AddParameter(cmd, "ZoneId", embed.ZoneId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Delete(long VideoId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxVideoEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoID", VideoId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BoxPlayEmbed
        public List<BoxPlayEmbedEntity> GetListPlayEmbed(int zoneId, int type, int typeId)
        {

            const string commandText = "CMS_BoxPlayEmbed_GetListByZone";
            try
            {
                List<BoxPlayEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "TypeId", typeId);

                data = _db.GetList<BoxPlayEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool InsertPlay(BoxPlayEmbedEntity boxPlayEmbeb)
        {
            const string commandText = "CMS_BoxPlayEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", boxPlayEmbeb.Name);
                _db.AddParameter(cmd, "ZoneId", boxPlayEmbeb.ZoneId);
                _db.AddParameter(cmd, "Type", boxPlayEmbeb.Type);
                _db.AddParameter(cmd, "VideoId", boxPlayEmbeb.VideoId);
                _db.AddParameter(cmd, "Order", boxPlayEmbeb.Order);
                _db.AddParameter(cmd, "Title", boxPlayEmbeb.Title);
                _db.AddParameter(cmd, "Avatar", boxPlayEmbeb.Avatar);
                _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                _db.AddParameter(cmd, "DisplayStyle", boxPlayEmbeb.DisplayStyle);
                _db.AddParameter(cmd, "Priority", boxPlayEmbeb.Priority);

                _db.AddParameter(cmd, "TypeId", boxPlayEmbeb.TypeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        
        public bool DeletePlay(int zoneId, int type, int typeId)
        {
            const string commandText = "CMS_BoxPlayEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "TypeId", typeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BoxVideoHighLightEmbed
        public List<BoxVideoHighlightEmbedEntity> GetVideoHighLightEmbed(int typeId)
        {

            const string commandText = "CMS_BoxVideoHighlightEmbed_GetListByTypeId";
            try
            {
                List<BoxVideoHighlightEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "TypeId", typeId);

                data = _db.GetList<BoxVideoHighlightEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool InsertVideoHighLight(BoxVideoHighlightEmbedEntity boxPlayEmbeb)
        {
            const string commandText = "CMS_BoxVideoHighlightEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Type", boxPlayEmbeb.Type);
                _db.AddParameter(cmd, "VideoId", boxPlayEmbeb.VideoId);
                _db.AddParameter(cmd, "Order", boxPlayEmbeb.Order);
                _db.AddParameter(cmd, "Title", boxPlayEmbeb.Title);
                _db.AddParameter(cmd, "Avatar", boxPlayEmbeb.Avatar);
                _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                _db.AddParameter(cmd, "Url", boxPlayEmbeb.Url);

                _db.AddParameter(cmd, "TypeId", boxPlayEmbeb.TypeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool DeleteVideoHighLight(int typeId)
        {
            const string commandText = "CMS_BoxVideoHighlightEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BoxProgramHotHomeEmbed and mục
        public List<BoxProgramHotHomeEmbedEntity> GetProgramHotHomeEmbed(int typeId, int zoneId)
        {

            const string commandText = "CMS_BoxProgramHotHomeEmbed_GetListByZone";
            try
            {
                List<BoxProgramHotHomeEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "TypeId", typeId);

                data = _db.GetList<BoxProgramHotHomeEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool InsertProgramHotHome(BoxProgramHotHomeEmbedEntity boxPlayEmbeb)
        {
            const string commandText = "CMS_BoxProgramHotHomeEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", boxPlayEmbeb.Name);
                _db.AddParameter(cmd, "ZoneId", boxPlayEmbeb.ZoneId);
                _db.AddParameter(cmd, "Type", boxPlayEmbeb.Type);
                _db.AddParameter(cmd, "VideoId", boxPlayEmbeb.VideoId);
                _db.AddParameter(cmd, "Order", boxPlayEmbeb.Order);
                _db.AddParameter(cmd, "Title", boxPlayEmbeb.Title);
                _db.AddParameter(cmd, "Avatar", boxPlayEmbeb.Avatar);
                _db.AddParameter(cmd, "LastModifiedDate", DateTime.Now);
                _db.AddParameter(cmd, "Url", boxPlayEmbeb.Url);

                _db.AddParameter(cmd, "TypeId", boxPlayEmbeb.TypeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool DeleteProgramHotHome(int typeId, int zoneId)
        {
            const string commandText = "CMS_BoxProgramHotHomeEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "TypeId", typeId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
        

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxVideoEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
