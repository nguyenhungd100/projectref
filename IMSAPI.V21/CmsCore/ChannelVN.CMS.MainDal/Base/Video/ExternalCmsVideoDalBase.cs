﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class ExternalCmsVideoDalBase
    {
        public List<VideoEntity> Search(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Video_SearchFromExternal";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxVideoEmbedEntity> GetListVideoEmbed(int zoneId, int type)
        {

            const string commandText = "CMS_BoxVideoEmbed_GetListByZone";
            try
            {
                List<BoxVideoEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxVideoEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public VideoEntity GetById(int videoId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetById";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsVideoDb _db;

        protected ExternalCmsVideoDalBase(ExternalCmsVideoDb db)
        {
            _db = db;
        }

        protected ExternalCmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
