﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class PlaylistDalBase
    {
        #region Update
       
        public bool Insert(PlaylistEntity playlistEntity,string zoneIdList, ref int playlistId)
        {

            const string commandText = "VideoCms_Playlist_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", playlistEntity.ZoneId);
                _db.AddParameter(cmd, "Name", playlistEntity.Name);
                _db.AddParameter(cmd, "UnsignName", playlistEntity.UnsignName);
                _db.AddParameter(cmd, "Description", playlistEntity.Description);
                _db.AddParameter(cmd, "Status", playlistEntity.Status);
                _db.AddParameter(cmd, "Mode", playlistEntity.Mode);
                _db.AddParameter(cmd, "Avatar", playlistEntity.Avatar);
                _db.AddParameter(cmd, "Priority", playlistEntity.Priority);
                _db.AddParameter(cmd, "Url", playlistEntity.Url);
                _db.AddParameter(cmd, "VideoCount", playlistEntity.VideoCount);
                _db.AddParameter(cmd, "CreatedBy", playlistEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", playlistEntity.CreatedDate);
                _db.AddParameter(cmd, "DistributionDate", playlistEntity.DistributionDate);                
                _db.AddParameter(cmd, "IntroClip", playlistEntity.IntroClip);
                _db.AddParameter(cmd, "PlaylistRelation", playlistEntity.PlaylistRelation);
                _db.AddParameter(cmd, "Cover", playlistEntity.Cover);
                _db.AddParameter(cmd, "MetaJson", playlistEntity.MetaJson);
                _db.AddParameter(cmd, "MetaAvatar", playlistEntity.MetaAvatar);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);

                var numberOfRow = cmd.ExecuteNonQuery();
                playlistId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        

        public bool Update(PlaylistEntity playlistEntity, string zoneIdList)
        {
            const string commandText = "VideoCms_Playlist_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", playlistEntity.ZoneId);
                _db.AddParameter(cmd, "Name", playlistEntity.Name);
                _db.AddParameter(cmd, "UnsignName", playlistEntity.UnsignName);
                _db.AddParameter(cmd, "Description", playlistEntity.Description);
                _db.AddParameter(cmd, "Status", playlistEntity.Status);
                _db.AddParameter(cmd, "Mode", playlistEntity.Mode);
                _db.AddParameter(cmd, "Avatar", playlistEntity.Avatar);
                _db.AddParameter(cmd, "Priority", playlistEntity.Priority);
                _db.AddParameter(cmd, "Url", playlistEntity.Url);
                _db.AddParameter(cmd, "VideoCount", playlistEntity.VideoCount);
                _db.AddParameter(cmd, "EditedBy", playlistEntity.EditedBy);
                _db.AddParameter(cmd, "EditedDate", playlistEntity.EditedDate);
                _db.AddParameter(cmd, "LastModifiedBy", playlistEntity.LastModifiedBy);
                _db.AddParameter(cmd, "LastModifiedDate", playlistEntity.LastModifiedDate);
                _db.AddParameter(cmd, "DistributionDate", playlistEntity.DistributionDate);
                _db.AddParameter(cmd, "IntroClip", playlistEntity.IntroClip);
                _db.AddParameter(cmd, "PlaylistRelation", playlistEntity.PlaylistRelation);
                _db.AddParameter(cmd, "Cover", playlistEntity.Cover);
                _db.AddParameter(cmd, "MetaJson", playlistEntity.MetaJson);
                _db.AddParameter(cmd, "MetaAvatar", playlistEntity.MetaAvatar);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "Id", playlistEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool RemoveVideoOutOfPlaylist(int playlistId, string videoIdList)
        {
            const string commandText = "VideoCms_Playlist_RemoveVideoList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);
                _db.AddParameter(cmd, "PlaylistId", playlistId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool ChangeStatus(int playlistId, int status)
        {
            const string commandText = "VideoCms_Playlist_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);
                _db.AddParameter(cmd, "Status", status);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeMode(int playlistId, int mode)
        {
            const string commandText = "VideoCms_Playlist_ChangeMode";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);
                _db.AddParameter(cmd, "Mode", mode);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateVideoPriorotyInPlayList(int playlistId, VideoPriorityEntity item)
        {
            const string commandText = "VideoCms_Playlist_UpdateVideoPriorotyInPlayList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlayListId", playlistId);
                _db.AddParameter(cmd, "VideoId", item.VideoId);
                _db.AddParameter(cmd, "Priority", item.Priority);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ChangeOrder(int playlistId, int videoId, int priority)
        {
            const string commandText = "VideoCms_Playlist_ChangeOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "Priority", priority);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool ChangeOrderByIdList(int playlistId, string playlistIdList, int priority)
        {
            const string commandText = "VideoCms_Playlist_ChangeOrderList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "VideoIdList", playlistIdList);
                _db.AddParameter(cmd, "Priority", priority);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool AddVideo(int videoId, int playlistId, int priority)
        {
            const string commandText = "VideoCms_Playlist_AddVideo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                _db.AddParameter(cmd, "PlaylistId", playlistId);
                _db.AddParameter(cmd, "Priority", priority);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool AddVideoList(string addVideoIds, string deleteVideoIds, int playlistId)
        {
            const string commandText = "VideoCms_Playlist_AddVideoList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AddVideoIdList", addVideoIds);
                _db.AddParameter(cmd, "DeleteVideoIdList", deleteVideoIds);
                _db.AddParameter(cmd, "PlaylistId", playlistId);                

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteById(int playlistId)
        {
            const string commandText = "VideoCms_Playlist_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool DeleteByIdList(string playlistIdList)
        {
            const string commandText = "VideoCms_Playlist_DeleteByIdList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistIdList", playlistIdList);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Publish(int playlistId, string userDoAction)
        {
            const string commandText = "VideoCms_Playlist_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Unpublish(int playlistId)
        {
            const string commandText = "VideoCms_Playlist_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MoveTrash(int playlistId, string username)
        {
            const string commandText = "VideoCms_Playlist_MoveTrash";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playlistId);
                _db.AddParameter(cmd, "UserName", username);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Send(int playListId, string userDoAction)
        {
            const string commandText = "VideoCms_PlayList_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playListId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);

                var numberOfRow = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Return(int playListId)
        {
            const string commandText = "VideoCms_PlayList_Return";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", playListId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public PlaylistEntity GetById(int playlistId)
        {
            const string commandText = "VideoCms_Playlist_GetById";
            try
            {
                PlaylistEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlaylistID", playlistId);
                data = _db.Get<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetListByVideoId(int videoId)
        {
            const string commandText = "VideoCms_Playlist_GetListByVideoId";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetListByVideoChannelId(int videoChannelId)
        {
            const string commandText = "VideoCms_Playlist_GetListByVideoChannelId";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", videoChannelId);
                data = _db.GetList<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetListPaging(int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {

            const string commandText = "VideoCms_Playlist_GetListPaging";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PlaylistEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetMyPlaylist(string username, int pageIndex, int pageSize, int status, string keyword, int sortOder, ref int totalRow)
        {
            const string commandText = "VideoCms_Playlist_GetMyList";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PlaylistEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> Search(string username, int zoneVideoId, int status, string keyword, int mode, int sortOder, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_Playlist_Search";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneVideoId", zoneVideoId);
                _db.AddParameter(cmd, "UserName", username);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Mode", mode);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "SortOrder", sortOder);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PlaylistEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistCountFollowEntity> CountVideoFollow(string playListIds)
        {

            const string commandText = "VideoCms_Playlist_CountVideoFollow";
            try
            {
                List<PlaylistCountFollowEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PlayListIds", playListIds);
                data = _db.GetList<PlaylistCountFollowEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistCounterEntity> GetPlaylistCounter()
        {

            const string commandText = "VideoCms_Playlist_GetPlaylistCounter";
            try
            {
                List<PlaylistCounterEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<PlaylistCounterEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetLastDaysHavePlaylist(DateTime fromDate, DateTime toDate, int group, int team, int mode)
        {
            const string commandText = "CMS_Playlist_GetLastDaysHavePlaylist";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (fromDate == DateTime.MinValue)
                {
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                }
                else
                    _db.AddParameter(cmd, "FromDate", fromDate);
                if (toDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "ToDate", toDate);
                else
                    _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Group", group);
                _db.AddParameter(cmd, "Team", team);
                _db.AddParameter(cmd, "Mode", mode);
                data = _db.GetList<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistInZoneEntity> GetZoneByPlaylist(int playlistId)
        {
            const string commandText = "CMS_Playlist_GetZoneByPlaylist";
            try
            {
                List<PlaylistInZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "playlistId", playlistId);
                data = _db.GetList<PlaylistInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetTopDayHaveVideo(int top)
        {
            const string commandText = "CMS_Playlist_GetTopDayHaveVideo";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Playlist_Update_DistributionDate(string ListDataPlayList)
        {
            const string commandText = "CMS_Playlist_UpdateDistributionDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListData", ListDataPlayList);
                var data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PlaylistGroupEntity PlaylistGroup_Current(DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_PlaylistGroup_Current";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                return _db.Get<PlaylistGroupEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> InitRedisAllPlayList(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = "CMS_PlayList_InitRedisAllPlayList";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<PlaylistEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PlaylistEntity> GetListByIdList(string playListIdList)
        {

            const string commandText = "CMS_PlayList_GetListByIdList";
            try
            {
                List<PlaylistEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IdList", playListIdList);
                data = _db.GetList<PlaylistEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsVideoDb _db;

        protected PlaylistDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
