﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoDistributionDalBase
    {

        #region Gets
        public VideoEntity GetById(int videoId, ref string zoneName)
        {

            const string commandText = "VideoCms_Video_GetById";
            try
            {
                VideoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.Get<VideoEntity>(cmd);

                if (data != null)
                    zoneName = data.ZoneName != null ? data.ZoneName : string.Empty;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VideoEntity> SearchVideoDistribution(string zoneVideoIds, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_VideoDistribution_Search";
            try
            {
                List<VideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneVideoIds", zoneVideoIds);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion


        #region Core members

        private readonly CmsVideoDistributionDb _db;

        protected VideoDistributionDalBase(CmsVideoDistributionDb db)
        {
            _db = db;
        }

        protected CmsVideoDistributionDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
