﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoThreadDalBase
    {
        public List<VideoThreadEntity> Search(string keyword, bool isHotThread, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "VideoCms_VideoThread_Search";
            try
            {
                List<VideoThreadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotThread", isHotThread);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<VideoThreadEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public VideoThreadEntity GetById(int id)
        {
            const string commandText = "VideoCms_VideoThread_GetById";
            try
            {
                VideoThreadEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<VideoThreadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(VideoThreadEntity videoThread, string videoIdList, ref int videoThreadId)
        {

            const string commandText = "VideoCms_VideoThread_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", videoThread.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", videoThread.Name);
                _db.AddParameter(cmd, "Description", videoThread.Description);
                _db.AddParameter(cmd, "Url", videoThread.Url);
                _db.AddParameter(cmd, "IsHotThread", videoThread.IsHotThread);

                if (videoThread.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoThread.DistributionDate);

                _db.AddParameter(cmd, "CreatedDate", videoThread.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", videoThread.CreatedBy);
                _db.AddParameter(cmd, "UnsignName", videoThread.UnsignName);
                _db.AddParameter(cmd, "Avatar", videoThread.Avatar);
                _db.AddParameter(cmd, "Status", videoThread.Status);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);


                var numberOfRow = cmd.ExecuteNonQuery();

                videoThreadId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(VideoThreadEntity videoThread, string videoIdList)
        {
            const string commandText = "VideoCms_VideoThread_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoThread.Id);

                _db.AddParameter(cmd, "Name", videoThread.Name);
                _db.AddParameter(cmd, "Description", videoThread.Description);
                _db.AddParameter(cmd, "Url", videoThread.Url);
                _db.AddParameter(cmd, "IsHotThread", videoThread.IsHotThread);

                if (videoThread.DistributionDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else
                    _db.AddParameter(cmd, "DistributionDate", videoThread.DistributionDate);

                _db.AddParameter(cmd, "ModifiedDate", videoThread.ModifiedDate);
                _db.AddParameter(cmd, "EditedBy", videoThread.EditedBy);
                _db.AddParameter(cmd, "UnsignName", videoThread.UnsignName);
                _db.AddParameter(cmd, "Avatar", videoThread.Avatar);
                _db.AddParameter(cmd, "Status", videoThread.Status);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int videoThreadId)
        {
            const string commandText = "VideoCms_VideoThread_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoThreadId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateVideoInVideoThread(int videoThreadId, string videoIdList)
        {
            const string commandText = "VideoCms_VideoInVideoThread_UpdateVideoList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoThreadId", videoThreadId);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
           
        }

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoThreadDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
