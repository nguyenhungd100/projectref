﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class VideoChannelDalBase
    {        
        #region insert update

        public bool Insert(VideoChannelEntity videoChannelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList, ref int videoChannelId)
        {
            const string commandText = "CMS_VideoChannel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoChannelEntity.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", videoChannelEntity.ZoneId);
                _db.AddParameter(cmd, "PublisherId", videoChannelEntity.PublisherId);                
                _db.AddParameter(cmd, "Name", videoChannelEntity.Name);
                _db.AddParameter(cmd, "Avatar", videoChannelEntity.Avatar);
                _db.AddParameter(cmd, "Priority", videoChannelEntity.Priority);                                
                _db.AddParameter(cmd, "CreatedBy", videoChannelEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", videoChannelEntity.CreatedDate);
                _db.AddParameter(cmd, "Url", videoChannelEntity.Url);
                _db.AddParameter(cmd, "FollowCount", videoChannelEntity.FollowCount);
                _db.AddParameter(cmd, "VideoCount", videoChannelEntity.VideoCount);
                _db.AddParameter(cmd, "Rank", videoChannelEntity.Rank);
                _db.AddParameter(cmd, "IntroClip", videoChannelEntity.IntroClip);
                _db.AddParameter(cmd, "Status", videoChannelEntity.Status);                               
                _db.AddParameter(cmd, "LastModifiedBy", videoChannelEntity.LastModifiedBy);
                _db.AddParameter(cmd, "LastModifiedDate", videoChannelEntity.LastModifiedDate);
                _db.AddParameter(cmd, "Cover", videoChannelEntity.Cover);
                _db.AddParameter(cmd, "Description", videoChannelEntity.Description);
                _db.AddParameter(cmd, "ChannelRelation", videoChannelEntity.ChannelRelation);

                _db.AddParameter(cmd, "MetaJson", videoChannelEntity.MetaJson);
                _db.AddParameter(cmd, "Mode", videoChannelEntity.Mode);
                _db.AddParameter(cmd, "MetaAvatar", videoChannelEntity.MetaAvatar);

                _db.AddParameter(cmd, "LabelId", videoChannelEntity.LabelId);                
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "LabelIdList", labelIdList);
                _db.AddParameter(cmd, "PlayListIdList", playlistIdList);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);                

                var numberOfRow = cmd.ExecuteNonQuery();
                videoChannelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoChannelId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(VideoChannelEntity videoChannelEntity, string zoneIdList, string labelIdList, string playlistIdList, string videoIdList)
        {
            const string commandText = "CMS_VideoChannel_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "PublisherId", videoChannelEntity.PublisherId);
                _db.AddParameter(cmd, "Name", videoChannelEntity.Name);
                _db.AddParameter(cmd, "Avatar", videoChannelEntity.Avatar);
                _db.AddParameter(cmd, "Description", videoChannelEntity.Description);
                _db.AddParameter(cmd, "Priority", videoChannelEntity.Priority);
                _db.AddParameter(cmd, "CreatedBy", videoChannelEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", videoChannelEntity.CreatedDate);
                _db.AddParameter(cmd, "Url", videoChannelEntity.Url);
                _db.AddParameter(cmd, "FollowCount", videoChannelEntity.FollowCount);
                _db.AddParameter(cmd, "VideoCount", videoChannelEntity.VideoCount);
                _db.AddParameter(cmd, "Rank", videoChannelEntity.Rank);
                _db.AddParameter(cmd, "IntroClip", videoChannelEntity.IntroClip);
                _db.AddParameter(cmd, "ChannelRelation", videoChannelEntity.ChannelRelation);
                _db.AddParameter(cmd, "Status", videoChannelEntity.Status);
                _db.AddParameter(cmd, "LastModifiedBy", videoChannelEntity.LastModifiedBy);
                _db.AddParameter(cmd, "LastModifiedDate", videoChannelEntity.LastModifiedDate);
                _db.AddParameter(cmd, "Cover", videoChannelEntity.Cover);

                _db.AddParameter(cmd, "ZoneId", videoChannelEntity.ZoneId);
                _db.AddParameter(cmd, "LabelId", videoChannelEntity.LabelId);

                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "LabelIdList", labelIdList);
                _db.AddParameter(cmd, "PlayListIdList", playlistIdList);
                _db.AddParameter(cmd, "VideoIdList", videoIdList);

                _db.AddParameter(cmd, "MetaJson", videoChannelEntity.MetaJson);
                _db.AddParameter(cmd, "Mode", videoChannelEntity.Mode);
                _db.AddParameter(cmd, "MetaAvatar", videoChannelEntity.MetaAvatar);

                _db.AddParameter(cmd, "Id", videoChannelEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int channelId)
        {
            const string commandText = "VideoCms_VideoChannel_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", channelId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public VideoChannelEntity GetById(int id)
        {

            const string commandText = "CMS_VideoChannel_GetById";
            try
            {                
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
               return _db.Get<VideoChannelEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoChannelEntity> GetVideoChannelByVideoId(int videoId)
        {
            const string commandText = "CMS_VideoChannel_GetVideoChannelByVideoId";
            try
            {
                List<VideoChannelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VideoId", videoId);
                data = _db.GetList<VideoChannelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoChannelEntity> InitAllVideoChannel(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = "CMS_VideoChannel_InitAllVideoChannel";
            try
            {
                List<VideoChannelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<VideoChannelEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Search



        #endregion

        #region Core members

        private readonly CmsVideoDb _db;

        protected VideoChannelDalBase(CmsVideoDb db)
        {
            _db = db;
        }

        protected CmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
