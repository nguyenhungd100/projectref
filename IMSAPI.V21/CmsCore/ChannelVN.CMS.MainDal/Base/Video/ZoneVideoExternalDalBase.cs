using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Video
{
    public abstract class ZoneVideoExternalDalBase
    {
        public List<ZoneVideoEntity> GetListByParentId(int parentId, int status)
        {
            const string commandText = "VideoCms_ZoneVideo_GetListByParentId";
            try
            {
                List<ZoneVideoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<ZoneVideoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly ExternalCmsVideoDb _db;

        protected ZoneVideoExternalDalBase(ExternalCmsVideoDb db)
        {
            _db = db;
        }

        protected ExternalCmsVideoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
