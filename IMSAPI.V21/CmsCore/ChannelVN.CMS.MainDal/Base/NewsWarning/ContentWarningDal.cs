﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsWarning
{
    public class ContentWarningDal : ContentWarningDalBase
    {
        internal ContentWarningDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
