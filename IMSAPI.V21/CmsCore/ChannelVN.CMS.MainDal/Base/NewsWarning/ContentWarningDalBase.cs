﻿using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.MainDal.Base.NewsWarning
{
    public class ContentWarningDalBase
    {
        public bool ContentWarningLog_Insert(long newsId, string Account)
        {
            const string commandText = "CMS_ContentWarningLog_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Account", Account);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ContentWarningEntity contentWarning)
        {
            const string commandText = "CMS_ContentWarning_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", contentWarning.NewsId);
                _db.AddParameter(cmd, "Level", contentWarning.Level);
                _db.AddParameter(cmd, "LimitedCategory", contentWarning.LimitedCategory);
                _db.AddParameter(cmd, "ListTopic", contentWarning.ListTopic);
                _db.AddParameter(cmd, "ListEnity", contentWarning.ListEnity);
                _db.AddParameter(cmd, "ListKeyword", contentWarning.ListKeyword);
                _db.AddParameter(cmd, "ListTopicKeyword", contentWarning.ListTopicKeyword);
                _db.AddParameter(cmd, "Status", contentWarning.Status);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ContentWarningEntity> GetListByLastModifiedDate(DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_ContentWarning_GetListByLastModifiedDate";
            try
            {
                List<ContentWarningEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                if (fromDate <= Constants.MinDateTime)
                {
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                }
                else
                {
                    _db.AddParameter(cmd, "FromDate", fromDate);
                }
                if (toDate <= Constants.MinDateTime)
                {
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                }
                else
                {
                    _db.AddParameter(cmd, "ToDate", toDate);
                }
                data = _db.GetList<ContentWarningEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ContentWarningEntity GetByNewsId(long newsId)
        {
            const string commandText = "CMS_ContentWarning_GetByNewsId";
            try
            {
                ContentWarningEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);

                data = _db.Get<ContentWarningEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ContentWarningEntity> GetByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_ContentWarning_GetByListNewsId";
            try
            {
                List<ContentWarningEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);

                data = _db.GetList<ContentWarningEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ContentWarningLogEntity> ContentWarningLog_GetByListNewsId(string listNewsId)
        {
            const string commandText = "CMS_ContentWarningLog_GetByListNewsId";
            try
            {
                List<ContentWarningLogEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);

                data = _db.GetList<ContentWarningLogEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ContentWarningLogEntity> ContentWarningLog_GetByNewsId(long NewsId)
        {
            const string commandText = "CMS_ContentWarningLog_GetByNewsId";
            try
            {
                List<ContentWarningLogEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", NewsId);

                data = _db.GetList<ContentWarningLogEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected ContentWarningDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
