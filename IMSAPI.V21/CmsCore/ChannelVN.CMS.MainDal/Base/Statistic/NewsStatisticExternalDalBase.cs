﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Statistic
{
    public abstract class NewsStatisticExternalDalBase
    {
        public List<NewsStatisticTotalProduction> GetStatisticNewsCommentFromExternalDB(string zoneId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Comment_GetCommentStatistic";
            try
            {
                List<NewsStatisticTotalProduction> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<NewsStatisticTotalProduction>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsForExportEntity> StatisticsExportNewsHasManyCommentsData(string zoneIds, int top, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Statistics_ExportNewsHasManyCommentsV2";
            try
            {
                List<NewsForExportEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ObjectIds", zoneIds);
                _db.AddParameter(cmd, "Top", top);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<NewsForExportEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly ExternalCmsDb _db;

        protected NewsStatisticExternalDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion


    }
}
