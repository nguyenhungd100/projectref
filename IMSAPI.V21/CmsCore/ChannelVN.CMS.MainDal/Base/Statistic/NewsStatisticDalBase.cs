﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Statistic
{
    public abstract class NewsStatisticDalBase
    {

        public List<NewsStatisticEntity> GetStatisticStatusNewsByAccount(string account, int status, DateTime startDate, DateTime endDate)
        {
            const string commandText = "CMS_News_StatisticStatusNewsByAccount";
            try
            {
                List<NewsStatisticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CreatedBy", account);
                _db.AddParameter(cmd, "Status", status);

                if (startDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", startDate);

                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", endDate);


                data = _db.GetList<NewsStatisticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<NewsStatisticEntity> GetStatisticStatusNewsByAccountAdmin(string createdBy, int status, DateTime startDate, DateTime endDate)
        {
            const string commandText = "CMS_News_StatisticStatusNewsByAccountAdmin";
            try
            {
                List<NewsStatisticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);

                if (startDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "StartDate", DBNull.Value);
                else _db.AddParameter(cmd, "StartDate", startDate);

                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "EndDate", DBNull.Value);
                else _db.AddParameter(cmd, "EndDate", endDate);
                data = _db.GetList<NewsStatisticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction(string zoneId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_StatisticNewsTotalProductionByDate";
            try
            {
                List<NewsStatisticTotalProduction> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<NewsStatisticTotalProduction>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ViewCountByUserEntity> GetUserViewCount(DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetViewCountForUser";
            try
            {
                List<ViewCountByUserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<ViewCountByUserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsStatisticTotalProduction> GetStatisticNewsComment(string zoneId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_StatisticNewsComment";
            try
            {
                List<NewsStatisticTotalProduction> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", toDate);

                data = _db.GetList<NewsStatisticTotalProduction>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<NewsStatisticTotalProduction> GetStatisticNewsCommentFromExternalDB(string zoneId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Comment_GetCommentStatistic";
            try
            {
                List<NewsStatisticTotalProduction> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<NewsStatisticTotalProduction>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsStatisticTotalProduction> GetStatisticNewsTotalProductionUser(string zoneId, DateTime fromDate, DateTime toDate, string userName)
        {
            const string commandText = "CMS_News_StatisticNewsTotalProductionUserByDate";
            try
            {
                List<NewsStatisticTotalProduction> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", toDate);

                _db.AddParameter(cmd, "userName", userName);
                data = _db.GetList<NewsStatisticTotalProduction>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<NewsStatisticSourceEntity> GetStatisticNewsBySourceId(int sourceId, DateTime startDate, DateTime endDate)
        {
            const string commandText = "CMS_News_StatisticNewsBySourceId";
            try
            {
                List<NewsStatisticSourceEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SourceId", sourceId);

                if (startDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DateTime.Now);
                else _db.AddParameter(cmd, "FromDate", startDate);

                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DateTime.Now);
                else _db.AddParameter(cmd, "ToDate", endDate);


                data = _db.GetList<NewsStatisticSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public DepartmentViewCountEntity GetDepartmentView(int departmentId, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_News_GetStatisticNewsByDepartmentId";
            try
            {
                DepartmentViewCountEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "DepartmentId", departmentId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);

                data = _db.Get<DepartmentViewCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsStatisticDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion


    }
}
