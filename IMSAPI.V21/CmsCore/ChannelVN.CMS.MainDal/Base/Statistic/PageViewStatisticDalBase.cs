﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.MainDal.Base.Statistic
{
    public abstract class PageViewStatisticDalBase
    {
        public bool PageViewByZone_UpdateByDay(PageViewByZoneEntity viewEntity)
        {
            const string commandText = "CMS_PageViewByZone_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", viewEntity.ChannelId);
                _db.AddParameter(cmd, "LogDate", viewEntity.LogDate);
                _db.AddParameter(cmd, "LogDateStamp", viewEntity.LogDateStamp);
                _db.AddParameter(cmd, "PageView", viewEntity.PageView);
                _db.AddParameter(cmd, "PageViewMobile", viewEntity.PageViewMobile);
                _db.AddParameter(cmd, "ParentId", viewEntity.ParentId);
                _db.AddParameter(cmd, "ZoneId", viewEntity.ZoneId);
                return _db.ExecuteNonQuery(cmd)>0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }

        public DateTime GetLastestRun()
        {
            const string commandText = "CMS_PageViewByZone_GetClosestDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                object objResult = Convert.ToDateTime(_db.GetFirtDataRecord(cmd));
                DateTime result = new DateTime();
                if (objResult!=null)
                {
                    result = Convert.ToDateTime(objResult);
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return new DateTime();
            }
        }

        public bool PageViewAllSite_UpdateByDay(PageViewAllSiteEntity objPageViewUpdate)
        {
            const string commandText = "CMS_PageViewAllSite_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", objPageViewUpdate.ChannelId);
                _db.AddParameter(cmd, "LogDate", objPageViewUpdate.LogDate);
                _db.AddParameter(cmd, "LogDateStamp", objPageViewUpdate.LogDateStamp);
                _db.AddParameter(cmd, "PageView", objPageViewUpdate.PageView);
                _db.AddParameter(cmd, "PageViewMobile", objPageViewUpdate.PageViewMobile);
                return _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }

        public List<PageViewAllSiteEntity> PageViewAllSite_GetData(DateTime fromDate,DateTime toDate,int ChannelId)
        {
            const string commandText = "CMS_PageViewAllSite_GetData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ChannelId", ChannelId);
                return _db.GetList<PageViewAllSiteEntity>(cmd);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw ex;
            }
        }

        public List<PageViewByZoneEntity> PageViewByZone_GetData(DateTime fromDate, DateTime toDate,string zoneIds, int ChannelId)
        {
            const string commandText = "CMS_PageViewByZone_GetData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "ZoneId", zoneIds);
                _db.AddParameter(cmd, "ChannelId", ChannelId);

                //Logger.WriteLog(Logger.LogType.Debug, string.Format("CMS_PageViewByZone_GetData => ChannelId:{0} zoneIds:{1} FromDate:{2} ToDate:{3}", ChannelId, zoneIds, fromDate, toDate));

                return _db.GetList<PageViewByZoneEntity>(cmd);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw ex;
            }
        }

        public ChannelEntity GetChannel_ByName(string channelName)
        {
            const string commandText = "CMS_Channel_GetByName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelName", channelName);
                var data = _db.GetList<ChannelEntity>(cmd);
                if (data.Count>0)
                {
                    return data.FirstOrDefault();
                }
                else
                {
                    return new ChannelEntity();
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw ex;
            }
        }

        #region Core members

        private readonly CmsPageViewStatisticDb _db;

        protected PageViewStatisticDalBase(CmsPageViewStatisticDb db)
        {
            _db = db;
        }

        protected CmsPageViewStatisticDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
