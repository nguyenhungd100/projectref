﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Thread
{
    public abstract class ThreadDalBase
    {
        #region Get
        public ThreadEntity GetThreadByThreadId(long threadId)
        {
            const string commandText = "CMS_Thread_GetThreadByThreadID";
            try
            {
                ThreadEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", threadId);
                data = _db.Get<ThreadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            const string commandText = "CMS_ThreadNews_GetByNewsId";
            try
            {
                List<ThreadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<ThreadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadEntity> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Thread_Search";
            try
            {
                List<ThreadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHot", isHotThread);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<ThreadEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadEntity> InitESAllThread(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_Thread_InitESAllThread";
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM Thread T 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT T.Id, T.Name, T.Title, T.Url, T.Avatar, T.IsHot, T.IsOnHome,
	                                        T.CreatedDate, T.CreatedBy, T.TemplateId, Invisibled,
	                                        0 AS NewsCount,
	                                        ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                        FROM Thread T 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<ThreadEntity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<ThreadEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestion(int top, int zoneId, string keyword, int orderBy)
        {

            const string commandText = "CMS_Thread_SearchForSuggest";
            try
            {
                List<ThreadWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                data = _db.GetList<ThreadWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadWithSimpleFieldEntity> SearchThreadForSuggestionIncludeParentZone(int top, int zoneId, string keyword, int orderBy)
        {

            const string commandText = "CMS_Thread_SearchForSuggest";
            try
            {
                List<ThreadWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                data = _db.GetList<ThreadWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ThreadWithSimpleFieldEntity> GetRelationThread(long threadId)
        {
            const string commandText = "CMS_Thread_GetRelationThread";
            try
            {
                List<ThreadWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ThreadId", threadId);
                data = _db.GetList<ThreadWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update
        public bool Insert(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread, ref int threadId)
        {

            const string commandText = "CMS_Thread_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", thread.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", thread.Name);
                _db.AddParameter(cmd, "UnsignName", thread.UnsignName);
                _db.AddParameter(cmd, "Title", thread.Title);
                _db.AddParameter(cmd, "Description", thread.Description);
                _db.AddParameter(cmd, "Url", thread.Url);
                _db.AddParameter(cmd, "Avatar", thread.Avatar);
                _db.AddParameter(cmd, "HomeAvatar", thread.HomeAvatar);
                _db.AddParameter(cmd, "SpecialAvatar", thread.SpecialAvatar);
                _db.AddParameter(cmd, "IsHot", thread.IsHot);
                _db.AddParameter(cmd, "IsOnHome", thread.IsOnHome);
                _db.AddParameter(cmd, "CreatedDate", thread.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", thread.CreatedBy);
                _db.AddParameter(cmd, "MetaKeyword", thread.MetaKeyword);
                _db.AddParameter(cmd, "MetaContent", thread.MetaContent);
                _db.AddParameter(cmd, "TemplateId", thread.TemplateId);
                _db.AddParameter(cmd, "Invisibled", thread.Invisibled);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "RelationThread", relationThread);
                _db.AddParameter(cmd, "NewsCoverId", thread.NewsCoverId);


                var numberOfRow = cmd.ExecuteNonQuery();

                threadId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(ThreadEntity thread, int zoneId, string zoneIdList, string relationThread)
        {
            const string commandText = "CMS_Thread_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", thread.Id);
                _db.AddParameter(cmd, "Name", thread.Name);
                _db.AddParameter(cmd, "UnsignName", thread.UnsignName);
                _db.AddParameter(cmd, "Title", thread.Title);
                _db.AddParameter(cmd, "Description", thread.Description);
                _db.AddParameter(cmd, "Url", thread.Url);
                _db.AddParameter(cmd, "Avatar", thread.Avatar);
                _db.AddParameter(cmd, "HomeAvatar", thread.HomeAvatar);
                _db.AddParameter(cmd, "SpecialAvatar", thread.SpecialAvatar);
                _db.AddParameter(cmd, "IsHot", thread.IsHot);
                _db.AddParameter(cmd, "IsOnHome", thread.IsOnHome);
                _db.AddParameter(cmd, "CreatedDate", thread.CreatedDate);
                _db.AddParameter(cmd, "EditedBy", thread.EditedBy);
                _db.AddParameter(cmd, "MetaKeyword", thread.MetaKeyword);
                _db.AddParameter(cmd, "MetaContent", thread.MetaContent);
                _db.AddParameter(cmd, "TemplateId", thread.TemplateId);
                _db.AddParameter(cmd, "Invisibled", thread.Invisibled);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "RelationThread", relationThread);
                _db.AddParameter(cmd, "NewsCoverId", thread.NewsCoverId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  bool DeleteById(long threadId)
        {
            const string commandText = "CMS_Thread_DeleteByThreadID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", threadId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateThreadHot(long threadId, bool isHotThread)
        {
            const string commandText = "CMS_Thread_UpdateThreadHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", threadId);
                _db.AddParameter(cmd, "IsHot", isHotThread);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsInvisibled(long threadId, bool invisibled)
        {
            const string commandText = "CMS_Thread_UpdateInvisibled";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", threadId);
                _db.AddParameter(cmd, "Invisibled", invisibled);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ThreadDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
