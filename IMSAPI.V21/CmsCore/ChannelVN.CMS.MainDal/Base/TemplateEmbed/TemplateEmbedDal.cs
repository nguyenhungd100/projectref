﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.TemplateEmbed
{
    public class TemplateEmbedDal : TemplateEmbedDalBase
    {
        internal TemplateEmbedDal(TemplateEmbedCmsDb db)
            : base(db)
        {
        }
    }
}
