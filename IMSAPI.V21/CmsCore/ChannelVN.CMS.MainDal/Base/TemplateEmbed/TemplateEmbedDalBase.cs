﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.TemplateEmbed;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.TemplateEmbed
{
    public abstract class TemplateEmbedDalBase
    {        
        public List<TemplateEmbedEntity> SearchTemplateEmbed(string keyword, int channelId, int typeId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Template_embed_channel_Search";
            try
            {
                List<TemplateEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ChannelId", channelId);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<TemplateEmbedEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
                       
        #region Core members

        private readonly TemplateEmbedCmsDb _db;

        protected TemplateEmbedDalBase(TemplateEmbedCmsDb db)
        {
            _db = db;
        }

        protected TemplateEmbedCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
