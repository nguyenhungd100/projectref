﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx;
using ChannelVN.CMS.MainDal.Databases;
using System.Data;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.MainDal.Base.NewsRoyaltiesEx
{
    public abstract class NewsRoyaltiesExDalBase
    {        
        public List<NewsRoyaltiesExEntity> SearchNewsRoyalty(string accountName, string zoneIds, string catIds, DateTime fromDate, DateTime toDate, int pageIndex, int pagaSize, ref int totalRows)
        {
            //const string commandText = "CMS_NewsRoyaltyEx_Search";
            string commandText = @"
                SELECT  @TotalRow = COUNT(1) from (
                    select distinct PNewsID
                        from VNCRoyalty n
                        where (('{0}' = '0' AND 1 = 1) OR('{0}' <> '0' AND PCategoryID in({0}))) AND (('{1}' = '0' AND 1 = 1) OR ('{1}' <> '0' AND PZoneID in({1}))) 
			            AND ((@FromDate = '' AND @ToDate = '' AND 1 = 1) OR(@FromDate <> '' AND n.PPublishDate >= @FromDate AND  @ToDate <> '' and n.PPublishDate <= @ToDate)) 
                        AND (('{2}' = '' AND 1 = 1) OR ('{2}' <> '' AND PCreatedBy='{2}'))
                    ) as tem

                SELECT*
                FROM (
                    SELECT PNewsID, ROW_NUMBER() OVER(ORDER BY PNewsID) AS RowNum FROM(
                        select distinct PNewsID
                        from VNCRoyalty n
                        where (('{0}' = '0' AND 1 = 1) OR('{0}' <> '0' AND PCategoryID in({0}))) AND (('{1}' = '0' AND 1 = 1) OR ('{1}' <> '0' AND PZoneID in({1}))) 
			            AND ((@FromDate = '' AND @ToDate = '' AND 1 = 1) OR(@FromDate <> '' AND n.PPublishDate >= @FromDate AND  @ToDate <> '' and n.PPublishDate <= @ToDate)) 
                        AND (('{2}' = '' AND 1 = 1) OR ('{2}' <> '' AND PCreatedBy='{2}'))
		            )as temp
	            ) AS DATA
                WHERE RowNum BETWEEN((@PageIndex - 1) * @PageSize + 1) AND(@PageIndex * @PageSize)";
            commandText = string.Format(commandText,catIds, zoneIds, accountName);

            try
            {
                List<NewsRoyaltiesExEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);

                _db.AddParameter(cmd, "TotalRow", totalRows, ParameterDirection.Output);
                //_db.AddParameter(cmd, "ZoneIds", zoneIds);
                //_db.AddParameter(cmd, "CatIds", catIds);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", "");
                else _db.AddParameter(cmd, "FromDate", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", "");
                else _db.AddParameter(cmd, "ToDate", toDate);

                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pagaSize);

                data = _db.GetList<NewsRoyaltiesExEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsCategoryEntity> GetListCateRoyalty()
        {        
            string commandText = @"SELECT Id,Name,Status FROM NewsCategory WHERE Status=1";
            
            try
            {
                List<NewsCategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, false);               

                data = _db.GetList<NewsCategoryEntity>(cmd);               
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly NewsRoyaltiesExDb _db;

        protected NewsRoyaltiesExDalBase(NewsRoyaltiesExDb db)
        {
            _db = db;
        }

        protected NewsRoyaltiesExDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
