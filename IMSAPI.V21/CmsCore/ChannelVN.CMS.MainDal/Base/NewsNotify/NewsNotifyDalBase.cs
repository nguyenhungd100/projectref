﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsNotify
{
    public abstract class NewsNotifyDalBase
    {
        public bool InsertNews(NewsNotifyEntity news)
        {
            const string commandText = "NewsNotify_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "NewsId", news.NewsId);
                _db.AddParameter(cmd, "PublishDate", news.PublishDate);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsProcess", news.IsProcess);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Notify_InsertNews(NewsNotifyEntity news)
        {
            const string commandText = "Notify_InsertNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "NewsId", news.NewsId);
                _db.AddParameter(cmd, "PublishDate", news.PublishDate);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsProcess", news.IsProcess);
                _db.AddParameter(cmd, "ChannelId", news.ChannelId);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                _db.AddParameter(cmd, "ZoneId", news.ZoneId);
                _db.AddParameter(cmd, "ZoneName", news.ZoneName);
                _db.AddParameter(cmd, "ZoneShortUrl", news.ZoneShortUrl);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertNewsApp(NewsAppEntity news)
        {
            const string commandText = "News_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", news.NewsId);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Content", news.Content);
                _db.AddParameter(cmd, "Image", news.Image);
                _db.AddParameter(cmd, "PublishDate", news.PublishDate);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "IsVideo", news.IsVideo);
                _db.AddParameter(cmd, "VideoUrl", news.VideoUrl);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsBreaking", news.IsBreaking);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "PushDate", DBNull.Value);
                _db.AddParameter(cmd, "InputDate", news.InputDate);
                _db.AddParameter(cmd, "NotifyId", news.NotifyId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly AppLogNotifyDb _db;

        protected NewsNotifyDalBase(AppLogNotifyDb db)
        {
            _db = db;
        }

        protected AppLogNotifyDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
