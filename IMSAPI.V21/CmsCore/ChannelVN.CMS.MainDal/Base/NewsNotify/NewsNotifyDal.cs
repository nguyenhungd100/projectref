﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Base.NewsNotify;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsNotify
{
    public class NewsNotifyDal : NewsNotifyDalBase
    {
        internal NewsNotifyDal(AppLogNotifyDb db)
            : base(db)
        {
        }
    }}
