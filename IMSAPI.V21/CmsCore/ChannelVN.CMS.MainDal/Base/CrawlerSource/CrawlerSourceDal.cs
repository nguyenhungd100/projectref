﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.CrawlerSource
{
    public class CrawlerSourceDal: CrawlerSourceDalBase
    {
        internal CrawlerSourceDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
