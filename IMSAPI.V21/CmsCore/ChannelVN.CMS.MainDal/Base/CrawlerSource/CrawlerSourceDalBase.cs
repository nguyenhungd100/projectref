﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.CrawlerSource;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.CrawlerSource
{
    public abstract class CrawlerSourceDalBase
    {
        public bool InsertCrawlerSource(CrawlerSourceTopicWrapperEntity news)
        {
            const string commandText = "CMS_CrawlerSource_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.CrawlerSourceEntity.Id);
                _db.AddParameter(cmd, "Username", news.CrawlerSourceEntity.Username);
                _db.AddParameter(cmd, "SourceId", news.CrawlerSourceEntity.SourceId);
                _db.AddParameter(cmd, "SourceName", news.CrawlerSourceEntity.SourceName);
                _db.AddParameter(cmd, "Description", news.CrawlerSourceEntity.Description);
                _db.AddParameter(cmd, "Domain", news.CrawlerSourceEntity.Domain);
                _db.AddParameter(cmd, "Avatar", news.CrawlerSourceEntity.Avatar);
                _db.AddParameter(cmd, "TopicId", news.CrawlerSourceTopicEntity.TopicId);
                _db.AddParameter(cmd, "TopicName", news.CrawlerSourceTopicEntity.TopicName);
                _db.AddParameter(cmd, "TopicDescription", news.CrawlerSourceTopicEntity.Description);
                _db.AddParameter(cmd, "TopicAvatar", news.CrawlerSourceTopicEntity.Avatar);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CrawlerSourceEntity> GetSourceByTopic(string userName, int topicId)
        {
            const string commandText = "CMS_CrawlerSource_GetByUserTopic";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "userName", userName);
                _db.AddParameter(cmd, "topicId", topicId);
                List<CrawlerSourceEntity> data = _db.GetList<CrawlerSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CrawlerSourceEntity> GetSource(string userName)
        {
            const string commandText = "CMS_CrawlerSource_GetByUser";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "userName", userName);
                List<CrawlerSourceEntity> data = _db.GetList<CrawlerSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CrawlerSourceTopicEntity> GetTopicByCrawlerSource(int crawlerSourceId)
        {
            const string commandText = "CMS_CrawlerSourceTopic_GetByCrawlerSource";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CrawlerSourceId", crawlerSourceId);
                List<CrawlerSourceTopicEntity> data = _db.GetList<CrawlerSourceTopicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected CrawlerSourceDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
