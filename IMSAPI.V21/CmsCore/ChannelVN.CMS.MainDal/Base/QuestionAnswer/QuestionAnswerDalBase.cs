﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.GroupQuestion;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.QuestionAnswer
{
    public abstract class QuestionAnswerDalBase
    {
        public bool InsertQuestionGroup(GroupQuestionEntity news)
        {
            const string commandText = "CMS_GroupQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "GroupQuestionName", news.GroupQuestionName);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateQuestion(QuestionEntity news)
        {
            const string commandText = "CMS_Question_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", news.Id);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Description", news.Description);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAnswer(AnswerEntity news)
        {
            const string commandText = "CMS_Answer_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AnswerId", news.Id);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "Description", news.Description);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "IsCorrect", news.IsCorrect);
                _db.AddParameter(cmd, "ReadMoreLink", news.ReadMoreLink);
                var numberOfRow = _db.ExecuteNonQuery(cmd);

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public bool UpdateNewsAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        //{
        //    const string commandText = "CMS_News_UpdateAvatar";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "@NewsId", newsId);
        //        _db.AddParameter(cmd, "@Avatar", avatar);
        //        _db.AddParameter(cmd, "@Avatar2", avatar2);
        //        _db.AddParameter(cmd, "@Avatar3", avatar3);
        //        _db.AddParameter(cmd, "@Avatar4", avatar4);
        //        _db.AddParameter(cmd, "@Avatar5", avatar5);
        //        _db.AddParameter(cmd, "@AvatarCustom", avatarCustom);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        public bool DeleteQuestion(int id)
        {
            const string commandText = "CMS_Question_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteAnswer(int id)
        {
            const string commandText = "CMS_Answer_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AnswerId", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public bool ChangeStatusToWaitForEdit(long id)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToWaitForEdit";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToRecievedForEdit(long id, string editedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToRecievedForEdit";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "EditedBy", editedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToWaitForPublish(long id, string editedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToWaitForPublish";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "EditedBy", editedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToRecievedForPublish(long id, string editedBy, string publishedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToRecievedForPublish";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "EditedBy", editedBy);
        //        _db.AddParameter(cmd, "PublishedBy", publishedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToPublished(long id, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string publishedBy, long publishedDate, string publishedContent)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToPublished";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "PublishedBy", publishedBy);
        //        _db.AddParameter(cmd, "NewsPositionTypeForHome", newsPositionTypeForHome);
        //        _db.AddParameter(cmd, "NewsPositionOrderForHome", newsPositionOrderForHome);
        //        _db.AddParameter(cmd, "NewsPositionTypeForList", newsPositionTypeForList);
        //        _db.AddParameter(cmd, "NewsPositionOrderForList", newsPositionOrderForList);
        //        _db.AddParameter(cmd, "PublishedDate", publishedDate);
        //        if (string.IsNullOrEmpty(publishedContent))
        //        {
        //            _db.AddParameter(cmd, "PublishedContent", DBNull.Value);
        //        }
        //        else
        //        {
        //            _db.AddParameter(cmd, "PublishedContent", publishedContent);
        //        }
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}
        //public bool ChangeStatusToUnpublished(long id, string lastModifiedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToUnpublished";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToMovedToTrash(long id, string lastModifiedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToMovedToTrash";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToRestoreFromTrash(long id, string lastModifidedBy)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToRestoreFromTrash";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "LastModidiedBy", lastModifidedBy);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToReturnedToReporter(long id, string note)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToReturnedToReporter";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "Note", note);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToReturnedToEditor(long id, string receiver, string note)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToReturnedToEditor";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "Receiver", receiver);
        //        _db.AddParameter(cmd, "Note", note);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusToRecieveFromReturnStore(long id)
        //{
        //    const string commandText = "CMS_News_ChangeStatusToRecieveFromReturnStore";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusForwardToOtherEditor(long id, string receiver)
        //{
        //    const string commandText = "CMS_News_ChangeStatusForwardToOtherEditor";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "Receiver", receiver);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public bool ChangeStatusForwardToOtherSecretary(long id, string receiver)
        //{
        //    const string commandText = "CMS_News_ChangeStatusForwardToOtherSecretary";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        _db.AddParameter(cmd, "Receiver", receiver);
        //        var numberOfRow = cmd.ExecuteNonQuery();

        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public NewsForValidateEntity GetNewsForValidateById(long id)
        //{
        //    const string commandText = "CMS_News_GetNewsForValidateByNewsId";
        //    try
        //    {
        //        NewsForValidateEntity data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        data = _db.Get<NewsForValidateEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public NewsInfoForCachedEntity GetNewsInfoForCachedById(long id)
        //{
        //    const string commandText = "CMS_News_GetNewsInfoForCachedByNewsId";
        //    try
        //    {
        //        NewsInfoForCachedEntity data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        data = _db.Get<NewsInfoForCachedEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public NewsEntity GetNewsById(long id)
        //{
        //    const string commandText = "CMS_News_GetNewsById";
        //    try
        //    {
        //        NewsEntity data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        data = _db.Get<NewsEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public List<NewsForOtherAPIEntity> GetNewsForOtherAPI(string listNewsId)
        //{
        //    const string commandText = "CMS_News_GetNewsForOtherAPIByListId";
        //    try
        //    {
        //        List<NewsForOtherAPIEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ListNewsId", listNewsId);
        //        data = _db.GetList<NewsForOtherAPIEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public List<NewsWithAnalyticEntity> GetNewsByListNewsId(int status, string listNewsId)
        //{
        //    const string commandText = "CMS_News_GetNewsByListNewsId";
        //    try
        //    {
        //        List<NewsWithAnalyticEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Status", status);
        //        _db.AddParameter(cmd, "ListNewsId", listNewsId);
        //        data = _db.GetList<NewsWithAnalyticEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public List<NewsWithDistributionDateEntity> GetNewsDistributionDateByListNewsId(string listNewsId)
        //{
        //    const string commandText = "CMS_News_GetNewsDistributionDateByListNewsId";
        //    try
        //    {
        //        List<NewsWithDistributionDateEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ListNewsId", listNewsId);
        //        data = _db.GetList<NewsWithDistributionDateEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public List<NewsForSuggestionEntity> GetNewsUseInterviewByListInterviewId(string listInterviewId)
        //{
        //    const string commandText = "CMS_News_GetNewsUseInterviewByListInterviewId";
        //    try
        //    {
        //        List<NewsForSuggestionEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ListInterviewId", listInterviewId);
        //        data = _db.GetList<NewsForSuggestionEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public List<NewsWithAnalyticEntity> GetTopMostNewsInHour(int top, int zoneId)
        //{
        //    const string commandText = "CMS_NewsAnalytic_GetTopMostNewsInHour";
        //    try
        //    {
        //        List<NewsWithAnalyticEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "ZoneId", zoneId);
        //        _db.AddParameter(cmd, "Top", top);
        //        data = _db.GetList<NewsWithAnalyticEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        public bool InsertQuestion(QuestionEntity question, ref int id)
        {
            const string commandText = "CMS_Question_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", question.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "QuestionGroupId", question.QuestionGroupId);
                _db.AddParameter(cmd, "Avatar", question.Avatar);
                _db.AddParameter(cmd, "Description", question.Description);
                _db.AddParameter(cmd, "CreatedBy", question.CreatedBy);
                int data = _db.ExecuteNonQuery(cmd);

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertQuestionInEvent(int questionId, int eventId)
        {
            const string commandText = "CMS_QuestionInEvent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", questionId);
                _db.AddParameter(cmd, "EventQuestionId", eventId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertAnswer(AnswerEntity answer, ref int id)
        {
            const string commandText = "CMS_Answer_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answer.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "QuestionId", answer.QuestionId);
                _db.AddParameter(cmd, "Avatar", answer.Avatar);
                _db.AddParameter(cmd, "Description", answer.Description);
                _db.AddParameter(cmd, "CreatedBy", answer.CreatedBy);
                _db.AddParameter(cmd, "Title", answer.Title);
                _db.AddParameter(cmd, "IsCorrect", answer.IsCorrect);
                _db.AddParameter(cmd, "ReadMoreLink", answer.ReadMoreLink);
                int data = _db.ExecuteNonQuery(cmd);

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertGroupQuestion(GroupQuestionEntity answer, ref int id)
        {
            const string commandText = "CMS_GroupQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answer.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "QuestionGroupName", answer.GroupQuestionName);
                int data = _db.ExecuteNonQuery(cmd);

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertEventQuestion(EventQuestionEntity answer, ref int id)
        {
            const string commandText = "CMS_EventQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "EventId", answer.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "EventQuestionName", answer.EventQuestionName);
                _db.AddParameter(cmd, "CreatedBy", answer.CreatedBy);
                int data = _db.ExecuteNonQuery(cmd);

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateGroupQuestion(GroupQuestionEntity answer)
        {
            const string commandText = "CMS_GroupQuestion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answer.Id);
                _db.AddParameter(cmd, "QuestionGroupName", answer.GroupQuestionName);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateEventQuestion(EventQuestionEntity answer)
        {
            const string commandText = "CMS_EventQuestion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "EventId", answer.Id);
                _db.AddParameter(cmd, "EventQuestionName", answer.EventQuestionName);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteEventQuestion(int id)
        {
            const string commandText = "CMS_EventQuestion_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "EventId", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteGroupQuestion(int id)
        {
            const string commandText = "CMS_GroupQuestion_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<GroupQuestionEntity> Search(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_GroupQuestion_GetList";
            try
            {
                List<GroupQuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<GroupQuestionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<EventQuestionEntity> SearchEvent(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_EventQuestion_GetList";
            try
            {
                List<EventQuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<EventQuestionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuestionEntity> GetListQuestionByGroup(string keyword, int groupId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Question_GetList_By_Group";
            try
            {
                List<QuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "groupId", groupId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuestionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuestionEntity> GetListQuestionByGroupEvent(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Question_GetList_By_Group_Event";
            try
            {
                List<QuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "groupId", groupId);
                _db.AddParameter(cmd, "eventId", eventId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuestionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuestionEntity> GetListQuestionBefore(string keyword, int groupId, int eventId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Question_GetList_Before";
            try
            {
                List<QuestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "groupId", groupId);
                _db.AddParameter(cmd, "eventId", eventId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<QuestionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<AnswerEntity> GetListAnswer(int questionId)
        {
            const string commandText = "CMS_Answer_GetByQuestionId";
            try
            {
                List<AnswerEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                data = _db.GetList<AnswerEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public QuestionEntity GetQuestionById(int questionId)
        {
            const string commandText = "CMS_Question_GetById";
            try
            {
                QuestionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", questionId);
                data = _db.Get<QuestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public GroupQuestionEntity GetGroupQuestionById(int questionId)
        {
            const string commandText = "CMS_GroupQuestion_GetById";
            try
            {
                GroupQuestionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                data = _db.Get<GroupQuestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public EventQuestionEntity GetEventQuestionById(int questionId)
        {
            const string commandText = "CMS_EventQuestion_GetById";
            try
            {
                EventQuestionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                data = _db.Get<EventQuestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected QuestionAnswerDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
