﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.EndUser;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.EndUser
{
    public abstract class EndUserDalBase
    {        
        #region insert update

        public bool Insert(EndUserEntity videoChannelEntity, ref int videoChannelId)
        {
            const string commandText = "CMS_EndUser_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", videoChannelEntity.Id, ParameterDirection.Output);

                _db.AddParameter(cmd, "Email", videoChannelEntity.Email);
                _db.AddParameter(cmd, "Password", videoChannelEntity.Password);
                _db.AddParameter(cmd, "Name", videoChannelEntity.Name);
                _db.AddParameter(cmd, "VietId", videoChannelEntity.VietId);
                _db.AddParameter(cmd, "FacebookId", videoChannelEntity.FacebookId);
                _db.AddParameter(cmd, "GoogleId", videoChannelEntity.GoogleId);
                _db.AddParameter(cmd, "Phone", videoChannelEntity.Phone);
                _db.AddParameter(cmd, "Avatar", videoChannelEntity.Avatar);
                _db.AddParameter(cmd, "Status", videoChannelEntity.Status);
                _db.AddParameter(cmd, "Sex", videoChannelEntity.Sex);
                _db.AddParameter(cmd, "Birthday", videoChannelEntity.Birthday);
                _db.AddParameter(cmd, "Address", videoChannelEntity.Address);

                //_db.AddParameter(cmd, "CreatedBy", videoChannelEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", videoChannelEntity.CreatedDate);
                //_db.AddParameter(cmd, "LastModifiedBy", videoChannelEntity.LastModifiedBy);
                _db.AddParameter(cmd, "LastModifiedDate", videoChannelEntity.LastModifiedDate);

                var numberOfRow = cmd.ExecuteNonQuery();
                videoChannelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return videoChannelId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(EndUserEntity videoChannelEntity)
        {
            const string commandText = "CMS_EndUser_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "Email", videoChannelEntity.Email);
                _db.AddParameter(cmd, "Password", videoChannelEntity.Password);
                _db.AddParameter(cmd, "Name", videoChannelEntity.Name);
                _db.AddParameter(cmd, "VietId", videoChannelEntity.VietId);
                _db.AddParameter(cmd, "FacebookId", videoChannelEntity.FacebookId);
                _db.AddParameter(cmd, "GoogleId", videoChannelEntity.GoogleId);
                _db.AddParameter(cmd, "Phone", videoChannelEntity.Phone);
                _db.AddParameter(cmd, "Avatar", videoChannelEntity.Avatar);
                _db.AddParameter(cmd, "Status", videoChannelEntity.Status);
                _db.AddParameter(cmd, "Sex", videoChannelEntity.Sex);
                _db.AddParameter(cmd, "Birthday", videoChannelEntity.Birthday);
                _db.AddParameter(cmd, "Address", videoChannelEntity.Address);

                //_db.AddParameter(cmd, "CreatedBy", videoChannelEntity.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", videoChannelEntity.CreatedDate);                                
                //_db.AddParameter(cmd, "LastModifiedBy", videoChannelEntity.LastModifiedBy);
                _db.AddParameter(cmd, "LastModifiedDate", videoChannelEntity.LastModifiedDate);               

                _db.AddParameter(cmd, "Id", videoChannelEntity.Id);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public EndUserEntity GetById(int id)
        {

            const string commandText = "CMS_EndUser_GetById";
            try
            {                
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
               return _db.Get<EndUserEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Search        

        #endregion

        #region follow

        public bool UserFollowPlayList(PlayListFollowEntity userFollow)
        {
            const string commandText = "CMS_PlayListFollow_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "PlayListId", userFollow.PlayListId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);
                _db.AddParameter(cmd, "FollowedDate", userFollow.FollowedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            const string commandText = "CMS_ZoneVideoFollow_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ZoneId", userFollow.ZoneId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);
                _db.AddParameter(cmd, "FollowedDate", userFollow.FollowedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserFollowChannel(VideoChannelFollowEntity userFollow)
        {
            const string commandText = "CMS_VideoChannelFollow_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ChannelId", userFollow.ChannelId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);
                _db.AddParameter(cmd, "FollowedDate", userFollow.FollowedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserFollowLabel(VideoLabelFollowEntity userFollow)
        {
            const string commandText = "CMS_VideoLabelFollow_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "LabelId", userFollow.LabelId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);
                _db.AddParameter(cmd, "FollowedDate", userFollow.FollowedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserFollowPublisher(PublisherFollowEntity userFollow)
        {
            const string commandText = "CMS_PublisherFollow_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "PublisherId", userFollow.PublisherId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);
                _db.AddParameter(cmd, "FollowedDate", userFollow.FollowedDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region unfollow

        public bool UserUnFollowPlayList(PlayListFollowEntity userFollow)
        {
            const string commandText = "CMS_PlayListFollow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "PlayListId", userFollow.PlayListId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserUnFollowZoneVideo(ZoneVideoFollowEntity userFollow)
        {
            const string commandText = "CMS_ZoneVideoFollow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ZoneId", userFollow.ZoneId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserUnFollowChannel(VideoChannelFollowEntity userFollow)
        {
            const string commandText = "CMS_VideoChannelFollow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "ChannelId", userFollow.ChannelId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserUnFollowLabel(VideoLabelFollowEntity userFollow)
        {
            const string commandText = "CMS_VideoLabelFollow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "LabelId", userFollow.LabelId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UserUnFollowPublisher(PublisherFollowEntity userFollow)
        {
            const string commandText = "CMS_PublisherFollow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "PublisherId", userFollow.PublisherId);
                _db.AddParameter(cmd, "UserId", userFollow.UserId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region thống kê

        public List<UserStatisticEntity> GetStatisticStatus(int status, DateTime startDate, DateTime endDate)
        {
            const string commandText = "CMS_EndUser_StatisticStatus";
            try
            {
                List<UserStatisticEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "Status", status);

                if (startDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "FromDate", DBNull.Value);
                else _db.AddParameter(cmd, "FromDate", startDate);

                if (endDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ToDate", DBNull.Value);
                else _db.AddParameter(cmd, "ToDate", endDate);


                data = _db.GetList<UserStatisticEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<PlayListCountEntity> StatictisFollowPlaylist(string username, DateTime from, DateTime to)
        {

            const string commandText = "CMS_EndUser_StatictisFollowPlaylist";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "From", from);
                _db.AddParameter(cmd, "To", to);

                return _db.GetList<PlayListCountEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<VideoChannelCountEntity> StatictisFollowVideoChannel(string username, DateTime from, DateTime to)
        {

            const string commandText = "CMS_EndUser_StatictisFollowVideoChannel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "From", from);
                _db.AddParameter(cmd, "To", to);

                return _db.GetList<VideoChannelCountEntity>(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected EndUserDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
