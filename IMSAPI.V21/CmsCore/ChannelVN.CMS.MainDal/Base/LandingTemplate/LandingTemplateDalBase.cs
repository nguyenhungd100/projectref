﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.LandingTemplate;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.LandingTemplate
{
    public abstract class LandingTemplateDalBase
    {
        #region LandingTemplate
        public List<LandingTemplateEntity> Search(string keyword, int categoryId, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_LandingTemplate_Search";
            try
            {
                List<LandingTemplateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "CategoryId", categoryId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<LandingTemplateEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public LandingTemplateEntity GetById(int id)
        {
            const string commandText = "CMS_LandingTemplate_GetById";
            try
            {
                LandingTemplateEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<LandingTemplateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Insert(LandingTemplateEntity obj, ref int id)
        {
            const string commandText = "CMS_LandingTemplate_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);                
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Html", obj.Html);
                _db.AddParameter(cmd, "Avatar", obj.Avatar);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "CategoryId", obj.CategoryId);               
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);                

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(LandingTemplateEntity obj)
        {
            const string commandText = "CMS_LandingTemplate_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Html", obj.Html);
                _db.AddParameter(cmd, "Avatar", obj.Avatar);
                _db.AddParameter(cmd, "Type", obj.Type);
                _db.AddParameter(cmd, "CategoryId", obj.CategoryId);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "ModifiedBy", obj.ModifiedBy);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_LandingTemplate_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id",id);                
                _db.AddParameter(cmd, "Status", status);
                
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_LandingTemplate_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region LandingTemplateCategory
        public List<LandingTemplateCategoryEntity> GetAllCategory()
        {
            const string commandText = "CMS_LandingTemplateCategory_GetAll";
            try
            {
                List<LandingTemplateCategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);               

                data = _db.GetList<LandingTemplateCategoryEntity>(cmd);                
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public LandingTemplateCategoryEntity GetCategoryById(int id)
        {
            const string commandText = "CMS_LandingTemplateCategory_GetById";
            try
            {
                LandingTemplateCategoryEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                data = _db.Get<LandingTemplateCategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool InsertCategory(LandingTemplateCategoryEntity obj, ref int id)
        {
            const string commandText = "CMS_LandingTemplateCategory_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Description", obj.Description);
                _db.AddParameter(cmd, "Priority", obj.Priority);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);                
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "CreatedBy", obj.CreatedBy);
                _db.AddParameter(cmd, "Type", obj.Type);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateCategory(LandingTemplateCategoryEntity obj)
        {
            const string commandText = "CMS_LandingTemplateCategory_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", obj.Id);
                _db.AddParameter(cmd, "Name", obj.Name);
                _db.AddParameter(cmd, "Description", obj.Description);
                _db.AddParameter(cmd, "Priority", obj.Priority);
                _db.AddParameter(cmd, "ParentId", obj.ParentId);
                _db.AddParameter(cmd, "Status", obj.Status);
                _db.AddParameter(cmd, "Type", obj.Type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        public bool DeleteCategory(int id)
        {
            const string commandText = "CMS_LandingTemplateCategory_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected LandingTemplateDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
