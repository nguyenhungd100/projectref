﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Tag
{
    public abstract class BoxThreadEmbedDalBase
    {
        public List<BoxThreadEmbedEntity> GetListThreadEmbed(int zoneId, int type)
        {
            const string commandText = "CMS_BoxThreadEmbed_GetListByZone";
            try
            {
                List<BoxThreadEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxThreadEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(BoxThreadEmbedEntity ThreadEmbebBox)
        {

            const string commandText = "CMS_BoxThreadEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", ThreadEmbebBox.ZoneId);
                _db.AddParameter(cmd, "ThreadId", ThreadEmbebBox.ThreadId);
                _db.AddParameter(cmd, "SortOrder", ThreadEmbebBox.SortOrder);
                _db.AddParameter(cmd, "Type", ThreadEmbebBox.Type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(string listThreadId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxThreadEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listThreadId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long ThreadId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxThreadEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ThreadId", ThreadId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxThreadEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
