﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Tag
{
    public abstract class TagDalBase
    {
        #region Get methods
        public List<TagEntity> GetTagByListOfId(string listTagId)
        {
            const string commandText = "CMS_Tag_GetTagByIds";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", listTagId);
                data = _db.GetList<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> GetTagByParentTagId(long parentTagId)
        {
            const string commandText = "CMS_Tag_GetTagByParentTagID";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentID", parentTagId);
                data = _db.GetList<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagEntity GetTagByTagId(long tagId)
        {
            const string commandText = "CMS_Tag_GetTagByTagID";
            try
            {
                TagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                data = _db.Get<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_Tag_GetByName";
            try
            {
                TagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "getTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.Get<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public TagEntity GetTagByTagNameOrUrl(string name, string url)
        {
            const string commandText = "CMS_Tag_GetByNameOrUrl";
            try
            {
                TagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "Url", url);
                data = _db.Get<TagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_Tag_Search";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ParentID", parentTagId);
                _db.AddParameter(cmd, "IsThread", isThread);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "GetTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.GetList<TagEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CountTagNewsEntity> CountTagNewsByListTagId(string listTagId)
        {
            const string commandText = "CMS_TagNews_CountTagNewsByListTagId";
            try
            {                
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "ListTagId", listTagId);  
                                          
                return _db.GetList<CountTagNewsEntity>(cmd);
            }
            catch (Exception ex)
            {
                return new List<CountTagNewsEntity>();
                //throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagEntity> InitESAllTag(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_Tag_InitESAllTag";
            const string commandText = @"DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM Tag T where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                        T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                        T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,
	                                        Z.Name AS ZoneName, count(TN.TagId) as NewsCount,
	                                        ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                        FROM Tag T 
                                        left join TagZone TZ ON T.Id = TZ.TagId
                                        left join Zone Z ON Z.Id = TZ.ZoneId
                                        left join TagNews TN on TN.TagId=T.Id
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        group by T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	                                        T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	                                        T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId,Z.Name
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText,false);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<TagEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagEntity> Tag_SearchInBoxEmbed(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Tag_SearchInBoxTag";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<TagEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> Tag_SearchInBoxEmbed_Approved(string keyword, int isHotTag, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Tag_SearchInBoxTag_Approved";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<TagEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagEntity> SearchAllByKeyword(string keyword, int zoneId, bool isThread)
        {
            const string commandText = "CMS_Tag_GetAllByKeyword";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "IsThread", isThread);
                data = _db.GetList<TagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagWithSimpleFieldEntity> SearchTagForSuggestion(int top, int zoneId, string keyword, int isHot, int orderBy, bool getTagHasNewsOnly)
        {
            const string commandText = "CMS_Tag_SearchForSuggest";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHotTag", isHot);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "GetTagHasNewsOnly", getTagHasNewsOnly);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagWithSimpleFieldEntity> GetAllCleanTag(string startByKeyword, int minWordCount, int maxWordCount)
        {
            const string commandText = "CMS_Tag_GetAllCleanTag";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "StartByKeyword", startByKeyword);
                _db.AddParameter(cmd, "MinWordCount", minWordCount);
                _db.AddParameter(cmd, "MaxWordCount", maxWordCount);

                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TagEntity> GetListTagByNewsId(long newsId)
        {
            const string commandText = "CMS_TagNews_GetByNewsId";
            try
            {
                List<TagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);

                data = _db.GetList<TagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagWithSimpleFieldEntity> GetRelationTag(long tagId)
        {
            const string commandText = "CMS_Tag_GetRelationTag";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update_RelationTag(long tagId, string TagRelationIds)
        {
            const string commandText = "CMS_Tag_UpdateTagRelation";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "TagRelationIds", TagRelationIds);
                int data = _db.ExecuteNonQuery(cmd);
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update methods
        public bool Insert(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {

            const string commandText = "CMS_Tag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "CreatedBy", tag.CreatedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);
                _db.AddParameter(cmd, "SubTitle", tag.SubTitle);
                _db.AddParameter(cmd, "NewsCoverId", tag.NewsCoverId);
                _db.AddParameter(cmd, "IsAmp", tag.IsAmp);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ChannelVN.CMS.Common.Logger.LogType.Fatal, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(TagEntity tag, ref long tagId)
        {
            const string commandText = "SYNCV2_CMS_Tag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        

        public bool Update(TagEntity tag, int zoneId, string zoneIdList)
        {

            const string commandText = "CMS_Tag_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "EditedBy", tag.EditedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);
                _db.AddParameter(cmd, "SubTitle", tag.SubTitle);
                _db.AddParameter(cmd, "NewsCoverId", tag.NewsCoverId);
                _db.AddParameter(cmd, "IsAmp", tag.IsAmp);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateViewCountByTag(int viewCount, string url)
        {

            const string commandText = "CMS_Tag_UpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "viewCount", viewCount);
                _db.AddParameter(cmd, "Url", url);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateListViewCountByTag(string value)
        {

            const string commandText = "CMS_Tag_UpdateListViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "value", value);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long tagId)
        {
            const string commandText = "CMS_Tag_DeleteByTagID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool DeleteTagNewsById(string tagName, long newsId)
        {
            const string commandText = "SYNCV2_CMS_Tag_DeleteTagNewByTagID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "tagName", tagName);
                _db.AddParameter(cmd, "newsId", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        //public static bool UpdateTagForNews(long newsId, string tagIds)
        //{
        //    
        //    try
        //    {
        //        var parameters = new[]
        //                             {
        //                                 new SqlParameter("@NewsId", newsId),
        //                                 new SqlParameter("@TagIDs", tagIds)
        //                             };
        //        var numberOfRow = _db.ExecuteNonQuery(
        //                                                    CommandType.StoredProcedure,
        //                                                    Constants.DatabaseSchema + "CMS_TagNews_UpdateTagForNews",
        //                                                    parameters);
        //        return numberOfRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(Constants.DatabaseSchema + "CMS_TagNews_UpdateTagForNews:{0}", ex.Message));
        //    }
        //}
        /// <summary>
        /// Thêm mới thông tin tag vào bảng tagNews
        /// </summary>
        /// <param name="newsIds"></param>
        /// <param name="tagId"></param>
        /// <param name="tagMode"></param>
        /// <returns></returns>
        public bool AddNews(string newsIds, long tagId, int tagMode = 1)
        {
            const string commandText = "CMS_Tag_AddNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", newsIds);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "TagMode", tagMode);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(long tagId, long priority)
        {
            const string commandText = "CMS_Tag_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "Priority", priority);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateTagHot(long tagId, bool isHotTag)
        {
            const string commandText = "CMS_Tag_UpdateTagHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                _db.AddParameter(cmd, "IsHotTag", isHotTag);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            const string commandText = "CMS_Tag_UpdateTagInvisibled";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                _db.AddParameter(cmd, "Invisibled", invisibled);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        /// <summary>
        /// Cập nhật tag cho bài viết
        /// </summary>
        /// <param name="newsId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public bool UpdateTagNews(long newsId, string tagName)
        {
            const string commandText = "SYNCV2_CMS_Tag_UpdateTagNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "newsId", newsId);
                _db.AddParameter(cmd, "tagName", tagName);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        public List<TagWithSimpleFieldEntity> GetTagByListOfTagName(string listTagName)
        {
            const string commandText = "CMS_Tag_GetTagByListTagName";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListTagName", listTagName);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        public bool RecieveFromCloud(TagEntity tag, int zoneId, string zoneIdList, ref long tagId)
        {

            const string commandText = "CMS_Tag_RecieveFromCloud";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentID", tag.ParentId);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Description", tag.Description);
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Invisibled", tag.Invisibled);
                _db.AddParameter(cmd, "IsHotTag", tag.IsHotTag);
                _db.AddParameter(cmd, "Type", tag.Type);
                _db.AddParameter(cmd, "CreatedBy", tag.CreatedBy);
                _db.AddParameter(cmd, "UnsignName", tag.UnsignName);
                _db.AddParameter(cmd, "IsThread", tag.IsThread);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Priority", tag.Priority);
                _db.AddParameter(cmd, "TagContent", tag.TagContent);
                _db.AddParameter(cmd, "TagTitle", tag.TagTitle);
                _db.AddParameter(cmd, "TagInit", tag.TagInit);
                _db.AddParameter(cmd, "TagMetaKeyword", tag.TagMetaKeyword);
                _db.AddParameter(cmd, "TagMetaContent", tag.TagMetaContent);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "OtherZoneId", zoneIdList);
                _db.AddParameter(cmd, "TemplateId", tag.TemplateId);
                _db.AddParameter(cmd, "CreatedDate", tag.CreatedDate);
                _db.AddParameter(cmd, "EditedBy", tag.EditedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ChannelVN.CMS.Common.Logger.LogType.Fatal, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }


        #region Seoer Insert tag
        public bool Seoer_Insert_Tag(string TagNames, string CreatedBy, int PrimaryZoneId, string OtherZoneId, ref string Ids)
        {
            Ids = "";
            const string commandText = "CMS_Tag_Expert_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", Ids, ParameterDirection.Output);
                _db.AddParameter(cmd, "Names", TagNames);
                _db.AddParameter(cmd, "CreatedBy", CreatedBy);
                _db.AddParameter(cmd, "PrimaryZoneId", PrimaryZoneId);
                _db.AddParameter(cmd, "OtherZoneId", OtherZoneId);
                var Data = cmd.ExecuteScalar();
                Ids = _db.GetParameterValueFromCommand(cmd, 0).ToString();
                Ids = Data.ToString();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region BoxTagSeoEmbed

        public List<BoxTagSeoEmbedEntity> ListBoxTagSeoEmbed(long tagId, int type)
        {
            const string commandText = "CMS_BoxTagSeoEmbed_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "Type", type);
                List<BoxTagSeoEmbedEntity> data = _db.GetList<BoxTagSeoEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertBoxTagSeoEmbed(BoxTagSeoEmbedEntity box, ref int id)
        {
            const string commandText = "CMS_BoxTagSeoEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagId", box.TagId);                
                _db.AddParameter(cmd, "Title", box.Title);
                _db.AddParameter(cmd, "Avatar", box.Avatar);
                _db.AddParameter(cmd, "Url", box.Url);
                _db.AddParameter(cmd, "Description", box.Description);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);
                _db.AddParameter(cmd, "ObjectId", box.ObjectId);
                _db.AddParameter(cmd, "ObjectType", box.ObjectType);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateBoxTagSeoEmbed(BoxTagSeoEmbedEntity box)
        {
            const string commandText = "CMS_BoxTagSeoEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id);
                _db.AddParameter(cmd, "TagId", box.TagId);                
                _db.AddParameter(cmd, "Title", box.Title);
                _db.AddParameter(cmd, "Avatar", box.Avatar);
                _db.AddParameter(cmd, "Url", box.Url);
                _db.AddParameter(cmd, "Description", box.Description);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);
                _db.AddParameter(cmd, "ObjectId", box.ObjectId);
                _db.AddParameter(cmd, "ObjectType", box.ObjectType);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteBoxTagSeoEmbed(int id)
        {
            const string commandText = "CMS_BoxTagSeoEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion
    }
}
