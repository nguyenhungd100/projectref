﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Tag
{
    public abstract class BoxTagEmbedDalBase
    {
        #region Tag
        public List<BoxTagEmbedEntity> GetListTagEmbed(int zoneId, int type)
        {
            const string commandText = "CMS_BoxTagEmbed_GetListByZone";
            try
            {
                List<BoxTagEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxTagEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(string listTagId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxTagEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listTagId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateExternalTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            const string commandText = "CMS_BoxTagEmbed_UpdateExternalTag";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listTagId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ListOfTitle", listTitle);
                _db.AddParameter(cmd, "ListOfUrl", listUrl);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region VideoTag
        public List<BoxTagEmbedEntity> GetListVideoTagEmbed(int zoneId, int type)
        {
            const string commandText = "CMS_BoxTagEmbed_GetListVideoTagByZone";
            try
            {
                List<BoxTagEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<BoxTagEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateVideoTag(string listTagId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxTagEmbed_UpdateVideoTag";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listTagId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateExternalVideoTag(string listTagId, int zoneId, int type, string listTitle, string listUrl)
        {
            const string commandText = "CMS_BoxTagEmbed_UpdateExternalVideoTag";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listTagId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ListOfTitle", listTitle);
                _db.AddParameter(cmd, "ListOfUrl", listUrl);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxTagEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
