﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Tag
{
    public abstract class TagCloudDalBase
    {
        #region Get methods
        public List<QueueUpdateTagChannelEntity> GetUnProcess(int channelId)
        {
            const string commandText = "CMS_QueueUpdateTagChannel_GetUnProcessByChannel";
            try
            {
                List<QueueUpdateTagChannelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ChannelId", channelId);
                data = _db.GetList<QueueUpdateTagChannelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update methods
        public bool UpdateIsProcessed(long queueUpdateTagCloudId)
        {

            const string commandText = "CMS_QueueUpdateTagChannel_UpdateProcessed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QueueUpdateTagChannelId", queueUpdateTagCloudId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ChannelVN.CMS.Common.Logger.LogType.Fatal, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Core members

        private readonly TagCloudCmsDb _db;

        protected TagCloudDalBase(TagCloudCmsDb db)
        {
            _db = db;
        }

        protected TagCloudCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
