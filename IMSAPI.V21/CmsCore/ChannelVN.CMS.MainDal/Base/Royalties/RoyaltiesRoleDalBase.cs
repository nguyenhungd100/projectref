﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public abstract class RoyaltiesRoleDalBase
    {
        #region Get

        public RoyaltiesRoleEntity GetById(int id)
        {
            const string commandText = "CMS_RoyaltiesRole_GetById";
            try
            {
                RoyaltiesRoleEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<RoyaltiesRoleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesRoleEntity> GetAll()
        {
            const string commandText = "CMS_RoyaltiesRole_GetAll";
            try
            {
                List<RoyaltiesRoleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<RoyaltiesRoleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool Insert(RoyaltiesRoleEntity royaltiesRole)
        {
            const string commandText = "CMS_RoyaltiesRole_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoleName", royaltiesRole.RoleName);
                _db.AddParameter(cmd, "IsManager", royaltiesRole.IsManager);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(RoyaltiesRoleEntity royaltiesRole)
        {
            const string commandText = "CMS_RoyaltiesRole_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesRole.Id);
                _db.AddParameter(cmd, "RoleName", royaltiesRole.RoleName);
                _db.AddParameter(cmd, "IsManager", royaltiesRole.IsManager);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int royaltiesRoleId)
        {
            const string commandText = "CMS_RoyaltiesRole_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesRoleId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesRoleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
