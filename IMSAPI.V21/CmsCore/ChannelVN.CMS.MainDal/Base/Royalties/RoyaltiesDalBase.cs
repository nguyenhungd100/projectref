﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public abstract class RoyaltiesDalBase
    {

        #region Get

        public RoyaltiesEntity GetById(long id)
        {
            const string commandText = "CMS_Royalties_GetById";
            try
            {
                RoyaltiesEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<RoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesEntity> Search(int type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Royalties_Search";
            try
            {
                List<RoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "RoyaltiesMemberId", royaltiesMemberId);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesRoleId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesRate);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<RoyaltiesEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesEntity> GetByNewsId(long newsId)
        {
            const string commandText = "CMS_Royalties_GetByNewsId";
            try
            {
                List<RoyaltiesEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<RoyaltiesEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesGroupByUserEntity> SearchGroupByUser(int type, int zoneId, int royaltiesMemberId, int royaltiesCategoryId, int royaltiesRoleId, string keyword, int royaltiesRate, DateTime fromDate, DateTime toDate)
        {
            const string commandText = "CMS_Royalties_SearchGroupByUser";
            try
            {
                List<RoyaltiesGroupByUserEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "RoyaltiesMemberId", royaltiesMemberId);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesRoleId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesRate);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<RoyaltiesGroupByUserEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool Insert(RoyaltiesEntity royalties, ref long newRoyaltiesId)
        {
            const string commandText = "CMS_Royalties_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", royalties.NewsId);
                _db.AddParameter(cmd, "VideoId", royalties.VideoId);
                _db.AddParameter(cmd, "PhotoId", royalties.PhotoId);
                _db.AddParameter(cmd, "RoyaltiesMemberId", royalties.RoyaltiesMemberId);
                _db.AddParameter(cmd, "MemberAlias", royalties.MemberAlias);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royalties.RoyaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRate", royalties.RoyaltiesRate);
                _db.AddParameter(cmd, "RoyaltiesValue", royalties.RoyaltiesValue);
                _db.AddParameter(cmd, "RoyaltiesBonus", royalties.RoyaltiesBonus);
                _db.AddParameter(cmd, "RoyaltiesPenalty", royalties.RoyaltiesPenalty);
                _db.AddParameter(cmd, "RoyaltiesTotalValue", royalties.RoyaltiesTotalValue);
                _db.AddParameter(cmd, "CountPhotoClassA", royalties.CountPhotoClassA);
                _db.AddParameter(cmd, "CountPhotoClassB", royalties.CountPhotoClassB);
                _db.AddParameter(cmd, "PhotoRoyaltiesValue", royalties.PhotoRoyaltiesValue);
                _db.AddParameter(cmd, "CountClipClassA", royalties.CountClipClassA);
                _db.AddParameter(cmd, "CountClipClassB", royalties.CountClipClassB);
                _db.AddParameter(cmd, "ClipRoyaltiesValue", royalties.ClipRoyaltiesValue);
                _db.AddParameter(cmd, "SelfProduceClipRoyaltiesValue", royalties.SelfProduceClipRoyaltiesValue);
                _db.AddParameter(cmd, "Note", royalties.Note);
                _db.AddParameter(cmd, "CreatedBy", royalties.CreatedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                newRoyaltiesId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(RoyaltiesEntity royalties)
        {
            const string commandText = "CMS_Royalties_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royalties.Id);
                _db.AddParameter(cmd, "NewsId", royalties.NewsId);
                _db.AddParameter(cmd, "VideoId", royalties.VideoId);
                _db.AddParameter(cmd, "PhotoId", royalties.PhotoId);
                _db.AddParameter(cmd, "RoyaltiesMemberId", royalties.RoyaltiesMemberId);
                _db.AddParameter(cmd, "MemberAlias", royalties.MemberAlias);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royalties.RoyaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRate", royalties.RoyaltiesRate);
                _db.AddParameter(cmd, "RoyaltiesValue", royalties.RoyaltiesValue);
                _db.AddParameter(cmd, "RoyaltiesBonus", royalties.RoyaltiesBonus);
                _db.AddParameter(cmd, "RoyaltiesPenalty", royalties.RoyaltiesPenalty);
                _db.AddParameter(cmd, "RoyaltiesTotalValue", royalties.RoyaltiesTotalValue);
                _db.AddParameter(cmd, "CountPhotoClassA", royalties.CountPhotoClassA);
                _db.AddParameter(cmd, "CountPhotoClassB", royalties.CountPhotoClassB);
                _db.AddParameter(cmd, "PhotoRoyaltiesValue", royalties.PhotoRoyaltiesValue);
                _db.AddParameter(cmd, "CountClipClassA", royalties.CountClipClassA);
                _db.AddParameter(cmd, "CountClipClassB", royalties.CountClipClassB);
                _db.AddParameter(cmd, "ClipRoyaltiesValue", royalties.ClipRoyaltiesValue);
                _db.AddParameter(cmd, "SelfProduceClipRoyaltiesValue", royalties.SelfProduceClipRoyaltiesValue);
                _db.AddParameter(cmd, "Note", royalties.Note);
                _db.AddParameter(cmd, "LastModifiedBy", royalties.LastModifiedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int royaltiesId)
        {
            const string commandText = "CMS_Royalties_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteByNewsId(long newsId)
        {
            const string commandText = "CMS_Royalties_DeleteByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
