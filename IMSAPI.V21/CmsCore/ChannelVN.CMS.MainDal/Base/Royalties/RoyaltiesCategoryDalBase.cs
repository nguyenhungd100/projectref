﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public abstract class RoyaltiesCategoryDalBase
    {
        #region Get

        public RoyaltiesCategoryEntity GetById(int id)
        {
            const string commandText = "CMS_RoyaltiesCategory_GetById";
            try
            {
                RoyaltiesCategoryEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<RoyaltiesCategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<RoyaltiesCategoryEntity> GetAll()
        {
            const string commandText = "CMS_RoyaltiesCategory_GetAll";
            try
            {
                List<RoyaltiesCategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<RoyaltiesCategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesCategoryEntity> GetByRoyaltiesCategoryType(int zoneId, int type)
        {
            const string commandText = "CMS_RoyaltiesCategory_GetByRoyaltiesCategoryType";
            try
            {
                List<RoyaltiesCategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<RoyaltiesCategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool Insert(RoyaltiesCategoryEntity royaltiesCategory)
        {
            const string commandText = "CMS_RoyaltiesCategory_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", royaltiesCategory.ZoneId);
                _db.AddParameter(cmd, "Name", royaltiesCategory.Name);
                _db.AddParameter(cmd, "Type", royaltiesCategory.Type);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(RoyaltiesCategoryEntity royaltiesCategory)
        {
            const string commandText = "CMS_RoyaltiesCategory_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesCategory.Id);
                _db.AddParameter(cmd, "ZoneId", royaltiesCategory.ZoneId);
                _db.AddParameter(cmd, "Name", royaltiesCategory.Name);
                _db.AddParameter(cmd, "Type", royaltiesCategory.Type);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int royaltiesCategoryId)
        {
            const string commandText = "CMS_RoyaltiesCategory_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesCategoryId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesCategoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
