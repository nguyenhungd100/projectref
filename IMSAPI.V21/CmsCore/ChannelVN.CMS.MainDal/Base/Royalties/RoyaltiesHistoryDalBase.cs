﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public class RoyaltiesHistoryDalBase
    {
        #region Get

        public List<RoyaltiesHistoryEntity> GetByRoyaltiesId(long royaltiesId)
        {
            const string commandText = "CMS_RoyaltiesHistory_GetByRoyaltiesId";
            try
            {
                List<RoyaltiesHistoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoyaltiesId", royaltiesId);
                data = _db.GetList<RoyaltiesHistoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesHistoryEntity> GetOldRoyaltiesHistoryByListRoyaltiesId(string listRoyaltiesId)
        {
            const string commandText = "CMS_RoyaltiesHistory_GetOldRoyaltiesHistoryByListRoyaltiesId";
            try
            {
                List<RoyaltiesHistoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListRoyaltiesId", listRoyaltiesId);
                data = _db.GetList<RoyaltiesHistoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool Insert(RoyaltiesHistoryEntity royaltiesHistory)
        {
            const string commandText = "CMS_RoyaltiesHistory_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "RoyaltiesId", royaltiesHistory.RoyaltiesId);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesHistory.RoyaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesHistory.RoyaltiesRate);
                _db.AddParameter(cmd, "RoyaltiesValue", royaltiesHistory.RoyaltiesValue);
                _db.AddParameter(cmd, "RoyaltiesBonus", royaltiesHistory.RoyaltiesBonus);
                _db.AddParameter(cmd, "RoyaltiesPenalty", royaltiesHistory.RoyaltiesPenalty);
                _db.AddParameter(cmd, "RoyaltiesTotalValue", royaltiesHistory.RoyaltiesTotalValue);
                _db.AddParameter(cmd, "CountPhotoClassA", royaltiesHistory.CountPhotoClassA);
                _db.AddParameter(cmd, "CountPhotoClassB", royaltiesHistory.CountPhotoClassB);
                _db.AddParameter(cmd, "PhotoRoyaltiesValue", royaltiesHistory.PhotoRoyaltiesValue);
                _db.AddParameter(cmd, "CountClipClassA", royaltiesHistory.CountClipClassA);
                _db.AddParameter(cmd, "CountClipClassB", royaltiesHistory.CountClipClassB);
                _db.AddParameter(cmd, "ClipRoyaltiesValue", royaltiesHistory.ClipRoyaltiesValue);
                _db.AddParameter(cmd, "SelfProduceClipRoyaltiesValue", royaltiesHistory.SelfProduceClipRoyaltiesValue);
                _db.AddParameter(cmd, "Note", royaltiesHistory.Note);
                _db.AddParameter(cmd, "LastModifiedBy", royaltiesHistory.LastModifiedBy);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion


        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesHistoryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
