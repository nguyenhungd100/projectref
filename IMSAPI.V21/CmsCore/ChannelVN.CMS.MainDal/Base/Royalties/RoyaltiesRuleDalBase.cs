﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public abstract class RoyaltiesRuleDalBase
    {
        #region Get

        public List<RoyaltiesRuleEntity> GetByZoneId(int zoneId)
        {
            const string commandText = "CMS_RoyaltiesRule_GetByZoneId";
            try
            {
                List<RoyaltiesRuleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<RoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public RoyaltiesRuleEntity GetByRoyaltiesCategoryIdAndRoyaltiesRate(bool isManager, int royaltiesCategoryId, int royaltiesRate)
        {
            const string commandText = "CMS_RoyaltiesRule_GetByRoyaltiesCategoryIdAndRoyaltiesRate";
            try
            {
                RoyaltiesRuleEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "IsManager", isManager);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesCategoryId);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesRate);
                data = _db.Get<RoyaltiesRuleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool UpdateValue(int royaltiesCategoryId, bool isManager, int royaltiesRate, int royaltiesValue)
        {

            const string commandText = "CMS_RoyaltiesRule_UpdateValue";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoyaltiesCategoryId", royaltiesCategoryId);
                _db.AddParameter(cmd, "IsManager", isManager);
                _db.AddParameter(cmd, "RoyaltiesRate", royaltiesRate);
                _db.AddParameter(cmd, "RoyaltiesValue", royaltiesValue);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesRuleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
