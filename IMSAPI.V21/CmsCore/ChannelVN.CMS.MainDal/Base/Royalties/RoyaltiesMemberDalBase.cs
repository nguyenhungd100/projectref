﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Royalties
{
    public abstract class RoyaltiesMemberDalBase
    {
        #region Get

        public RoyaltiesMemberEntity GetById(int id)
        {
            const string commandText = "CMS_RoyaltiesMember_GetById";
            try
            {
                RoyaltiesMemberEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<RoyaltiesMemberEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<RoyaltiesMemberEntity> Search(int royaltiesRoleId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {

            const string commandText = "CMS_RoyaltiesMember_Search";
            try
            {
                List<RoyaltiesMemberEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesRoleId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<RoyaltiesMemberEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Set

        public bool Insert(RoyaltiesMemberEntity royaltiesMember)
        {
            const string commandText = "CMS_RoyaltiesMember_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesMember.RoyaltiesRoleId);
                _db.AddParameter(cmd, "FullName", royaltiesMember.FullName);
                _db.AddParameter(cmd, "MemberAlias", royaltiesMember.MemberAlias);
                _db.AddParameter(cmd, "Username", royaltiesMember.Username);
                _db.AddParameter(cmd, "EmplyeeCode", royaltiesMember.EmplyeeCode);
                _db.AddParameter(cmd, "TaxCode", royaltiesMember.TaxCode);
                _db.AddParameter(cmd, "BankAccount", royaltiesMember.BankAccount);
                _db.AddParameter(cmd, "Phone", royaltiesMember.Phone);
                _db.AddParameter(cmd, "Address", royaltiesMember.Address);
                _db.AddParameter(cmd, "Email", royaltiesMember.Email);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(RoyaltiesMemberEntity royaltiesMember)
        {
            const string commandText = "CMS_RoyaltiesMember_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesMember.Id);
                _db.AddParameter(cmd, "RoyaltiesRoleId", royaltiesMember.RoyaltiesRoleId);
                _db.AddParameter(cmd, "FullName", royaltiesMember.FullName);
                _db.AddParameter(cmd, "MemberAlias", royaltiesMember.MemberAlias);
                _db.AddParameter(cmd, "Username", royaltiesMember.Username);
                _db.AddParameter(cmd, "EmplyeeCode", royaltiesMember.EmplyeeCode);
                _db.AddParameter(cmd, "TaxCode", royaltiesMember.TaxCode);
                _db.AddParameter(cmd, "BankAccount", royaltiesMember.BankAccount);
                _db.AddParameter(cmd, "Phone", royaltiesMember.Phone);
                _db.AddParameter(cmd, "Address", royaltiesMember.Address);
                _db.AddParameter(cmd, "Email", royaltiesMember.Email);
                _db.AddParameter(cmd, "Status", royaltiesMember.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int royaltiesMemberId)
        {
            const string commandText = "CMS_RoyaltiesMember_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", royaltiesMemberId);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected RoyaltiesMemberDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
