﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.RollingNews
{
    public abstract class RollingNewsDalBase
    {
       
        public List<RollingNewsEntity> Search(string keyword, int status, int pageIndex, int pageSize,
                                                     ref int totalRow)
        {
            const string commandText = "CMS_RollingNews_Search";
            try
            {
                List<RollingNewsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<RollingNewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<RollingNewsForSuggestionEntity> SearchForSuggestion(string keyword, int status, int top)
        {
            const string commandText = "CMS_RollingNews_SearchForSuggestion";
            try
            {
                List<RollingNewsForSuggestionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<RollingNewsForSuggestionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public RollingNewsEntity GetById(int rollingNewsId)
        {
            const string commandText = "CMS_RollingNews_GetById";
            try
            {
                RollingNewsEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsId);
                data = _db.Get<RollingNewsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(RollingNewsEntity rollingNews)
        {
            const string commandText = "CMS_RollingNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Title", rollingNews.Title);
                _db.AddParameter(cmd, "ShowTime", rollingNews.ShowTime);
                _db.AddParameter(cmd, "ShowAuthor", rollingNews.ShowAuthor);
                _db.AddParameter(cmd, "StartDate", rollingNews.StartDate);
                _db.AddParameter(cmd, "PublishedContent", rollingNews.PublishedContent);
                _db.AddParameter(cmd, "CreatedBy", rollingNews.CreatedBy);
                _db.AddParameter(cmd, "Status", rollingNews.Status);
                _db.AddParameter(cmd, "IsShowRollingNewsLabel", rollingNews.IsShowRollingNewsLabel);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertV2(RollingNewsEntity rollingNews, ref int Id)
        {
            const string commandText = "CMS_RollingNews_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", rollingNews.Title);
                _db.AddParameter(cmd, "ShowTime", rollingNews.ShowTime);
                _db.AddParameter(cmd, "ShowAuthor", rollingNews.ShowAuthor);
                _db.AddParameter(cmd, "StartDate", rollingNews.StartDate);
                _db.AddParameter(cmd, "PublishedContent", rollingNews.PublishedContent);
                _db.AddParameter(cmd, "CreatedBy", rollingNews.CreatedBy);
                _db.AddParameter(cmd, "Status", rollingNews.Status);
                _db.AddParameter(cmd, "IsShowRollingNewsLabel", rollingNews.IsShowRollingNewsLabel);

                var numberOfRow = cmd.ExecuteNonQuery();
                Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(RollingNewsEntity rollingNews)
        {
            const string commandText = "CMS_RollingNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNews.Id);
                _db.AddParameter(cmd, "Title", rollingNews.Title);
                _db.AddParameter(cmd, "ShowTime", rollingNews.ShowTime);
                _db.AddParameter(cmd, "ShowAuthor", rollingNews.ShowAuthor);
                _db.AddParameter(cmd, "StartDate", rollingNews.StartDate);
                _db.AddParameter(cmd, "PublishedContent", rollingNews.PublishedContent);
                _db.AddParameter(cmd, "LastModifiedBy", rollingNews.CreatedBy);
                _db.AddParameter(cmd, "Status", rollingNews.Status);
                _db.AddParameter(cmd, "IsShowRollingNewsLabel", rollingNews.IsShowRollingNewsLabel);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int rollingNewsId)
        {
            const string commandText = "CMS_RollingNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ChangeStatus(int rollingNewsId, int status, string lastModifiedBy)
        {
            const string commandText = "CMS_RollingNews_ChangeStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsId);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                _db.AddParameter(cmd, "Status", status);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected RollingNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
