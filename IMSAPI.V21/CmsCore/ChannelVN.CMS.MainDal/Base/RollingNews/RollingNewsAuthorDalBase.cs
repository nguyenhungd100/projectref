﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.RollingNews
{
    public class RollingNewsAuthorDalBase
    {
        public List<RollingNewsAuthorEntity> GetByRollingNewsId(int rollingNewsId)
        {
            const string commandText = "CMS_RollingNewsAuthor_GetByRollingNewsId";
            try
            {
                List<RollingNewsAuthorEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);
                data = _db.GetList<RollingNewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public RollingNewsAuthorEntity GetById(int rollingNewsAuthorId)
        {
            const string commandText = "CMS_RollingNewsAuthor_GetById";
            try
            {
                RollingNewsAuthorEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsAuthorId);
                data = _db.Get<RollingNewsAuthorEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Insert(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            const string commandText = "CMS_RollingNewsAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsAuthor.RollingNewsId);
                _db.AddParameter(cmd, "AuthorRole", rollingNewsAuthor.AuthorRole);
                _db.AddParameter(cmd, "AuthorName", rollingNewsAuthor.AuthorName);
                _db.AddParameter(cmd, "Username", rollingNewsAuthor.Username);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(RollingNewsAuthorEntity rollingNewsAuthor)
        {
            const string commandText = "CMS_RollingNewsAuthor_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsAuthor.Id);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsAuthor.RollingNewsId);
                _db.AddParameter(cmd, "AuthorRole", rollingNewsAuthor.AuthorRole);
                _db.AddParameter(cmd, "AuthorName", rollingNewsAuthor.AuthorName);
                _db.AddParameter(cmd, "Username", rollingNewsAuthor.Username);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int rollingNewsAuthorId)
        {
            const string commandText = "CMS_RollingNewsAuthor_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsAuthorId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #region Core members

        private readonly CmsMainDb _db;

        protected RollingNewsAuthorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
