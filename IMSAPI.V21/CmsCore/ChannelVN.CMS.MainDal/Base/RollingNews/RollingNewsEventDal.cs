﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.RollingNews
{
    public class RollingNewsEventDal : RollingNewsEventDalBase
    {
        internal RollingNewsEventDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
