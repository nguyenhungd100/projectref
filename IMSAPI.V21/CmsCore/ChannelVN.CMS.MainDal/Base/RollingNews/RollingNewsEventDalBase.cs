﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.RollingNews;
using ChannelVN.CMS.Entity.Base.Royalties;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.RollingNews
{
    public abstract class RollingNewsEventDalBase
    {
        public List<RollingNewsEventEntity> GetByRollingNewsId(int rollingNewsId, int eventType, int status)
        {
            const string commandText = "CMS_RollingNewsEvent_GetByRollingNewsId";
            try
            {
                List<RollingNewsEventEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);
                _db.AddParameter(cmd, "EventType", eventType);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<RollingNewsEventEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public RollingNewsEventEntity GetById(int rollingNewsEventId)
        {
            const string commandText = "CMS_RollingNewsEvent_GetById";
            try
            {
                RollingNewsEventEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEventId);
                data = _db.Get<RollingNewsEventEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Insert(RollingNewsEventEntity rollingNewsEvent)
        {
            const string commandText = "CMS_RollingNewsEvent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsEvent.RollingNewsId);
                _db.AddParameter(cmd, "EventType", rollingNewsEvent.EventType);
                _db.AddParameter(cmd, "EventContent", rollingNewsEvent.EventContent);
                _db.AddParameter(cmd, "EventNote", rollingNewsEvent.EventNote);
                _db.AddParameter(cmd, "EventTime", rollingNewsEvent.EventTime);
                _db.AddParameter(cmd, "IsFocus", rollingNewsEvent.IsFocus);
                _db.AddParameter(cmd, "CreatedBy", rollingNewsEvent.CreatedBy);
                _db.AddParameter(cmd, "MobileContent", rollingNewsEvent.MobileContent);
                _db.AddParameter(cmd, "ShortContent", rollingNewsEvent.ShortContent);
                _db.AddParameter(cmd, "Images", rollingNewsEvent.Images);
                _db.AddParameter(cmd, "ImageCount", rollingNewsEvent.ImageCount);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertV2(RollingNewsEventEntity rollingNewsEvent, ref int Id)
        {
            const string commandText = "CMS_RollingNewsEvent_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsEvent.RollingNewsId);
                _db.AddParameter(cmd, "EventType", rollingNewsEvent.EventType);
                _db.AddParameter(cmd, "EventContent", rollingNewsEvent.EventContent);
                _db.AddParameter(cmd, "EventNote", rollingNewsEvent.EventNote);
                _db.AddParameter(cmd, "EventTime", rollingNewsEvent.EventTime);
                _db.AddParameter(cmd, "IsFocus", rollingNewsEvent.IsFocus);
                _db.AddParameter(cmd, "CreatedBy", rollingNewsEvent.CreatedBy);
                _db.AddParameter(cmd, "MobileContent", rollingNewsEvent.MobileContent);
                _db.AddParameter(cmd, "ShortContent", rollingNewsEvent.ShortContent);
                _db.AddParameter(cmd, "Images", rollingNewsEvent.Images);
                _db.AddParameter(cmd, "ImageCount", rollingNewsEvent.ImageCount);

                var numberOfRow = cmd.ExecuteNonQuery();
                Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(RollingNewsEventEntity rollingNewsEvent)
        {
            const string commandText = "CMS_RollingNewsEvent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEvent.Id);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsEvent.RollingNewsId);
                _db.AddParameter(cmd, "EventType", rollingNewsEvent.EventType);
                _db.AddParameter(cmd, "EventContent", rollingNewsEvent.EventContent);
                _db.AddParameter(cmd, "EventNote", rollingNewsEvent.EventNote);
                _db.AddParameter(cmd, "EventTime", rollingNewsEvent.EventTime);
                _db.AddParameter(cmd, "IsFocus", rollingNewsEvent.IsFocus);
                _db.AddParameter(cmd, "LastModifiedBy", rollingNewsEvent.CreatedBy);
                _db.AddParameter(cmd, "MobileContent", rollingNewsEvent.MobileContent);
                _db.AddParameter(cmd, "ShortContent", rollingNewsEvent.ShortContent);
                _db.AddParameter(cmd, "Images", rollingNewsEvent.Images);
                _db.AddParameter(cmd, "ImageCount", rollingNewsEvent.ImageCount);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int rollingNewsEventId)
        {
            const string commandText = "CMS_RollingNewsEvent_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEventId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByRollingNewsId(int rollingNewsId)
        {
            const string commandText = "CMS_RollingNewsEvent_Delete_ByRollingNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteByIds(int rollingNewsId, string Ids)
        {
            const string commandText = "CMS_RollingNewsEvent_Delete_ByIds";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RollingNewsId", rollingNewsId);
                _db.AddParameter(cmd, "Ids", Ids);
                var numberOfRow = cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Publish(int rollingNewsEventId, string publishedBy)
        {
            const string commandText = "CMS_RollingNewsEvent_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEventId);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Unpublish(int rollingNewsEventId, string unpublishedBy)
        {
            const string commandText = "CMS_RollingNewsEvent_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEventId);
                _db.AddParameter(cmd, "UnpublishedBy", unpublishedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateIsFocus(int rollingNewsEventId, bool isFocus, string lastModifiedBy)
        {
            const string commandText = "CMS_RollingNewsEvent_Unpublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", rollingNewsEventId);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                _db.AddParameter(cmd, "LastModifiedBy", lastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }


        #region Core members

        private readonly CmsMainDb _db;

        protected RollingNewsEventDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
