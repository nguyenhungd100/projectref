﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewV3QuestionDalBase
    {
        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewV3QuestionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        #region GET
        public List<InterviewV3QuestionEntity> SearchInterviewV3Question(int interviewId, string keyword, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_InterviewV3Question_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Keyword", keyword);                
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);                
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<InterviewV3QuestionEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByInterviewId(int interviewId)
        {
            const string commandText = "CMS_InterviewV3Question_GetByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                List<InterviewV3QuestionEntity> interviewQuestionList = _db.GetList<InterviewV3QuestionEntity>(cmd);
                return interviewQuestionList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<InterviewV3QuestionDisplayListEntity> InterviewV3QuestionSimpleListByInterviewId(int interviewId)
        {
            const string commandText = "CMS_InterviewV3Question_GetListByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                List<InterviewV3QuestionDisplayListEntity> interviewQuestionList = _db.GetList<InterviewV3QuestionDisplayListEntity>(cmd);
                return interviewQuestionList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<InterviewV3QuestionEntity> InterviewV3QuestionGetListByStatus(int interviewId, EnumInterviewV3QuestionStatus status, EnumInterviewV3QuestionOrder orderBy)
        {
            const string commandText = "CMS_InterviewV3Question_GetByStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                List<InterviewV3QuestionEntity> interviewQuestionList = _db.GetList<InterviewV3QuestionEntity>(cmd);
                return interviewQuestionList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public InterviewV3QuestionEntity InterviewV3QuestionGetById(int id)
        {
            const string commandText = "CMS_InterviewV3Question_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                InterviewV3QuestionEntity interviewQuestionEntity = _db.Get<InterviewV3QuestionEntity>(cmd);
                return interviewQuestionEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertExtensionData(int interviewId, string data, string userDoAction, ref int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewV3Question_InsertExtensionData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Content", data);
                _db.AddParameter(cmd, "CreatedBy", userDoAction);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewQuestionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            const string commandText = "CMS_InterviewV3Question_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "ListOfInterviewQuestionId", listOfInterviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewV3QuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            const string commandText = "CMS_InterviewV3Question_GetAllPublishedQuestionByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                List<InterviewV3QuestionEntity> numberOfRow = _db.GetList<InterviewV3QuestionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region UPDATE
        public bool InterviewV3QuestionInsert(InterviewV3QuestionEntity interviewV3QuestionEntity, out int questionId)
        {
            const string commandText = "CMS_InterviewV3Question_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);

                _db.AddParameter(cmd, "Priority", interviewV3QuestionEntity.Priority);
                _db.AddParameter(cmd, "InterviewId", interviewV3QuestionEntity.InterviewId);
                _db.AddParameter(cmd, "Type", interviewV3QuestionEntity.Type);
                _db.AddParameter(cmd, "Status", interviewV3QuestionEntity.Status);
                _db.AddParameter(cmd, "ProcessUser", interviewV3QuestionEntity.ProcessUser);
                _db.AddParameter(cmd, "Content", interviewV3QuestionEntity.Content);
                _db.AddParameter(cmd, "FullName", interviewV3QuestionEntity.FullName);
                _db.AddParameter(cmd, "Sex", interviewV3QuestionEntity.Sex);
                _db.AddParameter(cmd, "Age", interviewV3QuestionEntity.Age);
                _db.AddParameter(cmd, "Email", interviewV3QuestionEntity.Email);
                _db.AddParameter(cmd, "Job", interviewV3QuestionEntity.Job);
                _db.AddParameter(cmd, "Address", interviewV3QuestionEntity.Address);
                _db.AddParameter(cmd, "Mobile", interviewV3QuestionEntity.Mobile);
                _db.AddParameter(cmd, "CreatedDate", interviewV3QuestionEntity.CreatedDate);
                _db.AddParameter(cmd, "ReceivedDate", interviewV3QuestionEntity.ReceivedDate);
                _db.AddParameter(cmd, "ReceivedBy", interviewV3QuestionEntity.ReceivedBy);
                _db.AddParameter(cmd, "PublishedDate", interviewV3QuestionEntity.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", interviewV3QuestionEntity.PublishedBy);
                _db.AddParameter(cmd, "LastModifiedDate", interviewV3QuestionEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", interviewV3QuestionEntity.LastModifiedBy);
                
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                questionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        public bool InterviewV3QuestionUpdate(InterviewV3QuestionEntity interviewV3QuestionEntity)
        {
            const string commandText = "CMS_InterviewV3Question_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewV3QuestionEntity.Id);
                _db.AddParameter(cmd, "Priority", interviewV3QuestionEntity.Priority);
                _db.AddParameter(cmd, "InterviewId", interviewV3QuestionEntity.InterviewId);
                _db.AddParameter(cmd, "Type", interviewV3QuestionEntity.Type);
                _db.AddParameter(cmd, "Status", interviewV3QuestionEntity.Status);
                _db.AddParameter(cmd, "ProcessUser", interviewV3QuestionEntity.ProcessUser);
                _db.AddParameter(cmd, "Content", interviewV3QuestionEntity.Content);
                _db.AddParameter(cmd, "FullName", interviewV3QuestionEntity.FullName);
                _db.AddParameter(cmd, "Sex", interviewV3QuestionEntity.Sex);
                _db.AddParameter(cmd, "Age", interviewV3QuestionEntity.Age);
                _db.AddParameter(cmd, "Email", interviewV3QuestionEntity.Email);
                _db.AddParameter(cmd, "Job", interviewV3QuestionEntity.Job);
                _db.AddParameter(cmd, "Address", interviewV3QuestionEntity.Address);
                _db.AddParameter(cmd, "Mobile", interviewV3QuestionEntity.Mobile);
                _db.AddParameter(cmd, "CreatedDate", interviewV3QuestionEntity.CreatedDate);
                _db.AddParameter(cmd, "ReceivedDate", interviewV3QuestionEntity.ReceivedDate);
                _db.AddParameter(cmd, "ReceivedBy", interviewV3QuestionEntity.ReceivedBy);
                _db.AddParameter(cmd, "PublishedDate", interviewV3QuestionEntity.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", interviewV3QuestionEntity.PublishedBy);
                _db.AddParameter(cmd, "LastModifiedDate", interviewV3QuestionEntity.LastModifiedDate);
                _db.AddParameter(cmd, "LastModifiedBy", interviewV3QuestionEntity.LastModifiedBy);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3QuestionUpdatePriority(int questionId,int priority)
        {
            const string commandText = "CMS_InterviewV3Question_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                _db.AddParameter(cmd, "Priority", priority);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3QuestionUpdateQuestionContent(int questionId, string questionContent,string updateBy)
        {
            const string commandText = "CMS_InterviewV3Question_UpdateQuestionContent";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                _db.AddParameter(cmd, "Content", questionContent);
                _db.AddParameter(cmd, "UserName", updateBy);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3QuestionUpdateQuestionStatus(int questionId, EnumInterviewV3QuestionStatus status)
        {
            const string commandText = "CMS_InterviewV3Question_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                _db.AddParameter(cmd, "Status", status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3QuestionReceiveQuestion(int questionId, string processUser, EnumInterviewV3QuestionStatus status)
        {
            const string commandText = "CMS_InterviewV3Question_ReceiveQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", questionId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ProcessUser", processUser);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewV3Question_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
    }
}
