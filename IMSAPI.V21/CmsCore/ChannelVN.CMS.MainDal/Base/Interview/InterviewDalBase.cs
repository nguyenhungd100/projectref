﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewDalBase
    {
        #region Get

        public InterviewEntity GetInterviewByInterviewId(int interviewId)
        {
            const string commandText = "CMS_Interview_GetInterviewByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                InterviewEntity numberOfRow = _db.Get<InterviewEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public InterviewWithSimpleFieldEntity GetInterviewShortInfoByInterviewId(int interviewId)
        {
            const string commandText = "CMS_Interview_GetInterviewShortInfoByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                InterviewWithSimpleFieldEntity numberOfRow = _db.Get<InterviewWithSimpleFieldEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewWithSimpleFieldEntity> SearchInterview(string keyword, int isFocus, int isActived, int status, DateTime startDateFrom, DateTime startDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Interview_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                _db.AddParameter(cmd, "IsActived", isActived);
                _db.AddParameter(cmd, "Status", status);
                if (startDateFrom == DateTime.MinValue)
                    _db.AddParameter(cmd, "StartDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "StartDateFrom", startDateFrom);
                if (startDateFrom == DateTime.MinValue)
                    _db.AddParameter(cmd, "StartDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "StartDateTo", startDateTo);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<InterviewWithSimpleFieldEntity> numberOfRow = _db.GetList<InterviewWithSimpleFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewForSuggestionEntity> SearchInterviewForSuggestion(int top, string keyword, int isFocus, int isActived)
        {
            const string commandText = "CMS_Interview_SearchForSuggest";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Top", top);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                _db.AddParameter(cmd, "IsActived", isActived);
                List<InterviewForSuggestionEntity> numberOfRow = _db.GetList<InterviewForSuggestionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public InterviewPublishedEntity GetInterviewPublishedByInterviewId(int interviewId)
        {
            const string commandText = "CMS_InterviewPublished_GetByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                InterviewPublishedEntity numberOfRow = _db.Get<InterviewPublishedEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Update

        public bool Insert(InterviewEntity interview, ref int interviewId)
        {
            const string commandText = "CMS_Interview_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", interview.Title);
                _db.AddParameter(cmd, "UnsignTitle", interview.UnsignTitle);
                _db.AddParameter(cmd, "StartDate", interview.StartDate);
                _db.AddParameter(cmd, "IsFocus", interview.IsFocus);
                _db.AddParameter(cmd, "IsActived", interview.IsActived);
                _db.AddParameter(cmd, "Status", interview.Status);
                _db.AddParameter(cmd, "CreatedBy", interview.CreatedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(InterviewEntity interview)
        {
            const string commandText = "CMS_Interview_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interview.Id);
                _db.AddParameter(cmd, "Title", interview.Title);
                _db.AddParameter(cmd, "UnsignTitle", interview.UnsignTitle);
                _db.AddParameter(cmd, "StartDate", interview.StartDate);
                _db.AddParameter(cmd, "IsFocus", interview.IsFocus);
                _db.AddParameter(cmd, "IsActived", interview.IsActived);
                _db.AddParameter(cmd, "Status", interview.Status);
                _db.AddParameter(cmd, "LastModifiedBy", interview.LastModifiedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(long interviewId)
        {
            const string commandText = "CMS_Interview_DeleteByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateInterviewFocus(long interviewId, bool isFocus)
        {
            const string commandText = "CMS_Interview_UpdateInterviewFocus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateInterviewActive(long interviewId, bool isActived)
        {
            const string commandText = "CMS_Interview_UpdateInterviewActive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                _db.AddParameter(cmd, "IsActived", isActived);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool PublishInterviewContent(long interviewId, string ascQuestionContent, string descQuestionContent, string publishedBy)
        {
            const string commandText = "CMS_Interview_PublishInterviewContent";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                _db.AddParameter(cmd, "AscQuestionContent", ascQuestionContent);
                _db.AddParameter(cmd, "DescQuestionContent", descQuestionContent);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
