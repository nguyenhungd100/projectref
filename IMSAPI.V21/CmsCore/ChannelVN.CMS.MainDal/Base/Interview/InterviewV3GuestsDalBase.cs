﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewV3GuestsDalBase
    {
        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewV3GuestsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        #region UPDATE
        public bool InterviewV3GuestsInsert(InterviewV3GuestsEntity interviewV3GuestsEntity,out int guestId)
        {
            const string commandText = "CMS_InterviewV3Guests_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0,System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewV3GuestsEntity.InterviewId);
                _db.AddParameter(cmd, "FullName", interviewV3GuestsEntity.FullName);
                _db.AddParameter(cmd, "Avatar", interviewV3GuestsEntity.Avatar);
                _db.AddParameter(cmd, "Desc", interviewV3GuestsEntity.Desc);
                _db.AddParameter(cmd, "OnlineUserName", interviewV3GuestsEntity.OnlineUserName);
                _db.AddParameter(cmd, "CreatedBy", interviewV3GuestsEntity.CreatedBy);
                _db.AddParameter(cmd, "Status", interviewV3GuestsEntity.Status);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                guestId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3GuestsUpdateInfo(InterviewV3GuestsEntity interviewV3GuestsEntity)
        {
            const string commandText = "CMS_InterviewV3Guests_UpdateInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewV3GuestsEntity.Id);
                _db.AddParameter(cmd, "FullName", interviewV3GuestsEntity.FullName);
                _db.AddParameter(cmd, "Avatar", interviewV3GuestsEntity.Avatar);
                _db.AddParameter(cmd, "OnlineUserName", interviewV3GuestsEntity.OnlineUserName);
                _db.AddParameter(cmd, "Desc", interviewV3GuestsEntity.Desc);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3GuestsDelete(int guestId,int interviewId)
        {
            const string commandText = "CMS_InterviewV3Guests_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GuestId", guestId);
                _db.AddParameter(cmd, "InterviewId", interviewId);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3GuestsUpdateStatus(int guestId, bool status)
        {
            const string commandText = "CMS_InterviewV3Guests_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "GuestId", guestId);
                _db.AddParameter(cmd, "Status", status);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region GET
        public InterviewV3GuestsEntity InterviewV3GuestsGetById(int id)
        {
            const string commandText = "CMS_InterviewV3Guests_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                InterviewV3GuestsEntity result = _db.Get<InterviewV3GuestsEntity>(cmd);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public InterviewV3GuestsEntity InterviewV3GuestsGetInterviewIdAndUserName(int interviewId,string userName)
        {
            const string commandText = "CMS_InterviewV3Guests_GetByInterviewIdAndUserName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "interviewId", interviewId);
                _db.AddParameter(cmd, "OnlineUserName", userName);

                InterviewV3GuestsEntity result = _db.Get<InterviewV3GuestsEntity>(cmd);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<InterviewV3GuestsEntity> InterviewV3GuestsGetByInterviewId(int interviewId,bool status)
        {
            const string commandText = "CMS_InterviewV3Guests_GetByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Status", status);

                var result = _db.GetList<InterviewV3GuestsEntity>(cmd);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
    }
}