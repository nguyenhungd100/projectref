﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewQuestionDalBase
    {
        #region Get

        public InterviewQuestionEntity GetInterviewQuestionByInterviewQuestionId(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_GetInterviewQuestionByInterviewQuestionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                InterviewQuestionEntity numberOfRow = _db.Get<InterviewQuestionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewQuestionWithSimpleFieldEntity> SearchInterviewQuestion(int interviewId, string keyword, int type, string username, int fieldFilterForUsername, int status, bool invertStatus, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_InterviewQuestion_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "FieldFilterByUsername", fieldFilterForUsername);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "InvertStatus", invertStatus);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                var numberOfRow = _db.GetList<InterviewQuestionWithSimpleFieldEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewQuestionEntity> GetAllPublishedQuestionByInterviewId(int interviewId)
        {
            const string commandText = "CMS_InterviewQuestion_GetAllPublishedQuestionByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                List<InterviewQuestionEntity> numberOfRow = _db.GetList<InterviewQuestionEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<QuestionCountByChannelEntity> GetQuestionCountByChannel(int interviewId)
        {
            const string commandText = "CMS_InterviewQuestion_GetCountInterviewQuestionForAllChannel";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                List<QuestionCountByChannelEntity> numberOfRow = _db.GetList<QuestionCountByChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Working flow

        public bool ReceivedQuestion(int interviewQuestionId, string receivedBy)
        {
            const string commandText = "CMS_InterviewQuestion_ReceivedQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "ReceivedBy", receivedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DistributeQuestion(long interviewQuestionId, int interviewChannelId, string distributedBy)
        {
            const string commandText = "CMS_InterviewQuestion_DistributeQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "ChannelId", interviewChannelId);
                _db.AddParameter(cmd, "DistributedBy", distributedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ForwardQuestion(int interviewQuestionId, int interviewChannelId)
        {
            const string commandText = "CMS_InterviewQuestion_ForwardQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "ChannelId", interviewChannelId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SendAnswerForQuestion(int interviewQuestionId, string answerContent, string answerBy)
        {
            const string commandText = "CMS_InterviewQuestion_SendAnswerForQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "AnswerContent", answerContent);
                _db.AddParameter(cmd, "AnswerBy", answerBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ReturnQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_ReturnQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SendWaitForPublish(int interviewQuestionId, string editedBy)
        {
            const string commandText = "CMS_InterviewQuestion_SendWaitForPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "EditedBy", editedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishQuestion(long interviewQuestionId, string publishedBy)
        {
            const string commandText = "CMS_InterviewQuestion_PublishQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                _db.AddParameter(cmd, "PublishedBy", publishedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UnpublishQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_UnpublishQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Update

        public bool Insert(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Priority", interviewQuestion.Priority);
                _db.AddParameter(cmd, "InterviewId", interviewQuestion.InterviewId);
                _db.AddParameter(cmd, "Type", interviewQuestion.Type);
                _db.AddParameter(cmd, "Content", interviewQuestion.Content);
                _db.AddParameter(cmd, "FullName", interviewQuestion.FullName);
                _db.AddParameter(cmd, "Sex", interviewQuestion.Sex);
                _db.AddParameter(cmd, "Age", interviewQuestion.Age);
                _db.AddParameter(cmd, "Email", interviewQuestion.Email);
                _db.AddParameter(cmd, "Job", interviewQuestion.Job);
                _db.AddParameter(cmd, "Address", interviewQuestion.Address);
                _db.AddParameter(cmd, "Mobile", interviewQuestion.Mobile);
                _db.AddParameter(cmd, "CreatedDate", interviewQuestion.CreatedDate);
                _db.AddParameter(cmd, "ReceivedBy", interviewQuestion.ReceivedBy);
                _db.AddParameter(cmd, "ChannelId", interviewQuestion.ChannelId);
                _db.AddParameter(cmd, "Status", interviewQuestion.Status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewQuestionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertExternalQuestion(InterviewQuestionEntity interviewQuestion, ref int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_InsertExternalQuestion";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Priority", interviewQuestion.Priority);
                _db.AddParameter(cmd, "InterviewId", interviewQuestion.InterviewId);
                _db.AddParameter(cmd, "Type", interviewQuestion.Type);
                _db.AddParameter(cmd, "Content", interviewQuestion.Content);
                _db.AddParameter(cmd, "FullName", interviewQuestion.FullName);
                _db.AddParameter(cmd, "Sex", interviewQuestion.Sex);
                _db.AddParameter(cmd, "Age", interviewQuestion.Age);
                _db.AddParameter(cmd, "Email", interviewQuestion.Email);
                _db.AddParameter(cmd, "Job", interviewQuestion.Job);
                _db.AddParameter(cmd, "Address", interviewQuestion.Address);
                _db.AddParameter(cmd, "Mobile", interviewQuestion.Mobile);
                _db.AddParameter(cmd, "CreatedDate", interviewQuestion.CreatedDate);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewQuestionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertExtensionData(int interviewId, string data, string userDoAction, ref int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_InsertExtensionData";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Answer", data);
                _db.AddParameter(cmd, "CreatedBy", userDoAction);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewQuestionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(InterviewQuestionEntity interviewQuestion)
        {
            const string commandText = "CMS_InterviewQuestion_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestion.Id);
                _db.AddParameter(cmd, "Priority", interviewQuestion.Priority);
                _db.AddParameter(cmd, "Type", interviewQuestion.Type);
                _db.AddParameter(cmd, "Content", interviewQuestion.Content);
                _db.AddParameter(cmd, "Answer", interviewQuestion.Answer);
                _db.AddParameter(cmd, "FullName", interviewQuestion.FullName);
                _db.AddParameter(cmd, "Sex", interviewQuestion.Sex);
                _db.AddParameter(cmd, "Age", interviewQuestion.Age);
                _db.AddParameter(cmd, "Email", interviewQuestion.Email);
                _db.AddParameter(cmd, "Job", interviewQuestion.Job);
                _db.AddParameter(cmd, "Address", interviewQuestion.Address);
                _db.AddParameter(cmd, "Mobile", interviewQuestion.Mobile);
                _db.AddParameter(cmd, "LastModifiedBy", interviewQuestion.LastModifiedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(int interviewId, string listOfInterviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "ListOfInterviewQuestionId", listOfInterviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteQuestion(int interviewQuestionId)
        {
            const string commandText = "CMS_InterviewQuestion_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewQuestionId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewQuestionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
