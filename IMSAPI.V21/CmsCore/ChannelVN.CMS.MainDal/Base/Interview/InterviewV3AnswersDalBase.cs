﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewV3AnswersDalBase
    {
        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewV3AnswersDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion

        #region UPDATE
        public bool InterviewV3AnswersInsert(InterviewV3AnswersEntity interviewV3AnswersEntity, out int answersId)
        {
            const string commandText = "CMS_InterviewV3Answers_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "QuestionId", interviewV3AnswersEntity.QuestionId);
                _db.AddParameter(cmd, "GuestId", interviewV3AnswersEntity.GuestId);
                _db.AddParameter(cmd, "AnswersContent", interviewV3AnswersEntity.AnswersContent);
                _db.AddParameter(cmd, "AnswersDate", interviewV3AnswersEntity.AnswersDate);
                _db.AddParameter(cmd, "UserName", interviewV3AnswersEntity.AnswersBy);
                _db.AddParameter(cmd, "Status", interviewV3AnswersEntity.Status);

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                answersId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InterviewV3AnswersUpdate(InterviewV3AnswersEntity interviewV3AnswersEntity)
        {
            const string commandText = "CMS_InterviewV3Answers_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewV3AnswersEntity.Id);
                _db.AddParameter(cmd, "QuestionId", interviewV3AnswersEntity.QuestionId);
                _db.AddParameter(cmd, "GuestId", interviewV3AnswersEntity.GuestId);
                _db.AddParameter(cmd, "AnswersContent", interviewV3AnswersEntity.AnswersContent);
                _db.AddParameter(cmd, "AnswersDate", interviewV3AnswersEntity.AnswersDate);
                _db.AddParameter(cmd, "UserName", interviewV3AnswersEntity.AnswersBy);
                _db.AddParameter(cmd, "Status", interviewV3AnswersEntity.Status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3AnswersUpdateAnswerContent(int answerId,string answerContent,string updateBy)
        {
            const string commandText = "CMS_InterviewV3Answers_UpdateAnswerContent";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answerId);
                _db.AddParameter(cmd, "AnswersContent", answerContent);
                _db.AddParameter(cmd, "UserName", updateBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3AnswersUpdateStatus(int answerId, bool status)
        {
            const string commandText = "CMS_InterviewV3Answers_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answerId);
                _db.AddParameter(cmd, "Status", status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3AnswersDelete(int answersId)
        {
            const string commandText = "CMS_InterviewV3Answers_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", answersId);                

                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region GET
        public List<InterviewV3AnswersEntity> InterviewV3AnswersGetListByQuestionId(int questionId)
        {
            const string commandText = "CMS_InterviewV3Answers_GetByQuestionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", questionId);
                List<InterviewV3AnswersEntity> interviewV3AnswerList = _db.GetList<InterviewV3AnswersEntity>(cmd);
                return interviewV3AnswerList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<InterviewV3AnswersCustomEntity> InterviewV3AnswersGetCustomListByQuestionId(int questionId)
        {
            const string commandText = "CMS_InterviewV3Answers_GetListCustomByQuestionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "QuestionId", questionId);
                List<InterviewV3AnswersCustomEntity> interviewV3AnswerList = _db.GetList<InterviewV3AnswersCustomEntity>(cmd);
                return interviewV3AnswerList;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public InterviewV3AnswersEntity InterviewV3AnswersGetById(int id)
        {
            const string commandText = "CMS_InterviewV3Answers_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                InterviewV3AnswersEntity interviewV3AnswersEntity = _db.Get<InterviewV3AnswersEntity>(cmd);
                return interviewV3AnswersEntity;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
    }
}
