﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public class InterviewChannelProcessDal : InterviewChannelProcessDalBase
    {
        internal InterviewChannelProcessDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
