﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewV3DalBase
    {
        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewV3DalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }
        #endregion

        #region UPDATE
        public bool InterviewV3Insert(InterviewV3Entity interview, out int interviewId)
        {
            const string commandText = "CMS_InterviewV3_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", interview.Title);
                _db.AddParameter(cmd, "UnsignTitle", interview.UnsignTitle);
                _db.AddParameter(cmd, "StartDate", interview.StartDate);
                _db.AddParameter(cmd, "IsFocus", interview.IsFocus);
                _db.AddParameter(cmd, "IsActived", interview.IsActived);
                _db.AddParameter(cmd, "Status", interview.Status);
                _db.AddParameter(cmd, "CreatedBy", interview.CreatedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                interviewId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3Update(InterviewV3Entity interview)
        {
            const string commandText = "CMS_InterviewV3_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interview.Id);
                _db.AddParameter(cmd, "Title", interview.Title);
                _db.AddParameter(cmd, "UnsignTitle", interview.UnsignTitle);
                _db.AddParameter(cmd, "StartDate", interview.StartDate);
                _db.AddParameter(cmd, "IsFocus", interview.IsFocus);
                _db.AddParameter(cmd, "IsActived", interview.IsActived);
                _db.AddParameter(cmd, "Status", interview.Status);
                _db.AddParameter(cmd, "LastModifiedBy", interview.LastModifiedBy);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InterviewV3UpdateStatus(int interviewId,int status)
        {
            const string commandText = "CMS_InterviewV3_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                _db.AddParameter(cmd, "Status", status);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region GET
        public InterviewV3Entity InterviewV3GetById(int interviewId)
        {
            const string commandText = "CMS_InterviewV3_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewId);
                InterviewV3Entity interviewInfo = _db.Get<InterviewV3Entity>(cmd);
                return interviewInfo;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion
    }
}
