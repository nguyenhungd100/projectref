﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewChannelDalBase
    {
        public InterviewChannelEntity GetInterviewChannelByInterviewChannelId(int interviewChannelId)
        {
            const string commandText = "CMS_InterviewChannel_GetByInterviewChannelId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewChannelId);
                InterviewChannelEntity numberOfRow = _db.Get<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewChannelEntity> GetByListOfInterviewId(string listOfInterviewId)
        {
            const string commandText = "CMS_InterviewChannel_GetByListOfInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfInterviewId", listOfInterviewId);
                List<InterviewChannelEntity> numberOfRow = _db.GetList<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewChannelEntity> GetChannelDetailByInterviewId(int interviewId, string username)
        {
            const string commandText = "CMS_InterviewChannel_GetChannelDetailByInterviewId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Username", username);
                List<InterviewChannelEntity> numberOfRow = _db.GetList<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewChannelEntity> GetByInterviewIdAndUsername(int interviewId, string username)
        {
            const string commandText = "CMS_InterviewChannel_GetByInterviewIdAndUsername";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "Username", username);
                List<InterviewChannelEntity> numberOfRow = _db.GetList<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewChannelEntity> GetUserOnChannelRole(int interviewId, int channelRole, string username)
        {
            const string commandText = "CMS_InterviewChannel_GetUserOnChannelRole";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewId", interviewId);
                _db.AddParameter(cmd, "ChannelRole", channelRole);
                _db.AddParameter(cmd, "Username", username);
                List<InterviewChannelEntity> numberOfRow = _db.GetList<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<InterviewChannelEntity> GetInterviewChannelProcess(int interviewChannelId)
        {
            const string commandText = "CMS_InterviewChannel_GetInterviewChannelProcess";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewChannelId", interviewChannelId);
                List<InterviewChannelEntity> numberOfRow = _db.GetList<InterviewChannelEntity>(cmd);
                return numberOfRow;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(InterviewChannelEntity interviewChannel, ref int interviewChannelId)
        {
            const string commandText = "CMS_InterviewChannel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "InterviewId", interviewChannel.InterviewId);
                _db.AddParameter(cmd, "ChannelRole", interviewChannel.ChannelRole);
                _db.AddParameter(cmd, "ChannelName", interviewChannel.ChannelName);
                _db.AddParameter(cmd, "ChannelDescription", interviewChannel.ChannelDescription);
                _db.AddParameter(cmd, "Username", interviewChannel.Username);
                _db.AddParameter(cmd, "Avatar", interviewChannel.Avatar);
                _db.AddParameter(cmd, "ChannelDetail", interviewChannel.ChannelDetail);
                var numberOfRow = cmd.ExecuteNonQuery();
                interviewChannelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(InterviewChannelEntity interviewChannel)
        {
            const string commandText = "CMS_InterviewChannel_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewChannel.Id);
                _db.AddParameter(cmd, "ChannelRole", interviewChannel.ChannelRole);
                _db.AddParameter(cmd, "ChannelName", interviewChannel.ChannelName);
                _db.AddParameter(cmd, "ChannelDescription", interviewChannel.ChannelDescription);
                _db.AddParameter(cmd, "Username", interviewChannel.Username);
                _db.AddParameter(cmd, "Avatar", interviewChannel.Avatar);
                _db.AddParameter(cmd, "ChannelDetail", interviewChannel.ChannelDetail);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Remove(int interviewChannelId)
        {
            const string commandText = "CMS_InterviewChannel_Remove";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", interviewChannelId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewChannelDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
