﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Interview
{
    public abstract class InterviewChannelProcessDalBase
    {
        #region Update

        public bool Update(int interviewChannelId, string listProcessInterviewChannelId)
        {
            const string commandText = "CMS_InterviewChannelProcess_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "InterviewChannelId", interviewChannelId);
                _db.AddParameter(cmd, "ListProcessInterviewChannelId", listProcessInterviewChannelId);
                int numberOfRow = _db.ExecuteNonQuery(cmd);
                return numberOfRow>0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected InterviewChannelProcessDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
