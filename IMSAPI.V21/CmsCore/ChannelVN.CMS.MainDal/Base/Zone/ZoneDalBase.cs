﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Zone
{
    public abstract class ZoneDalBase
    {
        #region Gets

        public ZoneEntity GetZoneById(int id)
        {
            const string commandText = "CMS_Zone_GetById";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneActiveByParentId(int parentId)
        {
            const string commandText = "CMS_Zone_Active_GetByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneByParentId(int parentId)
        {

            const string commandText = "CMS_Zone_GetByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByParentId(int parentId)
        {

            const string commandText = "CMS_Zone_GetActivedByParentId";
            try
            {
                List<ZoneWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneByUserId(int userId)
        {

            const string commandText = "CMS_Zone_GetByUserId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUsername(string username)
        {
            const string commandText = "CMS_Zone_GetByUsername";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUserIdAndPermissionId(int userId, int permissionId)
        {
            const string commandText = "CMS_Zone_GetByUserIdAndPermissionId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneEntity> GetZoneByUsernameAndPermissionId(string username, int permissionId)
        {
            const string commandText = "CMS_Zone_GetByUsernameAndPermissionId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionId", permissionId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "CMS_Zone_GetByUsernameAndPermissionIds";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "CMS_Zone_GetActivedByUsernameAndPermissionIds";
            try
            {
                List<ZoneWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZoneWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneEntity> GetZoneByUserId(int userId, string keyword)
        {

            const string commandText = "CMS_Zone_GetByUserId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "UserId", userId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            const string commandText = "CMS_Zone_GetByNewsId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        public List<ZoneEntity> GetZoneByKeyword(string keyword)
        {
            const string commandText = "CMS_Zone_GetListByKeyword";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ZoneEntity GetPrimaryZoneByNewsId(long newsId)
        {
            const string commandText = "CMS_Zone_GetPrimaryZoneByNewsId";
            try
            {
                ZoneEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.Get<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZoneEntity> GetGroupNewsZoneByParentId(int id)
        {
            const string commandText = "CMS_Zone_GetGroupNewsZoneByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RootZoneId", id);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public int GetMaxIdZone()
        {
            const string commandText = "CMS_Zone_GetMaxId";
            try
            {
                int data = 0;
                var cmd = _db.CreateCommand(commandText, true);              
                data = Utility.ConvertToInt(_db.GetFirtDataRecord(cmd));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Sets

        public bool Insert(ZoneEntity zoneEntity)
        {

            const string commandText = "CMS_Zone_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "Domain", zoneEntity.Domain);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneEntity.AvatarCover);
                _db.AddParameter(cmd, "Logo", zoneEntity.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneEntity.MetaAvatar);
                _db.AddParameter(cmd, "ZoneContent", zoneEntity.ZoneContent);

                var result = cmd.ExecuteNonQuery();

                return result > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertInitDB(ZoneEntity zoneEntity)
        {

            const string commandText = "CMS_Zone_InsertInitDB";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zoneEntity.Id);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "Domain", zoneEntity.Domain);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneEntity.AvatarCover);
                _db.AddParameter(cmd, "Logo", zoneEntity.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneEntity.MetaAvatar);
                _db.AddParameter(cmd, "ZoneContent", zoneEntity.ZoneContent);

                var result = cmd.ExecuteNonQuery();

                return result > 0;

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(ZoneEntity zoneEntity)
        {
            const string commandText = "CMS_Zone_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", zoneEntity.Name);
                _db.AddParameter(cmd, "Description", zoneEntity.Description);
                _db.AddParameter(cmd, "ShortURL", zoneEntity.ShortUrl);
                _db.AddParameter(cmd, "SortOrder", zoneEntity.SortOrder);
                _db.AddParameter(cmd, "ParentId", zoneEntity.ParentId);
                _db.AddParameter(cmd, "Invisibled", zoneEntity.Invisibled);
                _db.AddParameter(cmd, "Status", zoneEntity.Status);
                _db.AddParameter(cmd, "AllowComment", zoneEntity.AllowComment);
                _db.AddParameter(cmd, "Domain", zoneEntity.Domain);
                _db.AddParameter(cmd, "Avatar", zoneEntity.Avatar);
                _db.AddParameter(cmd, "AvatarCover", zoneEntity.AvatarCover);
                _db.AddParameter(cmd, "Logo", zoneEntity.Logo);
                _db.AddParameter(cmd, "MetaAvatar", zoneEntity.MetaAvatar);
                _db.AddParameter(cmd, "ZoneContent", zoneEntity.ZoneContent);

                _db.AddParameter(cmd, "Id", zoneEntity.Id);

                var result = cmd.ExecuteNonQuery();
                return result > 0;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public void MoveUp(int zoneId)
        {
            const string commandText = "CMS_Zone_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public void MoveDown(int zoneId)
        {
            const string commandText = "CMS_Zone_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool ToggleZoneInvisibled(int zoneId)
        {
            const string commandText = "CMS_Zone_ToggleZoneInvisibled";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ToggleZoneStatus(int zoneId)
        {
            const string commandText = "CMS_Zone_ToggleZoneStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ToggleZoneAllowComment(int zoneId)
        {
            const string commandText = "CMS_Zone_ToggleZoneAllowComment";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
