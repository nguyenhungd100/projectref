﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.ZoneDefaultTag
{
    public abstract class ZoneDefaultTagDalBase
    {
        public List<ZoneDefaultTagEntity> GetAllDefaultTagForZone()
        {

            const string commandText = "CMS_ZoneDefaultTag_GetAllDefaultTagForZone";
            try
            {
                List<ZoneDefaultTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<ZoneDefaultTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ZoneDefaultTagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
