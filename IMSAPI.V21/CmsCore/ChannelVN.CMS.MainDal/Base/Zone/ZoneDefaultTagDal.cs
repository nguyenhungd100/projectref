﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.ZoneDefaultTag
{
    public class ZoneDefaultTagDal : ZoneDefaultTagDalBase
    {
        internal ZoneDefaultTagDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
