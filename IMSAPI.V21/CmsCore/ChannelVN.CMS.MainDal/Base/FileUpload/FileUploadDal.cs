﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.FileUpload
{
    public class FileUploadDal : FileUploadDalBase
    {
        internal FileUploadDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
