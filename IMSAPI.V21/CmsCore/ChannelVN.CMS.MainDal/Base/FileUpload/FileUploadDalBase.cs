﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.FileUpload
{
    public abstract class FileUploadDalBase
    {
        #region Update

        public bool InsertFileUpload(FileUploadEntity fileUpload, ref int newFileUploadId)
        {
            const string commandText = "CMS_FileUpload_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUpload.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Status", fileUpload.Status);
                _db.AddParameter(cmd, "ZoneId", fileUpload.ZoneId);
                _db.AddParameter(cmd, "Title", fileUpload.Title);
                _db.AddParameter(cmd, "Description", fileUpload.Description);
                _db.AddParameter(cmd, "FileDownloadPath", fileUpload.FileDownloadPath);
                _db.AddParameter(cmd, "FilePath", fileUpload.FilePath);
                _db.AddParameter(cmd, "FileExt", fileUpload.FileExt);
                _db.AddParameter(cmd, "FileSize", fileUpload.FileSize);
                _db.AddParameter(cmd, "UploadedBy", fileUpload.UploadedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                newFileUploadId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateFileUpload(FileUploadEntity fileUpload)
        {
            const string commandText = "CMS_FileUpload_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUpload.Id);
                _db.AddParameter(cmd, "Status", fileUpload.Status);
                _db.AddParameter(cmd, "ZoneId", fileUpload.ZoneId);
                _db.AddParameter(cmd, "Title", fileUpload.Title);
                _db.AddParameter(cmd, "Description", fileUpload.Description);
                _db.AddParameter(cmd, "FileDownloadPath", fileUpload.FileDownloadPath);
                _db.AddParameter(cmd, "FilePath", fileUpload.FilePath);
                _db.AddParameter(cmd, "FileExt", fileUpload.FileExt);
                _db.AddParameter(cmd, "FileSize", fileUpload.FileSize);
                _db.AddParameter(cmd, "LastModifiedBy", fileUpload.LastModifiedBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteFileUpload(int fileUploadId)
        {
            const string commandText = "CMS_FileUpload_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUploadId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public FileUploadEntity GetFileUploadById(int fileUploadId)
        {
            const string commandText = "CMS_FileUpload_GetById";
            try
            {
                FileUploadEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", fileUploadId);
                data = _db.Get<FileUploadEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FileUploadEntity> SearchFileUpload(string keyword, int zoneId, string ext, string uploadedBy, EnumFileUploadStatus status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FileUpload_Search";
            try
            {
                List<FileUploadEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "UploadedBy", uploadedBy);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "Ext", ext);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<FileUploadEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<FileUploadExtEntity> GetAllExt()
        {
            const string commandText = "CMS_FileUpload_GetAllExt";
            try
            {
                List<FileUploadExtEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<FileUploadExtEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected FileUploadDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
