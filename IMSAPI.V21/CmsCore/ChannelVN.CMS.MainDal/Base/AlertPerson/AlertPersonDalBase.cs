﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AlertPerson;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.AlertPerson
{
    public abstract class AlertPersonDalBase
    {
        #region Get methods
        public AlertPersonEntity GetAlertPersonById(int personId)
        {
            const string commandText = "CMS_Tag_GetAlertPersonByID";
            try
            {
                AlertPersonEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", personId);
                data = _db.Get<AlertPersonEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<AlertPersonEntity> SearchAlertPerson(string keyword, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_AlertPerson_Search";
            try
            {
                List<AlertPersonEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<AlertPersonEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update methods
        public bool Insert(AlertPersonEntity tag, ref int tagId)
        {
            const string commandText = "CMS_AlertPerson_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "PersonName", tag.PersonName);
                _db.AddParameter(cmd, "Status", tag.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(AlertPersonEntity tag)
        {

            const string commandText = "CMS_AlertPerson_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id);
                _db.AddParameter(cmd, "PersonName", tag.PersonName);
                _db.AddParameter(cmd, "Status", tag.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteById(int tagId)
        {
            const string commandText = "CMS_AlertPerson_DeleteByID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tagId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected AlertPersonDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
