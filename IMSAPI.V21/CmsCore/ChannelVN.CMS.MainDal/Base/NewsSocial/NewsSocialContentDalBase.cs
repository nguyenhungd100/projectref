﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsSocial
{
    public abstract class NewsSocialContentDalBase
    {
        #region NewsSocialContent      
        public bool InsertNewsSocialContent(NewsSocialContentEntity newsSocialContent, ref long id)
        {
            const string commandText = "CMS_NewsSocialContent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsSocialContent.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", newsSocialContent.Title);
                _db.AddParameter(cmd, "Sapo", newsSocialContent.Sapo);
                _db.AddParameter(cmd, "Body", newsSocialContent.Body);
                _db.AddParameter(cmd, "Avatar", newsSocialContent.Avatar);
                _db.AddParameter(cmd, "OriginalId", newsSocialContent.OriginalId);
                _db.AddParameter(cmd, "OriginalName", newsSocialContent.OriginalName);
                _db.AddParameter(cmd, "Url", newsSocialContent.Url);
                _db.AddParameter(cmd, "OriginalUrl", newsSocialContent.OriginalUrl);
                _db.AddParameter(cmd, "AvatarCustom", newsSocialContent.AvatarCustom);
                _db.AddParameter(cmd, "Avatar1", newsSocialContent.Avatar1);
                _db.AddParameter(cmd, "Avatar2", newsSocialContent.Avatar2);
                _db.AddParameter(cmd, "Avatar3", newsSocialContent.Avatar3);
                _db.AddParameter(cmd, "Status", newsSocialContent.Status);
                _db.AddParameter(cmd, "Priority", newsSocialContent.Priority);
                _db.AddParameter(cmd, "CreatedDate", newsSocialContent.CreatedDate);
                _db.AddParameter(cmd, "CreatedBy", newsSocialContent.CreatedBy);
                //_db.AddParameter(cmd, "UpdatedDate", newsSocialContent.UpdatedDate);
                //_db.AddParameter(cmd, "UpdatedBy", newsSocialContent.UpdatedBy);                
                _db.AddParameter(cmd, "WordCount", newsSocialContent.WordCount);
                _db.AddParameter(cmd, "Note", newsSocialContent.Note);
                _db.AddParameter(cmd, "RawId", newsSocialContent.RawId);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsSocialContent(NewsSocialContentEntity newsSocialContent)
        {

            const string commandText = "CMS_NewsSocialContent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsSocialContent.Id);
                _db.AddParameter(cmd, "Title", newsSocialContent.Title);
                _db.AddParameter(cmd, "Sapo", newsSocialContent.Sapo);
                _db.AddParameter(cmd, "Body", newsSocialContent.Body);
                _db.AddParameter(cmd, "Avatar", newsSocialContent.Avatar);
                //_db.AddParameter(cmd, "OriginalId", newsSocialContent.OriginalId);
                //_db.AddParameter(cmd, "OriginalName", newsSocialContent.OriginalName);
                //_db.AddParameter(cmd, "Url", newsSocialContent.Url);
                //_db.AddParameter(cmd, "OriginalUrl", newsSocialContent.OriginalUrl);
                _db.AddParameter(cmd, "AvatarCustom", newsSocialContent.AvatarCustom);
                _db.AddParameter(cmd, "Avatar1", newsSocialContent.Avatar1);
                _db.AddParameter(cmd, "Avatar2", newsSocialContent.Avatar2);
                _db.AddParameter(cmd, "Avatar3", newsSocialContent.Avatar3);
                //_db.AddParameter(cmd, "Status", newsSocialContent.Status);
                _db.AddParameter(cmd, "Priority", newsSocialContent.Priority);
                //_db.AddParameter(cmd, "CreatedDate", newsSocialContent.CreatedDate);
                //_db.AddParameter(cmd, "CreatedBy", newsSocialContent.CreatedBy);
                _db.AddParameter(cmd, "UpdatedDate", newsSocialContent.UpdatedDate);
                _db.AddParameter(cmd, "UpdatedBy", newsSocialContent.UpdatedBy);                
                _db.AddParameter(cmd, "WordCount", newsSocialContent.WordCount);
                _db.AddParameter(cmd, "Note", newsSocialContent.Note);
                //_db.AddParameter(cmd, "RawId", newsSocialContent.RawId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNewsSocialContentById(long id)
        {
            const string commandText = "CMS_NewsSocialContent_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool OnToggleStatus(long id)
        {
            const string commandText = "CMS_NewsSocialContent_ToggleStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public NewsSocialContentEntity GetNewsSocialContentById(long id)
        {
            const string commandText = "CMS_NewsSocialContent_GetById";
            try
            {
                NewsSocialContentEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsSocialContentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsSocialContentEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            const string commandText = "CMS_NewsSocialContent_GetByRawId";
            try
            {
                NewsSocialContentEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RawId", rawId);
                data = _db.Get<NewsSocialContentEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsSocialContentEntity> SearchNewsSocialContent(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsSocialContent_Search";
            try
            {
                List<NewsSocialContentEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);                
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                
                data = _db.GetList<NewsSocialContentEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #endregion        

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsSocialContentDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion        
    }
}
