﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsSocial
{
    public abstract class BoxNewsSocialEmbedDalBase
    {
        #region BoxNewsSocialEmbed              

        public List<BoxNewsSocialEmbedEntity> GetListBoxNewsSocialEmbed(int type, int zoneId)
        {
            const string commandText = "CMS_BoxNewsSocialEmbed_GetListByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                List<BoxNewsSocialEmbedEntity> data = _db.GetList<BoxNewsSocialEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public void Update(string listNewsSocialContentId, int type, int zoneId)
        {
            const string commandText = "CMS_BoxNewsSocialEmbed_UpdateListId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listNewsSocialContentId);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected BoxNewsSocialEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion        
    }
}
