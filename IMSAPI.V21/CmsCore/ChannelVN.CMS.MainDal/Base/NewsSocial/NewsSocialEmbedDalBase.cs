﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsSocial
{
    public abstract class NewsSocialEmbedDalBase
    {
        #region NewsSocialEmbed
        public bool InsertNewsSocialEmbed(NewsSocialEmbedEntity streamItem, ref long id)
        {
            const string commandText = "CMS_NewsSocialEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "RawId", streamItem.RawId);
                _db.AddParameter(cmd, "CreatedBy", streamItem.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", streamItem.CreatedDate);
                _db.AddParameter(cmd, "Priority", streamItem.Priority);
                _db.AddParameter(cmd, "Status", streamItem.Status);
                _db.AddParameter(cmd, "OriginalId", streamItem.OriginalId);
                _db.AddParameter(cmd, "Url", streamItem.Url);
                _db.AddParameter(cmd, "Embed", streamItem.Embed);

                var numberOfRow = cmd.ExecuteNonQuery();

                id = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsSocialEmbed(NewsSocialEmbedEntity streamItem)
        {

            const string commandText = "CMS_NewsSocialEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", streamItem.Id);
                _db.AddParameter(cmd, "RawId", streamItem.RawId);
                _db.AddParameter(cmd, "CreatedBy", streamItem.CreatedBy);
                _db.AddParameter(cmd, "CreatedDate", streamItem.CreatedDate);
                _db.AddParameter(cmd, "Priority", streamItem.Priority);
                _db.AddParameter(cmd, "Status", streamItem.Status);
                _db.AddParameter(cmd, "OriginalId", streamItem.OriginalId);
                _db.AddParameter(cmd, "Url", streamItem.Url);
                _db.AddParameter(cmd, "Embed", streamItem.Embed);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNewsSocialEmbedById(long id)
        {
            const string commandText = "CMS_NewsSocialEmbed_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool OnToggleStatus(long id)
        {
            const string commandText = "CMS_NewsSocialEmbed_ToggleStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public NewsSocialEmbedEntity GetNewsSocialEmbedById(long id)
        {
            const string commandText = "CMS_NewsSocialEmbed_GetById";
            try
            {
                NewsSocialEmbedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsSocialEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsSocialEmbedEntity GetNewsSocialEmbedByRawId(string rawId)
        {
            const string commandText = "CMS_NewsSocialEmbed_GetByRawId";
            try
            {
                NewsSocialEmbedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "RawId", rawId);
                data = _db.Get<NewsSocialEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsSocialEmbedEntity> SearchNewsSocialEmbed(int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsSocialEmbed_Search";
            try
            {
                List<NewsSocialEmbedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);               
                _db.AddParameter(cmd, "Status", status);                
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                
                data = _db.GetList<NewsSocialEmbedEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #endregion        

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsSocialEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion        
    }
}
