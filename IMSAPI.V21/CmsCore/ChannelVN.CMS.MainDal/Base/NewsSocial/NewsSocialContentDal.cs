﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsSocial
{
    public class NewsSocialContentDal : NewsSocialContentDalBase
    {
        internal NewsSocialContentDal(CmsMainDb db)
            : base(db)
        {
        }

    }
}
