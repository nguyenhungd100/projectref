﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsMobileStream
{
    public class NewsMobileStreamDal : NewsMobileStreamDalBase
    {
        internal NewsMobileStreamDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
