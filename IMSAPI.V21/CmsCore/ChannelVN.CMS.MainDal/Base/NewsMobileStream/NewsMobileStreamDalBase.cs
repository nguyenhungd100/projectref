﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsMobileStream
{
    public abstract class NewsMobileStreamDalBase
    {
        //public List<NewsMobileStreamEntity> GetListByTypeAndZoneId(int typeId, int zoneId, string listOfOrder = "")
        //{
        //    const string commandText = "CMS_NewsMobileStream_GetByTypeAndZoneId";
        //    try
        //    {
        //        List<NewsMobileStreamEntity> data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "TypeId", typeId);
        //        _db.AddParameter(cmd, "ZoneId", zoneId);
        //        _db.AddParameter(cmd, "ListOfOrder", listOfOrder);
        //        data = _db.GetList<NewsMobileStreamEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        //public NewsMobileStreamEntity GetNewsMobileStreamById(long id)
        //{
        //    const string commandText = "CMS_NewsMobileStream_GetById";
        //    try
        //    {
        //        NewsMobileStreamEntity data = null;
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        data = _db.Get<NewsMobileStreamEntity>(cmd);

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        public List<NewsMobileStreamEntity> GetList(int zoneId)
        {
            const string commandText = "CMS_NewsMobileStream_List";
            try
            {
                List<NewsMobileStreamEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsMobileStreamEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool SaveNewsMobileStream(int zoneId, long newsId, int status, bool isHighlight)
        {
            const string commandText = "CMS_NewsMobileStream_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "IsHighlight", isHighlight);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public bool NewsMobileStream_RemoveTopLessView(int topRemove, int numberOfNewsOnTimeLine, int newsPositionTypeIdOnMobileFocus)
        {
            const string commandText = "CMS_NewsMobileStream_RemoveTopLessView";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopRemove", topRemove);
                _db.AddParameter(cmd, "NumberOfNewsOnTimeLine", numberOfNewsOnTimeLine);
                _db.AddParameter(cmd, "NewsPositionTypeIdOnMobileFocus", newsPositionTypeIdOnMobileFocus);
                _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected NewsMobileStreamDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
