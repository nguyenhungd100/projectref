﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.VoteYesNo
{
    public class VoteYesNoDal : VoteYesNoDalBase
    {
        internal VoteYesNoDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
