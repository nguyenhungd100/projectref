﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.VoteInZone
{
    public class VoteInZoneDal : VoteInZoneDalBase
    {
        internal VoteInZoneDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
