﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Vote
{
    public abstract class VoteYesNoGroupDalBase
    {
        public List<VoteYesNoGroupEntity> GetAll()
        {
            const string commandText = "CMS_VoteYesNoGroup_GetAll";
            try
            {
                List<VoteYesNoGroupEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<VoteYesNoGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VoteYesNoGroupEntity GetById(int voteYesNoGroupId)
        {
            const string commandText = "CMS_VoteYesNoGroup_GetById";
            try
            {
                VoteYesNoGroupEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoGroupId);
                data = _db.Get<VoteYesNoGroupEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(string sortedListIds)
        {
            const string commandText = "CMS_VoteYesNoGroup_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SortedListIds", sortedListIds);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(VoteYesNoGroupEntity voteYesNoGroup, ref int voteYesNoId)
        {
            const string commandText = "CMS_VoteYesNoGroup_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoGroup.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "GroupName", voteYesNoGroup.GroupName);
                _db.AddParameter(cmd, "Priority", voteYesNoGroup.Priority);
                _db.AddParameter(cmd, "Status", voteYesNoGroup.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                voteYesNoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(VoteYesNoGroupEntity voteYesNoGroup)
        {
            const string commandText = "CMS_VoteYesNoGroup_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoGroup.Id);
                _db.AddParameter(cmd, "GroupName", voteYesNoGroup.GroupName);
                _db.AddParameter(cmd, "Priority", voteYesNoGroup.Priority);
                _db.AddParameter(cmd, "Status", voteYesNoGroup.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Delete(int voteYesNoGroupId)
        {
            const string commandText = "CMS_VoteYesNoGroup_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoGroupId);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VoteYesNoGroupDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
