﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Vote
{
    public abstract class VoteDalBase
    {
        #region Gets
        public VoteEntity GetById(int id)
        {
            const string commandText = "CMS_Vote_SelectById";
            try
            {
                VoteEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VoteEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<VoteEntity> GetList(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {

            const string commandText = "CMS_Vote_SelectList";
            try
            {
                List<VoteEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "IsGetTotal", isGetTotal);
                data = _db.GetList<VoteEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VoteEntity> InitESAllVote(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_Vote_InitESAllVote";
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM Vote V 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,V.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,V.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT V.*,	ROW_NUMBER() OVER(ORDER BY V.Id DESC) AS RowNumber 
                                        FROM Vote V
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,V.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,V.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<VoteEntity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<VoteEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<VoteAnswersEntity> GetListAnswersByVoteId(int voteId)
        {
            const string commandText = "CMS_VoteAnswers_SelectListByVoteId";
            try
            {
                List<VoteAnswersEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VoteId", voteId);
                data = _db.GetList<VoteAnswersEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VoteAnswersEntity GetAnswersDetail(int id)
        {
            const string commandText = "CMS_VoteAnswers_SelectById";
            try
            {
                VoteAnswersEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<VoteAnswersEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VoteEntity GetByNewsId(long newsid)
        {
            const string commandText = "CMS_Vote_SelectByNewsId";
            try
            {
                VoteEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsid);
                data = _db.Get<VoteEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Sets

        public bool Insert(VoteEntity vote, int zoneId, string zoneIdList, ref int voteId)
        {

            const string commandText = "CMS_Vote_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteId, ParameterDirection.Output);
                _db.AddParameter(cmd, "Title", vote.Title);
                _db.AddParameter(cmd, "Avatar", vote.Avatar);
                _db.AddParameter(cmd, "Sapo", vote.Sapo);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "Status", vote.Status);
                _db.AddParameter(cmd, "Author", vote.Author);
                _db.AddParameter(cmd, "CreatedBy", vote.CreatedBy);
                _db.AddParameter(cmd, "LastModifiedBy", vote.LastModifiedBy);
                _db.AddParameter(cmd, "Priority", vote.Priority);
                _db.AddParameter(cmd, "DisplayPosition", vote.DisplayPosition);
                _db.AddParameter(cmd, "DisplayStyle", vote.DisplayStyle);
                _db.AddParameter(cmd, "ViewCount", vote.ViewCount);
                _db.AddParameter(cmd, "RateCount", vote.RateCount);
                _db.AddParameter(cmd, "MaxAnswers", vote.MaxAnswers);
                _db.AddParameter(cmd, "ShowInZone", vote.ShowInZone);
                _db.AddParameter(cmd, "ShowInFooter", vote.ShowInFooter);
                _db.AddParameter(cmd, "StartedDate", vote.StartedDate != DateTime.MinValue ? vote.StartedDate : DateTime.Now);
                _db.AddParameter(cmd, "EndedDate", vote.EndedDate != DateTime.MinValue ? vote.EndedDate : DateTime.Now);

                var numberOfRow = cmd.ExecuteNonQuery();

                voteId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Update(VoteEntity vote, int zoneId, string zoneIdList)
        {
            const string commandText = "CMS_Vote_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", vote.Id);
                _db.AddParameter(cmd, "Title", vote.Title);
                _db.AddParameter(cmd, "Avatar", vote.Avatar);
                _db.AddParameter(cmd, "Sapo", vote.Sapo);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "Status", vote.Status);
                _db.AddParameter(cmd, "Author", vote.Author);
                _db.AddParameter(cmd, "LastModifiedBy", vote.LastModifiedBy);
                _db.AddParameter(cmd, "Priority", vote.Priority);
                _db.AddParameter(cmd, "DisplayPosition", vote.DisplayPosition);
                _db.AddParameter(cmd, "DisplayStyle", vote.DisplayStyle);
                _db.AddParameter(cmd, "ViewCount", vote.ViewCount);
                _db.AddParameter(cmd, "RateCount", vote.RateCount);
                _db.AddParameter(cmd, "MaxAnswers", vote.MaxAnswers);
                _db.AddParameter(cmd, "ShowInZone", vote.ShowInZone);
                _db.AddParameter(cmd, "ShowInFooter", vote.ShowInFooter);
                _db.AddParameter(cmd, "StartedDate", vote.StartedDate != DateTime.MinValue ? vote.StartedDate : DateTime.Now);
                _db.AddParameter(cmd, "EndedDate", vote.EndedDate != DateTime.MinValue ? vote.EndedDate : DateTime.Now);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool InsertAnswers(VoteAnswersEntity voteAnswers, ref int voteAnswersId)
        {

            const string commandText = "CMS_VoteAnswers_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", voteAnswersId, ParameterDirection.Output);
                _db.AddParameter(cmd, "VoteId", voteAnswers.VoteId);
                _db.AddParameter(cmd, "Value", voteAnswers.Value);
                _db.AddParameter(cmd, "VoteRate", voteAnswers.VoteRate);
                _db.AddParameter(cmd, "Status", voteAnswers.Status);
                var numberOfRow = cmd.ExecuteNonQuery();

                voteAnswersId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAnswers(VoteAnswersEntity voteAnswers)
        {
            const string commandText = "CMS_VoteAnswers_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteAnswers.Id);
                _db.AddParameter(cmd, "VoteId", voteAnswers.VoteId);
                _db.AddParameter(cmd, "Value", voteAnswers.Value);
                _db.AddParameter(cmd, "VoteRate", voteAnswers.VoteRate);
                _db.AddParameter(cmd, "Status", voteAnswers.Status);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriorityVoteAnswers(string listVoteAnswersId)
        {
            const string commandText = "CMS_VoteAnswers_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listVoteAnswersId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public bool DeleteAnswers(int id)
        {
            const string commandText = "CMS_VoteAnswers_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_Vote_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = _db.ExecuteNonQuery(cmd);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertVoteInNews(int voteId, long newsId)
        {

            const string commandText = "CMS_VoteInNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VoteId", voteId);
                _db.AddParameter(cmd, "NewsId", newsId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected VoteDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
