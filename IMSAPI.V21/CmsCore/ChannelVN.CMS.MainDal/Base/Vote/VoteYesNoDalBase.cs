﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.Vote;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.VoteYesNo
{
    public abstract class VoteYesNoDalBase
    {
        public List<VoteYesNoEntity> Search(int voteYesNoGroupId, string keyword, int isFocus, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_VoteYesNo_Search";
            try
            {
                List<VoteYesNoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "VoteYesNoGroupId", voteYesNoGroupId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsFocus", isFocus);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<VoteYesNoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public VoteYesNoEntity GetById(int voteYesNoId)
        {
            const string commandText = "CMS_VoteYesNo_GetById";
            try
            {
                VoteYesNoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoId);
                data = _db.Get<VoteYesNoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePriority(string sortedListIds)
        {
            const string commandText = "CMS_VoteYesNo_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SortedListIds", sortedListIds);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Insert(VoteYesNoEntity voteYesNo, ref int NewsId)
        {
            const string commandText = "CMS_VoteYesNo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", NewsId, ParameterDirection.Output);
                _db.AddParameter(cmd, "VoteYesNoGroupId", voteYesNo.VoteYesNoGroupId);
                _db.AddParameter(cmd, "Title", voteYesNo.Title);
                _db.AddParameter(cmd, "Avatar", voteYesNo.Avatar);
                _db.AddParameter(cmd, "YesAnswer", voteYesNo.YesAnswer);
                _db.AddParameter(cmd, "NoAnswer", voteYesNo.NoAnswer);
                _db.AddParameter(cmd, "Source", voteYesNo.Source);
                _db.AddParameter(cmd, "Author", voteYesNo.Author);
                _db.AddParameter(cmd, "CreatedBy", voteYesNo.CreatedBy);
                _db.AddParameter(cmd, "Priority", voteYesNo.Priority);
                _db.AddParameter(cmd, "IsFocus", voteYesNo.IsFocus);
                _db.AddParameter(cmd, "Status", voteYesNo.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                NewsId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Update(VoteYesNoEntity voteYesNo)
        {
            const string commandText = "CMS_VoteYesNo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNo.Id);
                _db.AddParameter(cmd, "Title", voteYesNo.Title);
                _db.AddParameter(cmd, "Avatar", voteYesNo.Avatar);
                _db.AddParameter(cmd, "YesAnswer", voteYesNo.YesAnswer);
                _db.AddParameter(cmd, "NoAnswer", voteYesNo.NoAnswer);
                _db.AddParameter(cmd, "Source", voteYesNo.Source);
                _db.AddParameter(cmd, "Author", voteYesNo.Author);
                _db.AddParameter(cmd, "LastModifiedBy", voteYesNo.LastModifiedBy);
                _db.AddParameter(cmd, "Priority", voteYesNo.Priority);
                _db.AddParameter(cmd, "IsFocus", voteYesNo.IsFocus);
                _db.AddParameter(cmd, "Status", voteYesNo.Status);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int voteYesNoId)
        {

            const string commandText = "CMS_VoteYesNo_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", voteYesNoId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        #region Core members

        private readonly CmsMainDb _db;

        protected VoteYesNoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
