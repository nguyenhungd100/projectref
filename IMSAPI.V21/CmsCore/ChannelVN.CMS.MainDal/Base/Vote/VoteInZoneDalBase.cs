﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.MainDal.Common;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using System.Data;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.VoteInZone
{
    public abstract class VoteInZoneDalBase
    {
        public List<VoteInZoneEntity> GetVoteInZoneByVoteId(long voteId)
        {
            const string commandText = "CMS_VoteInZone_GetByVoteId";
            try
            {
                List<VoteInZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "VoteID", voteId);
                data = _db.GetList<VoteInZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #region Core members

        private readonly CmsMainDb _db;

        protected VoteInZoneDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
