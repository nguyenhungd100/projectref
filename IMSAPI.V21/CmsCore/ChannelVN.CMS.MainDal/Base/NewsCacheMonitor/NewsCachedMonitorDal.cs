﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsCacheMonitor
{
    public class NewsCachedMonitorDal : NewsCachedMonitorDalBase
    {
        internal NewsCachedMonitorDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
