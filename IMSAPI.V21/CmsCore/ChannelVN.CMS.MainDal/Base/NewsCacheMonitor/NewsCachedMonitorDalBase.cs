﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.NewsCacheMonitor;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsCacheMonitor
{
    public abstract class NewsCachedMonitorDalBase
    {
        public SqlDataReader GetChangedRecordForNews()
        {
            const string commandText = "CMS_NewsCachedMonitor_GetUnprocessNewsItem";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.ExecuteReader(cmd);

                return (SqlDataReader)data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public SqlDataReader GetChangedRecordForTag()
        {
            const string commandText = "CMS_NewsCachedMonitor_GetUnprocessTagItem";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                var data = _db.ExecuteReader(cmd);

                return (SqlDataReader)data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateProcessItemStateForNews(string listOfProcessIds)
        {
            const string commandText = "CMS_NewsCachedMonitor_UpdateProcessItemStateForNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfProcessId", listOfProcessIds);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateProcessItemStateForTag(string listOfProcessIds)
        {
            const string commandText = "CMS_NewsCachedMonitor_UpdateProcessItemStateForTag";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfProcessId", listOfProcessIds);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsCachedMonitorDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
