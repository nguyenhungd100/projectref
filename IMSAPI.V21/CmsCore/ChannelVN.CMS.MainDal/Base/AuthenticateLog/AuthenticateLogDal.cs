﻿using ChannelVN.CMS.MainDal.Base.AuthenticateLog;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.AuthenticateLog
{
    public class AuthenticateLogDal : AuthenticateLogDalBase
    {
        internal AuthenticateLogDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}