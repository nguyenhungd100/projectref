﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.MainDal.Base.AuthenticateLog
{
    public abstract class AuthenticateLogDalBase
    {
        public bool Insert(AuthenticateLogEntity user, ref long newLogId)
        {
            const string commandText = "CMS_AuthenticateLog_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MachineName", user.MachineName);
                _db.AddParameter(cmd, "IpAddress", user.IpAddress);
                _db.AddParameter(cmd, "UserAgent", user.UserAgent);
                _db.AddParameter(cmd, "UrlReferrer", user.UrlReferrer);
                if (user.LogDate == DateTime.MinValue)
                    _db.AddParameter(cmd, "LogDate", DBNull.Value);
                else _db.AddParameter(cmd, "LogDate", user.LogDate);
                _db.AddParameter(cmd, "UserName", user.UserName);
                _db.AddParameter(cmd, "Os", user.Os);
                _db.AddParameter(cmd, "Description", user.Description);
                _db.AddParameter(cmd, "Status", user.Status);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                var data = _db.ExecuteNonQuery(cmd);
                //newLogId = Utility.ConvertToLong(cmd.Parameters[cmd.Parameters.Count - 1]);
                newLogId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected AuthenticateLogDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
