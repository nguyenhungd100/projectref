﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public class AlbumByAuthorDalBase
    {
        public bool SaveAlbumByAuthor(AlbumByAuthorEntity albumByAuthor)
        {
            const string commandText = "PhotoCms_AlbumByAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumByAuthor.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "AlbumId", albumByAuthor.AlbumId);
                _db.AddParameter(cmd, "AuthorId", albumByAuthor.AuthorId);

                var numberOfRow = cmd.ExecuteNonQuery();
                albumByAuthor.Id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsPhotoDb _db;

        protected AlbumByAuthorDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
