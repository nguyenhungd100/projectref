﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System.Text;
using System.Linq;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class AlbumDalBase
    {
        #region Update

        public bool Insert(AlbumEntity album, string tagIdList, string eventIdList, ref int newAlbumId)
        {
            string commandText = "PhotoCms_Album_Insert";
            try
            {              
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    commandText = "PhotoCms_Album_InsertV2";
                }               
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", album.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", album.ZoneId);
                _db.AddParameter(cmd, "Name", album.Name);
                _db.AddParameter(cmd, "ThumbImage", album.ThumbImage);
                _db.AddParameter(cmd, "CreatedBy", album.CreatedBy);                
                _db.AddParameter(cmd, "Status", album.Status);
                _db.AddParameter(cmd, "Tags", album.Tags);
                _db.AddParameter(cmd, "Description", album.Description);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "EventIdList", eventIdList);
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    _db.AddParameter(cmd, "Author", album.Author);
                }
                var numberOfRow = cmd.ExecuteNonQuery();

                newAlbumId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(AlbumEntity album, string tagIdList, string eventIdList)
        {
            string commandText = "PhotoCms_Album_Update";
            try
            {
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    commandText = "PhotoCms_Album_UpdateV2";
                }
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", album.Id);
                _db.AddParameter(cmd, "ZoneId", album.ZoneId);
                _db.AddParameter(cmd, "Name", album.Name);
                _db.AddParameter(cmd, "ThumbImage", album.ThumbImage);
                _db.AddParameter(cmd, "ModifiedBy", album.ModifiedBy);
                _db.AddParameter(cmd, "Status", album.Status);
                _db.AddParameter(cmd, "Tags", album.Tags);
                _db.AddParameter(cmd, "Description", album.Description);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "EventIdList", eventIdList);

                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    _db.AddParameter(cmd, "Author", album.Author);
                }
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(long albumId)
        {
            const string commandText = "PhotoCms_Album_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Send(int albumId, string userDoAction)
        {
            const string commandText = "PhotoCms_Album_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Publish(int albumId, string userDoAction)
        {
            const string commandText = "PhotoCms_Album_Publish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UnPublish(int albumId, string userDoAction)
        {
            const string commandText = "PhotoCms_Album_UnPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Return(int albumId, string userDoAction)
        {
            const string commandText = "PhotoCms_Album_Return";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool AddPhotoInAlbum(string addPhotoIds, string deletePhotoIds, int albumId)
        {
            const string commandText = "PhotoCms_Album_AddPhotoInAlbum";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AddPhotoIdList", addPhotoIds);
                _db.AddParameter(cmd, "DeletePhotoIdList", deletePhotoIds);
                _db.AddParameter(cmd, "AlbumId", albumId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateThumbImageByPhotoId(int albumId, long photoId)
        {

            const string commandText = "PhotoCms_Album_UpdateThumbImageByPhotoId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "PhotoId", photoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public AlbumPublishedEntity Publish(int albumId, long newsId, int zoneId, string distributedBy)
        {
            const string commandText = "PhotoCms_Album_Publish";
            try
            {
                AlbumPublishedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "DistributedBy", distributedBy);
                data = _db.Get<AlbumPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public AlbumEntity GetById(long albumId)
        {
            const string commandText = "PhotoCms_Album_GetById";
            try
            {
                AlbumEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumId);
                data = _db.Get<AlbumEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AlbumEntity> Search(string keyword, int zoneId,int eventId,int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_Album_Search";
            try
            {
                List<AlbumEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "EventId", eventId);
                _db.AddParameter(cmd, "Status", status);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<AlbumEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CountAlbumStatus> CountAlbum(string username, List<string> listStatus, string userAction)
        {
            var commandText= new StringBuilder();
            try
            {
                List<CountAlbumStatus> data = null;
                if (listStatus != null && listStatus.Count > 0)
                {
                    foreach(var item in listStatus)
                    {
                        if (item == "0" || item == "4")
                        {
                            commandText.AppendFormat("select Status, Count(*) as [Count] from Album where Status={0} and CreatedBy='{1}' group by Status ", item, username);
                        }
                        else {
                            if(string.IsNullOrWhiteSpace(userAction))
                                commandText.AppendFormat("select Status, Count(*) as [Count] from Album where Status={0} group by Status ", item);
                            else
                                commandText.AppendFormat("select Status, Count(*) as [Count] from Album where Status={0} and CreatedBy='{1}' group by Status ", item, userAction);
                        }

                        if (listStatus.LastOrDefault() != item)
                        {
                            commandText.Append(" Union ");
                        }
                    }

                    var cmd = _db.CreateCommand(commandText.ToString(), false);

                    data = _db.GetList<CountAlbumStatus>(cmd);
                }         
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<AlbumEntity> InitRedisAllPhotoAlbum(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = "PhotoCms_Album_Search";
            try
            {
                List<AlbumEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", string.Empty);
                _db.AddParameter(cmd, "ZoneId", -1);
                _db.AddParameter(cmd, "EventId", -1);
                _db.AddParameter(cmd, "Status", -1);

                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", string.Empty);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<AlbumEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected AlbumDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
