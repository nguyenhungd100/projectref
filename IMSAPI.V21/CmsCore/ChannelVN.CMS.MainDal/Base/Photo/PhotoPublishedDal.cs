﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public class PhotoPublishedDal : PhotoPublishedDalBase
    {
        internal PhotoPublishedDal(CmsPhotoDb db)
            : base(db)
        {
        }

    }
}
