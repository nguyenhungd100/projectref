﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class PhotoFolderDalBase
    {
        #region Update

        public bool Insert(PhotoFolderEntity photoFolder, ref int newPhotoFolderId)
        {

            const string commandText = "PhotoCms_PhotoFolder_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoFolder.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", photoFolder.ParentId);
                _db.AddParameter(cmd, "Name", photoFolder.Name);
                _db.AddParameter(cmd, "CreatedBy", photoFolder.CreatedBy);
                _db.AddParameter(cmd, "Status", photoFolder.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                newPhotoFolderId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(PhotoFolderEntity photoFolder)
        {
            const string commandText = "PhotoCms_PhotoFolder_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", photoFolder.ParentId);
                _db.AddParameter(cmd, "Name", photoFolder.Name);
                _db.AddParameter(cmd, "Status", photoFolder.Status);
                _db.AddParameter(cmd, "Id", photoFolder.Id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int photoFolderId)
        {
            const string commandText = "PhotoCms_PhotoFolder_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoFolderId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public PhotoFolderEntity GetById(int photoFolderId)
        {
            const string commandText = "PhotoCms_PhotoFolder_GetById";
            try
            {
                PhotoFolderEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoFolderId);
                data = _db.Get<PhotoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoFolderEntity> GetByParentId(int parentId, string createdBy)
        {
            const string commandText = "PhotoCms_PhotoFolder_GetByParentId";
            try
            {
                List<PhotoFolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                data = _db.GetList<PhotoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        public List<PhotoFolderEntity> Search(int parentId, string name, string createdBy, int status)
        {
            const string commandText = "PhotoCms_PhotoFolder_Search";
            try
            {
                List<PhotoFolderEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Name", name);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<PhotoFolderEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoFolderDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
