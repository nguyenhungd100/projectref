﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class AlbumPublishedDalBase
    {
        #region Update

        public bool Update(AlbumPublishedEntity albumPublished)
        {

            const string commandText = "PhotoCms_AlbumPublished_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumPublished.Id);
                _db.AddParameter(cmd, "ZoneId", albumPublished.ZoneId);
                _db.AddParameter(cmd, "Name", albumPublished.Name);
                _db.AddParameter(cmd, "ThumbImage", albumPublished.ThumbImage);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAlbumInUsed(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        {
            const string commandText = "PhotoCms_AlbumPublished_UpdateAlbumInUsed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListAlbumPublishedIdInUsed", listAlbumPublishedIdInUsed);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public static bool UpdateAlbumInUsedWhenInsertNews(string listAlbumPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    
        //    try
        //    {
        //        var sqlParameters = new[]
        //                                {
        //                                    new SqlParameter("@ListAlbumPublishedIdInUsed", listAlbumPublishedIdInUsed),
        //                                    new SqlParameter("@NewsId", newsId),
        //                                    new SqlParameter("@CreatedBy", createdBy)
        //                                };
        //        var numberRow = SqlHelper.ExecuteNonQuery(Constants.GetConnectionString(Constants.Connection.CmsPhotoDb),
        //                                                  CommandType.StoredProcedure,
        //                                                  Constants.DatabaseSchema + "PhotoCms_AlbumPublished_UpdateAlbumInUsedWhenInsert",
        //                                                  sqlParameters);
        //        return numberRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(Constants.DatabaseSchema + "PhotoCms_AlbumPublished_UpdateAlbumInUsedWhenInsert:{0}", ex.Message));
        //    }
        //}

        #endregion

        #region Get

        public  AlbumPublishedEntity GetById(long albumPublishedId)
        {
            const string commandText = "PhotoCms_AlbumPublished_GetById";
            try
            {
                AlbumPublishedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", albumPublishedId);
                data = _db.Get<AlbumPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
           
        }

        public List<AlbumPublishedEntity> GetByAlbumId(long photoId)
        {
            const string commandText = "PhotoCms_AlbumPublished_GetByAlbumId";
            try
            {
                List<AlbumPublishedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoId", photoId);
                data = _db.GetList<AlbumPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected AlbumPublishedDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
