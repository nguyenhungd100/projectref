﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System.Text;
using System.Linq;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public class PhotoDalBase
    {
        #region Update

        public bool Insert(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {

            const string commandText = "PhotoCms_Photo_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photo.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "PhotoLabelId", photo.PhotoLabelId);
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Name", photo.Name);
                _db.AddParameter(cmd, "ImageUrl", photo.ImageUrl);
                _db.AddParameter(cmd, "ImageNote", photo.ImageNote);
                _db.AddParameter(cmd, "Size", photo.Size);
                _db.AddParameter(cmd, "Capacity", photo.Capacity);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "CreatedBy", photo.CreatedBy);
                _db.AddParameter(cmd, "OriginalName", photo.OriginalName);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Tags", photo.Tags);

                var numberOfRow = cmd.ExecuteNonQuery();

                newPhotoId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool InsertV2(PhotoEntity photo, string tagIdList, ref long newPhotoId)
        {

            const string commandText = "PhotoCms_Photo_InsertV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photo.Id, ParameterDirection.Output);                
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Name", photo.Name);
                _db.AddParameter(cmd, "ImageUrl", photo.ImageUrl);
                _db.AddParameter(cmd, "ImageNote", photo.ImageNote);
                _db.AddParameter(cmd, "Size", photo.Size);
                _db.AddParameter(cmd, "Capacity", photo.Capacity);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "CreatedBy", photo.CreatedBy);
                _db.AddParameter(cmd, "OriginalName", photo.OriginalName);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);                
                _db.AddParameter(cmd, "Tags", photo.Tags);

                _db.AddParameter(cmd, "AlbumId", photo.AlbumId);
                _db.AddParameter(cmd, "Author", photo.Author);
                _db.AddParameter(cmd, "ImagePreviewUrl", photo.ImagePreviewUrl);
                _db.AddParameter(cmd, "PublishedDate", photo.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", photo.PublishedBy);
                _db.AddParameter(cmd, "ImageDistribution", photo.ImageDistribution);
                _db.AddParameter(cmd, "Width", photo.Width);
                _db.AddParameter(cmd, "Height", photo.Height);
                _db.AddParameter(cmd, "ImageDimension", photo.ImageDimension);
                _db.AddParameter(cmd, "ImageExif", photo.ImageExif);
                _db.AddParameter(cmd, "RestrictionContent", photo.RestrictionContent);
                _db.AddParameter(cmd, "Location", photo.Location);

                _db.AddParameter(cmd, "TagIdList", tagIdList);

                _db.AddParameter(cmd, "PhotoFolderId", photo.PhotoFolderId);
                _db.AddParameter(cmd, "PhotoLabelId", photo.PhotoLabelId);

                var numberOfRow = cmd.ExecuteNonQuery();

                newPhotoId = Utility.ConvertToLong(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(PhotoEntity photo, string tagIdList)
        {

            const string commandText = "PhotoCms_Photo_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoLabelId", photo.PhotoLabelId);
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Name", photo.Name);
                _db.AddParameter(cmd, "ImageNote", photo.ImageNote);
                _db.AddParameter(cmd, "Size", photo.Size);
                _db.AddParameter(cmd, "Capacity", photo.Capacity);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "ModifiedBy", photo.ModifiedBy);
                _db.AddParameter(cmd, "OriginalName", photo.OriginalName);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Tags", photo.Tags);
                _db.AddParameter(cmd, "Id", photo.Id);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool UpdateV2(PhotoEntity photo, string tagIdList)
        {

            const string commandText = "PhotoCms_Photo_UpdateV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);                
                _db.AddParameter(cmd, "Id", photo.Id);
                _db.AddParameter(cmd, "ZoneId", photo.ZoneId);
                _db.AddParameter(cmd, "Name", photo.Name);
                _db.AddParameter(cmd, "ImageUrl", photo.ImageUrl);
                _db.AddParameter(cmd, "ImageNote", photo.ImageNote);
                _db.AddParameter(cmd, "Size", photo.Size);
                _db.AddParameter(cmd, "Capacity", photo.Capacity);
                _db.AddParameter(cmd, "Note", photo.Note);
                _db.AddParameter(cmd, "Status", photo.Status);
                _db.AddParameter(cmd, "ModifiedBy", photo.ModifiedBy);
                _db.AddParameter(cmd, "OriginalName", photo.OriginalName);
                _db.AddParameter(cmd, "UnSignName", photo.UnSignName);
                _db.AddParameter(cmd, "Tags", photo.Tags);

                _db.AddParameter(cmd, "AlbumId", photo.AlbumId);
                _db.AddParameter(cmd, "Author", photo.Author);
                _db.AddParameter(cmd, "ImagePreviewUrl", photo.ImagePreviewUrl);
                //_db.AddParameter(cmd, "PublishedDate", photo.PublishedDate);
                //_db.AddParameter(cmd, "PublishedBy", photo.PublishedBy);
                _db.AddParameter(cmd, "ImageDistribution", photo.ImageDistribution);
                _db.AddParameter(cmd, "Width", photo.Width);
                _db.AddParameter(cmd, "Height", photo.Height);
                _db.AddParameter(cmd, "ImageDimension", photo.ImageDimension);
                _db.AddParameter(cmd, "ImageExif", photo.ImageExif);
                _db.AddParameter(cmd, "RestrictionContent", photo.RestrictionContent);
                _db.AddParameter(cmd, "Location", photo.Location);

                _db.AddParameter(cmd, "TagIdList", tagIdList);

                _db.AddParameter(cmd, "PhotoFolderId", photo.PhotoFolderId);
                _db.AddParameter(cmd, "PhotoLabelId", photo.PhotoLabelId);


                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }       

        public bool Delete(long photoId)
        {
            const string commandText = "PhotoCms_Photo_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeletePhotoInAlbum(string listPhotoId)
        {
            const string commandText = "PhotoCms_Photo_DeletePhotoInAlbum";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListPhotoId", listPhotoId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public PhotoPublishedEntity Publish(long photoId, long newsId, int zoneId, int albumId, string distributedBy)
        {
            const string commandText = "PhotoCms_Photo_Publish";
            try
            {
                PhotoPublishedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoId", photoId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "DistributedBy", distributedBy);
                data = _db.Get<PhotoPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Send(long photoId, string userDoAction)
        {
            const string commandText = "PhotoCms_Photo_Send";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool PublishV2(long photoId, string userDoAction)
        {
            const string commandText = "PhotoCms_Photo_PublishV2";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UnPublish(long photoId, string userDoAction)
        {
            const string commandText = "PhotoCms_Photo_UnPublish";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Return(long photoId, string userDoAction)
        {
            const string commandText = "PhotoCms_Photo_Return";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);
                _db.AddParameter(cmd, "UserDoAction", userDoAction);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        
        #endregion

        #region Get

        public PhotoEntity GetById(long photoId)
        {
            const string commandText = "PhotoCms_Photo_GetById";
            try
            {
                PhotoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);

                data = _db.Get<PhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PhotoEntity GetByIdV2(long photoId)
        {
            const string commandText = "PhotoCms_Photo_GetByIdV2";
            try
            {
                PhotoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoId);

                data = _db.Get<PhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public PhotoEntity GetByImageUrl(string imageUrl)
        {
            const string commandText = "PhotoCms_Photo_GetByImageUrl";
            try
            {
                PhotoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ImageUrl", imageUrl);

                data = _db.Get<PhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoEntity> Search(string keyword, int photoLabelId, int zoneId, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_Photo_Search";
            try
            {
                List<PhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "PhotoLabelId", photoLabelId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoEntity> SearchV2(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_Photo_SearchV2";
            try
            {
                List<PhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_Photo_ListPhotoByAlbumId";
            try
            {
                List<PhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);                
                _db.AddParameter(cmd, "AlbumId", albumId);               
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<CountAlbumStatus> CountPhoto(string username, List<string> listStatus, string userAction)
        {
            var commandText = new StringBuilder();
            try
            {
                List<CountAlbumStatus> data = null;
                if (listStatus != null && listStatus.Count > 0)
                {
                    foreach (var item in listStatus)
                    {
                        if (item == "0" || item == "4")
                        {
                            commandText.AppendFormat("select Status, Count(*) as [Count] from Photo where Status={0} and CreatedBy='{1}' group by Status ", item, username);
                        }
                        else {
                            if (string.IsNullOrWhiteSpace(userAction))
                                commandText.AppendFormat("select Status, Count(*) as [Count] from Photo where Status={0} group by Status ", item);
                            else
                                commandText.AppendFormat("select Status, Count(*) as [Count] from Photo where Status={0} and CreatedBy='{1}' group by Status ", item, username);
                        }

                        if (listStatus.LastOrDefault() != item)
                        {
                            commandText.Append(" Union ");
                        }
                    }

                    var cmd = _db.CreateCommand(commandText.ToString(), false);

                    data = _db.GetList<CountAlbumStatus>(cmd);
                }
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoEntity> InitRedisAllPhoto(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            const string commandText = "PhotoCms_Photo_SearchV2";
            try
            {
                List<PhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", string.Empty);
                _db.AddParameter(cmd, "AlbumId", 0);
                _db.AddParameter(cmd, "Status", -1);
                _db.AddParameter(cmd, "ZoneId", 0);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateFrom", fromDate);

                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "CreatedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "CreatedDateTo", toDate);

                _db.AddParameter(cmd, "CreatedBy", string.Empty);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
