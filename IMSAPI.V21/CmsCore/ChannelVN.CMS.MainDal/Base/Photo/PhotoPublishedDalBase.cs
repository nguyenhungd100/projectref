﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class PhotoPublishedDalBase
    {
        #region Update

        public bool Update(PhotoPublishedEntity photoPublished)
        {
            const string commandText = "PhotoCms_PhotoPublished_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoPublished.Id);
                _db.AddParameter(cmd, "ZoneId", photoPublished.ZoneId);
                _db.AddParameter(cmd, "Name", photoPublished.Name);
                _db.AddParameter(cmd, "ImageNote", photoPublished.ImageNote);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateAlbumId(PhotoPublishedUpdateEntity photoPublished)
        {
            const string commandText = "CMS_PhotoPublished_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoPublished.Id);
                _db.AddParameter(cmd, "ZoneId", photoPublished.ZoneId);
                _db.AddParameter(cmd, "AlbumId", photoPublished.AlbumId);
                _db.AddParameter(cmd, "Priority", photoPublished.Priority);
                _db.AddParameter(cmd, "Name", photoPublished.Name);
                _db.AddParameter(cmd, "ImageNote", photoPublished.ImageNote);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdatePhotoInUsed(string listPhotoPublishedIdInUsed, long newsId, int albumId, string createdBy)
        {
            const string commandText = "PhotoCms_PhotoPublished_UpdatePhotoInUsed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListPhotoPublishedIdInUsed", listPhotoPublishedIdInUsed);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public static bool UpdatePhotoInUsedWhenInsertNews(string listPhotoPublishedIdInUsed, long newsId, string createdBy)
        //{
        //    
        //    try
        //    {
        //        var sqlParameters = new[]
        //                                {
        //                                    new SqlParameter("@ListPhotoPublishedIdInUsed", listPhotoPublishedIdInUsed),
        //                                    new SqlParameter("@NewsId", newsId),
        //                                    new SqlParameter("@CreatedBy", createdBy)
        //                                };
        //        var numberRow = SqlHelper.ExecuteNonQuery(Constants.GetConnectionString(Constants.Connection.CmsPhotoDb),
        //                                                  CommandType.StoredProcedure,
        //                                                  Constants.DatabaseSchema + "PhotoCms_PhotoPublished_UpdatePhotoInUsedWhenInsert",
        //                                                  sqlParameters);
        //        return numberRow > 0;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format(Constants.DatabaseSchema + "PhotoCms_PhotoPublished_UpdatePhotoInUsedWhenInsert:{0}", ex.Message));
        //    }
        //}

        public bool UpdatePriority(string listPhotoPublishedId)
        {
            const string commandText = "PhotoCms_PhotoPublished_UpdatePriority";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListPhotoPublishedId", listPhotoPublishedId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(string listPhotoPublishedId)
        {
            const string commandText = "PhotoCms_PhotoPublished_DeleteList";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListPhotoPublishedId", listPhotoPublishedId);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public PhotoPublishedEntity GetById(long photoPublishedId)
        {
            const string commandText = "PhotoCms_PhotoPublished_GetById";
            try
            {
                PhotoPublishedEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoPublishedId);
                data = _db.Get<PhotoPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<PhotoPublishedEntity> GetByPhotoId(long photoId)
        {
            const string commandText = "PhotoCms_PhotoPublished_GetByPhotoId";
            try
            {
                List<PhotoPublishedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoId", photoId);
                data = _db.GetList<PhotoPublishedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoPublishedEntity> SearchInAlbum(string keyword, int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_PhotoPublished_SearchInAlbum";
            try
            {
                List<PhotoPublishedEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "AlbumId", albumId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoPublishedEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoPublishedDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
