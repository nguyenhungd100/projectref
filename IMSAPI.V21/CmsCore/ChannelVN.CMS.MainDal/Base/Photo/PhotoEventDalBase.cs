﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Photo;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class PhotoEventDalBase
    {
        #region action
        public bool Insert(PhotoEventEntity zonePhoto, ref int zonePhotoId)
        {

            const string commandText = "PhotoCms_PhotoEvent_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhoto.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", zonePhoto.Name);
                _db.AddParameter(cmd, "Description", zonePhoto.Description);
                _db.AddParameter(cmd, "Avatar", zonePhoto.Avatar);
                _db.AddParameter(cmd, "Url", zonePhoto.Url);
                _db.AddParameter(cmd, "BeginDate", zonePhoto.BeginDate);
                _db.AddParameter(cmd, "EndDate", zonePhoto.EndDate);                
                _db.AddParameter(cmd, "Status", zonePhoto.Status);                
                _db.AddParameter(cmd, "IsFocus", zonePhoto.IsFocus);                               
                _db.AddParameter(cmd, "ZoneId", zonePhoto.ZoneId);                
                _db.AddParameter(cmd, "PublishedDate", zonePhoto.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", zonePhoto.PublishedBy);
                _db.AddParameter(cmd, "CreatedBy", zonePhoto.CreatedBy);
                _db.AddParameter(cmd, "Location", zonePhoto.Location);

                var numberOfRow = cmd.ExecuteNonQuery();

                zonePhotoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        public bool Update(PhotoEventEntity zonePhoto)
        {

            const string commandText = "PhotoCms_PhotoEvent_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhoto.Id);
                _db.AddParameter(cmd, "Name", zonePhoto.Name);
                _db.AddParameter(cmd, "Description", zonePhoto.Description);
                _db.AddParameter(cmd, "Avatar", zonePhoto.Avatar);
                _db.AddParameter(cmd, "Url", zonePhoto.Url);
                _db.AddParameter(cmd, "BeginDate", zonePhoto.BeginDate);
                _db.AddParameter(cmd, "EndDate", zonePhoto.EndDate);
                _db.AddParameter(cmd, "Status", zonePhoto.Status);
                _db.AddParameter(cmd, "IsFocus", zonePhoto.IsFocus);
                _db.AddParameter(cmd, "ZoneId", zonePhoto.ZoneId);
                _db.AddParameter(cmd, "PublishedDate", zonePhoto.PublishedDate);
                _db.AddParameter(cmd, "PublishedBy", zonePhoto.PublishedBy);
                _db.AddParameter(cmd, "ModifiedBy", zonePhoto.ModifiedBy);
                _db.AddParameter(cmd, "Location", zonePhoto.Location);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }           
        public bool Delete(int zonePhotoId)
        {
            const string commandText = "PhotoCms_PhotoEvent_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhotoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Gets
        public List<PhotoEventEntity> SearchPhotoEvent(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "PhotoCms_PhotoEvent_Search";
            try
            {
                List<PhotoEventEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<PhotoEventEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PhotoEventEntity GetById(int id)
        {
            const string commandText = "PhotoCms_PhotoEvent_GetById";
            try
            {
                PhotoEventEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<PhotoEventEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<PhotoEventSimpleEntity> GetByAlbumId(int id)
        {
            const string commandText = "PhotoCms_PhotoEvent_GetByAlbumId";
            try
            {
                List<PhotoEventSimpleEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AlbumId", id);
                data = _db.GetList<PhotoEventSimpleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<PhotoEventEntity> GetPhotoEventActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "PhotoCms_PhotoEvent_GetActivedByUsernameAndPermissionIds";
            try
            {
                List<PhotoEventEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<PhotoEventEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
           
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected PhotoEventDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
