﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Photo;

namespace ChannelVN.CMS.MainDal.Base.Zone
{
    public abstract class ZonePhotoDalBase
    {
        #region action
        public bool Insert(ZonePhotoEntity zonePhoto, ref int zonePhotoId)
        {

            const string commandText = "PhotoCms_ZonePhoto_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhoto.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", zonePhoto.Name);
                _db.AddParameter(cmd, "Url", zonePhoto.Url);
                _db.AddParameter(cmd, "Order", zonePhoto.Order);
                _db.AddParameter(cmd, "ParentId", zonePhoto.ParentId);                
                _db.AddParameter(cmd, "Status", zonePhoto.Status);                
                _db.AddParameter(cmd, "ShowOnHome", zonePhoto.ShowOnHome);
                _db.AddParameter(cmd, "Avatar", zonePhoto.Avatar);
                _db.AddParameter(cmd, "Logo", zonePhoto.Logo);
                _db.AddParameter(cmd, "Description", zonePhoto.Description);
                _db.AddParameter(cmd, "CatId", zonePhoto.CatId);
                _db.AddParameter(cmd, "ZoneRelation", zonePhoto.ZoneRelation);

                var numberOfRow = cmd.ExecuteNonQuery();

                zonePhotoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }        
        public bool Update(ZonePhotoEntity zonePhoto)
        {

            const string commandText = "PhotoCms_ZonePhoto_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhoto.Id);
                _db.AddParameter(cmd, "Name", zonePhoto.Name);
                _db.AddParameter(cmd, "Url", zonePhoto.Url);
                _db.AddParameter(cmd, "Order", zonePhoto.Order);
                _db.AddParameter(cmd, "ParentId", zonePhoto.ParentId);
                _db.AddParameter(cmd, "Status", zonePhoto.Status);
                _db.AddParameter(cmd, "ShowOnHome", zonePhoto.ShowOnHome);
                _db.AddParameter(cmd, "Avatar", zonePhoto.Avatar);
                _db.AddParameter(cmd, "Logo", zonePhoto.Logo);
                _db.AddParameter(cmd, "Description", zonePhoto.Description);
                _db.AddParameter(cmd, "CatId", zonePhoto.CatId);
                _db.AddParameter(cmd, "ZoneRelation", zonePhoto.ZoneRelation);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }    
        public bool MoveUp(int zonePhotoId)
        {

            const string commandText = "PhotoCms_ZonePhoto_MoveUp";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhotoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool MoveDown(int zonePhotoId)
        {
            const string commandText = "PhotoCms_ZonePhoto_MoveDown";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhotoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int zonePhotoId)
        {
            const string commandText = "PhotoCms_ZonePhoto_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", zonePhotoId);

                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Gets
        public List<ZonePhotoEntity> GetZonePhotoActivedByUsernameAndPermissionIds(string username, string permissionIds)
        {
            const string commandText = "PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds";
            try
            {
                List<ZonePhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "PermissionIds", permissionIds);
                data = _db.GetList<ZonePhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ZonePhotoEntity> GetListByParentId(int parentId, int status)
        {
            const string commandText = "PhotoCms_ZonePhoto_GetListByParentId";
            try
            {
                List<ZonePhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "Status", status);
                data = _db.GetList<ZonePhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public ZonePhotoEntity GetById(int id)
        {
            const string commandText = "PhotoCms_ZonePhoto_GetById";
            try
            {
                ZonePhotoEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ZonePhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<ZoneEntity> GetZoneByParentId(int parentId)
        {

            const string commandText = "CMS_ZonePhoto_GetByParentId";
            try
            {
                List<ZoneEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                data = _db.GetList<ZoneEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }

        public List<PhotoGroupDetailInZonePhotoEntity> GetZonePhotoByPhotoId(int id)
        {

            const string commandText = "CMS_PhotoGroupDetailInZonePhoto_GetByPhotoId";
            try
            {
                List<PhotoGroupDetailInZonePhotoEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.GetList<PhotoGroupDetailInZonePhotoEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }


        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected ZonePhotoDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
