﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class PhotoTagDalBase
    {
        public List<PhotoTagsEntity> GetByPhotoId(long photoId)
        {
            const string commandText = "PhotoCms_PhotoTag_GetByPhotoId";
            try
            {
                List<PhotoTagsEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PhotoId", photoId);
                data = _db.GetList<PhotoTagsEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Get methods
        public PhotoTagEntity GetTagByTagName(string name, bool getTagHasNewsOnly = false)
        {
            const string commandText = "CMS_PhotoTag_GetByName";
            try
            {
                PhotoTagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", name);
                data = _db.Get<PhotoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoTagEntity> SearchTag(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_PhotoTag_Search";
            try
            {
                List<PhotoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<PhotoTagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<PhotoTagEntity> SearchTag(string keyword)
        {
            const string commandText = "CMS_PhotoTag_SearchByName";
            try
            {
                List<PhotoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Name", keyword);                
                data = _db.GetList<PhotoTagEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public PhotoTagEntity GetById(int id)
        {
            const string commandText = "Cms_PhotoTag_GetById";
            try
            {
                PhotoTagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<PhotoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public List<PhotoTagEntity> GetByAlbumId(int id)
        {
            const string commandText = "Cms_PhotoTag_GetByAlbumId";
            try
            {
                List<PhotoTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "AlbumId", id);
                data = _db.GetList<PhotoTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        #endregion

        #region Update methods
        public bool Insert(PhotoTagEntity tag, ref int tagId)
        {
            const string commandText = "CMS_PhotoTag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Status", tag.Status);
                _db.AddParameter(cmd, "Url", tag.Url);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(PhotoTagEntity tag)
        {
            const string commandText = "CMS_PhotoTag_Update";
            try
            {

                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id);
                _db.AddParameter(cmd, "Name", tag.Name);
                _db.AddParameter(cmd, "Avatar", tag.Avatar);
                _db.AddParameter(cmd, "Status", tag.Status);
                _db.AddParameter(cmd, "Url", tag.Url);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateByName(PhotoTagEntity tag, ref int tagId)
        {
            const string commandText = "Cms_PhotoTag_UpdateByName";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", tag.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Name", tag.Name);                
                _db.AddParameter(cmd, "Url", tag.Url);
                _db.AddParameter(cmd, "Status", tag.Status);

                var numberOfRow = cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return tagId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion


        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoTagDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
