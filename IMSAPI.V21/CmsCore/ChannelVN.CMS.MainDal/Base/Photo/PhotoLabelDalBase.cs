﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class PhotoLabelDalBase
    {
        #region Update

        public bool Insert(PhotoLabelEntity photoLabel, ref int newPhotoLabelId)
        {

            const string commandText = "PhotoCms_PhotoLabel_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoLabel.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ParentId", photoLabel.ParentId);
                _db.AddParameter(cmd, "Name", photoLabel.Name);
                _db.AddParameter(cmd, "CreatedBy", photoLabel.CreatedBy);

                var numberOfRow = cmd.ExecuteNonQuery();

                newPhotoLabelId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(PhotoLabelEntity photoLabel)
        {
            const string commandText = "PhotoCms_PhotoLabel_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", photoLabel.ParentId);
                _db.AddParameter(cmd, "Name", photoLabel.Name);
                _db.AddParameter(cmd, "Id", photoLabel.Id);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int photoLabelId)
        {
            const string commandText = "PhotoCms_PhotoLabel_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoLabelId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Get

        public PhotoLabelEntity GetById(int photoLabelId)
        {
            const string commandText = "PhotoCms_PhotoLabel_GetById";
            try
            {
                PhotoLabelEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoLabelId);
                data = _db.Get<PhotoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<PhotoLabelEntity> GetByParentId(int parentId, string createdBy)
        {
            const string commandText = "PhotoCms_PhotoLabel_GetByParentLabelId";
            try
            {
                List<PhotoLabelEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                data = _db.GetList<PhotoLabelEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoLabelDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion

    }
}
