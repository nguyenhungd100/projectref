﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public class PhotoByAuthorDalBase
    {
        public bool SavePhotoByAuthor(PhotoByAuthorEntity photoByAuthor, ref int photoByAuthorId)
        {
            const string commandText = "PhotoCms_PhotoByAuthor_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", photoByAuthor.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "PhotoId", photoByAuthor.PhotoId);
                _db.AddParameter(cmd, "AuthorId", photoByAuthor.AuthorId);

                var numberOfRow = cmd.ExecuteNonQuery();
                photoByAuthorId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsPhotoDb _db;

        protected PhotoByAuthorDalBase(CmsPhotoDb db)
        {
            _db = db;
        }

        protected CmsPhotoDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
