﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Photo
{
    public abstract class EmbedAlbumDalBase
    {
        public List<EmbedAlbumEntity> Search(string keyword, int type, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_EmbedAlbum_Search";
            try
            {
                List<EmbedAlbumEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<EmbedAlbumEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public EmbedAlbumEntity GetById(long embedAlbumId)
        {
            const string commandText = "CMS_EmbedAlbum_GetById";
            try
            {
                EmbedAlbumEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbumId);

                data = _db.Get<EmbedAlbumEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(EmbedAlbumEntity embedAlbum, ref int newEmbedAlbumId)
        {
            const string commandText = "CMS_EmbedAlbum_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbum.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Type", embedAlbum.Type);
                _db.AddParameter(cmd, "ZoneId", embedAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", embedAlbum.Name);
                _db.AddParameter(cmd, "Avatar", embedAlbum.Avatar);
                _db.AddParameter(cmd, "CreatedBy", embedAlbum.CreatedBy);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);
                _db.AddParameter(cmd, "Description", embedAlbum.Description);

                var numberOfRow = cmd.ExecuteNonQuery();

                newEmbedAlbumId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(EmbedAlbumEntity embedAlbum)
        {
            const string commandText = "CMS_EmbedAlbum_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbum.Id);
                _db.AddParameter(cmd, "Type", embedAlbum.Type);
                _db.AddParameter(cmd, "ZoneId", embedAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", embedAlbum.Name);
                _db.AddParameter(cmd, "Avatar", embedAlbum.Avatar);
                _db.AddParameter(cmd, "LastModifiedBy", embedAlbum.LastModifiedBy);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);
                _db.AddParameter(cmd, "Description", embedAlbum.Description);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int embedAlbumId)
        {
            const string commandText = "CMS_EmbedAlbum_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbumId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected EmbedAlbumDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
