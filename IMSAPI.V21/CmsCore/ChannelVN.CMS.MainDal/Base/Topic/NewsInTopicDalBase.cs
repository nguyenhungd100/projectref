﻿using System;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Topic
{
    public abstract class TopicNewsDalBase
    {
        public bool UpdateNews(int TopicId, string deleteNewsId, string addNewsId)
        {
            const string commandText = "CMS_Topic_UpdateNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", TopicId);
                _db.AddParameter(cmd, "DeleteNewsIds", deleteNewsId);
                _db.AddParameter(cmd, "AddNewsIds", addNewsId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool MultiUpdateTopicNews(string listTopicId, long newsId, int topicId)
        {
            const string commandText = "CMS_Topic_MultiUpdateTopicNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ListTopicId", listTopicId);
                _db.AddParameter(cmd, "IsPrimaryTopicId", topicId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsHot(long topicId, string addNewsAvatar, string addNewsId)
        {
            const string commandText = "CMS_Topic_UpdateNewsHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "AddNewsAvatar", addNewsAvatar);
                _db.AddParameter(cmd, "AddNewsIds", addNewsId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateTopicNews(long newsId, string relatedTopics)
        {
            const string commandText = "CMS_TopicNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ListOfPriority", relatedTopics);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected TopicNewsDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
