﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Topic
{
    public class TopicInZoneDal : TopicInZoneDalBase
    {
        internal TopicInZoneDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
