﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.Topic
{
    public abstract class TopicDalBase
    {
        #region Get
        public TopicEntity GetTopicByName(string nameTopic)
        {
            const string commandText = "CMS_Topic_GetTopicByName";
            try
            {
                TopicEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicName", nameTopic);
                data = _db.Get<TopicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public TopicEntity GetTopicByTopicId(long TopicId)
        {
            const string commandText = "CMS_Topic_GetTopicByTopicID";
            try
            {
                TopicEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", TopicId);
                data = _db.Get<TopicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsInTopic_GetByNewsId";
            try
            {
                List<TopicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<TopicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TopicEntity> GetTopicByParentId(int topicId)
        {
            const string commandText = "CMS_Topic_GetTopicByParentId";
            try
            {
                List<TopicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ParentId", topicId);
                data = _db.GetList<TopicEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TagWithSimpleFieldEntity> GetTagsByTopicId(int topicId)
        {
            const string commandText = "CMS_Topic_GetTagByTopicId";
            try
            {
                List<TagWithSimpleFieldEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "topicId", topicId);
                data = _db.GetList<TagWithSimpleFieldEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TopicRelation> GetTopicRelationByTopicId(int topicId)
        {
            const string commandText = "CMS_Topic_GetTopicRelationByTopicId";
            try
            {
                List<TopicRelation> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "topicId", topicId);
                data = _db.GetList<TopicRelation>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TopicEntity> SearchTopic(string keyword, int zoneId, int orderBy, int isHotTopic, int isActive, int parentId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Topic_Search";
            try
            {
                List<TopicEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "IsHot", isHotTopic);
                _db.AddParameter(cmd, "IsActive", isActive);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "OrderBy", orderBy);
                _db.AddParameter(cmd, "ParentId", parentId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<TopicEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TopicParent> ListParentTopicById(int topicId)
        {
            const string commandText = "CMS_Topic_GetParentTopicById";
            try
            {
                List<TopicParent> data = null;
                var cmd = _db.CreateCommand(commandText, true);               
                _db.AddParameter(cmd, "TopicId", topicId);                
                data = _db.GetList<TopicParent>(cmd);
                
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ToggleTopicActive(int id)
        {
            const string commandText = "CMS_Topic_ToggleTopicActive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ToggleTopicIconActive(int id)
        {
            const string commandText = "CMS_Topic_ToggleTopicIconActive";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool ToggleTopicIsToolbar(int id)
        {
            const string commandText = "CMS_Topic_ToggleTopicIsToolbar";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var number = cmd.ExecuteNonQuery();
                return number > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<TopicEntity> InitESAllTopic(int pageIndex, int pageSize, DateTime fromDate, DateTime toDate, ref int totalRow)
        {
            //const string commandText = "CMS_Thread_InitESAllThread";
            const string commandText = @"
                                        DECLARE @UpperBand int, @LowerBand int

                                        SELECT @totalRow = COUNT(*) FROM Topic T 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

                                        SET @LowerBand  = (@pageIndex - 1) * @PageSize
                                        SET @UpperBand  = (@pageIndex * @PageSize)
                                        SELECT * FROM (
                                        SELECT T.*,
	                                        ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
                                        FROM Topic T 
                                        where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
                                        ) AS temp
                                        WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand";
            try
            {
                List<TopicEntity> data = null;
                var cmd = _db.CreateCommand(commandText);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                data = _db.GetList<TopicEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Update
        public bool Insert(TopicEntity Topic, int zoneId, string zoneIdList, ref int TopicId)
        {

            const string commandText = "CMS_Topic_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Topic.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicName", Topic.TopicName);
                _db.AddParameter(cmd, "Logo", Topic.Logo);
                _db.AddParameter(cmd, "LogoFancyClose", Topic.LogoFancyClose);
                _db.AddParameter(cmd, "LogoTopicName", Topic.LogoTopicName);
                _db.AddParameter(cmd, "LogoSubMenu", Topic.LogoSubMenu);
                _db.AddParameter(cmd, "Cover", Topic.Cover);
                _db.AddParameter(cmd, "Description", Topic.Description);
                _db.AddParameter(cmd, "DisplayUrl", Topic.DisplayUrl);
                _db.AddParameter(cmd, "IsActive", Topic.IsActive);
                _db.AddParameter(cmd, "IsIconActive", Topic.IsIconActive);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "ListTagId", Topic.TagInString);
                _db.AddParameter(cmd, "DefaultViewMode", Topic.DefaultViewMode);
                _db.AddParameter(cmd, "GuideToSendMail", Topic.GuideToSendMail);
                _db.AddParameter(cmd, "TopicEmail", Topic.TopicEmail);
                _db.AddParameter(cmd, "IsTopToolbar", Topic.IsTopToolbar);
                _db.AddParameter(cmd, "ParentId", Topic.ParentId);
                _db.AddParameter(cmd, "RelationTopic", Topic.RelationTopic);
                _db.AddParameter(cmd, "DisplayName", Topic.DisplayName);
                _db.AddParameter(cmd, "Priority", Topic.Priority);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "CreatedBy", Topic.CreatedBy);
                _db.AddParameter(cmd, "IsAmp", Topic.IsAmp);

                var numberOfRow = cmd.ExecuteNonQuery();

                TopicId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(TopicEntity Topic, int zoneId, string zoneIdList)
        {
            const string commandText = "CMS_Topic_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", Topic.Id);
                _db.AddParameter(cmd, "TopicName", Topic.TopicName);
                _db.AddParameter(cmd, "Logo", Topic.Logo);
                _db.AddParameter(cmd, "LogoFancyClose", Topic.LogoFancyClose);
                _db.AddParameter(cmd, "LogoTopicName", Topic.LogoTopicName);
                _db.AddParameter(cmd, "LogoSubMenu", Topic.LogoSubMenu);
                _db.AddParameter(cmd, "Cover", Topic.Cover);
                _db.AddParameter(cmd, "Description", Topic.Description);
                _db.AddParameter(cmd, "DisplayUrl", Topic.DisplayUrl);
                _db.AddParameter(cmd, "IsActive", Topic.IsActive);
                _db.AddParameter(cmd, "IsIconActive", Topic.IsIconActive);
                _db.AddParameter(cmd, "PrimaryZoneId", zoneId);
                _db.AddParameter(cmd, "ListTagId", Topic.TagInString);
                _db.AddParameter(cmd, "DefaultViewMode", Topic.DefaultViewMode==-1 ? 0: Topic.DefaultViewMode);
                _db.AddParameter(cmd, "GuideToSendMail", Topic.GuideToSendMail);
                _db.AddParameter(cmd, "TopicEmail", Topic.TopicEmail);
                _db.AddParameter(cmd, "IsTopToolbar", Topic.IsTopToolbar);
                _db.AddParameter(cmd, "ParentId", Topic.ParentId);
                _db.AddParameter(cmd, "RelationTopic", Topic.RelationTopic);
                _db.AddParameter(cmd, "DisplayName", Topic.DisplayName);
                _db.AddParameter(cmd, "Priority", Topic.Priority);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "ModifiedBy", Topic.ModifiedBy);
                _db.AddParameter(cmd, "IsAmp", Topic.IsAmp);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  bool DeleteById(long TopicId)
        {
            const string commandText = "CMS_Topic_DeleteByTopicID";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", TopicId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateTopicHot(long TopicId, bool isHotTopic)
        {
            const string commandText = "CMS_Topic_UpdateTopicHot";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", TopicId);
                _db.AddParameter(cmd, "IsHot", isHotTopic);
                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region BoxTopicEmbed

        public List<BoxTopicEmbedEntity> ListBoxTopicEmbed(int topicId, int type)
        {
            const string commandText = "CMS_BoxTopicEmbed_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TopicId", topicId);
                _db.AddParameter(cmd, "Type", type);
                List<BoxTopicEmbedEntity> data = _db.GetList<BoxTopicEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertBoxTopicEmbed(BoxTopicEmbedEntity box, ref int id)
        {
            const string commandText = "CMS_BoxTopicEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "TopicId", box.TopicId);
                _db.AddParameter(cmd, "Label", box.Label);
                _db.AddParameter(cmd, "Title", box.Title);
                _db.AddParameter(cmd, "Avatar", box.Avatar);
                _db.AddParameter(cmd, "Url", box.Url);
                _db.AddParameter(cmd, "Description", box.Description);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);
                _db.AddParameter(cmd, "DisplayStyle", box.DisplayStyle);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateBoxTopicEmbed(BoxTopicEmbedEntity box)
        {
            const string commandText = "CMS_BoxTopicEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id);
                _db.AddParameter(cmd, "TopicId", box.TopicId);
                _db.AddParameter(cmd, "Label", box.Label);
                _db.AddParameter(cmd, "Title", box.Title);
                _db.AddParameter(cmd, "Avatar", box.Avatar);
                _db.AddParameter(cmd, "Url", box.Url);
                _db.AddParameter(cmd, "Description", box.Description);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);
                _db.AddParameter(cmd, "DisplayStyle", box.DisplayStyle);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteBoxTopicEmbed(int id)
        {
            const string commandText = "CMS_BoxTopicEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);               
                _db.AddParameter(cmd, "Id", id);
                
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region BoxTopicOnPageEmbed

        public List<BoxTopicOnPageEmbedEntity> ListBoxTopicOnPageEmbed(int zoneId, int type)
        {
            const string commandText = "CMS_BoxTopicOnPageEmbed_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                List<BoxTopicOnPageEmbedEntity> data = _db.GetList<BoxTopicOnPageEmbedEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateBoxTopicOnPageEmbed(string listTopicId, int zoneId, int type)
        {
            const string commandText = "CMS_BoxTopicOnPageEmbed_UpdateBoxTopicOnPageEmbed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListOfPriority", listTopicId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box, ref int id)
        {
            const string commandText = "CMS_BoxTopicOnPageEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", box.ZoneId);
                _db.AddParameter(cmd, "TopicId", box.TopicId);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool UpdateBoxTopicOnPageEmbed(BoxTopicOnPageEmbedEntity box)
        {
            const string commandText = "CMS_BoxTopicOnPageEmbed_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);

                _db.AddParameter(cmd, "Id", box.Id);
                _db.AddParameter(cmd, "ZoneId", box.ZoneId);
                _db.AddParameter(cmd, "TopicId", box.TopicId);
                _db.AddParameter(cmd, "SortOrder", box.SortOrder);
                _db.AddParameter(cmd, "Type", box.Type);

                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteBoxTopicOnPageEmbed(int id)
        {
            const string commandText = "CMS_BoxTopicOnPageEmbed_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected TopicDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
