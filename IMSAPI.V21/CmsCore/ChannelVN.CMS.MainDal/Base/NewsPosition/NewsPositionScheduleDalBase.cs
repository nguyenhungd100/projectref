﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsPosition
{
    public abstract class NewsPositionScheduleDalBase
    {
        public  bool Insert(NewsPositionScheduleEntity newsPosition)
        {
            const string commandText = "CMS_NewsPositionSchedule_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPosition.NewsId);
                _db.AddParameter(cmd, "PositionTypeId", newsPosition.PositionTypeId);
                _db.AddParameter(cmd, "ZoneId", newsPosition.ZoneId);
                _db.AddParameter(cmd, "Order", newsPosition.Order);
                _db.AddParameter(cmd, "ScheduleDate", newsPosition.ScheduleDate);
                _db.AddParameter(cmd, "CreatedBy", newsPosition.CreatedBy);
                bool data = _db.ExecuteNonQuery(cmd)>0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  bool Update(NewsPositionScheduleEntity newsPosition)
        {
            const string commandText = "CMS_NewsPositionSchedule_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsPosition.NewsId);
                _db.AddParameter(cmd, "PositionTypeId", newsPosition.PositionTypeId);
                _db.AddParameter(cmd, "ZoneId", newsPosition.ZoneId);
                _db.AddParameter(cmd, "Order", newsPosition.Order);
                _db.AddParameter(cmd, "ScheduleDate", newsPosition.ScheduleDate);
                _db.AddParameter(cmd, "CreatedBy", newsPosition.CreatedBy);
                _db.AddParameter(cmd, "Id", newsPosition.Id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  bool Delete(int id)
        {
            const string commandText = "CMS_NewsPositionSchedule_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  List<NewsPositionScheduleEntity> GetListScheduleByPositionTypeId(int positionTypeId)
        {
            const string commandText = "CMS_NewsPositionSchedule_GetByPositionTypeId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PositionTypeId", positionTypeId);
                List<NewsPositionScheduleEntity> data = _db.GetList<NewsPositionScheduleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsPositionEntity> GetListScheduleByPositionTypeIdAndOrder(int typeId, int order, int zoneId)
        {
            const string commandText = "CMS_NewsPositionSchedule_GetByPositionTypeIdAndOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PositionTypeId", typeId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                List<NewsPositionEntity> data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  List<NewsPositionScheduleEntity> GetUnprocessItem(int maxProcess)
        {
            const string commandText = "CMS_NewsPositionSchedule_GetUnprocessItem";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "MaxProcess", maxProcess);
                List<NewsPositionScheduleEntity> data = _db.GetList<NewsPositionScheduleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public  bool UpdateProcessedState(string listProcessIds)
        {
            const string commandText = "CMS_NewsPositionSchedule_Processed";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListProcessId", listProcessIds);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPositionScheduleDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
