﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsPosition
{
    public class NewsPositionTypeDal : NewsPositionTypeDalBase
    {
        internal NewsPositionTypeDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
