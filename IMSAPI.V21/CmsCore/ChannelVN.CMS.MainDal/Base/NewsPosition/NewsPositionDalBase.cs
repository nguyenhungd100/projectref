﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsPosition
{
    public abstract class NewsPositionDalBase
    {
        public List<NewsPositionEntity> GetListByTypeAndZoneId(int typeId, int zoneId, string listOfOrder = "")
        {
            const string commandText = "CMS_NewsPosition_GetByTypeAndZoneId";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListOfOrder", listOfOrder);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionEntity> GetListNewsPositionAllBom(int typeId, int zoneId)
        {
            const string commandText = "CMS_NewsPosition_GetAllBomByTypeAndZoneId";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionEntity> GetListByTypeAndZoneIdWithOrderByDistritbutionDate(int typeId, int zoneId, int top)
        {
            const string commandText = "CMS_NewsPosition_GetListByTypeAndZoneIdWithOrderByDistritbutionDate";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Top", top);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool LockPosition(long id, int lockForZoneId, DateTime expiredLock)
        {
            const string commandText = "CMS_NewsPosition_LockPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "LockForZoneId", lockForZoneId);
                _db.AddParameter(cmd, "ExpiredLock", expiredLock);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UnlockPosition(long id)
        {
            const string commandText = "CMS_NewsPosition_UnlockPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPositionEntity GetNewsPositionById(long id)
        {
            const string commandText = "CMS_NewsPosition_GetById";
            try
            {
                NewsPositionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SaveLinkPosition(int typeId, int position, int zoneId, long newsId, int type, string title, string avatar, string url, string sapo)
        {
            const string commandText = "CMS_NewsPosition_UpdateLinkPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "Position", position);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "Avatar", avatar);
                _db.AddParameter(cmd, "Url", url);
                _db.AddParameter(cmd, "Sapo", sapo);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsId", newsId);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SaveNewsPosition(int typeId, int zoneId, int order, long newsId, DateTime expiredLock, string avatar, int avatarIndex, bool checkNewsExists = true)
        {
            Logger.WriteLog(Logger.LogType.Trace, "typeId:" + typeId + ";newsId:" + newsId);
            const string commandText = "CMS_NewsPosition_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsId", newsId);
                if (expiredLock < Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredLock", DateTime.Now.AddMonths(1));
                else
                    _db.AddParameter(cmd, "ExpiredLock", expiredLock);
                _db.AddParameter(cmd, "NewAvatar", avatar);
                _db.AddParameter(cmd, "AvatarIndex", avatarIndex);
                _db.AddParameter(cmd, "CheckNewsExists", checkNewsExists);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, string.Format("{0}:{1}", commandText, ex.Message));
                return false;
            }
        }

        public bool SaveNewsPositionSchedule(int typeId, int zoneId, int order, string listNewsId, string listScheduleDate, string listRemovePosition)
        {
            const string commandText = "CMS_NewsPosition_UpdateSchedule";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                _db.AddParameter(cmd, "ListScheduleDate", listScheduleDate);
                _db.AddParameter(cmd, "ListRemovePosition", listRemovePosition);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateForUnlockedPosition(int typeId, int zoneId)
        {
            const string commandText = "CMS_NewsPosition_UpdateForUnlockedPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateExpiredPosition()
        {
            const string commandText = "CMS_NewsPosition_UpdateExpiredPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePositionOrder(int typeId, int zoneId, string listPositionId)
        {
            const string commandText = "CMS_NewsPosition_UpdatePositionOrder";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListPositionId", listPositionId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdatePositionOrderByPositionId(long positionId, int positionOrder)
        {
            const string commandText = "CMS_NewsPosition_UpdatePositionOrderByPositionId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "PositionId", positionId);
                _db.AddParameter(cmd, "Order", positionOrder);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateForBomd(int typeId, int zoneId, int order, long newsId)
        {
            const string commandText = "CMS_NewsPosition_UpdateForBomd";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsId", newsId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionForCheckAutoUpdateEntity> GetListNewsByListNewsId(int typeId, int zoneId, string listNewsId)
        {
            const string commandText = "CMS_NewsPosition_GetListNewsByListNewsId";
            try
            {
                List<NewsPositionForCheckAutoUpdateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsPositionForCheckAutoUpdateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionForCheckAutoUpdateEntity> GetListNewsByListNewsIdAndTypeId(int typeId, string listNewsId)
        {
            const string commandText = "GetListNewsByListNewsIdAndTypeId";
            try
            {
                List<NewsPositionForCheckAutoUpdateEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ListNewsId", listNewsId);
                data = _db.GetList<NewsPositionForCheckAutoUpdateEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsPositionForAutoUpdate(long newsPositionId, NewsPublishEntity newsPublish)
        {
            const string commandText = "CMS_NewsPosition_UpdateNewsInfoInPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsPositionId);
                _db.AddParameter(cmd, "NewsId", newsPublish.NewsId);
                _db.AddParameter(cmd, "Title", newsPublish.Title);
                _db.AddParameter(cmd, "SubTitle", newsPublish.SubTitle);
                _db.AddParameter(cmd, "Sapo", newsPublish.Sapo);
                _db.AddParameter(cmd, "Avatar", newsPublish.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", newsPublish.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", newsPublish.Avatar2);
                _db.AddParameter(cmd, "Avatar3", newsPublish.Avatar3);
                _db.AddParameter(cmd, "Avatar4", newsPublish.Avatar4);
                _db.AddParameter(cmd, "Avatar5", newsPublish.Avatar5);
                _db.AddParameter(cmd, "Author", newsPublish.Author);
                _db.AddParameter(cmd, "NewsRelation", newsPublish.NewsRelation);
                _db.AddParameter(cmd, "Source", newsPublish.Source);
                _db.AddParameter(cmd, "IsFocus", newsPublish.IsFocus);
                _db.AddParameter(cmd, "NewsType", newsPublish.Type);
                _db.AddParameter(cmd, "ThreadId", newsPublish.ThreadId);
                _db.AddParameter(cmd, "DistributionDate", newsPublish.DistributionDate);
                _db.AddParameter(cmd, "Url", newsPublish.Url);
                _db.AddParameter(cmd, "DisplayStyle", newsPublish.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", newsPublish.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", newsPublish.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", newsPublish.AvatarCustom);
                _db.AddParameter(cmd, "OriginalId", newsPublish.OriginalId);
                _db.AddParameter(cmd, "InitSapo", newsPublish.InitSapo);
                _db.AddParameter(cmd, "OriginalUrl", newsPublish.OriginalUrl);
                _db.AddParameter(cmd, "InterviewId", newsPublish.InterviewId);
                _db.AddParameter(cmd, "IsBreakingNews", newsPublish.IsBreakingNews);
                _db.AddParameter(cmd, "IsPr", newsPublish.IsPr);
                _db.AddParameter(cmd, "AdStore", newsPublish.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", newsPublish.AdStoreUrl);
                _db.AddParameter(cmd, "RollingNewsId", newsPublish.RollingNewsId);
                _db.AddParameter(cmd, "ZoneIdForNews", newsPublish.ZoneId);
                _db.AddParameter(cmd, "TagSubTitleId", newsPublish.TagSubTitleId);
                _db.AddParameter(cmd, "LocationType", newsPublish.LocationType);
                _db.AddParameter(cmd, "ExpiredDate", newsPublish.ExpiredDate);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsInfoIntoNewsPosition(long newsId)
        {
            const string commandText = "CMS_NewsPosition_UpdateNewsInfo";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateDistributionDate(long newsId, DateTime distributionDate)
        {
            const string commandText = "CMS_NewsPosition_UpdateDistributionDate";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", newsId);
                _db.AddParameter(cmd, "DistributionDate", distributionDate);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionTypeEntity> GetAllNewsPositionType()
        {
            const string commandText = "CMS_NewsPositionType_GetAll";
            try
            {
                List<NewsPositionTypeEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<NewsPositionTypeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPositionTypeEntity GetNewsPositionTypeById(int typeId)
        {
            const string commandText = "CMS_NewsPositionType_GetByTypeId";
            try
            {
                NewsPositionTypeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                data = _db.Get<NewsPositionTypeEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPositionEntity GetNewsInfoInPosition(int typeId, int zoneId, long newsId)
        {
            const string commandText = "CMS_NewsPosition_GetNewsInPosition";
            try
            {
                NewsPositionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.Get<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public int GetCountNewsInPositionByDistributionDate(int typeId, int zoneId, DateTime distributionDate)
        {
            const string commandText = "CMS_NewsPosition_CountNewsInPositionByDistributionDate";
            try
            {
                SqlDataReader data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "DistributionDate", distributionDate);
                data = (SqlDataReader)_db.ExecuteReader(cmd);
                if (data.Read())
                    return Utility.ConvertToInt(data[0]);
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPositionEntity GetOldestPosition(int typeId, int zoneId)
        {
            const string commandText = "CMS_NewsPosition_GetOldestPosition";
            try
            {
                NewsPositionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.Get<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsPositionEntity GetOldestPositionIncludeNewsId(int typeId, int zoneId, long newsId)
        {
            const string commandText = "CMS_NewsPosition_GetOldestPositionIncludeNewsId";
            try
            {
                NewsPositionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "IncludeNewsId", newsId);
                data = _db.Get<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        /// <summary>
        /// Update news into Auto update position in NewsPosition table
        /// </summary>
        /// <param name="typeId">TypeId</param>
        /// <param name="listZoneId">ListZoneId in NewsPosition</param>
        /// <param name="newsId">NewsId for update</param>
        /// <param name="zoneIdForNews">ZoneId for this news (ZoneIdForNews!=0 => Use this ZoneId; else use Zone in each ListZoneId)</param>
        /// <returns></returns>
        public bool UpdateNewsIntoAutoUpdatePosition(int typeId, string listZoneId, long newsId, int zoneIdForNews)
        {
            const string commandText = "CMS_NewsPosition_UpdateNewsIntoAutoUpdatePosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ListZoneId", listZoneId);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneIdForNews", zoneIdForNews);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Update news into manual update position
        /// </summary>
        /// <param name="listTypeId">List NewsPosition TypeId (ngan cach boi , )</param>
        /// <param name="newsId">NewsId</param>
        /// <returns></returns>
        public bool UpdateNewsIntoManualUpdatePosition(string listTypeId, long newsId)
        {
            const string commandText = "CMS_NewsPosition_UpdateNewsIntoManualUpdatePosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ListTypeId", listTypeId);
                _db.AddParameter(cmd, "NewsId", newsId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        /// <summary>
        /// Remove news from auto update position in NewsPosition table
        /// </summary>
        /// <param name="newsId">NewsId for update</param>
        /// <returns></returns>
        public bool RemoveNewsFromAutoUpdatePosition(int typeId, long newsId)
        {
            const string commandText = "CMS_NewsPosition_RemoveNewsFromAutoUpdatePosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "NewsId", newsId);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionEntity> GetAllPositionHasNewsId(long newsId)
        {
            const string commandText = "CMS_NewsPosition_GetAllPositionHasNewsId";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPositionEntity> GetAllPositionHasIds(string ids)
        {
            const string commandText = "CMS_NewsPosition_GetAllPositionByListId";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Ids", ids);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool LockPositionByType(long newsPositionId, int lockTypeId, int order, DateTime expiredLock)
        {
            const string commandText = "CMS_NewsPosition_LockPositionByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsPositionId", newsPositionId);
                _db.AddParameter(cmd, "LockedTypeId", lockTypeId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "ExpiredLock", expiredLock);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool LockPositionByNewsId(long newsId, int zoneId, int lockTypeId, int order, DateTime expiredLock)
        {
            const string commandText = "CMS_NewsPosition_LockPositionByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "LockedTypeId", lockTypeId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "ExpiredLock", expiredLock);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UnlockPositionByType(int lockTypeId, int order)
        {
            const string commandText = "CMS_NewsPosition_UnlockPositionByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "LockedTypeId", lockTypeId);
                _db.AddParameter(cmd, "Order", order);
                int data = _db.ExecuteNonQuery(cmd);

                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region NewsPositionSchedule
        public List<NewsPositionEntity> GetScheduleByTypeAndOrder(int typeId, int order, int zoneId)
        {
            const string commandText = "CMS_NewsPosition_GetScheduleByTypeAndOrder";
            try
            {
                List<NewsPositionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                data = _db.GetList<NewsPositionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPositionDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
