﻿using System;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.NewsSlave
{
    public abstract class NewsSlaveDalBase
    {
        #region NewsSlave                      

        public bool DeleteNews(long newsId)
        {
            const string commandText = @"BEGIN TRANSACTION	
	                                    BEGIN TRY
		                                    DELETE FROM NewsRelation WHERE NewsId = @Id
		                                    DELETE FROM TagNews WHERE NewsID = @Id	
		                                    DELETE FROM NewsInZone WHERE NewsId = @Id
		                                    DELETE FROM News WHERE Id = @Id
		                                    DELETE FROM NewsPublish WHERE NewsId = @Id
		                                    DELETE FROM NewsContent WHERE NewsId = @Id
		
		                                    COMMIT TRANSACTION
	                                    END TRY
	                                    BEGIN CATCH
		                                    DECLARE @ErrorMessage nvarchar(MAX)
		                                    SET @ErrorMessage = ERROR_MESSAGE()
		                                    RAISERROR (@ErrorMessage, 16, 1);
		                                    ROLLBACK TRANSACTION
	                                    END CATCH";
            try
            {
                var cmd = _db.CreateCommand(commandText, false);
                _db.AddParameter(cmd, "Id", newsId);
                
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #endregion

        #region Core members

        private readonly SlaveCmsDb _db;

        protected NewsSlaveDalBase(SlaveCmsDb db)
        {
            _db = db;
        }

        protected SlaveCmsDb Database
        {
            get { return _db; }
        }

        #endregion        
    }
}
