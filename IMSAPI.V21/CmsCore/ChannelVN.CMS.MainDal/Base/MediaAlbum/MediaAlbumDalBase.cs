﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;
using System.Data;

namespace ChannelVN.CMS.MainDal.Base.MediaAlbum
{
    public abstract class MediaAlbumDalBase
    {
        public List<MediaAlbumEntity> Search(string keyword, int zoneId, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_MediaAlbum_Search";
            try
            {
                List<MediaAlbumEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);

                data = _db.GetList<MediaAlbumEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public MediaAlbumEntity GetById(long MediaAlbumId)
        {
            const string commandText = "CMS_MediaAlbum_GetById";
            try
            {
                MediaAlbumEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumId);

                data = _db.Get<MediaAlbumEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public bool Insert(MediaAlbumEntity MediaAlbum, ref int newMediaAlbumId)
        {
            const string commandText = "CMS_MediaAlbum_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbum.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", MediaAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", MediaAlbum.Name);
                _db.AddParameter(cmd, "Avatar", MediaAlbum.Avatar);
                _db.AddParameter(cmd, "CreatedBy", MediaAlbum.CreatedBy);
                _db.AddParameter(cmd, "Status", MediaAlbum.Status);
                _db.AddParameter(cmd, "IsOnHome", MediaAlbum.IsOnHome);

                var numberOfRow = cmd.ExecuteNonQuery();

                newMediaAlbumId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Update(MediaAlbumEntity MediaAlbum)
        {
            const string commandText = "CMS_MediaAlbum_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbum.Id);
                _db.AddParameter(cmd, "ZoneId", MediaAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", MediaAlbum.Name);
                _db.AddParameter(cmd, "Avatar", MediaAlbum.Avatar);
                _db.AddParameter(cmd, "LastModifiedBy", MediaAlbum.LastModifiedBy);
                _db.AddParameter(cmd, "Status", MediaAlbum.Status);
                _db.AddParameter(cmd, "IsOnHome", MediaAlbum.IsOnHome);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int MediaAlbumId)
        {
            const string commandText = "CMS_MediaAlbum_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumId);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected MediaAlbumDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
