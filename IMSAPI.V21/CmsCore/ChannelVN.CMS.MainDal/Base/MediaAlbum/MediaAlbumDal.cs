﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.MediaAlbum
{
    public class MediaAlbumDal : MediaAlbumDalBase
    {
        internal MediaAlbumDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
