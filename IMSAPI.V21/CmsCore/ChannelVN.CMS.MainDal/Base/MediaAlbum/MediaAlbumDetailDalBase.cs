﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.MediaAlbum;
using ChannelVN.CMS.MainDal.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.MainDal.Base.MediaAlbum
{
    public abstract class MediaAlbumDetailDalBase
    {
        public bool InsertMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumDetail, string tagIdList, ref int newPhotoId)
        {
            const string commandText = "CMS_MediaAlbumDetail_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumDetail.Id, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "AlbumId", MediaAlbumDetail.AlbumId);
                _db.AddParameter(cmd, "ZoneId", MediaAlbumDetail.ZoneId);
                _db.AddParameter(cmd, "MediaType", MediaAlbumDetail.MediaType);
                _db.AddParameter(cmd, "Title", MediaAlbumDetail.Title);
                _db.AddParameter(cmd, "MediaContent", MediaAlbumDetail.MediaContent);
                _db.AddParameter(cmd, "Description", MediaAlbumDetail.Description);
                _db.AddParameter(cmd, "Avatar", MediaAlbumDetail.Avatar);
                _db.AddParameter(cmd, "Status", MediaAlbumDetail.Status);
                _db.AddParameter(cmd, "IsHot", MediaAlbumDetail.IsHot);
                _db.AddParameter(cmd, "CreatedBy", MediaAlbumDetail.CreatedBy);
                _db.AddParameter(cmd, "Author", MediaAlbumDetail.Author);
                _db.AddParameter(cmd, "DistributionDate", MediaAlbumDetail.DistributionDate);
                _db.AddParameter(cmd, "UnSignName", MediaAlbumDetail.UnSignName);
                _db.AddParameter(cmd, "Tags", MediaAlbumDetail.Tags);
                _db.AddParameter(cmd, "NewsId", MediaAlbumDetail.NewsId);
                _db.AddParameter(cmd, "Url", MediaAlbumDetail.Url);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Priority", MediaAlbumDetail.Priority);

                var data = cmd.ExecuteNonQuery();
                newPhotoId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return newPhotoId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateMediaAlbumDetail(MediaAlbumDetailEntity MediaAlbumDetail, string tagIdList)
        {

            const string commandText = "CMS_MediaAlbumDetail_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumDetail.Id);
                _db.AddParameter(cmd, "ZoneId", MediaAlbumDetail.ZoneId);
                _db.AddParameter(cmd, "AlbumId", MediaAlbumDetail.AlbumId);
                _db.AddParameter(cmd, "MediaType", MediaAlbumDetail.MediaType);
                _db.AddParameter(cmd, "Title", MediaAlbumDetail.Title);
                _db.AddParameter(cmd, "MediaContent", MediaAlbumDetail.MediaContent);
                _db.AddParameter(cmd, "Description", MediaAlbumDetail.Description);
                _db.AddParameter(cmd, "Avatar", MediaAlbumDetail.Avatar);
                _db.AddParameter(cmd, "Status", MediaAlbumDetail.Status);
                _db.AddParameter(cmd, "IsHot", MediaAlbumDetail.IsHot);
                _db.AddParameter(cmd, "ModifiedBy", MediaAlbumDetail.CreatedBy);
                _db.AddParameter(cmd, "Author", MediaAlbumDetail.Author);
                _db.AddParameter(cmd, "DistributionDate", MediaAlbumDetail.DistributionDate);
                _db.AddParameter(cmd, "UnSignName", MediaAlbumDetail.UnSignName);
                _db.AddParameter(cmd, "Tags", MediaAlbumDetail.Tags);
                _db.AddParameter(cmd, "NewsId", MediaAlbumDetail.NewsId);
                _db.AddParameter(cmd, "Url", MediaAlbumDetail.Url);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "Priority", MediaAlbumDetail.Priority);

                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateMediaAlbumDetailStatus(int id, EnumMediaAlbumDetailStatus status)
        {

            const string commandText = "CMS_MediaAlbumDetail_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool UpdateMediaAlbumDetailViewCount(int id, int viewCount)
        {

            const string commandText = "CMS_MediaAlbumDetail_UpdateViewCount";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "ViewCount", viewCount);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool DeleteMediaAlbumDetail(long MediaAlbumDetailId)
        {
            const string commandText = "CMS_MediaAlbumDetail_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumDetailId);
                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public MediaAlbumDetailEntity GetMediaAlbumDetailById(int MediaAlbumDetailId)
        {
            const string commandText = "CMS_MediaAlbumDetail_GetById";
            try
            {
                MediaAlbumDetailEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumDetailId);
                data = _db.Get<MediaAlbumDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MediaAlbumDetailEntity> GetMediaAlbumDetailByAlbumId(int MediaAlbumId)
        {
            const string commandText = "CMS_MediaAlbumDetail_GetByAlbumId";
            try
            {
                List<MediaAlbumDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", MediaAlbumId);
                data = _db.GetList<MediaAlbumDetailEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<MediaAlbumDetailEntity> SearchMediaAlbumDetail(string keyword, int zoneId, EnumMediaAlbumDetailMediaType mediaType, DateTime fromDate, DateTime toDate, string createdBy, string author, EnumMediaAlbumDetailStatus status, int isHot, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_MediaAlbumDetail_Search";
            try
            {
                List<MediaAlbumDetailEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, System.Data.ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "MediaType", (int)mediaType);
                _db.AddParameter(cmd, "CreatedDateFrom", fromDate);
                _db.AddParameter(cmd, "CreatedDateTo", toDate);
                _db.AddParameter(cmd, "CreatedBy", createdBy);
                _db.AddParameter(cmd, "Author", author);
                _db.AddParameter(cmd, "Status", (int)status);
                _db.AddParameter(cmd, "IsHot", isHot);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<MediaAlbumDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<MediaAlbumDetailCountEntity> CountMediaAlbumDetail(string username, int zoneId)
        {
            const string commandText = "CMS_MediaAlbumDetail_GetPhotoCount";
            try
            {
                List<MediaAlbumDetailCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                data = _db.GetList<MediaAlbumDetailCountEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #region Constructor

        private readonly CmsMainDb _db;

        protected MediaAlbumDetailDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
