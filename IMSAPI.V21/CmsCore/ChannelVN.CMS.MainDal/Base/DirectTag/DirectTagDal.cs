﻿using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.Base.DirectTag
{
    public class DirectTagDal : DirectTagDalBase
    {
        internal DirectTagDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
