﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Common;

namespace ChannelVN.CMS.MainDal.Base.DirectTag
{
    public abstract class DirectTagDalBase
    {
        public bool InsertDirectTag(ref int tagId, string tagName, string tagLink, string quoteFormat, int tagType)
        {
            var value = false;
            const string commandText = "CMS_DirectTag_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId, ParameterDirection.Output);
                _db.AddParameter(cmd, "TagName", tagName);
                _db.AddParameter(cmd, "TagLink", tagLink);
                _db.AddParameter(cmd, "QuoteFormat", quoteFormat);
                _db.AddParameter(cmd, "TagType", tagType);

                cmd.ExecuteNonQuery();

                tagId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                value = true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return value;
        }

        public bool UpdateDirectTag(int tagId, string tagName, string tagLink, string quoteFormat, int type)
        {
            var value = false;
            const string commandText = "CMS_DirectTag_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "TagName", tagName);
                _db.AddParameter(cmd, "TagLink", tagLink);
                _db.AddParameter(cmd, "QuoteFormat", quoteFormat);
                _db.AddParameter(cmd, "Type", type);

                _db.ExecuteNonQuery(cmd);

                value = true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return value;
        }

        public bool DeleteDirectTag(string tagIds)
        {
            var value = false;
            const string commandText = "CMS_DirectTag_DeleteTags";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagIds", tagIds);

                _db.ExecuteNonQuery(cmd);

                value = true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return value;
        }

        public bool InsertDirectTagNewsMulti(string tagIds, string newsIds)
        {
            var value = false;
            const string commandText = "CMS_DirectTagNews_InsertMulti";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagIds", tagIds);
                _db.AddParameter(cmd, "NewsIds", newsIds);

                _db.ExecuteNonQuery(cmd);

                value = true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return value;
        }

        public bool InsertDirectTagNewsMultiByType(string tagIds, string newsIds, int type)
        {
            var value = false;
            const string commandText = "CMS_DirectTagNews_InsertMulti_ByType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagIds", tagIds);
                _db.AddParameter(cmd, "NewsIds", newsIds);
                _db.AddParameter(cmd, "Type", type);
                _db.ExecuteNonQuery(cmd);

                value = true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
            return value;
        }

        public bool DeleteDirectTagNewsFromNews(int tagId, Int64 newsId, int type)
        {
            const string commandText = "CMS_DirectTagNews_DeleteTagFromNews";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "TagId", tagId);
                _db.AddParameter(cmd, "Type", type);

                _db.ExecuteNonQuery(cmd);

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));

            }
            return false;
        }

        public List<DirectTagEntity> SelectDirectTagByNewsId(Int64 newsId)
        {
            const string commandText = "CMS_DirectTagNews_SelectByNewsId";
            try
            {
                List<DirectTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);

                data = _db.GetList<DirectTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<DirectTagEntity> SelectAll(int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_DirectTag_SelectAll";
            try
            {
                List<DirectTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<DirectTagEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<DirectTagEntity> SelectAllByType(int pageIndex, int pageSize, int type, ref int totalRow)
        {
            const string commandText = "CMS_DirectTag_SelectAll_ByType";
            try
            {
                List<DirectTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<DirectTagEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<DirectTagEntity> SearchDirectTag(string keyword, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_DirectTag_Search";
            try
            {
                List<DirectTagEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "KeyWord", keyword);
                _db.AddParameter(cmd, "Type", type);
                data = _db.GetList<DirectTagEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public DirectTagEntity IsTagAvailable(string tagName)
        {
            const string commandText = "CMS_DirectTag_IsTagAvailable";
            try
            {
                DirectTagEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TagName", tagName);
                data = _db.Get<DirectTagEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForSearchEntity> SearchNewsPublish(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_Search";
            try
            {
                List<NewsPublishForSearchEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsPublishForSearchEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsPublishForSearchEntity> SearchNewsPublish_DirectTag_LessThan_3Tag(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_NewsPublish_DirectTagNews_Search_LessThan_3Tag";
            try
            {
                List<NewsPublishForSearchEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Username", username);
                _db.AddParameter(cmd, "ZoneIds", zoneIds);
                if (fromDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DateFrom", fromDate);
                if (toDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DateTo", toDate);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsPublishForSearchEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected DirectTagDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }    
}
