﻿
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.StoreProcedures
{
    public partial class Kenh14OldVersionDbStoreProcedures
    {
        // Instance fields
        private readonly Kenh14OldVersionDb _db;
        
		/// <summary>
        /// Initializes a new instance of the <see cref="Kenh14OldVersionDbStoreProcedures"/> 
        /// class with the specified <see cref="Kenh14OldVersionDb"/>.
		/// </summary>
        /// <param name="db">The <see cref="Kenh14OldVersionDb"/> object.</param>
        public Kenh14OldVersionDbStoreProcedures(Kenh14OldVersionDb db)
		{
			_db = db;
		}

        /// <summary>
        /// Gets the database object that this table belongs to.
        ///	</summary>
        ///	<value>The <see cref="Kenh14OldVersionDb"/> object.</value>
        protected Kenh14OldVersionDb Database
        {
            get { return _db; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        protected int ExecuteNoneQuery(IDbCommand command)
        {
            return _db.ExecuteNonQuery(command);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected int ExecuteNoneQuery(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            return _db.ExecuteNoneQuery(commandType, commandText, parameters);
        }
        
        /// <summary>
        /// Mapping data reader to entity collection
        ///	</summary>
        /// <param name="command">The IDb command.</param>
        ///	<value>The entity list.</value>
        protected List<T> GetList<T>(IDbCommand command)
        {
            return _db.GetList<T>(command);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected List<T> GetList<T>(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            return _db.GetList<T>(commandType, commandText, parameters);
        }

        /// <summary>
        /// Mapping data reader to entity collection
        ///	</summary>
        /// <param name="command">The IDb command.</param>
        ///	<value>The entity list.</value>
        protected T Get<T>(IDbCommand command)
        {
            return _db.Get<T>(command);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected T Get<T>(CommandType commandType, string commandText, params SqlParameter[] parameters)
        {
            return _db.Get<T>(commandType, commandText, parameters);
        }
    }
}
