﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.External.Kenh14.BoxTVShow;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.Kenh14
{
    public abstract class BoxTVShowDalBase
    {

        public bool Update(BoxTVShowEntity boxTVShowEntity)
        {
            const string commandText = "CMS_BoxTVShow_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", boxTVShowEntity.Id);
                _db.AddParameter(cmd, "Title", boxTVShowEntity.Title);
                _db.AddParameter(cmd, "DisplayTime", boxTVShowEntity.DisplayTime);
                _db.AddParameter(cmd, "Icon", boxTVShowEntity.Icon);
                _db.AddParameter(cmd, "Note", boxTVShowEntity.Note);
                _db.AddParameter(cmd, "Priority", boxTVShowEntity.Priority);
                _db.AddParameter(cmd, "Status", boxTVShowEntity.Status);
                _db.AddParameter(cmd, "CreatedBy", boxTVShowEntity.CreatedBy);
                _db.AddParameter(cmd, "ModifiedBy", boxTVShowEntity.ModifiedBy);
                _db.AddParameter(cmd, "TagId", boxTVShowEntity.TagId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Delete(int id)
        {
            const string commandText = "CMS_BoxTVShow_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public BoxTVShowEntity GetById(int id)
        {
            const string commandText = "CMS_BoxTvShow_GetById";
            try
            {
                BoxTVShowEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<BoxTVShowEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BoxTVShowEntity> GetList()
        {
            const string commandText = "CMS_BoxTvShow_GetListOfAll";
            try
            {
                List<BoxTVShowEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<BoxTVShowEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected BoxTVShowDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }


}
