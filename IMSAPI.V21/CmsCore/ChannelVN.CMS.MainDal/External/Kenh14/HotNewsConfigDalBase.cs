﻿using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.Kenh14
{
    public abstract class HotNewsConfigDalBase
    {

        public bool Update(HotNewsConfigEntity hotNewsConfigEntity)
        {
            const string commandText = "CMS_HotNewsConfig_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", hotNewsConfigEntity.ZoneId);
                _db.AddParameter(cmd, "MinViewCount", hotNewsConfigEntity.MinViewCount);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }
        public bool Delete(int zoneId)
        {
            const string commandText = "CMS_HotNewsConfig_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                //_db.AddParameter(cmd, "ZoneId", zoneId);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }

        }

        public List<HotNewsConfigEntity> GetList()
        {
            const string commandText = "CMS_HotNewsConfig_GetList";
            try
            {
                List<HotNewsConfigEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                data = _db.GetList<HotNewsConfigEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected HotNewsConfigDalBase(CmsMainDb db)
		{
			_db = db;
		}

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }


}
