﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.MainDal.External.Kenh14
{
    public abstract class StatisticV3DalBase : Base.Statistic.StatisticV3Dal
    {
        public List<StatisticV3ViewCountEntity> PageViewAll(Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_K14_ViewCount_ByDay";
            try
            {
                List<StatisticV3ViewCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3ViewCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StatisticV3CategoryEntity> CategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_K14_CategoryDaily_Day1";
            try
            {
                List<StatisticV3CategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CateId", cateId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3CategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StatisticV3ViewCountEntity> PageViewAllMobile(Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_MobileK14_ViewCount_By_Day";
            try
            {
                List<StatisticV3ViewCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3ViewCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StatisticV3CategoryEntity> CategoryViewCountMobile(string cateId, Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_MobileK14_CategoryDaily_By_Day";
            try
            {
                List<StatisticV3CategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CateId", cateId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3CategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected StatisticV3DalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
