﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.NewsGallery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeBiz
{
    public class NewsGalleryDal : NewsGalleryDalBase
    {
        internal NewsGalleryDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
