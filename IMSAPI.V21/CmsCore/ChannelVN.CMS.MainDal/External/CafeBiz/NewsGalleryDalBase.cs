﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.External.CafeBiz.NewsGallery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeBiz
{
    public abstract class NewsGalleryDalBase
    {
        public bool SaveNewsGallery(NewsGalleryEntity info, ref int id)
        {
            const string commandText = "CMS_NewsGallery_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SlideId", id, ParameterDirection.Output);
                _db.AddParameter(cmd, "SlideTitle", info.SlideTitle);
                _db.AddParameter(cmd, "SlideOrder", info.SlideOrder);
                _db.AddParameter(cmd, "SlideDesc", info.SlideDesc);
                _db.AddParameter(cmd, "SlideImage", info.SlideImage);
                _db.AddParameter(cmd, "SlideVideo", info.SlideVideo);
                _db.AddParameter(cmd, "SlideStatus", info.SlideStatus);
                _db.AddParameter(cmd, "NewsId", info.NewsId);
                _db.AddParameter(cmd, "FullImage", info.FullImage);
                _db.AddParameter(cmd, "NoImage", info.NoImage);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                id = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNewsGallery(NewsGalleryEntity info)
        {
            const string commandText = "CMS_NewsGallery_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SlideTitle", info.SlideTitle);
                _db.AddParameter(cmd, "SlideOrder", info.SlideOrder);
                _db.AddParameter(cmd, "SlideDesc", info.SlideDesc);
                _db.AddParameter(cmd, "SlideImage", info.SlideImage);
                _db.AddParameter(cmd, "SlideVideo", info.SlideVideo);
                _db.AddParameter(cmd, "SlideStatus", info.SlideStatus);
                _db.AddParameter(cmd, "FullImage", info.FullImage);
                _db.AddParameter(cmd, "NoImage", info.NoImage);
                _db.AddParameter(cmd, "SlideId", info.SlideId);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteNewsGallery(int id)
        {
            const string commandText = "CMS_NewsGallery_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SlideId", id);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public NewsGalleryEntity SelectNewsGalleryById(int id)
        {
            const string commandText = "CMS_NewsGallery_SelectDataById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "SlideId", id);

                NewsGalleryEntity data = _db.Get<NewsGalleryEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId)
        {
            const string commandText = "CMS_NewsGallery_SelectDataByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);

                List<NewsGalleryEntity> data = _db.GetList<NewsGalleryEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<NewsGalleryEntity> SelectNewsGalleryByNewsId(long newsId, int pageIndex, int pageSize, ref int totalRows)
        {
            const string commandText = "CMS_NewsGallery_ListDataByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "totalRows", totalRows, ParameterDirection.Output);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "pageIndex", pageIndex);
                _db.AddParameter(cmd, "pageSize", pageSize);

                List<NewsGalleryEntity> data = _db.GetList<NewsGalleryEntity>(cmd);
                totalRows = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsGalleryDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
