﻿using System;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeBiz
{
    public abstract class NewsPositionDalBase : Base.NewsPosition.NewsPositionDalBase
    {
        public bool SaveLinkPosition(int type, int position, int zoneId, string title, string avatar, string url)
        {
            const string commandText = "CMS_NewsPosition_UpdateLinkPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", type);
                _db.AddParameter(cmd, "Position", position);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Title", title);
                _db.AddParameter(cmd, "Avatar", avatar);
                _db.AddParameter(cmd, "Url", url);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPositionDalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
