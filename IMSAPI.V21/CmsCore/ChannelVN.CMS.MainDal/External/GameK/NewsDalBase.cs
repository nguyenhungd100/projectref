﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.GameK
{
    public abstract class NewsDalBase : Base.News.NewsDalBase
    {
        public bool InsertNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList,
                                     string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_News_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "CreatedBy", news.CreatedBy);
                _db.AddParameter(cmd, "EditedBy", news.EditedBy);
                _db.AddParameter(cmd, "PublishedBy", news.PublishedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);
                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "RollingNewsId", news.RollingNewsId);
                _db.AddParameter(cmd, "InterviewId", news.InterviewId);

                //quangnv added on 07/01/2013
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);

                //fox added on 14/05/2014
                _db.AddParameter(cmd, "LocationType", news.LocationType);

                //thanhtn added on 03/06/2014
                if (news.ExpiredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredDate", DBNull.Value);
                else _db.AddParameter(cmd, "ExpiredDate", news.ExpiredDate);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);
                //fox added 4/9/2014
                _db.AddParameter(cmd, "BonusPrice", news.BonusPrice);
                //ngocnh 17/9/2014
                _db.AddParameter(cmd, "NewsRelationSpecialIdList", newsRelationSpecialId);
                //thanhtn 29/07/2015
                _db.AddParameter(cmd, "LastReceiver", news.LastReceiver);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateNews(NewsEntity news, int zoneId, string zoneIdList, string tagIdList, string tagIdListForPrimary, string newsRelationIdList, string newsRelationSpecialId, string listOfAuthorId, string listOfAuthorName, string listOfAuthorNote, int sourceId)
        {
            const string commandText = "CMS_News_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", news.Id);
                _db.AddParameter(cmd, "Title", news.Title);
                _db.AddParameter(cmd, "SubTitle", news.SubTitle);
                _db.AddParameter(cmd, "Sapo", news.Sapo);
                _db.AddParameter(cmd, "Body", news.Body);
                _db.AddParameter(cmd, "Avatar", news.Avatar);
                _db.AddParameter(cmd, "AvatarDesc", news.AvatarDesc);
                _db.AddParameter(cmd, "Avatar2", news.Avatar2);
                _db.AddParameter(cmd, "Avatar3", news.Avatar3);
                _db.AddParameter(cmd, "Avatar4", news.Avatar4);
                _db.AddParameter(cmd, "Avatar5", news.Avatar5);
                _db.AddParameter(cmd, "Author", news.Author);
                _db.AddParameter(cmd, "NewsRelation", news.NewsRelation);
                _db.AddParameter(cmd, "Status", news.Status);
                _db.AddParameter(cmd, "Source", news.Source);
                _db.AddParameter(cmd, "IsFocus", news.IsFocus);
                _db.AddParameter(cmd, "Type", news.Type);
                _db.AddParameter(cmd, "ThreadId", news.ThreadId);
                if (news.DistributionDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributionDate", DBNull.Value);
                else _db.AddParameter(cmd, "DistributionDate", news.DistributionDate);
                _db.AddParameter(cmd, "LastModifiedBy", news.LastModifiedBy);
                _db.AddParameter(cmd, "WordCount", news.WordCount);
                _db.AddParameter(cmd, "ViewCount", news.ViewCount);
                _db.AddParameter(cmd, "Priority", news.Priority);
                _db.AddParameter(cmd, "Tag", news.Tag);
                _db.AddParameter(cmd, "Note", news.Note);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ZoneIdList", zoneIdList);
                _db.AddParameter(cmd, "TagIdList", tagIdList);
                _db.AddParameter(cmd, "TagIdListForPrimary", tagIdListForPrimary);
                _db.AddParameter(cmd, "NewsRelationIdList", newsRelationIdList);
                _db.AddParameter(cmd, "TagPrimary", news.TagPrimary);
                _db.AddParameter(cmd, "Price", news.Price);
                _db.AddParameter(cmd, "IsOnHome", news.IsOnHome);
                _db.AddParameter(cmd, "OriginalId", news.OriginalId);
                _db.AddParameter(cmd, "NewsType", news.NewsType);

                // Extension fields
                _db.AddParameter(cmd, "DisplayStyle", news.DisplayStyle);
                _db.AddParameter(cmd, "DisplayPosition", news.DisplayPosition);
                _db.AddParameter(cmd, "DisplayInSlide", news.DisplayInSlide);
                _db.AddParameter(cmd, "AvatarCustom", news.AvatarCustom);
                // INSERT INTO NewsByAuthor
                _db.AddParameter(cmd, "AuthorNameList", listOfAuthorName);
                _db.AddParameter(cmd, "AuthorIdList", listOfAuthorId);
                _db.AddParameter(cmd, "AuthorNoteList", listOfAuthorNote);
                // thanhtn add (2012-12-13)
                _db.AddParameter(cmd, "TagItem", news.TagItem);
                _db.AddParameter(cmd, "Url", news.Url);
                _db.AddParameter(cmd, "NoteRoyalties", news.NoteRoyalties);
                _db.AddParameter(cmd, "NewsCategory", news.NewsCategory);
                _db.AddParameter(cmd, "InitSapo", news.InitSapo);
                _db.AddParameter(cmd, "SourceId", sourceId);
                // thanhtn add (2013-09-17)
                _db.AddParameter(cmd, "TemplateName", news.TemplateName);
                _db.AddParameter(cmd, "TemplateConfig", news.TemplateConfig);
                // thanhtn add (2013-11-25)
                _db.AddParameter(cmd, "IsPr", news.IsPr);
                _db.AddParameter(cmd, "AdStore", news.AdStore);
                _db.AddParameter(cmd, "AdStoreUrl", news.AdStoreUrl);
                _db.AddParameter(cmd, "PrBookingNumber", news.PrBookingNumber);
                _db.AddParameter(cmd, "IsBreakingNews", news.IsBreakingNews);
                _db.AddParameter(cmd, "PegaBreakingNews", news.PegaBreakingNews);
                _db.AddParameter(cmd, "IsOnMobile", news.IsOnMobile);
                _db.AddParameter(cmd, "RollingNewsId", news.RollingNewsId);
                _db.AddParameter(cmd, "InterviewId", news.InterviewId);

                //quangnv added on 07/01/2013
                _db.AddParameter(cmd, "TagSubTitleId", news.TagSubTitleId);

                //fox added on 14/05/2014
                _db.AddParameter(cmd, "LocationType", news.LocationType);

                //thanhtn added on 03/06/2014
                if (news.ExpiredDate <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "ExpiredDate", DBNull.Value);
                else _db.AddParameter(cmd, "ExpiredDate", news.ExpiredDate);
                _db.AddParameter(cmd, "SourceURL", news.SourceURL);
                //fox added 4/9/2014
                _db.AddParameter(cmd, "BonusPrice", news.BonusPrice);
                //ngocnh 17/9/2014
                _db.AddParameter(cmd, "NewsRelationSpecialIdList", newsRelationSpecialId);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
