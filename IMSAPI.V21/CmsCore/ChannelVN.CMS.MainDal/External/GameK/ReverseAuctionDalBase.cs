﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.External.GameK.Auction;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.GameK
{
    public abstract class ReverseAuctionDalBase
    {
        public bool AddNewAuction(ReverseAuctionEntity auction, ref int auctionId)
        {
            const string commandText = "CMS_ReverseAuctions_Insert";
            try
            {
                auctionId = 0;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0);
                _db.AddParameter(cmd, "NewsId", auction.NewsId);
                _db.AddParameter(cmd, "FullName", auction.FullName);
                _db.AddParameter(cmd, "Mobile", auction.Mobile);
                _db.AddParameter(cmd, "FacebookLink", auction.FacebookLink);
                _db.AddParameter(cmd, "BidPrice", auction.BidPrice);
                _db.AddParameter(cmd, "BidTime", auction.BidTime);
                _db.AddParameter(cmd, "Status", auction.Status);
                var numberOfRow = cmd.ExecuteNonQuery();

                auctionId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;// && auctionId > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateAuctionById(ReverseAuctionEntity auction)
        {
            const string commandText = "CMS_ReverseAuctions_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", auction.Id);
                _db.AddParameter(cmd, "NewsId", auction.NewsId);
                _db.AddParameter(cmd, "FullName", auction.FullName);
                _db.AddParameter(cmd, "Mobile", auction.Mobile);
                _db.AddParameter(cmd, "FacebookLink", auction.FacebookLink);
                _db.AddParameter(cmd, "BidPrice", auction.BidPrice);
                _db.AddParameter(cmd, "BidTime", auction.BidTime);
                _db.AddParameter(cmd, "Status", auction.Status);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteUserById(int id)
        {
            const string commandText = "CMS_User_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                var numberOfRow = cmd.ExecuteNonQuery();

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ReverseAuctionEntity GetById(long id)
        {
            const string commandText = "CMS_ReverseAuctions_GetById";
            try
            {
                ReverseAuctionEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                data = _db.Get<ReverseAuctionEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ReverseAuctionEntity> Search(string keyword, int status, long newsId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_ReverseAuctions_Seach";
            try
            {
                List<ReverseAuctionEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "NewsId", newsId);
                data = _db.GetList<ReverseAuctionEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly ExternalCmsDb _db;

        protected ReverseAuctionDalBase(ExternalCmsDb db)
		{
			_db = db;
		}

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
