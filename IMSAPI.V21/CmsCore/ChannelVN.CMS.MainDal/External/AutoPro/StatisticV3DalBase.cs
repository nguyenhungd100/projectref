﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;
using ChannelVN.CMS.Entity.Base.Statistic;

namespace ChannelVN.CMS.MainDal.External.AutoPro
{
    public abstract class StatisticV3DalBase : Base.Statistic.StatisticV3Dal
    {
        public List<StatisticV3ViewCountEntity> PageViewAll(Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_AutoPro_ViewCount_ByDay";
            try
            {
                List<StatisticV3ViewCountEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3ViewCountEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<StatisticV3CategoryEntity> CategoryViewCount(string cateId, Int64 fromDate, Int64 toDate)
        {
            const string commandText = "CMS_AutoPro_CategoryDaily_ByDay";
            try
            {
                List<StatisticV3CategoryEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "CateId", cateId);
                _db.AddParameter(cmd, "FromDate", fromDate);
                _db.AddParameter(cmd, "ToDate", toDate);
                data = _db.GetList<StatisticV3CategoryEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected StatisticV3DalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
