﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.CafeF.Expert;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeF
{
    public abstract class FeedBackToExpertArticleDalBase
    {
        public FeedBackToExpertArticleEntity GetById(int id)
        {
            const string commandText = "CMS_FeedBackToExpertArticle_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                FeedBackToExpertArticleEntity data = _db.Get<FeedBackToExpertArticleEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<FeedBackToExpertArticleEntity> Search(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_FeedBackToExpertArticle_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                List<FeedBackToExpertArticleEntity> data = _db.GetList<FeedBackToExpertArticleEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, cmd.Parameters.Count - 1));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatus(int id, int status)
        {
            const string commandText = "CMS_FeedBackToExpertArticle_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly ExternalCmsDb _db;

        protected FeedBackToExpertArticleDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
