﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.CafeF.BusinessNewsCrawler;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeF
{
    public abstract class BusinessNewsDalBase
    {
        public BusinessNewsCrawlerDetailEntity GetById(long id)
        {
            const string commandText = "CMS_tblDetail_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                BusinessNewsCrawlerDetailEntity data = _db.Get<BusinessNewsCrawlerDetailEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateStatus(long id, int status)
        {
            const string commandText = "CMS_tblDetail_UpdateStatus";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                _db.AddParameter(cmd, "Status", status);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BusinessNewsCrawlerSourceEntity> GetAll()
        {
            const string commandText = "CMS_tblSource_GetAllSource";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                List<BusinessNewsCrawlerSourceEntity> data = _db.GetList<BusinessNewsCrawlerSourceEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<BusinessNewsCrawlerDetailEntity> SearchNews(int source, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_tblDetail_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "SourceId", source);
                _db.AddParameter(cmd, "Status", status);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<BusinessNewsCrawlerDetailEntity> data = _db.GetList<BusinessNewsCrawlerDetailEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        //public bool Delete(int id)
        //{
        //    const string commandText = "CMS_BusinessNews_DeleteById";
        //    try
        //    {
        //        var cmd = _db.CreateCommand(commandText, true);
        //        _db.AddParameter(cmd, "Id", id);
        //        bool data = _db.ExecuteNonQuery(cmd) > 0;

        //        return data;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
        //    }
        //}

        #region Core members

        private readonly CafeFBusinessNewsCrawlerDb _db;

        protected BusinessNewsDalBase(CafeFBusinessNewsCrawlerDb db)
        {
            _db = db;
        }

        protected CafeFBusinessNewsCrawlerDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
