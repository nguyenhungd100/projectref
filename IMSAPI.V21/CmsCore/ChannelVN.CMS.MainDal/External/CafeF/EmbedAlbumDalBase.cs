﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.CafeF
{
    public abstract class EmbedAlbumDalBase
    {
        public bool Update(EmbedAlbumEntity embedAlbum)
        {
            const string commandText = "CMS_EmbedAlbum_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbum.Id);
                _db.AddParameter(cmd, "Type", embedAlbum.Type);
                _db.AddParameter(cmd, "ZoneId", embedAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", embedAlbum.Name);
                _db.AddParameter(cmd, "Avatar", embedAlbum.Avatar);
                _db.AddParameter(cmd, "LastModifiedBy", embedAlbum.LastModifiedBy);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);
                _db.AddParameter(cmd, "StartDate", embedAlbum.StartDate);
                _db.AddParameter(cmd, "ExpireDate", embedAlbum.ExpireDate);

                var numberOfRow = cmd.ExecuteNonQuery();
                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Insert(EmbedAlbumEntity embedAlbum, ref int newEmbedAlbumId)
        {
            const string commandText = "CMS_EmbedAlbum_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", embedAlbum.Id, ParameterDirection.Output);
                _db.AddParameter(cmd, "Type", embedAlbum.Type);
                _db.AddParameter(cmd, "ZoneId", embedAlbum.ZoneId);
                _db.AddParameter(cmd, "Name", embedAlbum.Name);
                _db.AddParameter(cmd, "Avatar", embedAlbum.Avatar);
                _db.AddParameter(cmd, "CreatedBy", embedAlbum.CreatedBy);
                _db.AddParameter(cmd, "Status", embedAlbum.Status);
                _db.AddParameter(cmd, "StartDate", embedAlbum.StartDate);
                _db.AddParameter(cmd, "ExpireDate", embedAlbum.ExpireDate);

                var numberOfRow = cmd.ExecuteNonQuery();

                newEmbedAlbumId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return numberOfRow > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected EmbedAlbumDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
