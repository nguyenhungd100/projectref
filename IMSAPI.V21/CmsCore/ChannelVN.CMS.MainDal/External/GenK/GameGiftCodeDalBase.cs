﻿using System;
using ChannelVN.CMS.Entity.External.GenK.GiftCode;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.GenK
{
    public abstract class GameGiftCodeDalBase
    {

        #region Constructor

        private readonly ExternalCmsDb _db;

        protected GameGiftCodeDalBase(ExternalCmsDb db)
        {
            _db = db;
        }

        protected ExternalCmsDb Database
        {
            get { return _db; }
        }

        #endregion

        public bool InsertGiftCode(long newsId, string giftCode, string tags)
        {

            const string commandText = "CMS_GiftCode_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "GiftCode", giftCode);
                _db.AddParameter(cmd, "Tags", tags);

                var data = cmd.ExecuteNonQuery();
                return data > 0;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public GameGiftCodeEntity GetGiftCodeByNewsId(long newsId)
        {

            const string commandText = "CMS_GiftCode_GetCodeByNewsId";
            try
            {
                GameGiftCodeEntity data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", newsId);

                data = _db.Get<GameGiftCodeEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
    }
}
