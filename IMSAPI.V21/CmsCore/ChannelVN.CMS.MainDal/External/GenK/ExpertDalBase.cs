﻿using System;
using System.Collections.Generic;
using System.Data;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.GenK.Expert;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.GenK
{
    public abstract class ExpertDalBase
    {
        public bool Insert(ExpertEntity expertEntity, ref int expertId)
        {
            const string commandText = "CMS_Expert_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", 0, ParameterDirection.Output);
                _db.AddParameter(cmd, "ExpertName", expertEntity.ExpertName);
                _db.AddParameter(cmd, "JobTitle", expertEntity.JobTitle);
                _db.AddParameter(cmd, "Description", expertEntity.Description);
                _db.AddParameter(cmd, "Avatar", expertEntity.Avatar);
                _db.AddParameter(cmd, "DisplayJobTitle", expertEntity.DisplayJobTitle);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                _db.AddParameter(cmd, "AcceptFeedback", expertEntity.AcceptFeedback);
                _db.AddParameter(cmd, "FacebookLink", expertEntity.FacebookLink);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                expertId = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ExpertEntity GetById(int id)
        {
            const string commandText = "CMS_Expert_GetById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                ExpertEntity data = _db.Get<ExpertEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Update(ExpertEntity expertEntity)
        {
            const string commandText = "CMS_Expert_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", expertEntity.Id);
                _db.AddParameter(cmd, "ExpertName", expertEntity.ExpertName);
                _db.AddParameter(cmd, "JobTitle", expertEntity.JobTitle);
                _db.AddParameter(cmd, "Description", expertEntity.Description);
                _db.AddParameter(cmd, "Avatar", expertEntity.Avatar);
                _db.AddParameter(cmd, "DisplayJobTitle", expertEntity.DisplayJobTitle);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                _db.AddParameter(cmd, "AcceptFeedback", expertEntity.AcceptFeedback);
                _db.AddParameter(cmd, "FacebookLink", expertEntity.FacebookLink);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<ExpertEntity> Search(string keyword)
        {
            const string commandText = "CMS_Expert_Search";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Keyword", keyword);
                List<ExpertEntity> data = _db.GetList<ExpertEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool Delete(int id)
        {
            const string commandText = "CMS_Expert_DeleteById";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public ExpertEntity GetExpertByNewsId(long id)
        {
            const string commandText = "CMS_ExpertInNews_GetByNewsId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "Id", id);
                ExpertEntity data = _db.Get<ExpertEntity>(cmd);

                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool InsertExpertToNews(ExpertInNews expertEntity)
        {
            const string commandText = "CMS_ExpertInNews_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ExpertId", expertEntity.ExpertId);
                _db.AddParameter(cmd, "NewsId", expertEntity.NewsId);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateExpertToNews(ExpertInNews expertEntity)
        {
            const string commandText = "CMS_ExpertInNews_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ExpertId", expertEntity.ExpertId);
                _db.AddParameter(cmd, "NewsId", expertEntity.NewsId);
                _db.AddParameter(cmd, "Quote", expertEntity.Quote);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool DeleteExpertInNews(long id)
        {
            const string commandText = "CMS_ExpertInNews_Delete";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsId", id);
                bool data = _db.ExecuteNonQuery(cmd) > 0;

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchExpertNewsWhichPublished(int zoneId, string keyword, int type, int displayPosition, DateTime distributedDateFrom, DateTime distributedDateTo, string excludeNewsIds, int newstype, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchExpertNewsWhichPublished";
            try
            {
                List<NewsInListEntity> data = null;
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "Type", type);
                _db.AddParameter(cmd, "NewsType", newstype);
                _db.AddParameter(cmd, "DisplayPosition", displayPosition);
                if (distributedDateFrom <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateFrom", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateFrom", distributedDateFrom);
                if (distributedDateTo <= Constants.MinDateTime)
                    _db.AddParameter(cmd, "DistributedDateTo", DBNull.Value);
                else _db.AddParameter(cmd, "DistributedDateTo", distributedDateTo);
                _db.AddParameter(cmd, "ExcludeNewsIds", excludeNewsIds);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                data = _db.GetList<NewsInListEntity>(cmd);

                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));

                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected ExpertDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
