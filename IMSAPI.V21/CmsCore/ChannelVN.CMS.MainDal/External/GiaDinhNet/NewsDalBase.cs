﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.GiaDinhNet
{
    public abstract class NewsDalBase : Base.News.NewsDal
    {
        //Magazine News
        public List<NewsEntity> SearchNewsByMagazineIdWithPaging(int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_Magazine_GetNewsByMagazineIdWithPaging";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "MagazineId", magazineId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsEntity> data = _db.GetList<NewsEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public List<NewsInListEntity> SearchNewsPublishExcludeNewsInMagazine(int zoneId, string keyword, int magazineId, int pageIndex, int pageSize, ref int totalRow)
        {
            const string commandText = "CMS_News_SearchNewsPublishExcludeNewsInMagazine";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TotalRow", totalRow, ParameterDirection.Output);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Keyword", keyword);
                _db.AddParameter(cmd, "MagazineId", magazineId);
                _db.AddParameter(cmd, "PageIndex", pageIndex);
                _db.AddParameter(cmd, "PageSize", pageSize);
                List<NewsInListEntity> data = _db.GetList<NewsInListEntity>(cmd);
                totalRow = Utility.ConvertToInt(_db.GetParameterValueFromCommand(cmd, 0));
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsDalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
