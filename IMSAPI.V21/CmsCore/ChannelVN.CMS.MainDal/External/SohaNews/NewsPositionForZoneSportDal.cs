﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.SohaNews
{
    public class NewsPositionForZoneSportDal : NewsPositionForZoneSportDalBase
    {
        internal NewsPositionForZoneSportDal(CmsMainDb db)
            : base(db)
        {
        }
    }
}
