﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.Entity.External.SohaNews.NewsPosition;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.SohaNews
{
    public abstract class NewsPositionForZoneSportDalBase
    {
        public List<NewsPositionForZoneSportEntity> GetListByTypeAndZoneId(int typeId, int zoneId, string listOfOrder = "")
        {
            const string commandText = "CMS_NewsPositionForZoneSport_GetByTypeAndZoneId";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "ListOfOrder", listOfOrder);

                List<NewsPositionForZoneSportEntity> data = _db.GetList<NewsPositionForZoneSportEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool SaveNewsPosition(int typeId, int zoneId, int order, long newsId, DateTime expiredLock, string avatar, int avatarIndex, bool checkNewsExists = true)
        {
            const string commandText = "CMS_NewsPositionForZoneSport_Update";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsId", newsId);
                _db.AddParameter(cmd, "ExpiredLock", expiredLock);
                _db.AddParameter(cmd, "NewAvatar", avatar);
                _db.AddParameter(cmd, "AvatarIndex", avatarIndex);
                _db.AddParameter(cmd, "CheckNewsExists", checkNewsExists);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateForBomd(int typeId, int zoneId, int order, long newsId)
        {
            const string commandText = "CMS_NewsPositionForZoneSport_UpdateForBomd";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Order", order);
                _db.AddParameter(cmd, "NewsId", newsId);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        public bool UpdateForUnlockedPosition(int typeId, int zoneId)
        {
            const string commandText = "CMS_NewsPositionForZoneSport_UpdateForUnlockedPosition";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "TypeId", typeId);
                _db.AddParameter(cmd, "ZoneId", zoneId);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }


        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPositionForZoneSportDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
