﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.External.NguoiLaoDong.ExternalVideoEmbed;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.NguoiLaoDong
{
    public abstract class ExternalVideoEmbedDalBase
    {
        public bool Insert(ExternalVideoEmbedEntity externalVideoEmbed)
        {
            const string commandText = "CMS_ExternalVideoEmbed_Insert";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", externalVideoEmbed.ZoneId);
                _db.AddParameter(cmd, "Title", externalVideoEmbed.Title);
                _db.AddParameter(cmd, "Avatar", externalVideoEmbed.Avatar);
                _db.AddParameter(cmd, "Url", externalVideoEmbed.Url);
                _db.AddParameter(cmd, "Sapo", externalVideoEmbed.Sapo);
                _db.AddParameter(cmd, "SortOrder", externalVideoEmbed.SortOrder);
                _db.AddParameter(cmd, "Type", externalVideoEmbed.Type);
                _db.AddParameter(cmd, "KeyVideo", externalVideoEmbed.KeyVideo);
                _db.AddParameter(cmd, "DistributionDate", externalVideoEmbed.DistributionDate);
                _db.AddParameter(cmd, "FileName", externalVideoEmbed.FileName);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public bool Delete(int zoneId, int type)
        {
            const string commandText = "CMS_ExternalVideoEmbed_DeleteByZoneType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        public List<ExternalVideoEmbedEntity> GetListByZoneType(int zoneId, int type)
        {
            const string commandText = "CMS_ExternalVideoEmbed_GetListByZoneType";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "ZoneId", zoneId);
                _db.AddParameter(cmd, "Type", type);
                List<ExternalVideoEmbedEntity> data = _db.GetList<ExternalVideoEmbedEntity>(cmd);
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }
        #region Core members

        private readonly CmsMainDb _db;

        protected ExternalVideoEmbedDalBase(CmsMainDb db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
