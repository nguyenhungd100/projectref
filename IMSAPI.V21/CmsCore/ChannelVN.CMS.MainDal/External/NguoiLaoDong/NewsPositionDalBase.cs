﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.MainDal.Common;
using System.Data;
using ChannelVN.CMS.MainDal.Databases;

namespace ChannelVN.CMS.MainDal.External.NguoiLaoDong
{
    public abstract class NewsPositionDalBase : Base.NewsPosition.NewsPositionDalBase
    {
        public bool RemoveNewsDisplayInSlide(string newsIdList)
        {
            const string commandText = "CMS_NewsPosition_RemoveNewsDisplayInSlide";
            try
            {
                var cmd = _db.CreateCommand(commandText, true);
                _db.AddParameter(cmd, "NewsIds", newsIdList);

                bool data = _db.ExecuteNonQuery(cmd) > 0;
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}:{1}", commandText, ex.Message));
            }
        }

        #region Core members

        private readonly CmsMainDb _db;

        protected NewsPositionDalBase(CmsMainDb db)
            : base(db)
        {
            _db = db;
        }

        protected CmsMainDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
