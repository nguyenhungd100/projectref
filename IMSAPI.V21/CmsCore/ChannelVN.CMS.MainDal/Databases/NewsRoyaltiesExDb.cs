﻿using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;
using System.Data;
using System.Data.SqlClient;

namespace ChannelVN.CMS.MainDal.Databases
{
    public class NewsRoyaltiesExDb: NewsRoyaltiesExDbBase
    {
        private const string ConnectionStringName = "NewsRoyaltiesExDb";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            return new SqlConnection(strConn);
        }
    }
}
