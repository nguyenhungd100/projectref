﻿using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.Statistic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.MainDal.Databases
{
    public abstract class CmsPageViewStatisticDbBase : MainDbBase
    {

        private PageViewStatisticDal _pageViewStatisticDal;
        public PageViewStatisticDal NewsSohaMainDal
        {
            get { return _pageViewStatisticDal ?? (_pageViewStatisticDal = new PageViewStatisticDal((CmsPageViewStatisticDb)this)); }
        }

        #region Protected members
        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();
        #endregion
    }
}
