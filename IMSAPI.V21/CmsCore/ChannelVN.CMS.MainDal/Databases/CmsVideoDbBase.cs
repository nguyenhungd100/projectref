﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.Video;
using ChannelVN.CMS.MainDal.Base.News;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsVideoDb"/> class that 
    /// represents a connection to the <c>CmsVideoDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsVideoDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsVideoDbBase : MainDbBase
    {
        #region Store procedures
        #region Interview Question
        private NewsUseVideoDal _newsUseVideoMainDal;
        public NewsUseVideoDal NewsUseVideoMainDal
        {
            get { return _newsUseVideoMainDal ?? (_newsUseVideoMainDal = new NewsUseVideoDal((CmsVideoDb)this)); }
        }
        #region Playlist
        private PlaylistDal _playlistMainDal;
        public PlaylistDal PlaylistMainDal
        {
            get { return _playlistMainDal ?? (_playlistMainDal = new PlaylistDal((CmsVideoDb)this)); }
        }
        #endregion
        #endregion

        #region Video
        private VideoDal _videoMainDal;
        public VideoDal VideoMainDal
        {
            get { return _videoMainDal ?? (_videoMainDal = new VideoDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoFolderDal
        private VideoFolderDal _videoFolderMainDal;
        public VideoFolderDal VideoFolderMainDal
        {
            get { return _videoFolderMainDal ?? (_videoFolderMainDal = new VideoFolderDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoFromYoutube
        private VideoFromYoutubeDal _videoFromYoutubeMainDal;
        public VideoFromYoutubeDal VideoFromYoutubeMainDal
        {
            get { return _videoFromYoutubeMainDal ?? (_videoFromYoutubeMainDal = new VideoFromYoutubeDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoThread
        private VideoThreadDal _videoThreadMainDal;
        public VideoThreadDal VideoThreadMainDal
        {
            get { return _videoThreadMainDal ?? (_videoThreadMainDal = new VideoThreadDal((CmsVideoDb)this)); }
        }
        #endregion

        #region ZoneVideo
        private ZoneVideoDal _zoneVideoMainDal;
        public ZoneVideoDal ZoneVideoMainDal
        {
            get { return _zoneVideoMainDal ?? (_zoneVideoMainDal = new ZoneVideoDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoInNews
        private VideoInNewsDal _videoInNewsDal;
        public VideoInNewsDal VideoInNewsMainDal
        {
            get { return _videoInNewsDal ?? (_videoInNewsDal = new VideoInNewsDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoTag
        private VideoTagDal _videoTagMainDal;
        public VideoTagDal VideoTagMainDal
        {
            get { return _videoTagMainDal ?? (_videoTagMainDal = new VideoTagDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoEPL
        private VideoEPLDal _videoEPLMainDal;
        public VideoEPLDal VideoEPLMainDal
        {
            get { return _videoEPLMainDal ?? (_videoEPLMainDal = new VideoEPLDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoChannel
        private VideoChannelDal _videoChannelMainDal;
        public VideoChannelDal VideoChannelMainDal
        {
            get { return _videoChannelMainDal ?? (_videoChannelMainDal = new VideoChannelDal((CmsVideoDb)this)); }
        }
        #endregion

        #region VideoLabel
        private VideoLabelDal _videoLabelideoMainDal;
        public VideoLabelDal VideoLabelMainDal
        {
            get { return _videoLabelideoMainDal ?? (_videoLabelideoMainDal = new VideoLabelDal((CmsVideoDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsVideoDbBase()
        {
        }
        protected CmsVideoDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}