﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.MainDal.Databases
{
    public class SlaveCmsDb : SlaveCmsDbBase
    {        
        public SlaveCmsDb(string connectString)
            :base(connectString)
        {          
              
        }
        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection(string conn)
        {            
            return new SqlConnection(conn);
        }
    }
}