﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.Photo;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsPhotoDb"/> class that 
    /// represents a connection to the <c>CmsPhotoDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsPhotoDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsPhotoDbBase : MainDbBase
    {
        #region Store procedures
        #region PhotoTagDal
        private PhotoTagDal _photoTagMainDal;
        public PhotoTagDal PhotoTagMainDal
        {
            get { return _photoTagMainDal ?? (_photoTagMainDal = new PhotoTagDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region PhotoPublishedDal
        private PhotoPublishedDal _photoPublishedMainDal;
        public PhotoPublishedDal PhotoPublishedMainDal
        {
            get { return _photoPublishedMainDal ?? (_photoPublishedMainDal = new PhotoPublishedDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region PhotoLabelDal
        private PhotoLabelDal _photoLabelMainDal;
        public PhotoLabelDal PhotoLabelMainDal
        {
            get { return _photoLabelMainDal ?? (_photoLabelMainDal = new PhotoLabelDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region PhotoFolderDal
        private PhotoFolderDal _photoFolderMainDal;
        public PhotoFolderDal PhotoFolderMainDal
        {
            get { return _photoFolderMainDal ?? (_photoFolderMainDal = new PhotoFolderDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region PhotoDal
        private PhotoDal _photoMainDal;
        public PhotoDal PhotoMainDal
        {
            get { return _photoMainDal ?? (_photoMainDal = new PhotoDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region PhotoByAuthorDal
        private PhotoByAuthorDal _photoByAuthorDal;
        public PhotoByAuthorDal PhotoByAuthorDal
        {
            get { return _photoByAuthorDal ?? (_photoByAuthorDal = new PhotoByAuthorDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region AlbumPublishedDal
        private AlbumPublishedDal _albumPublishedMainDal;
        public AlbumPublishedDal AlbumPublishedMainDal
        {
            get { return _albumPublishedMainDal ?? (_albumPublishedMainDal = new AlbumPublishedDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region AlbumDal
        private AlbumDal _albumMainDal;
        public AlbumDal AlbumMainDal
        {
            get { return _albumMainDal ?? (_albumMainDal = new AlbumDal((CmsPhotoDb)this)); }
        }
        #endregion

        #region AlbumByAuthorDal
        private AlbumByAuthorDal _albumByAuthorDal;
        public AlbumByAuthorDal AlbumByAuthorDal
        {
            get { return _albumByAuthorDal ?? (_albumByAuthorDal = new AlbumByAuthorDal((CmsPhotoDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected CmsPhotoDbBase()
        {
        }
        protected CmsPhotoDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    } 
}