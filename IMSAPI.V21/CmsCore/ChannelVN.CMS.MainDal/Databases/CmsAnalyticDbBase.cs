﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsAnalyticDb"/> class that 
    /// represents a connection to the <c>CmsAnalyticDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsAnalyticDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsAnalyticDbBase : MainDbBase
    {
        #region Store procedures
        #region News Analytic
        private NewsAnalyticV2Dal _newsAnalyticV2MainDal;
        public NewsAnalyticV2Dal NewsAnalyticV2MainDal
        {
            get { return _newsAnalyticV2MainDal ?? (_newsAnalyticV2MainDal = new NewsAnalyticV2Dal((CmsAnalyticDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected CmsAnalyticDbBase()
        {
        }
        protected CmsAnalyticDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}