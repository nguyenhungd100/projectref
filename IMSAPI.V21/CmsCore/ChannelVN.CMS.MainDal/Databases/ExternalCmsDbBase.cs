﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.Statistic;
using ChannelVN.CMS.MainDal.External.CafeF;
using ChannelVN.CMS.MainDal.External.GameK;
using ChannelVN.CMS.MainDal.External.GenK;
using ChannelVN.CMS.MainDal.External.Kenh14;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="ExternalCmsDb"/> class that 
    /// represents a connection to the <c>ExternalCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the ExternalCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ExternalCmsDbBase : MainDbBase
    {
        #region Store procedures
        #region News Os
        private NewsOsDal _newsOsMainDal;
        public NewsOsDal NewsOsMainDal
        {
            get { return _newsOsMainDal ?? (_newsOsMainDal = new NewsOsDal((ExternalCmsDb)this)); }
        }
        #endregion
        private FeedBackToExpertArticleDal _feedBackToExpertArticleMainDal;
        public FeedBackToExpertArticleDal FeedBackToExpertArticleMainDal
        {
            get { return _feedBackToExpertArticleMainDal ?? (_feedBackToExpertArticleMainDal = new FeedBackToExpertArticleDal((ExternalCmsDb)this)); }
        }
        #endregion

        private NewsStatisticExternalDal _newsStatisticExternalMainDal;
        public NewsStatisticExternalDal NewsStatisticExternalMainDal
        {
            get { return _newsStatisticExternalMainDal ?? (_newsStatisticExternalMainDal = new NewsStatisticExternalDal((ExternalCmsDb)this)); }
        }

        private ReverseAuctionDal _reverseAuctionMainDal;
        public ReverseAuctionDal ReverseAuctionMainDal
        {
            get { return _reverseAuctionMainDal ?? (_reverseAuctionMainDal = new ReverseAuctionDal((ExternalCmsDb)this)); }
        }

        private FacebookImageUploadDal _facebookImageUploadMainDal;
        public FacebookImageUploadDal FacebookImageUploadMainDal
        {
            get { return _facebookImageUploadMainDal ?? (_facebookImageUploadMainDal = new FacebookImageUploadDal((ExternalCmsDb)this)); }
        }

        #region Game GiftCode
        private GameGiftCodeDal _GameGiftCodeDal;
        public GameGiftCodeDalBase GameGiftCodeDal
        {
            get { return _GameGiftCodeDal ?? (_GameGiftCodeDal = new GameGiftCodeDal((ExternalCmsDb)this)); }
        }
        #endregion

        #region Constructors

        protected ExternalCmsDbBase()
        {
        }
        protected ExternalCmsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}