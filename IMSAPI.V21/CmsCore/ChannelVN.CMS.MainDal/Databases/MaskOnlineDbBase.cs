﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.NewsMaskOnline;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="MaskOnlineDb"/> class that 
    /// represents a connection to the <c>MaskOnlineDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the MaskOnlineDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class MaskOnlineDbBase : MainDbBase
    {
        #region Store procedures
        private NewsMaskOnlineDal _newsMaskOnlineDal;
        public NewsMaskOnlineDal NewsMaskOnlineDal
        {
            get { return _newsMaskOnlineDal ?? (_newsMaskOnlineDal = new NewsMaskOnlineDal((MaskOnlineDb)this)); }
        }

        #endregion

        #region Constructors

        protected MaskOnlineDbBase()
        {
        }
        protected MaskOnlineDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}