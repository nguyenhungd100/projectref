﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.Account;
using ChannelVN.CMS.MainDal.Base.AlertPerson;
using ChannelVN.CMS.MainDal.Base.AuthenticateLog;
using ChannelVN.CMS.MainDal.Base.CrawlerSource;
using ChannelVN.CMS.MainDal.Base.FileUpload;
using ChannelVN.CMS.MainDal.Base.Interview;
using ChannelVN.CMS.MainDal.Base.LogAnalytics;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.NewsCacheMonitor;
using ChannelVN.CMS.MainDal.Base.NewsMobileStream;
using ChannelVN.CMS.MainDal.Base.NewsPosition;
using ChannelVN.CMS.MainDal.Base.NewsRoyalties;
using ChannelVN.CMS.MainDal.Base.QuestionAnswer;
using ChannelVN.CMS.MainDal.Base.Topic;
using ChannelVN.CMS.MainDal.External.Kenh14;
using ChannelVN.CMS.MainDal.External.NguoiLaoDong;
using NewsSohaDal = ChannelVN.CMS.MainDal.External.SohaNews.NewsDal;
using NewsGenKDal = ChannelVN.CMS.MainDal.External.GenK.NewsDal;
using NewsPositionDal = ChannelVN.CMS.MainDal.Base.NewsPosition.NewsPositionDal;
using NewsGalleryDal = ChannelVN.CMS.MainDal.External.CafeBiz.NewsGalleryDal;
using NewsGalleryCafeFDal = ChannelVN.CMS.MainDal.External.CafeF.NewsGalleryDal;
using EmbedAlbumCafeFDal = ChannelVN.CMS.MainDal.External.CafeF.EmbedAlbumDal;
using ExpertCafeFDal = ChannelVN.CMS.MainDal.External.CafeF.ExpertDal;
using ExpertGenKDal = ChannelVN.CMS.MainDal.External.GenK.ExpertDal;
using BoxExpertNewsEmbedCafeFDal = ChannelVN.CMS.MainDal.External.CafeF.BoxExpertNewsEmbedDal;
using ChannelVN.CMS.MainDal.Base.Zone;
using ChannelVN.CMS.MainDal.Base.ZoneDefaultTag;
using ChannelVN.CMS.MainDal.Base.Vote;
using ChannelVN.CMS.MainDal.Base.VoteInZone;
using ChannelVN.CMS.MainDal.Base.VoteYesNo;
using ChannelVN.CMS.MainDal.Base.Video;
using ChannelVN.CMS.MainDal.Base.Thread;
using ChannelVN.CMS.MainDal.Base.Tag;
using ChannelVN.CMS.MainDal.Base.Statistic;
using ChannelVN.CMS.MainDal.Base.Royalties;
using ChannelVN.CMS.MainDal.Base.RollingNews;
using ChannelVN.CMS.MainDal.Base.PhotoGroup;
using ChannelVN.CMS.MainDal.Base.Photo;
using NewsDal = ChannelVN.CMS.MainDal.Base.News.NewsDal;
using ChannelVN.CMS.MainDal.Base.EndUser;
using ChannelVN.CMS.MainDal.Base.NewsWarning;
using ChannelVN.CMS.MainDal.Base.DirectTag;
using ChannelVN.CMS.MainDal.Base.StreamItem;
using ChannelVN.CMS.MainDal.Base.NewsSocial;
using ChannelVN.CMS.MainDal.Base.MediaAlbum;
using ChannelVN.CMS.MainDal.Base.LandingTemplate;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsMainDb"/> class that 
    /// represents a connection to the <c>CmsMainDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsMainDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsMainDbBase : MainDbBase
    {
        #region Store procedures

        #region Account

        private UserDal _userMainDal;
        public UserDal UserMainDal
        {
            get { return _userMainDal ?? (_userMainDal = new UserDal((CmsMainDb)this)); }
        }

        private GroupPermissionDal _groupPermissionMainDal;
        public GroupPermissionDal GroupPermissionMainDal
        {
            get { return _groupPermissionMainDal ?? (_groupPermissionMainDal = new GroupPermissionDal((CmsMainDb)this)); }
        }

        private PermissionDal _permissionMainDal;
        public PermissionDal PermissionMainDal
        {
            get { return _permissionMainDal ?? (_permissionMainDal = new PermissionDal((CmsMainDb)this)); }
        }

        private PermissionTemplateDal _permissionTemplateMainDal;
        public PermissionTemplateDal PermissionTemplateMainDal
        {
            get { return _permissionTemplateMainDal ?? (_permissionTemplateMainDal = new PermissionTemplateDal((CmsMainDb)this)); }
        }

        private UserPenNameDal _userPenNameMainDal;
        public UserPenNameDal UserPenNameMainDal
        {
            get { return _userPenNameMainDal ?? (_userPenNameMainDal = new UserPenNameDal((CmsMainDb)this)); }
        }

        private UserPermissionDal _userPermissionMainDal;
        public UserPermissionDal UserPermissionMainDal
        {
            get { return _userPermissionMainDal ?? (_userPermissionMainDal = new UserPermissionDal((CmsMainDb)this)); }
        }

        private UserSmsDal _userSmsMainDal;
        public UserSmsDal UserSmsMainDal
        {
            get { return _userSmsMainDal ?? (_userSmsMainDal = new UserSmsDal((CmsMainDb)this)); }
        }

        private AuthenticateLogDal _authenticateLogMainDal;
        public AuthenticateLogDal AuthenticateLogMainDal
        {
            get { return _authenticateLogMainDal ?? (_authenticateLogMainDal = new AuthenticateLogDal((CmsMainDb)this)); }
        }

        #endregion

        #region News Cache Monitor
        private NewsCachedMonitorDal _newsCachedMonitorMainDal;
        public NewsCachedMonitorDal NewsCachedMonitorMainDal
        {
            get { return _newsCachedMonitorMainDal ?? (_newsCachedMonitorMainDal = new NewsCachedMonitorDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position
        private NewsPositionDal _newsPositionMainDal;
        public NewsPositionDal NewsPositionMainDal
        {
            get { return _newsPositionMainDal ?? (_newsPositionMainDal = new NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position
        private NewsMobileStreamDal _newsMobileStreamMainDal;
        public NewsMobileStreamDal NewsMobileStreamMainDal
        {
            get { return _newsMobileStreamMainDal ?? (_newsMobileStreamMainDal = new NewsMobileStreamDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position For Mobile
        private NewsPositionForMobileDal _newsPositionForMobileMainDal;
        public NewsPositionForMobileDal NewsPositionForMobileMainDal
        {
            get { return _newsPositionForMobileMainDal ?? (_newsPositionForMobileMainDal = new NewsPositionForMobileDal((CmsMainDb)this)); }
        }
        #endregion

        #region Position Schedule
        private NewsPositionScheduleDal _newsPositionScheduleMainDal;
        public NewsPositionScheduleDal NewsPositionScheduleMainDal
        {
            get { return _newsPositionScheduleMainDal ?? (_newsPositionScheduleMainDal = new NewsPositionScheduleDal((CmsMainDb)this)); }
        }
        #endregion

        #region Position Type
        private NewsPositionTypeDal _newsPositionTypeMainDal;
        public NewsPositionTypeDal NewsPositionTypeMainDal
        {
            get { return _newsPositionTypeMainDal ?? (_newsPositionTypeMainDal = new NewsPositionTypeDal((CmsMainDb)this)); }
        }
        #endregion

        #region Position Type
        private NewsRoyaltiesCategoryDal _newsRoyaltiesCategoryDal;
        public NewsRoyaltiesCategoryDal NewsRoyaltiesCategoryMainDal
        {
            get { return _newsRoyaltiesCategoryDal ?? (_newsRoyaltiesCategoryDal = new NewsRoyaltiesCategoryDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Royalties
        private NewsRoyaltiesDal _newsRoyaltiesMainDal;
        public NewsRoyaltiesDal NewsRoyaltiesMainDal
        {
            get { return _newsRoyaltiesMainDal ?? (_newsRoyaltiesMainDal = new NewsRoyaltiesDal((CmsMainDb)this)); }
        }
        #endregion

        private RoyaltiesQuotaDal _royaltiesQuotaMainDal;
        public RoyaltiesQuotaDal RoyaltiesQuotaMainDal
        {
            get { return _royaltiesQuotaMainDal ?? (_royaltiesQuotaMainDal = new RoyaltiesQuotaDal((CmsMainDb)this)); }
        }

        #region News Royalties
        private NewsRoyaltiesLevelDal _newsRoyaltiesLevelMainDal;
        public NewsRoyaltiesLevelDal NewsRoyaltiesLevelMainDal
        {
            get { return _newsRoyaltiesLevelMainDal ?? (_newsRoyaltiesLevelMainDal = new NewsRoyaltiesLevelDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Royalties Media
        private NewsRoyaltiesMediaDal _newsRoyaltiesMediaMainDal;
        public NewsRoyaltiesMediaDal NewsRoyaltiesMediaMainDal
        {
            get { return _newsRoyaltiesMediaMainDal ?? (_newsRoyaltiesMediaMainDal = new NewsRoyaltiesMediaDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Royalties Media
        private NewsRoyaltiesRoleDal _newsRoyaltiesRoleMainDal;
        public NewsRoyaltiesRoleDal NewsRoyaltiesRoleMainDal
        {
            get { return _newsRoyaltiesRoleMainDal ?? (_newsRoyaltiesRoleMainDal = new NewsRoyaltiesRoleDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Royalties Rule
        private NewsRoyaltiesRuleDal _newsRoyaltiesRuleMainDal;
        public NewsRoyaltiesRuleDal NewsRoyaltiesRuleMainDal
        {
            get { return _newsRoyaltiesRuleMainDal ?? (_newsRoyaltiesRuleMainDal = new NewsRoyaltiesRuleDal((CmsMainDb)this)); }
        }
        #endregion

        #region News
        private Base.News.NewsDal _newsMainDal;
        public Base.News.NewsDal NewsMainDal
        {
            get { return _newsMainDal ?? (_newsMainDal = new NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region File Upload
        private FileUploadDal _fileUploadMainDal;
        public FileUploadDal FileUploadMainDal
        {
            get { return _fileUploadMainDal ?? (_fileUploadMainDal = new FileUploadDal((CmsMainDb)this)); }
        }
        #endregion

        #region Interview Channel
        private InterviewChannelDal _interviewChannelMainDal;
        public InterviewChannelDal InterviewChannelMainDal
        {
            get { return _interviewChannelMainDal ?? (_interviewChannelMainDal = new InterviewChannelDal((CmsMainDb)this)); }
        }
        #endregion

        #region Interview Channel Process
        private InterviewChannelProcessDal _interviewChannelProcessMainDal;
        public InterviewChannelProcessDal InterviewChannelProcessMainDal
        {
            get { return _interviewChannelProcessMainDal ?? (_interviewChannelProcessMainDal = new InterviewChannelProcessDal((CmsMainDb)this)); }
        }
        #endregion

        #region Interview
        private InterviewDal _interviewMainDal;
        public InterviewDal InterviewMainDal
        {
            get { return _interviewMainDal ?? (_interviewMainDal = new InterviewDal((CmsMainDb)this)); }
        }
        #endregion

        #region InterviewV3
        private InterviewV3Dal _interviewV3MainDal;
        public InterviewV3Dal InterviewV3MainDal
        {
            get { return _interviewV3MainDal ?? (_interviewV3MainDal = new InterviewV3Dal((CmsMainDb)this)); }
        }
        #endregion

        #region InterviewV3Question
        private InterviewV3QuestionDal _interviewV3QuestionDal;
        public InterviewV3QuestionDal InterviewV3QuestionDal
        {
            get { return _interviewV3QuestionDal ?? (_interviewV3QuestionDal = new InterviewV3QuestionDal((CmsMainDb)this)); }
        }
        #endregion

        #region InterviewV3Answers
        private InterviewV3AnswersDal _interviewV3AnswersDal;
        public InterviewV3AnswersDal InterviewV3AnswersDal
        {
            get { return _interviewV3AnswersDal ?? (_interviewV3AnswersDal = new InterviewV3AnswersDal((CmsMainDb)this)); }
        }
        #endregion

        #region InterviewV3Guests
        private InterviewV3GuestsDal _interviewV3GuestsDal;
        public InterviewV3GuestsDal InterviewV3GuestsDal
        {
            get { return _interviewV3GuestsDal ?? (_interviewV3GuestsDal = new InterviewV3GuestsDal((CmsMainDb)this)); }
        }
        #endregion

        #region News EmbedBox
        private NewsEmbedBoxDal _newsEmbedBoxMainDal;
        public NewsEmbedBoxDal NewsEmbedBoxMainDal
        {
            get { return _newsEmbedBoxMainDal ?? (_newsEmbedBoxMainDal = new NewsEmbedBoxDal((CmsMainDb)this)); }
        }
        #endregion

        #region News EmbedBox OnPage
        private NewsEmbedBoxOnPageDal _newsEmbedBoxOnPageMainDal;
        public NewsEmbedBoxOnPageDal NewsEmbedBoxOnPageMainDal
        {
            get { return _newsEmbedBoxOnPageMainDal ?? (_newsEmbedBoxOnPageMainDal = new NewsEmbedBoxOnPageDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Extension
        private NewsExtensionDal _newsExtensionMainDal;
        public NewsExtensionDal NewsExtensionMainDal
        {
            get { return _newsExtensionMainDal ?? (_newsExtensionMainDal = new NewsExtensionDal((CmsMainDb)this)); }
        }
        #endregion
        #region NewsInExpert
        private NewsInExpertDal _NewsInExpertMainDal;
        public NewsInExpertDal NewsInExpertMainDal
        {
            get { return _NewsInExpertMainDal ?? (_NewsInExpertMainDal = new NewsInExpertDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Group
        private NewsGroupDal _newsGroupMainDal;
        public NewsGroupDal NewsGroupMainDal
        {
            get { return _newsGroupMainDal ?? (_newsGroupMainDal = new NewsGroupDal((CmsMainDb)this)); }
        }
        #endregion

        #region News History
        private NewsHistoryDal _newsHistoryMainDal;
        public NewsHistoryDal NewsHistoryMainDal
        {
            get { return _newsHistoryMainDal ?? (_newsHistoryMainDal = new NewsHistoryDal((CmsMainDb)this)); }
        }
        #endregion

        #region News In Zone
        private NewsInZoneDal _newsInZoneMainDal;
        public NewsInZoneDal NewsInZoneMainDal
        {
            get { return _newsInZoneMainDal ?? (_newsInZoneMainDal = new NewsInZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Label
        private NewsLabelDal _newsLabelMainDal;
        public NewsLabelDal NewsLabelMainDal
        {
            get { return _newsLabelMainDal ?? (_newsLabelMainDal = new NewsLabelDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Live
        private NewsLiveDal _newsLiveMainDal;
        public NewsLiveDal NewsLiveMainDal
        {
            get { return _newsLiveMainDal ?? (_newsLiveMainDal = new NewsLiveDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Log External Action
        private NewsLogExternalActionDal _newsLogExternalActionMainDal;
        public NewsLogExternalActionDal NewsLogExternalActionMainDal
        {
            get { return _newsLogExternalActionMainDal ?? (_newsLogExternalActionMainDal = new NewsLogExternalActionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Notification
        private NewsNotificationDal _newsNotificationMainDal;
        public NewsNotificationDal NewsNotificationMainDal
        {
            get { return _newsNotificationMainDal ?? (_newsNotificationMainDal = new NewsNotificationDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Pr
        private NewsPrDal _newsPrMainDal;
        public NewsPrDal NewsPrMainDal
        {
            get { return _newsPrMainDal ?? (_newsPrMainDal = new NewsPrDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Publish
        private NewsPublishDal _newsPublishMainDal;
        public NewsPublishDal NewsPublishMainDal
        {
            get { return _newsPublishMainDal ?? (_newsPublishMainDal = new NewsPublishDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Source
        private NewsSourceDal _newsSourceMainDal;
        public NewsSourceDal NewsSourceMainDal
        {
            get { return _newsSourceMainDal ?? (_newsSourceMainDal = new NewsSourceDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Use Photo
        private NewsUsePhotoDal _newsUsePhotoMainDal;
        public NewsUsePhotoDal NewsUsePhotoMainDal
        {
            get { return _newsUsePhotoMainDal ?? (_newsUsePhotoMainDal = new NewsUsePhotoDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Version
        private NewsVersionDal _newsVersionMainDal;
        public NewsVersionDal NewsVersionMainDal
        {
            get { return _newsVersionMainDal ?? (_newsVersionMainDal = new NewsVersionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Wait Republish
        private NewsWaitRepublishDal _newsWaitRepublishMainDal;
        public NewsWaitRepublishDal NewsWaitRepublishMainDal
        {
            get { return _newsWaitRepublishMainDal ?? (_newsWaitRepublishMainDal = new NewsWaitRepublishDal((CmsMainDb)this)); }
        }
        #endregion

        #region Interview Question
        private InterviewQuestionDal _interviewQuestionMainDal;
        public InterviewQuestionDal InterviewQuestionMainDal
        {
            get { return _interviewQuestionMainDal ?? (_interviewQuestionMainDal = new InterviewQuestionDal((CmsMainDb)this)); }
        }
        #endregion

        #region Zone
        private ZoneDal _zoneMainDal;
        public ZoneDal ZoneMainDal
        {
            get { return _zoneMainDal ?? (_zoneMainDal = new ZoneDal((CmsMainDb)this)); }
        }
        private ZonePhotoDal _zonePhotoMainDal;
        public ZonePhotoDal ZonePhotoMainDal
        {
            get { return _zonePhotoMainDal ?? (_zonePhotoMainDal = new ZonePhotoDal((CmsMainDb)this)); }
        }
        #endregion

        #region ZoneDefaultTag
        private ZoneDefaultTagDal _zoneDefaultTagDal;
        public ZoneDefaultTagDal ZoneDefaultTagMainDal
        {
            get { return _zoneDefaultTagDal ?? (_zoneDefaultTagDal = new ZoneDefaultTagDal((CmsMainDb)this)); }
        }
        #endregion

        #region Vote
        private VoteDal _voteMainDal;
        public VoteDal VoteMainDal
        {
            get { return _voteMainDal ?? (_voteMainDal = new VoteDal((CmsMainDb)this)); }
        }
        #endregion

        #region VoteInZone
        private VoteInZoneDal _voteInZoneMainDal;
        public VoteInZoneDal VoteInZoneMainDal
        {
            get { return _voteInZoneMainDal ?? (_voteInZoneMainDal = new VoteInZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region VoteYesNo
        private VoteYesNoDal _voteYesNoDalMainDal;
        public VoteYesNoDal VoteYesNoMainDal
        {
            get { return _voteYesNoDalMainDal ?? (_voteYesNoDalMainDal = new VoteYesNoDal((CmsMainDb)this)); }
        }
        #endregion

        #region VoteYesNoGroup
        private VoteYesNoGroupDal _voteYesNoGroupMainDal;
        public VoteYesNoGroupDal VoteYesNoGroupMainDal
        {
            get { return _voteYesNoGroupMainDal ?? (_voteYesNoGroupMainDal = new VoteYesNoGroupDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxVideoEmbed
        private BoxVideoEmbedDal _boxVideoEmbedMainDal;
        public BoxVideoEmbedDal BoxVideoEmbedMainDal
        {
            get { return _boxVideoEmbedMainDal ?? (_boxVideoEmbedMainDal = new BoxVideoEmbedDal((CmsMainDb)this)); }
        }
        private BoxVideoEmbedByNewsZoneDal _boxVideoEmbedByNewsZoneMainDal;
        public BoxVideoEmbedByNewsZoneDal BoxVideoEmbedByNewsZoneMainDal
        {
            get { return _boxVideoEmbedByNewsZoneMainDal ?? (_boxVideoEmbedByNewsZoneMainDal = new BoxVideoEmbedByNewsZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxVideoProgramEmbed
        private BoxVideoProgramEmbedDal _boxVideoProgramEmbedMainDal;
        public BoxVideoProgramEmbedDal BoxVideoProgramEmbedMainDal
        {
            get { return _boxVideoProgramEmbedMainDal ?? (_boxVideoProgramEmbedMainDal = new BoxVideoProgramEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #region Thread
        private ThreadDal _threadMainDal;
        public ThreadDal ThreadMainDal
        {
            get { return _threadMainDal ?? (_threadMainDal = new ThreadDal((CmsMainDb)this)); }
        }
        #endregion

        #region ThreadInZone
        private ThreadInZoneDal _threadInZoneMainDal;
        public ThreadInZoneDal ThreadInZoneMainDal
        {
            get { return _threadInZoneMainDal ?? (_threadInZoneMainDal = new ThreadInZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region ThreadNews
        private ThreadNewsDal _threadNewsMainDal;
        public ThreadNewsDal ThreadNewsMainDal
        {
            get { return _threadNewsMainDal ?? (_threadNewsMainDal = new ThreadNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagZone
        private TagZoneDal _tagZoneMainDal;
        public TagZoneDal TagZoneMainDal
        {
            get { return _tagZoneMainDal ?? (_tagZoneMainDal = new TagZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagScoreDal
        private TagScoreDal _tagScoreMainDal;
        public TagScoreDal TagScoreMainDal
        {
            get { return _tagScoreMainDal ?? (_tagScoreMainDal = new TagScoreDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagNewsDal
        private TagNewsDal _tagNewsMainDal;
        public TagNewsDal TagNewsMainDal
        {
            get { return _tagNewsMainDal ?? (_tagNewsMainDal = new TagNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagDal
        private TagDal _tagMainDal;
        public TagDal TagMainDal
        {
            get { return _tagMainDal ?? (_tagMainDal = new TagDal((CmsMainDb)this)); }
        }
        #endregion

        #region TagAutoDal
        private TagAutoDal _tagAutoMainDal;
        public TagAutoDal TagAutoMainDal
        {
            get { return _tagAutoMainDal ?? (_tagAutoMainDal = new TagAutoDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxThreadEmbedDal
        private BoxThreadEmbedDal _boxThreadEmbedMainDal;
        public BoxThreadEmbedDal BoxThreadEmbedMainDal
        {
            get { return _boxThreadEmbedMainDal ?? (_boxThreadEmbedMainDal = new BoxThreadEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxTagEmbedDal
        private BoxTagEmbedDal _boxTagEmbedMainDal;
        public BoxTagEmbedDal BoxTagEmbedMainDal
        {
            get { return _boxTagEmbedMainDal ?? (_boxTagEmbedMainDal = new BoxTagEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #region StatisticV3Dal
        private Base.Statistic.StatisticV3Dal _statisticV3MainDal;
        public Base.Statistic.StatisticV3Dal StatisticV3MainDal
        {
            get { return _statisticV3MainDal ?? (_statisticV3MainDal = new Base.Statistic.StatisticV3Dal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsStatisticDal
        private NewsStatisticDal _newsStatisticMainDal;
        public NewsStatisticDal NewsStatisticMainDal
        {
            get { return _newsStatisticMainDal ?? (_newsStatisticMainDal = new NewsStatisticDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesRuleDal
        private RoyaltiesRuleDal _royaltiesRuleMainDal;
        public RoyaltiesRuleDal RoyaltiesRuleMainDal
        {
            get { return _royaltiesRuleMainDal ?? (_royaltiesRuleMainDal = new RoyaltiesRuleDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesRoleDal
        private RoyaltiesRoleDal _royaltiesRoleMainDal;
        public RoyaltiesRoleDal RoyaltiesRoleMainDal
        {
            get { return _royaltiesRoleMainDal ?? (_royaltiesRoleMainDal = new RoyaltiesRoleDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesMemberDal
        private RoyaltiesMemberDal _royaltiesMemberMainDal;
        public RoyaltiesMemberDal RoyaltiesMemberMainDal
        {
            get { return _royaltiesMemberMainDal ?? (_royaltiesMemberMainDal = new RoyaltiesMemberDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesHistoryDal
        private RoyaltiesHistoryDal _royaltiesHistoryMainDal;
        public RoyaltiesHistoryDal RoyaltiesHistoryMainDal
        {
            get { return _royaltiesHistoryMainDal ?? (_royaltiesHistoryMainDal = new RoyaltiesHistoryDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesDal
        private RoyaltiesDal _royaltiesMainDal;
        public RoyaltiesDal RoyaltiesMainDal
        {
            get { return _royaltiesMainDal ?? (_royaltiesMainDal = new RoyaltiesDal((CmsMainDb)this)); }
        }
        #endregion

        #region RoyaltiesCategoryDal
        private RoyaltiesCategoryDal _royaltiesCategoryMainDal;
        public RoyaltiesCategoryDal RoyaltiesCategoryMainDal
        {
            get { return _royaltiesCategoryMainDal ?? (_royaltiesCategoryMainDal = new RoyaltiesCategoryDal((CmsMainDb)this)); }
        }
        #endregion

        #region RollingNewsEventDal
        private RollingNewsEventDal _rollingNewsEventMainDal;
        public RollingNewsEventDal RollingNewsEventMainDal
        {
            get { return _rollingNewsEventMainDal ?? (_rollingNewsEventMainDal = new RollingNewsEventDal((CmsMainDb)this)); }
        }
        #endregion

        #region RollingNewsDal
        private RollingNewsDal _rollingNewsMainDal;
        public RollingNewsDal RollingNewsMainDal
        {
            get { return _rollingNewsMainDal ?? (_rollingNewsMainDal = new RollingNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region RollingNewsAuthorDal
        private RollingNewsAuthorDal _rollingNewsAuthorMainDal;
        public RollingNewsAuthorDal RollingNewsAuthorMainDal
        {
            get { return _rollingNewsAuthorMainDal ?? (_rollingNewsAuthorMainDal = new RollingNewsAuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #region PhotoGroupDal
        private PhotoGroupDal _photoGroupMainDal;
        public PhotoGroupDal PhotoGroupMainDal
        {
            get { return _photoGroupMainDal ?? (_photoGroupMainDal = new PhotoGroupDal((CmsMainDb)this)); }
        }
        #endregion

        #region EmbedAlbumDetailDal
        private EmbedAlbumDetailDal _embedAlbumDetailMainDal;
        public EmbedAlbumDetailDal EmbedAlbumDetailMainDal
        {
            get { return _embedAlbumDetailMainDal ?? (_embedAlbumDetailMainDal = new EmbedAlbumDetailDal((CmsMainDb)this)); }
        }
        #endregion

        #region EmbedAlbumDal
        private EmbedAlbumDal _embedAlbumMainDal;
        public EmbedAlbumDal EmbedAlbumMainDal
        {
            get { return _embedAlbumMainDal ?? (_embedAlbumMainDal = new EmbedAlbumDal((CmsMainDb)this)); }
        }
        #endregion

        #region Box Banner
        private BoxBannerDal _boxBannerMainDal;
        public BoxBannerDal BoxBannerMainDal
        {
            get { return _boxBannerMainDal ?? (_boxBannerMainDal = new BoxBannerDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxInboundComponentEmbed
        private BoxInboundComponentEmbedDal _boxInboundComponentEmbedMainDal;
        public BoxInboundComponentEmbedDal BoxInboundComponentEmbedMainDal
        {
            get { return _boxInboundComponentEmbedMainDal ?? (_boxInboundComponentEmbedMainDal = new BoxInboundComponentEmbedDal((CmsMainDb)this)); }
        }
        #endregion
        #region BoxLivePageEmbed
        private BoxLivePageEmbedDal _boxLivePageEmbedMainDal;
        public BoxLivePageEmbedDal BoxLivePageEmbedMainDal
        {
            get { return _boxLivePageEmbedMainDal ?? (_boxLivePageEmbedMainDal = new BoxLivePageEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #region Embed Group
        private EmbedGroupDal _embedGroupMainDal;
        public EmbedGroupDal EmbedGroupMainDal
        {
            get { return _embedGroupMainDal ?? (_embedGroupMainDal = new EmbedGroupDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Analytic Adtech
        private NewsAnalyticAdtechDal _newsAnalyticAdtechMainDal;
        public NewsAnalyticAdtechDal NewsAnalyticAdtechMainDal
        {
            get { return _newsAnalyticAdtechMainDal ?? (_newsAnalyticAdtechMainDal = new NewsAnalyticAdtechDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Analytic
        private NewsAnalyticDal _newsAnalyticMainDal;
        public NewsAnalyticDal NewsAnalyticMainDal
        {
            get { return _newsAnalyticMainDal ?? (_newsAnalyticMainDal = new NewsAnalyticDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Author
        private NewsAuthorDal _newsAuthorMainDal;
        public NewsAuthorDal NewsAuthorMainDal
        {
            get { return _newsAuthorMainDal ?? (_newsAuthorMainDal = new NewsAuthorDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsChild
        private NewsChildDal _newsChildMainDal;
        public NewsChildDal NewsChildMainDal
        {
            get { return _newsChildMainDal ?? (_newsChildMainDal = new NewsChildDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsChild Publish
        private NewsChildPublishDal _newsChildPublishMainDal;
        public NewsChildPublishDal NewsChildPublishMainDal
        {
            get { return _newsChildPublishMainDal ?? (_newsChildPublishMainDal = new NewsChildPublishDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsChild Version
        private NewsChildVersionDal _newsChildVersionDal;
        public NewsChildVersionDal NewsChildVersionMainDal
        {
            get { return _newsChildVersionDal ?? (_newsChildVersionDal = new NewsChildVersionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Config
        private NewsConfigDal _newsConfigMainDal;
        public NewsConfigDal NewsConfigMainDal
        {
            get { return _newsConfigMainDal ?? (_newsConfigMainDal = new NewsConfigDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsContent
        private NewsContentDal _newsContentMainDal;
        public NewsContentDal NewsContentMainDal
        {
            get { return _newsContentMainDal ?? (_newsContentMainDal = new NewsContentDal((CmsMainDb)this)); }
        }
        #endregion

        #region External
        #region News AutoPro
        private CMS.MainDal.External.AutoPro.NewsDal _autoProNewsMainDal;
        public CMS.MainDal.External.AutoPro.NewsDal AutoProMainDal
        {
            get { return _autoProNewsMainDal ?? (_autoProNewsMainDal = new External.AutoPro.NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Gallery CafeBiz
        private NewsGalleryDal _newsGalleryMainDal;
        public NewsGalleryDal NewsGalleryMainDal
        {
            get { return _newsGalleryMainDal ?? (_newsGalleryMainDal = new NewsGalleryDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Gallery CafeF
        private NewsGalleryCafeFDal _newsGalleryCafeFMainDal;
        public NewsGalleryCafeFDal NewsGalleryCafeFMainDal
        {
            get { return _newsGalleryCafeFMainDal ?? (_newsGalleryCafeFMainDal = new NewsGalleryCafeFDal((CmsMainDb)this)); }
        }
        #endregion
        private EmbedAlbumCafeFDal _embedAlbumCafeFMainDal;
        public EmbedAlbumCafeFDal EmbedAlbumCafeFMainDal
        {
            get { return _embedAlbumCafeFMainDal ?? (_embedAlbumCafeFMainDal = new EmbedAlbumCafeFDal((CmsMainDb)this)); }
        }

        #region News Position CafeBiz
        private CMS.MainDal.External.CafeBiz.NewsPositionDal _cafeBizNewsPositionMainDal;
        public CMS.MainDal.External.CafeBiz.NewsPositionDal CafeBizNewsPositionMainDal
        {
            get { return _cafeBizNewsPositionMainDal ?? (_cafeBizNewsPositionMainDal = new External.CafeBiz.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News GameK
        private CMS.MainDal.External.GameK.NewsDal _gameKNewsMainDal;
        public CMS.MainDal.External.GameK.NewsDal GameKNewsMainDal
        {
            get { return _gameKNewsMainDal ?? (_gameKNewsMainDal = new External.GameK.NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region News NguoiLaoDong
        private External.NguoiLaoDong.NewsDal _nguoiLaoDongNewsMainDal;
        public External.NguoiLaoDong.NewsDal NguoiLaoDongNewsMainDal
        {
            get { return _nguoiLaoDongNewsMainDal ?? (_nguoiLaoDongNewsMainDal = new External.NguoiLaoDong.NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region GateGameTagDal
        private GateGameTagDal _gateGameTagMainDal;
        public GateGameTagDal GateGameTagMainDal
        {
            get { return _gateGameTagMainDal ?? (_gateGameTagMainDal = new GateGameTagDal((CmsMainDb)this)); }
        }
        #endregion

        #region News GiaDinhNet
        private CMS.MainDal.External.GiaDinhNet.NewsDal _giaDinhNetNewsMainDal;
        public CMS.MainDal.External.GiaDinhNet.NewsDal GiaDinhNetNewsMainDal
        {
            get { return _giaDinhNetNewsMainDal ?? (_giaDinhNetNewsMainDal = new External.GiaDinhNet.NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region News K14
        private CMS.MainDal.External.Kenh14.NewsDal _kenh14NewsMainDal;
        public CMS.MainDal.External.Kenh14.NewsDal Kenh14NewsMainDal
        {
            get { return _kenh14NewsMainDal ?? (_kenh14NewsMainDal = new External.Kenh14.NewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position K14
        private CMS.MainDal.External.Kenh14.NewsPositionDal _kenh14NewsPositionMainDal;
        public CMS.MainDal.External.Kenh14.NewsPositionDal Kenh14NewsPositionMainDal
        {
            get { return _kenh14NewsPositionMainDal ?? (_kenh14NewsPositionMainDal = new External.Kenh14.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position NLD
        private CMS.MainDal.External.NguoiLaoDong.NewsPositionDal _nguoiLaoDongNewsPositionMainDal;
        public CMS.MainDal.External.NguoiLaoDong.NewsPositionDal NguoiLaoDongNewsPositionMainDal
        {
            get { return _nguoiLaoDongNewsPositionMainDal ?? (_nguoiLaoDongNewsPositionMainDal = new External.NguoiLaoDong.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position Soha News
        private CMS.MainDal.External.SohaNews.NewsPositionDal _sohaNewsNewsPositionMainDal;
        public CMS.MainDal.External.SohaNews.NewsPositionDal SohaNewsNewsPositionMainDal
        {
            get { return _sohaNewsNewsPositionMainDal ?? (_sohaNewsNewsPositionMainDal = new External.SohaNews.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position Soha News For Zone Sport
        private CMS.MainDal.External.SohaNews.NewsPositionForZoneSportDal _sohaNewsNewsPositionForZoneSportMainDal;
        public CMS.MainDal.External.SohaNews.NewsPositionForZoneSportDal SohaNewsNewsPositionForZoneSportMainDal
        {
            get { return _sohaNewsNewsPositionForZoneSportMainDal ?? (_sohaNewsNewsPositionForZoneSportMainDal = new External.SohaNews.NewsPositionForZoneSportDal((CmsMainDb)this)); }
        }
        #endregion

        #region News Position TheGuide
        private CMS.MainDal.External.TheGuide.NewsPositionDal _theGuideNewsPositionMainDal;
        public CMS.MainDal.External.TheGuide.NewsPositionDal TheGuiderNewsPositionMainDal
        {
            get { return _theGuideNewsPositionMainDal ?? (_theGuideNewsPositionMainDal = new External.TheGuide.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion
        #region News Position GiaDinhNet
        private CMS.MainDal.External.GiaDinhNet.NewsPositionDal _giaDinhNetNewsPositionMainDal;
        public CMS.MainDal.External.GiaDinhNet.NewsPositionDal GiaDinhNetNewsPositionMainDal
        {
            get { return _giaDinhNetNewsPositionMainDal ?? (_giaDinhNetNewsPositionMainDal = new External.GiaDinhNet.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion
        #region News Position VTV
        private CMS.MainDal.External.VTV.NewsPositionDal _vtvNewsPositionMainDal;
        public CMS.MainDal.External.VTV.NewsPositionDal VtvNewsPositionMainDal
        {
            get { return _vtvNewsPositionMainDal ?? (_vtvNewsPositionMainDal = new External.VTV.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion
        #region News Position VCCorp
        private CMS.MainDal.External.VCCorp.NewsPositionDal _vccorpNewsPositionMainDal;
        public CMS.MainDal.External.VCCorp.NewsPositionDal VCCorpNewsPositionMainDal
        {
            get { return _vccorpNewsPositionMainDal ?? (_vccorpNewsPositionMainDal = new External.VCCorp.NewsPositionDal((CmsMainDb)this)); }
        }
        #endregion

        #region CafeF
        private ExpertCafeFDal _expertMainDal;
        public ExpertCafeFDal ExpertMainDal
        {
            get { return _expertMainDal ?? (_expertMainDal = new ExpertCafeFDal((CmsMainDb)this)); }
        }
        private BoxExpertNewsEmbedCafeFDal _boxExpertNewsEmbedMainDal;
        public BoxExpertNewsEmbedCafeFDal BoxExpertNewsEmbedMainDal
        {
            get { return _boxExpertNewsEmbedMainDal ?? (_boxExpertNewsEmbedMainDal = new BoxExpertNewsEmbedCafeFDal((CmsMainDb)this)); }
        }
        #endregion

        private ExpertGenKDal _expertGenkMainDal;
        public ExpertGenKDal ExpertGenkMainDal
        {
            get { return _expertGenkMainDal ?? (_expertGenkMainDal = new ExpertGenKDal((CmsMainDb)this)); }
        }


        #region InteractiveUser
        private CMS.MainDal.External.Interactive.UserDal _interactiveUserMainDal;
        public CMS.MainDal.External.Interactive.UserDal InteractiveUserMainDal
        {
            get { return _interactiveUserMainDal ?? (_interactiveUserMainDal = new External.Interactive.UserDal((CmsMainDb)this)); }
        }
        #endregion


        private ExternalVideoEmbedDal _externalVideoEmbedMainDal;
        public ExternalVideoEmbedDal ExternalVideoEmbedMainDal
        {
            get { return _externalVideoEmbedMainDal ?? (_externalVideoEmbedMainDal = new ExternalVideoEmbedDal((CmsMainDb)this)); }
        }


        #region HotNewsConfig Kenh 14
        private HotNewsConfigDal _hotNewsConfigMainDal;
        public HotNewsConfigDal HotNewsConfigMainDal
        {
            get { return _hotNewsConfigMainDal ?? (_hotNewsConfigMainDal = new HotNewsConfigDal((CmsMainDb)this)); }
        }
        #endregion

        #region BoxTVShow Kenh 14
        private BoxTVShowDal _boxTVShowMainDal;
        public BoxTVShowDal BoxTVShowMainDal
        {
            get { return _boxTVShowMainDal ?? (_boxTVShowMainDal = new BoxTVShowDal((CmsMainDb)this)); }
        }
        #endregion

        #endregion

        #region AlertPerson
        private AlertPersonDal _alertPersonMainDal;
        public AlertPersonDal AlertPersonMainDal
        {
            get { return _alertPersonMainDal ?? (_alertPersonMainDal = new AlertPersonDal((CmsMainDb)this)); }
        }
        #endregion

        #region Source Crawler
        private CrawlerSourceDal _crawlerSourceMainDal;
        public CrawlerSourceDal CrawlerSourceMainDal
        {
            get { return _crawlerSourceMainDal ?? (_crawlerSourceMainDal = new CrawlerSourceDal((CmsMainDb)this)); }
        }
        #endregion

        #region Question Answer
        private QuestionAnswerDal _questionAnswerMainDal;
        public QuestionAnswerDal QuestionAnswerMainDal
        {
            get { return _questionAnswerMainDal ?? (_questionAnswerMainDal = new QuestionAnswerDal((CmsMainDb)this)); }
        }
        #endregion

        private LogAnalyticsDal _LogAnalyticsMainDal;
        public LogAnalyticsDal LogAnalyticsMainDal
        {
            get { return _LogAnalyticsMainDal ?? (_LogAnalyticsMainDal = new LogAnalyticsDal((CmsMainDb)this)); }
        }

        #endregion

        #region TopicInZone
        private TopicInZoneDal _TopicInZoneMainDal;
        public TopicInZoneDal TopicInZoneMainDal
        {
            get { return _TopicInZoneMainDal ?? (_TopicInZoneMainDal = new TopicInZoneDal((CmsMainDb)this)); }
        }
        #endregion

        #region TopicNews
        private TopicNewsDal _TopicNewsMainDal;
        public TopicNewsDal TopicNewsMainDal
        {
            get { return _TopicNewsMainDal ?? (_TopicNewsMainDal = new TopicNewsDal((CmsMainDb)this)); }
        }
        #endregion

        #region Topic
        private TopicDal _TopicMainDal;
        public TopicDal TopicMainDal
        {
            get { return _TopicMainDal ?? (_TopicMainDal = new TopicDal((CmsMainDb)this)); }
        }
        #endregion

        #region #Soha
        private NewsSohaDal _newsSohaMainDal;
        public NewsSohaDal NewsSohaMainDal
        {
            get { return _newsSohaMainDal ?? (_newsSohaMainDal = new NewsSohaDal((CmsMainDb)this)); }
        }

        private VideoExtensionDal _videoExtensionMainDal;
        public VideoExtensionDal VideoExtensionMainDal
        {
            get { return _videoExtensionMainDal ?? (_videoExtensionMainDal = new VideoExtensionDal((CmsMainDb)this)); }
        }

        private NewsGenKDal _newsGenKMainDal;
        public NewsGenKDal NewsGenKMainDal
        {
            get { return _newsGenKMainDal ?? (_newsGenKMainDal = new NewsGenKDal((CmsMainDb)this)); }
        }
        #endregion

        #region EndUser
        private EndUserDal _endUserMainDal;
        public EndUserDal EndUserMainDal
        {
            get { return _endUserMainDal ?? (_endUserMainDal = new EndUserDal((CmsMainDb)this)); }
        }
        #endregion

        #region ContentWarning
        private ContentWarningDal _contentWarningMainDal;
        public ContentWarningDal ContentWarningMainDal
        {
            get { return _contentWarningMainDal ?? (_contentWarningMainDal = new ContentWarningDal((CmsMainDb)this)); }
        }
        #endregion

        #region DirectTag
        private DirectTagDal _directTagMainDal;
        public DirectTagDal DirectTagMainDal
        {
            get { return _directTagMainDal ?? (_directTagMainDal = new DirectTagDal((CmsMainDb)this)); }
        }
        #endregion

        #region StreamItem
        private StreamItemDal _streamItemMainDal;
        public StreamItemDal StreamItemMainDal
        {
            get { return _streamItemMainDal ?? (_streamItemMainDal = new StreamItemDal((CmsMainDb)this)); }
        }
        #endregion

        #region NewsSocial
        private NewsSocialEmbedDal _newsSocialEmbedMainDal;
        public NewsSocialEmbedDal NewsSocialEmbedMainDal
        {
            get { return _newsSocialEmbedMainDal ?? (_newsSocialEmbedMainDal = new NewsSocialEmbedDal((CmsMainDb)this)); }
        }

        private NewsSocialContentDal _newsSocialContentMainDal;
        public NewsSocialContentDal NewsSocialContentMainDal
        {
            get { return _newsSocialContentMainDal ?? (_newsSocialContentMainDal = new NewsSocialContentDal((CmsMainDb)this)); }
        }

        private BoxNewsSocialEmbedDal _boxNewsSocialEmbedMainDal;
        public BoxNewsSocialEmbedDal BoxNewsSocialEmbedMainDal
        {
            get { return _boxNewsSocialEmbedMainDal ?? (_boxNewsSocialEmbedMainDal = new BoxNewsSocialEmbedDal((CmsMainDb)this)); }
        }
        #endregion

        #region MediaAlbum

        private MediaAlbumDal _mediaAlbumMainDal;
        public MediaAlbumDalBase MediaAlbumMainDal
        {
            get { return _mediaAlbumMainDal ?? (_mediaAlbumMainDal = new MediaAlbumDal((CmsMainDb)this)); }
        }

        private MediaAlbumDetailDal _mediaAlbumDetailMainDal;
        public MediaAlbumDetailDalBase MediaAlbumDetailMainDal
        {
            get { return _mediaAlbumDetailMainDal ?? (_mediaAlbumDetailMainDal = new MediaAlbumDetailDal((CmsMainDb)this)); }
        }

        #endregion

        #region LandingTemplate

        private LandingTemplateDal _landingTemplateMainDal;
        public LandingTemplateDalBase LandingTemplateMainDal
        {
            get { return _landingTemplateMainDal ?? (_landingTemplateMainDal = new LandingTemplateDal((CmsMainDb)this)); }
        }
        #endregion

        #region PhotoEvent
        private PhotoEventDal _photoEvenMainDal;
        public PhotoEventDal PhotoEventMainDal
        {
            get { return _photoEvenMainDal ?? (_photoEvenMainDal = new PhotoEventDal((CmsMainDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainDbBase()
        {
        }
        protected CmsMainDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}