﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.Video;
using ChannelVN.CMS.MainDal.Base.News;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsVideoDb"/> class that 
    /// represents a connection to the <c>CmsVideoDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsVideoDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsVideoDistributionDbBase : MainDbBase
    {
        #region Store procedures
        #region Video
        private VideoDistributionDal _videoMainDal;
        public VideoDistributionDal VideoMainDal
        {
            get { return _videoMainDal ?? (_videoMainDal = new VideoDistributionDal((CmsVideoDistributionDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsVideoDistributionDbBase()
        {
        }
        protected CmsVideoDistributionDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}