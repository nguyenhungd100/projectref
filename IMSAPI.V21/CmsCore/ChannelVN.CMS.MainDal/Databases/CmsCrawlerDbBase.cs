﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsCrawlerDb"/> class that 
    /// represents a connection to the <c>CmsCrawlerDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsCrawlerDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsCrawlerDbBase : MainDbBase
    {
        #region Store procedures
        #region NewsCrawler
        private NewsCrawlerDal _newsCrawlerMainDal;
        public NewsCrawlerDal NewsCrawlerMainDal
        {
            get { return _newsCrawlerMainDal ?? (_newsCrawlerMainDal = new NewsCrawlerDal((CmsCrawlerDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected CmsCrawlerDbBase()
        {
        }
        protected CmsCrawlerDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}