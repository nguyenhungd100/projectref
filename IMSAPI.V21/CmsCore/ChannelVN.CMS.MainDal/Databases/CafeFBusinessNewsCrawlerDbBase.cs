﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.Statistic;
using ChannelVN.CMS.MainDal.External.CafeF;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CafeFBusinessNewsCrawlerDb"/> class that 
    /// represents a connection to the <c>CafeFBusinessNewsCrawlerDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CafeFBusinessNewsCrawlerDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CafeFBusinessNewsCrawlerDbBase : MainDbBase
    {
        private BusinessNewsDal _BusinessNewsMainDal;
        public BusinessNewsDal BusinessNewsMainDal
        {
            get { return _BusinessNewsMainDal ?? (_BusinessNewsMainDal = new BusinessNewsDal((CafeFBusinessNewsCrawlerDb)this)); }
        }

        #region Constructors

        protected CafeFBusinessNewsCrawlerDbBase()
        {
        }
        protected CafeFBusinessNewsCrawlerDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}