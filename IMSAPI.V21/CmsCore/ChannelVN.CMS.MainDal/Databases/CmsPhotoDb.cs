﻿using System.Data;
using System.Data.SqlClient;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.MainDal.Databases
{
    public class CmsPhotoDb : CmsPhotoDbBase
    {
        private const string ConnectionStringName = "CmsPhotoDb";
        private const string ConnectionStringName_Dev = "CmsPhotoDb_Dev";

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>An <see cref="System.Data.IDbConnection"/> object.</returns>
        protected override IDbConnection CreateConnection()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            //if (AppSettings.GetBool("IsDevEnv"))
            //{
            //    strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
            //    ConnectionStringName_Dev, Constants.ConnectionDecryptKey);
            //}
            return new SqlConnection(strConn);
        }
    }
}