﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.Statistic;
using ChannelVN.CMS.MainDal.Base.Tag;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="TagCloudCmsDb"/> class that 
    /// represents a connection to the <c>TagCloudCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the TagCloudCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class TagCloudCmsDbBase : MainDbBase
    {
        private TagCloudDal _tagCloudMainDal;
        public TagCloudDal TagCloudMainDal
        {
            get { return _tagCloudMainDal ?? (_tagCloudMainDal = new TagCloudDal((TagCloudCmsDb)this)); }
        }

        #region Constructors

        protected TagCloudCmsDbBase()
        {
        }
        protected TagCloudCmsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}