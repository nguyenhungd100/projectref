﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.NewsSlave;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="ExternalCmsDb"/> class that 
    /// represents a connection to the <c>ExternalCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the ExternalCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class SlaveCmsDbBase : SlaveDbBase
    {
        private string _connectString;

        private NewsSlaveDal _newsSlaveMainDal;
        public NewsSlaveDal NewsSlaveMainDal
        {
            get { return _newsSlaveMainDal ?? (_newsSlaveMainDal = new NewsSlaveDal((SlaveCmsDb)this)); }
        }

        #region Constructors
        //protected SlaveCmsDbBase()
        //{            
        //}
        protected SlaveCmsDbBase(string conn)
            :base(conn)
        {
            if (conn!=null)
            {                
                InitConnection(conn);
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection(string conn)
        {
            _connection = CreateConnection(conn);
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection(string conn);

        #endregion
    }
}