﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.TemplateEmbed;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="TemplateEmbedCmsDb"/> class that 
    /// represents a connection to the <c>TemplateEmbedCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the TemplateEmbedCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class TemplateEmbedCmsDbBase : MainDbBase
    {        
        private TemplateEmbedDal _templateEmbedMainDal;
        public TemplateEmbedDal TemplateEmbedMainDal
        {
            get { return _templateEmbedMainDal ?? (_templateEmbedMainDal = new TemplateEmbedDal((TemplateEmbedCmsDb)this)); }
        }          

        #region Constructors

        protected TemplateEmbedCmsDbBase()
        {
        }
        protected TemplateEmbedCmsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}