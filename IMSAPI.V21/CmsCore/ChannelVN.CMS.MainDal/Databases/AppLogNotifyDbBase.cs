﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.NewsNotify;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="AppLogNotifyDb"/> class that 
    /// represents a connection to the <c>CmsCrawlerDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsCrawlerDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class AppLogNotifyDbBase : MainDbBase
    {
        #region Store procedures
        #region AppLogNotify
        private NewsNotifyDal _newsNotifyMainDal;
        public NewsNotifyDal NewsNotifyMainDal
        {
            get { return _newsNotifyMainDal ?? (_newsNotifyMainDal = new NewsNotifyDal((AppLogNotifyDb)this)); }
        }
        #endregion
        #endregion

        #region Constructors

        protected AppLogNotifyDbBase()
        {
        }
        protected AppLogNotifyDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}