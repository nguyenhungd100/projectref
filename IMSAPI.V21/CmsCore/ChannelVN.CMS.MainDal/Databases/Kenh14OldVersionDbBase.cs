﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="Kenh14OldVersionDb"/> class that 
    /// represents a connection to the <c>Kenh14OldVersionDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the Kenh14OldVersionDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class Kenh14OldVersionDbBase : MainDbBase
    {
        #region Store procedures

        #endregion

        #region Constructors

        protected Kenh14OldVersionDbBase()
        {
        }
        protected Kenh14OldVersionDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}