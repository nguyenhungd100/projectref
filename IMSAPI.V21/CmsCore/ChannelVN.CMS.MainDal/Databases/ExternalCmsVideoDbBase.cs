﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.News;
using ChannelVN.CMS.MainDal.Base.Statistic;
using ChannelVN.CMS.MainDal.Base.Video;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="ExternalCmsDb"/> class that 
    /// represents a connection to the <c>ExternalCmsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the ExternalCmsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class ExternalCmsVideoDbBase : MainDbBase
    {
        #region Store procedures
        private ExternalCmsVideoDal _videoMainDal;
        public ExternalCmsVideoDal VideoMainDal
        {
            get { return _videoMainDal ?? (_videoMainDal = new ExternalCmsVideoDal((ExternalCmsVideoDb)this)); }
        }
        private ZoneVideoExternalDal _zoneVideoExternalMainDal;
        public ZoneVideoExternalDal ZoneVideoExternalMainDal
        {
            get { return _zoneVideoExternalMainDal ?? (_zoneVideoExternalMainDal = new ZoneVideoExternalDal((ExternalCmsVideoDb)this)); }
        }
        #endregion

        #region Constructors

        protected ExternalCmsVideoDbBase()
        {
        }
        protected ExternalCmsVideoDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}