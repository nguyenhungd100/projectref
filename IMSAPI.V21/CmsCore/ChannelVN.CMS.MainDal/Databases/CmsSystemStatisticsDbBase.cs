﻿using System.Data;
using ChannelVN.CMS.Common.DalCommon;

namespace ChannelVN.CMS.MainDal.Databases
{
    /// <summary>
    /// The base class for the <see cref="CmsSystemStatisticsDb"/> class that 
    /// represents a connection to the <c>CmsSystemStatisticsDb</c> database. 
    /// </summary>
    /// <remarks>
    /// Do not change this source code. Modify the CmsSystemStatisticsDb class
    /// if you need to add or change some functionality.
    /// </remarks>
    public abstract class CmsSystemStatisticsDbBase : MainDbBase
    {
        #region Store procedures

        #endregion

        #region Constructors

        protected CmsSystemStatisticsDbBase()
        {
        }
        protected CmsSystemStatisticsDbBase(bool init)
        {
            if (init)
            {
                InitConnection();
            }
        }

        #endregion

        #region Protected members

        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();

        #endregion
    }
}