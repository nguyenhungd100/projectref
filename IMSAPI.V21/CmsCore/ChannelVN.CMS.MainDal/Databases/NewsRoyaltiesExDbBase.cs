﻿using ChannelVN.CMS.Common.DalCommon;
using ChannelVN.CMS.MainDal.Base.NewsRoyaltiesEx;
using System.Data;

namespace ChannelVN.CMS.MainDal.Databases
{
    public abstract class NewsRoyaltiesExDbBase : MainDbBase
    {

        private NewsRoyaltiesExDal _pageViewStatisticDal;
        public NewsRoyaltiesExDal NewsRoyaltiesExMainDal
        {
            get { return _pageViewStatisticDal ?? (_pageViewStatisticDal = new NewsRoyaltiesExDal((NewsRoyaltiesExDb)this)); }
        }

        #region Protected members
        protected override sealed void InitConnection()
        {
            _connection = CreateConnection();
            _connection.Open();
        }

        /// <summary>
        /// Creates a new connection to the database.
        /// </summary>
        /// <returns>A reference to the <see cref="System.Data.IDbConnection"/> object.</returns>
        protected abstract IDbConnection CreateConnection();
        #endregion
    }
}
