﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.DiscussionV2.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.CmsExtension.DiscussionV2
{
    public abstract class DiscussionV2DalBase
    {
        #region DiscussionV2

        public bool AddDiscussionV2(DiscussionV2Entity newsAuthor)
        {
            bool returnValue = false;
            try
            {                
                if (newsAuthor != null)
                {                    
                    var oDiscussionV2 = new DiscussionSearchEntity
                    {
                        Id = newsAuthor.Id
                    };
                    var fieldNameAnalyzer = "author_name";
                    returnValue = _db.CreateIndexSettings<DiscussionSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<DiscussionSearchEntity>(oDiscussionV2);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateDiscussionV2(DiscussionV2Entity newsAuthor)
        {
            bool returnValue = false;
            try
            {                
                if (newsAuthor != null)
                {
                    var oDiscussionV2 = _db.Get<DiscussionSearchEntity>(newsAuthor.Id);                                       
                    if (oDiscussionV2 == null)
                    {
                        oDiscussionV2 = new DiscussionSearchEntity
                        {
                            Id = newsAuthor.Id
                        };
                    }
                    else
                    {
                        oDiscussionV2.Id = newsAuthor.Id;                       
                    }           

                    returnValue = _db.Update<DiscussionSearchEntity>(oDiscussionV2.Id, oDiscussionV2);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public List<string> SearchDiscussionV2(string keyword)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "unsign_name", "author_name", "author_name.folded", "user_data", };

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                var data = _db.Search<DiscussionSearchEntity>(keyword, fields);
                if (data != null)
                {                   
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllDiscussionV2(List<DiscussionV2Entity> listDiscussionV2)
        {
            bool returnValue = false;
            try
            {
                var list = listDiscussionV2.Select(newsAuthor => new DiscussionSearchEntity
                {
                    Id = newsAuthor.Id
                }).ToList();

                var fieldNameAnalyzer = "author_name";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<DiscussionSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<DiscussionSearchEntity>(list);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }

        #endregion

        #region DiscussionTopicV2

        public bool AddDiscussionTopicV2(DiscussionTopicV2Entity topic)
        {
            bool returnValue = false;
            try
            {
                if (topic != null)
                {
                    var oDiscussionTopicV2 = new DiscussionSearchEntity
                    {
                        Id = topic.Id
                    };
                    var fieldNameAnalyzer = "author_name";
                    returnValue = _db.CreateIndexSettings<DiscussionSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<DiscussionSearchEntity>(oDiscussionTopicV2);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateDiscussionTopicV2(DiscussionTopicV2Entity topic)
        {
            bool returnValue = false;
            try
            {
                if (topic != null)
                {
                    var oDiscussionTopicV2 = _db.Get<DiscussionSearchEntity>(topic.Id);
                    if (oDiscussionTopicV2 == null)
                    {
                        oDiscussionTopicV2 = new DiscussionSearchEntity
                        {
                            Id = topic.Id
                        };
                    }
                    else
                    {
                        oDiscussionTopicV2.Id = topic.Id;
                    }

                    returnValue = _db.Update<DiscussionSearchEntity>(oDiscussionTopicV2.Id, oDiscussionTopicV2);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public List<string> SearchDiscussionTopicV2(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "title", "title.folded" };

                var sort = new Dictionary<string, dynamic>();
                sort.Add("desc", "created_date");
                
                var term = new Dictionary<string, dynamic>();
                
                term.Add("status", status);

                if (type > 0)
                {
                    term.Add("type", type);
                }               

                var terms = new Dictionary<string, dynamic>();                

                var data = _db.SearchPaging<DiscussionTopicSearchEntity>(keyword, fields, sort, term, pageIndex, pageSize);                

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllDiscussionTopicV2(List<DiscussionTopicSearchEntity> listDiscussionTopic)
        {
            bool returnValue = false;
            try
            {                
                var fieldNameAnalyzer = "title";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<DiscussionTopicSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<DiscussionTopicSearchEntity>(listDiscussionTopic);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }

        #endregion

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected DiscussionV2DalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
