﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.DiscussionV2.Entity;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.CmsExtension.DiscussionV2
{
    public class DiscussionV2DalFactory
    {
        #region DiscussionV2

        public static bool AddDiscussionV2(DiscussionV2Entity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.AddDiscussionV2(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateDiscussionV2(DiscussionV2Entity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.UpdateDiscussionV2(news);
            }
            return returnValue;
        }

        public static List<string> SearchDiscussionV2(string keyword)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.SearchDiscussionV2(keyword);
            }
            return returnValue;
        }                    

        public static bool InitAllDiscussionV2(List<DiscussionV2Entity> listDiscussionV2)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.InitAllDiscussionV2(listDiscussionV2);
            }
            return returnValue;
        }

        #endregion

        #region DiscussionTopicV2

        public static bool AddDiscussionTopicV2(DiscussionTopicV2Entity topic)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.AddDiscussionTopicV2(topic);
            }
            return returnValue;
        }

        public static bool UpdateDiscussionTopicV2(DiscussionTopicV2Entity topic)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.UpdateDiscussionTopicV2(topic);
            }
            return returnValue;
        }

        public static List<string> SearchDiscussionTopicV2(string keyword, int type, bool status, string userName, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.SearchDiscussionTopicV2(keyword, type, status, userName, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool InitAllDiscussionTopic(List<DiscussionTopicSearchEntity> listDiscussionTopic)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.DiscussionV2Dal.InitAllDiscussionTopicV2(listDiscussionTopic);
            }
            return returnValue;
        }

        #endregion
    }
}
