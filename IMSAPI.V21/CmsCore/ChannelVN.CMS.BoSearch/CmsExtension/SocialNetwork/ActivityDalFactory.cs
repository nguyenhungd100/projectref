﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.SocialNetwork.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.CmsExtension.SocialNetwork
{
    public class ActivityDalFactory
    {
        public static bool AddActivity(ActivityEntity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ActivityDal.AddActivity(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateActivity(ActivityEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ActivityDal.UpdateActivity(news);
            }
            return returnValue;
        }

        public static List<string> SearchActivity(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ActivityDal.SearchActivity(pageIndex, pageSize, applicationId, actionTypeDetail, type, dateFrom, dateTo, ref totalRows);
            }
            return returnValue;
        }                    

        public static bool InitAllActivity(List<ActivityEntity> listActivity)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ActivityDal.InitAllActivity(listActivity);
            }
            return returnValue;
        }
    }
}
