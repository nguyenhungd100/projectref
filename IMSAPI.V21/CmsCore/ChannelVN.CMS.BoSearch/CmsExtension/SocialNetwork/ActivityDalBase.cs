﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.SocialNetwork.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.CmsExtension.SocialNetwork
{
    public abstract class ActivityDalBase
    {
        public bool AddActivity(ActivityEntity activity)
        {
            bool returnValue = false;
            try
            {                
                if (activity != null)
                {                    
                    var oActivity = new ActivitySearchEntity
                    {
                        Id = activity.Id,
                        ApplicationId=activity.ApplicationId,
                        ActionTypeDetail = activity.ActionTypeDetail,                        
                        Type = activity.Type,
                        CreatedDate = activity.CreatedDate
                    };
                    var fieldNameAnalyzer = "";
                    returnValue = _db.CreateIndexSettings<ActivitySearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<ActivitySearchEntity>(oActivity);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateActivity(ActivityEntity activity)
        {
            bool returnValue = false;
            try
            {                
                if (activity != null)
                {
                    var oActivity = _db.Get<ActivitySearchEntity>(activity.Id);                                       
                    if (oActivity == null)
                    {
                        oActivity = new ActivitySearchEntity
                        {
                            Id = activity.Id,
                            ApplicationId = activity.ApplicationId,
                            ActionTypeDetail = activity.ActionTypeDetail,
                            Type = activity.Type,
                            CreatedDate = activity.CreatedDate
                        };
                    }
                    else
                    {
                        oActivity.Id = activity.Id;
                        oActivity.ApplicationId = activity.ApplicationId;
                        oActivity.ActionTypeDetail = activity.ActionTypeDetail;                       
                        oActivity.Type = activity.Type;
                        oActivity.CreatedDate = activity.CreatedDate;
                    }           

                    returnValue = _db.Update<ActivitySearchEntity>(oActivity.Id, oActivity);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public List<string> SearchActivity(int pageIndex, int pageSize, long applicationId, int actionTypeDetail, int type, DateTime dateFrom, DateTime dateTo, ref int totalRows)
        {
            try
            {
                var listSearch = new List<string>();                
                var data = _db.SearchActivity<ActivitySearchEntity>(pageIndex, pageSize, applicationId, actionTypeDetail, type, dateFrom, dateTo);
                if (data != null)
                {
                    totalRows = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllActivity(List<ActivityEntity> listActivity)
        {
            bool returnValue = false;
            try
            {
                var list = listActivity.Select(activity => new ActivitySearchEntity
                {
                    Id = activity.Id,
                    ApplicationId = activity.ApplicationId,
                    ActionTypeDetail = activity.ActionTypeDetail,
                    Type = activity.Type,
                    CreatedDate = activity.CreatedDate
                }).ToList();

                var fieldNameAnalyzer = "";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<ActivitySearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<ActivitySearchEntity>(list);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected ActivityDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
