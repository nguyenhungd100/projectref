﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.Kenh14.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Extension.Kenh14Extension
{
    public class QuoteDalFactory
    {
        #region Quote
        public static bool InitAllQuote(List<QuoteSearchEntity> listQuote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.InitAllQuote(listQuote);
            }
            return returnValue;
        }

        public static List<QuoteSearchEntity> Search(string keyword, int position, int pageIndex, int pageSize, ref int totalRow)
        {
            List<QuoteSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.Search(keyword, position, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool AddQuote(QuoteEntity quote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.AddQuote(quote);
            }
            return returnValue;
        }

        public static bool UpdateQuote(QuoteEntity quote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.UpdateQuote(quote);
            }
            return returnValue;
        }
        #endregion

        #region CharacterInfo
        public static bool InitAllCharacterInfo(List<CharacterInfoSearchEntity> listQuote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.InitAllCharacterInfo(listQuote);
            }
            return returnValue;
        }

        public static List<CharacterInfoSearchEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<CharacterInfoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static bool AddCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.AddCharacterInfo(quote);
            }
            return returnValue;
        }

        public static bool UpdateCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.UpdateCharacterInfo(quote);
            }
            return returnValue;
        }
        public static bool DeleteCharacterInfoById(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.QuoteDal.DeleteCharacterInfoById(id);
            }
            return returnValue;
        }
        #endregion
    }
}
