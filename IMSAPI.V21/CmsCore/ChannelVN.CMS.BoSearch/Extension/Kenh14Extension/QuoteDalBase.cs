﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.Kenh14.Entity;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Extension.Kenh14Extension
{
    public abstract class QuoteDalBase
    {
        #region Quote
        public bool InitAllQuote(List<QuoteSearchEntity> listQuote)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "title";

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<QuoteSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany(listQuote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }

        public List<QuoteSearchEntity> Search(string keyword, int position, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<QuoteSearchEntity>();
                var searchRequest = new SearchDescriptor<QuoteSearchEntity>();

                searchRequest.Sort(s => s.Ascending("position"));

                var positionTerm = new TermQuery();
                if (position > -1) {
                    positionTerm.Field = "position";
                    positionTerm.Value = position;
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        //.DefaultOperator(Operator.And)
                        .Type(TextQueryType.Phrase)
                   ) && positionTerm
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool AddQuote(QuoteEntity quote)
        {
            bool returnValue = false;
            try
            {
                var oQuote = new QuoteSearchEntity()
                {
                    Id = quote.QuoteId,
                    QuoteTitle = quote.QuoteTitle,
                    Avatar = quote.Avatar,
                    Quote = quote.Quote,
                    Position=quote.Position
                };

                var fieldNameAnalyzer = "title";
                returnValue = _db.CreateIndexSettings<QuoteSearchEntity>(fieldNameAnalyzer);
                returnValue = _db.Create<QuoteSearchEntity>(oQuote);
             
                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateQuote(QuoteEntity quote)
        {
            bool returnValue = false;
            try
            {
                if (quote != null)
                {
                    var oQuote = _db.Get<QuoteSearchEntity>(quote.QuoteId);
                    var zoneids = new List<string>();
                    
                    if (oQuote == null)
                    {
                        oQuote = new QuoteSearchEntity
                        {

                            Id = quote.QuoteId,
                            QuoteTitle = quote.QuoteTitle,
                            Avatar = quote.Avatar,
                            Quote = quote.Quote,
                            Position = quote.Position
                        };
                    }
                    else
                    {
                        oQuote.Id = quote.QuoteId;
                        oQuote.QuoteTitle = quote.QuoteTitle;
                        oQuote.Avatar = quote.Avatar;
                        oQuote.Quote = quote.Quote;
                        oQuote.Position = quote.Position;
                    }

                    returnValue = _db.Update<QuoteSearchEntity>(oQuote.Id, oQuote);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        #endregion

        #region CharacterInfo
        public bool InitAllCharacterInfo(List<CharacterInfoSearchEntity> listQuote)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "name";

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<CharacterInfoSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany(listQuote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }

        public List<CharacterInfoSearchEntity> SearchCharacterInfo(string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<CharacterInfoSearchEntity>();
                var searchRequest = new SearchDescriptor<CharacterInfoSearchEntity>();

                searchRequest.Sort(s => s.Descending("id"));

                var statusTerm = new TermQuery();
                if (status > -1)
                {
                    statusTerm.Field = "status";
                    statusTerm.Value = status;
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(new string[] { "name", "name.folded" }))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        //.DefaultOperator(Operator.And)
                        .Type(TextQueryType.Phrase)
                   ) && statusTerm
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool AddCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue = false;
            try
            {
                var oQuote = new CharacterInfoSearchEntity()
                {
                    Id = quote.Id,
                    Name = quote.Name,
                    Avatar = quote.Avatar,
                    Description=quote.Description,
                    Quote = quote.Quote,
                    BackgroundColor = quote.BackgroundColor,
                    BorderColor = quote.BorderColor,
                    Status = quote.Status                    
                };

                var fieldNameAnalyzer = "name";
                returnValue = _db.CreateIndexSettings<CharacterInfoSearchEntity>(fieldNameAnalyzer);
                returnValue = _db.Create(oQuote);

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue = false;
            try
            {
                if (quote != null)
                {
                    var oQuote = _db.Get<CharacterInfoSearchEntity>(quote.Id);
                    
                    if (oQuote == null)
                    {
                        oQuote = new CharacterInfoSearchEntity
                        {
                            Id = quote.Id,
                            Name = quote.Name,
                            Avatar = quote.Avatar,
                            Description = quote.Description,
                            Quote = quote.Quote,
                            BackgroundColor = quote.BackgroundColor,
                            BorderColor = quote.BorderColor,
                            Status = quote.Status
                        };
                    }
                    else
                    {
                        oQuote.Id = quote.Id;
                        oQuote.Name = quote.Name;
                        oQuote.Avatar = quote.Avatar;
                        oQuote.Description = quote.Description;
                        oQuote.Quote = quote.Quote;
                        oQuote.BackgroundColor = quote.BackgroundColor;
                        oQuote.BorderColor = quote.BorderColor;
                        oQuote.Status = quote.Status;
                    }

                    returnValue = _db.Update<CharacterInfoSearchEntity>(oQuote.Id, oQuote);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool DeleteCharacterInfoById(int id)
        {
            bool returnValue = false;
            try
            { 
                returnValue = _db.DeleteMapping<CharacterInfoSearchEntity>(id);

                return returnValue;
            }
            catch { return returnValue; }
        }
        #endregion

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected QuoteDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
