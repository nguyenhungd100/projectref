﻿using ChannelVN.CMS.Common.ESClientHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Common
{
    public abstract class SearchDbBase: ESClientHelper, IDisposable
    {
        #region Constructors

        protected SearchDbBase()
            : this(string.Empty)
        {
        }

        protected SearchDbBase(string channelNamespace)
            : base(channelNamespace)
        {            
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
