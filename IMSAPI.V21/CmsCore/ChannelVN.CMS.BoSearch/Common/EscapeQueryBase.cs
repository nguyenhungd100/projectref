﻿using System;
using System.Text.RegularExpressions;

namespace ChannelVN.CMS.BoSearch.Common
{
    public static class EscapeQueryBase
    {
        public static string EscapeSearchQuery(string query)
        {
            if (string.IsNullOrWhiteSpace(query)) return query;

            var sb = Regex.Replace(query, @"([^\w\s.])", " ");            

            //&& || not handled here
            //char[] special = { '+', '-', '=', '>', '<', '!', '(', ')', '{', '}', '[', ']', '^', '\"', '~', '*', '?', ':', '\\', '/', ' ' };
            //char[] qArray = query.ToCharArray();

            //StringBuilder sb = new StringBuilder();

            //foreach (var chr in qArray)
            //{
            //    if (special.Contains(chr))
            //    {
            //        sb.Append(String.Format("\\{0}", chr));
            //    }
            //    else
            //    {
            //        sb.Append(chr);
            //    }
            //}

            return sb.ToString();
        }
    }
}
