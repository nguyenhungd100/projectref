﻿using Nest;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class StreamItemSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Number(NumberType.Integer, Name = "order")]
        public long Order { get; set; }

        [Number(NumberType.Integer, Name = "type_id")]
        public int TypeId { get; set; }

        [Text(Name = "template_id")]
        public string TemplateId { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Date(Name = "published_date")]
        public DateTime PublishedDate { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Text(Name = "data_json")]
        public string DataJson { get; set; }

        [Number(NumberType.Integer, Name = "mode")]
        public int Mode { get; set; }

        [Text(Name = "title", Index =true)]
        public string Title { get; set; }
    }

    [ElasticsearchType]
    public class StreamItemDetailSearchEntity
    {
        [Number(NumberType.Integer, Name = "item_id")]
        public long ItemId { get; set; }

        [Number(NumberType.Integer, Name = "item_type_id")]
        public int ItemTypeId { get; set; }

        [Text(Name = "item_template_id")]
        public string ItemTemplateId { get; set; }


        [Number(NumberType.Integer, Name = "object_id")]
        public long ObjectId { get; set; }

        [Number(NumberType.Integer, Name = "object_type")]
        public int ObjectType { get; set; }

        [Text(Name = "title")]
        public string Title { get; set; }

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }

        [Number(NumberType.Integer, Name = "type")]
        public int Type { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }        
    }

    [ElasticsearchType]
    public class StreamItemMobileSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Number(NumberType.Integer, Name = "order")]
        public long Order { get; set; }

        [Number(NumberType.Integer, Name = "type_id")]
        public int TypeId { get; set; }

        [Text(Name = "template_id")]
        public string TemplateId { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Date(Name = "published_date")]
        public DateTime PublishedDate { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Text(Name = "data_json")]
        public string DataJson { get; set; }
    }

    [ElasticsearchType]
    public class StreamItemMobileDetailSearchEntity
    {
        [Number(NumberType.Integer, Name = "item_id")]
        public long ItemId { get; set; }

        [Number(NumberType.Integer, Name = "item_type_id")]
        public int ItemTypeId { get; set; }

        [Text(Name = "item_template_id")]
        public string ItemTemplateId { get; set; }


        [Number(NumberType.Integer, Name = "object_id")]
        public long ObjectId { get; set; }

        [Number(NumberType.Integer, Name = "object_type")]
        public int ObjectType { get; set; }

        [Text(Name = "title")]
        public string Title { get; set; }

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }

        [Number(NumberType.Integer, Name = "type")]
        public int Type { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }
    }
}
