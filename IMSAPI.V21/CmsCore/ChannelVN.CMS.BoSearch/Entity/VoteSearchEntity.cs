﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class VoteSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }
        
        [Text(Name = "zone_ids", Index = true)]
        public string[] ZoneIds { get; set; }        

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
