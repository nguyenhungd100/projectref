﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class TagSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public long Id { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Number(NumberType.Integer, Name = "parent_id")]              
        public long ParentId { get; set; }

        [Boolean(Name = "is_thread")]
        public bool IsThread { get; set; }

        [Boolean(Name = "is_hot_tag")]
        public bool IsHotTag { get; set; }

        [Boolean(Name = "invisibled")]
        public bool Invisibled { get; set; }

        [Number(NumberType.Integer, Name = "zone_id", Index = true)]
        public int ZoneId { get; set; }        

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Number(NumberType.Integer, Name = "news_count")]
        public int NewsCount { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }
    }
}
