﻿using Nest;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class VideoSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Number(NumberType.Integer, Name = "zone_id")]
        public int ZoneId { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]              
        public string Name { get; set; }

        [Text(Name = "zone_ids")]
        public string[] ZoneIds { get; set; }

        [Text(Name = "playlist_ids")]
        public string[] PlaylistIds { get; set; }
        
        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Text(Name = "edited_by")]
        public string EditedBy { get; set; }

        [Text(Name = "publish_by")]
        public string PublishBy { get; set; }

        [Date(Name = "Publish_date")]
        public DateTime PublishDate { get; set; }

        [Text(Name = "last_modified_by")]
        public string LastModifiedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Date(Name = "edited_date")]
        public DateTime EditedDate { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Number(NumberType.Integer, Name = "mode")]
        public int Mode { get; set; }

        [Number(NumberType.Integer, Name ="status", Index = true)]
        public int Status { get; set; }

        [Number(NumberType.Integer, Name = "is_video_youtube")]
        public int IsVideoYoutube { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Number(NumberType.Integer, Name = "view_count")]
        public int Views { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Text(Name = "html_code")]
        public string HtmlCode { get; set; }

        [Text(Name = "keyvideo")]
        public string KeyVideo { get; set; }

        [Number(NumberType.Integer, Name = "parent_id")]
        public int ParentId { get; set; }

        [Number(NumberType.Integer, Name = "type")]
        public int Type { get; set; }

        [Text(Name = "list_type")]
        public string[] ListType { get; set; }

        [Text(Name = "channel_ids")]
        public string[] ChannelIds { get; set; }

        [Text(Name = "playlist_name")]
        public string[] PlaylistName { get; set; }

        [Text(Name = "channel_name")]
        public string[] ChannelName { get; set; }

        [Text(Name = "duration")]
        public string Duration { get; set; }

        [Number(NumberType.Integer, Name = "original_id")]
        public int OriginalId { get; set; }

        [Text(Name = "description")]
        public string Description { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Text(Name = "filename")]
        public string FileName { get; set; }

        [Text(Name = "name_space")]
        public string Namespace { get; set; }

        [Text(Name = "is_on_home")]
        public bool IsOnHome { get; set; }

        [Text(Name = "is_amp")]
        public bool IsAMP { get; set; }

        [Text(Name = "source_id")]
        public int SourceId { get; set; }
    }

    [ElasticsearchType]
    public class Video
    {
        [Number(NumberType.Long, Name = "Id")]
        public long Id { get; set; }

        [Number(NumberType.Integer, Name = "ZoneId")]
        public int ZoneId { get; set; }

        [Text(Name = "Name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "AllZone")]
        public string[] AllZone { get; set; }        
        
        [Date(Name = "PublishDate")]
        public DateTime PublishDate { get; set; }        
        
        [Text(Name = "ZoneName")]
        public string ZoneName { get; set; }
       
        [Text(Name = "Avatar")]
        public string Avatar { get; set; }        

        [Text(Name = "KeyVideo")]
        public string KeyVideo { get; set; }                

        [Text(Name = "Url")]
        public string Url { get; set; }

        [Text(Name = "Description")]
        public string Description { get; set; }

        [Text(Name = "Duration")]
        public string Duration { get; set; }

        [Text(Name = "FileName")]
        public string FileName { get; set; }

        [Text(Name = "UnsignName")]
        public string UnsignName { get; set; }

        [Text(Name = "PName")]
        public string PName { get; set; }

        [Number(NumberType.Integer, Name = "Row")]
        public int Row { get; set; }

        [Number(NumberType.Integer, Name = "IsClone")]
        public int IsClone { get; set; }
    }
}
