﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class PlayListSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Number(NumberType.Integer, Name = "zone_id")]
        public int ZoneId { get; set; }

        [Text(Name = "zone_ids", Index = true)]
        public string[] ZoneIds { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Number(NumberType.Integer, Name = "mode")]
        public int Mode { get; set; }

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "last_modified_by")]
        public string LastModifiedBy { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Date(Name = "published_date")]
        public DateTime PublishedDate { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Number(NumberType.Integer, Name = "video_count")]
        public int VideoCount { get; set; }

        [Number(NumberType.Integer, Name = "follow_count")]
        public int FollowCount { get; set; }

        [Text(Name = "edited_by")]
        public string EditedBy { get; set; }

        [Date(Name = "edited_date")]
        public DateTime EditedDate { get; set; }

        [Number(NumberType.Integer, Name = "video_count_not_publish")]
        public int VideoCountNotPublish { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }
    }
}
