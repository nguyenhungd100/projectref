﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class QuoteSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }
        
        [Text(Name = "title", Index = true, Analyzer = "standard")]              
        public string QuoteTitle { get; set; }        

        [Text(Name = "avatar",Index = true, Analyzer = "standard")]
        public string Avatar { get; set; }
      
        [Text(Name = "quote", Index = true, Analyzer = "standard")]
        public string Quote { get; set; }

        [Number(NumberType.Integer, Name = "position")]
        public int Position { get; set; }
    }

    [ElasticsearchType]
    public class CharacterInfoSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "avatar", Index = true, Analyzer = "standard")]
        public string Avatar { get; set; }

        [Text(Name = "description")]
        public string Description { get; set; }

        [Text(Name = "quote")]
        public string Quote { get; set; }

        [Text(Name = "background_color")]
        public string BackgroundColor { get; set; }

        [Text(Name = "border_color")]
        public string BorderColor { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }
    }
}
