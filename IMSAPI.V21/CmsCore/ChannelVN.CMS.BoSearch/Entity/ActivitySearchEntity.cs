﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class ActivitySearchEntity
    {
        [Number(NumberType.Long, Name = "id")]
        public long Id { get; set; }

        [Number(NumberType.Long, Name = "application_id")]
        public long ApplicationId { get; set; }

        [Number(NumberType.Integer, Name = "action_type_detail")]
        public int ActionTypeDetail { get; set; }

        [Number(NumberType.Integer, Name = "type")]
        public int Type { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }        
    }
}
