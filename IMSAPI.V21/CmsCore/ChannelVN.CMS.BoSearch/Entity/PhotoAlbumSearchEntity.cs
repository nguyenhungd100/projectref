﻿using Nest;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class PhotoAlbumSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Number(NumberType.Integer, Name = "zone_id")]
        public int ZoneId { get; set; }        

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "thumb_image")]
        public string ThumbImage { get; set; }       

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "modified_by")]
        public string ModifiedBy { get; set; }

        [Date(Name = "modified_date")]
        public DateTime ModifiedDate { get; set; }
        
        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Text(Name = "tags")]
        public string Tags { get; set; }

        [Text(Name = "description")]
        public string Description { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "event_ids", Index = true)]
        public string[] EventIds { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }
    }
}
