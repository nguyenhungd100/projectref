﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class EndUserSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Text(Name = "email")]
        public string Email { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "viet_id")]
        public string VietId { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }
       
        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Number(NumberType.Integer, Name = "sex")]
        public int Sex { get; set; }
    }
}
