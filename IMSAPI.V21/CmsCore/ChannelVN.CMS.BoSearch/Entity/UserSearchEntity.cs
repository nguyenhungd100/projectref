﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class UserSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }
        
        [Text(Name = "fullname", Index = true, Analyzer = "standard")]              
        public string FullName { get; set; }

        [Text(Name = "username", Index = true, Analyzer = "standard")]
        public string UserName { get; set; }

        [Number(NumberType.Integer, Name ="status")]
        public int Status { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "staff_code")]
        public string StaffCode { get; set; }
    }
}
