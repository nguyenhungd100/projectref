﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]
    public class PhotoByAuthorSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Number(NumberType.Long, Name = "photo_id", Index = true)]
        public long PhotoId { get; set; }

        [Number(NumberType.Integer, Name = "author_id", Index = true)]
        public int AuthorId { get; set; }
    }  
}
