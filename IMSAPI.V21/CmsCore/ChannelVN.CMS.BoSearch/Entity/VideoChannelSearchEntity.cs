﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class VideoChannelSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Number(NumberType.Integer, Name = "zone_id")]
        public int ZoneId { get; set; }

        [Number(NumberType.Integer, Name = "publisher_id")]
        public int PublisherId { get; set; }

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Text(Name = "zone_ids", Index = true)]
        public string[] ZoneIds { get; set; }

        [Text(Name = "label_ids", Index = true)]
        public string[] Labelds { get; set; }

        [Text(Name = "playlist_ids", Index = true)]
        public string[] PlayListIds { get; set; }

        [Text(Name = "video_ids", Index = true)]
        public string[] VideoIds { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Number(NumberType.Integer, Name = "follow_count")]
        public int FollowCount { get; set; }

        [Number(NumberType.Integer, Name = "video_count")]

        public int VideoCount { get; set; }
        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Number(NumberType.Integer, Name = "mode")]
        public int Mode { get; set; }
    }
}
