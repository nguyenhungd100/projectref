﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class NewsAuthorSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Text(Name = "unsign_name", Index = true, Analyzer = "standard")]
        public string UnsignName { get; set; }

        [Text(Name = "author_name", Index = true, Analyzer = "standard")]              
        public string AuthorName { get; set; }

        [Text(Name = "user_data", Index = true, Analyzer = "standard")]
        public string UserData { get; set; }
    }
}
