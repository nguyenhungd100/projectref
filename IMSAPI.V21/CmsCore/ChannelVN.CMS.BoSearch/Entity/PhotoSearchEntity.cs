﻿using Nest;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class PhotoSearchEntity
    {
        [Number(NumberType.Long, Name = "id")]
        public long Id { get; set; }

        [Number(NumberType.Integer, Name = "zone_id")]
        public int ZoneId { get; set; }        

        [Text(Name = "name", Index = true, Analyzer = "standard")]
        public string Name { get; set; }

        [Text(Name = "image_url")]
        public string ImageUrl { get; set; }

        [Text(Name = "image_note")]
        public string ImageNote { get; set; }

        [Text(Name = "size")]
        public string Size { get; set; }

        [Number(NumberType.Double, Name = "capacity")]
        public double Capacity { get; set; }

        [Text(Name = "note")]
        public string Note { get; set; }

        [Text(Name = "created_by")]
        public string CreatedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Text(Name = "modified_by")]
        public string ModifiedBy { get; set; }

        [Date(Name = "modified_date")]
        public DateTime ModifiedDate { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Date(Name = "published_date")]
        public DateTime PublishedDate { get; set; }

        [Number(NumberType.Integer, Name = "status")]
        public int Status { get; set; }

        [Text(Name = "tags")]
        public string Tags { get; set; }

        [Text(Name = "original_name")]
        public string OriginalName { get; set; }

        [Text(Name = "un_signname")]
        public string UnSignName { get; set; }

        [Number(NumberType.Integer, Name = "album_id")]
        public int AlbumId { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }

        [Text(Name = "image_preview_url")]
        public string ImagePreviewUrl { get; set; }

        [Text(Name = "image_distribution")]
        public string ImageDistribution { get; set; }

        [Number(NumberType.Integer, Name = "width")]
        public int Width { get; set; }

        [Number(NumberType.Integer, Name = "height")]
        public int Height { get; set; }

        [Number(NumberType.Short, Name = "image_dimension")]
        public short ImageDimension { get; set; }

        [Text(Name = "image_exif")]
        public string ImageExif { get; set; }

        [Text(Name = "restriction_content")]
        public string RestrictionContent { get; set; }

        [Text(Name = "location")]
        public string Location { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "album_name")]
        public string AlbumName { get; set; }
    }
}
