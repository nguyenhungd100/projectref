﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class DiscussionTopicSearchEntity
    {
        [Number(NumberType.Integer, Name = "id")]
        public int Id { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }
        
        [Number(NumberType.Integer, Name = "type")]
        public int Type { get; set; }

        [Boolean(Name = "status")]
        public bool Status { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }
    }
}
