﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Entity
{
    public class CountStatus
    {
        public string Status { get; set; }
        public long? Count { get; set; }
    }
}
