﻿using System.Runtime.Serialization;
using Nest;
using Elasticsearch.Net;
using System;

namespace ChannelVN.CMS.BoSearch.Entity
{
    [ElasticsearchType]    
    public class NewsSearchEntity
    {
        [Number(NumberType.Long, Name = "id", Index = true)]
        public long Id { get; set; }

        [Text(Name = "encrypt_id")]
        public string EncryptId { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }

        [Text(Name = "zone_ids", Index = true, Analyzer = "standard")]
        public string[] ZoneIds { get; set; }

        [Number(NumberType.Integer, Name = "type", Index = true)]
        public int Type { get; set; }

        [Keyword(Name = "created_by", Index = true)]
        public string CreatedBy { get; set; }

        [Keyword(Name = "last_modified_by")]
        public string LastModifiedBy { get; set; }

        [Keyword(Name = "edited_by", Index = true)]
        public string EditedBy { get; set; }

        [Keyword(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Keyword(Name = "last_receiver")]
        public string LastReceiver { get; set; }

        [Keyword(Name = "approved_by", Index = true)]
        public string ApprovedBy { get; set; }

        [Keyword(Name = "returned_by")]
        public string ReturnedBy { get; set; }

        [Date(Name = "created_date", Index = true)]
        public DateTime CreatedDate { get; set; }

        [Date(Name = "distribution_date", Index = true)]
        public DateTime DistributionDate { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Date(Name = "approved_date")]
        public DateTime ApprovedDate { get; set; }

        [Number(NumberType.Integer, Name = "view_count")]
        public int ViewCount { get; set; }

        [Number(NumberType.Integer, Name = "status", Index = true)]
        public int Status { get; set; }

        [Boolean(Name = "is_on_home")]
        public bool IsOnHome { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Number(NumberType.Integer, Name = "view_royalties")]
        public int ViewRoyalties { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "note")]
        public string Note { get; set; }

        [Text(Name = "author", Index = true, Analyzer = "standard")]
        public string Author { get; set; }

        [Text(Name = "error_checked_by")]
        public string ErrorCheckedBy { get; set; }

        [Date(Name = "error_checked_date")]
        public DateTime ErrorCheckedDate { get; set; }

        [Keyword(Name = "sensitive_checked_by")]
        public string SensitiveCheckedBy { get; set; }

        [Date(Name = "sensitive_checked_date")]
        public DateTime SensitiveCheckedDate { get; set; }

        [Number(NumberType.Integer, Name = "word_count")]
        public int WordCount { get; set; }

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Number(NumberType.Integer, Name = "display_posotion", Index = true)]
        public int DisplayPosition { get; set; }

        [Number(NumberType.Integer, Name = "priority")]
        public int Priority { get; set; }

        [Text(Name = "source", Index = true, Analyzer = "standard")]
        public string Source { get; set; }

        [Boolean(Name = "is_focus")]
        public bool IsFocus { get; set; }

        [Number(NumberType.Integer, Name = "news_type")]
        public int NewsType { get; set; }

        [Number(NumberType.Integer, Name = "seo_review_status")]
        public int SeoReviewStatus { get; set; }

        [Keyword(Name = "seo_modified_by")]
        public string SeoModifiedBy { get; set; }

        [Keyword(Name = "seo_review_by")]
        public string SeoReviewBy { get; set; }

        [Date(Name = "seo_review_date")]
        public DateTime SeoReviewDate { get; set; }

        [Boolean(Name = "is_prod")]
        public bool IsProd { get; set; }

        [Boolean(Name = "is_pr", Index = true)]
        public bool IsPr { get; set; }

        [Number(NumberType.Integer, Name = "original_id")]
        public int OriginalId { get; set; }

        [Number(NumberType.Integer,Name="zone_id")]
        public int ZoneId { get; set; }
    }

    [ElasticsearchType]
    public class NewsSearchEntity_bk
    {
        [Number(NumberType.Long, Name = "id", Index = true)]
        public long Id { get; set; }

        [Text(Name = "encrypt_id")]
        public string EncryptId { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }

        [Text(Name = "zone_ids", Index = true, Analyzer = "standard")]
        public string[] ZoneIds { get; set; }

        [Number(NumberType.Integer, Name = "type", Index = true)]
        public int Type { get; set; }

        [Text(Name = "created_by", Index = true, Analyzer = "standard")]
        public string CreatedBy { get; set; }

        [Text(Name = "last_modified_by", Index = true, Analyzer = "standard")]
        public string LastModifiedBy { get; set; }

        [Text(Name = "edited_by", Index = true, Analyzer = "standard")]
        public string EditedBy { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Text(Name = "last_receiver", Index = true, Analyzer = "standard")]
        public string LastReceiver { get; set; }

        [Text(Name = "approved_by", Index = true, Analyzer = "standard")]
        public string ApprovedBy { get; set; }

        [Text(Name = "returned_by", Index = true, Analyzer = "standard")]
        public string ReturnedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Date(Name = "approved_date")]
        public DateTime ApprovedDate { get; set; }

        [Number(NumberType.Integer, Name = "view_count")]
        public int ViewCount { get; set; }

        [Number(NumberType.Integer, Name = "status", Index = true)]
        public int Status { get; set; }

        [Boolean(Name = "is_on_home")]
        public bool IsOnHome { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Number(NumberType.Integer, Name = "view_royalties")]
        public int ViewRoyalties { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "note")]
        public string Note { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }

        [Text(Name = "error_checked_by")]
        public string ErrorCheckedBy { get; set; }

        [Date(Name = "error_checked_date")]
        public DateTime ErrorCheckedDate { get; set; }

        [Text(Name = "sensitive_checked_by")]
        public string SensitiveCheckedBy { get; set; }

        [Date(Name = "sensitive_checked_date")]
        public DateTime SensitiveCheckedDate { get; set; }

        [Number(NumberType.Integer, Name = "word_count")]
        public int WordCount { get; set; }

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }

        [Number(NumberType.Integer, Name = "display_posotion")]
        public int DisplayPosition { get; set; }
    }

    public class NewsSearchViewEntity
    {
        [Number(NumberType.Long, Name = "id", Index = true)]
        public long Id { get; set; }

        [Text(Name = "encrypt_id")]
        public string EncryptId { get; set; }

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }

        [Text(Name = "zone_ids", Index = true, Analyzer = "standard")]
        public string[] ZoneIds { get; set; }

        [Number(NumberType.Integer, Name = "type", Index = true)]
        public int Type { get; set; }

        [Text(Name = "created_by", Index = true, Analyzer = "standard")]
        public string CreatedBy { get; set; }

        [Text(Name = "last_modified_by", Index = true, Analyzer = "standard")]
        public string LastModifiedBy { get; set; }

        [Text(Name = "edited_by", Index = true, Analyzer = "standard")]
        public string EditedBy { get; set; }

        [Text(Name = "published_by")]
        public string PublishedBy { get; set; }

        [Text(Name = "last_receiver", Index = true, Analyzer = "standard")]
        public string LastReceiver { get; set; }

        [Text(Name = "approved_by", Index = true, Analyzer = "standard")]
        public string ApprovedBy { get; set; }

        [Text(Name = "returned_by", Index = true, Analyzer = "standard")]
        public string ReturnedBy { get; set; }

        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }

        [Date(Name = "distribution_date")]
        public DateTime DistributionDate { get; set; }

        [Date(Name = "last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        [Date(Name = "approved_date")]
        public DateTime ApprovedDate { get; set; }

        [Number(NumberType.Integer, Name = "view_count")]
        public int ViewCount { get; set; }

        [Number(NumberType.Integer, Name = "view_pc_count")]
        public int ViewPcCount { get; set; }

        [Number(NumberType.Integer, Name = "view_mob_count")]
        public int ViewMobCount { get; set; }

        [Number(NumberType.Integer, Name = "status", Index = true)]
        public int Status { get; set; }

        [Boolean(Name = "is_on_home")]
        public bool IsOnHome { get; set; }

        [Text(Name = "avatar")]
        public string Avatar { get; set; }

        [Number(NumberType.Integer, Name = "view_royalties")]
        public int ViewRoyalties { get; set; }

        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }

        [Text(Name = "note")]
        public string Note { get; set; }

        [Text(Name = "author")]
        public string Author { get; set; }

        [Text(Name = "error_checked_by")]
        public string ErrorCheckedBy { get; set; }

        [Date(Name = "error_checked_date")]
        public DateTime ErrorCheckedDate { get; set; }

        [Text(Name = "sensitive_checked_by")]
        public string SensitiveCheckedBy { get; set; }

        [Date(Name = "sensitive_checked_date")]
        public DateTime SensitiveCheckedDate { get; set; }

        [Number(NumberType.Integer, Name = "word_count")]
        public int WordCount { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }        
    }

    [ElasticsearchType]
    public class NewsSearchForAdStoreEntity
    {
        [Number(NumberType.Long, Name = "id", Index = true)]
        public long Id { get; set; }        

        [Text(Name = "title", Index = true, Analyzer = "standard")]
        public string Title { get; set; }

        [Number(Name = "zone_id", Index = true)]
        public int ZoneId { get; set; }

        [Boolean(Name = "adstore", Index = true)]
        public bool AdStore { get; set; }

        [Keyword(Name = "adstore_url")]
        public string AdStoreUrl { get; set; }

        [Boolean(Name = "ispr", Index = true)]
        public bool IsPr { get; set; }        

        [Date(Name = "distribution_date", Index = true)]
        public DateTime DistributionDate { get; set; }        

        [Number(NumberType.Integer, Name = "status", Index = true)]
        public int Status { get; set; }        

        [Text(Name = "avatar")]
        public string Avatar { get; set; }
       
        [Text(Name = "zone_name")]
        public string ZoneName { get; set; }        

        [Text(Name = "sapo")]
        public string Sapo { get; set; }

        [Text(Name = "url")]
        public string Url { get; set; }        

        [Number(NumberType.Integer, Name = "pr_position")]
        public int PrPosition { get; set; }        
    }
}
