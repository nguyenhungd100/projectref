﻿using ChannelVN.CMS.BoSearch.Base.Account;
using ChannelVN.CMS.BoSearch.Base.EndUser;
using ChannelVN.CMS.BoSearch.Base.News;
using ChannelVN.CMS.BoSearch.Base.NewsAuthor;
using ChannelVN.CMS.BoSearch.Base.Photo;
using ChannelVN.CMS.BoSearch.Base.PlayList;
using ChannelVN.CMS.BoSearch.Base.StreamItem;
using ChannelVN.CMS.BoSearch.Base.Tag;
using ChannelVN.CMS.BoSearch.Base.Thread;
using ChannelVN.CMS.BoSearch.Base.Video;
using ChannelVN.CMS.BoSearch.Base.VideoChannel;
using ChannelVN.CMS.BoSearch.Base.Vote;
using ChannelVN.CMS.BoSearch.CmsExtension.DiscussionV2;
using ChannelVN.CMS.BoSearch.CmsExtension.SocialNetwork;
using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Extension.Kenh14Extension;

namespace ChannelVN.CMS.BoSearch.Databases
{
    public abstract class CmsMainSearchDbBase : SearchDbBase
    {
        #region UserDal
        private UserDal _userDal;
        public UserDal UserDal
        {
            get { return _userDal ?? (_userDal = new UserDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region NewsDal
        private NewsDal _newsDal;
        public NewsDal NewsDal
        {
            get { return _newsDal ?? (_newsDal = new NewsDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region NewsAuthorDal
        private NewsAuthorDal _newsAuthorDal;
        public NewsAuthorDal NewsAuthorDal
        {
            get { return _newsAuthorDal ?? (_newsAuthorDal = new NewsAuthorDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region ActivityDal
        private ActivityDal _activityDal;
        public ActivityDal ActivityDal
        {
            get { return _activityDal ?? (_activityDal = new ActivityDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region VideoDal
        private VideoDal _videoDal;
        public VideoDal VideoDal
        {
            get { return _videoDal ?? (_videoDal = new VideoDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region VideoYoutubeDal
        private VideoYoutubeDal _videoYoutubeDal;
        public VideoYoutubeDal VideoYoutubeDal
        {
            get { return _videoYoutubeDal ?? (_videoYoutubeDal = new VideoYoutubeDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region DiscussionV2Dal
        private DiscussionV2Dal _discussionV2Dal;
        public DiscussionV2Dal DiscussionV2Dal
        {
            get { return _discussionV2Dal ?? (_discussionV2Dal = new DiscussionV2Dal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region TagDal
        private TagDal _tagDal;
        public TagDal TagDal
        {
            get { return _tagDal ?? (_tagDal = new TagDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region QuoteDal
        private QuoteDal _quoteDal;
        public QuoteDal QuoteDal
        {
            get { return _quoteDal ?? (_quoteDal = new QuoteDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region ThreadDal
        private ThreadDal _threadDal;
        public ThreadDal ThreadDal
        {
            get { return _threadDal ?? (_threadDal = new ThreadDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region VoteDal
        private VoteDal _voteDal;
        public VoteDal VoteDal
        {
            get { return _voteDal ?? (_voteDal = new VoteDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region VideoChannelDal
        private VideoChannelDal _videoChannelDal;
        public VideoChannelDal VideoChannelDal
        {
            get { return _videoChannelDal ?? (_videoChannelDal = new VideoChannelDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region PlayListDal
        private PlayListDal _playListDal;
        public PlayListDal PlayListDal
        {
            get { return _playListDal ?? (_playListDal = new PlayListDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region EndUserDal
        private EndUserDal _endUserDal;
        public EndUserDal EndUserDal
        {
            get { return _endUserDal ?? (_endUserDal = new EndUserDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region StreamItemDal
        private StreamItemDal _streamItemDal;
        public StreamItemDal StreamItemDal
        {
            get { return _streamItemDal ?? (_streamItemDal = new StreamItemDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region Photo
        private PhotoDal _PhotoDal;
        public PhotoDal PhotoDal
        {
            get { return _PhotoDal ?? (_PhotoDal = new PhotoDal((CmsMainSearchDb)this)); }
        }

        private PhotoByAuthorDal _PhotoByAuthorDal;
        public PhotoByAuthorDal PhotoByAuthorDal
        {
            get { return _PhotoByAuthorDal ?? (_PhotoByAuthorDal = new PhotoByAuthorDal((CmsMainSearchDb)this)); }
        }

        private AlbumByAuthorDal _AlbumByAuthorDal;
        public AlbumByAuthorDal AlbumByAuthorDal
        {
            get { return _AlbumByAuthorDal ?? (_AlbumByAuthorDal = new AlbumByAuthorDal((CmsMainSearchDb)this)); }
        }

        private PhotoAlbumDal _PhotoAlbumDal;
        public PhotoAlbumDal PhotoAlbumDal
        {
            get { return _PhotoAlbumDal ?? (_PhotoAlbumDal = new PhotoAlbumDal((CmsMainSearchDb)this)); }
        }
        #endregion

        #region Constructors

        protected CmsMainSearchDbBase()
        {
            Initialize();
        }

        #endregion

        #region Protected methods

        protected abstract void Initialize();

        #endregion

    }
}
