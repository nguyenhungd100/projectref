﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Databases
{
    public class CmsMainSearchDb : CmsMainSearchDbBase
    {
        private const string ConnectionStringName = "CmsMainSearchDb";
        private const string ConnectionStringName_Dev = "CmsMainSearchDb_Dev";

        /// <summary>
        /// Initializes the information to connect the database.
        /// </summary>
        protected override void Initialize()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);

            //if (AppSettings.GetBool("IsDevEnv"))
            //{
            //    strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
            //    ConnectionStringName_Dev, Constants.ConnectionDecryptKey);
            //}

            this.ConnectionString = strConn;
            this.NameSpace = WcfMessageHeader.Current.Namespace;
        }
    }
}
