﻿using ChannelVN.CMS.BoSearch.Base.Video;
using ChannelVN.CMS.BoSearch.Common;

namespace ChannelVN.CMS.BoSearch.Databases
{
    public abstract class CmsVTVSearchDbBase : SearchDbBase
    {        
        #region VideoVTVDal
        private VideoVTVDal _videoVTVDal;
        public VideoVTVDal VideoVTVDal
        {
            get { return _videoVTVDal ?? (_videoVTVDal = new VideoVTVDal((CmsVTVSearchDb)this)); }
        }
        #endregion        

        #region Constructors

        protected CmsVTVSearchDbBase()
        {
            Initialize();
        }

        #endregion

        #region Protected methods

        protected abstract void Initialize();

        #endregion

    }
}
