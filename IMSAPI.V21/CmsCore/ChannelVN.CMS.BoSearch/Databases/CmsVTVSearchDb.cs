﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;

namespace ChannelVN.CMS.BoSearch.Databases
{
    public class CmsVTVSearchDb : CmsVTVSearchDbBase
    {
        private const string ConnectionStringName = "CmsVTVSearchDb";
        private const string ConnectionStringName_Dev = "CmsVTVSearchDb_Dev";

        /// <summary>
        /// Initializes the information to connect the database.
        /// </summary>
        protected override void Initialize()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);
            
            if (AppSettings.GetBool("IsDevEnv"))
            {                                
                strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName_Dev, Constants.ConnectionDecryptKey);
            }
            
            this.ConnectionString = strConn;
            this.NameSpace = WcfMessageHeader.Current.Namespace;
        }
    }
}
