﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.PlayList
{
    public abstract class PlayListDalBase
    {
        public bool AddPlayList(PlayListSearchEntity playList)
        {
            bool returnValue = false;
            try
            {
                if (playList != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<PlayListSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdatePlayList(PlayListSearchEntity playList)
        {
            bool returnValue = false;
            try
            {
                if (playList != null)
                {
                    var playListDb = _db.Get<PlayListSearchEntity>(playList.Id);
                    if (playListDb != null)
                    {
                        playList.CreatedBy = playListDb.CreatedBy;
                        playList.CreatedDate = playListDb.CreatedDate;
                    }
                    returnValue = _db.Update<PlayListSearchEntity>(playList.Id, playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        public bool Publish(int playListId, string account)
        {
            bool returnValue = false;
            try
            {
                var playList = _db.Get<PlayListSearchEntity>(playListId);
                if (playList != null)
                {
                    playList.Status = (int)EnumPlayListStatus.Published;
                    playList.PublishedBy = account;
                    playList.PublishedDate = DateTime.Now;
                    returnValue = _db.Update<PlayListSearchEntity>(playList.Id, playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UnPublish(int playListId, string account)
        {
            bool returnValue = false;
            try
            {
                var playList = _db.Get<PlayListSearchEntity>(playListId);
                if (playList != null)
                {
                    playList.Status = (int)EnumPlayListStatus.Unpublished;
                    returnValue = _db.Update<PlayListSearchEntity>(playList.Id, playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool MoveTrash(int playListId, string account)
        {
            bool returnValue = false;
            try
            {
                var playList = _db.Get<PlayListSearchEntity>(playListId);
                if (playList != null)
                {
                    playList.Status = (int)EnumPlayListStatus.MoveTrash;
                    playList.LastModifiedBy = account;
                    playList.LastModifiedDate = DateTime.Now;
                    returnValue = _db.Update<PlayListSearchEntity>(playList.Id, playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool ChangeStatus(int playListId, int status, string username)
        {
            bool returnValue = false;
            try
            {
                if (playListId <= 0)
                {
                    return returnValue;
                }
                var playList = _db.Get<PlayListSearchEntity>(playListId);
                if (playList != null)
                {
                    playList.Status = status;
                    playList.EditedBy = username;
                    playList.EditedDate = DateTime.Now;
                    playList.LastModifiedBy = username;
                    playList.LastModifiedDate = DateTime.Now;
                    if(status == (int)EnumPlayListStatus.Published)
                    {
                        playList.PublishedBy = username;
                        playList.PublishedDate = DateTime.Now;
                    }                    
                }

                return _db.Update<PlayListSearchEntity>(playListId, playList);
            }
            catch { return returnValue; }
        }

        public bool ChangeMode(string account, int playListId, int mode)
        {
            bool returnValue = false;
            try
            {
                var playList = _db.Get<PlayListSearchEntity>(playListId);
                if (playList != null)
                {
                    playList.Mode = mode;
                    returnValue = _db.Update<PlayListSearchEntity>(playList.Id, playList);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool Delete(List<string> listIds)
        {
            bool returnValue = false;
            try
            {
                var listDelete = GetListByListId(listIds);
                returnValue = _db.DeleteMappingMany(listDelete);

                return returnValue;
            }
            catch { return returnValue; }
        }

        public List<PlayListSearchEntity> GetListByListId(List<string> ids)
        {
            try
            {
                var list = new List<PlayListSearchEntity>();
                var searchRequest = new SearchDescriptor<PlayListSearchEntity>();

                var termsIds = new TermsQuery();
                termsIds.Field = "id";
                termsIds.Terms = ids.ToArray();

                searchRequest.Query(q => q && termsIds);

                var data = _db.Search(searchRequest);
                if (data != null)
                {                    
                    list = data.Data.ToList();
                }
                return list;
            }
            catch
            {
                return null;
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="zoneIds"></param>
        /// <param name="labelIds"></param>
        /// <param name="playlistIds"></param>
        /// <param name="videoIds"></param>
        /// <param name="status"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public List<PlayListSearchEntity> SearchPlayList(string username, int zoneId, int status, string keyword, int mode, int sortOder, DateTime createdDateFrom, DateTime createdDateTo, DateTime distributionDateFrom, DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<PlayListSearchEntity>();
                var searchRequest = new SearchDescriptor<PlayListSearchEntity>();

                //sort by created_date
                switch (sortOder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username)) { 
                    termUserName.Field = "created_by";
                    termUserName.Value = username;
                }

                //status
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                //zone
                var termZoneId = new TermQuery();
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneId;
                }

                var termsZoneIds = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneIds.Field = "zone_ids";
                    termsZoneIds.Terms = new string[] { zoneId.ToString() };
                }

                //mode
                var termMode = new TermQuery();
                if (mode != -1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }

                //created_date
                string fieldCreatedDate = "created_date";
                var range = new DateRangeQuery();
                range.Field = fieldCreatedDate;
                if (createdDateFrom != DateTime.MinValue && createdDateTo != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = createdDateFrom;
                    range.LessThanOrEqualTo = createdDateTo;
                }
                else {
                    if (createdDateFrom != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = createdDateFrom;
                    }
                    else if (createdDateTo != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = createdDateTo;
                    }
                }

                //distribution_date
                string fieldDistributionDate = "distribution_date";
                var range1 = new DateRangeQuery();
                range1.Field = fieldDistributionDate;
                if (distributionDateFrom != DateTime.MinValue && distributionDateTo != DateTime.MinValue)
                {
                    range1.GreaterThanOrEqualTo = distributionDateFrom;
                    range1.LessThanOrEqualTo = distributionDateTo;
                }
                else {
                    if (distributionDateFrom != DateTime.MinValue)
                    {
                        range1.GreaterThanOrEqualTo = distributionDateFrom;
                    }
                    else if (distributionDateTo != DateTime.MinValue)
                    {
                        range1.LessThanOrEqualTo = distributionDateTo;
                    }
                }

                //ko tim status=5 bi xoa roi
                var termStatusDel = new TermQuery();
                termStatusDel.Field = "status";
                termStatusDel.Value = (int)EnumPlayListStatus.MoveTrash;                

                var fields = new string[] { "name", "name.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {                    
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termUserName  && range && range1 && termStatus && (termZoneId || termsZoneIds) && termMode && (q.Bool(b => b.MustNot(termStatusDel)))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q && termUserName && range && range1 && termStatus && (termZoneId || termsZoneIds) && termMode && (q.Bool(b => b.MustNot(termStatusDel)))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<CountStatus> CountPlaylist(string username, List<string> listStatus)
        {
            try
            {
                var listSearch = new List<CountStatus>();
                var searchRequest = new SearchDescriptor<PlayListSearchEntity>();

                var querys = new List<QueryContainerDescriptor<PlayListSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<PlayListSearchEntity>();

                        var termUserName = new TermQuery();
                        if (status == "0" || status == "4")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                        }

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;
                                                
                        query.Bool(boo => boo.Must(mu => termUserName && termByStatus));

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);
                
                if (data != null)
                {
                    listSearch = data.Select(s => new CountStatus { Status = s.Key, Count = s.DocCount }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateCountVideoForPlaylist(int playListId, int countVideoAdd, int countVideoDel)
        {
            bool returnValue = false;
            try
            {
                if (playListId > 0)
                {
                    var playListDb = _db.Get<PlayListSearchEntity>(playListId);
                    if (playListDb != null)
                    {                      
                        var count= playListDb.VideoCount + countVideoAdd;
                        count = count - countVideoDel;
                        playListDb.VideoCount = count;                        
                    }
                    returnValue = _db.Update<PlayListSearchEntity>(playListId, playListDb);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateFollowVideoForPlaylist(int playListId, string action)
        {
            bool returnValue = false;
            try
            {
                if (playListId > 0)
                {
                    var playListDb = _db.Get<PlayListSearchEntity>(playListId);
                    if (playListDb != null)
                    {
                        if (action == "follow")
                        {
                            playListDb.FollowCount = playListDb.FollowCount + 1;
                        }
                        if (action == "unfollow")
                        {
                            playListDb.FollowCount = playListDb.FollowCount - 1;
                        }
                    }
                    returnValue = _db.Update<PlayListSearchEntity>(playListId, playListDb);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool InitAllPlayList(List<PlayListSearchEntity> listPlayList)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "name";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<PlayListSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listPlayList);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected PlayListDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
