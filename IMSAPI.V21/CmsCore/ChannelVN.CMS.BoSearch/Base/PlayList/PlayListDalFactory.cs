﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.PlayList
{
    public class PlayListDalFactory
    {
        public static bool AddPlayList(PlayListSearchEntity playList)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.AddPlayList(playList);
            }
            return returnValue;
        }

        public static bool UpdatePlayList(PlayListSearchEntity playList)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.UpdatePlayList(playList);
            }
            return returnValue;
        }

        public static List<PlayListSearchEntity> SearchPlayList(string username, int zoneId, EnumPlayListStatus status, string keyword, EnumPlayListMode mode, EnumPlayListSort sortOder, DateTime createdDateFrom, DateTime createdDateTo, DateTime distributionDateFrom, DateTime distributionDateTo, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PlayListSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.SearchPlayList(username, zoneId, (int)status, keyword, (int)mode, (int)sortOder, createdDateFrom, createdDateTo, distributionDateFrom, distributionDateTo, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CountStatus> CountPlaylist(string username, List<string> listStatus)
        {
            List<CountStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.CountPlaylist(username, listStatus);
            }
            return returnValue;
        }

        public static bool Publish(int playListId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.Publish(playListId, account);
            }
            return returnValue;
        }

        public static bool UnPublish(int playListId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.UnPublish(playListId, account);
            }
            return returnValue;
        }

        public static bool MoveTrash(int playListId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.MoveTrash(playListId, account);
            }
            return returnValue;
        }

        public static bool ChangeStatus(int playListId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.ChangeStatus(playListId, status, username);
            }
            return returnValue;
        }

        public static bool ChangeMode(string account, int playListId, int mode)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.ChangeMode(account, playListId, mode);
            }
            return returnValue;
        }

        public static bool Delete(List<string> listIds)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.Delete(listIds);
            }
            return returnValue;
        }

        public static bool UpdateCountVideoForPlaylist(int playListId, int countVideoAdd, int countVideoDel)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.UpdateCountVideoForPlaylist(playListId, countVideoAdd, countVideoDel);
            }
            return returnValue;
        }

        public static bool UpdateFollowVideoForPlaylist(int playListId, string action="")
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.UpdateFollowVideoForPlaylist(playListId, action);
            }
            return returnValue;
        }

        public static bool InitAllPlayList(List<PlayListSearchEntity> listPlayList)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PlayListDal.InitAllPlayList(listPlayList);
            }
            return returnValue;
        }
    }
}
