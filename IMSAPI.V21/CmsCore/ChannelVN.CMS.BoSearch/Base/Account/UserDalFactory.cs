﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Account
{
    public class UserDalFactory
    {
        public static bool AddUser(UserEntity user)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.AddUser(user);
            }
            return returnValue;
        }

        public static bool UpdateUser(UserEntity user)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.UpdateUser(user);
            }
            return returnValue;
        }

        public static List<string> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.SearchUser(keyword, status, sortOrder, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<string> SearchUser2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.SearchUser2(keyword, status, start, rows, ref totalRow);
            }
            return returnValue;
        }

        public static bool InitUsersByStatus(List<UserEntity> listUsers)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.InitUsersByStatus(listUsers);
            }
            return returnValue;
        }
        public static bool DeleteIndexEs(string indexName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.DeleteIndexEs(indexName);
            }
            return returnValue;
        }
        public static bool DeleteNewsIdsEsRedis(List<string> newsids)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.UserDal.DeleteNewsIdsEsRedis(newsids);
            }
            return returnValue;
        }
    }
}
