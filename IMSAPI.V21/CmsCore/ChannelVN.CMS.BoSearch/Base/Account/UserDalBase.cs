﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Account
{
    public abstract class UserDalBase
    {
        public bool AddUser(UserEntity user)
        {
            bool returnValue = false;
            try
            {
                if (user != null)
                {                    
                    var oUser = new UserSearchEntity
                    {
                        Id = user.Id,
                        FullName = user.FullName,
                        UserName = user.UserName,
                        Status = user.Status,
                        CreatedDate=user.CreatedDate ,
                        StaffCode=user.StaffCode                       
                    };
                    var fieldNameAnalyzer = "fullname";
                    returnValue = _db.CreateIndexSettings<UserSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(oUser);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }

        public bool UpdateUser(UserEntity user)
        {
            bool returnValue = false;
            try
            {
                if (user != null)
                {
                    var oUser = new UserSearchEntity
                    {
                        Id = user.Id,
                        FullName = user.FullName,
                        UserName = user.UserName,
                        Status = user.Status,
                        CreatedDate=user.CreatedDate,
                        StaffCode=user.StaffCode
                    };              

                    returnValue = _db.Update<UserSearchEntity>(oUser.Id, oUser);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public List<string> SearchUser(string keyword, int status, int sortOrder, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "username","fullname", "fullname.folded" };

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                var data = _db.SearchStatus<UserSearchEntity>(keyword, status, fields, pageIndex ,pageSize);                
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s=>s.Id.ToString()).ToList();
                }                
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<string> SearchUser2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "username", "fullname", "fullname.folded" };

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                var data = _db.SearchPaging<UserSearchEntity>(keyword, status, fields, start, rows);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitUsersByStatus(List<UserEntity> listUsers)
        {
            bool returnValue = false;
            try
            {
                var list = listUsers.Select(s => new UserSearchEntity
                {
                    Id = s.Id,
                    FullName = s.FullName,
                    UserName=s.UserName,
                    Status=s.Status,
                    CreatedDate=s.CreatedDate
                }).ToList();

                var fieldNameAnalyzer = "fullname";               
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                         _db.CreateIndexSettings<UserSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<UserSearchEntity>(list);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }
        public bool DeleteIndexEs(string indexName)
        {
            try
            {
                return _db.DeleteIndex(indexName);
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteNewsIdsEsRedis(List<string> newsids)
        {
            try
            {
                return _db.DeleteMappingMany<NewsSearchEntity>(newsids);
            }
            catch
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected UserDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
