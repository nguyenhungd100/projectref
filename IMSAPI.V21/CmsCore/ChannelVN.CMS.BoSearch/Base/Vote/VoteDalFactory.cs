﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Vote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BoSearch.Base.Vote
{
    public class VoteDalFactory
    {
        public static bool AddVote(VoteEntity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VoteDal.AddVote(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateVote(VoteEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VoteDal.UpdateVote(news);
            }
            return returnValue;
        }

        public static List<string> SearchVote(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VoteDal.SearchVote(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
            }
            return returnValue;
        }                    

        public static bool InitAllVote(List<VoteSearchEntity> listVote)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VoteDal.InitAllVote(listVote);
            }
            return returnValue;
        }
    }
}
