﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Vote;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Vote
{
    public abstract class VoteDalBase
    {
        public bool AddVote(VoteEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {                    
                    var oVote = new VoteSearchEntity
                    {
                        Id = tag.Id,
                        Title=tag.Title
                    };
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<VoteSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<VoteSearchEntity>(oVote);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateVote(VoteEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {
                    var oVote = _db.Get<VoteSearchEntity>(tag.Id);                                       
                    if (oVote == null)
                    {
                        oVote = new VoteSearchEntity
                        {
                            Id = tag.Id,
                            Title = tag.Title
                        };
                    }
                    else
                    {
                        oVote.Id = tag.Id;
                        oVote.Title = tag.Title;                        
                    }           

                    returnValue = _db.Update<VoteSearchEntity>(oVote.Id, oVote);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public List<string> SearchVote(string keyword, int zoneId, int pageIndex, int pageSize, bool isGetTotal, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();

                var searchDescriptor = new SearchDescriptor<VoteSearchEntity>();

                var fields = new string[] { "title", "title.folded" };
                                                           
                var termZoneId = new TermsQuery();                
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_ids";
                    termZoneId.Terms = new string[] { zoneId.ToString() };                    
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchDescriptor.Query(q =>q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termZoneId
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchDescriptor);                

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllVote(List<VoteSearchEntity> listVote)
        {
            bool returnValue = false;
            try
            {                
                var fieldNameAnalyzer = "title";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<VoteSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listVote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected VoteDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
