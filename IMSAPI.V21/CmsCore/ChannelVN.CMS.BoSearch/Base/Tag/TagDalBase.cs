﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Tag;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Tag
{
    public abstract class TagDalBase
    {
        public bool AddTag(TagEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {                    
                    var oTag = new TagSearchEntity
                    {
                        Id = tag.Id,
                        Name=tag.Name,
                        Avatar=tag.Avatar,
                        ZoneId=tag.ZoneId,
                        CreatedBy=tag.CreatedBy,
                        CreatedDate=tag.CreatedDate,
                        IsHotTag=tag.IsHotTag,
                        IsThread=tag.IsThread,
                        NewsCount=tag.NewsCount,
                        ParentId=tag.ParentId,
                        Status=tag.Status,
                        Url=tag.Url,
                        Invisibled=tag.Invisibled,
                        ZoneName=tag.ZoneName
                    };

                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<TagSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(oTag);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateTag(TagEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {
                    var oTag = _db.Get<TagSearchEntity>(tag.Id);                                       
                    if (oTag == null)
                    {
                        oTag = new TagSearchEntity
                        {
                            Id = tag.Id,
                            Name = tag.Name,
                            Avatar = tag.Avatar,
                            ZoneId = tag.ZoneId,
                            CreatedBy = tag.CreatedBy,
                            //CreatedDate = tag.CreatedDate,
                            IsHotTag = tag.IsHotTag,
                            IsThread = tag.IsThread,
                            NewsCount = tag.NewsCount,
                            ParentId = tag.ParentId,
                            Status = tag.Status,
                            Url = tag.Url,
                            Invisibled=tag.Invisibled,
                            ZoneName = tag.ZoneName
                        };
                    }
                    else
                    {
                        oTag.Id = tag.Id;
                        oTag.Name = tag.Name;
                        oTag.Avatar = tag.Avatar;
                        oTag.ZoneId = tag.ZoneId;
                        oTag.CreatedBy = tag.CreatedBy;
                        //oTag.CreatedDate = tag.CreatedDate;
                        oTag.IsHotTag = tag.IsHotTag;
                        oTag.IsThread = tag.IsThread;
                        //oTag.NewsCount = tag.NewsCount;
                        oTag.ParentId = tag.ParentId;
                        oTag.Status = tag.Status;
                        oTag.Url = tag.Url;
                        oTag.Invisibled = tag.Invisibled;
                        oTag.ZoneName = tag.ZoneName;
                    }           

                    returnValue = _db.Update<TagSearchEntity>(oTag.Id, oTag);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateTagHot(long tagId, bool isHotTag)
        {            
            try
            {                
                var oTag = _db.Get<TagSearchEntity>(tagId);
                if (oTag == null)
                {
                    return false;
                }
                else
                {                        
                    oTag.IsHotTag = isHotTag;                       
                }                             

                return _db.Update<TagSearchEntity>(oTag.Id, oTag);
            }
            catch { return false; }
        }
        public bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            try
            {
                var oTag = _db.Get<TagSearchEntity>(tagId);
                if (oTag == null)
                {
                    return false;
                }
                else
                {
                    oTag.Invisibled = invisibled;
                }

                return _db.Update<TagSearchEntity>(oTag.Id, oTag);
            }
            catch { return false; }
        }

        public bool UpdateTagNewsCount(long tagId, int count)
        {
            try
            {
                var oTag = _db.Get<TagSearchEntity>(tagId);
                if (oTag == null)
                {
                    return false;
                }
                else
                {
                    oTag.NewsCount = oTag.NewsCount + count;
                    if (oTag.NewsCount < 0)
                    {
                        oTag.NewsCount = 0;
                    }
                }

                return _db.Update<TagSearchEntity>(oTag.Id, oTag);
            }
            catch { return false; }
        }

        public bool DeleteById(long tagId)
        {
            try
            {
                var oTag = _db.Get<TagSearchEntity>(tagId);
                if (oTag == null)
                {
                    return true;
                }                

                return _db.DeleteMapping(oTag);
            }
            catch { return false; }
        }

        public List<TagSearchEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false, int isInvisibled = -1)
        {
            try
            {
                var listSearch = new List<TagSearchEntity>();

                var searchDescriptor = new SearchDescriptor<TagSearchEntity>();

                var fields = new string[] { "name", "name.folded" };
                
                switch (orderBy)
                {
                    case 0:
                        searchDescriptor.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchDescriptor.Sort(s => s.Descending("news_count"));
                        break;
                    case 2:
                        searchDescriptor.Sort(s => s.Ascending("news_count"));
                        break;
                    case 3:
                        searchDescriptor.Sort(s => s.Descending("is_hot_tag"));                        
                        break;                    
                    default:
                        searchDescriptor.Sort(s => s.Descending("created_date"));                        
                        break;
                }

                var termParentId = new TermQuery();
                if (parentTagId > 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentTagId;                    
                }

                var termIsThread = new TermQuery();
                if (isThread > -1)
                {
                    termIsThread.Field = "is_thread";
                    termIsThread.Value = (isThread==0)?false:true;
                }

                var termIsHotTag = new TermQuery();
                if (isHotTag > -1)
                {
                    termIsHotTag.Field = "is_hot_tag";
                    termIsHotTag.Value = (isHotTag == 0) ? false : true;
                }

                var termInvisibled = new TermQuery();
                if (isInvisibled > -1)
                {
                    termInvisibled.Field = "invisibled";
                    termInvisibled.Value = (isInvisibled == 0) ? false : true;
                }

                var termZoneId = new TermQuery();
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneId;                    
                }                

                var termType = new TermQuery();
                if (type > 0)
                {
                    termType.Field = "parent_id";
                    termType.Value = type;                    
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchDescriptor.Query(q =>q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .Type(TextQueryType.Phrase)
                    ) && termParentId && termIsThread && termIsHotTag && termInvisibled && termZoneId && termType
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchDescriptor);                

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllTag(List<TagSearchEntity> listTag)
        {
            bool returnValue = false;
            try
            {                
                var fieldNameAnalyzer = "name";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<TagSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listTag);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected TagDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
