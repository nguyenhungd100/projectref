﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Tag
{
    public class TagDalFactory
    {
        public static bool AddTag(TagEntity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.AddTag(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateTag(TagEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.UpdateTag(news);
            }
            return returnValue;
        }

        public static bool UpdateTagHot(long tagId, bool isHotTag)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.UpdateTagHot(tagId, isHotTag);
            }
            return returnValue;
        }
        public static bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.UpdateTagInvisibled(tagId, invisibled);
            }
            return returnValue;
        }
        public static bool UpdateTagNewsCount(long tagId, int count)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.UpdateTagNewsCount(tagId, count);
            }
            return returnValue;
        }
        public static bool DeleteById(long tagId)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.DeleteById(tagId);
            }
            return returnValue;
        }

        public static List<TagSearchEntity> SearchTag(string keyword, long parentTagId, int isThread, int zoneId, int type, int orderBy, int isHotTag, int pageIndex, int pageSize, ref int totalRow, bool getTagHasNewsOnly = false, int isInvisibled = -1)
        {
            List<TagSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.SearchTag(keyword, parentTagId, isThread, zoneId, type, orderBy, isHotTag, pageIndex, pageSize, ref totalRow, getTagHasNewsOnly, isInvisibled);
            }
            return returnValue;
        }                    

        public static bool InitAllTag(List<TagSearchEntity> listTag)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.TagDal.InitAllTag(listTag);
            }
            return returnValue;
        }
    }
}
