﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public abstract class VideoYoutubeDalBase
    {
        public bool AddVideoYoutube(VideoSearchEntity video)
        {
            bool returnValue = false;
            try
            {
                if (video != null)
                {                    
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<VideoSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<VideoSearchEntity>(video);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public bool UpdateVideoYoutube(VideoSearchEntity video)
        {
            bool returnValue = false;
            try
            {
                if (video != null)
                {                    
                    returnValue = _db.Update<VideoSearchEntity>(video.Id, video);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool Unpublish(int videoId)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }
                var video = _db.Get<VideoSearchEntity>(videoId);
                if (video != null)
                {
                    video.Status = (int)EnumVideoStatus.Unpublished;                                        
                }

                return _db.Update<VideoSearchEntity>(videoId, video);
            }
            catch { return returnValue; }
        }

        public List<string> SearchVideoYoutube(string username, string keyword, int status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "name", "name.folded" };
                var sort = new Dictionary<string, dynamic>();
                sort.Add("desc", "created_date");
                
                var term = new Dictionary<string, dynamic>();
                if (!string.IsNullOrEmpty(username))
                {
                    term.Add("created_by", username);
                }
                if (status != 0 && status != -1)
                {
                    term.Add("status_video_youtube", status);
                }
                //video youtube
                term.Add("status", 99);

                var terms = new Dictionary<string, dynamic>();
                if (zoneVideoId > 0)
                {
                    terms.Add("zone_ids", new string[] { zoneVideoId.ToString() });
                }
                if (playlistId > 0)
                {
                    terms.Add("playlist_ids", new string[] { playlistId.ToString() });
                }

                string fieldDate = "created_date";//field for fromDate,toDate

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                var data = _db.SearchPaging<VideoSearchEntity>(keyword, fields, sort, term, terms, fromDate, toDate, fieldDate, pageIndex, pageSize);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s=>s.Id.ToString()).ToList();
                }                
                return listSearch;
            }
            catch
            {
                return null;
            }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected VideoYoutubeDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
