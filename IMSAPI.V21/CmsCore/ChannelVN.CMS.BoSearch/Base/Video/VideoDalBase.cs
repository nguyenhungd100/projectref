﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public abstract class VideoDalBase
    {
        public bool AddVideo(VideoSearchEntity video)
        {
            bool returnValue = false;
            try
            {
                if (video != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<VideoSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(video);
                    if (video.ParentId > 0)
                    {
                        //update type cho parentId
                        var videoParent = _db.Get<VideoSearchEntity>(video.ParentId);
                        var listType = new List<string>();
                        if (videoParent != null)
                        {
                            var listTypeTemp = new List<string>();
                            if (videoParent.ListType != null)
                                listTypeTemp.AddRange(videoParent.ListType);                            
                            listTypeTemp.Add(video.Type.ToString());
                            listType.AddRange(listTypeTemp.Distinct());
                            videoParent.ListType = listType.ToArray();
                            _db.Update<VideoSearchEntity>(video.ParentId, videoParent);
                        }
                    }
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public bool UpdateVideo(VideoSearchEntity video)
        {
            bool returnValue = false;
            try
            {
                if (video != null)
                {
                    var videoDB = _db.Get<VideoSearchEntity>(video.Id);
                    //update chirent->parent
                    if (videoDB.ParentId > 0 && video.ParentId == 0)
                    {
                        var videoParentDB = _db.Get<VideoSearchEntity>(videoDB.ParentId);
                        if(videoParentDB != null)
                        {
                            var listRemove = videoParentDB.ListType.ToList();
                            listRemove.Remove(videoDB.Type.ToString());
                            videoParentDB.ListType = listRemove.ToArray();
                            _db.Update<VideoSearchEntity>(videoDB.ParentId, videoParentDB);
                        }
                    }
                    //update parent
                    video.CreatedBy = videoDB.CreatedBy;
                    video.CreatedDate = videoDB.CreatedDate;
                    returnValue = _db.Update<VideoSearchEntity>(video.Id, video);
                    if (video.ParentId > 0)
                    {
                        //update type cho parentId
                        var videoParent = _db.Get<VideoSearchEntity>(video.ParentId);
                        var listType = new List<string>();
                        if (videoParent != null)
                        {
                            //check chirent cùng parent
                            if(videoDB.ParentId== video.ParentId)
                            {
                                var listRemove = videoParent.ListType.ToList();
                                listRemove.Remove(videoDB.Type.ToString());
                                videoParent.ListType = listRemove.ToArray();
                            }
                            else
                            {
                                //xóa type cho parent cũ
                                var videoParentOld = _db.Get<VideoSearchEntity>(videoDB.ParentId);
                                if (videoParentOld != null)
                                {
                                    var listRemove = videoParentOld.ListType.ToList();
                                    listRemove.Remove(videoDB.Type.ToString());
                                    videoParentOld.ListType = listRemove.ToArray();
                                    _db.Update<VideoSearchEntity>(videoParentOld.Id, videoParentOld);
                                }
                            }
                            var listTypeTemp = new List<string>();
                            if(videoParent.ListType!=null)
                                listTypeTemp.AddRange(videoParent.ListType);
                            listTypeTemp.Add(video.Type.ToString());
                            listType.AddRange(listTypeTemp.Distinct());
                            videoParent.ListType = listType.ToArray();
                            _db.Update<VideoSearchEntity>(videoParent.Id, videoParent);
                        }
                    }
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateVideoInPlayList(int playlistId, List<string> addVideoIds, List<string> deleteVideoIds, string playlistName)
        {
            bool returnValue = false;
            try
            {
                //add playlist in video
                if (playlistId >0 && addVideoIds != null && addVideoIds.Count>0)
                {
                    //Logger.WriteLog(Logger.LogType.Debug, "1. UpdateVideoInPlayList => FuncTaskAsyn() addVideoIds: " + string.Join(",", addVideoIds.ToArray()));

                    foreach (var id in addVideoIds)
                    {                        
                        //Logger.WriteLog(Logger.LogType.Debug, "2. UpdateVideoInPlayList => FuncTaskAsyn() id: " + id);

                        var plIdNew = new List<string>();
                        var plNameNew = new List<string>();
                        int videoid = 0;
                        if (int.TryParse(id, out videoid))
                        {
                            var videoDB = _db.Get<VideoSearchEntity>(videoid);
                            if (videoDB == null)
                            {
                                Logger.WriteLog(Logger.LogType.Debug, "3. UpdateVideoInPlayList => FuncTaskAsyn() videoid: " + videoid + "videoDB == null");
                                return returnValue;
                            }
                            var plIdDB = videoDB.PlaylistIds == null ? new List<string>() : videoDB.PlaylistIds.ToList();
                            plIdDB.Add(playlistId.ToString());
                            plIdNew.AddRange(plIdDB.Distinct());

                            var plNameDB = videoDB.PlaylistName == null ? new List<string>() : videoDB.PlaylistName.ToList();
                            plNameDB.Add(playlistName);
                            plNameNew.AddRange(plNameDB.Distinct());

                            videoDB.PlaylistIds = plIdNew.ToArray();
                            videoDB.PlaylistName = plNameNew.ToArray();
                            returnValue = _db.Update<VideoSearchEntity>(videoid, videoDB);

                            //Logger.WriteLog(Logger.LogType.Debug, "4. UpdateVideoInPlayList => FuncTaskAsyn() videoid: " + videoid);
                        }
                        //else
                        //{
                        //    Logger.WriteLog(Logger.LogType.Debug, "5. UpdateVideoInPlayList => FuncTaskAsyn() TryParse videoid: " + videoid);
                        //}
                    }                    
                }

                //xóa playlist in video
                if (playlistId > 0 && deleteVideoIds != null && deleteVideoIds.Count > 0)
                {
                    foreach (var id in deleteVideoIds)
                    {
                        var plNew = new List<string>();
                        var plNameNew = new List<string>();
                        int videoid = 0;
                        if (int.TryParse(id, out videoid))
                        {
                            var videoDB = _db.Get<VideoSearchEntity>(videoid);
                            if (videoDB == null)
                            {
                                Logger.WriteLog(Logger.LogType.Debug, "Remove: UpdateVideoInPlayList => FuncTaskAsyn() videoid: " + videoid + " videoDB == null");
                                return returnValue;
                            }
                            var plIdDB = videoDB.PlaylistIds==null ? new List<string>() : videoDB.PlaylistIds.ToList();
                            plIdDB.Remove(playlistId.ToString());
                            plNew.AddRange(plIdDB.Distinct());

                            var plNameDB = videoDB.PlaylistName==null ? new List<string>() : videoDB.PlaylistName.ToList();
                            plNameDB.Remove(playlistName);
                            plNameNew.AddRange(plNameDB.Distinct());

                            videoDB.PlaylistIds = plNew.ToArray();
                            videoDB.PlaylistName = plNameNew.ToArray();
                            returnValue = _db.Update<VideoSearchEntity>(videoid, videoDB);
                        }                       
                    }
                }

                return returnValue;
            }
            catch(Exception ex) {
                Logger.WriteLog(Logger.LogType.Error, "Catch. UpdateVideoInPlayList => FuncTaskAsyn() ex: " + ex.Message);
                return returnValue;
            }
        }

        public bool DeleteVideo(int videoId)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }

                return _db.DeleteMapping<VideoSearchEntity>(videoId);
            }
            catch { return returnValue; }
        }

        public bool Unpublish(int videoId)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }
                var video = _db.Get<VideoSearchEntity>(videoId);
                if (video != null)
                {
                    video.Status = (int)EnumVideoStatus.Unpublished;
                }

                return _db.Update<VideoSearchEntity>(videoId, video);
            }
            catch { return returnValue; }
        }

        public bool ChangeStatus(int videoId, int status, string username)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }
                var video = _db.Get<VideoSearchEntity>(videoId);
                if (video != null)
                {
                    video.Status = status;
                    video.EditedBy = username;
                    video.EditedDate = DateTime.Now;
                    video.LastModifiedBy = username;
                    video.LastModifiedDate = DateTime.Now;
                    video.PublishBy = username;
                    video.PublishDate = DateTime.Now;
                }

                return _db.Update<VideoSearchEntity>(videoId, video);
            }
            catch { return returnValue; }
        }

        public bool UpdateMode(int videoId, int mode, string username)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }
                var video = _db.Get<VideoSearchEntity>(videoId);
                if (video != null)
                {
                    video.Mode = mode;
                    video.LastModifiedBy = username;
                    video.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<VideoSearchEntity>(videoId, video);
            }
            catch { return returnValue; }
        }
        public bool UpdateStatusVideoByHashId(int videoId,string keyVideo, string username)
        {
            bool returnValue = false;
            try
            {
                if (videoId <= 0)
                {
                    return returnValue;
                }
                var video = _db.Get<VideoSearchEntity>(videoId);
                if (video != null)
                {
                    video.Status = (int)EnumVideoStatus.DoneVideo;
                    video.KeyVideo = keyVideo;
                    video.LastModifiedBy = username;
                    video.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<VideoSearchEntity>(videoId, video);
            }
            catch { return returnValue; }
        }

        public List<VideoSearchEntity> ListVideoByStatus(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username.Trim().ToLower();
                }
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }
                var termMode = new TermQuery();
                if (mode == 1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                else if (mode == 0)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                searchRequest.Query(q => termStatus && termUserName && termMode && termParentId
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();                    
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<VideoSearchEntity> ListVideoByStatusSingle(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username.Trim().ToLower();
                }
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }
                var termMode = new TermQuery();
                if (mode == 1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                else if (mode == 0)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                //var existquery = new ExistsQuery();
                //existquery.Field = "playlist_ids";
                //q.Bool(b => b.MustNot(existquery))

                //check phai co zone
                var existZones = new ExistsQuery();
                existZones.Field = "zone_ids";
                //var existZone = new ExistsQuery();
                //existZone.Field = "zone_id";
                //|| q.Bool(b => b.Must(existZone))

                //check video tu vtv thi ko check zone
                if (status == (int)EnumVideoStatus.DoneVideo || status == (int)EnumVideoStatus.CloneVideo)
                {
                    var termStatus7 = new TermQuery();
                    termStatus7.Field = "status";
                    termStatus7.Value = 7;

                    searchRequest.Query(q => (termStatus || termStatus7) && termUserName && termMode && termParentId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else {
                    searchRequest.Query(q => termStatus && termUserName && termMode && termParentId && (q.Bool(b => b.Must(existZones)))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<VideoSearchEntity> ListMyVideoSingle(string username, int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termUserNameC = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserNameC.Field = "created_by";
                    termUserNameC.Value = username.Trim().ToLower();
                }                

                var termUserNameP = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserNameP.Field = "publish_by";
                    termUserNameP.Value = username.Trim().ToLower();
                }

                var termMode = new TermQuery();
                if (mode == 1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                else if (mode == 0)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                var termZoneId = new TermQuery();
                if (zoneVideoId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneVideoId;
                }

                var termsZoneId = new TermsQuery();
                if (zoneVideoId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneVideoId.ToString() };
                }

                //check phai co zone
                var existZones = new ExistsQuery();
                existZones.Field = "zone_ids";
                
                string fieldDate = "created_date";//field for fromDate,toDate
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var fields = new string[] { "name", "name.folded" };
                
                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && (termUserNameC || termUserNameP) && (termZoneId || termsZoneId) && range && termMode && termParentId && (q.Bool(b => b.Must(existZones)))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<VideoSearchEntity> ListVideoByParentId(int parentId, int status, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();
               
                var termParentId = new TermQuery();
                termParentId.Field = "parent_id";
                termParentId.Value = parentId;

                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }                

                searchRequest.Query(q => termStatus && termParentId);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<VideoSearchEntity> SearchVideoKeyByZone(DateTime fromDate, DateTime toDate, string username, List<string> zoneVideoId, int playlistId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = status;

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = username.Trim().ToLower();
                }
                
                var termsZoneId = new TermsQuery();
                if (zoneVideoId.Count > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = zoneVideoId.ToArray();
                }

                var termsPlaylistId = new TermsQuery();
                if (playlistId > 0)
                {
                    termsPlaylistId.Field = "playlist_ids";
                    termsPlaylistId.Terms = new string[] { playlistId.ToString() };
                }

                string fieldDate = "distribution_date";//field for fromDate,toDate
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var fields = new string[] { "name", "name.folded" };
                
                if (string.IsNullOrEmpty(keyword))
                {
                    searchRequest.Query(q => range && termStatus && termCreatedBy && termsZoneId && termsPlaylistId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && termStatus && termCreatedBy && termsZoneId && termsPlaylistId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<VideoSearchEntity> SearchVideo(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {            
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    case 10:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 11:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username.Trim().ToLower();
                }
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }
                var termMode = new TermQuery();
                if (mode == 1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                else if (mode == 0)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }

                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                var termZoneId = new TermQuery();
                if (zoneVideoId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneVideoId;
                }

                var termsZoneId = new TermsQuery();
                if (zoneVideoId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneVideoId.ToString() };
                }

                //var termsPlaylistId = new TermsQuery();
                //if (playlistId > 0)
                //{
                //    termsPlaylistId.Field = "playlist_ids";
                //    termsPlaylistId.Terms = new string[] { playlistId.ToString() };
                //}                

                //check phai co zone
                var existZones = new ExistsQuery();
                existZones.Field = "zone_ids";
                //var existZone = new ExistsQuery();
                //existZone.Field = "zone_id";
                //|| q.Bool(b => b.Must(existZone))

                //or ko thuoc zone and ko thuoc playslist->de hien thi video khi chen vao bai tu edite
                //check khong thuoc playlist
                var existPlaylist = new ExistsQuery();
                existPlaylist.Field = "playlist_ids";
                //q.Bool(b => b.MustNot(existquery))

                string fieldDate = "created_date";//field for fromDate,toDate
                if (status == (int)EnumVideoStatus.Published)
                {
                    fieldDate = "distribution_date";
                }
                if (status == (int)EnumVideoStatus.Unpublished || status == (int)EnumVideoStatus.Return)
                {
                    fieldDate = "last_modified_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var fields = new string[] { "name", "name.folded" };

                //check video tu vtv thi ko check zone
                if (status == (int)EnumVideoStatus.DoneVideo || status == (int)EnumVideoStatus.CloneVideo)
                {
                    var termStatus7 = new TermQuery();
                    termStatus7.Field = "status";
                    termStatus7.Value = 7;

                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && (termStatus || termStatus7) && termUserName && termMode && termParentId && (termZoneId || termsZoneId)//&& termsPlaylistId 
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && termStatus && termUserName && termMode && termParentId && (termZoneId || termsZoneId) &&
                        (q.Bool(b => b.Must(existZones)) || (q.Bool(b => b.MustNot(existZones)) && q.Bool(b => b.MustNot(existPlaylist))))//&& termsPlaylistId 
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<VideoSearchEntity> ExportVideoPublish(string zoneIds, string createdBy, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 1;
                                                
                var termsZoneIds = new TermsQuery();
                if (!string.IsNullOrEmpty(zoneIds))
                {
                    termsZoneIds.Field = "zone_ids";
                    termsZoneIds.Terms = zoneIds.Split(new char[] {';',','});
                }                

                string fieldDate = "distribution_date";                
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                var dateNow = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    if (toDate > dateNow)//ko lay video boom
                        toDate = dateNow;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        if (toDate > dateNow)//ko lay video boom
                            toDate = dateNow;
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                
                searchRequest.Query(q => range && termStatus && termCreatedBy && termsZoneIds
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<VideoSearchEntity> SearchVideoSingle(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    case 10:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 11:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }
                
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username.Trim().ToLower();
                }
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }
                var termMode = new TermQuery();
                if (mode == 1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }
                else if (mode == 0)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }

                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                var termZoneId = new TermQuery();
                if (zoneVideoId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneVideoId;
                }

                var termsZoneId = new TermsQuery();
                if (zoneVideoId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneVideoId.ToString() };
                }

                //var termsPlaylistId = new TermsQuery();
                //if (playlistId > 0)
                //{
                //    termsPlaylistId.Field = "playlist_ids";
                //    termsPlaylistId.Terms = new string[] { playlistId.ToString() };
                //}                

                //check phai co zone
                var existZones = new ExistsQuery();
                existZones.Field = "zone_ids";
                //var existZone = new ExistsQuery();
                //existZone.Field = "zone_id";
                //|| q.Bool(b => b.Must(existZone))

                //or ko thuoc zone and ko thuoc playslist->de hien thi video khi chen vao bai tu edite
                //check khong thuoc playlist
                var existPlaylist = new ExistsQuery();
                existPlaylist.Field = "playlist_ids";
                //q.Bool(b => b.MustNot(existquery))

                string fieldDate = "created_date";//field for fromDate,toDate
                if (status == (int)EnumVideoStatus.Published)
                {
                    fieldDate = "distribution_date";
                }
                if (status == (int)EnumVideoStatus.Unpublished || status == (int)EnumVideoStatus.Return)
                {
                    fieldDate = "last_modified_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var fields = new string[] { "name", "name.folded" };

                //check video tu vtv thi ko check zone
                if (status == (int)EnumVideoStatus.DoneVideo || status == (int)EnumVideoStatus.CloneVideo)
                {
                    var termStatus7 = new TermQuery();
                    termStatus7.Field = "status";
                    termStatus7.Value = 7;

                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && (termStatus || termStatus7) && termUserName && termMode && termParentId && (termZoneId || termsZoneId)//&& termsPlaylistId 
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && termStatus && termUserName && termMode && termParentId && (termZoneId || termsZoneId) && 
                        (q.Bool(b => b.Must(existZones)) || (q.Bool(b => b.MustNot(existZones)) && q.Bool(b => b.MustNot(existPlaylist))))//&& termsPlaylistId 
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<CountStatus> CountVideo(string username, List<string> listStatus)
        {
            try
            {
                var listSearch = new List<CountStatus>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                var querys = new List<QueryContainerDescriptor<VideoSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<VideoSearchEntity>();

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;

                        var termParentId = new TermQuery();
                        var parentId = 0;
                        if (parentId <= 0)
                        {
                            termParentId.Field = "parent_id";
                            termParentId.Value = parentId;
                        }

                        var termUserName = new TermQuery();
                        if (status == "0" || status == "4" || status == "6" || status == "7")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username.Trim().ToLower();
                        }                        

                        if (status == "6" || status == "7")
                        {
                            var termStatus7 = new TermQuery();
                            termStatus7.Field = "status";
                            termStatus7.Value = 7;

                            query.Bool(boo => boo.Must(mu => (termByStatus || termStatus7) && termUserName && termParentId));
                        }
                        else
                        {
                            query.Bool(boo => boo.Must(mu => termByStatus && termUserName && termParentId));
                        }

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);

                if (data != null)
                {
                    listSearch = data.Select(s => new CountStatus { Status = s.Key, Count = s.DocCount }).ToList();
                    listSearch = listSearch.OrderBy(n => Convert.ToInt32(n.Status)).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }

            //try
            //{
            //    var listSearch = new List<dynamic>();
            //    var data = _db.CountStatusVideo<VideoSearchEntity>(username, listStatus);
            //    if (data != null)
            //    {
            //        listSearch = data.Data.ToList();
            //    }
            //    return listSearch;
            //}
            //catch
            //{
            //    return null;
            //}
        }

        public List<CountStatus> CountVideoSingle(string username, List<string> listStatus)
        {
            try
            {
                var listSearch = new List<CountStatus>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                var querys = new List<QueryContainerDescriptor<VideoSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<VideoSearchEntity>();
                        
                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;

                        var termParentId = new TermQuery();
                        var parentId = 0;
                        if (parentId <= 0)
                        {
                            termParentId.Field = "parent_id";
                            termParentId.Value = parentId;
                        }

                        var termUserName = new TermQuery();
                        if (status == "0" || status == "4" || status == "6" || status == "7")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username.Trim().ToLower();
                        }

                        //check khong thuoc playlist
                        //var existquery = new ExistsQuery();
                        //existquery.Field = "playlist_ids";
                        //.MustNot(existquery)

                        //check phai co zone
                        var existZones = new ExistsQuery();
                        existZones.Field = "zone_ids";
                        //var existZone = new ExistsQuery();
                        //existZone.Field = "zone_id";

                        //or ko thuoc zone and ko thuoc playslist->de hien thi video khi chen vao bai tu edite
                        //check khong thuoc playlist
                        var existPlaylist = new ExistsQuery();
                        existPlaylist.Field = "playlist_ids";

                        if (status == "6" || status == "7")
                        {
                            var termStatus7 = new TermQuery();
                            termStatus7.Field = "status";
                            termStatus7.Value = 7;

                            query.Bool(boo => boo.Must(mu => (termByStatus || termStatus7) && termUserName && termParentId));
                        }
                        else
                        {
                            query.Bool(boo => boo.Must(mu => termByStatus && termUserName && termParentId && (existZones || (mu.Bool(b => b.MustNot(existZones)) && mu.Bool(b => b.MustNot(existPlaylist))))));                            
                        }                        

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);
                
                if (data != null)
                {
                    listSearch = data.Select(s => new CountStatus { Status = s.Key, Count = s.DocCount }).ToList();
                    listSearch = listSearch.OrderBy(n => Convert.ToInt32(n.Status)).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<string> SearchVideo2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "name", "name.folded" };

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                var data = _db.SearchPaging<VideoSearchEntity>(keyword, status, fields, start, rows);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool CheckCloneVideoVtv(long originalId)
        {
            bool returnValue = false;
            try
            {
                if (originalId <= 0)
                {
                    return returnValue;
                }
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                var termOriginalId = new TermQuery();
                termOriginalId.Field = "original_id";
                termOriginalId.Value = originalId;

                var termStatus = new TermQuery();                
                termStatus.Field = "status";
                termStatus.Value = (int)EnumVideoStatus.DoneVideo;

                var termStatus7 = new TermQuery();
                termStatus7.Field = "status";
                termStatus7.Value = (int)EnumVideoStatus.CloneVideo;

                searchRequest.Query(q => (termStatus || termStatus7) && termOriginalId);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    if ((int)data.Total > 0) {
                        returnValue = true;
                    };                    
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool CheckCloneVideoEPL(string keyvideo)
        {
            bool returnValue = false;
            try
            {
                if (string.IsNullOrEmpty(keyvideo))
                {
                    return returnValue;
                }
                var listSearch = new List<VideoSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoSearchEntity>();

                var termKeyVideo = new TermQuery();
                termKeyVideo.Field = "keyvideo";
                termKeyVideo.Value = keyvideo;
                
                searchRequest.Query(q => termKeyVideo);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    if ((int)data.Total > 0)
                    {
                        returnValue = true;
                    };
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool InitAllVideo(List<VideoSearchEntity> listVideo)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "name";

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<VideoSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<VideoSearchEntity>(listVideo);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected VideoDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
