﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public abstract class VideoVTVDalBase
    {
        public List<Entity.Video> SearchVideoVTV(int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<Entity.Video>();
                var searchRequest = new SearchDescriptor<Entity.Video>();

                searchRequest.Sort(s => s.Descending("PublishDate"));

                var termsZoneId = new TermsQuery();
                if (zoneVideoId > 0)
                {
                    termsZoneId.Field = "AllZone";
                    termsZoneId.Terms = new string[] { zoneVideoId.ToString() };
                }
                
                string fieldDate = "PublishDate";
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var fields = new string[] { "Name", "Name.folded" };                

                if (string.IsNullOrEmpty(keyword))
                {
                    searchRequest.Query(q => q && range && termsZoneId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && range && termsZoneId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search("vtv", searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }       

        #region Core members

        private readonly CmsVTVSearchDb _db;
        
        protected VideoVTVDalBase(CmsVTVSearchDb db)
        {
            _db = db;
        }

        protected CmsVTVSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
