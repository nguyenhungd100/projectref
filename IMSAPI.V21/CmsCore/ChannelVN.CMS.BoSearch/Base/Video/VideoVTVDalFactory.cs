﻿using ChannelVN.CMS.BoSearch.Databases;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public class VideoVTVDalFactory
    {
       
        public static List<Entity.Video> SearchVideoVTV(int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<Entity.Video> returnValue = null;
            using (var db = new CmsVTVSearchDb())
            {
                returnValue = db.VideoVTVDal.SearchVideoVTV(zoneVideoId, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        
    }
}
