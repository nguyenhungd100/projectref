﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public class VideoYoutubeDalFactory
    {
        public static bool AddVideoYoutube(VideoSearchEntity video)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoYoutubeDal.AddVideoYoutube(video);
            }
            return returnValue;
        }
        
        public static bool UpdateVideoYoutube(VideoSearchEntity video)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoYoutubeDal.UpdateVideoYoutube(video);
            }
            return returnValue;
        }

        public static bool Unpublish(int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoYoutubeDal.Unpublish(videoId);
            }
            return returnValue;
        }

        public static List<string> SearchVideoYoutube(string username, string keyword, int status, int zoneVideoId, int playlistId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoYoutubeDal.SearchVideoYoutube(username, keyword, status, zoneVideoId, playlistId, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }       
    }
}
