﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Video
{
    public class VideoDalFactory
    {
        public static bool AddVideo(VideoSearchEntity video)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.AddVideo(video);
            }
            return returnValue;
        }        

        public static bool UpdateVideo(VideoSearchEntity video)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.UpdateVideo(video);
            }
            return returnValue;
        }

        public static bool UpdateVideoInPlayList(int playlistId, List<string> addVideoIds, List<string> deleteVideoIds, string playlistName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.UpdateVideoInPlayList(playlistId, addVideoIds, deleteVideoIds, playlistName);
            }
            return returnValue;
        }

        public static bool DeleteVideo(int videoId)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.DeleteVideo(videoId);
            }
            return returnValue;
        }

        public static bool Unpublish(int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.Unpublish(videoId);
            }
            return returnValue;
        }

        public static bool ChangeStatus(int videoId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ChangeStatus(videoId, status, username);
            }
            return returnValue;
        }

        public static bool UpdateMode(int videoId, int mode, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.UpdateMode(videoId, mode, username);
            }
            return returnValue;
        }

        public static bool UpdateStatusVideoByHashId(int videoId,string keyVideo, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.UpdateStatusVideoByHashId(videoId, keyVideo, username);
            }
            return returnValue;
        }

        public static List<VideoSearchEntity> ListVideoByStatus(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ListVideoByStatus(username, status, order, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoSearchEntity> ListVideoByStatusSingle(string username, int status, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ListVideoByStatusSingle(username, status, order, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoSearchEntity> ListMyVideoSingle(string username, int zoneVideoId, string keyword, DateTime fromDate, DateTime toDate, int order, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ListMyVideoSingle(username, zoneVideoId, keyword, fromDate, toDate, order, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoSearchEntity> ListVideoByParentId(int parentId, int status, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ListVideoByParentId(parentId, status, ref totalRow);
            }
            return returnValue;
        }
        public static List<VideoSearchEntity> SearchVideo(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.SearchVideo(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoSearchEntity> ExportVideoPublish(string zoneIds, string createdBy, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoSearchEntity> SearchVideoSingle(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.SearchVideoSingle(username, zoneVideoId, playlistId, keyword, status, order, fromDate, toDate, mode, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<VideoSearchEntity> SearchVideoKeyByZone(DateTime fromDate, DateTime toDate, string username, List<string> zoneVideoId, int playlistId, string keyword, int status, int pageIndex, int pageSize, ref int totalRow)
        {
            List<VideoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.SearchVideoKeyByZone(fromDate, toDate, username, zoneVideoId, playlistId, keyword, status, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CountStatus> CountVideo(string username, List<string> listStatus)
        {
            List<CountStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.CountVideo(username, listStatus);
            }
            return returnValue;
        }

        public static List<CountStatus> CountVideoSingle(string username, List<string> listStatus)
        {
            List<CountStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.CountVideoSingle(username, listStatus);
            }
            return returnValue;
        }

        public static List<string> SearchVideo2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.SearchVideo2(keyword, status, start, rows, ref totalRow);
            }
            return returnValue;
        }

        public static bool CheckCloneVideoVtv(long originalId)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.CheckCloneVideoVtv(originalId);
            }
            return returnValue;
        }
        public static bool CheckCloneVideoEPL(string keyvideo)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.CheckCloneVideoEPL(keyvideo);
            }
            return returnValue;
        }

        public static bool InitAllVideo(List<VideoSearchEntity> listVideo)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoDal.InitAllVideo(listVideo);
            }
            return returnValue;
        }        
    }
}
