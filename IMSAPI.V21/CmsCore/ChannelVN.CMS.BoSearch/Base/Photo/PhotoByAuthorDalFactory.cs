﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Photo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public class PhotoByAuthorDalFactory
    {
        public static bool SavePhotoByAuthor(PhotoByAuthorSearchEntity photoByAuthorES)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoByAuthorDal.SavePhotoByAuthor(photoByAuthorES);
            }
            return returnValue;
        }

        public static int GetAuthorByPhotoId(long photoId)
        {
            var authorId = 0;
            using (var db = new CmsMainSearchDb())
            {
                authorId = db.PhotoByAuthorDal.GetAuthorIdByPhotoId(photoId);
            }
            return authorId;
        }
    }
}
