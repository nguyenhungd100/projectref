﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public class PhotoAlbumDalFactory
    {
        public static bool AddPhotoAlbum(PhotoAlbumSearchEntity Album)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.AddPhotoAlbum(Album);
            }
            return returnValue;
        }

        public static bool UpdatePhotoAlbum(PhotoAlbumSearchEntity Album)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.UpdatePhotoAlbum(Album);
            }
            return returnValue;
        }

        public static PhotoAlbumSearchEntity GetById(int id)
        {
            PhotoAlbumSearchEntity returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.GetById(id);
            }
            return returnValue;
        }

        public static List<PhotoAlbumSearchEntity> SearchPhotoAlbum(string keyword, int zoneId, int eventId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoAlbumSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.SearchPhotoAlbum(keyword, zoneId, eventId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CountAlbumStatus> CountPhotoAlbum(string username, List<string> listStatus, string userAction)
        {
            List<CountAlbumStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.CountPhotoAlbum(username, listStatus, userAction);
            }
            return returnValue;
        }

        public static bool Publish(int photoAlbumId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.Publish(photoAlbumId, account);
            }
            return returnValue;
        }

        public static bool UnPublish(int photoAlbumId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.UnPublish(photoAlbumId, account);
            }
            return returnValue;
        }

        public static bool MoveTrash(int photoAlbumId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.MoveTrash(photoAlbumId, account);
            }
            return returnValue;
        }

        public static bool ChangeStatus(int photoAlbumId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.ChangeStatus(photoAlbumId, status, username);
            }
            return returnValue;
        }       

        public static bool InitAllPhotoAlbum(List<PhotoAlbumSearchEntity> listPhotoAlbum)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoAlbumDal.InitAllPhotoAlbum(listPhotoAlbum);
            }
            return returnValue;
        }
    }
}
