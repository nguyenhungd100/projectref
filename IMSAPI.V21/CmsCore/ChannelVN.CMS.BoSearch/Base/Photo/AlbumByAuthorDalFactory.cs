﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public class AlbumByAuthorDalFactory
    {
        public static bool SaveAlbumByAuthor(AlbumByAuthorSearchEntity albumByAuthorES)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.AlbumByAuthorDal.SaveAlbumByAuthor(albumByAuthorES);
            }
            return returnValue;
        }

        public static int GetAuthorIdByAlbumId(int albumId)
        {
            var result = 0;
            using (var db = new CmsMainSearchDb())
            {
                result = db.AlbumByAuthorDal.GetAuthorIdByAlbumId(albumId);
            }
            return result;
        }
    }
}
