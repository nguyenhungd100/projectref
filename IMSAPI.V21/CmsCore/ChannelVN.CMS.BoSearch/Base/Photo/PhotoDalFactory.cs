﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public class PhotoDalFactory
    {
        public static bool AddPhoto(PhotoSearchEntity Album)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.AddPhoto(Album);
            }
            return returnValue;
        }

        public static bool UpdatePhoto(PhotoSearchEntity Album)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.UpdatePhoto(Album);
            }
            return returnValue;
        }     

        public static PhotoSearchEntity GetById(long id)
        {
            PhotoSearchEntity returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.GetById(id);
            }
            return returnValue;
        }

        public static List<PhotoSearchEntity> SearchPhoto(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.SearchPhoto(keyword, zoneId, albumId, status, fromDate, toDate, createdBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<PhotoSearchEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<PhotoSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CountAlbumStatus> CountPhoto(string username, List<string> listStatus, string userAction)
        {
            List<CountAlbumStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.CountPhoto(username, listStatus, userAction);
            }
            return returnValue;
        }

        public static bool Publish(int photoId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.Publish(photoId, account);
            }
            return returnValue;
        }

        public static bool UnPublish(int photoId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.UnPublish(photoId, account);
            }
            return returnValue;
        }

        public static bool MoveTrash(long photoId, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.MoveTrash(photoId, account);
            }
            return returnValue;
        }

        public static bool MoveTrash(string photoIds, string account)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.MoveTrash(photoIds, account);
            }
            return returnValue;
        }

        public static bool ChangeStatus(long photoId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.ChangeStatus(photoId, status, username);
            }
            return returnValue;
        }       

        public static bool InitAllPhoto(List<PhotoSearchEntity> listPhoto)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.PhotoDal.InitAllPhoto(listPhoto);
            }
            return returnValue;
        }      
    }
}
