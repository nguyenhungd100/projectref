﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public abstract class AlbumByAuthorDalBase
    {       
        public bool SaveAlbumByAuthor(AlbumByAuthorSearchEntity albumByAuthorES)
        {
            bool returnValue = false;
            try
            {
                if (albumByAuthorES != null)
                {
                    var fieldNameAnalyzer = "album_id";
                    var fieldNameAnalyzer2 = "author_id";
                    var albumByAuthorExist = _db.Get<AlbumByAuthorSearchEntity>(albumByAuthorES.Id);
                    if (albumByAuthorExist == null)
                    {
                        returnValue = _db.CreateIndexSettings<AlbumByAuthorSearchEntity>(fieldNameAnalyzer, fieldNameAnalyzer2);
                        returnValue = _db.Create(albumByAuthorES);
                    }
                    else
                    {
                        albumByAuthorExist.AuthorId = albumByAuthorES.AuthorId;
                        returnValue = _db.Update<AlbumByAuthorSearchEntity>(albumByAuthorExist.Id, albumByAuthorExist);
                    }                  
                }
                return returnValue;
            }
            catch { return returnValue; }
        }

        public int GetAuthorIdByAlbumId(int albumId)
        {
            var result = 0;
            try
            {
                var albumByAuthorExist = default(AlbumByAuthorSearchEntity);
                var listSearch = new List<AlbumByAuthorSearchEntity>();
                var searchRequest = new SearchDescriptor<AlbumByAuthorSearchEntity>();
                var termAlbumId = new TermQuery();
                termAlbumId.Field = "album_id";
                termAlbumId.Value = albumId;
                searchRequest.Query(q => q && termAlbumId);
                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    listSearch = data.Data.ToList();
                    if (listSearch.Count() > 0)
                    {
                        albumByAuthorExist = listSearch?.FirstOrDefault();
                        if (albumByAuthorExist != null)
                        {
                            result = albumByAuthorExist.AuthorId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected AlbumByAuthorDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
