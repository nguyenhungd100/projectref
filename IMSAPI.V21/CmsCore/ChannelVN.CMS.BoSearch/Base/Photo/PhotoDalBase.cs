﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public abstract class PhotoDalBase
    {
        public bool AddPhoto(PhotoSearchEntity Photo)
        {
            bool returnValue = false;
            try
            {
                if (Photo != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<PhotoSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(Photo);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdatePhoto(PhotoSearchEntity Photo)
        {
            bool returnValue = false;
            try
            {
                if (Photo != null)
                {
                    var photoAlbumDb = _db.Get<PhotoSearchEntity>(Photo.Id);
                    if (photoAlbumDb != null)
                    {
                        Photo.CreatedBy = photoAlbumDb.CreatedBy;
                        Photo.CreatedDate = photoAlbumDb.CreatedDate;
                        Photo.PublishedBy = photoAlbumDb.PublishedBy;
                        Photo.PublishedDate = photoAlbumDb.PublishedDate;
                    }
                    returnValue = _db.Update<PhotoSearchEntity>(Photo.Id, Photo);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }      

        public bool Publish(int photoId, string account)
        {
            bool returnValue = false;
            try
            {
                var Photo = _db.Get<PhotoSearchEntity>(photoId);
                if (Photo != null)
                {
                    Photo.Status = (int)EnumPhotoStatus.Published;                    
                    returnValue = _db.Update<PhotoSearchEntity>(Photo.Id, Photo);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UnPublish(int photoId, string account)
        {
            bool returnValue = false;
            try
            {
                var Photo = _db.Get<PhotoSearchEntity>(photoId);
                if (Photo != null)
                {
                    Photo.Status = (int)EnumPhotoStatus.Unpublished;
                    returnValue = _db.Update<PhotoSearchEntity>(Photo.Id, Photo);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool MoveTrash(long photoId, string account)
        {
            bool returnValue = false;
            try
            {
                var Photo = _db.Get<PhotoSearchEntity>(photoId);
                if (Photo != null)
                {
                    Photo.Status = (int)EnumPhotoStatus.Unpublished;
                    returnValue = _db.Update<PhotoSearchEntity>(Photo.Id, Photo);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool MoveTrash(string photoIds, string account)
        {
            bool returnValue = false;
            try
            {
                var listSearch = new List<PhotoSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoSearchEntity>();
               
                var termsListId = new TermsQuery();
                termsListId.Field = "_id";
                termsListId.Terms = photoIds.Split(new char[] { ',', ';'});
                
                searchRequest.Query(q => termsListId
                ).From(0).Size(1000);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    listSearch = data.Data.ToList();
                }

                if (listSearch != null && listSearch.Count>0)
                {
                    foreach (var Photo in listSearch)
                    {
                        Photo.Status = (int)EnumPhotoStatus.Unpublished;
                    }
                    returnValue = _db.UpdateMany(listSearch);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool ChangeStatus(long photoId, int status, string username)
        {
            bool returnValue = false;
            try
            {
                if (photoId <= 0)
                {
                    return returnValue;
                }
                var Photo = _db.Get<PhotoSearchEntity>(photoId);
                if (Photo != null)
                {
                    Photo.Status = status;                    
                    Photo.ModifiedBy = username;
                    Photo.ModifiedDate = DateTime.Now;                                      
                }

                return _db.Update<PhotoSearchEntity>(photoId, Photo);
            }
            catch { return returnValue; }
        }

        public PhotoSearchEntity GetById(long id)
        {
            PhotoSearchEntity returnValue = null;
            try
            {
                if (id <= 0)
                {
                    return returnValue;
                }
                return _db.Get<PhotoSearchEntity>(id);                
            }
            catch { return returnValue; }
        }

        public List<PhotoSearchEntity> GetListByListId(List<string> ids)
        {
            try
            {
                var list = new List<PhotoSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoSearchEntity>();

                var termsIds = new TermsQuery();
                termsIds.Field = "id";
                termsIds.Terms = ids.ToArray();

                searchRequest.Query(q => q && termsIds);

                var data = _db.Search(searchRequest);
                if (data != null)
                {                    
                    list = data.Data.ToList();
                }
                return list;
            }
            catch
            {
                return null;
            }            
        }
        
        public List<PhotoSearchEntity> SearchPhoto(string keyword, int zoneId, int albumId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<PhotoSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoSearchEntity>();
                
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy)) { 
                    termUserName.Field = "created_by";
                    termUserName.Value = createdBy;
                }

                //status
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                //zone
                var termZoneId = new TermQuery();
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneId;
                }

                var termAlbumId = new TermQuery();
                if (albumId > 0)
                {
                    termAlbumId.Field = "album_id";
                    termAlbumId.Value = albumId;
                }
                
                //created_date
                string fieldCreatedDate = "created_date";
                var range = new DateRangeQuery();
                range.Field = fieldCreatedDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                               
                var fields = new string[] { "name", "name.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {                    
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termUserName  && range && termStatus && termZoneId && termAlbumId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q && termUserName && range && termStatus && termZoneId && termAlbumId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<PhotoSearchEntity> ListPhotoByAlbumId(int albumId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<PhotoSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoSearchEntity>();
                                                
                var termAlbumId = new TermQuery();
                if (albumId > 0)
                {
                    termAlbumId.Field = "album_id";
                    termAlbumId.Value = albumId;
                }                
                
                searchRequest.Query(q => q && termAlbumId
                ).From((pageIndex - 1) * pageSize).Size(pageSize);               

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<CountAlbumStatus> CountPhoto(string username, List<string> listStatus, string userAction)
        {
            try
            {
                var listSearch = new List<CountAlbumStatus>();
                var searchRequest = new SearchDescriptor<PhotoSearchEntity>();

                var querys = new List<QueryContainerDescriptor<PhotoSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<PhotoSearchEntity>();

                        var termUserName = new TermQuery();
                        if (status == "0" || status == "4")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(userAction))
                            {
                                termUserName.Field = "created_by";
                                termUserName.Value = userAction;
                            }
                        }

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;
                                                
                        query.Bool(boo => boo.Must(mu => termUserName && termByStatus));

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);
                
                if (data != null)
                {
                    listSearch = data.Select(s => new CountAlbumStatus { Status = Utility.ConvertToInt(s.Key), Count = Utility.ConvertToInt(s.DocCount) }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }       

        public bool InitAllPhoto(List<PhotoSearchEntity> listPhoto)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "name";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<PhotoSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listPhoto);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected PhotoDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
