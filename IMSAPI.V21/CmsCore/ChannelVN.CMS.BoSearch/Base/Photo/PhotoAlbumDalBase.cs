﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public abstract class PhotoAlbumDalBase
    {
        public bool AddPhotoAlbum(PhotoAlbumSearchEntity PhotoAlbum)
        {
            bool returnValue = false;
            try
            {
                if (PhotoAlbum != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<PhotoAlbumSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(PhotoAlbum);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdatePhotoAlbum(PhotoAlbumSearchEntity PhotoAlbum)
        {
            bool returnValue = false;
            try
            {
                if (PhotoAlbum != null)
                {
                    var photoAlbumDb = _db.Get<PhotoAlbumSearchEntity>(PhotoAlbum.Id);
                    if (photoAlbumDb != null)
                    {
                        PhotoAlbum.CreatedBy = photoAlbumDb.CreatedBy;
                        PhotoAlbum.CreatedDate = photoAlbumDb.CreatedDate;
                    }
                    returnValue = _db.Update<PhotoAlbumSearchEntity>(PhotoAlbum.Id, PhotoAlbum);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        public bool Publish(int photoAlbumId, string account)
        {
            bool returnValue = false;
            try
            {
                var PhotoAlbum = _db.Get<PhotoAlbumSearchEntity>(photoAlbumId);
                if (PhotoAlbum != null)
                {
                    PhotoAlbum.Status = (int)EnumPhotoAlbumStatus.Published;                    
                    returnValue = _db.Update<PhotoAlbumSearchEntity>(PhotoAlbum.Id, PhotoAlbum);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UnPublish(int photoAlbumId, string account)
        {
            bool returnValue = false;
            try
            {
                var PhotoAlbum = _db.Get<PhotoAlbumSearchEntity>(photoAlbumId);
                if (PhotoAlbum != null)
                {
                    PhotoAlbum.Status = (int)EnumPhotoAlbumStatus.Unpublished;
                    returnValue = _db.Update<PhotoAlbumSearchEntity>(PhotoAlbum.Id, PhotoAlbum);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool MoveTrash(int photoAlbumId, string account)
        {
            bool returnValue = false;
            try
            {
                var PhotoAlbum = _db.Get<PhotoAlbumSearchEntity>(photoAlbumId);
                if (PhotoAlbum != null)
                {
                    PhotoAlbum.Status = (int)EnumPhotoAlbumStatus.Unpublished;
                    returnValue = _db.Update<PhotoAlbumSearchEntity>(PhotoAlbum.Id, PhotoAlbum);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool ChangeStatus(int photoAlbumId, int status, string username)
        {
            bool returnValue = false;
            try
            {
                if (photoAlbumId <= 0)
                {
                    return returnValue;
                }
                var PhotoAlbum = _db.Get<PhotoAlbumSearchEntity>(photoAlbumId);
                if (PhotoAlbum != null)
                {
                    PhotoAlbum.Status = status;                    
                    PhotoAlbum.ModifiedBy = username;
                    PhotoAlbum.ModifiedDate = DateTime.Now;                                      
                }

                return _db.Update<PhotoAlbumSearchEntity>(photoAlbumId, PhotoAlbum);
            }
            catch { return returnValue; }
        }

        public PhotoAlbumSearchEntity GetById(int id)
        {
            PhotoAlbumSearchEntity returnValue = null;
            try
            {
                if (id <= 0)
                {
                    return returnValue;
                }
                return _db.Get<PhotoAlbumSearchEntity>(id);                
            }
            catch { return returnValue; }
        }

        public List<PhotoAlbumSearchEntity> GetListByListId(List<string> ids)
        {
            try
            {
                var list = new List<PhotoAlbumSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoAlbumSearchEntity>();

                var termsIds = new TermsQuery();
                termsIds.Field = "id";
                termsIds.Terms = ids.ToArray();

                searchRequest.Query(q => q && termsIds);

                var data = _db.Search(searchRequest);
                if (data != null)
                {                    
                    list = data.Data.ToList();
                }
                return list;
            }
            catch
            {
                return null;
            }            
        }
        
        public List<PhotoAlbumSearchEntity> SearchPhotoAlbum(string keyword, int zoneId, int eventId, int status, DateTime fromDate, DateTime toDate, string createdBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<PhotoAlbumSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoAlbumSearchEntity>();
                
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy)) { 
                    termUserName.Field = "created_by";
                    termUserName.Value = createdBy;
                }

                //status
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                //zone
                var termZoneId = new TermQuery();
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_id";
                    termZoneId.Value = zoneId;
                }

                var termsEventIds = new TermsQuery();
                if (eventId > 0)
                {
                    termsEventIds.Field = "event_ids";
                    termsEventIds.Terms = new string[] { eventId.ToString() };
                }
                
                //created_date
                string fieldCreatedDate = "created_date";
                var range = new DateRangeQuery();
                range.Field = fieldCreatedDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                               
                var fields = new string[] { "name", "name.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {                    
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termUserName  && range && termStatus && termZoneId && termsEventIds
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q && termUserName && range && termStatus && termZoneId && termsEventIds
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<CountAlbumStatus> CountPhotoAlbum(string username, List<string> listStatus, string userAction)
        {
            try
            {
                var listSearch = new List<CountAlbumStatus>();
                var searchRequest = new SearchDescriptor<PhotoAlbumSearchEntity>();

                var querys = new List<QueryContainerDescriptor<PhotoAlbumSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<PhotoAlbumSearchEntity>();

                        var termUserName = new TermQuery();
                        if (status == "0" || status == "4")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(userAction))
                            {
                                termUserName.Field = "created_by";
                                termUserName.Value = userAction;
                            }
                        }

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;
                                                
                        query.Bool(boo => boo.Must(mu => termUserName && termByStatus));

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);
                
                if (data != null)
                {
                    listSearch = data.Select(s => new CountAlbumStatus { Status = Utility.ConvertToInt(s.Key), Count = Utility.ConvertToInt(s.DocCount) }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }       

        public bool InitAllPhotoAlbum(List<PhotoAlbumSearchEntity> listPhotoAlbum)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "name";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<PhotoAlbumSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listPhotoAlbum);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected PhotoAlbumDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
