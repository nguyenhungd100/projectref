﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Photo
{
    public abstract class PhotoByAuthorDalBase
    {
        public bool SavePhotoByAuthor(PhotoByAuthorSearchEntity photoByAuthorES)
        {
            bool returnValue = false;
            try
            {
                if (photoByAuthorES != null)
                {
                    var fieldNameAnalyzer = "photo_id";
                    var fieldNameAnalyzer2 = "author_id";
                    var photoByAuthorExist = _db.Get<PhotoByAuthorSearchEntity>(photoByAuthorES.Id);
                    if (photoByAuthorExist == null)
                    {
                        returnValue = _db.CreateIndexSettings<PhotoByAuthorSearchEntity>(fieldNameAnalyzer, fieldNameAnalyzer2);
                        returnValue = _db.Create(photoByAuthorES);
                    }
                    else
                    {
                        photoByAuthorExist.AuthorId = photoByAuthorES.AuthorId;
                        returnValue = _db.Update<PhotoByAuthorSearchEntity>(photoByAuthorExist.Id, photoByAuthorExist);
                    }
                }
                return returnValue;
            }
            catch { return returnValue; }
        }
        public int GetAuthorIdByPhotoId(long photoId)
        {
            var result = 0;
            try
            {
                var photoByAuthorExist = default(PhotoByAuthorSearchEntity);
                var listSearch = new List<PhotoByAuthorSearchEntity>();
                var searchRequest = new SearchDescriptor<PhotoByAuthorSearchEntity>();
                var termPhotoId = new TermQuery();
                termPhotoId.Field = "photo_id";
                termPhotoId.Value = photoId;
                searchRequest.Query(q => q && termPhotoId);
                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    listSearch = data.Data.ToList();
                    if (listSearch.Count() > 0)
                    {
                        photoByAuthorExist = listSearch?.FirstOrDefault();
                        if (photoByAuthorExist != null)
                        {
                            result = photoByAuthorExist.AuthorId;
                        }
                    }                    
                }           
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        #region Core members

        private readonly CmsMainSearchDb _db;

        protected PhotoByAuthorDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
