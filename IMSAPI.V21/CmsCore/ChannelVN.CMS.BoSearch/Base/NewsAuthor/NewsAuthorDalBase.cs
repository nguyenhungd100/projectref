﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.NewsAuthor
{
    public abstract class NewsAuthorDalBase
    {
        public bool AddNewsAuthor(NewsAuthorEntity newsAuthor)
        {
            bool returnValue = false;
            try
            {                
                if (newsAuthor != null)
                {                    
                    var oNewsAuthor = new NewsAuthorSearchEntity
                    {
                        Id = newsAuthor.Id,
                        UnsignName=newsAuthor.UnsignName,
                        AuthorName = newsAuthor.AuthorName,                        
                        UserData = newsAuthor.UserData
                    };
                    var fieldNameAnalyzer = "author_name";
                    returnValue = _db.CreateIndexSettings<NewsAuthorSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<NewsAuthorSearchEntity>(oNewsAuthor);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateNewsAuthor(NewsAuthorEntity newsAuthor)
        {
            bool returnValue = false;
            try
            {                
                if (newsAuthor != null)
                {
                    var oNewsAuthor = _db.Get<NewsAuthorSearchEntity>(newsAuthor.Id);                                       
                    if (oNewsAuthor == null)
                    {
                        oNewsAuthor = new NewsAuthorSearchEntity
                        {
                            Id = newsAuthor.Id,
                            UnsignName = newsAuthor.UnsignName,
                            AuthorName = newsAuthor.AuthorName,                            
                            UserData = newsAuthor.UserData
                        };
                    }
                    else
                    {
                        oNewsAuthor.Id = newsAuthor.Id;
                        oNewsAuthor.UnsignName = newsAuthor.UnsignName;
                        oNewsAuthor.AuthorName = newsAuthor.AuthorName;                       
                        oNewsAuthor.UserData = newsAuthor.UserData;
                    }           

                    returnValue = _db.Update<NewsAuthorSearchEntity>(oNewsAuthor.Id, oNewsAuthor);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public List<string> SearchNewsAuthor(string keyword)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "unsign_name", "author_name", "author_name.folded", "user_data", };
                var searchDescriptor = new SearchDescriptor<NewsAuthorSearchEntity>();

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchDescriptor.Query(q => q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    )
                ).From(0).Size(9999);

                var data = _db.Search(searchDescriptor);
                if (data != null)
                {                   
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool DeleteNewsAuthor(int id)
        {
            bool returnValue = false;
            try
            {
                returnValue = _db.DeleteMapping<NewsAuthorSearchEntity>(id);

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool InitAllNewsAuthor(List<NewsAuthorEntity> listNewsAuthor)
        {
            bool returnValue = false;
            try
            {
                var list = listNewsAuthor.Select(newsAuthor => new NewsAuthorSearchEntity
                {
                    Id = newsAuthor.Id,
                    UnsignName = newsAuthor.UnsignName,
                    AuthorName = newsAuthor.AuthorName,
                    UserData = newsAuthor.UserData
                }).ToList();

                var fieldNameAnalyzer = "author_name";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<NewsAuthorSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany<NewsAuthorSearchEntity>(list);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected NewsAuthorDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
