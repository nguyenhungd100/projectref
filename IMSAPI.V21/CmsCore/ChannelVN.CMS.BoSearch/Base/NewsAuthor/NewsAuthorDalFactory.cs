﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.NewsAuthor
{
    public class NewsAuthorDalFactory
    {
        public static bool AddNewsAuthor(NewsAuthorEntity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsAuthorDal.AddNewsAuthor(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateNewsAuthor(NewsAuthorEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsAuthorDal.UpdateNewsAuthor(news);
            }
            return returnValue;
        }

        public static bool DeleteNewsAuthor(int id)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsAuthorDal.DeleteNewsAuthor(id);
            }
            return returnValue;
        }

        public static List<string> SearchNewsAuthor(string keyword)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsAuthorDal.SearchNewsAuthor(keyword);
            }
            return returnValue;
        }                    

        public static bool InitAllNewsAuthor(List<NewsAuthorEntity> listNewsAuthor)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsAuthorDal.InitAllNewsAuthor(listNewsAuthor);
            }
            return returnValue;
        }
    }
}
