﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.EndUser;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.EndUser
{
    public class EndUserDalFactory
    {
        public static bool AddEndUser(EndUserSearchEntity videoChannel)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.EndUserDal.AddEndUser(videoChannel);
            }
            return returnValue;
        }        

        public static bool UpdateEndUser(EndUserSearchEntity videoChannel)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.EndUserDal.UpdateEndUser(videoChannel);
            }
            return returnValue;
        }

        public static List<EndUserSearchEntity> SearchEndUser(string keyword, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            List<EndUserSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.EndUserDal.SearchEndUser(keyword, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }

        public static object StatictisEndUser(string username, DateTime fromDate, DateTime toDate)
        {
            object returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.EndUserDal.StatictisEndUser(username, fromDate, toDate);
            }
            return returnValue;
        }

        public static bool InitAllEndUser(List<EndUserSearchEntity> listEndUser)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.EndUserDal.InitAllEndUser(listEndUser);
            }
            return returnValue;
        }
    }
}
