﻿using ChannelVN.CMS.BoSearch.Base.EndUser;
using ChannelVN.CMS.BoSearch.Databases;

namespace ChannelVN.CMS.BoSearch.Base.EndUser
{
    public class EndUserDal : EndUserDalBase
    {
        internal EndUserDal(CmsMainSearchDb db)
                : base(db)
        {
        }
    }
}
