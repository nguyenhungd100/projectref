﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.EndUser
{
    public abstract class EndUserDalBase
    {
        public bool AddEndUser(EndUserSearchEntity videoChannel)
        {
            bool returnValue = false;
            try
            {
                if (videoChannel != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<EndUserSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(videoChannel);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateEndUser(EndUserSearchEntity videoChannel)
        {
            bool returnValue = false;
            try
            {
                if (videoChannel != null)
                {                    
                    returnValue = _db.Update<EndUserSearchEntity>(videoChannel.Id, videoChannel);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="zoneIds"></param>
        /// <param name="labelIds"></param>
        /// <param name="playlistIds"></param>
        /// <param name="videoIds"></param>
        /// <param name="status"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public List<EndUserSearchEntity> SearchEndUser(string keyword, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                var listSearch = new List<EndUserSearchEntity>();
                var searchRequest = new SearchDescriptor<EndUserSearchEntity>();

                //sort by created_date
                searchRequest.Sort(s => s.Descending("created_date"));

                //status
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }                

                string fieldDate = "created_date";
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                var fields = new string[] { "name", "name.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termStatus
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q && range && termStatus
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRows = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public object StatictisEndUser(string username, DateTime fromDate, DateTime toDate)
        {
            try
            {
                var listStatus = new List<int> { -1, 1, 2, 3 };

                var listSearch = new List<CountStatus>();
                var searchRequest = new SearchDescriptor<EndUserSearchEntity>();

                var querys = new List<QueryContainerDescriptor<EndUserSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<EndUserSearchEntity>();

                        var termStatus = new TermQuery();
                        if (status != -1)
                        {
                            termStatus.Field = "status";
                            termStatus.Value = status;
                        }

                        string fieldDate = "created_date";
                        var range = new DateRangeQuery();
                        range.Field = fieldDate;
                        if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                        {
                            range.GreaterThanOrEqualTo = fromDate;
                            range.LessThanOrEqualTo = toDate;
                        }
                        else {
                            if (fromDate != DateTime.MinValue)
                            {
                                range.GreaterThanOrEqualTo = fromDate;
                            }
                            else if (toDate != DateTime.MinValue)
                            {
                                range.LessThanOrEqualTo = toDate;
                            }
                        }

                        query.Bool(boo => boo.Must(mu => termStatus && range));

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);                
                if (data != null)
                {
                    listSearch = data.Select(s => new CountStatus { Status = s.Key, Count = s.DocCount }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllEndUser(List<EndUserSearchEntity> listEndUser)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "title";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<EndUserSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listEndUser);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected EndUserDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
