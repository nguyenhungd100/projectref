﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Video;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.VideoChannel
{
    public abstract class VideoChannelDalBase
    {
        public bool AddVideoChannel(VideoChannelSearchEntity videoChannel)
        {
            bool returnValue = false;
            try
            {
                if (videoChannel != null)
                {
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<VideoChannelSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(videoChannel);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateVideoChannel(VideoChannelSearchEntity videoChannel)
        {
            bool returnValue = false;
            try
            {
                if (videoChannel != null)
                {                    
                    returnValue = _db.Update<VideoChannelSearchEntity>(videoChannel.Id, videoChannel);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="zoneIds"></param>
        /// <param name="labelIds"></param>
        /// <param name="playlistIds"></param>
        /// <param name="videoIds"></param>
        /// <param name="status"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRows"></param>
        /// <returns></returns>
        public List<VideoChannelSearchEntity> SearchVideoChannel(string keyword, string zoneIds, string labelIds, string playlistIds, string videoIds, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            try
            {
                var listSearch = new List<VideoChannelSearchEntity>();
                var searchRequest = new SearchDescriptor<VideoChannelSearchEntity>();

                //sort by created_date
                searchRequest.Sort(s => s.Descending("created_date"));

                //status
                var termStatus = new TermQuery();
                if (status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                //zone
                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                //label
                var termsLabelId = new TermsQuery();
                var termsLabelIds = new string[] { };
                if (labelIds != "")
                {
                    termsLabelIds = labelIds.Split(',');
                    termsLabelId.Field = "label_ids";
                    termsLabelId.Terms = termsLabelIds;
                }

                //playlist
                var termsPlaylistId = new TermsQuery();
                var termsPlaylistIds = new string[] { };
                if (playlistIds != "")
                {
                    termsPlaylistIds = playlistIds.Split(',');
                    termsPlaylistId.Field = "playlist_ids";
                    termsPlaylistId.Terms = termsPlaylistIds;
                }

                //playlist
                var termsVideoId = new TermsQuery();
                var termsVideoIds = new string[] { };
                if (videoIds != "")
                {
                    termsVideoIds = videoIds.Split(',');
                    termsVideoId.Field = "video_ids";
                    termsVideoId.Terms = termsVideoIds;
                }

                string fieldDate = "created_date";
                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                var fields = new string[] { "name", "name.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termStatus && termsZoneId && termsLabelId && termsPlaylistId && termsVideoId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q && range && termStatus && termsZoneId && termsLabelId && termsPlaylistId && termsVideoId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);
                if (data != null)
                {
                    totalRows = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool Delete(int channelId)
        {
            bool returnValue = false;
            try
            {
                if (channelId <= 0)
                {
                    return returnValue;
                }

                return _db.DeleteMapping<VideoChannelSearchEntity>(channelId);
            }
            catch { return returnValue; }
        }

        public bool InitAllVideoChannel(List<VideoChannelSearchEntity> listVideoChannel)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "title";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<VideoChannelSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listVideoChannel);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected VideoChannelDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
