﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.VideoChannel
{
    public class VideoChannelDalFactory
    {
        public static bool AddVideoChannel(VideoChannelSearchEntity videoChannel)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoChannelDal.AddVideoChannel(videoChannel);
            }
            return returnValue;
        }        

        public static bool UpdateVideoChannel(VideoChannelSearchEntity videoChannel)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoChannelDal.UpdateVideoChannel(videoChannel);
            }
            return returnValue;
        }

        public static List<VideoChannelSearchEntity> SearchVideoChannel(string keyword, string zoneIds, string labelIds, string playlistIds, string videoIds, int status, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRows)
        {
            List<VideoChannelSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoChannelDal.SearchVideoChannel(keyword, zoneIds, labelIds, playlistIds, videoIds, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
            }
            return returnValue;
        }

        public static bool Delete(int channelId)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoChannelDal.Delete(channelId);
            }
            return returnValue;
        }

        public static bool InitAllVideoChannel(List<VideoChannelSearchEntity> listVideoChannel)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.VideoChannelDal.InitAllVideoChannel(listVideoChannel);
            }
            return returnValue;
        }
    }
}
