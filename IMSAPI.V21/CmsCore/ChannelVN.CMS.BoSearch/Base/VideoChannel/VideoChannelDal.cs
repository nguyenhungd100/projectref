﻿using ChannelVN.CMS.BoSearch.Base.VideoChannel;
using ChannelVN.CMS.BoSearch.Databases;

namespace ChannelVN.CMS.BoSearch.Base.VideoChannel
{
    public class VideoChannelDal : VideoChannelDalBase
    {
        internal VideoChannelDal(CmsMainSearchDb db)
                : base(db)
        {
        }
    }
}
