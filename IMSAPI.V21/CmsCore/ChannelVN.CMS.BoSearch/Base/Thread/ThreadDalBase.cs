﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.Thread;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Thread
{
    public abstract class ThreadDalBase
    {
        public bool AddThread(ThreadEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {                    
                    var oThread = new ThreadSearchEntity
                    {
                        Id = tag.Id,
                        Name=tag.Name
                    };
                    var fieldNameAnalyzer = "name";
                    returnValue = _db.CreateIndexSettings<ThreadSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create<ThreadSearchEntity>(oThread);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateThread(ThreadEntity tag)
        {
            bool returnValue = false;
            try
            {                
                if (tag != null)
                {
                    var oThread = _db.Get<ThreadSearchEntity>(tag.Id);                                       
                    if (oThread == null)
                    {
                        oThread = new ThreadSearchEntity
                        {
                            Id = tag.Id,
                            Name = tag.Name
                        };
                    }
                    else
                    {
                        oThread.Id = tag.Id;
                        oThread.Name = tag.Name;                        
                    }           

                    returnValue = _db.Update<ThreadSearchEntity>(oThread.Id, oThread);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }        

        public List<string> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();

                var searchDescriptor = new SearchDescriptor<ThreadSearchEntity>();

                var fields = new string[] { "name", "name.folded" };
                
                switch (orderBy)
                {                    
                    case 1:
                        searchDescriptor.Sort(s => s.Descending("is_hot"));
                        break;                            
                    default:
                        searchDescriptor.Sort(s => s.Descending("created_date"));                        
                        break;
                }
                                
                var termIsHotThread = new TermQuery();
                if (isHotThread > -1)
                {
                    termIsHotThread.Field = "is_hot";
                    termIsHotThread.Value = (isHotThread == 0) ? false : true;
                }

                var termZoneId = new TermsQuery();                
                if (zoneId > 0)
                {
                    termZoneId.Field = "zone_ids";
                    termZoneId.Terms = new string[] { zoneId.ToString() };                    
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchDescriptor.Query(q =>q
                    .QueryString(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && termIsHotThread && termZoneId
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchDescriptor);                

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllThread(List<ThreadSearchEntity> listThread)
        {
            bool returnValue = false;
            try
            {                
                var fieldNameAnalyzer = "name";                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<ThreadSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(listThread);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }        

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected ThreadDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
