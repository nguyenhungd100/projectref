﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Thread;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.Thread
{
    public class ThreadDalFactory
    {
        public static bool AddThread(ThreadEntity newsAuthor)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ThreadDal.AddThread(newsAuthor);
            }
            return returnValue;
        }        

        public static bool UpdateThread(ThreadEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ThreadDal.UpdateThread(news);
            }
            return returnValue;
        }

        public static List<string> SearchThread(string keyword, int zoneId, int orderBy, int isHotThread, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ThreadDal.SearchThread(keyword, zoneId, orderBy, isHotThread, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }                    

        public static bool InitAllThread(List<ThreadSearchEntity> listThread)
        {
            bool returnValue=false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.ThreadDal.InitAllThread(listThread);
            }
            return returnValue;
        }
    }
}
