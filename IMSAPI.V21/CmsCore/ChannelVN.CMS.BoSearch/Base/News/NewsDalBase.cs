﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.News
{
    public abstract class NewsDalBase
    {
        public bool AddNews(NewsEntity news)
        {
            bool returnValue = false;
            try
            {
                if (news != null)
                {
                    var zoneids = new List<string>();
                    if (news.ListZoneId != null)
                    {
                        var zoneidsTemp = new List<string>();
                        if (news.ZoneId > 0)
                            zoneidsTemp.Add(news.ZoneId.ToString());
                        zoneidsTemp.AddRange(news.ListZoneId.Split(';').Where(s => s != ""));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    var oNews = new NewsSearchEntity
                    {
                        Id = news.Id,
                        EncryptId = news.Id.ToString(),
                        Title = news.Title,
                        ZoneIds = zoneids.ToArray(),
                        Type = news.Type,
                        CreatedBy = news.CreatedBy,
                        LastModifiedBy = news.LastModifiedBy,
                        EditedBy = news.EditedBy,
                        PublishedBy = news.PublishedBy,
                        LastReceiver = news.LastReceiver,
                        CreatedDate = news.CreatedDate,
                        DistributionDate = news.DistributionDate,
                        LastModifiedDate = news.LastModifiedDate,
                        ViewCount = news.ViewCount,
                        Status = news.Status,
                        IsOnHome = news.IsOnHome,
                        Avatar = news.Avatar,
                        ViewRoyalties = 0,
                        ZoneName = news.ZoneName,
                        Note = news.Note,
                        ApprovedBy = news.ApprovedBy,
                        Author = news.Author,
                        WordCount = news.WordCount,
                        Sapo = news.Sapo,
                        Url = news.Url,
                        DisplayPosition = news.DisplayPosition,
                        Priority = news.Priority,
                        Source = news.Source,
                        IsFocus = news.IsFocus,
                        NewsType = news.NewsType,
                        IsProd = news.IsProd,
                        IsPr = news.IsPr,
                        OriginalId = news.OriginalId,
                        ZoneId = news.ZoneId
                    };

                    returnValue = _db.CreateIndexSettings<NewsSearchEntity>("title", "source");
                    returnValue = _db.Create(oNews);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        public bool UpdateNews(NewsEntity news)
        {
            bool returnValue = false;
            try
            {
                if (news != null)
                {
                    var oNews = _db.Get<NewsSearchEntity>(news.Id);
                    var zoneids = new List<string>();
                    if (!string.IsNullOrEmpty(news.ListZoneId))
                    {
                        var zoneidsTemp = new List<string>();
                        if (news.ZoneId > 0)
                            zoneidsTemp.Add(news.ZoneId.ToString());
                        zoneidsTemp.AddRange(news.ListZoneId.Split(';').Where(s => s != ""));
                        zoneids.AddRange(zoneidsTemp.Distinct());
                    }
                    if (oNews == null)
                    {
                        oNews = new NewsSearchEntity
                        {
                            Id = news.Id,
                            EncryptId = news.Id.ToString(),
                            Title = news.Title,
                            ZoneIds = zoneids.ToArray(),
                            Type = news.Type,
                            CreatedBy = news.CreatedBy,
                            LastModifiedBy = news.LastModifiedBy,
                            EditedBy = news.EditedBy,
                            PublishedBy = news.PublishedBy,
                            LastReceiver = news.LastReceiver,
                            CreatedDate = news.CreatedDate,
                            DistributionDate = news.DistributionDate,
                            LastModifiedDate = news.LastModifiedDate,
                            ViewCount = news.ViewCount,
                            Status = news.Status,
                            IsOnHome = news.IsOnHome,
                            Avatar = news.Avatar,
                            ViewRoyalties = 0,
                            ZoneName = news.ZoneName,
                            Note = news.Note,
                            ApprovedBy = news.ApprovedBy,
                            Author = news.Author,
                            WordCount = news.WordCount,
                            Sapo = news.Sapo,
                            Url = news.Url,
                            DisplayPosition = news.DisplayPosition,
                            Priority = news.Priority,
                            Source = news.Source,
                            IsFocus = news.IsFocus,
                            NewsType = news.NewsType,
                            IsProd = news.IsProd,
                            IsPr = news.IsPr,
                            OriginalId = news.OriginalId,
                            ZoneId = news.ZoneId
                        };
                    }
                    else
                    {
                        oNews.Id = news.Id;
                        oNews.EncryptId = news.Id.ToString();
                        oNews.Title = news.Title;
                        oNews.ZoneIds = zoneids.ToArray();
                        oNews.Type = news.Type;
                        oNews.CreatedBy = news.CreatedBy;
                        oNews.LastModifiedBy = news.LastModifiedBy;
                        oNews.EditedBy = news.EditedBy;
                        oNews.LastReceiver = news.LastReceiver;
                        oNews.CreatedDate = news.CreatedDate;
                        oNews.DistributionDate = news.DistributionDate;
                        oNews.LastModifiedDate = news.LastModifiedDate;
                        oNews.ViewCount = news.ViewCount;
                        oNews.Status = news.Status;
                        oNews.IsOnHome = news.IsOnHome;
                        oNews.Avatar = news.Avatar;
                        oNews.ZoneName = news.ZoneName;
                        oNews.Note = news.Note;
                        oNews.ApprovedBy = news.ApprovedBy;
                        oNews.Author = news.Author;
                        oNews.WordCount = news.WordCount;
                        oNews.Sapo = news.Sapo;
                        oNews.Url = news.Url;
                        oNews.DisplayPosition = news.DisplayPosition;
                        oNews.Priority = news.Priority;
                        oNews.Source = news.Source;
                        oNews.IsFocus = news.IsFocus;
                        oNews.NewsType = news.NewsType;
                        oNews.IsProd = news.IsProd;
                        oNews.IsPr = news.IsPr;
                        oNews.OriginalId = news.OriginalId;
                        oNews.ZoneId = news.ZoneId;
                    }

                    returnValue = _db.Update<NewsSearchEntity>(oNews.Id, oNews);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.DisplayPosition = displayPosition;
                    news.LastModifiedBy = accountName;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdatePriority(long newsId, int priority, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Priority = priority;
                    news.LastModifiedBy = accountName;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchForAdStoreEntity>(newsId);
                if (news != null)
                {
                    news.AdStore = adStore == 1 ? true : false;
                    news.AdStoreUrl = adStoreUrl;
                }

                return _db.Update<NewsSearchForAdStoreEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Title = newsTitle;
                    news.LastModifiedBy = accountName;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateOnlyNewsAvatar(long newsId, string newsAvatar, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Avatar = newsAvatar;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateNewsByErrorCheck(long newsId, string newsTitle, string sapo, string avatar, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Title = newsTitle;
                    news.Sapo = sapo;
                    news.Avatar = avatar;
                    news.ErrorCheckedBy = accountName;
                    news.ErrorCheckedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateNewsIsOnHome(long newsId, bool isOnHome, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.IsOnHome = isOnHome;
                    news.LastModifiedBy = accountName;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateNewsIsFocus(long newsId, bool isFocus, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.IsFocus = isFocus;
                    news.LastModifiedBy = accountName;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateNewsSeoReviewStatus(long newsId, int reviewStatus, string accountName)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.SeoReviewStatus = reviewStatus;
                    news.SeoModifiedBy = accountName;
                    if (reviewStatus == (int)SEOMetaNewsReviewStatusEnum.Reviewed)
                    {
                        news.SeoReviewBy = accountName;
                        news.SeoReviewDate = DateTime.Now;
                    }
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        #region Change status
        public bool DeleteNews(long newsId)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    return _db.DeleteMapping(news);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToPublished(long newsId, string username, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    if (string.IsNullOrEmpty(news.PublishedBy) || news.Status != (int)NewsStatus.Published)
                        news.PublishedBy = username;

                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                    if (news.DistributionDate <= DateTime.MinValue)
                        news.DistributionDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified);
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToUnPublished(long newsId, string username, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = (int)NewsStatus.MovedToTrash;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToWaitForPublish(long newsId, string username, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(news.EditedBy))
                        news.EditedBy = username;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToWaitForEditorialBoard(long newsId, string username, int status)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(news.ApprovedBy))
                        news.PublishedBy = username;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return false; }
        }
        public bool ChangeStatusToWaitForEdit(long newsId, string username, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.EditedBy = "";
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToReceivedForEdit(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.EditedBy = receiver;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToRecievedForPublish(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(news.EditedBy))
                        news.EditedBy = username;
                    news.ApprovedBy = receiver;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToReceivedForEditorialBoard(long newsId, string username, string receiver, int status)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(news.ApprovedBy))
                        news.ApprovedBy = username;
                    news.PublishedBy = receiver;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return false; }
        }
        public bool ChangeStatusToReturnedToReporter(long newsId, string username, string note, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.ReturnedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.Note = note;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool UpdateNewsNoteByExpert(long newsId, string note)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0 || string.IsNullOrEmpty(note))
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Note = note;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToReturnedToEditorialSecretary(long newsId, string approvedBy, string username, string note, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.ApprovedBy = approvedBy;
                    news.ReturnedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.Note = note;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToReturnedToEditor(long newsId, string receiver, string username, string note, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.EditedBy = receiver;
                    news.ReturnedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.Note = note;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusForwardToOtherEditor(long newsId, string receiver)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.EditedBy = receiver;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusForwardToOtherSecretary(long newsId, string receiver)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.ApprovedBy = receiver;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToRecieveFromReturnStore(long newsId, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToRestoreFromTrash(long newsId, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedDate = DateTime.Now;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToCrawlerForEdit(long newsId, string username, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.CreatedBy = username;
                    news.CreatedDate = DateTime.Now;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.ZoneIds = new string[] { "0" };
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        public bool ChangeStatusToReturnToCooperator(long newsId, string username, string note, int status)
        {
            bool returnValue = false;
            try
            {
                if (newsId <= 0)
                {
                    return returnValue;
                }
                var news = _db.Get<NewsSearchEntity>(newsId);
                if (news != null)
                {
                    news.Status = status;
                    news.LastModifiedBy = username;
                    news.LastModifiedDate = DateTime.Now;
                    news.Note = note;
                }

                return _db.Update<NewsSearchEntity>(newsId, news);
            }
            catch { return returnValue; }
        }
        #endregion        

        public List<NewsSearchEntity> ListNewsByStatus(string username, int typeOfCheck, string zoneIds, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if(username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                            {
                                termUserName.Field = "error_checked_by";
                                termUserName.Value = username;
                            }
                            else if (typeOfCheck == 1)
                            {
                                termUserNameMustnot.Field = "error_checked_by";
                                termUserNameMustnot.Value = username;
                            }
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (type != 0 && type != -1)
                if (type > 0)
                {
                    termType.Field = "type";
                    termType.Value = type;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                //if (pageIndex >= 10) { return null; }

                //searchRequest.Query(q => q
                //    .Bool(b=>b
                //        .Must(m=> termStatus && termUserName && termsZoneId)
                //    )                    
                //).From((pageIndex - 1) * pageSize).Size(pageSize);

                searchRequest.Query(q => termStatus && termType && termUserName && termsZoneId
                                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> ListNewsByMultiStatus(string keyword, string author, DateTime fromDate, DateTime toDate, List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var querys = new List<QueryContainerDescriptor<NewsSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<NewsSearchEntity>();

                        var termsZoneIds = new string[] { };
                        if (statusZoneIds != null)
                        {                           
                            termsZoneIds = statusZoneIds[status].GetType().GetProperty("zoneIds").GetValue(statusZoneIds[status], null).Split(',');
                        }
                        var termsByZoneId = new TermsQuery();
                        termsByZoneId.Field = "zone_ids";
                        termsByZoneId.Terms = termsZoneIds;

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;

                        var termByType = new TermQuery();
                        var termTypeNotIn = new TermQuery();
                        if (type > 0)
                        {
                            termByType.Field = "type";
                            termByType.Value = type;
                        }
                        else
                        {
                            termTypeNotIn.Field = "type";
                            termTypeNotIn.Value = 26;
                        }

                        var filterFieldForUsername = statusZoneIds[status].GetType().GetProperty("filterFieldForUsername").GetValue(statusZoneIds[status], null);
                        var username = statusZoneIds[status].GetType().GetProperty("username").GetValue(statusZoneIds[status], null);
                        var termUserNameMustnot = new TermQuery();
                        var termUserName = new TermQuery();
                        if (!string.IsNullOrEmpty(username))
                        {
                            //if (username.Contains("@"))
                            //    username = username.Substring(0, username.IndexOf('@'));

                            switch ((int)filterFieldForUsername)
                            {
                                case 0:
                                    termUserName.Field = "created_by";
                                    termUserName.Value = username;
                                    break;
                                case 1:
                                    termUserName.Field = "last_modified_by";
                                    termUserName.Value = username;
                                    break;
                                case 2:
                                    termUserName.Field = "published_by";
                                    termUserName.Value = username;
                                    break;
                                case 3:
                                    termUserName.Field = "edited_by";
                                    termUserName.Value = username;
                                    break;
                                case 4:
                                    termUserName.Field = "last_receiver";
                                    termUserName.Value = username;
                                    break;
                                case 5:
                                    termUserName.Field = "approved_by";
                                    termUserName.Value = username;
                                    break;
                                case 6:
                                    termUserName.Field = "returned_by";
                                    termUserName.Value = username;
                                    break;
                                case 7:
                                    termUserName.Field = "sent_by";
                                    termUserName.Value = username;
                                    break;
                                case 8:
                                    if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                                    {
                                        termUserName.Field = "error_checked_by";
                                        termUserName.Value = username;
                                    }
                                    else if (typeOfCheck == 1)
                                    {
                                        termUserNameMustnot.Field = "error_checked_by";
                                        termUserNameMustnot.Value = username;
                                    }
                                    break;
                                case 9:
                                    termUserName.Field = "sensitive_checked_by";
                                    termUserName.Value = username;
                                    break;
                            }
                        }

                        query.Bool(boo => boo.Must(mu => termByStatus && termByType && termUserName,
                                mu => mu.Bool(bl => bl.Should(sh => termsByZoneId))
                            )
                            .MustNot(termUserNameMustnot)
                            .MustNot(termTypeNotIn)
                        );

                        querys.Add(query);
                    }
                }

                string fieldDate = "created_date";

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) && q.MultiMatch(mp => mp
                             .Query(author)
                             .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                             .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                             //.DefaultOperator(Operator.And)
                             .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                             .Query(author)
                             .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                             .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                             //.DefaultOperator(Operator.And)
                             .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> ListNewsProcessing(string keyword, string author, DateTime fromDate, DateTime toDate, List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var querys = new List<QueryContainerDescriptor<NewsSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<NewsSearchEntity>();

                        var termsZoneIds = new string[] { };
                        if (statusZoneIds != null)
                        {
                            termsZoneIds = statusZoneIds[status].GetType().GetProperty("zoneIds").GetValue(statusZoneIds[status], null).Split(',');
                        }
                        var termsByZoneId = new TermsQuery();
                        termsByZoneId.Field = "zone_ids";
                        termsByZoneId.Terms = termsZoneIds;

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;

                        var termByType = new TermQuery();
                        var termTypeNotIn = new TermQuery();
                        if (type > 0)
                        {
                            termByType.Field = "type";
                            termByType.Value = type;
                        }
                        else
                        {
                            termTypeNotIn.Field = "type";
                            termTypeNotIn.Value = 26;
                        }

                        query.Bool(boo => boo.Must(mu => termByStatus && termByType, //&& termUserName,
                                mu => mu.Bool(bl => bl.Should(sh => termsByZoneId))
                            )
                            //.MustNot(termUserNameMustnot)
                            .MustNot(termTypeNotIn)
                        );

                        querys.Add(query);
                    }
                }

                string fieldDate = "created_date";

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) && q.MultiMatch(mp => mp
                             .Query(author)
                             .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                             .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                             //.DefaultOperator(Operator.And)
                             .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && q.MultiMatch(mp => mp
                             .Query(author)
                             .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                             .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                             //.DefaultOperator(Operator.And),
                             .Type(TextQueryType.Phrase)
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m => m
                                .Bool(bo => bo
                                    .Should(querys.ToArray())
                                )
                            )
                        ) && range && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> ListNewsIsPr(string keyword, string username, string zoneIds, int status, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("created_date"));

                var termCreated = new TermQuery();
                var termAnother3 = new TermQuery();
                var termStatusAnother3 = new TermQuery();
                var termAnother6 = new TermQuery();
                var termStatusAnother6 = new TermQuery();

                var termStatus2 = new TermQuery();
                var termStatus5 = new TermQuery();
                var termStatus8 = new TermQuery();

                var termCheckStatus = new TermQuery();
                if (status != (int)NewsStatus.All)
                {
                    termCheckStatus = new TermQuery
                    {
                        Field = "status",
                        Value = status
                    };
                }

                // Xóa tạm
                var termStatus9 = new TermQuery
                {
                    Field = "status",
                    Value = 9
                };
                // Gỡ xuất bản
                var termStatus10 = new TermQuery
                {
                    Field = "status",
                    Value = 10
                };

                if (!string.IsNullOrEmpty(username))
                {
                    foreach (var item in filterFieldForUsername)
                    {
                        if (item == 0)
                        {
                            termCreated.Field = "created_by";
                            termCreated.Value = username;
                        }

                        if (item == 3)
                        {
                            termAnother3.Field = "edited_by";
                            termAnother3.Value = username;
                            termStatusAnother3.Field = "status";
                            termStatusAnother3.Value = 3;

                            termStatus2.Field = "status";
                            termStatus2.Value = 2;
                        }

                        if (item == 6)
                        {
                            termAnother6.Field = "approved_by";
                            termAnother6.Value = username;
                            termStatusAnother6.Field = "status";
                            termStatusAnother6.Value = 6;

                            termStatus5.Field = "status";
                            termStatus5.Value = 5;

                            termStatus8.Field = "status";
                            termStatus8.Value = 8;
                        }

                    }
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termIsPr = new TermQuery();
                termIsPr.Field = "is_pr";
                termIsPr.Value = true;

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) &&
                        q.Bool(b =>
                                b.Must(m =>
                                    termIsPr && termsZoneId && range,
                                    m => m.Bool(bo => bo
                                            .Should(s =>
                                                (
                                                    (termCreated ||
                                                    (termAnother3 && termStatusAnother3 || termStatus2) ||
                                                    (termAnother6 && termStatusAnother6 || termStatus5 || termStatus8))
                                                    ) && termCheckStatus)
                                                )
                                )
                                .MustNot(mn =>
                                    termStatus10 || termStatus9
                                )
                        )
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m =>
                                termIsPr && termsZoneId && range,
                                m => m.Bool(bo => bo
                                    .Should(s =>
                                        (
                                            (termCreated ||
                                            (termAnother3 && termStatusAnother3 || termStatus2) ||
                                            (termAnother6 && termStatusAnother6 || termStatus5 || termStatus8)
                                            ) && termCheckStatus
                                        )
                                    )
                                )
                            )
                            .MustNot(mn =>
                                termStatus10 || termStatus9
                            )
                        )
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> ListNewsIsPrSubsite(string keyword, string username, string zoneIds, List<int> zoneTotalChecks, int status, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("created_date"));

                var termCreated = new TermQuery();
                var termAnother3 = new TermQuery();
                var termStatusAnother3 = new TermQuery();
                var termAnother6 = new TermQuery();
                var termStatusAnother6 = new TermQuery();

                var termStatus2 = new TermQuery();
                var termStatus5 = new TermQuery();
                var termStatus8 = new TermQuery();

                var termCheckStatus = new TermQuery();
                if (status != (int)NewsStatus.All)
                {
                    termCheckStatus = new TermQuery
                    {
                        Field = "status",
                        Value = status
                    };
                }

                // Xóa tạm
                var termStatus9 = new TermQuery
                {
                    Field = "status",
                    Value = 9
                };
                // Gỡ xuất bản
                var termStatus10 = new TermQuery
                {
                    Field = "status",
                    Value = 10
                };

                if (!string.IsNullOrEmpty(username))
                {
                    foreach (var item in filterFieldForUsername)
                    {
                        if (item == 0)
                        {
                            termCreated.Field = "created_by";
                            termCreated.Value = username;
                        }

                        if (item == 3)
                        {
                            termAnother3.Field = "edited_by";
                            termAnother3.Value = username;
                            termStatusAnother3.Field = "status";
                            termStatusAnother3.Value = 3;

                            termStatus2.Field = "status";
                            termStatus2.Value = 2;
                        }

                        if (item == 6)
                        {
                            termAnother6.Field = "approved_by";
                            termAnother6.Value = username;
                            termStatusAnother6.Field = "status";
                            termStatusAnother6.Value = 6;

                            termStatus5.Field = "status";
                            termStatus5.Value = 5;

                            termStatus8.Field = "status";
                            termStatus8.Value = 8;
                        }

                    }
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new List<string>();
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',').ToList();
                    if (zoneTotalChecks.Count() > 0)
                    {
                        termsZoneIds?.AddRange(zoneTotalChecks.Select(c => c.ToString()));
                    }
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds?.Distinct();
                }

                var termIsPr = new TermQuery();
                termIsPr.Field = "is_pr";
                termIsPr.Value = true;

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q.MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        ) &&
                        q.Bool(b =>
                                b.Must(m =>
                                    termIsPr && termsZoneId && range,
                                    m => m.Bool(bo => bo
                                            .Should(s =>
                                                (
                                                    (termCreated ||
                                                    (termAnother3 && termStatusAnother3 || termStatus2) ||
                                                    (termAnother6 && termStatusAnother6 || termStatus5 || termStatus8))
                                                    ) && termCheckStatus)
                                                )
                                )
                                .MustNot(mn =>
                                    termStatus10 || termStatus9
                                )
                        )
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        .Bool(b => b
                            .Must(m =>
                                termIsPr && termsZoneId && range,
                                m => m.Bool(bo => bo
                                    .Should(s =>
                                        (
                                            (termCreated ||
                                            (termAnother3 && termStatusAnother3 || termStatus2) ||
                                            (termAnother6 && termStatusAnother6 || termStatus5 || termStatus8)
                                            ) && termCheckStatus
                                        )
                                    )
                                )
                            )
                            .MustNot(mn =>
                                termStatus10 || termStatus9
                            )
                        )
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public List<NewsSearchEntity> ListNewsPublishByDate(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termTypeNotIn = new TermQuery();
                termTypeNotIn.Field = "type";
                termTypeNotIn.Value = 26;

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                searchRequest.Query(q => termStatus && range && q.Bool(b => b.MustNot(termTypeNotIn))
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> ListNewsPublish(string accountName, DateTime fromDate, DateTime toDate, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termTypeNotIn = new TermQuery();
                termTypeNotIn.Field = "type";
                termTypeNotIn.Value = 26;

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (!string.IsNullOrEmpty(zoneIds))
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(accountName))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = accountName;
                }

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                searchRequest.Query(q => termUserName && termStatus && termsZoneId && range && q.Bool(b => b.MustNot(termTypeNotIn))
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsForSeo(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termUsername = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUsername.Field = "created_by";
                    termUsername.Value = username;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                if (string.IsNullOrEmpty(keyword))
                {
                    searchRequest.Query(q => termStatus && termsZoneId && range && termUsername
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q.QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termStatus && termsZoneId && range && termUsername
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchForAdStoreEntity> SearchNewsForAdPage(string keyword, int zoneId, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchForAdStoreEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchForAdStoreEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termsZoneId = new TermQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_id";
                    termsZoneId.Value = zoneId;
                }

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else
                {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                if (string.IsNullOrEmpty(keyword))
                {
                    searchRequest.Query(q => termStatus && termsZoneId && range
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q.QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(new string[] { "title", "title.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termStatus && termsZoneId && range
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<long> ListNewsPublishByUserName(string username, string zoneIds, int top)
        {
            try
            {
                var listSearch = new List<long>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username;
                }

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                range.GreaterThanOrEqualTo = DateTime.Now.AddHours(-24);
                range.LessThanOrEqualTo = DateTime.Now;

                searchRequest.Query(q => termStatus && termsZoneId && range).Size(top);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    listSearch = data.Data.Select(s => s.Id).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> ListMyNews(string username, string zoneIds, List<int> filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();

                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termCreated = new TermQuery();
                var termAnother3 = new TermQuery();
                var termStatusAnother3 = new TermQuery();
                var termAnother6 = new TermQuery();
                var termStatusAnother6 = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    foreach (var item in filterFieldForUsername)
                    {
                        if (item == 0)
                        {
                            termCreated.Field = "created_by";
                            termCreated.Value = username;
                        }

                        if (item == 3)
                        {
                            termAnother3.Field = "edited_by";
                            termAnother3.Value = username;
                            termStatusAnother3.Field = "status";
                            termStatusAnother3.Value = 3;
                        }

                        if (item == 6)
                        {
                            termAnother6.Field = "approved_by";
                            termAnother6.Value = username;
                            termStatusAnother6.Field = "status";
                            termStatusAnother6.Value = 6;
                        }

                    }
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (type != 0 && type != -1)
                if (type > 0)
                {
                    termType.Field = "type";
                    termType.Value = type;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                //if (pageIndex >= 10) { return null; }

                searchRequest.Query(q => q.Bool(b => b.Must(m => termType && termsZoneId,
                                m => m.Bool(bo => bo.Should(s => termCreated || (termAnother3 && termStatusAnother3) || (termAnother6 && termStatusAnother6)))
                               )
                               .MustNot(termTypeNotIn)
                    )
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> SearchNews(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                            {
                                termUserName.Field = "error_checked_by";
                                termUserName.Value = username;
                            }
                            else if (typeOfCheck == 1)
                            {
                                termUserNameMustnot.Field = "error_checked_by";
                                termUserNameMustnot.Value = username;
                            }
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };

                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && q.MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    var test = data.Data.Where(c => c.ZoneId != 128).ToList();
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsByZoneParent(string keyword, int typeOfCheck, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                            {
                                termUserName.Field = "error_checked_by";
                                termUserName.Value = username;
                            }
                            else if (typeOfCheck == 1)
                            {
                                termUserNameMustnot.Field = "error_checked_by";
                                termUserNameMustnot.Value = username;
                            }
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new List<string>();

                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',').ToList();
                    if (zoneTotalChecks.Count() > 0)
                    {
                        termsZoneIds?.AddRange(zoneTotalChecks.Select(c => c.ToString()));
                    }
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds?.Distinct();
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && q.MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public List<NewsSearchEntity> SearchNewsSeoReviewStatus(string keyword, int reviewStatus, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            termUserName.Field = "error_checked_by";
                            termUserName.Value = username;
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termReviewStatus = new TermQuery();
                termReviewStatus.Field = "seo_review_status";
                termReviewStatus.Value = reviewStatus;

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .AllowLeadingWildcard(true)
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && q.QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsSeoReviewStatusSubsite(string keyword, int reviewStatus, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            termUserName.Field = "error_checked_by";
                            termUserName.Value = username;
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termReviewStatus = new TermQuery();
                termReviewStatus.Field = "seo_review_status";
                termReviewStatus.Value = reviewStatus;

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new List<string>();
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',').ToList();
                    if (zoneTotalChecks.Count() > 0)
                    {
                        termsZoneIds?.AddRange(zoneTotalChecks.Select(c => c.ToString()));
                    }
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds?.Distinct();
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .AllowLeadingWildcard(true)
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && q.QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy && termReviewStatus
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public List<NewsSearchEntity> SearchNewsPublishES(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                            {
                                termUserName.Field = "error_checked_by";
                                termUserName.Value = username;
                            }
                            else if (typeOfCheck == 1)
                            {
                                termUserNameMustnot.Field = "error_checked_by";
                                termUserNameMustnot.Value = username;
                            }
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termUserName.Field = "created_by";
                    termUserName.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;

                    //ko lay bai boom    
                    if (to >= DateTime.Now)
                        range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    else
                        range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        //ko lay bai boom    
                        if (to >= DateTime.Now)
                            range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        else
                            range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .AllowLeadingWildcard(true)
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && q.QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            //.Query("*" + keyword + "*")                            
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsPublishSubsiteES(string keyword, int typeOfCheck, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termUserNameMustnot = new TermQuery();
                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    //if (username.Contains("@"))
                    //    username = username.Substring(0, username.IndexOf('@'));

                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                            break;
                        case 1:
                            termUserName.Field = "last_modified_by";
                            termUserName.Value = username;
                            break;
                        case 2:
                            termUserName.Field = "published_by";
                            termUserName.Value = username;
                            break;
                        case 3:
                            termUserName.Field = "edited_by";
                            termUserName.Value = username;
                            break;
                        case 4:
                            termUserName.Field = "last_receiver";
                            termUserName.Value = username;
                            break;
                        case 5:
                            termUserName.Field = "approved_by";
                            termUserName.Value = username;
                            break;
                        case 6:
                            termUserName.Field = "returned_by";
                            termUserName.Value = username;
                            break;
                        case 7:
                            termUserName.Field = "sent_by";
                            termUserName.Value = username;
                            break;
                        case 8:
                            if (typeOfCheck == 2)//lấy tất cả bài của soát lỗi
                            {
                                termUserName.Field = "error_checked_by";
                                termUserName.Value = username;
                            }
                            else if (typeOfCheck == 1)
                            {
                                termUserNameMustnot.Field = "error_checked_by";
                                termUserNameMustnot.Value = username;
                            }
                            break;
                        case 9:
                            termUserName.Field = "sensitive_checked_by";
                            termUserName.Value = username;
                            break;
                    }
                }

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termUserName.Field = "created_by";
                    termUserName.Value = createdBy;
                }

                var termStatus = new TermQuery();
                if (status != 0 && status != -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new List<string>();
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',').ToList();
                    if (zoneTotalChecks.Count() > 0)
                    {
                        termsZoneIds?.AddRange(zoneTotalChecks.Select(c => c.ToString()));
                    }
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds?.Distinct();
                }

                var termType = new TermQuery();
                var termTypeNotIn = new TermQuery();
                //if (newsType != -1)
                if (newsType > 0)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                else
                {
                    termTypeNotIn.Field = "type";
                    termTypeNotIn.Value = 26;
                }

                string fieldDate = "created_date";
                if (status == 8)
                {
                    fieldDate = "distribution_date";
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;

                    //ko lay bai boom    
                    if (to >= DateTime.Now)
                        range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    else
                        range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        //ko lay bai boom    
                        if (to >= DateTime.Now)
                            range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        else
                            range.LessThanOrEqualTo = to;
                    }
                }
                var fields = new string[] { "title", "title.folded" };

                //if (pageIndex >= 10) { return null; }

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .AllowLeadingWildcard(true)
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && q.QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            //.Query("*" + keyword + "*")                            
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && range && termUserName && termStatus && termType && termsZoneId && termCreatedBy
                        && q.Bool(b => b.MustNot(termTypeNotIn)) && q.Bool(b => b.MustNot(termUserNameMustnot))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public List<NewsSearchEntity> SearchNewsForNewsPositionES(string keyword, int zoneId, int displayPosition, int priority, bool showBomb, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termDisplayPosition = new TermQuery();
                if (displayPosition >= 0)
                {
                    termDisplayPosition.Field = "display_posotion";
                    termDisplayPosition.Value = displayPosition;
                }

                var termPriority = new TermQuery();
                var termPriority1 = new TermQuery();
                var termPriority2 = new TermQuery();
                if (priority >= 0)
                {
                    if (priority == 3)
                    {
                        termPriority1.Field = "priority";
                        termPriority1.Value = 1;

                        termPriority2.Field = "priority";
                        termPriority2.Value = 2;
                    }
                    else
                    {
                        termPriority.Field = "priority";
                        termPriority.Value = priority;
                    }
                }

                var termsZoneId = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneId.ToString() };
                }

                //ko lay bai boom
                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                //range.GreaterThanOrEqualTo = DateTime.MinValue;
                if (!showBomb)
                    range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termStatus && termsZoneId && termDisplayPosition && range && (termPriority || termPriority1 || termPriority2)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && termStatus && termsZoneId && termDisplayPosition && range && (termPriority || termPriority1 || termPriority2)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsForNewsPositionESInDateNow(string keyword, int zoneId, int displayPosition, int priority, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var dateNow = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                var dateForm = DateTime.MinValue;

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termDisplayPosition = new TermQuery();
                if (displayPosition >= 0)
                {
                    termDisplayPosition.Field = "display_posotion";
                    termDisplayPosition.Value = displayPosition;
                    dateForm = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);//lay trong ngay suggert web
                }

                var termPriority = new TermQuery();
                var termPriority1 = new TermQuery();
                var termPriority2 = new TermQuery();
                if (priority >= 0)
                {
                    if (priority == 3)
                    {
                        termPriority1.Field = "priority";
                        termPriority1.Value = 1;

                        termPriority2.Field = "priority";
                        termPriority2.Value = 2;
                    }
                    else
                    {
                        termPriority.Field = "priority";
                        termPriority.Value = priority;
                    }
                    dateForm = new DateTime(dateNow.Year, dateNow.Month, dateNow.Day, 0, 0, 0);//lay trong ngay suggert mobile
                }

                var termsZoneId = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneId.ToString() };
                }

                //ko lay bai boom
                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                range.GreaterThanOrEqualTo = dateForm;
                range.LessThanOrEqualTo = dateNow;

                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termStatus && termsZoneId && termDisplayPosition && range && (termPriority || termPriority1 || termPriority2)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && termStatus && termsZoneId && termDisplayPosition && range && (termPriority || termPriority1 || termPriority2)
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                //var termTopicId = new TermQuery();
                //if (topicId >= 0)
                //{
                //    termTopicId.Field = "display_posotion";
                //    termTopicId.Value = topicId;
                //}

                var termsZoneId = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneId.ToString() };
                }

                //ko lay bai boom
                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                //range.GreaterThanOrEqualTo = DateTime.MinValue;
                range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && termStatus && termsZoneId && range
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && termStatus && termsZoneId && range
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> SearchNewsByType(string keyword, string author, int newsType, string zoneIds, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                searchRequest.Sort(s => s.Descending("created_date"));

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var termByStatus = new TermQuery();
                termByStatus.Field = "status";
                termByStatus.Value = 8;

                var termByType = new TermQuery();
                termByType.Field = "type";
                termByType.Value = newsType;

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && q.MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search(searchRequest);

                //var data = _db.SearchType<NewsSearchEntity>(keyword, newsType, zoneIds, from, to, fields, pageIndex, pageSize);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsSearchEntity> SearchNewsByTypeSubsite(string keyword, string author, int newsType, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                searchRequest.Sort(s => s.Descending("created_date"));

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new List<string>();
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',').ToList();
                    if (zoneTotalChecks.Count() > 0)
                    {
                        termsZoneIds?.AddRange(zoneTotalChecks.Select(c => c.ToString()));
                    }
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds?.Distinct();
                }

                var termByStatus = new TermQuery();
                termByStatus.Field = "status";
                termByStatus.Value = 8;

                var termByType = new TermQuery();
                termByType.Field = "type";
                termByType.Value = newsType;

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var fields = new string[] { "title", "title.folded" };

                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && q.MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .MultiMatch(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            //.DefaultOperator(Operator.And)
                            .Type(TextQueryType.Phrase)
                        )
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                        && termByType && termByStatus && range && termsZoneId && termCreatedBy
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                var data = _db.Search(searchRequest);

                //var data = _db.SearchType<NewsSearchEntity>(keyword, newsType, zoneIds, from, to, fields, pageIndex, pageSize);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }


        public List<NewsSearchEntity> GetListMagazineByType(string keyword, int newsType, int magazineType, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();
                var termType = new TermQuery();
                if (newsType != -1)
                {
                    termType.Field = "type";
                    termType.Value = newsType;
                }
                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termMustNotStatus = new TermQuery();
                termMustNotStatus.Field = "status";
                termMustNotStatus.Value = 9;

                var fields = new string[] { "title", "title.folded" };
                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && q.Bool(b => b.MustNot(termMustNotStatus)) && termType && termStatus
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<string> SearchNewsByType2(string keyword, int newsType, string zoneIds, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "title", "title.folded" };
                var data = _db.SearchType<NewsSearchEntity>(keyword, newsType, zoneIds, from, to, fields, pageIndex, pageSize);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<CountStatus> CountNews(List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int type)
        {
            try
            {
                var listSearch = new List<CountStatus>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var querys = new List<QueryContainerDescriptor<NewsSearchEntity>>();
                if (listStatus != null)
                {
                    foreach (var status in listStatus)
                    {
                        var query = new QueryContainerDescriptor<NewsSearchEntity>();

                        var termsZoneIds = new string[] { };
                        if (statusZoneIds != null)
                        {
                            termsZoneIds = statusZoneIds[status].GetType().GetProperty("zoneIds").GetValue(statusZoneIds[status], null).Split(',');
                        }
                        var termsByZoneId = new TermsQuery();
                        termsByZoneId.Field = "zone_ids";
                        termsByZoneId.Terms = termsZoneIds;

                        var termByStatus = new TermQuery();
                        termByStatus.Field = "status";
                        termByStatus.Value = status;

                        var termByType = new TermQuery();
                        var termTypeNotIn = new TermQuery();
                        if (type > 0)
                        {
                            termByType.Field = "type";
                            termByType.Value = type;
                        }
                        else
                        {
                            termTypeNotIn.Field = "type";
                            termTypeNotIn.Value = 26;
                        }

                        var filterFieldForUsername = statusZoneIds[status].GetType().GetProperty("filterFieldForUsername").GetValue(statusZoneIds[status], null);
                        var username = statusZoneIds[status].GetType().GetProperty("username").GetValue(statusZoneIds[status], null);
                        var termUserName = new TermQuery();
                        if (!string.IsNullOrEmpty(username))
                        {
                            //if (username.Contains("@"))
                            //    username = username.Substring(0, username.IndexOf('@'));

                            switch ((int)filterFieldForUsername)
                            {
                                case 0:
                                    termUserName.Field = "created_by";
                                    termUserName.Value = username;
                                    break;
                                case 1:
                                    termUserName.Field = "last_modified_by";
                                    termUserName.Value = username;
                                    break;
                                case 2:
                                    termUserName.Field = "published_by";
                                    termUserName.Value = username;
                                    break;
                                case 3:
                                    termUserName.Field = "edited_by";
                                    termUserName.Value = username;
                                    break;
                                case 4:
                                    termUserName.Field = "last_receiver";
                                    termUserName.Value = username;
                                    break;
                                case 5:
                                    termUserName.Field = "approved_by";
                                    termUserName.Value = username;
                                    break;
                                case 6:
                                    termUserName.Field = "returned_by";
                                    termUserName.Value = username;
                                    break;
                                case 7:
                                    termUserName.Field = "sent_by";
                                    termUserName.Value = username;
                                    break;
                                case 8:
                                    termUserName.Field = "error_checked_by";
                                    termUserName.Value = username;
                                    break;
                                case 9:
                                    termUserName.Field = "sensitive_checked_by";
                                    termUserName.Value = username;
                                    break;
                            }
                        }

                        query.Bool(boo => boo.Must(mu => termByStatus && termByType && termUserName,
                                mu => mu.Bool(bl => bl.Should(sh => termsByZoneId))
                            )
                            .MustNot(termTypeNotIn)
                        );

                        querys.Add(query);
                    }
                }

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("count_status", t => t
                        .Field("status")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountStatus(searchRequest);

                //var data= _db.CountStatus<NewsSearchEntity>(username, listStatus, statusZoneIds, currentUserName);
                if (data != null)
                {
                    listSearch = data.Select(s => new CountStatus { Status = s.Key, Count = s.DocCount }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public List<string> SearchNews2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var fields = new string[] { "title", "title.folded" };
                var data = _db.SearchPaging<NewsSearchEntity>(keyword, status, fields, start, rows);
                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> SearchNewsPublishForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow, bool showBomb = false, bool isSearchQuery = false)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termsZoneId = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneId.ToString() };
                }

                var fields = new string[] { "title", "title.folded" };

                searchRequest.Sort(s => s.Descending("distribution_date"));

                //ko lay bai boom
                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                //range.GreaterThanOrEqualTo = DateTime.MinValue;
                if (!showBomb)
                    range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                if (isSearchQuery)
                {
                    searchRequest.Query(q => q
                       //search có phân giã từ
                       .QueryString(mp => mp
                           .Query(keyword)
                           .Fields(f => f.Fields(fields))
                           .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                           .DefaultOperator(Operator.And)
                           ) && termStatus && termsZoneId && range
                       ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => q
                    //search theo dúng cụm từ
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .Type(TextQueryType.Phrase)
                        ) && termStatus && termsZoneId && range
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<string> SearchNewsPublishForPosition(int zoneId, string keyword, int displayPosition, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<string>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termsZoneId = new TermsQuery();
                if (zoneId > 0)
                {
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = new string[] { zoneId.ToString() };
                }

                var fields = new string[] { "title", "title.folded" };

                searchRequest.Sort(s => s.Descending("distribution_date"));

                keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        //.DefaultOperator(Operator.And)
                        .Type(TextQueryType.Phrase)
                    //.Slop(3)
                    ) && termStatus && termsZoneId
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.Select(s => s.Id.ToString()).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsSearchEntity> GetListNewsByListId(List<string> ids)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var termsListId = new TermsQuery();
                termsListId.Field = "_id";
                termsListId.Terms = ids;

                searchRequest.Sort(s => s.Descending("distribution_date"));

                searchRequest.Query(q => termStatus && termsListId
                ).From(0).Size(5000);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        public bool InitAllNews(List<NewsSearchEntity> listNews)
        {
            bool returnValue = false;
            try
            {
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<NewsSearchEntity>("title","source");
                        _db.CreateMany(listNews);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
                //return true;
            }
            catch { return returnValue; }
        }
        public bool InitAllNewsForAdPage(List<NewsSearchForAdStoreEntity> listNews)
        {
            bool returnValue = false;
            try
            {
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<NewsSearchForAdStoreEntity>("title");
                        _db.CreateMany(listNews);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
                //return true;
            }
            catch { return returnValue; }
        }

        public List<NewsSearchEntity> GetHotNews(int top, int day)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("distribution_date"));

                var inDay = -day;
                var fromDate = DateTime.Now.AddDays(inDay);
                var todate = DateTime.Now;

                var termStatus = new TermQuery();
                termStatus.Field = "status";
                termStatus.Value = 8;

                var range = new DateRangeQuery();
                range.Field = "distribution_date";
                if (fromDate != DateTime.MinValue && todate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    //range.LessThanOrEqualTo = todate;
                    range.LessThanOrEqualTo = DateTime.ParseExact(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                }

                searchRequest.Query(q => range && termStatus).From(0).Size(top);

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    listSearch = data.Data.ToList();
                }

                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        #region thongke
        public List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction(string zoneId, DateTime from, DateTime to)
        {
            try
            {
                var listSearch = new List<NewsStatisticTotalProduction>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                var querys = new List<QueryContainerDescriptor<NewsSearchEntity>>();

                var query1 = new QueryContainerDescriptor<NewsSearchEntity>();
                var query2 = new QueryContainerDescriptor<NewsSearchEntity>();

                var termsByZoneId = new TermsQuery();
                if (zoneId != "0")
                {
                    termsByZoneId.Field = "zone_ids";
                    termsByZoneId.Terms = new string[] { zoneId };
                }

                var termByStatus = new TermQuery();
                termByStatus.Field = "status";
                termByStatus.Value = 8;

                var termByNotStatus = new TermQuery();
                termByNotStatus.Field = "status";
                termByNotStatus.Value = 8;


                var range1 = new DateRangeQuery();
                range1.Field = "distribution_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range1.GreaterThanOrEqualTo = from;
                    range1.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range1.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range1.LessThanOrEqualTo = to;
                    }
                }

                query1.Bool(boo => boo.Must(mu => termByStatus && range1,
                        mu => mu.Bool(bl => bl.Should(sh => termsByZoneId))
                    )
                );

                var range2 = new DateRangeQuery();
                range2.Field = "distribution_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range2.GreaterThanOrEqualTo = from;
                    range2.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range2.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range2.LessThanOrEqualTo = to;
                    }
                }

                query2.Bool(boo => boo.Must(mu => range2,
                        mu => mu.Bool(bl => bl.Should(sh => termsByZoneId))
                    )
                    .MustNot(termByNotStatus)
                );

                querys.Add(query1);
                querys.Add(query2);

                searchRequest.Size(0).Query(q => q
                    .Bool(b => b
                        .Must(m => m
                            .Bool(bo => bo
                                .Should(querys.ToArray())
                            )
                        )
                    )
                )
                .Aggregations(a => a
                    .Terms("published_date_status", t => t
                        .Field("distribution_date")
                        .Size(int.MaxValue)
                    )
                    .Terms("created_date_status", t => t
                        .Field("created_date")
                        .Size(int.MaxValue)
                    )
                );

                var data = _db.CountDateStatus(searchRequest);

                if (data != null)
                {
                    listSearch = data.Select(s => new NewsStatisticTotalProduction { ValuesDate = s.Key, ValuesDataCreated = s.DocCount1, ValuesDataPublish = s.DocCount2 }).ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region check tin
        public List<NewsSearchEntity> CheckNews(string keyword, string author, string createdBy, string zoneIds, string listStatus, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<NewsSearchEntity>();
                var searchRequest = new SearchDescriptor<NewsSearchEntity>();

                searchRequest.Sort(s => s.Descending("last_modified_date"));

                var termCreatedBy = new TermQuery();
                if (!string.IsNullOrEmpty(createdBy))
                {
                    if (createdBy.Contains("@"))
                        createdBy = createdBy.Substring(0, createdBy.IndexOf('@'));

                    termCreatedBy.Field = "created_by";
                    termCreatedBy.Value = createdBy;
                }

                var termsZoneId = new TermsQuery();
                var termsZoneIds = new string[] { };
                if (zoneIds != "" && zoneIds != "-1" && zoneIds != "0")
                {
                    termsZoneIds = zoneIds.Split(',');
                    termsZoneId.Field = "zone_ids";
                    termsZoneId.Terms = termsZoneIds;
                }

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else
                {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                var termsStatusNotIn = new TermsQuery();
                var termsStatus = new string[] { };
                if (!string.IsNullOrEmpty(listStatus))
                {
                    termsStatus = listStatus.Split(',');
                    termsStatusNotIn.Field = "status";
                    termsStatusNotIn.Terms = termsStatus;
                }

                var fields = new string[] { "title", "title.folded" };
                var newsId = 0L;
                long.TryParse(keyword, out newsId);

                if (newsId > 0)
                {
                    var termNewsId = new TermQuery();
                    termNewsId.Field = "id";
                    termNewsId.Value = newsId;

                    searchRequest.Query(q => q
                        && termNewsId
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && q.QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termsZoneId && termCreatedBy && q.Bool(b => b.MustNot(termsStatusNotIn))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termsZoneId && termCreatedBy && q.Bool(b => b.MustNot(termsStatusNotIn))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else if (!string.IsNullOrEmpty(author))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    author = EscapeQueryBase.EscapeSearchQuery(author);
                    searchRequest.Query(q => q
                        .QueryString(mp => mp
                            .Query(author)
                            .Fields(f => f.Fields(new string[] { "author", "author.folded" }))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                        && range && termsZoneId && termCreatedBy && q.Bool(b => b.MustNot(termsStatusNotIn))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchRequest.Query(q => range && termsZoneId && termCreatedBy && q.Bool(b => b.MustNot(termsStatusNotIn))
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchRequest);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainSearchDb _db;
        private object termByUserName;

        protected NewsDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
