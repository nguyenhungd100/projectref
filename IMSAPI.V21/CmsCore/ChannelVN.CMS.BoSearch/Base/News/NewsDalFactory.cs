﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.MessageQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.News
{
    public class NewsDalFactory
    {
        public static bool AddNews(NewsEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.AddNews(news);
            }            
            return returnValue;
        }
        public static bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateDisplayPosition(newsId, displayPosition, accountName);
            }
            return returnValue;
        }
        public static bool UpdatePriority(long newsId, int priority, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdatePriority(newsId, priority, accountName);
            }
            return returnValue;
        }
        public static bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateAdStoreMode(newsId, adStore, adStoreUrl);
            }
            return returnValue;
        }
        public static bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateOnlyNewsTitle(newsId, newsTitle, accountName);
            }
            return returnValue;
        }
        public static bool UpdateOnlyNewsAvatar(long newsId, string newsAvatar, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateOnlyNewsAvatar(newsId, newsAvatar, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsByErrorCheck(long newsId, string newsTitle, string sapo, string avatar, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNewsByErrorCheck(newsId, newsTitle, sapo, avatar, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsIsOnHome(long newsId, bool isOnHome, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNewsIsOnHome(newsId, isOnHome, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsIsFocus(long newsId, bool isFocus, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNewsIsFocus(newsId, isFocus, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsSeoReviewStatus(long newsId, int reviewStatus, string accountName)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNewsSeoReviewStatus(newsId, reviewStatus, accountName);
            }
            return returnValue;
        }
        #region Change status
        public static bool DeleteNews(long newsId)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.DeleteNews(newsId);
            }
            return returnValue;
        }
        public static bool ChangeStatusToPublished(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToPublished(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToUnPublished(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToUnPublished(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToMovedToTrash(newsId, username);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForPublish(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForPublish(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForEditorialBoard(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForEditorialBoard(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForEdit(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForEdit(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReceivedForEdit(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReceivedForEdit(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRecievedForPublish(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRecievedForPublish(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReceivedForEditorialBoard(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReceivedForEditorialBoard(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToReporter(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToReporter(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool UpdateNewsNoteByExpert(long newsId, string note)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNewsNoteByExpert(newsId, note);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToEditorialSecretary(long newsId,string approvedBy, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToEditorialSecretary(newsId, approvedBy, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToEditor(long newsId, string receiver, string username, string note, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToEditor(newsId, receiver, username, note, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusForwardToOtherEditor(long newsId, string receiver)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusForwardToOtherEditor(newsId, receiver);
            }
            return returnValue;
        }
        public static bool ChangeStatusForwardToOtherSecretary(long newsId, string receiver)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusForwardToOtherSecretary(newsId, receiver);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRecieveFromReturnStore(long newsId, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRecieveFromReturnStore(newsId, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRestoreFromTrash(long newsId, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRestoreFromTrash(newsId, status);
            }
            return returnValue;
        }

        public static bool ChangeStatusToCrawlerForEdit(long newsId, string username, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToCrawlerForEdit(newsId, username, status);
            }
            return returnValue;
        }

        public static bool ChangeStatusToReturnToCooperator(long newsId, string username, string receiver, int status)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnToCooperator(newsId, username, receiver, status);
            }
            return returnValue;
        }
        #endregion
        public static bool UpdateNews(NewsEntity news)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.UpdateNews(news);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> ListNewsByStatus(string username, int typeOfCheck, string zoneIds, int filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsByStatus(username, typeOfCheck, zoneIds, filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsByMultiStatus(string keyword, string author, DateTime fromDate, DateTime toDate, List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsByMultiStatus(keyword, author, fromDate, toDate, listStatus, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsByMultiStatusSubsite(string keyword, string author, DateTime fromDate, DateTime toDate, List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsByMultiStatus(keyword, author, fromDate, toDate, listStatus, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsProcessing(string keyword, string author, DateTime fromDate, DateTime toDate, List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int typeOfCheck, int type, int sortOrder, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsProcessing(keyword, author, fromDate, toDate, listStatus, statusZoneIds, typeOfCheck, type, sortOrder, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsIsPr(string keyword, string username, string zoneIds, int newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsIsPr(keyword, username, zoneIds, newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsIsPrSubsite(string keyword, string username, string zoneIds, List<int> zoneTotalChecks, int newsStatus, List<int> filterFieldForUsername, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsIsPrSubsite(keyword, username, zoneIds, zoneTotalChecks, newsStatus, filterFieldForUsername, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> ListNewsPublishByDate(DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsPublishByDate(fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> ListNewsPublish(string accountName, DateTime fromDate, DateTime toDate, string zoneIds, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsPublish(accountName, fromDate, toDate, zoneIds, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> SearchNewsForSeo(string keyword, string username, string zoneIds, DateTime fromDate, DateTime toDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsForSeo(keyword, username, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchForAdStoreEntity> SearchNewsForAdPage(string keyword, int zoneId, DateTime fromDistributionDate, DateTime toDistributionDate, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchForAdStoreEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsForAdPage(keyword, zoneId, fromDistributionDate, toDistributionDate, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<long> ListNewsPublishByUserName(string username, string zoneIds, int top)
        {
            List<long> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListNewsPublishByUserName(username, zoneIds, top);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> ListMyNews(string username, string zoneIds, List<int> filterFieldForUsername, int sortOrder, int status, int type, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.ListMyNews(username, zoneIds, filterFieldForUsername, sortOrder, status, type, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> SearchNews(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy="")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNews(keyword, typeOfCheck, author, username, zoneIds, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsByParent(string keyword, int typeOfCheck, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsByZoneParent(keyword, typeOfCheck, author, username, zoneIds, zoneTotalChecks, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsSeoReviewStatus(string keyword, int reviewStatus, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsSeoReviewStatus(keyword, reviewStatus, author, username, zoneIds, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsSeoReviewStatusSubsite(string keyword, int reviewStatus, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsSeoReviewStatusSubsite(keyword, reviewStatus, author, username, zoneIds, zoneTotalChecks, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsPublishES(string keyword, int typeOfCheck, string author, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsPublishES(keyword, typeOfCheck, author, username, zoneIds, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsPublishSubsiteES(string keyword, int typeOfCheck, string author, string username, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, int pageIndex, int pageSize, ref int totalRow, string createdBy = "")
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsPublishSubsiteES(keyword, typeOfCheck, author, username, zoneIds, zoneTotalChecks, from, to, filterFieldForUsername, sortOrder, status, newsType, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsForNewsPositionES(string keyword, int zoneId, int displayPosition, int priority, bool showBomb, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsForNewsPositionES(keyword, zoneId, displayPosition, priority, showBomb, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsForNewsPositionESInDateNow(string keyword, int zoneId, int displayPosition, int priority, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsForNewsPositionESInDateNow(keyword, zoneId, displayPosition, priority, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsPublishExcludeNewsInTopic(int zoneId, string keyword, long topicId, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsPublishExcludeNewsInTopic(zoneId, keyword, topicId, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsByType(string keyword, string author, int newsType, string zoneIds, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsByType(keyword, author, newsType, zoneIds, from, to, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> SearchNewsByTypeSubsite(string keyword, string author, int newsType, string zoneIds, List<int> zoneTotalChecks, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow, string createdBy)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsByTypeSubsite(keyword, author, newsType, zoneIds, zoneTotalChecks, from, to, pageIndex, pageSize, ref totalRow, createdBy);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> GetListMagazineByType(string keyword, int newsType, int magazineType, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.GetListMagazineByType(keyword, newsType, magazineType, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<string> SearchNewsByType2(string keyword, int newsType, string zoneIds, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsByType2(keyword, newsType, zoneIds, from, to, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static List<CountStatus> CountNews(List<string> listStatus, Dictionary<string, dynamic> statusZoneIds, int type)
        {
            List<CountStatus> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.CountNews(listStatus, statusZoneIds, type);
            }
            return returnValue;
        }

        public static List<string> SearchNews2(string keyword, int status, int start, int rows, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNews2(keyword, status, start, rows, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> SearchNewsPublishForNewsRelation(int zoneId, string keyword, int pageIndex, int pageSize, ref int totalRow, bool showBomb, bool isSearchQuery)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsPublishForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRow, showBomb, isSearchQuery);
            }
            return returnValue;
        }
        public static List<string> SearchNewsPublishForPosition(int zoneId, string keyword, int displayPosition, int pageIndex, int pageSize, ref int totalRow)
        {
            List<string> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.SearchNewsPublishForPosition(zoneId, keyword, displayPosition, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        public static List<NewsSearchEntity> GetListNewsByListId(List<string> ids)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.GetListNewsByListId(ids);
            }
            return returnValue;
        }

        public static bool InitAllNews(List<NewsSearchEntity> listNews)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.InitAllNews(listNews);
            }
            return returnValue;
        }

        public static bool InitAllNewsForAdPage(List<NewsSearchForAdStoreEntity> listNews)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.InitAllNewsForAdPage(listNews);
            }
            return returnValue;
        }

        public static List<NewsSearchEntity> GetHotNews(int top, int day)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.GetHotNews(top, day);
            }
            return returnValue;
        }

        #region thongke
        public static List<NewsStatisticTotalProduction> GetStatisticNewsTotalProduction(string zoneId, DateTime from, DateTime to)
        {
            List<NewsStatisticTotalProduction> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.GetStatisticNewsTotalProduction(zoneId, from, to);
            }
            return returnValue;
        }
        #endregion

        #region check tin
        public static List<NewsSearchEntity> CheckNews(string keyword, string author, string createdBy, string zoneIds, string listStatus, DateTime from, DateTime to, int pageIndex, int pageSize, ref int totalRow)
        {
            List<NewsSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.NewsDal.CheckNews(keyword, author, createdBy, zoneIds, listStatus, from, to, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }
        #endregion
    }
}
