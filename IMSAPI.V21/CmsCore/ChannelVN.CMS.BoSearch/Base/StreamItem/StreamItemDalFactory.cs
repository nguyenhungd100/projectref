﻿using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.StreamItem;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoSearch.Base.StreamItem
{
    public class StreamItemDalFactory
    {
        #region StreamItem

        public static bool AddStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.AddStreamItem(streamItem);
            }
            return returnValue;
        }        

        public static bool UpdateStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.UpdateStreamItem(streamItem);
            }
            return returnValue;
        }
        
        public static bool DeleteStreamItemById(long id)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.DeleteStreamItemById(id);
            }
            return returnValue;
        }

        public static List<StreamItemSearchEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<StreamItemSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        public static long CountStreamItem()
        {
            long returnValue = 0;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.CountStreamItem();
            }
            return returnValue;
        }

        public static bool InitAllStreamItem(List<StreamItemSearchEntity> list)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.InitAllStreamItem(list);
            }
            return returnValue;
        }

        #endregion

        #region StreamItemMobile

        public static bool AddStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.AddStreamItemMobile(streamItem);
            }
            return returnValue;
        }

        public static bool UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.UpdateStreamItemMobile(streamItem);
            }
            return returnValue;
        }

        public static bool DeleteStreamItemMobileById(long id)
        {
            bool returnValue = false;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.DeleteStreamItemMobileById(id);
            }
            return returnValue;
        }

        public static List<StreamItemMobileSearchEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            List<StreamItemMobileSearchEntity> returnValue = null;
            using (var db = new CmsMainSearchDb())
            {
                returnValue = db.StreamItemDal.SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
            }
            return returnValue;
        }

        #endregion
    }
}
