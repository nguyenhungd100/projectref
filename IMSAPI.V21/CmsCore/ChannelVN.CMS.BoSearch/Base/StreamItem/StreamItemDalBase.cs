﻿using ChannelVN.CMS.BoSearch.Common;
using ChannelVN.CMS.BoSearch.Databases;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.StreamItem;
using Nest;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoSearch.Base.StreamItem
{
    public abstract class StreamItemDalBase
    {
        #region StreamItem

        public bool AddStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            try
            {                
                if (streamItem != null)
                {                    
                    var oStreamItem = new StreamItemSearchEntity
                    {
                        Id = streamItem.Id,
                        Order=streamItem.Order,
                        TypeId=streamItem.TypeId,
                        TemplateId=streamItem.TemplateId,
                        Status = streamItem.Status,
                        CreatedDate =streamItem.CreatedDate,
                        CreatedBy = streamItem.CreatedBy,
                        PublishedDate =streamItem.PublishedDate,
                        PublishedBy=streamItem.PublishedBy,
                        DataJson=streamItem.DataJson,
                        Mode=streamItem.Mode,
                        Title=streamItem.Title
                    };
                   
                    returnValue = _db.CreateIndexSettings<StreamItemSearchEntity>("title");
                    returnValue = _db.Create(oStreamItem);

                    //ext
                    AddStreamItemDetail(streamItem);
                }

                return returnValue;                
            }
            catch { return returnValue; }
        }        

        public bool UpdateStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            try
            {                
                if (streamItem != null)
                {
                    var oStreamItem = _db.Get<StreamItemSearchEntity>(streamItem.Id);                                       
                    if (oStreamItem == null)
                    {
                        oStreamItem = new StreamItemSearchEntity
                        {
                            Id = streamItem.Id,
                            Order = streamItem.Order,
                            TypeId = streamItem.TypeId,
                            TemplateId = streamItem.TemplateId,
                            Status = streamItem.Status,
                            CreatedDate = streamItem.CreatedDate,
                            CreatedBy = streamItem.CreatedBy,
                            PublishedDate = streamItem.PublishedDate,
                            PublishedBy = streamItem.PublishedBy,
                            DataJson = streamItem.DataJson,
                            Mode = streamItem.Mode,
                            Title = streamItem.Title
                        };
                    }
                    else
                    {
                        oStreamItem.Id = streamItem.Id;
                        oStreamItem.Order = streamItem.Order;
                        oStreamItem.TypeId = streamItem.TypeId;
                        oStreamItem.TemplateId = streamItem.TemplateId;
                        oStreamItem.Status = streamItem.Status;
                        //oStreamItem.CreatedDate = streamItem.CreatedDate;
                        //oStreamItem.CreatedBy = streamItem.CreatedBy;
                        //oStreamItem.PublishedDate = streamItem.PublishedDate;
                        //oStreamItem.PublishedBy = streamItem.PublishedBy;
                        oStreamItem.DataJson = streamItem.DataJson;
                        oStreamItem.Mode = streamItem.Mode;
                        oStreamItem.Title = streamItem.Title;
                    }           

                    returnValue = _db.Update<StreamItemSearchEntity>(oStreamItem.Id, oStreamItem);

                    //ext
                    UpdateStreamItemDetail(streamItem);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }       

        public bool DeleteStreamItemById(long id)
        {
            try
            {
                var oStreamItem = _db.Get<StreamItemSearchEntity>(id);
                if (oStreamItem == null)
                {
                    return false;
                }
                var returnValue = _db.DeleteMapping(oStreamItem);

                //ext
                //delete all ItemId
                var request = new QueryContainer
                (
                    new TermQuery
                    {
                        Field = "item_id",
                        Value = id
                    }
                );
                _db.DeleteByQuery<StreamItemDetailSearchEntity>(request);

                return returnValue;
            }
            catch { return false; }
        }

        public List<StreamItemSearchEntity> SearchStreamItem(string keyword, int typeId, int mode, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<StreamItemSearchEntity>();

                var searchDescriptor = new SearchDescriptor<StreamItemSearchEntity>();

                var fields = new string[] { "title", "title.folded" };
                
                switch (orderBy)
                {
                    case 0:
                        searchDescriptor.Sort(s => s.Descending("id"));
                        break;
                    case 1:
                        searchDescriptor.Sort(s => s.Descending("order"));
                        break;
                    case 2:
                        searchDescriptor.Sort(s => s.Ascending("order"));
                        break;                                     
                    default:
                        searchDescriptor.Sort(s => s.Descending("created_date"));                        
                        break;
                }

                var termTypeId = new TermQuery();
                if (typeId > 0)
                {
                    termTypeId.Field = "type_id";
                    termTypeId.Value = typeId;                    
                }

                var termMode = new TermQuery();
                if (mode > -1)
                {
                    termMode.Field = "mode";
                    termMode.Value = mode;
                }

                var termStatus = new TermQuery();
                if (status > -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termTemplateId = new TermQuery();
                if (!string.IsNullOrEmpty(templateId))
                {
                    termTemplateId.Field = "template_id";
                    termTemplateId.Value = templateId;
                }

                if (!string.IsNullOrEmpty(keyword))
                {
                    keyword = EscapeQueryBase.EscapeSearchQuery(keyword);
                    searchDescriptor.Query(q => q
                        .QueryString(mp => mp
                            .Query(keyword)
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && termTypeId && termMode && termTemplateId && termStatus
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }
                else
                {
                    searchDescriptor.Query(q => termTypeId && termMode && termTemplateId && termStatus
                    ).From((pageIndex - 1) * pageSize).Size(pageSize);
                }

                var data = _db.Search(searchDescriptor);                

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        public long CountStreamItem()
        {
            try
            {                                                
                return _db.Count<StreamItemSearchEntity>();
            }
            catch
            {
                return 0;
            }
        }

        public bool InitAllStreamItem(List<StreamItemSearchEntity> list)
        {
            bool returnValue = false;
            try
            {
                var fieldNameAnalyzer = "template_id";
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.CreateIndexSettings<StreamItemSearchEntity>(fieldNameAnalyzer);
                        _db.CreateMany2(list);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch { return returnValue; }
        }

        #endregion

        #region StreamItemMobile

        public bool AddStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null)
                {
                    var oStreamItem = new StreamItemMobileSearchEntity
                    {
                        Id = streamItem.Id,
                        Order = streamItem.Order,
                        TypeId = streamItem.TypeId,
                        TemplateId = streamItem.TemplateId,
                        Status = streamItem.Status,
                        CreatedDate = streamItem.CreatedDate,
                        CreatedBy = streamItem.CreatedBy,
                        PublishedDate = streamItem.PublishedDate,
                        PublishedBy = streamItem.PublishedBy,
                        DataJson = streamItem.DataJson
                    };

                    var fieldNameAnalyzer = "template_id";
                    returnValue = _db.CreateIndexSettings<StreamItemMobileSearchEntity>(fieldNameAnalyzer);
                    returnValue = _db.Create(oStreamItem);

                    //ext
                    AddStreamItemMobileDetail(streamItem);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null)
                {
                    var oStreamItem = _db.Get<StreamItemMobileSearchEntity>(streamItem.Id);
                    if (oStreamItem == null)
                    {
                        oStreamItem = new StreamItemMobileSearchEntity
                        {
                            Id = streamItem.Id,
                            Order = streamItem.Order,
                            TypeId = streamItem.TypeId,
                            TemplateId = streamItem.TemplateId,
                            Status = streamItem.Status,
                            CreatedDate = streamItem.CreatedDate,
                            CreatedBy = streamItem.CreatedBy,
                            PublishedDate = streamItem.PublishedDate,
                            PublishedBy = streamItem.PublishedBy,
                            DataJson = streamItem.DataJson
                        };
                    }
                    else
                    {
                        oStreamItem.Id = streamItem.Id;
                        oStreamItem.Order = streamItem.Order;
                        oStreamItem.TypeId = streamItem.TypeId;
                        oStreamItem.TemplateId = streamItem.TemplateId;
                        oStreamItem.Status = streamItem.Status;
                        //oStreamItem.CreatedDate = streamItem.CreatedDate;
                        //oStreamItem.CreatedBy = streamItem.CreatedBy;
                        //oStreamItem.PublishedDate = streamItem.PublishedDate;
                        //oStreamItem.PublishedBy = streamItem.PublishedBy;
                        oStreamItem.DataJson = streamItem.DataJson;
                    }

                    returnValue = _db.Update<StreamItemMobileSearchEntity>(oStreamItem.Id, oStreamItem);

                    //ext
                    UpdateStreamItemMobileDetail(streamItem);
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool DeleteStreamItemMobileById(long id)
        {
            try
            {
                var oStreamItem = _db.Get<StreamItemMobileSearchEntity>(id);
                if (oStreamItem == null)
                {
                    return false;
                }
                var returnValue = _db.DeleteMapping(oStreamItem);

                //ext
                //delete all ItemId
                var request = new QueryContainer
                (
                    new TermQuery
                    {
                        Field = "item_id",
                        Value = id
                    }
                );
                _db.DeleteByQuery<StreamItemMobileDetailSearchEntity>(request);

                return returnValue;
            }
            catch { return false; }
        }

        public List<StreamItemMobileSearchEntity> SearchStreamItemMobile(int typeId, string templateId, int status, int orderBy, int pageIndex, int pageSize, ref int totalRow)
        {
            try
            {
                var listSearch = new List<StreamItemMobileSearchEntity>();

                var searchDescriptor = new SearchDescriptor<StreamItemMobileSearchEntity>();

                var fields = new string[] { "tempalate_id", "tempalate_id.folded" };

                switch (orderBy)
                {
                    case 0:
                        searchDescriptor.Sort(s => s.Descending("id"));
                        break;
                    case 1:
                        searchDescriptor.Sort(s => s.Descending("order"));
                        break;
                    case 2:
                        searchDescriptor.Sort(s => s.Ascending("order"));
                        break;
                    default:
                        searchDescriptor.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termTypeId = new TermQuery();
                if (typeId > 0)
                {
                    termTypeId.Field = "type_id";
                    termTypeId.Value = typeId;
                }

                var termStatus = new TermQuery();
                if (status > -1)
                {
                    termStatus.Field = "status";
                    termStatus.Value = status;
                }

                var termTemplateId = new TermQuery();
                if (!string.IsNullOrEmpty(templateId))
                {
                    termTemplateId.Field = "template_id";
                    termTemplateId.Value = templateId;
                }

                //searchDescriptor.Query(q =>q
                //    .QueryString(mp => mp
                //        .Query(templateId)
                //        .Fields(f => f.Fields(fields))
                //        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                //        .DefaultOperator(Operator.And)
                //    ) && termTypeId && termStatus
                //).From((pageIndex - 1) * pageSize).Size(pageSize);

                searchDescriptor.Query(q => termTemplateId && termTypeId && termStatus
                ).From((pageIndex - 1) * pageSize).Size(pageSize);

                var data = _db.Search(searchDescriptor);

                if (data != null)
                {
                    totalRow = (int)data.Total;
                    listSearch = data.Data.ToList();
                }
                return listSearch;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Ext StreamItem
        public bool AddStreamItemDetail(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null)
                {
                    if (!string.IsNullOrEmpty(streamItem.DataJson))
                    {
                        //array obj
                        var data = new List<StreamItemDetailSearchEntity>();
                        if (streamItem.DataJson.StartsWith("["))
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StreamItemDetailSearchEntity>>(streamItem.DataJson);

                        if(data!=null && data.Count > 0)
                        {
                            var listItemDetail = new List<StreamItemDetailSearchEntity>();
                            foreach(var item in data)
                            {
                                var oStreamItemDetail = new StreamItemDetailSearchEntity
                                {
                                    ItemId = streamItem.Id,                                    
                                    ItemTypeId = streamItem.TypeId,
                                    ItemTemplateId = streamItem.TemplateId,

                                    ObjectId = item.ObjectId,
                                    ObjectType = item.ObjectType,
                                    Title = item.Title,
                                    Sapo = item.Sapo,
                                    Avatar = item.Avatar,
                                    Author = item.Author,
                                    Type = item.Type,
                                    Url = item.Url,
                                    DistributionDate = item.DistributionDate,
                                };
                                listItemDetail.Add(oStreamItemDetail);
                            }
                            var fieldNameAnalyzer = "title";
                            var tasks = new List<Task>
                            {
                                Task.Run( () => {
                                    returnValue = _db.CreateIndexSettings<StreamItemDetailSearchEntity>(fieldNameAnalyzer);
                                    returnValue = _db.CreateMany2(listItemDetail);
                                })
                            };

                            var result = Task.WaitAny(tasks.ToArray());
                            return returnValue = result > 0;
                        }
                        else
                        {
                            //1 obj
                            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<StreamItemDetailSearchEntity>(streamItem.DataJson);
                            if (obj != null)
                            {
                                var oStreamItemDetail = new StreamItemDetailSearchEntity
                                {
                                    ItemId = streamItem.Id,
                                    ItemTypeId = streamItem.TypeId,
                                    ItemTemplateId = streamItem.TemplateId,

                                    ObjectId = obj.ObjectId,
                                    ObjectType = obj.ObjectType,
                                    Title = obj.Title,
                                    Sapo = obj.Sapo,
                                    Avatar = obj.Avatar,
                                    Author = obj.Author,
                                    Type = obj.Type,
                                    Url = obj.Url,
                                    DistributionDate = obj.DistributionDate,
                                };

                                var fieldNameAnalyzer = "title";
                                var tasks = new List<Task>
                                {
                                    Task.Run( () => {
                                        returnValue = _db.CreateIndexSettings<StreamItemDetailSearchEntity>(fieldNameAnalyzer);
                                        returnValue = _db.Create(oStreamItemDetail);
                                    })
                                };

                                var result = Task.WaitAny(tasks.ToArray());
                                return returnValue = result > 0;
                            }
                        }
                    }                                      
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateStreamItemDetail(StreamItemEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null && !string.IsNullOrEmpty(streamItem.DataJson))
                {
                    //array obj
                    var data = new List<StreamItemDetailSearchEntity>();
                    if (streamItem.DataJson.StartsWith("["))
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StreamItemDetailSearchEntity>>(streamItem.DataJson);

                    if (data != null && data.Count > 0)
                    {
                        //delete all ItemId
                        var request = new QueryContainer
                                (
                                    new TermQuery
                                    {
                                        Field = "item_id",
                                        Value = streamItem.Id
                                    }
                                );
                        var del = _db.DeleteByQuery<StreamItemDetailSearchEntity>(request);

                        var listItemDetail = new List<StreamItemDetailSearchEntity>();
                        foreach (var item in data)
                        {
                            var oStreamItemDetail = new StreamItemDetailSearchEntity
                            {
                                ItemId = streamItem.Id,
                                ItemTypeId = streamItem.TypeId,
                                ItemTemplateId = streamItem.TemplateId,

                                ObjectId = item.ObjectId,
                                ObjectType = item.ObjectType,
                                Title = item.Title,
                                Sapo = item.Sapo,
                                Avatar = item.Avatar,
                                Author = item.Author,
                                Type = item.Type,
                                Url = item.Url,
                                DistributionDate = item.DistributionDate,
                            };
                            listItemDetail.Add(oStreamItemDetail);
                        }
                        var fieldNameAnalyzer = "title";
                        var tasks = new List<Task>
                            {
                                Task.Run( () => {
                                    returnValue = _db.CreateIndexSettings<StreamItemDetailSearchEntity>(fieldNameAnalyzer);
                                    returnValue = _db.CreateMany2(listItemDetail);
                                })
                            };

                        var result = Task.WaitAny(tasks.ToArray());
                        return returnValue = result > 0;
                    }
                    else
                    {
                        //1 obj
                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<StreamItemDetailSearchEntity>(streamItem.DataJson);
                        if (obj != null)
                        {
                            //delete all ItemId
                            var request = new QueryContainer
                                (
                                    new TermQuery
                                    {
                                        Field="item_id",
                                        Value=streamItem.Id
                                    }
                                );
                            var del = _db.DeleteByQuery<StreamItemDetailSearchEntity>(request);

                            var oStreamItemDetail = new StreamItemDetailSearchEntity
                            {
                                ItemId = streamItem.Id,
                                ItemTypeId = streamItem.TypeId,
                                ItemTemplateId = streamItem.TemplateId,

                                ObjectId = obj.ObjectId,
                                ObjectType = obj.ObjectType,
                                Title = obj.Title,
                                Sapo = obj.Sapo,
                                Avatar = obj.Avatar,
                                Author = obj.Author,
                                Type = obj.Type,
                                Url = obj.Url,
                                DistributionDate = obj.DistributionDate,
                            };

                            var fieldNameAnalyzer = "title";
                            var tasks = new List<Task>
                                {
                                    Task.Run( () => {
                                        returnValue = _db.CreateIndexSettings<StreamItemDetailSearchEntity>(fieldNameAnalyzer);
                                        returnValue = _db.Create(oStreamItemDetail);
                                    })
                                };

                            var result = Task.WaitAny(tasks.ToArray());
                            return returnValue = result > 0;
                        }
                    }

                    return returnValue;
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        #endregion

        #region Ext StreamItemMobile
        public bool AddStreamItemMobileDetail(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null)
                {
                    if (!string.IsNullOrEmpty(streamItem.DataJson))
                    {
                        //array obj
                        var data = new List<StreamItemMobileDetailSearchEntity>();
                        if (streamItem.DataJson.StartsWith("["))
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StreamItemMobileDetailSearchEntity>>(streamItem.DataJson);

                        if (data != null && data.Count > 0)
                        {
                            var listItemDetail = new List<StreamItemMobileDetailSearchEntity>();
                            foreach (var item in data)
                            {
                                var oStreamItemDetail = new StreamItemMobileDetailSearchEntity
                                {
                                    ItemId = streamItem.Id,
                                    ItemTypeId = streamItem.TypeId,
                                    ItemTemplateId = streamItem.TemplateId,

                                    ObjectId = item.ObjectId,
                                    ObjectType = item.ObjectType,
                                    Title = item.Title,
                                    Sapo = item.Sapo,
                                    Avatar = item.Avatar,
                                    Author = item.Author,
                                    Type = item.Type,
                                    Url = item.Url,
                                    DistributionDate = item.DistributionDate,
                                };
                                listItemDetail.Add(oStreamItemDetail);
                            }
                            var fieldNameAnalyzer = "title";
                            var tasks = new List<Task>
                            {
                                Task.Run( () => {
                                    returnValue = _db.CreateIndexSettings<StreamItemMobileDetailSearchEntity>(fieldNameAnalyzer);
                                    returnValue = _db.CreateMany2(listItemDetail);
                                })
                            };

                            var result = Task.WaitAny(tasks.ToArray());
                            return returnValue = result > 0;
                        }
                        else
                        {
                            //1 obj
                            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<StreamItemMobileDetailSearchEntity>(streamItem.DataJson);
                            if (obj != null)
                            {
                                var oStreamItemDetail = new StreamItemMobileDetailSearchEntity
                                {
                                    ItemId = streamItem.Id,
                                    ItemTypeId = streamItem.TypeId,
                                    ItemTemplateId = streamItem.TemplateId,

                                    ObjectId = obj.ObjectId,
                                    ObjectType = obj.ObjectType,
                                    Title = obj.Title,
                                    Sapo = obj.Sapo,
                                    Avatar = obj.Avatar,
                                    Author = obj.Author,
                                    Type = obj.Type,
                                    Url = obj.Url,
                                    DistributionDate = obj.DistributionDate,
                                };

                                var fieldNameAnalyzer = "title";
                                var tasks = new List<Task>
                                {
                                    Task.Run( () => {
                                        returnValue = _db.CreateIndexSettings<StreamItemMobileDetailSearchEntity>(fieldNameAnalyzer);
                                        returnValue = _db.Create(oStreamItemDetail);
                                    })
                                };

                                var result = Task.WaitAny(tasks.ToArray());
                                return returnValue = result > 0;
                            }
                        }
                    }
                }

                return returnValue;
            }
            catch { return returnValue; }
        }

        public bool UpdateStreamItemMobileDetail(StreamItemMobileEntity streamItem)
        {
            bool returnValue = false;
            try
            {
                if (streamItem != null && !string.IsNullOrEmpty(streamItem.DataJson))
                {
                    //array obj
                    var data = new List<StreamItemMobileDetailSearchEntity>();
                    if (streamItem.DataJson.StartsWith("["))
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<List<StreamItemMobileDetailSearchEntity>>(streamItem.DataJson);

                    if (data != null && data.Count > 0)
                    {
                        //delete all ItemId
                        var request = new QueryContainer
                                (
                                    new TermQuery
                                    {
                                        Field = "item_id",
                                        Value = streamItem.Id
                                    }
                                );
                        var del = _db.DeleteByQuery<StreamItemMobileDetailSearchEntity>(request);

                        var listItemDetail = new List<StreamItemMobileDetailSearchEntity>();
                        foreach (var item in data)
                        {
                            var oStreamItemDetail = new StreamItemMobileDetailSearchEntity
                            {
                                ItemId = streamItem.Id,
                                ItemTypeId = streamItem.TypeId,
                                ItemTemplateId = streamItem.TemplateId,

                                ObjectId = item.ObjectId,
                                ObjectType = item.ObjectType,
                                Title = item.Title,
                                Sapo = item.Sapo,
                                Avatar = item.Avatar,
                                Author = item.Author,
                                Type = item.Type,
                                Url = item.Url,
                                DistributionDate = item.DistributionDate,
                            };
                            listItemDetail.Add(oStreamItemDetail);
                        }
                        var fieldNameAnalyzer = "title";
                        var tasks = new List<Task>
                            {
                                Task.Run( () => {
                                    returnValue = _db.CreateIndexSettings<StreamItemMobileDetailSearchEntity>(fieldNameAnalyzer);
                                    returnValue = _db.CreateMany2(listItemDetail);
                                })
                            };

                        var result = Task.WaitAny(tasks.ToArray());
                        return returnValue = result > 0;
                    }
                    else
                    {
                        //1 obj
                        var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<StreamItemMobileDetailSearchEntity>(streamItem.DataJson);
                        if (obj != null)
                        {
                            //delete all ItemId
                            var request = new QueryContainer
                                (
                                    new TermQuery
                                    {
                                        Field = "item_id",
                                        Value = streamItem.Id
                                    }
                                );
                            var del = _db.DeleteByQuery<StreamItemMobileDetailSearchEntity>(request);

                            var oStreamItemDetail = new StreamItemMobileDetailSearchEntity
                            {
                                ItemId = streamItem.Id,
                                ItemTypeId = streamItem.TypeId,
                                ItemTemplateId = streamItem.TemplateId,

                                ObjectId = obj.ObjectId,
                                ObjectType = obj.ObjectType,
                                Title = obj.Title,
                                Sapo = obj.Sapo,
                                Avatar = obj.Avatar,
                                Author = obj.Author,
                                Type = obj.Type,
                                Url = obj.Url,
                                DistributionDate = obj.DistributionDate,
                            };

                            var fieldNameAnalyzer = "title";
                            var tasks = new List<Task>
                                {
                                    Task.Run( () => {
                                        returnValue = _db.CreateIndexSettings<StreamItemMobileDetailSearchEntity>(fieldNameAnalyzer);
                                        returnValue = _db.Create(oStreamItemDetail);
                                    })
                                };

                            var result = Task.WaitAny(tasks.ToArray());
                            return returnValue = result > 0;
                        }
                    }

                    return returnValue;
                }

                return returnValue;
            }
            catch { return returnValue; }
        }
        #endregion

        #region Core members

        private readonly CmsMainSearchDb _db;

        protected StreamItemDalBase(CmsMainSearchDb db)
        {
            _db = db;
        }

        protected CmsMainSearchDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
