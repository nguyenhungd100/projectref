﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.BrandContent.Entity;

namespace ChannelVN.CMS.BoCached.CmsExtension.BrandContent
{
    public abstract class BrandContentDalBase
    {
        public bool AddBrandContent(BrandContentEntity newsContent)
        {
            try
            {
                if (null == newsContent)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<BrandContentEntity>(newsContent.NewsId.ToString(), newsContent);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddBrandContentByNewsId(long newsId, List<BrandContentEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.BrandContent = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BrandContentEntity> GetBrandContentByNewsId(long newsId)
        {
            var result = new List<BrandContentEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.BrandContent;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected BrandContentDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
