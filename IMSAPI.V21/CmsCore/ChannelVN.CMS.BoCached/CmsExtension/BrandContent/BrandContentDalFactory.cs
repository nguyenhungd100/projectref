﻿using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.CmsExtension.BrandContent
{
    public class BrandContentDalFactory
    {
        public static bool AddBrandContent(BrandContentEntity newsContent)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.BrandContentDal.AddBrandContent(newsContent);
            }
            return returnValue;
        }
        public static bool AddBrandContentByNewsId(long newsId, List<BrandContentEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.BrandContentDal.AddBrandContentByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<BrandContentEntity> GetBrandContentByNewsId(long newsId)
        {
            List<BrandContentEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.BrandContentDal.GetBrandContentByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
