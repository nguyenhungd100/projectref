﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.SocialNetwork.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.CmsExtension.SocialNetwork
{
    public class ActivityDalFactory
    {
        public static bool AddActivity(ActivityEntity activity)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ActivityDal.AddActivity(activity);
            }
            return returnValue;
        }

        public static List<ActivityEntity> GetActivityByListId(List<string> listId)
        {
            List<ActivityEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ActivityDal.GetActivityByListId(listId);
            }
            return returnValue;
        }
    }
}
