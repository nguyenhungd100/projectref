﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.SocialNetwork.Entity;

namespace ChannelVN.CMS.BoCached.CmsExtension.SocialNetwork
{
    public abstract class ActivityDalBase
    {
        public bool AddActivity(ActivityEntity activity)
        {
            try
            {
                if (null == activity)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<ActivityEntity>(activity.Id.ToString(), activity);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ActivityEntity> GetActivityByListId(List<string> listId)
        {
            try
            {                
                return _db.GetsFromHash<ActivityEntity>(listId);
            }
            catch
            {
                return null;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected ActivityDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
