﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using ChannelVN.CMS.BoCached.Entity.Init;
using ChannelVN.CMS.BoCached.Entity.Queue;
using ChannelVN.CMS.Entity.Base.News;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.BoCached.CmsExtension.Queue
{
    public abstract class QueueDalBase
    {
        #region Zone
        public long AddQueueZone(string keyId, string action, ZoneEntity zone)
        {
            try
            {
                if (null == zone || string.IsNullOrEmpty(keyId))
                {
                    return 0;
                }
                var queue = new QueueZoneEntity
                {
                    Action = action,
                    Message = zone,
                    Datetime = DateTime.Now
                };

                return _db.PushQueue(keyId, queue);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueueZoneEntity PopQueueZone(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueueZoneEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region News
        public long AddQueue(string keyId, string action, NewsEntity news)
        {
            try
            {
                if (null == news || string.IsNullOrEmpty(keyId))
                {
                    return 0;
                }
                var queue = new QueueEntity
                {
                    Action = action,
                    Message = news,
                    Datetime = DateTime.Now
                };                

                return _db.PushQueue(keyId, queue);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueueEntity PopQueue(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueueEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion        

        #region ZoneVideo
        public long AddQueueZoneVideo(string keyId, string action, ZoneVideoEntity zone)
        {
            try
            {
                if (null == zone || string.IsNullOrEmpty(keyId))
                {
                    return 0;
                }
                var queue = new QueueZoneVideoEntity
                {
                    Action = action,
                    Message = zone,
                    Datetime = DateTime.Now
                };

                return _db.PushQueue(keyId, queue);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueueZoneVideoEntity PopQueueZoneVideo(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueueZoneVideoEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Video
        public long AddQueueVideo(string keyId, string action, VideoEntity video)
        {
            try
            {
                if (null == video || string.IsNullOrEmpty(keyId))
                {
                    return 0;
                }
                var queue = new QueueVideoEntity
                {
                    Action = action,
                    Message = video,
                    Datetime = DateTime.Now
                };

                return _db.PushQueue(keyId, queue);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueueVideoEntity PopQueueVideo(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueueVideoEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        public List<QueueEntity> CheckQueue(string keyId, int start, int num, ref long count)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.ListQueue<QueueEntity>(keyId, start, num, ref count);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region CD

        public long AddQueueCD(string keyId, string sessionId, string jsonKey, string action, string strParamValue, string channelName, string distributionDate, string message)
        {
            try
            {
                if (null == sessionId || string.IsNullOrEmpty(sessionId))
                {
                    return 0;
                }
                var queueCD = new QueuePushCDErrorEntity
                {
                    SessionId = sessionId,
                    JsonKey = jsonKey,
                    Action = action,
                    StrParamValue = strParamValue,
                    ChannelName = channelName,
                    DistributionDate = distributionDate,
                    Message=message
                };

                return _db.PushQueue(keyId, queueCD);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueuePushCDErrorEntity PopQueueCD(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueuePushCDErrorEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<QueuePushCDErrorEntity> CheckQueueCD(string keyId, int start, int num, ref long count)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.ListQueue<QueuePushCDErrorEntity>(keyId, start, num, ref count);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region ShareVideo

        public long AddQueueShareVideo(string keyId, string sessionId, string action, string channelName, PayLoadShareVideoEntity payload)
        {
            try
            {
                if (null == sessionId || string.IsNullOrEmpty(sessionId))
                {
                    return 0;
                }
                var queueVideo = new QueuePushShareVideoEntity
                {
                    SessionId = sessionId,                    
                    Action = action,                    
                    PayLoad = payload,
                    ChannelName = channelName,
                    Datetime = DateTime.Now
                };

                return _db.PushQueue(keyId, queueVideo);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public QueuePushShareVideoEntity PopQueueShareVideo(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.PopQueue<QueuePushShareVideoEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<QueuePushShareVideoEntity> CheckQueueShareVideo(string keyId, int start, int num, ref long count)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.ListQueue<QueuePushShareVideoEntity>(keyId, start, num, ref count);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected QueueDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
