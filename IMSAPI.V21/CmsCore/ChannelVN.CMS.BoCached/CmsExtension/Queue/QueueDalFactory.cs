﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Init;
using ChannelVN.CMS.BoCached.Entity.Queue;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoCached.CmsExtension.Queue
{
    public class QueueDalFactory
    {
        #region Zone
        public static long AddQueueZone(string keyId, string action, ZoneEntity zone)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueueZone(keyId, action, zone);
            }
            return returnValue;
        }

        public static QueueZoneEntity PopQueueZone(string keyId)
        {
            QueueZoneEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueueZone(keyId);
            }
            return returnValue;
        }
        #endregion

        #region News
        public static long AddQueue(string keyId, string action, NewsEntity news)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueue(keyId, action, news);
            }            
            return returnValue;
        }

        public static QueueEntity PopQueue(string keyId)
        {
            QueueEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueue(keyId);
            }
            return returnValue;
        }
        #endregion
        
        #region ZoneVideo
        public static long AddQueueZoneVideo(string keyId, string action, ZoneVideoEntity zone)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueueZoneVideo(keyId, action, zone);
            }
            return returnValue;
        }

        public static QueueZoneVideoEntity PopQueueZoneVideo(string keyId)
        {
            QueueZoneVideoEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueueZoneVideo(keyId);
            }
            return returnValue;
        }
        #endregion

        #region Video
        public static long AddQueueVideo(string keyId, string action, VideoEntity video)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueueVideo(keyId, action, video);
            }
            return returnValue;
        }

        public static QueueVideoEntity PopQueueVideo(string keyId)
        {
            QueueVideoEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueueVideo(keyId);
            }
            return returnValue;
        }
        #endregion

        public static List<QueueEntity> CheckQueue(string keyId, int start, int num, ref long count)
        {
            List<QueueEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.CheckQueue(keyId, start, num, ref count);
            }
            return returnValue;
        }

        #region CD

        public static long AddQueueCD(string keyId, string sessionId, string jsonKey, string action, string strParamValue, string channelName, string distributionDate, string message)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueueCD(keyId, sessionId, jsonKey, action, strParamValue, channelName, distributionDate, message);
            }
            return returnValue;
        }

        public static QueuePushCDErrorEntity PopQueueCD(string keyId)
        {
            QueuePushCDErrorEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueueCD(keyId);
            }
            return returnValue;
        }

        public static List<QueuePushCDErrorEntity> CheckQueueCD(string keyId, int start, int num, ref long count)
        {
            List<QueuePushCDErrorEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.CheckQueueCD(keyId, start, num, ref count);
            }
            return returnValue;
        }

        #endregion

        #region ShareVideo

        public static long AddQueueShareVideo(string keyId, string sessionId, string action, string channelName, PayLoadShareVideoEntity payload)
        {
            long returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.AddQueueShareVideo(keyId, sessionId, action, channelName, payload);
            }
            return returnValue;
        }

        public static QueuePushShareVideoEntity PopQueueShareVideo(string keyId)
        {
            QueuePushShareVideoEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.PopQueueShareVideo(keyId);
            }
            return returnValue;
        }

        public static List<QueuePushShareVideoEntity> CheckQueueShareVideo(string keyId, int start, int num, ref long count)
        {
            List<QueuePushShareVideoEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QueueDal.CheckQueueShareVideo(keyId, start, num, ref count);
            }
            return returnValue;
        }

        #endregion
    }
}
