﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.DiscussionV2.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.CmsExtension.DiscussionV2
{
    public class DiscussionV2DalFactory
    {
        #region DiscussionTopic

        public static bool AddDiscussionTopic(ViewDiscussionV2Entity viewDiscussionTopic)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.DiscussionV2Dal.AddDiscussionTopic(viewDiscussionTopic);
            }
            return returnValue;
        }

        public static List<ViewDiscussionV2Entity> GetDiscussionTopicByListId(List<string> listId)
        {
            List<ViewDiscussionV2Entity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.DiscussionV2Dal.GetDiscussionTopicByListId(listId);
            }
            return returnValue;
        }

        public static bool InitAllDiscussionTopic(List<ViewDiscussionV2Entity> listDiscussionTopic)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.DiscussionV2Dal.InitAllDiscussionTopic(listDiscussionTopic);
            }
            return returnValue;
        }

        #endregion

        #region Discussion


        #endregion
    }
}
