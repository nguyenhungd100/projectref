﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.DiscussionV2.Entity;

namespace ChannelVN.CMS.BoCached.CmsExtension.DiscussionV2
{
    public abstract class DiscussionV2DalBase
    {
        #region DiscussionTopic

        public bool AddDiscussionTopic(ViewDiscussionV2Entity viewDiscussionTopic)
        {
            try
            {
                if (null == viewDiscussionTopic)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<ViewDiscussionV2Entity>(viewDiscussionTopic.Id.ToString(), viewDiscussionTopic);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ViewDiscussionV2Entity> GetDiscussionTopicByListId(List<string> listId)
        {
            try
            {                
                return _db.GetsFromHash<ViewDiscussionV2Entity>(listId);
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllDiscussionTopic(List<ViewDiscussionV2Entity> listDiscussionTopic)
        {
            try
            {
                var data = new Dictionary<string, ViewDiscussionV2Entity>();
                foreach (var entry in listDiscussionTopic)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<ViewDiscussionV2Entity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected DiscussionV2DalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
