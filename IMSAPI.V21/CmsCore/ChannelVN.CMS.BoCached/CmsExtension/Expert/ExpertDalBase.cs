﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.Expert.Entity;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.CmsExtension.Expert
{
    public abstract class ExpertDalBase
    {
        public bool AddExpert(ExpertEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<ExpertEntity>(news.Id.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddExpertByNewsId(long id, ExpertEntity expert)
        {
            try
            {
                if (null == expert)
                {
                    return false;
                }
                var news = _db.GetFromHash<NewsCachedEntity>(id.ToString());
                if (news != null)
                {
                    news.ExpertId = expert.Id;
                    news.ExpertQuote = expert.Quote;
                }
    
                return _db.AddInHash(id.ToString(), news);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ExpertEntity GetExpertByNewsId(long id)
        {
            try
            {
                var result = _db.GetFromHash<NewsCachedEntity>(id.ToString());
                if (result != null)
                {
                    return new ExpertEntity
                    {
                        Id = result.ExpertId,
                        Quote = result.ExpertQuote
                    };
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected ExpertDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
