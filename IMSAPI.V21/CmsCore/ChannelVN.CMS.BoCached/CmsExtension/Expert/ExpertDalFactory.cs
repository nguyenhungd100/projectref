﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.Expert.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.CmsExtension.Expert
{
    public class ExpertDalFactory
    {
        public static bool AddExpert(ExpertEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ExpertDal.AddExpert(news);
            }
            return returnValue;
        }

        public static bool AddExpertByNewsId(long id, ExpertEntity expert)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ExpertDal.AddExpertByNewsId(id,expert);
            }
            return returnValue;
        }

        public static ExpertEntity GetExpertByNewsId(long id)
        {
            ExpertEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ExpertDal.GetExpertByNewsId(id);
            }
            return returnValue;
        }
    }
}
