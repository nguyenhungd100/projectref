﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.CmsExtension.LogInitAsync
{
    public class LogInitAsyncDal: LogInitAsyncDalBase
    {
        internal LogInitAsyncDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
