﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Init;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.CmsExtension.LogInitAsync
{
    public class LogInitAsyncDalFactory
    {
        public static bool AddLogInitAsync(LogInitAsyncEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.LogInitAsyncDal.AddLogInitAsync(news);
            }
            return returnValue;
        }

        public static LogInitAsyncEntity GetLogInitAsyncById(string id)
        {
            LogInitAsyncEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.LogInitAsyncDal.GetLogInitAsyncById(id);
            }
            return returnValue;
        }
    }
}
