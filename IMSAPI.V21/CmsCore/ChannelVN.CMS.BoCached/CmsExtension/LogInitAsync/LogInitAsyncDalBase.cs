﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using ChannelVN.CMS.BoCached.Entity.Init;

namespace ChannelVN.CMS.BoCached.CmsExtension.LogInitAsync
{
    public abstract class LogInitAsyncDalBase
    {
        public bool AddLogInitAsync(LogInitAsyncEntity log)
        {
            try
            {
                if (null == log)
                {
                    return false;
                }
                //_db.RemoveFromHash<LogInitAsyncEntity>(log.Id.ToString()); 
                                     
                return _db.AddInHash(log.Id.ToString(), log);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LogInitAsyncEntity GetLogInitAsyncById(string id)
        {
            try
            {                
                return _db.GetFromHash<LogInitAsyncEntity>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected LogInitAsyncDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
