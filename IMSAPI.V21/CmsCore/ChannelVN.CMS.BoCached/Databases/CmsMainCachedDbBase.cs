﻿using ChannelVN.CMS.BoCached.Base.Account;
using ChannelVN.CMS.BoCached.Base.AuthenticateLog;
using ChannelVN.CMS.BoCached.Base.News;
using ChannelVN.CMS.BoCached.Base.NewsNotify;
using ChannelVN.CMS.BoCached.Base.NewsPosition;
using ChannelVN.CMS.BoCached.Base.NewsSource;
using ChannelVN.CMS.BoCached.Base.Security;
using ChannelVN.CMS.BoCached.Base.Tag;
using ChannelVN.CMS.BoCached.Base.Topic;
using ChannelVN.CMS.BoCached.Base.Vote;
using ChannelVN.CMS.BoCached.Base.Zone;
using ChannelVN.CMS.BoCached.CmsExtension.Expert;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.BoCached.Base.NewsAuthor;
using ChannelVN.CMS.BoCached.Base.NewsExtension;
using ChannelVN.CMS.BoCached.Base.NewsContent;
using ChannelVN.CMS.BoCached.Base.NewsVersion;
using ChannelVN.CMS.BoCached.Base.NewsChildVersion;
using ChannelVN.CMS.BoCached.Base.NewsChild;
using ChannelVN.CMS.BoCached.CmsExtension.BrandContent;
using ChannelVN.CMS.BoCached.Base.Thread;
using ChannelVN.CMS.BoCached.Base.NewsPr;
using ChannelVN.CMS.BoCached.Base.NewsHistory;
using ChannelVN.CMS.BoCached.CmsExtension.SocialNetwork;
using ChannelVN.CMS.BoCached.Base.NewsConfig;
using ChannelVN.CMS.BoCached.CmsExtension.LogInitAsync;
using ChannelVN.CMS.BoCached.Base.Video;
using ChannelVN.CMS.BoCached.Base.ZoneVideo;
using ChannelVN.CMS.BoCached.CmsExtension.DiscussionV2;
using ChannelVN.CMS.BoCached.Extension.Kenh14Extension;
using ChannelVN.CMS.BoCached.Base.Statistic;
using ChannelVN.CMS.BoCached.Base.VideoChannel;
using ChannelVN.CMS.BoCached.Base.PlayList;
using ChannelVN.CMS.BoCached.Base.EndUser;
using ChannelVN.CMS.BoCached.CmsExtension.Queue;
using ChannelVN.CMS.BoCached.Base.StreamItem;

namespace ChannelVN.CMS.BoCached.Databases
{
    public abstract class CmsMainCachedDbBase : CachedDbBase
    {
        #region Store procedures

        #region UserDal
        private UserDal _userDal;
        public UserDal UserDal
        {
            get { return _userDal ?? (_userDal = new UserDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region ZoneDal
        private ZoneDal _zoneDal;
        public ZoneDal ZoneDal
        {
            get { return _zoneDal ?? (_zoneDal = new ZoneDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region PermissionDal
        private PermissionDal _permissionDal;
        public PermissionDal PermissionDal
        {
            get { return _permissionDal ?? (_permissionDal = new PermissionDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region UserSmsDal
        private UserSmsDal _userSmsDal;
        public UserSmsDal UserSmsDal
        {
            get { return _userSmsDal ?? (_userSmsDal = new UserSmsDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsDal
        private NewsDal _newsDal;
        public NewsDal NewsDal
        {
            get { return _newsDal ?? (_newsDal = new NewsDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsAuthorDal
        private NewsAuthorDal _newsAuthorDal;
        public NewsAuthorDal NewsAuthorDal
        {
            get { return _newsAuthorDal ?? (_newsAuthorDal = new NewsAuthorDal((CmsMainCachedDb)this)); }
        }
        #endregion        

        #region NewsNotifyDal
        private NewsNotifyDal _newsNotifyDal;
        public NewsNotifyDal NewsNotifyDal
        {
            get { return _newsNotifyDal ?? (_newsNotifyDal = new NewsNotifyDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsPositionDal
        private NewsPositionDal _newsPositionDal;
        public NewsPositionDal NewsPositionDal
        {
            get { return _newsPositionDal ?? (_newsPositionDal = new NewsPositionDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region AuthenticateLogDal
        private AuthenticateLogDal _authenticateLogDal;
        public AuthenticateLogDal AuthenticateLogDal
        {
            get { return _authenticateLogDal ?? (_authenticateLogDal = new AuthenticateLogDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region TopicDal
        private TopicDal _topicDal;
        public TopicDal TopicDal
        {
            get { return _topicDal ?? (_topicDal = new TopicDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region VoteDal
        private VoteDal _voteDal;
        public VoteDal VoteDal
        {
            get { return _voteDal ?? (_voteDal = new VoteDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region TagDal
        private TagDal _tagDal;
        public TagDal TagDal
        {
            get { return _tagDal ?? (_tagDal = new TagDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsSourceDal
        private NewsSourceDal _newsSourceDal;
        public NewsSourceDal NewsSourceDal
        {
            get { return _newsSourceDal ?? (_newsSourceDal = new NewsSourceDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsExtensionDal
        private NewsExtensionDal _newsExtensionDal;
        public NewsExtensionDal NewsExtensionDal
        {
            get { return _newsExtensionDal ?? (_newsExtensionDal = new NewsExtensionDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsContentDal
        private NewsContentDal _newsContentDal;
        public NewsContentDal NewsContentDal
        {
            get { return _newsContentDal ?? (_newsContentDal = new NewsContentDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsPrDal
        private NewsPrDal _newsPrDal;
        public NewsPrDal NewsPrDal
        {
            get { return _newsPrDal ?? (_newsPrDal = new NewsPrDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsVersionDal
        private NewsVersionDal _newsVersionDal;
        public NewsVersionDal NewsVersionDal
        {
            get { return _newsVersionDal ?? (_newsVersionDal = new NewsVersionDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsChildVersionDal
        private NewsChildVersionDal _newsChildVersionDal;
        public NewsChildVersionDal NewsChildVersionDal
        {
            get { return _newsChildVersionDal ?? (_newsChildVersionDal = new NewsChildVersionDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsChildDal
        private NewsChildDal _newsChildDal;
        public NewsChildDal NewsChildDal
        {
            get { return _newsChildDal ?? (_newsChildDal = new NewsChildDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsHistoryDal
        private NewsHistoryDal _newsHistoryDal;
        public NewsHistoryDal NewsHistoryDal
        {
            get { return _newsHistoryDal ?? (_newsHistoryDal = new NewsHistoryDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region NewsConfigDal
        private NewsConfigDal _newsConfigDal;
        public NewsConfigDal NewsConfigDal
        {
            get { return _newsConfigDal ?? (_newsConfigDal = new NewsConfigDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region ThreadDal
        private ThreadDal _threadDal;
        public ThreadDal ThreadDal
        {
            get { return _threadDal ?? (_threadDal = new ThreadDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region CmsExtension.ExpertDal
        private ExpertDal _expertDal;
        public ExpertDal ExpertDal
        {
            get { return _expertDal ?? (_expertDal = new ExpertDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region CmsExtension.BrandContentDal
        private BrandContentDal _brandContentDal;
        public BrandContentDal BrandContentDal
        {
            get { return _brandContentDal ?? (_brandContentDal = new BrandContentDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region CmsExtension.ActivityDal
        private ActivityDal _activityDal;
        public ActivityDal ActivityDal
        {
            get { return _activityDal ?? (_activityDal = new ActivityDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region CmsExtension.LogInitAsyncDal
        private LogInitAsyncDal _logInitAsyncDal;
        public LogInitAsyncDal LogInitAsyncDal
        {
            get { return _logInitAsyncDal ?? (_logInitAsyncDal = new LogInitAsyncDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region CmsExtension.DiscussionV2Dal
        private DiscussionV2Dal _discussionV2Dal;
        public DiscussionV2Dal DiscussionV2Dal
        {
            get { return _discussionV2Dal ?? (_discussionV2Dal = new DiscussionV2Dal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region VideoDal
        private VideoDal _videoDal;
        public VideoDal VideoDal
        {
            get { return _videoDal ?? (_videoDal = new VideoDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region ZoneVideoDal
        private ZoneVideoDal _zoneVideoDal;
        public ZoneVideoDal ZoneVideoDal
        {
            get { return _zoneVideoDal ?? (_zoneVideoDal = new ZoneVideoDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region VideoTagDal
        private VideoTagDal _videoTagDal;
        public VideoTagDal VideoTagDal
        {
            get { return _videoTagDal ?? (_videoTagDal = new VideoTagDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region VideoYoutubeDal
        private VideoYoutubeDal _videoYoutubeDal;
        public VideoYoutubeDal VideoYoutubeDal
        {
            get { return _videoYoutubeDal ?? (_videoYoutubeDal = new VideoYoutubeDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region QuoteDal
        private QuoteDal _quoteDal;
        public QuoteDal QuoteDal
        {
            get { return _quoteDal ?? (_quoteDal = new QuoteDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region StatisticDal
        private StatisticDal _statisticDal;
        public StatisticDal StatisticDal
        {
            get { return _statisticDal ?? (_statisticDal = new StatisticDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region VideoChannelDal
        private VideoChannelDal _videoChannelDal;
        public VideoChannelDal VideoChannelDal
        {
            get { return _videoChannelDal ?? (_videoChannelDal = new VideoChannelDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region PlayListDal
        private PlayListDal _playListDal;
        public PlayListDal PlayListDal
        {
            get { return _playListDal ?? (_playListDal = new PlayListDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region EndUserDal
        private EndUserDal _endUserDal;
        public EndUserDal EndUserDal
        {
            get { return _endUserDal ?? (_endUserDal = new EndUserDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region Queue
        private QueueDal _queueDal;
        public QueueDal QueueDal
        {
            get { return _queueDal ?? (_queueDal = new QueueDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #region StreamItem
        private StreamItemDal _streamItemDal;
        public StreamItemDal StreamItemDal
        {
            get { return _streamItemDal ?? (_streamItemDal = new StreamItemDal((CmsMainCachedDb)this)); }
        }
        #endregion

        #endregion

        #region Constructors

        protected CmsMainCachedDbBase()
        {
            Initialize();
        }

        #endregion

        #region Protected methods

        protected abstract void Initialize();

        #endregion
    }
}
