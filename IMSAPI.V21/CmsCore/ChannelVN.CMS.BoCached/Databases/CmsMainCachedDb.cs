﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Databases
{
    public class CmsMainCachedDb: CmsMainCachedDbBase
    {
        private const string ConnectionStringName = "CmsMainCachedDb";
        private const string ConnectionStringName_Dev = "CmsMainCachedDb_Dev";        

        /// <summary>
        /// Initializes the information to connect the database.
        /// </summary>
        protected override void Initialize()
        {
            var strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName, Constants.ConnectionDecryptKey);

            if (AppSettings.GetBool("IsDevEnv"))
            {
                //strConn = "10.3.11.211:2001,password=bbbsf34@45dfdfvbnvbvb3SFKKJgjs,defaultDatabase=10";
                strConn = ServiceChannelConfiguration.GetConnectionString(WcfMessageHeader.Current.Namespace,
                ConnectionStringName_Dev, Constants.ConnectionDecryptKey);
            }

            //var strConn = "";

            //if (AppSettings.GetBool("IsDevEnv"))
            //{                
            //    //strConn = "redis://192.168.38.97:6379?db=15";
            //    strConn = "192.168.38.97:6379,defaultDatabase=15";
            //}
            //else
            //{                
            //    //strConn = "redis://bdmpos@kSFMCS3534sjd@dfg@10.3.11.77:2027?db=10";                
            //    strConn = "10.3.11.211:2001,password=bbbsf34@45dfdfvbnvbvb3SFKKJgjs,defaultDatabase=10";
            //}

            this.ConnectionString = strConn;
            this.Namespace = WcfMessageHeader.Current.Namespace;
        }
    }
}
