﻿using System;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;
using ChannelVN.CMS.Common.RedisClientHelper;

namespace ChannelVN.CMS.BoCached.CacheLib
{
    public class RedisCache : ICached
    {
        private int DbNumber
        {
            get { return Utility.ConvertToInt(ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "RedisPublishDb")); }
        }

        public bool Add<T>(string key, T value)
        {
            try
            {
                var redisClient = new RedisStackClient(DbNumber);
                redisClient.Remove(key);
                redisClient.Add(key, value);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Add<T>(string key, T value, DateTime expiredDate)
        {
            try
            {
                var redisClient = new RedisStackClient(DbNumber);
                redisClient.Remove(key);
                redisClient.Add(key, value, expiredDate.TimeOfDay);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Remove(string key)
        {
            try
            {
                var redisClient = new RedisStackClient(DbNumber);
                redisClient.Remove(key);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Exists(string key)
        {
            var redisClient = new RedisStackClient(DbNumber);
            return redisClient.Get(key) != null;
        }

        public T Get<T>(string key)
        {
            try
            {
                var redisClient = new RedisStackClient(DbNumber);
                return redisClient.Get<T>(key);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
    }
}
