﻿using System.Collections.Generic;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.BoCached.CacheObjects
{
    public class PermissionCached : CacheObjectBase
    {
        public ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId)
        {
            var cachedKey = string.Format("CheckUserPermission[{0},{1}]", username, permissionId);
            var data = Get<string>(username, cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                var existsUser = UserDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    data = bool.FalseString;
                }
                else
                {
                    if (existsUser.IsFullPermission)
                    {
                        data = bool.TrueString;
                    }
                    else
                    {
                        var userPermission = UserPermissionDal.GetListUserPermissionByUsernameAndPermissionId(username, permissionId);

                        data = userPermission.Count > 0 ? bool.TrueString : bool.FalseString;
                    }
                }
                Add(username, cachedKey, data);
            }
            return data == bool.TrueString ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }
        public ErrorMapping.ErrorCodes CheckUserPermission(string username, int permissionId, int zoneId)
        {
            var cachedKey = string.Format("CheckUserPermission[{0},{1}]", username, permissionId);
            var data = Get<string>(username, cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                var existsUser = UserDal.GetUserByUsername(username);
                if (null == existsUser)
                {
                    data = bool.FalseString;
                }
                else
                {
                    if (existsUser.IsFullPermission)
                    {
                        data = bool.TrueString;
                    }
                    else
                    {
                        var userPermission = new UserPermissionEntity
                        {
                            UserId = existsUser.Id,
                            PermissionId = permissionId,
                            ZoneId = zoneId
                        };

                        data = UserPermissionDal.CheckUserPermission(userPermission) ? bool.TrueString : bool.FalseString;
                    }
                }
                Add(username, cachedKey, data);
            }
            return data == bool.TrueString ? ErrorMapping.ErrorCodes.Success : ErrorMapping.ErrorCodes.BusinessError;
        }

        public List<UserPermissionEntity> GetListByUserName(string username)
        {
            var cachedKey = string.Format("GetUserByUsername[{0}]", username);
            var data = Get<List<UserPermissionEntity>>(username, cachedKey);
            if (data == null)
            {
                data = UserPermissionDal.GetListUserPermissionByUserName(username);
                Add(username, cachedKey, data);
            }
            return data;
        }

        public bool CheckUserInGroupPermission(int userId, int groupId)
        {
            var cachedKey = string.Format("CheckUserInGroupPermission[{0},{1}]", userId, groupId);
            var data = Get<string>(userId.ToString(), cachedKey);
            if (string.IsNullOrEmpty(data))
            {
                data = GroupPermissionDal.CheckUserInGroupPermission(userId, groupId)
                           ? bool.TrueString
                           : bool.FalseString;
                Add(userId.ToString(), cachedKey, data);
            }
            return data == bool.TrueString;
        }

        public bool RemoveAllCachedByGroup(int userId)
        {
            RemoveAllCachedByGroup(userId.ToString());
            var user = UserDal.GetUserById(userId);
            if (user != null)
            {
                RemoveAllCachedByGroup(user.UserName);
            }
            return true;
        }

        public override void RemoveAllCachedByGroup(string group, bool removeGroup = true)
        {
            var user = UserDal.GetUserByUsername(group);
            if (user != null)
            {
                RemoveAllCachedByGroup(user.Id.ToString());
            }
            base.RemoveAllCachedByGroup(group, removeGroup);
        }
    }
}
