﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;

namespace ChannelVN.CMS.BoCached.CacheObjects
{
    public class UserCached : CacheObjectBase
    {
        public UserEntity GetUserByUsername(string username)
        {
            var cachedKey = string.Format("GetUserByUsername[{0}]", username);
            var data = Get<UserEntity>(username, cachedKey);
            if (data == null)
            {
                data = UserDal.GetUserByUsername(username);
                //if (null != data)
                //{
                //    data.EncryptId = CryptonForId.EncryptId(data.Id);
                //    data.Id = 0;
                //}
                Add(username, cachedKey, data);
            }
            return data;
        }

        public bool RemoveAllCachedByGroup(int userId)
        {
            RemoveAllCachedByGroup(userId.ToString());
            var user = UserDal.GetUserById(userId);
            if (user != null)
            {
                RemoveAllCachedByGroup(user.UserName);
            }
            return true;
        }

        public override void RemoveAllCachedByGroup(string group, bool removeGroup = true)
        {
            var user = UserDal.GetUserByUsername(group);
            if (user != null)
            {
                RemoveAllCachedByGroup(user.Id.ToString());
            }
            base.RemoveAllCachedByGroup(group, removeGroup);
        }
    }
}
