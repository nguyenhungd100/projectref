﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Zone;

namespace ChannelVN.CMS.BoCached.CacheObjects
{
    public class ZoneCached : CacheObjectBase
    {
        public ZoneEntity GetZoneById(int id)
        {

            var cachedKey = string.Format("GetZoneById[{0}]", id);
            var data = Get<ZoneEntity>(id.ToString(), cachedKey);
            if (data == null)
            {
                data = ZoneDal.GetZoneById(id);
                Add(id.ToString(), cachedKey, data);
            }
            return data;
        }
    }
}
