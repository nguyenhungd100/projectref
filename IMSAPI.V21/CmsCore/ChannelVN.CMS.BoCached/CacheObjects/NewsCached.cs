﻿using System.Collections.Generic;
using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.News;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BoCached.CacheObjects
{
    public class NewsCached : CacheObjectBase
    {
        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            var cachedKey = string.Format("GetNewsForValidateById[{0}]", id);
            var data = Get<NewsForValidateEntity>(id.ToString(), cachedKey);
            if (data == null)
            {
                data = NewsDal.GetNewsForValidateById(id);
                Add(id.ToString(), cachedKey, data);
            }
            return data;
        }

        public NewsEntity GetNewsById(long id)
        {
            var cachedKey = string.Format("GetNewsById[{0}]", id);
            var data = Get<NewsEntity>(id.ToString(), cachedKey);
            if (data == null)
            {
                data = NewsDal.GetNewsById(id);
                Add(id.ToString(), cachedKey, data);
            }
            return data;
        }

        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            var cachedKey = string.Format("GetNewsById[{0}]", newsId);
            var data = Get<List<NewsInZoneEntity>>(newsId.ToString(), cachedKey);
            if (data == null)
            {
                data = NewsInZoneDal.GetNewsInZoneByNewsId(newsId);
                Add(newsId.ToString(), cachedKey, data);
            }
            return data;
        }
    }
}
