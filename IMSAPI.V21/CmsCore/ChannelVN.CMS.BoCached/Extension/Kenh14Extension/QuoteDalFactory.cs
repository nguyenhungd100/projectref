﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.Kenh14.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Extension.Kenh14Extension
{
    public class QuoteDalFactory
    {
        #region Quote
        public static bool InitAllQuote(List<QuoteEntity> listQuote)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.InitAllQuote(listQuote);
            }
            return returnValue;
        }

        public static QuoteEntity GetQuoteById(int quoteId)
        {
            QuoteEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.GetQuoteById(quoteId);
            }
            return returnValue;
        }

        public static bool AddQuote(QuoteEntity quote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.AddQuote(quote);
            }
            return returnValue;
        }

        public static bool UpdateQuote(QuoteEntity quote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.UpdateQuote(quote);
            }
            return returnValue;
        }
        #endregion

        #region CharacterInfo
        public static bool InitAllCharacterInfo(List<CharacterInfoEntity> listQuote)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.InitAllCharacterInfo(listQuote);
            }
            return returnValue;
        }

        public static CharacterInfoEntity GetCharacterInfoById(int quoteId)
        {
            CharacterInfoEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.GetCharacterInfoById(quoteId);
            }
            return returnValue;
        }

        public static bool AddCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.AddCharacterInfo(quote);
            }
            return returnValue;
        }

        public static bool UpdateCharacterInfo(CharacterInfoEntity quote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.UpdateCharacterInfo(quote);
            }
            return returnValue;
        }

        public static bool DeleteCharacterInfoById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.QuoteDal.DeleteCharacterInfoById(id);
            }
            return returnValue;
        }
        #endregion
    }
}
