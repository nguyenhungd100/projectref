﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.Kenh14.Entity;

namespace ChannelVN.CMS.BoCached.Extension.Kenh14Extension
{
    public abstract class QuoteDalBase
    {
        #region Quote
        public bool InitAllQuote(List<QuoteEntity> listQuote)
        {
            try
            {               
                var data = new Dictionary<string, QuoteEntity>();
                foreach(var entry in listQuote)
                {
                    data[entry.QuoteId.ToString()] = entry;
                }                
                return _db.AddInHash<QuoteEntity>(data);
            }
            catch(Exception)
            {
                return false;
            }
        }

        public QuoteEntity GetQuoteById(int quoteId)
        {
            try
            {
                var replies = _db.GetFromHash<QuoteEntity>(quoteId.ToString());

                if (null != replies)
                {
                    return replies;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool AddQuote(QuoteEntity quote)
        {
            try
            {
                if (null == quote)
                {
                    return false;
                }
                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<QuoteEntity>(quote.QuoteId.ToString(), quote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateQuote(QuoteEntity quote)
        {
            try
            {
                if (null == quote)
                {
                    return false;
                }

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<QuoteEntity>(quote.QuoteId.ToString(), quote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Quote
        public bool InitAllCharacterInfo(List<CharacterInfoEntity> listQuote)
        {
            try
            {
                var data = new Dictionary<string, CharacterInfoEntity>();
                foreach (var entry in listQuote)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<CharacterInfoEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public CharacterInfoEntity GetCharacterInfoById(int quoteId)
        {
            try
            {
                var replies = _db.GetFromHash<CharacterInfoEntity>(quoteId.ToString());

                if (null != replies)
                {
                    return replies;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool AddCharacterInfo(CharacterInfoEntity quote)
        {
            try
            {
                if (null == quote)
                {
                    return false;
                }

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash(quote.Id.ToString(), quote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateCharacterInfo(CharacterInfoEntity quote)
        {
            try
            {
                if (null == quote)
                {
                    return false;
                }

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash(quote.Id.ToString(), quote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteCharacterInfoById(int id)
        {
            try
            {                              
                return _db.RemoveFromHash<CharacterInfoEntity>(id.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected QuoteDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
