﻿using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.BoCached.Entity.Video
{
    [DataContract]
    public class VideoCachedEntity: EntityBase
    {
        [DataMember]
        public VideoEntity VideoInfo { get; set; }

        [DataMember]
        public List<VideoTagEntity> TagList { get; set; }

        [DataMember]
        public List<VideoEntity> VideoRelation { get; set; }

        [DataMember]
        public List<PlaylistEntity> Playlist { get; set; }

        [DataMember]
        public List<VideoChannelEntity> VideoChannel { get; set; }

        [DataMember]
        public List<ZoneVideoEntity> ZoneVideoList { get; set; }

        [DataMember]
        public string ShareLink { get; set; }

        [DataMember]
        public List<VideoExtensionEntity> VideoExtension { get; set; }

        [DataMember]
        public VideoFromYoutubeEntity VideoYoutube { get; set; }
    }
}
