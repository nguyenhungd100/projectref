﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Entity.Security
{
    [DataContract]
    public class UserCachedEntity: EntityBase
    {
        [DataMember]
        public UserEntity User { get; set; }
        [DataMember]
        public List<UserPermissionEntity> PermissionList { get; set; }

        public UserCachedEntity()
        {
            this.User = new UserEntity();
            this.PermissionList = new List<UserPermissionEntity>();
        }
    }
}
