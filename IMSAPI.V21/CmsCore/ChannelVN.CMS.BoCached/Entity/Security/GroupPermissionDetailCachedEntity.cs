﻿using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Entity.Security
{
    [DataContract]
    public class GroupPermissionDetailCachedEntity: GroupPermissionEntity
    {
        [DataMember]
        public List<string> PermissionList { get; set; }

        public GroupPermissionDetailCachedEntity()
        {
            PermissionList = new List<string>();
        }
    }
}
