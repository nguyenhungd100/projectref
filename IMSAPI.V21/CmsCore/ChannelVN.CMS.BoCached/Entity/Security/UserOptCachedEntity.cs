﻿using ChannelVN.CMS.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Entity.Security
{
    [DataContract]
    public class UserOptCachedEntity : EntityBase
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string OtpSecretKey { get; set; }
    }
}
