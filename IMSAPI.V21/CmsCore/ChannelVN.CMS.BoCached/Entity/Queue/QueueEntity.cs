﻿using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.BoCached.Entity.Queue
{
    [DataContract]
    public class QueueEntity
    {
        [DataMember]
        public NewsEntity Message { get; set; }
        [DataMember]
        public DateTime Datetime { get; set; }
        [DataMember]
        public string Action { get; set; }
    }

    [DataContract]
    public class QueueZoneEntity
    {
        [DataMember]
        public ZoneEntity Message { get; set; }
        [DataMember]
        public DateTime Datetime { get; set; }
        [DataMember]
        public string Action { get; set; }
    }

    [DataContract]
    public class QueueZoneVideoEntity
    {
        [DataMember]
        public ZoneVideoEntity Message { get; set; }
        [DataMember]
        public DateTime Datetime { get; set; }
        [DataMember]
        public string Action { get; set; }
    }

    [DataContract]
    public class QueueVideoEntity
    {
        [DataMember]
        public VideoEntity Message { get; set; }
        [DataMember]
        public DateTime Datetime { get; set; }
        [DataMember]
        public string Action { get; set; }
    }

    [DataContract]
    public class QueuePushCDErrorEntity
    {
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public string JsonKey { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public string StrParamValue { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
        [DataMember]
        public string DistributionDate { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class QueuePushShareVideoEntity
    {
        [DataMember]
        public string SessionId { get; set; }
        [DataMember]
        public PayLoadShareVideoEntity PayLoad { get; set; }
        [DataMember]
        public DateTime Datetime { get; set; }
        [DataMember]
        public string Action { get; set; }
        [DataMember]
        public string ChannelName { get; set; }
    }

    [DataContract]
    public class PayLoadShareVideoEntity
    {
        [DataMember]
        public List<PayLoadShareVideo> Data { get; set; }        
    }

    [DataContract]
    public class PayLoadShareVideo
    {        
        [DataMember]
        public VideoEntity Video { get; set; }
        [DataMember]
        public string TagIdList { get; set; }
        [DataMember]
        public string PlaylistIdList { get; set; }
        [DataMember]
        public string ZoneIdList { get; set; }
    }
}
