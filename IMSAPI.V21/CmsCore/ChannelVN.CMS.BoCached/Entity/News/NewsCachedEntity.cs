﻿using ChannelVN.BrandContent.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.Entity.Base.Zone;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ChannelVN.CMS.BoCached.Entity.News
{
    [DataContract]
    public class NewsCachedEntity: EntityBase
    {
        [DataMember]
        public NewsEntity NewsInfo { get; set; }        
        [DataMember]
        public List<ZoneWithSimpleFieldEntity> AllZone { get; set; }
        [DataMember]
        public List<NewsInZoneEntity> NewsInZone { get; set; }
        [DataMember]
        public List<TagNewsWithTagInfoEntity> TagInNews { get; set; }
        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelation { get; set; }
        [DataMember]
        public List<NewsPublishForNewsRelationEntity> NewsRelationSpecial { get; set; }
        [DataMember]
        public List<NewsByAuthorEntity> NewsByAuthor { get; set; }
        [DataMember]
        public List<NewsBySourceEntity> NewsBySource { get; set; }
        [DataMember]
        public List<NewsVersionWithSimpleFieldsEntity> ListVersion { get; set; }
        [DataMember]
        public List<NewsChildEntity> ListNewsChild { get; set; }
        [DataMember]
        public List<NewsExtensionEntity> NewsExtensions { get; set; }
        [DataMember]
        public string NewsContentWithTemplate { get; set; }
        [DataMember]
        public int ExpertId { get; set; }
        [DataMember]
        public string ExpertQuote { get; set; }
        [DataMember]
        public VoteEntity VoteInfo { get; set; }
        [DataMember]
        public int TopicId { get; set; }
        [DataMember]
        public List<BrandContentEntity> BrandContent { get; set; }
        [DataMember]
        public List<ThreadEntity> ThreadNewsInfo { get; set; }
        [DataMember]
        public NewsPrEntity NewsPr { get; set; }
        
    }

    [DataContract]
    public class NewsParentIdAndOriginalIdEntity
    {
        [DataMember]
        public NewsEntity NewsInfo { get; set; }        
        [DataMember]
        public long NewsParentId { get; set; }
        [DataMember]
        public int OriginalId { get; set; }
    }

    [DataContract]
    public class NewsCheckDuplicate
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public int Status { get; set; }
    }

    [DataContract]
    public class NewsInUsingEntity
    {
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string AccountName { get; set; }
        [DataMember]
        public long ExprireTime { get; set; }
    }
}
