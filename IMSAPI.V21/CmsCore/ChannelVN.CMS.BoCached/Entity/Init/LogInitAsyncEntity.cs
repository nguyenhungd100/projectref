﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Entity.Init
{
    [DataContract]
    public class LogInitAsyncEntity
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public int Total { get; set; }
        [DataMember]
        public List<Section> Sections { get; set; }
    }
    [DataContract]
    public class Section
    {
        [DataMember]
        public int Page { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}
