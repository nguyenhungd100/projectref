﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BoCached.CacheObjectV2
{
    public class UserDataCached : CacheObjectV2Base
    {
        private static int CacheExpiredInSecond = 60 * 60 * 24 * 30;//In 30 days
        public static UserEntity GetUserByUsername(string username)
        {
            var cachedKey = string.Format("GetUserByUsername[{0}]", username);
            var data = Get<UserEntity>(cachedKey);
            if (data == null)
            {
                data = UserDal.GetUserByUsername(username);
                Add(cachedKey, data, CacheExpiredInSecond);
            }
            return data;
        }
        public static UserEntity GetUserById(int id)
        {
            var cachedKey = string.Format("GetUserById[{0}]", id);
            var data = Get<UserEntity>(cachedKey);
            if (data == null)
            {
                data = UserDal.GetUserById(id);
                Add(cachedKey, data, CacheExpiredInSecond);
            }
            return data;
        }
        public override void RemoveCachedByPrefix(string cachePrefix)
        {
            var us = GetUserByUsername(cachePrefix);
            if(us != null)
                Remove(string.Format("GetUserById[{0}]", us.Id));
            Remove(string.Format("GetUserByUsername[{0}]", cachePrefix));
        }
    }
}
