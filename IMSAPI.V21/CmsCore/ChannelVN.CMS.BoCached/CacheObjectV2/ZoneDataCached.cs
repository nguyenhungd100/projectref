﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.DAL.Base.Zone;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BoCached.CacheObjectV2
{
    public class ZoneDataCached : CacheObjectV2Base
    {
        private static int CacheExpiredInSecond = 60 * 60 * 24 * 30;//In 30 days

        public static List<ZoneEntity> GetZoneByUsername(string username)
        {
            var cachedKey = string.Format("GetZoneByUsername[{0}]", username);
            var data = Get<List<ZoneEntity>>(cachedKey);
            if (data == null)
            {
                data = ZoneDal.GetZoneByUsername(username);
                Add(cachedKey, data, CacheExpiredInSecond);
            }
            return data;
        }
        public override void RemoveCachedByPrefix(string cachePrefix)
        {
            Remove(string.Format("GetZoneByUsername[{0}]", cachePrefix));
        }
    }
}
