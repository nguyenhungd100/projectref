﻿using ChannelVN.CMS.BoCached.Common;
using ChannelVN.CMS.DAL.Base.Account;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.BoCached.CacheObjectV2
{
    public class PermissionDataCached : CacheObjectV2Base
    {
        private static int CacheExpiredInSecond = 60 * 60 * 24 * 30;//In 30 days

        public static List<UserPermissionEntity> GetListUserPermissionByUserName(string username)
        {
            var cachedKey = string.Format("GetListUserPermissionByUserName[{0}]", username);
            var data = Get<List<UserPermissionEntity>>(cachedKey);
            if (data == null)
            {
                data = UserPermissionDal.GetListUserPermissionByUserName(username);
                Add(cachedKey, data, CacheExpiredInSecond);
            }
            return data;
        }
        public override void RemoveCachedByPrefix(string cachePrefix)
        {
            Remove(string.Format("GetListUserPermissionByUserName[{0}]", cachePrefix));
        }
    }
}
