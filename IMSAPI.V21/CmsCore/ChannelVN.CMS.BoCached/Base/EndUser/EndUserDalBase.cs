﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.EndUser;

namespace ChannelVN.CMS.BoCached.Base.EndUser
{
    public abstract class EndUserDalBase
    {
        public bool AddEndUser(EndUserEntity videoChannel)
        {
            try
            {
                if (null == videoChannel)
                {
                    return false;
                }                
                _db.RemoveFromHash<EndUserEntity>(videoChannel.Id.ToString());

                return _db.AddInHash(videoChannel.Id.ToString(), videoChannel);                
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool AddEndUserByNewsId(long newsId, EndUserEntity vote)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    //data.EndUserInfo = vote;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public EndUserEntity GetEndUserByNewsId(long newsId)
        {
            var result = new EndUserEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    //result = news.EndUserInfo;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<EndUserEntity> GetEndUserByListId(List<string> voteIds)
        {
            try
            {
                return _db.GetsFromHash<EndUserEntity>(voteIds);
            }
            catch
            {
                return null;
            }
        }
        public EndUserEntity GetEndUserById(int id)
        {
            try
            {
                return _db.GetFromHash<EndUserEntity>(id.ToString());
            }
            catch
            {
                return null;
            }
        }
        public bool InitAllEndUser(List<EndUserEntity> listEndUser)
        {
            try
            {
                var data = new Dictionary<string, EndUserEntity>();
                foreach (var entry in listEndUser)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<EndUserEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected EndUserDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
