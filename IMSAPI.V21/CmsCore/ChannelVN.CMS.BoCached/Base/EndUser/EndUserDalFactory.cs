﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.EndUser;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoCached.Base.EndUser
{
    public class EndUserDalFactory
    {
        public static bool AddEndUser(EndUserEntity videoChannel)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.AddEndUser(videoChannel);
            }
            return returnValue;
        }
        public static bool AddEndUserByNewsId(long newsId,EndUserEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.AddEndUserByNewsId(newsId,vote);
            }
            return returnValue;
        }
        public static EndUserEntity GetEndUserByNewsId(long newsId)
        {
            EndUserEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.GetEndUserByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<EndUserEntity> GetEndUserByListId(List<string> voteIds)
        {
            List<EndUserEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.GetEndUserByListId(voteIds);
            }
            return returnValue;
        }

        public static EndUserEntity GetEndUserById(int id)
        {
            EndUserEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.GetEndUserById(id);
            }
            return returnValue;
        }

        public static bool InitAllEndUser(List<EndUserEntity> listEndUser)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.EndUserDal.InitAllEndUser(listEndUser);
            }
            return returnValue;
        }
    }
}
