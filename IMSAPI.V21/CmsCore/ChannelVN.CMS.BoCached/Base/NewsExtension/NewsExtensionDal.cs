﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsExtension
{
    public class NewsExtensionDal: NewsExtensionDalBase
    {
        internal NewsExtensionDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
