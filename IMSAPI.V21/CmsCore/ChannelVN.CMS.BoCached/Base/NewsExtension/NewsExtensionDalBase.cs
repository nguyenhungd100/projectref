﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;
using System.Linq;

namespace ChannelVN.CMS.BoCached.Base.NewsExtension
{
    public abstract class NewsExtensionDalBase
    {
        public bool AddNewsExtension(NewsExtensionEntity newsEx)
        {
            try
            {
                if (null == newsEx)
                {
                    return false;
                }
                var data = _db.GetFromHash<NewsCachedEntity>(newsEx.NewsId.ToString());
                if (data != null && data.NewsExtensions != null)
                {
                    data.NewsExtensions.RemoveAll(i => i.NewsId == newsEx.NewsId && i.Type == newsEx.Type);
                    data.NewsExtensions.Add(newsEx);
                    _db.AddInHash(newsEx.NewsId.ToString(), data);
                }

                return _db.AddInHash(newsEx.NewsId + "_" + newsEx.Type, newsEx); ;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool AddNewsExtensionByNewsId(long newsId, List<NewsExtensionEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsExtensions = list;
                }
                //_db.RemoveFromHash<NewsCachedEntity>(newsId.ToString());

                return _db.AddInHash(newsId.ToString(), data);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            var result = new List<NewsExtensionEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.NewsExtensions;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public NewsExtensionEntity GetNewsExtensionByKey(string key)
        {
            try
            {                                                
                return _db.GetFromHash<NewsExtensionEntity>(key);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<NewsExtensionEntity> GetNewsExtensionByListKey(List<string> listKey)
        {
            try
            {
                return _db.GetsFromHash<NewsExtensionEntity>(listKey);
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllNewsExtension(List<NewsExtensionEntity> list)
        {
            try
            {
                var data = new Dictionary<string, NewsExtensionEntity>();
                foreach (var entry in list)
                {
                    data[entry.NewsId+"_"+entry.Type] = entry;
                }
                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsExtensionDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
