﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsExtension
{
    public class NewsExtensionDalFactory
    {
        public static bool AddNewsExtension(NewsExtensionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.AddNewsExtension(news);
            }
            return returnValue;
        }
        public static bool AddNewsExtensionByNewsId(long newsId, List<NewsExtensionEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.AddNewsExtensionByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsExtensionEntity> GetNewsExtensionByNewsId(long newsId)
        {
            List<NewsExtensionEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.GetNewsExtensionByNewsId(newsId);
            }
            return returnValue;
        }

        public static NewsExtensionEntity GetNewsExtensionByKey(string key)
        {
            NewsExtensionEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.GetNewsExtensionByKey(key);
            }
            return returnValue;
        }

        public static List<NewsExtensionEntity> GetNewsExtensionByListKey(List<string> listKey)
        {
            List<NewsExtensionEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.GetNewsExtensionByListKey(listKey);
            }
            return returnValue;
        }

        public static bool InitAllNewsExtension(List<NewsExtensionEntity> list)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsExtensionDal.InitAllNewsExtension(list);
            }
            return returnValue;
        }
    }
}
