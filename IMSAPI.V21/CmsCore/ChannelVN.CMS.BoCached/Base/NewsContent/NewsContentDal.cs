﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsContent
{
    public class NewsContentDal: NewsContentDalBase
    {
        internal NewsContentDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
