﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsWarning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsContent
{
    public class NewsContentDalFactory
    {
        public static bool AddNewsContent(NewsContentEntity newsContent)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.AddNewsContent(newsContent);
            }
            return returnValue;
        }
        public static bool AddNewsContentByNewsId(long newsId,NewsContentEntity newsContent)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.AddNewsContentByNewsId(newsId, newsContent);
            }
            return returnValue;
        }
        public static NewsContentEntity GetNewsContentByNewsId(long newsId)
        {
            NewsContentEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.GetNewsContentByNewsId(newsId);
            }
            return returnValue;
        }

        #region ContentWarning
        public static bool UpdateContentWarning(ContentWarningEntity content)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.UpdateContentWarning(content);
            }
            return returnValue;
        }
        public static bool AddContentWarningLog(long newsId, string account)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.AddContentWarningLog(newsId, account);
            }
            return returnValue;
        }
        public static List<ContentWarningEntity> ListContentWarningByListNewsId(List<string> newsIds)
        {
            List<ContentWarningEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsContentDal.ListContentWarningByListNewsId(newsIds);
            }
            return returnValue;
        }
        #endregion
    }
}
