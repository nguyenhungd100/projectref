﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.NewsWarning;

namespace ChannelVN.CMS.BoCached.Base.NewsContent
{
    public abstract class NewsContentDalBase
    {
        public bool AddNewsContent(NewsContentEntity newsContent)
        {
            try
            {
                if (null == newsContent)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsContentEntity>(newsContent.NewsId.ToString(), newsContent);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsContentByNewsId(long newsId, NewsContentEntity newsContent)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsContentWithTemplate = newsContent.Body;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NewsContentEntity GetNewsContentByNewsId(long newsId)
        {
            var result = new NewsContentEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result.NewsId = newsId;
                    result.Body = news.NewsContentWithTemplate;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region ContentWarning
        public bool UpdateContentWarning(ContentWarningEntity content)
        {
            try
            {
                if (null == content)
                {
                    return false;
                }                                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        var data = _db.GetFromHash<ContentWarningEntity>(content.NewsId.ToString());
                        if (data == null)
                        {
                            data = content;
                        }
                        data.Level = content.Level;
                        data.LimitedCategory = content.LimitedCategory;
                        data.ListEnity = content.ListEnity;
                        data.ListKeyword = content.ListKeyword;
                        data.ListTopic = content.ListTopic;
                        data.Status = content.Status;

                        _db.AddInHash(data.NewsId.ToString(), data);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddContentWarningLog(long newsId, string account)
        {
            try
            {
                if (newsId<=0)
                {
                    return false;
                }               
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                       var data = _db.GetFromHash<ContentWarningEntity>(newsId.ToString());
                        if (data != null)
                        {
                            var listLog = data.ConfirmLog;
                            if(listLog==null)
                            {
                                listLog=new List<ContentWarningLogEntity>();
                            }
                            var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                            if (news != null)
                            {
                                listLog.Add(new ContentWarningLogEntity
                                {
                                    NewsId = newsId,
                                    Account = account,
                                    Status = news.NewsInfo.Status,
                                    CreatedDate = DateTime.Now
                                });
                            }else
                            {
                                listLog.Add(new ContentWarningLogEntity
                                {
                                    NewsId = newsId,
                                    Account = account,
                                    Status = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            data.ConfirmLog = listLog;

                            _db.AddInHash(newsId.ToString(), data);
                        } 
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ContentWarningEntity> ListContentWarningByListNewsId(List<string> newsIds)
        {
            try
            {                                
                return _db.GetsFromHash<ContentWarningEntity>(newsIds);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsContentDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
