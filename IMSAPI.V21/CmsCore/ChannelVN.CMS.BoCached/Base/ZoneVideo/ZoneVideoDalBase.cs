﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.BoCached.Entity.Video;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BoCached.Base.ZoneVideo
{
    public abstract class ZoneVideoDalBase
    {
        public bool AddZoneVideo(ZoneVideoEntity data)
        {
            try
            {
                _db.RemoveFromHash<ZoneVideoEntity>(data.Id.ToString());

                return _db.AddInHash(data.Id.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public ZoneVideoEntity GetZoneVideoById(int zoneVideoId)
        {
            try
            {
                var result = _db.GetFromHash<ZoneVideoEntity>(zoneVideoId.ToString());
                return result;
            }
            catch
            {
                return null;
            }
        }

        public List<ZoneVideoEntity> GetZoneVideoActivedByUsernameAndPermissionIds(string username, string listPermissionId)
        {
            try
            {
                var list = new List<ZoneVideoEntity>();

                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);
                var user = _db.GetFromHash<UserCachedEntity>(userId.ToString());
                if (user != null)
                {
                    if (user.User.IsFullZone && user.User.IsFullPermission)
                    {
                        var result = _db.GetsFromHash<ZoneVideoEntity>();
                        if (result != null && result.Count > 0)
                        {
                            return result;
                        }
                    }
                    else if (user.User.IsFullZone && !user.User.IsFullPermission)
                    {
                        var listPermistion = listPermissionId.Split(';').ToList();
                        if (user.PermissionList.Where(s => listPermistion.Any(a => a.Equals(s.PermissionId.ToString()))).Count() > 0)
                        {
                            var result = _db.GetsFromHash<ZoneVideoEntity>();
                            if (result != null && result.Count > 0)
                            {
                                return result;
                            }
                        }
                    }
                    else if (!user.User.IsFullZone && user.User.IsFullPermission)
                    {
                        var listZone = user.PermissionList.Select(s => s.ZoneId.ToString()).Distinct().ToList();
                        if (listZone != null && listZone.Count() > 0)
                        {
                            var result = new List<ZoneVideoEntity>();
                            foreach (var item in listZone)
                            {
                                result.AddRange(_db.GetsFromHash<ZoneVideoEntity>().Where(w => w.Status == 1 && w.ParentId.ToString().Equals(item)).ToList());
                            }

                            result.AddRange(_db.GetsFromHash<ZoneVideoEntity>(listZone).Where(w => w.Status == 1));
                            if (result != null && result.Count > 0)
                            {
                                return result;
                            }
                        }
                    }
                    else
                    {
                        var listZone = user.PermissionList.Select(s => s.ZoneId.ToString()).Distinct().ToList();
                        if (listZone != null && listZone.Count() > 0)
                        {
                            var result = new List<ZoneVideoEntity>();
                            foreach (var item in listZone)
                            {
                                result.AddRange(_db.GetsFromHash<ZoneVideoEntity>().Where(w => w.Status == 1 && w.ParentId.ToString().Equals(item)).ToList());
                            }

                            result.AddRange(_db.GetsFromHash<ZoneVideoEntity>(listZone).Where(w => w.Status == 1));
                            if (result != null && result.Count > 0)
                            {
                                return result;
                            }
                        }
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<ZoneVideoEntity> GetZoneVideoByVideoId(int videoId)
        {
            try
            {
                var data = new List<ZoneVideoEntity>();
                
                var oVideo = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (oVideo != null)
                {
                    data = oVideo.ZoneVideoList;
                }   
                
                return data;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneVideoEntity> GetZoneVideoByPlayListId(int playListId)
        {
            try
            {
                var data = new List<ZoneVideoEntity>();

                var playListCached = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (playListCached != null)
                {
                    data = playListCached.ZoneVideoList;
                }

                return data;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneVideoEntity> GetZoneVideoByChannelId(int channelId)
        {
            try
            {
                var data = new List<ZoneVideoEntity>();

                var videoChannelCached = _db.GetFromHash<VideoChannelCachedEntity>(channelId.ToString());
                if (videoChannelCached != null)
                {
                    data = videoChannelCached.ZoneVideoList;
                }

                return data;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneVideoEntity> GetZoneVideoByParentId(int parentId, int status)
        {
            try
            {
                var list = new List<ZoneVideoEntity>();

                var result = _db.GetsFromHash<ZoneVideoEntity>();
                if (result != null && result.Count > 0)
                {
                    if (parentId == -1 && status == 0)
                    {
                        list = result;
                    }
                    else if (parentId == -1 && status > 0)
                    {
                        list = result.Where(w => w.Status == status).ToList();
                    }
                    else if (parentId >= 0 && status == 0)
                    {
                        list = result.Where(w =>w.ParentId == parentId).ToList();
                    }
                    else {
                        list = result.Where(w => w.Status == status && w.ParentId == parentId).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool InitAllZoneVideo(List<ZoneVideoEntity> list)
        {
            try
            {
                var data = new Dictionary<string, ZoneVideoEntity>();
                foreach (var entry in list)
                {
                    data[entry.Id.ToString()] = entry;
                }     

                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected ZoneVideoDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
