﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.ZoneVideo
{
    public class ZoneVideoDalFactory
    {
        public static ZoneVideoEntity GetZoneVideoById(int zoneId)
        {
            ZoneVideoEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoById(zoneId);
            }
            return returnValue;
        }
        public static List<ZoneVideoEntity> GetZoneVideoActivedByUsernameAndPermissionIds(string username, string listPermissionId)
        {
            List<ZoneVideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoActivedByUsernameAndPermissionIds(username, listPermissionId);
            }
            return returnValue;
        }
        public static bool AddZoneVideo(ZoneVideoEntity data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.AddZoneVideo(data);
            }
            return returnValue;
        }
        public static List<ZoneVideoEntity> GetZoneVideoByVideoId(int videoId)
        {
            List<ZoneVideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoByVideoId(videoId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetZoneVideoByPlayListId(int playListId)
        {
            List<ZoneVideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoByPlayListId(playListId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetZoneVideoByChannelId(int channelId)
        {
            List<ZoneVideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoByChannelId(channelId);
            }
            return returnValue;
        }

        public static List<ZoneVideoEntity> GetZoneVideoByParentId(int parentId, int status)
        {
            List<ZoneVideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.GetZoneVideoByParentId(parentId, status);
            }
            return returnValue;
        }

        public static bool InitAllZoneVideo(List<ZoneVideoEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneVideoDal.InitAllZoneVideo(data);
            }
            return returnValue;
        }
    }
}
