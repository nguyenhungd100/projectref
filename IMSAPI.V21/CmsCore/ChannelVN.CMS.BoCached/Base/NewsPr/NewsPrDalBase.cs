﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsPr
{
    public abstract class NewsPrDalBase
    {
        public bool AddNewsPr(NewsPrEntity newsPr)
        {
            try
            {
                if (null == newsPr)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsPrEntity>(newsPr.Id.ToString(), newsPr);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsPrByNewsId(long newsId, NewsPrEntity newsPr)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsPr = newsPr;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            var result = new NewsPrEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.NewsPr;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsPrDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
