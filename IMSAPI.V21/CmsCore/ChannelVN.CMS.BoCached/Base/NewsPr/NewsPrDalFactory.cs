﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsPr
{
    public class NewsPrDalFactory
    {
        public static bool AddNewsPr(NewsPrEntity newsPr)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsPrDal.AddNewsPr(newsPr);
            }
            return returnValue;
        }
        public static bool AddNewsPrByNewsId(long newsId,NewsPrEntity newsPr)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsPrDal.AddNewsPrByNewsId(newsId, newsPr);
            }
            return returnValue;
        }
        public static NewsPrEntity GetNewsPrByNewsId(long newsId)
        {
            NewsPrEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsPrDal.GetNewsPrByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
