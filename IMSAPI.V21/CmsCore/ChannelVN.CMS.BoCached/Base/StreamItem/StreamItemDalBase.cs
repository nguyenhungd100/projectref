﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using ChannelVN.CMS.Entity.Base.StreamItem;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoCached.Base.StreamItem
{
    public abstract class StreamItemDalBase
    {
        #region StreamItem

        public bool AddStreamItem(StreamItemEntity streamItem)
        {
            try
            {
                if (null == streamItem)
                {
                    return false;
                }                                
  
                return _db.AddInHash(streamItem.Id.ToString(), streamItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public bool DeleteStreamItemById(long id)
        {
            try
            {
                var streamItem = _db.GetFromHash<StreamItemEntity>(id.ToString());
                if (null == streamItem)
                {
                    return false;
                }
                
                return _db.RemoveFromHash<StreamItemEntity>(streamItem.Id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public StreamItemEntity GetStreamItemById(long id)
        {
            try
            {                
                return _db.GetFromHash<StreamItemEntity>(id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InitAllStreamItem(List<StreamItemEntity> list)
        {
            try
            {
                var data = new Dictionary<string, StreamItemEntity>();
                foreach (var entry in list)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region StreamItemMobile

        public bool AddStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            try
            {
                if (null == streamItem)
                {
                    return false;
                }

                return _db.AddInHash(streamItem.Id.ToString(), streamItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteStreamItemMobileById(long id)
        {
            try
            {
                var streamItem = _db.GetFromHash<StreamItemMobileEntity>(id.ToString());
                if (null == streamItem)
                {
                    return false;
                }

                return _db.RemoveFromHash<StreamItemMobileEntity>(streamItem.Id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected StreamItemDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
