﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.StreamItem;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoCached.Base.StreamItem
{
    public class StreamItemDalFactory
    {
        #region StreamItem
        public static bool AddStreamItem(StreamItemEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.AddStreamItem(streamItem);
            }
            return returnValue;
        }
        
        public static bool DeleteStreamItemById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.DeleteStreamItemById(id);
            }
            return returnValue;
        }

        public static StreamItemEntity GetStreamItemById(long id)
        {
            StreamItemEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.GetStreamItemById(id);
            }
            return returnValue;
        }

        public static bool InitAllStreamItem(List<StreamItemEntity> list)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.InitAllStreamItem(list);
            }
            return returnValue;
        }

        #endregion

        #region StreamItemMobile
        public static bool AddStreamItemMobile(StreamItemMobileEntity streamItem)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.AddStreamItemMobile(streamItem);
            }
            return returnValue;
        }

        public static bool DeleteStreamItemMobileById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StreamItemDal.DeleteStreamItemMobileById(id);
            }
            return returnValue;
        }

        #endregion
    }
}
