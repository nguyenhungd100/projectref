﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public class VideoYoutubeDalFactory
    {        
        public static List<VideoFromYoutubeDetailEntity> GetVideoYoutubeByListId(List<string> listId)
        {
            List<VideoFromYoutubeDetailEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoYoutubeDal.GetVideoYoutubeByListId(listId);
            }
            return returnValue;
        }        
        
    }
}
