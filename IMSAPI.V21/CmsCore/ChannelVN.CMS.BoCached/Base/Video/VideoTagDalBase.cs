﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.BoCached.Entity.Video;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public abstract class VideoTagDalBase
    {
        public bool AddVideoTag(VideoTagEntity tag)
        {
            try
            {
                if (null == tag)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<VideoTagEntity>(tag.Id.ToString(), tag);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddVideoTagByVideoId(int videoId, List<VideoTagEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.TagList = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<VideoCachedEntity>(videoId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<VideoTagEntity> GetVideoTagByVideoId(int videoId)
        {
            var result = new List<VideoTagEntity>();
            try
            {
                var video = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (video != null)
                {
                    result = video.TagList;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public List<VideoTagEntity> GetVideoTagByIds(string tagIds)
        {
            try
            {
                if (string.IsNullOrEmpty(tagIds))
                {
                    return null;
                }
                var listTagId = tagIds.Split(';').ToList();
                
                return _db.GetsFromHash<VideoTagEntity>(listTagId);
            }
            catch
            {
                return null;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected VideoTagDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
