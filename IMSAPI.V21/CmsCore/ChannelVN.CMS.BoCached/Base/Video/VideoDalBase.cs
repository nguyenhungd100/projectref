﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Video;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public abstract class VideoDalBase
    {
        public bool AddVideo(VideoEntity video)
        {
            try
            {
                if (null == video)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<VideoCachedEntity>(video.Id.ToString());
                if (null == newsCached)
                {
                    newsCached = new VideoCachedEntity
                    {
                        VideoInfo = video,
                        TagList = null,
                        ZoneVideoList=null,
                        Playlist=null,
                        VideoRelation=null,
                        VideoChannel=null,
                    };
                }
                else
                {
                    newsCached.VideoInfo = video;
                    newsCached.TagList = null;
                    newsCached.ZoneVideoList = null;
                    newsCached.Playlist = null;
                    newsCached.VideoRelation = null;
                    newsCached.VideoChannel = null;
                }
                _db.RemoveFromHash<VideoCachedEntity>(video.Id.ToString());

                return _db.AddInHash(video.Id.ToString(), newsCached);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteVideo(int videoId)
        {
            try
            {
                return _db.RemoveFromHash<VideoCachedEntity>(videoId.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateOnlyVideoTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<VideoCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.VideoInfo != null)
                {
                    newsCached.VideoInfo.Name = newsTitle;
                    newsCached.VideoInfo.Url = newsUrl;
                    newsCached.VideoInfo.LastModifiedBy = accountName;
                    newsCached.VideoInfo.LastModifiedDate = DateTime.Now;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoCachedEntity>(newsId.ToString(), newsCached);
                    });
                }

                var newsPublish = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Name = newsTitle;
                    newsPublish.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPublish);
                    });
                }

                var newsPosition = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Name = newsTitle;
                    newsPosition.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPosition);
                    });
                }

                var newsContent = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Name = newsTitle;
                    newsContent.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsContent);
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateVideoAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<VideoCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.VideoInfo != null)
                {
                    newsCached.VideoInfo.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoCachedEntity>(newsId.ToString(), newsCached);
                    });
                }

                var newsPublish = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPublish);
                    });
                }

                var newsContent = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsContent);
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<VideoCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.VideoInfo != null)
                {
                    newsCached.VideoInfo.Name = newsTitle;
                    newsCached.VideoInfo.Avatar = avatar;
                    newsCached.VideoInfo.LastModifiedBy = accountName;
                    newsCached.VideoInfo.LastModifiedDate = DateTime.Now;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoCachedEntity>(newsId.ToString(), newsCached);
                    });
                }

                var newsPublish = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Name = newsTitle;
                    newsPublish.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPublish);
                    });
                }

                var newsPosition = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Name = newsTitle;
                    newsPosition.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPosition);
                    });
                }

                var newsContent = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Name = newsTitle;
                    newsContent.Avatar = avatar;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsContent);
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<VideoCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.VideoInfo != null)
                {
                    newsCached.VideoInfo.Name = newsTitle;
                    newsCached.VideoInfo.Url = newsUrl;
                    newsCached.VideoInfo.LastModifiedBy = accountName;
                    newsCached.VideoInfo.LastModifiedDate = DateTime.Now;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoCachedEntity>(newsId.ToString(), newsCached);
                    });
                }

                var newsPublish = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Name = newsTitle;
                    newsPublish.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPublish);
                    });
                }

                var newsPosition = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Name = newsTitle;
                    newsPosition.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsPosition);
                    });
                }

                var newsContent = _db.GetFromHash<VideoEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Name = newsTitle;
                    newsContent.Url = newsUrl;

                    Task.Run(() =>
                    {
                        _db.AddInHash<VideoEntity>(newsId.ToString(), newsContent);
                    });
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddListZoneRelationByVideoId(int videoId, List<ZoneVideoEntity> listZoneRelation)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.ZoneVideoList = listZoneRelation;
                }

                return _db.AddInHash(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(newsId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    //data.VideoInfo.Status = (int)VideoStatus.MovedToTrash;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<VideoCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public List<VideoTagEntity> GetVideoTagByVideoId(int videoId)
        {
            try
            {
                var list = new List<VideoTagEntity>();
                var replies = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != replies)
                {
                    list = replies.TagList;
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
        public bool AddVideoTagByVideoId(int videoId, List<VideoTagEntity> tagList)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.TagList = tagList;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public List<PlaylistEntity> GetPlayListByVideoId(int videoId)
        {
            try
            {
                var list = new List<PlaylistEntity>();
                var replies = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != replies)
                {
                    list = replies.Playlist;
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
        public bool AddPlayListByVideoId(int videoId, List<PlaylistEntity> playList)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.Playlist = playList;
                }
                return _db.AddInHash(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool DeletePlayListInVideo(List<string> videoIds)
        {
            try
            {
                bool result = false;
                var data = _db.GetsFromHash<VideoCachedEntity>(videoIds);
                if (null != data && data.Count>0)
                {                    
                    foreach (var item in data)
                    {
                        item.Playlist = null;
                        result=_db.AddInHash(item.VideoInfo.Id.ToString(), item);
                    }
                }
                return result;
            }
            catch
            {
                return false;
            }
        }

        public bool CheckCloneVideoVtv(long id)
        {
            try
            {
                bool result = false;
                var data = _db.GetsFromHash<VideoCachedEntity>(id.ToString());
                if (null != data && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        item.Playlist = null;
                        result = _db.AddInHash(item.VideoInfo.Id.ToString(), item);
                    }
                }
                return result;
            }
            catch
            {
                return false;
            }
        }

        public List<VideoExtensionEntity> GetVideoExtensionByVideoId(int videoId)
        {
            try
            {
                var list = new List<VideoExtensionEntity>();
                var replies = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != replies)
                {
                    list = replies.VideoExtension;
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
        public bool AddVideoExtensionByVideoId(int videoId, List<VideoExtensionEntity> videoEx)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.VideoExtension = videoEx;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public VideoEntity GetVideoById(long videoId, ref string zoneName)
        {
            try
            {
                var replies = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());

                if (null != replies && replies.VideoInfo != null)
                {
                    zoneName = replies.VideoInfo.ZoneName;
                    return replies.VideoInfo;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public List<VideoEntity> GetVideoByListId(List<string> listId)
        {
            try
            {
                var list = new List<VideoEntity>();
                var replies = _db.GetsFromHash<VideoCachedEntity>(listId);

                if (null != replies && replies.Count() > 0)
                {
                    list.AddRange(replies.Select(s => s.VideoInfo));
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool Unpublish(int videoId)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Status = (int)EnumVideoStatus.Unpublished;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool Publish(int videoId, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Status = (int)EnumVideoStatus.Published;
                    data.VideoInfo.PublishBy = username;
                    data.VideoInfo.PublishDate = DateTime.Now;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool Send(int videoId, int status, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Status = status;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;

                    if (status == (int)EnumVideoStatus.WaitForPublish)
                        data.VideoInfo.EditedBy = username;

                    _db.RemoveFromHash<VideoCachedEntity>(data.VideoInfo.Id.ToString());

                    return _db.AddInHash(videoId.ToString(), data);
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool Return(int videoId, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Status = (int)EnumVideoStatus.Return;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateMode(int videoId, int mode, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Mode = mode;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;
                }
                return _db.AddInHash<VideoCachedEntity>(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool UpdateStatusVideoByHashId(int videoId,string keyVideo, string username)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data && data.VideoInfo != null)
                {
                    data.VideoInfo.Status = (int)EnumVideoStatus.DoneVideo;
                    data.VideoInfo.KeyVideo = keyVideo;
                    data.VideoInfo.LastModifiedBy = username;
                    data.VideoInfo.LastModifiedDate = DateTime.Now;
                }
                return _db.AddInHash(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool InitAllVideo(List<VideoEntity> listVideo)
        {
            try
            {
                var data = new Dictionary<string, VideoCachedEntity>();
                foreach (var entry in listVideo)
                {
                    data[entry.Id.ToString()] = new VideoCachedEntity() { VideoInfo = entry };
                }
                return _db.AddInHash<VideoCachedEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteDataSearchVideoInPlaylist(string hashName)
        {
            return _db.Remove(hashName);
        }

        public string GetDataSearchVideoInPlaylist(string hashName, string key)
        {
            return _db.GetsFromHashWithCustomKey(hashName, key);
        }

        public bool AddDataSearchVideoInPlaylist(string hashName, string key, string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return false;
                }
                var expireTime = TimeSpan.FromDays(1);
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHashWithCustomName(hashName, key, value);
                        _db.SetExpireKey(hashName, expireTime);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected VideoDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
