﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public class VideoTagDalFactory
    {
        public static bool AddVideoTag(VideoTagEntity tag)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoTagDal.AddVideoTag(tag);
            }
            return returnValue;
        }
        public static bool AddVideoTagByVideoId(int videoId, List<VideoTagEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoTagDal.AddVideoTagByVideoId(videoId,list);
            }
            return returnValue;
        }
        public static List<VideoTagEntity> GetVideoTagByVideoId(int videoId)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoTagDal.GetVideoTagByVideoId(videoId);
            }
            return returnValue;
        }
        public static List<VideoTagEntity> GetVideoTagByIds(string tagIds)
        {
            List<VideoTagEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoTagDal.GetVideoTagByIds(tagIds);
            }
            return returnValue;
        }
    }
}
