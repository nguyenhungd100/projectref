﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public class VideoDalFactory
    {
        public static bool AddVideo(VideoEntity video)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddVideo(video);
            }
            return returnValue;
        }
        public static bool DeleteVideo(int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.DeleteVideo(videoId);
            }
            return returnValue;
        }
        public static bool UpdateOnlyVideoTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateOnlyVideoTitle(newsId, newsTitle, newsUrl, accountName);
            }
            return returnValue;
        }
        public static bool UpdateVideoAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateVideoAvatar(newsId, avatar, avatar2, avatar3, avatar4, avatar5, avatarCustom);
            }
            return returnValue;
        }

        public static bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName);
            }
            return returnValue;
        }
        public static bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName);
            }
            return returnValue;
        }

        public static bool AddListZoneRelationByVideoId(int videoId, List<ZoneVideoEntity> listZoneRelation)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddListZoneRelationByVideoId(videoId, listZoneRelation);
            }
            return returnValue;
        }

        public static bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.ChangeStatusToMovedToTrash(newsId, username);
            }
            return returnValue;
        }

        public static List<VideoTagEntity> GetVideoTagByVideoId(int videoId)
        {
            List<VideoTagEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.GetVideoTagByVideoId(videoId);
            }
            return returnValue;
        }
        public static bool AddVideoTagByVideoId(int videoId, List<VideoTagEntity> tagList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddVideoTagByVideoId(videoId, tagList);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetPlayListByVideoId(int videoId)
        {
            List<PlaylistEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.GetPlayListByVideoId(videoId);
            }
            return returnValue;
        }
        public static bool AddPlayListByVideoId(int videoId, List<PlaylistEntity> playList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddPlayListByVideoId(videoId, playList);
            }
            return returnValue;
        }

        public static List<VideoExtensionEntity> GetVideoExtensionByVideoId(int videoId)
        {
            List<VideoExtensionEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.GetVideoExtensionByVideoId(videoId);
            }
            return returnValue;
        }
        public static bool AddVideoExtensionByVideoId(int videoId, List<VideoExtensionEntity> videoEx)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddVideoExtensionByVideoId(videoId, videoEx);
            }
            return returnValue;
        }

        public static VideoEntity GetVideoById(long newsId, ref string zoneName)
        {
            VideoEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.GetVideoById(newsId, ref zoneName);
            }
            return returnValue;
        }

        public static List<VideoEntity> GetVideoByListId(List<string> listId)
        {
            List<VideoEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.GetVideoByListId(listId);
            }
            return returnValue;
        }

        public static bool DeleteDataSearchVideoInPlaylist(string hashName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.DeleteDataSearchVideoInPlaylist(hashName);
            }
            return returnValue;
        }

        public static string GetDataSearchVideoInPlaylist(string hashName, string key)
        {
            using (var db = new CmsMainCachedDb())
            {
                return db.VideoDal.GetDataSearchVideoInPlaylist(hashName, key);
            }
        }

        public static bool AddDataSearchVideoInPlaylist(string hashName, string key, string value)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.AddDataSearchVideoInPlaylist(hashName, key, value);
            }
            return returnValue;
        }

        public static bool Unpublish(int videoId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.Unpublish(videoId);
            }
            return returnValue;
        }

        public static bool Publish(int videoId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.Publish(videoId, username);
            }
            return returnValue;
        }

        public static bool Send(int videoId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.Send(videoId, status, username);
            }
            return returnValue;
        }

        public static bool Return(int videoId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.Return(videoId, username);
            }
            return returnValue;
        }

        public static bool UpdateMode(int videoId, int mode, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateMode(videoId, mode, username);
            }
            return returnValue;
        }
        public static bool UpdateStatusVideoByHashId(int videoId, string keyVideo, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.UpdateStatusVideoByHashId(videoId, keyVideo, username);
            }
            return returnValue;
        }

        public static bool DeletePlayListInVideo(List<string> videoIds)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.DeletePlayListInVideo(videoIds);
            }
            return returnValue;
        }

        public static bool CheckCloneVideoVtv(long id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.CheckCloneVideoVtv(id);
            }
            return returnValue;
        }

        public static bool InitAllVideo(List<VideoEntity> listVideo)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoDal.InitAllVideo(listVideo);
            }
            return returnValue;
        }

    }
}
