﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Video;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Common;

namespace ChannelVN.CMS.BoCached.Base.Video
{
    public abstract class VideoYoutubeDalBase
    {        
        public List<VideoFromYoutubeDetailEntity> GetVideoYoutubeByListId(List<string> listId)
        {
            try
            {
                var list = new List<VideoFromYoutubeDetailEntity>();
                var replies = _db.GetsFromHash<VideoCachedEntity>(listId);

                if (null != replies && replies.Count() > 0)
                {
                    list.AddRange(replies.Select(s => new VideoFromYoutubeDetailEntity {
                        Id= s.VideoYoutube.Id,
                        VideoId=s.VideoYoutube.Id,                        
                        SourceVideoUrl=s.VideoYoutube.SourceVideoUrl,
                        AccountName=s.VideoYoutube.AccountName,
                        FilePath=s.VideoYoutube.FilePath,
                        CompletedDate=s.VideoYoutube.CompletedDate,
                        CreatedDate=s.VideoYoutube.CreatedDate,
                        Status=s.VideoYoutube.Status,
                        Avatar=s.VideoInfo.Avatar,
                        Name=s.VideoInfo.Name,
                        DistributionDate=s.VideoInfo.DistributionDate
                    }));
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected VideoYoutubeDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
