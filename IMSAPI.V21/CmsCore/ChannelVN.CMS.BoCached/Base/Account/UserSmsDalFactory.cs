﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Account
{
    public class UserSmsDalFactory
    {
        public static bool AddnewSmsCode(UserSmsEntity userSms)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserSmsDal.AddnewSmsCode(userSms);
            }
            return returnValue;
        }

        public static UserSmsEntity GetUserSmsByUserId(int userId)
        {
            UserSmsEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserSmsDal.GetUserSmsByUserId(userId);
            }
            return returnValue;
        }

        public static bool RemoveInUsing(int userId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserSmsDal.RemoveInUsing(userId);
            }
            return returnValue;
        }
    }
}
