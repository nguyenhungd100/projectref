﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Account
{
    public abstract class UserSmsDalBase
    {
        public bool AddnewSmsCode(UserSmsEntity userSms)
        {
            try
            {
                return _db.Add<UserSmsEntity>(userSms.UserId.ToString(), userSms, userSms.ExpiredDate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserSmsEntity GetUserSmsByUserId(int userId)
        {
            try
            {
                return _db.Get<UserSmsEntity>(userId.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemoveInUsing(int userId)
        {
            try
            {
                var stringId = userId.ToString();
                var userSms = _db.Get<UserSmsEntity>(stringId);

                if(null != userSms)
                {
                    userSms.InUsing = false;
                }
                return _db.Add<UserSmsEntity>(stringId, userSms);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected UserSmsDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
