﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Account
{
    public abstract class UserDalBase
    {
        public bool AddUser(UserEntity user)
        {
            try
            {
                if(null == user)
                {
                    return false;
                }
                var userCached = _db.GetFromHash<UserCachedEntity>(user.Id.ToString());
                if (userCached == null)
                {
                    userCached = new UserCachedEntity
                    {
                        User = user
                    };
                }
                else
                {
                    userCached.User = user;
                }
                _db.RemoveFromHash<UserCachedEntity>(user.Id.ToString());

                var result=_db.AddInHash(user.Id.ToString(), userCached);
                result=_db.AddInSortedSet<UserEntity>(user.UserName.ToLower(), user.Id);  
                              
                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public UserEntity GetUserByUsername(string username)
        {
            var result = default(UserEntity);
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return result;
                }
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username.ToLower());                

                if(userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                    if (reply != null)
                    {
                        result = reply.User;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
                throw ex;
            }
        }

        public UserCachedEntity GetUserCachedByUsername(string username)
        {
            var result = default(UserCachedEntity);
            try
            {
                if (string.IsNullOrEmpty(username))
                {
                    return result;
                }
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username.ToLower());

                if (userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    result = _db.GetFromHash<UserCachedEntity>(hashId);                    
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserEntity GetUserById(int id)
        {
            try
            {
                var result = default(UserEntity);
                var reply = _db.GetFromHash<UserCachedEntity>(id.ToString());

                if (null != reply)
                {
                    result = reply.User;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteUserById(int id)
        {
            try
            {
                return _db.Remove<UserCachedEntity>(id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemovePermissionListByUserId(int userId)
        {
            try
            {
                var hashId = userId.ToString();
                var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                if(null != reply)
                {
                    reply.PermissionList = new List<UserPermissionEntity>();

                    return _db.AddInHash(hashId, reply);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddPermissionListByUserId(int userId, List<UserPermissionEntity> userPermisstionList)
        {
            try
            {
                var hashId = userId.ToString();
                var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                if(null != reply)
                {
                    reply.PermissionList = userPermisstionList;
                    return _db.AddInHash(hashId, reply);
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<UserEntity> GetUserFullPermisstion()
        {
            try
            {
                var list = new List<UserEntity>();
                var replies = _db.GetsFromHash<UserCachedEntity>();

                if(null != replies && replies.Count()>0)
                {
                    list = replies.Select(s => s.User).ToList();                    
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            try
            {
                var userOpt = new UserOptCachedEntity
                {
                    UserName = username,
                    OtpSecretKey = otpSecretKey
                };

                return _db.AddInHash(username?.ToLower(), userOpt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetOtpSecretKeyByUsername(string username)
        {
            try
            {
                var result = "";                
                var userOpt = _db.GetFromHash<UserOptCachedEntity>(username?.ToLower());

                if(null != userOpt)
                {
                    result = userOpt.OtpSecretKey;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            try
            {
                //lay zoneid by newsid
                var zoneId = 0;
                var oNews = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (oNews != null)
                {
                    zoneId = oNews.NewsInfo.ZoneId;
                }

                if (string.IsNullOrEmpty(userName))
                {
                    return null;
                }                
                //lay userid by current username          
                var userId = _db.GetScoreFromSortedSet<UserEntity>(userName.ToLower());                

                //
                var list = new List<UserEntity>();
                var replies = _db.GetsFromHash<UserCachedEntity>();

                if (null != replies && replies.Count() > 0)
                {
                    foreach (var item in replies)
                    {
                        if (item.User.Id != userId && 
                            ((item.User.IsFullPermission==true && item.User.IsFullZone == true) ||
                                (item.User.IsFullPermission == true && (item.PermissionList.Count(s => s.ZoneId == zoneId) > 0)) ||
                                (item.User.IsFullZone == true && (item.PermissionList.Count(s => s.PermissionId == permissionId) > 0)) ||
                                ((item.PermissionList.Count(s => s.PermissionId == permissionId) > 0) && (item.PermissionList.Count(s => s.ZoneId == zoneId) > 0))
                            ))
                        {
                            list.Add(item.User);
                        }                        
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            try
            {                                
                var list = new List<UserEntity>();
                var replies = _db.GetsFromHash<UserCachedEntity>();

                if (null != replies && replies.Count() > 0)
                {
                    foreach (var item in replies)
                    {
                        if ((item.PermissionList.Count(s => s.PermissionId == permissionId) > 0) && (item.PermissionList.Count(s => zoneIds.Contains(s.ZoneId.ToString())) > 0))
                        {
                            list.Add(item.User);
                        }
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<UserEntity> GetUserByListId(List<string> userIds)
        {
            try
            {
                var list = new List<UserEntity>();
                var replies = _db.GetsFromHash<UserCachedEntity>(userIds);

                if (null != replies && replies.Count() > 0)
                {
                    foreach (var item in replies)
                    {
                        if (item != null)
                        {
                            list.Add(item.User);
                        }
                    }
                }
                return list;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public bool InitUsersByStatus(List<UserEntity> listUsers)
        {
            try
            {                
                var data = new Dictionary<string, UserCachedEntity>();
                foreach (var entry in listUsers)
                {
                    data[entry.Id.ToString()] = new UserCachedEntity() { User = entry };
                    var task=Task.Run(() =>
                    {                        
                        _db.AddInSortedSet<UserEntity>(entry.UserName.ToLower(), entry.Id);
                        _db.Remove("grouppermissiondetailcachedentity");
                    });
                    task.Wait();
                }
                
                return _db.AddInHash<UserCachedEntity>(data);        
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteIndexRedis(string indexName)
        {
            try
            {
                return _db.Remove(indexName);
            }
            catch
            {
                return false;
            }
        }        

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected UserDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
