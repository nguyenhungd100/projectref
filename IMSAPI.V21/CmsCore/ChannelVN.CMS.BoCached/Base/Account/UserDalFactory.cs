﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Account
{
    public class UserDalFactory
    {
        public static bool AddUser(UserEntity user)
        {
            bool returnValue;
            using(var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.AddUser(user);
            }
            return returnValue;
        }

        public static UserEntity GetUserByUsername(string username)
        {
            UserEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserByUsername(username);
            }
            return returnValue;
        }

        public static UserCachedEntity GetUserCachedByUsername(string username)
        {
            UserCachedEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserCachedByUsername(username);
            }
            return returnValue;
        }

        public static UserEntity GetUserById(int id)
        {
            UserEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserById(id);
            }
            return returnValue;
        }

        public static bool DeleteUserById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.DeleteUserById(id);
            }
            return returnValue;
        }

        public static bool RemovePermissionListByUserId(int userId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.RemovePermissionListByUserId(userId);
            }
            return returnValue;
        }

        public static bool AddPermissionListByUserId(int userId, List<UserPermissionEntity> userPermissionList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.AddPermissionListByUserId(userId, userPermissionList);
            }
            return returnValue;
        }

        public static List<UserEntity> GetUserFullPermisstion()
        {
            List<UserEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserFullPermisstion();
            }
            return returnValue;
        }

        public static bool UpdateOtpSecretKeyForUsername(string username, string otpSecretKey)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.UpdateOtpSecretKeyForUsername(username, otpSecretKey);
            }
            return returnValue;
        }

        public static string GetOtpSecretKeyByUsername(string username)
        {
            string returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetOtpSecretKeyByUsername(username);
            }
            return returnValue;
        }        

        public static List<UserEntity> GetUserWithFullPermissionAndZoneId(int permissionId, long newsId, string userName)
        {
            List<UserEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserWithFullPermissionAndZoneId(permissionId, newsId, userName);
            }
            return returnValue;
        }

        public static List<UserEntity> GetUserByPermissionIdAndZoneList(int permissionId, string zoneIds)
        {
            List<UserEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserByPermissionIdAndZoneList(permissionId, zoneIds);
            }
            return returnValue;
        }

        public static List<UserEntity> GetUserByListId(List<string> userIds)
        {
            List<UserEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.GetUserByListId(userIds);
            }
            return returnValue;
        }

        public static bool InitUsersByStatus(List<UserEntity> listUsers)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.InitUsersByStatus(listUsers);
            }
            return returnValue;
        }

        public static bool DeleteIndexRedis(string indexName)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.UserDal.DeleteIndexRedis(indexName);
            }
            return returnValue;
        }        
    }
}
