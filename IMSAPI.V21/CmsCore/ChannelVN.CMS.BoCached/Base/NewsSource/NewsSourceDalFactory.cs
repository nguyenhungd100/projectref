﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsSource
{
    public class NewsSourceDalFactory
    {
        public static bool AddNewsSource(NewsSourceEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsSourceDal.AddNewsSource(vote);
            }
            return returnValue;
        }
        public static bool AddNewsSourceByNewsId(long newsId,List<NewsBySourceEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsSourceDal.AddNewsSourceByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsBySourceEntity> GetNewsSourceByNewsId(long newsId)
        {
            List<NewsBySourceEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsSourceDal.GetNewsSourceByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
