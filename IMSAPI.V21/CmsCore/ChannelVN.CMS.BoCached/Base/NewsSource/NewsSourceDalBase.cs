﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsSource
{
    public abstract class NewsSourceDalBase
    {
        public bool AddNewsSource(NewsSourceEntity vote)
        {
            try
            {
                if (null == vote)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsSourceEntity>(vote.Id.ToString(), vote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsSourceByNewsId(long newsId, List<NewsBySourceEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsBySource = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<NewsBySourceEntity> GetNewsSourceByNewsId(long newsId)
        {
            var result = new List<NewsBySourceEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.NewsBySource;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsSourceDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
