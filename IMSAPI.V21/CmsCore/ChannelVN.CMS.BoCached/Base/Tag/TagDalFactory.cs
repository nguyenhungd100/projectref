﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Tag
{
    public class TagDalFactory
    {
        public static bool AddTag(TagEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.AddTag(vote);
            }
            return returnValue;
        }
        public static bool UpdateTagHot(long tagId, bool isHotTag)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.UpdateTagHot(tagId, isHotTag);
            }
            return returnValue;
        }
        public static bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.UpdateTagInvisibled(tagId, invisibled);
            }
            return returnValue;
        }
        public static bool UpdateTagNewsCount(long tagId, int count)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.UpdateTagNewsCount(tagId, count);
            }
            return returnValue;
        }
        public static bool DeleteById(long tagId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.DeleteById(tagId);
            }
            return returnValue;
        }
        public static bool AddTagNewsByNewsId(long newsId, List<TagNewsWithTagInfoEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.AddTagNewsByNewsId(newsId,list);
            }
            return returnValue;
        }
        public static List<TagNewsWithTagInfoEntity> GetTagNewsByNewsId(long newsId)
        {
            List<TagNewsWithTagInfoEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.GetTagNewsByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<TagEntity> GetTagByListId(List<string> tagIds)
        {
            List<TagEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.GetTagByListId(tagIds);
            }
            return returnValue;
        }

        public static bool RemoveTagInNews(string deleteNewsId, string addNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.RemoveTagInNews(deleteNewsId, addNewsId);
            }
            return returnValue;
        }

        public static bool InitAllTag(List<TagEntity> listTag)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TagDal.InitAllTag(listTag);
            }
            return returnValue;
        }
    }
}
