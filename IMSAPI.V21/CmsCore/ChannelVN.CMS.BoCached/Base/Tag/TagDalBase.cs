﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.BoCached.Entity.News;
using System.Linq;

namespace ChannelVN.CMS.BoCached.Base.Tag
{
    public abstract class TagDalBase
    {
        public bool AddTag(TagEntity tag)
        {
            try
            {
                if (null == tag)
                {
                    return false;
                }                                
  
                return _db.AddInHash(tag.Id.ToString(), tag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateTagHot(long tagId, bool isHotTag)
        {
            try
            {
                var tag = _db.GetFromHash<TagEntity>(tagId.ToString());
                if (null == tag)
                {
                    return false;
                }
                tag.IsHotTag = isHotTag;

                return _db.AddInHash(tag.Id.ToString(), tag);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateTagInvisibled(long tagId, bool invisibled)
        {
            try
            {
                var tag = _db.GetFromHash<TagEntity>(tagId.ToString());
                if (null == tag)
                {
                    return false;
                }
                tag.Invisibled = invisibled;

                return _db.AddInHash(tag.Id.ToString(), tag);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateTagNewsCount(long tagId, int count)
        {
            try
            {                
                var tag = _db.GetFromHash<TagEntity>(tagId.ToString());
                if (null == tag)
                {
                    return false;
                }
                tag.NewsCount = tag.NewsCount + count;
                if (tag.NewsCount < 0)
                {
                    tag.NewsCount = 0;
                }

                return _db.AddInHash(tag.Id.ToString(), tag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteById(long tagId)
        {
            try
            {                
                return _db.RemoveFromHash<TagEntity>(tagId.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool AddTagNewsByNewsId(long newsId, List<TagNewsWithTagInfoEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.TagInNews = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TagNewsWithTagInfoEntity> GetTagNewsByNewsId(long newsId)
        {
            var result = new List<TagNewsWithTagInfoEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.TagInNews;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<TagEntity> GetTagByListId(List<string> tagIds)
        {
            try
            {                
                return _db.GetsFromHash<TagEntity>(tagIds);
            }
            catch
            {
                return null;
            }
        }
        public bool RemoveTagInNews(string deleteNewsId, string addNewsId)
        {
            try
            {
                var tasks = new List<Task>();
                if (!string.IsNullOrEmpty(deleteNewsId))
                {
                    var arrayNewsId = deleteNewsId.Split(new char[] { ';', ',' }).ToList();
                    foreach(var newsid in arrayNewsId){                        
                        tasks.Add(Task.Run(() =>
                        {
                            var newsCached = _db.GetFromHash<NewsCachedEntity>(newsid);
                            if (newsCached != null)
                            {
                                newsCached.TagInNews = null;
                            }
                            _db.AddInHash(newsid, newsCached);
                        }));
                    }
                    
                }
                if (!string.IsNullOrEmpty(addNewsId))
                {
                    var arrayNewsId = addNewsId.Split(new char[] { ';', ',' }).ToList();
                    foreach (var newsid in arrayNewsId)
                    {
                        tasks.Add(Task.Run(() =>
                        {
                            var newsCached = _db.GetFromHash<NewsCachedEntity>(newsid);
                            if (newsCached != null)
                            {
                                newsCached.TagInNews = null;
                            }
                            _db.AddInHash(newsid, newsCached);
                        }));
                    }

                }

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InitAllTag(List<TagEntity> listTag)
        {
            try
            {
                var data = new Dictionary<string, TagEntity>();
                foreach (var entry in listTag)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<TagEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected TagDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
