﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsAuthor
{
    public abstract class NewsAuthorDalBase
    {
        public bool AddNewsAuthor(NewsAuthorEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsAuthorEntity>(news.Id.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsByAuthorByNewsId(long newsId, List<NewsByAuthorEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsByAuthor = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<NewsByAuthorEntity> GetNewsByAuthorByNewsId(long newsId)
        {
            var result = new List<NewsByAuthorEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.NewsByAuthor;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<NewsAuthorEntity> GetNewsAuthorByUsername(string username)
        {           
            try
            {
                var result = _db.GetsFromHash<NewsAuthorEntity>();
                if (result != null)
                {
                    result = result.Where(w=>w.UserData==username).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public NewsAuthorEntity GetNewsAuthorById(int id)
        {
            var result = new NewsAuthorEntity();
            try
            {
                result = _db.GetFromHash<NewsAuthorEntity>(id.ToString());                
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public bool DeleteNewsAuthorById(int id)
        {
            try
            {
                return _db.RemoveFromHash<NewsAuthorEntity>(id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool InitAllNewsAuthor(List<NewsAuthorEntity> list)
        {
            try
            {
                var data = new Dictionary<string, NewsAuthorEntity>();
                foreach (var entry in list)
                {
                    data[entry.Id.ToString()] = entry;
                }

                return _db.AddInHash<NewsAuthorEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool InsertToNewsByAuthor(NewsByAuthorEntity newsAuthor)
        {
            try
            {
                if (null == newsAuthor)
                {
                    return false;
                }
                var news = _db.GetFromHash<NewsCachedEntity>(newsAuthor.NewsId.ToString()); 
                if(news == null || (news != null && (news.NewsByAuthor==null || (news.NewsByAuthor!=null && news.NewsByAuthor.Count()<=0))))
                {
                    return false;
                }
                var newsAuthorDb = news.NewsByAuthor.Where(w=>w.NewsId==newsAuthor.NewsId && w.AuthorId==newsAuthor.AuthorId).FirstOrDefault();
                if (null == newsAuthorDb)
                {
                    news.NewsByAuthor.Add(newsAuthor);
                    var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsAuthor.NewsId.ToString(), news);
                        })
                    };
                }                
                return false;                                
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<NewsAuthorEntity> GetNewsAuthorByListId(List<string> authorIds)
        {
            try
            {                
                return _db.GetsFromHash<NewsAuthorEntity>(authorIds);
            }
            catch
            {
                return null;
            }
        }        

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsAuthorDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
