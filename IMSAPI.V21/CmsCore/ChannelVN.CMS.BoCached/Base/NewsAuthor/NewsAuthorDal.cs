﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsAuthor
{
    public class NewsAuthorDal: NewsAuthorDalBase
    {
        internal NewsAuthorDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
