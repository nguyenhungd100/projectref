﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsAuthor
{
    public class NewsAuthorDalFactory
    {
        public static bool AddNewsAuthor(NewsAuthorEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.AddNewsAuthor(news);
            }
            return returnValue;
        }
        public static bool AddNewsByAuthorByNewsId(long newsId, List<NewsByAuthorEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.AddNewsByAuthorByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsByAuthorEntity> GetNewsByAuthorByNewsId(long newsId)
        {
            List<NewsByAuthorEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.GetNewsByAuthorByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<NewsAuthorEntity> GetNewsAuthorByUsername(string username)
        {
            List<NewsAuthorEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.GetNewsAuthorByUsername(username);
            }
            return returnValue;
        }
        public static NewsAuthorEntity GetNewsAuthorById(int id)
        {
            NewsAuthorEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.GetNewsAuthorById(id);
            }
            return returnValue;
        }
        public static bool DeleteNewsAuthorById(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.DeleteNewsAuthorById(id);
            }
            return returnValue;
        }
        public static bool InitAllNewsAuthor(List<NewsAuthorEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.InitAllNewsAuthor(data);
            }
            return returnValue;
        }
        public static bool InsertToNewsByAuthor(NewsByAuthorEntity newsAuthor)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.InsertToNewsByAuthor(newsAuthor);
            }
            return returnValue;
        }
        public static List<NewsAuthorEntity> GetNewsAuthorByListId(List<string> authorIds)
        {
            List<NewsAuthorEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsAuthorDal.GetNewsAuthorByListId(authorIds);
            }
            return returnValue;
        }        
    }
}
