﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsChild
{
    public class NewsChildDalFactory
    {
        public static bool AddNewsChild(NewsChildEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildDal.AddNewsChild(news);
            }
            return returnValue;
        }
        public static bool AddNewsChildByNewsId(long newsId, List<NewsChildEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildDal.AddNewsChildByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsChildEntity> GetNewsChildByNewsId(long newsId,bool isGetContent)
        {
            List<NewsChildEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildDal.GetNewsChildByNewsId(newsId, isGetContent);
            }
            return returnValue;
        }
    }
}
