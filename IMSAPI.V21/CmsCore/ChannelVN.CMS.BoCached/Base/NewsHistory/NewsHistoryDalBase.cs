﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BoCached.Base.NewsHistory
{
    public abstract class NewsHistoryDalBase
    {
        public bool AddNewsHistory(NewsHistoryEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsHistoryEntity>(news.NewsId.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NewsHistoryEntity> GetByNewsId(long newsId)
        {
            var data = new List<NewsHistoryEntity>();
            try
            {
                var list = _db.GetsFromHash<NewsHistoryEntity>();
                if (list != null && list.Count() > 0)
                {
                    data = list.Where(w => w.NewsId==newsId).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                return data;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsHistoryDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
