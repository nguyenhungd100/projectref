﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsHistory
{
    public class NewsHistoryDalFactory
    {
        public static bool AddNewsHistory(NewsHistoryEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsHistoryDal.AddNewsHistory(news);
            }
            return returnValue;
        }
        public static List<NewsHistoryEntity> GetByNewsId(long newsId)
        {
            List<NewsHistoryEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsHistoryDal.GetByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
