﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsHistory
{
    public class NewsHistoryDal: NewsHistoryDalBase
    {
        internal NewsHistoryDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
