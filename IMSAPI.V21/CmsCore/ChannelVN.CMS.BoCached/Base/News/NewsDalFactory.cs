﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Entity.Base.News;
using System.Collections.Generic;

namespace ChannelVN.CMS.BoCached.Base.News
{
    public class NewsDalFactory
    {
        #region News
        public static bool AddNews(NewsEntity news, bool isUpdate=false)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNews(news, isUpdate);
            }            
            return returnValue;
        }
        public static bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateDisplayPosition(newsId, displayPosition, accountName);
            }
            return returnValue;
        }
        public static bool UpdatePriority(long newsId, int priority, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdatePriority(newsId, priority, accountName);
            }
            return returnValue;
        }
        public static bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateAdStoreMode(newsId, adStore, adStoreUrl);
            }
            return returnValue;
        }
        public static bool DeleteNewsByNewsIdRedis(string newsId)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.DeleteNewsByNewsIdRedis(newsId);
            }
            return returnValue;
        }
        public static bool DeleteNewsByNewsIdsRedis(List<string> newsIds)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.DeleteNewsByNewsIdsRedis(newsIds);
            }
            return returnValue;
        }
        public static bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateOnlyNewsTitle(newsId, newsTitle, newsUrl, accountName);
            }
            return returnValue;
        }
        public static bool UpdateOnlyNewsBody(long newsId, string body, string accountName, int reviewStatus)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateOnlyNewsBody(newsId, body, accountName, reviewStatus);
            }
            return returnValue;
        }
        public static bool UpdateOnlyNewsBody(long newsId, string body, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateOnlyNewsBody(newsId, body, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateNewsAvatar(newsId, avatar, avatar2, avatar3, avatar4, avatar5, avatarCustom);
            }
            return returnValue;
        }
        public static bool UpdateNewsIsOnHome(long newsId, bool isOnhome, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateNewsIsOnHome(newsId, isOnhome, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsIsFocus(long newsId, bool isFocus, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateNewsIsFocus(newsId, isFocus, accountName);
            }
            return returnValue;
        }
        public static bool UpdateSeoReviewStatus(long newsId, int reviewstatus, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateSeoReviewStatus(newsId, reviewstatus, accountName);
            }
            return returnValue;
        }
        public static bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateDetailSimple(newsId, newsTitle, avatar, avatar2, avatar3, avatar4, avatar5, accountName);
            }
            return returnValue;
        }
        public static bool UpdateDetail(long newsId, string newsTitle, string sapo, string body,string newsUrl, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateDetail(newsId, newsTitle, sapo, body, newsUrl, accountName);
            }
            return returnValue;
        }
        public static bool UpdateNewsByErrorCheck(NewsEntity news, string newsUrl, string tag, string accountName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateNewsByErrorCheck(news, newsUrl, tag, accountName);
            }
            return returnValue;
        }
        public static bool UpdateThreadIdInNewsByNewsId(long threadId, string listDeleteNewsId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateThreadIdInNewsByNewsId(threadId, listDeleteNewsId);
            }
            return returnValue;
        }
        public static bool AddNewsPublish(NewsPublishEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNewsPublish(news);
            }
            return returnValue;
        }
        public static bool AddNewsInZoneByNewsId(long newsId, List<NewsInZoneEntity> listNewsInZone)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNewsInZoneByNewsId(newsId, listNewsInZone);
            }
            return returnValue;
        }
        public static bool AddNewsInZoneByNewsId(long newsId, string listZoneId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNewsInZoneByNewsId(newsId, listZoneId);
            }
            return returnValue;
        }
        #region Change Status
        public static bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToMovedToTrash(newsId, username);
            }
            return returnValue;
        }
        public static bool ChangeStatusToPublished(long id, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string publishedBy, long publishedDate, string publishedContent)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToPublished(id, newsPositionTypeForHome, newsPositionOrderForHome, newsPositionTypeForList, newsPositionOrderForList, publishedBy, publishedDate, publishedContent);
            }
            return returnValue;
        }
        public static bool ChangeStatusToUnPublished(long newsId, string username, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToUnPublished(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForPublish(long newsId, string username, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForPublish(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForEditorialBoard(long newsId, string username, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForEditorialBoard(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToWaitForEdit(long newsId, string username, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToWaitForEdit(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReceivedForEdit(long newsId, string username,string receiver, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReceivedForEdit(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRecievedForPublish(long newsId, string username, string receiver, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRecievedForPublish(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReceivedForEditorialBoard(long newsId, string username, string receiver, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReceivedForEditorialBoard(newsId, username, receiver, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToReporter(long newsId, string username,string note, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToReporter(newsId, username, note, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToEditorialSecretary(long newsId, string approvedBy, string username, string note, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToEditorialSecretary(newsId, approvedBy, username, note, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnedToEditor(long newsId, string receiver, string username, string note, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnedToEditor(newsId, receiver, username, note, status);
            }
            return returnValue;
        }        
        public static bool ChangeStatusForwardToOtherEditor(long newsId, string receiver)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusForwardToOtherEditor(newsId, receiver);
            }
            return returnValue;
        }
        public static bool ChangeStatusForwardToOtherSecretary(long newsId, string receiver)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusForwardToOtherSecretary(newsId, receiver);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRecieveFromReturnStore(long newsId, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRecieveFromReturnStore(newsId, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToRestoreFromTrash(long newsId, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToRestoreFromTrash(newsId, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToCrawlerForEdit(long newsId, string username, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToCrawlerForEdit(newsId, username, status);
            }
            return returnValue;
        }
        public static bool ChangeStatusToReturnToCooperator(long newsId, string username, string note, int status)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ChangeStatusToReturnToCooperator(newsId, username, note, status);
            }
            return returnValue;
        }
        #endregion
        public static List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            List<NewsInZoneEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsInZoneByNewsId(newsId);
            }
            return returnValue;
        }
        public static NewsForValidateEntity GetNewsForValidateById(long id)
        {
            NewsForValidateEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsForValidateById(id);
            }
            return returnValue;
        }
        public static bool AddRelatedNewsByNewsId(long newsId, List<NewsPublishForNewsRelationEntity> listRelatedNews)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddRelatedNewsByNewsId(newsId, listRelatedNews);
            }
            return returnValue;
        }
        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsId(long newsId)
        {
            List<NewsPublishForNewsRelationEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetRelatedNewsByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsIds(string newsIds)
        {
            List<NewsPublishForNewsRelationEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetRelatedNewsByNewsIds(newsIds);
            }
            return returnValue;
        }
        public static NewsEntity GetNewsById(long newsId)
        {
            NewsEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsById(newsId);
            }
            return returnValue;
        }
        public static NewsCheckDuplicate GetTitleNewsByKey(string key)
        {
            NewsCheckDuplicate returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetTitleNewsByKey(key);
            }
            return returnValue;
        }
        public static bool UpdateNotification(NewsNotificationEntity newsnotify)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.UpdateNotification(newsnotify);
            }
            return returnValue;
        }
        public static List<NewsCachedEntity> GetNewsCachedByListId(List<string> newsIds)
        {
            List<NewsCachedEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsCachedByListId(newsIds);
            }
            return returnValue;
        }
        public static List<NewsInListEntity> GetNewsByListId(List<string> newsIds)
        {
            List<NewsInListEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsByListId(newsIds);
            }
            return returnValue;
        }
        public static List<NewsPublishForObjectBoxEntity> GetNewsPublishByListId(List<string> newsIds)
        {
            List<NewsPublishForObjectBoxEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsPublishByListId(newsIds);
            }
            return returnValue;
        }
        public static List<CountStatus> CountStatus(string username)
        {
            List<CountStatus> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.CountStatus(username);
            }
            return returnValue;
        }
        public static bool AddCountStatus(string username, List<CountStatus> listCount)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddCountStatus(username, listCount);
            }
            return returnValue;
        }
        public static bool InsertNewsHistory(NewsHistoryEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.InsertNewsHistory(news);
            }
            return returnValue;
        }
        public static bool InitAllNews(List<NewsEntity> listNews)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.InitAllNews(listNews);
            }
            return returnValue;
        }
        public static bool InitAllNewsCheckDuplicate(List<NewsEntity> listNews)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.InitAllNewsCheckDuplicate(listNews);
            }
            return returnValue;
        }

        //appbot
        public static bool AddNewsParentIdAndOriginalId(NewsEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNewsParentIdAndOriginalId(news);
            }
            return returnValue;
        }

        public static NewsParentIdAndOriginalIdEntity GetNewsParentIdAndOriginalId(string keyId)
        {
            NewsParentIdAndOriginalIdEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsParentIdAndOriginalId(keyId);
            }
            return returnValue;
        }
        #endregion

        #region DictsNewsInUsing
        public static List<NewsInUsingEntity> GetAllNewsInUsing()
        {
            List<NewsInUsingEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetAllNewsInUsing();
            }
            return returnValue;
        }
        public static NewsInUsingEntity GetNewsInUsingById(long id)
        {
            NewsInUsingEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.GetNewsInUsingById(id);
            }
            return returnValue;
        }
        public static bool AddNewsInUsing(NewsInUsingEntity obj)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.AddNewsInUsing(obj);
            }
            return returnValue;
        }
        public static bool RemoveNewsInUsing(long id, string accountName, bool allowUnLock=false)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.RemoveNewsInUsing(id, accountName, allowUnLock);
            }
            return returnValue;
        }

        public static bool ResetNewsInUsing(long id)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsDal.ResetNewsInUsing(id);
            }
            return returnValue;
        }
        #endregion       
    }
}
