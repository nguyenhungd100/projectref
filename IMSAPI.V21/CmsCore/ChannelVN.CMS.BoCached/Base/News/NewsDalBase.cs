﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.BoSearch.Entity;

namespace ChannelVN.CMS.BoCached.Base.News
{
    public abstract class NewsDalBase
    {
        public bool AddNews(NewsEntity news, bool isUpdate=false)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }
                var prevTitle = news.Title;
                var prevStatus = news.Status;
                var newsCached = _db.GetFromHash<NewsCachedEntity>(news.Id.ToString());
                if (null == newsCached)
                {
                    newsCached = new NewsCachedEntity
                    {
                        NewsInfo = news,
                        NewsExtensions=null,
                        NewsRelation =null
                    };
                }
                else
                {
                    prevTitle = newsCached.NewsInfo?.Title;
                    prevStatus = newsCached.NewsInfo == null ? 0 : newsCached.NewsInfo.Status;

                    newsCached.NewsInfo = news;
                    newsCached.NewsExtensions = null;
                    newsCached.NewsRelation = null;
                }
                var zoneids = new List<string>();
                var zoneidsTemp = new List<string>();
                if (newsCached.NewsInfo != null && newsCached.NewsInfo.ZoneId > 0)
                    zoneidsTemp.Add(newsCached.NewsInfo.ZoneId.ToString());
                if (newsCached.NewsInfo != null && !string.IsNullOrEmpty(newsCached.NewsInfo.ListZoneId))
                    zoneidsTemp.AddRange(newsCached.NewsInfo.ListZoneId.Split(new char[] { ';',',' }).Where(s => s != ""));
                zoneids.AddRange(zoneidsTemp.Distinct());
                newsCached.NewsInfo.ListZoneId = string.Join(";", zoneids);

                //_db.RemoveFromHash<NewsCachedEntity>(news.Id.ToString());
                var result = _db.AddInHash(news.Id.ToString(), newsCached);

                if (prevStatus == (int)NewsStatus.Published && prevTitle != news.Title)
                {
                    //xoa key check trung title                
                    _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));
                    //add key check trung title                
                    _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", news.Title, prevStatus)), new NewsCheckDuplicate { Id=news.Id, Title = news.Title, Status = prevStatus });
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public bool UpdateDisplayPosition(long newsId, int displayPosition, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.DisplayPosition = displayPosition;                    
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;
                    
                    _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.DisplayPosition = displayPosition;
                    
                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.DisplayPosition = displayPosition;
                    
                    _db.AddInHash(newsId.ToString(), newsPosition);
                }                

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public bool UpdatePriority(long newsId, int priority, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.Priority = priority;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Priority = priority;

                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Priority = priority;

                    _db.AddInHash(newsId.ToString(), newsPosition);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateAdStoreMode(long newsId, int adStore, string adStoreUrl)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.AdStore = adStore==1?true:false;
                    newsCached.NewsInfo.AdStoreUrl = adStoreUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.AdStore = adStore == 1 ? true : false;
                    newsPublish.AdStoreUrl = adStoreUrl;

                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.AdStore = adStore == 1 ? true : false;
                    newsPosition.AdStoreUrl = adStoreUrl;

                    _db.AddInHash(newsId.ToString(), newsPosition);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteNewsByNewsIdRedis(string newsId)
        {
            try
            {
                return _db.RemoveFromHash<NewsCachedEntity>(newsId);
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteNewsByNewsIdsRedis(List<string> newsIds)
        {
            try
            {
                return _db.RemoveMuiltiFromHash<NewsCachedEntity>(newsIds);
            }
            catch
            {
                return false;
            }
        }
        public bool UpdateOnlyNewsTitle(long newsId, string newsTitle, string newsUrl, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return false;
                }
                var prevTitle = newsTitle;
                var prevStatus = 0;
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    prevTitle = newsCached.NewsInfo?.Title;
                    prevStatus = newsCached.NewsInfo.Status;

                    newsCached.NewsInfo.Title = newsTitle;
                    newsCached.NewsInfo.Url = newsUrl;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    result = _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Title = newsTitle;
                    newsPublish.Url = newsUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Title = newsTitle;
                    newsPosition.Url = newsUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsPosition);
                }

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Title = newsTitle;
                    newsContent.Url = newsUrl;

                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                if (prevStatus == (int)NewsStatus.Published && prevTitle != newsTitle)
                {
                    //xoa key check trung title                
                    _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));
                    //add key check trung title                
                    _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, prevStatus)), new NewsCheckDuplicate { Id = newsId, Title = newsTitle, Status = prevStatus });
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public bool UpdateOnlyNewsBody(long newsId, string body, string accountName, int reviewStatus)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.Body = body;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;
                    newsCached.TagInNews = null;                   

                    _db.AddInHash(newsId.ToString(), newsCached);
                }

                _db.AddInSortedSet("SEOMetaNewsReviewStatusEnum:" + reviewStatus.ToString(), newsId.ToString(), Utility.DateTimeToSpan10(DateTime.Now));

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Body = body;                   
                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public bool UpdateOnlyNewsBody(long newsId, string body, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return result;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.Body = body;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;
                    newsCached.NewsInfo.LastModifiedBy = accountName;

                    result = _db.AddInHash(newsId.ToString(), newsCached);
                }                

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Body = body;
                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }
        public bool UpdateNewsAvatar(long newsId, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string avatarCustom)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.Avatar = avatar;
                    newsCached.NewsInfo.Avatar2 = avatar2;
                    newsCached.NewsInfo.Avatar3 = avatar3;
                    newsCached.NewsInfo.Avatar4 = avatar4;
                    newsCached.NewsInfo.Avatar5 = avatar5;
                    newsCached.NewsInfo.AvatarCustom = avatarCustom;
                    
                    _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Avatar = avatar;
                    newsPublish.Avatar2 = avatar2;
                    newsPublish.Avatar3 = avatar3;
                    newsPublish.Avatar4 = avatar4;
                    newsPublish.Avatar5 = avatar5;
                    newsPublish.AvatarCustom = avatarCustom;
                    
                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Avatar = avatar;
                    newsContent.Avatar2 = avatar2;
                    newsContent.Avatar3 = avatar3;
                    newsContent.Avatar4 = avatar4;
                    newsContent.Avatar5 = avatar5;
                    
                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateNewsIsOnHome(long newsId, bool isOnhome, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.IsOnHome = isOnhome;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    result=_db.AddInHash(newsId.ToString(), newsCached);
                }

                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateNewsIsFocus(long newsId, bool isFocus, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return false;
                }
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    newsCached.NewsInfo.IsFocus = isFocus;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    result = _db.AddInHash(newsId.ToString(), newsCached);
                }

                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateSeoReviewStatus(long newsId, int reviewStatus, string accountName)
        {
            try
            {
                if (newsId <= 0)
                {
                    return false;
                }
                if (reviewStatus == (int)SEOMetaNewsReviewStatusEnum.Reviewed)
                {
                    _db.RemoveFromSortedSet("SEOMetaNewsReviewStatusEnum:" + (int)SEOMetaNewsReviewStatusEnum.ReviewWaiting, newsId.ToString());
                    _db.AddInSortedSet("SEOMetaNewsReviewStatusEnum:" + (int)SEOMetaNewsReviewStatusEnum.Reviewed, newsId.ToString(), Utility.DateTimeToSpan10(DateTime.Now));
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateDetailSimple(long newsId, string newsTitle, string avatar, string avatar2, string avatar3, string avatar4, string avatar5, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return false;
                }
                var prevTitle = newsTitle;
                var prevStatus = 0;
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    prevTitle = newsCached.NewsInfo?.Title;
                    prevStatus = newsCached.NewsInfo.Status;

                    newsCached.NewsInfo.Title = newsTitle;
                    newsCached.NewsInfo.Avatar = avatar;
                    newsCached.NewsInfo.Avatar2 = avatar2;
                    newsCached.NewsInfo.Avatar3 = avatar3;
                    newsCached.NewsInfo.Avatar4 = avatar4;
                    newsCached.NewsInfo.Avatar5 = avatar5;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    result = _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Title = newsTitle;
                    newsPublish.Avatar = avatar;
                    newsPublish.Avatar2 = avatar2;
                    newsPublish.Avatar3 = avatar3;
                    newsPublish.Avatar4 = avatar4;
                    newsPublish.Avatar5 = avatar5;
                    
                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Title = newsTitle;
                    newsPosition.Avatar = avatar;
                    newsPosition.Avatar2 = avatar2;
                    newsPosition.Avatar3 = avatar3;
                    newsPosition.Avatar4 = avatar4;
                    newsPosition.Avatar5 = avatar5;
                    
                    _db.AddInHash(newsId.ToString(), newsPosition);
                }

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Title = newsTitle;
                    newsContent.Avatar = avatar;
                    newsContent.Avatar2 = avatar2;
                    newsContent.Avatar3 = avatar3;
                    newsContent.Avatar4 = avatar4;
                    newsContent.Avatar5 = avatar5;
                    
                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                if (prevStatus == (int)NewsStatus.Published && prevTitle != newsTitle)
                {
                    //xoa key check trung title                
                    _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));
                    //add key check trung title                
                    _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, prevStatus)), new NewsCheckDuplicate { Id = newsId, Title = newsTitle, Status = prevStatus });
                }

                return result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateDetail(long newsId, string newsTitle, string sapo, string body, string newsUrl, string accountName)
        {
            try
            {
                var result = false;
                if (newsId <= 0)
                {
                    return false;
                }
                var prevTitle = newsTitle;
                var prevStatus = 0;
                var newsCached = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    prevTitle = newsCached.NewsInfo?.Title;
                    prevStatus = newsCached.NewsInfo.Status;

                    newsCached.NewsInfo.Title = newsTitle;
                    newsCached.NewsInfo.Sapo = sapo;
                    newsCached.NewsInfo.Body = body;
                    newsCached.NewsInfo.Url = newsUrl;
                    newsCached.NewsInfo.LastModifiedBy = accountName;
                    newsCached.NewsInfo.LastModifiedDate = DateTime.Now;

                    result = _db.AddInHash(newsId.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(newsId.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Title = newsTitle;
                    newsPublish.Sapo = sapo;
                    newsPublish.Url = newsUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(newsId.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Title = newsTitle;
                    newsPosition.Sapo = sapo;
                    newsPosition.Url = newsUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsPosition);
                }

                var newsContent = _db.GetFromHash<NewsContentEntity>(newsId.ToString());
                if (null != newsContent)
                {
                    newsContent.Title = newsTitle;
                    newsContent.Sapo = sapo;
                    newsContent.Body = body;
                    newsContent.Url = newsUrl;
                    
                    _db.AddInHash(newsId.ToString(), newsContent);
                }

                if (prevStatus == (int)NewsStatus.Published && prevTitle!= newsTitle)
                {
                    //xoa key check trung title                
                    _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));
                    //add key check trung title                
                    _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", newsTitle, prevStatus)), new NewsCheckDuplicate { Id= newsId, Title = newsTitle, Status = prevStatus });
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return false;
            }
        }

        public bool UpdateNewsByErrorCheck(NewsEntity news, string newsUrl, string tag, string accountName)
        {
            try
            {
                var result = false;
                if (news.Id <= 0)
                {
                    return false;
                }
                var prevTitle = news.Title;
                var prevStatus = 0;
                var newsCached = _db.GetFromHash<NewsCachedEntity>(news.Id.ToString());
                if (null != newsCached && newsCached.NewsInfo != null)
                {
                    prevTitle = newsCached.NewsInfo?.Title;
                    prevStatus = newsCached.NewsInfo.Status;

                    newsCached.NewsInfo.Title = news.Title;
                    newsCached.NewsInfo.SubTitle = news.SubTitle;
                    newsCached.NewsInfo.Sapo = news.Sapo;
                    newsCached.NewsInfo.Body = news.Body;
                    newsCached.NewsInfo.Url = newsUrl;
                    newsCached.NewsInfo.AvatarDesc = news.AvatarDesc;
                    newsCached.NewsInfo.Note = news.Note;
                    newsCached.NewsInfo.Tag = tag;
                    newsCached.NewsInfo.ErrorCheckedBy = accountName;
                    newsCached.NewsInfo.ErrorCheckedDate = DateTime.Now;
                    newsCached.NewsInfo.Avatar = news.Avatar;
                    newsCached.NewsInfo.Avatar2 = news.Avatar2;
                    newsCached.NewsInfo.Avatar3 = news.Avatar3;
                    newsCached.NewsInfo.Avatar4 = news.Avatar4;
                    newsCached.NewsInfo.Avatar5 = news.Avatar5;
                    newsCached.NewsInfo.TagSubTitleId = news.TagSubTitleId;
                    newsCached.NewsInfo.ShortTitle = news.ShortTitle;

                    result = _db.AddInHash(news.Id.ToString(), newsCached);
                }

                var newsPublish = _db.GetFromHash<NewsPublishEntity>(news.Id.ToString());
                if (null != newsPublish)
                {
                    newsPublish.Title = news.Title;
                    newsPublish.Sapo = news.Sapo;
                    newsPublish.Url = newsUrl;
                    newsPublish.AvatarDesc = news.AvatarDesc;
                    newsPublish.Avatar = news.Avatar;
                    newsPublish.Avatar2 = news.Avatar2;
                    newsPublish.Avatar3 = news.Avatar3;
                    newsPublish.Avatar4 = news.Avatar4;
                    newsPublish.Avatar5 = news.Avatar5;
                    newsPublish.TagSubTitleId = news.TagSubTitleId;                    

                    _db.AddInHash(news.Id.ToString(), newsPublish);
                }

                var newsPosition = _db.GetFromHash<NewsPositionEntity>(news.Id.ToString());
                if (null != newsPosition)
                {
                    newsPosition.Title = news.Title;
                    newsPosition.Sapo = news.Sapo;
                    newsPosition.Url = newsUrl;
                    newsPosition.AvatarDesc = news.AvatarDesc;
                    newsPosition.Avatar = news.Avatar;
                    newsPosition.Avatar2 = news.Avatar2;
                    newsPosition.Avatar3 = news.Avatar3;
                    newsPosition.Avatar4 = news.Avatar4;
                    newsPosition.Avatar5 = news.Avatar5;
                    newsPosition.TagSubTitleId = news.TagSubTitleId;

                    _db.AddInHash(news.Id.ToString(), newsPosition);
                }

                var newsContent = _db.GetFromHash<NewsContentEntity>(news.Id.ToString());
                if (null != newsContent)
                {
                    newsContent.Title = news.Title;
                    newsContent.Sapo = news.Sapo;
                    newsContent.Body = news.Body;
                    newsContent.Url = newsUrl;
                    newsContent.Avatar = news.Avatar;
                    newsContent.Avatar2 = news.Avatar2;
                    newsContent.Avatar3 = news.Avatar3;
                    newsContent.Avatar4 = news.Avatar4;
                    newsContent.Avatar5 = news.Avatar5;
                    newsContent.TagSubTitleId = news.TagSubTitleId;

                    _db.AddInHash(news.Id.ToString(), newsContent);
                }

                if (prevStatus == (int)NewsStatus.Published && prevTitle != news.Title)
                {
                    //xoa key check trung title                
                    _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));
                    //add key check trung title                
                    _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", news.Title, prevStatus)), new NewsCheckDuplicate { Id= news.Id, Title = news.Title, Status = prevStatus });
                }

                return result;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateThreadIdInNewsByNewsId(long threadId, string listDeleteNewsId)
        {
            try
            {
                if (threadId <= 0)
                {
                    return false;
                }
                var listId = listDeleteNewsId.Split(';').ToList();
                if(listId!=null && listId.Count > 0)
                {
                    var data = new Dictionary<string, NewsCachedEntity>();
                    var items = _db.GetsFromHash<NewsCachedEntity>(listId);
                    foreach(var item in items)
                    {
                        if (item.NewsInfo.ThreadId == threadId)
                        {
                            item.NewsInfo.ThreadId = 0;
                            data[item.NewsInfo.Id.ToString()] = item;
                        }
                    }                                       
                    _db.AddInHash(data);
                }                
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddNewsPublish(NewsPublishEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }
                
                _db.AddInHash(news.Id.ToString(), news);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddNewsInZoneByNewsId(long newsId, List<NewsInZoneEntity> listNewsInZone)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsInZone = listNewsInZone;
                }
                //var tasks = new List<Task>
                //    {
                //        Task.Run( () => {
                //            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                //        })
                //    };

                //var result = Task.WaitAny(tasks.ToArray());
                //return result > 0;

                _db.AddInHash(newsId.ToString(), data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool AddNewsInZoneByNewsId(long newsId, string listZoneId)
        {
            try
            {
                var listNewsInZone = new List<NewsInZoneEntity>();

                var arrayZoneId = listZoneId.Split(';');
                for (int i = 0; i < arrayZoneId.Length; i++)
                {
                    if (i == 0)
                    {
                        listNewsInZone.Add(new NewsInZoneEntity
                        {
                            NewsId = newsId,
                            ZoneId = Utility.ConvertToInt(arrayZoneId[i]),
                            IsPrimary = true
                        });
                    }
                    else {
                        listNewsInZone.Add(new NewsInZoneEntity
                        {
                            NewsId = newsId,
                            ZoneId = Utility.ConvertToInt(arrayZoneId[i]),
                            IsPrimary = false
                        });
                    }
                }

                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsInZone = listNewsInZone;
                }
                //var tasks = new List<Task>
                //    {
                //        Task.Run( () => {
                //            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                //        })
                //    };

                //var result = Task.WaitAny(tasks.ToArray());
                //return result > 0;

                _db.AddInHash(newsId.ToString(), data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #region Change status
        public bool ChangeStatusToMovedToTrash(long newsId, string username)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = (int)NewsStatus.MovedToTrash;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }                
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToPublished(long id, int newsPositionTypeForHome, int newsPositionOrderForHome, int newsPositionTypeForList, int newsPositionOrderForList, string publishedBy, long publishedDate, string publishedContent)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(id.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    if (string.IsNullOrEmpty(data.NewsInfo.PublishedBy) || data.NewsInfo.Status!=(int)NewsStatus.Published)
                    {
                        data.NewsInfo.PublishedBy = publishedBy;
                    }

                    if (string.IsNullOrEmpty(publishedContent))
                    {
                        publishedContent = data.NewsInfo.Body;
                    }

                    data.NewsInfo.Status = (int)NewsStatus.Published;                    
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.Body = publishedContent;

                    if (data.NewsInfo.DistributionDate <= DateTime.MinValue)
                        data.NewsInfo.DistributionDate = DateTime.Now;
                }
                
                var result = _db.AddInHash(id.ToString(), data);

                _db.AddInHash(id.ToString(), new NewsContentEntity
                {
                    NewsId = data.NewsInfo.Id,
                    ZoneId = data.NewsInfo.ZoneId,
                    Title = data.NewsInfo.Title,
                    Sapo = data.NewsInfo.Sapo,
                    Avatar = data.NewsInfo.Avatar,
                    Avatar2 = data.NewsInfo.Avatar2,
                    Avatar3 = data.NewsInfo.Avatar3,
                    Avatar4 = data.NewsInfo.Avatar4,
                    Avatar5 = data.NewsInfo.Avatar5,
                    Body = data.NewsInfo.Body,
                    Author = data.NewsInfo.Author,
                    NewsRelation = data.NewsInfo.NewsRelation,
                    Source = data.NewsInfo.Source,
                    PublishedDate = data.NewsInfo.DistributionDate,
                    LocationType = data.NewsInfo.LocationType,
                    Tags = data.NewsInfo.Tag,
                    SourceURL = data.NewsInfo.SourceURL
                });
                _db.AddInHash(id.ToString(), new NewsPublishEntity
                {
                    Id = id,
                    NewsId = data.NewsInfo.Id,
                    EncryptId = data.NewsInfo.EncryptId,
                    ZoneId = data.NewsInfo.ZoneId,
                    Title = data.NewsInfo.Title,
                    SubTitle = data.NewsInfo.SubTitle,
                    Sapo = data.NewsInfo.Sapo,
                    InitSapo = data.NewsInfo.InitSapo,
                    Avatar = data.NewsInfo.Avatar,
                    AvatarDesc = data.NewsInfo.AvatarDesc,
                    Avatar2 = data.NewsInfo.Avatar2,
                    Avatar3 = data.NewsInfo.Avatar3,
                    Avatar4 = data.NewsInfo.Avatar4,
                    Avatar5 = data.NewsInfo.Avatar5,
                    Author = data.NewsInfo.Author,
                    NewsRelation = data.NewsInfo.NewsRelation,
                    Source = data.NewsInfo.Source,
                    IsFocus = data.NewsInfo.IsFocus,
                    Type = data.NewsInfo.Type,
                    ThreadId = data.NewsInfo.ThreadId,
                    DistributionDate = data.NewsInfo.DistributionDate,
                    Url = data.NewsInfo.Url,
                    OriginalUrl = data.NewsInfo.OriginalUrl,
                    DisplayStyle = data.NewsInfo.DisplayStyle,
                    DisplayPosition = data.NewsInfo.DisplayPosition,
                    DisplayInSlide = data.NewsInfo.DisplayInSlide,
                    AvatarCustom = data.NewsInfo.AvatarCustom,
                    TagItem = data.NewsInfo.TagItem,
                    OriginalId = data.NewsInfo.OriginalId,
                    PublishedDate = publishedDate,
                    InterviewId = data.NewsInfo.InterviewId,
                    IsBreakingNews = data.NewsInfo.IsBreakingNews,
                    IsPr = data.NewsInfo.IsPr,
                    AdStore = data.NewsInfo.AdStore,
                    AdStoreUrl = data.NewsInfo.AdStoreUrl,
                    RollingNewsId = data.NewsInfo.RollingNewsId,
                    TagSubTitleId = data.NewsInfo.TagSubTitleId,
                    LocationType = data.NewsInfo.LocationType,
                    ExpiredDate = data.NewsInfo.ExpiredDate
                });
                
                //add key check trung title                
                _db.AddInHash(Utility.GenerateSHA256String(string.Format("{0}_{1}", data.NewsInfo?.Title, (int)NewsStatus.Published)), new NewsCheckDuplicate {Id= id, Title = data.NewsInfo?.Title, Status = (int)NewsStatus.Published });

                return result;
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToUnPublished(long newsId, string username, int status)
        {
            try
            {
                var prevTitle = string.Empty;
                var prevStatus = 0;
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());                
                if (null != data && data.NewsInfo != null)
                {
                    prevTitle = data.NewsInfo.Title;
                    prevStatus = data.NewsInfo.Status;

                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsRelation = null;
                }
                //update news status=10
                var result = _db.AddInHash(newsId.ToString(), data);
                //remove to newscontent
                _db.RemoveFromHash<NewsContentEntity>(newsId.ToString());
                //remove to newspublish
                _db.RemoveFromHash<NewsPublishEntity>(newsId.ToString());

                //xoa key check trung title                
                _db.RemoveFromHash<NewsCheckDuplicate>(Utility.GenerateSHA256String(string.Format("{0}_{1}", prevTitle, prevStatus)));

                return result;
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToWaitForPublish(long newsId, string username, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(data.NewsInfo.EditedBy))
                        data.NewsInfo.EditedBy = username;
                }

                return _db.AddInHash(newsId.ToString(), data);                
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToWaitForEditorialBoard(long newsId, string username, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(data.NewsInfo.ApprovedBy))
                        data.NewsInfo.ApprovedBy = username;
                }

                _db.AddInHash(newsId.ToString(), data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToWaitForEdit(long newsId, string username, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.EditedBy = "";
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReceivedForEdit(long newsId, string username, string receiver, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.EditedBy = receiver;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToRecievedForPublish(long newsId, string username, string receiver, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(data.NewsInfo.EditedBy))
                        data.NewsInfo.EditedBy = username;
                    data.NewsInfo.ApprovedBy = receiver;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReceivedForEditorialBoard(long newsId, string username, string receiver, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    if (string.IsNullOrEmpty(data.NewsInfo.ApprovedBy))
                        data.NewsInfo.ApprovedBy = username;
                    data.NewsInfo.PublishedBy = receiver;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReturnedToReporter(long newsId, string username, string note, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.ReturnedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.Note = note;
                }

                _db.AddInHash(newsId.ToString(), data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReturnedToEditorialSecretary(long newsId,string approvedBy, string username, string note, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.ApprovedBy = approvedBy;
                    data.NewsInfo.ReturnedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.Note = note;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReturnedToEditor(long newsId, string receiver, string username, string note, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.EditedBy = receiver;
                    data.NewsInfo.ReturnedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.Note = note;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool ChangeStatusForwardToOtherEditor(long newsId, string receiver)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.EditedBy = receiver;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }
                                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusForwardToOtherSecretary(long newsId, string receiver)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.ApprovedBy = receiver;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }                
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToRecieveFromReturnStore(long newsId, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToRestoreFromTrash(long newsId, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToCrawlerForEdit(long newsId, string username, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.CreatedBy = username;
                    data.NewsInfo.CreatedDate = DateTime.Now;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeStatusToReturnToCooperator(long newsId, string username, string note, int status)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data && data.NewsInfo != null)
                {
                    data.NewsInfo.Status = status;
                    data.NewsInfo.LastModifiedBy = username;
                    data.NewsInfo.LastModifiedDate = DateTime.Now;
                    data.NewsInfo.Note = note;
                }
                
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        #endregion
        public List<NewsInZoneEntity> GetNewsInZoneByNewsId(long newsId)
        {
            try
            {
                var list = new List<NewsInZoneEntity>();
                var replies = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());

                if (null != replies)
                {
                    list = replies.NewsInZone;
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public NewsForValidateEntity GetNewsForValidateById(long id)
        {
            var result = default(NewsForValidateEntity);
            try
            {
                var reply = _db.GetFromHash<NewsCachedEntity>(id.ToString());

                if (null != reply)
                {
                    result = new NewsForValidateEntity
                    {
                        Id = reply.NewsInfo.Id,
                        EncryptId = reply.NewsInfo.EncryptId,
                        Title = reply.NewsInfo.Title,
                        ListZoneId = reply.NewsInfo.ListZoneId,
                        Status = reply.NewsInfo.Status,
                        CreatedDate = reply.NewsInfo.CreatedDate,
                        LastModifiedDate = reply.NewsInfo.LastModifiedDate,
                        DistributionDate = reply.NewsInfo.DistributionDate,
                        CreatedBy = reply.NewsInfo.CreatedBy,
                        LastModifiedBy = reply.NewsInfo.LastModifiedBy,
                        PublishedBy = reply.NewsInfo.PublishedBy,
                        EditedBy = reply.NewsInfo.EditedBy,
                        LastReceiver = reply.NewsInfo.LastReceiver
                    };
                }
                return result;
            }
            catch
            {
                return result;
            }
        }
        public bool AddRelatedNewsByNewsId(long newsId, List<NewsPublishForNewsRelationEntity> listRelatedNews)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.NewsRelation = listRelatedNews;
                }
                //var tasks = new List<Task>
                //    {
                //        Task.Run( () => {
                //            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                //        })
                //    };

                //var result = Task.WaitAny(tasks.ToArray());
                //return result > 0;
                _db.AddInHash<NewsCachedEntity>(newsId.ToString(), data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsId(long newsId)
        {
            try
            {
                var list = new List<NewsPublishForNewsRelationEntity>();
                var replies = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());

                if (null != replies)
                {
                    list = replies.NewsRelation;
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<NewsPublishForNewsRelationEntity> GetRelatedNewsByNewsIds(string newsIds)
        {
            try
            {
                var listNewsId = newsIds.Split(';').ToList();
                var list = new List<NewsPublishForNewsRelationEntity>();
                var replies = _db.GetsFromHash<NewsPublishEntity>(listNewsId);

                if (null != replies && replies.Count() > 0)
                {
                    list = replies.Select(s => new NewsPublishForNewsRelationEntity
                    {
                        NewsId = s.NewsId,
                        EncryptId = s.EncryptId,
                        ZoneId = s.ZoneId,
                        Title = s.Title,
                        Avatar = s.Avatar,
                        Url = s.Url,
                        DistributionDate = s.DistributionDate,
                        Sapo = s.Sapo,
                        Author = s.Author
                    }).ToList();
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public NewsEntity GetNewsById(long newsId)
        {
            try
            {
                var replies = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());

                if (null != replies)
                {
                    return replies.NewsInfo;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public NewsCheckDuplicate GetTitleNewsByKey(string key)
        {
            try
            {
                var replies = _db.GetFromHash<NewsCheckDuplicate>(key);

                if (null != replies)
                {
                    return replies;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public bool UpdateNotification(NewsNotificationEntity newsnotify)
        {
            try
            {
                if (null == newsnotify)
                {
                    return false;
                }

                //var tasks = new List<Task>
                //{
                //    Task.Run( () => {
                //        _db.AddInHash<NewsNotificationEntity>(newsnotify.NewsId.ToString(), newsnotify);
                //    })
                //};

                //var result = Task.WaitAny(tasks.ToArray());
                //return result > 0;
                _db.AddInHash(newsnotify.NewsId.ToString(), newsnotify);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<NewsCachedEntity> GetNewsCachedByListId(List<string> newsIds)
        {
            try
            {
                return _db.GetsFromHash<NewsCachedEntity>(newsIds);
            }
            catch
            {
                return null;
            }
        }
        public List<NewsInListEntity> GetNewsByListId(List<string> newsIds)
        {
            try
            {
                var list = new List<NewsInListEntity>();
                var replies = _db.GetsFromHash<NewsCachedEntity>(newsIds);

                if (null != replies && replies.Count() > 0)
                {
                    list.AddRange(replies.Select(s => new NewsInListEntity
                    {
                        Id = s.NewsInfo.Id,
                        EncryptId = s.NewsInfo.EncryptId,
                        Title = s.NewsInfo.Title,
                        Sapo = s.NewsInfo.Sapo,
                        Avatar = s.NewsInfo.Avatar,
                        Avatar2 = s.NewsInfo.Avatar2,
                        Avatar3 = s.NewsInfo.Avatar3,
                        Avatar4 = s.NewsInfo.Avatar4,
                        Avatar5 = s.NewsInfo.Avatar5,
                        AvatarCustom = s.NewsInfo.AvatarCustom,
                        CreatedDate = s.NewsInfo.CreatedDate,
                        CreatedBy = s.NewsInfo.CreatedBy,
                        //EditedDate = s.News.EditedDate,
                        EditedBy = s.NewsInfo.EditedBy,
                        //PublishedDate = s.News.PublishedDate,
                        PublishedBy = s.NewsInfo.PublishedBy,
                        ApprovedBy = s.NewsInfo.ApprovedBy,
                        ApprovedDate=s.NewsInfo.ApprovedDate,
                        LastModifiedDate = s.NewsInfo.LastModifiedDate,
                        LastModifiedBy = s.NewsInfo.LastModifiedBy,
                        DistributionDate = s.NewsInfo.DistributionDate,
                        Status = s.NewsInfo.Status,
                        Type = s.NewsInfo.Type,
                        NewsType = s.NewsInfo.NewsType,
                        Note = s.NewsInfo.Note,
                        ViewCount = s.NewsInfo.ViewCount,
                        Url = s.NewsInfo.Url,
                        NoteRoyalties = s.NewsInfo.NoteRoyalties,
                        TagItem = s.NewsInfo.TagItem,
                        Price = s.NewsInfo.Price.HasValue ? s.NewsInfo.Price.Value : 0,
                        NewsCategory = s.NewsInfo.NewsCategory,
                        OriginalId = s.NewsInfo.OriginalId,
                        Source = s.NewsInfo.Source,
                        Author = s.NewsInfo.Author,
                        //ActiveUsers = s.News.ActiveUsers,
                        IsOnHome = s.NewsInfo.IsOnHome,
                        IsFocus = s.NewsInfo.IsFocus,
                        DisplayInSlide = s.NewsInfo.DisplayInSlide,
                        ZoneName = s.NewsInfo.ZoneName,
                        ZoneId = s.NewsInfo.ZoneId,
                        WordCount = s.NewsInfo.WordCount,
                        InterviewId = s.NewsInfo.InterviewId,
                        IsOnMobile = s.NewsInfo.IsOnMobile,
                        BonusPrice = s.NewsInfo.BonusPrice.HasValue ? s.NewsInfo.BonusPrice.Value:0,
                        LastReceiver = s.NewsInfo.LastReceiver,
                        //MobileCount = s.News.MobileCount,
                        //PictureCount = s.News.PictureCount,
                        //VideoCount = s.News.VideoCount,
                        DisplayPosition = s.NewsInfo.DisplayPosition,
                        Priority = s.NewsInfo.Priority,
                        ParentNewsId = s.NewsInfo.ParentNewsId
                    }));
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
        public List<NewsPublishForObjectBoxEntity> GetNewsPublishByListId(List<string> newsIds)
        {
            try
            {
                var list = new List<NewsPublishForObjectBoxEntity>();
                var replies = _db.GetsFromHash<NewsCachedEntity>(newsIds);

                if (null != replies && replies.Count() > 0)
                {
                    list.AddRange(replies.Select(s => new NewsPublishForObjectBoxEntity
                    {
                        NewsId = s.NewsInfo.Id,
                        EncryptId = s.NewsInfo.EncryptId,
                        Title = s.NewsInfo.Title,
                        Sapo = s.NewsInfo.Sapo,
                        Avatar = s.NewsInfo.Avatar,
                        Avatar2 = s.NewsInfo.Avatar2,
                        Avatar3 = s.NewsInfo.Avatar3,
                        Avatar4 = s.NewsInfo.Avatar4,
                        Avatar5 = s.NewsInfo.Avatar5,
                        DistributionDate = s.NewsInfo.DistributionDate,
                        Url = s.NewsInfo.Url,
                        ZoneId = s.NewsInfo.ZoneId,
                        DisplayStyle = s.NewsInfo.DisplayStyle
                    }));
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<CountStatus> CountStatus(string username)
        {
            try
            {
                return _db.Gets<CountStatus>(username);
            }
            catch
            {
                return null;
            }
        }
        public bool AddCountStatus(string username, List<CountStatus> listCount)
        {
            try
            {
                if (null == listCount)
                {
                    return false;
                }
                
                _db.Add(username, listCount, 30);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertNewsHistory(NewsHistoryEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }
                
                _db.AddInHash(news.NewsId.ToString(), news);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region init DB
        public bool InitAllNews(List<NewsEntity> listNews)
        {
            try
            {
                var data = new Dictionary<string, NewsCachedEntity>();                
                foreach (var entry in listNews)
                {                                        
                    data[entry.Id.ToString()] = new NewsCachedEntity() { NewsInfo = entry };                    
                }
                              
                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool InitAllNewsCheckDuplicate(List<NewsEntity> listNews)
        {
            try
            {                
                var dataCheck = new Dictionary<string, NewsCheckDuplicate>();
                foreach (var entry in listNews)
                {                                        
                    dataCheck[Utility.GenerateSHA256String(string.Format("{0}_{1}", entry.Title, entry.Status))] = new NewsCheckDuplicate() { Id = entry.Id, Title = entry.Title, Status = entry.Status };
                }
                                
                return _db.AddInHash(dataCheck);
            }
            catch (Exception)
            {
                return false;
            }
        }

        //appbot
        public bool AddNewsParentIdAndOriginalId(NewsEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }
                var newsCached = new NewsParentIdAndOriginalIdEntity
                {
                    NewsInfo = news,
                    NewsParentId = news.ParentNewsId,
                    OriginalId = news.OriginalId
                };
                var zoneids = new List<string>();
                var zoneidsTemp = new List<string>();
                if (newsCached.NewsInfo != null && newsCached.NewsInfo.ZoneId > 0)
                    zoneidsTemp.Add(newsCached.NewsInfo.ZoneId.ToString());
                if (newsCached.NewsInfo != null && !string.IsNullOrEmpty(newsCached.NewsInfo.ListZoneId))
                    zoneidsTemp.AddRange(newsCached.NewsInfo.ListZoneId.Split(';').Where(s => s != ""));
                zoneids.AddRange(zoneidsTemp.Distinct());
                newsCached.NewsInfo.ListZoneId = string.Join(";", zoneids);
                var keyId = newsCached.NewsParentId + "_" + newsCached.OriginalId;

                return _db.AddInHash(keyId, newsCached);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public NewsParentIdAndOriginalIdEntity GetNewsParentIdAndOriginalId(string keyId)
        {
            try
            {
                if (string.IsNullOrEmpty(keyId))
                {
                    return null;
                }

                return _db.GetFromHash<NewsParentIdAndOriginalIdEntity>(keyId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region DictsNewsInUsing
        public List<NewsInUsingEntity> GetAllNewsInUsing()
        {
            try
            {
                var replies = _db.GetsFromHash<NewsInUsingEntity>();

                if (null != replies)
                {
                    return replies;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public NewsInUsingEntity GetNewsInUsingById(long id)
        {
            try
            {
                var replies = _db.GetFromHash<NewsInUsingEntity>(id.ToString());

                if (null != replies)
                {
                    return replies;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }
        public bool AddNewsInUsing(NewsInUsingEntity obj)
        {
            try
            {                
                return _db.AddInHash(obj.Id.ToString(),obj);
            }
            catch
            {
                return false;
            }
        }
        public bool RemoveNewsInUsing(long id, string accountName, bool allowUnlock=false)
        {
            try
            {
                if (allowUnlock)
                {
                    _db.RemoveFromHash<NewsInUsingEntity>(id.ToString());
                    return _db.AddInHash(id.ToString(), new NewsInUsingEntity { Id=id,AccountName=accountName});
                }

                var replies = _db.GetFromHash<NewsInUsingEntity>(id.ToString());

                if (null != replies && !string.IsNullOrEmpty(replies.AccountName) && replies.AccountName.ToLower() == accountName.ToLower())
                {
                    return _db.RemoveFromHash<NewsInUsingEntity>(id.ToString());
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool ResetNewsInUsing(long id)
        {
            try
            {                
                return _db.RemoveFromHash<NewsInUsingEntity>(id.ToString());
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
