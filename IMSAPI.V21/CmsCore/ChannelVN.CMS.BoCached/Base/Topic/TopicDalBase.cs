﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.Topic
{
    public abstract class TopicDalBase
    {
        public bool AddTopic(TopicEntity topic)
        {
            try
            {
                if (null == topic)
                {
                    return false;
                }

                var result = _db.GetFromHash<TopicEntity>(topic.Id.ToString());
                if (result != null)
                {
                    topic.CreatedDate = result.CreatedDate;
                    topic.CreatedBy = result.CreatedBy;
                }

                return _db.AddInHash(topic.Id.ToString(), topic);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddDataSearchNewsInTopic(string hashName, string key, string value)
        {
            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    return false;
                }
      
                return _db.AddInHashWithCustomName(hashName, key, value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddTopicByNewsId(long newsId, int topicId)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.TopicId = topicId;
                    
                    return _db.AddInHash(newsId.ToString(), data);
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeleteTopicById(long id)
        {
            try
            {
                return _db.RemoveFromHash<TopicEntity>(id.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            var result = new List<TopicEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null && news.TopicId<=0)
                {
                    result = _db.GetsFromHash<TopicEntity>(news.TopicId.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public TopicEntity GetTopicByTopicId(long topicId)
        {
            var result = new TopicEntity();
            try
            {
                return result = _db.GetFromHash<TopicEntity>(topicId.ToString());
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public string GetDataSearchNewsInTopic(string hashName, string key)
        {
            return _db.GetsFromHashWithCustomKey(hashName, key);
        }

        public bool DeleteDataSearchNewsInTopic(string hashName)
        {
            return _db.Remove(hashName);
        }

        public bool ToggleTopicActive(int id)
        {
            try
            {
                var result = _db.GetFromHash<TopicEntity>(id.ToString());
                if (result != null)
                {
                    if (result.IsActive)
                    {
                        result.IsActive = false;
                    }
                    else
                    {
                        result.IsActive = true;
                    }
                }
                return _db.AddInHash(id.ToString(), result);
            }
            catch
            {
                return false;
            }
        }
        public bool ToggleTopicIconActive(int id)
        {
            try
            {
                var result = _db.GetFromHash<TopicEntity>(id.ToString());
                if (result != null)
                {
                    if (result.IsIconActive)
                    {
                        result.IsIconActive = false;
                    }
                    else
                    {
                        result.IsIconActive = true;
                    }
                }
                return _db.AddInHash(id.ToString(), result);
            }
            catch
            {
                return false;
            }
        }
        public bool ToggleTopicIsToolbar(int id)
        {
            try
            {
                var result = _db.GetFromHash<TopicEntity>(id.ToString());
                if (result != null)
                {
                    if (result.IsTopToolbar)
                    {
                        result.IsTopToolbar = false;
                    }
                    else
                    {
                        result.IsTopToolbar = true;
                    }
                }
                return _db.AddInHash(id.ToString(), result);
            }
            catch
            {
                return false;
            }
        }

        public bool InitAllTopic(List<TopicEntity> listTopic)
        {
            try
            {
                var data = new Dictionary<string, TopicEntity>();
                foreach (var entry in listTopic)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected TopicDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
