﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Topic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Topic
{
    public class TopicDalFactory
    {
        public static bool AddTopic(TopicEntity topic)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.AddTopic(topic);
            }
            return returnValue;
        }
        public static bool AddTopicByNewsId(long newsId,int topicId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.AddTopicByNewsId(newsId, topicId);
            }
            return returnValue;
        }
        //ket qua search news in topic
        public static bool AddDataSearchNewsInTopic(string hashName, string key, string value)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.AddDataSearchNewsInTopic(hashName, key, value);
            }
            return returnValue;
        }
        public static bool DeleteTopicById(long id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.DeleteTopicById(id);
            }
            return returnValue;
        }
        public static List<TopicEntity> GetTopicByNewsId(long newsId)
        {
            List<TopicEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.GetTopicByNewsId(newsId);
            }
            return returnValue;
        }
        public static TopicEntity GetTopicByTopicId(long topicId)
        {
            TopicEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.GetTopicByTopicId(topicId);
            }
            return returnValue;
        }
        public static string GetDataSearchNewsInTopic(string hashName, string key)
        {
            using (var db = new CmsMainCachedDb())
            {
                return db.TopicDal.GetDataSearchNewsInTopic(hashName, key);
            }

            return string.Empty;
        }
        public static bool DeleteDataSearchNewsInTopic(string hashName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.DeleteDataSearchNewsInTopic(hashName);
            }
            return returnValue;
        }
        public static bool ToggleTopicActive(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.ToggleTopicActive(id);
            }
            return returnValue;
        }
        public static bool ToggleTopicIconActive(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.ToggleTopicIconActive(id);
            }
            return returnValue;
        }
        public static bool ToggleTopicIsToolbar(int id)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.ToggleTopicIsToolbar(id);
            }
            return returnValue;
        }
        public static bool InitAllTopic(List<TopicEntity> listTag)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.TopicDal.InitAllTopic(listTag);
            }
            return returnValue;
        }
    }
}
