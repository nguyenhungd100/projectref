﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsConfig
{
    public class NewsConfigDal: NewsConfigDalBase
    {
        internal NewsConfigDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
