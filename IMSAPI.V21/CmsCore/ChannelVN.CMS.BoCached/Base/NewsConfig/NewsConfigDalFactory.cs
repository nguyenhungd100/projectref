﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsConfig
{
    public class NewsConfigDalFactory
    {
        public static bool AddNewsConfig(NewsConfigEntity newsConfig)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsConfigDal.AddNewsConfig(newsConfig);
            }
            return returnValue;
        }
        public static bool SetValue(string configName, string configValue)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsConfigDal.SetValue(configName, configValue);
            }
            return returnValue;
        }
        public static bool SetValueAndLabel(string configName, string configLabel, string configValue, int configType)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsConfigDal.SetValueAndLabel(configName, configLabel, configValue, configType);
            }
            return returnValue;
        }
        public static NewsConfigEntity GetNewsConfigByConfigName(string configName)
        {
            NewsConfigEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsConfigDal.GetNewsConfigByConfigName(configName);
            }
            return returnValue;
        }
        public static List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey, int type)
        {
            List<NewsConfigEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsConfigDal.GetListConfigByGroupKey(groupConfigKey, type);
            }
            return returnValue;
        }
    }
}
