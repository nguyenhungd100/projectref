﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsConfig
{
    public abstract class NewsConfigDalBase
    {
        public bool AddNewsConfig(NewsConfigEntity newsConfig)
        {
            try
            {
                if (null == newsConfig)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsConfigEntity>(newsConfig.ConfigName, newsConfig);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SetValue(string configName, string configValue)
        {
            try
            {                
                var newsConfig = new NewsConfigEntity
                {
                    ConfigName = configName,
                    ConfigValue = configValue
                };
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsConfigEntity>(newsConfig.ConfigName, newsConfig);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool SetValueAndLabel(string configName, string configLabel, string configValue, int configType)
        {
            try
            {
                var newsConfig = new NewsConfigEntity
                {
                    ConfigName = configName,
                    ConfigLabel = configLabel,
                    ConfigValue = configValue,
                    ConfigValueType = configType
                };
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsConfigEntity>(newsConfig.ConfigName, newsConfig);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public NewsConfigEntity GetNewsConfigByConfigName(string configName)
        {            
            try
            {                         
                return _db.GetFromHash<NewsConfigEntity>(configName);
            }
            catch (Exception ex)
            {
                return new NewsConfigEntity();
            }
        }
        public List<NewsConfigEntity> GetListConfigByGroupKey(string groupConfigKey, int type)
        {
            var data = new List<NewsConfigEntity>();
            try
            {
                var list= _db.GetsFromHash<NewsConfigEntity>();
                if(list!=null && list.Count() > 0)
                {
                    data=list.Where(w => w.ConfigGroupKey==groupConfigKey && w.ConfigValueType==type).ToList();
                }
                return data;
            }
            catch (Exception ex)
            {
                return data;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsConfigDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
