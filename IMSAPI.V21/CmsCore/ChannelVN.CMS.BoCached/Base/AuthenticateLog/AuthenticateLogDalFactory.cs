﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.AuthenticateLog
{
    public class AuthenticateLogDalFactory
    {        
        public static bool Insert(long newLogId, AuthenticateLogEntity authenticate)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                authenticate.Id = newLogId;
                returnValue = db.AuthenticateLogDal.Insert(newLogId, authenticate);
            }
            return returnValue;
        }        
        
    }
}
