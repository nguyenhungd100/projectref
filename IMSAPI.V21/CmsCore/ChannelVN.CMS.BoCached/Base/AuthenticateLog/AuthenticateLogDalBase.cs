﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.AuthenticateLog
{
    public abstract class AuthenticateLogDalBase
    {
        public bool Insert(long newLogId, AuthenticateLogEntity authenticate)
        {
            try
            {                
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<AuthenticateLogEntity>(newLogId.ToString(),authenticate);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;                
            }
            catch
            {
                return false;
            }
        }        

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected AuthenticateLogDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
