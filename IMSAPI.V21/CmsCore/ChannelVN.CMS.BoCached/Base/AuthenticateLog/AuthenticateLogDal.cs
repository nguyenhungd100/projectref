﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.AuthenticateLog
{
    public class AuthenticateLogDal: AuthenticateLogDalBase
    {
        internal AuthenticateLogDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
