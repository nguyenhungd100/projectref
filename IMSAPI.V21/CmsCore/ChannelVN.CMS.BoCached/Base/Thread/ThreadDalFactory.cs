﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Thread;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Thread
{
    public class ThreadDalFactory
    {
        public static bool AddThread(ThreadEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ThreadDal.AddThread(news);
            }
            return returnValue;
        }
        public static bool AddThreadByNewsId(long newsId, List<ThreadEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ThreadDal.AddThreadByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            List<ThreadEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ThreadDal.GetThreadByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<ThreadEntity> GetThreadByListId(List<string> threadIds)
        {
            List<ThreadEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ThreadDal.GetThreadByListId(threadIds);
            }
            return returnValue;
        }
        public static bool InitAllThread(List<ThreadEntity> listTag)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ThreadDal.InitAllThread(listTag);
            }
            return returnValue;
        }
    }
}
