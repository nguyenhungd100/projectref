﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.Thread;

namespace ChannelVN.CMS.BoCached.Base.Thread
{
    public abstract class ThreadDalBase
    {
        public bool AddThread(ThreadEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<ThreadEntity>(news.Id.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddThreadByNewsId(long newsId, List<ThreadEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.ThreadNewsInfo = list;
                }

                return _db.AddInHash(newsId.ToString(), data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ThreadEntity> GetThreadByNewsId(long newsId)
        {
            var result = new List<ThreadEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.ThreadNewsInfo;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<ThreadEntity> GetThreadByListId(List<string> threadIds)
        {
            try
            {
                return _db.GetsFromHash<ThreadEntity>(threadIds);
            }
            catch
            {
                return null;
            }
        }
        public bool InitAllThread(List<ThreadEntity> listThread)
        {
            try
            {
                var data = new Dictionary<string, ThreadEntity>();
                foreach (var entry in listThread)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<ThreadEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected ThreadDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
