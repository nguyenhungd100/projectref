﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsPosition
{
    public class NewsPositionDalFactory
    {
        public static bool AddNewsPosition(NewsPositionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsPositionDal.AddNewsPosition(news);
            }
            return returnValue;
        }                       
    }
}
