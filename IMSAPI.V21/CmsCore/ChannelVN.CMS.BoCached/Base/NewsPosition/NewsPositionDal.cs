﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsPosition
{
    public class NewsPositionDal: NewsPositionDalBase
    {
        internal NewsPositionDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
