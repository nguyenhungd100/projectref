﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.Entity.Base.News;

namespace ChannelVN.CMS.BoCached.Base.Statistic
{
    public abstract class StatisticDalBase
    {

        public bool AddStaticNewsTotalProducttionCache(List<NewsStatisticTotalProduction> data, string zoneId, string fromDate, string toDate)
        {
            try
            {
                if (null == data)
                {
                    return false;
                }

                var key = string.Format("{0}:{1}:{2}", zoneId, fromDate, toDate);
                var expireTime = 1800; //30p

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.Add(key, data, expireTime);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NewsStatisticTotalProduction> GetStaticNewsTotalProducttionCache(string zoneId, string fromDate, string toDate)
        {
            var key = string.Format("{0}:{1}:{2}", zoneId, fromDate, toDate);
            try
            {
                return _db.Gets<NewsStatisticTotalProduction>(key);
            }
            catch
            {
                return null;
            }
        }

        public bool AddHotNews(int top, int day, List<NewsWithAnalyticEntity> data)
        {
            try
            {
                if (null == data)
                {
                    return false;
                }

                var key = string.Format("{0}:{1}", top, day);
                var expireTime = 180; //5p
                _db.Add(key, data, expireTime);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NewsWithAnalyticEntity> GetHotNews(int top, int day)
        {
            var key = string.Format("{0}:{1}", top, day);
            try
            {
                return _db.Gets<NewsWithAnalyticEntity>(key);
            }
            catch
            {
                return null;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected StatisticDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
