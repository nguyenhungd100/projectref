﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.Entity.Base.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Statistic
{
    public class StatisticDalFactory
    {
        public static bool AddStaticNewsTotalProducttionCache(List<NewsStatisticTotalProduction> data, string zoneId, string fromDate, string toDate)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StatisticDal.AddStaticNewsTotalProducttionCache(data, zoneId, fromDate, toDate);
            }
            return returnValue;
        }

        public static List<NewsStatisticTotalProduction> GetStaticNewsTotalProducttionCache(string zoneId, string fromDate, string toDate)
        {
            List<NewsStatisticTotalProduction> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StatisticDal.GetStaticNewsTotalProducttionCache(zoneId, fromDate, toDate);
            }
            return returnValue;
        }

        public static bool AddHotNews(int top, int day, List<NewsWithAnalyticEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StatisticDal.AddHotNews(top, day, data);
            }
            return returnValue;
        }

        public static List<NewsWithAnalyticEntity> GetHotNews(int top, int day)
        {
            List<NewsWithAnalyticEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.StatisticDal.GetHotNews(top, day);
            }
            return returnValue;
        }
    }
}
