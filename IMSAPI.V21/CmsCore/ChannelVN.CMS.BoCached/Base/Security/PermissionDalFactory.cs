﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Security
{
    public class PermissionDalFactory
    {

        public static PermissionEntity GetPermissionById(int permissionId)
        {
            PermissionEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetPermissionById(permissionId);
            }
            return returnValue;
        }

        public static List<UserPermissionEntity> GetListByUserId(int userId, bool isGetChildZone)
        {
            List<UserPermissionEntity> returnValue=null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetListByUserId(userId, isGetChildZone);
            }
            return returnValue;
        }

        public static List<UserPermissionEntity> GetListByUserName(string username)
        {
            List<UserPermissionEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetListByUserName(username);
            }
            return returnValue;
        }

        public static bool AddListUserPermissionByUserId(int userId, bool isGetChildZone, List<UserPermissionEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.AddListUserPermissionByUserId(userId, isGetChildZone, data);
            }
            return returnValue;
        }

        public static bool InitUserPermission(int userId, List<UserPermissionEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.InitUserPermission(userId, data);
            }
            return returnValue;
        }

        public static List<GroupPermissionDetailEntity> GetAllPermissionGroupDetail()
        {
            List<GroupPermissionDetailEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetAllPermissionGroupDetail();
            }
            return returnValue;
        }

        public static bool AddAllPermissionGroupDetail(List<GroupPermissionDetailEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.AddAllPermissionGroupDetail(data);
            }
            return returnValue;
        }

        public static bool RemovePermissionById(int permissionId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.RemovePermissionById(permissionId);
            }
            return returnValue;
        }

        public static bool AddPermission(PermissionEntity data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.AddPermission(data);
            }
            return returnValue;
        }

        public static bool CheckUserPermission(string username, int permissionId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.CheckUserPermission(username,permissionId);
            }
            return returnValue;
        }

        public static bool CheckUserPermission(string username, int permissionId, int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.CheckUserPermission(username, permissionId, zoneId);
            }
            return returnValue;
        }

        public static bool CheckUserInGroupPermission(int userId, int groupId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.CheckUserInGroupPermission(userId, groupId);
            }
            return returnValue;
        }

        public static EnumPermission GetCurrentRole(string username)
        {
            EnumPermission returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetCurrentRole(username);
            }
            return returnValue;
        }

        public static List<int> GetListCurrentRole(string username)
        {
            List<int> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PermissionDal.GetListCurrentRole(username);
            }
            return returnValue;
        }
    }
}
