﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Entity.Base.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Security
{
    public abstract class PermissionDalBase
    {
        public bool AddListUserPermissionByUserId(int userId, bool isGetChildZone, List<UserPermissionEntity> userPermisstionList)
        {
            try
            {
                var hashId = userId.ToString();
                var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                if (null != reply)
                {
                    reply.PermissionList = userPermisstionList;

                    _db.AddInHash(hashId, reply);

                    if (isGetChildZone)
                        _db.Add(hashId, userPermisstionList);
                   
                    return true;
                }

                return false;          
            }
            catch
            {
                return false;
            }
        }

        public PermissionEntity GetPermissionById(int permissionId)
        {
            try
            {
                return _db.GetFromHash<PermissionEntity>(permissionId.ToString());
            }
            catch
            {
                return null;
            }
        }

        public List<UserPermissionEntity> GetListByUserId(int userId, bool isGetChildZone)
        {
            try
            {
                var results = new List<UserPermissionEntity>();

                var hashId = userId.ToString();
                if (isGetChildZone)
                {
                    results = _db.Gets<UserPermissionEntity>(hashId);
                    if (null != results && results.Count > 0) return results;
                }

                var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                if(null != reply)
                {
                    results = reply.PermissionList;
                }
                return results;
            }
            catch
            {
                return null;
            }
        }

        public List<UserPermissionEntity> GetListByUserName(string username)
        {
            try
            {
                var results = new List<UserPermissionEntity>();
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);                
                if (userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                    if (reply != null)
                    {
                        results = reply.PermissionList;
                    }
                }
                return results;                
            }
            catch
            {
                return null;
            }
        }

        public List<GroupPermissionDetailEntity> GetAllPermissionGroupDetail()
        {
            try
            {
                var list = new List<GroupPermissionDetailEntity>();
                var data = _db.GetsFromHash<GroupPermissionDetailCachedEntity>();
                if (data != null && data.Count() > 0)
                {
                    foreach (var item in data)
                    {
                        var listPermisstion = _db.GetsFromHash<PermissionEntity>(item.PermissionList);
                        //init list permisstion redis
                        if (listPermisstion != null && listPermisstion[0]==null)
                        {
                            var listPermissionDb = ChannelVN.CMS.DAL.Base.Account.PermissionDal.GetListPermissionByGroupId(item.Id);                            
                            InitPermission(listPermissionDb);
                        }

                        list.Add(new GroupPermissionDetailEntity
                        {
                            Id = item.Id,
                            Name = item.Name,                            
                            PermissionList = listPermisstion
                        });
                    }
                }  
                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool AddAllPermissionGroupDetail(List<GroupPermissionDetailEntity> listGroupPermission)
        {
            try
            {
                var data = new Dictionary<string, GroupPermissionDetailCachedEntity>();
                
                foreach (var entry in listGroupPermission)
                {
                    data[entry.Id.ToString()] = new GroupPermissionDetailCachedEntity
                    {
                        Id = entry.Id,
                        Name = entry.Name,
                        PermissionList = entry.PermissionList.Select(p => p.Id.ToString()).ToList()
                    };
                }

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash(data);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());

                return result > 0;
            }
            catch
            {
                return false;
            }
        }

        public bool RemovePermissionById(int permissionId)
        {
            try
            {
                return _db.RemoveFromHash<PermissionEntity>(permissionId.ToString());
            }
            catch
            {
                return false;
            }
        }

        public bool AddPermission(PermissionEntity data)
        {
            try
            {
                return _db.AddInHash<PermissionEntity>(data.Id.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool InitPermission(List<PermissionEntity> data)
        {
            if (data != null)
            {
                foreach (var item in data)
                {
                    AddPermission(item);
                }
            }
            return true;
        }

        public bool CheckUserPermission(string username, int permissionId)
        {
            try
            {
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);                
                if (userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    var data = _db.GetFromHash<UserCachedEntity>(hashId);

                    if (data != null)
                    {
                        if (data.User.IsFullPermission)
                        {
                            return true;
                        }
                        else
                        {
                            if (data.PermissionList.Count(s => s.PermissionId == permissionId) > 0)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool CheckUserPermission(string username, int permissionId,int zoneId)
        {
            try
            {
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);
                if (userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    var data = _db.GetFromHash<UserCachedEntity>(hashId);

                    if (data != null)
                    {
                        if (data.User.IsFullPermission)
                        {
                            return true;
                        }
                        else
                        {
                            if (data.PermissionList.Count(s => s.PermissionId == permissionId && s.ZoneId==zoneId) > 0)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool CheckUserInGroupPermission(int userId, int groupId)
        {
            try
            {                                
                var hashId = string.Format("{0}", userId.ToString());
                var data = _db.GetFromHash<UserCachedEntity>(hashId);

                if (data != null)
                {
                    if (data.User.IsFullPermission)
                    {
                        return true;
                    }
                    else
                    {
                        if (data.PermissionList.Count(s => s.PermissionId == groupId) > 0)
                        {
                            return true;
                        }
                    }
                }                
                return false;
            }
            catch
            {
                return false;
            }
        }

        public EnumPermission GetCurrentRole(string username)
        {
            try
            {
                var maxRole = EnumPermission.ArticleReporter;
                if (CheckUserPermission(username,(int)EnumPermission.ArticleAdmin))
                {
                    maxRole = EnumPermission.ArticleAdmin;
                }
                else if (CheckUserPermission(username, (int)EnumPermission.ArticleEditor))
                {
                    maxRole = EnumPermission.ArticleEditor;
                }
                
                return maxRole;
            }
            catch
            {
                return EnumPermission.ArticleReporter;
            }
        }

        public List<int> GetListCurrentRole(string username)
        {
            var results = new List<int>();
            results.Add(1);
            try
            {
                
                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);
                if (userId != null)
                {
                    var hashId = string.Format("{0}", userId.Value);
                    var reply = _db.GetFromHash<UserCachedEntity>(hashId);

                    if (reply != null)
                    {
                        results = reply.PermissionList.Select(s=>s.PermissionId).Distinct().ToList();
                    }
                }
                return results;
            }
            catch
            {                
                return results;
            }
        }

        public bool InitUserPermission(int userId, List<UserPermissionEntity> userPermisstionList)
        {
            try
            {
                var hashId = userId.ToString();
                _db.Add(hashId, userPermisstionList);

                var reply = _db.GetFromHash<UserCachedEntity>(hashId);
                if (reply != null)
                {
                    reply.PermissionList= userPermisstionList;
                    _db.AddInHash(hashId, reply);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected PermissionDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
