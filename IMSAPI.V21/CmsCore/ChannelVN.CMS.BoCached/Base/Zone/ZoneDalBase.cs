﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Zone
{
    public abstract class ZoneDalBase
    {
        public bool AddZone(ZoneEntity data)
        {
            try
            {
                var result = _db.GetFromHash<ZoneEntity>(data.Id.ToString());
                if (result != null)
                {
                    data.CreatedDate = result.CreatedDate;
                }
                return _db.AddInHash(data.Id.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ToggleZoneInvisibled(int zoneId)
        {
            try
            {
                var result = _db.GetFromHash<ZoneEntity>(zoneId.ToString());
                if (result != null)
                {
                    if (result.Invisibled)
                    {
                        result.Invisibled = false;
                    }
                    else
                    {
                        result.Invisibled = true;
                    }
                }                
                return _db.AddInHash(zoneId.ToString(), result);
            }
            catch
            {
                return false;
            }
        }
        public bool ToggleZoneStatus(int zoneId)
        {
            try
            {
                var result = _db.GetFromHash<ZoneEntity>(zoneId.ToString());
                if (result != null)
                {
                    if (result.Status==1)
                    {
                        result.Status = 0;
                    }
                    else
                    {
                        result.Status = 1;
                    }
                }
                return _db.AddInHash(zoneId.ToString(), result);
            }
            catch
            {
                return false;
            }
        }
        public bool ToggleZoneAllowComment(int zoneId)
        {
            try
            {
                var result = _db.GetFromHash<ZoneEntity>(zoneId.ToString());
                if (result != null)
                {
                    if (result.AllowComment)
                    {
                        result.AllowComment = false;
                    }
                    else
                    {
                        result.AllowComment = true;
                    }
                }
                return _db.AddInHash(zoneId.ToString(), result);
            }
            catch
            {
                return false;
            }
        }
        public ZoneEntity GetZoneById(int zoneId)
        {
            try
            {
                var result = _db.GetFromHash<ZoneEntity>(zoneId.ToString());
                return result;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByParentId(int parentId)
        {
            try
            {
                var list = new List<ZoneWithSimpleFieldEntity>();

                var result = _db.GetsFromHash<ZoneEntity>();
                if (result != null && result.Count>0)
                {
                    list = result.Where(w => w.Status == 1 && w.ParentId == parentId).Select(s => new ZoneWithSimpleFieldEntity
                    {
                        Id = s.Id,
                        Name = s.Name,
                        ParentId = s.ParentId,
                        ShortUrl = s.ShortUrl,
                        SortOrder = s.SortOrder,
                        Invisibled = s.Invisibled,
                        Status = s.Status
                    }).ToList();

                    if (parentId == 0)
                    {
                        //lấy cha lấy cả parantid=-1 đề phong cha=-1
                        list.AddRange(result.Where(w => w.Status == 1 && w.ParentId == -1).Select(s => new ZoneWithSimpleFieldEntity
                        {
                            Id = s.Id,
                            Name = s.Name,
                            ParentId = s.ParentId,
                            ShortUrl = s.ShortUrl,
                            SortOrder = s.SortOrder,
                            Invisibled = s.Invisibled,
                            Status = s.Status
                        }).ToList()
                        );
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneEntity> GetZoneByParentId(int parentId)
        {
            try
            {
                var list = new List<ZoneEntity>();

                var result = _db.GetsFromHash<ZoneEntity>();
                if (result != null && result.Count > 0)
                {
                    if (parentId < 0)
                    {
                        list = result;
                    }
                    else
                    {
                        list = result.Where(s => s.ParentId == parentId).ToList();
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            try
            {
                var data = new List<ZoneEntity>();
                var listKey = new List<string>();

                var oNews = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (oNews != null)
                {
                    listKey = oNews.NewsInZone.Select(s=>s.ZoneId.ToString()).ToList();
                }   
                if (listKey != null && listKey.Count > 0)
                {
                    data = _db.GetsFromHash<ZoneEntity>(listKey);
                }
                return data;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneEntity> GetZoneByUserId(int userId)
        {
            try
            {
                var data = new List<ZoneEntity>();
                
                var user = _db.GetFromHash<UserCachedEntity>(userId.ToString());
                if (user != null && user.User.IsFullZone)
                {
                    data = _db.GetsFromHash<ZoneEntity>();
                }
                else
                {
                    var listZone = user.PermissionList.Select(s => s.ZoneId.ToString()).Distinct().ToList();
                    if (listZone != null && listZone.Count() > 0)
                    {                        
                        foreach (var item in listZone)
                        {
                            data.AddRange(_db.GetsFromHash<ZoneEntity>().Where(w => w.Status == 1 && w.ParentId.ToString().Equals(item)).ToList());
                        }

                        data.AddRange(_db.GetsFromHash<ZoneEntity>(listZone).Where(w => w.Status == 1));                        
                    }
                }
                                
                return data;
            }
            catch
            {
                return null;
            }
        }
        public ZoneEntity GetPrimaryZoneByNewsId(long newsId)
        {
            try
            {
                var data = new ZoneEntity();
                var zonePrimary = string.Empty;

                var oNews = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (oNews != null)
                {
                    zonePrimary = oNews.NewsInZone.Where(w=>w.IsPrimary==true).Select(s => s.ZoneId.ToString()).FirstOrDefault();
                }
                if (zonePrimary != null)
                {
                    data = _db.GetFromHash<ZoneEntity>(zonePrimary);
                }
                return data;
            }
            catch
            {
                return null;
            }
        }
        public List<ZoneWithSimpleFieldEntity> GetZoneActivedByUsernameAndPermissionIds(string username, string listPermissionId)
        {
            try
            {
                var list = new List<ZoneWithSimpleFieldEntity>();

                var userId = _db.GetScoreFromSortedSet<UserEntity>(username);
                var user = _db.GetFromHash<UserCachedEntity>(userId.ToString());
                if (user != null)
                {
                    if(user.User.IsFullZone && user.User.IsFullPermission)
                    {
                        var result = _db.GetsFromHash<ZoneEntity>();
                        if (result != null && result.Count > 0)
                        {
                            list = result.Where(w => w.Status == 1).Select(s => new ZoneWithSimpleFieldEntity
                            {
                                Id = s.Id,
                                Name = s.Name,
                                ParentId = s.ParentId,
                                ShortUrl = s.ShortUrl,
                                SortOrder = s.SortOrder,
                                Invisibled = s.Invisibled,
                                Status = s.Status
                            }).ToList();
                        }
                    }
                    else if(user.User.IsFullZone && !user.User.IsFullPermission)
                    {
                        var listPermistion = listPermissionId.Split(';').ToList();
                        if (user.PermissionList.Where(s => listPermistion.Any(a => a.Equals(s.PermissionId.ToString()))).Count()>0)
                        {
                            var result = _db.GetsFromHash<ZoneEntity>();
                            if (result != null && result.Count > 0)
                            {
                                list = result.Where(w => w.Status == 1).Select(s => new ZoneWithSimpleFieldEntity
                                {
                                    Id = s.Id,
                                    Name = s.Name,
                                    ParentId = s.ParentId,
                                    ShortUrl = s.ShortUrl,
                                    SortOrder = s.SortOrder,
                                    Invisibled = s.Invisibled,
                                    Status = s.Status
                                }).ToList();
                            }
                        }
                    }
                    else if (!user.User.IsFullZone && user.User.IsFullPermission)
                    {
                        var listZone = user.PermissionList.Select(s => s.ZoneId.ToString()).Distinct().ToList();
                        if (listZone != null && listZone.Count() > 0)
                        {
                            var result = new List<ZoneEntity>();
                            foreach (var item in listZone)
                            {
                                result.AddRange(_db.GetsFromHash<ZoneEntity>().Where(w => w.Status == 1 && w.ParentId.ToString().Equals(item)).ToList());
                            }

                            result.AddRange(_db.GetsFromHash<ZoneEntity>(listZone).Where(w => w.Status == 1));
                            if (result != null && result.Count > 0)
                            {
                                list = result.Select(s => new ZoneWithSimpleFieldEntity
                                {
                                    Id = s.Id,
                                    Name = s.Name,
                                    ParentId = s.ParentId,
                                    ShortUrl = s.ShortUrl,
                                    SortOrder = s.SortOrder,
                                    Invisibled = s.Invisibled,
                                    Status = s.Status
                                }).ToList();
                            }
                        }
                    }
                    else
                    {
                        var listZone = user.PermissionList.Select(s => s.ZoneId.ToString()).Distinct().ToList();
                        if (listZone != null && listZone.Count()>0)
                        {
                            var result = new List<ZoneEntity>();
                            foreach (var item in listZone) {
                                result.AddRange(_db.GetsFromHash<ZoneEntity>().Where(w => w.Status == 1 && w.ParentId.ToString().Equals(item)).ToList());
                            }
                            
                            result.AddRange(_db.GetsFromHash<ZoneEntity>(listZone).Where(w => w.Status == 1));
                            if (result != null && result.Count > 0)
                            {
                                list = result.Select(s => new ZoneWithSimpleFieldEntity
                                {
                                    Id = s.Id,
                                    Name = s.Name,
                                    ParentId = s.ParentId,
                                    ShortUrl = s.ShortUrl,
                                    SortOrder = s.SortOrder,
                                    Invisibled = s.Invisibled,
                                    Status = s.Status
                                }).ToList();
                            }
                        }
                    }                    
                }
                return list;
            }
            catch
            {
                return null;
            }
        }
        public bool InitAllZone(List<ZoneEntity> list)
        {
            try
            {
                var data = new Dictionary<string, ZoneEntity>();
                foreach (var entry in list)
                {
                    data[entry.Id.ToString()] = entry;
                }     

                return _db.AddInHash<ZoneEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected ZoneDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
