﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Zone;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Zone
{
    public class ZoneDalFactory
    {
        public static ZoneEntity GetZoneById(int zoneId)
        {
            ZoneEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneById(zoneId);
            }
            return returnValue;
        }

        public static List<ZoneWithSimpleFieldEntity> GetZoneActivedByParentId(int parentId)
        {
            List<ZoneWithSimpleFieldEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneActivedByParentId(parentId);
            }
            return returnValue;
        }

        public static List<ZoneEntity> GetZoneByParentId(int parentId)
        {
            List<ZoneEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneByParentId(parentId);
            }
            return returnValue;
        }

        public static bool AddZone(ZoneEntity data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.AddZone(data);
            }
            return returnValue;
        }
        public static bool ToggleZoneInvisibled(int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.ToggleZoneInvisibled(zoneId);
            }
            return returnValue;
        }
        public static bool ToggleZoneStatus(int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.ToggleZoneStatus(zoneId);
            }
            return returnValue;
        }
        public static bool ToggleZoneAllowComment(int zoneId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.ToggleZoneAllowComment(zoneId);
            }
            return returnValue;
        }

        public static List<ZoneEntity> GetZoneByNewsId(long newsId)
        {
            List<ZoneEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<ZoneEntity> GetZoneByUserId(int userId)
        {
            List<ZoneEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneByUserId(userId);
            }
            return returnValue;
        }

        public static ZoneEntity GetPrimaryZoneByNewsId(long newsId)
        {
            ZoneEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetPrimaryZoneByNewsId(newsId);
            }
            return returnValue;
        }

        public static List<ZoneWithSimpleFieldEntity> GetZoneActivedByUsernameAndPermissionIds(string username, string listPermissionId)
        {
            List<ZoneWithSimpleFieldEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.GetZoneActivedByUsernameAndPermissionIds(username, listPermissionId);
            }
            return returnValue;
        }

        public static bool InitAllZone(List<ZoneEntity> data)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.ZoneDal.InitAllZone(data);
            }
            return returnValue;
        }
    }
}
