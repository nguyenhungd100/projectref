﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsChildVersion
{
    public abstract class NewsChildVersionDalBase
    {
        public bool AddNewsChildVersion(NewsChildVersionEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsChildVersionEntity>(news.NewsVersionId.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsChildVersionByNewsId(long newsId, List<NewsChildVersionEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.ListNewsChild = list.Select(s=>new NewsChildEntity {
                        NewsParentId=s.NewsParentId,
                        Order=s.Order,
                        Body=s.Body,
                        Title=s.Title
                    }).ToList();
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId,bool isGetContent)
        {
            var result = new List<NewsChildVersionEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.ListNewsChild.Select(s=>new NewsChildVersionEntity {
                        NewsParentId=s.NewsParentId,
                        Order=s.Order,
                        Body=s.Body
                    }).ToList();
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsChildVersionDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
