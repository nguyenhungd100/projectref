﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsChildVersion
{
    public class NewsChildVersionDalFactory
    {
        public static bool AddNewsChildVersion(NewsChildVersionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildVersionDal.AddNewsChildVersion(news);
            }
            return returnValue;
        }
        public static bool AddNewsChildVersionByNewsId(long newsId, List<NewsChildVersionEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildVersionDal.AddNewsChildVersionByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsChildVersionEntity> GetNewsChildVersionByNewsId(long newsId,bool isGetContent)
        {
            List<NewsChildVersionEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsChildVersionDal.GetNewsChildVersionByNewsId(newsId, isGetContent);
            }
            return returnValue;
        }
    }
}
