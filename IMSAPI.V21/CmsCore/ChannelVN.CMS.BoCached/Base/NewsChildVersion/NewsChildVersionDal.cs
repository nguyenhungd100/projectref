﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.NewsChildVersion
{
    public class NewsChildVersionDal: NewsChildVersionDalBase
    {
        internal NewsChildVersionDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
