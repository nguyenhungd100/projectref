﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Vote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.Vote
{
    public class VoteDalFactory
    {
        public static bool AddVote(VoteEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VoteDal.AddVote(vote);
            }
            return returnValue;
        }
        public static bool AddVoteByNewsId(long newsId,VoteEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VoteDal.AddVoteByNewsId(newsId,vote);
            }
            return returnValue;
        }
        public static VoteEntity GetVoteByNewsId(long newsId)
        {
            VoteEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VoteDal.GetVoteByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<VoteEntity> GetVoteByListId(List<string> voteIds)
        {
            List<VoteEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VoteDal.GetVoteByListId(voteIds);
            }
            return returnValue;
        }

        public static bool InitAllVote(List<VoteEntity> listVote)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VoteDal.InitAllVote(listVote);
            }
            return returnValue;
        }
    }
}
