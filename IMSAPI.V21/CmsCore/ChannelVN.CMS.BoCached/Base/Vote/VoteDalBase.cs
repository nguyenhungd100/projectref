﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.Vote
{
    public abstract class VoteDalBase
    {
        public bool AddVote(VoteEntity vote)
        {
            try
            {
                if (null == vote)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<VoteEntity>(vote.Id.ToString(), vote);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddVoteByNewsId(long newsId, VoteEntity vote)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.VoteInfo = vote;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public VoteEntity GetVoteByNewsId(long newsId)
        {
            var result = new VoteEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.VoteInfo;
                }
                                 
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<VoteEntity> GetVoteByListId(List<string> voteIds)
        {
            try
            {
                return _db.GetsFromHash<VoteEntity>(voteIds);
            }
            catch
            {
                return null;
            }
        }
        public bool InitAllVote(List<VoteEntity> listVote)
        {
            try
            {
                var data = new Dictionary<string, VoteEntity>();
                foreach (var entry in listVote)
                {
                    data[entry.Id.ToString()] = entry;
                }
                return _db.AddInHash<VoteEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected VoteDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
