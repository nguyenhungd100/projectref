﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.NewsNotify;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsNotify
{
    public class NewsNotifyDalFactory
    {
        public static bool AddNewsNotify(NewsNotifyEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsNotifyDal.AddNewsNotify(news);
            }
            return returnValue;
        }                       
    }
}
