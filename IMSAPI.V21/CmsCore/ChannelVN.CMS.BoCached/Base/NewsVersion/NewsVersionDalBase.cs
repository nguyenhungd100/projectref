﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.BoCached.Entity.News;

namespace ChannelVN.CMS.BoCached.Base.NewsVersion
{
    public abstract class NewsVersionDalBase
    {
        public bool AddNewsVersion(NewsVersionEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }                                
                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsVersionEntity>(news.Id.ToString(), news);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool UpdateFirstVersion(NewsEntity news)
        {
            try
            {
                if (null == news)
                {
                    return false;
                }

                var newsversion = new NewsVersionEntity
                {
                    NewsId = news.Id,
                    ListZoneId = news.ListZoneId,
                    Title = news.Title,
                    SubTitle = news.SubTitle,
                    Sapo = news.Sapo,
                    Body = news.Body,
                    Avatar = news.Avatar,
                    AvatarDesc = news.AvatarDesc,
                    Avatar2 = news.Avatar2,
                    Avatar3 = news.Avatar3,
                    Avatar4 = news.Avatar4,
                    Avatar5 = news.Avatar5,
                    Author = news.Author,
                    NewsRelation = news.NewsRelation,
                    Status = news.Status,
                    Source = news.Source,
                    IsFocus = news.IsFocus,
                    Type = news.Type,
                    ThreadId = news.ThreadId,
                    CreatedDate = news.CreatedDate,
                    LastModifiedDate = news.LastModifiedDate,
                    DistributionDate = news.DistributionDate,
                    CreatedBy = news.CreatedBy,
                    LastModifiedBy = news.LastModifiedBy,
                    PublishedBy = news.PublishedBy,
                    EditedBy = news.EditedBy,
                    LastReceiver = news.LastReceiver,
                    WordCount = news.WordCount,
                    ViewCount = news.ViewCount,
                    Priority = news.Priority,
                    Tag = news.Tag,
                    Note = news.Note,
                    //CreateVersionBy= usernameForUpdateAction,                        
                    DisplayStyle = news.DisplayStyle,
                    DisplayPosition = news.DisplayPosition,
                    DisplayInSlide = news.DisplayInSlide,
                    AvatarCustom = news.AvatarCustom,
                    IsOnHome = news.IsOnHome,
                    Price = news.Price.HasValue?news.Price.Value:0,
                    OriginalId = news.OriginalId,
                    TagItem = news.TagItem,
                    Url = news.Url,
                    NoteRoyalties = news.NoteRoyalties,
                    NewsCategory = news.NewsCategory,
                    InitSapo = news.InitSapo,
                    ShortTitle = news.ShortTitle
                };

                var tasks = new List<Task>
                {
                    Task.Run( () => {
                        _db.AddInHash<NewsVersionEntity>(newsversion.NewsId.ToString(), newsversion);
                    })
                };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool AddNewsVersionByNewsId(long newsId, List<NewsVersionWithSimpleFieldsEntity> list)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    data.ListVersion = list;
                }
                var tasks = new List<Task>
                    {
                        Task.Run( () => {
                            _db.AddInHash<NewsCachedEntity>(newsId.ToString(),data);
                        })
                    };

                var result = Task.WaitAny(tasks.ToArray());
                return result > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<NewsVersionWithSimpleFieldsEntity> GetNewsVersionByNewsId(long newsId)
        {
            var result = new List<NewsVersionWithSimpleFieldsEntity>();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    result = news.ListVersion;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected NewsVersionDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
