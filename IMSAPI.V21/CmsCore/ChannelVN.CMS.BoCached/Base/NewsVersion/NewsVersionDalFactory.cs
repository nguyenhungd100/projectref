﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.News;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.NewsVersion
{
    public class NewsVersionDalFactory
    {
        public static bool AddNewsVersion(NewsVersionEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsVersionDal.AddNewsVersion(news);
            }
            return returnValue;
        }
        public static bool UpdateFirstVersion(NewsEntity news)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsVersionDal.UpdateFirstVersion(news);
            }
            return returnValue;
        }
        public static bool AddNewsVersionByNewsId(long newsId, List<NewsVersionWithSimpleFieldsEntity> list)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsVersionDal.AddNewsVersionByNewsId(newsId, list);
            }
            return returnValue;
        }
        public static List<NewsVersionWithSimpleFieldsEntity> GetNewsVersionByNewsId(long newsId)
        {
            List<NewsVersionWithSimpleFieldsEntity> returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.NewsVersionDal.GetNewsVersionByNewsId(newsId);
            }
            return returnValue;
        }
    }
}
