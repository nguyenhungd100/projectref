﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.VideoChannel
{
    public class VideoChannelDalFactory
    {
        public static bool AddVideoChannel(VideoChannelEntity videoChannel)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.AddVideoChannel(videoChannel);
            }
            return returnValue;
        }
        public static bool AddVideoChannelByNewsId(long newsId,VideoChannelEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.AddVideoChannelByNewsId(newsId,vote);
            }
            return returnValue;
        }
        public static VideoChannelEntity GetVideoChannelByNewsId(long newsId)
        {
            VideoChannelEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.GetVideoChannelByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<VideoChannelCachedEntity> GetVideoChannelByListId(List<string> Ids)
        {
            List<VideoChannelCachedEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.GetVideoChannelByListId(Ids);
            }
            return returnValue;
        }
        public static VideoChannelEntity GetVideoChannelById(int id)
        {
            VideoChannelEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.GetVideoChannelById(id);
            }
            return returnValue;
        }
        public static List<VideoChannelEntity> GetVideoChannelByVideoId(int videoId)
        {
            List<VideoChannelEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.GetVideoChannelByVideoId(videoId);
            }
            return returnValue;
        }

        public static bool AddPlayListByVideoChannelId(int videoChannelId, List<PlaylistEntity> playList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.AddPlayListByVideoChannelId(videoChannelId, playList);
            }
            return returnValue;
        }

        public static List<PlaylistEntity> GetPlayListByVideoChannelId(int videoChannelId)
        {
            List<PlaylistEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.GetPlayListByVideoChannelId(videoChannelId);
            }
            return returnValue;
        }

        public static bool AddVideoChannelListByVideoId(int videoId, List<VideoChannelEntity> videoChannelList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.AddVideoChannelListByVideoId(videoId, videoChannelList);
            }
            return returnValue;
        }

        public static bool Delete(int channelId)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.Delete(channelId);
            }
            return returnValue;
        }

        public static bool InitAllVideoChannel(List<VideoChannelEntity> listVideoChannel)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.VideoChannelDal.InitAllVideoChannel(listVideoChannel);
            }
            return returnValue;
        }
    }
}
