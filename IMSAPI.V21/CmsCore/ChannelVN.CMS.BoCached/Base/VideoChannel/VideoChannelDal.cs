﻿using ChannelVN.CMS.BoCached.Databases;

namespace ChannelVN.CMS.BoCached.Base.VideoChannel
{
    public class VideoChannelDal: VideoChannelDalBase
    {
        internal VideoChannelDal(CmsMainCachedDb db)
                : base(db)
        {
        }
    }
}
