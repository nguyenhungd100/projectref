﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.BoCached.Entity.Video;

namespace ChannelVN.CMS.BoCached.Base.VideoChannel
{
    public abstract class VideoChannelDalBase
    {
        public bool AddVideoChannel(VideoChannelEntity videoChannel)
        {
            try
            {
                if (null == videoChannel)
                {
                    return false;
                }
                var channelCached = _db.GetFromHash<VideoChannelCachedEntity>(videoChannel.Id.ToString());
                if (null == channelCached)
                {
                    channelCached = new VideoChannelCachedEntity
                    {
                        VideoChannelInfo = videoChannel,
                        PlayList = null,
                        ZoneVideoList=null
                    };
                }
                else
                {
                    channelCached.VideoChannelInfo = videoChannel;
                    channelCached.PlayList = null;
                    channelCached.ZoneVideoList = null;
                }

                _db.RemoveFromHash<VideoChannelCachedEntity>(videoChannel.Id.ToString());

                return _db.AddInHash(videoChannel.Id.ToString(), channelCached);                
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool AddVideoChannelByNewsId(long newsId, VideoChannelEntity vote)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    //data.VideoChannelInfo = vote;
                }
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public VideoChannelEntity GetVideoChannelByNewsId(long newsId)
        {
            var result = new VideoChannelEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    //result = news.VideoChannelInfo;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<VideoChannelCachedEntity> GetVideoChannelByListId(List<string> Ids)
        {
            try
            {
                return _db.GetsFromHash<VideoChannelCachedEntity>(Ids);
            }
            catch
            {
                return null;
            }
        }
        public VideoChannelEntity GetVideoChannelById(int id)
        {
            var channel = new VideoChannelEntity();
            try
            {
                var data= _db.GetFromHash<VideoChannelCachedEntity>(id.ToString());
                if (data!=null)
                {
                    channel = data.VideoChannelInfo;
                }
                return channel;
            }
            catch
            {
                return channel;
            }
        }

        public List<VideoChannelEntity> GetVideoChannelByVideoId(int videoId)
        {
            try
            {
                var list = new List<VideoChannelEntity>();
                var replies = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != replies)
                {
                    list = replies.VideoChannel;
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool AddPlayListByVideoChannelId(int videoChannelId, List<PlaylistEntity> playList)
        {
            try
            {
                var data = _db.GetFromHash<VideoChannelCachedEntity>(videoChannelId.ToString());
                if (null != data)
                {
                    data.PlayList = playList;
                }
                return _db.AddInHash(videoChannelId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public List<PlaylistEntity> GetPlayListByVideoChannelId(int videoChannelId)
        {
            try
            {
                var list = new List<PlaylistEntity>();
                var replies = _db.GetFromHash<VideoChannelCachedEntity>(videoChannelId.ToString());
                if (null != replies)
                {
                    list = replies.PlayList;
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public bool AddVideoChannelListByVideoId(int videoId, List<VideoChannelEntity> videoChannelList)
        {
            try
            {
                var data = _db.GetFromHash<VideoCachedEntity>(videoId.ToString());
                if (null != data)
                {
                    data.VideoChannel = videoChannelList;
                }
                return _db.AddInHash(videoId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int channelId)
        {
            try
            {
                return _db.RemoveFromHash<VideoChannelCachedEntity>(channelId.ToString());
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool InitAllVideoChannel(List<VideoChannelEntity> listVideoChannel)
        {
            try
            {
                var data = new Dictionary<string, VideoChannelCachedEntity>();
                foreach (var entry in listVideoChannel)
                {
                    data[entry.Id.ToString()] = new VideoChannelCachedEntity() { VideoChannelInfo = entry }; ;
                }
                return _db.AddInHash<VideoChannelCachedEntity>(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected VideoChannelDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
