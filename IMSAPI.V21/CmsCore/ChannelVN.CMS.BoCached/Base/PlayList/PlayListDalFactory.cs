﻿using ChannelVN.CMS.BoCached.Databases;
using ChannelVN.CMS.Entity.Base.Video;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Base.PlayList
{
    public class PlayListDalFactory
    {
        public static bool AddPlayList(PlaylistEntity playList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.AddPlayList(playList);
            }
            return returnValue;
        }
        public static bool AddPlayListByNewsId(long newsId,PlaylistEntity vote)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.AddPlayListByNewsId(newsId,vote);
            }
            return returnValue;
        }
        public static PlaylistEntity GetPlayListByNewsId(long newsId)
        {
            PlaylistEntity returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.GetPlayListByNewsId(newsId);
            }
            return returnValue;
        }
        public static List<PlayListCachedEntity> GetPlayListByListId(List<string> playListIds)
        {
            List<PlayListCachedEntity> returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.GetPlayListByListId(playListIds);
            }
            return returnValue;
        }
        public static PlaylistEntity GetPlayListById(int id)
        {
            PlaylistEntity returnValue = null;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.GetPlayListById(id);
            }
            return returnValue;
        }
        public static bool Publish(int playListId, string account)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.Publish(playListId, account);
            }
            return returnValue;
        }
        public static bool UnPublish(int playListId, string account)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.UnPublish(playListId, account);
            }
            return returnValue;
        }
        public static bool MoveTrash(int playListId, string account)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.MoveTrash(playListId, account);
            }
            return returnValue;
        }
        public static bool Send(int playListId, int status, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.Send(playListId, status, username);
            }
            return returnValue;
        }
        public static bool Return(int playListId, string username)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.Return(playListId, username);
            }
            return returnValue;
        }
        public static bool ChangeMode(string account, int playListId, int mode)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.ChangeMode(account, playListId, mode);
            }
            return returnValue;
        }
        public static bool Delete(List<string> playlistIdList)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.Delete(playlistIdList);
            }
            return returnValue;
        }
        public static bool RemoveVideoOutOfPlaylist(string hashName)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.RemoveVideoOutOfPlaylist(hashName);
            }
            return returnValue;
        }

        public static bool AddListZoneRelationByPlayListId(int playListId, List<ZoneVideoEntity> listZoneRelation)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.AddListZoneRelationByPlayListId(playListId, listZoneRelation);
            }
            return returnValue;
        }

        public static bool AddListZoneRelationByChannelId(int channelId, List<ZoneVideoEntity> listZoneRelation)
        {
            bool returnValue;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.AddListZoneRelationByChannelId(channelId, listZoneRelation);
            }
            return returnValue;
        }

        public static bool InitAllPlayList(List<PlaylistEntity> listPlayList)
        {
            bool returnValue = false;
            using (var db = new CmsMainCachedDb())
            {
                returnValue = db.PlayListDal.InitAllPlayList(listPlayList);
            }
            return returnValue;
        }
    }
}
