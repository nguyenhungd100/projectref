﻿using ChannelVN.CMS.BoCached.Databases;
using System;
using System.Collections.Generic;
using ChannelVN.CMS.BoCached.Entity.News;
using ChannelVN.CMS.Entity.Base.Video;

namespace ChannelVN.CMS.BoCached.Base.PlayList
{
    public abstract class PlayListDalBase
    {
        public bool AddPlayList(PlaylistEntity playList)
        {
            try
            {
                if (null == playList)
                {
                    return false;
                }
                var data = _db.GetFromHash<PlayListCachedEntity>(playList.Id.ToString());
                if (data != null)
                {
                    data.PlaylistInfo = playList;
                    data.ZoneVideoList = null;
                }
                else
                {
                    data = new PlayListCachedEntity();
                    data.PlaylistInfo = playList;
                    data.ZoneVideoList = null;
                }
                _db.RemoveFromHash<PlayListCachedEntity>(playList.Id.ToString());

                return _db.AddInHash(playList.Id.ToString(), data);                
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool AddPlayListByNewsId(long newsId, PlaylistEntity vote)
        {
            try
            {
                var data = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (null != data)
                {
                    //data.PlayListInfo = vote;
                }
  
                return _db.AddInHash(newsId.ToString(), data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public PlaylistEntity GetPlayListByNewsId(long newsId)
        {
            var result = new PlaylistEntity();
            try
            {
                var news = _db.GetFromHash<NewsCachedEntity>(newsId.ToString());
                if (news != null)
                {
                    //result = news.PlayListInfo;
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }
        public List<PlayListCachedEntity> GetPlayListByListId(List<string> playListIds)
        {
            try
            {
                return _db.GetsFromHash<PlayListCachedEntity>(playListIds);
            }
            catch
            {
                return null;
            }
        }
        public PlaylistEntity GetPlayListById(int id)
        {
            try
            {
                var result = new PlaylistEntity();
                var data= _db.GetFromHash<PlayListCachedEntity>(id.ToString());
                if (data != null)
                {
                    result = data.PlaylistInfo;
                }
                return result;
            }
            catch
            {
                return null;
            }
        }
        public bool Publish(int playListId, string account)
        {
            try
            {
                var playList = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (playList != null)
                {
                    playList.PlaylistInfo.Status = (int)EnumPlayListStatus.Published;
                    playList.PlaylistInfo.LastModifiedBy = account;
                    playList.PlaylistInfo.LastModifiedDate = DateTime.Now;
                    playList.PlaylistInfo.PublishedBy = account;
                    playList.PlaylistInfo.PublishedDate = DateTime.Now;                    
                }
                _db.RemoveFromHash<PlayListCachedEntity>(playListId.ToString());

                return _db.AddInHash(playListId.ToString(), playList);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UnPublish(int playListId, string account)
        {
            try
            {
                var playList = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (playList != null)
                {
                    playList.PlaylistInfo.Status = (int)EnumPlayListStatus.Unpublished;
                    playList.PlaylistInfo.LastModifiedBy = account;
                    playList.PlaylistInfo.LastModifiedDate = DateTime.Now;
                }
                _db.RemoveFromHash<PlayListCachedEntity>(playListId.ToString());

                return _db.AddInHash(playListId.ToString(), playList);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool MoveTrash(int playListId, string account)
        {
            try
            {
                var playList = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (playList != null)
                {
                    playList.PlaylistInfo.Status = (int)EnumPlayListStatus.MoveTrash;
                    playList.PlaylistInfo.LastModifiedBy = account;
                    playList.PlaylistInfo.LastModifiedDate = DateTime.Now;
                }
                _db.RemoveFromHash<PlayListCachedEntity>(playListId.ToString());

                return _db.AddInHash(playListId.ToString(), playList);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Send(int playListId, int status, string username)
        {
            try
            {
                var data = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (null != data)
                {
                    data.PlaylistInfo.Status = status;
                    data.PlaylistInfo.LastModifiedBy = username;
                    data.PlaylistInfo.LastModifiedDate = DateTime.Now;

                    if (status == (int)EnumPlayListStatus.WaitForPublished)
                        data.PlaylistInfo.EditedBy = username;

                    _db.RemoveFromHash<PlayListCachedEntity>(playListId.ToString());

                    return _db.AddInHash(playListId.ToString(), data);
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        public bool Return(int playListId, string username)
        {
            try
            {
                var data = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (null != data)
                {
                    data.PlaylistInfo.Status = (int)EnumPlayListStatus.ReturnForEditor;
                    data.PlaylistInfo.LastModifiedBy = username;
                    data.PlaylistInfo.LastModifiedDate = DateTime.Now;
                }
                return _db.AddInHash(playListId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool ChangeMode(string account, int playListId, int mode)
        {
            try
            {
                var playList=_db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (playList != null)
                {
                    playList.PlaylistInfo.Mode = mode;
                    playList.PlaylistInfo.LastModifiedBy = account;
                    playList.PlaylistInfo.LastModifiedDate = DateTime.Now;
                }
                _db.RemoveFromHash<PlayListCachedEntity>(playListId.ToString());

                return _db.AddInHash(playListId.ToString(), playList);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(List<string> playlistIdList)
        {
            try
            {               
                return _db.RemoveMuiltiFromHash<PlayListCachedEntity>(playlistIdList);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool RemoveVideoOutOfPlaylist(string hashName)
        {
            try
            {
                return _db.Remove(hashName);
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool AddListZoneRelationByPlayListId(int playListId, List<ZoneVideoEntity> listZoneRelation)
        {
            try
            {
                var data = _db.GetFromHash<PlayListCachedEntity>(playListId.ToString());
                if (null != data)
                {
                    data.ZoneVideoList = listZoneRelation;
                }

                return _db.AddInHash(playListId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool AddListZoneRelationByChannelId(int channelId, List<ZoneVideoEntity> listZoneRelation)
        {
            try
            {
                var data = _db.GetFromHash<VideoChannelCachedEntity>(channelId.ToString());
                if (null != data)
                {
                    data.ZoneVideoList = listZoneRelation;
                }

                return _db.AddInHash(channelId.ToString(), data);
            }
            catch
            {
                return false;
            }
        }
        public bool InitAllPlayList(List<PlaylistEntity> listPlayList)
        {
            try
            {
                var data = new Dictionary<string, PlayListCachedEntity>();
                foreach (var entry in listPlayList)
                {
                    data[entry.Id.ToString()] = new PlayListCachedEntity() { PlaylistInfo = entry };
                }
                return _db.AddInHash(data);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Core members

        private readonly CmsMainCachedDb _db;

        protected PlayListDalBase(CmsMainCachedDb db)
        {
            _db = db;
        }

        protected CmsMainCachedDb Database
        {
            get { return _db; }
        }

        #endregion
    }
}
