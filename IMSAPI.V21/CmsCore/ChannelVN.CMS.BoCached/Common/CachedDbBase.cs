﻿using ChannelVN.CMS.Common.RedisClientHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Common
{
    public abstract class CachedDbBase : RedisClientHelper2
    {
        #region Constructors

        protected CachedDbBase()
            : this(string.Empty)
        {
        }

        protected CachedDbBase(string channelNamespace)
            : base(channelNamespace)
        {
        }

        #endregion
    }
}
