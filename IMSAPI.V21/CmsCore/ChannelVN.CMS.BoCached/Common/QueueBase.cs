﻿using System.Collections.Generic;
using System.Linq;

namespace ChannelVN.CMS.BoCached.Common
{
    public class QueueBase<T> where T:class
    {
        private readonly object lockObject = new object();
        private readonly SortedList<string, Queue<T>> list = new SortedList<string, Queue<T>>();

        public int Count
        {
            get
            {
                lock (lockObject)
                {
                    return list.Sum(keyValuePair => keyValuePair.Value.Count);
                }
            }
        }
        public int CountPriority(string priority)
        {            
            lock (lockObject)
            {
                return list.Sum(keyValuePair => keyValuePair.Value.Count);
                //if (list.ContainsKey(priority))                                   
                //    return list[priority].Count;                
            }            
        }

        public void Push(string priority, T item)
        {
            lock (lockObject)
            {
                if (!list.ContainsKey(priority))
                    list.Add(priority, new Queue<T>());
                list[priority].Enqueue(item);
            }
        }
        public T Pop()
        {
            lock (lockObject)
            {
                if (list.Count > 0)
                {
                    T obj = list.First().Value.Dequeue();
                    if (list.First().Value.Count == 0)
                        list.Remove(list.First().Key);
                    return obj;
                }
            }
            return null;
        }
        public T PopPriority(string priority)
        {
            lock (lockObject)
            {
                if (list.ContainsKey(priority))
                {
                    T obj = list[priority].Dequeue();
                    if (list[priority].Count == 0)
                        list.Remove(priority);
                    return obj;
                }
            }
            return null;
        }
        public IEnumerable<T> PopAllPriority(string priority)
        {
            List<T> ret = new List<T>();
            lock (lockObject)
            {
                if (list.ContainsKey(priority))
                {
                    while (list.ContainsKey(priority) && list[priority].Count > 0)
                        ret.Add(PopPriority(priority));
                    return ret;
                }
            }
            return ret;
        }
        public T Peek()
        {
            lock (lockObject)
            {
                if (list.Count > 0)
                    return list.First().Value.Peek();
            }
            return null;
        }
        public IEnumerable<T> PeekAll()
        {
            List<T> ret = new List<T>();
            lock (lockObject)
            {
                foreach (KeyValuePair<string, Queue<T>> keyValuePair in list)
                    ret.AddRange(keyValuePair.Value.AsEnumerable());
            }
            return ret;
        }
        public IEnumerable<T> PopAll()
        {
            List<T> ret = new List<T>();
            lock (lockObject)
            {
                while (list.Count > 0)
                    ret.Add(Pop());
            }
            return ret;
        }
    }
}
