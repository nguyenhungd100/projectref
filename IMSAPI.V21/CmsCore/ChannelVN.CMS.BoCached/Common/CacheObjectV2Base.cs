﻿using ChannelVN.CMS.BoCached.CacheLib;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ChannelVN.CMS.BoCached.Common
{
    public abstract class CacheObjectV2Base
    {
        private static readonly Assembly CurrentAssembly = Assembly.GetExecutingAssembly();
        private static ICached _cached;
        private static ICached Cached
        {
            get
            {
                if (_cached == null)
                {
                    try
                    {
                        var assemblyInString = ServiceChannelConfiguration.GetAppSetting(WcfMessageHeader.Current.Namespace, "BoCached.CacheType");
                        if (string.IsNullOrEmpty(assemblyInString))
                        {
                            _cached = new NoCache();
                        }
                        else
                        {
                            _cached = (ICached)Activator.CreateInstance(Type.GetType(assemblyInString));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
                        _cached = new NoCache();
                    }
                }
                return _cached;
            }
        }

        protected static bool Add<T>(string key, T value, int cachedExpiredInSeconds)
        {
            if (cachedExpiredInSeconds <= 0) cachedExpiredInSeconds = 60;
            return Cached.Add(key, value, DateTime.Now.AddSeconds(cachedExpiredInSeconds));
        }
        protected static bool Remove(string key)
        {
            return Cached.Remove(key);
        }
        protected static bool Exists(string key)
        {
            return Cached.Exists(key);
        }
        protected static T Get<T>(string key)
        {
            return Cached.Get<T>(key);
        }
        public abstract void RemoveCachedByPrefix(string cachePrefix);
    }
}
