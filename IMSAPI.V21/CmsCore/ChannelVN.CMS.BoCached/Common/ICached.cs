﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Common
{
    public interface ICached
    {
        bool Add<T>(string key, T value);
        bool Add<T>(string key, T value, DateTime expiredDate);
        bool Remove(string key);
        bool Exists(string key);
        T Get<T>(string key);
    }
}
