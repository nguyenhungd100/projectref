﻿using ChannelVN.CMS.BoCached.Entity.Init;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ChannelVN.CMS.BoCached.Common
{
    public class Queue
    {
        private static CancellationTokenSource _cts;

        private static QueueBase<Section> queueBase = new QueueBase<Section>();

        public Queue() {
            queueBase = new QueueBase<Section>();
        }

        private static bool AddQueue(string id,int num, int startPage=1)
        {
            if (startPage < 0)
            {
                startPage = 1;
            }
            if (startPage > num)
            {
                startPage = num;
            }
            for (int i = startPage; i <= num; i++)
            {
                queueBase.Push(id, new Section
                {
                    Page = i,
                    Message = "Đang sync..."
                });                
            }
            return true;
        }

        public static LogInitAsyncEntity InitSyncData(string name, int startPage, DateTime startDate, DateTime endDate, Func<int> fc, Action<int> action,string act=null)
        {
            string id = name + "_" + startDate.Ticks + "_" + endDate.Ticks;
            var logSync = CmsExtension.LogInitAsync.LogInitAsyncDalFactory.GetLogInitAsyncById(id);
            try {
                if (null == logSync)
                {
                    var num = fc();
                    logSync = new LogInitAsyncEntity
                    {
                        Id = id,
                        Status = 0,
                        Total = num,
                        Date = DateTime.Now,
                        Message = "Bắt đầu sync...",
                        Sections = null
                    };
                    CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                    //sync                
                    DoSyncData(id, num, startPage, logSync, action);
                }
                else
                {
                    if (logSync.Status == 0)
                    {
                        logSync.Message = "Đang sync...";
                        if (act == "stop")
                        {
                            //var num = fc();
                            var num = 0;
                            logSync.Status = 1;
                            logSync.Message = "Kết thúc sync.";
                            //sync
                            DoSyncData(id, num, startPage, logSync, action, act);
                        }
                        CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                    }
                    else
                    {
                        if (act == "start")
                        {
                            var num = fc();
                            logSync = new LogInitAsyncEntity
                            {
                                Id = id,
                                Status = 0,
                                Total = num,
                                Date = DateTime.Now,
                                Message = "Bắt đầu sync...",
                                Sections = null
                            };
                            CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                            //sync
                            DoSyncData(id, num, startPage, logSync, action, act);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                logSync = new LogInitAsyncEntity
                {
                    Id = id,
                    Status = 0,
                    Total = 0,
                    Date = DateTime.Now,
                    Message = "InitSyncData: "+ex.Message,
                    Sections = null
                };
                return logSync;
            }
            return logSync;
        }

        private static async void DoSyncData(string id, int num, int startPage, LogInitAsyncEntity logSync, Action<int> action,string act=null)
        {            
            if (act == null || act=="start")
            {
                _cts=new CancellationTokenSource();
                var token = _cts.Token;
                
                if (logSync.Sections == null) { logSync.Sections = new List<Section>(); }
                await Task.Run(async () =>
                {
                    try
                    {
                        if (AddQueue(id, num, startPage))
                        {
                            while (queueBase.CountPriority(id) > 0)
                            {
                                if (token.IsCancellationRequested)
                                {
                                    queueBase.PopAllPriority(id);

                                    logSync.Status = 0;
                                    logSync.Message = "PopAllPriority: " + id;
                                    CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);

                                    break;
                                }

                                Section currentElement = queueBase.PopPriority(id);
                                if (currentElement != null)
                                {
                                    logSync.Sections.Add(new Section
                                    {
                                        Page = currentElement.Page,
                                        Message = "Đang sync..."
                                    });
                                    CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);

                                    await Task.Run(() =>
                                    {
                                        try
                                        {
                                            action(currentElement.Page);

                                            //logSync.Sections.Where(s => s.Page == currentElement.Page).Select(s => { s.Message = "Done"; return s; }).ToList();
                                            //CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                                        }
                                        catch (Exception ex)
                                        {
                                            logSync.Status = 0;
                                            logSync.Message = "Action: currentElement.Page: " + currentElement.Page + " Error: " + ex.Message;
                                            CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                                        }
                                    }).ContinueWith(c =>
                                    {
                                        logSync.Sections.Where(s => s.Page == currentElement.Page).Select(s => { s.Message = "Done"; return s; }).ToList();
                                        CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                                    });
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logSync.Status = 0;
                        logSync.Message = "Error QueueBase: " + ex.Message;
                        CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                        //if (_cts != null)
                        //    _cts.Cancel();
                    }
                }, token).ContinueWith(c =>
                {
                    logSync.Status = 1;
                    logSync.Message = "Kết thúc sync.";
                    CmsExtension.LogInitAsync.LogInitAsyncDalFactory.AddLogInitAsync(logSync);
                    //if (_cts != null)
                    //    _cts.Cancel();
                });                
            }else if (act == "stop")
            {
                if (_cts != null)
                    _cts.Cancel();
            }
        }
    }
}
