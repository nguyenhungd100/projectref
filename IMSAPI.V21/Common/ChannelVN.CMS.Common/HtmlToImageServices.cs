﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
//using ChannelVN.CMS.WcfMapping.Cms.Extension.JobManagerServices;

namespace ChannelVN.CMS.Common
{
    public class HtmlToImageServices
    {
        /// <summary>
        /// Gen ảnh từ html
        /// </summary>
        /// <param name="html">mã html để chuyển thành ảnh</param>
        /// <param name="url">đường dẫn website muốn gen thành ảnh, nếu bỏ trống sẽ lấy trường html</param>
        /// <param name="width">độ rộng ảnh xuất ra, mặc định 1024</param>
        /// <param name="zoom">zoom, mặc định 1</param>
        /// <returns></returns>
        public static string Generate(string html = "", string url = "", int width = 1024, float zoom = 0F)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(CmsChannelConfiguration.GetAppSetting("HTML_TO_IMAGE_API_URL"));
                html = HttpContext.Current.Server.UrlEncode(html);
                var sb = new StringBuilder();
                sb.AppendFormat("secretkey={0}", "ahur3478edfjh23losa9d212sj3e2asldoijd29");
                sb.AppendFormat("&html={0}", html);
                sb.AppendFormat("&url={0}", url);
                sb.AppendFormat("&width={0}", width);
                sb.AppendFormat("&zoom={0}", zoom);
                sb.AppendFormat("&type={0}", "json");
                var data = Encoding.UTF8.GetBytes(sb.ToString());
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                var response = (HttpWebResponse)request.GetResponse();
                return new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return "";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="url"></param>
        /// <param name="width"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static Stream GenerateStream(string html = "", string url = "", int width = 1024, float zoom = 0F)
        {
            try
            {
                //html = Uri.EscapeDataString(html);// HttpContext.Current.Server.UrlEncode(html);
                //html = System.Web.HttpUtility.UrlEncode(html);
                //html = html.Replace("+", "%20");
                html = System.Net.WebUtility.UrlEncode(html);
                var request = (HttpWebRequest)WebRequest.Create(CmsChannelConfiguration.GetAppSetting("HTML_TO_IMAGE_API_URL"));

                var sb = new StringBuilder();
                sb.AppendFormat("secret_key={0}", "ahur3478edfjh23losa9d212sj3e2asldoijd29");
                sb.AppendFormat("&html={0}", html);
                sb.AppendFormat("&url={0}", url);
                sb.AppendFormat("&width={0}", width);
                sb.AppendFormat("&zoom={0}", zoom);
                sb.AppendFormat("&type={0}", "stream");
                var data = Encoding.UTF8.GetBytes(sb.ToString());
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                var response = (HttpWebResponse)request.GetResponse();
                return response.GetResponseStream();
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return Stream.Null;
            }
        }
        private const string FormatPathTime = "{0:yyyy}";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="html"></param>
        /// <param name="url"></param>
        /// <param name="imageFileName"></param>
        /// <param name="width"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static string GenerateToImage(string html = "", string url = "", string imageFileName = "", int width = 1024, float zoom = 0F)
        {
            try
            {
                var stream1 = GenerateStream(html, url, width, zoom);
                using (var stream2 = new MemoryStream())
                {
                    stream1.CopyTo(stream2);
                    if (string.IsNullOrEmpty(imageFileName)) imageFileName = DateTime.Now.Ticks + ".png";
                    if (!(imageFileName.EndsWith(".png") || imageFileName.EndsWith(".jpg"))) imageFileName = imageFileName += ".png";

                    var path = string.Format(FormatPathTime, DateTime.Now);
                    var policyData = FileStorageServices.CreatePolicyForUpload("admin", path, imageFileName, true);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var remotePath = policyData[3];
                    var contentType = "image/png";

                    var imageInBytes = stream2.ToArray();
                    stream2.Close();
                    stream2.Dispose();

                    if (FileStorageServices.UploadImage(policy, signature, imageFileName, contentType,
                        "data:" + contentType + ";base64," + Convert.ToBase64String(imageInBytes)))
                    {
                        var url2 = (remotePath + "/" + imageFileName);
                        return CmsChannelConfiguration.GetAppSetting(FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD) + url2;
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return "";
            }
        }
        public static string GenerateToImageBase64(string html = "", string url = "", string imageFileName = "", int width = 1024, float zoom = 0F)
        {
            try
            {
                var stream1 = GenerateStream(html, url, width, zoom);
                using (var stream2 = new MemoryStream())
                {
                    stream1.CopyTo(stream2);
                    if (string.IsNullOrEmpty(imageFileName)) imageFileName = DateTime.Now.Ticks + ".png";
                    if (!(imageFileName.EndsWith(".png") || imageFileName.EndsWith(".jpg"))) imageFileName = imageFileName += ".png";

                    var path = string.Format(FormatPathTime, DateTime.Now);
                    var policyData = FileStorageServices.CreatePolicyForUpload("admin", path, imageFileName, true);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var remotePath = policyData[3];
                    var contentType = "image/png";

                    var imageInBytes = stream2.ToArray();
                    stream2.Close();
                    stream2.Dispose();

                    return "data:" + contentType + ";base64," + Convert.ToBase64String(imageInBytes);
                }
                return "";
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return "";
            }
        }
    }
}
