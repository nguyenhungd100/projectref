using System;
using System.Data;
using System.Xml;
using System.Web.Configuration;

namespace ChannelVN.CMS.Common
{
    public class XmlHelper : IDisposable
    {
        #region properties

        string _filepath = "";

        /// <summary>
        /// Gets or sets value the xml file path 
        /// </summary>
        public string FilePath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }

        string _xpath = "";
        /// <summary>
        /// The xml xpath to define / find a node.
        /// </summary>
        public string XPath
        {
            get { return _xpath; }
            set { _xpath = value; }
        }

        #endregion

        #region constructors

        /// <summary>
        /// Init the XML class with XML file path get from "XMLConnection" string in AppSetiings
        /// </summary>
        public XmlHelper()
        {
            FilePath = WebConfigurationManager.AppSettings["PathDataXml"];
        }

        /// <summary>
        /// Init the XML class with XML file path get defined string
        /// </summary>
        /// <param name="filePath">The path to XML data file.</param>
        public XmlHelper(string filePath)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Init the XML class with XML file path get defined string
        /// </summary>
        /// <param name="filePath">The path to XML data file.</param>
        public XmlHelper(string filePath, bool fullPath)
        {
            if (fullPath)
                FilePath = filePath;
            else
                FilePath = WebConfigurationManager.AppSettings["PathDataXml"] + filePath;
        }

        #endregion

        /// <summary>
        /// Check if the data file exist in configured location.
        /// </summary>
        public bool DataFileExist()
        {
            return DataFileExist(FilePath);
        }

        /// <summary>
        /// Check if the data file exist in configured location.
        /// </summary>
        /// <param name="filePath">The path to XML data file.</param>
        public bool DataFileExist(string filePath)
        {
            if (System.IO.File.Exists(filePath))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Load the data file content to a dataset.
        /// </summary>
        public DataSet LoadDataFileContentToDataSet()
        {
            if (string.IsNullOrEmpty(FilePath))
                return null;
            DataSet ds = new DataSet();
            ds.ReadXml(FilePath);
            if (ds.Tables.Count > 0)
                return ds;
            else
                return null;
        }

        /// <summary>
        /// Load the data file content to a datatable.
        /// </summary>
        public DataTable LoadDataFileContentToDataTable()
        {
            if (string.IsNullOrEmpty(FilePath))
                return null;
            DataSet ds = new DataSet();
            ds.ReadXml(FilePath);
            if (ds.Tables.Count > 0)
                return ds.Tables[0];
            else
                return null;
        }

        /// <summary>
        /// Get xmlnode from xpath.
        /// </summary>
        public XmlNode GetNodeFromXpath()
        {
            return GetNodeFromXpath(XPath);
        }

        /// <summary>
        /// Get xmlnode from xpath.
        /// </summary>
        /// <param name="xpath">The path to XML node to find.</param>
        public XmlNode GetNodeFromXpath(string xpath)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(FilePath);
            XmlNode xnode = xdoc.SelectSingleNode(xpath);
            return xnode;
        }

        /// <summary>
        /// Get xmlnodelist from xpath.
        /// </summary>
        public XmlNodeList GetNodeListFromXpath()
        {
            return GetNodeListFromXpath(XPath);
        }

        /// <summary>
        /// Get xmlnodelist from xpath.
        /// </summary>
        /// <param name="xpath">The path to XML nodes to find.</param>
        public XmlNodeList GetNodeListFromXpath(string xpath)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(FilePath);
            XmlNodeList xnodelist = xdoc.SelectNodes(xpath);
            return xnodelist;
        }

        /// <summary>
        /// Get InnerText from a node.
        /// </summary>
        /// <param name="xnode">The node to get text.</param>
        public string GetTextContentFromNode(XmlNode xnode)
        {
            return xnode.InnerText;
        }

        /// <summary>
        /// Get InnerXML from a node - All child XML nodes from current node.
        /// </summary>
        /// <param name="xnode">The node to get XML.</param>
        public string GetXmlContentUnderNode(XmlNode xnode)
        {
            return xnode.InnerXml;
        }

        /// <summary>
        /// Get OuterXML from a node - The current node and all child.
        /// </summary>
        /// <param name="xnode">The node to get XML.</param>
        public string GetXmlContentFromNode(XmlNode xnode)
        {
            return xnode.OuterXml;
        }

        /// <summary>
        /// Save DataSet to XML file with pre-defined File Path
        /// </summary>
        /// <param name="ds">DataSet to save.</param>
        public void SaveDataSetToXML(DataSet ds)
        {
            SaveDataSetToXML(ds, FilePath);
        }

        /// <summary>
        /// Save DataSet to XML file with File Path
        /// </summary>
        /// <param name="ds">DataSet to save.</param>
        /// <param name="filePath">Path to XML file to save.</param>
        public void SaveDataSetToXML(DataSet ds, string filePath)
        {
            ds.WriteXml(filePath);
        }

        public void InsertXMLNode(XmlNode xnode)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(FilePath);
            if (xdoc.FirstChild != null)
                xdoc.InsertBefore(xnode, xdoc.FirstChild);
            else
                xdoc.AppendChild(xnode);
        }

        public void SaveDataSet(DataSet ds)
        {
            ds.WriteXml(FilePath);
        }

        public DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(FilePath);
            return ds;
        }

        public void AppendXMLNode(XmlNode xnode)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(FilePath);
            if (xdoc.FirstChild != null)
                xdoc.InsertAfter(xnode, xdoc.LastChild);
            else
                xdoc.AppendChild(xnode);
        }

        public void ReplaceXMLNode(XmlNode newNode, XmlNode oldNode)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(this.FilePath);
            XmlElement xmlElement = xdoc.DocumentElement;
            xmlElement.ReplaceChild(newNode, oldNode);
            xdoc.Save(this.FilePath);
        }

        public void LoadXml()
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(this.FilePath);
        }

        public void SaveXml()
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Save(this.FilePath);
        }

        public void SaveXml(string sPath)
        {
            XmlDocument xdoc = new XmlDocument();
            XmlNode xNode = GetNodeFromXpath(XPath);
            xdoc.Save(sPath);
        }

        public virtual void Dispose()
        {

        }
    }
}
