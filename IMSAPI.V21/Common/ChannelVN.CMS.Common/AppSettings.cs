﻿using System.Configuration;

namespace ChannelVN.CMS.Common
{
    public class AppSettings
    {
        public static string GetString(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? ConfigurationManager.AppSettings[key].Trim() : string.Empty;
        }

        public static int GetInt32(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? int.Parse(ConfigurationManager.AppSettings[key]) : 0;
        }

        public static long GetInt64(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? long.Parse(ConfigurationManager.AppSettings[key]) : 0;
        }

        public static bool GetBool(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? bool.Parse(ConfigurationManager.AppSettings[key]) : false;
        }

        public static string GetConnection(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.ConnectionStrings[key].ConnectionString) ? ConfigurationManager.ConnectionStrings[key].ConnectionString : string.Empty;
        }
    }
}
