﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;

namespace ChannelVN.CMS.Common.ESClientHelper
{
    public class ESClientHelper: IESClient
    {
        const int REQUEST_TIMEOUT = 5; //default => 5 seconds

        private StaticConnectionPool _pool;
        private ElasticClient _client;
        private string _nameSpace;

        #region Constructor

        public ESClientHelper()
            : this(string.Empty)
        {            
        }

        public ESClientHelper(string nameSpace)
        {
            this.NameSpace = nameSpace;
        }

        protected ElasticClient GetConnection()
        {
            try {
                if (_client == null)
                {
                    var nodes = new Uri[]
                    {
                        new Uri(this.ConnectionString)
                    };
                    _pool = new StaticConnectionPool(nodes);
                    var settings = new ConnectionSettings(_pool).RequestTimeout(TimeSpan.FromSeconds(REQUEST_TIMEOUT));

                    _client = new ElasticClient(settings);
                }
            }
            catch {
                _client = null;
            }
            return _client;
        }
        protected void CloseIndex(string stringId)
        {
            if (_client != null)
            {
                _client.CloseIndex(stringId);
            }
        }

        protected string NameOf(Object entity)
        {
            var name = "";
            if (entity != null)
            {
                if (string.IsNullOrEmpty(this.NameSpace))
                    name = entity.GetType().Name.ToLower();
                else
                    name = (this.NameSpace + "_" + entity.GetType().Name).ToLower();
            }
            return name;
        }

        protected string NameOf(params string[] values)
        {
            var name = "";

            if (null != values)
            {
                var strValue = string.Join("_", values);
                if (!string.IsNullOrEmpty(strValue))
                {
                    if (string.IsNullOrEmpty(this.NameSpace))
                        name = strValue.ToLower();
                    else
                        name = (this.NameSpace + "_" + strValue).ToLower();
                }
            }
            return name;
        }

        #endregion        

        #region Properties
        public string ConnectionString
        {
            get;
            set;
        }

        public string NameSpace
        {
            get
            {
                return _nameSpace;
            }
            set
            {
                _nameSpace = value;
                if (!string.IsNullOrEmpty(_nameSpace))
                {
                    _nameSpace = _nameSpace.Trim();
                }
            }
        }
        #endregion

        #region Create        
        public bool Create<T>(T document) where T : class
        {
            try
            {
                GetConnection();
                                
                string stringId = this.NameOf(typeof(T).Name);
                var index = _client.Index(document, i => i.Index(stringId).Refresh(Refresh.True));

                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateMany<T>(List<T> documents) where T : class
        {
            try
            {                
                GetConnection(); 
                               
                string stringId = this.NameOf(typeof(T).Name);
                var bulkIndexer = new BulkDescriptor();
                foreach (var document in documents)
                {
                    bulkIndexer.Index<T>(i => i
                        .Document(document)
                        .Index(stringId)                                            
                        );
                }

                var index = _client.Bulk(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch
            {
                return false;
            }
        }
        public bool CreateMany2<T>(List<T> documents) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var bulkIndexer = new BulkDescriptor();                
                    bulkIndexer.IndexMany(documents,(d,doc)=>d.Document(doc).Index(stringId));                

                var index = _client.Bulk(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        public bool CreateIndexSettings<T>(string fieldNameAnalyzer, List<string> filterAnalyzer = null) where T : class
        {
            try
            {
                GetConnection();   
                             
                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                CustomAnalyzer customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                string stringId = NameOf(typeof(T).Name);

                if (!_client.IndexExists(stringId).Exists)
                {
                    var index = _client.CreateIndex(stringId, i => i                        
                        .Settings(s => s
                            .Analysis(a => a
                                .Analyzers(al => al
                                    .UserDefined("analyzer_userdefind", customAnlyzer)
                                )
                            )
                            .Setting("index.max_result_window", 999999999)
                        )                        
                        .Mappings(ms => ms
                            .Map<T>(m => m
                                .AutoMap()
                                .Properties(p => p
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    );                    

                    return index.IsValid;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateIndexSettings<T>(string fieldNameAnalyzer, string fieldNameAnalyzer2, List<string> filterAnalyzer = null) where T : class
        {
            try
            {
                GetConnection();

                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                CustomAnalyzer customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };

                string stringId = NameOf(typeof(T).Name);

                if (!_client.IndexExists(stringId).Exists)
                {
                    var index = _client.CreateIndex(stringId, i => i                        
                        .Settings(s => s
                            .Analysis(a => a
                                .Analyzers(al => al
                                    .UserDefined("analyzer_userdefind", customAnlyzer)
                                )
                            )
                            .Setting("index.max_result_window", 999999999)
                        )
                        .Mappings(ms => ms
                            .Map<T>(m => m
                                .AutoMap()
                                .Properties(p => p
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                    .Text(s => s
                                        .Name(fieldNameAnalyzer2)
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    );

                    return index.IsValid;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public bool CreateIndexSettings<T>(List<string> listFieldNameAnalyzer, List<string> filterAnalyzer = null) where T : class
        {
            try
            {
                GetConnection();

                if (filterAnalyzer == null)
                {
                    filterAnalyzer = new List<string>()
                    {
                        "lowercase", "asciifolding"
                    };
                }
                CustomAnalyzer customAnlyzer = new CustomAnalyzer
                {
                    Tokenizer = "standard",
                    Filter = filterAnalyzer
                };
               
                string stringId = this.NameOf(typeof(T).Name);
                if (!_client.IndexExists(stringId).Exists)
                {
                    var index = _client.CreateIndex(stringId, i => i                        
                        .Settings(s => s
                            .Analysis(a => a
                                .Analyzers(al => al
                                    .UserDefined("analyzer_userdefind", customAnlyzer)
                                )
                            )
                            .Setting("index.max_result_window", 999999999)
                        )
                        .Mappings(ms => ms
                            .Map<T>(m => m
                                .AutoMap()
                                .Properties(p => p
                                    .Text(s => s
                                        .Name(listFieldNameAnalyzer[0])
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                    .Text(s => s
                                        .Name(listFieldNameAnalyzer[1])
                                        .Fields(f => f
                                            .Text(ss => ss
                                                .Name("folded")
                                                .Analyzer("analyzer_userdefind")
                                            )
                                        )
                                    )
                                )
                            )
                            )
                        );

                    return index.IsValid;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Update
        public bool Update<T>(long id, dynamic updateDocument) where T : class
        {
            try
            {                
                GetConnection();
                
                string stringId = this.NameOf(typeof(T).Name);
                var response = _client.Update<T, dynamic>(id, u => u.Index(stringId).Doc(updateDocument).Refresh(Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }
        public bool UpdateMany<T>(List<T> documents) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var bulkIndexer = new BulkDescriptor();
                bulkIndexer.UpdateMany(documents, (d, doc) => d.Doc(doc).Index(stringId));

                var index = _client.Bulk(bulkIndexer.Refresh(Refresh.True));
                return index.IsValid;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion        

        #region Delete
        public bool DeleteIndex(string indexName)
        {
            try
            {                
                GetConnection();
                
                var response = _client.DeleteIndex(this.NameOf(indexName));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(T document) where T : class
        {
            try
            {                
                GetConnection(); 
                               
                string stringId = this.NameOf(typeof(T).Name);

                var response = _client.Delete<T>(document, i => i.Index(stringId).Refresh(Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(int id) where T : class
        {
            try
            {                
                GetConnection(); 
                               
                var response = _client.Delete<T>(id, i => i.Index(this.NameOf(typeof(T).Name)).Refresh(Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMapping<T>(string id) where T : class
        {
            try
            {
                GetConnection();

                var response = _client.Delete<T>(id, i => i.Index(this.NameOf(typeof(T).Name)).Refresh(Refresh.True));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteByQuery<T>(QueryContainer request) where T : class
        {
            try
            {
                GetConnection();

                var response = _client.DeleteByQuery<T>(q => q.Index(this.NameOf(typeof(T).Name)).Query(rq=> request));

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMappingMany<T>(List<T> documents) where T : class
        {
            try
            {                
                GetConnection();
                                
                string stringId = this.NameOf(typeof(T).Name);
                var response = _client.DeleteMany(documents, stringId);

                return response.IsValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMappingMany<T>(List<int> listId) where T : class
        {
            try
            {
                var isValid = false;
                foreach (int id in listId)
                {
                    isValid = DeleteMapping<T>(id);
                }

                return isValid;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteMappingMany<T>(List<string> listId) where T : class
        {
            try
            {
                var isValid = false;
                foreach (var id in listId)
                {
                    isValid = DeleteMapping<T>(id);
                }

                return isValid;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Get
        public T Get<T>(T document) where T : class
        {
            try
            {               
                GetConnection(); 
                               
                string stringId = this.NameOf(typeof(T).Name);
                var response = _client.Get<T>(document, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public T Get<T>(long id) where T : class
        {
            try
            {                
                GetConnection();
                
                string stringId = this.NameOf(typeof(T).Name);
                var response = _client.Get<T>(id, i => i.Index(stringId).Refresh());
                if (response.IsValid)
                {
                    return response.Source;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Search       
        public ESClientEntity.SearchResult<T> Search<T>(SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                GetConnection();
                string stringId = this.NameOf(typeof(T).Name);
                searchRequest.Index(stringId);
                
                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> Search<T>(string indexName, SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                GetConnection();
                searchRequest.Index(indexName);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> Search<T>(string keyword, string[] fields) where T : class
        {
            try
            {
                GetConnection();
                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                searchRequest.Query(q => q
                    .QueryString(qs => qs
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    )
                );


                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> Search<T>(string keyword, string[] fields, Dictionary<string, dynamic> sort, Dictionary<string, dynamic> term, Dictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(term[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(term[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }

                        i++;
                    }
                }

                var terms1 = new TermsQuery();
                var terms2 = new TermsQuery();
                var terms3 = new TermsQuery();
                var terms4 = new TermsQuery();
                var terms5 = new TermsQuery();

                if (terms != null && terms.Count > 0)
                {
                    var listKey = new List<string>(terms.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: terms1.Field = key; terms1.Terms = term[key]; break;
                            case 2: terms2.Field = key; terms2.Terms = term[key]; break;
                            case 3: terms3.Field = key; terms3.Terms = term[key]; break;
                            case 4: terms4.Field = key; terms4.Terms = term[key]; break;
                            case 5: terms5.Field = key; terms5.Terms = term[key]; break;
                        }

                        i++;
                    }
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && range && term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                    && terms1 && terms2 && terms3 && terms4 && terms5
                );

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = 0,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> SearchPaging<T>(Dictionary<string, dynamic> sort, Dictionary<string, dynamic> term, Dictionary<string, dynamic> terms, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(sort.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(sort[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(sort[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }

                        i++;
                    }
                }

                var terms1 = new TermsQuery();
                var terms2 = new TermsQuery();
                var terms3 = new TermsQuery();
                var terms4 = new TermsQuery();
                var terms5 = new TermsQuery();

                if (terms != null && terms.Count > 0)
                {
                    var listKey = new List<string>(terms.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: terms1.Field = key; terms1.Terms = terms[key]; break;
                            case 2: terms2.Field = key; terms2.Terms = terms[key]; break;
                            case 3: terms3.Field = key; terms3.Terms = terms[key]; break;
                            case 4: terms4.Field = key; terms4.Terms = terms[key]; break;
                            case 5: terms5.Field = key; terms5.Terms = terms[key]; break;
                        }

                        i++;
                    }
                }

                if (page >= 6) { return null; }

                searchRequest.Query(q => term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                    && terms1 && terms2 && terms3 && terms4 && terms5
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, Dictionary<string, dynamic> sort, Dictionary<string, dynamic> term, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(sort.Keys);
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(sort[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(sort[key]));
                        }
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();

                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }

                        i++;
                    }
                }                                

                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10                    
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, Dictionary<string, dynamic> sort, Dictionary<string,dynamic> term, Dictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                if (sort != null && sort.Count > 0)
                {
                    var listKey = new List<string>(sort.Keys);                    
                    foreach (var key in listKey)
                    {
                        if (key == "desc")
                        {
                            searchRequest.Sort(s => s.Descending(sort[key]));
                        }
                        else if (key == "asc")
                        {
                            searchRequest.Sort(s => s.Ascending(sort[key]));
                        }                                                
                    }
                }

                var term1 = new TermQuery();
                var term2 = new TermQuery();
                var term3 = new TermQuery();
                var term4 = new TermQuery();
                var term5 = new TermQuery();
                var term6 = new TermQuery();
                var term7 = new TermQuery();
                var term8 = new TermQuery();
                var term9 = new TermQuery();
                var term10 = new TermQuery();
                
                if (term != null && term.Count > 0)
                {
                    var listKey = new List<string>(term.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {                        
                        switch (i)
                        {
                            case 1: term1.Field = key; term1.Value = term[key]; break;
                            case 2: term2.Field = key; term2.Value = term[key]; break;
                            case 3: term3.Field = key; term3.Value = term[key]; break;
                            case 4: term4.Field = key; term4.Value = term[key]; break;
                            case 5: term5.Field = key; term5.Value = term[key]; break;
                            case 6: term6.Field = key; term6.Value = term[key]; break;
                            case 7: term7.Field = key; term7.Value = term[key]; break;
                            case 8: term8.Field = key; term8.Value = term[key]; break;
                            case 9: term9.Field = key; term9.Value = term[key]; break;
                            case 10: term10.Field = key; term10.Value = term[key]; break;
                        }
                        
                        i++;
                    }
                }

                var terms1 = new TermsQuery();
                var terms2 = new TermsQuery();
                var terms3 = new TermsQuery();
                var terms4 = new TermsQuery();
                var terms5 = new TermsQuery();

                if (terms != null && terms.Count > 0)
                {
                    var listKey = new List<string>(terms.Keys);
                    var i = 1;
                    foreach (var key in listKey)
                    {                        
                        switch (i)
                        {
                            case 1: terms1.Field = key; terms1.Terms = terms[key]; break;
                            case 2: terms2.Field = key; terms2.Terms = terms[key]; break;
                            case 3: terms3.Field = key; terms3.Terms = terms[key]; break;
                            case 4: terms4.Field = key; terms4.Terms = terms[key]; break;
                            case 5: terms5.Field = key; terms5.Terms = terms[key]; break;                            
                        }
                        
                        i++;
                    }
                }

                var range = new DateRangeQuery();
                range.Field = fieldDate;
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }
                
                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        .DefaultOperator(Operator.And)
                    ) && range && term1 && term2 && term3 && term4 && term5 && term6 && term7 && term8 && term8 && term10
                    && terms1 && terms2 && terms3 && terms4 && terms5
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, int status, string[] fields, int start, int rows) where T : class
        {
            try
            {
                GetConnection();
                if (start <= 0) start = 1;
                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);
                if (status == -1)
                {
                    searchRequest.Query(q => q
                        .QueryString(qs => qs
                            .Query("*" + keyword + "*")
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        )
                    ).Skip(start - 1).Size(rows);
                }
                else {
                    searchRequest.Query(q => q
                        .QueryString(qs => qs
                            .Query("*" + keyword + "*")
                            .Fields(f => f.Fields(fields))
                            .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                            .DefaultOperator(Operator.And)
                        ) && q
                        .Term(t => t
                            .Field("status")
                            .Value(status)
                        )
                    ).Skip(start - 1).Size(rows);
                }

                searchRequest.Sort(s=>s.Descending("created_date"));

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = start,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }        
        public ESClientEntity.SearchResult<T> SearchStatus<T>(string keyword, int status, string[] fields, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .Operator(Operator.Or)
                    ) && q
                    .Term(t => t
                        .Field("status")
                        .Value(status)
                    )
                ).From(page - 1).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<T> SearchType<T>(string keyword, int newsType, string zoneIds, DateTime from, DateTime to, string[] fields, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();
                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }
                    else if (to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }

                searchRequest.Sort(s => s.Descending("created_date"));

                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                }

                var termByStatus = new TermQuery();
                termByStatus.Field = "status";
                termByStatus.Value = 8;

                var termByType = new TermQuery();
                termByType.Field = "type";
                termByType.Value = newsType;

                searchRequest.Query(q => q
                    .MultiMatch(mp => mp                        
                        .Query(keyword)
                        .Fields(f => f.Fields(fields))
                        .MinimumShouldMatch(MinimumShouldMatch.Percentage(100))
                        //.DefaultOperator(Operator.And)
                        .Type(TextQueryType.Phrase)
                    ) && termByType && termByStatus && range
                      && q.Terms(t => t
                            .Field("zone_ids")
                            .Terms(termsZoneIds)
                        )
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }        
        public ESClientEntity.SearchResult<T> SearchPagingGroupBy<T>(string query, string[] fields, string groupBy, int page, int pageSize, string orderBy = null, string sortBy = null) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);
                if (orderBy != null)
                {
                    orderBy = orderBy.ToLower();
                    if (sortBy == null || (sortBy != null && sortBy.ToUpper() == "ASC"))
                        searchRequest.Sort(s => s.Ascending(orderBy));
                    if (sortBy != null && sortBy.ToUpper() == "DESC")
                        searchRequest.Sort(s => s.Descending(orderBy));
                }
                searchRequest.Query(q => q
                    .MultiMatch(mp => mp
                        .Query(query)
                        .Fields(f => f.Fields(fields))
                    )
                )
                .Aggregations(a => a
                    .Terms("by_groupBy", t => t
                        .Field(groupBy)
                        .Size(int.MaxValue)
                    )
                )
                .From(page - 1).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var itemsGroup = response.Aggs.Terms("by_groupBy");

                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public IEnumerable<string> AutocompleteSuggest<T>(string query, string fieldName, int count = 10) where T : class
        {
            try
            {
                GetConnection();

                var response = _client.Suggest<T>(x => x
                    .Completion("tag_suggestions", c => c
                         .Contexts(ct => ct
                             .Context(query)
                         )
                         .Field(fieldName)
                         .Size(count)
                    )
                );
                if (response.IsValid)
                {
                    return (IEnumerable<string>)response.Suggestions["tag_suggestions"];
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<dynamic> CountStatus<T>(string username, List<string> listStatus, Dictionary<string, string> statusZoneIds,string currentUserName=null) where T : class
        {
            try
            {
                var searchResult = new ESClientEntity.SearchResult<dynamic>
                {
                };
                
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var countRequest = new CountDescriptor<T>().Index(stringId);                
                                
                if (listStatus != null)
                {
                    var tempStatus = new List<dynamic>();
                    foreach (var Status in listStatus)
                    {
                        var termsZoneIds = new string[] { };
                        if (statusZoneIds != null)
                        {
                            termsZoneIds = statusZoneIds[Status].Split(',');
                        }
                        var termsByZoneId = new TermsQuery();
                        termsByZoneId.Field = "zone_ids";
                        termsByZoneId.Terms = termsZoneIds;
                                                                                               
                        var termByStatus = new TermQuery();
                        var termUserName = new TermQuery();
                        if (Status == "-1" || Status == "12")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = currentUserName;
                            termByStatus = new TermQuery();//count theo bai cua toi
                        }
                        else
                        {                            
                            if (!string.IsNullOrEmpty(username))
                            {
                                termUserName.Field = "created_by";
                                termUserName.Value = username;
                            }
                            termByStatus.Field = "status";
                            termByStatus.Value = Status;
                        }

                        countRequest.Query(q =>
                                termByStatus
                                && termUserName                                
                                && termsByZoneId
                        );

                        var response = _client.Count<T>(countRequest);

                        if (response.IsValid)
                        {
                            var count = response.Count;
                            tempStatus.Add(new { Status, response.Count });
                        }
                    }
                    searchResult.Data = tempStatus;
                }

                return searchResult;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<dynamic> CountStatus<T>(string username, List<string> listStatus) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var countRequest = new CountDescriptor<T>().Index(stringId);

                var termUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termUserName.Field = "created_by";
                    termUserName.Value = username;
                }

                var searchResult = new ESClientEntity.SearchResult<dynamic>
                {
                };
                if (listStatus != null)
                {
                    var tempStatus = new List<dynamic>();
                    foreach (var Status in listStatus)
                    {
                        countRequest.Query(q => q
                            .Term(t => t
                                .Field("status")
                                .Value(Status)
                            ) && termUserName
                        );

                        var response = _client.Count<T>(countRequest);

                        if (response.IsValid)
                        {
                            var count = response.Count;
                            tempStatus.Add(new { Status, response.Count });
                        }
                        else
                        {
                            tempStatus.Add(new { Status, Count = 0, response.IsValid });
                        }
                    }
                    searchResult.Data = tempStatus;
                }

                return searchResult;
            }
            catch
            {
                return null;
            }
        }
        public ESClientEntity.SearchResult<dynamic> CountStatusVideo<T>(string username, List<string> listStatus) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var countRequest = new CountDescriptor<T>().Index(stringId);                

                var termParentId = new TermQuery();
                var parentId = 0;
                if (parentId <= 0)
                {
                    termParentId.Field = "parent_id";
                    termParentId.Value = parentId;
                }

                var searchResult = new ESClientEntity.SearchResult<dynamic>
                {
                };
                if (listStatus != null)
                {
                    var tempStatus = new List<dynamic>();
                    foreach (var Status in listStatus)
                    {
                        var termUserName = new TermQuery();
                        //lưu tạm, trả lại, CloneVideo
                        if (Status=="0" || Status=="4" || Status == "6")
                        {
                            termUserName.Field = "created_by";
                            termUserName.Value = username;
                        }

                        countRequest.Query(q => q
                            .Term(t => t
                                .Field("status")
                                .Value(Status)
                            ) && termUserName && termParentId
                        );

                        var response = _client.Count<T>(countRequest);

                        if (response.IsValid)
                        {
                            var count = response.Count;
                            tempStatus.Add(new { Status, response.Count });
                        }
                        else
                        {
                            tempStatus.Add(new { Status, Count = 0, response.IsValid });
                        }
                    }
                    searchResult.Data = tempStatus;
                }

                return searchResult;
            }
            catch
            {
                return null;
            }
        }
        public List<ESClientEntity.CountStatus> CountStatus<T>(SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                GetConnection();
                string stringId = this.NameOf(typeof(T).Name);
                searchRequest.Index(stringId);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var listCountStatus = new List<ESClientEntity.CountStatus>();

                    var terms = response.Aggs.Terms("count_status");
                    if (terms != null && terms.Buckets.Count > 0)
                    {
                        foreach (var bucket in terms.Buckets)
                        {
                            listCountStatus.Add(new ESClientEntity.CountStatus
                            {
                                Key = bucket.Key,
                                DocCount = bucket.DocCount
                            });
                        }
                    }
                    return listCountStatus;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        public List<ESClientEntity.CountDateStatus> CountDateStatus<T>(SearchDescriptor<T> searchRequest) where T : class
        {
            try
            {
                GetConnection();
                string stringId = this.NameOf(typeof(T).Name);
                searchRequest.Index(stringId);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var listCountStatus = new List<ESClientEntity.CountDateStatus>();

                    var terms1 = response.Aggs.Terms("published_date_status");
                    var terms2 = response.Aggs.Terms("created_date_status");

                    if (terms1 != null && terms1.Buckets.Count > 0)
                    {
                        foreach (var bucket in terms1.Buckets)
                        {
                            var abc = new ESClientEntity.CountDateStatus
                            {
                                Key = bucket.Key,
                                DocCount1 = (int)bucket.DocCount,
                                DocCount2 = 0
                            };                            

                            if (terms1 != null && terms1.Buckets.Count > 0)
                            {
                                foreach (var bucket2 in terms2.Buckets)
                                {
                                    if(bucket.Key== bucket2.Key)
                                    {
                                        abc.DocCount2 = (int)bucket2.DocCount;
                                    }
                                }
                            }

                            listCountStatus.Add(abc);
                        }
                    }
                    return listCountStatus;
                }

                return null;
            }
            catch
            {
                return null;
            }
        }

        public long Count<T>() where T : class
        {
            try
            {
                GetConnection();
                string stringId = NameOf(typeof(T).Name);
                
                var response = _client.Count<T>(c=>c.Index(stringId).Type(typeof(T).Name.ToLower()));

                if (response.IsValid)
                {                    
                    return response.Count;
                }

                return 0;
            }
            catch
            {
                return 0;
            }
        }

        #region Search News
        //search news
        private ESClientEntity.SearchResult<T> SearchNews<T>(string keyword, string username, string zoneIds, DateTime from, DateTime to, int filterFieldForUsername, int sortOrder, int status, int newsType, string[] fields, int page, int pageSize) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);
                
                var range = new DateRangeQuery();                
                if (status == 8)
                {
                    range.Field = "distribution_date";                    
                }
                else
                {                    
                    range.Field = "created_date";                    
                }
                if (from != DateTime.MinValue && to != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = from;
                    range.LessThanOrEqualTo = to;
                }
                else {
                    if (from != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = from;
                    }else if(to != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = to;
                    }
                }            

                var termByUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    switch (filterFieldForUsername)
                    {
                        case 0:
                            termByUserName.Field = "created_by";
                            termByUserName.Value = username;
                            break;
                        case 1:
                            termByUserName.Field = "last_modified_by";
                            termByUserName.Value = username;
                            break;
                        case 2:
                            termByUserName.Field = "published_by";
                            termByUserName.Value = username;
                            break;
                        case 3:
                            termByUserName.Field = "edited_by";
                            termByUserName.Value = username;
                            break;
                        case 4:
                            termByUserName.Field = "last_receiver";
                            termByUserName.Value = username;
                            break;
                    }
                }

                switch (sortOrder)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Descending("title"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Ascending("title"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("view_count"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("view_count"));
                        break;
                    case 8:
                        searchRequest.Sort(s => s.Descending("last_modified_date"));
                        break;
                    case 9:
                        searchRequest.Sort(s => s.Ascending("last_modified_date"));
                        break;
                    default:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                }

                var termsZoneIds = new string[] { };
                if (zoneIds != "")
                {
                    termsZoneIds = zoneIds.Split(',');
                }
                var termsByZoneId = new TermsQuery();
                termsByZoneId.Field = "zone_ids";
                termsByZoneId.Terms = termsZoneIds;

                var termByStatus = new TermQuery();
                if (status != 0 && status!=-1)
                {
                    termByStatus.Field = "status";
                    termByStatus.Value = status;
                }

                var termByType = new TermQuery();
                if (newsType != -1)
                {
                    termByType.Field = "type";
                    termByType.Value = newsType;
                }

                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*"+keyword+"*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && range && termByType && termByStatus && termByUserName
                        && termsByZoneId && new TermsQuery()
                ).From((page - 1)* pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }                                
        #endregion       

        #region Search Video        
        public ESClientEntity.SearchResult<T> SearchVideo<T>(string username, int zoneVideoId, int playlistId, string keyword, int status, int order, DateTime fromDate, DateTime toDate, int mode, int page, int pageSize, string[] fields) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                var range = new DateRangeQuery();
                range.Field = "created_date";                
                if (fromDate != DateTime.MinValue && toDate != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = fromDate;
                    range.LessThanOrEqualTo = toDate;
                }
                else {
                    if (fromDate != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = fromDate;
                    }
                    else if (toDate != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = toDate;
                    }
                }

                var termByUserName = new TermQuery();
                if (!string.IsNullOrEmpty(username))
                {
                    termByUserName.Field = "created_by";
                    termByUserName.Value = username;
                }

                switch (order)
                {
                    case 0:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                    case 1:
                        searchRequest.Sort(s => s.Ascending("created_date"));
                        break;
                    case 2:
                        searchRequest.Sort(s => s.Ascending("name"));
                        break;
                    case 3:
                        searchRequest.Sort(s => s.Descending("name"));
                        break;
                    case 4:
                        searchRequest.Sort(s => s.Ascending("id"));
                        break;
                    case 5:
                        searchRequest.Sort(s => s.Descending("id"));
                        break;
                    case 6:
                        searchRequest.Sort(s => s.Descending("distribution_date"));
                        break;
                    case 7:
                        searchRequest.Sort(s => s.Ascending("distribution_date"));
                        break;                                       
                    default:
                        searchRequest.Sort(s => s.Descending("created_date"));
                        break;
                }

                var termByZoneId = new TermsQuery();
                if (zoneVideoId > 0)
                {
                    termByZoneId.Field = "zone_ids";
                    termByZoneId.Terms = new string[] { zoneVideoId.ToString() };
                }                

                var termsPlaylistId = new TermsQuery();
                if (playlistId > 0)
                {
                    termsPlaylistId.Field = "playlist_ids";
                    termsPlaylistId.Terms = new string[] { playlistId.ToString() };
                }

                var termByStatus = new TermQuery();
                if (status >= 0)
                {
                    termByStatus.Field = "status";
                    termByStatus.Value = status;
                }

                var termByMode = new TermQuery();
                if (mode == 1)
                {
                    termByMode.Field = "mode";
                    termByMode.Value = mode;
                }else if(mode == 0)
                {
                    termByMode.Field = "mode";
                    termByMode.Value = mode;
                }
                
                searchRequest.Query(q => q
                    .QueryString(mp => mp
                        .Query("*" + keyword + "*")
                        .Fields(f => f.Fields(fields))
                        .DefaultOperator(Operator.Or)
                    ) && termByUserName && termByStatus && termByZoneId && termsPlaylistId && termByMode && range
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }        
        
        #endregion

        #region search Activity
        public ESClientEntity.SearchResult<T> SearchActivity<T>(int page, int pageSize, long applicationId, int actionTypeDetail, int type, DateTime dateFrom, DateTime dateTo) where T : class
        {
            try
            {
                GetConnection();

                string stringId = this.NameOf(typeof(T).Name);
                var searchRequest = new SearchDescriptor<T>().Index(stringId);

                var range = new DateRangeQuery();
                range.Field = "created_date";
                if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
                {
                    range.GreaterThanOrEqualTo = dateFrom;
                    range.LessThanOrEqualTo = dateTo;
                }
                else {
                    if (dateFrom != DateTime.MinValue)
                    {
                        range.GreaterThanOrEqualTo = dateFrom;
                    }
                    else if (dateTo != DateTime.MinValue)
                    {
                        range.LessThanOrEqualTo = dateTo;
                    }
                }

                var termByApplicationId = new TermQuery();
                if (applicationId != -1)
                {
                    termByApplicationId.Field = "application_id";
                    termByApplicationId.Value = applicationId;
                }

                var termByActionTypeDetail = new TermQuery();
                if (actionTypeDetail != -1)
                {
                    termByActionTypeDetail.Field = "action_type_detail";
                    termByActionTypeDetail.Value = actionTypeDetail;
                }

                var termByType = new TermQuery();
                if (type != -1)
                {
                    termByType.Field = "type";
                    termByType.Value = type;
                }

                searchRequest.Query(q => termByType && termByApplicationId && termByActionTypeDetail && range
                ).From((page - 1) * pageSize).Size(pageSize);

                var response = _client.Search<T>(searchRequest);

                if (response.IsValid)
                {
                    var searchResult = new ESClientEntity.SearchResult<T>
                    {
                        Total = response.Total,
                        Page = page,
                        Data = (List<T>)response.Documents,
                        ElapsedMilliseconds = response.Took
                    };

                    return searchResult;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion        

        #endregion
    }
}
