﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChannelVN.CMS.Common.ESClientHelper
{
    public interface IESClient
    {
        #region Create        
        bool Create<T>(T document) where T : class;
        bool CreateMany<T>(List<T> document) where T : class;
        bool CreateIndexSettings<T>(string fieldNameAnalyzer, List<string> filterAnalyzer = null) where T : class;

        #endregion

        #region Update
        bool Update<T>(long id, dynamic updateDocument) where T : class;

        #endregion

        #region Delete
        bool DeleteIndex(string nameIndex);
        bool DeleteMapping<T>(T document) where T : class;
        bool DeleteMapping<T>(int id) where T : class;
        bool DeleteMappingMany<T>(List<T> documents) where T : class;
        bool DeleteMappingMany<T>(List<int> listId) where T : class;

        #endregion

        #region get a object
        T Get<T>(T document) where T : class;
        T Get<T>(long id) where T : class;

        #endregion

        #region Search     
        ESClientEntity.SearchResult<T> Search<T>(string keyword, string[] fields) where T : class;
        ESClientEntity.SearchResult<T> Search<T>(string keyword, string[] fields, Dictionary<string, dynamic> sort, Dictionary<string, dynamic> term, Dictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate) where T : class;                
        ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, string[] fields, Dictionary<string, dynamic> sort, Dictionary<string, dynamic> term, Dictionary<string, dynamic> terms, DateTime from, DateTime to, string fieldDate, int page, int pageSize) where T : class;
        ESClientEntity.SearchResult<T> SearchPaging<T>(string keyword, int status, string[] fields, int start, int rows) where T : class;
        ESClientEntity.SearchResult<T> SearchStatus<T>(string keyword, int status, string[] fields, int page, int pageSize) where T : class;
        ESClientEntity.SearchResult<T> SearchPagingGroupBy<T>(string query, string[] fields, string groupBy, int page, int pageSize, string orderBy = null, string sortBy = null) where T : class;
        IEnumerable<string> AutocompleteSuggest<T>(string query, string fieldName, int count = 10) where T : class;

        #endregion
    }
}
