﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Common.ESClientHelper
{
    public class ESClientEntity
    {
        public class SearchResult<T>
        {            
            public long Total { get; set; }
            public int Page { get; set; }
            public IEnumerable<T> Data { get; set; }
            public long ElapsedMilliseconds { get; set; }
        }

        public class CountStatus
        {            
            public string Key { get; set; }
            public long? DocCount { get; set; }            
        }

        public class CountDateStatus
        {
            public string Key { get; set; }
            public int DocCount1 { get; set; }
            public int DocCount2 { get; set; }
        }
    }
}
