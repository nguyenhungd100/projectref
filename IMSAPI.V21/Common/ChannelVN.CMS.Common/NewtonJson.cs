﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
//using JsonSerializer = ServiceStack.Text.JsonSerializer;

namespace ChannelVN.CMS.Common
{
    public class NewtonJson
    {
        // Serialize
        public static string Serialize(object @object, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewtonJson: Serialize => " + ex.Message);
                return string.Empty;
            }
        }

        public static string Serialize(object @object)
        {
            try
            {
                return JsonConvert.SerializeObject(@object, MicrosoftDateFormatSettings);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewtonJson: Serialize => " + ex.Message);
                return string.Empty;
            }
        }

        // DeSerialize
        public static T Deserialize<T>(string jsonString)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, MicrosoftDateFormatSettings);                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewtonJson: Deserialize => " + ex.Message);
                return default(T);
            }
        }
        public static T Deserialize<T>(string jsonString, string dateTimeFormat)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(jsonString, new IsoDateTimeConverter { DateTimeFormat = dateTimeFormat });                
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "NewtonJson: Deserialize => " + ex.Message);
                return default(T);
            }
        }

        // Privates Format Date
        private static readonly JsonSerializerSettings MicrosoftDateFormatSettings = new JsonSerializerSettings
                                                                         {
                                                                             DateFormatHandling =
                                                                                 DateFormatHandling.MicrosoftDateFormat                                                                                 
                                                                         };
    }
}
