﻿using ChannelVN.CMS.Common.ChannelConfig;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Web;

namespace ChannelVN.CMS.Common
{
    public class TelegramNotification
    {
        public static void SendMessageAsync(string message)
        {
            var enabled = CmsChannelConfiguration.GetAppSettingInBoolean("TelegramNotificationApiEnabled");
            var configRootApiUrl = CmsChannelConfiguration.GetAppSetting("TelegramNotificationApiUrl");
            var apiKey = CmsChannelConfiguration.GetAppSetting("TelegramNotificationApiSecretKey");
            if (enabled && configRootApiUrl != null && !string.IsNullOrEmpty(configRootApiUrl))
            {
                new Thread(() =>
                {                
                    Thread.CurrentThread.IsBackground = true;
                    try
                    {                                       
                        var content = new FormUrlEncodedContent(new[]
                        {                            
                            new KeyValuePair<string, string>("Content",message)
                        });

                        using (var client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Add("Authorization", apiKey);
                            client.Timeout = TimeSpan.FromSeconds(5);
                            var response = client.PostAsync(configRootApiUrl + "message", content).Result;
                            if (!response.IsSuccessStatusCode)
                            {
                                response.EnsureSuccessStatusCode();
                            }
                            var rep = response.Content.ReadAsStringAsync().Result;
                            if (!string.IsNullOrEmpty(rep))
                            {
                                //returnValue = Json.Parse<dynamic>(rep).successful;

                                //Logger.WriteLog(Logger.LogType.Trace, "SendMessageAsync => success:" + rep + ", channel: Kinghub, message:" + message);
                            }
                        }                    
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Trace, "TelegramNotification: SendMessageAsync => exception :" + ex.Message);
                    }

                }).Start();
            }
        }

        public static void SendNotifyCDErrorAsync(string message)
        {
            var env = CmsChannelConfiguration.GetAppSetting("TelegramNotificationApiSecretKey");
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                try
                {
                    const string api = "https://api.telegram.org/bot780970931:AAHARR7AO1g93Zk4lxYBdDb7lRWPkNaSMUY/sendMessage";
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("chat_id","-372263689"),
                        new KeyValuePair<string, string>("text",message)
                    });

                    using (var client = new HttpClient())
                    {                        
                        client.Timeout = TimeSpan.FromSeconds(5);
                        var response = client.PostAsync(api, content).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            response.EnsureSuccessStatusCode();
                        }
                        var rep = response.Content.ReadAsStringAsync().Result;
                        if (!string.IsNullOrEmpty(rep))
                        {
                            //returnValue = Json.Parse<dynamic>(rep).successful;                                
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Trace, "PushNotifyCDErrorAsync => exception :" + ex.Message);
                }

            }).Start();
        }
    }
}
