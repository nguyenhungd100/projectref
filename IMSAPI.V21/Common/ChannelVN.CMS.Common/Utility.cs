﻿using ChannelVN.CMS.Common.ChannelConfig;
using DocumentFormat.OpenXml.Packaging;
using HtmlAgilityPack;
using OpenXmlPowerTools;
using Spire.Doc;
using System;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace ChannelVN.CMS.Common
{
    public class Utility
    {
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            const string pattern = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

            return Regex.IsMatch(email, pattern);
        }

        #region Convert data

        public static string ConvertToString(object value)
        {
            return null == value ? string.Empty : value.ToString();
        }
        public static int ConvertToInt(object value)
        {
            int returnValue;
            if (null == value || !int.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }
        public static long ConvertToLong(object value)
        {
            long returnValue;
            if (null == value || !long.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }
        public static short ConvertToShort(object value)
        {
            short returnValue;
            if (null == value || !short.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }
        public static decimal ConvertToDecimal(object value)
        {
            decimal returnValue;
            if (null == value || !decimal.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }
        public static double ConvertToDouble(object value)
        {
            double returnValue;
            if (null == value || !double.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }
        public static DateTime ConvertToDateTime(object value)
        {
            DateTime returnValue;

            //var ci = new CultureInfo("en-US");
            //var dtf = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", ShortTimePattern = "HH:mm:ss" };
            //ci.DateTimeFormat = dtf;
            //System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.MinValue;
            }
            return returnValue;
        }
        public static DateTime ConvertToDateTimeByFormat(object value, string format)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParseExact(value.ToString().Trim(), format, CultureInfo.InvariantCulture, DateTimeStyles.None, out returnValue))
            {
                returnValue = DateTime.MinValue;
            }
            return returnValue;
        }
        public static DateTime ConvertToDateTimeByFormatAndDateNow(object value, string format)
        {
            DateTime returnValue;

            if (null == value || !DateTime.TryParseExact(value.ToString().Trim(), format, CultureInfo.InvariantCulture, DateTimeStyles.None, out returnValue))
            {
                returnValue = DateTime.ParseExact(DateTime.Now.ToString(format), format, CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
            return returnValue;
        }
        public static DateTime ParseDateTime(string dateToParse, string[] formats = null, IFormatProvider provider = null, DateTimeStyles styles = DateTimeStyles.None)
        {
            var CUSTOM_DATE_FORMATS = new string[]
                {
                "dd-MM-yyyy",
                "dd-MM-yyyy HH:mm",
                "dd-MM-yyyy HH:mm:ss",
                "dd/MM/yyyy",
                "dd/MM/yyyy HH:mm",
                "dd/MM/yyyy HH:mm:ss"
                };

            if (formats == null)
            {
                formats = CUSTOM_DATE_FORMATS;
            }

            DateTime validDate;

            foreach (var format in formats)
            {
                if (format.EndsWith("Z"))
                {
                    if (DateTime.TryParseExact(dateToParse?.Trim(), format,
                             provider,
                             DateTimeStyles.AssumeUniversal,
                             out validDate))
                    {
                        return validDate;
                    }
                }

                if (DateTime.TryParseExact(dateToParse?.Trim(), format,
                         provider, styles, out validDate))
                {
                    return validDate;
                }
            }

            return DateTime.MinValue;
        }        
        public static DateTime ConvertToDateTimeDefaultDateNow(object value)
        {
            DateTime returnValue;

            //var ci = new CultureInfo("en-US");
            //var dtf = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", ShortTimePattern = "HH:mm:ss" };
            //ci.DateTimeFormat = dtf;
            //System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            if (null == value || !DateTime.TryParse(value.ToString().Trim(), out returnValue))
            {
                returnValue = DateTime.Now;
            }
            return returnValue;
        }
        public static DateTime ConvertToDateTimeDefaultMaxDate(object value)
        {
            DateTime returnValue;

            //var ci = new CultureInfo("en-US");
            //var dtf = new DateTimeFormatInfo { ShortDatePattern = "dd/MM/yyyy", ShortTimePattern = "HH:mm:ss" };
            //ci.DateTimeFormat = dtf;
            //System.Threading.Thread.CurrentThread.CurrentCulture = ci;

            if (null == value || !DateTime.TryParse(value.ToString(), out returnValue))
            {
                returnValue = DateTime.MaxValue;
            }
            return returnValue;
        }
        public static TimeSpan ConvertToTimeSpan(object value)
        {
            TimeSpan timeSpan;
            if (null == value || !TimeSpan.TryParse(value.ToString(), out timeSpan))
            {
                timeSpan = TimeSpan.MinValue;
            }
            return timeSpan;

        }
        public static long DateTimeToSpan(DateTime dateTime)
        {
            var span = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            return (long)(span.TotalMilliseconds);
        }
        public static long DateTimeToSpan10(DateTime dateTime)
        {
            var timeSpan = (dateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            return (long)timeSpan.TotalSeconds;
        }
        public static bool ConvertToBoolean(object value)
        {
            bool returnValue;
            if (null == value || !bool.TryParse(value.ToString(), out returnValue))
            {
                returnValue = false;
            }
            return returnValue;
        }
        public static byte ConvertToByte(object value)
        {
            byte returnValue;
            if (null == value || !byte.TryParse(value.ToString(), out returnValue))
            {
                returnValue = 0;
            }
            return returnValue;
        }    
        #endregion

        #region Old functions

        public static long ConvertToInt(object obj, long defaultValue = 0)
        {
            //long result;
            //return long.TryParse(obj.ToString(), out result) ? result : defaultValue;

            long returnValue;
            if (null == obj || !long.TryParse(obj.ToString(), out returnValue))
            {
                returnValue = defaultValue;
            }
            return returnValue;
        }

        public static int ConvertToInt(object obj, int defaultValue = 0)
        {
            //int result;
            //return int.TryParse(obj.ToString(), out result) ? result : defaultValue;

            int returnValue;
            if (null == obj || !int.TryParse(obj.ToString(), out returnValue))
            {
                returnValue = defaultValue;
            }
            return returnValue;
        }

        public static bool ConvertToBoolean(object obj, bool defaultValue = true)
        {
            bool returnValue;
            if (null == obj || !bool.TryParse(obj.ToString(), out returnValue))
            {
                returnValue = defaultValue;
            }
            return returnValue;
        }

        public static string JavaScriptSring(string input)
        {
            input = input.Replace("'", "\\u0027");
            input = input.Replace("\"", "\\u0022");
            return input;
        }

        const string UniChars = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ";
        const string KoDauChars = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU";

        public static string UnicodeToKoDau(string s)
        {
            var retVal = "";
            for (int i = 0; i < s.Length; i++)
            {
                var pos = UniChars.IndexOf(s[i].ToString());
                if (pos >= 0)
                    retVal += KoDauChars[pos];
                else
                    retVal += s[i];
            }
            return retVal;
        }

        public static string ConvertToUnsignChar(string inputString)
        {
            return ConvertTextToLink(inputString, " ");
        }

        public static string UnicodeToKoDauAndGach(string s)
        {
            const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789/- ";
            s = UnicodeToKoDau(s.ToLower().Trim());
            var sReturn = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (strChar.IndexOf(s[i]) > -1)
                {
                    if (s[i] != ' ')
                        sReturn += s[i];
                    else if (i > 0 && s[i - 1] != ' ' && s[i - 1] != '-')
                        sReturn += "-";
                }
            }
            return sReturn.Replace("--", "-").Replace("/", "-").Replace("²","");
        }
        public static string UnicodeToKoDauNotSpace(string s)
        {
            const string strChar = "abcdefghijklmnopqrstxyzuvxw0123456789/- ";
            s = UnicodeToKoDau(s.ToLower().Trim());
            var sReturn = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (strChar.IndexOf(s[i]) > -1)
                {
                    if (s[i] != ' ')
                        sReturn += s[i];
                    else if (i > 0 && s[i - 1] != ' ' && s[i - 1] != '-')
                        sReturn += "";
                }
            }
            return sReturn.Replace("--", "").Replace("/", "").Replace("²", "");
        }

        #endregion

        #region new functions

        public static string AddSlash(string input)
        {
            var str = !string.IsNullOrEmpty(input) ? input.Trim() : "";
            if (str != "")
            {
                str = str.Replace("'", "'").Replace("\"", "\\\"");
            }
            return str;
        }

        public static string ReplaceSpaceToPlus(string input)
        {
            return string.IsNullOrEmpty(input) ? input : Regex.Replace(input, "\\s+", "+", RegexOptions.IgnoreCase);
        }

        public static string ConvertCurentce(string sNumber)
        {
            var num = 3;
            var num2 = 0;
            for (int i = 1; i <= (sNumber.Length / 3); i++)
            {
                if ((num + num2) < sNumber.Length)
                {
                    sNumber = sNumber.Insert((sNumber.Length - num) - num2, ".");
                }
                num += 3;
                num2++;
            }
            return sNumber;
        }

        public static string ConvertTextToLink(string inputString, params string[] paternList)
        {
            if (inputString == "")
            {
                return "";
            }
            var input = inputString.Trim().ToLower();
            var replacement = "-";
            if (paternList.Length > 0)
            {
                replacement = paternList[0];
            }
            var str = Regex.Replace(input, "^[0-9]+", string.Empty);
            str = Regex.Replace(str, "\x00e1|\x00e0|ạ|ả|\x00e3|ă|ắ|ằ|ặ|ẳ|ẵ|\x00e2|ấ|ầ|ậ|ẩ|ẫ", "a");
            str = Regex.Replace(str, "đ|Đ", "d");
            str = Regex.Replace(str, "\x00f3|\x00f2|ọ|ỏ|\x00f5|ơ|ớ|ờ|ợ|ở|ỡ|\x00f4|ố|ồ|ộ|ổ|ỗ", "o");
            str = Regex.Replace(str, "\x00fa|\x00f9|ụ|ủ|ũ|ư|ứ|ừ|ự|ử|ữ", "u");
            str = Regex.Replace(str, "\x00e9|\x00e8|ẹ|ẻ|ẽ|\x00ea|ế|ề|ệ|ể|ễ", "e");
            str = Regex.Replace(str, "\x00ed|\x00ec|ị|ỉ|ĩ", "i");
            str = Regex.Replace(str, "\x00fd|ỳ|ỵ|ỷ|ỹ", "y");
            str = Regex.Replace(str, "\"|'|_+|\\.+", string.Empty);

            str = Regex.Replace(str, @"[^\w\-]", " ");
            str = Regex.Replace(str, @"-$|-+|\s+", replacement);
            return str;
        }

        public static string ConvertTextToUnsignName(string inputString, params string[] paternList)
        {
            if (inputString == "")
            {
                return "";
            }
            var input = inputString.Trim().ToLower();
            var replacement = " ";
            if (paternList.Length > 0)
            {
                replacement = paternList[0];
            }
            var str = Regex.Replace(input, "^[0-9]+", string.Empty);
            str = Regex.Replace(str, "\x00e1|\x00e0|ạ|ả|\x00e3|ă|ắ|ằ|ặ|ẳ|ẵ|\x00e2|ấ|ầ|ậ|ẩ|ẫ", "a");
            str = Regex.Replace(str, "đ|Đ", "d");
            str = Regex.Replace(str, "\x00f3|\x00f2|ọ|ỏ|\x00f5|ơ|ớ|ờ|ợ|ở|ỡ|\x00f4|ố|ồ|ộ|ổ|ỗ", "o");
            str = Regex.Replace(str, "\x00fa|\x00f9|ụ|ủ|ũ|ư|ứ|ừ|ự|ử|ữ", "u");
            str = Regex.Replace(str, "\x00e9|\x00e8|ẹ|ẻ|ẽ|\x00ea|ế|ề|ệ|ể|ễ", "e");
            str = Regex.Replace(str, "\x00ed|\x00ec|ị|ỉ|ĩ", "i");
            str = Regex.Replace(str, "\x00fd|ỳ|ỵ|ỷ|ỹ", "y");
            str = Regex.Replace(str, "\"|'|_+|\\.+", string.Empty);

            str = Regex.Replace(str, @"[^\w\-]", " ");
            str = Regex.Replace(str, @"-$|-+|\s+", replacement);
            return str;
        }

        public static string QuoteString(string inputString)
        {
            var str = inputString.Trim();
            if (str != "")
            {
                str = str.Replace("'", "''");
            }
            return str;
        }

        public static string RemoveStrHtmlTags(string inputString)
        {
            var input = inputString.Trim();
            if (input != "")
            {
                input = Regex.Replace(input, @"<(.|\n)*?>", string.Empty);
            }
            return input;
        }

        public static string ReplaceSpecialCharater(string inputString)
        {
            inputString = inputString.Trim();
            inputString = inputString.Replace(@"\", @"\\");
            inputString = inputString.Replace("\"", "&quot;");
            inputString = inputString.Replace("“", "&ldquo;");
            inputString = inputString.Replace("”", "&rdquo;");
            inputString = inputString.Replace("‘", "&lsquo;");
            inputString = inputString.Replace("’", "&rsquo;");
            inputString = inputString.Replace("'", "&#39;");
            return inputString;
        }

        public static string WrapString(string inputString, int length)
        {
            var str = string.Empty;
            var startIndex = 0;
            for (var i = (int)Math.Ceiling(inputString.Length / ((double)length)); (startIndex < inputString.Length) && (i > 1); i--)
            {
                str = str + inputString.Substring(startIndex, length) + ' ';
                startIndex += length;
            }
            return (str + inputString.Substring(startIndex));
        }

        public static string EncryptUserName(string userName, string host, string secretkey, string prefComparison)
        {
            if (host.IndexOf(prefComparison) == -1)
            {
                if (!string.IsNullOrEmpty(secretkey))
                {
                    userName = userName + secretkey;
                }
            }
            string userNameEncrypt = Crypton.EncryptForHTML(userName);
            userNameEncrypt = userNameEncrypt.Length > 30 ? userNameEncrypt.Substring(0, 30) : userNameEncrypt;
            userNameEncrypt = ConvertTextToLink(userNameEncrypt, "-");
            return userNameEncrypt;
        }

        public enum Browser { MozilaFirefox = 1, InternetExplorer = 2, AppleSafari = 3, NetScapeOpera = 4, Other = 5 }

        public static bool DetectBrowser(Browser browser, string browserType)
        {
            int result;
            switch (browserType.ToLower())
            {
                case "ie":
                    result = (int)Browser.InternetExplorer;
                    break;
                case "firefox":
                    result = (int)Browser.MozilaFirefox;
                    break;
                case "safari":
                    result = (int)Browser.AppleSafari;
                    break;
                default:
                    result = (int)Browser.Other;
                    break;
            }
            return (int)browser == result;
        }

        public static string CreateMD5Checksum(string data)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checksumBytes = md5.ComputeHash(Encoding.Unicode.GetBytes(data));
            return BitConverter.ToString(checksumBytes).Replace("-", string.Empty);
        }

        public static string GenerateRandomString(int length)
        {
            //Initiate objects & vars    
            var random = new Random();
            var randomString = "";

            //Loop ‘length’ times to generate a random number or character
            for (var i = 0; i < length; i++)
            {
                var randNumber = random.Next(1, 3) == 1 ? random.Next(97, 123) : random.Next(48, 58);

                //append random char or digit to random string
                randomString = randomString + (char)randNumber;
            }
            //return the random string
            return randomString;
        }

        public static string GetMACAddress()
        {
            var nics = NetworkInterface.GetAllNetworkInterfaces();
            var sMacAddress = string.Empty;
            foreach (var adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    var properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }

        /// <summary>
        /// Compares two instances of System.DateTime and returns an integer that indicates
        /// where the dateFrom earlier or same as or later the dateTo.<br />
        /// <example>
        /// dateFrom = new DateTime(2009, 8, 1, 0, 0, 0);
        /// dateTo = new DateTime(2009, 8, 1, 12, 0, 0);
        /// result = 1 (dateFrom is earlier than dateTo)
        /// </example> 
        /// </summary>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns>integer</returns>
        public static int CompareDiffirenceDate(DateTime dateFrom, DateTime dateTo)
        {
            var date1 = new DateTime(dateFrom.Year, dateFrom.Month, dateFrom.Day, dateFrom.Hour, dateFrom.Minute, 0);
            var date2 = new DateTime(dateTo.Year, dateTo.Month, dateTo.Day, dateTo.Hour, dateTo.Minute, 0);
            return DateTime.Compare(date1, date2);
        }

        public static double DateDiff(DateTime dateFrom, DateTime dateTo, string instant)
        {
            var diff = dateTo - dateFrom;
            double result = 0;
            switch (instant.ToLower())
            {
                case "d":
                    result = diff.TotalDays;
                    break;
                case "h":
                    result = diff.TotalHours;
                    break;
                case "m":
                    result = diff.TotalMinutes;
                    break;
                case "s":
                    result = diff.TotalSeconds;
                    break;
            }
            return result;
        }

        public static int CountWords(string stringInput)
        {
            if (string.IsNullOrEmpty(stringInput)) return 0;
            // Replaces all HTML tags
            stringInput = RemoveStrHtmlTags(stringInput);
            // Counts all words
            var collection = Regex.Matches(stringInput, @"[\S]+");
            // Returns number words.
            return collection.Count;
        }

        public static long SetPublishedDate(DateTime dateTime)
        {
            const string formatDate = "yyyyMMddHHmmssFFF";
            var dt = dateTime;
            if (dt <= DateTime.MinValue)
            {
                dt = DateTime.Now;
            }

            var dateToString = dt.ToString(formatDate);
            while (dateToString.Length < formatDate.Length)
            {
                dateToString += "0";
            }
            return long.Parse(dateToString);
        }

        public static long SetPublishedDate()
        {
            const string format = "yyyyMMddHHmmssFFF";
            var stringNewsId = DateTime.Now.ToString(format);
            while (stringNewsId.Length < format.Length)
            {
                stringNewsId += "0";
            }
            return long.Parse(stringNewsId);
        }

        /// <summary>
        /// compress script content
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string StripWhitespace(string content)
        {
            var lines = content.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var emptyLines = new StringBuilder();
            foreach (var s in lines.Select(line => line.Trim()).Where(s => s.Length > 0 && !s.StartsWith("//")))
            {
                emptyLines.AppendLine(s.Trim());
            }
            content = emptyLines.ToString();

            // remove C styles comments
            content = Regex.Replace(content, "/\\*.*?\\*/", String.Empty, RegexOptions.Compiled | RegexOptions.Singleline);
            // remove line comments
            //content = Regex.Replace(content, "//.*\r\n", String.Empty, RegexOptions.Compiled | RegexOptions.Singleline);
            var reg = new Regex("//([^/]+)//([^/]+)[\r\t\n]+", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            const string http = "http:";
            content = reg.Replace(content, delegate (System.Text.RegularExpressions.Match m)
            {
                var length = m.Groups[1].Value.Length;
                if (length > http.Length)
                {
                    var h = m.Groups[1].Value.Substring(length - http.Length);
                    return h.Equals(http) ? m.Groups[0].Value : m.Groups[1].Value;
                }
                return m.Groups[1].Value;
            });
            //// trim left
            content = Regex.Replace(content, "^\\s*", String.Empty, RegexOptions.Compiled | RegexOptions.Multiline);
            //// trim right
            content = Regex.Replace(content, "\\s*[\\r\\n]", "\r\n", RegexOptions.Compiled | RegexOptions.ECMAScript);
            // remove whitespace beside of left curly braced
            content = Regex.Replace(content, "\\s*{\\s*", "{", RegexOptions.Compiled | RegexOptions.ECMAScript);
            // remove whitespace beside of coma
            content = Regex.Replace(content, "\\s*,\\s*", ",", RegexOptions.Compiled | RegexOptions.ECMAScript);
            // remove whitespace beside of semicolon
            content = Regex.Replace(content, "\\s*;\\s*", ";", RegexOptions.Compiled | RegexOptions.ECMAScript);
            // remove newline after keywords
            content = Regex.Replace(content, "\\r\\n(?<=\\b(abstract|boolean|break|byte|case|catch|char|class|const|continue|default|delete|do|double|else|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|while|with)\\r\\n)", " ", RegexOptions.Compiled | RegexOptions.ECMAScript);
            content = Regex.Replace(content, @"[\n\r]+\s*", string.Empty); // space
            return content;
        }

        public static string SubWordInString(string inputString, int maxWord)
        {
            if (string.IsNullOrEmpty(inputString)) return inputString;
            inputString = Regex.Replace(inputString, "\\s+", " ");
            string[] words = Regex.Split(inputString, " ");
            if (words.Length <= maxWord) return inputString;
            else
            {
                inputString = string.Empty;
                for (int i = 0; i < maxWord; i++)
                {
                    inputString += words[i] + " ";
                }
                return inputString.Trim() + "...";
            }
        }

        #endregion

        #region 

        public static string ImportWordFileV2(HttpPostedFile docFile, int zoneId)
        {
            var outputHtml = "";
            //var docFile = HttpContext.Current.Request.Files["word_file"];
            //var zoneId = GetQueryString.GetPost("zoneId", 0);            

            try
            {
                if (docFile != null && !string.IsNullOrEmpty(docFile.FileName))
                {
                    var subPath = "/Data/Temp/";
                    bool exists = Directory.Exists(HttpContext.Current.Server.MapPath(subPath));
                    if (!exists)
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(subPath));

                    var docTempFile =
                        HttpContext.Current.Server.MapPath("/Data/Temp/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") +
                                                           docFile.FileName);

                    // if doc file => use Spire.Doc convert it to docx file to process
                    if (docTempFile.EndsWith(".doc", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docTempFile = docTempFile + "x";
                        var document = new Document(docFile.InputStream) { JPEGQuality = 100 };
                        document.SaveToFile(docTempFile, FileFormat.Docx);
                        document.Close();
                    }
                    else if (docTempFile.EndsWith(".odt", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docTempFile = docTempFile.Substring(0, docTempFile.LastIndexOf(".", StringComparison.Ordinal)) + ".docx";
                        var document = new Document(docFile.InputStream) { JPEGQuality = 100 };
                        document.SaveToFile(docTempFile, FileFormat.Docx);
                        document.Close();
                    }
                    else
                    {
                        docFile.SaveAs(docTempFile);
                    }
                    if (File.Exists(docTempFile))
                    {
                        if (ConvertToHtmlV2(docTempFile, zoneId, ref outputHtml))
                        {
                            File.Delete(docTempFile);
                            // Remove Evaluation text if use Spire.Doc
                            outputHtml =
                                outputHtml.Replace(
                                    "<p>Evaluation Warning : The document was created with Spire.Doc for .NET.</p>", "");
                            outputHtml = ProcessTable(outputHtml);
                            return outputHtml;
                        }
                        File.Delete(docTempFile);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }

        public static string ProcessTable(string html)
        {
            if (CmsChannelConfiguration.GetAppSetting("HTML_TO_IMAGE_API_URL") != "")
            {
                try
                {
                    var prefix = string.Format("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"><link href=\"{0}/Themes/Cms/Styles/table_to_image.css\" type=\"text/css\" rel=\"stylesheet\"/>", CmsChannelConfiguration.GetAppSetting("HOST_BOOKMARK"));
                    var htmlDoc = IMSBrowserHelper.CreateHtmlDoc(html);

                    var elms = htmlDoc.DocumentNode.SelectNodes("//table");
                    if (elms != null)
                    {
                        for (var i = 0; i < elms.Count(); i++)
                        {
                            var elm = elms[i];
                            if (!string.IsNullOrEmpty(elm.InnerText))
                            {
                                var tableHtml = prefix + elm.OuterHtml;
                                var image = HtmlToImageServices.GenerateToImageBase64(tableHtml, "", "table-" + DateTime.Now.Ticks + ".png", 650);
                                HtmlNode node = HtmlNode.CreateNode(string.Format("<div><img src=\"{0}\"/></div>", image));
                                if (elm.ParentNode.ChildNodes.Contains(elm))
                                {
                                    elm.ParentNode.ReplaceChild(node, elm);
                                }
                            }
                        }
                    }

                    return htmlDoc.DocumentNode.InnerHtml;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    return html;
                }
            }
            else
            {
                Logger.WriteLog(Logger.LogType.Debug, "Skip ProcessTable, missing config: HTML_TO_IMAGE_API_URL");
                return html;
            }
        }

        public static bool ConvertToHtmlV2(string filePath, int zoneId, ref string outputHtml)
        {
            try
            {
                var byteArray = File.ReadAllBytes(filePath);
                using (var memoryStream = new MemoryStream())
                {
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    using (var doc = WordprocessingDocument.Open(memoryStream, true))
                    {
                        var settings = new HtmlConverterSettings()
                        {
                            PageTitle = "My Page Title"
                        };
                        //var accountName = Policy.GetAccountName();
                        var html = HtmlConverter.ConvertToHtml(doc, settings, imageHandler =>
                        {
                            #region Get file extension

                            var extension = imageHandler.ContentType.Split('/')[1].ToLower();
                            ImageFormat imageFormat = null;
                            switch (extension)
                            {
                                case "png":
                                    extension = "jpg";
                                    imageFormat = ImageFormat.Jpeg;
                                    break;
                                case "bmp":
                                    imageFormat = ImageFormat.Bmp;
                                    break;
                                case "jpeg":
                                    extension = "jpg";
                                    imageFormat = ImageFormat.Jpeg;
                                    break;
                                case "tiff":
                                    imageFormat = ImageFormat.Tiff;
                                    break;
                            }
                            // If the image format is not one that you expect, ignore it,
                            // and do not return markup for the link.
                            if (imageFormat == null)
                                return null;

                            #endregion

                            #region Init data for upload image

                            var width = imageHandler.Bitmap.Width;
                            var height = imageHandler.Bitmap.Height;

                            var imageMemoryStream = new MemoryStream();
                            imageHandler.Bitmap.Save(imageMemoryStream, imageFormat);
                            var imageInBytes = imageMemoryStream.ToArray();
                            string base64String = Convert.ToBase64String(imageInBytes);

                            if (!base64String.StartsWith("data:image"))
                            {
                                base64String = "data:image/" + extension + ";base64," + base64String;
                            }
                            imageMemoryStream.Close();
                            imageMemoryStream.Dispose();

                            #endregion

                            // Insert image into html document
                            return new XElement(Xhtml.img,
                                                new XAttribute(NoNamespace.src, base64String),
                                                new XAttribute("type", "photo"), null);
                        });
                        outputHtml = html.ToString();
                        outputHtml = HttpContext.Current.Server.HtmlDecode(outputHtml);
                        var extractBody = Regex.Match(outputHtml, @"<body[^>]*>(.*?)</body>",
                                                      RegexOptions.Singleline | RegexOptions.Compiled |
                                                      RegexOptions.IgnoreCase);
                        if (extractBody.Groups.Count >= 2)
                        {
                            outputHtml = extractBody.Groups[1].Value;
                        }
                        else
                        {
                            outputHtml = extractBody.Value;
                            outputHtml = new Regex(@"</?body( [^>]*|/)?>",
                                             RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                                             Replace(outputHtml, string.Empty);
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }

        public static bool ConvertToHtml(string filePath, ref string outputHtml)
        {
            try
            {
                var byteArray = File.ReadAllBytes(filePath);

                using (var memoryStream = new MemoryStream())
                {
                    memoryStream.Write(byteArray, 0, byteArray.Length);
                    using (var doc = WordprocessingDocument.Open(memoryStream, true))
                    {
                        doc.CompressionOption = CompressionOption.NotCompressed;
                        doc.MainDocumentPart.Document.Body.InnerXml =
                            doc.MainDocumentPart.Document.Body.InnerXml.Replace("<w:tab />", "<w:t>@@@@@</w:t>");
                        doc.MainDocumentPart.Document.Save();
                        var settings = new HtmlConverterSettings()
                        {
                            PageTitle = "My Page Title"
                        };
                        var html = HtmlConverter.ConvertToHtml(doc, settings);
                        outputHtml = html.ToString();
                        outputHtml = HttpContext.Current.Server.HtmlDecode(outputHtml);
                        var extractBody = Regex.Match(outputHtml, @"<body[^>]*>(.*?)</body>",
                                                      RegexOptions.Singleline | RegexOptions.Compiled |
                                                      RegexOptions.IgnoreCase);
                        if (extractBody.Groups.Count >= 2)
                        {
                            outputHtml = extractBody.Groups[1].Value;
                        }
                        else
                        {
                            outputHtml = extractBody.Value;
                            outputHtml = new Regex(@"</?body( [^>]*|/)?>",
                                             RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                                             Replace(outputHtml, string.Empty);
                        }

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return false;
        }
        #endregion

        public static string GetRemoteIp()
        {
            var context = HttpContext.Current;
            var ipList = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            return !string.IsNullOrEmpty(ipList) ? ipList.Split(',')[0] : context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string BuildRealImageUrl(object url)
        {
            return CmsChannelConfiguration.GetAppSetting(FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD) + url;
        }

        public static string FormatThumb(int width, int height, object url, DateTime? createdDate = null, string noImageUrl = "")
        {
            //var hostThumbFormat = (null == createdDate
            //                ? CmsChannelConfiguration.GetAppSetting(Constants.HOST_CMS_THUMB_FORMAT)
            //                : (createdDate <
            //                   Utility.ConvertToDateTime(CmsChannelConfiguration.GetAppSetting(Constants.TIME_TO_CHANGE_CMS))
            //                       ? CmsChannelConfiguration.GetAppSetting(Constants.HOST_CMS_OLD_THUMB_FORMAT)
            //                       : CmsChannelConfiguration.GetAppSetting(Constants.HOST_CMS_THUMB_FORMAT)));
            var hostThumbFormat = CmsChannelConfiguration.GetAppSetting("HOST_CMS_THUMB_FORMAT");

            //if (url == null || url.ToString() == "" || url.ToString().Length < 5)
            //{
            //    if (noImageUrl == "")
            //        url = CmsChannelConfiguration.GetAppSetting(Constants.HOST_STATIC) + "/images/avatar1.gif";
            //    else
            //        url = CmsChannelConfiguration.GetAppSetting(Constants.HOST_BOOKMARK) + noImageUrl;
            //}
            if (url.ToString().StartsWith("http://") || url.ToString().StartsWith("https://"))
            {
                return url.ToString();
            }
            else
            {
                return string.Format(hostThumbFormat, width, height, url);
            }
        }

        public static uint Crc32(string input)
        {
            var table = new uint[]{
            0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F,
                0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
                0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2,
                0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
                0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
                0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
                0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C,
                0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
                0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423,
                0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
                0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x01DB7106,
                0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
                0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D,
                0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
                0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
                0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
                0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7,
                0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
                0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA,
                0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
                0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81,
                0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
                0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84,
                0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
                0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
                0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
                0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E,
                0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
                0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55,
                0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
                0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28,
                0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
                0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F,
                0x72076785, 0x05005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
                0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
                0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
                0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69,
                0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
                0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC,
                0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
                0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693,
                0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
                0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
        };

            unchecked
            {
                uint crc = (uint)(((uint)0) ^ (-1));
                var len = input.Length;
                for (var i = 0; i < len; i++)
                {
                    crc = (crc >> 8) ^ table[
                        (crc ^ (byte)input[i]) & 0xFF
                ];
                }
                crc = (uint)(crc ^ (-1));

                if (crc < 0)
                {
                    crc += (uint)4294967296;
                }

                return crc;
            }
        }

        public static string GenerateSHA256String(string inputString)
        {
            try {
                var sha256 = SHA256.Create();
                var bytes = Encoding.UTF8.GetBytes(inputString);
                var hash = sha256.ComputeHash(bytes);
                return GetStringFromHash(hash);
            }
            catch
            {
                return string.Empty;
            }
        }
        private static string GetStringFromHash(byte[] hash)
        {
            var result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }
    }
}
