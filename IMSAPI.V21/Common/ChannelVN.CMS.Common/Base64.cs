﻿using System;
using System.Text;

namespace ChannelVN.CMS.Common
{
    public class Base64
    {
        public static string Decode(string data)
        {
            string str2;
            try
            {
                var decoder = new UTF8Encoding().GetDecoder();
                var bytes = Convert.FromBase64String(data);
                var chars = new char[decoder.GetCharCount(bytes, 0, bytes.Length)];
                decoder.GetChars(bytes, 0, bytes.Length, chars, 0);
                var str = new string(chars);
                str2 = str;
            }
            catch (Exception exception)
            {
                Logger.WriteLog(Logger.LogType.Error, exception.ToString());
            }
            return "";
        }

        public static string Encode(string data)
        {
            string str2=string.Empty;
            try
            {
                str2 = Convert.ToBase64String(Encoding.UTF8.GetBytes(data));

                str2 = str2.Split('=')[0]; // Remove any trailing '='s
                str2 = str2.Replace('+', '-'); // 62nd char of encoding
                str2 = str2.Replace('/', '_'); // 63rd char of encoding
            }
            catch (Exception exception)
            {
                Logger.WriteLog(Logger.LogType.Error, exception.ToString());
            }

            return str2;
        }        
    }
}
