﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Xml;

namespace ChannelVN.CMS.Common.ChannelConfig
{
    public class ServiceChannelConfiguration
    {
        #region Private members

        private static string ConfigFile
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    return HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["ChannelConfigFile"]);
                }
                return System.Web.Hosting.HostingEnvironment.MapPath(ConfigurationManager.AppSettings["ChannelConfigFile"]);
            }
        }

        private const string _CONFIG_CACHED_NAME = "API_CHANNEL_CONFIGURATION_CACHED_{0}";
        private const string _CONFIG_CACHED_NAME_FORMAT_CONNECTION_STRING = "API_CHANNEL_CONFIGURATION_CACHED_CONNECTION_STRING_{0}";
        private const string _CONFIG_CACHED_NAME_FORMAT_APPSETTING = "API_CHANNEL_CONFIGURATION_CACHED_FORMAT_APPSETTING_{0}";
        private const string _CONFIG_CACHED_NAME_FORMAT_SECRET_KEY = "API_CHANNEL_CONFIGURATION_CACHED_FORMAT_SECRET_KEY_{0}";

        private static ServiceChannelConfiguration InitConfig(string channelNamespace)
        {
            var outputConfigs = new ServiceChannelConfiguration()
            {
                SecretKey = "",
                ConnectionStrings = new Dictionary<string, string>(),
                AppSettings = new Dictionary<string, string>()
            };

            try
            {                
                var baseConfigFile = ConfigFile;

                if (File.Exists(baseConfigFile))
                {
                    GetConfigInFile(ref outputConfigs, baseConfigFile);
                }

                if (!string.IsNullOrEmpty(channelNamespace))
                {
                    var fileName = baseConfigFile.Substring(baseConfigFile.LastIndexOf(@"\") + 1);
                    var baseFolder = baseConfigFile.Substring(0, baseConfigFile.LastIndexOf(@"\") + 1);
                    var extenConfigFile = baseFolder + channelNamespace + @"\" + fileName;

                    if (File.Exists(extenConfigFile))
                    {
                        GetConfigInFile(ref outputConfigs, extenConfigFile);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }

            return outputConfigs;
        }

        private static void GetConfigInFile(ref ServiceChannelConfiguration channelConfig, string configFilePath)
        {
            if (channelConfig == null) channelConfig = new ServiceChannelConfiguration();

            var xmlConfigFile = new XmlDocument();
            xmlConfigFile.Load(configFilePath);

            var channelNodes = xmlConfigFile.SelectNodes("//Channel");
            if (channelNodes != null && channelNodes.Count > 0)
            {
                var channelNode = channelNodes[0];
                if (channelNode != null && channelNode.Attributes != null)
                {
                    var secretKey = channelNode.Attributes["secretKey"].Value;

                    if (!string.IsNullOrEmpty(secretKey))
                    {
                        channelConfig.SecretKey = secretKey;
                    }

                    #region ConnectionStrings

                    var connectionStrings = channelConfig.ConnectionStrings;
                    if (connectionStrings == null)
                    {
                        connectionStrings = new Dictionary<string, string>();
                    }

                    var connectionStringNodes = channelNode.SelectNodes("connectionStrings/add");
                    if (connectionStringNodes != null)
                    {
                        foreach (XmlNode connectionStringNode in connectionStringNodes)
                        {
                            if (connectionStringNode != null && connectionStringNode.Attributes != null)
                            {
                                if (connectionStrings.ContainsKey(connectionStringNode.Attributes["name"].Value))
                                {
                                    connectionStrings[connectionStringNode.Attributes["name"].Value] =
                                        connectionStringNode.Attributes["connectionString"].Value;
                                }
                                else
                                {
                                    connectionStrings.Add(connectionStringNode.Attributes["name"].Value,
                                                         connectionStringNode.Attributes["connectionString"].Value);
                                }
                            }
                        }
                    }
                    channelConfig.ConnectionStrings = connectionStrings;

                    #endregion

                    #region AppSettings

                    var appSettings = channelConfig.AppSettings;
                    if (appSettings == null)
                    {
                        appSettings = new Dictionary<string, string>();
                    }

                    var appSettingNodes = channelNode.SelectNodes("appSettings/add");
                    if (appSettingNodes != null)
                    {
                        foreach (XmlNode appSettingNode in appSettingNodes)
                        {
                            if (appSettingNode != null && appSettingNode.Attributes != null)
                            {
                                if (appSettings.ContainsKey(appSettingNode.Attributes["key"].Value))
                                {
                                    appSettings[appSettingNode.Attributes["key"].Value] =
                                        appSettingNode.Attributes["value"].Value;
                                }
                                else
                                {
                                    appSettings.Add(appSettingNode.Attributes["key"].Value,
                                                appSettingNode.Attributes["value"].Value);
                                }
                            }
                        }
                    }
                    channelConfig.AppSettings = appSettings;

                    #endregion
                }
            }
        }

        #endregion

        #region Channel configuration define

        public string SecretKey { get; set; }
        public Dictionary<string, string> ConnectionStrings { get; set; }
        public Dictionary<string, string> AppSettings { get; set; }

        #endregion

        #region public methods
        
        public static ServiceChannelConfiguration GetConfigs(string channelNamespace)
        {
            var cacheName = string.Format(_CONFIG_CACHED_NAME, channelNamespace);
            ServiceChannelConfiguration currentConfigs = null;
            //var currentConfigs =
            //        HttpContext.Current.Cache[cacheName] as ServiceChannelConfiguration;

            if (null == currentConfigs)
            {
                currentConfigs = InitConfig(channelNamespace);

                //HttpContext.Current.Cache.Remove(cacheName);
                //HttpContext.Current.Cache.Insert(cacheName, currentConfigs,
                //                                 new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                //                                 TimeSpan.Zero);
            }
            return currentConfigs;
        }

        public static string GetConnectionString(string channelNamespace, string connectionStringName, string decryptKey)
        {
            var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_CONNECTION_STRING,channelNamespace + "_" + connectionStringName);
            var currentValue = string.Empty;
            //var currentValue = HttpContext.Current.Cache[cachedKey] as string;
            if (string.IsNullOrEmpty(currentValue))
            {
                try
                {
                    currentValue = GetConfigs(channelNamespace).ConnectionStrings[connectionStringName];
                    currentValue = Crypton.DecryptByKey(currentValue, decryptKey);

                    //HttpContext.Current.Cache.Remove(cachedKey);
                    //HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                    //                                 new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                    //                                 TimeSpan.Zero);
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Fatal, "Read ServiceChannelConfig.GetConnectionString(" + connectionStringName + ") error => " + ex);
                }
            }
            return currentValue ?? (currentValue = "");
        }

        private static Dictionary<string, string> AppSettingCached { get; set; }
        public static string GetAppSetting(string channelNamespace, string key)
        {
            var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_APPSETTING,
                                          channelNamespace + "_" + key);
            var currentValue = string.Empty;
            if (HttpContext.Current != null)
            {
                currentValue = HttpContext.Current.Cache[cachedKey] as string;
                if (string.IsNullOrEmpty(currentValue))
                {
                    try
                    {
                        currentValue = GetConfigs(channelNamespace).AppSettings[key];

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                                                         new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                                                         TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "Read ServiceChannelConfig.GetAppSetting(" + key + ") error => " + ex);
                    }
                }
            }
            else
            {
                if (AppSettingCached == null || AppSettingCached.Count <= 0)
                {
                    AppSettingCached = InitConfig(channelNamespace).AppSettings;
                }
                currentValue = AppSettingCached[key];
            }
            return currentValue ?? (currentValue = "");
        }

        public static string GetSecretKey(string channelNamespace)
        {
            var cachedKey = string.Format(_CONFIG_CACHED_NAME_FORMAT_SECRET_KEY,
                                          channelNamespace);
            var currentValue = string.Empty;
            if (HttpContext.Current != null)
            {
                currentValue = HttpContext.Current.Cache[cachedKey] as string;
                if (string.IsNullOrEmpty(currentValue))
                {
                    try
                    {
                        currentValue = GetConfigs(channelNamespace).SecretKey;

                        HttpContext.Current.Cache.Remove(cachedKey);
                        HttpContext.Current.Cache.Insert(cachedKey, currentValue,
                                                         new CacheDependency(ConfigFile), DateTime.Now.AddMonths(1),
                                                         TimeSpan.Zero);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Fatal, "Read ServiceChannelConfig.GetSecretKey error => " + ex);
                    }
                }
            }
            else
            {
                currentValue = GetConfigs(channelNamespace).SecretKey;
            }
            return currentValue ?? (currentValue = "");
        }

        #endregion
    }
}