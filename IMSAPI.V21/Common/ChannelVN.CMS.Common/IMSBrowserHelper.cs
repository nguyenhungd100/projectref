﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using ChannelVN.CMS.Common.ChannelConfig;
using HtmlAgilityPack;

namespace ChannelVN.CMS.Common
{
    /// <summary>
    /// Created by FOX - 20/8/2014 -  SiteEditing, PageManager Helper
    /// </summary>
    public class IMSBrowserHelper
    {
        /// <summary>
        /// Tải nội dung website về
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string DownloadPage(string url, bool isAddReferCms = true)
        {
            try
            {
                var domain = CmsChannelConfiguration.GetAppSetting("FRONT_END_LOCAL_URL");
                if (string.IsNullOrEmpty(url)) url = domain;
                if (!url.StartsWith("http")) url = domain + url;

                //fox added 7.7.2015
                if (isAddReferCms)
                {
                    url = url.Contains("?") ? (url + "&refer=cms") : (url + "?refer=cms");
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.MaximumAutomaticRedirections = 10;
                request.AllowAutoRedirect = false;
                request.Referer = url;
                request.KeepAlive = false;
                request.ProtocolVersion = HttpVersion.Version10;
                request.ServicePoint.ConnectionLimit = 1;
                request.Headers.Add("Accept-Encoding", "gzip,deflate");
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36";
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                var response = (HttpWebResponse)request.GetResponse();
                var responseString = "";

                using (StreamReader r = new StreamReader(response.GetResponseStream()))
                {
                    responseString = r.ReadToEnd();
                }
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    response.Close();
                    response.Dispose();
                }
                return responseString;
            }
            catch (Exception ex)
            {
                return "IMSBrowserErrMsg: " + ex.ToString();
            }
        }
        /// <summary>
        /// Xóa flash không cần thiết trong trang
        /// </summary>
        /// <param name="filterFileUrl"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static HtmlDocument RemoveFlash(string filterFileUrl, ref HtmlDocument htmlDoc)
        {
            //get remove flash
            string pathF = HttpContext.Current.Server.MapPath(filterFileUrl);
            List<string> listRemoveFlash = new List<string>();
            var linesF = File.ReadAllLines(pathF);
            for (int i = 0; i < linesF.Length; i++)
            {
                listRemoveFlash.Add(linesF[i]);
            }
            List<HtmlNode> removeNode = new List<HtmlNode>();
            var flashNodes = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "object");

            foreach (var flash in flashNodes)
            {
                for (int i = 0; i < listRemoveFlash.Count; i++)
                {
                    if (flash.InnerHtml.Contains(listRemoveFlash[i]))
                    {
                        removeNode.Add(flash);
                    }
                }
            }
            for (int i = 0; i < removeNode.Count(); i++)
            {
                if (removeNode[i].ParentNode != null)
                {
                    if (removeNode[i].ParentNode.ChildNodes.Contains(removeNode[i]))
                    {
                        removeNode[i].ParentNode.RemoveChild(removeNode[i]);
                    }
                }
            }
            return htmlDoc;
        }
        /// <summary>
        /// Xóa js không cần thiết trong trang
        /// </summary>
        /// <param name="filterFileUrl"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public static HtmlDocument RemoveJavascript(string filterFileUrl, ref HtmlDocument htmlDoc)
        {
            //get remove js
            string path = HttpContext.Current.Server.MapPath(filterFileUrl);

            List<string> listRemoveJs = new List<string>();
            var lines = File.ReadAllLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                listRemoveJs.Add(lines[i]);
            }
            var jsNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "script");
            List<HtmlNode> removeNode = new List<HtmlNode>();
            foreach (var js in jsNode)
            {
                for (int i = 0; i < listRemoveJs.Count(); i++)
                {
                    if (js.InnerHtml.Contains(listRemoveJs[i]) ||
                        js.InnerText.Contains(listRemoveJs[i]))
                    {
                        removeNode.Add(js);
                    }
                    else
                    {
                        if (js.Attributes["src"] != null)
                        {
                            if (js.Attributes["src"].Value.ToLower().Contains(listRemoveJs[i].ToLower()))
                            {
                                removeNode.Add(js);
                            }
                        }
                    }
                }
            }
            for (int i = 0; i < removeNode.Count(); i++)
            {
                if (removeNode[i].ParentNode != null)
                {
                    if (removeNode[i].ParentNode.ChildNodes.Contains(removeNode[i]))
                    {
                        removeNode[i].ParentNode.RemoveChild(removeNode[i]);
                    }
                }
            }
            return htmlDoc;
        }
        /// <summary>
        /// Remove elements
        /// </summary>
        /// <param name="filterFileUrl"></param>
        /// <param name="htmlDoc"></param>
        /// <returns></returns>
        public static HtmlDocument RemoveElement(string filterFileUrl, ref HtmlDocument htmlDoc)
        {
            try
            {
                string path = HttpContext.Current.Server.MapPath(filterFileUrl);
                List<HtmlNode> removeNodes = new List<HtmlNode>();
                var lines = File.ReadAllLines(path);
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].StartsWith("."))
                    {
                        HtmlNodeCollection elms =
                            htmlDoc.DocumentNode.SelectNodes(string.Format("//*[contains(@class,'{0}')]",
                                                                           lines[i].Replace(".", "")));
                        if (elms != null)
                        {
                            foreach (var elm in elms)
                            {
                                removeNodes.Add(elm);
                            }
                        }
                    }
                    else if (lines[i].StartsWith("#"))
                    {
                        var elms =
                            htmlDoc.DocumentNode.SelectNodes(string.Format("//*[@id='{0}']", lines[i].Replace("#", "")));
                        if (elms != null)
                        {
                            foreach (var elm in elms)
                            {
                                removeNodes.Add(elm);
                            }
                        }
                    }
                }

                for (int i = 0; i < removeNodes.Count(); i++)
                {
                    if (removeNodes[i].ParentNode != null)
                    {
                        if (removeNodes[i].ParentNode.ChildNodes.Contains(removeNodes[i]))
                        {
                            removeNodes[i].ParentNode.RemoveChild(removeNodes[i]);
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return htmlDoc;
        }

        //public static string RewriteCssUrl(string inputContent, string rootUrl)
        //{
        //    var baseUrl = rootUrl.LastIndexOf("/", StringComparison.CurrentCultureIgnoreCase) > 0
        //        ? rootUrl.Substring(0, rootUrl.LastIndexOf("/", StringComparison.CurrentCultureIgnoreCase))
        //        : rootUrl;

        //    var domainUrl = rootUrl.IndexOf("/", 8, StringComparison.CurrentCultureIgnoreCase) > 0
        //        ? rootUrl.Substring(0, rootUrl.IndexOf("/", 8, StringComparison.CurrentCultureIgnoreCase))
        //        : rootUrl;

        //    // Process font's url in css file
        //    const string patternFontUrl = @"src:[\s]*url[\s]*\([\s]*(?<url>[^\)]*)\)";
        //    inputContent = new Regex(patternFontUrl, RegexOptions.Multiline |
        //                                             RegexOptions.IgnoreCase |
        //                                             RegexOptions.Compiled).Replace(inputContent,
        //                                                 delegate (Match match)
        //                                                 {
        //                                                     var matchString = match.ToString();
        //                                                     var currentUrl = match.Groups[1].ToString();
        //                                                     var fixUrl =
        //                                                         currentUrl.Replace("\"", "").Replace("'", "").Trim();
        //                                                     if (
        //                                                         !fixUrl.StartsWith("data:image/",
        //                                                             StringComparison.CurrentCultureIgnoreCase) &&
        //                                                         !fixUrl.StartsWith("http://",
        //                                                             StringComparison.CurrentCultureIgnoreCase) &&
        //                                                         !fixUrl.StartsWith("https://",
        //                                                             StringComparison.CurrentCultureIgnoreCase))
        //                                                     {
        //                                                         if (fixUrl.StartsWith("/"))
        //                                                         {
        //                                                             fixUrl = domainUrl + fixUrl;
        //                                                         }
        //                                                         else
        //                                                         {
        //                                                             fixUrl = baseUrl + "/" + fixUrl;
        //                                                         }
        //                                                     }
        //                                                     else if (
        //                                                         fixUrl.StartsWith("http://",
        //                                                             StringComparison.CurrentCultureIgnoreCase) ||
        //                                                         fixUrl.StartsWith("https://",
        //                                                             StringComparison.CurrentCultureIgnoreCase))
        //                                                     {
        //                                                         fixUrl =
        //                                                             fixUrl.Substring(
        //                                                                 fixUrl.IndexOf("://",
        //                                                                     StringComparison
        //                                                                         .CurrentCultureIgnoreCase) + 3);
        //                                                         fixUrl = domainUrl +
        //                                                                  fixUrl.Substring(fixUrl.IndexOf("/",
        //                                                                      StringComparison
        //                                                                          .CurrentCultureIgnoreCase));
        //                                                     }
        //                                                     fixUrl = "'" + fixUrl + "'";
        //                                                     return matchString.Replace(currentUrl, fixUrl);
        //                                                 });

        //    // Process resource's url in css file
        //    const string patternCssUrl = @"url[\s]*\([\s]*(?<url>[^\)]*)\)";
        //    inputContent = new Regex(patternCssUrl, RegexOptions.Multiline |
        //                                            RegexOptions.IgnoreCase |
        //                                            RegexOptions.Compiled).Replace(inputContent,
        //                                                delegate (Match match)
        //                                                {
        //                                                    var matchString = match.ToString();
        //                                                    var currentUrl = match.Groups[1].ToString();
        //                                                    var fixUrl =
        //                                                        currentUrl.Replace("\"", "").Replace("'", "").Trim();
        //                                                    if (
        //                                                        !fixUrl.StartsWith("data:image/",
        //                                                            StringComparison.CurrentCultureIgnoreCase) &&
        //                                                        !fixUrl.StartsWith("http://",
        //                                                            StringComparison.CurrentCultureIgnoreCase) &&
        //                                                        !fixUrl.StartsWith("https://",
        //                                                            StringComparison.CurrentCultureIgnoreCase))
        //                                                    {
        //                                                        if (fixUrl.StartsWith("/"))
        //                                                        {
        //                                                            fixUrl = domainUrl + fixUrl;
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            fixUrl = baseUrl + "/" + fixUrl;
        //                                                        }
        //                                                    }
        //                                                    fixUrl = "'" + fixUrl + "'";
        //                                                    return matchString.Replace(currentUrl, fixUrl);
        //                                                });
        //    return inputContent;
        //}
        /// <summary>
        /// Format lại thẻ script & css
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <param name="url"></param>
        /// <param name="currentModuleUrl"></param>
        /// <returns></returns>
        public static HtmlDocument ReFormatDocument(ref HtmlDocument htmlDoc, string url, string currentModuleUrl)
        {
            var jsNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "script");
            HtmlNode head = htmlDoc.DocumentNode.SelectSingleNode("/html/head");
            var domainUrl = new Uri(url).GetLeftPart(UriPartial.Authority);
            // Thêm base tag để hiển thị ảnh nếu ảnh không bắt đầu với http
            HtmlNode baseTag = htmlDoc.CreateElement("base");
            head.PrependChild(baseTag);
            baseTag.SetAttributeValue("href", domainUrl);
            //Nếu script không bắt đầu bằng http thì cộng thêm vào
            foreach (var js in jsNode)
            {
                if (js.Attributes["src"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(js.Attributes["src"].Value))
                    {
                        js.SetAttributeValue("src", domainUrl + js.Attributes["src"].Value);
                    }
                }
            }
            //Nếu style không bắt đầu bằng http thì phải cộng thêm domain vào
            var cssNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "style" || n.Name == "link");
            foreach (var css in cssNode)
            {
                if (css.Attributes["href"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(css.Attributes["href"].Value))
                    {
                        css.SetAttributeValue("href", domainUrl + css.Attributes["href"].Value);
                    }
                }
                if (css.Attributes["href"] != null)
                {
                    if (!css.Attributes["href"].Value.Contains("fonts.googleapis.com") &&
                        (css.Attributes["href"].Value.StartsWith(domainUrl) || css.Attributes["href"].Value.Replace("https://", "http://").StartsWith(domainUrl)))
                        css.SetAttributeValue("href",
                            currentModuleUrl + "/Browser/css/FakeCss.ashx?url=" +
                            css.Attributes["href"].Value);
                }
            }
            return htmlDoc;
        }
        /// <summary>
        /// Nhúng js vào page
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <param name="jsUrl"></param>
        /// <param name="isPrepend"></param>
        public static void EmbedJs(ref HtmlDocument htmlDoc, string jsUrl, bool isPrepend = true)
        {
            HtmlNode linkJs = htmlDoc.CreateElement("script");
            HtmlNode head = htmlDoc.DocumentNode.SelectSingleNode("/html/head");
            if (isPrepend)
                head.PrependChild(linkJs);
            else
                head.AppendChild(linkJs);
            linkJs.SetAttributeValue("type", "text/javascript");
            linkJs.SetAttributeValue("src", jsUrl);
        }
        /// <summary>
        /// Nhúng css vào page
        /// </summary>
        /// <param name="htmlDoc"></param>
        /// <param name="cssUrl"></param>
        /// <param name="isPrepend"></param>
        public static void EmbedCss(ref HtmlDocument htmlDoc, string cssUrl, bool isPrepend = true)
        {
            HtmlNode linkCss = htmlDoc.CreateElement("link");
            HtmlNode head = htmlDoc.DocumentNode.SelectSingleNode("/html/head");
            if (isPrepend)
                head.PrependChild(linkCss);
            else
                head.AppendChild(linkCss);
            linkCss.SetAttributeValue("rel", "stylesheet");
            linkCss.SetAttributeValue("href", cssUrl);
        }

        public static bool IsValidLink(object _link)
        {
            if (_link == null)
            {
                return true;
            }
            return ((_link + "").StartsWith("http://") || (_link + "").StartsWith("https://")) || (_link + "").StartsWith("#") || (_link + "").StartsWith("javascript:");
        }

        public static HtmlDocument CreateHtmlDoc(string html, bool isAppendHtmlTag = true)
        {
            if (isAppendHtmlTag)
            {
                html = "<html><head></head><body>" + html + "</body></html>";
            }
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.OptionFixNestedTags = true;
            htmlDoc.OptionCheckSyntax = false;
            htmlDoc.LoadHtml(html);
            return htmlDoc;
        }
    }
}