﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ChannelVN.CMS.Common
{
    [DataContract]
    public abstract class BoErrorMappingBase<T> : DictionaryBase
    {
        public string this[T code]
        {
            get
            {
                //return InnerHashtable.ContainsKey(code)
                //           ? InnerHashtable[code].ToString()
                //           : "Không tìm thấy thông tin ứng với mã lỗi";
                return GetMassageLanguage(code);
            }
        }

        public string GetMassageLanguage(T code)
        {
            var res = InnerHashtable.ContainsKey(code)
                           ? InnerHashtable[code].ToString()
                           : "Không tìm thấy thông tin ứng với mã lỗi";
            try
            {
                var res_man = Resources.Resource.ResourceManager;
                var language = WcfExtensions.WcfMessageHeader.Current.Language;

                if (!string.IsNullOrEmpty(language) && language.ToLower().Equals("en"))
                {
                    var msg = res_man.GetString(code.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("en"));
                    return msg == null ? res : msg;
                }
                else if(!string.IsNullOrEmpty(language) && language.ToLower().Equals("vi"))
                {
                    var msg = res_man.GetString(code.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("vi"));
                    return msg == null ? res : msg;
                }
                return res;
            }
            catch
            {
                return res;
            }
        }

        protected abstract void InitErrorMapping();

        protected BoErrorMappingBase()
        {
            #region General error

            InnerHashtable[ErrorCodeBase.Success] = "Xử lý thành công";
            InnerHashtable[ErrorCodeBase.UnknowError] = "Lỗi không xác định";
            InnerHashtable[ErrorCodeBase.Exception] = "Lỗi trong quá trình xử lý";
            InnerHashtable[ErrorCodeBase.BusinessError] = "Lỗi nghiệp vụ";
            InnerHashtable[ErrorCodeBase.InvalidRequest] = "Yêu cầu không hợp lệ";
            InnerHashtable[ErrorCodeBase.TimeOutSession] = "Phiên làm việc của bạn đã hết.";
            InnerHashtable[ErrorCodeBase.DuplicateError] = "Đã tồi tại";

            #endregion

            InitErrorMapping();
        }
        [DataContract]
        public enum ErrorCodeBase
        {
            #region General error

            [EnumMember]
            Success = 0,
            [EnumMember]
            UnknowError = 9999,
            [EnumMember]
            Exception = 9998,
            [EnumMember]
            BusinessError = 9997,
            [EnumMember]
            InvalidRequest = 9996,
            [EnumMember]
            TimeOutSession = -100,
            [EnumMember]
            DuplicateError = 1985,

            #endregion
        }
    }
}
