﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Common.RedisClientHelper
{
    public interface IRedisClientHelper
    {
        #region Create

        bool Add(string key, string value);
        bool Add(string key, string value, DateTime expiresAt);

        bool Add<T>(string key, T document) where T : class, new();
        bool Add<T>(string key, T document, DateTime expiresAt) where T : class, new();

        bool AddInHash<T>(string hashId, string key, T document) where T : class, new();
        bool AddInHash<T>(string key, T document) where T : class, new();

        bool AddInHash<T>(string hashId, IEnumerable<KeyValuePair<string, T>> data) where T : class, new();
        bool AddInHash<T>(IEnumerable<KeyValuePair<string, T>> data) where T : class, new();

        bool AddInSortedSet(string key, string value, double score);
        bool AddInSortedSet<T>(string value, double score) where T : class, new();

        #endregion

        #region Get

        T Get<T>(string key) where T : class, new();

        T GetFromHash<T>(string hashId, string key) where T : class, new();
        T GetFromHash<T>(string key) where T : class, new();

        double? GetScoreFromSortedSet(string key, string value);
        double? GetScoreFromSortedSet<T>(string value) where T : class, new();

        List<T> GetsFromHash<T>(string hashId, List<string> keys) where T : class, new();
        List<T> GetsFromHash<T>(List<string> keys) where T : class, new();

        List<T> GetsFromHash<T>(string hashId) where T : class, new();
        List<T> GetsFromHash<T>() where T : class, new();

        List<string> GetDomainModels(string matchPattern, int topSize);

        #endregion

        #region Remove

        bool Remove(string key);
        bool Remove<T>(string key) where T : class, new();

        bool RemoveFromHash<T>(string hashId, string key) where T : class, new();
        bool RemoveFromHash<T>(string key) where T : class, new();

        bool RemoveFromSortedSet(string key, string value);
        bool RemoveFromSortedSet<T>(string value) where T : class, new();

        bool RemoveScoreFromSortedSet(string key, double score);
        bool RemoveScoreFromSortedSet<T>(double score) where T : class, new();

        #endregion
    }
}
