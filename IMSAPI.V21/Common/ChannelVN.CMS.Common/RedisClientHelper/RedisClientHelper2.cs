﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace ChannelVN.CMS.Common.RedisClientHelper
{
    public class RedisClientHelper2 : IRedisClientHelper2, IDisposable
    {
        private static readonly object _synRoot = new object();

        private ConnectionMultiplexer _conn;
        private static Dictionary<string, ConnectionMultiplexer> dicConn = new Dictionary<string, ConnectionMultiplexer>();
        private IDatabase db { get; set; }
        private string _namespace = string.Empty;

        #region Constructor

        public RedisClientHelper2()
            : this(string.Empty)
        {
        }

        public RedisClientHelper2(string nameSpace)
        {
            Namespace = nameSpace;
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            //if (null != _conn && _conn.IsConnected)
            //{
            //    _conn.Close(false);
            //    _conn.Dispose();
            //    _conn = null;
            //}
        }

        #endregion

        #region Protected methods

        protected ConnectionMultiplexer GetConnection()
        {
            try
            {
                lock (_synRoot)
                {
                    if(!dicConn.TryGetValue(Namespace, out _conn) || _conn == null ||  !_conn.IsConnected)
                    {
                        _conn = ConnectionMultiplexer.Connect(GetRedisConfiguration(ConnectionString));

                        if (dicConn.ContainsKey(Namespace))
                        {
                            dicConn[Namespace] = _conn;
                        }
                        else
                        {
                            dicConn.Add(Namespace, _conn);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _conn;
        }

        private ConfigurationOptions GetRedisConfiguration(string connectionString)
        {
            var options = ConfigurationOptions.Parse(connectionString);
            options.AbortOnConnectFail = false;
            //options.Ssl = true;
            //options.Password = "";
            //options.AllowAdmin = true;
            //options.KeepAlive = 30;
            options.ConnectRetry = 3;
            options.ConnectTimeout = 7000;
            options.SyncTimeout = 7000;
            return options;
        }

        private IDatabase GetDBInstance()
        {
            try
            {
                lock (_synRoot)
                {
                    if (db == null)
                    {
                        db = GetConnection().GetDatabase();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return db;
        }

        protected string NameOf(object entity)
        {
            var name = "";
            if (entity != null)
            {
                if (string.IsNullOrEmpty(Namespace))
                    name = entity.GetType().Name.ToLower();
                else
                    name = (Namespace + ":" + entity.GetType().Name).ToLower();
            }
            return name;
        }

        protected string NameOf(params string[] values)
        {
            var name = "";

            if (null != values)
            {
                var strValue = string.Join(":", values);
                if (!string.IsNullOrEmpty(strValue))
                {
                    if (string.IsNullOrEmpty(Namespace))
                        name = strValue.ToLower();
                    else
                        name = (Namespace + ":" + strValue).ToLower();
                }
            }
            return name;
        }

        protected byte[] GetBytes(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            return Encoding.ASCII.GetBytes(value);
        }

        #endregion

        #region Properties        
        public string ConnectionString
        {
            get;
            set;
        }

        public string Namespace
        {
            get
            {
                return _namespace;
            }
            set
            {
                _namespace = value;
                if (!string.IsNullOrEmpty(_namespace))
                {
                    _namespace = _namespace.Trim();
                }
            }
        }
        #endregion

        #region Public methods

        #region Create

        public bool Add(string key, string value)
        {
            try
            {
                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(NameOf(key), value);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Add(string key, string value, DateTime expiresAt)
        {
            try
            {
                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(NameOf(key), value, new TimeSpan(expiresAt.Ticks));
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Add<T>(string key, T document) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);
                var valueJson = NewtonJson.Serialize(document);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(stringId, valueJson);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }
        public bool Add<T>(string key, T document, DateTime expiresAt) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);
                var valueJson = NewtonJson.Serialize(document);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(stringId, valueJson, new TimeSpan(expiresAt.Ticks));
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }
        public bool Add<T>(string key, List<T> documents) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);
                var valueJson = NewtonJson.Serialize(documents);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(stringId, valueJson);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }
        public bool Add<T>(string key, List<T> documents, long expiresTime) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);
                var valueJson = NewtonJson.Serialize(documents);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.StringSet(stringId, valueJson, TimeSpan.FromSeconds(expiresTime));
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string hashId, string key, T document) where T : class, new()
        {
            try
            {
                var valueJson = NewtonJson.Serialize(document);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.HashSet(NameOf(hashId), key, valueJson);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string key, T document) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return AddInHash<T>(hashId, key, document);
        }

        public bool AddInHashCustom<T>(string extHashName, string key, T document) where T : class, new()
        {
            var hashId = NameOf(typeof(T).Name, extHashName);
            return AddInHash<T>(hashId, key, document);
        }

        public bool AddInHashWithCustomName(string hashName, string key, string value)
        {
            try
            {
                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.HashSet(NameOf(hashName), key, value);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool SetExpireKey(string key, TimeSpan expireTime)
        {
            try
            {
                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.KeyExpire(NameOf(key), expireTime);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string hashId, IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            try
            {
                if (data == null) return false;

                var meta = new List<HashEntry>();
                meta.AddRange(data.Select(s => new HashEntry(s.Key, Common.NewtonJson.Serialize(s.Value))).ToList());
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    db.HashSet(NameOf(hashId), meta.ToArray());
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return AddInHash<T>(hashId, data);
        }

        public bool AddInSortedSet(string key, string value, double score)
        {
            try
            {
                var setId = NameOf(key);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.SortedSetAdd(setId, value, score);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInSortedSet<T>(string value, double score) where T : class, new()
        {
            var setId = typeof(T).Name;
            return AddInSortedSet(setId, value, score);
        }

        #endregion

        #region Get

        public T Get<T>(string key) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);

                T returnValue = default(T);
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = NewtonJson.Deserialize<T>(db.StringGet(stringId));
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public List<T> Gets<T>(string key) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var stringId = NameOf(typeof(T).Name, key);

                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    var res = db.StringGet(stringId);
                    if (res.HasValue)
                        results = NewtonJson.Deserialize<List<T>>(res);
                }
                return results;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return results;
            }
        }

        public T GetFromHash<T>(string hashId, string key) where T : class, new()
        {
            try
            {
                T returnValue = default(T);
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    var valueReply = db.HashGet(NameOf(hashId), key);
                    if (valueReply.HasValue)
                    {
                        returnValue = Common.NewtonJson.Deserialize<T>(valueReply);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public T GetFromHash<T>(string key) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return GetFromHash<T>(hashId, key);
        }

        public double? GetScoreFromSortedSet(string key, string value)
        {
            try
            {
                var setId = NameOf(key);

                double? returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.SortedSetScore(setId, value);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        public double? GetScoreFromSortedSet<T>(string value) where T : class, new()
        {
            var setId = typeof(T).Name;
            return GetScoreFromSortedSet(setId, value);
        }

        public List<T> GetsFromHash<T>(string hashId, List<RedisValue> keys) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var valueReplies = new List<RedisValue>();

                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    valueReplies = db.HashGet(NameOf(hashId), keys.ToArray()).ToList();
                }
                results.AddRange(valueReplies.Where(w => w.HasValue).Select(s => Common.NewtonJson.Deserialize<T>(s)).ToList());
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<T> GetsFromHash<T>(List<string> keys) where T : class, new()
        {
            var hashId = typeof(T).Name;
            var valueReplies = new List<RedisValue>();
            valueReplies.AddRange(keys.Select(s => (RedisValue)s));

            return GetsFromHash<T>(hashId, valueReplies);
        }

        public List<T> GetsFromHash<T>(string hashId) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var valueReplies = new List<HashEntry>();

                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    valueReplies = db.HashGetAll(NameOf(hashId)).ToList();
                }
                if (null != valueReplies)
                {
                    results.AddRange(valueReplies.Select(s => Common.NewtonJson.Deserialize<T>(s.Value)).ToList());
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<T> GetsFromHash<T>() where T : class, new()
        {
            var hashId = typeof(T).Name;
            return GetsFromHash<T>(hashId);
        }

        public string GetsFromHashWithCustomKey(string hashId, string key)
        {
            var valueReply = string.Empty;
            try
            {
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    valueReply = db.HashGet(NameOf(hashId), key);
                    return valueReply;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return valueReply;
        }

        #endregion

        #region Remove

        public bool Remove(string key)
        {
            try
            {
                var stringId = NameOf(key);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.KeyDelete(stringId);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Remove<T>(string key) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.KeyDelete(stringId);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromHash<T>(string hashId, string key) where T : class, new()
        {
            try
            {
                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.HashDelete(NameOf(hashId), key);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveMuiltiFromHash<T>(List<string> keys) where T : class, new()
        {
            try
            {
                var hashId = typeof(T).Name;
                long returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();

                    var valueReplies = new List<RedisValue>();
                    valueReplies.AddRange(keys.Select(s => (RedisValue)s));

                    returnValue = db.HashDelete(NameOf(hashId), valueReplies.ToArray());
                }
                return returnValue > 0;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromHash<T>(string key) where T : class, new()
        {
            var hashId = typeof(T).Name;
            return RemoveFromHash<T>(hashId, key);
        }

        public bool RemoveFromSortedSet(string key, string value)
        {
            try
            {
                var setId = NameOf(key);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.SortedSetRemove(setId, value);
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromSortedSet<T>(string value) where T : class, new()
        {
            var setId = typeof(T).Name;
            return RemoveFromSortedSet(setId, value);
        }

        public bool RemoveScoreFromSortedSet(string key, double score)
        {
            try
            {
                var setId = NameOf(key);

                bool returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.SortedSetRemoveRangeByScore(setId, score, score) > 0;
                }
                //return returnValue;
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveScoreFromSortedSet<T>(double score) where T : class, new()
        {
            var setId = typeof(T).Name;
            return RemoveScoreFromSortedSet(setId, score);
        }

        #endregion

        #endregion

        #region Queue

        public long PushQueue<T>(string key, T document) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);
                var valueJson = NewtonJson.Serialize(document);

                long returnValue;
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    returnValue = db.ListRightPush(stringId, valueJson);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return 0;
            }
        }

        public T PopQueue<T>(string key) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);

                T returnValue = default(T);
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    var data = db.ListLeftPop(stringId);
                    if (data.HasValue)
                        returnValue = NewtonJson.Deserialize<T>(data);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public List<T> ListQueue<T>(string key, int start, int num, ref long count) where T : class, new()
        {
            try
            {
                var stringId = NameOf(typeof(T).Name, key);

                var returnValue = new List<T>();
                //using (var conn = GetConnection())
                {
                    var db = GetDBInstance();
                    count = db.ListLength(stringId);
                    var data = db.ListRange(stringId, start, num);
                    if (data.Length > 0)
                        returnValue.AddRange(data.Where(w => w.HasValue).Select(s => NewtonJson.Deserialize<T>(s)).ToList());
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new List<T>();
            }
        }

        #endregion

    }
}
