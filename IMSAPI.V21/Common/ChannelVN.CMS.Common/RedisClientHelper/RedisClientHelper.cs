﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.Common.RedisClientHelper
{
    public class RedisClientHelper: IRedisClientHelper, IDisposable
    {
        private static readonly object _synRoot = new object();

        private RedisManagerPool _redisManager = null;
        private IRedisClient _client = null;
        private string _namespace = String.Empty;

        #region Constructor

        public RedisClientHelper()
            : this(String.Empty)
        {
        }

        public RedisClientHelper(string nameSpace)
        {
            this.Namespace = nameSpace;
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            if(null != _redisManager)
            {
                _redisManager.Dispose();
                _redisManager = null;
            }
        }

        #endregion

        #region Protected methods

        protected IRedisClient GetConnection()
        {
            try
            {
                lock (_synRoot)
                {
                    if (_redisManager == null)
                    {
                        _redisManager = new RedisManagerPool(this.ConnectionString);
                    }
                    if (_client == null)
                    {
                        _client = _redisManager.GetClient() as RedisClient;
                    }
                    else if (_client.HadExceptions)
                    {
                        _client.Dispose();
                        _client = _redisManager.GetClient() as RedisClient;
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return _client;
        }

        protected string NameOf(Object entity)
        {
            var name = "";
            if (entity != null)
            {
                if (String.IsNullOrEmpty(this.Namespace))
                    name = entity.GetType().Name.ToLower();
                else
                    name = (this.Namespace + ":" + entity.GetType().Name).ToLower();
            }
            return name;
        }

        protected string NameOf(params string[] values)
        {
            var name = "";

            if(null != values)
            {
                var strValue = String.Join(":", values);
                if (!String.IsNullOrEmpty(strValue))
                {
                    if (String.IsNullOrEmpty(this.Namespace))
                        name = strValue.ToLower();
                    else
                        name = (this.Namespace + ":" + strValue).ToLower();
                }
            }
            return name;
        }

        protected byte[] GetBytes(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return null;
            }
            return Encoding.ASCII.GetBytes(value);
        }

        #endregion

        #region Properties
        // Redis Connection Strings supports with multiple options specified on the QueryString.
        // 
        // Some examples of supported formats:
        //  localhost
        //  127.0.0.1:6379
        //  redis://localhost:6379
        //  password @localhost:6379
        //  clientid:password @localhost:6379
        //  redis://clientid:password@localhost:6380?ssl=true&db=1
        public string ConnectionString
        {
            get;
            set;
        }

        public string Namespace
        {
            get
            {
                return _namespace;
            }
            set
            {
                _namespace = value;
                if (!String.IsNullOrEmpty(_namespace))
                {
                    _namespace = _namespace.Trim();
                }
            }
        }
        #endregion

        #region Public methods

        #region Create
        
        public bool Add(string key, string value)
        {
            try
            {
                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Add<string>(this.NameOf(key), value);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Add(string key, string value, DateTime expiresAt)
        {
            try
            {
                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Add<string>(this.NameOf(key), value, expiresAt);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Add<T>(string key, T document) where T : class, new()
        {
            try
            {
                var stringId = this.NameOf(typeof(T).Name, key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Add<T>(stringId, document);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Add<T>(string key, T document, DateTime expiresAt) where T : class, new()
        {
            try
            {
                var stringId = this.NameOf(typeof(T).Name, key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Add<T>(stringId, document, expiresAt);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string hashId, string key, T document) where T : class, new()
        {
            try
            {
                var valueJson = Common.NewtonJson.Serialize(document);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.SetEntryInHash(this.NameOf(hashId), key, valueJson);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string key, T document) where T : class, new()
        {
            try
            {
                var hashId = typeof(T).Name;
                return this.AddInHash<T>(hashId, key, document);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(string hashId, IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            try
            {
                if(data == null) return false;

                var meta = new Dictionary<string, string>();
                foreach(var entry in data)
                {
                    meta[entry.Key] = Common.NewtonJson.Serialize(entry.Value);
                }

                using (var client = this.GetConnection())
                {
                    client.SetRangeInHash( this.NameOf(hashId), meta);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInHash<T>(IEnumerable<KeyValuePair<string, T>> data) where T : class, new()
        {
            try
            {
                var hashId = typeof(T).Name;
                return this.AddInHash<T>(hashId, data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInSortedSet(string key, string value, double score)
        {
            try
            {
                var setId = this.NameOf(key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.AddItemToSortedSet(setId, value, score);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool AddInSortedSet<T>(string value, double score) where T : class, new()
        {
            try
            {
                var setId = typeof(T).Name;
                return this.AddInSortedSet(setId, value, score);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        #endregion

        #region Get

        public T Get<T>(string key) where T : class, new()
        {
            try
            {
                var stringId = this.NameOf(typeof(T).Name, key);

                T returnValue = default(T);
                using (var client = this.GetConnection())
                {
                    returnValue = client.Get<T>(stringId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public T GetFromHash<T>(string hashId, string key) where T : class, new()
        {
            try
            {
                T returnValue = default(T);
                using (var client = this.GetConnection())
                {
                    var valueReply = client.GetValueFromHash(this.NameOf(hashId), key);
                    if (valueReply != null)
                    {
                        returnValue = Common.NewtonJson.Deserialize<T>(valueReply);
                    }
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public T GetFromHash<T>(string key) where T : class, new()
        {
            try
            {
                var hashId = typeof(T).Name;
                return this.GetFromHash<T>(hashId, key);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return default(T);
            }
        }

        public double? GetScoreFromSortedSet(string key, string value)
        {
            try
            {
                var setId = this.NameOf(key);

                double? returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.GetItemScoreInSortedSet(setId, value);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        public double? GetScoreFromSortedSet<T>(string value) where T : class, new()
        {
            try
            {
                var setId = typeof(T).Name;
                return this.GetScoreFromSortedSet(setId, value);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return null;
            }
        }

        public List<T> GetsFromHash<T>(string hashId, List<string> keys) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var valueReplies = new List<string>();

                using (var client = this.GetConnection())
                {
                    valueReplies = client.GetValuesFromHash(this.NameOf(hashId), keys.ToArray());
                }

                foreach (var valueReply in valueReplies)
                {
                    if (null != valueReply)
                    {
                        var result = Common.NewtonJson.Deserialize<T>(valueReply);
                        results.Add(result);
                    }
                    else
                    {
                        results.Add(default(T));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<T> GetsFromHash<T>(List<string> keys) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var hashId = typeof(T).Name;
                results = this.GetsFromHash<T>(hashId, keys);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<T> GetsFromHash<T>(string hashId) where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var valueReplies = new Dictionary<string, string>();

                using (var client = this.GetConnection())
                {
                    valueReplies = client.GetAllEntriesFromHash(this.NameOf(hashId));
                }
                if (null != valueReplies)
                {
                    foreach (var key in valueReplies.Keys)
                    {
                        if (valueReplies[key] != null)
                        {
                            var result = Common.NewtonJson.Deserialize<T>(valueReplies[key]);
                            results.Add(result);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<T> GetsFromHash<T>() where T : class, new()
        {
            var results = new List<T>();
            try
            {
                var hashId = typeof(T).Name;
                results = this.GetsFromHash<T>(hashId);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        public List<string> GetDomainModels(string matchPattern = null, int topSize = 10000)
        {
            var results = new List<string>();
            try
            {
                if (String.IsNullOrEmpty(matchPattern))
                {
                    matchPattern = "*";
                }
                IEnumerable<string> valueReplies = null;

                using (var client = this.GetConnection())
                {
                    valueReplies = client.ScanAllKeys(this.NameOf(matchPattern), topSize);
                }
                if (null != valueReplies)
                {
                    results = new List<string>(valueReplies);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
            }
            return results;
        }

        #endregion

        #region Remove

        public bool Remove(string key)
        {
            try
            {
                var stringId = this.NameOf(key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Remove(stringId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool Remove<T>(string key) where T : class, new()
        {
            try
            {
                var stringId = this.NameOf(typeof(T).Name, key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.Remove(stringId);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromHash<T>(string hashId, string key) where T : class, new()
        {
            try
            {
                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.RemoveEntryFromHash(this.NameOf(hashId), key);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromHash<T>(string key) where T : class, new()
        {
            try
            {
                var hashId = typeof(T).Name;
                return this.RemoveFromHash<T>(hashId, key);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromSortedSet(string key, string value)
        {
            try
            {
                var setId = this.NameOf(key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.RemoveItemFromSortedSet(setId, value);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveFromSortedSet<T>(string value) where T : class, new()
        {
            try
            {
                var setId = typeof(T).Name;
                return this.RemoveFromSortedSet(setId, value);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveScoreFromSortedSet(string key, double score)
        {
            try
            {
                var setId = this.NameOf(key);

                bool returnValue;
                using (var client = this.GetConnection())
                {
                    returnValue = client.RemoveRangeFromSortedSetByScore(setId, score, score) > 0;
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        public bool RemoveScoreFromSortedSet<T>(double score) where T : class, new()
        {
            try
            {
                var setId = typeof(T).Name;
                return this.RemoveScoreFromSortedSet(setId, score);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return false;
            }
        }

        #endregion

        #endregion

    }
}
