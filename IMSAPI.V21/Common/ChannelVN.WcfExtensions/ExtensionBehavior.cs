﻿using System;
using System.ServiceModel.Configuration;

namespace ChannelVN.WcfExtensions
{
    public class ExtensionBehavior : BehaviorExtensionElement
    {
        #region Overrides of BehaviorExtensionElement

        protected override object CreateBehavior()
        {
            return new ExtensionInspector();
        }

        public override Type BehaviorType
        {
            get { return typeof(ExtensionInspector); }
        }

        #endregion
    }
}
