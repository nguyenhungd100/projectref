﻿using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System;

namespace ChannelVN.WcfExtensions
{
    public class AppConfigs
    {
        public static string CmsApiNamespace
        {
            get
            {
                return GetString("CmsApi.Namespace");
            }
        }

        public static string CmsApiSecretKey
        {
            get
            {                
                return CreateMD5Checksum(GetString("CmsApi.SecretKey"));
            }
        }

        public static string CmsApiLanguage
        {
            get
            {
                return GetString("CmsApi.Language");
            }
        }

        public static bool IsValidClientSecretKey(string channelNamespance, string clientSecretKey)
        {
            //return CreateMD5Checksum(ServiceChannelConfiguration.GetSecretKey(channelNamespance)) == clientSecretKey;
            return true;
        }

        public static string CmsApiPasscode
        {
            get
            {
                return GetString("CmsApi.Passcode");
            }
        }
       
        public static long CmsApiTokenExpireTime
        {
            get
            {
                var value = GetInt64("CmsApi.TokenExpireTime");
                return value <= 0 ? 1200 : value;
            }
        }

        public static string GetString(string key)
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings[key]) ? ConfigurationManager.AppSettings[key].Trim() : string.Empty;
        }

        public static long GetInt64(string key)
        {           
            var number = 0L;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings[key])){
                long.TryParse(ConfigurationManager.AppSettings[key], out number);
            }
            return number;            
        }

        public static string CmsApiNamespaceInit
        {
            get
            {
                return GetString("CmsApi.NamespaceInit");
            }
        }

        public static string CmsApiPassInit
        {
            get { return GetString("CmsApi.PassInit"); }
        }

        public static string CreateMD5Checksum(string data)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] checksumBytes = md5.ComputeHash(Encoding.Unicode.GetBytes(data));
            return BitConverter.ToString(checksumBytes).Replace("-", string.Empty);
        }
    }
}
