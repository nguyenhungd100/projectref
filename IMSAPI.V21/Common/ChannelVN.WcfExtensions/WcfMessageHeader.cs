﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace ChannelVN.WcfExtensions
{
    [DataContract]
    public class WcfMessageHeader
    {
        public static WcfMessageHeader CustomWcfHeader { get; set; }

        public const string MessageName = "WcfMessageHeader";
        public const string MessageNamespace = "http://www.channelvn.net";

        public static WcfMessageHeader Current
        {
            get
            {
                var header = CustomWcfHeader;
                try
                {
                    var context = HttpContext.Current;
                    try
                    {
                        if (context != null)
                        {
                            //check cho request login lay nsp                        
                            if (!string.IsNullOrEmpty(context.Request.Params["namespace"]))
                            {
                                header.Namespace = context.Request.Params["namespace"].ToString();
                            }
                            if (!string.IsNullOrEmpty(context.Request.Params["lang"]))
                            {
                                header.Language = context.Request.Params["lang"].ToString();
                            }
                        }
                    }
                    catch{
                    }

                    if (context != null && context.User != null)
                    {                        
                        var principal = context.User as System.Security.Claims.ClaimsPrincipal;
                        //var claim = principal.Claims.FirstOrDefault(c => c.Type == "iss");
                        //var isRole = principal.Claims.FirstOrDefault(c => c.Type == "uisrole");                        
                        //if (claim != null)
                        //{                            
                        //    header.Namespace = CMS.Common.Crypton.Decrypt(claim.Value);                            
                        //}
                        //if (isRole != null)
                        //{                            
                        //    header.IsRole = CMS.Common.Crypton.Decrypt(isRole.Value).ToLower();
                        //}

                        var nsp = principal.Claims.FirstOrDefault(c => c.Type == "nsp");
                        if (nsp != null)
                        {
                            header.Namespace = Crypton.Decrypt(nsp.Value);
                        }

                        var lang = principal.Claims.FirstOrDefault(c => c.Type == "lang");
                        if (lang != null)
                        {
                            header.Language = Crypton.Decrypt(lang.Value);
                        }

                        if (!string.IsNullOrEmpty(context.User.Identity.Name))
                            header.ClientUsername = Crypton.Decrypt(context.User.Identity.Name).ToLower();                        
                    }
                }
                catch(Exception ex)
                {
                    header = CustomWcfHeader;
                }
                return header;
            }
        }

        static WcfMessageHeader()
        {
            if(CustomWcfHeader == null)
            {
                CustomWcfHeader = new WcfMessageHeader
                {
                    Namespace = AppConfigs.CmsApiNamespace,
                    SecretKey = AppConfigs.CmsApiSecretKey,
                    Language = AppConfigs.CmsApiLanguage
                };
            }
        }

        [DataMember]
        public string Namespace { get; set; }
        [DataMember]
        public string SecretKey { get; set; }
        [DataMember]
        public string ClientUsername { get; set; }
        [DataMember]
        public string IsRole { get; set; }
        [DataMember]
        public string Language { get; set; }
    }
}
