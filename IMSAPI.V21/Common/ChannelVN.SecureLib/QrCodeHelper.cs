﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;

namespace ChannelVN.SecureLib
{
    public class QrCodeHelper
    {
        /// <summary>
        /// Generates an img tag with a data uri encoded image of the QR code from the content given.
        /// </summary>
        /// <param name="secretKey">secrerKey</param>
        /// <param name="channelNamespace">channelNamespace</param>
        /// <returns>img tag contain</returns>
        public static string GenerateQrCode(string secretKey, string channelNamespace = "")
        {
            var content = string.Format("otpauth://totp/VCCorp?secret={1}&issuer=IMS%20OTP{0}",
                string.IsNullOrEmpty(channelNamespace) ? "" : "%20" + channelNamespace, secretKey);
            var enc = new QrEncoder(ErrorCorrectionLevel.H);
            var code = enc.Encode(content);

            var render = new GraphicsRenderer(new FixedModuleSize(5, QuietZoneModules.Zero), Brushes.Black, Brushes.White);

            using (var ms = new MemoryStream())
            {
                render.WriteToStream(code.Matrix, ImageFormat.Png, ms);

                byte[] image = ms.ToArray();

                return string.Format(@"<img src=""data:image/png;base64,{0}"" alt=""{1}"" />", Convert.ToBase64String(image), content);
            }
        }
    }
}
