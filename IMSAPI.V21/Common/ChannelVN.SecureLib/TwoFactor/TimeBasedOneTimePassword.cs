﻿using System;
using System.Configuration;
using System.Runtime.Caching;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using ChannelVN.CMS.Common;
using CacheItemPriority = System.Web.Caching.CacheItemPriority;

namespace ChannelVN.SecureLib.TwoFactor
{
    public static class TimeBasedOneTimePassword
    {
        public static readonly DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static MemoryCache _cache;

        static TimeBasedOneTimePassword()
        {
            _cache = new MemoryCache("TimeBasedOneTimePassword" + WcfExtensions.WcfMessageHeader.Current.Namespace); //ConfigurationManager.AppSettings["CmsApi.Namespace"]
        }

        public static string GetPassword(string secret)
        {
            return GetPassword(secret, GetCurrentCounter());
        }
        public static string GetPassword(string secret, int timeStep)
        {
            return GetPassword(secret, GetCurrentCounter(timeStep));
        }

        public static string GetPassword(string secret, DateTime epoch, int timeStep)
        {
            long counter = GetCurrentCounter(DateTime.UtcNow, epoch, timeStep);

            return GetPassword(secret, counter);
        }

        public static string GetPassword(string secret, DateTime now, DateTime epoch, int timeStep, int digits)
        {
            long counter = GetCurrentCounter(now, epoch, timeStep);

            return GetPassword(secret, counter, digits);
        }

        private static string GetPassword(string secret, long counter, int digits = 6)
        {
            return HashedOneTimePassword.GeneratePassword(secret, counter, digits);
        }

        private static long GetCurrentCounter()
        {
            return GetCurrentCounter(DateTime.UtcNow, UNIX_EPOCH, 30);
        }

        private static long GetCurrentCounter(int timeStep)
        {
            return GetCurrentCounter(DateTime.UtcNow, UNIX_EPOCH, timeStep);
        }

        private static long GetCurrentCounter(DateTime now, DateTime epoch, int timeStep)
        {
            return (long)(now - epoch).TotalSeconds / timeStep;
        }

        public static string GenerateEncryptKey(string privateKey)
        {
            return new Base32Encoder().Encode(Encoding.ASCII.GetBytes(privateKey)).ToUpper();
        }

        public static string GeneratePrivateKey(string inputKey)
        {
            // MD5 Encrypt
            var sb = new StringBuilder();
            var md5 = new MD5CryptoServiceProvider();
            var originalBytes = Encoding.UTF8.GetBytes(inputKey);

            foreach (var b in md5.ComputeHash(originalBytes))
                sb.Append(b.ToString("x2").ToLower());

            // Output MD5
            inputKey = sb.ToString();

            const int maxKeyLength = 10;
            const string defaultInputKey = "1234567890";
            if (string.IsNullOrEmpty(inputKey))
            {
                inputKey = defaultInputKey;
            }
            if (string.IsNullOrEmpty(inputKey) || inputKey.Length < maxKeyLength)
            {
                inputKey = inputKey + defaultInputKey.Substring(0, maxKeyLength - inputKey.Length);
            }
            return inputKey.Substring(0, 10).Replace('/', '0').Replace('+', '1').ToUpper();
        }

        public static bool IsValid(string secret, string password, int checkAdjacentIntervals = 1)
        {
            // Keeping a cache of the secret/password combinations that have been requested allows us to
            // make this a real one time use system. Once a secret/password combination has been tested,
            // it cannot be tested again until after it is no longer valid.
            // See http://tools.ietf.org/html/rfc6238#section-5.2 for more info.
            var cacheKey = string.Format("CACHE_OTP_{0}_{1}", secret, password);

            if (_cache.Contains(cacheKey))
            {
                throw new OneTimePasswordException("Bạn chỉ được sử dụng mã PIN một lần duy nhất.");
            }

            _cache.Add(cacheKey, password, new CacheItemPolicy { SlidingExpiration = TimeSpan.FromMinutes(1) });

            if (password == GetPassword(secret))
                return true;

            if (password == GetPassword(secret,60))
                return true;

            for (var i = 1; i <= checkAdjacentIntervals; i++)
            {
                if (password == GetPassword(secret, GetCurrentCounter() + i))
                    return true;

                if (password == GetPassword(secret, GetCurrentCounter() - i))
                    return true;

                if (password == GetPassword(secret, GetCurrentCounter(60) + i))
                    return true;

                if (password == GetPassword(secret, GetCurrentCounter(60) - i))
                    return true;
            }

            return false;
        }
    }
}