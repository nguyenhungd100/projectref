﻿INSERT INTO GroupPermission (Id, Name) VALUES (1, N'Quản lý bài viết')
INSERT INTO GroupPermission (Id, Name) VALUES (2, N'Quản lý comment')
INSERT INTO GroupPermission (Id, Name) VALUES (4, N'Quản lý hệ thống')
INSERT INTO GroupPermission (Id, Name) VALUES (8, N'Cài bài nổi bật')
--INSERT INTO GroupPermission (Id, Name) VALUES (9, N'Cài bài nổi bật mobile')
INSERT INTO GroupPermission (Id, Name) VALUES (10, N'Quản lý media')
INSERT INTO GroupPermission (Id, Name) VALUES (11, N'Quản lý box nhúng')
INSERT INTO GroupPermission (Id, Name) VALUES (12, N'Quản lý Tag')
INSERT INTO GroupPermission (Id, Name) VALUES (13, N'Chấm nhuận bút')
INSERT INTO GroupPermission (Id, Name) VALUES (14, N'Thảo luận')
INSERT INTO GroupPermission (Id, Name) VALUES (15, N'Quản lý bài đặc biệt')
INSERT INTO GroupPermission (Id, Name) VALUES (16, N'Thông tin khác')

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (1,1,N'Phóng viên',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (2,1,N'Biên tập viên',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (3,1,N'Thư ký',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (39,2,N'Nhận comment tin',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (31,2,N'Duyệt comment tin',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (43,2,N'Nhận comment bình chọn',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (44,2,N'Duyệt comment bình chọn',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (46,2,N'Nhận comment video',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (47,2,N'Duyệt comment video',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (11,4,N'Tạo user',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (12,4,N'Phân quyền',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (13,4,N'Xóa user',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (15,4,N'Thống kê',0)
--INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (16,4,N'Chấm nhuận bút',0)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (17,8,N'Cài bài trang chủ',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (18,8,N'Cài bài chuyên mục',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (20,10,N'Quản lý ảnh',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (21,10,N'Quản lý video',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (32,10,N'Kiểm duyệt video',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (22,11,N'Xem box nhúng',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (23,11,N'Cài bài box nhúng',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (30,11,N'Duyệt box nhúng ngoài trang',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (24,12,N'Quản lý MetaTag',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (25,12,N'Quản lý Tag',0)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (26,13,N'Xem nhuận bút',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (27,13,N'Chấm nhuận bút',1)
--INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (28,9,N'Chỉ xem',1)
--INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (29,9,N'Cài bài',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (33,14,N'Tạo topic thảo luận',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (34,14,N'Comment topic thảo luận',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (35,15,N'Bài giao lưu trực tuyến',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (36,15,N'Bài tường thuật sự kiện',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (37,15,N'Bài tường thuật bóng đá',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (38,15,N'Bài FunnyNews',1)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (40,16,N'Quản lý quảng cáo',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (41,16,N'Quản lý bình chọn chung',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (42,16,N'Quản lý bình chọn cá nhân',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (43,16,N'Xem và trích xuất Comment Poll',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (44,16,N'Quản lý Comment Poll',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (45,16,N'Đẩy bài sang đối tác',1)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (48,16,N'Quản lý tin Crawler',0)


INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (60,15,N'Landing Page cá nhân',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (61,15,N'Landing Page chung',0)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (62,15,N'Interactive cá nhân',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (63,15,N'Interactive chung',0)

INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (64,15,N'Quiz cá nhân',0)
INSERT INTO Permission (Id, GroupId, Name, IsGrantByCategory) VALUES (65,15,N'Quiz chung',0)

--43,44 comment poll

SET IDENTITY_INSERT [User] ON
INSERT INTO [User] (Id, UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass, IsSystem)
VALUES (1, 'Admin', 'VIvpPY2QHs5QTAfa2G4Wtw==', N'IMS Administrator', '', 'cms@channelvn.net', '0986806961', 1, 1, 1, GETDATE(), GETDATE(), NULL, NULL, 1)
SET IDENTITY_INSERT [User] OFF

INSERT INTO UserProfile (UserId, Address, Birthday, Description)
VALUES (1, N'VCCorp', '2012-01-01', N'IMS Administrator')

