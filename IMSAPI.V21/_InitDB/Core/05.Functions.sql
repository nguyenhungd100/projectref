﻿
GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fConvertDateTimeToBigInt]    Script Date: 03/28/2014 09:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CMS_fConvertDateTimeToBigInt] 
(
	@dateTimeValue datetime
)
RETURNS bigint
AS
BEGIN
	DECLARE @ReturnValue bigint
	IF @dateTimeValue IS NULL
		SET @ReturnValue = 0
	ELSE
		SET @ReturnValue = CONVERT(bigint,
				CONVERT(varchar(5), DATEPART(YEAR, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(MONTH, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(DAY, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(HOUR, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(MINUTE, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(SECOND, @dateTimeValue)) + 
				CONVERT(varchar(5), DATEPART(MILLISECOND, @dateTimeValue)))
	RETURN @ReturnValue
END



GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListOfProcessInterviewChannelIdsByInterviewIdAndUsername]    Script Date: 03/28/2014 09:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[CMS_fGetListOfProcessInterviewChannelIdsByInterviewIdAndUsername]
(
	@InterviewId int,
	@Username nvarchar(50),
	@ChannelRole int
)
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @ProcessChannelIds varchar(4000),
			@ProcessChannelId int

	SET @ProcessChannelIds = ''
	
	DECLARE ProcessChannelIdsCursor CURSOR FOR
	SELECT ICP.ProcessInterviewChannelId
	FROM InterviewChannel AS IC INNER JOIN
		InterviewChannelProcess AS ICP ON IC.Id = ICP.InterviewChannelId
	WHERE (IC.Username = @Username) 
		AND (IC.InterviewId = @InterviewId) 
		AND (IC.ChannelRole = @ChannelRole)

	OPEN ProcessChannelIdsCursor
	
	FETCH NEXT FROM ProcessChannelIdsCursor INTO @ProcessChannelId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		SET @ProcessChannelIds = @ProcessChannelIds + ';' + CONVERT(varchar(10), @ProcessChannelId)
			
		FETCH NEXT FROM ProcessChannelIdsCursor INTO @ProcessChannelId
	END   

	CLOSE ProcessChannelIdsCursor   
	DEALLOCATE ProcessChannelIdsCursor

	IF @ProcessChannelIds <> ''
		SET @ProcessChannelIds = SUBSTRING(@ProcessChannelIds, 2, LEN(@ProcessChannelIds) - 1)

	RETURN @ProcessChannelIds
END
GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListOfZoneIdByNewsId]    Script Date: 03/28/2014 09:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-28>
-- Description:	<Get list of zone id by news id>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListOfZoneIdByNewsId]
(
	@NewsId bigint
)
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @ZoneIds varchar(4000),
			@ZoneId int

	SET @ZoneIds = ''
	
	DECLARE ZoneCursor CURSOR FOR
	SELECT ZoneId
	FROM NewsInZone
	WHERE (NewsId = @NewsId)
	ORDER BY IsPrimary

	OPEN ZoneCursor
	
	FETCH NEXT FROM ZoneCursor INTO @ZoneId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		SET @ZoneIds = @ZoneIds + ',' + CONVERT(varchar(10), @ZoneId)
			
		FETCH NEXT FROM ZoneCursor INTO @ZoneId 
	END   

	CLOSE ZoneCursor   
	DEALLOCATE ZoneCursor

	IF @ZoneIds <> ''
		SET @ZoneIds = SUBSTRING(@ZoneIds, 2, LEN(@ZoneIds) - 1)

	RETURN @ZoneIds
END

GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListOfZoneIdByParentZoneId]    Script Date: 03/28/2014 09:59:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-06>
-- Description:	<Get list of zone id by parent zone id>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListOfZoneIdByParentZoneId]
(
	@ParentZoneId int
)
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @ZoneIds varchar(4000),
			@ZoneId int

	SET @ZoneIds = ''
	
	DECLARE ZoneCursor CURSOR FOR
	SELECT Id
	FROM Zone
	WHERE (Id = @ParentZoneId OR ParentId = @ParentZoneId)

	OPEN ZoneCursor
	
	FETCH NEXT FROM ZoneCursor INTO @ZoneId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		SET @ZoneIds = @ZoneIds + ',' + CONVERT(varchar(10), @ZoneId)
			
		FETCH NEXT FROM ZoneCursor INTO @ZoneId 
	END   

	CLOSE ZoneCursor   
	DEALLOCATE ZoneCursor

	IF @ZoneIds <> ''
		SET @ZoneIds = SUBSTRING(@ZoneIds, 2, LEN(@ZoneIds) - 1)

	RETURN @ZoneIds
END

GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListTagIdByNewsId]    Script Date: 03/28/2014 09:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-18>
-- Description:	<Get list tag id by NewsId>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListTagIdByNewsId]
(
	@NewsId bigint
)
RETURNS varchar(2000)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ReturnTagIds varchar(2000)
	DECLARE @TagId int
	
	SET @ReturnTagIds = ''
	
	IF @NewsId > 0
	BEGIN
		DECLARE tag_cursor CURSOR FOR
		SELECT TN.TagId
		FROM TagNews AS TN INNER JOIN Tag AS T ON TN.TagId = T.Id
		WHERE TN.NewsId = @NewsId AND T.IsThread = 0

		OPEN tag_cursor   
		FETCH NEXT FROM tag_cursor INTO @TagId

		WHILE @@FETCH_STATUS = 0   
		BEGIN
			SET @ReturnTagIds = @ReturnTagIds + ';' + CONVERT(varchar(10), @TagId)
				
			FETCH NEXT FROM tag_cursor INTO @TagId 
		END   

		CLOSE tag_cursor   
		DEALLOCATE tag_cursor
	END

	IF @ReturnTagIds <> ''
		SET @ReturnTagIds = SUBSTRING(@ReturnTagIds, 2, LEN(@ReturnTagIds) - 1)
		
	RETURN @ReturnTagIds
	
END

GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListTagIdByNewsIdAndTagMode]    Script Date: 03/28/2014 09:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-18>
-- Description:	<Get list tag id by NewsId>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListTagIdByNewsIdAndTagMode]
(
	@NewsId bigint,
	@TagMode int
)
RETURNS varchar(MAX)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ReturnTagIds varchar(MAX)
	DECLARE @TagId int
	
	SET @ReturnTagIds = ''
	
	IF @NewsId > 0
	BEGIN
		DECLARE tag_cursor CURSOR FOR
		SELECT TN.TagId
		FROM TagNews AS TN INNER JOIN Tag AS T ON TN.TagId = T.Id
		WHERE TN.NewsId = @NewsId AND T.IsThread = 0 and TagMode =@TagMode

		OPEN tag_cursor   
		FETCH NEXT FROM tag_cursor INTO @TagId

		WHILE @@FETCH_STATUS = 0   
		BEGIN
			SET @ReturnTagIds = @ReturnTagIds + ';' + CONVERT(varchar(10), @TagId)
				
			FETCH NEXT FROM tag_cursor INTO @TagId 
		END   

		CLOSE tag_cursor   
		DEALLOCATE tag_cursor
	END

	IF @ReturnTagIds <> ''
		SET @ReturnTagIds = SUBSTRING(@ReturnTagIds, 2, LEN(@ReturnTagIds) - 1)
		
	RETURN @ReturnTagIds
	
END




GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListThreadIdByNewsId]    Script Date: 03/28/2014 09:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-18>
-- Description:	<Get list thread id by NewsId>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListThreadIdByNewsId]
(
	@NewsId bigint
)
RETURNS varchar(2000)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ReturnTagIds varchar(2000)
	DECLARE @TagId int
	
	SET @ReturnTagIds = ''
	
	IF @NewsId > 0
	BEGIN
		DECLARE tag_cursor CURSOR FOR
		SELECT TN.TagId
		FROM TagNews AS TN INNER JOIN Tag AS T ON TN.TagId = T.Id
		WHERE TN.NewsId = @NewsId AND T.IsThread = 1

		OPEN tag_cursor   
		FETCH NEXT FROM tag_cursor INTO @TagId

		WHILE @@FETCH_STATUS = 0   
		BEGIN
			SET @ReturnTagIds = @ReturnTagIds + ';' + CONVERT(varchar(10), @TagId)
				
			FETCH NEXT FROM tag_cursor INTO @TagId 
		END   

		CLOSE tag_cursor   
		DEALLOCATE tag_cursor
	END

	IF @ReturnTagIds <> ''
		SET @ReturnTagIds = SUBSTRING(@ReturnTagIds, 2, LEN(@ReturnTagIds) - 1)
		
	RETURN @ReturnTagIds
	
END


GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListZoneId]    Script Date: 03/28/2014 09:59:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-18>
-- Description:	<Get list zone id and child zone id by list zone id>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListZoneId]
(
	@ZoneIds varchar(MAX)
)
RETURNS varchar(2000)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ReturnZoneIds varchar(MAX)
	DECLARE @ZoneId int
	
	SET @ReturnZoneIds = ''
	
	IF @ZoneIds <> ''
	BEGIN
		DECLARE zone_cursor CURSOR FOR
		SELECT Id
		FROM Zone
		WHERE (PATINDEX('%,' + CONVERT(varchar(10), ParentId) + ',%', ',' + @ZoneIds + ',') > 0)
			AND (PATINDEX('%,' + CONVERT(varchar(10), Id) + ',%', ',' + @ZoneIds + ',') <= 0)

		OPEN zone_cursor   
		FETCH NEXT FROM zone_cursor INTO @ZoneId

		WHILE @@FETCH_STATUS = 0   
		BEGIN
			SET @ReturnZoneIds = @ReturnZoneIds + ',' + CONVERT(varchar(10), @ZoneId)
				
			FETCH NEXT FROM zone_cursor INTO @ZoneId 
		END   

		CLOSE zone_cursor   
		DEALLOCATE zone_cursor
	END

	SET @ReturnZoneIds = @ZoneIds + @ReturnZoneIds
		
	RETURN @ReturnZoneIds
	
END


GO
/****** Object:  UserDefinedFunction [dbo].[ConvertToBasicLatin]    Script Date: 03/28/2014 10:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        Doãn Hải Vân
-- Create date: <Create Date, ,>
-- Description:    Convert sang tiếng việt không dấu
-- =============================================
CREATE FUNCTION [dbo].[ConvertToBasicLatin]
(
    -- Add the parameters for the function here
    @Value    NVARCHAR(1000)
)
RETURNS VARCHAR(1000)
AS
BEGIN
    -- Declare the return variable here
    DECLARE @Results        VARCHAR(1000)
    DECLARE @UniChars        NVARCHAR(140)
    DECLARE    @UnsignChars    NVARCHAR(140)
    DECLARE    @AllowChars        VARCHAR(38)
       DECLARE @Position        INT
       DECLARE    @Index            INT
       DECLARE    @Char            NCHAR(1)
       DECLARE    @NonChar        NCHAR(1)
       DECLARE    @PreviousChar    NCHAR(1)

    -- Add the T-SQL statements to compute the return value here
    SET    @Results        = ''
    SET @Position        = 1
    SET @UniChars        = N'àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ'
    SET @UnsignChars    = N'aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU'
    SET @AllowChars        = 'abcdefghijklmnopqrstxyzuvxw0123456789,;'            
    SET    @PreviousChar    = ''
    WHILE @Position        <=    DATALENGTH(@Value) / 2
        BEGIN
            SET    @Char    = ''
            SET    @NonChar= ''
            SET    @Index    = 0
            SET    @Char    = LOWER(SUBSTRING(@Value, @Position, 1))
            IF    @Char    =    ' '    OR    @Char    =    '-'
                BEGIN
                    IF    @PreviousChar    !=    ' '    AND    @PreviousChar    !=    '-'
                        BEGIN
                            SET    @Results    =    @Results + '-'
                        END
                    SET    @PreviousChar    =    @Char
                END
            ELSE
                BEGIN
                    SET    @Index    = CHARINDEX(@Char , @UniChars)
                    IF    @Index    > 0
                        BEGIN
                            SET    @NonChar        =    LOWER(SUBSTRING(@UnsignChars, @Index, 1))
                            SET    @Results        =    @Results + @NonChar
                            SET    @PreviousChar    =    @Char
                        END
                    ELSE
                        BEGIN
                            SET    @Index    =    0
                            SET    @Index    =    CHARINDEX(@Char , @AllowChars)
                            IF    @Index    >    0
                                BEGIN
                                    SET    @Results        =    @Results + @Char
                                    SET    @PreviousChar    =    @Char
                                END
                        END
                END            
            SET @Position    =    @Position + 1
        END

    -- Return the result of the function
    RETURN    REPLACE(@Results,',','-')
END
GO
/****** Object:  UserDefinedFunction [dbo].[ConvertToBasicLatinAndSpace]    Script Date: 03/28/2014 10:00:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:        ThanhTN
-- Create date: <Create Date, ,>
-- Description:    Convert sang tiếng việt không dấu
-- =============================================
CREATE FUNCTION [dbo].[ConvertToBasicLatinAndSpace]
(
    -- Add the parameters for the function here
    @Value    NVARCHAR(1000)
)
RETURNS VARCHAR(1000)
AS
BEGIN
    -- Declare the return variable here
    DECLARE @Results        VARCHAR(1000)
    DECLARE @UniChars        NVARCHAR(140)
    DECLARE    @UnsignChars    NVARCHAR(140)
    DECLARE    @AllowChars        VARCHAR(38)
       DECLARE @Position        INT
       DECLARE    @Index            INT
       DECLARE    @Char            NCHAR(1)
       DECLARE    @NonChar        NCHAR(1)
       DECLARE    @PreviousChar    NCHAR(1)

    -- Add the T-SQL statements to compute the return value here
    SET    @Results        = ''
    SET @Position        = 1
    SET @UniChars        = N'àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ'
    SET @UnsignChars    = N'aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU'
    SET @AllowChars        = 'abcdefghijklmnopqrstxyzuvxw0123456789;'           
    SET    @PreviousChar    = ''
    WHILE @Position        <=    DATALENGTH(@Value) / 2
        BEGIN
            SET    @Char    = ''
            SET    @NonChar= ''
            SET    @Index    = 0
            SET    @Char    = LOWER(SUBSTRING(@Value, @Position, 1))
            IF    @Char    =    ' '
                BEGIN
                    IF    @PreviousChar    !=    ' '
                        BEGIN
                            SET    @Results    =    @Results + ' '
                        END
                    SET    @PreviousChar    =    @Char
                END
            ELSE
                BEGIN
                    SET    @Index    = CHARINDEX(@Char , @UniChars)
                    IF    @Index    > 0
                        BEGIN
                            SET    @NonChar        =    LOWER(SUBSTRING(@UnsignChars, @Index, 1))
                            SET    @Results        =    @Results + @NonChar
                            SET    @PreviousChar    =    @Char
                        END
                    ELSE
                        BEGIN
                            SET    @Index    =    0
                            SET    @Index    =    CHARINDEX(@Char , @AllowChars)
                            IF    @Index    >    0
                                BEGIN
                                    SET    @Results        =    @Results + @Char
                                    SET    @PreviousChar    =    @Char
                                END
                        END
                END           
            SET @Position    =    @Position + 1
        END

    -- Return the result of the function
    RETURN    @Results
END


GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetDefaultNewsPublishForNewsPosition]    Script Date: 03/28/2014 09:59:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-08>
-- Description:	<Get default list newspublish for newsposition>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetDefaultNewsPublishForNewsPosition]
(	
	@TopNews int,
	@ZoneId int,
	@TypeId int,
	@ExludeIds varchar(500)
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int,
	[TypeId] int,
	[Order] int,
	[NewsId] bigint 
)
AS
BEGIN
	DECLARE @IsFilterByZoneId tinyint
	SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), -1)
	
	-- Có type
	IF @IsFilterByZoneId > -1
	BEGIN
		IF @IsFilterByZoneId = 0 -- No filter
		BEGIN
			SET @ZoneId = 0
		
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId)
			SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId
			FROM
			(
				SELECT DISTINCT N.NewsId, N.DistributionDate
				FROM NewsPublish AS N INNER JOIN
				NewsInZone AS NZ ON N.NewsId = NZ.NewsId AND NZ.IsPrimary = 1
				WHERE IsOnHome = 1 AND PATINDEX('%,' + CONVERT(varchar(20), N.NewsId) + ',%', @ExludeIds) <= 0 AND DistributionDate <= GETDATE()
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		ELSE
		BEGIN
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId)
			SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId
			FROM
			(
				SELECT DISTINCT NewsId, DistributionDate
				FROM NewsPublish
				WHERE (ZoneId = @ZoneId AND PATINDEX('%,' + CONVERT(varchar(20), NewsId) + ',%', @ExludeIds) <= 0 ) AND DistributionDate <= GETDATE()
				--AND IsOnHome = 1 
				
				
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		
		DECLARE @Index int
		SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
		WHILE(@Index < @TopNews) 
		BEGIN
			SET @Index = @Index + 1
			
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId)
			VALUES (@ZoneId, @TypeId, @Index, 0)
		END
	END
		
	RETURN
END





GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetDefaultNewsPublishForNewsPositionZoneHauTruongTheThao]    Script Date: 03/28/2014 09:59:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-08>
-- Description:	<Get default list newspublish for newsposition>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetDefaultNewsPublishForNewsPositionZoneHauTruongTheThao]
(	
	@RootZoneId int,
	@TopNews int,
	@ZoneHauTruongId int,
	@TypeId int,
	@ExludeTypeIds varchar(50)
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int,
	[TypeId] int,
	[Order] int,
	[NewsId] bigint,
	DistributionDate datetime,
	Title nvarchar(250),
	SubTitle nvarchar(250),
	Sapo nvarchar(max), 
	Avatar nvarchar(500), 
	AvatarDesc nvarchar(500), 
	Avatar1 nvarchar(500), 
	Avatar2 nvarchar(500), 
	Avatar3 nvarchar(500), 
	Avatar4 nvarchar(500), 
	Avatar5 nvarchar(500), 
	Author nvarchar(500), 
	NewsRelation nvarchar(max), 
	Source nvarchar(100), 
	IsFocus bit, 
	ThreadId int, 
	Url nvarchar(max), 
	OriginalId int,
	InitSapo nvarchar(max)
)
AS
BEGIN
	INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], 
		NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
		Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo)
	SELECT TOP(@TopNews) @ZoneHauTruongId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], 
		NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
		Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
	FROM
	(
		SELECT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, 
			Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
		FROM NewsPublish
		WHERE ZoneId = @ZoneHauTruongId
			AND NewsId NOT IN (
									SELECT NewsId 
									FROM NewsPositionForZoneSport 
									WHERE ZoneId = @ZoneHauTruongId AND PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExludeTypeIds) > 0
								)
	) AS NewsData
	ORDER BY DistributionDate DESC
	
	DECLARE @Index int
	SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
	WHILE(@Index < @TopNews) 
	BEGIN
		SET @Index = @Index + 1
		
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
			Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
			NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo)
		VALUES (@ZoneHauTruongId, @TypeId, @Index, 0, null, null, null, null,
			null, null, null, null, null, null, null, null,
			null, null, 0, 0, null, 0, null)
	END
		
	RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetDefaultNewsPublishForNewsPositionZoneSport]    Script Date: 03/28/2014 09:59:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-08>
-- Description:	<Get default list newspublish for newsposition>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetDefaultNewsPublishForNewsPositionZoneSport]
(	
	@RootZoneId int,
	@TopNews int,
	@ZoneId int,
	@TypeId int,
	@ExludeTypeIds varchar(MAX),
	@IsOnHome int = -1
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int,
	[TypeId] int,
	[Order] int,
	[NewsId] bigint,
	DistributionDate datetime,
	Title nvarchar(250),
	SubTitle nvarchar(250),
	Sapo nvarchar(max), 
	Avatar nvarchar(500), 
	AvatarDesc nvarchar(500), 
	Avatar1 nvarchar(500), 
	Avatar2 nvarchar(500), 
	Avatar3 nvarchar(500), 
	Avatar4 nvarchar(500), 
	Avatar5 nvarchar(500), 
	Author nvarchar(500), 
	NewsRelation nvarchar(max), 
	Source nvarchar(100), 
	IsFocus bit, 
	ThreadId int, 
	Url nvarchar(max), 
	OriginalId int,
	InitSapo nvarchar(max)
)
AS
BEGIN
	IF @ZoneId = 0 -- No filter
	BEGIN
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], 
			NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
			Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo)
		SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], 
			NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
			Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
		FROM
		(
			SELECT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, 
				Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
			FROM NewsPublish
			WHERE IsPrimary = 1 
				AND ((@RootZoneId = 0) OR (@RootZoneId > 0 AND ZoneId IN (SELECT Id FROM Zone WHERE Id = @RootZoneId OR ParentId = @RootZoneId)))
				AND NewsId NOT IN (
										SELECT NewsId 
										FROM NewsPositionForZoneSport 
										WHERE PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExludeTypeIds) > 0
									)
				AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
		) AS NewsData
		ORDER BY DistributionDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], 
			NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
			Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo)
		SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], 
			NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, 
			Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
		FROM
		(
			SELECT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, 
				Source, IsFocus, ThreadId, Url, OriginalId, InitSapo
			FROM NewsPublish
			WHERE ZoneId = @ZoneId
				AND NewsId NOT IN (
										SELECT NewsId 
										FROM NewsPositionForZoneSport 
										WHERE ZoneId = @ZoneId AND PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExludeTypeIds) > 0
									)
				AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
		) AS NewsData
		ORDER BY DistributionDate DESC
	END
	
	DECLARE @Index int
	SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
	WHILE(@Index < @TopNews) 
	BEGIN
		SET @Index = @Index + 1
		
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
			Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
			NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo)
		VALUES (@ZoneId, @TypeId, @Index, 0, null, null, null, null,
			null, null, null, null, null, null, null, null,
			null, null, 0, 0, null, 0, null)
	END
		
	RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListOfZoneByUserIdWithMaxPermission]    Script Date: 03/28/2014 09:59:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-03-05>
-- Description:	<get list zone by username (use max permission)>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListOfZoneByUserIdWithMaxPermission]  
(	
	@UserId int,
	@MinPermissionId int,
	@ZoneForFilter varchar(1000)
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int
)
AS
BEGIN
	DECLARE @ZoneId int
	
	IF @ZoneForFilter <> '' AND LEFT(@ZoneForFilter, 1) <> ','
		SET @ZoneForFilter = ',' + @ZoneForFilter
	ELSE IF @ZoneForFilter <> '' AND RIGHT(@ZoneForFilter, 1) <> ','
		SET @ZoneForFilter = @ZoneForFilter + ','
		
	DECLARE ZoneCursor CURSOR FOR
	SELECT DISTINCT ZoneId
	FROM UserPermission
	WHERE (UserId = @UserId) AND (PermissionId >= @MinPermissionId AND PermissionId <= 3) 
		AND (@ZoneForFilter = '' 
			OR (@ZoneForFilter <> '' AND PATINDEX('%,' + CONVERT(varchar(10), ZoneId) + ',%', @ZoneForFilter) >= 0))

	OPEN ZoneCursor
	
	FETCH NEXT FROM ZoneCursor INTO @ZoneId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM @ReturnTable WHERE ZoneId = @ZoneId)
		BEGIN
			INSERT INTO @ReturnTable (ZoneId)
			SELECT Id 
			FROM Zone 
			WHERE Id = @ZoneId OR ParentId = @ZoneId
		END
			
		FETCH NEXT FROM ZoneCursor INTO @ZoneId 
	END   

	CLOSE ZoneCursor   
	DEALLOCATE ZoneCursor
	
	RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetNewsPublishForNewsPosition]    Script Date: 03/28/2014 09:59:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-03-27>
-- Description:	<Get list newspublish for newsposition>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetNewsPublishForNewsPosition]
(	
	@TopNews int,
	@ZoneId int,
	@TypeId int,
	@ExcludeTypeIds varchar(MAX),
	@IsOnHome int = -1,
	@IsIncludeChildZoneId int = 1,
	@ExcludeOrder varchar(MAX) = '',
	@DisplayInSlide int = -1,
	@IsBreakingNews int = -1,
	@DisplayPosition int = 0,
	@IsFocus int = -1
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int,
	[TypeId] int,
	[Order] int,
	[NewsId] bigint,
	DistributionDate datetime,
	Title nvarchar(250),
	SubTitle nvarchar(250),
	Sapo nvarchar(max), 
	Avatar nvarchar(500), 
	AvatarDesc nvarchar(500), 
	Avatar1 nvarchar(500), 
	Avatar2 nvarchar(500), 
	Avatar3 nvarchar(500), 
	Avatar4 nvarchar(500), 
	Avatar5 nvarchar(500), 
	Author nvarchar(500), 
	NewsRelation nvarchar(max), 
	Source nvarchar(100), 
	IsFocus bit, 
	ThreadId int, 
	Url nvarchar(max), 
	OriginalId int,
	InitSapo nvarchar(max),
	NewsType tinyint,
	Type tinyint,
	OriginalUrl nvarchar(500),
	IsPr bit,
	AdStore bit,
	AdStoreUrl nvarchar(500),
	DisplayStyle int,
	InterviewId int,
	RollingNewsId int
)
AS
BEGIN
	DECLARE @IsFilterByZoneId tinyint
	SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), -1)
	
	DECLARE @LockPositionCount int
	DECLARE @ExcludeNewsIdTable TABLE (NewsId bigint)
	DECLARE @ZoneIdIncludeChildId TABLE (ZoneId bigint)
	
	IF @ExcludeTypeIds <> ''
		SET @ExcludeTypeIds = ',' + @ExcludeTypeIds + ','
	
	IF @ExcludeOrder <> ''
		SET @ExcludeOrder = ',' + @ExcludeOrder + ','
	
	-- Có type
	IF @IsFilterByZoneId > -1
	BEGIN
		IF @IsFilterByZoneId = 0 -- No filter
		BEGIN
			SET @ZoneId = 0
			
			SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND AllowAutoUpdate = 0)
			SET @TopNews = @TopNews - @LockPositionCount
			
			INSERT INTO @ExcludeNewsIdTable (NewsId)
			SELECT NewsId 
			FROM NewsPosition 
			WHERE (PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0 AND 
					(@ExcludeOrder = '' OR (@ExcludeOrder <> '' AND PATINDEX('%,' + CONVERT(varchar(20), [Order]) + ',%', @ExcludeOrder) > 0)))
				OR (TypeId = @TypeId AND AllowAutoUpdate = 0)
		
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
				Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, Type, OriginalUrl, IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId)
			SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId, 
				Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, 
				Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, [Type], OriginalUrl, 
				IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId
			FROM
			(
				SELECT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
					Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, 
					InitSapo, NewsType, [Type], OriginalUrl, IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId
				FROM NewsPublish
				WHERE IsPrimary = 1 
					AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
					AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
					AND (@DisplayInSlide = -1 OR (@DisplayInSlide > -1 AND DisplayInSlide = @DisplayInSlide))
					AND (@IsBreakingNews = -1 OR (@IsBreakingNews > -1 AND IsBreakingNews = @IsBreakingNews))
					AND (@IsFocus = -1 OR (@IsFocus > -1 AND IsFocus = @IsFocus))
					AND (@DisplayPosition = 0 OR (@DisplayPosition > 0 AND DisplayPosition = @DisplayPosition))
					AND DistributionDate <= GETDATE()
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		ELSE
		BEGIN
			SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND ZoneId = @ZoneId AND AllowAutoUpdate = 0)
			SET @TopNews = @TopNews - @LockPositionCount
			
			INSERT INTO @ExcludeNewsIdTable (NewsId)
			SELECT NewsId 
			FROM NewsPosition 
			WHERE ZoneId = @ZoneId 
				AND ((PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0 AND 
					(@ExcludeOrder = '' OR (@ExcludeOrder <> '' AND PATINDEX('%,' + CONVERT(varchar(20), [Order]) + ',%', @ExcludeOrder) > 0)))
				OR (TypeId = @TypeId AND AllowAutoUpdate = 0))
					
			IF @IsIncludeChildZoneId = 1
				INSERT INTO @ZoneIdIncludeChildId (ZoneId)
				SELECT Id 
				FROM Zone 
				WHERE Id = @ZoneId OR ParentId = @ZoneId
			ELSE
				INSERT INTO @ZoneIdIncludeChildId (ZoneId)
				SELECT Id 
				FROM Zone 
				WHERE Id = @ZoneId
		
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
				Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, Type, OriginalUrl, IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId)
			SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId, 
				Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, 
				Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, [Type], OriginalUrl,
				IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId
			FROM
			(
				SELECT DISTINCT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, 
					Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, 
					OriginalId, InitSapo, NewsType, [Type], OriginalUrl, IsPr, AdStore, AdStoreUrl, DisplayStyle, InterviewId, RollingNewsId
				FROM NewsPublish
				WHERE ZoneId IN (SELECT ZoneId FROM @ZoneIdIncludeChildId)
					AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
					AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
					AND (@DisplayInSlide = -1 OR (@DisplayInSlide > -1 AND DisplayInSlide = @DisplayInSlide))
					AND (@IsBreakingNews = -1 OR (@IsBreakingNews > -1 AND IsBreakingNews = @IsBreakingNews))
					AND (@IsFocus = -1 OR (@IsFocus > -1 AND IsFocus = @IsFocus))
					AND (@DisplayPosition = 0 OR (@DisplayPosition > 0 AND DisplayPosition = @DisplayPosition))
					AND DistributionDate <= GETDATE()
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		
		DECLARE @Index int
		SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
		WHILE(@Index < @TopNews) 
		BEGIN
			SET @Index = @Index + 1
			
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
				Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
				NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, OriginalUrl)
			VALUES (@ZoneId, @TypeId, @Index, 0, null, null, null, null,
				null, null, null, null, null, null, null, null,
				null, null, 0, 0, null, 0, null, null)
		END
		
		DECLARE @LockPosition int
		DECLARE PositionCursor CURSOR FOR
		SELECT [Order]
		FROM NewsPosition
		WHERE (TypeId = @TypeId AND (@IsFilterByZoneId <> 1 OR (@IsFilterByZoneId = 1 AND ZoneId = @ZoneId)) AND AllowAutoUpdate = 0)
		ORDER BY [Order]
		
		OPEN PositionCursor
	
		FETCH NEXT FROM PositionCursor INTO @LockPosition
		WHILE @@FETCH_STATUS = 0   
		BEGIN
			UPDATE @ReturnTable
			SET [Order] = [Order] + 1
			WHERE [Order] >= @LockPosition
				
			FETCH NEXT FROM PositionCursor INTO @LockPosition 
		END   

		CLOSE PositionCursor   
		DEALLOCATE PositionCursor
	END
		
	RETURN
END



GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetNewsPublishForNewsPositionZoneSport]    Script Date: 03/28/2014 09:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-03-27>
-- Description:	<Get list newspublish for newsposition>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetNewsPublishForNewsPositionZoneSport]
(	
	@TopNews int,
	@ZoneId int,
	@TypeId int,
	@ExcludeTypeIds varchar(MAX),
	@IsOnHome int = -1,
	@IsIncludeChildZoneId int = 1,
	@ExcludeOrder varchar(MAX) = ''
)
RETURNS @ReturnTable TABLE 
(
	[ZoneId] int,
	[TypeId] int,
	[Order] int,
	[NewsId] bigint,
	DistributionDate datetime,
	Title nvarchar(250),
	SubTitle nvarchar(250),
	Sapo nvarchar(max), 
	Avatar nvarchar(500), 
	AvatarDesc nvarchar(500), 
	Avatar1 nvarchar(500), 
	Avatar2 nvarchar(500), 
	Avatar3 nvarchar(500), 
	Avatar4 nvarchar(500), 
	Avatar5 nvarchar(500), 
	Author nvarchar(500), 
	NewsRelation nvarchar(max), 
	Source nvarchar(100), 
	IsFocus bit, 
	ThreadId int, 
	Url nvarchar(max), 
	OriginalId int,
	InitSapo nvarchar(max),
	NewsType tinyint,
	Type tinyint,
	OriginalUrl nvarchar(500),
	IsPr bit,
	AdStore bit,
	AdStoreUrl nvarchar(500)
)
AS
BEGIN
	DECLARE @LockPositionCount int
	DECLARE @ZoneIdForZoneSport int
	DECLARE @ExcludeNewsIdTable TABLE (NewsId bigint)
	DECLARE @ZoneIdIncludeChildId TABLE (ZoneId bigint)
	
	SET @ZoneIdForZoneSport = 10002
	
	IF @ExcludeTypeIds <> ''
		SET @ExcludeTypeIds = ',' + @ExcludeTypeIds + ','
	
	IF @ExcludeOrder <> ''
		SET @ExcludeOrder = ',' + @ExcludeOrder + ','
	
	IF @ZoneId = 0 -- No filter
	BEGIN
		SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND AllowAutoUpdate = 0)
		SET @TopNews = @TopNews - @LockPositionCount
		
		INSERT INTO @ExcludeNewsIdTable (NewsId)
		SELECT NewsId 
		FROM NewsPosition 
		WHERE (PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0 AND 
				PATINDEX('%,' + CONVERT(varchar(20), [Order]) + ',%', @ExcludeOrder) > 0)
			OR (TypeId = @TypeId AND AllowAutoUpdate = 0)
			
		INSERT INTO @ZoneIdIncludeChildId (ZoneId)
		SELECT Id 
		FROM Zone 
		WHERE Id = @ZoneIdForZoneSport OR ParentId = @ZoneIdForZoneSport
	
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
			Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, Type, OriginalUrl, IsPr, AdStore, AdStoreUrl)
		SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId, 
			Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, 
			Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, [Type], OriginalUrl,
			IsPr, AdStore, AdStoreUrl
		FROM
		(
			SELECT DISTINCT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, 
				Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, 
				OriginalId, InitSapo, NewsType, [Type], OriginalUrl, IsPr, AdStore, AdStoreUrl
			FROM NewsPublish
			WHERE ZoneId IN (SELECT ZoneId FROM @ZoneIdIncludeChildId)
				AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
				AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
				AND DistributionDate <= GETDATE()
		) AS NewsData
		ORDER BY DistributionDate DESC
	END
	ELSE
	BEGIN
		SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND ZoneId = @ZoneId AND AllowAutoUpdate = 0)
		SET @TopNews = @TopNews - @LockPositionCount
		
		INSERT INTO @ExcludeNewsIdTable (NewsId)
		SELECT NewsId 
		FROM NewsPosition 
		WHERE ZoneId = @ZoneId 
			AND (PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0
				OR (TypeId = @TypeId AND AllowAutoUpdate = 0))
				
		IF @IsIncludeChildZoneId = 1
			INSERT INTO @ZoneIdIncludeChildId (ZoneId)
			SELECT Id 
			FROM Zone 
			WHERE Id = @ZoneId OR ParentId = @ZoneId
		ELSE
			INSERT INTO @ZoneIdIncludeChildId (ZoneId)
			SELECT Id 
			FROM Zone 
			WHERE Id = @ZoneId
	
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
			Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, Type, OriginalUrl, IsPr, AdStore, AdStoreUrl)
		SELECT TOP(@TopNews) @ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], NewsId, 
			Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, 
			Author, NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, NewsType, [Type], OriginalUrl,
			IsPr, AdStore, AdStoreUrl
		FROM
		(
			SELECT DISTINCT NewsId, Title, SubTitle, DistributionDate, Sapo, Avatar, AvatarDesc, Avatar1, 
				Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, ThreadId, Url, 
				OriginalId, InitSapo, NewsType, [Type], OriginalUrl, IsPr, AdStore, AdStoreUrl
			FROM NewsPublish
			WHERE ZoneId IN (SELECT ZoneId FROM @ZoneIdIncludeChildId)
				AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
				AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
				AND DistributionDate <= GETDATE()
		) AS NewsData
		ORDER BY DistributionDate DESC
	END
	
	DECLARE @Index int
	SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
	WHILE(@Index < @TopNews) 
	BEGIN
		SET @Index = @Index + 1
		
		INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
			Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
			NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, OriginalUrl)
		VALUES (@ZoneId, @TypeId, @Index, 0, null, null, null, null,
			null, null, null, null, null, null, null, null,
			null, null, 0, 0, null, 0, null, null)
	END
	
	DECLARE @LockPosition int
	DECLARE PositionCursor CURSOR FOR
	SELECT [Order]
	FROM NewsPosition
	WHERE (TypeId = @TypeId AND (@ZoneId = 0 OR (@ZoneId > 0 AND ZoneId = @ZoneId)) AND AllowAutoUpdate = 0)
	ORDER BY [Order]
	
	OPEN PositionCursor

	FETCH NEXT FROM PositionCursor INTO @LockPosition
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		UPDATE @ReturnTable
		SET [Order] = [Order] + 1
		WHERE [Order] >= @LockPosition
			
		FETCH NEXT FROM PositionCursor INTO @LockPosition 
	END   

	CLOSE PositionCursor   
	DEALLOCATE PositionCursor
		
	RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[CmsDL_fSplitString]    Script Date: 03/28/2014 09:59:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CmsDL_fSplitString] 
(
    -- Add the parameters for the function here
    @myString nvarchar(max),
    @deliminator varchar(10)
)
RETURNS 
@ReturnTable TABLE 
(
    -- Add the column definitions for the TABLE variable here
    [id] int IDENTITY(1,1) NOT NULL,
    [part] nvarchar(500) NULL
)
AS
BEGIN
        Declare @iSpaces int
        Declare @part nvarchar(500)

        --initialize spaces
        Select @iSpaces = charindex(@deliminator,@myString,0)
        While @iSpaces > 0

        Begin
            Select @part = substring(@myString,0,charindex(@deliminator,@myString,0))

            Insert Into @ReturnTable(part) Select @part
            --Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString) - charindex(' ',@myString,0))
            Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString))
            Select @iSpaces = charindex(@deliminator,@myString,0)
        end

        If len(@myString) > 0
            Insert Into @ReturnTable
            Select @myString

    RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 03/28/2014 09:59:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitString] 
(
    -- Add the parameters for the function here
    @myString nvarchar(max),
    @deliminator varchar(10)
)
RETURNS 
@ReturnTable TABLE 
(
    -- Add the column definitions for the TABLE variable here
    [id] int IDENTITY(1,1) NOT NULL,
    [part] nvarchar(500) NULL
)
AS
BEGIN
        Declare @iSpaces int
        Declare @part nvarchar(500)

        --initialize spaces
        Select @iSpaces = charindex(@deliminator,@myString,0)
        While @iSpaces > 0

        Begin
            Select @part = substring(@myString,0,charindex(@deliminator,@myString,0))

            Insert Into @ReturnTable(part) Select @part
            --Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString) - charindex(' ',@myString,0))
            Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString))
            Select @iSpaces = charindex(@deliminator,@myString,0)
        end

        If len(@myString) > 0
            Insert Into @ReturnTable
            Select @myString

    RETURN 
END

GO
/****** Object:  UserDefinedFunction [dbo].[SplitStrings]    Script Date: 03/28/2014 09:59:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitStrings]
(
@List VARCHAR(MAX)
)
RETURNS @Items TABLE
(
Item VARCHAR(255) NOT NULL
)
AS
BEGIN
DECLARE
@Item VARCHAR(MAX),
@Pos  INT;

WHILE DATALENGTH(@List) > 0
BEGIN
SET @Pos = CHARINDEX(',', @List);

IF @Pos = 0
SET @Pos = DATALENGTH(@List) + 1;

SET @Item = LTRIM(RTRIM(LEFT(@List, @Pos-1)));

IF NOT EXISTS (SELECT 1 FROM @Items WHERE Item = @Item)
INSERT @Items SELECT CONVERT(VARCHAR(255), @Item);

SET @List = SUBSTRING(@List, @Pos+1, 8000);
END
RETURN;
END
