﻿
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 03/28/2014 09:56:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [bigint] NULL,
	[SourceId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SourceName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DestinationId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DestinationName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[ActionText] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Message] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OwnerId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [int] NULL,
	[ActionTypeDetail] [int] NULL,
	[GroupTimeLine] [bigint] NULL,
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Album]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Album](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbImage] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModifiedBy] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PhotoAlbum_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL,
	[Status] [int] NULL,
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PhotoAlbum] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AlbumPublished]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlbumPublished](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AlbumId] [int] NOT NULL,
	[NewsId] [bigint] NOT NULL CONSTRAINT [DF_PhotoAlbumPublished_NewsId]  DEFAULT ((0)),
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_PhotoAlbumPublished_ZoneId]  DEFAULT ((0)),
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThumbImage] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NOT NULL CONSTRAINT [DF_PhotoAlbumPublished_DistributionDate]  DEFAULT (getdate()),
	[DistributedBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_AlbumPublished_Status]  DEFAULT ((0)),
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PhotoAlbumPublished] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AlbumTags]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlbumTags](
	[AlbumId] [bigint] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[TagMode] [tinyint] NOT NULL CONSTRAINT [DF_AlbumTags_TagMode]  DEFAULT ((0)),
	[TagProperty] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [int] NOT NULL CONSTRAINT [DF_AlbumTags_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_AlbumTags] PRIMARY KEY CLUSTERED 
(
	[AlbumId] ASC,
	[TagId] ASC,
	[TagMode] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Application]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AutoTag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AutoTag](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[IsProcessed] [bit] NOT NULL,
 CONSTRAINT [PK_AutoTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxBanner]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BoxBanner](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_BoxBanner_ZoneId]  DEFAULT ((0)),
	[Position] [int] NOT NULL CONSTRAINT [DF_BoxBanner_Position]  DEFAULT ((0)),
	[Priority] [int] NOT NULL CONSTRAINT [DF_BoxBanner_Priority]  DEFAULT ((0)),
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[DisplayStyle] [int] NULL DEFAULT ((0)),
	[CreatedDate] [datetime] NULL DEFAULT (getdate()),
	[Type] [int] NULL,
	[ObjectId] [bigint] NULL,
 CONSTRAINT [PK_BoxBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BoxBannerInZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxBannerInZone](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[BoxBannerId] [int] NULL,
	[ZoneId] [int] NULL,
 CONSTRAINT [PK_BoxBannerInZone] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxNews](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[News_ID] [bigint] NULL,
	[ThuTu] [int] NULL,
	[Type] [int] NULL,
	[Status] [int] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_BoxNews] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxNewsEmbed]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxNewsEmbed](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL,
	[NewsId] [bigint] NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_BoxNewsEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxStaticNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxStaticNews](
	[Id] [int] NOT NULL,
	[NewsId] [bigint] IDENTITY(1,1) NOT NULL,
	[Priority] [int] NULL,
	[ZoneId] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_BoxStaticNews_ZoneId]  DEFAULT ((0)),
 CONSTRAINT [PK_BoxStaticNews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxTagEmbed]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxTagEmbed](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[SortOrder] [int] NOT NULL CONSTRAINT [DF_BoxTagEmbed_ShortOrder]  DEFAULT ((0)),
	[Type] [int] NOT NULL CONSTRAINT [DF_BoxTagEmbed_Type]  DEFAULT ((1)),
	[Title] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_BoxTagEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxThreadEmbed]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxThreadEmbed](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL,
	[ThreadId] [bigint] NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_BoxThreadEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BoxVideoEmbed]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BoxVideoEmbed](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL,
	[VideoId] [bigint] NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_BoxVideoEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CmsDLModule]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CmsDLModule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModuleConfigSrc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModuleImage] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_CmsDLModule_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_CmsDLModule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CmsDLModuleConfiguration]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CmsDLModuleConfiguration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModuleInPageId] [int] NOT NULL,
	[ConfigName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ConfigValue] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_CmsDLModuleConfiguration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CmsDLModuleInPage]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CmsDLModuleInPage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NOT NULL,
	[ModuleId] [int] NOT NULL,
	[PlaceHolderName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SkinSrc] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_CmsDLModuleInPage_DisplayType]  DEFAULT ((0)),
	[Priority] [int] NOT NULL CONSTRAINT [DF_CmsDLModuleInPage_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_CmsDLModuleInPage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CmsDLModuleSkin]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CmsDLModuleSkin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModuleId] [int] NOT NULL,
	[SkinName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SkinSrc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_CmsDLModuleSkin_Status]  DEFAULT ((1)),
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_CmsDLModuleSkin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CmsDLPage]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CmsDLPage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PageSrc] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_CmsDLPage_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_CmsDLPage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommentPublished]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommentPublished](
	[Id] [bigint] NOT NULL,
	[ParentId] [bigint] NULL CONSTRAINT [DF_CommentPublished_ParentId]  DEFAULT ((0)),
	[ObjectId] [bigint] NOT NULL,
	[ObjectTitle] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ObjectUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ObjectType] [int] NULL,
	[Title] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Content] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Emotion] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SenderEmail] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SenderFullName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SenderAvatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SenderAddress] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SenderPhoneNumber] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReceivedDate] [datetime] NULL,
	[ReceivedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Liked] [int] NULL CONSTRAINT [DF_CommentPublished_Rate]  DEFAULT ((0)),
	[LastLikeDate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_CommentPublished] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Discussion]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Discussion](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TopicId] [bigint] NOT NULL,
	[ParentId] [bigint] NULL,
	[Detail] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[DeletedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DeletedDate] [datetime] NULL,
	[Status] [int] NULL,
	[Priority] [int] NULL,
	[Data] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastSendDiscussion] [datetime] NULL,
 CONSTRAINT [PK_Discussion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiscussionAttachment]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiscussionAttachment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DiscussionTopicId] [bigint] NULL,
	[DiscussionId] [bigint] NULL,
	[FileName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FileUrl] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Attach] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiscussionTag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiscussionTag](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DiscussionTopicId] [bigint] NULL,
	[DiscussionId] [bigint] NULL,
	[CreatedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagTo] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_DiscussionTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiscussionTopic]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiscussionTopic](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[ObjectId] [bigint] NOT NULL,
	[ObjectTitle] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ObjectData] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[ResolvedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ResovedDate] [datetime] NULL,
	[DeletedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DeletedDate] [datetime] NULL,
	[Status] [int] NULL,
	[ZoneId] [int] NOT NULL DEFAULT ((0)),
	[IsPrivate] [bit] NOT NULL DEFAULT ((0)),
	[LastSendDiscussion] [datetime] NULL,
	[TopicContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TopicPriority] [int] NULL,
	[IsFocus] [bit] NULL,
	[TopicNote] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_DiscussionTopic] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiscussionTopicFollow]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiscussionTopicFollow](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DiscussionTopicId] [bigint] NULL,
	[FollowBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_DiscussionTopicFollow] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Email]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Email](
	[Email] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GroupId] [int] NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[Address] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Birthday] [datetime] NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Email] PRIMARY KEY CLUSTERED 
(
	[Email] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmailGroup]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_EmailGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmailQueue]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[Email] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SendMailDate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_EmailQueue_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EmbedGroup]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmbedGroup](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[TemplateId] [tinyint] NULL,
	[SortOrder] [int] NULL,
	[IsVisible] [bit] NULL,
 CONSTRAINT [PK_EmbedGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExportToChannels]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExportToChannels](
	[NewsId] [bigint] NOT NULL,
	[InProcess] [bit] NOT NULL CONSTRAINT [DF_ExportToChannels_InProcess]  DEFAULT ((1)),
	[OriginalNewsId] [bigint] NOT NULL,
	[IsDeleted] [bit] NULL CONSTRAINT [DF_ExportToChannels_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_ExportToChannels_1] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FootballScore]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FootballScore](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortOrder] [int] NULL,
	[Data] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_FootballScore] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FootballScoreDetail]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FootballScoreDetail](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FootballScoreId] [int] NOT NULL,
	[StartTime] [datetime] NULL,
	[TeamA] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TeamB] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LogoTeamA] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LogoTeamB] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ScoreOfTeamA] [int] NULL,
	[ScoreOfTeamB] [int] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_FootballScoreDetail_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_FootballScoreDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FunnyNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FunnyNews](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MediaType] [int] NOT NULL,
	[MediaContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Tags] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NULL,
	[DistributionBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ViewCount] [int] NULL,
	[VoteCount] [int] NULL,
	[CommentCount] [int] NULL,
	[IsHot] [bit] NULL CONSTRAINT [DF_FunnyNews_IsHot]  DEFAULT ((0)),
	[NewsId] [bigint] NULL,
	[Status] [int] NULL,
	[ZoneId] [int] NULL,
	[Avatar] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDateInNumeric] [bigint] NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_FunnyNews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Ảnh, 1 - TVC Video, 2 - Video nhúng' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FunnyNews', @level2type=N'COLUMN', @level2name=N'MediaType'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bút danh' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FunnyNews', @level2type=N'COLUMN', @level2name=N'Author'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lượt xem' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FunnyNews', @level2type=N'COLUMN', @level2name=N'ViewCount'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lượt thích' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FunnyNews', @level2type=N'COLUMN', @level2name=N'VoteCount'

GO
/****** Object:  Table [dbo].[FunnyNewsTags]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FunnyNewsTags](
	[TagId] [bigint] NOT NULL,
	[FunnyNewsId] [bigint] NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_FunnyNewsTags] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[FunnyNewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FunnyNewsZones]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FunnyNewsZones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL,
 CONSTRAINT [PK_FunnyNewsZones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupNews](
	[Id] [int] NOT NULL,
	[GroupNewsName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[GroupNewsDescription] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TotalRow] [int] NULL,
	[IsFilterByZone] [bit] NULL,
 CONSTRAINT [PK_GroupNews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupNewsDetail]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupNewsDetail](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupNewsId] [int] NOT NULL,
	[ZoneId] [int] NULL CONSTRAINT [DF_GroupNewsDetail_ZoneId]  DEFAULT ((0)),
	[Order] [int] NULL CONSTRAINT [DF_GroupNewsDetail_Order]  DEFAULT ((0)),
	[NewsId] [bigint] NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_GroupNewsDetail_IsFocus]  DEFAULT ((0)),
	[NewsType] [tinyint] NULL,
	[DistributionDate] [datetime] NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL CONSTRAINT [DF_GroupNewsDetail_OriginalId]  DEFAULT ((0)),
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_GroupNewsDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupPermission]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupPermission](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_GroupPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Interview]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Interview](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignTitle] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartDate] [datetime] NULL,
	[IsFocus] [bit] NULL,
	[IsActived] [bit] NULL,
	[Status] [int] NULL CONSTRAINT [DF_Interview_Status]  DEFAULT ((1)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Interview_CreatedDate]  DEFAULT (getdate()),
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_Interview_LastModifiedDate]  DEFAULT (getdate()),
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Interview] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InterviewChannel]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InterviewChannel](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[InterviewId] [int] NULL,
	[ChannelRole] [int] NULL,
	[ChannelName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ChannelDescription] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_InterviewChannel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Chu toa, 2 - Editor,3-  Kiem duyet, 4 - Dieu phoi, 5- Truc kenh' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'InterviewChannel', @level2type=N'COLUMN', @level2name=N'ChannelRole'

GO
/****** Object:  Table [dbo].[InterviewChannelProcess]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InterviewChannelProcess](
	[InterviewChannelId] [int] NOT NULL,
	[ProcessInterviewChannelId] [int] NOT NULL,
 CONSTRAINT [PK_InterviewChannelProcess] PRIMARY KEY CLUSTERED 
(
	[InterviewChannelId] ASC,
	[ProcessInterviewChannelId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InterviewPublished]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InterviewPublished](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InterviewId] [int] NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StartDate] [datetime] NULL,
	[IsFocus] [bit] NULL,
	[IsActived] [bit] NULL,
	[AscQuestionContent] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DescQuestionContent] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_InterviewPublished] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InterviewQuestion]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InterviewQuestion](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Priority] [int] NULL CONSTRAINT [DF_InterviewQuestion_Priority]  DEFAULT ((0)),
	[InterviewId] [int] NULL,
	[Type] [int] NULL CONSTRAINT [DF_InterviewQuestion_Type]  DEFAULT ((0)),
	[Status] [int] NULL CONSTRAINT [DF_InterviewQuestion_Status]  DEFAULT ((0)),
	[Content] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Answer] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FullName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sex] [int] NULL CONSTRAINT [DF_InterviewQuestion_Sex]  DEFAULT ((-1)),
	[Age] [int] NULL,
	[Email] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Job] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mobile] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_InterviewQuestion_CreatedDate]  DEFAULT (getdate()),
	[ReceivedDate] [datetime] NULL,
	[ReceivedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributedDate] [datetime] NULL,
	[DistributedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedDate] [datetime] NULL,
	[EditedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL CONSTRAINT [DF_InterviewQuestion_LastModifiedDate]  DEFAULT (getdate()),
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ChannelId] [int] NULL,
 CONSTRAINT [PK_InterviewQuestion_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 - Câu hỏi mới 2-Câu hỏi đã phân phối 3- Câu hỏi đã trả lời chờ duyệt - 4 Câu hỏi đã duyệt' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'InterviewQuestion', @level2type=N'COLUMN', @level2name=N'Status'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Duoc phan phoi boi ai ?' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'InterviewQuestion', @level2type=N'COLUMN', @level2name=N'DistributedBy'

GO
/****** Object:  Table [dbo].[LiveMatch]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveMatch](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TeamA] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TeamB] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LogoTeamA] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LogoTeamB] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ScoreOfTeamA] [int] NULL CONSTRAINT [DF_LiveMatch_RatioOfTeamA]  DEFAULT ((0)),
	[ScoreOfTeamB] [int] NULL,
	[Stadium] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Referee] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BeginFirstHalf] [bit] NULL CONSTRAINT [DF_LiveMatch_BeginFirstHalf]  DEFAULT ((0)),
	[FinishFirstHalf] [bit] NULL CONSTRAINT [DF_LiveMatch_FinishFirstHalf]  DEFAULT ((0)),
	[BeginSecondHalf] [bit] NULL CONSTRAINT [DF_LiveMatch_BeginSecondHalf]  DEFAULT ((0)),
	[FinishSecondHalf] [bit] NULL CONSTRAINT [DF_LiveMatch_FinishSecondHalf]  DEFAULT ((0)),
	[BeginFirstExtraTime] [bit] NULL CONSTRAINT [DF_LiveMatch_BeginFirstExtraTime]  DEFAULT ((0)),
	[FinishFirstExtraTime] [bit] NULL CONSTRAINT [DF_LiveMatch_FinishFirstExtraTime]  DEFAULT ((0)),
	[BeginSecondExtraTime] [bit] NULL CONSTRAINT [DF_LiveMatch_BeginSecondExtraTime]  DEFAULT ((0)),
	[FinishSecondExtraTime] [bit] NULL CONSTRAINT [DF_LiveMatch_FinishSecondExtraTime]  DEFAULT ((0)),
	[IsPenalty] [bit] NULL,
	[MatchFinished] [bit] NULL CONSTRAINT [DF_LiveMatch_MatchFinished]  DEFAULT ((0)),
	[TimeOfMatchBegin] [datetime] NULL,
	[TimeOfFinishFirstHalf] [datetime] NULL,
	[TimeOfBeginSecondHalf] [datetime] NULL,
	[TimeOfFinishSecondHalf] [datetime] NULL,
	[TimeOfBeginFirstExtraTime] [datetime] NULL,
	[TimeOfFinishFirstExtraTime] [datetime] NULL,
	[TimeOfBeginSecondExtraTime] [datetime] NULL,
	[TimeOfFinishSecondExtraTime] [datetime] NULL,
	[TimeOfMatchFinished] [datetime] NULL,
	[Status] [int] NULL CONSTRAINT [DF_LiveMatch_Status]  DEFAULT ((1)),
	[LinkLiveTV1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LinkLiveTV2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LinkLiveTV3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LinkLiveTV4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LinkLiveTV5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShowOnLiveTV] [bit] NULL,
 CONSTRAINT [PK_LiveMatch] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiveMatchEvent]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveMatchEvent](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LiveMatchId] [int] NULL,
	[InMinute] [int] NULL CONSTRAINT [DF_LiveMatchEvent_InMinute]  DEFAULT ((0)),
	[EventType] [int] NULL CONSTRAINT [DF_LiveMatchEvent_EventType]  DEFAULT ((1)),
	[IsTeamA] [bit] NULL CONSTRAINT [DF_LiveMatchEvent_IsTeamA]  DEFAULT ((1)),
	[PlayerName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PlayerNameOut] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_LiveMatchEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiveMatchPenalty]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveMatchPenalty](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LiveMatchId] [int] NULL,
	[Turn] [int] NULL CONSTRAINT [DF_LiveMatchPenalty_Turn]  DEFAULT ((1)),
	[PlayerName] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsTeamA] [bit] NULL CONSTRAINT [DF_LiveMatchPenalty_IsTeamA]  DEFAULT ((1)),
	[IsGoal] [bit] NULL CONSTRAINT [DF_LiveMatchPenalty_IsGoal]  DEFAULT ((1)),
 CONSTRAINT [PK_LiveMatchPenalty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LiveMatchTimeline]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveMatchTimeline](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[LiveMatchId] [int] NULL,
	[InMinute] [int] NULL CONSTRAINT [DF_LiveMatchTimeline_InSecond]  DEFAULT ((0)),
	[EventType] [int] NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VideoKey] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VideoAvatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Images] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExtraTime] [int] NULL CONSTRAINT [DF_LiveMatchTimeline_ExtraTime]  DEFAULT ((0)),
	[Status] [int] NULL CONSTRAINT [DF_LiveMatchTimeline_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_LiveMatchTimeline] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[News]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[News](
	[Id] [bigint] NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Content_Status]  DEFAULT ((0)),
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_News_IsFocus]  DEFAULT ((0)),
	[Type] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF_News_ThreadID]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_News_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedDate] [datetime] NULL,
	[DistributionDate] [datetime] NULL,
	[CreatedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastReceiver] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WordCount] [int] NULL,
	[ViewCount] [int] NULL,
	[Priority] [tinyint] NULL,
	[Tag] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagPrimary] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] [decimal](18, 3) NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL CONSTRAINT [DF_News_OriginalId]  DEFAULT ((0)),
	[NewsType] [tinyint] NULL,
	[IsOnHome] [bit] NULL CONSTRAINT [DF_News_IsOnHome]  DEFAULT ((1)),
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteRoyalties] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagItem] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsCategory] [int] NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateConfig] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[IsBreakingNews] [bit] NULL DEFAULT ((0)),
	[IsPr] [bit] NULL DEFAULT ((0)),
	[AdStore] [bit] NULL DEFAULT ((0)),
	[AdStoreUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrBookingNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PegaBreakingNews] [int] NULL,
	[RollingNewsId] [int] NULL,
	[IsOnMobile] [bit] NULL,
	[TagSubTitleId] [int] NULL,
	[PrPosition] [int] NULL,
	[ViewCount2] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Luu tam, 2 = Cho duyet, 3 = Nhan Bien Tap, 4 = Phong Vien Gui, 5 = Tra lai, 6 = Xoa Tam, 7 = Go Bai, 8 = Xuat Ban ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'News', @level2type=N'COLUMN', @level2name=N'Status'

GO
/****** Object:  Table [dbo].[NewsAuthor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsAuthor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserData] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AuthorName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AuthorType] [tinyint] NOT NULL CONSTRAINT [DF_NewsAuthor_AuthorType]  DEFAULT ((0)),
	[OriginalName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsAuthor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nếu thuộc CMS trong thì UserData là UserName; Nếu thuộc CMS ngoài thì UserData là MingID' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsAuthor', @level2type=N'COLUMN', @level2name=N'UserData'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Tác giả thuộc CMS trong; 1:Tác giả thuộc CMS ngoài (dùng MingID)' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsAuthor', @level2type=N'COLUMN', @level2name=N'AuthorType'

GO
/****** Object:  Table [dbo].[NewsByAuthor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsByAuthor](
	[NewsId] [bigint] NOT NULL,
	[AuthorId] [int] NOT NULL,
	[AuthorName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Note] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsByAuthor] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[AuthorId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsBySource]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsBySource](
	[NewsId] [bigint] NOT NULL,
	[SourceId] [int] NOT NULL,
 CONSTRAINT [PK_NewsBySource] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[SourceId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsCachedMonitor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsCachedMonitor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ParentZoneId] [int] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_ParentZoneId]  DEFAULT ((0)),
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_ZoneId]  DEFAULT ((0)),
	[NewsId] [bigint] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_NewsId]  DEFAULT ((0)),
	[TagIds] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_NewsCacheMonitor_TagId]  DEFAULT (''),
	[ThreadIds] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_DistributedDate]  DEFAULT (getdate()),
	[InsertedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_InsertedDate]  DEFAULT (getdate()),
	[NewsProcessedDate] [datetime] NULL,
	[TagProcessedDate] [datetime] NULL,
	[ThreadProcessedDate] [datetime] NULL,
	[IsProcessedNews] [bit] NOT NULL CONSTRAINT [DF_NewsCacheMonitor_IsProcessed]  DEFAULT ((0)),
	[IsProcessedTag] [bit] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_IsProcessedTag]  DEFAULT ((0)),
	[IsProcessedThread] [bit] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_IsProcessedThread]  DEFAULT ((0)),
	[PositionTypeForHome] [int] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_PositionType]  DEFAULT ((0)),
	[PositionOrderForHome] [int] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_PositionOrder]  DEFAULT ((0)),
	[PositionTypeForList] [int] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_PositionTypeForList]  DEFAULT ((0)),
	[PositionOrderForList] [int] NOT NULL CONSTRAINT [DF_NewsCachedMonitor_PositionOrderForList]  DEFAULT ((0)),
	[IsChangeDistributionDate] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_NewsCacheMonitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsChild]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsChild](
	[NewsParentId] [bigint] NOT NULL,
	[Order] [int] NOT NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsChild] PRIMARY KEY CLUSTERED 
(
	[NewsParentId] ASC,
	[Order] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsChildPublish]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsChildPublish](
	[NewsParentId] [bigint] NOT NULL,
	[Order] [int] NOT NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsChildPublish] PRIMARY KEY CLUSTERED 
(
	[NewsParentId] ASC,
	[Order] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsChildVersion]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsChildVersion](
	[NewsVersionId] [bigint] NOT NULL,
	[Order] [int] NOT NULL,
	[NewsParentId] [bigint] NOT NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsChildVersion_1] PRIMARY KEY CLUSTERED 
(
	[NewsVersionId] ASC,
	[Order] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsConfig]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsConfig](
	[ConfigName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ConfigValue] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfigGroup] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfigGroupKey] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfigLabel] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ConfigValueType] [int] NULL,
	[ConfigInitValue] [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigName] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsConfigGroup]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsConfigGroup](
	[ConfigGroupKey] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ConfigGroupName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsConfigGroup] PRIMARY KEY CLUSTERED 
(
	[ConfigGroupKey] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsContent]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsContent](
	[NewsId] [bigint] NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PublishedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsContent_PublishedDate]  DEFAULT (getdate()),
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagPrimary] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ZoneId] [int] NULL,
	[OriginalId] [bigint] NULL CONSTRAINT [DF_NewsContent_OriginalId]  DEFAULT ((0)),
	[TagItem] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [tinyint] NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[ThreadId] [int] NULL DEFAULT ((0)),
	[RollingNewsId] [int] NULL,
	[AdStore] [bit] NULL,
	[AdStoreUrl] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagSubTitleId] [int] NULL,
	[Position] [int] NULL,
	[PrPosition] [int] NULL,
 CONSTRAINT [PK_NewsContent] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsExtension]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsExtension](
	[NewsId] [bigint] NOT NULL,
	[Type] [int] NOT NULL,
	[Value] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_NewsExtension] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[Type] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsHistory]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[ActionName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsHistory_CreatedDate]  DEFAULT (getdate()),
	[SentBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ReceivedBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[Description] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsInZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsInZone](
	[NewsId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_NewsInZone_IsPrimary]  DEFAULT ((0)),
 CONSTRAINT [PK_NewsInZone] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[ZoneId] ASC,
	[IsPrimary] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsLabel]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsLabel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[LabelName] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LabelColor] [varchar](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsSystemLabel] [bit] NOT NULL CONSTRAINT [DF_NewsLabel_IsSystemLabel]  DEFAULT ((0)),
	[Priority] [int] NOT NULL CONSTRAINT [DF_NewsLabel_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_NewsLabel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsletterMonitor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsletterMonitor](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_NewsletterMonitor_ZoneId]  DEFAULT ((0)),
	[ThreadId] [int] NOT NULL CONSTRAINT [DF_NewsletterMonitor_ThreadId]  DEFAULT ((0)),
	[DistributionDate] [datetime] NOT NULL,
	[InsertedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsletterMonitor_InsertedDate]  DEFAULT (getdate()),
	[ProcessedDate] [datetime] NULL,
	[IsProcessed] [bit] NOT NULL CONSTRAINT [DF_NewsletterMonitor_IsProcessed]  DEFAULT ((0)),
 CONSTRAINT [PK_NewsletterMonitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsLive]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsLive](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[NewsId] [bigint] NOT NULL CONSTRAINT [DF_NewsLive_NewsId]  DEFAULT ((0)),
	[Title] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Content] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_NewsLive_CreatedDate]  DEFAULT (getdate()),
	[PublishedDate] [datetime] NULL,
	[CreatedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [tinyint] NULL CONSTRAINT [DF_NewsLive_Status]  DEFAULT ((0)),
	[Priority] [int] NULL,
	[PublishedDateNumber] [bigint] NULL,
	[Avatar] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[EventTime] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsLive] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsLiveAuthor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsLiveAuthor](
	[NewsId] [bigint] NOT NULL,
	[AuthorId] [int] NOT NULL,
	[AuthorName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsLiveAuthor] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[AuthorId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsLiveSettings]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsLiveSettings](
	[NewsId] [bigint] NOT NULL,
	[IsFinished] [bit] NULL CONSTRAINT [DF_NewsLiveSettings_IsFinished]  DEFAULT ((0)),
	[ShowTime] [bit] NULL,
	[ShowAuthor] [bit] NULL,
 CONSTRAINT [PK_NewsLiveSettings] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsMessage]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsMessage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NewsID] [bigint] NOT NULL,
	[Message] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_NewsMessage_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_NewsMessage] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsNotification]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsNotification](
	[NewsId] [bigint] NOT NULL,
	[UserName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NewsStatus] [int] NOT NULL CONSTRAINT [DF_NewsNofication_NewsStatus]  DEFAULT ((1)),
	[LabelId] [int] NOT NULL,
	[IsRead] [bit] NOT NULL CONSTRAINT [DF_NewsNofication_IsRead]  DEFAULT ((0)),
	[LastNotifiedDate] [datetime] NOT NULL CONSTRAINT [DF_NewsNofication_LastNotifiedDate]  DEFAULT (getdate()),
	[IsWarning] [bit] NOT NULL,
	[Message] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsNofication] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[UserName] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsPosition]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPosition](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL CONSTRAINT [DF_NewsPosition_ZoneId]  DEFAULT ((0)),
	[TypeId] [int] NULL,
	[Order] [int] NULL CONSTRAINT [DF_NewsPosition_Order]  DEFAULT ((0)),
	[Lockable] [bit] NULL CONSTRAINT [DF_NewsPosition_Lockable]  DEFAULT ((0)),
	[Locked] [bit] NULL CONSTRAINT [DF_NewsPosition_Locked]  DEFAULT ((0)),
	[ExpiredLock] [datetime] NULL,
	[NewsIdForBomd] [bigint] NULL CONSTRAINT [DF_NewsPosition_OldNewsId]  DEFAULT ((0)),
	[NewsId] [bigint] NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_NewsPosition_IsFocus]  DEFAULT ((0)),
	[NewsType] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF_NewsPosition_ThreadId]  DEFAULT ((0)),
	[DistributionDate] [datetime] NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AllowAutoUpdate] [bit] NOT NULL CONSTRAINT [DF_NewsPosition_AllowAutoUpdate]  DEFAULT ((0)),
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL DEFAULT ((0)),
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[IsBreakingNews] [bit] NULL DEFAULT ((0)),
	[IsPr] [bit] NULL DEFAULT ((0)),
	[AdStore] [bit] NULL DEFAULT ((0)),
	[AdStoreUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RollingNewsId] [int] NULL,
	[LastModifiedDate] [datetime] NULL DEFAULT (getdate()),
	[TagSubTitleId] [int] NULL,
	[ZoneIdForNews] [int] NULL DEFAULT ((0)),
	[Position] [int] NULL,
	[PrPosition] [int] NULL,
 CONSTRAINT [PK_NewsPosition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:8 bài nổi bật trang chủ; 2:Bài nổi bật chuyên mục trang chủ; 3:Bài nổi bật trang chủ; 4:Bài trang đầu tiên của trang chuyên mục;' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsPosition', @level2type=N'COLUMN', @level2name=N'TypeId'

GO
/****** Object:  Table [dbo].[NewsPositionForMobile]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPositionForMobile](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL,
	[TypeId] [int] NULL,
	[Order] [int] NULL,
	[Lockable] [bit] NULL,
	[Locked] [bit] NULL,
	[ExpiredLock] [datetime] NULL,
	[NewsIdForBomd] [bigint] NULL,
	[NewsId] [bigint] NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL,
	[NewsType] [tinyint] NULL,
	[ThreadId] [int] NULL,
	[DistributionDate] [datetime] NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AllowAutoUpdate] [bit] NOT NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsPositionForZoneSport]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPositionForZoneSport](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL CONSTRAINT [DF_NewsPositionForZoneSport_ZoneId]  DEFAULT ((0)),
	[TypeId] [int] NULL,
	[Order] [int] NULL CONSTRAINT [DF_NewsPositionForZoneSport_Order]  DEFAULT ((0)),
	[Lockable] [bit] NULL CONSTRAINT [DF_NewsPositionForZoneSport_Lockable]  DEFAULT ((0)),
	[Locked] [bit] NULL CONSTRAINT [DF_NewsPositionForZoneSport_Locked]  DEFAULT ((0)),
	[ExpiredLock] [datetime] NULL,
	[NewsIdForBomd] [bigint] NULL CONSTRAINT [DF_NewsPositionForZoneSport_NewsIdForBomd]  DEFAULT ((0)),
	[NewsId] [bigint] NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_NewsPositionForZoneSport_IsFocus]  DEFAULT ((0)),
	[NewsType] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF_NewsPositionForZoneSport_ThreadId]  DEFAULT ((0)),
	[DistributionDate] [datetime] NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AllowAutoUpdate] [bit] NOT NULL CONSTRAINT [DF_NewsPositionForZoneSport_AllowAutoUpdate]  DEFAULT ((0)),
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL CONSTRAINT [DF_NewsPositionForZoneSport_OriginalId]  DEFAULT ((0)),
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[IsPr] [bit] NULL DEFAULT ((0)),
	[AdStore] [bit] NULL DEFAULT ((0)),
	[AdStoreUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RollingNewsId] [int] NULL,
	[LastModifiedDate] [datetime] NULL DEFAULT (getdate()),
 CONSTRAINT [PK_NewsPositionForZoneSport] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:8 bài nổi bật trang chủ; 2:Bài nổi bật chuyên mục trang chủ; 3:Bài nổi bật trang chủ; 4:Bài trang đầu tiên của trang chuyên mục;' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsPositionForZoneSport', @level2type=N'COLUMN', @level2name=N'TypeId'

GO
/****** Object:  Table [dbo].[NewsPositionType]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsPositionType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PositionTypeName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FilterByZoneId] [tinyint] NOT NULL,
	[AllowAutoUpdate] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_PositionType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:No filter by zoneid;1:filter by extract zone;3:filter  by parnet zone only' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsPositionType', @level2type=N'COLUMN', @level2name=N'FilterByZoneId'

GO
/****** Object:  Table [dbo].[NewsPR]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsPR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ContentID] [bigint] NULL,
	[DistributionDate] [datetime] NULL,
	[DistributionID] [bigint] NULL,
	[ExpireDate] [datetime] NULL,
	[IsFocus] [bit] NULL,
	[Mode] [tinyint] NULL,
	[Position] [int] NULL CONSTRAINT [DF_NewsPR_Position]  DEFAULT ((1)),
	[NewsID] [bigint] NULL,
	[PublishDate] [datetime] NULL,
	[AdStore] [bit] NULL,
 CONSTRAINT [PK_NewsPR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsPrConfig]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsPrConfig](
	[Mode] [int] NOT NULL,
	[PositionTypeId] [int] NOT NULL,
	[PositionOrder] [int] NOT NULL,
 CONSTRAINT [PK_NewsPrConfig] PRIMARY KEY CLUSTERED 
(
	[Mode] ASC,
	[PositionTypeId] ASC,
	[PositionOrder] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsPublish]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsPublish](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar1] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF__NewsPubli__IsFoc__245D67DE]  DEFAULT ((0)),
	[Type] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF__NewsPubli__Threa__25518C17]  DEFAULT ((0)),
	[DistributionDate] [datetime] NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL CONSTRAINT [DF_NewsPublish_OriginalId]  DEFAULT ((0)),
	[PublishedDate] [bigint] NULL CONSTRAINT [DF_NewsPublish_PublishedDate]  DEFAULT ((0)),
	[IsOnHome] [bit] NULL CONSTRAINT [DF_NewsPublish_IsOnHome]  DEFAULT ((1)),
	[TagItem] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsPrimary] [bit] NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL DEFAULT (getdate()),
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[IsBreakingNews] [bit] NULL DEFAULT ((0)),
	[IsPr] [bit] NULL DEFAULT ((0)),
	[AdStore] [bit] NULL DEFAULT ((0)),
	[AdStoreUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsType] [int] NULL,
	[RollingNewsId] [int] NULL,
	[IsOnMobile] [bit] NULL,
	[TagSubTitleId] [int] NULL,
	[Position] [int] NULL,
	[PrPosition] [int] NULL,
 CONSTRAINT [PK_PublishContent] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nguồn gốc của bài. Ex: Từ TTVN -> OriginalId = 1' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsPublish', @level2type=N'COLUMN', @level2name=N'OriginalId'

GO
/****** Object:  Table [dbo].[NewsRelation]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsRelation](
	[NewsId] [bigint] NOT NULL,
	[NewsRelationId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsChange] [bit] NULL CONSTRAINT [DF_NewsRelation_IsChange]  DEFAULT ((0)),
	[Priority] [int] NOT NULL CONSTRAINT [DF_NewsRelation_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_NewsRelation] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[NewsRelationId] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsSource]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsSource](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SourceType] [int] NULL,
	[Avatar] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsSource] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsTemp]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsTemp](
	[News_ID] [bigint] NOT NULL,
	[Cat_ID] [int] NULL,
	[News_Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Subtitle] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Image] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_ImageNote] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Source] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_InitContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Content] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Athor] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Approver] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Status] [int] NULL,
	[News_PublishDate] [datetime] NULL,
	[News_isFocus] [bit] NULL,
	[News_Mode] [int] NULL,
	[News_Relation] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_Rate] [int] NULL,
	[News_ModifedDate] [datetime] NULL,
	[News_OtherCat] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isComment] [bit] NULL,
	[isUserRate] [bit] NULL,
	[Template] [int] NULL,
	[WordCount] [int] NOT NULL,
	[Icon] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension1] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension2] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension3] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Extension4] [int] NULL,
	[News_IsRootShare] [bit] NULL,
	[News_ShareID] [bigint] NULL,
	[Thread_ID] [int] NULL,
	[Site_ID] [int] NULL,
	[News_Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[News_TagLink] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsVideo] [int] NULL,
	[Royalties] [decimal](18, 0) NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT (''),
	[NewsType] [int] NULL DEFAULT ((1)),
	[ZoneUrl] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT (''),
	[RowNumber] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_NewsTemp] PRIMARY KEY CLUSTERED 
(
	[News_ID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsUsePhoto]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsUsePhoto](
	[NewsId] [bigint] NOT NULL,
	[PhotoPublishedId] [bigint] NOT NULL,
	[Priority] [int] NOT NULL,
 CONSTRAINT [PK_NewsUsePhoto] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[PhotoPublishedId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NewsUseVideo]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsUseVideo](
	[NewsId] [bigint] NOT NULL,
	[VideoId] [bigint] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HtmlCode] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KeyVideo] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pname] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NULL,
	[Views] [int] NULL CONSTRAINT [DF_NewsUseVideo_Views]  DEFAULT ((0)),
	[Mode] [int] NULL CONSTRAINT [DF_NewsUseVideo_Mode]  DEFAULT ((0)),
	[Url] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FileName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Duration] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] [int] NULL,
	[AllowAd] [bit] NULL,
	[Priority] [int] NULL CONSTRAINT [DF_NewsUseVideo_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_NewsUseVideo] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC,
	[VideoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Thong thuong, 1: Noi bat' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsUseVideo', @level2type=N'COLUMN', @level2name=N'Mode'

GO
/****** Object:  Table [dbo].[NewsVersion]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsVersion](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Version] [int] NULL,
	[CreatedDateVersion] [datetime] NULL,
	[IsEditing] [bit] NULL CONSTRAINT [DF_NewsVersion_IsEditing]  DEFAULT ((1)),
	[CreateVersionBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsId] [bigint] NOT NULL CONSTRAINT [DF_NewsVersion_NewsId]  DEFAULT ((0)),
	[ListZoneId] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL CONSTRAINT [DF_NewsVersion_Status]  DEFAULT ((0)),
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_NewsVersion_IsFocus]  DEFAULT ((0)),
	[Type] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF_NewsVersion_ThreadId]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_NewsVersion_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedDate] [datetime] NULL,
	[DistributionDate] [datetime] NULL,
	[CreatedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastReceiver] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WordCount] [int] NULL,
	[ViewCount] [int] NULL,
	[Priority] [tinyint] NULL,
	[Tag] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsOnHome] [bit] NULL CONSTRAINT [DF_NewsVersion_IsOnHome]  DEFAULT ((0)),
	[OriginalId] [int] NULL,
	[Price] [decimal](18, 3) NULL,
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteRoyalties] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagItem] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsCategory] [int] NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_NewsVersion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Luu tam, 2 = Cho duyet, 3 = Nhan Bien Tap, 4 = Phong Vien Gui, 5 = Tra lai, 6 = Xoa Tam, 7 = Go Bai, 8 = Xuat Ban ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsVersion', @level2type=N'COLUMN', @level2name=N'Status'

GO
/****** Object:  Table [dbo].[NewsWaitRepublish]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewsWaitRepublish](
	[Id] [bigint] NOT NULL,
	[Title] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SubTitle] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Body] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AvatarDesc] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar2] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar3] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar4] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar5] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Author] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsRelation] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_NewsWaitRepublish_Status]  DEFAULT ((0)),
	[Source] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsFocus] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_IsFocus]  DEFAULT ((0)),
	[Type] [tinyint] NULL,
	[ThreadId] [int] NULL CONSTRAINT [DF_NewsWaitRepublish_ThreadId]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_NewsWaitRepublish_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedDate] [datetime] NULL,
	[DistributionDate] [datetime] NULL,
	[CreatedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastReceiver] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[WordCount] [int] NULL,
	[ViewCount] [int] NULL,
	[Priority] [tinyint] NULL,
	[Tag] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagPrimary] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Price] [decimal](18, 3) NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalId] [int] NULL CONSTRAINT [DF_NewsWaitRepublish_OriginalId]  DEFAULT ((0)),
	[NewsType] [tinyint] NULL,
	[IsOnHome] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_IsOnHome]  DEFAULT ((1)),
	[Url] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NoteRoyalties] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagItem] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NewsCategory] [int] NULL,
	[InitSapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateConfig] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InterviewId] [int] NULL,
	[IsBreakingNews] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_IsBreakingNews]  DEFAULT ((0)),
	[IsPr] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_IsPr]  DEFAULT ((0)),
	[AdStore] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_AdStore]  DEFAULT ((0)),
	[AdStoreUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PrBookingNumber] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PegaBreakingNews] [int] NULL,
	[IsOnMobile] [bit] NULL CONSTRAINT [DF_NewsWaitRepublish_IsOnMobile]  DEFAULT ((1)),
 CONSTRAINT [PK_NewsWaitRepublish] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 = Luu tam, 2 = Cho duyet, 3 = Nhan Bien Tap, 4 = Phong Vien Gui, 5 = Tra lai, 6 = Xoa Tam, 7 = Go Bai, 8 = Xuat Ban ' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'NewsWaitRepublish', @level2type=N'COLUMN', @level2name=N'Status'

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ApplicationId] [int] NULL,
	[SourceId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SourceName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DestinationId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DestinationName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SentDate] [datetime] NULL CONSTRAINT [DF_Notification_SentDate]  DEFAULT (getdate()),
	[ActionText] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Message] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsRead] [bit] NULL CONSTRAINT [DF_Notification_Enabled]  DEFAULT ((0)),
	[OwnerId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [int] NULL,
	[GroupTimeLine] [bigint] NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[Name] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsGrantByCategory] [bit] NOT NULL CONSTRAINT [DF_Permission_IsGrantByCategory]  DEFAULT ((0)),
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionTemplate]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionTemplate](
	[Id] [int] NOT NULL,
	[TemplateName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_PermissionTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PermissionTemplateDetail]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PermissionTemplateDetail](
	[TemplateId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_PermissionTemplateDetail] PRIMARY KEY CLUSTERED 
(
	[TemplateId] ASC,
	[PermissionId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Photo]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Photo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_Photo_ZoneId]  DEFAULT ((0)),
	[Name] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ImageNote] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] [float] NULL,
	[Note] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Photo_CreatedDate]  DEFAULT (getdate()),
	[ModifiedBy] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnSignName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_Photo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhotoInLabel]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoInLabel](
	[PhotoLabelId] [int] NOT NULL,
	[PhotoId] [bigint] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
 CONSTRAINT [PK_PhotoInLabel] PRIMARY KEY CLUSTERED 
(
	[PhotoLabelId] ASC,
	[PhotoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhotoLabel]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoLabel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NOT NULL CONSTRAINT [DF_PhotoLabel_ParentId]  DEFAULT ((0)),
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_PhotoLabel_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_PhotoLabel_ModifiedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_PhotoLabel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhotoPublished]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhotoPublished](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[PhotoId] [bigint] NOT NULL,
	[AlbumId] [int] NOT NULL CONSTRAINT [DF_PhotoPublished_AlbumId_1]  DEFAULT ((0)),
	[NewsId] [bigint] NOT NULL CONSTRAINT [DF_PhotoPublished_NewsId]  DEFAULT ((0)),
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_PhotoPublished_ZoneId]  DEFAULT ((0)),
	[Name] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ImageNote] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NULL,
	[DistributedBy] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_PhotoPublished_Status]  DEFAULT ((0)),
	[Priority] [int] NOT NULL CONSTRAINT [DF_PhotoPublished_Priority]  DEFAULT ((0)),
	[Size] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PhotoPublished] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PhotoTags]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoTags](
	[PhotoId] [bigint] NOT NULL,
	[TagId] [bigint] NOT NULL,
	[TagMode] [tinyint] NOT NULL CONSTRAINT [DF_PhotoTags_TagMode]  DEFAULT ((0)),
	[TagProperty] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [int] NOT NULL CONSTRAINT [DF_PhotoTags_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_PhotoTags] PRIMARY KEY CLUSTERED 
(
	[PhotoId] ASC,
	[TagId] ASC,
	[TagMode] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlayList]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL CONSTRAINT [DF_PlayList_ZoneId]  DEFAULT ((0)),
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UnsignName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL,
	[Mode] [int] NOT NULL,
	[Avatar] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastInsertVideoDate] [datetime] NULL,
	[VideoCount] [int] NULL,
	[Url] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_PlayList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProgramChannel]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramChannel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[Priority] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_ProgramChannel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramSchedule]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramSchedule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramChannelId] [int] NOT NULL,
	[ScheduleName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ScheduleAvatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PlayListId] [int] NULL,
	[ScheduleDate] [smalldatetime] NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_ProgramSchedule_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_ProgramSchedule_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_ProgramSchedule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramScheduleDetail]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramScheduleDetail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramScheduleId] [int] NOT NULL,
	[ScheduleTime] [datetime] NOT NULL,
	[Title] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VideoId] [int] NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_ProgramScheduleDetail_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_ProgramScheduleDetail_Status]  DEFAULT ((1)),
	[ShowOnSchedule] [bit] NULL CONSTRAINT [DF_ProgramScheduleDetail_ShowOnSchedule]  DEFAULT ((0)),
 CONSTRAINT [PK_ProgramScheduleDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuickNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuickNews](
	[QuickNewsId] [bigint] NULL,
	[CatId] [int] NULL,
	[VietId] [bigint] NULL,
	[News_ID] [bigint] NULL,
	[Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Content] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Note] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Link] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FullName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Type] [int] NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[PenName] [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuickNewsAuthor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QuickNewsAuthor](
	[VietId] [bigint] NULL,
	[UserName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FullName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
	[Job] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Slogan] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PenName] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuickNewsImages]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuickNewsImages](
	[QuickNewsImagesId] [bigint] NULL,
	[QuickNewsId] [bigint] NULL,
	[Image] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsAvatar] [bit] NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Image - 1:video' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'QuickNewsImages', @level2type=N'COLUMN', @level2name=N'Type'

GO
/****** Object:  Table [dbo].[RollingNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RollingNews](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShowTime] [bit] NOT NULL CONSTRAINT [DF_RollingNews_ShowTime]  DEFAULT ((1)),
	[ShowAuthor] [bit] NOT NULL CONSTRAINT [DF_RollingNews_ShowAuthor]  DEFAULT ((0)),
	[PublishedContent] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StartDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[Status] [int] NOT NULL,
	[IsShowRollingNewsLabel] [bit] NULL,
 CONSTRAINT [PK_RollingNews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Not start; 1:Started; 2:Finished' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'RollingNews', @level2type=N'COLUMN', @level2name=N'Status'

GO
/****** Object:  Table [dbo].[RollingNewsAuthor]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RollingNewsAuthor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RollingNewsId] [int] NOT NULL,
	[AuthorRole] [int] NOT NULL CONSTRAINT [DF_RollingNewsAuthor_AuthorRole]  DEFAULT ((1)),
	[AuthorName] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Username] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_RollingNewsAuthor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Reporter; 2:Publisher' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'RollingNewsAuthor', @level2type=N'COLUMN', @level2name=N'AuthorRole'

GO
/****** Object:  Table [dbo].[RollingNewsEvent]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RollingNewsEvent](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[RollingNewsId] [int] NOT NULL,
	[EventType] [int] NOT NULL CONSTRAINT [DF_RollingNewsEvent_EventType]  DEFAULT ((0)),
	[EventContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EventNote] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EventTime] [datetime] NOT NULL CONSTRAINT [DF_RollingNewsEvent_EventTime]  DEFAULT (getdate()),
	[IsFocus] [bit] NOT NULL CONSTRAINT [DF_RollingNewsEvent_IsFocus]  DEFAULT ((0)),
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[PublishedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_RollingNewsEvent_Status]  DEFAULT ((1)),
	[MobileContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ShortContent] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Images] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageCount] [int] NULL,
 CONSTRAINT [PK_RollingNewsEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:BreakingNews; 1:Video; 2:Photo; 3:Quotation; 4:Facebook' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'RollingNewsEvent', @level2type=N'COLUMN', @level2name=N'EventType'

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:WaitForPublished; 1:Published; 2:Unpublished' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'RollingNewsEvent', @level2type=N'COLUMN', @level2name=N'Status'

GO
/****** Object:  Table [dbo].[Royalties]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Royalties](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[VideoId] [int] NULL,
	[PhotoId] [bigint] NULL,
	[RoyaltiesMemberId] [int] NULL,
	[MemberAlias] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RoyaltiesCategoryId] [int] NULL,
	[RoyaltiesRate] [int] NULL CONSTRAINT [DF_Royalties_RoyaltiesRate]  DEFAULT ((0)),
	[RoyaltiesValue] [int] NULL CONSTRAINT [DF_Royalties_RoyaltiesValue]  DEFAULT ((0)),
	[RoyaltiesBonus] [int] NULL CONSTRAINT [DF_Royalties_RoyaltiesBonus]  DEFAULT ((0)),
	[RoyaltiesPenalty] [int] NULL CONSTRAINT [DF_Royalties_RoyaltiesPenalty]  DEFAULT ((0)),
	[RoyaltiesTotalValue] [int] NULL CONSTRAINT [DF_Royalties_RoyaltiesTotalValue]  DEFAULT ((0)),
	[CountPhotoClassA] [int] NULL CONSTRAINT [DF_Royalties_CountPhotoClassA]  DEFAULT ((0)),
	[CountPhotoClassB] [int] NULL CONSTRAINT [DF_Royalties_CountPhotoClassB]  DEFAULT ((0)),
	[PhotoRoyaltiesValue] [int] NULL CONSTRAINT [DF_Royalties_PhotoRoyaltiesValue]  DEFAULT ((0)),
	[CountClipClassA] [int] NULL CONSTRAINT [DF_Royalties_CountClipClassA]  DEFAULT ((0)),
	[CountClipClassB] [int] NULL CONSTRAINT [DF_Royalties_CountClipClassB]  DEFAULT ((0)),
	[ClipRoyaltiesValue] [int] NULL CONSTRAINT [DF_Royalties_ClipRoyaltiesValue]  DEFAULT ((0)),
	[SelfProduceClipRoyaltiesValue] [int] NULL CONSTRAINT [DF_Royalties_SelfProduceClipRoyaltiesValue]  DEFAULT ((0)),
	[Note] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL CONSTRAINT [DF_Royalties_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Royalties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoyaltiesCategory]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoyaltiesCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NOT NULL,
	[Name] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Type] [int] NOT NULL CONSTRAINT [DF_RoyaltiesCategory_Type]  DEFAULT ((0)),
 CONSTRAINT [PK_RoyaltiesCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoyaltiesHistory]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoyaltiesHistory](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RoyaltiesId] [bigint] NULL,
	[RoyaltiesCategoryId] [int] NOT NULL,
	[RoyaltiesRate] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_RoyaltiesRate]  DEFAULT ((0)),
	[RoyaltiesValue] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_RoyaltiesValue]  DEFAULT ((0)),
	[RoyaltiesBonus] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_RoyaltiesBonus]  DEFAULT ((0)),
	[RoyaltiesPenalty] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_RoyaltiesPenalty]  DEFAULT ((0)),
	[RoyaltiesTotalValue] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_RoyaltiesTotalValue]  DEFAULT ((0)),
	[CountPhotoClassA] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_CountPhotoClassA]  DEFAULT ((0)),
	[CountPhotoClassB] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_CountPhotoClassB]  DEFAULT ((0)),
	[PhotoRoyaltiesValue] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_PhotoRoyaltiesValue]  DEFAULT ((0)),
	[CountClipClassA] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_CountClipClassA]  DEFAULT ((0)),
	[CountClipClassB] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_CountClipClassB]  DEFAULT ((0)),
	[ClipRoyaltiesValue] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_ClipRoyaltiesValue]  DEFAULT ((0)),
	[SelfProduceClipRoyaltiesValue] [int] NOT NULL CONSTRAINT [DF_RoyaltiesHistory_SelfProduceClipRoyaltiesValue]  DEFAULT ((0)),
	[Note] [nvarchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[LastModifiedBy] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_RoyaltiesHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoyaltiesMember]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoyaltiesMember](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoyaltiesRoleId] [int] NOT NULL,
	[FullName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[MemberAlias] [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Username] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmplyeeCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TaxCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BankAccount] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Phone] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_RoyaltiesMember] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoyaltiesRole]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoyaltiesRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsManager] [bit] NOT NULL CONSTRAINT [DF_RoyaltiesRole_IsManager]  DEFAULT ((0)),
 CONSTRAINT [PK_RoyaltiesRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoyaltiesRule]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoyaltiesRule](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoyaltiesCategoryId] [int] NOT NULL,
	[IsManager] [bit] NOT NULL CONSTRAINT [DF_RoyaltiesRule_IsManager]  DEFAULT ((0)),
	[RoyaltiesRate] [int] NOT NULL,
	[RoyaltiesValue] [int] NOT NULL,
 CONSTRAINT [PK_RoyaltiesRule] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SEOMetaNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SEOMetaNews](
	[NewsId] [bigint] NOT NULL,
	[MetaTitle] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaKeyword] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaDescription] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaNewsKeyword] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KeywordFocus] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_SEOMetaNews] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SEOMetaTags]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEOMetaTags](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaKeyword] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaDescription] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ZoneId] [int] NULL CONSTRAINT [DF_MetaTags_ZoneId]  DEFAULT ((0)),
 CONSTRAINT [PK_MetaTags] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SEOTagZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SEOTagZone](
	[TagId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NULL CONSTRAINT [DF__SEOTagZon__IsPri__0559BDD1]  DEFAULT ((0)),
 CONSTRAINT [PK_SEOTagZone] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tag](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ParentId] [bigint] NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Invisibled] [bit] NOT NULL,
	[IsHotTag] [bit] NOT NULL,
	[Type] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsThread] [bit] NOT NULL CONSTRAINT [DF_Tag_IsThread]  DEFAULT ((0)),
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [bigint] NOT NULL CONSTRAINT [DF_Tag_Priority]  DEFAULT ((0)),
	[TagContent] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagTitle] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagInit] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagMetaKeyword] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TagMetaContent] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateId] [tinyint] NULL,
 CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TagNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagNews](
	[TagID] [int] NOT NULL,
	[NewsID] [bigint] NOT NULL,
	[TagMode] [tinyint] NOT NULL CONSTRAINT [DF_TagNews_TagMode]  DEFAULT ((0)),
	[TagProperty] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [int] NOT NULL CONSTRAINT [DF_TagNews_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_TagNews] PRIMARY KEY CLUSTERED 
(
	[TagID] ASC,
	[NewsID] ASC,
	[TagMode] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TagZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TagZone](
	[TagId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL CONSTRAINT [DF_TagZone_IsPrimary]  DEFAULT ((0)),
 CONSTRAINT [PK_TagZone] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Thread]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Thread](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Title] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HomeAvatar] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SpecialAvatar] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsHot] [bit] NOT NULL,
	[IsOnHome] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaKeyword] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MetaContent] [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TemplateId] [tinyint] NULL,
	[Invisibled] [bit] NULL,
 CONSTRAINT [PK_Thread] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThreadInZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThreadInZone](
	[ZoneId] [int] NOT NULL,
	[ThreadId] [int] NOT NULL,
	[IsPrimary] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_ThreadInZone] PRIMARY KEY CLUSTERED 
(
	[ZoneId] ASC,
	[ThreadId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThreadNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThreadNews](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ThreadId] [int] NOT NULL,
	[NewsId] [bigint] NOT NULL,
 CONSTRAINT [PK_ThreadNews] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThreadRelation]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThreadRelation](
	[ThreadId] [int] NOT NULL,
	[ThreadRelationId] [int] NOT NULL,
 CONSTRAINT [PK_ThreadRelation] PRIMARY KEY CLUSTERED 
(
	[ThreadId] ASC,
	[ThreadRelationId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FullName] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__User__FullName__7D78A4E7]  DEFAULT (''),
	[Avatar] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Email] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__User__Email__7E6CC920]  DEFAULT (''),
	[Mobile] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__User__Tel__7F60ED59]  DEFAULT (''),
	[IsFullPermission] [bit] NOT NULL CONSTRAINT [DF_User_IsFullPermission]  DEFAULT ((0)),
	[IsFullZone] [bit] NOT NULL CONSTRAINT [DF_User_IsFullZone]  DEFAULT ((0)),
	[Status] [tinyint] NOT NULL CONSTRAINT [DF__User__Status__00551192]  DEFAULT ((0)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF__User__CreatedDat__014935CB]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL,
	[LastLogined] [datetime] NULL,
	[LastChangePass] [datetime] NULL,
	[IsSystem] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK__User__7C8480AE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserApplication]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserApplication](
	[ApplicationId] [int] NOT NULL,
	[AccountId] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ApplicationItemId] [bigint] NOT NULL,
	[AllowNotification] [bit] NULL CONSTRAINT [DF_UserApplication_AllowNotification]  DEFAULT ((1)),
	[AllowLogActivity] [bit] NULL CONSTRAINT [DF_UserApplication_AllowLogActivity]  DEFAULT ((1)),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_UserApplication_CreatedDate]  DEFAULT (getdate()),
 CONSTRAINT [PK_UserApplication] PRIMARY KEY CLUSTERED 
(
	[ApplicationId] ASC,
	[AccountId] ASC,
	[ApplicationItemId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[UserId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[ZoneId] [int] NOT NULL CONSTRAINT [DF_UserPermission_ZoneId]  DEFAULT ((0)),
 CONSTRAINT [PK_User_Permission] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[PermissionId] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserId] [int] NOT NULL,
	[Address] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Birthday] [datetime] NULL,
	[Description] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_UserProfile_1] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserSms]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserSms](
	[UserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_UserSms_CreatedDate]  DEFAULT (getdate()),
	[SmsCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ExpiredDate] [datetime] NOT NULL,
	[InUsing] [bit] NOT NULL CONSTRAINT [DF_UserSms_InUsing]  DEFAULT ((1)),
 CONSTRAINT [PK_UserSms] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[CreatedDate] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[HtmlCode] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[KeyVideo] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Pname] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_TVC_Video_Status]  DEFAULT ((0)),
	[NewsId] [bigint] NULL,
	[DistributionDate] [datetime] NULL,
	[Views] [int] NULL CONSTRAINT [DF_TVC_Video_Views]  DEFAULT ((0)),
	[Mode] [int] NULL CONSTRAINT [DF_TVC_Video_Mode]  DEFAULT ((0)),
	[Tags] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedDate] [datetime] NULL,
	[PublishDate] [datetime] NULL,
	[Url] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VideoRelation] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FileName] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Duration] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Size] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Capacity] [int] NULL,
	[AllowAd] [bit] NULL,
	[EditedBy] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PublishBy] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsRemoveLogo] [bit] NULL DEFAULT ((0)),
	[OriginalUrl] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT (''),
 CONSTRAINT [PK_TVC_Video] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Thong thuong, 1: Noi bat' ,@level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Video', @level2type=N'COLUMN', @level2name=N'Mode'

GO
/****** Object:  Table [dbo].[VideoFromYoutube]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoFromYoutube](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SourceVideoUrl] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FilePath] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
	[VideoId] [int] NULL DEFAULT ((0)),
 CONSTRAINT [PK_VideoFromYoutube] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoInNews]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoInNews](
	[VideoId] [int] NOT NULL,
	[NewsId] [bigint] NOT NULL,
 CONSTRAINT [PK_VideoInNews] PRIMARY KEY CLUSTERED 
(
	[VideoId] ASC,
	[NewsId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoInTag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoInTag](
	[VideoTagId] [int] NOT NULL,
	[VideoId] [int] NOT NULL,
	[TagMode] [int] NOT NULL CONSTRAINT [DF_VideoInTag_TagMode]  DEFAULT ((0)),
	[Priority] [int] NULL CONSTRAINT [DF_VideoInTag_Priority]  DEFAULT ((0)),
 CONSTRAINT [PK_VideoInTag] PRIMARY KEY CLUSTERED 
(
	[VideoTagId] ASC,
	[VideoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoInVideoThread]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoInVideoThread](
	[VideoThreadId] [int] NOT NULL,
	[VideoId] [int] NOT NULL,
 CONSTRAINT [PK_VideoInVideoThread] PRIMARY KEY CLUSTERED 
(
	[VideoThreadId] ASC,
	[VideoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoInZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoInZone](
	[VideoId] [int] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NULL CONSTRAINT [DF_VideoInZone_IsPrimaryZoneId]  DEFAULT ((0)),
 CONSTRAINT [PK_VideoInZone] PRIMARY KEY CLUSTERED 
(
	[VideoId] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoPlayList]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoPlayList](
	[PlaylistId] [int] NOT NULL,
	[VideoId] [int] NOT NULL,
	[Priority] [int] NOT NULL CONSTRAINT [DF_VideoPlayList_Order]  DEFAULT ((0)),
	[PlayOnTime] [datetime] NULL,
 CONSTRAINT [PK_VideoPlayList] PRIMARY KEY CLUSTERED 
(
	[PlaylistId] ASC,
	[VideoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoTag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoTag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsHotTag] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_VideoTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoTags]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VideoTags](
	[TagId] [bigint] NOT NULL,
	[VideoId] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_VideoTags_1] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[VideoId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VideoThread]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoThread](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsHotThread] [bit] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EditedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DistributionDate] [datetime] NULL,
	[UnsignName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Avatar] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_VideoThread_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_VideoThread] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vote]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vote](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Avatar] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Sapo] [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Vote_CreatedDate]  DEFAULT (getdate()),
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Vote_ModifiedDate]  DEFAULT (getdate()),
	[Status] [int] NULL,
	[Author] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreatedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastModifiedBy] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Priority] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayStyle] [int] NULL,
	[ViewCount] [int] NULL,
	[RateCount] [int] NULL,
	[MaxAnswers] [int] NULL,
	[StartedDate] [datetime] NULL,
	[EndedDate] [datetime] NULL,
	[ShowInZone] [bit] NULL DEFAULT ((0)),
	[ShowInFooter] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_Vote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VoteAnswers]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteAnswers](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[VoteId] [int] NULL,
	[Value] [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VoteRate] [float] NULL,
	[Status] [int] NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_VoteItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VoteInZone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VoteInZone](
	[VoteID] [int] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NULL,
 CONSTRAINT [PK_Vote_Assign] PRIMARY KEY CLUSTERED 
(
	[VoteID] ASC,
	[ZoneId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Zone]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zone](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ModifiedDate] [datetime] NULL CONSTRAINT [DF_Zone_ModifiedDate]  DEFAULT (getdate()),
	[CreatedDate] [datetime] NULL CONSTRAINT [DF_Zone_CreatedDate]  DEFAULT (getdate()),
	[ShortURL] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortOrder] [int] NULL,
	[ParentId] [int] NULL,
	[Invisibled] [bit] NOT NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Zone_Status]  DEFAULT ((0)),
	[AllowComment] [bit] NULL DEFAULT ((1)),
	[UseForFunnyNews] [bit] NULL CONSTRAINT [DF_Zone_UseForFunnyNews]  DEFAULT ((0)),
 CONSTRAINT [PK_Zone_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZoneInEmbedGroup]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZoneInEmbedGroup](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupId] [int] NULL,
	[ZoneId] [int] NULL,
	[SortOrder] [int] NULL,
 CONSTRAINT [PK_EmbedGroupZones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ZoneVideo]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZoneVideo](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Url] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Order] [int] NULL,
	[ParentId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CatId] [int] NULL,
 CONSTRAINT [PK_ZoneVideo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ZoneVideoInVideoTag]    Script Date: 03/28/2014 09:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZoneVideoInVideoTag](
	[ZoneVideoId] [int] NOT NULL,
	[VideoTagId] [int] NOT NULL,
	[Priority] [int] NOT NULL,
 CONSTRAINT [PK_ZoneVideoInVideoTag] PRIMARY KEY CLUSTERED 
(
	[ZoneVideoId] ASC,
	[VideoTagId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO

GO

GO

GO

GO

GO

GO
ALTER TABLE [dbo].[CmsDLModuleInPage]  WITH CHECK ADD  CONSTRAINT [FK_CmsDLModuleInPage_CmsDLModule] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[CmsDLModule] ([Id])
GO
ALTER TABLE [dbo].[CmsDLModuleInPage]  WITH CHECK ADD  CONSTRAINT [FK_CmsDLModuleInPage_CmsDLPage] FOREIGN KEY([PageId])
REFERENCES [dbo].[CmsDLPage] ([Id])
GO
ALTER TABLE [dbo].[CmsDLModuleSkin]  WITH CHECK ADD  CONSTRAINT [FK_CmsDLModuleSkin_CmsDLModule] FOREIGN KEY([ModuleId])
REFERENCES [dbo].[CmsDLModule] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NewsInZone]  WITH CHECK ADD  CONSTRAINT [FK_NewsInZone_Zone] FOREIGN KEY([ZoneId])
REFERENCES [dbo].[Zone] ([Id])
GO
ALTER TABLE [dbo].[NewsPublish]  WITH CHECK ADD  CONSTRAINT [FK_NewsPublish_Zone] FOREIGN KEY([ZoneId])
REFERENCES [dbo].[Zone] ([Id])
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_GroupPermission] FOREIGN KEY([GroupId])
REFERENCES [dbo].[GroupPermission] ([Id])
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([Id])
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])