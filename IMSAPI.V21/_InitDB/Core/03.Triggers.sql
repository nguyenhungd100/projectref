
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2014-03-25>
-- Description:	<Sync view to NewsAnalytic>
-- =============================================
CREATE TRIGGER [CMS_tSyncViewToNewsAnalytic]
   ON  [dbo].[News]
   AFTER UPDATE
AS 
BEGIN
	IF UPDATE(ViewCount)
	BEGIN
		DECLARE @View int,
				@NewsId bigint

		SELECT @NewsId = I.Id, @View = I.ViewCount
		FROM DELETED AS D INNER JOIN INSERTED AS I ON I.ViewCount <> D.ViewCount

		IF @NewsId IS NOT NULL AND @View IS NOT NULL AND @NewsId > 0 AND @View > 0
		BEGIN
			IF EXISTS(SELECT 1 FROM NewsAnalytic WHERE NewsId = @NewsId)
				UPDATE NewsAnalytic 
				SET ViewCount = @View,
					LastUpdateViewCount = GETDATE()
				WHERE NewsId = @NewsId
			ELSE
				INSERT INTO NewsAnalytic (NewsId, ViewCount, LastUpdateViewCount)
				VALUES (@NewsId, @View, GETDATE())
		END
	END
END




GO
/****** Object:  Trigger [TriggerAutoTag_Delete]    Script Date: 03/28/2014 09:58:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-03-11>
-- Description:	<Trigger DELETE for AutoTag>
-- =============================================
CREATE TRIGGER [TriggerAutoTag_Delete]
   ON  [dbo].[News]
   AFTER DELETE
AS 
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRY
		DELETE FROM AutoTag WHERE NewsId IN (SELECT Id FROM DELETED)
	END TRY
	BEGIN CATCH
		-- Do nothing
	END CATCH

END





