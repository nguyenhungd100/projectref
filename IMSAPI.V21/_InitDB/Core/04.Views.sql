﻿CREATE VIEW [dbo].[NewsNotificationWithLabel]
AS
SELECT     dbo.NewsNotification.NewsId, dbo.NewsNotification.UserName, dbo.NewsNotification.NewsStatus, dbo.NewsNotification.LabelId, dbo.NewsNotification.IsRead, 
                      dbo.NewsNotification.LastNotifiedDate, dbo.NewsNotification.IsWarning, dbo.NewsLabel.LabelName, dbo.NewsLabel.LabelColor, dbo.NewsLabel.Priority, dbo.NewsNotification.Message
FROM         dbo.NewsLabel RIGHT OUTER JOIN
                      dbo.NewsNotification ON dbo.NewsLabel.Id = dbo.NewsNotification.LabelId

GO