SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Quote_Insert]
@Quote nvarchar(MAX),
@QuoteTitle nvarchar(300),
@Avatar nvarchar(500),
@Position int,
@Id INT = 0 OUT 
AS
BEGIN

 INSERT INTO Quote(
	 Quote,
	 QuoteTitle,
	 Avatar,
	 Position
 )
 VALUES(
	  @Quote,
	  @QuoteTitle,
	  @Avatar,
	  @Position
 )
 
 SET @Id = SCOPE_IDENTITY();

END