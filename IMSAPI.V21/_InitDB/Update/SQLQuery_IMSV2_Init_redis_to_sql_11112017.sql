//init lai tu redis to sql

CREATE PROCEDURE [dbo].[CMS_News_GetNewsPublishByDate]	--'2017-11-06 11:59','2017-11-06 12:59'
	@DateFrom datetime = null,
	@DateTo datetime = null	
AS
BEGIN

SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel, n.ApprovedBy,
n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate
FROM News n 
where n.status=8 and ((@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo))))
order by n.CreatedDate desc

END

go

CREATE PROCEDURE [dbo].[CMS_News_UpdateNewsByPublishBy]	--2017110710430799,'',''
	@Id bigint,
	@PublishedBy varchar(200),
	@ApprovedBy varchar(200)
AS
BEGIN



update news set PublishedBy=@PublishedBy,ApprovedBy=@ApprovedBy
where Id=@Id

END