USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToReturnedToEditor]    Script Date: 12/15/2017 13:36:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-09-28>
-- Description:	<change news status to ReturnedToEditor>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToReturnedToEditor]
	@Id bigint,
	@Receiver varchar(255),
	@UserName varchar(255),
	@Note nvarchar(2000) = ''
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @Receiver <> ''
		BEGIN
			UPDATE News
			SET [Status] = 7,
				EditedBy = @Receiver,
				ReturnedBy = @UserName,
				LastModifiedDate = GETDATE(),
				Note = CASE @Note WHEN '' THEN Note ELSE @Note END
			WHERE Id = @Id
		END
		ELSE
		BEGIN
			UPDATE News
			SET [Status] = 7, 
				ReturnedBy = @UserName,
				LastModifiedDate = GETDATE(),
				Note = CASE @Note WHEN '' THEN Note ELSE @Note END
			WHERE Id = @Id
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END










