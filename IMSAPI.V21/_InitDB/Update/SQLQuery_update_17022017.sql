--update sp thiếu 22/03/2017
GO
CREATE PROCEDURE [dbo].[CMS_ThreadNews_GetByNewsId]
	@NewsId bigint
AS
BEGIN
Select * from Thread
inner join ThreadNews on NewsId=Id
where NewsId=@NewsId
END

GO
CREATE PROCEDURE [dbo].[CMS_BrandContent_GetByNewId]
	@NewsId bigint
AS
BEGIN
Select * from BrandContent
inner join BrandInNews on BrandInNews.BrandId=BrandContent.BrandId
where BrandInNews.NewsId=@NewsId
END

Go
---27/03/2017
ALTER PROCEDURE [dbo].[CMS_UserPermission_GetListByUserId] --449,0
	@UserId int,
	@IsGetChildZone bit
AS
BEGIN
	DECLARE @IsFullPermission bit,
			@IsFullZone bit
			
	SELECT @IsFullPermission = IsFullPermission, @IsFullZone = IsFullZone FROM [User] WHERE Id = @UserId
	
	IF @IsFullPermission = 1 AND @IsFullZone = 1
		SELECT @UserId as UserId, P.Id AS PermissionId, Z.Id AS ZoneId
		FROM Permission AS P, Zone AS Z
		WHERE (@IsGetChildZone = 1 OR (@IsGetChildZone = 0 AND Z.ParentId = 0))
	ELSE
	BEGIN
		IF @IsFullPermission = 1
		BEGIN
			SELECT UserId, Id AS PermissionId, ZoneId 
			FROM Permission,
			(
				SELECT UP.UserId, UP.ZoneId
				FROM UserPermission AS UP
				WHERE (UP.UserId = @UserId)
				UNION
				SELECT UP.UserId, Z.Id
				FROM UserPermission AS UP
					INNER JOIN Zone AS Z ON UP.ZoneId = Z.ParentId
				WHERE (UP.UserId = @UserId) AND (@IsGetChildZone = 1)
			) AS DATA
		END
		ELSE IF @IsFullZone = 1
		BEGIN
			SELECT UP.UserId, UP.PermissionId, Z.Id AS ZoneId
			FROM UserPermission AS UP, Zone AS Z
			WHERE (UP.UserId = @UserId)
				AND (@IsGetChildZone = 1 OR (@IsGetChildZone = 0 AND Z.ParentId = 0))
		END
		ELSE
		BEGIN
			SELECT UP.UserId, UP.PermissionId, UP.ZoneId
			FROM UserPermission AS UP
			WHERE (UP.UserId = @UserId)
			UNION
			SELECT UP.UserId, UP.PermissionId, Z.Id
			FROM UserPermission AS UP
				INNER JOIN Zone AS Z ON UP.ZoneId = Z.ParentId
			WHERE (UP.UserId = @UserId) AND (@IsGetChildZone = 1)
		END
	END
END