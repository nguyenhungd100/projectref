
--Update sql video 11/04/2017
ALTER TABLE Video
ADD AvatarShareFacebook varchar(255),Author NVARCHAR(300),OriginalId INT;

go

CREATE PROCEDURE  [dbo].[VideoCms_Video_InsertV3]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100) = '',
	@Pname varchar(100) = '',
	@Status int = 0,
	@FileName varchar(500),
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@CreatedBy varchar(255),
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@Source nvarchar(200) = '',
	@TagIdList varchar(300) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	@Duration varchar(50),
	@Size varchar(20),
	@Capacity int,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
    @Author NVARCHAR(300) = '' ,
    @OriginalId INT = 0 ,
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION
	
	--SET NOCOUNT ON;
--	IF @ZoneIdList <> ''
--		SET @ZoneIdList = dbo.CMS_fGetListOfZoneVideoIdIncludeParentZoneVideoId(@ZoneId, @ZoneIdList + ';' + CONVERT(varchar(10), @ZoneId))

    IF (EXISTS (SELECT Id FROM Video WHERE	KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0))
	BEGIN
		SELECT @Id = Id FROM Video WHERE KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0
		--RAISERROR ('This video has existed in database. Try again or create a other video.', 16, 1);
		--ROLLBACK TRANSACTION
	END ELSE
    BEGIN
		DECLARE @ParentPrimaryZoneId int
		SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
		IF @ParentPrimaryZoneId > 0
		BEGIN
			IF @ZoneIdList <> ''
				SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
			ELSE
				SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
		END
		ELSE
		BEGIN
			DECLARE @ChildZoneId int
			SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
			IF @ChildZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
				SET @ZoneId = @ChildZoneId
			END
		END

		INSERT	INTO Video(	ZoneId,
							Name,
							UnsignName,
							Description,
							HtmlCode,
							Avatar,
							KeyVideo,
							Pname,
							Status,
							FileName,
							NewsId,
							Views,
							Mode,
							Tags,
							CreatedBy,
							LastModifiedBy,
							Url,
							[Source],
							DistributionDate,
							CreatedDate,
							LastModifiedDate,
							VideoRelation,
							Duration,
							Size,
							Capacity,
							AllowAd,
							IsRemoveLogo,
							AvatarShareFacebook,
							Author,
							OriginalId)
		VALUES	(		@ZoneId,
						@Name,
						@UnsignName,
						@Description,
						@HtmlCode,
						@Avatar,
						@KeyVideo,
						@Pname,
						@Status,
						@FileName,
						@NewsId,
						@Views,
						@Mode,
						@Tags,
						@CreatedBy,
						@CreatedBy,
						@Url,
						@Source,
						@DistributionDate,
						GETDATE(),
						GETDATE(),
						@VideoRelationIdList,
						@Duration,
						@Size,
						@Capacity,
						@AllowAd,
						@IsRemoveLogo,
						@AvatarShareFacebook,
						@Author,
						@OriginalId
					)
						
		SET @Id = SCOPE_IDENTITY();
		
		UPDATE Video SET Url = REPLACE(Url, '{VideoId}', @Id) WHERE Id = @Id
		
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert video error. Try again or create a other video.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN
			-- Process Tags
			DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
			DECLARE @Index int,
					@TagId int;

				
			IF  @TagIdList <> ''
			BEGIN
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
					
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- Process Playlist
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					
					IF @PlaylistId IS NOT NULL AND @PlayListId > 0
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @Index)
						
					SET @Index = @Index + 1
				END
			END
		END		
    END
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
GO
CREATE PROCEDURE [dbo].[VideoCms_Video_UpdateV4]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100),
	@Pname varchar(100) = '',
	@Status int = 0,
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@LastModifiedBy varchar(255) = '',
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@Source nvarchar(200) ='',
	@TagIdList varchar(300) = '',
	@TagNameList nvarchar(1000) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	-- Video File Info 
	@FileName	varchar(500) = '',
	@Duration	varchar(50) = '',
	@Size	varchar(20) = '',
	@Capacity	int = 0,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	-- Primary Key
    @Id int
AS
	BEGIN TRANSACTION
	
    IF (@Id <= 0 OR NOT EXISTS (SELECT Id 
					FROM Video 
					WHERE	Id	= @Id))
		BEGIN
			RAISERROR ('Can not found this video.', 16, 1);
			ROLLBACK TRANSACTION
		END
    ELSE
		BEGIN

--			IF @ZoneIdList <> ''
--				SET @ZoneIdList = dbo.CMS_fGetListOfZoneVideoIdIncludeParentZoneVideoId(@ZoneId, @ZoneIdList + ';' + CONVERT(varchar(10), @ZoneId))

			DECLARE @ParentPrimaryZoneId int
			SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
			IF @ParentPrimaryZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
			END
			ELSE
			BEGIN
				DECLARE @ChildZoneId int
				SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
				IF @ChildZoneId > 0
				BEGIN
					IF @ZoneIdList <> ''
						SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
					ELSE
						SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
					SET @ZoneId = @ChildZoneId
				END
			END

			UPDATE Video
			SET
				ZoneId = @ZoneId,
				Name = @Name,
				UnsignName = @UnsignName,
				Description = @Description,
				HtmlCode = @HtmlCode,
				KeyVideo = @KeyVideo,
				Avatar = @Avatar,
				Pname = @Pname,
				Status = @Status,
				NewsId = @NewsId,
				Mode = @Mode,
				Tags = @Tags,
				LastModifiedBy = @LastModifiedBy,
				LastModifiedDate = GETDATE(),
				DistributionDate = @DistributionDate,
				Url = @Url,
				[Source] = @Source,
				VideoRelation = @VideoRelationIdList,
				FileName = @FileName,
				Duration = @Duration,
				Size = @Size,
				Capacity = @Capacity,
				AllowAd = @AllowAd,
				AvatarShareFacebook = @AvatarShareFacebook
			WHERE Id = @Id
			
			-- Process Tags

				
			-- DELETE ALL Tags of this video
			DELETE FROM VideoInTag WHERE VideoId = @Id
			
			-- DELETE ALL VideoInZone of this Video
			DELETE FROM VideoInZone  WHERE VideoId = @Id
				
			IF  @TagIdList <> '' AND @TagNameList <> ''
			BEGIN
				DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
				DECLARE @TblTagsList TABLE (Id int identity(1,1), Tag nvarchar(200) NOT NULL);
				DECLARE @TagName nvarchar(200),
						@Index int,
						@TagId int;
						
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
				-- Tag List
				INSERT INTO @TblTagsList (Tag)
				SELECT part FROM SplitString(@TagNameList, ';');
					
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					SET @TagName = (SELECT Tag FROM @TblTagsList WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') 
			BEGIN
				DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
				DECLARE @ZoneIdTemp int
			
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
					
			-- Process Playlist
			DELETE FROM VideoPlayList WHERE VideoId = @Id
			
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					IF @PlaylistId IS NOT NULL AND @PlaylistId > 0
					BEGIN
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @Index)
					END
					SET @Index = @Index + 1
				END
			END
			
		END
    
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION