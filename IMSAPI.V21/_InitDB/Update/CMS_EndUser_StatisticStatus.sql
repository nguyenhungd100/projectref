USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_StatisticStatus]    Script Date: 01/08/2018 10:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
CREATE PROCEDURE [dbo].[CMS_EndUser_StatisticStatus] --1,'2018-01-07','2018-12-08'
@Status int ,
@FromDate datetime = null,
@ToDate datetime = null
AS    
/*
	SELECT	
		CONVERT (NVARCHAR(20), EndUser.CreatedDate,103) as ValuesDate,
		COUNT (EndUser.[Status]) as ValuesData,
		EndUser.[Status]
	FROM EndUser
	WHERE	
		CreatedDate >=@FromDate AND CreatedDate <=@ToDate
		--AND	EndUser.[Status] = @Status			
	GROUP BY  CONVERT (NVARCHAR(20), EndUser.CreatedDate,103),EndUser.[Status] ORDER BY ValuesDate ASC
*/

SELECT tmp.ValuesDate,SUM(ValuesDataActive)AS ValuesDataActive,SUM(ValuesDataJoin) AS  ValuesDataJoin FROM 
        (
			   SELECT
					  CONVERT (NVARCHAR(20),EndUser.CreatedDate,103) as ValuesDate,
					  0 as ValuesDataJoin,
					  COUNT (*)  as ValuesDataActive
			   FROM EndUser
			   
			   WHERE	
					 EndUser.CreatedDate >=  @FromDate AND  EndUser.CreatedDate <= @ToDate
					 AND	EndUser.[Status]	=	1
			   GROUP BY CONVERT (NVARCHAR(20), EndUser.CreatedDate,103)
		
	      UNION ALL
	      
		       SELECT
					  CONVERT (NVARCHAR(20),EndUser.CreatedDate,103) as ValuesDate,
					  COUNT (*) as ValuesDataJoin,
					  0 as ValuesDataActive 
					  
			   FROM EndUser 
			  
			   WHERE
						EndUser.CreatedDate >=  @FromDate AND  EndUser.CreatedDate <= @ToDate
						AND	EndUser.[Status] = 2
			   GROUP BY  CONVERT (NVARCHAR(20),EndUser.CreatedDate,103)
			  
         ) AS tmp GROUP BY tmp.ValuesDate ORDER BY  convert(datetime, tmp.ValuesDate, 103) ASC
	
	
	
	
	
	
	
	
	

















