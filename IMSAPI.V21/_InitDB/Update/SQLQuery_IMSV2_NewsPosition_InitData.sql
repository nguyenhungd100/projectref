GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_InitData]    Script Date: 10/11/2017 9:47:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- Luu y:
-- - Bai BOMB hien chi co GiaDinh.Net su dung co che sync tu bang [NewsPositionSchedule] ;
-- - Tat ca kenh bao khac (neu có Bomb) thi su dung co che (cu) khac.



-- =============================================
-- Author:		<ThanhTN>
-- ALTER date: <2012-10-08>
-- Description:	<Init news position>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_InitData]
	@BaiNoiBatTrangChu int = 16,
	@BaiMoiNhatTrangChu int = 50,
	@BaiNoiBatMuc int = 0,
	@BaiMoiMucTrangMuc int = 100,
	@BaiNoiBatMobile int = 4,
	@BaiStreamMobile int = 20
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		DECLARE @ExludeIds varchar(500);
		-- Delete all row in NewsPosition first
		DELETE FROM NewsPosition
		-- Delete all row in NewsPositionType and reinsert new type
		DELETE FROM NewsPositionType
		-- Insert new type with new id
		SET IDENTITY_INSERT NewsPositionType ON -- Set identity off to insert Id
		-- @BaiNoiBatTrangChu
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (1, N'Bài nổi bật trang chủ (Set tay)', 0, 0)
		-- @BaiMoiNhatTrangChu
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (2, N'Bài mới nhất trang chủ (auto)', 0, 1)
		-- @BaiNoiBatMuc
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (3, N'Bài nổi bật mục (Set tay)', 1, 0)
		-- @BaiMoiMucTrangChu
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (4, N'Bài mới mục trang chủ (auto)', 1, 1)
		-- @BaiNoiBatMobile
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (5, N'Bài nổi bật mobile (Set tay)', 1, 1)
		-- @BaiStreamMobile
		INSERT INTO NewsPositionType (Id, PositionTypeName, FilterByZoneId, AllowAutoUpdate)
		VALUES (6, N'Bài stream mobile (auto)', 1, 1)
		SET IDENTITY_INSERT NewsPositionType OFF -- Set identity on
		
/*
		@TopNews int,
		@ZoneId int,
		@TypeId int,
		@ExcludeTypeIds varchar(MAX),
		@IsOnHome int = -1,
		@IsIncludeChildZoneId int = 1,
		@ExcludeOrder varchar(MAX) = '',
		@DisplayInSlide int = -1,
		@IsBreakingNews int = -1,
		@DisplayPosition int = 0,
		@IsFocus int = -1
*/
		------------------------------------------------
		-- @BaiNoiBatTrangChu
		INSERT INTO NewsPosition 
			(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
			LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
		SELECT 0, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			1 AS Lockable, 1 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 0 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
			GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
		FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiNoiBatTrangChu, 0, 1, '', -1, 1,'',-1,-1,0,-1, 0) AS NP
		-- @BaiMoiNhatTrangChu
		INSERT INTO NewsPosition 
			(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
			LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
		SELECT 0, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			0 AS Lockable, 0 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 1 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
			GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
		FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiMoiNhatTrangChu, 0, 2, '', -1, 1, '',-1,-1,0,-1, 1) AS NP
		------------------------------------------------
		
		------------------------------------------------
		-- @BaiNoiBatMobile
		INSERT INTO NewsPosition 
			(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
			LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
		SELECT 0, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			1 AS Lockable, 1 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 0 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
			GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
		FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiNoiBatMobile, 0, 5, '', -1, 1,'',-1,-1,0,-1, 0) AS NP
		-- @BaiStreamMobile
		INSERT INTO NewsPosition 
			(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
			LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
		SELECT 0, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
			Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
			IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
			InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
			RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
			PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
			0 AS Lockable, 0 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 1 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
			GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
		FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiStreamMobile, 0, 6, '', -1, 1, '',-1,-1,0,-1, 0) AS NP
		------------------------------------------------

		DECLARE @ZoneId int,
				@ParentZoneId int
			
		-- cursor loop throw all zone
		DECLARE ZoneCursor CURSOR FOR
		SELECT Id, ParentId
		FROM Zone
		WHERE Status = 1
		ORDER BY ParentId
		
		OPEN ZoneCursor
		
		FETCH NEXT FROM ZoneCursor INTO @ZoneId, @ParentZoneId
		WHILE @@FETCH_STATUS = 0   
		BEGIN
			
			IF @ParentZoneId = 5 -- Mục Fashion chỉ có 3 bài
				SET @BaiNoiBatMuc = 3

			-- @BaiNoiBatMuc
			INSERT INTO NewsPosition 
				(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
				Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
				LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
			SELECT @ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
				1 AS Lockable, 1 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 0 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
				GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
			FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiNoiBatMuc, @ZoneId, 3, '', -1, 0, '',-1,-1,0,-1, 0) AS NP
			
			-- @BaiMoiMucTrangMuc
			INSERT INTO NewsPosition 
				(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
				Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
				LastModifiedDate, LastModifiedDateStamp, ScheduleDate)
			SELECT @ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
				Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
				IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome,
				0 AS Lockable, 0 AS Locked, GETDATE() AS ExpiredLock, 0 AS NewsIdForBomd, 1 AS AllowAutoUpdate, ZoneId AS ZoneIdForNews,
				GETDATE() AS LastModifiedDate, dbo.UNIX_TIMESTAMP(GETDATE()) AS LastModifiedDateStamp, DistributionDate AS ScheduleDate
			FROM dbo.CMS_fGetNewsPublishForNewsPosition(@BaiMoiMucTrangMuc, @ZoneId, 4, '', -1, 0, '',-1,-1,0,-1, 1) AS NP
			
			FETCH NEXT FROM ZoneCursor INTO @ZoneId, @ParentZoneId 
		END   

		CLOSE ZoneCursor   
		DEALLOCATE ZoneCursor
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		IF CURSOR_STATUS('global','ZoneCursor') = 1
		BEGIN
		 CLOSE ZoneCursor
		END
		IF CURSOR_STATUS('global','ZoneCursor') >= -1
		BEGIN
		 DEALLOCATE ZoneCursor
		END
		PRINT ERROR_MESSAGE()
		ROLLBACK TRANSACTION
	END CATCH
END






