USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ThreadNews_GetByNewsId]    Script Date: 10/10/2017 16:23:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_ThreadNews_GetByNewsId]
	@NewsId bigint
AS
BEGIN
Select * from Thread
inner join ThreadNews on ThreadId=Id
where NewsId=@NewsId
END
