USE [IMS2_VNE]
GO
------------------------------------[CMS_News_Update]---------------------------------------------
/****** Object:  StoredProcedure [dbo].[CMS_News_Update]    Script Date: 04/11/2018 13:43:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-25>
-- Edited:		<CHINHNB>
-- ModifiedDate: <2018-04-11>
-- Description:	<Update news>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_Update]
	@Id bigint,
	@Title nvarchar(250),
	@SubTitle nvarchar(250),
	@Sapo nvarchar(MAX),
	@Body ntext,
	@Avatar nvarchar(500),
	@AvatarDesc nvarchar(500),
	@Avatar2 nvarchar(500),
	@Avatar3 nvarchar(500),
	@Avatar4 nvarchar(500),
	@Avatar5 nvarchar(500),
	@Author nvarchar(500),
	@NewsRelation nvarchar(max),
	@Status int,
	@Source nvarchar(100),
	@IsFocus bit,
	@Type tinyint,
	@ThreadId int,
	@DistributionDate datetime,
	@LastModifiedBy varchar(200),
	@WordCount int,
	@ViewCount int,
	@Priority tinyint,
	@Tag nvarchar(max),
	@Note nvarchar(2000),
	@TagPrimary nvarchar(250) = '',
	@Price decimal(18,3) = 0,
	@IsOnHome bit = 1,
	@NewsType int = 0,
	@OriginalId int = 0,
	-- Extension fields
	@DisplayStyle int = 0,
	@DisplayPosition int = 0,
	@DisplayInSlide int = 0,
	@AvatarCustom varchar(500) = NULL,
	-- INSERT INTO OTHER TABLES
	@ZoneId int = 0,
	@ZoneIdList varchar(500) = '',
	@TagIdList varchar(500) = '',
	@TagIdListForPrimary varchar(500) = '',
	@NewsRelationIdList varchar(max) = '',
	-- INSERT INTO NewsByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
	-- thanhtn add 2012-12-13
	@TagItem nvarchar(1000),
	@Url nvarchar(500),
	@NoteRoyalties nvarchar(1000),
	@NewsCategory int = 0,
	@InitSapo nvarchar(500) = '',
	-- Insert into NewsBySource table
	@SourceId int = 0,
	@TemplateName varchar(200),
	@TemplateConfig nvarchar(max),
	@IsBreakingNews bit,
	@PegaBreakingNews int = 0,
	@IsPr bit,
	@AdStore bit,
	@AdStoreUrl nvarchar(500),
	@PrBookingNumber varchar(30),
	@IsOnMobile bit = 1,
	@TagSubTitleId int = 0,
	@RollingNewsId int = 0,
	@InterviewId int = 0,
	@LocationType int = 0,
	@ExpiredDate datetime = null,
	@SourceUrl nvarchar(500) = null,
	--BonusPrice
	@BonusPrice decimal(18,3) = 0,
	@ShortTitle nvarchar(250) = '',
	@ApprovedBy varchar(200) = '',
	@ParentNewsId bigint
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @Source = ''
			SELECT @Source = Name FROM NewsSource WHERE Id = @SourceId

		IF @Id=20160704030726973
			SET @Type=27
		
		IF ISNULL((SELECT IsPublish FROM NewsTemp WHERE Id = @Id), 0) = 1 
			SET @Url = Null;
		-- Update News
		UPDATE News
		SET Title = @Title,
			SubTitle = @SubTitle,
			Sapo = @Sapo,
			Body = @Body,
			Avatar = @Avatar,
			AvatarDesc = @AvatarDesc,
			Avatar2 = @Avatar2,
			Avatar3 = @Avatar3,
			Avatar4 = @Avatar4,
			Avatar5 = @Avatar5,
			Author = @Author,
			NewsRelation = @NewsRelation,
			[Status] = @Status,
			[Source] = @Source,
			IsFocus = @IsFocus,
			[Type] = @Type,
			ThreadId = @ThreadId,
			LastModifiedDate = GETDATE(),
			DistributionDate = @DistributionDate,
			LastModifiedBy = @LastModifiedBy,
			WordCount = @WordCount,
			ViewCount = @ViewCount,
			Priority = @Priority,
			Tag = @Tag,
			Note = @Note,
			Price = @Price,
			TagPrimary = @TagPrimary
			,DisplayStyle = @DisplayStyle
			,DisplayPosition = @DisplayPosition
			,DisplayInSlide = @DisplayInSlide
			,AvatarCustom = @AvatarCustom
			,IsOnHome = @IsOnHome
			,NewsType = @NewsType
			,OriginalId = @OriginalId
			,TagItem = @TagItem
			,OriginalUrl = Url
			,Url = ISNULL(@Url, Url)
			,NoteRoyalties = @NoteRoyalties
			,NewsCategory = @NewsCategory
			,InitSapo = @InitSapo
			,TemplateName = @TemplateName
			,IsBreakingNews = @IsBreakingNews
			,PegaBreakingNews = @PegaBreakingNews
			,IsPr = @IsPr
			,AdStore = @AdStore
			,AdStoreUrl = @AdStoreUrl
			,PrBookingNumber = @PrBookingNumber
			,IsOnMobile = @IsOnMobile
			,TagSubTitleId = @TagSubTitleId
			,RollingNewsId = @RollingNewsId
			,InterviewId = @InterviewId
			,LocationType = @LocationType
			,ExpiredDate = @ExpiredDate
			,SourceUrl = @SourceUrl
			,BonusPrice = @BonusPrice
			,ShortTitle = @ShortTitle
			,ApprovedBy = @ApprovedBy
			,ParentNewsId=@ParentNewsId
		WHERE Id = @Id

		SET NOCOUNT ON
		
		DECLARE @Index int;
		
		-- CALL STORE DELETE ALL NewsByAuthor
		DELETE FROM NewsByAuthor WHERE NewsId = @Id
		-- RE INSERT
		IF @AuthorNameList <> '' AND @AuthorIdList <> ''
		BEGIN
			DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
			INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
			DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
			INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
			
			DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
			IF @AuthorNoteList <> ''
				INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
			SET @Index = 0;
			DECLARE @NumberRow@TblTempAuthorName int,
					@AuthorId int,
					@AuthorName nvarchar(255),
					@AuthorNote nvarchar(255);
			SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
			
			WHILE(@Index <= @NumberRow@TblTempAuthorName)
			BEGIN
				SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
				SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
				SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
				IF @AuthorId > 0
				BEGIN
					INSERT NewsByAuthor (NewsId, AuthorId, AuthorName, Note)
					VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
				END
				SET @Index = @Index + 1;
			END
		END
				
		-- DELETE all NewsInZone width NewsId
		DELETE FROM NewsInZone WHERE NewsId = @Id
		
		-- Insert Primary Zone NewsInZone
		INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneId, 1, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneId));
		
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0 AND @ZoneItemId <> @ZoneId
					IF NOT EXISTS(SELECT 1 FROM NewsInZone WHERE ZoneId = @ZoneItemId AND NewsId = @Id)
						INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneItemId, 0, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneItemId));
				SET @Index = @Index + 1
			END
		END		
		
		DELETE FROM TagNews WHERE (TagMode = 0 OR TagMode = 3) AND NewsID = @Id
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @Id AND (TagMode = 0 or TagMode = 3))
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemId, 0, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemId AND NewsId = @Id AND (TagMode = 0 or TagMode = 3)
				SET @Index = @Index + 1
			END
		END
		
		DELETE FROM TagNews WHERE TagMode = 1 AND NewsID = @Id
		-- Insert TagNewsForPrimary
		IF @TagIdListForPrimary <> ''
		BEGIN
			DECLARE @TblTempTagForPrimary TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemIdForPrimary bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTagForPrimary(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdListForPrimary, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTagForPrimary)) 
			BEGIN
				SET @TagItemIdForPrimary = (SELECT TagId FROM @TblTempTagForPrimary WHERE Id = @Index)
				IF @TagItemIdForPrimary IS NOT NULL AND @TagItemIdForPrimary > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemIdForPrimary, 1, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1
				SET @Index = @Index + 1
			END
		END

		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @Id)
				INSERT INTO ThreadNews (ThreadId, NewsId)
				VALUES (@ThreadId, @Id)
		END
		
		
		-- DELETE All NewsRelation
		DELETE FROM NewsRelation WHERE NewsId = @Id
		-- Insert NewsRelation
		IF @NewsRelationIdList <> ''
		BEGIN
			DECLARE @TblTempRelation TABLE (Id int identity(1,1), NewsId bigint)
			DECLARE @NewsItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempRelation(NewsId) SELECT CONVERT(bigint, Part) FROM SplitString(@NewsRelationIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
			BEGIN
				SET @NewsItemId = (SELECT NewsId FROM @TblTempRelation WHERE Id = @Index)
				IF @NewsItemId IS NOT NULL AND @NewsItemId > 0
					IF NOT EXISTS(SELECT NewsRelationId FROM NewsRelation WHERE NewsRelationId = @NewsItemId AND NewsId = @Id AND ZoneId = @ZoneId)
						INSERT INTO NewsRelation(NewsId, NewsRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @NewsItemId, @ZoneId, 0, @Index);
				SET @Index = @Index + 1
			END
		END

		-- insert NewsBySource 
		DELETE FROM NewsBySource where NewsId = @Id
		IF @SourceId <> 0
		BEGIN
			INSERT NewsBySource (NewsId, SourceId)
		    VALUES (@Id, @SourceId)
		END
		/* Xử lý autotag */
--		DELETE FROM AutoTag WHERE NewsId = @Id
--			
--		INSERT INTO AutoTag (NewsId, IsProcessed)
--		VALUES (@Id, 0)
		
		/***** NANIA Edited: Check them cho an toan ****/
--		IF @Status = 8
--		BEGIN
--			/***** NewsPosition ****/
--			UPDATE NewsPosition 
--				SET Avatar = @Avatar,
--					SubTitle = @SubTitle,
--					Title = @Title,
--					Sapo = @Sapo,
--					InitSapo = @InitSapo,
--					Author = @Author,
--					DisplayStyle = @DisplayStyle,
--					DisplayPosition = @DisplayPosition,
--					Url = @Url,
--					NewsRelation = @NewsRelation
--			WHERE NewsId = @Id
--		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO

---------------------------------[CMS_News_Insert]---------------------------------------------

/****** Object:  StoredProcedure [dbo].[CMS_News_Insert]    Script Date: 04/11/2018 13:37:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-25>
-- Edited:		<CHINHNB>
-- ModifiedDate: <2018-04-11>
-- Description:	<insert news>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_Insert]
	@Id bigint,
	@Title nvarchar(250),
	@SubTitle nvarchar(250),
	@Sapo nvarchar(MAX),
	@Body ntext,
	@Avatar nvarchar(500),
	@AvatarDesc nvarchar(500),
	@Avatar2 nvarchar(500),
	@Avatar3 nvarchar(500),
	@Avatar4 nvarchar(500),
	@Avatar5 nvarchar(500),
	@Author nvarchar(500),
	@NewsRelation nvarchar(max),
	@Source nvarchar(100),
	@IsFocus bit,
	@Type tinyint,
	@ThreadId int,
	@DistributionDate datetime,
	@CreatedBy varchar(200),
	@EditedBy varchar(200),
	@PublishedBy varchar(200),
	@WordCount int,
	@ViewCount int,
	@Priority tinyint,
	@Status int,
	@Tag nvarchar(max),
	@Note nvarchar(2000),
	@TagPrimary nvarchar(250) = '',
	@Price decimal(18,3) = 0,
	@IsOnHome bit = 1,
	@NewsType int = 0,
	@OriginalId int = 0,
	-- Extension fields
	@DisplayStyle int = 0,
	@DisplayPosition int = 0,
	@DisplayInSlide int = 0,
	@AvatarCustom varchar(500) = NULL,
	-- INSERT INTO OTHER TABLES
	@ZoneId int = 0,
	@ZoneIdList varchar(500) = '',
	@TagIdList varchar(500) = '',
	@TagIdListForPrimary varchar(500) = '',
	@NewsRelationIdList varchar(max) = '',
	-- INSERT INTO NewsByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
	-- thanhtn add 2012-12-13
	@TagItem nvarchar(1000),
	@Url nvarchar(500),
	@NoteRoyalties nvarchar(1000),
	@NewsCategory int = 0,
	@InitSapo nvarchar(500) = '',
	-- Insert into NewsBySource table
	@SourceId int = 0,
	@TemplateName varchar(200),
	@TemplateConfig nvarchar(max),
	@IsBreakingNews bit,
	@PegaBreakingNews int = 0,
	@IsPr bit,
	@AdStore bit,
	@AdStoreUrl nvarchar(500),
	@PrBookingNumber varchar(30),
	@IsOnMobile bit = 1,
	@TagSubTitleId int = 0,
	@RollingNewsId int = 0,
	@InterviewId int = 0,
	@LocationType int = 0,
	@ExpiredDate datetime = null,
	@SourceUrl nvarchar(500) = null,
	--BonusPrice
	@BonusPrice decimal(18,3) = 0,
	@LastReceiver nvarchar(200) = '',
	@ShortTitle nvarchar(250) = '',
	@ParentNewsId bigint
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @Source = ''
			SELECT @Source = Name FROM NewsSource WHERE Id = @SourceId

		DECLARE @LastModifiedBy varchar(255);
		SET @LastModifiedBy = @CreatedBy
		IF @Type = 5 SET @LastModifiedBy = @EditedBy
		-- Insert News (Save news with status Temporary = 1)
		INSERT INTO News (Id,Title,SubTitle,Sapo,Body,Avatar,AvatarDesc,Avatar2,Avatar3,Avatar4,Avatar5
			,Author,NewsRelation,[Status],[Source],IsFocus,[Type],ThreadId,CreatedDate,LastModifiedDate
			,DistributionDate,CreatedBy,LastModifiedBy,PublishedBy,EditedBy,LastReceiver,WordCount,ViewCount
			,Priority,Tag, Note, TagPrimary, Price,DisplayStyle, DisplayPosition, DisplayInSlide, AvatarCustom
			,NewsType, IsOnHome, OriginalId, TagItem, Url, NoteRoyalties, NewsCategory, InitSapo, OriginalUrl
			,TemplateName, TemplateConfig, IsBreakingNews, PegaBreakingNews, IsPr, AdStore, AdStoreUrl
			, PrBookingNumber, IsOnMobile, TagSubTitleId, RollingNewsId, InterviewId,LocationType, ExpiredDate,SourceUrl,BonusPrice,ShortTitle,ParentNewsId
			)
		VALUES (@Id,@Title,@SubTitle,@Sapo,@Body,@Avatar,@AvatarDesc,@Avatar2,@Avatar3,@Avatar4,@Avatar5
			,@Author,@NewsRelation,@Status,@Source,@IsFocus,@Type,@ThreadId,GETDATE(),GETDATE()
			,@DistributionDate,@CreatedBy,@LastModifiedBy,@PublishedBy,@EditedBy,@LastReceiver,@WordCount,@ViewCount
			,@Priority,@Tag, @Note, @TagPrimary, @Price, @DisplayStyle, @DisplayPosition, @DisplayInSlide, @AvatarCustom
			,@NewsType, @IsOnHome, @OriginalId, @TagItem, @Url, @NoteRoyalties, @NewsCategory, @InitSapo, @Url
			,@TemplateName, @TemplateConfig, @IsBreakingNews, @PegaBreakingNews, @IsPr, @AdStore, @AdStoreUrl
			, @PrBookingNumber, @IsOnMobile, @TagSubTitleId, @RollingNewsId, @InterviewId,@LocationType, @ExpiredDate,@SourceUrl,@BonusPrice,@ShortTitle,@ParentNewsId
			);
			
		SET NOCOUNT ON
		
		DECLARE @Index int
			
		IF @AuthorNameList <> '' AND @AuthorIdList <> '' 
		BEGIN
			DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
			INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
			DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
			INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
			DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
			IF @AuthorNoteList <> ''
				INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
				
			SET @Index = 0;
			DECLARE @NumberRow@TblTempAuthorName int,
					@AuthorId int,
					@AuthorName nvarchar(255),
					@AuthorNote nvarchar(255);
			SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
			
			WHILE(@Index <= @NumberRow@TblTempAuthorName)
			BEGIN
				SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
				SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
				SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
				IF @AuthorId > 0
				BEGIN
					INSERT NewsByAuthor (NewsId, AuthorId, AuthorName, Note)
					VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
				END
				SET @Index = @Index + 1;
			END
		END
		
			
		-- Insert Primary Zone NewsInZone
		INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneId, 1, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneId));
		
		-- INSERT Other Zone into NewsInZone
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0 AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM NewsInZone WHERE ZoneId = @ZoneItemId AND NewsId = @Id)
						INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneItemId, 0, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneItemId));
				SET @Index = @Index + 1
			END
		END
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @Id AND TagMode = 0)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemId, 0, @Index);
				SET @Index = @Index + 1
			END
		END
		-- Insert TagNewsForSubtitle
		IF @TagIdListForPrimary <> ''
		BEGIN
			DECLARE @TblTempTagForPrimary TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemIdForPrimary bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTagForPrimary(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdListForPrimary, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTagForPrimary)) 
			BEGIN
				SET @TagItemIdForPrimary = (SELECT TagId FROM @TblTempTagForPrimary WHERE Id = @Index)
				IF @TagItemIdForPrimary IS NOT NULL AND @TagItemIdForPrimary > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemIdForPrimary, 1, @Index);
				SET @Index = @Index + 1
			END
		END

		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @Id)
				INSERT INTO ThreadNews (ThreadId, NewsId)
				VALUES (@ThreadId, @Id)
		END

	
		-- Insert NewsRelation
		IF @NewsRelationIdList <> ''
		BEGIN
			DECLARE @TblTempRelation TABLE (Id int identity(1,1), NewsId bigint)
			DECLARE @NewsItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempRelation(NewsId) SELECT CONVERT(bigint, Part) FROM SplitString(@NewsRelationIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
			BEGIN
				SET @NewsItemId = (SELECT NewsId FROM @TblTempRelation WHERE Id = @Index)
				IF @NewsItemId IS NOT NULL AND @NewsItemId > 0
					IF NOT EXISTS(SELECT NewsRelationId FROM NewsRelation WHERE NewsRelationId = @NewsItemId AND NewsId = @Id AND ZoneId = @ZoneId)
						INSERT INTO NewsRelation(NewsId, NewsRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @NewsItemId, @ZoneId, 0, @Index);
				SET @Index = @Index + 1
			END
		END
		-- insert NewsBySource 
		IF @SourceId <> 0
		BEGIN
			DELETE FROM NewsBySource where NewsId = @Id
			INSERT NewsBySource (NewsId, SourceId)
		    VALUES (@Id, @SourceId)
		END
		/* Xử lý autotag */
		DELETE FROM AutoTag WHERE NewsId = @Id
			
		INSERT INTO AutoTag (NewsId, IsProcessed)
		VALUES (@Id, 0)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

---------------------------------[CMS_NewsPublish_GetNewsRelationByNewsId]---------------------------------------------
GO

-- =============================================
-- Author	  :	<ThanhTN>
-- Create date: <2012-10-01>
-- Edit: chinhnb : 10-04-2018
-- Description:	<get news relation by newsid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPublish_GetNewsRelationByNewsId]
	@NewsId bigint
AS
BEGIN
	SELECT * 
	FROM
		(
			SELECT DISTINCT NP.NewsId, 0 AS ZoneId, NP.Title, NP.Url, NP.Avatar, NR.Priority, NR.Type
			FROM NewsPublish AS NP INNER JOIN
				NewsRelation AS NR ON NP.NewsId = NR.NewsRelationId
			WHERE (NR.NewsId = @NewsId)
			AND NP.IsPrimary=1
		) AS TMP
	ORDER BY Priority ASC
END

GO
--19/4/2018 -> khi nao up code thi chay 2 cai nay:
--------------------------VideoCms_ZoneVideo_Insert-------------------
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Insert]
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@CreatedDate datetime,
	@Status int,
	@DisplayStyle int = 0,
	@ShowOnHome bit = 1,
	@ListVideoTagId varchar(1000) = '',
	@Invisibled bit = 1,
	@Id int output,
	@Avatar varchar(300) = '',
	@AvatarCover varchar(300) = '',
	@ZoneRelation varchar(100) = '',
	@Logo varchar(300) = '',
	@MetaAvatar varchar(max) = '',
	@CatId int
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZoneVideo (Name, Url, [Order], ParentId, Status, CreatedDate, ModifiedDate,DisplayStyle,ShowOnHome,Invisibled,Avatar,AvatarCover,ZoneRelation,Logo,MetaAvatar,CatId)
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@CreatedDate,@CreatedDate,@DisplayStyle,@ShowOnHome,@Invisibled,@Avatar,@AvatarCover,@ZoneRelation,@Logo,@MetaAvatar,@CatId)
	
	SET @Id = SCOPE_IDENTITY()
	
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
--------------------------------VideoCms_ZoneVideo_Update------------------------------------------
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@ModifiedDate datetime,
	@Status int,
	@ListVideoTagId varchar(1000) = '',
	@DisplayStyle int=0,
	@ShowOnHome bit = 1,
	@Invisibled bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@ZoneRelation varchar(100),
	@Logo varchar(300),
	@MetaAvatar varchar(max),
	@CatId int
AS
	BEGIN TRANSACTION
	
	UPDATE ZoneVideo
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		Status = @Status, 
		ModifiedDate = @ModifiedDate,
		DisplayStyle =@DisplayStyle,
		ShowOnHome =@ShowOnHome,
		Invisibled = @Invisibled,
		AvatarCover = @AvatarCover,
		Avatar = @Avatar,
		ZoneRelation=@ZoneRelation,
		Logo=@Logo,
		MetaAvatar=@MetaAvatar,
		CatId=@CatId
	WHERE (Id = @Id)

	DELETE FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
--------------------------[CMS_SEOMetaTags_Insert]-----------------
GO
--chinhnb
--03/04/2018
ALTER PROC [dbo].[CMS_SEOMetaTags_Insert]
@Name nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@ZoneId int,
@Id int OUT
AS
BEGIN 

  INSERT INTO SEOMetaTags (Name, MetaKeyword, MetaDescription,ZoneId) 
  VALUES (@Name,@MetaKeyword,@MetaDescription,@ZoneId)
  
  SET @Id = SCOPE_IDENTITY()
   
END

---------------------------ZONE --------------------------

-------------------------ADD COLLUM FOR TABLE ZONE----------------------------
GO
--chinhnb
--20/04/2018
ALTER TABLE Zone
ADD Avatar varchar(300),
	AvatarCover varchar(300),
	Logo varchar(300),	
	MetaAvatar varchar(max);
	
	
--------------------------CMS_Zone_Insert-------------------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Insert]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max)
AS
	BEGIN TRANSACTION CMS_Zone_Insert
	
	BEGIN TRY
		IF @Name IS NOT NULL AND @Name <> ''
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment,
				[Avatar]
				,[AvatarCover]
				,[Logo]				
				,[MetaAvatar]
			)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment,
				@Avatar
				,@AvatarCover
				,@Logo			
				,@MetaAvatar
			)
			COMMIT TRANSACTION CMS_Zone_Insert
		END
		ELSE BEGIN
			RAISERROR ('Invalid Zone Name', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Insert
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Insert
	END CATCH
	
	
-------------------------------CMS_Zone_Update-------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Update]
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(100),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@AllowComment bit = 1,
	@Domain nvarchar(300),
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max),
	@Id int
AS
	BEGIN TRANSACTION CMS_Zone_Update
	
	BEGIN TRY
		IF EXISTS (SELECT Id FROM Zone WHERE Id = @Id)
		BEGIN
			UPDATE Zone
			SET
				Name = @Name, 
				Description = @Description, 
				ShortURL = @ShortURL, 
				SortOrder = @SortOrder, 
				ParentId = @ParentId, 
				Invisibled = @Invisibled, 
				Status = @Status,
				ModifiedDate = GETDATE(),
				AllowComment = @AllowComment,
				Domain=@Domain,
				Avatar=@Avatar,
				AvatarCover=@AvatarCover,
				Logo=@Logo,
				MetaAvatar=@MetaAvatar
			WHERE Id = @Id
			COMMIT TRANSACTION CMS_Zone_Update
		END
		ELSE BEGIN
			RAISERROR ('This zone does not existed in database.', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Update
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Update
	END CATCH
		
-----------------------------------CMS_Zone_GetByParentId--------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-26>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE (ParentId = @ParentId) ORDER BY SortOrder asc
END

-----------------------------------------CMS_Zone_GetById---------------------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-24>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetById]
	@Id int
AS
BEGIN
	SELECT * FROM Zone	WHERE (Id = @Id)
END

---------------------------------------CMS_Zone_Active_GetByParentId------------------------
GO
-- =============================================
-- Author:		longlh
-- Create date: 04/09/2014
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id with status=1>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_Active_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone WHERE Status=1  ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE ParentId = @ParentId AND Status=1 ORDER BY SortOrder asc
END

GO
---------------------------------------










