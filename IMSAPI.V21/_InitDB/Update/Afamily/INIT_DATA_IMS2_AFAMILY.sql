
--SYNC DATA IMS_AFAMILY sang IMS2_AFAMILY
/*
[dbo].[Activity]	115602	1
-[dbo].[AfamilyVideo]	37238	2
[dbo].[AuthenticateLog]	19482	3
[dbo].[AutoTag]	56068	4
[dbo].[BoxNews]	11	5
[dbo].[BoxNewsEmbed]	15	6
[dbo].[BoxTagEmbed]	22	7
[dbo].[BoxThreadEmbed]	2	8
[dbo].[DiscussionAssignV2]	1970854	9
[dbo].[DiscussionAttachV2]	63009	10
[dbo].[DiscussionTopicFollowV2]	559705	11
[dbo].[DiscussionTopicV2]	18017	12
[dbo].[DiscussionV2]	63011	13
[dbo].[FEMonitor]	8244	14
[dbo].[FileUpload]	2	15
[dbo].[GroupPermission]	11	16
[dbo].[HotNewsTopic]	93	17
[dbo].[MenuExtension]	665	18
[dbo].[News]	307786	19
[dbo].[News_BackupTag]	173110	20
[dbo].[NewsAnalytic]	210042	21
[dbo].[NewsContent]	281028	22
[dbo].[NewsExtension]	76689	23
[dbo].[NewsHistory]	617	24
[dbo].[NewsInTopic]	541	25
[dbo].[NewsInZone]	465634	26
[dbo].[NewsLogExternalAction]	13	27
[dbo].[NewsNotification]	61922	28
[dbo].[NewsPosition]	6246	29
[dbo].[NewsPositionType]	4	30
[dbo].[NewsPr]	36869	31
[dbo].[NewsPublish]	431888	32
[dbo].[NewsPublishTemp]	69665	33
[dbo].[NewsRelation]	784330	34
[dbo].[NewsTemp]	52810	35
[dbo].[NewsUsePhoto]	10655	36
[dbo].[NewsVersion]	219330	37
[dbo].[Permission]	42	38
[dbo].[Photo]	68403	39
[dbo].[PhotoGroupDetail]	58436	40
[dbo].[PhotoGroupDetailInPhotoTag]	41407	41
[dbo].[PhotoGroupDetailInZone]	8745	42
[dbo].[PhotoInFolder]	15601	43
[dbo].[PhotoInLabel]	68403	44
[dbo].[PhotoPublished]	68403	45
[dbo].[PhotoTag]	4841	46
[dbo].[PlayList]	3	47
[dbo].[QuickNews]	6738	48
[dbo].[QuickNewsAuthor]	122	49
[dbo].[QuickNewsImages]	7399	50
[dbo].[Quote]	2	51
[dbo].[RollingNews]	2	52
[dbo].[RollingNewsEvent]	9	53
[dbo].[SEOMetaNews]	175348	54
[dbo].[SEOMetaVideo]	8266	55
[dbo].[SEOTagZone]	39323	56
[dbo].[Tag]	84038	57
[dbo].[TagInTopic]	5	58
[dbo].[TagNews]	1123680	59
[dbo].[TagZone]	39362	60
[dbo].[Thread]	478	61
[dbo].[ThreadInZone]	511	62
[dbo].[ThreadNews]	17775	63
[dbo].[ThreadRelation]	7	64
[dbo].[Topic]	5	65
[dbo].[TopicInZone]	5	66
[dbo].[User]	195	67
[dbo].[UserPermission]	21191	68
[dbo].[UserProfile]	107	69
[dbo].[UserSms]	30122	70
[dbo].[Video]	47453	71
[dbo].[VideoInFolder]	2136	72
[dbo].[VideoInNews]	21991	73
[dbo].[VideoInTag]	29765	74
[dbo].[VideoInZone]	51310	75
[dbo].[VideoTag]	5734	76
[dbo].[VideoTags]	97512	77
[dbo].[Zone]	176	78
[dbo].[ZonePhoto]	17	79
[dbo].[ZoneVideo]	30	80
[dbo].[ZoneVideoInVideoTag]	43	81
*/

--Set identity cho bang Tag

---------------------Use----------------------------
USE IMS2_AFAMILY

----------------------Activity---------------------------------------
GO
delete [Activity]
go
SET IDENTITY_INSERT [Activity] ON;
go
INSERT INTO [Activity](
		[Id]
      ,[ApplicationId]
      ,[SourceId]
      ,[SourceName]
      ,[DestinationId]
      ,[DestinationName]
      ,[CreatedDate]
      ,[ActionText]
      ,[Message]
      ,[OwnerId]
      ,[Type]
      ,[ActionTypeDetail]
      ,[GroupTimeLine]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Activity]
go
SET IDENTITY_INSERT [Activity] OFF;


----------------------AfamilyVideo---------------------------------------
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'AfamilyVideo')
BEGIN
    CREATE TABLE [dbo].[AfamilyVideo](
	[KeyVideo] [varchar](100) NULL,
	[FilePath] [nvarchar](500) NOT NULL
	) ON [PRIMARY]
END
GO
delete [AfamilyVideo]
go
SET IDENTITY_INSERT [AfamilyVideo] ON;
go
INSERT INTO [AfamilyVideo](
		[KeyVideo]
      ,[FilePath]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[AfamilyVideo]
go
SET IDENTITY_INSERT [AfamilyVideo] OFF;


----------------------AuthenticateLog---------------------------------------
GO
delete [AuthenticateLog]
go
SET IDENTITY_INSERT [AuthenticateLog] ON;
go
INSERT INTO [AuthenticateLog](
		[ID]
      ,[MachineName]
      ,[IpAddress]
      ,[UserAgent]
      ,[UrlReferrer]
      ,[Os]
      ,[LogDate]
      ,[UserName]
      ,[Status]
      ,[Description]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[AuthenticateLog]
go
SET IDENTITY_INSERT [AuthenticateLog] OFF;


----------------------AutoTag---------------------------------------
GO
delete [AutoTag]
go
SET IDENTITY_INSERT [AutoTag] ON;
go
INSERT INTO [AutoTag](
		[Id]
      ,[NewsId]
      ,[IsProcessed]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[AutoTag]
go
SET IDENTITY_INSERT [AutoTag] OFF;


----------------------BoxNews---------------------------------------
GO
delete [BoxNews]
go
SET IDENTITY_INSERT [BoxNews] ON;
go
INSERT INTO [BoxNews](
		[ID]
      ,[News_ID]
      ,[ThuTu]
      ,[Type]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[BoxNews]
go
SET IDENTITY_INSERT [BoxNews] OFF;


----------------------BoxNewsEmbed---------------------------------------
GO
delete [BoxNewsEmbed]
go
SET IDENTITY_INSERT [BoxNewsEmbed] ON;
go
INSERT INTO [BoxNewsEmbed](
		[Id]
      ,[ZoneId]
      ,[NewsId]
      ,[SortOrder]
      ,[Type]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[BoxNewsEmbed]
go
SET IDENTITY_INSERT [BoxNewsEmbed] OFF;

----------------------BoxTagEmbed---------------------------------------
GO
delete [BoxTagEmbed]
go
SET IDENTITY_INSERT [BoxTagEmbed] ON;
go
INSERT INTO [BoxTagEmbed](
		[Id]
      ,[ZoneId]
      ,[TagId]
      ,[SortOrder]
      ,[Type]
      ,[Title]
      ,[Url]
      ,[Name]
      ,[Avatar]
      ,[Embed]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[BoxTagEmbed]
go
SET IDENTITY_INSERT [BoxTagEmbed] OFF;

----------------------BoxThreadEmbed---------------------------------------
GO
delete [BoxThreadEmbed]
go
SET IDENTITY_INSERT [BoxThreadEmbed] ON;
go
INSERT INTO [BoxThreadEmbed](
		[Id]
      ,[ZoneId]
      ,[ThreadId]
      ,[SortOrder]
      ,[Type]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[BoxThreadEmbed]
go
SET IDENTITY_INSERT [BoxThreadEmbed] OFF;

----------------------DiscussionAssignV2---------------------------------------
GO
delete [DiscussionAssignV2]
go
SET IDENTITY_INSERT [DiscussionAssignV2] ON;
go
INSERT INTO [DiscussionAssignV2](
		[Id]
      ,[DiscussionTopicId]
      ,[DiscussionId]
      ,[UserAssigned]
      ,[Type]
      ,[IsRead]
      ,[IsHighlight]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[DiscussionAssignV2]
go
SET IDENTITY_INSERT [DiscussionAssignV2] OFF;


----------------------DiscussionAttachV2---------------------------------------
GO
delete [DiscussionAttachV2]
go
SET IDENTITY_INSERT [DiscussionAttachV2] ON;
go
INSERT INTO [DiscussionAttachV2](
		[Id]
      ,[DiscussionTopicId]
      ,[DiscussionId]
      ,[AttachName]
      ,[AttachUrl]
      ,[Note]
      ,[Avatar]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[DiscussionAttachV2]
go
SET IDENTITY_INSERT [DiscussionAttachV2] OFF;


----------------------DiscussionTopicFollowV2---------------------------------------
GO
delete [DiscussionTopicFollowV2]
go
SET IDENTITY_INSERT [DiscussionTopicFollowV2] ON;
go
INSERT INTO [DiscussionTopicFollowV2](
		[Id]
      ,[DiscussionTopicId]
      ,[UserFollow]
      ,[IsFollow]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[DiscussionTopicFollowV2]
go
SET IDENTITY_INSERT [DiscussionTopicFollowV2] OFF;


----------------------DiscussionTopicV2---------------------------------------
GO
delete [DiscussionTopicV2]
go
SET IDENTITY_INSERT [DiscussionTopicV2] ON;
go
INSERT INTO [DiscussionTopicV2](
		[Id]
      ,[ObjectId]
      ,[Title]
      ,[Type]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[CloseDate]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[DiscussionTopicV2]
go
SET IDENTITY_INSERT [DiscussionTopicV2] OFF;


----------------------DiscussionV2---------------------------------------
GO
delete [DiscussionV2]
go
SET IDENTITY_INSERT [DiscussionV2] ON;
go
INSERT INTO [DiscussionV2](
		[Id]
      ,[ParentId]
      ,[DiscussionTopicId]
      ,[Content]
      ,[CreatedBy]
      ,[ListReceiver]
      ,[CreatedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[DiscussionV2]
go
SET IDENTITY_INSERT [DiscussionV2] OFF;

/* Không dùng tự tạo trigger
----------------------FEMonitor---------------------------------------
GO
delete [FEMonitor]
go
SET IDENTITY_INSERT [FEMonitor] ON;
go
INSERT INTO [FEMonitor](
		[ID]
      ,[IsProcessed]
      ,[IsNewProcessed]
      ,[NewsId]
      ,[ZoneId]
      ,[ParentId]
      ,[OriginalTitle]
      ,[TitleLast]
      ,[Url]
      ,[UrlLast]
      ,[TypeProcessed]
      ,[TimeInsert]
      ,[TimeUpdate]
      ,[IsBeta]
      ,[IsNodeProcessed]
      ,[IsProcessedMobile]
      ,[MobileProcessedDate]
      ,[Action]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[FEMonitor]
go
SET IDENTITY_INSERT [FEMonitor] OFF;
*/

----------------------FileUpload---------------------------------------
GO
delete [FileUpload]
go
SET IDENTITY_INSERT [FileUpload] ON;
go
INSERT INTO [FileUpload](
		[Id]
      ,[ZoneId]
      ,[Title]
      ,[Description]
      ,[FileDownloadPath]
      ,[FilePath]
      ,[FileExt]
      ,[FileSize]
      ,[UploadedDate]
      ,[UploadedBy]
      ,[LastModifiedDate]
      ,[LastModifiedBy]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[FileUpload]
go
SET IDENTITY_INSERT [FileUpload] OFF;


----------------------GroupPermission---------------------------------------
GO
delete [GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] ON;
go
INSERT INTO [GroupPermission](
		[Id]
      ,[Name]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] OFF;

----------------------HotNewsTopic---------------------------------------
GO
delete [HotNewsTopic]
go
SET IDENTITY_INSERT [HotNewsTopic] ON;
go
INSERT INTO [HotNewsTopic](
		[Id]
      ,[NewsId]
      ,[TopicId]
      ,[Avatar]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[HotNewsTopic]
go
SET IDENTITY_INSERT [HotNewsTopic] OFF;

----------------------MenuExtension---------------------------------------
--GO
--IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'MenuExtension')
--BEGIN
--	ALTER TABLE [MenuExtension]
--	ADD [TagId] [bigint],	
--	[OldId] [int],
--	[OldParentId] [int],
--	[OldType] [int],
--	[OldIsTopMenu] [int],
--	[OldCatId] [int];
--END
GO
delete [MenuExtension]
go
SET IDENTITY_INSERT [MenuExtension] ON;
go
INSERT INTO [MenuExtension](
		[Id]
      ,[ParentMenuId]
      ,[GroupMenuId]
      ,[ZoneId]
      ,[ParentZoneId]
      ,[TagId]
      ,[Name]
      ,[Avatar]
      ,[Url]
      ,[Priority]
      ,[Status]
      ,[OldId]
      ,[OldParentId]
      ,[OldType]
      ,[OldIsTopMenu]
      ,[OldCatId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[MenuExtension]
go
SET IDENTITY_INSERT [MenuExtension] OFF;


----------------------News---------------------------------------
GO
delete [News]
go
SET IDENTITY_INSERT [News] ON;
go
INSERT INTO [News](
		[Id]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Status]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[PublishedBy]
      ,[EditedBy]
      ,[LastReceiver]
      ,[WordCount]
      ,[ViewCount]
      ,[Priority]
      ,[Tag]
      ,[Note]
      ,[TagPrimary]
      ,[Price]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[NewsType]
      ,[IsOnHome]
      ,[Url]
      ,[NoteRoyalties]
      ,[TagItem]
      ,[NewsCategory]
      ,[InitSapo]
      ,[TemplateName]
      ,[TemplateConfig]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[PrBookingNumber]
      ,[PegaBreakingNews]
      ,[RollingNewsId]
      ,[IsOnMobile]
      ,[TagSubTitleId]
      ,[PrPosition]
      ,[LocationType]
      ,[ExpiredDate]
      ,[SourceUrl]
      ,[BonusPrice]
      ,[ViewCountMobile]
      ,[PhotoCount]
      ,[VideoCount]
      ,[ShortTitle]
      ,[ParentNewsId]
      ,[WarningLevel]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[News]
go
SET IDENTITY_INSERT [News] OFF;


----------------------NewsAnalytic---------------------------------------
GO
delete [NewsAnalytic]
go
SET IDENTITY_INSERT [NewsAnalytic] ON;
go
INSERT INTO [NewsAnalytic](
		[NewsId]
      ,[ViewCount]
      ,[LastUpdateViewCount]
      ,[ViewCountHourly]
      ,[ViewCountDaily]
      ,[ViewCountMobile]
      ,[ViewCountMobileDaily]
      ,[ViewCountMobileHourly]
      ,[LastUpdateViewCountMobile]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsAnalytic]
go
SET IDENTITY_INSERT [NewsAnalytic] OFF;

----------------------NewsContent---------------------------------------
GO
delete [NewsContent]
go
SET IDENTITY_INSERT [NewsContent] ON;
go
INSERT INTO [NewsContent](
		[NewsId]
      ,[Title]
      ,[Sapo]
      ,[Avatar]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Body]
      ,[PublishedDate]
      ,[Source]
      ,[NewsRelation]
      ,[Tags]
      ,[Author]
      ,[TagPrimary]
      ,[Url]
      ,[ZoneId]
      ,[OriginalId]
      ,[TagItem]
      ,[SubTitle]
      ,[InitSapo]
      ,[InterviewId]
      ,[OriginalUrl]
      ,[Type]
      ,[AvatarDesc]
      ,[NewsType]
      ,[RollingNewsId]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[TagSubTitleId]
      ,[ThreadId]
      ,[Position]
      ,[PrPosition]
      ,[IsOnMobile]
      ,[UseTemplate]
      ,[LocationType]
      ,[ExpiredDate]
      ,[SourceUrl]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsContent]
go
SET IDENTITY_INSERT [NewsContent] OFF;


----------------------NewsExtension---------------------------------------
GO
delete [NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] ON;
go
INSERT INTO [NewsExtension](
		[NewsId]
      ,[Type]
      ,[Value]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] OFF;


----------------------NewsHistory---------------------------------------
GO
delete [NewsHistory]
go
SET IDENTITY_INSERT [NewsHistory] ON;
go
INSERT INTO [NewsHistory](
		[ID]
      ,[NewsId]
      ,[ActionName]
      ,[CreatedDate]
      ,[SentBy]
      ,[ReceivedBy]
      ,[Status]
      ,[Description]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsHistory]
go
SET IDENTITY_INSERT [NewsHistory] OFF;

----------------------NewsInTopic---------------------------------------
GO
delete [NewsInTopic]
go
SET IDENTITY_INSERT [NewsInTopic] ON;
go
INSERT INTO [NewsInTopic](
		[Id]
      ,[NewsId]
      ,[TopicId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsInTopic]
go
SET IDENTITY_INSERT [NewsInTopic] OFF;


----------------------NewsInZone---------------------------------------
GO
delete [NewsInZone]
go
SET IDENTITY_INSERT [NewsInZone] ON;
go
INSERT INTO [NewsInZone](
		[NewsId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[DistributionID]
      ,[RootZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsInZone]
go
SET IDENTITY_INSERT [NewsInZone] OFF;


----------------------NewsLogExternalAction---------------------------------------
GO
delete [NewsLogExternalAction]
go
SET IDENTITY_INSERT [NewsLogExternalAction] ON;
go
INSERT INTO [NewsLogExternalAction](
		[Id]
      ,[NewsId]
      ,[ExternalActionType]
      ,[ExternalActionDate]
      ,[ProcessedBy]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsLogExternalAction]
go
SET IDENTITY_INSERT [NewsLogExternalAction] OFF;


----------------------NewsNotification---------------------------------------
GO
delete [NewsNotification]
go
SET IDENTITY_INSERT [NewsNotification] ON;
go
INSERT INTO [NewsNotification](
		[NewsId]
      ,[UserName]
      ,[NewsStatus]
      ,[LabelId]
      ,[IsRead]
      ,[LastNotifiedDate]
      ,[IsWarning]
      ,[Message]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsNotification]
go
SET IDENTITY_INSERT [NewsNotification] OFF;


----------------------NewsPosition---------------------------------------
GO
delete [NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] ON;
go
INSERT INTO [NewsPosition](
		[Id]
      ,[ZoneId]
      ,[TypeId]
      ,[Order]
      ,[Lockable]
      ,[Locked]
      ,[ExpiredLock]
      ,[NewsIdForBomd]
      ,[NewsId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[IsFocus]
      ,[NewsType]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[AllowAutoUpdate]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[InitSapo]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[RollingNewsId]
      ,[LastModifiedDate]
      ,[TagSubTitleId]
      ,[ZoneIdForNews]
      ,[Position]
      ,[PrPosition]
      ,[Type]
      ,[Priority]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[LocationType]
      ,[ExpiredDate]
      ,[ScheduleDate]
      ,[IsOnHome]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] OFF;


----------------------NewsPositionType---------------------------------------
GO
delete [NewsPositionType]
go
SET IDENTITY_INSERT [NewsPositionType] ON;
go
INSERT INTO [NewsPositionType](
		[Id]
      ,[PositionTypeName]
      ,[FilterByZoneId]
      ,[AllowAutoUpdate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPositionType]
go
SET IDENTITY_INSERT [NewsPositionType] OFF;


----------------------NewsPr---------------------------------------
GO
delete [NewsPr]
go
SET IDENTITY_INSERT [NewsPr] ON;
go
INSERT INTO [NewsPr](
		[Id]
      ,[ContentId]
      ,[DistributionId]
      ,[NewsId]
      ,[Mode]
      ,[IsFocus]
      ,[DistributionDate]
      ,[PublishDate]
      ,[ExpireDate]
      ,[CreatedDate]
      ,[Duration]
      ,[LastModifiedDate]
      ,[BookingId]
      ,[ContractNo]
      ,[ZoneId]
      ,[Price]
      ,[ViewPlusBannerId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPr]
go
SET IDENTITY_INSERT [NewsPr] OFF;


----------------------NewsPublish---------------------------------------
GO
delete [NewsPublish]
go
SET IDENTITY_INSERT [NewsPublish] ON;
go
INSERT INTO [NewsPublish](
		[Id]
      ,[NewsId]
      ,[ZoneId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[PublishedDate]
      ,[IsOnHome]
      ,[TagItem]
      ,[IsPrimary]
      ,[InitSapo]
      ,[NewsType]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[LastModifiedDate]
      ,[RollingNewsId]
      ,[IsOnMobile]
      ,[TagSubTitleId]
      ,[Position]
      ,[PrPosition]
      ,[Priority]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[LocationType]
      ,[ExpiredDate]
      ,[DistributionID]
      ,[PrimaryZoneId]
      ,[TitleDetail]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPublish]
go
SET IDENTITY_INSERT [NewsPublish] OFF;


----------------------NewsPublishTemp---------------------------------------
GO
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'NewsPublishTemp')
BEGIN
	ALTER TABLE [NewsPublishTemp]
	ADD [Avatar4] [nvarchar](500);
END
GO
delete [NewsPublishTemp]
go
SET IDENTITY_INSERT [NewsPublishTemp] ON;
go
INSERT INTO [NewsPublishTemp](
		[ID]
      ,[NewsId]
      ,[ZoneId]
      ,[Title]
      ,[Sapo]
      ,[Avatar]
      ,[IsFocus]
      ,[Type]
      ,[DistributionDate]
      ,[Url]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[OriginalId]
      ,[PublishedDate]
      ,[IsOnHome]
      ,[TagItem]
      ,[IsPrimary]
      ,[NewsType]
      ,[IsOnMobile]
      ,[Position]
      ,[Priority]
      ,[ParentNewsId]
      ,[Avatar4]
      ,[Source]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPublishTemp]
go
SET IDENTITY_INSERT [NewsPublishTemp] OFF;


----------------------NewsRelation---------------------------------------
GO
delete [NewsRelation]
go
SET IDENTITY_INSERT [NewsRelation] ON;
go
INSERT INTO [NewsRelation](
		[NewsId]
      ,[NewsRelationId]
      ,[ZoneId]
      ,[IsChange]
      ,[Priority]
      ,[Type]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsRelation]
go
SET IDENTITY_INSERT [NewsRelation] OFF;

----------------------NewsTemp---------------------------------------
GO
delete [NewsTemp]
go
SET IDENTITY_INSERT [NewsTemp] ON;
go
INSERT INTO [NewsTemp](
		[Id]
      ,[Title]
      ,[Avatar]
      ,[Author]
      ,[Status]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[PublishedBy]
      ,[EditedBy]
      ,[LastReceiver]
      ,[WordCount]
      ,[ViewCount]
      ,[ViewCountMobile]
      ,[Note]
      ,[DisplayInSlide]
      ,[NewsType]
      ,[IsOnHome]
      ,[NewsCategory]
      ,[IsBreakingNews]
      ,[Url]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[InterviewId]
      ,[RollingNewsId]
      ,[OriginalId]
      ,[IsPr]
      ,[AdStore]
      ,[Priority]
      ,[ParentNewsId]
      ,[WarningLevel]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsTemp]
go
SET IDENTITY_INSERT [NewsTemp] OFF;


----------------------NewsUsePhoto---------------------------------------
GO
delete [NewsUsePhoto]
go
SET IDENTITY_INSERT [NewsUsePhoto] ON;
go
INSERT INTO [NewsUsePhoto](
		[NewsId]
      ,[PhotoPublishedId]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsUsePhoto]
go
SET IDENTITY_INSERT [NewsUsePhoto] OFF;

----------------------NewsVersion---------------------------------------
GO
delete [NewsVersion]
go
SET IDENTITY_INSERT [NewsVersion] ON;
go
INSERT INTO [NewsVersion](
		[Id]
      ,[Version]
      ,[CreatedDateVersion]
      ,[IsEditing]
      ,[CreateVersionBy]
      ,[NewsId]
      ,[ListZoneId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Status]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[PublishedBy]
      ,[EditedBy]
      ,[LastReceiver]
      ,[WordCount]
      ,[ViewCount]
      ,[Priority]
      ,[Tag]
      ,[Note]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[IsOnHome]
      ,[OriginalId]
      ,[Price]
      ,[Url]
      ,[NoteRoyalties]
      ,[TagItem]
      ,[NewsCategory]
      ,[InitSapo]
      ,[InterviewId]
      ,[ShortTitle]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsVersion]
go
SET IDENTITY_INSERT [NewsVersion] OFF;

----------------------Permission---------------------------------------
GO
delete [Permission]
go
SET IDENTITY_INSERT [Permission] ON;
go
INSERT INTO [Permission](
		[Id]
      ,[GroupId]
      ,[Name]
      ,[IsGrantByCategory]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Permission]
go
SET IDENTITY_INSERT [Permission] OFF;

----------------------Photo---------------------------------------
GO
delete [Photo]
go
SET IDENTITY_INSERT [Photo] ON;
go
INSERT INTO [Photo](
		[Id]
      ,[ZoneId]
      ,[Name]
      ,[ImageUrl]
      ,[ImageNote]
      ,[Size]
      ,[Capacity]
      ,[Note]
      ,[Status]
      ,[ModifiedDate]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[CreatedBy]
      ,[OriginalName]
      ,[UnSignName]
      ,[Tags]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Photo]
go
SET IDENTITY_INSERT [Photo] OFF;


----------------------PhotoGroupDetail---------------------------------------
GO
delete [PhotoGroupDetail]
go
SET IDENTITY_INSERT [PhotoGroupDetail] ON;
go
INSERT INTO [PhotoGroupDetail](
		[Id]
      ,[ZoneId]
      ,[Title]
      ,[ImageUrl]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Description]
      ,[IsHot]
      ,[Note]
      ,[RelatedLink]
      ,[Status]
      ,[ModifiedDate]
      ,[ModifiedBy]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[DistributionDate]
      ,[DistributionBy]
      ,[UnSignName]
      ,[PhotoGroupId]
      ,[Tags]
      ,[ViewCount]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoGroupDetail]
go
SET IDENTITY_INSERT [PhotoGroupDetail] OFF;

----------------------PhotoGroupDetailInPhotoTag---------------------------------------
GO
delete [PhotoGroupDetailInPhotoTag]
go
SET IDENTITY_INSERT [PhotoGroupDetailInPhotoTag] ON;
go
INSERT INTO [PhotoGroupDetailInPhotoTag](
		[Id]
      ,[PhotoGroupDetailId]
      ,[PhotoTagId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoGroupDetailInPhotoTag]
go
SET IDENTITY_INSERT [PhotoGroupDetailInPhotoTag] OFF;

----------------------PhotoGroupDetailInZone---------------------------------------
GO
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'PhotoGroupDetailInZone')
BEGIN
    CREATE TABLE [dbo].[PhotoGroupDetailInZone](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PhotoGroupDetailId] [int] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsPrimary] [bit] NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	 CONSTRAINT [PK_PhotoGroupDetailInZone] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[PhotoGroupDetailInZone] ADD  CONSTRAINT [DF_PhotoGroupDetailInZone_IsPrimary]  DEFAULT ((0)) FOR [IsPrimary]
	
	ALTER TABLE [dbo].[PhotoGroupDetailInZone] ADD  CONSTRAINT [DF_PhotoGroupDetailInZone_LastModifiedDate]  DEFAULT (getdate()) FOR [LastModifiedDate]
	
END
go
delete [PhotoGroupDetailInZone]
go
SET IDENTITY_INSERT [PhotoGroupDetailInZone] ON;
go
INSERT INTO [PhotoGroupDetailInZone](
		[Id]
      ,[PhotoGroupDetailId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoGroupDetailInZone]
go
SET IDENTITY_INSERT [PhotoGroupDetailInZone] OFF;

----------------------PhotoInFolder---------------------------------------
GO
delete [PhotoInFolder]
go
SET IDENTITY_INSERT [PhotoInFolder] ON;
go
INSERT INTO [PhotoInFolder](
		[PhotoId]
      ,[FolderId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoInFolder]
go
SET IDENTITY_INSERT [PhotoInFolder] OFF;

----------------------PhotoInLabel---------------------------------------
GO
delete [PhotoInLabel]
go
SET IDENTITY_INSERT [PhotoInLabel] ON;
go
INSERT INTO [PhotoInLabel](
		[PhotoLabelId]
      ,[PhotoId]
      ,[IsPrimary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoInLabel]
go
SET IDENTITY_INSERT [PhotoInLabel] OFF;

/*
----------------------PhotoDetail---------------------------------------
GO
delete [PhotoDetail]
go
SET IDENTITY_INSERT [PhotoDetail] ON;
go
INSERT INTO [PhotoDetail](
		[PhotoDetail_ID]
      ,[News_ID]
      ,[PhotoDetail_Image]
      ,[PhotoDetail_Des]
      ,[CreateDated]
      ,[Status]
      ,[PageView]
      ,[Icon]
      ,[PhotoDetail_Title]
      ,[PhotoOrder]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoDetail]
go
SET IDENTITY_INSERT [PhotoDetail] OFF;
*/

----------------------PhotoPublished---------------------------------------
GO
delete [PhotoPublished]
go
SET IDENTITY_INSERT [PhotoPublished] ON;
go
INSERT INTO [PhotoPublished](
		[Id]
      ,[PhotoId]
      ,[AlbumId]
      ,[NewsId]
      ,[ZoneId]
      ,[Name]
      ,[ImageUrl]
      ,[ImageNote]
      ,[DistributionDate]
      ,[DistributedBy]
      ,[Status]
      ,[Priority]
      ,[Size]
      ,[Tags]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoPublished]
go
SET IDENTITY_INSERT [PhotoPublished] OFF;


----------------------PhotoTag---------------------------------------
GO
delete [PhotoTag]
go
SET IDENTITY_INSERT [PhotoTag] ON;
go
INSERT INTO [PhotoTag](
		[Id]
      ,[Name]
      ,[Avatar]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PhotoTag]
go
SET IDENTITY_INSERT [PhotoTag] OFF;

----------------------PlayList---------------------------------------
GO
delete [PlayList]
go
SET IDENTITY_INSERT [PlayList] ON;
go
INSERT INTO [PlayList](
		[Id]
      ,[ZoneId]
      ,[Name]
      ,[UnsignName]
      ,[Description]
      ,[Status]
      ,[Mode]
      ,[Avatar]
      ,[Priority]
      ,[DistributionDate]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[LastModifiedDate]
      ,[LastModifiedBy]
      ,[PublishedDate]
      ,[PublishedBy]
      ,[LastInsertVideoDate]
      ,[VideoCount]
      ,[Url]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[PlayList]
go
SET IDENTITY_INSERT [PlayList] OFF;

----------------------QuickNews---------------------------------------
GO
delete [QuickNews]
go
SET IDENTITY_INSERT [QuickNews] ON;
go
INSERT INTO [QuickNews](
		[QuickNewsId]
      ,[CatId]
      ,[VietId]
      ,[News_ID]
      ,[Title]
      ,[Sapo]
      ,[Content]
      ,[Note]
      ,[Link]
      ,[FullName]
      ,[Phone]
      ,[Email]
      ,[Type]
      ,[Status]
      ,[PenName]
      ,[Time]
      ,[Location]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[ReceivedBy]
      ,[ReceivedDate]
      ,[DeletedBy]
      ,[DeletedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[QuickNews]
go
SET IDENTITY_INSERT [QuickNews] OFF;

----------------------QuickNewsAuthor---------------------------------------
GO
delete [QuickNewsAuthor]
go
SET IDENTITY_INSERT [QuickNewsAuthor] ON;
go
INSERT INTO [QuickNewsAuthor](
		[VietId]
      ,[UserName]
      ,[FullName]
      ,[Avatar]
      ,[Phone]
      ,[Address]
      ,[Email]
      ,[Status]
      ,[Job]
      ,[Slogan]
      ,[PenName]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[LastLoginDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[QuickNewsAuthor]
go
SET IDENTITY_INSERT [QuickNewsAuthor] OFF;

----------------------QuickNewsImages---------------------------------------
GO
delete [QuickNewsImages]
go
SET IDENTITY_INSERT [QuickNewsImages] ON;
go
INSERT INTO [QuickNewsImages](
		[QuickNewsImagesId]
      ,[QuickNewsId]
      ,[Image]
      ,[Description]
      ,[IsAvatar]
      ,[Status]
      ,[Type]
      ,[CreatedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[QuickNewsImages]
go
SET IDENTITY_INSERT [QuickNewsImages] OFF;


----------------------Quote---------------------------------------
GO
delete [Quote]
go
SET IDENTITY_INSERT [Quote] ON;
go
INSERT INTO [Quote](
		[QuoteId]
      ,[Avatar]
      ,[Quote]
      ,[QuoteTitle]
      ,[Position]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Quote]
go
SET IDENTITY_INSERT [Quote] OFF;

----------------------RollingNews---------------------------------------
GO
delete [RollingNews]
go
SET IDENTITY_INSERT [RollingNews] ON;
go
INSERT INTO [RollingNews](
		[Id]
      ,[Title]
      ,[ShowTime]
      ,[ShowAuthor]
      ,[PublishedContent]
      ,[StartDate]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate]
      ,[Status]
      ,[IsShowRollingNewsLabel]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[RollingNews]
go
SET IDENTITY_INSERT [RollingNews] OFF;

----------------------RollingNewsEvent---------------------------------------
GO
delete [RollingNewsEvent]
go
SET IDENTITY_INSERT [RollingNewsEvent] ON;
go
INSERT INTO [RollingNewsEvent](
		[Id]
      ,[RollingNewsId]
      ,[EventType]
      ,[EventContent]
      ,[EventNote]
      ,[EventTime]
      ,[IsFocus]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[PublishedBy]
      ,[PublishedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate]
      ,[Status]
      ,[MobileContent]
      ,[ShortContent]
      ,[Images]
      ,[ImageCount]
      ,[FacebookId]
      ,[FacebookIdUrl]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[RollingNewsEvent]
go
SET IDENTITY_INSERT [RollingNewsEvent] OFF;

----------------------SEOMetaNews---------------------------------------
GO
delete [SEOMetaNews]
go
SET IDENTITY_INSERT [SEOMetaNews] ON;
go
INSERT INTO [SEOMetaNews](
		[NewsId]
      ,[MetaTitle]
      ,[MetaKeyword]
      ,[MetaDescription]
      ,[MetaNewsKeyword]
      ,[KeywordFocus]
      ,[CreatedBy]
      ,[SocialTitle]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[SEOMetaNews]
go
SET IDENTITY_INSERT [SEOMetaNews] OFF;



----------------------SEOMetaVideo---------------------------------------
GO
delete [SEOMetaVideo]
go
SET IDENTITY_INSERT [SEOMetaVideo] ON;
go
INSERT INTO [SEOMetaVideo](
		[VideoId]
      ,[MetaTitle]
      ,[MetaKeyword]
      ,[MetaDescription]
      ,[MetaVideoKeyword]
      ,[KeywordFocus]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[SEOMetaVideo]
go
SET IDENTITY_INSERT [SEOMetaVideo] OFF;


----------------------SEOTagZone---------------------------------------
GO
delete [SEOTagZone]
go
SET IDENTITY_INSERT [SEOTagZone] ON;
go
INSERT INTO [SEOTagZone](
		[TagId]
      ,[ZoneId]
      ,[IsPrimary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[SEOTagZone]
go
SET IDENTITY_INSERT [SEOTagZone] OFF;


----------------------Tag---------------------------------------
GO
delete [Tag]
go
SET IDENTITY_INSERT [Tag] ON;
go
INSERT INTO [Tag](
		[Id]
      ,[ParentId]
      ,[Name]
      ,[Description]
      ,[Url]
      ,[Invisibled]
      ,[IsHotTag]
      ,[Type]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CreatedBy]
      ,[EditedBy]
      ,[UnsignName]
      ,[IsThread]
      ,[Avatar]
      ,[Priority]
      ,[TagContent]
      ,[TagTitle]
      ,[TagInit]
      ,[TagMetaKeyword]
      ,[TagMetaContent]
      ,[TemplateId]
      ,[ViewCount]
      ,[SubTitle]
      ,[SubTile]
      ,[NewsCoverId]
      ,[Status]
      ,[CountNewsInTag]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Tag]
go
SET IDENTITY_INSERT [Tag] OFF;

----------------------TagInTopic---------------------------------------
GO
delete [TagInTopic]
go
SET IDENTITY_INSERT [TagInTopic] ON;
go
INSERT INTO [TagInTopic](
		[Id]
      ,[TopicId]
      ,[TagId]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TagInTopic]
go
SET IDENTITY_INSERT [TagInTopic] OFF;

----------------------TagNews---------------------------------------
GO
delete [TagNews]
go
SET IDENTITY_INSERT [TagNews] ON;
go
INSERT INTO [TagNews](
		[TagID]
      ,[NewsID]
      ,[TagMode]
      ,[TagProperty]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TagNews]
go
SET IDENTITY_INSERT [TagNews] OFF;


----------------------TagZone---------------------------------------
GO
delete [TagZone]
go
SET IDENTITY_INSERT [TagZone] ON;
go
INSERT INTO [TagZone](
		[TagId]
      ,[ZoneId]
      ,[IsPrimary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TagZone]
go
SET IDENTITY_INSERT [TagZone] OFF;


----------------------Thread---------------------------------------
GO
delete [Thread]
go
SET IDENTITY_INSERT [Thread] ON;
go
INSERT INTO [Thread](
		[Id]
      ,[Name]
      ,[UnsignName]
      ,[Title]
      ,[Description]
      ,[Url]
      ,[Avatar]
      ,[HomeAvatar]
      ,[SpecialAvatar]
      ,[IsHot]
      ,[IsOnHome]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CreatedBy]
      ,[EditedBy]
      ,[MetaKeyword]
      ,[MetaContent]
      ,[TemplateId]
      ,[RelationThread]
      ,[RelationZone]
      ,[Ordinary]
      ,[Invisibled]
      ,[IsHightLight]
      ,[IsHightLightOnMobile]
      ,[ViewCount]
      ,[NewsCoverId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Thread]
go
SET IDENTITY_INSERT [Thread] OFF;


----------------------ThreadInZone---------------------------------------
GO
delete [ThreadInZone]
go
SET IDENTITY_INSERT [ThreadInZone] ON;
go
INSERT INTO [ThreadInZone](
		[ZoneId]
      ,[ThreadId]
      ,[CreatedDate]
      ,[IsPrimary]
      ,[Ordinary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadInZone]
go
SET IDENTITY_INSERT [ThreadInZone] OFF;


----------------------ThreadNews---------------------------------------
GO
delete [ThreadNews]
go
SET IDENTITY_INSERT [ThreadNews] ON;
go
INSERT INTO [ThreadNews](
		[ThreadId]
      ,[NewsId]
      ,[Ordinary]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadNews]
go
SET IDENTITY_INSERT [ThreadNews] OFF;

----------------------ThreadRelation---------------------------------------
GO
delete [ThreadRelation]
go
SET IDENTITY_INSERT [ThreadRelation] ON;
go
INSERT INTO [ThreadRelation](
		[ThreadId]
      ,[ThreadRelationId]
      ,[Ordinary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadRelation]
go
SET IDENTITY_INSERT [ThreadRelation] OFF;


----------------------Topic---------------------------------------
GO
delete [Topic]
go
SET IDENTITY_INSERT [Topic] ON;
go
INSERT INTO [Topic](
		[Id]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive]
      ,[DisplayUrl]
      ,[Description]
      ,[LogoFancyClose]
      ,[LogoTopicName]
      ,[LogoSubMenu]
      ,[DefaultViewMode]
      ,[GuideToSendMail]
      ,[TopicEmail]
      ,[isTopToolbar]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Topic]
go
SET IDENTITY_INSERT [Topic] OFF;


----------------------TopicInZone---------------------------------------
GO
delete [TopicInZone]
go
SET IDENTITY_INSERT [TopicInZone] ON;
go
INSERT INTO [TopicInZone](
		[Id]
      ,[TopicId]
      ,[ZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TopicInZone]
go
SET IDENTITY_INSERT [TopicInZone] OFF;

----------------------USER---------------------------------------
GO
delete [User]
go
SET IDENTITY_INSERT [User] ON;
go
INSERT INTO [User](
		[Id]
      ,[UserName]
      ,[Password]
      ,[FullName]
      ,[Avatar]
      ,[Email]
      ,[Mobile]
      ,[IsFullPermission]
      ,[IsFullZone]
      ,[Status]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[LastLogined]
      ,[LastChangePass]
      ,[IsSystem]
      ,[OtpSecretKey]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[User]
go
SET IDENTITY_INSERT [User] OFF;


----------------------UserPermission---------------------------------------
GO
delete [UserPermission]
go
SET IDENTITY_INSERT [UserPermission] ON;
go
INSERT INTO [UserPermission](
		[UserId]
      ,[PermissionId]
      ,[ZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[UserPermission]
go
SET IDENTITY_INSERT [UserPermission] OFF;


----------------------UserProfile---------------------------------------
GO
delete [UserProfile]
go
SET IDENTITY_INSERT [UserProfile] ON;
go
INSERT INTO [UserProfile](
		[UserId]
      ,[Address]
      ,[Birthday]
      ,[Description]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[UserProfile]
go
SET IDENTITY_INSERT [UserProfile] OFF;


----------------------UserSms---------------------------------------
GO
delete [UserSms]
go
SET IDENTITY_INSERT [UserSms] ON;
go
INSERT INTO [UserSms](
		[UserId]
      ,[CreatedDate]
      ,[SmsCode]
      ,[ExpiredDate]
      ,[InUsing]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[UserSms]
go
SET IDENTITY_INSERT [UserSms] OFF;


----------------------Video---------------------------------------
GO
delete [Video]
go
SET IDENTITY_INSERT [Video] ON;
go
INSERT INTO [Video](
		[Id]
      ,[ZoneId]
      ,[Name]
      ,[UnsignName]
      ,[Description]
      ,[HtmlCode]
      ,[Avatar]
      ,[KeyVideo]
      ,[Pname]
      ,[Status]
      ,[NewsId]
      ,[Views]
      ,[Mode]
      ,[Tags]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate]
      ,[PublishDate]
      ,[PublishBy]
      ,[EditedDate]
      ,[EditedBy]
      ,[Url]
      ,[Source]
      ,[VideoRelation]
      ,[FileName]
      ,[Duration]
      ,[Size]
      ,[Capacity]
      ,[AllowAd]
      ,[IsRemoveLogo]
      ,[OriginalUrl]
      ,[Type]
      ,[IsConverted]
      ,[AvatarShareFacebook]
      ,[Author]
      ,[OriginalId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Video]
go
SET IDENTITY_INSERT [Video] OFF;

----------------------VideoInFolder---------------------------------------
GO
delete [VideoInFolder]
go
SET IDENTITY_INSERT [VideoInFolder] ON;
go
INSERT INTO [VideoInFolder](
		[VideoId]
      ,[FolderId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoInFolder]
go
SET IDENTITY_INSERT [VideoInFolder] OFF;


----------------------VideoInNews---------------------------------------
GO
delete [VideoInNews]
go
SET IDENTITY_INSERT [VideoInNews] ON;
go
INSERT INTO [VideoInNews](
		[VideoId]
      ,[NewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoInNews]
go
SET IDENTITY_INSERT [VideoInNews] OFF;

----------------------VideoInTag---------------------------------------
GO
delete [VideoInTag]
go
SET IDENTITY_INSERT [VideoInTag] ON;
go
INSERT INTO [VideoInTag](
		[VideoTagId]
      ,[VideoId]
      ,[TagMode]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoInTag]
go
SET IDENTITY_INSERT [VideoInTag] OFF;


----------------------VideoInZone---------------------------------------
GO
delete [VideoInZone]
go
SET IDENTITY_INSERT [VideoInZone] ON;
go
INSERT INTO [VideoInZone](
		[VideoId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[LastModifiedDate]
      ,[RootZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoInZone]
go
SET IDENTITY_INSERT [VideoInZone] OFF;


----------------------VideoTag---------------------------------------
GO
delete [VideoTag]
go
SET IDENTITY_INSERT [VideoTag] ON;
go
INSERT INTO [VideoTag](
		[Id]
      ,[ParentId]
      ,[Name]
      ,[UnsignName]
      ,[Avatar]
      ,[Description]
      ,[Url]
      ,[IsHotTag]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoTag]
go
SET IDENTITY_INSERT [VideoTag] OFF;

----------------------VideoTags---------------------------------------
GO
delete [VideoTags]
go
SET IDENTITY_INSERT [VideoTags] ON;
go
INSERT INTO [VideoTags](
		[TagId]
      ,[VideoId]
      ,[Type]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoTags]
go
SET IDENTITY_INSERT [VideoTags] OFF;


----------------------Vote---------------------------------------
GO
delete [Vote]
go
SET IDENTITY_INSERT [Vote] ON;
go
INSERT INTO [Vote](
		[Id]
      ,[Title]
      ,[Avatar]
      ,[Sapo]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[Status]
      ,[Author]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[Priority]
      ,[DisplayPosition]
      ,[DisplayStyle]
      ,[ViewCount]
      ,[RateCount]
      ,[MaxAnswers]
      ,[StartedDate]
      ,[EndedDate]
      ,[ShowInZone]
      ,[ShowInFooter]
      ,[NewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Vote]
go
SET IDENTITY_INSERT [Vote] OFF;


----------------------VoteAnswers---------------------------------------
GO
delete [VoteAnswers]
go
SET IDENTITY_INSERT [VoteAnswers] ON;
go
INSERT INTO [VoteAnswers](
		[Id]
      ,[VoteId]
      ,[Value]
      ,[VoteRate]
      ,[Status]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VoteAnswers]
go
SET IDENTITY_INSERT [VoteAnswers] OFF;


----------------------VoteInZone---------------------------------------
GO
delete [VoteInZone]
go
SET IDENTITY_INSERT [VoteInZone] ON;
go
INSERT INTO [VoteInZone](
		[VoteID]
      ,[ZoneId]
      ,[IsPrimary]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VoteInZone]
go
SET IDENTITY_INSERT [VoteInZone] OFF;


----------------------Zone---------------------------------------
GO
delete [Zone]
go
SET IDENTITY_INSERT [Zone] ON;
go
INSERT INTO [Zone](
		[Id]
      ,[Name]
      ,[Description]
      ,[ModifiedDate]
      ,[CreatedDate]
      ,[ShortURL]
      ,[SortOrder]
      ,[ParentId]
      ,[Invisibled]
      ,[Status]
      ,[AllowComment]
      ,[Domain]
      ,[UseForFunnyNews]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Zone]
go
SET IDENTITY_INSERT [Zone] OFF;


----------------------ZonePhoto---------------------------------------
GO
delete [ZonePhoto]
go
SET IDENTITY_INSERT [ZonePhoto] ON;
go
INSERT INTO [ZonePhoto](
		[Id]
      ,[ParentId]
      ,[Name]
      ,[Description]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ZonePhoto]
go
SET IDENTITY_INSERT [ZonePhoto] OFF;

----------------------ZoneVideo---------------------------------------
GO
delete [ZoneVideo]
go
SET IDENTITY_INSERT [ZoneVideo] ON;
go
INSERT INTO [ZoneVideo](
		[Id]
      ,[Name]
      ,[Url]
      ,[Order]
      ,[ParentId]
      ,[Status]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CatId]
      ,[DisplayStyle]
      ,[ShowOnHome]
      ,[Invisibled]
      ,[Keyword]
      ,[ListNewsZoneId]
      ,[Avatar]
      ,[AvatarCover]
      ,[ZoneRelation]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ZoneVideo]
go
SET IDENTITY_INSERT [ZoneVideo] OFF;

----------------------ZoneVideoInVideoTag---------------------------------------
GO
delete [ZoneVideoInVideoTag]
go
SET IDENTITY_INSERT [ZoneVideoInVideoTag] ON;
go
INSERT INTO [ZoneVideoInVideoTag](
		[ZoneVideoId]
      ,[VideoTagId]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ZoneVideoInVideoTag]
go
SET IDENTITY_INSERT [ZoneVideoInVideoTag] OFF;


----------------------NewsMobileStream---------------------------------------
GO
delete [NewsMobileStream]
go
SET IDENTITY_INSERT [NewsMobileStream] ON;
go
INSERT INTO [NewsMobileStream](
		[Id]
      ,[ZoneId]
      ,[NewsId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[NewsType]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[DisplayStyle]
      ,[DisplayInSlide]
      ,[OriginalId]
      ,[InitSapo]
      ,[InterviewId]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[RollingNewsId]
      ,[LastModifiedDate]
      ,[TagSubTitleId]
      ,[PrPosition]
      ,[Type]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[IsHighlight]
      ,[Status]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsMobileStream]
go
SET IDENTITY_INSERT [NewsMobileStream] OFF;

GO


