CREATE TABLE #counts
(
    table_name varchar(255),
    row_count int
)

EXEC sp_MSForEachTable @command1='INSERT #counts (table_name, row_count) SELECT ''?'', COUNT(*) FROM ?'

SELECT table_name, row_count, RowNum = ROW_NUMBER() OVER(ORDER BY table_name) FROM #counts 
	where row_count>0
	ORDER BY table_name, row_count DESC

CREATE TABLE #CUS
(
    table_name varchar(255),
    row_count int,
    RowNum int
)

INSERT INTO #CUS
SELECT table_name, row_count, RowNum = ROW_NUMBER() OVER(ORDER BY table_name) FROM #counts 
	where row_count>0 --and table_name in ('[dbo].[User]','[dbo].[Zone]','[dbo].[Tag]')
	ORDER BY table_name, row_count DESC
	
	
	
	
	
DROP TABLE #counts
DROP TABLE #CUS