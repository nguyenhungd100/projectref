
----------------------USER---------------------------------------
GO
delete [User]
go
SET IDENTITY_INSERT [User] ON;
go
INSERT INTO [User](
		[Id]
      ,[UserName]
      ,[Password]
      ,[FullName]
      ,[Avatar]
      ,[Email]
      ,[Mobile]
      ,[IsFullPermission]
      ,[IsFullZone]
      ,[Status]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[LastLogined]
      ,[LastChangePass]
      ,[IsSystem]
      ,[OtpSecretKey]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[User]
go
SET IDENTITY_INSERT [User] OFF;


----------------------UserPermission---------------------------------------
GO
delete [UserPermission]
go
SET IDENTITY_INSERT [UserPermission] ON;
go
INSERT INTO [UserPermission](
		[UserId]
      ,[PermissionId]
      ,[ZoneId]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[UserPermission]
go
SET IDENTITY_INSERT [UserPermission] OFF;


----------------------UserProfile---------------------------------------
GO
delete [UserProfile]
go
SET IDENTITY_INSERT [UserProfile] ON;
go
INSERT INTO [UserProfile](
		[UserId]
      ,[Address]
      ,[Birthday]
      ,[Description]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[UserProfile]
go
SET IDENTITY_INSERT [UserProfile] OFF;


----------------------GroupPermission---------------------------------------
GO
delete [GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] ON;
go
INSERT INTO [GroupPermission](
		[Id]
      ,[Name]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] OFF;


----------------------Permission---------------------------------------
GO
delete [Permission]
go
SET IDENTITY_INSERT [Permission] ON;
go
INSERT INTO [Permission](
		[Id]
      ,[GroupId]
      ,[Name]
      ,[IsGrantByCategory]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[Permission]
go
SET IDENTITY_INSERT [Permission] OFF;

----------------------Zone---------------------------------------
GO
delete [Zone]
go
SET IDENTITY_INSERT [Zone] ON;
go
INSERT INTO [Zone](
		[Id]
      ,[Name]
      ,[Description]
      ,[ModifiedDate]
      ,[CreatedDate]
      ,[ShortURL]
      ,[SortOrder]
      ,[ParentId]
      ,[Invisibled]
      ,[Status]
      ,[AllowComment]
      ,[Domain]
      ,[UseForFunnyNews]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[Zone]
go
SET IDENTITY_INSERT [Zone] OFF;

----------------------News---------------------------------------
GO
delete [News]
go
SET IDENTITY_INSERT [News] ON;
go
INSERT INTO [News](
		[Id]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Status]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[PublishedBy]
      ,[EditedBy]
      ,[LastReceiver]
      ,[WordCount]
      ,[ViewCount]
      ,[Priority]
      ,[Tag]
      ,[Note]
      ,[TagPrimary]
      ,[Price]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[NewsType]
      ,[IsOnHome]
      ,[Url]
      ,[NoteRoyalties]
      ,[TagItem]
      ,[NewsCategory]
      ,[InitSapo]
      ,[TemplateName]
      ,[TemplateConfig]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[PrBookingNumber]
      ,[PegaBreakingNews]
      ,[RollingNewsId]
      ,[IsOnMobile]
      ,[TagSubTitleId]
      ,[PrPosition]
      ,[LocationType]
      ,[ExpiredDate]
      ,[SourceUrl]
      ,[BonusPrice]
      ,[ViewCountMobile]
      ,[PhotoCount]
      ,[VideoCount]
      ,[ShortTitle]
      ,[ParentNewsId]
      ,[WarningLevel]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[News] --WHERE DistributionDate >= DATEADD(day,-5, getdate()) and DistributionDate <= getdate() and Status=8
go
SET IDENTITY_INSERT [News] OFF;

----------------------NewsInZone---------------------------------------
GO
delete [NewsInZone]
go
SET IDENTITY_INSERT [NewsInZone] ON;
go
INSERT INTO [NewsInZone](
		[NewsId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[DistributionID]
      ,[RootZoneId]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[NewsInZone]
go
SET IDENTITY_INSERT [NewsInZone] OFF;


----------------------NewsExtension---------------------------------------
GO
delete [NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] ON;
go
INSERT INTO [NewsExtension](
		[NewsId]
      ,[Type]
      ,[Value]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] OFF;


----------------------NewsPosition---------------------------------------
GO
delete [NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] ON;
go
INSERT INTO [NewsPosition](
		[Id]
      ,[ZoneId]
      ,[TypeId]
      ,[Order]
      ,[Lockable]
      ,[Locked]
      ,[ExpiredLock]
      ,[NewsIdForBomd]
      ,[NewsId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[IsFocus]
      ,[NewsType]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[AllowAutoUpdate]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[InitSapo]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[RollingNewsId]
      ,[LastModifiedDate]
      ,[TagSubTitleId]
      ,[ZoneIdForNews]
      ,[Position]
      ,[PrPosition]
      ,[Type]
      ,[Priority]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[LocationType]
      ,[ExpiredDate]
      ,[ScheduleDate]
      ,[IsOnHome]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS2_AFAMILY].[dbo].[NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] OFF;

