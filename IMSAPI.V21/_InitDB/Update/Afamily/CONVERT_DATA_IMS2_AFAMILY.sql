Use [IMS2_AFAMILY_BETA]

--"B?ng NewsPublish:
GO
--select * from News where NewsType=4
--select * from News where NewsType=0 and [Type]=8
--C�c tin c� NewsType = 4 chuy?n NewsType = 0 v� set Type = 8
UPDATE NewsPublish set NewsType=0, [Type]=8 WHERE NewsType=4
GO
UPDATE News set NewsType=0, [Type]=8 WHERE NewsType=4

GO
--select * from News where NewsType=5
--select * from News where NewsType=0 and [Type]=18
--C�c tin c� NewsType = 5 chuy?n NewsType = 0 v� set Type = 18"
UPDATE NewsPublish set NewsType=0, [Type]=18 WHERE NewsType=5
GO
UPDATE News set NewsType=0, [Type]=18 WHERE NewsType=5


--"B?ng NewsExtension:
GO
--select * from NewsExtension where [Type]=8008
--select * from NewsExtension where [Type]=3005
--Item c� Type = 8008 th� chuy?n Type v? 3005
UPDATE NewsExtension set [Type]=3005 WHERE [Type]=8008

GO
--select * from NewsExtension where [Type]=8009
--select * from NewsExtension where [Type]=3006
--Item c� Type = 8009 th� chuy?n Type v? 3006
UPDATE NewsExtension set [Type]=3006 WHERE [Type]=8009

GO
--select * from NewsExtension where [Type]=8010
--select * from NewsExtension where [Type]=3007
--Item c� Type = 8010 th� chuy?n Type v? 3007
UPDATE NewsExtension set [Type]=3007 WHERE [Type]=8010


--"Avatar b�i chi ti?t:
GO
--select Id, Avatar3, Avatar5,type, createddate from News WHERE [Type]<>27 and Avatar5 is not null and avatar5<>'' order by createddate desc
--select Id, Avatar3, Avatar5,type, createddate from News WHERE [Type]<>27 and (Avatar5 is null or avatar5='') order by createddate desc

--N?u b�i vi?t c� Avatar5 th� ??i gi� tr? c?a 2 c?t Avatar5 v� Avatar3
--N?u b�i vi?t kh�ng c� Avatar5 th� chuy?n gi� tr? Avatar3 sang Avatar5 (avatar share fb)
UPDATE News set Avatar5=Avatar3,Avatar3=Avatar5 WHERE [Type]<>27 and Avatar5 is not null and avatar5<>''
GO
UPDATE News set Avatar5=Avatar3 WHERE [Type]<>27 and (Avatar5 is null or avatar5 ='')

--N?u l� b�i Magazine:
GO
--select avatar3,Avatar4 from News where [Type]=27
--Chuy?n gi� tr? c?a Avatar4 sang Avatar3 (Cover b?n desktop)
UPDATE News set Avatar3=Avatar4 WHERE [Type]=27

GO
--select Avatar4, Value from News INNER JOIN NewsExtension ON News.Id=NewsExtension.NewsId 
--WHERE NewsExtension.[Type]=3004 and News.[Type]=27
--Chuy?n gi� tr? t? b?ng NewsExtension Type = 3004 set v�o Avatar4 (Cover b?n mobile)"
UPDATE News set Avatar4=NewsExtension.Value FROM News 
INNER JOIN NewsExtension ON News.Id=NewsExtension.NewsId 
WHERE NewsExtension.[Type]=3004 and News.[Type]=27

--UPDATE News set Avatar4=NewsExtension.Value FROM NewsExtension WHERE NewsExtension.NewsId=Id AND NewsExtension.[Type]=3004