


----------------------USER---------------------------------------
GO
delete [User]
go
SET IDENTITY_INSERT [User] ON;
go
INSERT INTO [User](
		[Id]
      ,[UserName]
      ,[Password]
      ,[FullName]
      ,[Avatar]
      ,[Email]
      ,[Mobile]
      ,[IsFullPermission]
      ,[IsFullZone]
      ,[Status]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[LastLogined]
      ,[LastChangePass]
      ,[IsSystem]
      ,[OtpSecretKey]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[User]
go
SET IDENTITY_INSERT [User] OFF;

----------------------UserPermission---------------------------------------
GO
delete [UserPermission]
go
SET IDENTITY_INSERT [UserPermission] ON;
go
INSERT INTO [UserPermission](
		[UserId]
      ,[PermissionId]
      ,[ZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[UserPermission]
go
SET IDENTITY_INSERT [UserPermission] OFF;


----------------------UserProfile---------------------------------------
GO
delete [UserProfile]
go
SET IDENTITY_INSERT [UserProfile] ON;
go
INSERT INTO [UserProfile](
		[UserId]
      ,[Address]
      ,[Birthday]
      ,[Description]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[UserProfile]
go
SET IDENTITY_INSERT [UserProfile] OFF;

----------------------GroupPermission---------------------------------------
GO
delete [GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] ON;
go
INSERT INTO [GroupPermission](
		[Id]
      ,[Name]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[GroupPermission]
go
SET IDENTITY_INSERT [GroupPermission] OFF;


----------------------Permission---------------------------------------
GO
delete [Permission]
go
SET IDENTITY_INSERT [Permission] ON;
go
INSERT INTO [Permission](
		[Id]
      ,[GroupId]
      ,[Name]
      ,[IsGrantByCategory]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Permission]
go
SET IDENTITY_INSERT [Permission] OFF;


----------------------Zone---------------------------------------
GO
delete [Zone]
go
SET IDENTITY_INSERT [Zone] ON;
go
INSERT INTO [Zone](
		[Id]
      ,[Name]
      ,[Description]
      ,[ModifiedDate]
      ,[CreatedDate]
      ,[ShortURL]
      ,[SortOrder]
      ,[ParentId]
      ,[Invisibled]
      ,[Status]
      ,[AllowComment]
      ,[Domain]
      ,[UseForFunnyNews]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Zone]
go
SET IDENTITY_INSERT [Zone] OFF;


----------------------News---------------------------------------
GO
delete [News]
go
SET IDENTITY_INSERT [News] ON;
go
INSERT INTO [News](
		[Id]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Status]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[CreatedDate]
      ,[LastModifiedDate]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[LastModifiedBy]
      ,[PublishedBy]
      ,[EditedBy]
      ,[LastReceiver]
      ,[WordCount]
      ,[ViewCount]
      ,[Priority]
      ,[Tag]
      ,[Note]
      ,[TagPrimary]
      ,[Price]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[NewsType]
      ,[IsOnHome]
      ,[Url]
      ,[NoteRoyalties]
      ,[TagItem]
      ,[NewsCategory]
      ,[InitSapo]
      ,[TemplateName]
      ,[TemplateConfig]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[PrBookingNumber]
      ,[PegaBreakingNews]
      ,[RollingNewsId]
      ,[IsOnMobile]
      ,[TagSubTitleId]
      ,[PrPosition]
      ,[LocationType]
      ,[ExpiredDate]
      ,[SourceUrl]
      ,[BonusPrice]
      ,[ViewCountMobile]
      ,[PhotoCount]
      ,[VideoCount]
      ,[ShortTitle]
      ,[ParentNewsId]
      ,[WarningLevel]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[News]
go
SET IDENTITY_INSERT [News] OFF;

----------------------NewsContent---------------------------------------
GO
delete [NewsContent]
go
SET IDENTITY_INSERT [NewsContent] ON;
go
INSERT INTO [NewsContent](
		[NewsId]
      ,[Title]
      ,[Sapo]
      ,[Avatar]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Body]
      ,[PublishedDate]
      ,[Source]
      ,[NewsRelation]
      ,[Tags]
      ,[Author]
      ,[TagPrimary]
      ,[Url]
      ,[ZoneId]
      ,[OriginalId]
      ,[TagItem]
      ,[SubTitle]
      ,[InitSapo]
      ,[InterviewId]
      ,[OriginalUrl]
      ,[Type]
      ,[AvatarDesc]
      ,[NewsType]
      ,[RollingNewsId]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[TagSubTitleId]
      ,[ThreadId]
      ,[Position]
      ,[PrPosition]
      ,[IsOnMobile]
      ,[UseTemplate]
      ,[LocationType]
      ,[ExpiredDate]
      ,[SourceUrl]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsContent]
go
SET IDENTITY_INSERT [NewsContent] OFF;


----------------------NewsExtension---------------------------------------
GO
delete [NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] ON;
go
INSERT INTO [NewsExtension](
		[NewsId]
      ,[Type]
      ,[Value]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsExtension]
go
SET IDENTITY_INSERT [NewsExtension] OFF;


----------------------NewsPosition---------------------------------------
GO
delete [NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] ON;
go
INSERT INTO [NewsPosition](
		[Id]
      ,[ZoneId]
      ,[TypeId]
      ,[Order]
      ,[Lockable]
      ,[Locked]
      ,[ExpiredLock]
      ,[NewsIdForBomd]
      ,[NewsId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[IsFocus]
      ,[NewsType]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[AllowAutoUpdate]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[InitSapo]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[RollingNewsId]
      ,[LastModifiedDate]
      ,[TagSubTitleId]
      ,[ZoneIdForNews]
      ,[Position]
      ,[PrPosition]
      ,[Type]
      ,[Priority]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[LocationType]
      ,[ExpiredDate]
      ,[ScheduleDate]
      ,[IsOnHome]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPosition]
go
SET IDENTITY_INSERT [NewsPosition] OFF;


----------------------NewsPositionType---------------------------------------
GO
delete [NewsPositionType]
go
SET IDENTITY_INSERT [NewsPositionType] ON;
go
INSERT INTO [NewsPositionType](
		[Id]
      ,[PositionTypeName]
      ,[FilterByZoneId]
      ,[AllowAutoUpdate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPositionType]
go
SET IDENTITY_INSERT [NewsPositionType] OFF;


----------------------NewsPublish---------------------------------------
GO
delete [NewsPublish]
go
SET IDENTITY_INSERT [NewsPublish] ON;
go
INSERT INTO [NewsPublish](
		[Id]
      ,[NewsId]
      ,[ZoneId]
      ,[Title]
      ,[SubTitle]
      ,[Sapo]
      ,[Avatar]
      ,[AvatarDesc]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Avatar4]
      ,[Avatar5]
      ,[Author]
      ,[NewsRelation]
      ,[Source]
      ,[IsFocus]
      ,[Type]
      ,[ThreadId]
      ,[DistributionDate]
      ,[Url]
      ,[DisplayStyle]
      ,[DisplayPosition]
      ,[DisplayInSlide]
      ,[AvatarCustom]
      ,[OriginalId]
      ,[PublishedDate]
      ,[IsOnHome]
      ,[TagItem]
      ,[IsPrimary]
      ,[InitSapo]
      ,[NewsType]
      ,[InterviewId]
      ,[IsBreakingNews]
      ,[OriginalUrl]
      ,[IsPr]
      ,[AdStore]
      ,[AdStoreUrl]
      ,[LastModifiedDate]
      ,[RollingNewsId]
      ,[IsOnMobile]
      ,[TagSubTitleId]
      ,[Position]
      ,[PrPosition]
      ,[Priority]
      ,[LastModifiedDateStamp]
      ,[PublishedDateStamp]
      ,[LocationType]
      ,[ExpiredDate]
      ,[DistributionID]
      ,[PrimaryZoneId]
      ,[TitleDetail]
      ,[ParentNewsId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsPublish]
go
SET IDENTITY_INSERT [NewsPublish] OFF;


----------------------NewsRelation---------------------------------------
GO
delete [NewsRelation]
go
SET IDENTITY_INSERT [NewsRelation] ON;
go
INSERT INTO [NewsRelation](
		[NewsId]
      ,[NewsRelationId]
      ,[ZoneId]
      ,[IsChange]
      ,[Priority]
      ,[Type]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsRelation]
go
SET IDENTITY_INSERT [NewsRelation] OFF;


----------------------Tag---------------------------------------
GO
delete [Tag]
go
SET IDENTITY_INSERT [Tag] ON;
go
INSERT INTO [Tag](
		[Id]
      ,[ParentId]
      ,[Name]
      ,[Description]
      ,[Url]
      ,[Invisibled]
      ,[IsHotTag]
      ,[Type]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CreatedBy]
      ,[EditedBy]
      ,[UnsignName]
      ,[IsThread]
      ,[Avatar]
      ,[Priority]
      ,[TagContent]
      ,[TagTitle]
      ,[TagInit]
      ,[TagMetaKeyword]
      ,[TagMetaContent]
      ,[TemplateId]
      ,[ViewCount]
      ,[SubTitle]
      ,[SubTile]
      ,[NewsCoverId]
      ,[Status]
      ,[CountNewsInTag]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Tag]
go
SET IDENTITY_INSERT [Tag] OFF;


----------------------TagNews---------------------------------------
GO
delete [TagNews]
go
SET IDENTITY_INSERT [TagNews] ON;
go
INSERT INTO [TagNews](
		[TagID]
      ,[NewsID]
      ,[TagMode]
      ,[TagProperty]
      ,[Priority]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TagNews]
go
SET IDENTITY_INSERT [TagNews] OFF;


----------------------TagZone---------------------------------------
GO
delete [TagZone]
go
SET IDENTITY_INSERT [TagZone] ON;
go
INSERT INTO [TagZone](
		[TagId]
      ,[ZoneId]
      ,[IsPrimary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TagZone]
go
SET IDENTITY_INSERT [TagZone] OFF;


----------------------Thread---------------------------------------
GO
delete [Thread]
go
SET IDENTITY_INSERT [Thread] ON;
go
INSERT INTO [Thread](
		[Id]
      ,[Name]
      ,[UnsignName]
      ,[Title]
      ,[Description]
      ,[Url]
      ,[Avatar]
      ,[HomeAvatar]
      ,[SpecialAvatar]
      ,[IsHot]
      ,[IsOnHome]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CreatedBy]
      ,[EditedBy]
      ,[MetaKeyword]
      ,[MetaContent]
      ,[TemplateId]
      ,[RelationThread]
      ,[RelationZone]
      ,[Ordinary]
      ,[Invisibled]
      ,[IsHightLight]
      ,[IsHightLightOnMobile]
      ,[ViewCount]
      ,[NewsCoverId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Thread]
go
SET IDENTITY_INSERT [Thread] OFF;


----------------------ThreadInZone---------------------------------------
GO
delete [ThreadInZone]
go
SET IDENTITY_INSERT [ThreadInZone] ON;
go
INSERT INTO [ThreadInZone](
		[ZoneId]
      ,[ThreadId]
      ,[CreatedDate]
      ,[IsPrimary]
      ,[Ordinary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadInZone]
go
SET IDENTITY_INSERT [ThreadInZone] OFF;


----------------------ThreadNews---------------------------------------
GO
delete [ThreadNews]
go
SET IDENTITY_INSERT [ThreadNews] ON;
go
INSERT INTO [ThreadNews](
		[ThreadId]
      ,[NewsId]
      ,[Ordinary]
      ,[LastModifiedDate]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadNews]
go
SET IDENTITY_INSERT [ThreadNews] OFF;

----------------------ThreadRelation---------------------------------------
GO
delete [ThreadRelation]
go
SET IDENTITY_INSERT [ThreadRelation] ON;
go
INSERT INTO [ThreadRelation](
		[ThreadId]
      ,[ThreadRelationId]
      ,[Ordinary]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ThreadRelation]
go
SET IDENTITY_INSERT [ThreadRelation] OFF;


----------------------Topic---------------------------------------
GO
delete [Topic]
go
SET IDENTITY_INSERT [Topic] ON;
go
INSERT INTO [Topic](
		[Id]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive]
      ,[DisplayUrl]
      ,[Description]
      ,[LogoFancyClose]
      ,[LogoTopicName]
      ,[LogoSubMenu]
      ,[DefaultViewMode]
      ,[GuideToSendMail]
      ,[TopicEmail]
      ,[isTopToolbar]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Topic]
go
SET IDENTITY_INSERT [Topic] OFF;


----------------------TopicInZone---------------------------------------
GO
delete [TopicInZone]
go
SET IDENTITY_INSERT [TopicInZone] ON;
go
INSERT INTO [TopicInZone](
		[Id]
      ,[TopicId]
      ,[ZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[TopicInZone]
go
SET IDENTITY_INSERT [TopicInZone] OFF;

----------------------NewsInTopic---------------------------------------
GO
delete [NewsInTopic]
go
SET IDENTITY_INSERT [NewsInTopic] ON;
go
INSERT INTO [NewsInTopic](
		[Id]
      ,[NewsId]
      ,[TopicId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[NewsInTopic]
go
SET IDENTITY_INSERT [NewsInTopic] OFF;


----------------------Video---------------------------------------
GO
delete [Video]
go
SET IDENTITY_INSERT [Video] ON;
go
INSERT INTO [Video](
		[Id]
      ,[ZoneId]
      ,[Name]
      ,[UnsignName]
      ,[Description]
      ,[HtmlCode]
      ,[Avatar]
      ,[KeyVideo]
      ,[Pname]
      ,[Status]
      ,[NewsId]
      ,[Views]
      ,[Mode]
      ,[Tags]
      ,[DistributionDate]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[LastModifiedBy]
      ,[LastModifiedDate]
      ,[PublishDate]
      ,[PublishBy]
      ,[EditedDate]
      ,[EditedBy]
      ,[Url]
      ,[Source]
      ,[VideoRelation]
      ,[FileName]
      ,[Duration]
      ,[Size]
      ,[Capacity]
      ,[AllowAd]
      ,[IsRemoveLogo]
      ,[OriginalUrl]
      ,[Type]
      ,[IsConverted]
      ,[AvatarShareFacebook]
      ,[Author]
      ,[OriginalId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[Video]
go
SET IDENTITY_INSERT [Video] OFF;


----------------------ZoneVideo---------------------------------------
GO
delete [ZoneVideo]
go
SET IDENTITY_INSERT [ZoneVideo] ON;
go
INSERT INTO [ZoneVideo](
		[Id]
      ,[Name]
      ,[Url]
      ,[Order]
      ,[ParentId]
      ,[Status]
      ,[CreatedDate]
      ,[ModifiedDate]
      ,[CatId]
      ,[DisplayStyle]
      ,[ShowOnHome]
      ,[Invisibled]
      ,[Keyword]
      ,[ListNewsZoneId]
      ,[Avatar]
      ,[AvatarCover]
      ,[ZoneRelation]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[ZoneVideo]
go
SET IDENTITY_INSERT [ZoneVideo] OFF;


----------------------VideoInZone---------------------------------------
GO
delete [VideoInZone]
go
SET IDENTITY_INSERT [VideoInZone] ON;
go
INSERT INTO [VideoInZone](
		[VideoId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[LastModifiedDate]
      ,[RootZoneId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[VideoInZone]
go
SET IDENTITY_INSERT [VideoInZone] OFF;


----------------------MenuExtension---------------------------------------
GO
--IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'MenuExtension')
--BEGIN
--	ALTER TABLE [MenuExtension]
--	ADD [TagId] [bigint],	
--	[OldId] [int],
--	[OldParentId] [int],
--	[OldType] [int],
--	[OldIsTopMenu] [int],
--	[OldCatId] [int];
--END
--Go
delete [MenuExtension]
go
SET IDENTITY_INSERT [MenuExtension] ON;
go
INSERT INTO [MenuExtension](
		[Id]
      ,[ParentMenuId]
      ,[GroupMenuId]
      ,[ZoneId]
      ,[ParentZoneId]
      ,[TagId]
      ,[Name]
      ,[Avatar]
      ,[Url]
      ,[Priority]
      ,[Status]
      ,[OldId]
      ,[OldParentId]
      ,[OldType]
      ,[OldIsTopMenu]
      ,[OldCatId]
    )
SELECT * FROM [IMS_AFAMILY].[dbo].[MenuExtension]
go
SET IDENTITY_INSERT [MenuExtension] OFF;