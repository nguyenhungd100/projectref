
ALTER PROCEDURE [dbo].[CMS_Thread_Search]
	@Keyword nvarchar(500),
	@IsHot int = -1,
	@ZoneId int,
	@OrderBy int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Name) > 0 OR PATINDEX(@Keyword, T.UnsignName) > 0)' 
		
	IF @IsHot = 0
		SET @where = @where + ' AND (T.IsHot = 0)' 
	ELSE IF @IsHot = 1
		SET @where = @where + ' AND (T.IsHot = 1)' 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId)' 
	ELSE
		SET @where = @where + ' AND (TZ.IsPrimary = 1 OR TZ.IsPrimary IS NULL)' 
		
	SET @order = ' ORDER BY T.CreatedDate DESC, T.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Thread AS T LEFT JOIN ThreadInZone AS TZ ON T.Id = TZ.ThreadId ' + @where + ';'
	SET @search = @search + 'SELECT Id, Name, Title, Url, Avatar, IsHot, IsOnHome, CreatedDate, CreatedBy, TemplateId, Invisibled, NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.Id, T.Name, T.Title, T.Url, T.Avatar, T.IsHot, T.IsOnHome, '
	SET @search = @search + '		T.CreatedDate, T.CreatedBy, T.TemplateId, Invisibled, '
	SET @search = @search + '		SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Thread AS T '
	SET @search = @search + '		LEFT JOIN ThreadInZone AS TZ ON T.Id = TZ.ThreadId '
	SET @search = @search + '		LEFT JOIN ThreadNews AS TN ON T.Id = TN.ThreadId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.Id, T.Name, T.Title, T.Url, T.Avatar, T.IsHot, T.IsOnHome, '
	SET @search = @search + '		T.CreatedDate, T.CreatedBy, T.TemplateId, Invisibled '
	SET @search = @search + ') AS ThreadTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	 
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END














