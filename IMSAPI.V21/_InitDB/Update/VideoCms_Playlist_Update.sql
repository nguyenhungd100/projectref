﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_Update]    Script Date: 1/12/2018 8:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update Playlist>
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_Update]
	@ZoneId int = 0,
	@Name nvarchar(200),
	@UnsignName varchar(200),
	@Description nvarchar(max) = '',
	@Status int = 0,
	@Mode int = 0,
	@Avatar varchar(300),
	@Priority int,
	@Url varchar(300),
	@VideoCount int,
	@EditedBy varchar(50) = '',
	@EditedDate datetime,	
	@LastModifiedBy varchar(50) = '',
	@LastModifiedDate datetime,	
	@DistributionDate datetime,	
	@IntroClip nvarchar(500) = '',
	@PlaylistRelation nvarchar(max) = '',
	@Cover nvarchar(300),
	@MetaJson nvarchar(max),
	@MetaAvatar nvarchar(max),
	@ZoneIdList varchar(500) = '',
	@Id int
AS
	BEGIN TRANSACTION
	
	IF NOT EXISTS (SELECT Id FROM PlayList WHERE Id = @Id)
	BEGIN
		RAISERROR ('This video playlist does not exist.', 16, 1);
		ROLLBACK TRANSACTION
	END ELSE
	BEGIN
		IF @Status = 1 AND @DistributionDate IS NULL
			SET @DistributionDate = ISNULL((SELECT DistributionDate FROM PlayList WHERE Id = @Id), GETDATE())
		-- 
		UPDATE PlayList
		SET
			ZoneId = @ZoneId,
			Name = @Name,
			UnsignName = @UnsignName,
			[Description] = @Description,
			[Status] = @Status,
			Mode = @Mode,
			Avatar = @Avatar,
			Priority = @Priority,
			VideoCount = @VideoCount,
			Url = @Url,
			EditedBy = @EditedBy,
			EditedDate = @EditedDate,
			LastModifiedBy = @LastModifiedBy,
			LastModifiedDate = @LastModifiedDate,
			LastInsertVideoDate = GETDATE(),			
			DistributionDate = @DistributionDate,
			@IntroClip=@IntroClip,
			PlaylistRelation=PlaylistRelation,
			Cover=@Cover,
			MetaJson=@MetaJson,
			MetaAvatar=@MetaAvatar
		WHERE Id = @Id

		DECLARE @Index int;
						
			-- DELETE ALL PlaylistInZone of this PlaylistId
			DELETE FROM PlaylistInZone  WHERE PlaylistId = @Id			
			-- Process PlaylistInZone					
			INSERT INTO PlaylistInZone(ZoneId, PlaylistId, LastModifiedDate, IsPrimary)
			VALUES (@ZoneId, @Id, @LastModifiedDate, 1)
			
			DECLARE @TblPlaylistInZone TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblPlaylistInZone(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblPlaylistInZone)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblPlaylistInZone WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT PlaylistId  FROM PlaylistInZone WHERE PlaylistId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO PlaylistInZone(ZoneId, PlaylistId, LastModifiedDate, IsPrimary)
						VALUES (@ZoneIdTemp, @Id, @LastModifiedDate, 0)
					END
					SET @Index = @Index + 1;
				END
			END						
	END
	
	IF @@ERROR <> 0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
















