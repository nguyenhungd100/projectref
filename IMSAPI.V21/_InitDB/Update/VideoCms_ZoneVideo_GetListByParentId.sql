USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_GetListByParentId]    Script Date: 01/09/2018 11:44:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListByParentId] ---1,-1
	@ParentId int = 0,
	@Status int 
AS
	SELECT Id, Name, Url, [Order], ParentId, Status, CreatedDate, ModifiedDate, CatId, (SELECT COUNT(*) FROM ZoneVideo WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZoneVideo AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status < 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]

















