USE [IMS2_WEBTHETHAO]
GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetListOfZoneIdByNewsId]    Script Date: 05/10/2018 09:33:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<ThanhTN>
-- Edit: <chinhnb> <10/05/2018>
-- Create date: <2012-09-28>
-- Description:	<Get list of zone id by news id>
-- =============================================
ALTER FUNCTION [dbo].[CMS_fGetListOfZoneIdByNewsId]
(
	@NewsId bigint
)
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @ZoneIds varchar(4000),
			@ZoneId int

	SET @ZoneIds = ''
	
	DECLARE ZoneCursor CURSOR FOR
	SELECT ZoneId
	FROM NewsInZone
	WHERE (NewsId = @NewsId)
	ORDER BY IsPrimary desc

	OPEN ZoneCursor
	
	FETCH NEXT FROM ZoneCursor INTO @ZoneId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		SET @ZoneIds = @ZoneIds + ',' + CONVERT(varchar(10), @ZoneId)
			
		FETCH NEXT FROM ZoneCursor INTO @ZoneId 
	END   

	CLOSE ZoneCursor   
	DEALLOCATE ZoneCursor

	IF @ZoneIds <> ''
		SET @ZoneIds = SUBSTRING(@ZoneIds, 2, LEN(@ZoneIds) - 1)

	RETURN @ZoneIds
END












