﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Video_CountVideoNotPublishByListPlaylist]    Script Date: 1/12/2018 5:59:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<DungDT>
-- Create date: <2018-01-12>
-- Description:	<count video not publish in playlist>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Video_CountVideoNotPublishByListPlaylist]
	@PlaylitIds Varchar(1000)
AS
	SELECT DISTINCT vp.PlaylistId, 
		(SELECT COUNT(Id) from Video WHERE Id IN (SELECT VideoId FROM videoplaylist WHERE PlaylistId = vp.PlaylistId) and Status <> 1) as VideoNotPublishCount
FROM VideoPlaylist vp
WHERE vp.PlaylistId IN (SELECT * FROM SplitStrings(@PlaylitIds))













