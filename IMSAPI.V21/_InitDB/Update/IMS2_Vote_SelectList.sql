USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Vote_SelectList]    Script Date: 10/16/2017 16:36:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		Doan Van
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Vote_SelectList]
	-- Add the parameters for the stored procedure here
	@Keyword	NVARCHAR(100),
	@ZoneId	INT,
	@PageIndex	INT,
	@PageSize	INT,
	@IsGetTotal	BIT,
	@TotalRow	INT	OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    
    -- Insert statements for procedure here

  


 IF @PageIndex =1
 
            SELECT TOP(@PageSize)  
                Id,
				Title	,
				Avatar	,
				Sapo	,
				CreatedDate	,
				ModifiedDate,
				Status	,
				Author	,
				CreatedBy	,
				LastModifiedBy	,
				Priority	,
				DisplayPosition	,
				DisplayStyle	,
				ViewCount	,
				RateCount	,
				MaxAnswers	,
				ShowInZone,
				ShowInFooter,
				StartedDate	,
				EndedDate	
				FROM	Vote
					INNER JOIN VoteInZone ON Vote.Id = VoteInZone.VoteId
				WHERE	(@Keyword = '' OR Title LIKE '%' + @Keyword + '%' )
					AND ((@ZoneId <= 0 AND VoteInZone.IsPrimary = 1) OR (@ZoneId > 0 AND VoteInZone.ZoneId = @ZoneId))
				ORDER	BY	CreatedDate	DESC
ELSE
 SELECT	*
	FROM	(	SELECT	
				Id,
				Title	,
				Avatar	,
				Sapo	,
				CreatedDate	,
				ModifiedDate,
				Status	,
				Author	,
				CreatedBy	,
				LastModifiedBy	,
				Priority	,
				DisplayPosition	,
				DisplayStyle	,
				ViewCount	,
				RateCount	,
				MaxAnswers	,
				ShowInZone,
				ShowInFooter,
				StartedDate	,
				EndedDate	,
						ROW_NUMBER()	OVER	(ORDER	BY	CreatedDate	DESC)	Row
				FROM	Vote
					INNER JOIN VoteInZone ON Vote.Id = VoteInZone.VoteId
				WHERE	(@Keyword = '' OR Title LIKE '%' + @Keyword + '%')
					AND ((@ZoneId <= 0 AND VoteInZone.IsPrimary = 1) OR (@ZoneId > 0 AND VoteInZone.ZoneId = @ZoneId))
			)	AS	Temp
	WHERE	Row	BETWEEN	((@PageIndex - 1)*@PageSize + 1)	AND	@PageIndex*@PageSize ORDER	BY	CreatedDate	DESC

SELECT	@TotalRow = COUNT(Id)FROM Vote  INNER JOIN VoteInZone ON Vote.Id = VoteInZone.VoteId WHERE	(@Keyword = '' OR Title LIKE '%' + @Keyword + '%')
	AND ((@ZoneId <= 0 AND VoteInZone.IsPrimary = 1) OR (@ZoneId > 0 AND VoteInZone.ZoneId = @ZoneId))

	
	
	
	
END















