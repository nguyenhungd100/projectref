USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ProgramChannel_Search]    Script Date: 1/2/2018 8:10:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<DungDT>
-- Create date: <2018-01-02>
-- Description:	<search album>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_ProgramChannel_Search]
	@Keyword nvarchar(300),
	@Status int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		--SET @where = @where + ' AND (Name LIKE @Keyword OR FullName LIKE @Keyword)'
		SET @where = @where + ' AND (PATINDEX(@Keyword, Name) > 0 OR PATINDEX(@Keyword, FullName) > 0)'
		
	END
		
	IF @Status > -1
		SET @where = @where + ' AND (Status = @Status)'	
		
	SET @search =			' SELECT @TotalRow = COUNT(*) FROM ProgramChannel ' + @where + ';'
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT *, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY Priority ASC) AS RowNum '
	SET @search = @search + '	FROM ProgramChannel '
	SET @search = @search + @where
	SET @search = @search + ' ) AS ProgramChannelData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@Status,								
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END
















