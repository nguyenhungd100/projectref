﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ProgramChannel_Insert]    Script Date: 1/2/2018 6:57:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[CMS_ProgramChannel_Insert]
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@CreatedBy nvarchar(50),
	@Priority int,
	@Status int,
	@Type int,
	@LiveTvThumb nvarchar(200),
	@LiveTvEmbed nvarchar(200),
	@FullName nvarchar(300),
	@Id int out
AS
BEGIN
	INSERT INTO ProgramChannel (Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Priority, Status, Type, LiveTvThumb, LiveTvEmbed, FullName)
	VALUES (@Name,@Avatar,@CreatedBy, GETDATE(),@CreatedBy, GETDATE(),@Priority,@Status,@Type, @LiveTvThumb, @LiveTvEmbed, @FullName)
	
	SET @Id = SCOPE_IDENTITY()
END
-----------------------











