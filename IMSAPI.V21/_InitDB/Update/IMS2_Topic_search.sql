﻿USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Search]    Script Date: 10/16/2017 14:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_Topic_Search]
	@Keyword nvarchar(500),
	@IsHot int = -1,
	@ZoneId int,
	@OrderBy int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.TopicName) > 0)' 		 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId)' 
		
	SET @order = ' ORDER BY T.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Topic AS T LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId ' + @where + ';'
	SET @search = @search + 'SELECT [Id]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive], NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.[Id]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive], '
	SET @search = @search + '		SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Topic AS T '
	SET @search = @search + '		LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId '
	SET @search = @search + '		LEFT JOIN NewsInTopic AS TN ON T.Id = TN.TopicId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.[Id]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive] '
	SET @search = @search + ') AS TopicTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	-- print(@search)
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END



