﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ProgramChannel_GetbyId]    Script Date: 1/2/2018 8:00:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[CMS_ProgramChannel_GetbyId]
	@Id int
AS
BEGIN
	SELECT Id, Name, Avatar, CreatedBy, CreatedDate, LastModifiedBy, LastModifiedDate, Priority, Status, Type, ZoneVideoId, LiveTvThumb, LiveTvEmbed,FullName
	FROM ProgramChannel
	WHERE Id = @Id
END
-------------------------












