USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsById]    Script Date: 10/18/2017 20:01:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author	  :	<ThanhTN>
-- Create date: <2012-09-17>
-- Description:	<Search news for news relation>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_GetNewsById] @Id BIGINT
AS
    BEGIN
        SELECT  Id ,
                Title ,
                SubTitle ,
                Sapo ,
                Body ,
                Avatar ,
                AvatarDesc ,
                Avatar2 ,
                Avatar3 ,
                Avatar4 ,
                Avatar5 ,
                AvatarCustom ,
                DisplayInSlide ,
                DisplayPosition ,
                DisplayStyle ,
                Author ,
                NewsRelation ,
                [Status] ,
                [Source] ,
                IsFocus ,
                [Type] ,
                ThreadId ,
                CreatedDate ,
                LastModifiedDate ,
                DistributionDate ,
                CreatedBy ,
                LastModifiedBy ,
                PublishedBy ,
                EditedBy ,
                LastReceiver ,
                WordCount ,
                ViewCount ,
                Priority ,
                dbo.CMS_fGetListOfZoneIdByNewsId(@Id) AS ListZoneId ,
                Tag ,
                Note ,
                IsOnHome ,
                NewsType ,
                OriginalId ,
                Price ,
                Url ,
                TagItem ,
                NoteRoyalties ,
                NewsCategory ,
                InitSapo ,
                TemplateName ,
                TemplateConfig ,
                IsBreakingNews ,
                OriginalUrl ,
                IsPr ,
                AdStore ,
                AdStoreUrl ,
                PrBookingNumber ,
                LocationType ,
                ExpiredDate ,
                BonusPrice,
                SourceUrl,
                TagSubTitleId,
				ShortTitle, 
				PegaBreakingNews,
				ApprovedBy,
				ApprovedDate,
				ReturnedBy,
				SentBy,
				ErrorCheckedBy,
				ErrorCheckedDate,
				SensitiveCheckedBy,
				SensitiveCheckedDate
        FROM    News
        WHERE   ( Id = @Id );
    END;










