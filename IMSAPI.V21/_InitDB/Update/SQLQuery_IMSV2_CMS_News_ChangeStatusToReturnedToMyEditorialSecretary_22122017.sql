USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToReturnedToEditorialSecretary]    Script Date: 12/22/2017 11:09:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_News_ChangeStatusToReturnedToMyEditorialSecretary]
	@Id bigint,
	@ApprovedBy varchar(200),
	@UserName varchar(200),
	@Note nvarchar(2000) = ''
AS
BEGIN
	
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 20, 
			ApprovedBy=@ApprovedBy,
			ReturnedBy=@UserName,
			LastModifiedDate = GETDATE(),
			Note = CASE @Note WHEN '' THEN Note ELSE @Note END
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

