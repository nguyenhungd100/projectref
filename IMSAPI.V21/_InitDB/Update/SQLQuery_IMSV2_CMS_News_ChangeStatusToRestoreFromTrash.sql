USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToRestoreFromTrash]    Script Date: 10/13/2017 14:29:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-28>
-- Description:	<restore news from trash>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToRestoreFromTrash]
	@Id bigint,
	@LastModidiedBy nvarchar(200)
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		DECLARE @ApprovedBy varchar(200),
				@EditedBy varchar(200)
				
		SELECT @ApprovedBy = ApprovedBy, @EditedBy = EditedBy FROM News WHERE Id = @Id
	
		IF @ApprovedBy <> '' AND @ApprovedBy LIKE @LastModidiedBy
		BEGIN
			UPDATE News
			SET [Status] = 6, LastModifiedDate = GETDATE()
			WHERE Id = @Id
		END
		ELSE
		BEGIN
			IF @EditedBy <> '' AND @EditedBy LIKE @LastModidiedBy
			BEGIN
				UPDATE News
				SET [Status] = 3, LastModifiedDate = GETDATE()
				WHERE Id = @Id
			END
			ELSE
			BEGIN
				UPDATE News
				SET [Status] = 1, LastModifiedDate = GETDATE()
				WHERE Id = @Id
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

















