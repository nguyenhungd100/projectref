USE [IMS_VNE]
GO
/****** Object:  UserDefinedFunction [dbo].[CMS_fGetNewsPublishForNewsPosition]    Script Date: 10/13/2017 10:55:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-03-27>
-- Description:	<Get list newspublish for newsposition>
-- =============================================
ALTER FUNCTION [dbo].[CMS_fGetNewsPublishForNewsPosition]
(	
	@TopNews int,
	@ZoneId int,
	@TypeId int,
	@ExcludeTypeIds varchar(MAX),
	@IsOnHome int = -1,
	@IsIncludeChildZoneId int = 1,
	@ExcludeOrder varchar(MAX) = '',
	@DisplayInSlide int = -1,
	@IsBreakingNews int = -1,
	@DisplayPosition int = 0,
	@IsFocus int = -1,
	@IncludeBomdNews bit = 0
)
RETURNS @ReturnTable TABLE 
(
	ZoneId int,
	TypeId int,
	[Order] int,
	NewsId bigint,
	Title nvarchar(250),
	SubTitle nvarchar(250),
	Sapo nvarchar(MAX),
	Avatar nvarchar(500),
	AvatarDesc nvarchar(500),
	Avatar1 nvarchar(500),
	Avatar2 nvarchar(500),
	Avatar3 nvarchar(500),
	Avatar4 nvarchar(500),
	Avatar5 nvarchar(500),
	Author nvarchar(500),
	NewsRelation nvarchar(MAX),
	Source nvarchar(100),
	IsFocus bit,
	NewsType tinyint,
	ThreadId int,
	DistributionDate datetime,
	Url nvarchar(500),
	DisplayStyle int,
	DisplayPosition int,
	DisplayInSlide int,
	AvatarCustom varchar(500),
	OriginalId int,
	InitSapo nvarchar(500),
	InterviewId int,
	IsBreakingNews bit,
	OriginalUrl nvarchar(500),
	IsPr bit,
	AdStore bit,
	AdStoreUrl nvarchar(500),
	RollingNewsId int,
	TagSubTitleId int,
	Position int,
	PrPosition int,
	Type int,
	Priority int,
	PublishedDateStamp bigint,
	LocationType int,
	ExpiredDate datetime,
	IsOnHome bit
)
AS
BEGIN
	DECLARE @IsFilterByZoneId tinyint
	SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), -1)
	
	DECLARE @LockPositionCount int
	DECLARE @ExcludeNewsIdTable TABLE (NewsId bigint)
	DECLARE @ZoneIdIncludeChildId TABLE (ZoneId bigint)
	
	IF @ExcludeTypeIds <> ''
		SET @ExcludeTypeIds = ',' + @ExcludeTypeIds + ','
	
	IF @ExcludeOrder <> ''
		SET @ExcludeOrder = ',' + @ExcludeOrder + ','
	
	-- Có type
	IF @IsFilterByZoneId > -1
	BEGIN
		IF @IsFilterByZoneId = 0 -- No filter
		BEGIN
			SET @ZoneId = 0
			
			SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND AllowAutoUpdate = 0)
			SET @TopNews = @TopNews - @LockPositionCount
			
			INSERT INTO @ExcludeNewsIdTable (NewsId)
			SELECT NewsId 
			FROM NewsPosition 
			WHERE (PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0 AND 
					(@ExcludeOrder = '' OR (@ExcludeOrder <> '' AND PATINDEX('%,' + CONVERT(varchar(20), [Order]) + ',%', @ExcludeOrder) > 0)))
				OR (TypeId = @TypeId AND AllowAutoUpdate = 0)
		
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], 
				NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
				Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
				NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome)
			SELECT TOP(@TopNews) ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], 
				NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
				Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
				NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome
			FROM
			(
				SELECT ZoneId, NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
					Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
					NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
					DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
					InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
					RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
					PublishedDateStamp, LocationType, ExpiredDate, IsOnHome
				FROM NewsPublish
				WHERE IsPrimary = 1 
					AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
					AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
					AND (@DisplayInSlide = -1 OR (@DisplayInSlide > -1 AND DisplayInSlide = @DisplayInSlide))
					AND (@IsBreakingNews = -1 OR (@IsBreakingNews > -1 AND IsBreakingNews = @IsBreakingNews))
					AND (@IsFocus = -1 OR (@IsFocus > -1 AND IsFocus = @IsFocus))
					AND (@DisplayPosition = 0 OR (@DisplayPosition > 0 AND DisplayPosition = @DisplayPosition))
					AND (@IncludeBomdNews = 1 OR (@IncludeBomdNews = 0 AND DistributionDate <= GETDATE()))
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		ELSE
		BEGIN
			SELECT @LockPositionCount = COUNT(*) FROM NewsPosition WHERE (TypeId = @TypeId AND ZoneId = @ZoneId AND AllowAutoUpdate = 0)
			SET @TopNews = @TopNews - @LockPositionCount
			
			INSERT INTO @ExcludeNewsIdTable (NewsId)
			SELECT NewsId 
			FROM NewsPosition 
			WHERE ZoneId = @ZoneId 
				AND ((PATINDEX('%,' + CONVERT(varchar(20), TypeId) + ',%', @ExcludeTypeIds) > 0 AND 
					(@ExcludeOrder = '' OR (@ExcludeOrder <> '' AND PATINDEX('%,' + CONVERT(varchar(20), [Order]) + ',%', @ExcludeOrder) > 0)))
				OR (TypeId = @TypeId AND AllowAutoUpdate = 0))
					
			IF @IsIncludeChildZoneId = 1
				INSERT INTO @ZoneIdIncludeChildId (ZoneId)
				SELECT Id 
				FROM Zone 
				WHERE Id = @ZoneId OR ParentId = @ZoneId
			ELSE
				INSERT INTO @ZoneIdIncludeChildId (ZoneId)
				SELECT Id 
				FROM Zone 
				WHERE Id = @ZoneId
		
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], 
				NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
				Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
				NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome)
			SELECT TOP(@TopNews) ZoneId, @TypeId, ROW_NUMBER() OVER (ORDER BY DistributionDate DESC) AS [Order], 
				NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
				Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
				NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
				DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
				InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
				PublishedDateStamp, LocationType, ExpiredDate, IsOnHome
			FROM
			(
				SELECT DISTINCT ZoneId, NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, 
					Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, IsFocus, 
					NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
					DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
					InterviewId, IsBreakingNews, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
					RollingNewsId, TagSubTitleId, Position, PrPosition, Type, Priority, 
					PublishedDateStamp, LocationType, ExpiredDate, IsOnHome
				FROM NewsPublish
				WHERE ZoneId IN (SELECT ZoneId FROM @ZoneIdIncludeChildId)
					AND NewsId NOT IN (SELECT NewsId FROM @ExcludeNewsIdTable)
					AND (@IsOnHome = -1 OR (@IsOnHome > -1 AND IsOnHome = @IsOnHome))
					AND (@DisplayInSlide = -1 OR (@DisplayInSlide > -1 AND DisplayInSlide = @DisplayInSlide))
					AND (@IsBreakingNews = -1 OR (@IsBreakingNews > -1 AND IsBreakingNews = @IsBreakingNews))
					AND (@IsFocus = -1 OR (@IsFocus > -1 AND IsFocus = @IsFocus))
					AND (@DisplayPosition = 0 OR (@DisplayPosition > 0 AND DisplayPosition = @DisplayPosition))
					AND (@IncludeBomdNews = 1 OR (@IncludeBomdNews = 0 AND DistributionDate <= GETDATE()))
			) AS NewsData
			ORDER BY DistributionDate DESC
		END
		
		DECLARE @Index int
		SET @Index = (SELECT COUNT(*) FROM @ReturnTable)
		WHILE(@Index < @TopNews) 
		BEGIN
			SET @Index = @Index + 1
			
			INSERT INTO @ReturnTable (ZoneId, TypeId, [Order], NewsId, Title, SubTitle, DistributionDate, Sapo, 
				Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
				NewsRelation, Source, IsFocus, ThreadId, Url, OriginalId, InitSapo, OriginalUrl)
			VALUES (@ZoneId, @TypeId, @Index, 0, null, null, null, null,
				null, null, null, null, null, null, null, null,
				null, null, 0, 0, null, 0, null, null)
		END
		
		DECLARE @LockPosition int
		DECLARE PositionCursor CURSOR FOR
		SELECT [Order]
		FROM NewsPosition
		WHERE (TypeId = @TypeId AND (@IsFilterByZoneId <> 1 OR (@IsFilterByZoneId = 1 AND ZoneId = @ZoneId)) AND AllowAutoUpdate = 0)
		ORDER BY [Order]
		
		OPEN PositionCursor
	
		FETCH NEXT FROM PositionCursor INTO @LockPosition
		WHILE @@FETCH_STATUS = 0   
		BEGIN
			UPDATE @ReturnTable
			SET [Order] = [Order] + 1
			WHERE [Order] >= @LockPosition
				
			FETCH NEXT FROM PositionCursor INTO @LockPosition 
		END   

		CLOSE PositionCursor   
		DEALLOCATE PositionCursor
	END
		
	RETURN
END
















