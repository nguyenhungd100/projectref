﻿USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[nodejs_nativeapp_vtv_dungdt_Video_GetListVideoByZone]    Script Date: 12/28/2017 09:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[nodejs_nativeapp_vtv_dungdt_Video_GetListVideoByZone] 
	@PageIndex int,
	@PageSize int,
	@ZoneId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @StartRow INT;
	DECLARE @EndRow INT;
	SET @StartRow = (@PageIndex - 1) * @PageSize + 1;
	SET @EndRow = @PageIndex * @PageSize;
	
	IF @ZoneId = 0
	BEGIN
		SELECT * FROM
			(
				SELECT TOP(@EndRow)
					v.Id,
					v.ZoneId,
					v.Name,
					v.UnsignName,
					v.Description,
					v.Avatar,
					v.DistributionDate,
					v.KeyVideo,
					v.Url,
					v.Tags,
					v.Views,
					v.Duration,
					v.FileName,
					ROW_NUMBER() OVER(ORDER BY v.DistributionDate DESC) RowIndex
				FROM Video v
				INNER JOIN VideoInZone viz ON v.Id = viz.VideoId			
				WHERE 	
					viz.IsPrimary = 1
					AND viz.ZoneId > 0		
					AND v.[Status] = 1
					and v.DistributionDate <= GETDATE()
			) indexTable
		WHERE RowIndex BETWEEN @StartRow AND @EndRow
	END
	ELSE
	BEGIN
		SELECT * FROM
			(
				SELECT TOP(@EndRow)
					v.Id,
					v.ZoneId,
					v.Name,
					v.UnsignName,
					v.Description,
					v.Avatar,
					v.DistributionDate,
					v.KeyVideo,
					v.Url,
					v.Tags,
					v.Views,
					v.Duration,
					v.FileName,
					ROW_NUMBER() OVER(ORDER BY v.DistributionDate DESC) RowIndex
				FROM Video v
				INNER JOIN VideoInZone viz ON v.Id = viz.VideoId			
				WHERE 	
					viz.ZoneId = @ZoneId		
					AND v.[Status] = 1
					and v.DistributionDate <= GETDATE()
			) indexTable
		WHERE RowIndex BETWEEN @StartRow AND @EndRow
	END
END
