﻿USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_StatisticNewsBySourceId]    Script Date: 11/13/2017 15:57:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_News_StatisticNewsBySourceId]
    @SourceId INT ,
    @FromDate DATETIME ,
    @ToDate DATETIME
AS 
    SELECT  *
    FROM    ( SELECT    CONVERT (NVARCHAR(20), a.DistributionDate, 103) AS ValuesDate ,
                        COUNT(0) AS ValuesData
              FROM      news a
                        INNER JOIN dbo.NewsBySource b ON a.id = b.NewsId
                                                         AND b.SourceId = @SourceId
                                                         AND a.Status=8
              WHERE     a.DistributionDate >= @FromDate
                        AND a.DistributionDate <= @ToDate
              GROUP BY  CONVERT (NVARCHAR(20), a.DistributionDate, 103)
            ) tmp
    ORDER BY CONVERT(DATETIME, ValuesDate, 103) ASC
