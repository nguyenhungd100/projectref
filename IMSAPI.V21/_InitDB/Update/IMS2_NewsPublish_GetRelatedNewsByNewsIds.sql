USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_GetRelatedNewsByNewsIds]    Script Date: 10/19/2017 14:51:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-28>
-- Description:	<get related news by list of news id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPublish_GetRelatedNewsByNewsIds]
	@NewsIds varchar(MAX) = ''
AS
BEGIN
	IF @NewsIds <> ''
	BEGIN
		DECLARE @NewsTemp TABLE(NewsId bigint)
		INSERT INTO @NewsTemp (NewsId)
		SELECT CONVERT(bigint, Part) FROM SplitString(@NewsIds, ';');

		SELECT NP.NewsId, NP.ZoneId, NP.Title, NP.Url, NP.DistributionDate, NP.Sapo, NP.Avatar, NP.Author
		FROM NewsPublish AS NP 
			INNER JOIN @NewsTemp AS NT ON NP.NewsId = NT.NewsId
		WHERE NP.IsPrimary=1
		ORDER BY NP.DistributionDate DESC
	END
END

