--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Expert_Update]    Script Date: 4/1/2019 11:00:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb edit:28-03-2019
ALTER PROCEDURE [dbo].[CMS_News_Expert_Update]
	@NewsId bigint,
	@Body ntext,
	@Tag nvarchar(max),
	@TagIdList varchar(500) = '',
	@TagItem nvarchar(1000),
	@MetaTitle nvarchar(300),
	@MetaKeyword nvarchar(300),
	@MetaDescription nvarchar(300),
	@ReviewStatus int=0,
	@ModifiedBy varchar(100) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		-- Update News
		UPDATE News
		SET Body = @Body,
			TagItem = @TagItem,
			Tag = @Tag
		WHERE Id = @NewsId
		-- Update NewsPublish
		UPDATE NewsPublish SET TagItem = @TagItem, LastModifiedDate = GETDATE() WHERE NewsId = @NewsId
		-- Update NewsContent
		UPDATE NewsContent SET Body = @Body,Tags = @Tag,TagItem = @TagItem WHERE NewsId = @NewsId

		SET NOCOUNT ON
		
		DECLARE @Index int;
		SET @Index=0
		DELETE FROM TagNews WHERE (TagMode = 0 OR TagMode = 3) AND NewsID = @NewsId
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @NewsId AND (TagMode = 0 or TagMode = 3))
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@NewsId, @TagItemId, 0, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemId AND NewsId = @NewsId AND (TagMode = 0 or TagMode = 3)
				SET @Index = @Index + 1
			END
		END
		
		
		--Update Seo
		UPDATE  SEOMetaNews SET
							MetaTitle =@MetaTitle,
							MetaDescription =@MetaDescription,
							KeywordFocus =@MetaKeyword,
							ReviewStatus=@ReviewStatus,
							ModifiedBy=@ModifiedBy
		WHERE NewsId =@NewsId
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_FULL]
