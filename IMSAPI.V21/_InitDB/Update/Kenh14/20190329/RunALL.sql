﻿--alter table SeoMetaNews
--add ReviewStatus int null

GO
alter table SeoMetaNews
add ModifiedBy varchar(100) null,ReviewedBy varchar(100) null,ReviewedDate datetime null

--USE [IMS2_TTVH]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Expert_Update]    Script Date: 3/28/2019 2:26:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb edit:28-03-2019
ALTER PROCEDURE [dbo].[CMS_News_Expert_Update]
	@NewsId bigint,
	@Body ntext,
	@Tag nvarchar(max),
	@TagIdList varchar(500) = '',
	@TagItem nvarchar(1000),
	@MetaTitle nvarchar(300),
	@MetaKeyword nvarchar(300),
	@MetaDescription nvarchar(300),
	@ReviewStatus int=0,
	@ModifiedBy varchar(100) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		-- Update News
		UPDATE News
		SET Body = @Body,
			TagItem = @TagItem,
			Tag = @Tag
		WHERE Id = @NewsId
		-- Update NewsPublish
		UPDATE NewsPublish SET TagItem = @TagItem, LastModifiedDate = GETDATE() WHERE NewsId = @NewsId
		-- Update NewsContent
		UPDATE NewsContent SET Body = @Body,Tags = @Tag,TagItem = @TagItem WHERE NewsId = @NewsId

		SET NOCOUNT ON
		
		DECLARE @Index int;
		SET @Index=0
		DELETE FROM TagNews WHERE (TagMode = 0 OR TagMode = 3) AND NewsID = @NewsId
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @NewsId AND (TagMode = 0 or TagMode = 3))
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@NewsId, @TagItemId, 0, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemId AND NewsId = @NewsId AND (TagMode = 0 or TagMode = 3)
				SET @Index = @Index + 1
			END
		END
		
		
		--Update Seo
		UPDATE  SEOMetaNews SET
							MetaTitle =@MetaTitle,
							MetaDescription =@MetaDescription,
							KeywordFocus =@MetaKeyword,
							ReviewStatus=@ReviewStatus,
							ModifiedBy=@ModifiedBy
		WHERE NewsId =@NewsId
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateIsFocus]    Script Date: 3/29/2019 9:15:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 
alter PROCEDURE [dbo].[CMS_SeoMetaNews_UpdateReviewStatus]
	@Id bigint,
	@ReviewStatus int,
	@ReviewedBy varchar(100)=''
AS
BEGIN	
	UPDATE SeoMetaNews SET 
		ReviewStatus = @ReviewStatus,
		ReviewedBy=@ReviewedBy,
		ReviewedDate=getdate()
	WHERE NewsId = @Id
END


--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaNews_GetById]    Script Date: 4/1/2019 2:16:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_GetById]
@NewsId bigint
AS

BEGIN
	SELECT * FROM SEOMetaNews WHERE NewsId = @NewsId
END


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_GetByName]    Script Date: 4/2/2019 10:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <02042019>
-- Description:	<Get tag by name or url>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Tag_GetByNameOrUrl] --N'tiệc thác loan',''
	@Name nvarchar(250),
	@Url varchar(500)=''
AS
BEGIN
	SELECT top(1) Id, ParentId, Name, Description, Url, Invisibled, IsHotTag, Type, CreatedDate, ModifiedDate, CreatedBy, EditedBy, UnsignName, IsThread, Avatar, Priority
	FROM Tag
	WHERE LOWER(Name) = LOWER(@Name) OR [Url]=@Url
END
