--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateIsFocus]    Script Date: 3/29/2019 9:15:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 
CREATE PROCEDURE [dbo].[CMS_SeoMetaNews_UpdateReviewStatus]
	@Id bigint,
	@ReviewStatus int,
	@ReviewedBy varchar(100)=''
AS
BEGIN	
	UPDATE SeoMetaNews SET 
		ReviewStatus = @ReviewStatus,
		ReviewedBy=@ReviewedBy,
		ReviewedDate=getdate()
	WHERE NewsId = @Id
END















