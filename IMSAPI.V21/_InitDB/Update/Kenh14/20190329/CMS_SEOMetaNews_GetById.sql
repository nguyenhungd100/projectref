--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaNews_GetById]    Script Date: 4/1/2019 2:16:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_GetById]
@NewsId bigint
AS

BEGIN
	SELECT * FROM SEOMetaNews WHERE NewsId = @NewsId
END
