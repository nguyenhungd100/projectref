--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_GetByName]    Script Date: 4/2/2019 10:35:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <02042019>
-- Description:	<Get tag by name or url>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Tag_GetByNameOrUrl] --N'tiệc thác loan',''
	@Name nvarchar(250),
	@Url varchar(500)=''
AS
BEGIN
	SELECT top(1) Id, ParentId, Name, Description, Url, Invisibled, IsHotTag, Type, CreatedDate, ModifiedDate, CreatedBy, EditedBy, UnsignName, IsThread, Avatar, Priority
	FROM Tag
	WHERE LOWER(Name) = LOWER(@Name) OR [Url]=@Url
END


















