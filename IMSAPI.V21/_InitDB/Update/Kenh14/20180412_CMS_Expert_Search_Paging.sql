USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search]    Script Date: 04/12/2018 16:08:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
CREATE PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
    BEGIN
        SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                  FROM      Expert
                  WHERE     ExpertName LIKE '%' + @Keyword + '%'
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
	
    END







