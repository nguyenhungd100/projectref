USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BrandContent_Search]    Script Date: 03/12/2018 10:45:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_BrandContent_Search]
	@Keyword nvarchar(500),
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
As
	IF(@Keyword <> '')
		SET @Keyword = '%' + @Keyword + '%' ;
	
	Select @TotalRow=count(*) FROM BrandContent WHERE (@Keyword = ''  OR PATINDEX(@Keyword, BrandName) > 0)
			
	SELECT * FROM (
		SELECT [BrandId]
			  ,[BrandName]
			  ,[BrandUrl]
			  ,[Icon]
			  ,[Logo]
			  ,[LogoStream]
			  ,ROW_NUMBER() OVER (ORDER BY BrandId DESC) AS RowNum
		FROM BrandContent
		WHERE (@Keyword = ''  OR PATINDEX(@Keyword, BrandName) > 0)
	)AS B WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)


