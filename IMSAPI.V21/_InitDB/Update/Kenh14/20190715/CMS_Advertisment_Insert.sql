--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Advertisment_Insert]    Script Date: 8/10/2019 9:45:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--10/08/2019
CREATE PROCEDURE [dbo].[CMS_Advertisment_Insert]
	@Title nvarchar(200),
	@TargetUrl nvarchar(300),
	@AdCode nvarchar(500),
	@Note nvarchar(500),
	@Priority int = 0,
	@TypeId int = 0,
	@ZoneId int = 0,
	@Status int,
	@DisplayStyle int = 0,
	@PositionId int = 0,
	@StartDate datetime,
	@EndDate datetime,	
	@ZoneIdList varchar(500) = '',
	@Width int = 0,
	@Height int = 0
AS
BEGIN
	INSERT INTO Advertisment(	
		Title,
		TargetUrl,
		AdCode,
		Note,
		Priority,
		TypeId,
		ZoneId,
		DisplayStyle,		
		PositionId,
		[Status],
		StartDate,
		EndDate,
		Width ,
		Height
	)
	VALUES( 
		@Title,
		@TargetUrl,
		@AdCode,
		@Note,
		@Priority,
		@TypeId,
		@ZoneId,
		@DisplayStyle,
		@PositionId,
		@Status,
		@StartDate,
		@EndDate,
		@Width ,
		@Height
		)	
	DECLARE @Id int	
	SET @Id = SCOPE_IDENTITY()
	
		DECLARE @Index int;
		DELETE FROM AdvertismentInZone  WHERE AdvertismentId = @Id
		
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > -1 --AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM AdvertismentInZone WHERE ZoneId = @ZoneItemId AND AdvertismentId = @Id)
						INSERT INTO AdvertismentInZone(AdvertismentId, ZoneId) VALUES (@Id, @ZoneItemId);
				SET @Index = @Index + 1
			END
		END
END

