--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Delete]    Script Date: 7/17/2019 10:23:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Delete]
	@Id int
AS
BEGIN
	DELETE FROM LandingTemplate WHERE (Id = @Id)
END

