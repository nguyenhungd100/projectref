--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Insert]    Script Date: 7/17/2019 10:29:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Insert]
	@Id int out,
	@Name nvarchar(200),
	@Html ntext,
	@Avatar nvarchar(500),	
	@Type int,
	@CategoryId int,
	@Status int,
	@CreatedBy varchar(100)
AS
BEGIN
	INSERT INTO LandingTemplate(Name, Html, Avatar, Type, CategoryId, Status, CreatedBy, CreatedDate)
	VALUES (@Name,@Html, @Avatar,@Type,@CategoryId,@Status,@CreatedBy,GETDATE())
	
	SET @Id = SCOPE_IDENTITY()
END

