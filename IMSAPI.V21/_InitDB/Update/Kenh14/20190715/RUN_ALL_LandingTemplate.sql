--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[LandingTemplate]    Script Date: 7/17/2019 11:02:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LandingTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Html] [ntext] NULL,
	[Avatar] [nvarchar](500) NULL,
	[Type] [int] NULL,
	[CategoryId] [int] NULL,
	[Status] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_LandingTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_Type]  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_CategoryId]  DEFAULT ((0)) FOR [CategoryId]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_Status]  DEFAULT ((1)) FOR [Status]
GO

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[LandingTemplateCategory]    Script Date: 7/17/2019 11:02:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LandingTemplateCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[Priority] [int] NULL,
	[Status] [int] NULL,
	[ParentId] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_LandingTemplateCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LandingTemplateCategory] ADD  CONSTRAINT [DF_LandingTemplateCategory_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Search]    Script Date: 7/17/2019 10:37:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Search]
	@Keyword nvarchar(200),
	@CategoryId int,
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SELECT @TotalRow = COUNT(*) 
	FROM LandingTemplate
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
		AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (CategoryId = @CategoryId)))
		AND (@Status < 0 OR (@Status >= 0 AND (Status = @Status)))
		AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))
	
	IF @PageIndex <= 0
		SELECT TOP(@PageSize) LT.*
		FROM LandingTemplate as LT		
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, LT.Name) > 0))
			AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (LT.CategoryId = @CategoryId)))
			AND (@Status < 0 OR (@Status >= 0 AND (LT.Status = @Status)))
			AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))		
		ORDER BY LT.CreatedDate DESC
	ELSE
		SELECT *
		FROM (
			SELECT LT.*,
				ROW_NUMBER() OVER(ORDER BY LT.CreatedDate DESC) AS RowNum
			FROM LandingTemplate as LT			
			WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, LT.Name) > 0))
				AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (LT.CategoryId = @CategoryId)))
				AND (@Status < 0 OR (@Status >= 0 AND (LT.Status = @Status)))
				AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))			
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)		
		ORDER BY CreatedDate DESC
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Insert]    Script Date: 7/17/2019 10:29:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Insert]
	@Id int out,
	@Name nvarchar(200),
	@Html ntext,
	@Avatar nvarchar(500),	
	@Type int,
	@CategoryId int,
	@Status int,
	@CreatedBy varchar(100)
AS
BEGIN
	INSERT INTO LandingTemplate(Name, Html, Avatar, Type, CategoryId, Status, CreatedBy, CreatedDate)
	VALUES (@Name,@Html, @Avatar,@Type,@CategoryId,@Status,@CreatedBy,GETDATE())
	
	SET @Id = SCOPE_IDENTITY()
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Update]    Script Date: 7/17/2019 10:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Update]
	@Id int,
	@Name nvarchar(200),
	@Html ntext,
	@Avatar nvarchar(500),
	@Type int,
	@CategoryId int,
	@Status int,	
	@ModifiedBy varchar(100)	
AS
BEGIN
	UPDATE LandingTemplate
	SET Name = @Name, 
		Html=@Html,
		Avatar = @Avatar, 
		Type = @Type, 		
		CategoryId = @CategoryId,
		Status = @Status,
		ModifiedBy = @ModifiedBy, 
		ModifiedDate = GETDATE()
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_GetById]    Script Date: 7/17/2019 10:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM LandingTemplate
	WHERE Id = @Id
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Delete]    Script Date: 7/17/2019 10:23:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Delete]
	@Id int
AS
BEGIN
	DELETE FROM LandingTemplate WHERE (Id = @Id)
END

---Category
--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Delete]    Script Date: 7/17/2019 11:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Delete]
	@Id int
AS
BEGIN
	DELETE FROM LandingTemplateCategory WHERE (Id = @Id)
END

--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Search]    Script Date: 7/17/2019 11:47:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_GetAll]	
AS
BEGIN
	SELECT * FROM LandingTemplateCategory
END

--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_GetById]    Script Date: 7/17/2019 11:42:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM LandingTemplateCategory
	WHERE Id = @Id
END

--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Insert]    Script Date: 7/17/2019 11:43:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Insert]
	@Id int out,
	@Name nvarchar(500),	
	@Description nvarchar(500),	
	@Priority int,
	@ParentId int,
	@Status int,
	@CreatedBy varchar(100),
	@Type int
AS
BEGIN
	INSERT INTO LandingTemplateCategory(Name, Description, Priority, ParentId, Status, CreatedBy, CreatedDate, Type)
	VALUES (@Name,@Description,@Priority,@ParentId,@Status,@CreatedBy,GETDATE(),@Type)
	
	SET @Id = SCOPE_IDENTITY()
END

--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Update]    Script Date: 7/17/2019 11:46:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Update]
	@Id int,
	@Name nvarchar(500),	
	@Description nvarchar(500),	
	@Priority int,
	@ParentId int,
	@Status int,
	@Type int
AS
BEGIN
	UPDATE LandingTemplateCategory
	SET Name = @Name, 
		Description=@Description,		
		Priority = @Priority, 		
		ParentId = @ParentId,
		Status = @Status,
		Type=@Type
	WHERE (Id = @Id)
END

---
--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTagIdWithPaging]    Script Date: 7/15/2019 3:50:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTagIdWithPaging]
	@TagId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
	WHERE TN.TagID = @TagId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
		WHERE TN.TagId = @TagId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTopicIdWithPaging]    Script Date: 7/15/2019 3:48:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTopicIdWithPaging]
	@Keyword nvarchar(500)='',
	@TopicId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
	WHERE TN.TopicID = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
		WHERE TN.TopicId = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByThreadIdWithPaging]    Script Date: 7/15/2019 3:42:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByThreadIdWithPaging]
	@ThreadId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
	WHERE TN.ThreadID = @ThreadId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
		WHERE TN.ThreadId = @ThreadId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC


--USE [IMS2_BAODANSINH]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Expert_Update]    Script Date: 8/5/2019 9:48:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb edit:28-03-2019
CREATE PROCEDURE [dbo].[CMS_News_ExpertUpdate]
	@NewsId bigint,
	@Body ntext,	
	@ModifiedBy varchar(100) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		-- Update News
		UPDATE News
		SET Body = @Body,LastModifiedBy=@ModifiedBy
		WHERE Id = @NewsId		
		-- Update NewsContent
		UPDATE NewsContent SET Body = @Body WHERE NewsId = @NewsId
							
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Advertisment_GetAll]    Script Date: 8/5/2019 4:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Advertisment_GetAll]	
AS
BEGIN
	SELECT *
	FROM Advertisment
END

--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Advertisment_Insert]    Script Date: 8/5/2019 4:45:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Advertisment_Insert]
	@Title nvarchar(200),
	@TargetUrl nvarchar(300),
	@AdCode nvarchar(500),
	@Note nvarchar(500),
	@Priority int = 0,
	@TypeId int = 0,
	@ZoneId int = 0,
	@Status int,
	@DisplayStyle int = 0,
	@PositionId int = 0,
	@StartDate datetime,
	@EndDate datetime,	
	@ZoneIdList varchar(500) = '',
	@Width int = 0,
	@Height int = 0
AS
BEGIN
	INSERT INTO Advertisment(	
		Title,
		TargetUrl,
		AdCode,
		Note,
		Priority,
		TypeId,
		ZoneId,
		DisplayStyle,		
		PositionId,
		[Status],
		StartDate,
		EndDate,
		Width ,
		Height
	)
	VALUES( 
		@Title,
		@TargetUrl,
		@AdCode,
		@Note,
		@Priority,
		@TypeId,
		@ZoneId,
		@DisplayStyle,
		@PositionId,
		@Status,
		@StartDate,
		@EndDate,
		@Width ,
		@Height
		)	
	DECLARE @Id int	
	SET @Id = SCOPE_IDENTITY()
	
		DECLARE @Index int;
		DELETE FROM AdvertismentInZone  WHERE AdvertismentId = @Id
		
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > -1 AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM AdvertismentInZone WHERE ZoneId = @ZoneItemId AND AdvertismentId = @Id)
						INSERT INTO AdvertismentInZone(AdvertismentId, ZoneId) VALUES (@Id, @ZoneItemId);
				SET @Index = @Index + 1
			END
		END
END




