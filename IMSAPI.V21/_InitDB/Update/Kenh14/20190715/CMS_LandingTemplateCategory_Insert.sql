--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Insert]    Script Date: 7/17/2019 11:43:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Insert]
	@Id int out,
	@Name nvarchar(500),	
	@Description nvarchar(500),	
	@Priority int,
	@ParentId int,
	@Status int,
	@CreatedBy varchar(100),
	@Type int
AS
BEGIN
	INSERT INTO LandingTemplateCategory(Name, Description, Priority, ParentId, Status, CreatedBy, CreatedDate, Type)
	VALUES (@Name,@Description,@Priority,@ParentId,@Status,@CreatedBy,GETDATE(),@Type)
	
	SET @Id = SCOPE_IDENTITY()
END

