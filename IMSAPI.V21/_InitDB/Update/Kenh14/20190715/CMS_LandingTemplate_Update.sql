--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Update]    Script Date: 7/17/2019 10:35:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Update]
	@Id int,
	@Name nvarchar(200),
	@Html ntext,
	@Avatar nvarchar(500),
	@Type int,
	@CategoryId int,
	@Status int,	
	@ModifiedBy varchar(100)	
AS
BEGIN
	UPDATE LandingTemplate
	SET Name = @Name, 
		Html=@Html,
		Avatar = @Avatar, 
		Type = @Type, 		
		CategoryId = @CategoryId,
		Status = @Status,
		ModifiedBy = @ModifiedBy, 
		ModifiedDate = GETDATE()
	WHERE (Id = @Id)
END

