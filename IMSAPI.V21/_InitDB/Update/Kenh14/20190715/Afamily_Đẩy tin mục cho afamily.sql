﻿
--có mấy cái cần đẩy tin:
--1. Đẩy tin từ mục Dạy con(214) vào mục Kỹ năng sống(234) (mới tạo)
GO
--select * from zone where name like N'%Dạy con%'
--select * from zone where name like N'%Kỹ năng sống%'

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=214;
SET @zoneB=234;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneA

COMMIT TRANSACTION


--2. Đẩy tin từ mục Khoe nhà(148) vào mục con Nhà hay(236) (mới tạo)
GO
--select * from zone where name like N'%Khoe nhà%'
--select * from zone where name like N'%Nhà hay%'

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=148;
SET @zoneB=236;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneA

COMMIT TRANSACTION


--3. Đẩy tin từ mục Tâm sự(37) vào mục con Tổng đài trái tim(238) (mới tạo)
GO
--select * from zone where name like N'%Tâm sự%'
--select * from zone where name like N'%Tổng đài trái tim%'

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=37;
SET @zoneB=238;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneA

COMMIT TRANSACTION


--4.1 Đẩy tin từ 2 mục Chuyên gia(79) và Nuôi con(213) vào mục Làm mẹ(158) (cả 3 cái này là mục con trong Mẹ & Bé)
GO
--select * from zone where name like N'%Chuyên gia%' and parentid in(select id from zone where name like N'%Mẹ và bé%')
--select * from zone where name like N'%Nuôi con%' and parentid in(select id from zone where name like N'%Mẹ và bé%')
--select * from zone where name like N'%Làm mẹ%' and parentid in(select id from zone where name like N'%Mẹ và bé%')

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=79;
SET @zoneB=158;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsPublish where newsid in (select newsid from NewsPublish where zoneid =@zoneA) and zoneid=@zoneB)

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone 
WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsInZone where newsid in (select newsid from NewsInZone where zoneid =@zoneA) and zoneid=@zoneB)

COMMIT TRANSACTION

--4.2 Đẩy tin từ 2 mục Chuyên gia(79) và Nuôi con(213) vào mục Làm mẹ(158) (cả 3 cái này là mục con trong Mẹ & Bé)
GO
BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=213;
SET @zoneB=158;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsPublish where newsid in (select newsid from NewsPublish where zoneid =@zoneA) and zoneid=@zoneB)

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone 
WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsInZone where newsid in (select newsid from NewsInZone where zoneid =@zoneA) and zoneid=@zoneB)

COMMIT TRANSACTION


--5.1 Đẩy tin từ  2 mục Tư vấn(152)(của nhà hay) và Tiêu dùng(59)(của Xã hội) vào mục Chi tiêu(237) (mới tạo)
GO
--select * from zone where name like N'%Tiêu dùng%' and parentid in(select id from zone where name like N'%Xã hội%')
--select * from zone where name like N'%Tư vấn%' and parentid in(select id from zone where name like N'%nhà hay%')
--select * from zone where name like N'%Chi tiêu%'

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=152;
SET @zoneB=237;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneA

COMMIT TRANSACTION

--5.2 Đẩy tin từ  2 mục Tư vấn(152)(của nhà hay) và Tiêu dùng(59)(của Xã hội) vào mục Chi tiêu(237) (mới tạo)
GO

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
DECLARE @zoneC int;
SET @zoneA=59;
SET @zoneB=237;
SET @zoneC=152;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsInZone where zoneid=@zoneC)

COMMIT TRANSACTION


--6. Đẩy tin từ mục Giải pháp - Tiện ích(48) vào mục Mua sắm(149) đều là con của Mua sắm - Nhà hay
GO
--select * from zone where name like N'%Giải pháp - Tiện ích%' and parentid in(select id from zone where name like N'%Mua sắm - Nhà hay%')
--select * from zone where name like N'%Mua sắm%' and parentid in(select id from zone where name like N'%Mua sắm - Nhà hay%')

BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=48;
SET @zoneB=149;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsPublish where newsid in (select newsid from NewsPublish where zoneid =@zoneA) and zoneid=@zoneB)

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone 
WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsInZone where newsid in (select newsid from NewsInZone where zoneid =@zoneA) and zoneid=@zoneB)

COMMIT TRANSACTION

--END
GO
