--USE [IMS2_BAODANSINH]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Expert_Update]    Script Date: 8/5/2019 9:48:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb edit:28-03-2019
CREATE PROCEDURE [dbo].[CMS_News_ExpertUpdate]
	@NewsId bigint,
	@Body ntext,	
	@ModifiedBy varchar(100) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		-- Update News
		UPDATE News
		SET Body = @Body,LastModifiedBy=@ModifiedBy
		WHERE Id = @NewsId		
		-- Update NewsContent
		UPDATE NewsContent SET Body = @Body WHERE NewsId = @NewsId
							
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


