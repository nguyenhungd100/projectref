--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_GetById]    Script Date: 7/17/2019 10:28:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM LandingTemplate
	WHERE Id = @Id
END

