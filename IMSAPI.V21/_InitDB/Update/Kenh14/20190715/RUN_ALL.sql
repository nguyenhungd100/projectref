--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTagIdWithPaging]    Script Date: 7/15/2019 3:50:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTagIdWithPaging]
	@TagId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
	WHERE TN.TagID = @TagId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
		WHERE TN.TagId = @TagId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTopicIdWithPaging]    Script Date: 7/15/2019 3:48:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTopicIdWithPaging]
	@Keyword nvarchar(500)='',
	@TopicId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
	WHERE TN.TopicID = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
		WHERE TN.TopicId = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByThreadIdWithPaging]    Script Date: 7/15/2019 3:42:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByThreadIdWithPaging]
	@ThreadId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
	WHERE TN.ThreadID = @ThreadId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
		WHERE TN.ThreadId = @ThreadId and ((@Status < 1 and 1=1) OR (@Status > 0 and N.Status=@Status))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

