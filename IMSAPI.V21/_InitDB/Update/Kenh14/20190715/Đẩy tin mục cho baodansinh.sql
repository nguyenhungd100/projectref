﻿
--1 Đẩy tin từ mục Khám phá(9) vào mục Du lịch - Khám phá(12) (cả 2 cái này là mục con trong Giải trí)
GO
--select * from zone where name like N'%Khám phá%' and parentid in(select id from zone where name like N'%Giải trí%') and status=1
--select * from zone where name like N'%Du lịch - Khám phá%' and parentid in(select id from zone where name like N'%Giải trí%') and status=1
BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=9;
SET @zoneB=12;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsPublish where newsid in (select newsid from NewsPublish where zoneid =@zoneA) and zoneid=@zoneB)

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone 
WHERE ZoneId=@zoneA and newsid not in(select newsid from NewsInZone where newsid in (select newsid from NewsInZone where zoneid =@zoneA) and zoneid=@zoneB)

COMMIT TRANSACTION