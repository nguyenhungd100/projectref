--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplate_Search]    Script Date: 7/17/2019 10:37:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_LandingTemplate_Search]
	@Keyword nvarchar(200),
	@CategoryId int,
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SELECT @TotalRow = COUNT(*) 
	FROM LandingTemplate
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
		AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (CategoryId = @CategoryId)))
		AND (@Status < 0 OR (@Status >= 0 AND (Status = @Status)))
		AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))
	
	IF @PageIndex <= 0
		SELECT TOP(@PageSize) LT.*
		FROM LandingTemplate as LT		
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, LT.Name) > 0))
			AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (LT.CategoryId = @CategoryId)))
			AND (@Status < 0 OR (@Status >= 0 AND (LT.Status = @Status)))
			AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))		
		ORDER BY LT.CreatedDate DESC
	ELSE
		SELECT *
		FROM (
			SELECT LT.*,
				ROW_NUMBER() OVER(ORDER BY LT.CreatedDate DESC) AS RowNum
			FROM LandingTemplate as LT			
			WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, LT.Name) > 0))
				AND (@CategoryId <= 0 OR (@CategoryId > 0 AND (LT.CategoryId = @CategoryId)))
				AND (@Status < 0 OR (@Status >= 0 AND (LT.Status = @Status)))
				AND (@Type < 0 OR (@Type >= 0 AND (Type = @Type)))			
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)		
		ORDER BY CreatedDate DESC
END

