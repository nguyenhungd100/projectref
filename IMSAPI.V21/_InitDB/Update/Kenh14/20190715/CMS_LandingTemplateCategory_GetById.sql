--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_GetById]    Script Date: 7/17/2019 11:42:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM LandingTemplateCategory
	WHERE Id = @Id
END

