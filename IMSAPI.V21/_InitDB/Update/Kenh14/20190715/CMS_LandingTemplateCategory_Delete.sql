--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Delete]    Script Date: 7/17/2019 11:42:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Delete]
	@Id int
AS
BEGIN
	DELETE FROM LandingTemplateCategory WHERE (Id = @Id)
END

