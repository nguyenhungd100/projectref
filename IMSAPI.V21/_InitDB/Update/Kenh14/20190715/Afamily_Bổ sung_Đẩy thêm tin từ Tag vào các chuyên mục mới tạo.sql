﻿--Đẩy tin bổ sung tin từ Tag vào Mục
GO
--1. đẩy tin từ Tag: http://afamily.vn/xu-huong-trai-nghiem-moi.html(193876) => mục Xem gì(231) của Đời sống
--select top(10) * from tag where url = 'xu-huong-trai-nghiem-moi'
--select * from zone where name like N'%Xem gì%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'xu-huong-trai-nghiem-moi';
SET @TagA=193876;
SET @zoneB=231;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--2. đẩy tin từ Tag: http://afamily.vn/vui-choi-an-uong.html(189925) => mục Ăn gì(163) của Đời sống
--select top(10) * from tag where url = 'vui-choi-an-uong'
--select * from zone where name like N'%Ăn gì%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'vui-choi-an-uong';
SET @TagA=189925;
SET @zoneB=163;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--3. đẩy tin từ Tag: http://afamily.vn/loi-song.html(43583) => mục Lối sống(179) của Đời sống
--select top(10) * from tag where url = 'loi-song'
--select * from zone where name like N'%Lối sống%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'loi-song';
SET @TagA=43583;
SET @zoneB=179;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--4. đẩy tin từ Tag: http://afamily.vn/cam-nang-du-lich.html(72811) => mục Du lịch(136) của Đời sống(Lifestyle)
--select top(10) * from tag where url = 'cam-nang-du-lich'
--select * from zone where name like N'%Du lịch%'


BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'cam-nang-du-lich';
SET @TagA=72811;
SET @zoneB=136;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--5. đẩy tin từ Tag: http://afamily.vn/ban-tay-kheo-leo.html(87406) => mục Khéo tay(134) của Đời sống(Lifestyle)
--select top(10) * from tag where url = 'ban-tay-kheo-leo'
--select * from zone where name like N'%Khéo tay%'


BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'ban-tay-kheo-leo';
SET @TagA=87406;
SET @zoneB=134;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--6. đẩy tin từ Tag: http://afamily.vn/af-hoc-duong.html(189922) => mục Học đường(235) của Dạy con
--select top(10) * from tag where url = 'af-hoc-duong'
--select * from zone where name like N'%Học đường%'


BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'af-hoc-duong';
SET @TagA=189922;
SET @zoneB=235;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--7. đẩy tin từ Tag: http://afamily.vn/cac-ca-phau-thuat-tham-my.html(193877) => mục Phẫu thuật thẩm mỹ(212) của Sức khỏe
--select top(10) * from tag where url = 'cac-ca-phau-thuat-tham-my'
--select * from zone where name like N'%Phẫu thuật thẩm mỹ%'


BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'phong-su-xa-hoi';
SET @TagA=193877;
SET @zoneB=212;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO

--8. Chuyển tin trong mục Phẫu thuật thẩm mỹ(212) mà có Mục chính là Đẹp(70) => đổi sang mục chính là Sức khỏe(86)
--select * from zone where name like N'%Phẫu thuật thẩm mỹ%'
--select * from zone where name like N'%Đẹp%'
--select * from zone where name like N'%Sức khỏe%'

BEGIN TRANSACTION
--select * from NewsInZone where NewsId in(select NewsId from NewsInZone where zoneid=212) and IsPrimary=1 and ZoneId=70
UPDATE NewsInZone set ZoneId=86 where NewsId in(select NewsId from NewsInZone where zoneid=212) and IsPrimary=1 and ZoneId=70

--select id,newsid,zoneid from NewsPublish where NewsId in(select NewsId from NewsInZone where NewsId in(select NewsId from NewsInZone where zoneid=212) and IsPrimary=1 and ZoneId=70) and ZoneId=70
UPDATE NewsPublish set ZoneId=86 where NewsId in(select NewsId from NewsInZone where NewsId in(select NewsId from NewsInZone where zoneid=212) and IsPrimary=1 and ZoneId=70) and ZoneId=70

COMMIT TRANSACTION

GO