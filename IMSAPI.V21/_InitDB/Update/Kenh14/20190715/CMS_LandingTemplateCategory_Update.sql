--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_LandingTemplateCategory_Update]    Script Date: 7/17/2019 11:46:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-07-17
CREATE PROCEDURE [dbo].[CMS_LandingTemplateCategory_Update]
	@Id int,
	@Name nvarchar(500),	
	@Description nvarchar(500),	
	@Priority int,
	@ParentId int,
	@Status int,
	@Type int
AS
BEGIN
	UPDATE LandingTemplateCategory
	SET Name = @Name, 
		Description=@Description,		
		Priority = @Priority, 		
		ParentId = @ParentId,
		Status = @Status,
		Type=@Type
	WHERE (Id = @Id)
END

