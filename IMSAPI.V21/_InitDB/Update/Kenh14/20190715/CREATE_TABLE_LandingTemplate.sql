--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[LandingTemplate]    Script Date: 7/17/2019 11:02:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LandingTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Html] [ntext] NULL,
	[Avatar] [nvarchar](500) NULL,
	[Type] [int] NULL,
	[CategoryId] [int] NULL,
	[Status] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_LandingTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_Type]  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_CategoryId]  DEFAULT ((0)) FOR [CategoryId]
GO

ALTER TABLE [dbo].[LandingTemplate] ADD  CONSTRAINT [DF_LandingTemplate_Status]  DEFAULT ((1)) FOR [Status]
GO

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[LandingTemplateCategory]    Script Date: 7/17/2019 11:02:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LandingTemplateCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[Priority] [int] NULL,
	[Status] [int] NULL,
	[ParentId] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_LandingTemplateCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LandingTemplateCategory] ADD  CONSTRAINT [DF_LandingTemplateCategory_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO



