﻿--Đẩy tin vào mục mới tạo từ tag
GO
--1. đẩy tin từ Tag: http://afamily.vn/phong-su-xa-hoi.html(189918) => mục Phóng sự(224) =>ok
--select top(10) * from tag where url = 'phong-su-xa-hoi'
--select * from zone where name like N'%Phóng sự%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'phong-su-xa-hoi';
SET @TagA=189918;
SET @zoneB=224;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--2. đẩy tin từ Tag: http://afamily.vn/fitness.html(69704) => mục Fitness(225) =>0k
--select top(10) * from tag where url = 'fitness'
--select * from zone where name like N'%Fitness%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'fitness';
SET @TagA=69704;
SET @zoneB=225;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--3. đẩy tin từ Tag: http://afamily.vn/chuyen-gia-trang-diem.html(79740) => mục Make up(226) =>ok
--select top(10) * from tag where url = 'chuyen-gia-trang-diem'
--select * from zone where name like N'%Make up%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'chuyen-gia-trang-diem';
SET @TagA=79740;
SET @zoneB=226;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--4. đẩy tin từ Tag: http://afamily.vn/phim-bo-online.html(189911) => mục Phim bộ online(227) => 0k
--select top(10) * from tag where url = 'phim-bo-online'
--select * from zone where name like N'%Phim bộ online%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select  @TagA=Id from tag where url = 'phim-bo-online';
SET @TagA=189911;
SET @zoneB=227;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--5. đẩy tin từ Tag: http://afamily.vn/triet-ly-cong-so.html(189441) => mục Triết lý công sở(228) =>0k
--select top(10) * from tag where url = 'triet-ly-cong-so'
--select * from zone where name like N'%Triết lý công sở%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'triet-ly-cong-so';
SET @TagA=189441;
SET @zoneB=228;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--6. đẩy tin từ Tag: http://afamily.vn/bi-kip-cong-so.html(189442) => mục Bí kíp công sở(229) =>0k
--select top(10) * from tag where url = 'bi-kip-cong-so'
--select * from zone where name like N'%Bí kíp công sở%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'bi-kip-cong-so';
SET @TagA=189442;
SET @zoneB=229;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--7. đẩy tin từ Tag: http://afamily.vn/chuyen-phiem-cong-so.html(121196) => mục Chuyện phiếm công sở(230) =>ok
--select top(10) * from tag where url = 'chuyen-phiem-cong-so'
--select * from zone where name like N'%Chuyện phiếm công sở%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'chuyen-phiem-cong-so';
SET @TagA=121196;
SET @zoneB=230;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--8. đẩy tin từ Tag: http://afamily.vn/vui-choi-an-uong.html(189925) => mục Ăn gì(163) mục con của LifeStyle(97) =>ok
--select top(10) * from tag where url = 'vui-choi-an-uong'
--select * from zone where name like N'%Ăn gì%' and parentid=97

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'vui-choi-an-uong';
SET @TagA=189925;
SET @zoneB=163;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--9. đẩy tin từ Tag: http://afamily.vn/ban-tay-kheo-leo.html(87406) => mục Khéo tay(134) mục con của LifeStyle(97) =>ok
--select top(10) * from tag where url = 'ban-tay-kheo-leo'
--select * from zone where name like N'%Khéo tay%' and parentid=97

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'ban-tay-kheo-leo';
SET @TagA=87406;
SET @zoneB=134;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--10. đẩy tin từ Tag: http://afamily.vn/cap-doi-tinh-yeu.html(189359) => mục Cặp đôi(232) =>ok
--select top(10) * from tag where url = 'cap-doi-tinh-yeu'
--select * from zone where name like N'%Cặp đôi%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'cap-doi-tinh-yeu';
SET @TagA=189359;
SET @zoneB=232;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--11. đẩy tin từ Tag: http://afamily.vn/tin-y-te.html(189382) => mục Tin y tế(233) =>ok
--select top(10) * from tag where url = 'tin-y-te'
--select * from zone where name like N'%Tin y tế%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'tin-y-te';
SET @TagA=189382;
SET @zoneB=233;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--12.1 đẩy tin từ Tag: http://afamily.vn/suc-khoe-tre-em.html(2020) => mục Sức khỏe trẻ em(239)=>ok
--select top(10) * from tag where url = 'suc-khoe-tre-em'(count news 997 bài)
--select * from zone where name like N'%Sức khỏe trẻ em%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'suc-khoe-tre-em';
SET @TagA=2020;
SET @zoneB=239;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--12.2 đẩy tin từ Tag: http://afamily.vn/benh-tre-em.html(12466) => mục Sức khỏe trẻ em(239)
--select top(10) * from tag where url = 'benh-tre-em'
--select * from zone where name like N'%Sức khỏe trẻ em%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'benh-tre-em';
SET @TagA=12466;
SET @zoneB=239;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--13 đẩy tin từ Tag: http://afamily.vn/hoc-duong.html(69039) => mục Học đường(235) =>ok
--select top(10) * from tag where url = 'hoc-duong'
--select * from zone where name like N'%Học đường%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'hoc-duong';
SET @TagA=69039;
SET @zoneB=235;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--14 đẩy tin từ Tag: http://afamily.vn/dia-chi-mua-sam.html(9467) => mục Mua sắm(149)
--select top(10) * from tag where url = 'dia-chi-mua-sam'
--select * from zone where name like N'%Mua sắm%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'dia-chi-mua-sam';
SET @TagA=9467;
SET @zoneB=149;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--15.1 đẩy tin từ Tag: http://afamily.vn/tieu-dung-thong-minh.html(150823) => mục Chi tiêu(237)
--select top(10) * from tag where url = 'tieu-dung-thong-minh'
--select * from zone where name like N'%Chi tiêu%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'tieu-dung-thong-minh';
SET @TagA=150823;
SET @zoneB=237;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--15.2 đẩy tin từ Tag: http://afamily.vn/thi-truong-tieu-dung.html (184629) => mục Chi tiêu(237)
--select top(10) * from tag where url = 'thi-truong-tieu-dung'
--select * from zone where name like N'%Chi tiêu%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'thi-truong-tieu-dung';
SET @TagA=184629;
SET @zoneB=237;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION

GO
--16 đẩy tin từ Tag: http://afamily.vn/tong-dai-doc-het-trai-tim.html(108506) => mục Tổng đài trái tim(238)
--select top(10) * from tag where url = 'tong-dai-doc-het-trai-tim'
--select * from zone where name like N'%Tổng đài trái tim%'

BEGIN TRANSACTION

DECLARE @TagA int;
DECLARE @zoneB int;
--select @TagA=Id from tag where url = 'tong-dai-doc-het-trai-tim';
SET @TagA=108506;
SET @zoneB=238;

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneB as ZoneId,0 as IsPrimary,-1 as DistributionID,0 as RootZoneId from TagNews inner join News on Id=NewsID 
WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsInZone WHERE ZoneId=@zoneB);

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select distinct NewsId,@zoneB as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,null as LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,0 as Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE NewsId in(select NewsId from TagNews inner join News on Id=NewsID WHERE TagID=@TagA and Status=8 and newsid not in(select NewsId from NewsPublish WHERE ZoneId=@zoneB))

COMMIT TRANSACTION


GO

