--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsNotByExpert]    Script Date: 5/29/2019 4:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsNotByExpert] --0,0,'','',1,20,0
    @TopicId INT=0 ,	
	@ZoneId INT=0 ,
	@Status INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
	@Account varchar(50)='' ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select distinct n.Id
		from news n	left join (select NewsId from newsextension where type=51) as newstemp
		on 	newstemp.NewsId=n.Id
		left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1
		left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
		where newstemp.NewsId IS NULL AND (n.Status=@Status) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
		AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
		AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))
    ) as tem

	SELECT *
	FROM ( 
	select nn.*--,nz.ZoneId
		,ROW_NUMBER() OVER ( ORDER BY nn.CreatedDate DESC ) AS RowNum
		from news nn inner join
		(select distinct n.id from news n left join (select NewsId from newsextension where type=51) as newstemp
			on 	newstemp.NewsId=n.Id
			left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1 
			left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
			where newstemp.NewsId IS NULL AND (n.Status=@Status) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))
		) as tempall 
		on tempall.Id=nn.Id
		--inner join NewsInZone nz on nz.NewsId=tempall.Id
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END





--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsAllByExpert]    Script Date: 5/29/2019 3:47:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsAllByExpert] --5,0,'','',1,20,0
    @ExpertStatus INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select n.Id
		from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
		on 	newstemp.NewsId=n.Id
		inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6,8)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
		SELECT stemp.*,nes.Value as NoteExpert, nesdate.Value as SendDateExpert
		FROM (
			select n.*, nz.ZoneId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum
			from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
			on 	newstemp.NewsId=n.Id
			inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6,8)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) AS stemp
		left join newsextension nes on nes.NewsId=stemp.Id 
		left join newsextension nesdate on nesdate.NewsId=stemp.Id 
		where nes.Type=53 and nesdate.Type=54		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ReSendExpert]    Script Date: 5/29/2019 4:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-05-29
CREATE PROCEDURE [dbo].[CMS_NewsExtension_ReSendExpert] --0,0,'','',1,20,0
    @ExpertId INT=0,
	@RemindDate nvarchar(50),
	@ExpireDate int=24,
	@Note nvarchar(255)=''
AS 
BEGIN
	UPDATE NewsExtension set Value=@RemindDate from (
		select net.* from
		(select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
		inner join newsextension ne on ne.newsid=abc.newsid
		where ne.type=52  and ne.value=0) as abcd
		inner join newsextension net on net.newsid=abcd.newsid
		where net.type=54  and net.value <= DATEADD(hh, DateDiff(hh, 0, GETDATE()) - @ExpireDate, 0)
	) as abcde 
	where NewsExtension.NewsId=abcde.NewsId and NewsExtension.Type=abcde.Type
END

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_GetAllBomByTypeAndZoneId]    Script Date: 5/30/2019 3:38:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-05-22>
-- Description:	<get list news position all bom by type and zoneid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_GetAllBomByTypeAndZoneId] --1, 0
	@TypeId int,
	@ZoneId int
AS
BEGIN	
	SELECT Id, ZoneId, TypeId, [Order], Lockable, Locked, ExpiredLock, NewsIdForBomd, NewsId, Title, 
		SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
		NewsRelation, Source, IsFocus, NewsType, ThreadId, DistributionDate, Url, AllowAutoUpdate, AvatarCustom,
		ViewCount, OriginalId, Type, ScheduleDate
	FROM
		(
			SELECT NP.Id, NP.ZoneId, NP.TypeId, NP.[Order], NP.Lockable, NP.Locked, NP.ExpiredLock, NP.NewsIdForBomd, NP.NewsId, NP.Title, 
				NP.SubTitle, NP.Sapo, NP.Avatar, NP.AvatarDesc, NP.Avatar1, NP.Avatar2, NP.Avatar3, NP.Avatar4, NP.Avatar5, NP.Author, 
				NP.NewsRelation, NP.Source, NP.IsFocus, NP.NewsType, NP.ThreadId, NP.DistributionDate, NP.Url, NP.AllowAutoUpdate, NP.AvatarCustom,
				N.ViewCount, N.OriginalId, NP.Type, NP.ScheduleDate,
				(ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY np.ScheduleDate DESC, np.DistributionDate DESC )) AS ROW
			FROM NewsPosition AS NP LEFT JOIN News AS N ON NP.NewsId = N.Id		
			WHERE
				np.TypeId = @typeId
				AND (@zoneId = 0 OR np.ZoneId = @zoneId)
				--AND np.DistributionDate <= GETDATE()
				--AND (np.[ScheduleDate] IS NULL OR np.[ScheduleDate] <= GETDATE())				
		)TEMP
	WHERE 
		NewsId NOT IN (
			SELECT NewsId
			FROM
			(
				SELECT NewsId,(ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY np.ScheduleDate DESC, np.DistributionDate DESC )) AS ROW
				FROM NewsPosition AS NP
				WHERE
					np.TypeId = @TypeId
					AND np.ZoneId = @ZoneId
					AND (np.[ScheduleDate] IS NULL OR np.[ScheduleDate] <= GETDATE())
			)TEMP
			WHERE 
				ROW = 1
		)
	ORDER BY
		[Order]
END

