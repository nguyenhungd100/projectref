﻿alter table ThreadNews
add IsPrimary bit

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ThreadNews_MultiUpdateThreadNews]    Script Date: 5/17/2019 11:17:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 22/06/2018
ALTER PROCEDURE [dbo].[CMS_ThreadNews_MultiUpdateThreadNews]
	@NewsId bigint,
	@ListThreadId varchar(500),
	@IsPrimaryThreadId int=0
AS
BEGIN	
	BEGIN TRANSACTION		
	
	DELETE FROM ThreadNews WHERE NewsId = @NewsId

	DECLARE @Index int, 
			@TotalRowInTemp int,
			@ThreadId int;
				
	-- Insert later
	DECLARE @TblAddTemp TABLE (Id int identity(1,1), ThreadId int)
	INSERT @TblAddTemp (ThreadId)
	SELECT CONVERT(int, part) FROM SplitString(@ListThreadId, ';')
	
	SET @Index = 0
	SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblAddTemp), 0)
	WHILE (@Index <= @TotalRowInTemp)
	BEGIN
		SET @ThreadId = ISNULL((SELECT ThreadId FROM @TblAddTemp WHERE Id = @Index), 0)
		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE NewsId = @NewsId AND ThreadId = @ThreadId)
			BEGIN
				INSERT INTO ThreadNews(NewsID, ThreadId)
				VALUES(@NewsId, @ThreadId)
			END
		END
		SET @Index = @Index + 1
	END
	
	--update isparimary
	update ThreadNews set IsPrimary=1 where ThreadId=@IsPrimaryThreadId and NewsId = @NewsId
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500)
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE 
	BEGIN
		COMMIT TRANSACTION
	END
END

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_Insert]    Script Date: 5/17/2019 1:17:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Insert new thread>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Thread_Insert]
	@Id INT OUTPUT,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Title nvarchar(300),
	@Description nvarchar(500),
	@Url varchar(500),
	@Avatar nvarchar(500),
	@HomeAvatar varchar(255),
	@SpecialAvatar varchar(255),
	@IsHot bit,
	@IsOnHome bit,
	@CreatedDate datetime,
	@CreatedBy varchar(255),
	@MetaKeyword nvarchar(300),
	@MetaContent nvarchar(300),
	@TemplateId int,
	@Invisibled BIT,
	
	@PrimaryZoneId int,
	@OtherZoneId varchar(1000),
	@RelationThread varchar(1000),

	@NewsCoverId bigint = 0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		
		INSERT INTO Thread (Name, UnsignName, Title, Description, Url, Avatar, HomeAvatar, SpecialAvatar, 
			IsHot, IsOnHome, CreatedDate, ModifiedDate, CreatedBy, EditedBy, MetaKeyword, MetaContent, 
			TemplateId, Invisibled, NewsCoverId)
		VALUES (@Name,@UnsignName,@Title,@Description,@Url,@Avatar,@HomeAvatar,@SpecialAvatar,
			@IsHot,@IsOnHome, @CreatedDate, GETDATE(),@CreatedBy,@CreatedBy,@MetaKeyword,@MetaContent,
			@TemplateId,@Invisibled, @NewsCoverId)
		
		SET @Id = SCOPE_IDENTITY();
		
		/* Insert vào bảng  ThreadZone*/
		
		INSERT INTO ThreadInZone (ThreadId, ZoneId, IsPrimary)
		VALUES (@Id, @PrimaryZoneId, 1)
		
		IF @OtherZoneId <> ''
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
			DECLARE @ZoneItemId bigint, @Index int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
					IF NOT EXISTS(SELECT ZoneId FROM ThreadInZone WHERE ZoneId = @ZoneItemId AND ThreadId = @Id)
						INSERT INTO ThreadInZone(ThreadId, ZoneId, IsPrimary) 
						VALUES (@Id, @ZoneItemId, 0);
						
				SET @Index = @Index + 1
			END
		END
		
		/* Insert vào bảng  ThreadRelation*/
		IF @RelationThread <> ''
		BEGIN
			DECLARE @TblTempThread TABLE (Id int identity(1,1), ThreadId bigint)
			DECLARE @ThreadItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempThread(ThreadId) SELECT CONVERT(bigint, Part) FROM SplitString(@RelationThread, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempThread)) 
			BEGIN
				SET @ThreadItemId = (SELECT ThreadId FROM @TblTempThread WHERE Id = @Index)
				IF @ThreadItemId IS NOT NULL AND @ThreadItemId > 0
					IF NOT EXISTS(SELECT ThreadId FROM ThreadRelation WHERE ThreadRelationId = @ThreadItemId AND ThreadId = @Id)
						INSERT INTO ThreadRelation(ThreadId, ThreadRelationId) 
						VALUES (@Id, @ThreadItemId);
				  				
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_Update]    Script Date: 5/17/2019 1:18:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Update thread>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Thread_Update]
	@Id INT,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Title nvarchar(300),
	@Description nvarchar(500),
	@Url varchar(500),
	@Avatar nvarchar(500),
	@HomeAvatar varchar(255),
	@SpecialAvatar varchar(255),
	@IsHot bit,
	@IsOnHome bit,
	@CreatedDate datetime,
	@EditedBy varchar(255),
	@MetaKeyword nvarchar(300),
	@MetaContent nvarchar(300),
	@TemplateId int,
	@Invisibled BIT = 0,
	
	@PrimaryZoneId int,
	@OtherZoneId varchar(1000),
	@RelationThread varchar(1000),

	@NewsCoverId bigint = 0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE Thread
		SET	Name = @Name,
			UnsignName = CASE @UnsignName WHEN '' THEN UnsignName ELSE @UnsignName END,
			Title = @Title,
			Description = @Description,
			Url = @Url,
			Avatar = CASE @Avatar WHEN '' THEN Avatar ELSE @Avatar END,
			HomeAvatar = CASE @HomeAvatar WHEN '' THEN HomeAvatar ELSE @HomeAvatar END,
			SpecialAvatar = CASE @SpecialAvatar WHEN '' THEN SpecialAvatar ELSE @SpecialAvatar END,
			IsHot = @IsHot,
			IsOnHome = @IsOnHome,
			--CreatedDate = CASE @CreatedDate WHEN NULL THEN CreatedDate ELSE @CreatedDate END,
			ModifiedDate = GETDATE(),
			EditedBy = @EditedBy,
			MetaKeyword = @MetaKeyword,
			MetaContent = @MetaContent,
			TemplateId = @TemplateId,
			Invisibled = @Invisibled,
			NewsCoverId = @NewsCoverId
		WHERE (Id = @Id)
		
		/* Xóa dữ liệu bảng ThreadZone */
		DELETE FROM ThreadInZone WHERE ThreadId = @Id
		
		INSERT INTO ThreadInZone (ThreadId, ZoneId, IsPrimary)
		VALUES (@Id, @PrimaryZoneId, 1)
		
		IF @OtherZoneId <> ''
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
			DECLARE @ZoneItemId bigint, @Index int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
					IF NOT EXISTS(SELECT ZoneId FROM ThreadInZone WHERE ZoneId = @ZoneItemId AND ThreadId = @Id)
						INSERT INTO ThreadInZone(ThreadId, ZoneId, IsPrimary) 
						VALUES (@Id, @ZoneItemId, 0);
				  				
				SET @Index = @Index + 1
			END
		END
		
		/* Xóa dữ liệu bảng ThreadRelation */
		DELETE FROM ThreadRelation WHERE ThreadId = @Id
		
		IF @RelationThread <> ''
		BEGIN
			DECLARE @TblTempThread TABLE (Id int identity(1,1), ThreadId bigint)
			DECLARE @ThreadItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempThread(ThreadId) SELECT CONVERT(bigint, Part) FROM SplitString(@RelationThread, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempThread)) 
			BEGIN
				SET @ThreadItemId = (SELECT ThreadId FROM @TblTempThread WHERE Id = @Index)
				IF @ThreadItemId IS NOT NULL AND @ThreadItemId > 0
					IF NOT EXISTS(SELECT ThreadId FROM ThreadRelation WHERE ThreadRelationId = @ThreadItemId AND ThreadId = @Id)
						INSERT INTO ThreadRelation(ThreadId, ThreadRelationId) 
						VALUES (@Id, @ThreadItemId);
				  				
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_DEMO]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_UpdateNews]    Script Date: 5/17/2019 5:03:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <10:26 13/08/2013>
-- Description:	<Update NewsList into Threadnews>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Thread_UpdateNews]
	@ThreadId bigint,
	@DeleteNewsIds varchar(max) = '',
	@AddNewsIds varchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	DECLARE @ThreadName nvarchar(200)

	SELECT @ThreadName = Name FROM Thread WHERE Id = @ThreadId
	
	IF @ThreadName IS NOT NULL AND @ThreadName <> ''
	BEGIN
		DECLARE @Index int, 
				@TotalRowInTemp int,
				@NewsId bigint;
		-- Delete first
		DECLARE @TblDeleteTemp TABLE (Id int identity(1,1), NewsId bigint)
		INSERT @TblDeleteTemp (NewsId)
		SELECT CONVERT(bigint, part) FROM SplitString(@DeleteNewsIds, ';')
		
		SET @Index = 0
		SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblDeleteTemp), 0)
		WHILE (@Index <= @TotalRowInTemp)
		BEGIN
			SET @NewsId = ISNULL((SELECT NewsId FROM @TblDeleteTemp WHERE Id = @Index), 0)
			IF @NewsId > 0
			BEGIN
				DELETE FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @NewsId

				UPDATE News SET ThreadId=0 WHERE ThreadId = @ThreadId AND Id = @NewsId
			END
			SET @Index = @Index + 1
		END
		
		-- Insert later
		DECLARE @TblAddTemp TABLE (Id int identity(1,1), NewsId bigint)
		INSERT @TblAddTemp (NewsId)
		SELECT CONVERT(bigint, part) FROM SplitString(@AddNewsIds, ';')
		
		SET @Index = 0
		SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblAddTemp), 0)
		WHILE (@Index <= @TotalRowInTemp)
		BEGIN
			SET @NewsId = ISNULL((SELECT NewsId FROM @TblAddTemp WHERE Id = @Index), 0)
			IF @NewsId > 0
			BEGIN
				IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE NewsId = @NewsId AND ThreadId = @ThreadId)
				BEGIN
					INSERT INTO ThreadNews(NewsID, ThreadID)
					VALUES(@NewsId, @ThreadId)
				END
			END
			SET @Index = @Index + 1
		END
	END
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500)
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE 
	BEGIN
		COMMIT TRANSACTION
	END
END

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_GetNewsExpertByListIds]    Script Date: 6/4/2019 2:59:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
CREATE PROCEDURE [dbo].[CMS_NewsExpert_GetNewsExpertByListIds] --'20190307115504946,20181022095701044,20181022095512794'
    @NewsIds varchar(max)=''
AS 
BEGIN
	DECLARE @search nvarchar(max);
	SET @search = 'SELECT ne.*,e.ExpertName FROM NewsExpert ne INNER JOIN Expert e ON e.Id=ne.ExpertId WHERE ne.NewsId IN('+@NewsIds+')';

	EXEC SP_EXECUTESQL @search;
END

