--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_ListNewsAllByExpert]    Script Date: 6/11/2019 5:21:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
ALTER PROCEDURE [dbo].[CMS_NewsExpert_ListNewsAllByExpert] --3,0,1,0,'','',1,20,0
    @ExpertId INT=0 ,
	@TopicId INT=0 ,
    @ExpertStatus INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	IF(@TopicId<=0)
	BEGIN
		SELECT  @TotalRow = COUNT(1) from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 			
				where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
				AND (ne.Status=@ExpertStatus)

		SELECT *
		FROM ( 
		select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
			from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 			
				where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
				AND (ne.Status=@ExpertStatus)
		
		) AS DATA
		WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
		ORDER BY SendDateExpert DESC
	END
	ELSE
	BEGIN
		SELECT  @TotalRow = COUNT(1) from news n
				left join newsexpert ne on ne.newsid=n.id
				left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
				left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1  
					where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) AND (nt.TopicId=@TopicId) 
					AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
					AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
					AND (ne.Status=@ExpertStatus)

			SELECT *
			FROM ( 
			select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
				,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
				from news n
				left join newsexpert ne on ne.newsid=n.id
				left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
				left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1  
					where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) AND (nt.TopicId=@TopicId) 
					AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
					AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
					AND (ne.Status=@ExpertStatus)
		
			) AS DATA
		WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
		ORDER BY SendDateExpert DESC	
	END
END