--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_GetNewsExpertByListIds]    Script Date: 6/4/2019 2:59:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
CREATE PROCEDURE [dbo].[CMS_NewsExpert_GetNewsExpertByListIds] --'20190307115504946,20181022095701044,20181022095512794'
    @NewsIds varchar(max)=''
AS 
BEGIN
	DECLARE @search nvarchar(max);
	SET @search = 'SELECT ne.*,e.ExpertName FROM NewsExpert ne INNER JOIN Expert e ON e.Id=ne.ExpertId WHERE ne.NewsId IN('+@NewsIds+')';

	EXEC SP_EXECUTESQL @search;
END

