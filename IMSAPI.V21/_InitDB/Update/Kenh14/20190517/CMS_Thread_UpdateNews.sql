--USE [IMS2_DEMO]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_UpdateNews]    Script Date: 5/17/2019 5:03:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <10:26 13/08/2013>
-- Description:	<Update NewsList into Threadnews>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Thread_UpdateNews]
	@ThreadId bigint,
	@DeleteNewsIds varchar(max) = '',
	@AddNewsIds varchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	DECLARE @ThreadName nvarchar(200)

	SELECT @ThreadName = Name FROM Thread WHERE Id = @ThreadId
	
	IF @ThreadName IS NOT NULL AND @ThreadName <> ''
	BEGIN
		DECLARE @Index int, 
				@TotalRowInTemp int,
				@NewsId bigint;
		-- Delete first
		DECLARE @TblDeleteTemp TABLE (Id int identity(1,1), NewsId bigint)
		INSERT @TblDeleteTemp (NewsId)
		SELECT CONVERT(bigint, part) FROM SplitString(@DeleteNewsIds, ';')
		
		SET @Index = 0
		SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblDeleteTemp), 0)
		WHILE (@Index <= @TotalRowInTemp)
		BEGIN
			SET @NewsId = ISNULL((SELECT NewsId FROM @TblDeleteTemp WHERE Id = @Index), 0)
			IF @NewsId > 0
			BEGIN
				DELETE FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @NewsId

				UPDATE News SET ThreadId=0 WHERE ThreadId = @ThreadId AND Id = @NewsId
			END
			SET @Index = @Index + 1
		END
		
		-- Insert later
		DECLARE @TblAddTemp TABLE (Id int identity(1,1), NewsId bigint)
		INSERT @TblAddTemp (NewsId)
		SELECT CONVERT(bigint, part) FROM SplitString(@AddNewsIds, ';')
		
		SET @Index = 0
		SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblAddTemp), 0)
		WHILE (@Index <= @TotalRowInTemp)
		BEGIN
			SET @NewsId = ISNULL((SELECT NewsId FROM @TblAddTemp WHERE Id = @Index), 0)
			IF @NewsId > 0
			BEGIN
				IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE NewsId = @NewsId AND ThreadId = @ThreadId)
				BEGIN
					INSERT INTO ThreadNews(NewsID, ThreadID)
					VALUES(@NewsId, @ThreadId)
				END
			END
			SET @Index = @Index + 1
		END
	END
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500)
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE 
	BEGIN
		COMMIT TRANSACTION
	END
END