﻿--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsExpert]    Script Date: 6/6/2019 3:28:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsExpert](
	[NewsId] [bigint] NOT NULL,
	[ExpertId] [int] NOT NULL,
	[Status] [int] NULL,
	[SendDate] [datetime] NULL,
	[ConfirmDate] [datetime] NULL,
	[ReturnDate] [datetime] NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsExpert] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsExpert] ADD  CONSTRAINT [DF_NewsExpert_Status]  DEFAULT ((0)) FOR [Status]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: chờ thẩm định, 1: đã được thẩm định, 2: trả lại' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NewsExpert', @level2type=N'COLUMN',@level2name=N'Status'
GO


