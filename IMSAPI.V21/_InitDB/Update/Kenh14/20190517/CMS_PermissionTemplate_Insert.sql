--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_Insert]    Script Date: 5/20/2019 3:41:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
ALTER PROCEDURE [dbo].[CMS_PermissionTemplate_Insert]
@Id int OUTPUT,
@TemplateName nvarchar(200)
AS
BEGIN
	INSERT INTO PermissionTemplate(TemplateName) values(@TemplateName)
	SET @Id = SCOPE_IDENTITY();
END

