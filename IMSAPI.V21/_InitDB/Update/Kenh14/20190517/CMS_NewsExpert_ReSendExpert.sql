--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ReSendExpert]    Script Date: 5/29/2019 4:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-05-29
CREATE PROCEDURE [dbo].[CMS_NewsExpert_ReSendExpert] --0,0,'','',1,20,0
    @ExpertId INT=0,
	@RemindDate datetime,
	@ExpireDate int=24,
	@Note nvarchar(255)=''
AS 
BEGIN
	UPDATE NewsExpert set SendDate=@RemindDate, Note=@Note where ExpertId=@ExpertId and Status=0  and SendDate <= DATEADD(hh, DateDiff(hh, 0, GETDATE()) - @ExpireDate, 0)	
END