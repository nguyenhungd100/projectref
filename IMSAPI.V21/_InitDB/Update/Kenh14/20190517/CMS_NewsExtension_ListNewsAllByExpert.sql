--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsAllByExpert]    Script Date: 5/29/2019 3:47:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsAllByExpert] --5,0,'','',1,20,0
    @ExpertStatus INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select n.Id
		from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
		on 	newstemp.NewsId=n.Id
		inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6,8)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
		SELECT stemp.*,nes.Value as NoteExpert, nesdate.Value as SendDateExpert
		FROM (
			select n.*, nz.ZoneId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum
			from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
			on 	newstemp.NewsId=n.Id
			inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6,8)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) AS stemp
		left join newsextension nes on nes.NewsId=stemp.Id 
		left join newsextension nesdate on nesdate.NewsId=stemp.Id 
		where nes.Type=53 and nesdate.Type=54		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]


--USE [IMS2_suckhoehangngay]
