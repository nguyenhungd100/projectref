--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_ListNewsNotByExpert]    Script Date: 6/11/2019 5:06:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
ALTER PROCEDURE [dbo].[CMS_NewsExpert_ListNewsNotByExpert] --252,0,8,'','',1,20,'',0
    @TopicId INT=0 ,	
	@ZoneId INT=0 ,
	@Status INT=5 ,
	@FromDate datetime='',
	@ToDate datetime='',
    @PageIndex INT ,
    @PageSize INT ,
	@Account varchar(50)='',
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
IF(@TopicId<=0)
	BEGIN
		SELECT  @TotalRow = COUNT(1) from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 			
				where ne.NewsId IS NULL AND n.Status=@Status 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))

		SELECT *
		FROM ( 
		select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
			from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 			
				where ne.NewsId IS NULL AND n.Status=@Status 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))
		
		) AS DATA
		WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
		ORDER BY CreatedDate DESC
	END
	ELSE
	BEGIN
		SELECT  @TotalRow = COUNT(1) from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1  
				where ne.NewsId IS NULL AND n.Status=@Status AND (nt.TopicId=@TopicId) 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))

		SELECT *
		FROM ( 
		select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
			from news n
			left join newsexpert ne on ne.newsid=n.id
			left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1  
				where ne.NewsId IS NULL AND n.Status=@Status AND (nt.TopicId=@TopicId) 
				AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
				AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))
		
		) AS DATA
		WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
		ORDER BY CreatedDate DESC
	END
END