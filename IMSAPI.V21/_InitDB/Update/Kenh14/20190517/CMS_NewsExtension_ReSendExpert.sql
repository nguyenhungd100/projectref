--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ReSendExpert]    Script Date: 5/29/2019 4:22:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-05-29
CREATE PROCEDURE [dbo].[CMS_NewsExtension_ReSendExpert] --0,0,'','',1,20,0
    @ExpertId INT=0,
	@RemindDate nvarchar(50),
	@ExpireDate int=24,
	@Note nvarchar(255)=''
AS 
BEGIN
	UPDATE NewsExtension set Value=@RemindDate from (
		select net.* from
		(select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
		inner join newsextension ne on ne.newsid=abc.newsid
		where ne.type=52  and ne.value=0) as abcd
		inner join newsextension net on net.newsid=abcd.newsid
		where net.type=54  and net.value <= DATEADD(hh, DateDiff(hh, 0, GETDATE()) - @ExpireDate, 0)
	) as abcde 
	where NewsExtension.NewsId=abcde.NewsId and NewsExtension.Type=abcde.Type
END

		