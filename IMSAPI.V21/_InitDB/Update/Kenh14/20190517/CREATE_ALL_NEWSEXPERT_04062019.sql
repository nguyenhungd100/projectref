﻿--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsExpert]    Script Date: 6/6/2019 3:28:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsExpert](
	[NewsId] [bigint] NOT NULL,
	[ExpertId] [int] NOT NULL,
	[Status] [int] NULL,
	[SendDate] [datetime] NULL,
	[ConfirmDate] [datetime] NULL,
	[ReturnDate] [datetime] NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsExpert] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsExpert] ADD  CONSTRAINT [DF_NewsExpert_Status]  DEFAULT ((0)) FOR [Status]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: chờ thẩm định, 1: đã được thẩm định, 2: trả lại' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NewsExpert', @level2type=N'COLUMN',@level2name=N'Status'
GO


--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_ListNewsAllByExpert]    Script Date: 6/3/2019 5:28:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
CREATE PROCEDURE [dbo].[CMS_NewsExpert_ListNewsAllByExpert] --3,0,1,0,'','',1,20,0
    @ExpertId INT=0 ,
	@TopicId INT=0 ,
    @ExpertStatus INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
			AND (ne.Status=@ExpertStatus)

	SELECT *
	FROM ( 
	select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
		,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
		from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
			AND (ne.Status=@ExpertStatus)
		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY SendDateExpert DESC
END


--USE [IMS2_TTVH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsInExpert_ListNewsNotByExpert]    Script Date: 6/3/2019 4:56:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
CREATE PROCEDURE [dbo].[CMS_NewsExpert_ListNewsNotByExpert] --0,0,8,'','',1,20,'',0
    @TopicId INT=0 ,	
	@ZoneId INT=0 ,
	@Status INT=5 ,
	@FromDate datetime='',
	@ToDate datetime='',
    @PageIndex INT ,
    @PageSize INT ,
	@Account varchar(50)='',
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NULL AND n.Status=@Status AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))

	SELECT *
	FROM ( 
	select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
		,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
		from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NULL AND n.Status=@Status AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@Account='' AND 1=1) OR (@Account<>'' AND n.ApprovedBy=@Account))
		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExpert_ListNewsByExpertId]    Script Date: 6/5/2019 4:38:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-03
CREATE PROCEDURE [dbo].[CMS_NewsExpert_ListNewsByExpertId] --4,0,0,'','',1,20,0
    @ExpertId INT=0 ,
	@ExpertStatus INT=0 ,
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
SELECT  @TotalRow = COUNT(1) from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
			AND (ne.Status=@ExpertStatus)

	SELECT *
	FROM ( 
	select n.*,nz.ZoneId,ne.Note as NoteExpert, ne.SendDate as SendDateExpert,ne.Status as StatusExpert, ne.ExpertId
		,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum		
		from news n
		left join newsexpert ne on ne.newsid=n.id
		left join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		left join NewsInTopic nt on nt.NewsId=n.id and nt.IsPrimary=1  
			where ne.NewsId IS NOT NULL AND n.Status in (5,6,8) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate)) 
			AND ((@ExpertId<=0 AND 1=1) OR (@ExpertId>0 AND ne.ExpertId=@ExpertId)) 
			AND (ne.Status=@ExpertStatus)
		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY SendDateExpert DESC
END




