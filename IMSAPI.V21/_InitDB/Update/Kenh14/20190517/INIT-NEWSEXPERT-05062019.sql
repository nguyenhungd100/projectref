--select * from newsexpert
--select * from NewsExtension where Type in(51,52,53,54)

--INIT NEWSEXPERT
BEGIN TRANSACTION
--xoa newisid=0
delete NewsExtension where Type in(51,52,53,54) and NewsId=0
--init newsextension => newsexpert
insert into NewsExpert(NewsId,ExpertId,Status,Note,SendDate)
select a.*,b.Status,c.Note,d.SendDate from 
(select t1.NewsId,t1.Value as ExpertId from NewsExtension t1 where t1.Type=51) as a
inner join (select t1.NewsId,t1.Value as Status from NewsExtension t1 where t1.Type=52) as b
on b.newsid=a.newsid
inner join (select t1.NewsId,t1.Value as Note from NewsExtension t1 where t1.Type=53) as c
on c.newsid=a.newsid
inner join (select t1.NewsId,t1.Value as SendDate from NewsExtension t1 where t1.Type=54) as d
on d.newsid=a.newsid 
WHERE NOT EXISTS(SELECT t2.NewsId FROM NewsExpert t2 WHERE t2.NewsId = a.NewsId)
--xoa newsextension
delete NewsExtension where Type in(51,52,53,54)

COMMIT TRANSACTION
