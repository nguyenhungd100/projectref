--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_Update]    Script Date: 5/17/2019 1:18:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Update thread>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Thread_Update]
	@Id INT,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Title nvarchar(300),
	@Description nvarchar(500),
	@Url varchar(500),
	@Avatar nvarchar(500),
	@HomeAvatar varchar(255),
	@SpecialAvatar varchar(255),
	@IsHot bit,
	@IsOnHome bit,
	@CreatedDate datetime,
	@EditedBy varchar(255),
	@MetaKeyword nvarchar(300),
	@MetaContent nvarchar(300),
	@TemplateId int,
	@Invisibled BIT = 0,
	
	@PrimaryZoneId int,
	@OtherZoneId varchar(1000),
	@RelationThread varchar(1000),

	@NewsCoverId bigint = 0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE Thread
		SET	Name = @Name,
			UnsignName = CASE @UnsignName WHEN '' THEN UnsignName ELSE @UnsignName END,
			Title = @Title,
			Description = @Description,
			Url = @Url,
			Avatar = CASE @Avatar WHEN '' THEN Avatar ELSE @Avatar END,
			HomeAvatar = CASE @HomeAvatar WHEN '' THEN HomeAvatar ELSE @HomeAvatar END,
			SpecialAvatar = CASE @SpecialAvatar WHEN '' THEN SpecialAvatar ELSE @SpecialAvatar END,
			IsHot = @IsHot,
			IsOnHome = @IsOnHome,
			--CreatedDate = CASE @CreatedDate WHEN NULL THEN CreatedDate ELSE @CreatedDate END,
			ModifiedDate = GETDATE(),
			EditedBy = @EditedBy,
			MetaKeyword = @MetaKeyword,
			MetaContent = @MetaContent,
			TemplateId = @TemplateId,
			Invisibled = @Invisibled,
			NewsCoverId = @NewsCoverId
		WHERE (Id = @Id)
		
		/* Xóa dữ liệu bảng ThreadZone */
		DELETE FROM ThreadInZone WHERE ThreadId = @Id
		
		INSERT INTO ThreadInZone (ThreadId, ZoneId, IsPrimary)
		VALUES (@Id, @PrimaryZoneId, 1)
		
		IF @OtherZoneId <> ''
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
			DECLARE @ZoneItemId bigint, @Index int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
					IF NOT EXISTS(SELECT ZoneId FROM ThreadInZone WHERE ZoneId = @ZoneItemId AND ThreadId = @Id)
						INSERT INTO ThreadInZone(ThreadId, ZoneId, IsPrimary) 
						VALUES (@Id, @ZoneItemId, 0);
				  				
				SET @Index = @Index + 1
			END
		END
		
		/* Xóa dữ liệu bảng ThreadRelation */
		DELETE FROM ThreadRelation WHERE ThreadId = @Id
		
		IF @RelationThread <> ''
		BEGIN
			DECLARE @TblTempThread TABLE (Id int identity(1,1), ThreadId bigint)
			DECLARE @ThreadItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempThread(ThreadId) SELECT CONVERT(bigint, Part) FROM SplitString(@RelationThread, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempThread)) 
			BEGIN
				SET @ThreadItemId = (SELECT ThreadId FROM @TblTempThread WHERE Id = @Index)
				IF @ThreadItemId IS NOT NULL AND @ThreadItemId > 0
					IF NOT EXISTS(SELECT ThreadId FROM ThreadRelation WHERE ThreadRelationId = @ThreadItemId AND ThreadId = @Id)
						INSERT INTO ThreadRelation(ThreadId, ThreadRelationId) 
						VALUES (@Id, @ThreadItemId);
				  				
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END




















