--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_GetAllBomByTypeAndZoneId]    Script Date: 5/30/2019 3:38:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-05-22>
-- Description:	<get list news position all bom by type and zoneid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_GetAllBomByTypeAndZoneId] --1, 0
	@TypeId int,
	@ZoneId int
AS
BEGIN	
	SELECT Id, ZoneId, TypeId, [Order], Lockable, Locked, ExpiredLock, NewsIdForBomd, NewsId, Title, 
		SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
		NewsRelation, Source, IsFocus, NewsType, ThreadId, DistributionDate, Url, AllowAutoUpdate, AvatarCustom,
		ViewCount, OriginalId, Type, ScheduleDate
	FROM
		(
			SELECT NP.Id, NP.ZoneId, NP.TypeId, NP.[Order], NP.Lockable, NP.Locked, NP.ExpiredLock, NP.NewsIdForBomd, NP.NewsId, NP.Title, 
				NP.SubTitle, NP.Sapo, NP.Avatar, NP.AvatarDesc, NP.Avatar1, NP.Avatar2, NP.Avatar3, NP.Avatar4, NP.Avatar5, NP.Author, 
				NP.NewsRelation, NP.Source, NP.IsFocus, NP.NewsType, NP.ThreadId, NP.DistributionDate, NP.Url, NP.AllowAutoUpdate, NP.AvatarCustom,
				N.ViewCount, N.OriginalId, NP.Type, NP.ScheduleDate,
				(ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY np.ScheduleDate DESC, np.DistributionDate DESC )) AS ROW
			FROM NewsPosition AS NP LEFT JOIN News AS N ON NP.NewsId = N.Id		
			WHERE
				np.TypeId = @typeId
				AND (@zoneId = 0 OR np.ZoneId = @zoneId)
				--AND np.DistributionDate <= GETDATE()
				--AND (np.[ScheduleDate] IS NULL OR np.[ScheduleDate] <= GETDATE())				
		)TEMP
	WHERE 
		NewsId NOT IN (
			SELECT NewsId
			FROM
			(
				SELECT NewsId,(ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY np.ScheduleDate DESC, np.DistributionDate DESC )) AS ROW
				FROM NewsPosition AS NP
				WHERE
					np.TypeId = @TypeId
					AND np.ZoneId = @ZoneId
					AND (np.[ScheduleDate] IS NULL OR np.[ScheduleDate] <= GETDATE())
			)TEMP
			WHERE 
				ROW = 1
		)
	ORDER BY
		[Order]
END










