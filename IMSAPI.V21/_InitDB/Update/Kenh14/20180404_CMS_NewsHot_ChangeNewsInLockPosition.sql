USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsHot_ChangeNewsInLockPosition]    Script Date: 04/04/2018 17:43:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[CMS_NewsHot_ChangeNewsInLockPosition]
	@NewsHotId INT,
	@NewsId bigint
AS
BEGIN
	--DECLARE @Locked bit
	--SELECT @Locked = Locked FROM NewsTopHot WHERE Id = @NewsHotId
	
	--IF @Locked IS NOT NULL AND @Locked = 1
	--BEGIN
		DECLARE 
				@Title nvarchar(200),
				@Avatar nvarchar(500),
				@Sapo nvarchar(2000),
				@DistributionDate datetime,
				@Url nvarchar(500),
				@IsBreakingNews bit,
				@ZoneName nvarchar(255),
				@ZoneUrl nvarchar(100),
				@DisplayStyle int,
				@NewsRelation nvarchar(max),
				@NewsType int,
				@Type int,
				@SubTitle nvarchar(250),
				@TagSubTitleId int,
				@InitSapo nvarchar(500)
	
		SELECT	
				@Title = NP.Title, 
				@Avatar = NP.Avatar, 
				@Sapo = NP.Sapo, 
				@DistributionDate = NP.DistributionDate, 
				@Url = NP.Url, 
				@IsBreakingNews = NP.IsBreakingNews, 
				@ZoneName = Z.Name, 
				@ZoneUrl = Z.ShortURL,
				@DisplayStyle = NP.DisplayStyle,
				@NewsRelation = NP.NewsRelation,
				@NewsType = NP.NewsType,
				@Type = NP.Type,
				@SubTitle = NP.SubTitle,
				@TagSubTitleId = NP.TagSubTitleId,
				@InitSapo = NP.InitSapo
		FROM NewsPublish AS NP INNER JOIN
			Zone AS Z ON NP.ZoneId = Z.Id
		WHERE (NP.IsPrimary = 1) AND (NP.NewsId = @NewsId)
	
		IF @Title IS NOT NULL AND @Title <> ''
			UPDATE NewsTopHot
			SET NewsId=@NewsId,
				Title = @Title, 
				PictureUrl = @Avatar, 
				Sapo = @Sapo, 
				PublishDate = @DistributionDate,
				Url = @Url,  
				IsBreakingNews = @IsBreakingNews,
				CategoryName = @ZoneName, 
				CategoryHref = @ZoneUrl, 
				LastModifiedDate = GETDATE(),
				DisplayStyle = @DisplayStyle,
				NewsRelation = @NewsRelation,
				NewsType = @NewsType,
				Type = @Type,
				SubTitle = @SubTitle,
				TagSubTitleId = @TagSubTitleId,
				InitSapo = @InitSapo
			WHERE Id = @NewsHotId
	--END
END






