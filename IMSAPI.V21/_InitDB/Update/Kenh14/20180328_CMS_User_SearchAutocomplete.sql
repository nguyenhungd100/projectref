USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_SeachUser]    Script Date: 03/29/2018 16:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-03-28>
-- Description:	<search user auto>
-- =============================================
CREATE Procedure [dbo].[CMS_User_SearchAutocomplete]  
 @ZoneId int,  
 @Keyword nvarchar(50),  
 @IsShowTKTS bit  
AS  
BEGIN  
 DECLARE @ZoneIds varchar(1000);  
 DECLARE @ParentId int;  
   
 IF @Keyword <> ''  
  SET @Keyword = '%' + @Keyword + '%'  
   
   
 SELECT @ParentId = ParentId FROM Zone WHERE Id = @ZoneId;  
   
 SET @ZoneIds = dbo.CMS_fGetListZoneId(@ZoneId)  
 IF(ISNULL(@ParentId,0) > 0)  
 BEGIN  
  IF(@ZoneIds <> '')  
   SET @ZoneIds = CONVERT(varchar(10), @ParentId) +','+ @ZoneIds;  
  ELSE  
   SET @ZoneIds = CONVERT(varchar(10), @ParentId)  
 END  
    
 SELECT DISTINCT Top(15) [Id],[UserName]  
 FROM [User] U  
 INNER JOIN UserPermission P ON  U.Id = P.UserId  AND P.PermissionId IN (1,2,3)
 AND (PATINDEX(@Keyword, U.UserName) > 0 OR PATINDEX(@Keyword, U.FullName) > 0 or @Keyword='')   
 AND (U.IsFullZone = 1 OR PATINDEX('%,' + CONVERT(varchar(10), P.ZoneId) + ',%', ',' + @ZoneIds + ',') > 0)   
 WHERE IsRole != 1 AND IsRole != 3 AND IsRole != 2
 UNION
 SELECT DISTINCT Top(15) [Id],[UserName]
 FROM [User] U
 WHERE U.IsFullZone = 1 
 AND IsRole = 2 AND @IsShowTKTS = 1
 AND (PATINDEX(@Keyword, U.UserName) > 0 OR PATINDEX(@Keyword, U.FullName) > 0) 
END
















