--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Update]    Script Date: 11/13/2018 10:53:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Update] 
	@Id int,
	@ZoneId int= 0,
	@TopicId int= 0,
	@Type int =0,	
	@SortOrder int=0
AS
BEGIN
    UPDATE BoxTopicOnPageEmbed SET 
			ZoneId=@ZoneId,	
			TopicId=@TopicId,	
			Type=@Type,			
			SortOrder=@SortOrder,
			LastModifiedDate=getdate()
	WHERE Id=@Id
END


