--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxTopicOnPageEmbed]    Script Date: 11/13/2018 11:05:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxTopicOnPageEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL,
	[TopicId] [int] NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxTopicOnPageEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Delete]    Script Date: 11/13/2018 10:49:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Delete]
@Id int
AS
DELETE BoxTopicOnPageEmbed where Id =@Id


--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Insert]    Script Date: 11/13/2018 10:51:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Insert]	
	@Id int OUTPUT,
	@ZoneId int= 0,
	@TopicId int= 0,
	@Type int =0,	
	@SortOrder int=0
AS
BEGIN 
	INSERT INTO BoxTopicOnPageEmbed (
			ZoneId,
			TopicId,	
			Type,			
			SortOrder, 			
			LastModifiedDate
		)
		VALUES(
			@ZoneId,
			@TopicId,	
			@Type,					
			@SortOrder,
			getdate()
		)

	SET @Id = SCOPE_IDENTITY();
END



--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Search]    Script Date: 11/13/2018 10:52:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Search]
@ZoneId int,
@Type int
AS
BEGIN	
	SELECT TOP(20) BT.Id, BT.ZoneId,BT.TopicId, BT.SortOrder, BT.Type, TP.Logo, TP.TopicName, TP.DisplayName 
	FROM BoxTopicOnPageEmbed as BT
		inner join Topic as TP on BT.TopicId =TP.Id
	where BT.ZoneId =@ZoneId and BT.Type=@Type
	ORDER BY BT.SortOrder ASC	
END


--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Update]    Script Date: 11/13/2018 10:53:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Update] 
	@Id int,
	@ZoneId int= 0,
	@TopicId int= 0,
	@Type int =0,	
	@SortOrder int=0
AS
BEGIN
    UPDATE BoxTopicOnPageEmbed SET 
			ZoneId=@ZoneId,	
			TopicId=@TopicId,	
			Type=@Type,			
			SortOrder=@SortOrder,
			LastModifiedDate=getdate()
	WHERE Id=@Id
END


