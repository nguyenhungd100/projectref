--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Search]    Script Date: 11/13/2018 10:52:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Search]
@ZoneId int,
@Type int
AS
BEGIN	
	SELECT TOP(20) BT.Id, BT.ZoneId,BT.TopicId, BT.SortOrder, BT.Type, TP.Logo, TP.TopicName, TP.DisplayName 
	FROM BoxTopicOnPageEmbed as BT
		inner join Topic as TP on BT.TopicId =TP.Id
	where BT.ZoneId =@ZoneId and BT.Type=@Type
	ORDER BY BT.SortOrder ASC	
END