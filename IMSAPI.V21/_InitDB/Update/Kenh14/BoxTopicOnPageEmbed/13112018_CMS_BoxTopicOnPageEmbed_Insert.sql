--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Insert]    Script Date: 11/13/2018 10:51:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_Insert]	
	@Id int OUTPUT,
	@ZoneId int= 0,
	@TopicId int= 0,
	@Type int =0,	
	@SortOrder int=0
AS
BEGIN 
	INSERT INTO BoxTopicOnPageEmbed (
			ZoneId,
			TopicId,	
			Type,			
			SortOrder, 			
			LastModifiedDate
		)
		VALUES(
			@ZoneId,
			@TopicId,	
			@Type,					
			@SortOrder,
			getdate()
		)

	SET @Id = SCOPE_IDENTITY();
END