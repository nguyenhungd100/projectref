--USE [IMS2_THOIDAI_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Update]    Script Date: 11/15/2018 4:06:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--15112018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_UpdateBoxTopicOnPageEmbed] 
	@ListOfPriority nvarchar(MAX),
	@ZoneId int,
	@Type int
AS
BEGIN
    DELETE BoxTopicOnPageEmbed WHERE ZoneId =@ZoneId and Type=@Type
	DECLARE @Priority int, 
			@TopicId bigint
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	

	FETCH NEXT FROM PriorityCursor INTO @Priority, @TopicId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM BoxTopicOnPageEmbed WHERE TopicId = @TopicId and ZoneId =@ZoneId and Type =@Type)
			INSERT INTO BoxTopicOnPageEmbed (ZoneId,TopicId,SortOrder,Type) values( @ZoneId,@TopicId,@Priority,@Type)
			FETCH NEXT FROM PriorityCursor INTO @Priority, @TopicId 
	END   

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor


END


