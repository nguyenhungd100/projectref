USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBanner_Update]    Script Date: 03/20/2018 11:41:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-10-22>
-- Editted: 2018-03-20 - chinhnb
-- Description:	<Update box banner>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_BoxBanner_Update]
	@Id int,
	@ZoneId int,
	@Position int,
	@Priority int,
	@Avatar nvarchar(500),
	@Title nvarchar(250),
	@Sapo nvarchar(MAX),
	@Url varchar(500),
	@Status int,
	@DisplayStyle int = 0,
	@Type int =0,
	@ObjectId bigint,
	@ZoneIdList varchar(500) = ''
AS
BEGIN
	UPDATE BoxBanner
	SET ZoneId = @ZoneId, 
		Position = @Position, 
		Priority = @Priority, 
		Avatar = @Avatar, 
		Title = @Title, 
		Sapo = @Sapo, 
		Url = @Url, 
		Status = @Status,
		DisplayStyle = @DisplayStyle,
		Type = @Type,
		ObjectId = @ObjectId
				
	WHERE (Id = @Id)
	
		DECLARE @Index int;
		DELETE FROM BoxBannerInZone  WHERE BoxBannerId = @Id
		
--		INSERT INTO BoxBannerInZone(BoxBannerId, ZoneId) VALUES (@Id, @ZoneId);
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > -1 --AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM BoxBannerInZone WHERE ZoneId = @ZoneItemId AND BoxBannerId = @Id)
						INSERT INTO BoxBannerInZone(BoxBannerId, ZoneId) VALUES (@Id, @ZoneItemId);
				SET @Index = @Index + 1
			END
		END
END













