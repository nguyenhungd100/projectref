--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_GetById]    Script Date: 11/16/2018 4:04:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_GetById]
	@Id int
AS
BEGIN
	SELECT * 
	FROM QuickAnswer
	WHERE Id = @Id
END
