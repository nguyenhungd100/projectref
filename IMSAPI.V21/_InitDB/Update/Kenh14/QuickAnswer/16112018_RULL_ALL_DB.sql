----USE [IMS2_BVHTTDL_EXT]
GO

/****** Object:  Table [dbo].[QuickAnswer]    Script Date: 11/16/2018 4:03:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[QuickAnswer](
	[Id] [int] NOT NULL,
	[FullName] [nvarchar](100) NULL,
	[Address] [nvarchar](250) NULL,
	[Email] [varchar](250) NULL,
	[Phone] [varchar](20) NULL,
	[Title] [nvarchar](1000) NULL,
	[AttachFile] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[QuestedDate] [datetime] NULL,
	[AnsweredBy] [nvarchar](100) NULL,
	[AnsweredDate] [datetime] NULL,
	[ModifiedBy] [nvarchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[QuestContent] [nvarchar](max) NULL,
	[AnswerContent] [nvarchar](max) NULL,
 CONSTRAINT [PK_QuickAnswer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[QuickAnswer] ADD  CONSTRAINT [DF_QuickAnswer_Status]  DEFAULT ((1)) FOR [Status]
GO

--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_GetById]    Script Date: 11/16/2018 4:04:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_GetById]
	@Id int
AS
BEGIN
	SELECT * 
	FROM QuickAnswer
	WHERE Id = @Id
END


--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Search]    Script Date: 11/16/2018 3:59:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Search]
	@Keyword nvarchar(200) = '',	
	@Status int = 0,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	SELECT @TotalRow=COUNT(*) FROM QuickAnswer	
	WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		
	IF @PageIndex <= 1
		SELECT TOP(@PageSize) * FROM QuickAnswer
		WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		ORDER BY QuestedDate DESC
	ELSE
		SELECT * FROM
		(
			SELECT *, row_number() OVER (ORDER BY QuestedDate DESC) AS RowNum
			FROM QuickAnswer
			WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
		ORDER BY QuestedDate DESC
END


--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Delete]    Script Date: 11/16/2018 4:20:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Delete]
	@Id int,
	@DeletedBy varchar(200)
AS
BEGIN
	UPDATE QuickAnswer
	SET Status = 2, ModifiedBy = @DeletedBy, ModifiedDate = GETDATE()
	WHERE Id = @Id
END

--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_DeleteByIdList]    Script Date: 11/16/2018 4:22:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[CMS_QuickAnswer_DeleteByIdList]
	@IdList varchar(500) = '',
	@DeletedBy varchar(200)
AS
	BEGIN TRANSACTION
	
	IF LTRIM(RTRIM(@IdList)) = ''
	BEGIN
		RAISERROR ('Invalid QuickAnswerId list', 16, 1);
		ROLLBACK TRANSACTION
	END ELSE
	BEGIN
		DECLARE @TblQuickAnswer TABLE(Id int identity(1,1), QuickAnswerId bigint)
		INSERT @TblQuickAnswer (QuickAnswerId) SELECT CONVERT(int, Part) FROM SplitString(@IdList, ';')
		DECLARE @Index int, @QuickAnswerId bigint;
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(Id) FROM @TblQuickAnswer))
		BEGIN
			SET @QuickAnswerId = (SELECT QuickAnswerId FROM @TblQuickAnswer WHERE Id = @Index)
			IF @QuickAnswerId > 0 
			BEGIN			
				UPDATE QuickAnswer
				SET Status = 2, ModifiedBy = @DeletedBy, ModifiedDate = GETDATE()
				WHERE Id = @QuickAnswerId
			END
			SET @Index = @Index + 1;
		END
	END
	
	IF @@ERROR <> 0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION


--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Receive]    Script Date: 11/16/2018 4:45:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Receive]
	@QuickAnswerId int,	
	@ReceivedBy	varchar(200)
AS
BEGIN
	UPDATE QuickAnswer
	SET ModifiedBy = @ReceivedBy,
		ModifiedDate = GETDATE(),
		Status = 1
	WHERE Id = @QuickAnswerId
END


--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Insert]    Script Date: 11/17/2018 9:26:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Insert]
	@Id int,
	@FullName nvarchar(100),
	@Address nvarchar(250),
	@Email varchar(250),
	@Phone varchar(20),
	@Title nvarchar(1000),
	@AttachFile nvarchar(500),
	@Status int,
	@QuestedDate datetime,
	@AnsweredBy nvarchar(100),
	@AnsweredDate datetime,
	@ModifiedBy nvarchar(100),
	@ModifiedDate datetime,
	@QuestContent nvarchar(max),
	@AnswerContent nvarchar(max)=''
AS
BEGIN
	INSERT INTO QuickAnswer (Id, FullName, Address, Email, Phone, Title, AttachFile, Status, QuestedDate, AnsweredBy, AnsweredDate, ModifiedBy, ModifiedDate, QuestContent, AnswerContent)
	VALUES (@Id, @FullName, @Address, @Email, @Phone, @Title, @AttachFile, @Status, @QuestedDate, @AnsweredBy, @AnsweredDate, @ModifiedBy, @ModifiedDate, @QuestContent, @AnswerContent)	
END


--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Insert]    Script Date: 11/17/2018 9:26:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Update]
	@Id int,
	--@FullName nvarchar(100),
	--@Address nvarchar(250),
	--@Email varchar(250),
	--@Phone varchar(20),
	@Title nvarchar(1000),
	--@AttachFile nvarchar(500),
	--@Status int,
	--@QuestedDate datetime,
	--@AnsweredBy nvarchar(100),
	--@AnsweredDate datetime,
	@ModifiedBy nvarchar(100),
	@ModifiedDate datetime,
	@QuestContent nvarchar(max),
	@AnswerContent nvarchar(max)=''
AS
BEGIN
	UPDATE QuickAnswer set Title=@Title,ModifiedBy=@ModifiedBy,ModifiedDate=@ModifiedDate,QuestContent=@QuestContent,AnswerContent=@AnswerContent
	Where Id=@Id
END
