--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Search]    Script Date: 11/19/2018 11:42:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_QuickAnswer_Search]
	@Keyword nvarchar(200) = '',	
	@Status int = 0,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	SELECT @TotalRow=COUNT(*) FROM QuickAnswer	
	WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		
	IF @PageIndex <= 1
		SELECT TOP(@PageSize) * FROM QuickAnswer
		WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		ORDER BY QuestedDate DESC
	ELSE
		SELECT * FROM
		(
			SELECT *, row_number() OVER (ORDER BY QuestedDate DESC) AS RowNum
			FROM QuickAnswer
			WHERE (@Status = -1 OR Status = @Status) AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
		ORDER BY QuestedDate DESC
END
