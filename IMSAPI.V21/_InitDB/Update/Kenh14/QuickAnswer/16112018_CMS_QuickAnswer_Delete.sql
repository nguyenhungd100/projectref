--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Delete]    Script Date: 11/16/2018 4:20:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Delete]
	@Id int,
	@DeletedBy varchar(200)
AS
BEGIN
	UPDATE QuickAnswer
	SET Status = 2, ModifiedBy = @DeletedBy, ModifiedDate = GETDATE()
	WHERE Id = @Id
END