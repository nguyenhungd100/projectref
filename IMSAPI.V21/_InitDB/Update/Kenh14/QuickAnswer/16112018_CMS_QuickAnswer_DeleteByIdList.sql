--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_DeleteByIdList]    Script Date: 11/16/2018 4:22:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[CMS_QuickAnswer_DeleteByIdList]
	@IdList varchar(500) = '',
	@DeletedBy varchar(200)
AS
	BEGIN TRANSACTION
	
	IF LTRIM(RTRIM(@IdList)) = ''
	BEGIN
		RAISERROR ('Invalid QuickAnswerId list', 16, 1);
		ROLLBACK TRANSACTION
	END ELSE
	BEGIN
		DECLARE @TblQuickAnswer TABLE(Id int identity(1,1), QuickAnswerId bigint)
		INSERT @TblQuickAnswer (QuickAnswerId) SELECT CONVERT(int, Part) FROM SplitString(@IdList, ';')
		DECLARE @Index int, @QuickAnswerId bigint;
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(Id) FROM @TblQuickAnswer))
		BEGIN
			SET @QuickAnswerId = (SELECT QuickAnswerId FROM @TblQuickAnswer WHERE Id = @Index)
			IF @QuickAnswerId > 0 
			BEGIN			
				UPDATE QuickAnswer
				SET Status = 2, ModifiedBy = @DeletedBy, ModifiedDate = GETDATE()
				WHERE Id = @QuickAnswerId
			END
			SET @Index = @Index + 1;
		END
	END
	
	IF @@ERROR <> 0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

