--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Insert]    Script Date: 11/17/2018 9:26:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Update]
	@Id int,
	--@FullName nvarchar(100),
	--@Address nvarchar(250),
	--@Email varchar(250),
	--@Phone varchar(20),
	@Title nvarchar(1000),
	--@AttachFile nvarchar(500),
	--@Status int,
	--@QuestedDate datetime,
	--@AnsweredBy nvarchar(100),
	--@AnsweredDate datetime,
	@ModifiedBy nvarchar(100),
	@ModifiedDate datetime,
	@QuestContent nvarchar(max),
	@AnswerContent nvarchar(max)=''
AS
BEGIN
	UPDATE QuickAnswer set Title=@Title,ModifiedBy=@ModifiedBy,ModifiedDate=@ModifiedDate,QuestContent=@QuestContent,AnswerContent=@AnswerContent
	Where Id=@Id
END