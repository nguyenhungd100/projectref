--USE [IMS2_BVHTTDL_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickAnswer_Receive]    Script Date: 11/16/2018 4:45:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_QuickAnswer_Receive]
	@QuickAnswerId int,	
	@ReceivedBy	varchar(200)
AS
BEGIN
	UPDATE QuickAnswer
	SET ModifiedBy = @ReceivedBy,
		ModifiedDate = GETDATE(),
		Status = 1
	WHERE Id = @QuickAnswerId
END
