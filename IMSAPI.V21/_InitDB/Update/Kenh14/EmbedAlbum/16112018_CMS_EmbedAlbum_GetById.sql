--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EmbedAlbum_GetById]    Script Date: 11/16/2018 11:34:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_EmbedAlbum_GetById]
	@Id int
AS
BEGIN
	SELECT Id, Type, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, Description
	FROM EmbedAlbum
	WHERE Id = @Id
END




