--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EmbedAlbum_Update]    Script Date: 11/16/2018 11:34:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_EmbedAlbum_Update]
	@Id int,
	@Type int,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@LastModifiedBy nvarchar(100),
	@Status int,
	@Description nvarchar(max)=''
AS
BEGIN
	UPDATE EmbedAlbum
	SET Type = @Type, 
		ZoneId = @ZoneId, 
		Name = @Name, 
		Avatar = @Avatar, 
		LastModifiedBy = @LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		Status = @Status,
		Description=@Description
	WHERE (Id = @Id)
END




