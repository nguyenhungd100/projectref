--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EmbedAlbum_Search]    Script Date: 11/16/2018 11:35:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_EmbedAlbum_Search]
	@Keyword nvarchar(200),
	@Type int,
	@ZoneId int,
	@Status int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SELECT @TotalRow = COUNT(*) 
	FROM EmbedAlbum
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
		AND (@Type <= 0 OR (@Type > 0 AND (Type = @Type)))
		AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (ZoneId = @ZoneId)))
		AND (@Status <= 0 OR (@Status > 0 AND (Status = @Status)))
	
	IF @PageIndex <= 0
		SELECT TOP(@PageSize) Id, Type, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
			LastModifiedBy, LastModifiedDate, Status, Description
		FROM EmbedAlbum
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
			AND (@Type <= 0 OR (@Type > 0 AND (Type = @Type)))
			AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (ZoneId = @ZoneId)))
			AND (@Status <= 0 OR (@Status > 0 AND (Status = @Status)))
		ORDER BY CreatedDate DESC
	ELSE
		SELECT *
		FROM (
			SELECT Id, Type, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
				LastModifiedBy, LastModifiedDate, Status, Description,
				ROW_NUMBER() OVER(ORDER BY CreatedDate DESC) AS RowNum
			FROM EmbedAlbum
			WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
				AND (@Type <= 0 OR (@Type > 0 AND (Type = @Type)))
				AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (ZoneId = @ZoneId)))
				AND (@Status <= 0 OR (@Status > 0 AND (Status = @Status)))
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
		ORDER BY CreatedDate DESC
END




