--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EmbedAlbum_Insert]    Script Date: 11/16/2018 11:33:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_EmbedAlbum_Insert]
	@Id int out,
	@Type int,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@CreatedBy nvarchar(100),
	@Status int,
	@Description nvarchar(max)=''
AS
BEGIN
	INSERT INTO EmbedAlbum (Type, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, Description)
	VALUES (@Type,@ZoneId,@Name,@Avatar,@CreatedBy, GETDATE(),
		@CreatedBy, GETDATE(),@Status, @Description)
	
	SET @Id = SCOPE_IDENTITY()
END




