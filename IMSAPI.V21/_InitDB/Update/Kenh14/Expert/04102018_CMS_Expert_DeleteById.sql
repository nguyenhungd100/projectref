--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_DeleteById]    Script Date: 10/4/2018 5:20:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_DeleteById] @Id INT
AS 
    IF NOT EXISTS ( SELECT  Id
                    FROM    Expert
                    WHERE   Id = @Id ) 
        BEGIN
            RAISERROR ('This Expert does not exist in database', 16, 1)
        END
    ELSE 
        BEGIN
            DELETE  FROM Expert WHERE Id = @Id

			--delete lien quan
			DELETE FROM ExpertInNews WHERE ExpertId = @Id
        END







