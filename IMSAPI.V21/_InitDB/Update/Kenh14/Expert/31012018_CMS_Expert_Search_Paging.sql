--USE [IMS2_VIETNAMBIZ]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 1/31/2019 4:37:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
    BEGIN
		SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

        SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                  FROM      Expert
                  WHERE     ExpertName LIKE '%' + @Keyword + '%'
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
	
    END







