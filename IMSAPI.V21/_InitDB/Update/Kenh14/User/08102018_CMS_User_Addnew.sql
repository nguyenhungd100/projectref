--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_Addnew]    Script Date: 10/8/2018 4:44:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass,TelegramId,DepartmentId)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate(),@TelegramId,@DepartmentId)
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

