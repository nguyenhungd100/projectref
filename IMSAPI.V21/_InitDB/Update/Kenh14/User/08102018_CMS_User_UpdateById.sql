--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateById]    Script Date: 10/8/2018 5:02:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE(),
				TelegramId=@TelegramId,
				DepartmentId=@DepartmentId
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END


