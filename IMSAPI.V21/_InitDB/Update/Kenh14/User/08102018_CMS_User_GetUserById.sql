--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_GetUserById]    Script Date: 10/8/2018 5:04:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode,U.TelegramId,U.DepartmentId
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END


