use [IMS2_KENH14_DEV]
Go
--chinhnb
--14/03/2015
CREATE PROCEDURE [dbo].[CMS_ContentWarning_GetByListNewsId]
	@ListNewsId NVARCHAR(1000)
AS
BEGIN
	SELECT NewsId
		  ,Level
		  ,LimitedCategory
		  ,ListTopic
		  ,ListEnity
		  ,ListKeyword
		  ,ListTopicKeyword
		  ,Status
	FROM ContentWarning C
	INNER JOIN CmsDL_fSplitString(@ListNewsId, ',') S
	ON C.NewsId = CONVERT(bigint, S.part)
END