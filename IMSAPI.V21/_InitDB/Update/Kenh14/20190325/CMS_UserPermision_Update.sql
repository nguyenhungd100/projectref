--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_UserPermision_Update]    Script Date: 3/25/2019 1:18:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit:CHINHNB 25-03-2019
-- Create date: <2012-09-24>
-- Description:	<Add new user permission information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_UserPermision_Update] --57,25,25
	@UserId int,
	@PermissionId int,
	@ZoneId int
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS(SELECT 1 FROM UserPermission WHERE UserId = @UserId AND PermissionId = @PermissionId AND ZoneId = @ZoneId)
			INSERT INTO UserPermission (UserId, PermissionId, ZoneId)
			VALUES (@UserId, @PermissionId, @ZoneId)
		ELSE
			UPDATE UserPermission set UserId=@UserId,PermissionId=@PermissionId,ZoneId=@ZoneId WHERE UserId = @UserId AND PermissionId = @PermissionId AND ZoneId = @ZoneId
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END
