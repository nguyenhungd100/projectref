USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_UpdateStatusIms]    Script Date: 03/20/2018 10:53:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_UpdateStatusIms]
    @ImsNewsId BIGINT ,
    @Status INT ,
    @Id INT
AS 
    UPDATE  dbo.FeedBackNews
    SET     News_Status = @Status ,
            ImsNewsId = @ImsNewsId
    WHERE   FeedBack_ID = @Id