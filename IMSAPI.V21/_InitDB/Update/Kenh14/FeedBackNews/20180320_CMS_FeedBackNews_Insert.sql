USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_Insert]    Script Date: 03/20/2018 10:42:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_Insert]
(
@FeedBack_ID bigint OUT,
@News_Id bigint,
@Cat_ID int,
@News_Title nvarchar(500),
@News_Subtitle nvarchar(500),
@News_Image nvarchar(200),
@News_ImageNote ntext,
@News_Source nvarchar(200),
@News_InitContent nvarchar(MAX),
@News_Content ntext  ,
@News_Athor nvarchar (200) ,
@News_Approver nvarchar (200) ,
@News_Status int  ,
@News_PublishDate datetime  ,
@News_isFocus bit  ,
@News_Mode int  ,
@News_Relation nvarchar (1000) ,
@News_ModifedDate datetime  ,
@News_OtherCat nvarchar (50) ,
@isComment bit  ,
@WordCount int  ,
@FullName nvarchar(150),
@Email nvarchar(150),
@Phone nvarchar(50),
@Address nvarchar(500),
@FeedBack_Count int  ,
@FeedBack_ModifedDate datetime 
)
AS

BEGIN 
		INSERT INTO FeedBackNews
		(
		   [News_ID]
		  ,[Cat_ID]
		  ,[News_Title]
		  ,[News_Subtitle]
		  ,[News_Image]
		  ,[News_ImageNote]
		  ,[News_Source]
		  ,[News_InitContent]
		  ,[News_Content]
		  ,[News_Athor]
		  ,[News_Approver]
		  ,[News_Status]
		  ,[News_PublishDate]
		  ,[News_isFocus]
		  ,[News_Mode]
		  ,[News_Relation]
		  ,[News_Rate]
		  ,[News_ModifedDate]
		  ,[News_OtherCat]
		  ,[isComment]
		  ,[isUserRate]
		  ,[Template]
		  ,[Icon]
		  ,[Nhuanbut]
		  ,[News_Relation1]
		  ,[WordCount]
		  ,[Note]
		  ,[Meta_Title]
		  ,[Meta_Description]
		  ,[FullName]
		  ,[Email]
		  ,[Phone]
		  ,[Address]
		  ,[FeedBack_Count]
		  ,[FeedBack_ModifedDate]
		
		)
		VALUES
		(
		   @News_ID 
		  ,@Cat_ID 
		  ,@News_Title 
		  ,@News_Subtitle 
		  ,@News_Image 
		  ,@News_ImageNote 
		  ,@News_Source 
		  ,@News_InitContent 
		  ,@News_Content 
		  ,@News_Athor 
		  ,@News_Approver 
		  ,@News_Status 
		  ,@News_PublishDate 
		  ,@News_isFocus 
		  ,@News_Mode 
		  ,@News_Relation 
		  ,0 
		  ,@News_ModifedDate 
		  ,@News_OtherCat 
		  ,0 
		  ,0 
		  ,1 
		  ,1 
		  ,0 
		  ,'' 
		  ,@WordCount 
		  ,'' 
		  ,'' 
		  ,''
		  ,@FullName
		  ,@Email
		  ,@Phone
		  ,@Address
		  ,@FeedBack_Count 
		  ,GETDATE()
		)
		
		SET @FeedBack_ID = SCOPE_IDENTITY();

END
