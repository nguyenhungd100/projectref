USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_GetByFeedId]    Script Date: 03/20/2018 10:37:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_GetByFeedId]
@FeedBackId bigint
AS
BEGIN 

	SELECT *,
	(SELECT Count(FeedBack_ID) FROM FeedBackNews AS P WHERE P.News_ID=@FeedBackId AND News_Status = 0) AS  TotalRowChildren
	FROM FeedBackNews
	WHERE FeedBack_ID=@FeedBackId 
	ORDER BY FeedBack_ModifedDate DESC

END
