USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_News_Search]    Script Date: 03/20/2018 10:43:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_News_Search]
    @Keyword NVARCHAR(300) ,
    @PageIndex INT ,
    @PageSize INT ,
    @Status INT ,
    @TotalRow INT OUT
AS 
    SELECT  @TotalRow = COUNT(0)
    FROM    dbo.FeedBackNews
    WHERE   News_Title LIKE '%' + @Keyword + '%'
            AND News_Status = @Status
    SELECT  *
    FROM    ( SELECT    FeedBack_ID ,
                        News_ID ,
                        Cat_ID ,
                        News_Title ,
                        News_Subtitle ,
                        News_Image ,
                        News_ImageNote ,
                        News_Source ,
                        News_InitContent ,
                        News_Content ,
                        News_Athor ,
                        News_Approver ,
                        News_Status ,
                        News_PublishDate ,
                        News_isFocus ,
                        News_Mode ,
                        News_Relation ,
                        News_Rate ,
                        News_ModifedDate ,
                        News_OtherCat ,
                        isComment ,
                        isUserRate ,
                        Template ,
                        Icon ,
                        Nhuanbut ,
                        News_Relation1 ,
                        WordCount ,
                        Note ,
                        Meta_Title ,
                        Meta_Description ,
                        FullName ,
                        Email ,
                        Phone ,
                        Address ,
                        FeedBack_Count ,
                        FeedBack_ModifedDate ,
                        ImsNewsId ,
                        ROW_NUMBER() OVER ( ORDER BY FeedBack_ID DESC ) AS RowNum
              FROM      dbo.FeedBackNews
              WHERE     News_Title LIKE '%' + @Keyword + '%'
                        AND News_Status = @Status
            ) abc
    WHERE   RowNum BETWEEN ( @PageIndex - 1 ) * @PageSize
                   AND     ( @PageIndex * @PageSize )