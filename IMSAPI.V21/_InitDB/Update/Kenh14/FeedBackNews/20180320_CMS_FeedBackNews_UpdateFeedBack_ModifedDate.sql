USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_UpdateFeedBack_ModifedDate]    Script Date: 03/20/2018 10:45:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_UpdateFeedBack_ModifedDate]
@FeedBack_ID bigint
AS
BEGIN
	IF @FeedBack_ID >0 
	BEGIN
    	 UPDATE FeedBackNews SET FeedBack_ModifedDate=GETDATE() WHERE FeedBack_ID=@FeedBack_ID and News_ID =0
	END
		

END
