USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedbackNews_Search]    Script Date: 03/20/2018 10:44:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedbackNews_Search]
    @Keyword NVARCHAR(255) ,
    @CatId INT ,
    @Status INT
AS 
    SELECT   FeedBack_ID ,
            News_ID ,
            Cat_ID ,
            News_Title ,
            News_Subtitle ,
            News_Image ,
            News_ImageNote ,
            News_Source ,
            News_InitContent ,
            News_Content ,
            News_Athor ,
            News_Approver ,
            News_Status ,
            News_PublishDate ,
            News_isFocus ,
            News_Mode ,
            News_Relation ,
            News_Rate ,
            News_ModifedDate ,
            News_OtherCat ,
            isComment ,
            isUserRate ,
            Template ,
            Icon ,
            Nhuanbut ,
            News_Relation1 ,
            WordCount ,
            Note ,
            Meta_Title ,
            Meta_Description ,
            FullName ,
            Email ,
            Phone ,
            Address ,
            FeedBack_Count ,
            FeedBack_ModifedDate ,
            ImsNewsId
    FROM    dbo.FeedBackNews
    WHERE   News_Title LIKE '%' + @Keyword + '%'
            AND ( Cat_ID = @CatId
                  OR @CatId = 0
                )
            AND ( News_Status = @Status
                  OR @Status = 0
                )
