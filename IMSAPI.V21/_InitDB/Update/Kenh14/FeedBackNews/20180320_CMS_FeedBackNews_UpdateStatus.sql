USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_UpdateStatus]    Script Date: 03/20/2018 10:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_UpdateStatus]
	@FeedBackId bigint,
	@News_Status int
AS
BEGIN 
	UPDATE 
		FeedBackNews 
	SET News_Status = @News_Status 
	WHERE FeedBack_ID =@FeedBackId
END
