USE [IMS_KENH14_EXT]
GO

/****** Object:  Table [dbo].[FeedBackNews]    Script Date: 03/20/2018 09:36:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE TABLE [dbo].[FeedBackNews](
	[FeedBack_ID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[News_ID] [bigint] NOT NULL,
	[Cat_ID] [int] NULL,
	[News_Title] [nvarchar](500) NULL,
	[News_Subtitle] [nvarchar](500) NULL,
	[News_Image] [nvarchar](200) NULL,
	[News_ImageNote] [ntext] NULL,
	[News_Source] [nvarchar](200) NULL,
	[News_InitContent] [nvarchar](max) NULL,
	[News_Content] [ntext] NULL,
	[News_Athor] [nvarchar](200) NULL,
	[News_Approver] [nvarchar](200) NULL,
	[News_Status] [int] NULL,
	[News_PublishDate] [datetime] NULL,
	[News_isFocus] [bit] NULL,
	[News_Mode] [int] NULL,
	[News_Relation] [nvarchar](1000) NULL,
	[News_Rate] [float] NULL,
	[News_ModifedDate] [datetime] NULL,
	[News_OtherCat] [nvarchar](50) NULL,
	[isComment] [bit] NULL,
	[isUserRate] [bit] NULL,
	[Template] [int] NULL,
	[Icon] [nvarchar](200) NULL,
	[Nhuanbut] [int] NULL,
	[News_Relation1] [nvarchar](500) NULL,
	[WordCount] [int] NULL,
	[Note] [nvarchar](1000) NULL,
	[Meta_Title] [nvarchar](500) NULL,
	[Meta_Description] [nvarchar](max) NULL,
	[FullName] [nvarchar](150) NULL,
	[Email] [nvarchar](150) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](500) NULL,
	[FeedBack_Count] [int] NULL,
	[FeedBack_ModifedDate] [datetime] NULL,
	[ImsNewsId] [bigint] NULL,
 CONSTRAINT [PK_FeedBackNews] PRIMARY KEY CLUSTERED 
(
	[FeedBack_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[FeedBackNews] ADD  CONSTRAINT [DF_FeedBackNews_ImsNewsId]  DEFAULT ((0)) FOR [ImsNewsId]
GO


