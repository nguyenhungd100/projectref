USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_GetListNewsParent]    Script Date: 03/20/2018 10:40:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_GetListNewsParent]

AS
BEGIN 
  SELECT * FROM (
	SELECT *,
	(SELECT Count(FeedBack_ID) FROM FeedBackNews AS P WHERE P.News_ID=FBN.FeedBack_ID AND News_Status = 0) AS  TotalRowChildren
	FROM 
	FeedBackNews AS FBN
    --WHERE News_ID = 0 
    )
  AS FBNParent WHERE (TotalRowChildren >0 OR News_Status = 0)  ORDER BY FeedBack_ModifedDate DESC 
END
