USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FeedBackNews_GetListByNewsId]    Script Date: 03/20/2018 10:40:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_FeedBackNews_GetListByNewsId]
@NewsId bigint
AS
BEGIN 

	SELECT * FROM FeedBackNews
	WHERE News_ID=@NewsId AND News_Status = 0
	ORDER BY FeedBack_ModifedDate DESC

END
