use [IMS2_FULL]
Go
--chinhnb
--14/03/2015
CREATE PROCEDURE [dbo].[CMS_ContentWarning_Update]
	@NewsId bigint
     ,@Level int
     ,@LimitedCategory nvarchar(500)
     ,@ListTopic nvarchar(1000)
     ,@ListEnity nvarchar(1000)
     ,@ListKeyword nvarchar(1000)
     ,@ListTopicKeyword nvarchar(1000)
     ,@Status int
AS
BEGIN
	IF(@Level > 0)
	BEGIN
		IF((SELECT COUNT(1) FROM ContentWarning WHERE NewsId = @NewsId) > 0)
			UPDATE ContentWarning 
			SET Level = @Level
				,LimitedCategory = @LimitedCategory
				,ListTopic= @ListTopic
				,ListEnity = @ListEnity
				,ListKeyword = @ListKeyword
				,ListTopicKeyword = @ListTopicKeyword
				,Status = @Status
			WHERE NewsId = @NewsId;
		ELSE
			INSERT INTO ContentWarning(NewsId
			  ,Level
			  ,LimitedCategory
			  ,ListTopic
			  ,ListEnity
			  ,ListKeyword
			  ,ListTopicKeyword
			  ,Status) 
			VALUES(@NewsId
			  ,@Level
			  ,@LimitedCategory
			  ,@ListTopic
			  ,@ListEnity
			  ,@ListKeyword
			  ,@ListTopicKeyword
			  ,@Status)
	
		UPDATE News SET WarningLevel = @Level WHERE Id = @NewsId;
		UPDATE NewsTemp SET WarningLevel = @Level WHERE Id = @NewsId;
	END
	ELSE
	BEGIN
		DELETE ContentWarning WHERE NewsId = @NewsId;
		UPDATE News SET WarningLevel = @Level WHERE Id = @NewsId;
		UPDATE NewsTemp SET WarningLevel = @Level WHERE Id = @NewsId;
	END
END