USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_RecieveFeedbackNewsIntoCms]    Script Date: 03/20/2018 12:01:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/03/2018
CREATE PROCEDURE [dbo].[CMS_News_RecieveFeedbackNewsIntoCms]
    @NewsId BIGINT ,
    @IsFocus BIT ,
    @DistributionDate DATETIME ,
    @CreatedBy NVARCHAR(50) ,
    @ZoneId INT ,
    @Title NVARCHAR(250) ,
    @SubTitle NVARCHAR(250) ,
    @Sapo NVARCHAR(MAX) ,
    @Body NTEXT ,
    @Avatar NVARCHAR(500) ,
    @AvatarDesc NVARCHAR(500) ,
    @Source NVARCHAR(100) ,
    @Author NVARCHAR(500) ,
    @WordCount INT ,
    @IsPr BIT ,
    @AdStore BIT ,
    @AdStoreUrl NVARCHAR(500) ,
    @Url NVARCHAR(200) ,
    @LocationType INT ,
    @Status INT ,
    @NewsRelation VARCHAR(MAX) ,
    @ParentNewsId BIGINT
AS 
    BEGIN
        BEGIN TRANSACTION	
        BEGIN TRY		
            BEGIN
		
                IF NOT EXISTS ( SELECT  0
                                FROM    news
                                WHERE   id = @NewsId ) 
                    BEGIN
                        INSERT  INTO News
                                ( Id ,
                                  Title ,
                                  SubTitle ,
                                  Sapo ,
                                  Body ,
                                  Avatar ,
                                  AvatarDesc ,
                                  Author ,
                                  [Status] ,
                                  [Source] ,
                                  IsFocus ,
                                  CreatedDate ,
                                  LastModifiedDate ,
                                  CreatedBy ,
                                  LastModifiedBy ,
                                  PublishedBy ,
                                  ApprovedBy,
                                  EditedBy ,
                                  LastReceiver ,
                                  WordCount ,
                                  Url ,
                                  OriginalUrl ,
                                  IsPr ,
                                  AdStore ,
                                  AdStoreUrl ,
                                  LocationType ,
                                  DistributionDate ,
                                  [Type] ,
                                  NewsCategory ,
                                  Price ,
                                  ViewCount ,
                                  NewsType ,
                                  ParentNewsId
                                )
                        VALUES  ( @NewsId ,
                                  @Title ,
                                  @SubTitle ,
                                  @Sapo ,
                                  @Body ,
                                  @Avatar ,
                                  @AvatarDesc ,
                                  @Author ,
                                  @Status ,
                                  @Source ,
                                  @IsFocus ,
                                  GETDATE() ,
                                  GETDATE() ,
                                  @CreatedBy ,
                                  @CreatedBy ,
                                  CASE @Status
                                    WHEN 6 THEN @CreatedBy
                                    ELSE ''
                                  END ,
                                  CASE @Status
                                    WHEN 16 THEN @CreatedBy
                                    ELSE ''
                                  END ,
                                  CASE @Status
                                    WHEN 3 THEN @CreatedBy
                                    WHEN 6 THEN @CreatedBy
                                    ELSE ''
                                  END ,
                                  @CreatedBy ,
                                  @WordCount ,
                                  @Url ,
                                  @Url ,
                                  @IsPr ,
                                  @AdStore ,
                                  @AdStoreUrl ,
                                  @LocationType ,
                                  @DistributionDate ,
                                  0 ,
                                  0 ,
                                  0 ,
                                  0 ,
                                  0 ,
                                  @ParentNewsId
                                )
				
                        INSERT  NewsInZone
                                ( ZoneId, NewsId, IsPrimary )
                        VALUES  ( @ZoneId, @NewsId, 1 )
			
                        
                    END
                ELSE 
                    BEGIN
			
                        UPDATE  News
                        SET     Title = @Title ,
                                SubTitle = @SubTitle ,
                                Sapo = @Sapo ,
                                Body = @Body ,
                                Avatar = @Avatar ,
                                AvatarDesc = @AvatarDesc ,
                                Author = @Author ,
                                [Source] = @Source ,
                                IsFocus = @IsFocus ,
                                LastModifiedDate = GETDATE() ,
                                LastModifiedBy = @CreatedBy ,
                                WordCount = @WordCount ,
                                Url = @Url ,
                                OriginalUrl = @Url ,
                                IsPr = @IsPr ,
                                AdStore = @AdStore ,
                                AdStoreUrl = @AdStoreUrl ,
                                LocationType = @LocationType ,
                                DistributionDate = @DistributionDate ,
                                ParentNewsId = @ParentNewsId
                        WHERE   ( Id = @NewsId )
                    END
            END
		
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END


