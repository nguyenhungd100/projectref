USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBottomEmbed_Update]    Script Date: 03/21/2018 10:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_BoxBottomEmbed_Update]
    @Name NVARCHAR(200) ,
    @Code NVARCHAR(MAX) ,
    @CategoryID NVARCHAR(500) ,
    @IsActive BIT ,
    @EndDate DATETIME ,
    @Status INT ,
    @Id INT
AS 
    UPDATE  [dbo].[BoxBottomEmbed]
    SET     [Name] = @Name ,
            [Code] = @Code ,
            [CategoryID] = @CategoryID ,
            [IsActive] = @IsActive ,
            [EndDate] = @EndDate ,
            [Status] = @Status
    WHERE   Id = @Id




