USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBottomEmbed_DeleteById]    Script Date: 03/21/2018 10:27:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_BoxBottomEmbed_DeleteById] @Id INT
AS 
    DELETE  FROM BoxBottomEmbed
    WHERE   Id = @Id




