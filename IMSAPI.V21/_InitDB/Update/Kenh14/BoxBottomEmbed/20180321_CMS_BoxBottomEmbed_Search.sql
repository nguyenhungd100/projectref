USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBottomEmbed_Search]    Script Date: 03/21/2018 10:31:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_BoxBottomEmbed_Search]
    @Keyword NVARCHAR(255) = ''
AS 
    DECLARE @KeyWordLike NVARCHAR(500);
    SET @KeyWordLike = '%' + @Keyword + '%';
    SELECT  [ID] ,
            [Name] ,
            [Code] ,
            [CategoryID] ,
            [IsActive] ,
            [EndDate] ,
            [Status]
    FROM    [dbo].[BoxBottomEmbed]
    WHERE   Name LIKE @KeyWordLike




