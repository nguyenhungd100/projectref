USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[BoxBottomEmbed]    Script Date: 03/21/2018 10:19:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxBottomEmbed](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Code] [nvarchar](max) NULL,
	[CategoryID] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[EndDate] [datetime] NULL,
	[Status] [int] NULL,
	[Name] [nvarchar](200) NULL,
 CONSTRAINT [PK_BoxBottomEmbed] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

----------------------K14Embed---------------------------------------
GO
SET IDENTITY_INSERT [BoxBottomEmbed] ON;
go
INSERT INTO [BoxBottomEmbed](
		[ID]
      ,[Code]
      ,[CategoryID]
      ,[IsActive]
      ,[EndDate]
      ,[Status]
      ,[Name]
    )
SELECT * FROM [K14Embed]
go
SET IDENTITY_INSERT [BoxBottomEmbed] OFF;
go
drop table [K14Embed]
go


