USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBottomEmbed_Insert]    Script Date: 03/21/2018 10:30:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_BoxBottomEmbed_Insert]
    @Name NVARCHAR(200) ,
    @Code NVARCHAR(MAX) ,
    @CategoryID NVARCHAR(500) ,
    @IsActive BIT ,
    @EndDate DATETIME ,
    @Status INT ,
    @Id INT = 0 OUT
AS 
    INSERT  INTO [dbo].[BoxBottomEmbed]
            ( [Name] ,
              [Code] ,
              [CategoryID] ,
              [IsActive] ,
              [EndDate] ,
              [Status]
            )
    VALUES  ( @Name ,
              @Code ,
              @CategoryID ,
              @IsActive ,
              @EndDate ,
              @Status 
            )
    SET @Id = SCOPE_IDENTITY();




