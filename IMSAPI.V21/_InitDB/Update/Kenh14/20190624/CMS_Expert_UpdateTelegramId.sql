ALTER TABLE EXPERT
ADD TelegramId bigint NULL;

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_UpdateTelegramId]    Script Date: 6/24/2019 11:36:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24-06-2019
CREATE PROC [dbo].[CMS_Expert_UpdateTelegramId] 
@ExpertId int,
@TelegramId bigint
AS
BEGIN
UPDATE [Expert] SET TelegramId =@TelegramId WHERE Id =@ExpertId
END

