--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ExpertInNews_GetByNewsId]    Script Date: 6/24/2019 2:33:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_ExpertInNews_GetByNewsId] @Id BIGINT
AS 
    SELECT  a.Id ,
            a.ExpertName ,
            a.JobTitle ,
            a.Description ,
            a.Avatar ,
            a.DisplayJobTitle,
            b.Quote,
			a.TelegramId
    FROM    dbo.Expert a inner join ExpertInNews b
    ON a.Id=b.ExpertId
    WHERE   ( b.NewsId = @Id )




