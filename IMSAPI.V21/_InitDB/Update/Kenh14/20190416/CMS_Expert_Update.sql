--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Update]    Script Date: 4/16/2019 9:23:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Update]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0
AS 
BEGIN
    UPDATE  [dbo].[Expert]
    SET     [ExpertName] = @ExpertName ,
            [JobTitle] = @JobTitle ,
            [Description] = @Description ,
            [Avatar] = @Avatar ,
            Quote=@Quote,
            [DisplayJobTitle] = @DisplayJobTitle,
            AcceptFeedback = @AcceptFeedback,
            FacebookLink = @FacebookLink,
			Mobile=@Mobile
    WHERE   @id = Id
END





