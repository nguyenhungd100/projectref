--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_GetById]    Script Date: 4/16/2019 9:25:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_GetById] @Id INT
AS 
BEGIN
    SELECT  *
    FROM    dbo.Expert
    WHERE   ( Id = @Id )
END






