--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ExpertInNews_GetByExpertId]    Script Date: 4/16/2019 4:50:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
CREATE PROCEDURE [dbo].[CMS_NewsExtension_ListNewsByExpertId] --3,0,1,20,0
    @ExpertId INT ,
	@ExpertStatus INT ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
    SELECT  @TotalRow = COUNT(1) from (    
	select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
	inner join newsextension ne on ne.newsid=abc.newsid
	where ne.type=52  and ne.value=@ExpertStatus) as tem
    
	SELECT *
	FROM ( 
		select n.*
		,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum
		from news n inner join 
		(select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
		inner join newsextension ne on ne.newsid=abc.newsid
		where ne.type=52  and ne.value=@ExpertStatus) as newstemp
		on newstemp.newsid=n.id
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

