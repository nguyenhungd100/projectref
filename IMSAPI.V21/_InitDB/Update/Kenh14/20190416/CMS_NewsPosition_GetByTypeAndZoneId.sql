USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_GetByTypeAndZoneId]    Script Date: 6/8/2019 10:00:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-05>
-- Edit: chinhnb 12/06/2018
-- Description:	<get list news position by type and zoneid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_GetByTypeAndZoneId] --1, 0, ''
	@TypeId int,
	@ZoneId int,
	@ListOfOrder varchar(200) = ''
AS
BEGIN
	IF @ListOfOrder <> ''
		SET @ListOfOrder = ';' + @ListOfOrder + ';'

	SELECT Id, ZoneId, TypeId, [Order], Lockable, Locked, ExpiredLock, NewsIdForBomd, NewsId, Title, 
		SubTitle, Sapo, Avatar, AvatarDesc, Avatar1, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
		NewsRelation, Source, IsFocus, NewsType, ThreadId, DistributionDate, Url, AllowAutoUpdate, AvatarCustom,
		ViewCount, OriginalId, Type
	FROM
		(
			SELECT NP.Id, NP.ZoneId, NP.TypeId, NP.[Order], NP.Lockable, NP.Locked, NP.ExpiredLock, NP.NewsIdForBomd, NP.NewsId, NP.Title, 
				NP.SubTitle, NP.Sapo, NP.Avatar, NP.AvatarDesc, NP.Avatar1, NP.Avatar2, NP.Avatar3, NP.Avatar4, NP.Avatar5, NP.Author, 
				NP.NewsRelation, NP.Source, NP.IsFocus, NP.NewsType, NP.ThreadId, NP.DistributionDate, NP.Url, NP.AllowAutoUpdate, NP.AvatarCustom,
				N.ViewCount, N.OriginalId, NP.Type,
				(ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY np.ScheduleDate DESC, np.DistributionDate DESC )) AS ROW
			FROM NewsPosition AS NP LEFT JOIN News AS N ON NP.NewsId = N.Id		
			WHERE
				np.TypeId = @typeId
				AND (@zoneId = 0 OR np.ZoneId = @zoneId)
				--AND np.DistributionDate <= GETDATE()
				AND (np.[ScheduleDate] IS NULL OR np.[ScheduleDate] <= GETDATE())
				AND (@ListOfOrder = '' OR (@ListOfOrder <> '' AND PATINDEX('%;' + CONVERT(varchar(10), [Order]) + ';%', @ListOfOrder) > 0))
		)TEMP
	WHERE 
		ROW = 1
	ORDER BY
		[Order]
END










