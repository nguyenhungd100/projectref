--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search]    Script Date: 4/16/2019 9:26:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Search]
    @Keyword NVARCHAR(255) = ''
AS 
BEGIN
    DECLARE @KeyWordLike NVARCHAR(500) ;
    SET @KeyWordLike = '%' + @Keyword + '%' ;
    SELECT  *
    FROM    dbo.Expert
    WHERE   ExpertName LIKE @KeyWordLike
END
