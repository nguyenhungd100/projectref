GO
ALTER TABLE Expert
ADD Mobile VARCHAR(20)

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Insert]    Script Date: 4/16/2019 9:21:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Insert]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0 OUT
AS 
BEGIN
    INSERT  INTO [dbo].[Expert]
            ( [ExpertName] ,
              [JobTitle] ,
              [Description] ,
              [Avatar] ,
              Quote,
              [DisplayJobTitle],
              AcceptFeedback,
              FacebookLink,
			  Mobile
            )
    VALUES  ( @ExpertName ,
              @JobTitle ,
              @Description ,
              @Avatar ,
              @Quote,
              @DisplayJobTitle,
              @AcceptFeedback,
              @FacebookLink,
			  @Mobile
            )

    SET @Id = SCOPE_IDENTITY() ;
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Update]    Script Date: 4/16/2019 9:23:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Update]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0
AS 
BEGIN
    UPDATE  [dbo].[Expert]
    SET     [ExpertName] = @ExpertName ,
            [JobTitle] = @JobTitle ,
            [Description] = @Description ,
            [Avatar] = @Avatar ,
            Quote=@Quote,
            [DisplayJobTitle] = @DisplayJobTitle,
            AcceptFeedback = @AcceptFeedback,
            FacebookLink = @FacebookLink,
			Mobile=@Mobile
    WHERE   @id = Id
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 4/16/2019 9:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
--edit 2019/04/10
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
BEGIN
	SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

    SELECT  *
    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                        E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink,E.Mobile
						,SUM(CASE WHEN EN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
                        ,ROW_NUMBER() OVER ( ORDER BY E.Id DESC ) AS RowNum
                FROM      Expert E
				LEFT JOIN ExpertInNews AS EN ON E.Id = EN.ExpertId
                WHERE     E.ExpertName LIKE '%' + @Keyword + '%'
				GROUP BY E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink,E.Mobile
            ) baseTable
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                    AND     ( @PageIndex * @PageSize )
	
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search]    Script Date: 4/16/2019 9:26:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Search]
    @Keyword NVARCHAR(255) = ''
AS 
BEGIN
    DECLARE @KeyWordLike NVARCHAR(500) ;
    SET @KeyWordLike = '%' + @Keyword + '%' ;
    SELECT  *
    FROM    dbo.Expert
    WHERE   ExpertName LIKE @KeyWordLike
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_GetById]    Script Date: 4/16/2019 9:25:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_GetById] @Id INT
AS 
BEGIN
    SELECT  *
    FROM    dbo.Expert
    WHERE   ( Id = @Id )
END