--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Insert]    Script Date: 4/16/2019 9:21:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Insert]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0 OUT
AS 
BEGIN
    INSERT  INTO [dbo].[Expert]
            ( [ExpertName] ,
              [JobTitle] ,
              [Description] ,
              [Avatar] ,
              Quote,
              [DisplayJobTitle],
              AcceptFeedback,
              FacebookLink,
			  Mobile
            )
    VALUES  ( @ExpertName ,
              @JobTitle ,
              @Description ,
              @Avatar ,
              @Quote,
              @DisplayJobTitle,
              @AcceptFeedback,
              @FacebookLink,
			  @Mobile
            )

    SET @Id = SCOPE_IDENTITY() ;
END




