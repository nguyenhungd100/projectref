--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FileUpload_Insert]    Script Date: 9/18/2018 11:59:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Fox>
-- Create date: <2012-11-15>
-- Edit: chinhnb, 19-09-2018
-- Description:	<Insert new file upload>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_FileUpload_Insert]
	@Id int output,
	@ZoneId int,
	@Title nvarchar(200),
	@Description nvarchar(2000),
	@FileDownloadPath varchar(500),
	@FilePath varchar(500),
	@FileExt varchar(5),
	@FileSize int,
	@UploadedBy nvarchar(50),
	@Status int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		IF(select count(*) from FileUpload where Title=@Title) <= 0
			BEGIN
			INSERT INTO FileUpload (			
				ZoneId,
				Title,
				Description,
				FileDownloadPath,
				FilePath,
				FileExt,
				FileSize,
				UploadedDate,
				UploadedBy,
				LastModifiedDate,
				LastModifiedBy,
				Status
				)
			VALUES (
				@ZoneId,
				@Title,
				@Description,
				@FileDownloadPath,
				@FilePath,
				@FileExt,
				@FileSize,
				GETDATE(),
				@UploadedBy,
				GETDATE(),
				@UploadedBy,
				@Status
				)
			
				SET @Id = SCOPE_IDENTITY()
			END
		ELSE
			SET @Id = 0  
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_FileUpload_Insert]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END



















