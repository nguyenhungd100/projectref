--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateAvatar]    Script Date: 10/18/2018 10:09:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--18-10-2018
CREATE PROC [dbo].[CMS_User_UpdateTelegramId] 
@UserName varchar(255),
@TelegramId bigint
AS
BEGIN
UPDATE [User] SET TelegramId =@TelegramId WHERE UserName =@UserName
END

