--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Insert]    Script Date: 3/1/2019 6:36:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
--Edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Insert]
    @Id BIGINT OUTPUT ,
    @TopicName NVARCHAR(300) ,
    @Logo VARCHAR(255) ,
    @Cover VARCHAR(255) ,
    @IsActive BIT ,
    @IsIconActive BIT ,
    @PrimaryZoneId INT ,
    @Description NVARCHAR(max) ,
    @DisplayUrl VARCHAR(300) ,
    @LogoFancyClose VARCHAR(300) ,
    @LogoTopicName VARCHAR(300) ,
    @LogoSubMenu VARCHAR(300) ,
    @ListTagId VARCHAR(300) ,
    @DefaultViewMode TINYINT ,
    @GuideToSendMail NVARCHAR(1000) ,
    @TopicEmail VARCHAR(300) ,
    @IsTopToolbar BIT = 0,
    @ParentId INT,
    @RelationTopic VARCHAR(500)='',
    @DisplayName NVARCHAR(300),
	@Priority INT,
	@ZoneIdList varchar(1000)='',
	@CreatedBy varchar(100)=''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
		
            INSERT  INTO [dbo].[Topic]
                    ( [TopicName] ,
                      [Logo] ,
                      [Cover] ,
                      [IsActive] ,
                      [IsIconActive] ,
                      DESCRIPTION ,
                      DisplayUrl ,
                      LogoFancyClose ,
                      LogoTopicName ,
                      LogoSubMenu ,
                      DefaultViewMode ,
                      GuideToSendMail ,
                      TopicEmail ,
                      IsTopToolbar,
                      ParentId,
                      RelationTopic,
                      DisplayName,
					  [Priority],
					  CreatedBy,
					  CreatedDate
                    )
            VALUES  ( @TopicName ,
                      @Logo ,
                      @Cover ,
                      @IsActive ,
                      @IsIconActive ,
                      @Description ,
                      @DisplayUrl ,
                      @LogoFancyClose ,
                      @LogoTopicName ,
                      @LogoSubMenu ,
                      @DefaultViewMode ,
                      @GuideToSendMail ,
                      @TopicEmail ,
                      @IsTopToolbar,
                      @ParentId,
                      @RelationTopic,
                      @DisplayName,
					  @Priority,
					  @CreatedBy,
					  getdate()
                    )
		
            SET @Id = SCOPE_IDENTITY();
            
            UPDATE Topic SET DisplayUrl = REPLACE(DisplayUrl, '{TopicId}', @Id) WHERE Id = @Id
		
			/* Insert vào bảng  TopicZone*/		
            INSERT  INTO TopicInZone(TopicId,ZoneId,IsPrimary)
            VALUES  ( @Id, @PrimaryZoneId, 1)

			DECLARE @Index INT;

			IF @ZoneIdList <> ''
			BEGIN
				DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
				DECLARE @ZoneItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@ZoneIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
				BEGIN
					SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
					IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
						IF NOT EXISTS(SELECT ZoneId FROM TopicInZone WHERE ZoneId = @ZoneItemId AND TopicId = @Id)
							INSERT INTO TopicInZone(TopicId, ZoneId, IsPrimary) 
							VALUES (@Id, @ZoneItemId, 0);
						
					SET @Index = @Index + 1
				END
			END

            --Tag
            DELETE  FROM TagInTopic
            WHERE   TopicId = @Id
            DECLARE @TblAddTemp TABLE
                (
                  Id INT IDENTITY(1, 1) ,
                  NewsId BIGINT
                )
            INSERT  @TblAddTemp
                    ( NewsId
                    )
                    SELECT  CONVERT(BIGINT, part)
                    FROM    SplitString(@ListTagId, ';')
		
            DECLARE @TotalRowInTemp INT ,
                @TagId BIGINT ,
                @Avatar NVARCHAR(500);
		
            SET @Index = 0
            SET @TotalRowInTemp = ISNULL(( SELECT   COUNT(Id)
                                           FROM     @TblAddTemp
                                         ), 0)
            WHILE ( @Index <= @TotalRowInTemp ) 
                BEGIN
                    SET @TagId = ISNULL(( SELECT    NewsId
                                          FROM      @TblAddTemp
                                          WHERE     Id = @Index
                                        ), 0)
                    IF @TagId > 0 
                        BEGIN
                            IF NOT EXISTS ( SELECT  1
                                            FROM    TagInTopic
                                            WHERE   TagId = @TagId
                                                    AND TopicId = @Id ) 
                                BEGIN
                                    INSERT  INTO TagInTopic
                                            ( TagId, TopicID, Priority )
                                    VALUES  ( @TagId, @Id, @Index + 1 )
                                END
                        END
                    SET @Index = @Index + 1
                END
			
			--Xu ly TopicRelation
			IF @RelationTopic <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), TopicId int)
				DECLARE @TopicItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(TopicId) SELECT CONVERT(int, Part) FROM SplitString(@RelationTopic, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @TopicItemId = (SELECT TopicId FROM @TblTempRelation WHERE Id = @Index)
					IF @TopicItemId IS NOT NULL AND @TopicItemId > 0
						IF NOT EXISTS(SELECT TopicRelationId FROM TopicRelation WHERE TopicRelationId = @TopicItemId AND TopicId = @Id)
							INSERT INTO TopicRelation(TopicId, TopicRelationId, Ordinary) VALUES (@Id, @TopicItemId,0);
					SET @Index = @Index + 1
				END
			END
			
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

--USE [IMS2_FULL]
