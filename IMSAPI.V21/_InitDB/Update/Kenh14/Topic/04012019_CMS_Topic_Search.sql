--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Search]    Script Date: 1/4/2019 3:49:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Search] --'',-1,-1,0,-1,1,10,0
	@Keyword nvarchar(500),
	@IsHot int = -1,
	@IsActive int = -1,
	@ZoneId int,
	@OrderBy int,
	@ParentId int=-1,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@IsHot int,'
	SET @params = @params + '@IsActive int,'
	SET @params = @params + '@ParentId int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.TopicName) > 0)' 		 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId or TZ.ZoneId in(select id from Zone where parentid= @ZoneId))' 
		
	IF @IsHot > -1
		SET @where = @where + ' AND (T.isTopToolbar = @IsHot)'

	IF @IsActive > -1
		SET @where = @where + ' AND (T.IsActive = @IsActive)'

	IF @ParentId > -1
		SET @where = @where + ' AND (T.ParentId = @ParentId)'
			
	SET @order = ' ORDER BY T.Id DESC'
	IF @OrderBy = 1
		SET @order = ' ORDER BY T.TopicName ASC'
	IF @OrderBy = 2
		SET @order = ' ORDER BY T.ParentId ASC, T.Priority ASC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Topic AS T LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId ' + @where + ';'
	SET @search = @search + 'SELECT [Id]
		,[ZoneId]
		,[Priority]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive]
      ,[ParentId]      
      ,[RelationTopic]
      ,ParentName
      ,DisplayName
      ,DisplayUrl
      ,NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
      ,T.[ParentId]      
      ,T.[RelationTopic]
      ,TP.[TopicName] as ParentName
      ,T.[DisplayName]
      ,T.DisplayUrl, '
	SET @search = @search + '		SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Topic AS T '
	SET @search = @search + '		LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId AND TZ.IsPrimary=1'
	SET @search = @search + '		LEFT JOIN NewsInTopic AS TN ON T.Id = TN.TopicId '
	SET @search = @search + '		LEFT JOIN Topic AS TP ON TP.Id = T.ParentId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
      ,T.[ParentId]
      ,T.[RelationTopic]
      ,TP.[TopicName]
      ,T.[DisplayName]
      ,T.DisplayUrl '
	SET @search = @search + ') AS TopicTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	--print(@search)
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@IsHot,
								@IsActive,
								@ParentId,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END