--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetTopicByTopicID]    Script Date: 12/29/2018 9:56:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Edit:21/06/2018
CREATE PROCEDURE [dbo].[CMS_Topic_GetTopicByName] @TopicName nvarchar(300)
AS 
    BEGIN
        SELECT top 1  T.[Id] ,
                [TopicName] ,
                [Logo] ,
                [Cover] ,
                [IsActive] ,
                [IsIconActive] ,
                DESCRIPTION ,
                DisplayUrl ,
                LogoFancyClose ,
                LogoTopicName ,
                LogoSubMenu ,
                DefaultViewMode ,
                GuideToSendMail ,
                TopicEmail ,
                isTopToolbar,
                ParentId,
                RelationTopic,
                DisplayName,
				Priority
        FROM    Topic T
        WHERE   ( T.TopicName = @TopicName )
    END
