--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetTopicRelationByTopicId]    Script Date: 3/6/2019 3:04:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Created:06/03/2019 
CREATE PROCEDURE [dbo].[CMS_Topic_GetParentTopicById] --4184
@TopicId INT
AS
BEGIN

	WITH temp(Id, TopicName, ParentId, Logo, DisplayName, DisplayUrl)
	as (
			Select Id, TopicName, ParentId, Logo, DisplayName, DisplayUrl
			From Topic
			Where Id=(select ParentId from topic where id=@TopicId)
			Union All
			Select a.Id, a.TopicName, a.ParentId, a.Logo, a.DisplayName, a.DisplayUrl
			From Topic as a
			inner join temp as b on a.Id = b.ParentId	
			Where a.ParentId>=0	
	)
	Select * From temp

END
