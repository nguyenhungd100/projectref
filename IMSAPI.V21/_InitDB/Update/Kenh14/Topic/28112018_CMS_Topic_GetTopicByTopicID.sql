--USE [IMS2_BVHTTDL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetTopicByTopicID]    Script Date: 11/28/2018 5:06:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Edit:21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_GetTopicByTopicID] @Id BIGINT
AS 
    BEGIN
        SELECT top 1  T.[Id] ,
                [TopicName] ,
                [Logo] ,
                [Cover] ,
                [IsActive] ,
                [IsIconActive] ,
                DESCRIPTION ,
                DisplayUrl ,
                LogoFancyClose ,
                LogoTopicName ,
                LogoSubMenu ,
                DefaultViewMode ,
                GuideToSendMail ,
                TopicEmail ,
                isTopToolbar,
                ParentId,
                RelationTopic,
                DisplayName,
				Priority,
				TZ.ZoneId
        FROM    Topic T left join TopicInZone TZ on TZ.TopicId=T.Id
        WHERE   ( T.Id = @Id )
    END
