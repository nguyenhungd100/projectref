USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[AlertPerson]    Script Date: 03/30/2018 15:05:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AlertPerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonName] [nvarchar](50) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_AlertPerson] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[AlertPerson] ADD  CONSTRAINT [DF_AlertPerson_Status]  DEFAULT ((1)) FOR [Status]
GO


