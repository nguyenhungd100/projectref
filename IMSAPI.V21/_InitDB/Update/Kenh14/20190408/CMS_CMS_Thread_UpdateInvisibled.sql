--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_UpdateInvisibled]    Script Date: 4/10/2019 10:07:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-10>
-- Description:	<Update Thread Invisibled>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Thread_UpdateInvisibled]
	@Id BIGINT,
	@Invisibled bit
AS
BEGIN
	UPDATE Thread
	SET Invisibled = @Invisibled
	WHERE (Id = @Id)
END
