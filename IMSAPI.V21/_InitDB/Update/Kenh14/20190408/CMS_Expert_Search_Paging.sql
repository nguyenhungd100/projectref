--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 4/10/2019 10:23:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
--edit 2019/04/10
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
BEGIN
	SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

    SELECT  *
    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                        E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
						,SUM(CASE WHEN EN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
                        ,ROW_NUMBER() OVER ( ORDER BY E.Id DESC ) AS RowNum
                FROM      Expert E
				LEFT JOIN ExpertInNews AS EN ON E.Id = EN.ExpertId
                WHERE     E.ExpertName LIKE '%' + @Keyword + '%'
				GROUP BY E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
            ) baseTable
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                    AND     ( @PageIndex * @PageSize )
	
END
