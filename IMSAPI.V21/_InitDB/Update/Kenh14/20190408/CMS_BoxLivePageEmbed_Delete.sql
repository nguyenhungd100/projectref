--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Delete]    Script Date: 4/8/2019 11:14:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Delete box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Delete]
	@Id int
AS
BEGIN	
	DELETE FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END


