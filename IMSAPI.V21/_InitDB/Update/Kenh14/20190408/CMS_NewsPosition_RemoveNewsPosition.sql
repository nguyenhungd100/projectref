--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_RemoveNewsPosition]    Script Date: 4/11/2019 10:59:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-115>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsPosition_RemoveNewsPosition] --1, 0, ''
	@TypeId int,
	@ZoneId int,
	@ConfigPosition int
AS
BEGIN
	DELETE NewsPosition where TypeId = @TypeId AND ZoneId = @ZoneId and Id not in
	(
	Select top(@ConfigPosition) Id from NewsPosition
	WHERE TypeId = @TypeId AND ZoneId = @ZoneId
	ORDER BY [Order] , DistributionDate DESC
	)
END
	
