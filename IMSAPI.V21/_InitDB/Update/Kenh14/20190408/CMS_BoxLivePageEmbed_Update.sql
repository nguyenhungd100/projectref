--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Update]    Script Date: 4/8/2019 11:23:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Update]
	@Id int,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@ModifiedDate datetime,
	@ModifiedBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
	UPDATE BoxLivePageEmbed
	SET PageName = @PageName, 
		PageUrl = @PageUrl, 				
		TemplateId = @TemplateId,
		Position = @Position, 
		Status = @Status,
		Type = @Type,
		Priority = @Priority, 				
		ModifiedDate = @ModifiedDate,
		ModifiedBy = @ModifiedBy,
		DataJson=@DataJson
	WHERE (Id = @Id)				
END

