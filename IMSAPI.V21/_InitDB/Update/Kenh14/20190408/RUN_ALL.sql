--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxLivePageEmbed]    Script Date: 4/8/2019 2:24:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxLivePageEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageUrl] [nvarchar](1000) NULL,
	[PageName] [nvarchar](250) NULL,
	[Position] [int] NULL,
	[TemplateId] [int] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatdBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[DataJson] [nvarchar](max) NULL,
 CONSTRAINT [PK_BoxLivePageEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_BoxLivePageEmbed_TemplateId]  DEFAULT ((0)) FOR [TemplateId]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_BoxLivePageEmbed_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_Table_1_LastModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO


--

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Search]    Script Date: 4/8/2019 11:27:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by zone id and position>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Search] 
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@Position int,
	@Status int,
	@Type int,
	@TemplateId int
AS
BEGIN
	IF @PageName <> ''
		SET @PageName = '%' + @PageName + '%'

	SELECT Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	FROM BoxLivePageEmbed
	WHERE ((@PageName='' AND 1=1) OR (@PageName<>'' AND PATINDEX(@PageName, PageName) > 0)) 
		AND ((@PageUrl='' AND 1=1) OR (@PageUrl<>'' AND PageUrl=@PageUrl)) 
		AND (@Position < 0 OR (@Position >= 0 AND Position = @Position)) 
		AND (@Status < 0 OR (@Status >= 0 AND Status = @Status)) 
		AND (@Type < 0 OR (@Type >= 0 AND Type = @Type))
		AND (@TemplateId < 0 OR (@TemplateId >= 0 AND TemplateId = @TemplateId)) 				
	Group By Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	ORDER BY Priority ASC
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]    Script Date: 4/8/2019 11:25:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]
	@ListId varchar(1000)
AS
BEGIN
	DECLARE @SortOrder int, 
			@BoxLivePageEmbedId int
			
	DECLARE SortOrderCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListId, ';')

	OPEN SortOrderCursor
	
	FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		UPDATE BoxLivePageEmbed
		SET Priority = @SortOrder
		WHERE Id = @BoxLivePageEmbedId
			
		FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId 
	END
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Update]    Script Date: 4/8/2019 11:23:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Update]
	@Id int,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@ModifiedDate datetime,
	@ModifiedBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
	UPDATE BoxLivePageEmbed
	SET PageName = @PageName, 
		PageUrl = @PageUrl, 				
		TemplateId = @TemplateId,
		Position = @Position, 
		Status = @Status,
		Type = @Type,
		Priority = @Priority, 				
		ModifiedDate = @ModifiedDate,
		ModifiedBy = @ModifiedBy,
		DataJson=@DataJson
	WHERE (Id = @Id)				
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Insert]    Script Date: 4/8/2019 11:16:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Insert box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Insert]	
	@Id int OUTPUT,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@CreatedDate datetime,
	@CreatdBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO BoxLivePageEmbed (PageName,PageUrl,TemplateId,Position,Status, Type, Priority, CreatedDate, CreatdBy, DataJson)
		VALUES (@PageName,@PageUrl,@TemplateId,@Position,@Status,@Type,@Priority,@CreatedDate,@CreatdBy,@DataJson)		
		
		SET @Id = SCOPE_IDENTITY()
			
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_GetById]    Script Date: 4/8/2019 11:16:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Delete]    Script Date: 4/8/2019 11:14:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Delete box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Delete]
	@Id int
AS
BEGIN	
	DELETE FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_RemoveNewsPosition]    Script Date: 4/11/2019 10:59:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-115>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsPosition_RemoveNewsPosition] --1, 0, ''
	@TypeId int,
	@ZoneId int,
	@ConfigPosition int
AS
BEGIN
	DELETE NewsPosition where TypeId = @TypeId AND ZoneId = @ZoneId and Id not in
	(
	Select top(@ConfigPosition) Id from NewsPosition
	WHERE TypeId = @TypeId AND ZoneId = @ZoneId
	ORDER BY [Order] , DistributionDate DESC
	)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_UpdateInvisibled]    Script Date: 4/10/2019 10:07:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-10>
-- Description:	<Update Thread Invisibled>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Thread_UpdateInvisibled]
	@Id BIGINT,
	@Invisibled bit
AS
BEGIN
	UPDATE Thread 
	SET Invisibled = @Invisibled
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 4/10/2019 10:23:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
--edit 2019/04/10
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
BEGIN
	SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

    SELECT  *
    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                        E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
						,SUM(CASE WHEN EN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
                        ,ROW_NUMBER() OVER ( ORDER BY E.Id DESC ) AS RowNum
                FROM      Expert E
				LEFT JOIN ExpertInNews AS EN ON E.Id = EN.ExpertId
                WHERE     E.ExpertName LIKE '%' + @Keyword + '%'
				GROUP BY E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
            ) baseTable
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                    AND     ( @PageIndex * @PageSize )
	
END
