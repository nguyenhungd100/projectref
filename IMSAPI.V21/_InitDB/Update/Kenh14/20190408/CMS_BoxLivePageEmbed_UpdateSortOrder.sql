--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]    Script Date: 4/8/2019 11:25:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]
	@ListId varchar(1000)
AS
BEGIN
	DECLARE @SortOrder int, 
			@BoxLivePageEmbedId int
			
	DECLARE SortOrderCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfBoxLivePageEmbedId, ';')

	OPEN SortOrderCursor
	
	FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		UPDATE BoxLivePageEmbed
		SET Priority = @SortOrder
		WHERE Id = @BoxLivePageEmbedId
			
		FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId 
	END
END

