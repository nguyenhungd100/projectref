--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Insert]    Script Date: 4/8/2019 11:16:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Insert box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Insert]	
	@Id int OUTPUT,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@CreatedDate datetime,
	@CreatdBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO BoxLivePageEmbed (PageName,PageUrl,TemplateId,Position,Status, Type, Priority, CreatedDate, CreatdBy, DataJson)
		VALUES (@PageName,@PageUrl,@TemplateId,@Position,@Status,@Type,@Priority,@CreatedDate,@CreatdBy,@DataJson)		
		
		SET @Id = SCOPE_IDENTITY()
			
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END