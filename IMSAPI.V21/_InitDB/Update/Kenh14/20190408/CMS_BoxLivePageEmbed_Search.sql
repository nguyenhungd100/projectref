--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Search]    Script Date: 4/8/2019 11:27:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by zone id and position>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Search] 
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@Position int,
	@Status int,
	@Type int,
	@TemplateId int
AS
BEGIN
	IF @PageName <> ''
		SET @PageName = '%' + @PageName + '%'

	SELECT Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	FROM BoxLivePageEmbed
	WHERE ((@PageName='' AND 1=1) OR (@PageName<>'' AND PATINDEX(@PageName, PageName) > 0)) 
		AND ((@PageUrl='' AND 1=1) OR (@PageUrl<>'' AND PageUrl=@PageUrl)) 
		AND (@Position < 0 OR (@Position >= 0 AND Position = @Position)) 
		AND (@Status < 0 OR (@Status >= 0 AND Status = @Status)) 
		AND (@Type < 0 OR (@Type >= 0 AND Type = @Type))
		AND (@TemplateId < 0 OR (@TemplateId >= 0 AND TemplateId = @TemplateId)) 				
	Group By Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	ORDER BY Priority ASC
END

