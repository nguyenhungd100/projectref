--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_GetById]    Script Date: 4/8/2019 11:16:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END

