USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertVideoApi]    Script Date: 04/20/2018 16:48:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--06/04/2018
ALTER proc [dbo].[sp_InsertVideoApi] 
(
   @videoId varchar(100)='229979298866',
   @ZoneId int=0,
   @Name nvarchar(250)='Test',
   @Avatar varchar(255)='https://video-thumbs.mediacdn.vn/premierleague/2017/10/9/tong-hop-100396938_1200_96.jpg',
   @CreatedDate datetime='2017-10-10',
   @CreatedBy varchar(255)='admin',
   @Duration varchar(50)='00:00:22',
   @FileName varchar(500)='premierleague',
   @Url varchar(300)='-----'
)
as
 Begin transaction sp_InsertVideoApi
 BEGIN TRY
      Declare @count int
      DECLARE @Id int
   if exists(select 1 from [Video] where [KeyVideo] = @videoId)
   Begin
     SET @count=0
   End
   Else
   BEGIN
     Insert into Video(
    [ZoneId]
    ,[Name]
    ,[Avatar]
    ,[KeyVideo]
    ,[Pname]
    ,[CreatedBy]
    ,[CreatedDate]
    ,[FileName]
    ,[Duration]
    ,[Status]
    ,[UnsignName]
          ,[Description]
          ,[HtmlCode]
          ,[NewsId]
          ,[Tags]
          ,[LastModifiedBy]
          ,[LastModifiedDate]
          ,[Size]
    ,[Capacity]
    ,[AllowAd]
    ,[IsRemoveLogo]
    ,[OriginalUrl]
    ,[Source],[DistributionDate]) values(@ZoneId,@Name,@Avatar,@videoId,'',@CreatedBy,@CreatedDate,@FileName,@Duration,3,'','','',0,'',@CreatedBy,GETDATE(),'','',1,0,'','',GETDATE())
        SET @count=1
        SET @Id = SCOPE_IDENTITY()
        SET @Url= REPLACE(@Url, '{VideoId}', convert(varchar(100),@Id))
        Update Video SET Url=@Url Where Id=@Id
   END
    if @count=1
    BEGIN
       Insert into [VideoInZone]([VideoId]
      ,[ZoneId]
      ,[IsPrimary]
      ,[LastModifiedDate]
      ,[RootZoneId]) VALUES(@Id,@ZoneId,1,GETDATE(),NULL)
    END
    ELSE 
    BEGIN
   RAISERROR ('Video is exists.', 16, 1)
   ROLLBACK TRANSACTION CMS_BoxNews_Insert
    END
    Select @count
    COMMIT TRANSACTION
 END TRY
 BEGIN CATCH
      DECLARE @Message nvarchar(255);
  SET @Message = ERROR_MESSAGE();
  RAISERROR (@Message, 16, 1);
  ROLLBACK TRANSACTION sp_InsertVideoApi
 END CATCH