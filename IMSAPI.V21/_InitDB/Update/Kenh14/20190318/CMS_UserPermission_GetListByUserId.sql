--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_UserPermission_GetListByUserId]    Script Date: 3/18/2019 9:42:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---27/03/2017
ALTER PROCEDURE [dbo].[CMS_UserPermission_GetListByUserId] --569,1
	@UserId int,
	@IsGetChildZone bit
AS
BEGIN
	DECLARE @IsFullPermission bit,
			@IsFullZone bit
			
	SELECT @IsFullPermission = IsFullPermission, @IsFullZone = IsFullZone FROM [User] WHERE Id = @UserId
	
	IF @IsFullPermission = 1 AND @IsFullZone = 1
		SELECT @UserId as UserId, P.Id AS PermissionId, Z.Id AS ZoneId
		FROM Permission AS P, Zone AS Z
		WHERE (@IsGetChildZone = 1 OR (@IsGetChildZone = 0 AND Z.ParentId = 0))
	ELSE
	BEGIN
		IF @IsFullPermission = 1
		BEGIN
			SELECT UserId, Id AS PermissionId, ZoneId 
			FROM Permission,
			(
				SELECT UP.UserId, UP.ZoneId
				FROM UserPermission AS UP
				WHERE (UP.UserId = @UserId)
				UNION
				SELECT UP.UserId, Z.Id
				FROM UserPermission AS UP
					INNER JOIN Zone AS Z ON UP.ZoneId = Z.ParentId
				WHERE (UP.UserId = @UserId) AND (@IsGetChildZone = 1)
			) AS DATA
		END
		ELSE IF @IsFullZone = 1
		BEGIN
			SELECT UP.UserId, UP.PermissionId, Z.Id AS ZoneId
			FROM UserPermission AS UP, Zone AS Z
			WHERE (UP.UserId = @UserId)
				AND (@IsGetChildZone = 1 OR (@IsGetChildZone = 0 AND Z.ParentId = 0))
		END
		ELSE
		BEGIN
			with temp(UserId, PermissionId, ZoneId) as (
				SELECT UP.UserId, UP.PermissionId, UP.ZoneId
				FROM UserPermission AS UP
				WHERE (UP.UserId = @UserId)
				UNION
				SELECT UP.UserId, UP.PermissionId, Z.Id
				FROM UserPermission AS UP
					INNER JOIN Zone AS Z ON UP.ZoneId = Z.ParentId
				WHERE (UP.UserId = @UserId) AND (@IsGetChildZone = 1)
			)
			Select * From temp
			UNION 
			SELECT UP.UserId, UP.PermissionId, Z.Id
			FROM temp AS UP
			INNER JOIN Zone AS Z ON UP.ZoneId = Z.ParentId
		END
	END
END

