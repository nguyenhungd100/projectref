use [IMS2_KENH14_DEV]
Go
--chinhnb
--14/03/2015
CREATE PROCEDURE [dbo].[CMS_ContentWarningLog_GetByListNewsId]
	@ListNewsId varchar(1000)
AS
BEGIN
	SELECT NewsId, Account, Status, CreatedDate FROM ContentWarningLog C
	INNER JOIN CmsDL_fSplitString(@ListNewsId, ',') S
	ON C.NewsId = CONVERT(bigint, S.part)
	ORDER BY C.CreatedDate DESC;
END