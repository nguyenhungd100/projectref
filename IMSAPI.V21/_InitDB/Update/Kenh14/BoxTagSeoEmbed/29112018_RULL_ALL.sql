--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxTagSeoEmbed]    Script Date: 11/29/2018 11:05:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxTagSeoEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TagId] [bigint] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Avatar] [varchar](1000) NULL,
	[Url] [varchar](1000) NULL,
	[Description] [nvarchar](500) NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[ObjectId] [varchar](100) NULL,
	[ObjectType] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxTagSeoEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[BoxTagSeoEmbed] ADD  CONSTRAINT [DF_BoxTagSeoEmbed_ObjectType]  DEFAULT ((0)) FOR [ObjectType]
GO




--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Delete]    Script Date: 11/29/2018 10:34:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Delete]
@Id int
AS
DELETE BoxTagSeoEmbed where Id =@Id


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Insert]    Script Date: 11/29/2018 10:35:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Insert]	
	@Id int OUTPUT,
	@TagId int= 0,
	@Type int =0,	
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@ObjectId varchar(100)='0',
	@ObjectType int=0
AS
BEGIN 
	INSERT INTO BoxTagSeoEmbed (
			TagId,	
			Type,			
			Title,
			Avatar,
			Url,
			Description,
			SortOrder, 			
			LastModifiedDate,
			ObjectId,
			ObjectType
		)
		VALUES(
			@TagId,	
			@Type,					
			@Title,
			@Avatar,
			@Url,
			@Description,
			@SortOrder,
			getdate(),
			@ObjectId,
			@ObjectType
		)

	SET @Id = SCOPE_IDENTITY();
END


--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Search]    Script Date: 11/30/2018 3:12:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Search]
@TagId int,
@Type int
AS
BEGIN
	--SELECT * FROM BoxTagSeoEmbed where TagId =@TagId and Type=@Type	ORDER BY SortOrder ASC

	select * from (
		select b.id,b.tagid,b.sortorder,b.type,b.objectid,b.objecttype,b.lastmodifieddate 
		,case when b.objecttype=1 then n.title when b.objecttype=2 then v.name when b.objecttype=3 then t.topicname end as title
		,case when b.objecttype=1 then n.avatar when b.objecttype=2 then v.avatar when b.objecttype=3 then t.logo end as avatar
		,case when b.objecttype=1 then n.url when b.objecttype=2 then v.url when b.objecttype=3 then t.displayurl end as url
		,case when b.objecttype=1 then n.sapo when b.objecttype=2 then v.description when b.objecttype=3 then t.description end as description
		from boxtagseoembed b
		left join news n on n.id=CAST(b.objectid AS bigint)
		left join video v on v.id=CAST(b.objectid AS bigint)
		left join topic t on t.id=CAST(b.objectid AS bigint)
		where b.tagid=@TagId and b.type=@Type
	) as tb
	group by id,tagid,sortorder,type,objectid,objecttype,lastmodifieddate,title,avatar,url,description
	order by sortorder asc
END



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Update]    Script Date: 11/29/2018 10:41:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Update] 
	@Id int,
	@TagId int= 0,
	@Type int =0,	
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@ObjectId varchar(100)='0',
	@ObjectType int=0
AS
BEGIN
    UPDATE BoxTagSeoEmbed SET 
			TagId=@TagId,	
			Type=@Type,			
			Title=@Title,
			Avatar=@Avatar,
			Url=@Url,
			Description=@Description,
			SortOrder=@SortOrder,
			LastModifiedDate=getdate(),
			ObjectId=@ObjectId,
			ObjectType=@ObjectType
	WHERE Id=@Id
END
