--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Update]    Script Date: 11/29/2018 10:41:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Update] 
	@Id int,
	@TagId int= 0,
	@Type int =0,	
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@ObjectId varchar(100)='0',
	@ObjectType int=0
AS
BEGIN
    UPDATE BoxTagSeoEmbed SET 
			TagId=@TagId,	
			Type=@Type,			
			Title=@Title,
			Avatar=@Avatar,
			Url=@Url,
			Description=@Description,
			SortOrder=@SortOrder,
			LastModifiedDate=getdate(),
			ObjectId=@ObjectId,
			ObjectType=@ObjectType
	WHERE Id=@Id
END

