--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagSeoEmbed_Insert]    Script Date: 11/29/2018 10:35:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
create PROCEDURE [dbo].[CMS_BoxTagSeoEmbed_Insert]	
	@Id int OUTPUT,
	@TagId int= 0,
	@Type int =0,	
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@ObjectId varchar(100)='0',
	@ObjectType int=0
AS
BEGIN 
	INSERT INTO BoxTagSeoEmbed (
			TagId,	
			Type,			
			Title,
			Avatar,
			Url,
			Description,
			SortOrder, 			
			LastModifiedDate,
			ObjectId,
			ObjectType
		)
		VALUES(
			@TagId,	
			@Type,					
			@Title,
			@Avatar,
			@Url,
			@Description,
			@SortOrder,
			getdate(),
			@ObjectId,
			@ObjectType
		)

	SET @Id = SCOPE_IDENTITY();
END

