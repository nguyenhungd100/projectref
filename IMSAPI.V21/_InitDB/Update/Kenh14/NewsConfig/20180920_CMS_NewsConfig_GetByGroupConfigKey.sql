--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByGroupConfigKey]    Script Date: 9/20/2018 3:48:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edite: chinhnb 20-09-2018
-- Description:	<Get news config by GroupConfigKey>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByGroupConfigKey]
	@ConfigGroupKey varchar(50),
	@ConfigType int
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE ConfigGroupKey LIKE @ConfigGroupKey AND ((@ConfigType <= 0) OR (@ConfigType > 0 AND ConfigValueType = @ConfigType))
END


















