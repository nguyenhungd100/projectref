--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetAll]    Script Date: 9/20/2018 4:03:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edit: chinhnb 20-09-2018
-- Description:	<Get all news config>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetAll]
@PageIndex INT = 1 ,
@PageSize INT = 10 ,
@TotalRow INT = 0 OUT
AS
BEGIN
	SELECT @TotalRow = COUNT(*) FROM NewsConfig

	SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY LastModifiedDate DESC ) AS RowNum
                  FROM      NewsConfig                  
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
END


















