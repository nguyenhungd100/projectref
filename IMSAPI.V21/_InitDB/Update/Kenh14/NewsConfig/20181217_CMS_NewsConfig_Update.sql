--USE [IMS2_THOIDAI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_Update]    Script Date: 12/17/2018 3:33:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <2018-09-20>
-- Description:	<set value and label for news config>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_Update]
	@ConfigName varchar(200),
	@ConfigValue ntext,
	@ConfigLabel nvarchar(200),
	@ConfigType int,
	@ConfigInitValue varchar(max),
	@ConfigGroupKey varchar(50),
	@ConfigGroup nvarchar(200)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM NewsConfig WHERE ConfigName = @ConfigName)
		UPDATE NewsConfig
		SET ConfigValue = @ConfigValue,
			ConfigLabel = @ConfigLabel,
			ConfigValueType = @ConfigType,
			ConfigInitValue=@ConfigInitValue,
			ConfigGroupKey=@ConfigGroupKey,
			ConfigGroup=@ConfigGroup,
			LastModifiedDate=getdate()
		WHERE ConfigName = @ConfigName
	ELSE
		INSERT INTO NewsConfig (ConfigName,ConfigValue, ConfigValueType, ConfigLabel, ConfigInitValue, ConfigGroupKey, ConfigGroup, LastModifiedDate)
		VALUES (@ConfigName, @ConfigValue, @ConfigType, @ConfigLabel, @ConfigInitValue, @ConfigGroupKey, @ConfigGroup, getdate())
END

--USE [IMS2_VINFAST_DEV]
