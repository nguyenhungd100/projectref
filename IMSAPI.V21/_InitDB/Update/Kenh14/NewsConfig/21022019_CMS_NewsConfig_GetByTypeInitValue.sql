--USE [IMS2_VIETNAMBIZ]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByConfigName]    Script Date: 2/23/2019 10:50:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: chinhnb 20-09-2018
-- Create date: <20-02-2019>
-- Description:	<Get news config by type value>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsConfig_GetByTypeInitValue] --10, '169'
	@ConfigValueType int,
	@ConfigInitValue varchar(max)
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE ConfigValueType = @ConfigValueType and ConfigInitValue = @ConfigInitValue
END
