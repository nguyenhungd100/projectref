--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByConfigName]    Script Date: 9/20/2018 3:50:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb 20-09-2018
-- Create date: <2012-10-23>
-- Description:	<Get news config by config name>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByConfigName]
	@ConfigName varchar(200)
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE (ConfigName = @ConfigName)
END


















