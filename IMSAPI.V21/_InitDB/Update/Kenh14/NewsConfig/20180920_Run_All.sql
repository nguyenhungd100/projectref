alter table NewsConfig
add ConfigGroup nvarchar(200),LastModifiedDate datetime

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_SetValueAndLabelByType]    Script Date: 9/20/2018 2:57:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <2018-09-20>
-- Description:	<set value and label for news config>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsConfig_Update]
	@ConfigName varchar(200),
	@ConfigValue ntext,
	@ConfigLabel nvarchar(200),
	@ConfigType int,
	@ConfigInitValue varchar(max),
	@ConfigGroupKey varchar(50),
	@ConfigGroup nvarchar(200)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM NewsConfig WHERE ConfigName = @ConfigName)
		UPDATE NewsConfig
		SET ConfigValue = @ConfigValue,
			ConfigLabel = @ConfigLabel,
			ConfigValueType = @ConfigType,
			ConfigInitValue=@ConfigInitValue,
			ConfigGroupKey=@ConfigGroupKey,
			ConfigGroup=@ConfigGroup,
			LastModifiedDate=getdate()
		WHERE ConfigName = @ConfigName
	ELSE
		INSERT INTO NewsConfig (ConfigName,ConfigValue, ConfigValueType, ConfigLabel, ConfigInitValue, ConfigGroupKey, ConfigGroup)
		VALUES (@ConfigName, @ConfigValue, @ConfigType, @ConfigLabel, @ConfigInitValue, @ConfigGroupKey, @ConfigGroup)
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetAll]    Script Date: 9/20/2018 4:03:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edit: chinhnb 20-09-2018
-- Description:	<Get all news config>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetAll]
@PageIndex INT = 1 ,
@PageSize INT = 10 ,
@TotalRow INT = 0 OUT
AS
BEGIN
	SELECT @TotalRow = COUNT(*) FROM NewsConfig

	SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY LastModifiedDate DESC ) AS RowNum
                  FROM      NewsConfig                  
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByConfigName]    Script Date: 9/20/2018 3:50:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb 20-09-2018
-- Create date: <2012-10-23>
-- Description:	<Get news config by config name>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByConfigName]
	@ConfigName varchar(200)
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE (ConfigName = @ConfigName)
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByGroupConfigKey]    Script Date: 9/20/2018 3:48:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edite: chinhnb 20-09-2018
-- Description:	<Get news config by GroupConfigKey>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByGroupConfigKey]
	@ConfigGroupKey varchar(50),
	@ConfigType int
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE ConfigGroupKey LIKE @ConfigGroupKey AND ((@ConfigType <= 0) OR (@ConfigType > 0 AND ConfigValueType = @ConfigType))
END

