USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_InitESAllTag]    Script Date: 05/02/2018 17:56:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_Tag_InitESAllTag]	
	@PageIndex int = 1,
	@PageSize int = 10,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@TotalRow int = 0 OUTPUT
AS
begin
DECLARE @UpperBand int, @LowerBand int

SELECT @totalRow = COUNT(*) FROM Tag T where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))

SET @LowerBand  = (@pageIndex - 1) * @PageSize
SET @UpperBand  = (@pageIndex * @PageSize)
SELECT * FROM (
SELECT T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId, Z.Name AS ZoneName, 
	SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, ROW_NUMBER() OVER(ORDER BY T.Id DESC) AS RowNumber 
FROM Tag T 
left join TagZone TZ ON T.Id = TZ.TagId
left join TagNews AS TN ON T.Id = TN.TagId
left join Zone Z ON Z.Id = TZ.ZoneId
where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,T.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,T.CreatedDate)<=CONVERT(datetime,@DateTo)))
group by T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,
	T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName,
	T.IsThread, T.Avatar, T.Priority, T.TemplateId, TZ.ZoneId, Z.Name
) AS temp
WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
end

--declare @total int
--exec [CMS_Tag_InitESAllTag] 1,100,null,null,@TotalRow=@total