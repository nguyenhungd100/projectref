--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_Update]    Script Date: 11/26/2018 11:07:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--26/11/2018
CREATE PROC [dbo].[CMS_SEOMetaTopic_Update]
@MetaTitle nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@TopicId int
AS
BEGIN 
	IF(select count(*) from SEOMetaTopic where TopicId=@TopicId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTopic (MetaTitle, MetaKeyword, MetaDescription,TopicId) 
		  VALUES (@MetaTitle,@MetaKeyword,@MetaDescription,@TopicId)  						
		END
	ELSE
		UPDATE SEOMetaTopic SET MetaTitle =@MetaTitle, MetaKeyword =@MetaKeyword, MetaDescription =@MetaDescription  WHERE TopicId =@TopicId
END
