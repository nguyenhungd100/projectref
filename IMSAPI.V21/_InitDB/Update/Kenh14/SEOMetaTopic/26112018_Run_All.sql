--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[SEOMetaTopic]    Script Date: 11/26/2018 9:15:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SEOMetaTopic](
	[TopicId] [int] NOT NULL,
	[MetaTitle] [nvarchar](300) NULL,
	[MetaKeyword] [nvarchar](300) NULL,
	[MetaDescription] [nvarchar](300) NULL,
 CONSTRAINT [PK_SEOMetaTopic] PRIMARY KEY CLUSTERED 
(
	[TopicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_Delete]    Script Date: 11/26/2018 9:13:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--26/11/2018
CREATE PROC [dbo].[CMS_SEOMetaTopic_Delete]
@Id int
AS
BEGIN 
DELETE SEOMetaTopic WHERE TopicId =@Id 
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_GetList]    Script Date: 11/26/2018 9:18:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
CREATE PROCEDURE [dbo].[CMS_SEOMetaTopic_GetList]
@Keyword nvarchar(2000),
@TopicId int ,
@PageIndex int,
@PageSize int,
@TotalRow int out
AS

DECLARE @sql nvarchar(max),
		@where nvarchar(500);
		
DECLARE @lbound int,
		@ubound int,
		@recct  int;

SET @where ='1=1';

IF @TopicId = -1
	BEGIN
   SET @where= @where+ ' '
   END
ELSE 
BEGIN
    SET @where = @where+ ' AND S.TopicId='+CONVERT(varchar,@TopicId)
    END
IF @KeyWord IS NOT NULL AND @KeyWord <>''
BEGIN
SET @KeyWord ='N''%'+@KeyWord +'%''';
SET @where=@where+'AND (S.MetaTitle LIKE '+@KeyWord+'OR S.MetaKeyword LIKE '+@KeyWord+'OR S.MetaDescription LIKE '+@KeyWord+')';

END

SET @sql ='SELECT @recct=COUNT(*)FROM SEOMetaTopic AS S INNER JOIN Topic AS Z ON S.TopicId = Z.Id WHERE '+@where
PRINT   @sql
EXEC sp_executeSQL @sql, @params = N'@recct INT OUTPUT', @recct = @recct OUTPUT

	SET @lbound = (@PageIndex - 1)*@PageSize;
	SET @ubound = @lbound + @PageSize
	
	IF @recct IS NOT NULL AND @recct > 0
		SET @TotalRow = @recct;
	ELSE SET @TotalRow = 0;
DECLARE @Field nvarchar(1000);
SET @Field ='[Id]
			 ,[MetaTitle]
			 ,[MetaKeyword]
			 ,[MetaDescription]
			 ,[TopicId]
			 ,[TopicMetaTitle]
			
'
SET @sql ='SELECT '+@Field+' FROM (SELECT ROW_NUMBER() OVER(ORDER BY Id DESC)AS row,'+@Field+'
                  FROM(SELECT S.Id, S.MetaTitle, S.MetaKeyword, S.MetaDescription, S.TopicId, Z.MetaTitle AS TopicMetaTitle FROM SEOMetaTopic AS S LEFT JOIN Topic AS Z ON S.TopicId = Z.Id WHERE '+@where+'
                                
                  
                  )AS TblTemp

)AS TblTemp2 WHERE row > '+CONVERT(varchar(9),@lbound)+'
              AND row<='+CONVERT(varchar(9),@ubound)
   EXEC (@sql);


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_Insert]    Script Date: 11/26/2018 9:20:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
CREATE PROC [dbo].[CMS_SEOMetaTopic_Insert]
@MetaTitle nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@TopicId int,
@Id int OUT
AS
BEGIN 
	IF(select count(*) from SEOMetaTopic where TopicId=@TopicId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTopic (MetaTitle, MetaKeyword, MetaDescription,TopicId) 
		  VALUES (@MetaTitle,@MetaKeyword,@MetaDescription,@TopicId)  
			
			SET @Id = SCOPE_IDENTITY()
		END
	ELSE
		SET @Id = 0   
END

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_Update]    Script Date: 11/26/2018 11:07:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--26/11/2018
CREATE PROC [dbo].[CMS_SEOMetaTopic_Update]
@MetaTitle nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@TopicId int
AS
BEGIN 
	IF(select count(*) from SEOMetaTopic where TopicId=@TopicId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTopic (MetaTitle, MetaKeyword, MetaDescription,TopicId) 
		  VALUES (@MetaTitle,@MetaKeyword,@MetaDescription,@TopicId)  						
		END
	ELSE
		UPDATE SEOMetaTopic SET MetaTitle =@MetaTitle, MetaKeyword =@MetaKeyword, MetaDescription =@MetaDescription  WHERE TopicId =@TopicId
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_GetById]    Script Date: 11/26/2018 10:03:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--26/11/2018
CREATE PROCEDURE [dbo].[CMS_SEOMetaTopic_GetByTopicId]
@TopicId int 
AS
BEGIN 
	SELECT *  FROM SEOMetaTopic WHERE TopicId =@TopicId
END