--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTopic_Insert]    Script Date: 11/26/2018 9:20:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
CREATE PROC [dbo].[CMS_SEOMetaTopic_Insert]
@MetaTitle nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@TopicId int,
@Id int OUT
AS
BEGIN 
	IF(select count(*) from SEOMetaTopic where TopicId=@TopicId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTopic (MetaTitle, MetaKeyword, MetaDescription,TopicId) 
		  VALUES (@MetaTitle,@MetaKeyword,@MetaDescription,@TopicId)  
			
			SET @Id = 0
		END
	ELSE
		SET @Id = 0   
END
