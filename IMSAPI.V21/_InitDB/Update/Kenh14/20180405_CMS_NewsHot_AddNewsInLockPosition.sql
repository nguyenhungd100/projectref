USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsHot_ChangeNewsInLockPosition]    Script Date: 04/05/2018 15:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--05/04/2018
CREATE PROCEDURE [dbo].[CMS_NewsHot_AddNewsInLockPosition]	
	@NewsId bigint,
	@SortOrder int,
	@StreamType int
AS
BEGIN		
		DECLARE 
				@Title nvarchar(200),
				@Avatar nvarchar(500),
				@Sapo nvarchar(2000),
				@DistributionDate datetime,
				@Url nvarchar(500),
				@IsBreakingNews bit,
				@ZoneName nvarchar(255),
				@ZoneUrl nvarchar(100),
				@DisplayStyle int,
				@NewsRelation nvarchar(max),
				@NewsType int,
				@Type int,
				@SubTitle nvarchar(250),
				@TagSubTitleId int,
				@InitSapo nvarchar(500)
	
		SELECT	
				@Title = NP.Title, 
				@Avatar = NP.Avatar, 
				@Sapo = NP.Sapo, 
				@DistributionDate = NP.DistributionDate, 
				@Url = NP.Url, 
				@IsBreakingNews = NP.IsBreakingNews, 
				@ZoneName = Z.Name, 
				@ZoneUrl = Z.ShortURL,
				@DisplayStyle = NP.DisplayStyle,
				@NewsRelation = NP.NewsRelation,
				@NewsType = NP.NewsType,
				@Type = NP.Type,
				@SubTitle = NP.SubTitle,
				@TagSubTitleId = NP.TagSubTitleId,
				@InitSapo = NP.InitSapo
		FROM NewsPublish AS NP INNER JOIN
			Zone AS Z ON NP.ZoneId = Z.Id
		WHERE (NP.IsPrimary = 1) AND (NP.NewsId = @NewsId)
	
		IF @Title IS NOT NULL AND @Title <> ''
			INSERT INTO NewsTopHot
				(
				NewsId,
				Title, 
				PictureUrl, 
				Sapo, 
				PublishDate,
				Url,  
				IsBreakingNews,
				CategoryName, 
				CategoryHref, 
				LastModifiedDate,
				DisplayStyle,
				NewsRelation,
				NewsType,
				Type,
				SubTitle,
				TagSubTitleId,
				InitSapo,
				SortOrder,
				StreamType
				)
			VALUES
				(
				@NewsId,
				@Title, 
				@Avatar, 
				@Sapo, 
				@DistributionDate,
				@Url,  
				@IsBreakingNews,
				@ZoneName, 
				@ZoneUrl, 
				GETDATE(),
				@DisplayStyle,
				@NewsRelation,
				@NewsType,
				@Type,
				@SubTitle,
				@TagSubTitleId,
				@InitSapo,
				@SortOrder,
				@StreamType
				)	
END





