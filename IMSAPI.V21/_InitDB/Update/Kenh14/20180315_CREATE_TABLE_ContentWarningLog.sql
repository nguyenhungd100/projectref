use [IMS2_FULL]
go
--chinhnb
--15/03/2018
CREATE TABLE [dbo].[ContentWarningLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[Account] [varchar](50) NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_ContentWarningLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
