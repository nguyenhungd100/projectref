USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoChannel_Update]    Script Date: 05/17/2018 16:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Update VideoChannel>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_VideoChannel_Update]
	@PublisherId int,	
	@Name nvarchar(200),
	@Avatar nvarchar(300),	
	@Description nvarchar(max) = '',
	@Priority int = 0,
	@CreatedBy varchar(50),
	@CreatedDate datetime,
	@Url nvarchar(500)='',
	@FollowCount int = 0,
	@VideoCount int = 0,
	@Rank int = 0,
	@IntroClip nvarchar(500)='',
	@ChannelRelation nvarchar(max)='',	
	@Status bit = 0,	
	@LastModifiedBy varchar(50),
	@LastModifiedDate datetime,	
	
	@ZoneId int,
	@LabelId int,
	@ZoneIdList varchar(500) = '',
	@LabelIdList varchar(500) = '',	
	@PlayListIdList varchar(500) = '',
	@VideoIdList varchar(500) = '',
	@Cover nvarchar(500),	
	
	@MetaJson nvarchar(max)='',
	@Mode int=0,
	@MetaAvatar nvarchar(max)='',
	
    @Id int
AS
	BEGIN TRANSACTION
	
    IF (@Id <= 0 OR NOT EXISTS (SELECT Id FROM VideoChannel WHERE Id = @Id))
		BEGIN
			RAISERROR ('Can not found this videochannel.', 16, 1);
			ROLLBACK TRANSACTION
		END
    ELSE
		BEGIN						
			UPDATE VideoChannel
			SET
				ZoneId=@ZoneId,
				PublisherId=@PublisherId,
				Name=@Name,
				Avatar=@Avatar,						
				[Description]=@Description,
				Priority=@Priority,
				--CreatedBy=@CreatedBy,
				--CreatedDate=@CreatedDate,
				Url=@Url,
				FollowCount=@FollowCount,
				VideoCount=@VideoCount,
				[Rank]=@Rank,
				IntroClip=@IntroClip,
				ChannelRelation=@IntroClip,
				[Status]=@Status,
				LastModifiedBy=@LastModifiedBy,
				LastModifiedDate=@LastModifiedDate,
				Cover=@Cover,
				MetaJson=@MetaJson,
				Mode=@Mode,
				MetaAvatar=@MetaAvatar
			WHERE Id = @Id
			
			DECLARE @Index int;
						
			-- DELETE ALL VideoChannelInZone of this ChannelId
			DELETE FROM VideoChannelInZone  WHERE ChannelId = @Id			
			-- Process VideoChannelInZone					
			INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
			VALUES (@ZoneId, @Id, @CreatedDate, 1)
			
			DECLARE @TblVideoChannelInZone TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInZone(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInZone)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblVideoChannelInZone WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInZone WHERE ChannelId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
						VALUES (@ZoneIdTemp, @Id, @CreatedDate, 0)
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- DELETE ALL VideoChannelInLabel of this VideoChannelIdChannelId
			DELETE FROM VideoChannelInLabel  WHERE ChannelId = @Id
			-- Process VideoChannelInLabel					
			INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
			VALUES (@Id, @LabelId, 1)
			
			DECLARE @TblVideoChannelInLabel TABLE(Id int identity(1,1), LabelId int NOT NULL);
			DECLARE @LabelIdTemp int
			IF (LTRIM(RTRIM(@LabelIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInLabel(LabelId)
				SELECT CONVERT(int, part) FROM SplitString(@LabelIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInLabel)) BEGIN
					SET @LabelIdTemp = (SELECT LabelId FROM @TblVideoChannelInLabel WHERE Id = @Index);
					
					IF @LabelIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInLabel WHERE ChannelId = @Id AND LabelId = @LabelIdTemp)
					BEGIN
						INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
						VALUES (@Id, @LabelIdTemp, 0)
					END
					SET @Index = @Index + 1;
				END
			END	
			
			-- DELETE ALL PlayListInChannel of this VideoChannelIdChannelId
			DELETE FROM PlayListInChannel  WHERE ChannelId = @Id
			-- Process PlayListInChannel
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblPlayListInChannel TABLE(Id int identity(1,1), PlayListId int NOT NULL);
				DECLARE @PlayListId int;
				
				INSERT @TblPlayListInChannel(PlayListId)
				SELECT CONVERT(int, part) FROM SplitString(@PlayListIdList, ';')
								
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblPlayListInChannel))
				BEGIN
					SET @PlayListId = (SELECT PlayListId FROM @TblPlayListInChannel WHERE Id = @Index)
					
					IF @PlayListId IS NOT NULL AND @PlayListId > 0
					BEGIN
						INSERT PlayListInChannel (PlayListId, ChannelId)
						VALUES (@PlayListId, @Id)
					END												
					SET @Index = @Index + 1;
				END
			END	
			
			-- Process VideoInChannel: chưa xử lý	
			
		END
		      
IF @@ERROR <> 0
BEGIN
	DECLARE @Message nvarchar(500);
	SET @Message = ERROR_MESSAGE();
	RAISERROR (@Message, 16, 1)
	ROLLBACK TRANSACTION
END ELSE
	COMMIT TRANSACTION



