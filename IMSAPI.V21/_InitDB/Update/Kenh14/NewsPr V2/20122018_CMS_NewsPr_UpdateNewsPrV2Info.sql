--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPr_UpdateNewsPrInfo]    Script Date: 12/21/2018 12:11:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
CREATE PROCEDURE [dbo].[CMS_NewsPr_UpdateNewsPrV2Info]
    @NewsId BIGINT ,
    @DistributionDate DATETIME ,   
    @Mode INT ,    
    @ZoneId INT = 0    
AS 
    BEGIN
        BEGIN TRANSACTION	
        BEGIN TRY		
            BEGIN
                UPDATE  NewsPr
                SET     Mode = @Mode ,
                        ZoneId = @ZoneId ,
                        DistributionDate = @DistributionDate                        
                WHERE   ( NewsId = @NewsId )
		
            END
		
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END



