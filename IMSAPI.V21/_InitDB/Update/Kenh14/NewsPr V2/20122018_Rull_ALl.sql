--USE [IMS_TECHCLOUD_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPr_RecieveNewsPrIntoCms]    Script Date: 12/20/2018 3:29:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_NewsPr_RecieveNewsPrV2IntoCms]
    @NewsId BIGINT ,
    @ContentId BIGINT ,
    @DistributionId BIGINT ,
    @Mode INT ,
    @IsFocus BIT ,
    @Duration INT ,
    @DistributionDate DATETIME ,
    @PublishDate DATETIME ,
    @ExpireDate DATETIME ,
    @CreatedBy NVARCHAR(50) ,
    @ZoneId INT ,
    @Title NVARCHAR(250) ,
    @SubTitle NVARCHAR(250) ,
    @Sapo NVARCHAR(MAX) ,
    @Body NTEXT ,
    @Avatar NVARCHAR(500) ,
	@Avatar2 NVARCHAR(500) ,
	@Avatar3 NVARCHAR(500) ,
	@Avatar4 NVARCHAR(500) ,
    @Avatar5 NVARCHAR(500) ,
    @AvatarDesc NVARCHAR(500) ,
    @Source NVARCHAR(100) ,
    @Author NVARCHAR(500) ,
    @WordCount INT ,
    @IsPr BIT ,
    @PrPosition INT ,
    @AdStore BIT ,
    @AdStoreUrl NVARCHAR(500) ,
    @PrBookingNumber VARCHAR(30) ,
    @Url NVARCHAR(200) ,
    @LocationType INT ,
    @Status INT ,
    @NewsRelation VARCHAR(MAX) ,
    @ListNewsRelationId VARCHAR(2000) ,
    @TagIdList VARCHAR(2000),
    @BookingId BIGINT,
    @DisplayStyle INT = 0,
	@Price decimal = 0
AS 
    BEGIN
        BEGIN TRANSACTION	
        BEGIN TRY		
            BEGIN
                DECLARE @NewsPrId BIGINT
		
                SET @NewsPrId = ISNULL(( SELECT Id
                                         FROM   NewsPr
                                         WHERE  ContentId = @ContentId
                                                AND NewsId = @NewsId
                                       ), 0)
		
                IF @NewsPrId = 0 
                    BEGIN
                        INSERT  INTO NewsPr
                                ( ContentId ,
                                  DistributionId ,
                                  NewsId ,
                                  Mode ,
                                  IsFocus ,
                                  DistributionDate ,
                                  PublishDate ,
                                  ExpireDate ,
                                  CreatedDate ,
                                  Duration,
                                  BookingId,
								  ContractNo,
								  ZoneId,
								  Price
                                )
                        VALUES  ( @ContentId ,
                                  @DistributionId ,
                                  @NewsId ,
                                  @Mode ,
                                  @IsFocus ,
                                  @DistributionDate ,
                                  @PublishDate ,
                                  @ExpireDate ,
                                  GETDATE() ,
                                  @Duration,
                                  @BookingId,
								  @PrBookingNumber,
								  @ZoneId,
								  @Price
                                )
			
                        INSERT  INTO News
                                ( Id ,
                                  Title ,
                                  SubTitle ,
                                  Sapo ,
                                  Body ,
                                  Avatar ,
								  Avatar2 ,
								  Avatar3 ,
								  Avatar4 ,
                                  Avatar5 ,
                                  AvatarDesc ,
                                  Author ,
                                  Status ,
                                  Source ,
                                  IsFocus ,
                                  CreatedDate ,
                                  LastModifiedDate ,
                                  CreatedBy ,
                                  LastModifiedBy ,
                                  PublishedBy ,
                                  EditedBy ,
                                  LastReceiver ,
                                  WordCount ,
                                  Url ,
                                  OriginalUrl ,
                                  IsPr ,
                                  AdStore ,
                                  AdStoreUrl ,
                                  PrBookingNumber ,
                                  PrPosition ,
                                  LocationType ,
                                  DistributionDate ,
                                  Type ,
                                  DisplayStyle ,
                                  NewsCategory ,
                                  Price ,
                                  ViewCount ,
                                  NewsType
                                )
                        VALUES  ( @NewsId ,
                                  @Title ,
                                  @SubTitle ,
                                  @Sapo ,
                                  @Body ,
                                  @Avatar ,
								  @Avatar2 ,
								  @Avatar3 ,
								  @Avatar4 ,
								  @Avatar5 ,
                                  @AvatarDesc ,
                                  @Author ,
                                  @Status ,
                                  @Source ,
                                  @IsFocus ,
                                  GETDATE() ,
                                  GETDATE() ,
                                  @CreatedBy ,
                                  @CreatedBy ,
                                  CASE @Status
                                    WHEN 6 THEN @CreatedBy
                                    ELSE ''
                                  END ,
                                  CASE @Status
                                    WHEN 3 THEN @CreatedBy
                                    WHEN 6 THEN @CreatedBy
                                    ELSE ''
                                  END ,
                                  @CreatedBy ,
                                  @WordCount ,
                                  @Url ,
                                  @Url ,
                                  @IsPr ,
                                  @AdStore ,
                                  @AdStoreUrl ,
                                  @PrBookingNumber ,
                                  @Mode ,
                                  @LocationType ,
                                  @PublishDate ,
                                  0 ,
                                  @DisplayStyle ,
                                  0 ,
                                  0 ,
                                  0 ,
                                  0
                                )
				
                        INSERT  NewsInZone
                                ( ZoneId, NewsId, IsPrimary )
                        VALUES  ( @ZoneId, @NewsId, 1 )
			
                        IF @ListNewsRelationId IS NOT NULL
                            AND @ListNewsRelationId <> '' 
                            BEGIN
                                DECLARE @TblTempRelation TABLE
                                    (
                                      Id INT IDENTITY(1, 1) ,
                                      NewsId BIGINT
                                    )
                                DECLARE @NewsItemId BIGINT ,
                                    @Index INT ,
                                    @Count INT
                                INSERT  INTO @TblTempRelation
                                        ( NewsId
                                        )
                                        SELECT  CONVERT(BIGINT, Part)
                                        FROM    SplitString(@ListNewsRelationId,
                                                            ',')
                                SET @Count = ( SELECT   COUNT(Id)
                                               FROM     @TblTempRelation
                                             )
                                SET @Index = 0;
                                WHILE ( @Index <= @Count ) 
                                    BEGIN
                                        SET @NewsItemId = ( SELECT
                                                              NewsId
                                                            FROM
                                                              @TblTempRelation
                                                            WHERE
                                                              Id = @Index
                                                          )
                                        IF @NewsItemId IS NOT NULL
                                            AND @NewsItemId > 0 
                                            IF NOT EXISTS ( SELECT
                                                              NewsRelationId
                                                            FROM
                                                              NewsRelation
                                                            WHERE
                                                              NewsRelationId = @NewsItemId
                                                              AND NewsId = @NewsId
                                                              AND ZoneId = @ZoneId ) 
                                                INSERT  INTO NewsRelation
                                                        ( NewsId ,
                                                          NewsRelationId ,
                                                          ZoneId ,
                                                          IsChange ,
                                                          Priority
                                                        )
                                                VALUES  ( @NewsId ,
                                                          @NewsItemId ,
                                                          @ZoneId ,
                                                          0 ,
                                                          @Index
                                                        );
                                        SET @Index = @Index + 1
                                    END
                            END
                            -- Insert TagNews
                        IF @TagIdList <> '' 
                            BEGIN
                                DECLARE @TblTempTag TABLE
                                    (
                                      Id INT IDENTITY(1, 1) ,
                                      TagId BIGINT
                                    )
                                DECLARE @TagItemId BIGINT; 
                                SET @Index = 0;
                                INSERT  INTO @TblTempTag
                                        ( TagId
                                        )
                                        SELECT  CONVERT(BIGINT, Part)
                                        FROM    SplitString(@TagIdList, ';')
                                WHILE ( @Index <= ( SELECT  COUNT(Id)
                                                    FROM    @TblTempTag
                                                  ) ) 
                                    BEGIN
                                        SET @TagItemId = ( SELECT
                                                              TagId
                                                           FROM
                                                              @TblTempTag
                                                           WHERE
                                                              Id = @Index
                                                         )
                                        IF @TagItemId IS NOT NULL
                                            AND @TagItemId > 0 
                                            IF NOT EXISTS ( SELECT
                                                              TagID
                                                            FROM
                                                              TagNews
                                                            WHERE
                                                              TagID = @TagItemId
                                                              AND NewsId = @NewsId
                                                              AND TagMode = 0 ) 
                                                INSERT  INTO TagNews
                                                        ( NewsId, TagID,
                                                          TagMode, Priority )
                                                VALUES  ( @NewsId, @TagItemId,
                                                          0, @Index );
                                        SET @Index = @Index + 1
                                    END
                            END
                    END
                ELSE 
                    BEGIN
                        UPDATE  NewsPr
                        SET     Mode = @Mode ,
                                IsFocus = @IsFocus ,
                                DistributionDate = @DistributionDate ,
                                PublishDate = @PublishDate ,
                                ExpireDate = @ExpireDate ,
                                Duration = @Duration,
                                BookingId = @BookingId,
								ContractNo = @PrBookingNumber,
								ZoneId = @ZoneId,
								Price = @Price
                        WHERE   ( Id = @NewsPrId )
			
                        UPDATE  News
                        SET     Title = @Title ,
                                SubTitle = @SubTitle ,
                                Sapo = @Sapo ,
                                Body = @Body ,
                                Avatar = @Avatar ,
								Avatar2 = @Avatar2 ,
								Avatar3 = @Avatar3 ,
								Avatar4 = @Avatar4 ,
								Avatar5 = @Avatar5 ,
                                AvatarDesc = @AvatarDesc ,
                                Author = @Author ,
                                Source = @Source ,
                                IsFocus = @IsFocus ,
                                LastModifiedDate = GETDATE() ,
                                LastModifiedBy = @CreatedBy ,
                                WordCount = @WordCount ,
                                Url = @Url ,
                                OriginalUrl = @Url ,
                                IsPr = @IsPr ,
                                AdStore = @AdStore ,
                                AdStoreUrl = @AdStoreUrl ,
                                PrBookingNumber = @PrBookingNumber ,
                                PrPosition = @Mode ,
                                LocationType = @LocationType ,
                                DistributionDate = @PublishDate,
                                DisplayStyle = @DisplayStyle
                        WHERE   ( Id = @NewsId )
                    END
            END
		
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END



--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPr_UpdateNewsPrInfo]    Script Date: 12/21/2018 12:11:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
CREATE PROCEDURE [dbo].[CMS_NewsPr_UpdateNewsPrV2Info]
    @NewsId BIGINT ,
    @DistributionDate DATETIME ,   
    @Mode INT ,    
    @ZoneId INT = 0    
AS 
    BEGIN
        BEGIN TRANSACTION	
        BEGIN TRY		
            BEGIN
                UPDATE  NewsPr
                SET     Mode = @Mode ,
                        ZoneId = @ZoneId ,
                        DistributionDate = @DistributionDate                        
                WHERE   ( NewsId = @NewsId )
		
            END
		
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END



