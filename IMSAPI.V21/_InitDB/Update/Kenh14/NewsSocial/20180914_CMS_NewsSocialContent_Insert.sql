--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Insert]
	@Id BIGINT OUTPUT
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    ,@OriginalId int
    ,@OriginalName nvarchar(250)
    ,@Url nvarchar(500)
    ,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    ,@Status int
    ,@Priority bigint
    ,@CreatedDate datetime
    ,@CreatedBy varchar(100)
    --,@UpdatedDate datetime
    --,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)
    ,@RawId varchar(36)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialContent (
		[Title]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[OriginalId]
      ,[OriginalName]
      ,[Url]
      ,[OriginalUrl]
      ,[AvatarCustom]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Status]
      ,[Priority]
      ,[CreatedDate]
      ,[CreatedBy]      
      ,[WordCount]
      ,[Note]
      ,[RawId]
	  )
		VALUES (
		@Title
		,@Sapo
		,@Body
		,@Avatar
		,@OriginalId
		,@OriginalName
		,@Url
		,@OriginalUrl
		,@AvatarCustom
		,@Avatar1
		,@Avatar2
		,@Avatar3
		,@Status
		,@Priority
		,@CreatedDate
		,@CreatedBy		
		,@WordCount
		,@Note
		,@RawId)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

