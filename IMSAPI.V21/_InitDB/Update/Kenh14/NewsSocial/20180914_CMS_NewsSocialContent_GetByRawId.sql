--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_GetById]    Script Date: 9/15/2018 10:07:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by rawid>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetByRawId]
	@RawId varchar(36)
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (RawId = @RawId)
END
