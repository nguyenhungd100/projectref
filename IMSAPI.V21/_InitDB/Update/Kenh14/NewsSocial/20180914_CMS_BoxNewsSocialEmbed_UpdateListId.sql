--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_Update]    Script Date: 9/14/2018 8:13:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
alter PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_UpdateListId] 
	@ListOfPriority nvarchar(MAX),	
	@Type int,
	@ZoneId int
AS
BEGIN
    DELETE BoxNewsSocialEmbed WHERE Type=@Type and ZoneId=@ZoneId
	DECLARE @Priority int, 
			@NewsId bigint
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	
	FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM BoxNewsSocialEmbed WHERE NewsId = @NewsId and Type =@Type and ZoneId=@ZoneId)
			INSERT INTO BoxNewsSocialEmbed (NewsId,SortOrder,Type, ZoneId, LastModifiedDate) values(@NewsId,@Priority,@Type,@ZoneId,DATEADD(second, @Priority, GETDATE()))
			FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId 
	END

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor
END
















