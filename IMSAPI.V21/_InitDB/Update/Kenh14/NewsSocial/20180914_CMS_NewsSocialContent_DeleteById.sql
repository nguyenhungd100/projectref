--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_DeleteById]    Script Date: 9/14/2018 2:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Delete NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM NewsSocialContent WHERE Id = @Id

		DELETE FROM BoxNewsSocialEmbed WHERE NewsId = @Id

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

