--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialContent]    Script Date: 9/14/2018 7:10:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialContent](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Sapo] [nvarchar](max) NULL,
	[Body] [ntext] NULL,
	[Avatar] [nvarchar](500) NULL,
	[OriginalId] [int] NULL,
	[OriginalName] [nvarchar](250) NULL,
	[Url] [nvarchar](500) NULL,
	[OriginalUrl] [nvarchar](500) NULL,
	[AvatarCustom] [nvarchar](500) NULL,
	[Avatar1] [nvarchar](500) NULL,
	[Avatar2] [nvarchar](500) NULL,
	[Avatar3] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[Priority] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[WordCount] [int] NULL,
	[Note] [nvarchar](2000) NULL,
	[RawId] [varchar](36) NULL,
 CONSTRAINT [PK_NewsSocialContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_WordCount]  DEFAULT ((0)) FOR [WordCount]
GO


