--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialEmbed]    Script Date: 9/14/2018 2:41:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialEmbed](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RawId] [varchar](36) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Priority] [bigint] NULL,
	[Status] [int] NULL,
	[OriginalId] [int] NULL,
	[Url] [nvarchar](1000) NULL,
	[Embed] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=Facebook|2=Tweeter|3=Intagram' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NewsSocialEmbed', @level2type=N'COLUMN',@level2name=N'OriginalId'
GO


