--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxNewsSocialEmbed]    Script Date: 9/14/2018 4:58:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxNewsSocialEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxNewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


