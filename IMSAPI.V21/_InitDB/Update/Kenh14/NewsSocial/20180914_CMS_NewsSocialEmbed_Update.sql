--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Update]
	@Id BIGINT,		
	@Priority bigint,	
	@Status int,	
	@Embed nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialEmbed
		SET [Priority] = @Priority, 			
			[Status] = @Status, 
			Embed = @Embed
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

