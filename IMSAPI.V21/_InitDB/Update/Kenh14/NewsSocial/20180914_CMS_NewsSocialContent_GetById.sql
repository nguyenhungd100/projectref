--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocial_GetById]    Script Date: 9/14/2018 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (Id = @Id)
END

