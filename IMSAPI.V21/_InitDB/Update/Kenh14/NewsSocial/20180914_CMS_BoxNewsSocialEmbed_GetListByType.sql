--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_GetListByZone]    Script Date: 9/14/2018 8:01:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
alter PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_GetListByType]
@Type int,
@ZoneId int
AS
BEGIN
	SELECT TOP(20) BN.NewsId,BN.SortOrder,BN.Type,NC.Avatar,NC.Title, BN.ZoneId
	FROM BoxNewsSocialEmbed as BN 
	inner join NewsSocialContent as NC on BN.NewsId =NC.Id
	where BN.Type=@Type And BN.ZoneId=@ZoneId
	ORDER BY BN.SortOrder ASC
END

















