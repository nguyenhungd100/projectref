--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_DeleteById]    Script Date: 9/14/2018 8:42:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Toggle Status NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_ToggleStatus]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		Update NewsSocialEmbed SET [Status] = CASE WHEN [Status]=1 THEN 0 ELSE 1 END WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END
