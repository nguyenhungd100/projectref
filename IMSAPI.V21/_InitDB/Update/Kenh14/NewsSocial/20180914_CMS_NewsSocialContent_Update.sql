--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Update]
	@Id BIGINT	
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    --,@OriginalId int
    --,@OriginalName nvarchar(250)
    --,@Url nvarchar(500)
    --,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    --,@Status int
    ,@Priority bigint
    --,@CreatedDate datetime
    --,@CreatedBy varchar(100)
    ,@UpdatedDate datetime
    ,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)    
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialContent
		SET Title = @Title, 			
			Sapo = @Sapo, 
			Body = @Body,
			Avatar=@Avatar,
			AvatarCustom=@AvatarCustom,
			Avatar1=@Avatar1,
			Avatar2=@Avatar2,
			Avatar3=@Avatar3,
			Priority=@Priority,
			UpdatedDate=@UpdatedDate,
			UpdatedBy=@UpdatedBy,
			WordCount=@WordCount,
			Note=@Note
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

