--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Insert]
	@Id BIGINT OUTPUT,
	@RawId varchar(36),
	@CreatedBy varchar(100) = '',
	@CreatedDate datetime,
	@Priority bigint,	
	@Status int,		
	@OriginalId int,
	@Url nvarchar(1000) = '',
	@Embed nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialEmbed (RawId, CreatedBy, CreatedDate, [Priority],[Status], OriginalId, [Url], Embed)
		VALUES (@RawId,@CreatedBy,@CreatedDate,@Priority,@Status,@OriginalId,@Url, @Embed)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

