--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Search]    Script Date: 9/14/2018 6:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Search NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Search] --'', 0, 1, 20, 0	
	@Keyword nvarchar(500)='',
	@Status int=-1,	
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Title) > 0 )'
	IF @Status > -1
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM NewsSocialContent AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM NewsSocialContent AS T '+ @where + ' '	
	SET @search = @search + ') AS NewsSocialContentTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,								
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

