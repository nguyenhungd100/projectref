--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialEmbed]    Script Date: 9/14/2018 2:41:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialEmbed](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RawId] [varchar](36) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Priority] [bigint] NULL,
	[Status] [int] NULL,
	[OriginalId] [int] NULL,
	[Url] [nvarchar](1000) NULL,
	[Embed] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=Facebook|2=Tweeter|3=Intagram' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NewsSocialEmbed', @level2type=N'COLUMN',@level2name=N'OriginalId'
GO

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialContent]    Script Date: 9/14/2018 7:10:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialContent](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Sapo] [nvarchar](max) NULL,
	[Body] [ntext] NULL,
	[Avatar] [nvarchar](500) NULL,
	[OriginalId] [int] NULL,
	[OriginalName] [nvarchar](250) NULL,
	[Url] [nvarchar](500) NULL,
	[OriginalUrl] [nvarchar](500) NULL,
	[AvatarCustom] [nvarchar](500) NULL,
	[Avatar1] [nvarchar](500) NULL,
	[Avatar2] [nvarchar](500) NULL,
	[Avatar3] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[Priority] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[WordCount] [int] NULL,
	[Note] [nvarchar](2000) NULL,
	[RawId] [varchar](36) NULL,
 CONSTRAINT [PK_NewsSocialContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_WordCount]  DEFAULT ((0)) FOR [WordCount]
GO


--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxNewsSocialEmbed]    Script Date: 9/14/2018 4:58:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxNewsSocialEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL,
	[NewsId] [bigint] NOT NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxNewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_GetListByZone]    Script Date: 9/14/2018 8:01:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
CREATE PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_GetListByType]
@Type int,
@ZoneId int
AS
BEGIN
	SELECT TOP(20) BN.NewsId,BN.SortOrder,BN.Type,NC.Avatar,NC.Title, BN.ZoneId
	FROM BoxNewsSocialEmbed as BN 
	inner join NewsSocialContent as NC on BN.NewsId =NC.Id
	where BN.Type=@Type And BN.ZoneId=@ZoneId
	ORDER BY BN.SortOrder ASC
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_Update]    Script Date: 9/14/2018 8:13:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
CREATE PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_UpdateListId] 
	@ListOfPriority nvarchar(MAX),	
	@Type int,
	@ZoneId int
AS
BEGIN
    DELETE BoxNewsSocialEmbed WHERE Type=@Type and ZoneId=@ZoneId
	DECLARE @Priority int, 
			@NewsId bigint
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	
	FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM BoxNewsSocialEmbed WHERE NewsId = @NewsId and Type =@Type and ZoneId=@ZoneId)
			INSERT INTO BoxNewsSocialEmbed (NewsId,SortOrder,Type, ZoneId, LastModifiedDate) values(@NewsId,@Priority,@Type,@ZoneId,DATEADD(second, @Priority, GETDATE()))
			FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId 
	END

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_DeleteById]    Script Date: 9/14/2018 8:42:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Toggle Status NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_ToggleStatus]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		Update NewsSocialEmbed SET [Status] = CASE WHEN [Status]=1 THEN 0 ELSE 1 END WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_DeleteById]    Script Date: 9/14/2018 8:42:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Toggle Status NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_ToggleStatus]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		Update NewsSocialContent SET [Status] = CASE WHEN [Status]=1 THEN 0 ELSE 1 END WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_DeleteById]    Script Date: 9/14/2018 2:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Delete NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM NewsSocialContent WHERE Id = @Id

		DELETE FROM BoxNewsSocialEmbed WHERE NewsId = @Id

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocial_GetById]    Script Date: 9/14/2018 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (Id = @Id)
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Insert]
	@Id BIGINT OUTPUT
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    ,@OriginalId int
    ,@OriginalName nvarchar(250)
    ,@Url nvarchar(500)
    ,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    ,@Status int
    ,@Priority bigint
    ,@CreatedDate datetime
    ,@CreatedBy varchar(100)
    --,@UpdatedDate datetime
    --,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)
    ,@RawId varchar(36)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialContent (
		[Title]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[OriginalId]
      ,[OriginalName]
      ,[Url]
      ,[OriginalUrl]
      ,[AvatarCustom]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Status]
      ,[Priority]
      ,[CreatedDate]
      ,[CreatedBy]      
      ,[WordCount]
      ,[Note]
      ,[RawId]
	  )
		VALUES (
		@Title
		,@Sapo
		,@Body
		,@Avatar
		,@OriginalId
		,@OriginalName
		,@Url
		,@OriginalUrl
		,@AvatarCustom
		,@Avatar1
		,@Avatar2
		,@Avatar3
		,@Status
		,@Priority
		,@CreatedDate
		,@CreatedBy		
		,@WordCount
		,@Note
		,@RawId)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Search]    Script Date: 9/14/2018 6:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Search NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Search] --'', 0, 1, 20, 0	
	@Keyword nvarchar(500)='',
	@Status int=-1,	
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Title) > 0 )'
	IF @Status > -1
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM NewsSocialContent AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM NewsSocialContent AS T '+ @where + ' '	
	SET @search = @search + ') AS NewsSocialContentTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,								
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Update]
	@Id BIGINT	
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    --,@OriginalId int
    --,@OriginalName nvarchar(250)
    --,@Url nvarchar(500)
    --,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    --,@Status int
    ,@Priority bigint
    --,@CreatedDate datetime
    --,@CreatedBy varchar(100)
    ,@UpdatedDate datetime
    ,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)    
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialContent
		SET Title = @Title, 			
			Sapo = @Sapo, 
			Body = @Body,
			Avatar=@Avatar,
			AvatarCustom=@AvatarCustom,
			Avatar1=@Avatar1,
			Avatar2=@Avatar2,
			Avatar3=@Avatar3,
			Priority=@Priority,
			UpdatedDate=@UpdatedDate,
			UpdatedBy=@UpdatedBy,
			WordCount=@WordCount,
			Note=@Note
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_GetById]    Script Date: 9/15/2018 10:07:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by rawid>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetByRawId]
	@RawId varchar(36)
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (RawId = @RawId)
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_DeleteById]    Script Date: 9/14/2018 2:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Delete NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM NewsSocialEmbed WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocial_GetById]    Script Date: 9/14/2018 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM NewsSocialEmbed
	WHERE (Id = @Id)
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Insert]
	@Id BIGINT OUTPUT,
	@RawId varchar(36),
	@CreatedBy varchar(100) = '',
	@CreatedDate datetime,
	@Priority bigint,	
	@Status int,		
	@OriginalId int,
	@Url nvarchar(1000) = '',
	@Embed nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialEmbed (RawId, CreatedBy, CreatedDate, [Priority],[Status], OriginalId, [Url], Embed)
		VALUES (@RawId,@CreatedBy,@CreatedDate,@Priority,@Status,@OriginalId,@Url, @Embed)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Search]    Script Date: 9/14/2018 6:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Search NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Search] --0, 1, 20, 0	
	@Status int=-1,	
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @Status > -1
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM NewsSocialEmbed AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM NewsSocialEmbed AS T '+ @where + ' '	
	SET @search = @search + ') AS NewsSocialEmbedTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,								
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Update]
	@Id BIGINT	
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    --,@OriginalId int
    --,@OriginalName nvarchar(250)
    --,@Url nvarchar(500)
    --,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    --,@Status int
    ,@Priority bigint
    --,@CreatedDate datetime
    --,@CreatedBy varchar(100)
    ,@UpdatedDate datetime
    ,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)    
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialContent
		SET Title = @Title, 			
			Sapo = @Sapo, 
			Body = @Body,
			Avatar=@Avatar,
			AvatarCustom=@AvatarCustom,
			Avatar1=@Avatar1,
			Avatar2=@Avatar2,
			Avatar3=@Avatar3,
			Priority=@Priority,
			UpdatedDate=@UpdatedDate,
			UpdatedBy=@UpdatedBy,
			WordCount=@WordCount,
			Note=@Note
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END



--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_GetById]    Script Date: 9/15/2018 10:07:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialEmbed by rawid>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_GetByRawId]
	@RawId varchar(36)
AS
BEGIN
	SELECT *
	FROM NewsSocialEmbed
	WHERE (RawId = @RawId)
END
