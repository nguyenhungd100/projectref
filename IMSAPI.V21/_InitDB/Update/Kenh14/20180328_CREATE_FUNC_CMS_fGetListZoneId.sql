USE [IMS2_KENH14_DEV]
GO
-- =============================================
-- Author:  <ThanhTN>
-- Create date: <2012-09-18>
-- Description: <Get list zone id and child zone id by list zone id>
-- =============================================
CREATE FUNCTION [dbo].[CMS_fGetListZoneId]
(
 @ZoneIds varchar(MAX)
)
RETURNS varchar(max)
AS
BEGIN
 -- Declare the return variable here
 DECLARE @ReturnZoneIds varchar(MAX)
 DECLARE @ZoneId int
 
 SET @ReturnZoneIds = ''
 
 IF @ZoneIds <> ''
 BEGIN
  DECLARE zone_cursor CURSOR FOR
  SELECT Id
  FROM Zone
  WHERE (PATINDEX('%,' + CONVERT(varchar(10), ParentId) + ',%', ',' + @ZoneIds + ',') > 0)
   AND (PATINDEX('%,' + CONVERT(varchar(10), Id) + ',%', ',' + @ZoneIds + ',') <= 0)

  OPEN zone_cursor   
  FETCH NEXT FROM zone_cursor INTO @ZoneId

  WHILE @@FETCH_STATUS = 0   
  BEGIN
   SET @ReturnZoneIds = @ReturnZoneIds + ',' + CONVERT(varchar(10), @ZoneId)
    
   FETCH NEXT FROM zone_cursor INTO @ZoneId 
  END   

  CLOSE zone_cursor   
  DEALLOCATE zone_cursor
 END

 SET @ReturnZoneIds = @ZoneIds + @ReturnZoneIds
  
 RETURN @ReturnZoneIds
 
END