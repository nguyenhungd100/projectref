--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]    Script Date: 9/15/2018 8:02:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]
	@NewsId int,
	@VideoInfo nvarchar(1000),
	@SaveFilePath nvarchar(500),
	@KeyVideo nvarchar(500)
AS
BEGIN
	 UPDATE VideoAutoDownloadMonitor SET Status = 2,DownloadCompleted = GETDATE(), KeyVideo = @KeyVideo, SaveFilePath = @SaveFilePath
	 WHERE NewsId = @NewsId;
	 
	 --UPDATE Interview SET VideoInfo = @VideoInfo WHERE Id = @InterviewId;
END
