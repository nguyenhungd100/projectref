--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_GetByInterview]    Script Date: 9/15/2018 7:25:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_GetByNewsId] @NewsId bigint
AS 
    SELECT  *
    FROM    VideoAutoDownloadMonitor
    WHERE   NewsId = @NewsId


