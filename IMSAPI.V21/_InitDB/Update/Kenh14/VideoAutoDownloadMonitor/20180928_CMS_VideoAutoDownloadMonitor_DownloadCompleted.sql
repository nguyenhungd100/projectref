--USE [IMS2_VINFAST]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]    Script Date: 9/28/2018 10:34:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit:28-09-2018
ALTER PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]
	@NewsId bigint,
	@VideoInfo nvarchar(1000),
	@SaveFilePath nvarchar(500),
	@KeyVideo nvarchar(500)
AS
BEGIN
	 UPDATE VideoAutoDownloadMonitor SET Status = 2,DownloadCompleted = GETDATE(), KeyVideo = @KeyVideo, SaveFilePath = @SaveFilePath
	 WHERE NewsId = @NewsId;
	 
	 --UPDATE Interview SET VideoInfo = @VideoInfo WHERE Id = @InterviewId;
END
