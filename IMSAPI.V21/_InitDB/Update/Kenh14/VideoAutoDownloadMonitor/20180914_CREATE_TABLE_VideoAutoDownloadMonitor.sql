--USE [IMS_VTV]
GO

/****** Object:  Table [dbo].[VideoAutoDownloadMonitor]    Script Date: 9/15/2018 6:24:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoAutoDownloadMonitor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[SourceUrl] [varchar](300) NULL,
	[SaveFilePath] [varchar](300) NULL,
	[KeyVideo] [varchar](100) NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[SentDownloadRequest] [datetime] NULL,
	[DownloadCompleted] [datetime] NULL,
	[UpdateDatabase] [datetime] NULL,
	[CounterCheckDownLoad] [int] NULL,
 CONSTRAINT [PK_VideoAutoDownloadMonitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


