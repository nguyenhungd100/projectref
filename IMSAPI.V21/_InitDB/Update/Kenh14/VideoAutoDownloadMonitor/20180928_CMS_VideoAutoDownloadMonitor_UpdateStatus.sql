--USE [IMS2_VINFAST]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]    Script Date: 9/28/2018 10:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit:28-09-2018
ALTER PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]
	@Ids nvarchar(1000),
	@Status INT
AS
BEGIN
	IF(@Status = 3)
		UPDATE VideoAutoDownloadMonitor SET Status = @Status, UpdateDatabase = GETDATE()
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(bigint,part))
	ELSE
		UPDATE VideoAutoDownloadMonitor SET Status = @Status
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(bigint,part))
END

