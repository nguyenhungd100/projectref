--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateInvisibled]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggerZoneStatus]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @Status int
			
			SET @Status = (SELECT Status FROM Zone WHERE Id=@ZoneId)
			
			IF(@Status = 1)
			UPDATE Zone SET Status = 0 WHERE Id = @ZoneId
			 
			ELSE IF (@Status = 0)
			 UPDATE Zone SET Status = 1 WHERE Id = @ZoneId
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH