--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicActive]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicActive]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @IsActive bit
						
			SET @IsActive = (SELECT IsActive FROM Topic WHERE Id=@Id)

			IF(@IsActive = 1)
			UPDATE Topic SET IsActive = 0 WHERE Id = @Id
			
			ELSE IF (@IsActive = 0)
			 UPDATE Topic SET IsActive = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH