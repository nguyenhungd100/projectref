--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateInvisibled]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneInvisibled]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @Invisibled bit
			--DECLARE @CountId int
			SET @Invisibled = (SELECT Invisibled FROM Zone WHERE Id=@ZoneId)
			--SET @CountId=( SELECT COUNT(*) FROM Zone WHERE ParentId =@ZoneId)
			IF(@Invisibled = 1)
			UPDATE Zone SET Invisibled = 0 WHERE Id = @ZoneId
			---	if(@CountId >0) UPDATE Zone SET Invisibled = 0 WHERE ParentId = @ZoneId
			 	
			 	 
			ELSE IF (@Invisibled = 0)
			 UPDATE Zone SET Invisibled = 1 WHERE Id = @ZoneId
			-- if(@CountId >0)UPDATE Zone SET Invisibled = 0 WHERE ParentId = @ZoneId
			 
			
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateInvisibled]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneStatus]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @Status int
			
			SET @Status = (SELECT Status FROM Zone WHERE Id=@ZoneId)
			
			IF(@Status = 1)
			UPDATE Zone SET Status = 0 WHERE Id = @ZoneId
			 
			ELSE IF (@Status = 0)
			 UPDATE Zone SET Status = 1 WHERE Id = @ZoneId
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateAllowComment]    Script Date: 4/3/2019 5:10:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneAllowComment]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @AllowComment bit
			--DECLARE @CountId int
			SET @AllowComment = (SELECT ISNULL(AllowComment, 0) FROM Zone WHERE Id=@ZoneId)
			--SET @CountId=( SELECT COUNT(*) FROM Zone WHERE ParentId =@ZoneId)
			IF(@AllowComment = 1)
			UPDATE Zone SET AllowComment = 0 WHERE Id = @ZoneId
			---	if(@CountId >0) UPDATE Zone SET AllowComment = 0 WHERE ParentId = @ZoneId
			 	
			 	 
			ELSE IF (@AllowComment = 0)
			 UPDATE Zone SET AllowComment = 1 WHERE Id = @ZoneId
			-- if(@CountId >0)UPDATE Zone SET AllowComment = 0 WHERE ParentId = @ZoneId
			 
			
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicActive]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicActive]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @IsActive bit
						
			SET @IsActive = (SELECT IsActive FROM Topic WHERE Id=@Id)

			IF(@IsActive = 1)
			UPDATE Topic SET IsActive = 0 WHERE Id = @Id
			
			ELSE IF (@IsActive = 0)
			 UPDATE Topic SET IsActive = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicIconActive]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicIconActive]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @IsIconActive bit
						
			SET @IsIconActive = (SELECT IsIconActive FROM Topic WHERE Id=@Id)

			IF(@IsIconActive = 1)
			UPDATE Topic SET IsIconActive = 0 WHERE Id = @Id
			
			ELSE IF (@IsIconActive = 0)
			 UPDATE Topic SET IsIconActive = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicIsToolbar]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicIsToolbar]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @isTopToolbar bit
						
			SET @isTopToolbar = (SELECT isTopToolbar FROM Topic WHERE Id=@Id)

			IF(@isTopToolbar = 1)
			UPDATE Topic SET isTopToolbar = 0 WHERE Id = @Id
			
			ELSE IF (@isTopToolbar = 0)
			 UPDATE Topic SET isTopToolbar = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Search]    Script Date: 4/4/2019 5:41:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Search] --'',-1,-1,-1,0,-1,1,200,0
	@Keyword nvarchar(500),
	@IsHot int = -1,
	@IsActive int = -1,
	@ZoneId int,
	@OrderBy int,
	@ParentId int=-1,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@IsHot int,'
	SET @params = @params + '@IsActive int,'
	SET @params = @params + '@ParentId int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.TopicName) > 0)' 		 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId or TZ.ZoneId in(select id from Zone where parentid= @ZoneId))' 
		
	IF @IsHot > -1
		SET @where = @where + ' AND (T.isTopToolbar = @IsHot)'

	IF @IsActive > -1
		SET @where = @where + ' AND (T.IsActive = @IsActive)'

	IF @ParentId > -1
		SET @where = @where + ' AND (T.ParentId = @ParentId)'
			
	SET @order = ' ORDER BY T.Id DESC'
	IF @OrderBy = 1
		SET @order = ' ORDER BY T.TopicName ASC'
	IF @OrderBy = 2
		SET @order = ' ORDER BY T.ParentId ASC, T.Priority ASC'
	IF @OrderBy = 3
		SET @order = ' ORDER BY T.Priority ASC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Topic AS T LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId AND TZ.IsPrimary=1' + @where + ';'
	SET @search = @search + 'SELECT [Id]
		,[ZoneId]
		,[Priority]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive]
	  ,[isTopToolbar]
      ,[ParentId]      
      ,[RelationTopic]
      ,ParentName
      ,DisplayName
      ,DisplayUrl
      ,NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
	  ,T.[isTopToolbar]
      ,T.[ParentId]      
      ,T.[RelationTopic]
      ,TP.[TopicName] as ParentName
      ,T.[DisplayName]
      ,T.DisplayUrl, '
	SET @search = @search + '		SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Topic AS T '
	SET @search = @search + '		LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId AND TZ.IsPrimary=1'
	SET @search = @search + '		LEFT JOIN NewsInTopic AS TN ON T.Id = TN.TopicId '
	SET @search = @search + '		LEFT JOIN Topic AS TP ON TP.Id = T.ParentId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
	  ,T.[isTopToolbar]
      ,T.[ParentId]
      ,T.[RelationTopic]
      ,TP.[TopicName]
      ,T.[DisplayName]
      ,T.DisplayUrl '
	SET @search = @search + ') AS TopicTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	--print(@search)
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@IsHot,
								@IsActive,
								@ParentId,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END