--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_UpdateTagHot]    Script Date: 4/4/2019 11:23:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-04>
-- Description:	<Update tag Invisibled>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Tag_UpdateTagInvisibled]
	@Id BIGINT,
	@Invisibled bit
AS
BEGIN
	UPDATE Tag
	SET Invisibled = @Invisibled
	WHERE (Id = @Id)
END














