--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Insert]    Script Date: 1/7/2019 10:14:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
ALTER PROCEDURE [dbo].[CMS_BoxTopicEmbed_Insert]	
	@Id int OUTPUT,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@DisplayStyle int =0
AS
BEGIN 
	INSERT INTO BoxTopicEmbed (
			TopicId,	
			Type,
			Label,
			Title,
			Avatar,
			Url,
			Description,
			SortOrder, 			
			LastModifiedDate,
			DisplayStyle
		)
		VALUES(
			@TopicId,	
			@Type,		
			@Label,
			@Title,
			@Avatar,
			@Url,
			@Description,
			@SortOrder,
			getdate(),
			@DisplayStyle
		)

	SET @Id = SCOPE_IDENTITY();
END

