--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_GetListByZone]    Script Date: 10/25/2018 11:26:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Search]
@TopicId int,
@Type int
AS
BEGIN
	SELECT * FROM BoxTopicEmbed
	where TopicId =@TopicId and Type=@Type
	ORDER BY SortOrder ASC
END