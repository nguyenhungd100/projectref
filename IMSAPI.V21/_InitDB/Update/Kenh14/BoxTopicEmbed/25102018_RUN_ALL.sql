--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxTopicEmbed]    Script Date: 10/25/2018 2:57:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxTopicEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopicId] [int] NULL,
	[Label] [nvarchar](250) NULL,
	[Title] [nvarchar](500) NULL,
	[Avatar] [varchar](1000) NULL,
	[Url] [varchar](1000) NULL,
	[Description] [nvarchar](500) NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxTopicEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_GetListByZone]    Script Date: 10/25/2018 11:26:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Search]
@TopicId int,
@Type int
AS
BEGIN
	SELECT * FROM BoxTopicEmbed
	where TopicId =@TopicId and Type=@Type
	ORDER BY SortOrder ASC
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Insert]    Script Date: 10/25/2018 11:47:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Insert]	
	@Id int OUTPUT,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0
AS
BEGIN 
	INSERT INTO BoxTopicEmbed (
			TopicId,	
			Type,
			Label,
			Title,
			Avatar,
			Url,
			Description,
			SortOrder, 			
			LastModifiedDate
		)
		VALUES(
			@TopicId,	
			@Type,		
			@Label,
			@Title,
			@Avatar,
			@Url,
			@Description,
			@SortOrder,
			getdate()
		)

	SET @Id = SCOPE_IDENTITY();
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Update]    Script Date: 10/25/2018 11:51:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Update] 
	@Id int,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0
AS
BEGIN
    UPDATE BoxTopicEmbed SET 
			TopicId=@TopicId,	
			Type=@Type,
			Label=@Label,
			Title=@Title,
			Avatar=@Avatar,
			Url=@Url,
			Description=@Description,
			SortOrder=@SortOrder,
			LastModifiedDate=getdate()
	WHERE Id=@Id
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Delete]    Script Date: 10/25/2018 2:39:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Delete]
@Id int
AS
DELETE BoxTopicEmbed where Id =@Id


