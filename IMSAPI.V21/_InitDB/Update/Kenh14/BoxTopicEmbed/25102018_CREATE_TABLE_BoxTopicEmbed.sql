--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxTopicEmbed]    Script Date: 10/25/2018 2:57:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxTopicEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TopicId] [int] NULL,
	[Label] [nvarchar](250) NULL,
	[Title] [nvarchar](500) NULL,
	[Avatar] [varchar](1000) NULL,
	[Url] [varchar](1000) NULL,
	[Description] [nvarchar](500) NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxTopicEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


