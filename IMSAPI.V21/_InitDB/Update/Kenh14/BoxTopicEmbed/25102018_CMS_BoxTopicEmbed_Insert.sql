--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Insert]    Script Date: 10/25/2018 11:47:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Insert]	
	@Id int OUTPUT,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0
AS
BEGIN 
	INSERT INTO BoxTopicEmbed (
			TopicId,	
			Type,
			Label,
			Title,
			Avatar,
			Url,
			Description,
			SortOrder, 			
			LastModifiedDate
		)
		VALUES(
			@TopicId,	
			@Type,		
			@Label,
			@Title,
			@Avatar,
			@Url,
			@Description,
			@SortOrder,
			getdate()
		)

	SET @Id = SCOPE_IDENTITY();
END
