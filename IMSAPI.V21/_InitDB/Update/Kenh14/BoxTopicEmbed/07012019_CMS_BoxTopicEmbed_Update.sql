--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Update]    Script Date: 1/7/2019 10:16:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
ALTER PROCEDURE [dbo].[CMS_BoxTopicEmbed_Update] 
	@Id int,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0,
	@DisplayStyle int=0
AS
BEGIN
    UPDATE BoxTopicEmbed SET 
			TopicId=@TopicId,	
			Type=@Type,
			Label=@Label,
			Title=@Title,
			Avatar=@Avatar,
			Url=@Url,
			Description=@Description,
			SortOrder=@SortOrder,
			LastModifiedDate=getdate(),
			DisplayStyle=@DisplayStyle
	WHERE Id=@Id
END

