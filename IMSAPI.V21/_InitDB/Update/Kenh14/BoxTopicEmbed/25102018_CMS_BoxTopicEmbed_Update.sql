--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicEmbed_Update]    Script Date: 10/25/2018 11:51:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--25/10/2018
CREATE PROCEDURE [dbo].[CMS_BoxTopicEmbed_Update] 
	@Id int,
	@TopicId int= 0,
	@Type int =0,
	@Label nvarchar(250),
	@Title nvarchar(500),
	@Avatar varchar(1000),
	@Url varchar(1000),
	@Description nvarchar(500),
	@SortOrder int=0
AS
BEGIN
    UPDATE BoxTopicEmbed SET 
			TopicId=@TopicId,	
			Type=@Type,
			Label=@Label,
			Title=@Title,
			Avatar=@Avatar,
			Url=@Url,
			Description=@Description,
			SortOrder=@SortOrder,
			LastModifiedDate=getdate()
	WHERE Id=@Id
END