USE [IMS_TECHCLOUD]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToRecievedForEdit]    Script Date: 03/07/2018 15:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-03-07>
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_ChangeStatusToCrawlerForEditt]
	@Id bigint,
	@CreatedBy varchar(200)
AS
BEGIN
	
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 1,
			CreatedBy = @CreatedBy,
			CreatedDate = GETDATE(),
			LastModifiedBy = @CreatedBy, 
			LastModifiedDate = GETDATE()
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


















