USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Sticker_AddNews]    Script Date: 03/12/2018 16:47:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_Sticker_AddNews]
    @NewsIds VARCHAR(MAX) = '' ,
    @StickerId BIGINT = 0
AS 
    BEGIN
        BEGIN TRANSACTION
	
        IF @NewsIds = '' 
            BEGIN
                RAISERROR ('Invalid NewsId list', 16, 1)
                ROLLBACK TRANSACTION
            END
        ELSE 
            IF NOT EXISTS ( SELECT  1
                            FROM    Sticker
                            WHERE   Id = @StickerId ) 
                BEGIN
					--IF	@StickerId=0
					DELETE FROM StickerInNews WHERE NewsId=@NewsIds
                    --RAISERROR ('This Sticker does not exist', 16, 1)
                    --ROLLBACK TRANSACTION
                END
            ELSE 
                BEGIN
                    DECLARE @StickerName NVARCHAR(200)

                    SELECT  @StickerName = StickerName
                    FROM    Sticker
                    WHERE   Id = @StickerId
		
                    IF @StickerName IS NOT NULL
                        AND @StickerName <> '' 
                        BEGIN
                            DELETE  StickerInNews
                            WHERE   StickerID = @StickerId
				
                            DECLARE @TblTemp TABLE
                                (
                                  Id INT IDENTITY(1, 1) ,
                                  NewsId BIGINT
                                )
                            INSERT  @TblTemp
                                    ( NewsId
                                    )
                                    SELECT  CONVERT(BIGINT, part)
                                    FROM    SplitString(@NewsIds, ';')
			
                            DECLARE @Index INT ,
                                @TotalRowInTemp INT ,
                                @NewsId BIGINT;
					
                            SET @Index = 0
                            SET @TotalRowInTemp = ISNULL(( SELECT
                                                              COUNT(Id)
                                                           FROM
                                                              @TblTemp
                                                         ), 0)
                            WHILE ( @Index <= @TotalRowInTemp ) 
                                BEGIN
                                    SET @NewsId = ISNULL(( SELECT
                                                              NewsId
                                                           FROM
                                                              @TblTemp
                                                           WHERE
                                                              Id = @Index
                                                         ), 0)
                                    IF @NewsId > 0 
                                        BEGIN
											DELETE FROM StickerInNews WHERE NewsId=@NewsId
                                            INSERT  INTO StickerInNews
                                                    ( NewsID, StickerID )
                                            VALUES  ( @NewsId, @StickerId )
                                        END
                                    SET @Index = @Index + 1
                                END
                        END
                END
	
        IF @@ERROR <> 0 
            BEGIN
                DECLARE @Message NVARCHAR(500)
                SET @Message = ERROR_MESSAGE();
                RAISERROR (@Message, 16, 1)
                ROLLBACK TRANSACTION
            END
        ELSE 
            BEGIN
                COMMIT TRANSACTION
            END
    END


