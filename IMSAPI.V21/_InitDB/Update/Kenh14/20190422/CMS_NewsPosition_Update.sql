--USE [IMS2_TTVH]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_Update]    Script Date: 4/22/2019 10:17:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:				<ThanhTN>
-- Edited:				<NANIA>
-- Create date:			<2012-10-08>
-- Last modified date:	<2018-06-13>
-- Last modified by:	<CHINHNB>
-- Description:			<Update news position>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_Update]
	@TypeId int,
	@ZoneId int = 0,
	@Order int,
	@NewsId bigint,
	@ExpiredLock datetime = NULL,
	@NewAvatar nvarchar(500) = '',
	@AvatarIndex int = 0,
	@CheckNewsExists bit = 1
AS
BEGIN
	BEGIN TRANSACTION
	IF @CheckNewsExists = 0 OR (@CheckNewsExists = 1 AND NOT EXISTS (SELECT 1 FROM NewsPosition WHERE NewsId = @NewsId AND TypeId = @TypeId AND ZoneId = @ZoneId))	
	BEGIN
		BEGIN TRY
			DECLARE @Id int,
					@IsAllowAutoUpdate bit
			
			DECLARE @IsFilterByZoneId tinyint
			SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), 0)
			
			SELECT @Id = Id, @IsAllowAutoUpdate = AllowAutoUpdate
			FROM NewsPosition
			WHERE TypeId = @TypeId AND (@IsFilterByZoneId <> 1 OR (@IsFilterByZoneId = 1 AND ZoneId = @ZoneId)) AND [Order] = @Order
			
			IF @Id > 0
			BEGIN
				DECLARE @Title nvarchar(250),
						@SubTitle nvarchar(250),
						@Sapo nvarchar(MAX),
						@Avatar nvarchar(250),
						@AvatarDesc nvarchar(500),
						@Avatar2 nvarchar(500),
						@Avatar3 nvarchar(500),
						@Avatar4 nvarchar(500),
						@Avatar5 nvarchar(500),
						@Author nvarchar(50),
						@NewsRelation nvarchar(max),
						@Source nvarchar(100),
						@IsFocus bit,
						@NewsType tinyint,
						@Type tinyint,
						@ThreadId int,
						@DistributionDate datetime,
						@Url nvarchar(500),						
						-- Extension fields
						@DisplayStyle int,
						@DisplayPosition int,
						@DisplayInSlide int,
						@AvatarCustom varchar(500),
						@OriginalId int,
						@InitSapo nvarchar(500),
						@OriginalUrl nvarchar(500),
						@IsPr bit,
						@AdStore bit,
						@AdStoreUrl nvarchar(500),
						@InterviewId int,
						@RollingNewsId int,
						@LocationType int,
						@ZoneIdForNews int
						
				SELECT @Title=Title, 
						@SubTitle=SubTitle, 
						@Sapo=Sapo, 
						@Avatar= Avatar, 
						@AvatarDesc=AvatarDesc, 
						@Avatar2=Avatar2, 
						@Avatar3=Avatar3, 
						@Avatar4=Avatar4, 
						@Avatar5=Avatar5, 
						@Author=Author, 
						@NewsRelation=NewsRelation, 
						@Source=Source, 
						@IsFocus=IsFocus, 
						@Type=[Type], 
						@NewsType=[NewsType], 
						@ThreadId=ThreadId, 
						@DistributionDate=DistributionDate, 
						@Url=Url,
						@DisplayStyle = DisplayStyle,
						@DisplayPosition = DisplayPosition,
						@DisplayInSlide = DisplayInSlide,
						@AvatarCustom = AvatarCustom,
						@OriginalId = OriginalId,
						@InitSapo = InitSapo,
						@OriginalUrl = OriginalUrl,
						@IsPr = IsPr,
						@AdStore = AdStore,
						@AdStoreUrl = AdStoreUrl,
						@InterviewId = InterviewId,
						@RollingNewsId = RollingNewsId,
						@LocationType = LocationType,
						@ZoneIdForNews = ZoneId
				FROM NewsPublish
				WHERE NewsId=@NewsId AND IsPrimary = 1

				DECLARE @ScheduleDate datetime
				SET @ScheduleDate = GETDATE()
				IF @ScheduleDate <= @DistributionDate
					SET @ScheduleDate = @DistributionDate
				
				UPDATE NewsPosition
				SET NewsIdForBomd = NewsId,
					NewsId=@NewsId,
					Title=@Title,
					SubTitle=@SubTitle,
					Sapo=@Sapo,
					Avatar=@Avatar,
					AvatarDesc=@AvatarDesc,
					Avatar2=@Avatar2,
					Avatar3=@Avatar3,
					Avatar4=@Avatar4,
					Avatar5=@Avatar5,
					Author=@Author,
					NewsRelation=@NewsRelation,
					Source=@Source,
					IsFocus=@IsFocus,
					NewsType=@NewsType,
					Type=@Type,
					ThreadId=@ThreadId,
					DistributionDate=@DistributionDate,
					Url=@Url,
					Locked=1,
					ExpiredLock = GETDATE(),					
					DisplayStyle = @DisplayStyle,
					DisplayPosition = @DisplayPosition,
					DisplayInSlide = @DisplayInSlide,
					AvatarCustom = @AvatarCustom,
					OriginalId = @OriginalId,
					InitSapo = @InitSapo,
					OriginalUrl = @OriginalUrl,
					IsPr = @IsPr,
					AdStore = @AdStore,
					AdStoreUrl = @AdStoreUrl,
					InterviewId = @InterviewId,
					RollingNewsId = @RollingNewsId,
					LocationType = @LocationType,
					ScheduleDate = @ScheduleDate,
					LastModifiedDate = GETDATE(),
					ZoneIdForNews = @ZoneIdForNews
				WHERE Id = @Id
				
				--chỉ mở cho afamily=>tự động đổi sugget nổi bật mục
				--UPDATE NewsPublish
				--SET DisplayPosition = 2
				--WHERE NewsId = @NewsId
				
				--UPDATE News
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
				
				--UPDATE NewsPosition
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
			END
			ELSE--khong co thi tu them vitri moi: chinhnb
			BEGIN
				DECLARE @Title1 nvarchar(250),
				@SubTitle1 nvarchar(250),
				@Sapo1 nvarchar(MAX),
				@Avatar1 nvarchar(250),
				@AvatarDesc1 nvarchar(500),
				@Avatar21 nvarchar(500),
				@Avatar31 nvarchar(500),
				@Avatar41 nvarchar(500),
				@Avatar51 nvarchar(500),
				@Author1 nvarchar(50),
				@NewsRelation1 nvarchar(max),
				@Source1 nvarchar(100),
				@IsFocus1 bit,
				@NewsType1 tinyint,
				@Type1 tinyint,
				@ThreadId1 int,
				@DistributionDate1 datetime,
				@Url1 nvarchar(500),						
				-- Extension fields
				@DisplayStyle1 int,
				@DisplayPosition1 int,
				@DisplayInSlide1 int,
				@AvatarCustom1 varchar(500),
				@OriginalId1 int,
				@InitSapo1 nvarchar(500),
				@OriginalUrl1 nvarchar(500),
				@IsPr1 bit,
				@AdStore1 bit,
				@AdStoreUrl1 nvarchar(500),
				@InterviewId1 int,
				@RollingNewsId1 int,
				@LocationType1 int,
				@ZoneIdForNews1 int,
				@Priority1 int,
				@IsOnHome1 int
				
				SELECT @Title1=Title, 
						@SubTitle1=SubTitle, 
						@Sapo1=Sapo, 
						@Avatar1= Avatar, 
						@AvatarDesc1=AvatarDesc, 
						@Avatar21=Avatar2, 
						@Avatar31=Avatar3, 
						@Avatar41=Avatar4, 
						@Avatar51=Avatar5, 
						@Author1=Author, 
						@NewsRelation1=NewsRelation, 
						@Source1=Source, 
						@IsFocus1=IsFocus, 
						@Type1=[Type], 
						@NewsType1=[NewsType], 
						@ThreadId1=ThreadId, 
						@DistributionDate1=DistributionDate, 
						@Url1=Url,
						@DisplayStyle1 = DisplayStyle,
						@DisplayPosition1 = DisplayPosition,
						@DisplayInSlide1 = DisplayInSlide,
						@AvatarCustom1 = AvatarCustom,
						@OriginalId1 = OriginalId,
						@InitSapo1 = InitSapo,
						@OriginalUrl1 = OriginalUrl,
						@IsPr1 = IsPr,
						@AdStore1 = AdStore,
						@AdStoreUrl1 = AdStoreUrl,
						@InterviewId1 = InterviewId,
						@RollingNewsId1 = RollingNewsId,
						@LocationType1 = LocationType,
						@ZoneIdForNews1 = ZoneId,
						@Priority1=Priority,
						@IsOnHome1=IsOnHome
				FROM NewsPublish
				WHERE NewsId=@NewsId AND IsPrimary = 1
				
				INSERT INTO NewsPosition(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
						Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
						IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
						DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
						InterviewId, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
						RollingNewsId, Type, Priority, 
						LocationType, IsOnHome,
						Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
						LastModifiedDate, LastModifiedDateStamp, ScheduleDate
				)
				Values(@ZoneId, @TypeId, @Order, @NewsId, @Title1, @SubTitle1, @Sapo1, @Avatar1, @AvatarDesc1, 
						@Avatar21, @Avatar31, @Avatar41, @Avatar51, @Author1, @NewsRelation1, @Source1, 
						@IsFocus1, @NewsType1, @ThreadId1, @DistributionDate1, @Url1, @DisplayStyle1, 
						@DisplayPosition1, @DisplayInSlide1, @AvatarCustom1, @OriginalId1, @InitSapo1, 
						@InterviewId1, @OriginalUrl1, @IsPr1, @AdStore1, @AdStoreUrl1, 
						@RollingNewsId1, @Type1, @Priority1, 
						@LocationType1, @IsOnHome1,
						1, 1, GETDATE(), 0, 0, @ZoneId, 
						GETDATE(), dbo.UNIX_TIMESTAMP(GETDATE()), @DistributionDate1
				)
				
				--chỉ mở cho afamily=>tự động đổi sugget nổi bật mục
				--UPDATE NewsPublish
				--SET DisplayPosition = 2
				--WHERE NewsId = @NewsId
				
				--UPDATE News
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
				
				--UPDATE NewsPosition
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId				
			END
				
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
	END
	ELSE
	BEGIN										
		RAISERROR ('#403: This news existed in Position', 16, 1);
		ROLLBACK TRANSACTION
	END
END