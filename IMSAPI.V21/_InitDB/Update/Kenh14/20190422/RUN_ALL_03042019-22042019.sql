﻿--03042019

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateInvisibled]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneInvisibled]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @Invisibled bit
			--DECLARE @CountId int
			SET @Invisibled = (SELECT Invisibled FROM Zone WHERE Id=@ZoneId)
			--SET @CountId=( SELECT COUNT(*) FROM Zone WHERE ParentId =@ZoneId)
			IF(@Invisibled = 1)
			UPDATE Zone SET Invisibled = 0 WHERE Id = @ZoneId
			---	if(@CountId >0) UPDATE Zone SET Invisibled = 0 WHERE ParentId = @ZoneId
			 	
			 	 
			ELSE IF (@Invisibled = 0)
			 UPDATE Zone SET Invisibled = 1 WHERE Id = @ZoneId
			-- if(@CountId >0)UPDATE Zone SET Invisibled = 0 WHERE ParentId = @ZoneId
			 
			
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateInvisibled]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneStatus]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @Status int
			
			SET @Status = (SELECT Status FROM Zone WHERE Id=@ZoneId)
			
			IF(@Status = 1)
			UPDATE Zone SET Status = 0 WHERE Id = @ZoneId
			 
			ELSE IF (@Status = 0)
			 UPDATE Zone SET Status = 1 WHERE Id = @ZoneId
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_UpdateAllowComment]    Script Date: 4/3/2019 5:10:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Zone_ToggleZoneAllowComment]
	@ZoneId int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Zone WHERE Id = @ZoneId)
		BEGIN
			RAISERROR ('This zone does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @AllowComment bit
			--DECLARE @CountId int
			SET @AllowComment = (SELECT ISNULL(AllowComment, 0) FROM Zone WHERE Id=@ZoneId)
			--SET @CountId=( SELECT COUNT(*) FROM Zone WHERE ParentId =@ZoneId)
			IF(@AllowComment = 1)
			UPDATE Zone SET AllowComment = 0 WHERE Id = @ZoneId
			---	if(@CountId >0) UPDATE Zone SET AllowComment = 0 WHERE ParentId = @ZoneId
			 	
			 	 
			ELSE IF (@AllowComment = 0)
			 UPDATE Zone SET AllowComment = 1 WHERE Id = @ZoneId
			-- if(@CountId >0)UPDATE Zone SET AllowComment = 0 WHERE ParentId = @ZoneId
			 
			
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicActive]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicActive]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @IsActive bit
						
			SET @IsActive = (SELECT IsActive FROM Topic WHERE Id=@Id)

			IF(@IsActive = 1)
			UPDATE Topic SET IsActive = 0 WHERE Id = @Id
			
			ELSE IF (@IsActive = 0)
			 UPDATE Topic SET IsActive = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicIconActive]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicIconActive]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @IsIconActive bit
						
			SET @IsIconActive = (SELECT IsIconActive FROM Topic WHERE Id=@Id)

			IF(@IsIconActive = 1)
			UPDATE Topic SET IsIconActive = 0 WHERE Id = @Id
			
			ELSE IF (@IsIconActive = 0)
			 UPDATE Topic SET IsIconActive = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_ToggleTopicIsToolbar]    Script Date: 4/3/2019 5:12:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb 03042019
CREATE PROCEDURE [dbo].[CMS_Topic_ToggleTopicIsToolbar]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM Topic WHERE Id = @Id)
		BEGIN
			RAISERROR ('This Topic does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @isTopToolbar bit
						
			SET @isTopToolbar = (SELECT isTopToolbar FROM Topic WHERE Id=@Id)

			IF(@isTopToolbar = 1)
			UPDATE Topic SET isTopToolbar = 0 WHERE Id = @Id
			
			ELSE IF (@isTopToolbar = 0)
			 UPDATE Topic SET isTopToolbar = 1 WHERE Id = @Id
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Search]    Script Date: 4/4/2019 5:41:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Search] --'',-1,-1,-1,0,-1,1,200,0
	@Keyword nvarchar(500),
	@IsHot int = -1,
	@IsActive int = -1,
	@ZoneId int,
	@OrderBy int,
	@ParentId int=-1,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@IsHot int,'
	SET @params = @params + '@IsActive int,'
	SET @params = @params + '@ParentId int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.TopicName) > 0)' 		 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId or TZ.ZoneId in(select id from Zone where parentid= @ZoneId))' 
		
	IF @IsHot > -1
		SET @where = @where + ' AND (T.isTopToolbar = @IsHot)'

	IF @IsActive > -1
		SET @where = @where + ' AND (T.IsActive = @IsActive)'

	IF @ParentId > -1
		SET @where = @where + ' AND (T.ParentId = @ParentId)'
			
	SET @order = ' ORDER BY T.Id DESC'
	IF @OrderBy = 1
		SET @order = ' ORDER BY T.TopicName ASC'
	IF @OrderBy = 2
		SET @order = ' ORDER BY T.ParentId ASC, T.Priority ASC'
	IF @OrderBy = 3
		SET @order = ' ORDER BY T.Priority ASC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Topic AS T LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId AND TZ.IsPrimary=1' + @where + ';'
	SET @search = @search + 'SELECT [Id]
		,[ZoneId]
		,[Priority]
      ,[TopicName]
      ,[Logo]
      ,[Cover]
      ,[IsActive]
      ,[IsIconActive]
	  ,[isTopToolbar]
      ,[ParentId]      
      ,[RelationTopic]
      ,ParentName
      ,DisplayName
      ,DisplayUrl
      ,NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
	  ,T.[isTopToolbar]
      ,T.[ParentId]      
      ,T.[RelationTopic]
      ,TP.[TopicName] as ParentName
      ,T.[DisplayName]
      ,T.DisplayUrl, '
	SET @search = @search + '		SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Topic AS T '
	SET @search = @search + '		LEFT JOIN TopicInZone AS TZ ON T.Id = TZ.TopicId AND TZ.IsPrimary=1'
	SET @search = @search + '		LEFT JOIN NewsInTopic AS TN ON T.Id = TN.TopicId '
	SET @search = @search + '		LEFT JOIN Topic AS TP ON TP.Id = T.ParentId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.[Id]
		,TZ.[ZoneId]
		,T.[Priority]
      ,T.[TopicName]
      ,T.[Logo]
      ,T.[Cover]
      ,T.[IsActive]
      ,T.[IsIconActive]
	  ,T.[isTopToolbar]
      ,T.[ParentId]
      ,T.[RelationTopic]
      ,TP.[TopicName]
      ,T.[DisplayName]
      ,T.DisplayUrl '
	SET @search = @search + ') AS TopicTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	--print(@search)
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@IsHot,
								@IsActive,
								@ParentId,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--07042019
--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxLivePageEmbed]    Script Date: 4/8/2019 2:24:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxLivePageEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageUrl] [nvarchar](1000) NULL,
	[PageName] [nvarchar](250) NULL,
	[Position] [int] NULL,
	[TemplateId] [int] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[Priority] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatdBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[DataJson] [nvarchar](max) NULL,
 CONSTRAINT [PK_BoxLivePageEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_BoxLivePageEmbed_TemplateId]  DEFAULT ((0)) FOR [TemplateId]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_BoxLivePageEmbed_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[BoxLivePageEmbed] ADD  CONSTRAINT [DF_Table_1_LastModifiedDate]  DEFAULT (getdate()) FOR [ModifiedDate]
GO


--

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Search]    Script Date: 4/8/2019 11:27:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by zone id and position>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Search] 
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@Position int,
	@Status int,
	@Type int,
	@TemplateId int
AS
BEGIN
	IF @PageName <> ''
		SET @PageName = '%' + @PageName + '%'

	SELECT Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	FROM BoxLivePageEmbed
	WHERE ((@PageName='' AND 1=1) OR (@PageName<>'' AND PATINDEX(@PageName, PageName) > 0)) 
		AND ((@PageUrl='' AND 1=1) OR (@PageUrl<>'' AND PageUrl=@PageUrl)) 
		AND (@Position < 0 OR (@Position >= 0 AND Position = @Position)) 
		AND (@Status < 0 OR (@Status >= 0 AND Status = @Status)) 
		AND (@Type < 0 OR (@Type >= 0 AND Type = @Type))
		AND (@TemplateId < 0 OR (@TemplateId >= 0 AND TemplateId = @TemplateId)) 				
	Group By Id, PageUrl, PageName, TemplateId,Status,Position, Type, Priority, CreatedDate, CreatdBy, ModifiedDate,ModifiedBy, DataJson
	ORDER BY Priority ASC
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]    Script Date: 4/8/2019 11:25:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_UpdateSortOrder]
	@ListId varchar(1000)
AS
BEGIN
	DECLARE @SortOrder int, 
			@BoxLivePageEmbedId int
			
	DECLARE SortOrderCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListId, ';')

	OPEN SortOrderCursor
	
	FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		UPDATE BoxLivePageEmbed
		SET Priority = @SortOrder
		WHERE Id = @BoxLivePageEmbedId
			
		FETCH NEXT FROM SortOrderCursor INTO @SortOrder, @BoxLivePageEmbedId 
	END
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Update]    Script Date: 4/8/2019 11:23:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Update box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Update]
	@Id int,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@ModifiedDate datetime,
	@ModifiedBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
	UPDATE BoxLivePageEmbed
	SET PageName = @PageName, 
		PageUrl = @PageUrl, 				
		TemplateId = @TemplateId,
		Position = @Position, 
		Status = @Status,
		Type = @Type,
		Priority = @Priority, 				
		ModifiedDate = @ModifiedDate,
		ModifiedBy = @ModifiedBy,
		DataJson=@DataJson
	WHERE (Id = @Id)				
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Insert]    Script Date: 4/8/2019 11:16:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Insert box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Insert]	
	@Id int OUTPUT,
	@PageName nvarchar(250),
	@PageUrl nvarchar(1000),
	@TemplateId int = 0,
	@Position int=0,
	@Status int=0,
	@Type int =0,
	@Priority int=0,
	@CreatedDate datetime,
	@CreatdBy varchar(100),
	@DataJson nvarchar(max) = ''
AS
BEGIN
BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO BoxLivePageEmbed (PageName,PageUrl,TemplateId,Position,Status, Type, Priority, CreatedDate, CreatdBy, DataJson)
		VALUES (@PageName,@PageUrl,@TemplateId,@Position,@Status,@Type,@Priority,@CreatedDate,@CreatdBy,@DataJson)		
		
		SET @Id = SCOPE_IDENTITY()
			
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_GetById]    Script Date: 4/8/2019 11:16:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <08/04/2019>
-- Description:	<Get box by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxLivePageEmbed_Delete]    Script Date: 4/8/2019 11:14:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <08/04/2019>
-- Description:	<Delete box>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxLivePageEmbed_Delete]
	@Id int
AS
BEGIN	
	DELETE FROM BoxLivePageEmbed
	WHERE (Id = @Id)
END


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_RemoveNewsPosition]    Script Date: 4/11/2019 10:59:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-115>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsPosition_RemoveNewsPosition] --1, 0, ''
	@TypeId int,
	@ZoneId int,
	@ConfigPosition int
AS
BEGIN
	DELETE NewsPosition where TypeId = @TypeId AND ZoneId = @ZoneId and Id not in
	(
	Select top(@ConfigPosition) Id from NewsPosition
	WHERE TypeId = @TypeId AND ZoneId = @ZoneId
	ORDER BY [Order] , DistributionDate DESC
	)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Thread_UpdateInvisibled]    Script Date: 4/10/2019 10:07:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-04-10>
-- Description:	<Update Thread Invisibled>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_Thread_UpdateInvisibled]
	@Id BIGINT,
	@Invisibled bit
AS
BEGIN
	UPDATE Thread 
	SET Invisibled = @Invisibled
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 4/10/2019 10:23:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
--edit 2019/04/10
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
BEGIN
	SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

    SELECT  *
    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                        E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
						,SUM(CASE WHEN EN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
                        ,ROW_NUMBER() OVER ( ORDER BY E.Id DESC ) AS RowNum
                FROM      Expert E
				LEFT JOIN ExpertInNews AS EN ON E.Id = EN.ExpertId
                WHERE     E.ExpertName LIKE '%' + @Keyword + '%'
				GROUP BY E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink
            ) baseTable
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                    AND     ( @PageIndex * @PageSize )
	
END


--16042019

GO

ALTER TABLE Expert
ADD Mobile VARCHAR(20)

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Insert]    Script Date: 4/16/2019 9:21:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Insert]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0 OUT
AS 
BEGIN
    INSERT  INTO [dbo].[Expert]
            ( [ExpertName] ,
              [JobTitle] ,
              [Description] ,
              [Avatar] ,
              Quote,
              [DisplayJobTitle],
              AcceptFeedback,
              FacebookLink,
			  Mobile
            )
    VALUES  ( @ExpertName ,
              @JobTitle ,
              @Description ,
              @Avatar ,
              @Quote,
              @DisplayJobTitle,
              @AcceptFeedback,
              @FacebookLink,
			  @Mobile
            )

    SET @Id = SCOPE_IDENTITY() ;
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Update]    Script Date: 4/16/2019 9:23:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Update]
    @ExpertName NVARCHAR(250) ,
    @JobTitle NVARCHAR(255) ,
    @Description NVARCHAR(MAX) ,
    @Avatar VARCHAR(250) ,
    @DisplayJobTitle BIT ,
    @Quote NVARCHAR(500),
    @AcceptFeedback bit = 1,
    @FacebookLink NVARCHAR(200) = '',
	@Mobile VARCHAR(20) = '',
    @Id INT = 0
AS 
BEGIN
    UPDATE  [dbo].[Expert]
    SET     [ExpertName] = @ExpertName ,
            [JobTitle] = @JobTitle ,
            [Description] = @Description ,
            [Avatar] = @Avatar ,
            Quote=@Quote,
            [DisplayJobTitle] = @DisplayJobTitle,
            AcceptFeedback = @AcceptFeedback,
            FacebookLink = @FacebookLink,
			Mobile=@Mobile
    WHERE   @id = Id
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search_Paging]    Script Date: 4/16/2019 9:27:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/04/2018
--edit 2019/04/10
ALTER PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS     
BEGIN
	SELECT @TotalRow = Count(1) from Expert WHERE ExpertName LIKE '%' + @Keyword + '%'

    SELECT  *
    FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                        E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink,E.Mobile
						,SUM(CASE WHEN EN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
                        ,ROW_NUMBER() OVER ( ORDER BY E.Id DESC ) AS RowNum
                FROM      Expert E
				LEFT JOIN ExpertInNews AS EN ON E.Id = EN.ExpertId
                WHERE     E.ExpertName LIKE '%' + @Keyword + '%'
				GROUP BY E.Id,E.ExpertName,E.JobTitle,E.Description,E.Avatar,E.DisplayJobTitle,E.Status,E.Quote,E.AcceptFeedback,E.FacebookLink,E.Mobile
            ) baseTable
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                    AND     ( @PageIndex * @PageSize )
	
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search]    Script Date: 4/16/2019 9:26:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_Search]
    @Keyword NVARCHAR(255) = ''
AS 
BEGIN
    DECLARE @KeyWordLike NVARCHAR(500) ;
    SET @KeyWordLike = '%' + @Keyword + '%' ;
    SELECT  *
    FROM    dbo.Expert
    WHERE   ExpertName LIKE @KeyWordLike
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_GetById]    Script Date: 4/16/2019 9:25:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Expert_GetById] @Id INT
AS 
BEGIN
    SELECT  *
    FROM    dbo.Expert
    WHERE   ( Id = @Id )
END


--22042019 => afamily ko chạy sp này

--USE [IMS2_TTVH]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_Update]    Script Date: 4/22/2019 10:17:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:				<ThanhTN>
-- Edited:				<NANIA>
-- Create date:			<2012-10-08>
-- Last modified date:	<2018-06-13>
-- Last modified by:	<CHINHNB>
-- Description:			<Update news position>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_Update]
	@TypeId int,
	@ZoneId int = 0,
	@Order int,
	@NewsId bigint,
	@ExpiredLock datetime = NULL,
	@NewAvatar nvarchar(500) = '',
	@AvatarIndex int = 0,
	@CheckNewsExists bit = 1
AS
BEGIN
	BEGIN TRANSACTION
	IF @CheckNewsExists = 0 OR (@CheckNewsExists = 1 AND NOT EXISTS (SELECT 1 FROM NewsPosition WHERE NewsId = @NewsId AND TypeId = @TypeId AND ZoneId = @ZoneId))	
	BEGIN
		BEGIN TRY
			DECLARE @Id int,
					@IsAllowAutoUpdate bit
			
			DECLARE @IsFilterByZoneId tinyint
			SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), 0)
			
			SELECT @Id = Id, @IsAllowAutoUpdate = AllowAutoUpdate
			FROM NewsPosition
			WHERE TypeId = @TypeId AND (@IsFilterByZoneId <> 1 OR (@IsFilterByZoneId = 1 AND ZoneId = @ZoneId)) AND [Order] = @Order
			
			IF @Id > 0
			BEGIN
				DECLARE @Title nvarchar(250),
						@SubTitle nvarchar(250),
						@Sapo nvarchar(MAX),
						@Avatar nvarchar(250),
						@AvatarDesc nvarchar(500),
						@Avatar2 nvarchar(500),
						@Avatar3 nvarchar(500),
						@Avatar4 nvarchar(500),
						@Avatar5 nvarchar(500),
						@Author nvarchar(50),
						@NewsRelation nvarchar(max),
						@Source nvarchar(100),
						@IsFocus bit,
						@NewsType tinyint,
						@Type tinyint,
						@ThreadId int,
						@DistributionDate datetime,
						@Url nvarchar(500),						
						-- Extension fields
						@DisplayStyle int,
						@DisplayPosition int,
						@DisplayInSlide int,
						@AvatarCustom varchar(500),
						@OriginalId int,
						@InitSapo nvarchar(500),
						@OriginalUrl nvarchar(500),
						@IsPr bit,
						@AdStore bit,
						@AdStoreUrl nvarchar(500),
						@InterviewId int,
						@RollingNewsId int,
						@LocationType int,
						@ZoneIdForNews int
						
				SELECT @Title=Title, 
						@SubTitle=SubTitle, 
						@Sapo=Sapo, 
						@Avatar= Avatar, 
						@AvatarDesc=AvatarDesc, 
						@Avatar2=Avatar2, 
						@Avatar3=Avatar3, 
						@Avatar4=Avatar4, 
						@Avatar5=Avatar5, 
						@Author=Author, 
						@NewsRelation=NewsRelation, 
						@Source=Source, 
						@IsFocus=IsFocus, 
						@Type=[Type], 
						@NewsType=[NewsType], 
						@ThreadId=ThreadId, 
						@DistributionDate=DistributionDate, 
						@Url=Url,
						@DisplayStyle = DisplayStyle,
						@DisplayPosition = DisplayPosition,
						@DisplayInSlide = DisplayInSlide,
						@AvatarCustom = AvatarCustom,
						@OriginalId = OriginalId,
						@InitSapo = InitSapo,
						@OriginalUrl = OriginalUrl,
						@IsPr = IsPr,
						@AdStore = AdStore,
						@AdStoreUrl = AdStoreUrl,
						@InterviewId = InterviewId,
						@RollingNewsId = RollingNewsId,
						@LocationType = LocationType,
						@ZoneIdForNews = ZoneId
				FROM NewsPublish
				WHERE NewsId=@NewsId AND IsPrimary = 1

				DECLARE @ScheduleDate datetime
				SET @ScheduleDate = GETDATE()
				IF @ScheduleDate <= @DistributionDate
					SET @ScheduleDate = @DistributionDate
				
				UPDATE NewsPosition
				SET NewsIdForBomd = NewsId,
					NewsId=@NewsId,
					Title=@Title,
					SubTitle=@SubTitle,
					Sapo=@Sapo,
					Avatar=@Avatar,
					AvatarDesc=@AvatarDesc,
					Avatar2=@Avatar2,
					Avatar3=@Avatar3,
					Avatar4=@Avatar4,
					Avatar5=@Avatar5,
					Author=@Author,
					NewsRelation=@NewsRelation,
					Source=@Source,
					IsFocus=@IsFocus,
					NewsType=@NewsType,
					Type=@Type,
					ThreadId=@ThreadId,
					DistributionDate=@DistributionDate,
					Url=@Url,
					Locked=1,
					ExpiredLock = GETDATE(),					
					DisplayStyle = @DisplayStyle,
					DisplayPosition = @DisplayPosition,
					DisplayInSlide = @DisplayInSlide,
					AvatarCustom = @AvatarCustom,
					OriginalId = @OriginalId,
					InitSapo = @InitSapo,
					OriginalUrl = @OriginalUrl,
					IsPr = @IsPr,
					AdStore = @AdStore,
					AdStoreUrl = @AdStoreUrl,
					InterviewId = @InterviewId,
					RollingNewsId = @RollingNewsId,
					LocationType = @LocationType,
					ScheduleDate = @ScheduleDate,
					LastModifiedDate = GETDATE(),
					ZoneIdForNews = @ZoneIdForNews
				WHERE Id = @Id
				
				--chỉ mở cho afamily=>tự động đổi sugget nổi bật mục
				--UPDATE NewsPublish
				--SET DisplayPosition = 2
				--WHERE NewsId = @NewsId
				
				--UPDATE News
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
				
				--UPDATE NewsPosition
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
			END
			ELSE--khong co thi tu them vitri moi: chinhnb
			BEGIN
				DECLARE @Title1 nvarchar(250),
				@SubTitle1 nvarchar(250),
				@Sapo1 nvarchar(MAX),
				@Avatar1 nvarchar(250),
				@AvatarDesc1 nvarchar(500),
				@Avatar21 nvarchar(500),
				@Avatar31 nvarchar(500),
				@Avatar41 nvarchar(500),
				@Avatar51 nvarchar(500),
				@Author1 nvarchar(50),
				@NewsRelation1 nvarchar(max),
				@Source1 nvarchar(100),
				@IsFocus1 bit,
				@NewsType1 tinyint,
				@Type1 tinyint,
				@ThreadId1 int,
				@DistributionDate1 datetime,
				@Url1 nvarchar(500),						
				-- Extension fields
				@DisplayStyle1 int,
				@DisplayPosition1 int,
				@DisplayInSlide1 int,
				@AvatarCustom1 varchar(500),
				@OriginalId1 int,
				@InitSapo1 nvarchar(500),
				@OriginalUrl1 nvarchar(500),
				@IsPr1 bit,
				@AdStore1 bit,
				@AdStoreUrl1 nvarchar(500),
				@InterviewId1 int,
				@RollingNewsId1 int,
				@LocationType1 int,
				@ZoneIdForNews1 int,
				@Priority1 int,
				@IsOnHome1 int
				
				SELECT @Title1=Title, 
						@SubTitle1=SubTitle, 
						@Sapo1=Sapo, 
						@Avatar1= Avatar, 
						@AvatarDesc1=AvatarDesc, 
						@Avatar21=Avatar2, 
						@Avatar31=Avatar3, 
						@Avatar41=Avatar4, 
						@Avatar51=Avatar5, 
						@Author1=Author, 
						@NewsRelation1=NewsRelation, 
						@Source1=Source, 
						@IsFocus1=IsFocus, 
						@Type1=[Type], 
						@NewsType1=[NewsType], 
						@ThreadId1=ThreadId, 
						@DistributionDate1=DistributionDate, 
						@Url1=Url,
						@DisplayStyle1 = DisplayStyle,
						@DisplayPosition1 = DisplayPosition,
						@DisplayInSlide1 = DisplayInSlide,
						@AvatarCustom1 = AvatarCustom,
						@OriginalId1 = OriginalId,
						@InitSapo1 = InitSapo,
						@OriginalUrl1 = OriginalUrl,
						@IsPr1 = IsPr,
						@AdStore1 = AdStore,
						@AdStoreUrl1 = AdStoreUrl,
						@InterviewId1 = InterviewId,
						@RollingNewsId1 = RollingNewsId,
						@LocationType1 = LocationType,
						@ZoneIdForNews1 = ZoneId,
						@Priority1=Priority,
						@IsOnHome1=IsOnHome
				FROM NewsPublish
				WHERE NewsId=@NewsId AND IsPrimary = 1
				
				INSERT INTO NewsPosition(ZoneId, TypeId, [Order], NewsId, Title, SubTitle, Sapo, Avatar, AvatarDesc, 
						Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, 
						IsFocus, NewsType, ThreadId, DistributionDate, Url, DisplayStyle, 
						DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, InitSapo, 
						InterviewId, OriginalUrl, IsPr, AdStore, AdStoreUrl, 
						RollingNewsId, Type, Priority, 
						LocationType, IsOnHome,
						Lockable, Locked, ExpiredLock, NewsIdForBomd, AllowAutoUpdate,ZoneIdForNews,
						LastModifiedDate, LastModifiedDateStamp, ScheduleDate
				)
				Values(@ZoneId, @TypeId, @Order, @NewsId, @Title1, @SubTitle1, @Sapo1, @Avatar1, @AvatarDesc1, 
						@Avatar21, @Avatar31, @Avatar41, @Avatar51, @Author1, @NewsRelation1, @Source1, 
						@IsFocus1, @NewsType1, @ThreadId1, @DistributionDate1, @Url1, @DisplayStyle1, 
						@DisplayPosition1, @DisplayInSlide1, @AvatarCustom1, @OriginalId1, @InitSapo1, 
						@InterviewId1, @OriginalUrl1, @IsPr1, @AdStore1, @AdStoreUrl1, 
						@RollingNewsId1, @Type1, @Priority1, 
						@LocationType1, @IsOnHome1,
						1, 1, GETDATE(), 0, 0, @ZoneId, 
						GETDATE(), dbo.UNIX_TIMESTAMP(GETDATE()), @DistributionDate1
				)
				
				--chỉ mở cho afamily=>tự động đổi sugget nổi bật mục
				--UPDATE NewsPublish
				--SET DisplayPosition = 2
				--WHERE NewsId = @NewsId
				
				--UPDATE News
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId
				
				--UPDATE NewsPosition
				--SET DisplayPosition = 2
				--WHERE Id = @NewsId				
			END
				
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
	END
	ELSE
	BEGIN										
		RAISERROR ('#403: This news existed in Position', 16, 1);
		ROLLBACK TRANSACTION
	END
END