USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Sticker_Search]    Script Date: 03/12/2018 09:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_Sticker_Search]
    @Keyword NVARCHAR(500) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS 
    BEGIN
		SELECT @TotalRow = COUNT(*) FROM Sticker n where StickerName LIKE '%' + @Keyword + '%'
		
        SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                  FROM      Sticker
                  WHERE     StickerName LIKE '%' + @Keyword + '%'
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
	
    END


