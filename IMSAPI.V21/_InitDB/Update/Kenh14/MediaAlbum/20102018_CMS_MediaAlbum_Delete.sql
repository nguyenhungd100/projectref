--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Delete]    Script Date: 10/31/2018 11:48:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Delete]
	@Id int
AS
BEGIN
	DELETE FROM MediaAlbum WHERE (Id = @Id)

	DELETE FROM MediaAlbumDetail WHERE (AlbumId = @Id)
END




