
alter table MediaAlbum
add StartDate datetime,ExpireDate datetime,IsOnHome bit

GO


alter table MediaAlbumDetail
add Priority int


--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Search]    Script Date: 10/31/2018 10:11:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Search]
	@Keyword nvarchar(200),
	@ZoneId int,
	@Status int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SELECT @TotalRow = COUNT(*) 
	FROM MediaAlbum
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
		AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (ZoneId = @ZoneId)))
		AND (@Status < 0 OR (@Status >= 0 AND (Status = @Status)))
	
	IF @PageIndex <= 0
		SELECT TOP(@PageSize) MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome, Sum(1) as CountItem
		FROM MediaAlbum as MA
		LEFT JOIN MediaAlbumDetail as MAD ON MAD.AlbumId=MA.Id
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, MA.Name) > 0))
			AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (MA.ZoneId = @ZoneId)))
			AND (@Status < 0 OR (@Status >= 0 AND (MA.Status = @Status)))		
		GROUP BY MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome
		ORDER BY MA.CreatedDate DESC
	ELSE
		SELECT *
		FROM (
			SELECT MA.Id,  MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
				MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome, Sum(1) as CountItem,
				ROW_NUMBER() OVER(ORDER BY MA.CreatedDate DESC) AS RowNum
			FROM MediaAlbum as MA
			LEFT JOIN MediaAlbumDetail as MAD ON MAD.AlbumId=MA.Id
			WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, MA.Name) > 0))
				AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (MA.ZoneId = @ZoneId)))
				AND (@Status < 0 OR (@Status >= 0 AND (MA.Status = @Status)))
			GROUP BY MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)		
		ORDER BY CreatedDate DESC
END


--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Insert]    Script Date: 10/30/2018 5:51:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Insert]
	@Id int out,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@CreatedBy nvarchar(100),
	@Status int,
	@IsOnHome bit
AS
BEGIN
	INSERT INTO MediaAlbum (ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, IsOnHome)
	VALUES (@ZoneId,@Name,@Avatar,@CreatedBy, GETDATE(),
		@CreatedBy, GETDATE(),@Status, @IsOnHome)
	
	SET @Id = SCOPE_IDENTITY()
END



--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Update]    Script Date: 10/30/2018 5:52:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Update]
	@Id int,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@LastModifiedBy nvarchar(100),
	@Status int,
	@IsOnHome bit
AS
BEGIN
	UPDATE MediaAlbum
	SET ZoneId = @ZoneId, 
		Name = @Name, 
		Avatar = @Avatar, 
		LastModifiedBy = @LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		Status = @Status,
		IsOnHome=@IsOnHome
	WHERE (Id = @Id)
END



--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_GetById]    Script Date: 10/31/2018 10:11:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_GetById]
	@Id int
AS
BEGIN
	SELECT Id, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, IsOnHome
	FROM MediaAlbum
	WHERE Id = @Id
END


--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Delete]    Script Date: 10/31/2018 11:48:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Delete]
	@Id int
AS
BEGIN
	DELETE FROM MediaAlbum WHERE (Id = @Id)

	DELETE FROM MediaAlbumDetail WHERE (AlbumId = @Id)
END



--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Delete]    Script Date: 10/31/2018 9:51:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Delete]
	@Id bigint
AS
BEGIN
	DELETE FROM MediaAlbumDetail
	WHERE (Id = @Id)
END


--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_GetByAlbumId]    Script Date: 10/31/2018 11:25:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_GetByAlbumId]
	@Id int
AS
BEGIN
	SELECT [Id]
      ,[AlbumId]
      ,[Title]
      ,[UnsignName]
      ,[Description]
      ,[MediaType]
      ,[MediaContent]
      ,[Author]
      ,[Tags]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBy]
      ,[DistributionDate]
      ,[DistributionBy]
      ,[ViewCount]
      ,[VoteCount]
      ,[CommentCount]
      ,[IsHot]
      ,[NewsId]
      ,[Status]
      ,[ZoneId]
      ,[Avatar]
      ,[DistributionDateInNumeric]
      ,[Url]
	  ,[Priority]
  FROM [dbo].[MediaAlbumDetail]
	WHERE albumId = @Id
	ORDER BY [Priority] ASC
END




--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_GetById]    Script Date: 10/31/2018 11:25:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_GetById]	
	@id INT
AS
BEGIN
SELECT  Id ,
        AlbumId ,
        Title ,
        UnsignName ,
        Description ,
        MediaType ,
        MediaContent ,
        Author ,
        Tags ,
        CreatedDate ,
        CreatedBy ,
        ModifiedDate ,
        ModifiedBy ,
        DistributionDate ,
        DistributionBy ,
        ViewCount ,
        VoteCount ,
        CommentCount ,
        IsHot ,
        NewsId ,
        Status ,
        ZoneId ,
        Avatar ,
        DistributionDateInNumeric,	
        Url,
		Priority 
		FROM dbo.MediaAlbumDetail WHERE Id = @id
END




--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Insert]    Script Date: 10/31/2018 11:23:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Insert]
	@Id bigint output,	
	@ZoneId int,
	@MediaType int,
	@Title nvarchar(1000),
	@MediaContent nvarchar(MAX),	
	@Description nvarchar(MAX),	
	@Avatar varchar(300),	
	@Status int,
	@IsHot bit,
	@CreatedBy nvarchar(100),
	@Author nvarchar(100),
	@DistributionDate datetime,
	@UnSignName nvarchar(500),
	@Tags nvarchar(2000),
	@NewsId bigint,
	@Url varchar(500) = '',
	-- other table
	@TagIdList varchar(500) = '',
	@AlbumId INT,
	@Priority int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO MediaAlbumDetail (			
			Title,
			UnsignName,
			[Description],
			Avatar,
			MediaType,
			MediaContent,
			Author,
			Tags,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy,
			DistributionDateInNumeric,
			DistributionDate,
			DistributionBy,
			ViewCount,
			VoteCount,
			CommentCount,
			[Status],
			IsHot,
			NewsId,
			Url,
			ZoneId,
			AlbumId,
			Priority
			)
		VALUES (
			@Title, 
			@UnsignName,			
			@Description,
			@Avatar,
			@MediaType, 
			@MediaContent,
			@Author,
			@Tags,
			GETDATE(),
			@CreatedBy,
			GETDATE(),
			@CreatedBy,
			[dbo].[CMS_fConvertDateTimeToBigInt](@DistributionDate),
			@DistributionDate,
			@CreatedBy,
			0,
			0,
			0,
			@Status,
			@IsHot,
			@NewsId,
			@Url,
			@ZoneId,
			@AlbumId,
			@Priority
			)
			
		SET @Id = SCOPE_IDENTITY()
				
		DECLARE @Index int;
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM MediaAlbumDetailTags WHERE TagID = @TagItemId AND MediaAlbumDetailId = @Id)
						INSERT INTO MediaAlbumDetailTags(MediaAlbumDetailId, TagID, Priority) 
						VALUES (@Id, @TagItemId, @Index);
				SET @Index = @Index + 1
			END
		END	
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_MediaAlbumDetail_Insert]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END




--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Update]    Script Date: 10/31/2018 11:24:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Update]
	@Id bigint output,	
	@ZoneId int,
	@MediaType int,
	@Title nvarchar(1000),
	@MediaContent nvarchar(MAX),	
	@Description nvarchar(MAX),	
	@Avatar varchar(300),
	@Status int,
	@IsHot bit,
	@ModifiedBy nvarchar(100),
	@Author nvarchar(100),
	@DistributionDate datetime,
	@UnSignName nvarchar(500),
	@Tags nvarchar(2000),
	@NewsId bigint,	
	@Url varchar(500) = '',
	-- other table
	@TagIdList varchar(500) = '',
	@AlbumId INT,
	@Priority int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE MediaAlbumDetail SET
		
			Title = @Title,
			UnsignName = @UnsignName,
			[Description] = @Description,
			Avatar = @Avatar,
			MediaType = @MediaType,
			MediaContent = @MediaContent,
			Author = @Author,
			Tags = @Tags,			
			ModifiedDate = GETDATE(),
			ModifiedBy = @ModifiedBy,
			DistributionDateInNumeric = [dbo].[CMS_fConvertDateTimeToBigInt](@DistributionDate),
			DistributionDate = @DistributionDate,
			DistributionBy = @ModifiedBy,		
			[Status] = @Status,
			IsHot = @IsHot,
			NewsId = @NewsId,
			Url = @Url,
			ZoneId = @ZoneId,
			AlbumId = @AlbumId,
			Priority=@Priority
		WHERE Id = @Id		
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_MediaAlbumDetail_Update]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END






