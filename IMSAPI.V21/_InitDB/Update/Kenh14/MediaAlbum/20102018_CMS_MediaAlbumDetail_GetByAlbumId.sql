--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_GetByAlbumId]    Script Date: 10/31/2018 11:25:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_GetByAlbumId]
	@Id int
AS
BEGIN
	SELECT [Id]
      ,[AlbumId]
      ,[Title]
      ,[UnsignName]
      ,[Description]
      ,[MediaType]
      ,[MediaContent]
      ,[Author]
      ,[Tags]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBy]
      ,[DistributionDate]
      ,[DistributionBy]
      ,[ViewCount]
      ,[VoteCount]
      ,[CommentCount]
      ,[IsHot]
      ,[NewsId]
      ,[Status]
      ,[ZoneId]
      ,[Avatar]
      ,[DistributionDateInNumeric]
      ,[Url]
	  ,[Priority]
  FROM [dbo].[MediaAlbumDetail]
	WHERE albumId = @Id
	ORDER BY [Priority] ASC
END




