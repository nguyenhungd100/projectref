--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Update]    Script Date: 10/31/2018 11:24:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Update]
	@Id bigint output,	
	@ZoneId int,
	@MediaType int,
	@Title nvarchar(1000),
	@MediaContent nvarchar(MAX),	
	@Description nvarchar(MAX),	
	@Avatar varchar(300),
	@Status int,
	@IsHot bit,
	@ModifiedBy nvarchar(100),
	@Author nvarchar(100),
	@DistributionDate datetime,
	@UnSignName nvarchar(500),
	@Tags nvarchar(2000),
	@NewsId bigint,	
	@Url varchar(500) = '',
	-- other table
	@TagIdList varchar(500) = '',
	@AlbumId INT,
	@Priority int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE MediaAlbumDetail SET
		
			Title = @Title,
			UnsignName = @UnsignName,
			[Description] = @Description,
			Avatar = @Avatar,
			MediaType = @MediaType,
			MediaContent = @MediaContent,
			Author = @Author,
			Tags = @Tags,			
			ModifiedDate = GETDATE(),
			ModifiedBy = @ModifiedBy,
			DistributionDateInNumeric = [dbo].[CMS_fConvertDateTimeToBigInt](@DistributionDate),
			DistributionDate = @DistributionDate,
			DistributionBy = @ModifiedBy,		
			[Status] = @Status,
			IsHot = @IsHot,
			NewsId = @NewsId,
			Url = @Url,
			ZoneId = @ZoneId,
			AlbumId = @AlbumId,
			Priority=@Priority
		WHERE Id = @Id		
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_MediaAlbumDetail_Update]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END




