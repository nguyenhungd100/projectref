--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Insert]    Script Date: 10/31/2018 11:23:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Insert]
	@Id bigint output,	
	@ZoneId int,
	@MediaType int,
	@Title nvarchar(1000),
	@MediaContent nvarchar(MAX),	
	@Description nvarchar(MAX),	
	@Avatar varchar(300),	
	@Status int,
	@IsHot bit,
	@CreatedBy nvarchar(100),
	@Author nvarchar(100),
	@DistributionDate datetime,
	@UnSignName nvarchar(500),
	@Tags nvarchar(2000),
	@NewsId bigint,
	@Url varchar(500) = '',
	-- other table
	@TagIdList varchar(500) = '',
	@AlbumId INT,
	@Priority int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO MediaAlbumDetail (			
			Title,
			UnsignName,
			[Description],
			Avatar,
			MediaType,
			MediaContent,
			Author,
			Tags,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy,
			DistributionDateInNumeric,
			DistributionDate,
			DistributionBy,
			ViewCount,
			VoteCount,
			CommentCount,
			[Status],
			IsHot,
			NewsId,
			Url,
			ZoneId,
			AlbumId,
			Priority
			)
		VALUES (
			@Title, 
			@UnsignName,			
			@Description,
			@Avatar,
			@MediaType, 
			@MediaContent,
			@Author,
			@Tags,
			GETDATE(),
			@CreatedBy,
			GETDATE(),
			@CreatedBy,
			[dbo].[CMS_fConvertDateTimeToBigInt](@DistributionDate),
			@DistributionDate,
			@CreatedBy,
			0,
			0,
			0,
			@Status,
			@IsHot,
			@NewsId,
			@Url,
			@ZoneId,
			@AlbumId,
			@Priority
			)
			
		SET @Id = SCOPE_IDENTITY()
				
		DECLARE @Index int;
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM MediaAlbumDetailTags WHERE TagID = @TagItemId AND MediaAlbumDetailId = @Id)
						INSERT INTO MediaAlbumDetailTags(MediaAlbumDetailId, TagID, Priority) 
						VALUES (@Id, @TagItemId, @Index);
				SET @Index = @Index + 1
			END
		END	
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_MediaAlbumDetail_Insert]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END




