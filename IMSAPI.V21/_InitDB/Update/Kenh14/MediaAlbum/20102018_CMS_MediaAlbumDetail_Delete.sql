--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbumDetail_Delete]    Script Date: 10/31/2018 9:51:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbumDetail_Delete]
	@Id bigint
AS
BEGIN
	DELETE FROM MediaAlbumDetail
	WHERE (Id = @Id)
END




