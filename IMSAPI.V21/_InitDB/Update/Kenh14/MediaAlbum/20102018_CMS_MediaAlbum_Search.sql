--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Search]    Script Date: 10/31/2018 10:11:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Search]
	@Keyword nvarchar(200),
	@ZoneId int,
	@Status int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int out
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SELECT @TotalRow = COUNT(*) 
	FROM MediaAlbum
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Name) > 0))
		AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (ZoneId = @ZoneId)))
		AND (@Status < 0 OR (@Status >= 0 AND (Status = @Status)))
	
	IF @PageIndex <= 0
		SELECT TOP(@PageSize) MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome, Sum(1) as CountItem
		FROM MediaAlbum as MA
		LEFT JOIN MediaAlbumDetail as MAD ON MAD.AlbumId=MA.Id
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, MA.Name) > 0))
			AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (MA.ZoneId = @ZoneId)))
			AND (@Status < 0 OR (@Status >= 0 AND (MA.Status = @Status)))		
		GROUP BY MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome
		ORDER BY MA.CreatedDate DESC
	ELSE
		SELECT *
		FROM (
			SELECT MA.Id,  MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
				MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome, Sum(1) as CountItem,
				ROW_NUMBER() OVER(ORDER BY MA.CreatedDate DESC) AS RowNum
			FROM MediaAlbum as MA
			LEFT JOIN MediaAlbumDetail as MAD ON MAD.AlbumId=MA.Id
			WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, MA.Name) > 0))
				AND (@ZoneId <= 0 OR (@ZoneId > 0 AND (MA.ZoneId = @ZoneId)))
				AND (@Status < 0 OR (@Status >= 0 AND (MA.Status = @Status)))
			GROUP BY MA.Id, MA.ZoneId, MA.Name, MA.Avatar, MA.CreatedBy, MA.CreatedDate, 
			MA.LastModifiedBy, MA.LastModifiedDate, MA.Status, MA.IsOnHome
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)		
		ORDER BY CreatedDate DESC
END




