--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Insert]    Script Date: 10/30/2018 5:51:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Insert]
	@Id int out,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@CreatedBy nvarchar(100),
	@Status int,
	@IsOnHome bit
AS
BEGIN
	INSERT INTO MediaAlbum (ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, IsOnHome)
	VALUES (@ZoneId,@Name,@Avatar,@CreatedBy, GETDATE(),
		@CreatedBy, GETDATE(),@Status, @IsOnHome)
	
	SET @Id = SCOPE_IDENTITY()
END




