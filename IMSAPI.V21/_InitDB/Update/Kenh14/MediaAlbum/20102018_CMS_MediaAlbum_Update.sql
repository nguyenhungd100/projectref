--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_Update]    Script Date: 10/30/2018 5:52:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_Update]
	@Id int,
	@ZoneId int,
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@LastModifiedBy nvarchar(100),
	@Status int,
	@IsOnHome bit
AS
BEGIN
	UPDATE MediaAlbum
	SET ZoneId = @ZoneId, 
		Name = @Name, 
		Avatar = @Avatar, 
		LastModifiedBy = @LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		Status = @Status,
		IsOnHome=@IsOnHome
	WHERE (Id = @Id)
END




