--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MediaAlbum_GetById]    Script Date: 10/31/2018 10:11:02 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_MediaAlbum_GetById]
	@Id int
AS
BEGIN
	SELECT Id, ZoneId, Name, Avatar, CreatedBy, CreatedDate, 
		LastModifiedBy, LastModifiedDate, Status, IsOnHome
	FROM MediaAlbum
	WHERE Id = @Id
END




