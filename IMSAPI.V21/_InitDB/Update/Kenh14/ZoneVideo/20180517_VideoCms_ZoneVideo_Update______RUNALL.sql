--USE [IMS2_AFAMILY_BETA]
GO

--ALTER TABLE ZoneVideo
--Add Mode int,[Description] nvarchar(max)

GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Insert]
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@CreatedDate datetime,
	@Status int,
	@DisplayStyle int = 0,
	@ShowOnHome bit = 1,
	@ListVideoTagId varchar(1000) = '',
	@Invisibled bit = 1,
	@Id int output,
	@Avatar varchar(300) = '',
	@AvatarCover varchar(300) = '',
	@ZoneRelation varchar(100) = '',
	@Logo varchar(300) = '',
	@MetaAvatar varchar(max) = '',
	@CatId int,
	@Mode int,
	@Description nvarchar(max)
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZoneVideo (Name, Url, [Order], ParentId, [Status], CreatedDate, ModifiedDate,DisplayStyle,ShowOnHome,Invisibled,Avatar,AvatarCover,ZoneRelation,Logo,MetaAvatar,CatId,Mode,[Description])
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@CreatedDate,@CreatedDate,@DisplayStyle,@ShowOnHome,@Invisibled,@Avatar,@AvatarCover,@ZoneRelation,@Logo,@MetaAvatar,@CatId,@Mode,@Description)
	
	SET @Id = SCOPE_IDENTITY()
	
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@ModifiedDate datetime,
	@Status int,
	@ListVideoTagId varchar(1000) = '',
	@DisplayStyle int=0,
	@ShowOnHome bit = 1,
	@Invisibled bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@ZoneRelation varchar(100),
	@Logo varchar(300),
	@MetaAvatar varchar(max),
	@CatId int,	
	@Mode int,
	@Description nvarchar(max)
AS
	BEGIN TRANSACTION
	
	UPDATE ZoneVideo
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		[Status] = @Status, 
		ModifiedDate = @ModifiedDate,
		DisplayStyle =@DisplayStyle,
		ShowOnHome =@ShowOnHome,
		Invisibled = @Invisibled,
		AvatarCover = @AvatarCover,
		Avatar = @Avatar,
		ZoneRelation=@ZoneRelation,
		Logo=@Logo,
		MetaAvatar=@MetaAvatar,
		CatId=@CatId,
		Mode=@Mode,
		[Description]=@Description
	WHERE (Id = @Id)

	DELETE FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
-----------------------------------CMS_Zone_GetByParentId--------------------

GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM ZoneVideo
	WHERE Id = @Id
	
-----------------------
GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListByParentId]
	@ParentId int = 0,
	@Status int 
AS
	SELECT *, (SELECT COUNT(*) FROM ZoneVideo WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZoneVideo AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status <= 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]