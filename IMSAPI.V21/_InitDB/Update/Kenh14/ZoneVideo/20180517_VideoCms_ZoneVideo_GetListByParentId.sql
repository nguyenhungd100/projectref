USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_GetListByParentId]    Script Date: 05/18/2018 15:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListByParentId]
	@ParentId int = 0,
	@Status int 
AS
	SELECT *, (SELECT COUNT(*) FROM ZoneVideo WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZoneVideo AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status <= 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]