--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_Update]    Script Date: 9/15/2018 7:28:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_Update]
    @Id INT OUT ,
    @NewsId bigint ,
    @SourceUrl VARCHAR(300) ,
    @Status INT
AS 
    IF NOT EXISTS ( SELECT  1
                    FROM    [VideoAutoDownloadMonitor]
                    WHERE   NewsId = @NewsId ) 
        BEGIN
            INSERT  INTO [dbo].[VideoAutoDownloadMonitor]
                    ( [NewsId] ,
                      [SourceUrl] ,
                      [Status] ,
                      [CreatedDate] 
                    )
            VALUES  ( @NewsId ,
                      @SourceUrl ,
                      @Status ,
                      GETDATE()
                    )
            SELECT  @Id = @@IDENTITY
        END
    
    ELSE 
        BEGIN
            UPDATE  [dbo].[VideoAutoDownloadMonitor]
            SET     [SourceUrl] = @SourceUrl ,
                    [Status] = @Status
            WHERE   NewsId = @NewsId
            SELECT  @Id = Id
            FROM    [VideoAutoDownloadMonitor]
            WHERE   NewsId = @NewsId
        END



