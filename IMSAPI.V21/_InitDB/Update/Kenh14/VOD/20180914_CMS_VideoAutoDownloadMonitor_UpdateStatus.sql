--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]    Script Date: 9/15/2018 8:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]
	@Ids nvarchar(1000),
	@Status INT
AS
BEGIN
	IF(@Status = 3)
		UPDATE VideoAutoDownloadMonitor SET Status = @Status, UpdateDatabase = GETDATE()
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(INT,part))
	ELSE
		UPDATE VideoAutoDownloadMonitor SET Status = @Status
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(INT,part))
END
