--USE [IMS_VTV]
GO

/****** Object:  Table [dbo].[VideoAutoDownloadMonitor]    Script Date: 9/15/2018 6:24:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoAutoDownloadMonitor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[SourceUrl] [varchar](300) NULL,
	[SaveFilePath] [varchar](300) NULL,
	[KeyVideo] [varchar](100) NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[SentDownloadRequest] [datetime] NULL,
	[DownloadCompleted] [datetime] NULL,
	[UpdateDatabase] [datetime] NULL,
	[CounterCheckDownLoad] [int] NULL,
 CONSTRAINT [PK_VideoAutoDownloadMonitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]    Script Date: 9/15/2018 8:30:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]
	@Ids nvarchar(1000),
	@Status INT
AS
BEGIN
	IF(@Status = 3)
		UPDATE VideoAutoDownloadMonitor SET Status = @Status, UpdateDatabase = GETDATE()
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(INT,part))
	ELSE
		UPDATE VideoAutoDownloadMonitor SET Status = @Status
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(INT,part))
END


--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_Update]    Script Date: 9/15/2018 7:28:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_Update]
    @Id INT OUT ,
    @NewsId bigint ,
    @SourceUrl VARCHAR(300) ,
    @Status INT
AS 
    IF NOT EXISTS ( SELECT  1
                    FROM    [VideoAutoDownloadMonitor]
                    WHERE   NewsId = @NewsId ) 
        BEGIN
            INSERT  INTO [dbo].[VideoAutoDownloadMonitor]
                    ( [NewsId] ,
                      [SourceUrl] ,
                      [Status] ,
                      [CreatedDate] 
                    )
            VALUES  ( @NewsId ,
                      @SourceUrl ,
                      @Status ,
                      GETDATE()
                    )
            SELECT  @Id = @@IDENTITY
        END
    
    ELSE 
        BEGIN
            UPDATE  [dbo].[VideoAutoDownloadMonitor]
            SET     [SourceUrl] = @SourceUrl ,
                    [Status] = @Status
            WHERE   NewsId = @NewsId
            SELECT  @Id = Id
            FROM    [VideoAutoDownloadMonitor]
            WHERE   NewsId = @NewsId
        END



--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_GetByInterview]    Script Date: 9/15/2018 7:25:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_GetByNewsId] @NewsId bigint
AS 
    SELECT  *
    FROM    VideoAutoDownloadMonitor
    WHERE   NewsId = @NewsId


--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]    Script Date: 9/15/2018 8:02:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]
	@NewsId int,
	@VideoInfo nvarchar(1000),
	@SaveFilePath nvarchar(500),
	@KeyVideo nvarchar(500)
AS
BEGIN
	 UPDATE VideoAutoDownloadMonitor SET Status = 2,DownloadCompleted = GETDATE(), KeyVideo = @KeyVideo, SaveFilePath = @SaveFilePath
	 WHERE NewsId = @NewsId;
	 
	 --UPDATE Interview SET VideoInfo = @VideoInfo WHERE Id = @InterviewId;
END
