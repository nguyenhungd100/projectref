USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBanner_GetListByZoneIdAndPosition]    Script Date: 03/20/2018 15:07:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-03-20>
-- Edited 2018-03-20 - chinhnb
-- Description:	<>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_BoxBanner_GetListZoneIdById] 
	@Id int
AS
BEGIN
	SELECT * from BoxBannerInZone WHERE BoxBannerId=@Id
END













