USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsHotCafeF_GetAllForRedis]    Script Date: 04/07/2018 12:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--07/04/2018
CREATE PROCEDURE [dbo].[CMS_NewsHotCafeF_GetAllForRedis]
AS 
    SELECT  nth.Id ,
            nth.NewsId ,
            nth.Title ,
            nth.CategoryName ,
            nth.Rank ,
            nth.PredictDesc ,
            nth.PictureUrl ,
            nth.CategoryHref ,
            nth.PredictColor ,
            nth.Sapo ,
            nth.Url ,
            nth.PublishDate ,
            nth.LastModifiedDate ,
            nth.Type ,
            nth.SortOrder ,
            nth.IsBreakingNews ,
            nth.Locked ,
            nth.inCover ,
            nth.CheckUpdate ,
            nth.NewsRelation ,
            nth.NewsType
            ,e.Id AS ExpertId
			,e.ExpertName
			,e.JobTitle
			,e.[Description]
			,e.DisplayJobTitle
			,e.Quote
			,e.Avatar AS AvatarExpert
			,ne.[Value]
    FROM    NewsTopHot nth
    LEFT JOIN NewsExtension ne ON ne.NewsId = nth.NewsId AND ne.[Type] = 5001
	LEFT JOIN ExpertInNews ein ON ein.NewsId = nth.NewsId
	LEFT JOIN Expert e ON e.Id = ein.ExpertId
	WHERE (nth.Type = 1) OR (nth.Type = 2) OR (nth.Type = 3)
    ORDER BY Type ,SortOrder
     
