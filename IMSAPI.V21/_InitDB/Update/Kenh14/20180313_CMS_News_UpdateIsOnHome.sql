USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateIsOnHome]    Script Date: 03/13/2018 10:18:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/03/2017
ALTER PROCEDURE [dbo].[CMS_News_UpdateIsOnHome]
	@Id bigint,
	@IsOnHome bit	
AS
	BEGIN	
		UPDATE News set IsOnHome=@IsOnHome where Id=@Id	
		IF (@IsOnHome = 'True')
		BEGIN
			UPDATE NewsPublish set IsOnHome=@IsOnHome where ID=@Id
		END	
	END












