--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTagIdWithPaging]    Script Date: 3/9/2019 11:16:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTagIdWithPaging]
	@TagId bigint,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
	WHERE TN.TagID = @TagId

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
		WHERE TN.TagId = @TagId
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC




















