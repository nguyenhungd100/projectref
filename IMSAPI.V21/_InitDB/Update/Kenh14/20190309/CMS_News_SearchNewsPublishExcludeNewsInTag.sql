--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTag]    Script Date: 3/9/2019 11:43:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTag]
	@ZoneId int,
	@Keyword nvarchar(200),
	@TagId int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	DECLARE @CurrentNewsInTag TABLE(NewsId bigint)
	INSERT INTO @CurrentNewsInTag (NewsId) SELECT NewsId FROM TagNews WHERE TagId = @TagId

	SELECT @TotalRow = COUNT(*) 
	FROM NewsPublish
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
		AND (NewsId NOT IN (SELECT NewsId FROM @CurrentNewsInTag))

	SELECT * FROM
	(
		SELECT NewsId AS Id, Title, Avatar, DistributionDate, Url, 8 as Status,
			row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
		FROM NewsPublish
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
			AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
			AND (NewsId NOT IN (SELECT NewsId FROM @CurrentNewsInTag)) AND (DistributionDate <= GETDATE())
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC





















