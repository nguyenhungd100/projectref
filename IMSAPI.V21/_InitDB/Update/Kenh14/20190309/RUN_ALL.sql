--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTagIdWithPaging]    Script Date: 3/9/2019 11:16:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTagIdWithPaging]
	@TagId bigint,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
	WHERE TN.TagID = @TagId

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN TagNews AS TN ON N.Id = TN.NewsID
		WHERE TN.TagId = @TagId
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC


	--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByThreadIdWithPaging]    Script Date: 3/9/2019 11:35:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByThreadIdWithPaging]
	@ThreadId bigint,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
	WHERE TN.ThreadID = @ThreadId

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN ThreadNews AS TN ON N.Id = TN.NewsID
		WHERE TN.ThreadId = @ThreadId
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC


	--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTopicIdWithPaging]    Script Date: 3/9/2019 11:32:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTopicIdWithPaging]
	@Keyword nvarchar(500)='',
	@TopicId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
	WHERE TN.TopicID = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and (@Status=-1 or (@Status<>-1 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url, N.[Status],
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
		WHERE TN.TopicId = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC



	--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTag]    Script Date: 3/9/2019 11:43:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/03/2019
ALTER PROCEDURE [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTag]
	@ZoneId int,
	@Keyword nvarchar(200),
	@TagId int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	DECLARE @CurrentNewsInTag TABLE(NewsId bigint)
	INSERT INTO @CurrentNewsInTag (NewsId) SELECT NewsId FROM TagNews WHERE TagId = @TagId

	SELECT @TotalRow = COUNT(*) 
	FROM NewsPublish
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
		AND (NewsId NOT IN (SELECT NewsId FROM @CurrentNewsInTag))

	SELECT * FROM
	(
		SELECT NewsId AS Id, Title, Avatar, DistributionDate, Url, 8 as Status,
			row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
		FROM NewsPublish
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
			AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
			AND (NewsId NOT IN (SELECT NewsId FROM @CurrentNewsInTag)) AND (DistributionDate <= GETDATE())
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC























