USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTag_DeleteTags]
	@TagIds varchar(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @query nvarchar(1000)
	declare @param nvarchar(200)
	set @param = '@TagIds varchar(1000)'
	set @query = 'Delete DirectTagNews where TagId in ('+@TagIds+') '
    set @query = @query + 'Delete DirectTag where Id in ('+@TagIds+')'
    EXEC SP_EXECUTESQL  @query, @param, @TagIds
END