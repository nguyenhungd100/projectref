USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTag_Insert]
	@TagId int output,
	@TagName nvarchar(200),
	@TagLink nvarchar(500),
	@QuoteFormat nvarchar(500),
	@TagType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO DirectTag
           ([TagName]
           ,[TagLink]
		   ,[QuoteFormat]
		   ,[Type])
     VALUES
           (@TagName
           ,@TagLink
           ,@QuoteFormat
		   ,@TagType)

	select @TagId = CAST(scope_identity() AS int)
END