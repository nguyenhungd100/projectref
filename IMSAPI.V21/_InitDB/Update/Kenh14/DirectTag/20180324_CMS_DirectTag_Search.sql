USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTag_Search] --N'trung quoc',0,1,10,0
	@KeyWord nvarchar(300) = '',
	@Type int = 0,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @KeyWord <> ''
		SET @KeyWord = '%' + @KeyWord + '%'

	DECLARE @search nvarchar(max)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)

	-- PARAMETERS
	SET @params = 			'@KeyWord nvarchar(300),'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'

	SET @where = ' WHERE (PATINDEX(@KeyWord, dt.TagName) > 0 )'

	if @Type > 0
		SET @where = @where + ' AND dt.Type = @Type'

	-- ORDER
	SET @order = ' ORDER BY dt.Id DESC'

	-- SELECT TotalRow count
	SET @search = 'SELECT @TotalRow = COUNT(*) from DirectTag dt ' + @where + ';'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' select TOP '+ CONVERT(varchar(5), @PageSize) +' dt.Id,'
		SET @search = @search + ' dt.TagName,'
		SET @search = @search + ' dt.TagLink,'
		SET @search = @search + ' dt.QuoteFormat,'
		SET @search = @search + ' dt.[Type]'
		SET @search = @search + ' from DirectTag dt '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' select * from('
		SET @search = @search + '	select dt.Id,'
		SET @search = @search + '		dt.TagName,'
		SET @search = @search + '		dt.TagLink,'
		SET @search = @search + '		dt.QuoteFormat,'
		SET @search = @search + '		dt.[Type],'
		SET @search = @search + '		ROW_NUMBER() over(' + @order + ') as RowNum'
		SET @search = @search + '	from DirectTag dt '
		SET @search = @search + @where
		SET @search = @search + ' ) AS N'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END

	EXEC SP_EXECUTESQL  @search, @params, 
								@KeyWord,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END