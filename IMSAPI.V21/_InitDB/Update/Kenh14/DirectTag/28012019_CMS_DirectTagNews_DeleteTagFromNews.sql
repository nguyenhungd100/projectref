--USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_DirectTagNews_DeleteTagFromNews]    Script Date: 1/28/2019 5:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
ALTER PROCEDURE [dbo].[CMS_DirectTagNews_DeleteTagFromNews]
	@NewsId bigint,
	@TagId int,
	@Type int=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM DirectTagNews
    WHERE NewsId = @NewsId and TagId = @TagId and Type=@Type
END
