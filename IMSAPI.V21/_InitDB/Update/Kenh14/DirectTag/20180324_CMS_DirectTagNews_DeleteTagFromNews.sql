USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTagNews_DeleteTagFromNews]
	@NewsId bigint,
	@TagId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE FROM DirectTagNews
    WHERE NewsId = @NewsId and TagId = @TagId
END
