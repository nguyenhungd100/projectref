USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTag_Update]
	@TagId int,
	@TagName nvarchar(200),
	@TagLink nvarchar(500),
	@QuoteFormat nvarchar(500),
	@Type int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE DirectTag
	SET [TagName] = @TagName
      ,[TagLink] = @TagLink
	  ,[QuoteFormat] = @QuoteFormat
	  ,[Type] = @Type
	WHERE Id = @TagId
END
