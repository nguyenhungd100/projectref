USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[DirectTag]    Script Date: 03/24/2018 11:38:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE TABLE [dbo].[DirectTag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TagName] [nvarchar](200) NULL,
	[TagLink] [nvarchar](500) NULL,
	[QuoteFormat] [nvarchar](500) NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_DirectTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type = 0 Tag|Type =1 link ch�n b�i' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DirectTag', @level2type=N'COLUMN',@level2name=N'Type'
GO


