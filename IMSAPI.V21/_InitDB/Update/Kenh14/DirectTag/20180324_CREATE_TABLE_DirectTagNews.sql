USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[DirectTag]    Script Date: 03/24/2018 11:38:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE TABLE [dbo].[DirectTagNews](
	[TagId] [int] NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[Type] [int] NOT NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_DirectTagNews_1] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[NewsId] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type=1 key-trang-chu | Type=2 key-cate' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DirectTagNews', @level2type=N'COLUMN',@level2name=N'Type'
GO