USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTagNews_InsertMulti] 
	@TagIds varchar(max),
	@NewsIds varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @tbl_TagIds table(Id int identity(1,1),TagId int)
    declare @tbl_NewsIds table(Id int identity(1,1),NewsId bigint)
    
    insert into @tbl_TagIds(TagId) 
    select t.NewsId from SplitNewsStringToTable(@TagIds,',') as t
    
    declare @tagId int
    declare @newsId bigint
    declare @i int
    declare @j int
    declare @iCountTagNews int
    declare @iCountTag int
    declare @iCountNews int
    
    select @iCountTag = COUNT(0) from @tbl_TagIds
    
    set @i = 1
    set @j = 1
    -- Loop through rows of TagID table
    while @i <= @iCountTag
    begin
		select @tagId = TagId from @tbl_TagIds where Id = @i
		if @tagId > 0
		begin
			insert into @tbl_NewsIds(NewsId)
			select t.NewsId from SplitNewsStringToTable(@NewsIds,',') as t
			
			select @iCountNews = COUNT(0) from @tbl_NewsIds
			
			-- Loop through rows of NewsID table
			while @j <= @iCountNews
			begin
				select @newsId = NewsId from @tbl_NewsIds where Id = @j
				if @newsId > 0
				begin
					-- Count number of Tag was bound to New
					select @iCountTagNews = COUNT(0) from DirectTagNews where NewsId = @newsId and [Type] = 0
					if @iCountTagNews < 3
					begin
						-- Don't exists row has TagId = @tagId => insert into DirectTagNews
						if not Exists(select * from DirectTagNews where NewsId = @newsId and TagId = @tagId and [Type] = 0)
						begin
							insert into DirectTagNews(TagId,NewsId,[Type],LastModifiedDate)
							values(@tagId,@newsId,0,GETDATE())
						end
					end
				end
				set @j = @j + 1
			end
		end
		set @i = @i + 1
    end
    
END
