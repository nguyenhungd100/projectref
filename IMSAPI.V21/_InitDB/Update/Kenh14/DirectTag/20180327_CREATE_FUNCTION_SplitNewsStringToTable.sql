USE [IMS2_KENH14_DEV]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitNewsStringToTable]    Script Date: 03/27/2018 10:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE FUNCTION [dbo].[SplitNewsStringToTable] 
(
    @myString nvarchar(max),
    @deliminator varchar(10)
)
RETURNS 
@ReturnTable TABLE 
(
	NewsId bigint null
)
AS
BEGIN
        Declare @iSpaces int
        Declare @part nvarchar(500)

        Select @iSpaces = charindex(@deliminator,@myString,0)
        While @iSpaces > 0

        Begin
            Select @part = substring(@myString,0,charindex(@deliminator,@myString,0))

            Insert Into @ReturnTable Select @part
            Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString))
            Select @iSpaces = charindex(@deliminator,@myString,0)
        end

        If len(@myString) > 0
            Insert Into @ReturnTable
            Select @myString

    RETURN 
END

