USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_NewsPublish_Search] --N'','','',null,null,1,50,0
	@Keyword nvarchar(300) = '',
	@Username varchar(200) = '',
	@ZoneIds varchar(2000),
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--IF(@ZoneIds = '0')
	--	SET @ZoneIds = '';
	
    IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	DECLARE @filter_date_field varchar(20)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@Username varchar(200),'
	SET @params = @params + '@ZoneIds varchar(MAX),'
	SET @params = @params + '@DateFrom datetime,'
	SET @params = @params + '@DateTo datetime,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @filter_date_field = 'N.DistributionDate'
	
	SET @ZoneIds = dbo.CMS_fGetListZoneId(@ZoneIds)
	
	-- WHERE
	IF @Username <> ''
		SET @where = ' WHERE (N.Author = @Username) '
	ELSE SET @where = ' WHERE N.Status = 8'

	IF @DateFrom IS NOT NULL AND @DateTo IS NOT NULL
		SET @where = @where + ' AND (' + @filter_date_field + ' BETWEEN @DateFrom AND @DateTo)'
	ELSE
		IF @DateFrom IS NOT NULL
			SET @where = @where + ' AND (' + @filter_date_field + ' >= @DateFrom)'
		ELSE IF @DateTo IS NOT NULL
			SET @where = @where + ' AND (' + @filter_date_field + ' <= @DateTo)'

	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, N.Title) > 0 )'
		
	IF @ZoneIds <> ''					 -- Filter by zoneid
	  IF PATINDEX('%,%', @ZoneIds) > 0
		SET @where = @where + ' AND (Z.IsPrimary = 1 AND Z.ZoneID in(select item from SplitStrings('''+ @ZoneIds +''')))'
	  ELSE        -- Has only one zoneid -> get all news in this zone
		SET @where = @where + ' AND (Z.ZoneID in(select item from SplitStrings('''+ @ZoneIds +''')))'
		--IF PATINDEX('%,%', @ZoneIds) > 0 -- Has more than one zoneid -> get news in primary zone only
		--	SET @where = @where + ' AND (Z.IsPrimary = 1 AND Z.ZoneID in(select item from SplitStrings('''+ @ZoneIds +''')))'
		--ELSE							 -- Has only one zoneid -> get all news in this zone
		--	SET @where = @where + ' AND (Z.ZoneID in(select item from SplitStrings('''+ @ZoneIds +''')))'
	ELSE								 -- Not filter by zone id -> get all news in primary zone and news which not in any zone
		SET @where = @where + ' AND (Z.IsPrimary = 1 OR Z.IsPrimary IS NULL)'

		
	-- ORDER
	SET @order = ' ORDER BY N.DistributionDate DESC'
		
	-- SELECT TotalRow count
	SET @search =			'SELECT @TotalRow = 1000';-- COUNT(0) FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId ' + @where + ';'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP ' + CONVERT(varchar(5), @PageSize) + ' N.Id as newsID, ISNULL(Z.ZoneId, 0) AS ZoneID,   '
		SET @search = @search + '		N.Title, N.Avatar, N.Author, N.Source,  '
		SET @search = @search + '		N.IsFocus, N.DistributionDate,  '
		SET @search = @search + '		N.Url, N.OriginalId '
		IF @DateFrom IS NOT NULL OR @DateTo IS NOT NULL OR @Keyword <> ''
			SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		ELSE
			SET @search = @search + ' FROM NewsTemp AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		--SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('
		SET @search = @search + ' SELECT N.Id  as newsID, ISNULL(Z.ZoneId, 0) AS ZoneID,  '
		SET @search = @search + '		N.Title, N.Avatar, N.Author, N.Source,  '
		SET @search = @search + '		N.IsFocus, N.DistributionDate,  '
		SET @search = @search + '		N.Url, N.OriginalId,  '
		SET @search = @search + '		row_number() OVER (' + @order + ') AS RowNum '
		IF @DateFrom IS NOT NULL OR @DateTo IS NOT NULL OR @Keyword <> ''
			SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		ELSE
			SET @search = @search + ' FROM NewsTemp AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		--SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		SET @search = @search + @where
		SET @search = @search + ' ) AS N'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
	print(@search)
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@Username,
								@ZoneIds,
								@DateFrom,
								@DateTo,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END




