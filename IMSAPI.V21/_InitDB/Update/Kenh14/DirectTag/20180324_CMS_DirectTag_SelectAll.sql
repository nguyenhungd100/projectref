USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_Search]    Script Date: 03/24/2018 11:05:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--24/03/2018
CREATE PROCEDURE [dbo].[CMS_DirectTag_SelectAll] 
	@PageIndex int,
	@PageSize int,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select @TotalRow = COUNT(0) from DirectTag
	
    if @PageIndex = 1
    begin 
		select Top(@PageSize) d.Id,
		d.TagName,
		d.TagLink,
		d.QuoteFormat,
		d.Type,
		(select COUNT(0) from DirectTagNews dt where dt.TagId = d.Id) as CountInNews
		from DirectTag d
		order by d.Id DESC
    end
    else
    begin
		SELECT * FROM 
		(
			SELECT d.Id,
			d.TagName,
			d.TagLink,
			d.QuoteFormat,
			d.Type,
			(select COUNT(0) from DirectTagNews dt where dt.TagId = d.Id) as CountInNews,
			row_number() OVER (order by Id DESC) AS RowNum 
			FROM DirectTag AS d
		) AS N
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
    end
END