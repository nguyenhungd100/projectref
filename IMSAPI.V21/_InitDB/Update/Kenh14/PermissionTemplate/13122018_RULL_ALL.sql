
Drop Table PermissionTemplate


--USE [IMS2_SPORT5]
GO

/****** Object:  Table [dbo].[PermissionTemplate]    Script Date: 12/13/2018 3:33:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PermissionTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_PermissionTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


alter table PermissionTemplateDetail
add ZoneId int not null;
GO
alter table PermissionTemplateDetail
DROP CONSTRAINT PK_PermissionTemplateDetail;
GO
alter table PermissionTemplateDetail
add CONSTRAINT PK_PermissionTemplateDetail PRIMARY KEY NONCLUSTERED (TemplateId,PermissionId,ZoneId);


GO
--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	<get all permission template>
-- =============================================
alter PROCEDURE [dbo].[CMS_PermissionTemplate_GetAll]
AS
BEGIN
	SELECT Id, TemplateName
	FROM PermissionTemplate
END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplate_Insert]
@TemplateName nvarchar(200)
AS
BEGIN
	INSERT INTO PermissionTemplate(TemplateName) values(@TemplateName)	
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplate_Update]
@TemplateId int,
@TemplateName nvarchar(200)
AS
BEGIN
	UPDATE PermissionTemplate set TemplateName=@TemplateName where Id=@TemplateId
END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplate_Delete]
@TemplateId int
AS
BEGIN
	DELETE PermissionTemplate where Id=@TemplateId
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplateDetail_Update]
@TemplateId int,
@PermissionId int,
@ZoneId int
AS
BEGIN
	IF NOT EXISTS (select 1 from PermissionTemplateDetail where TemplateId=@TemplateId and PermissionId=@PermissionId and ZoneId=@ZoneId)
		INSERT INTO PermissionTemplateDetail(TemplateId,PermissionId,ZoneId) values(@TemplateId,@PermissionId,@ZoneId)
	ELSE
		UPDATE PermissionTemplateDetail set PermissionId=@PermissionId, ZoneId=@ZoneId where TemplateId=@TemplateId and PermissionId=@PermissionId and ZoneId=@ZoneId
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplateDetail_ById]
@TemplateId int
AS
BEGIN
	Select * from PermissionTemplateDetail where TemplateId=@TemplateId
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplateDetail_Update]    Script Date: 12/18/2018 9:59:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplateDetail_RemoveAll]
@TemplateId int
AS
BEGIN
	DELETE PermissionTemplateDetail where TemplateId=@TemplateId
END