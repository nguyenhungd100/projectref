--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplateDetail_Update]
@TemplateId int,
@PermissionId int,
@ZoneId int
AS
BEGIN
	IF NOT EXISTS (select 1 from PermissionTemplateDetail where TemplateId=@TemplateId and PermissionId=@PermissionId and ZoneId=@ZoneId)
		INSERT INTO PermissionTemplateDetail(TemplateId,PermissionId,ZoneId) values(@TemplateId,@PermissionId,@ZoneId)
	ELSE
		UPDATE PermissionTemplateDetail set PermissionId=@PermissionId, ZoneId=@ZoneId where TemplateId=@TemplateId and PermissionId=@PermissionId and ZoneId=@ZoneId
END