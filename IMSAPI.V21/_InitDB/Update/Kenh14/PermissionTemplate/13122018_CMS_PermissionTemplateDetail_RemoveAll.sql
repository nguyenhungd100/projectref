--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplateDetail_Update]    Script Date: 12/18/2018 9:59:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplateDetail_RemoveAll]
@TemplateId int
AS
BEGIN
	DELETE PermissionTemplateDetail where TemplateId=@TemplateId
END