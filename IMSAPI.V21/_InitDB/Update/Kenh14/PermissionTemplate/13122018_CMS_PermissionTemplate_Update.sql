--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplate_Update]
@TemplateId int,
@TemplateName nvarchar(200)
AS
BEGIN
	UPDATE PermissionTemplate set TemplateName=@TemplateName where Id=@TemplateId
END
