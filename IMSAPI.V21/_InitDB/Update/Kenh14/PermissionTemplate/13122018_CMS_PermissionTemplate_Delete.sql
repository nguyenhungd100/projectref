--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PermissionTemplate_GetAll]    Script Date: 12/13/2018 2:40:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-11-13>
-- Description:	< >
-- =============================================
CREATE PROCEDURE [dbo].[CMS_PermissionTemplate_Delete]
@TemplateId int
AS
BEGIN
	DELETE PermissionTemplate where Id=@TemplateId
END