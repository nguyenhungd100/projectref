USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTags_Insert]    Script Date: 04/24/2018 11:10:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
ALTER PROC [dbo].[CMS_SEOMetaTags_Insert]
@Name nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@ZoneId int,
@Id int OUT
AS
BEGIN 

  INSERT INTO SEOMetaTags (Name, MetaKeyword, MetaDescription,ZoneId) 
  VALUES (@Name,@MetaKeyword,@MetaDescription,@ZoneId)
  
  SET @Id = SCOPE_IDENTITY()
   
END













