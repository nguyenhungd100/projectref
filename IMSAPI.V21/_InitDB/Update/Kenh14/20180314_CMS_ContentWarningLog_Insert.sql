use [IMS2_FULL]
go
--chinhnb
--15/03/2018
CREATE PROCEDURE [dbo].[CMS_ContentWarningLog_Insert]
	@NewsId bigint,@Account varchar(50)
AS
BEGIN
	DECLARE @Status INT;
	SELECT @Status = Status FROM News WHERE Id = @NewsId;
	
	INSERT INTO ContentWarningLog(NewsId,Account,CreatedDate,Status) 
	VALUES(@NewsId,@Account,GETDATE(),@Status)
END