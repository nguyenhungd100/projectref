--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Mp3_SaveOnPlugin]    Script Date: 10/8/2018 1:42:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_Update]
    @Id INT ,
    @Title NVARCHAR(300) ,
    @Sapo NVARCHAR(500) ,
    @CreatedBy VARCHAR(100)
AS 
    UPDATE  dbo.Mp3
    SET     Name = @Title ,
            Description = @Sapo ,
            LastModifiedDate = GETDATE() ,
            LastModifiedBy = @CreatedBy
    WHERE   Id = @Id