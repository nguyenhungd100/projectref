--USE [Nldv3]
GO

/****** Object:  Table [dbo].[Mp3]    Script Date: 10/8/2018 10:29:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mp3](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[UnsignName] [varchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[Avatar] [varchar](255) NULL,
	[KeyVideo] [varchar](100) NULL,
	[Status] [int] NOT NULL,
	[DistributionDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](255) NULL,
	[LastModifiedDate] [datetime] NULL,
	[PublishDate] [datetime] NULL,
	[Url] [varchar](300) NULL,
	[FileName] [varchar](500) NULL,
	[Duration] [varchar](50) NULL,
	[Size] [varchar](20) NULL,
	[Capacity] [int] NULL,
	[EditedBy] [nvarchar](255) NULL,
	[PublishBy] [nvarchar](255) NULL,
 CONSTRAINT [PK_TVC_MP3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


