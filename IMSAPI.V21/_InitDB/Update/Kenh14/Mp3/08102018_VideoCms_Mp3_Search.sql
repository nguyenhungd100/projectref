--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Mp3_Search]    Script Date: 10/8/2018 11:06:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_Search]
    @Title NVARCHAR(300) ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CreatedBy NVARCHAR(250) ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT OUT
AS 
    SELECT  @TotalRow = COUNT(0)
    FROM    Mp3
    WHERE   name LIKE N'%' + @Title + '%'
            AND ( createdBy = @CreatedBy
                  OR @CreatedBy = ''
                )
            AND ( CreatedDate >= @FromDate
                  OR @FromDate IS NULL
                )
            AND ( CreatedDate <= @Todate
                  OR @ToDate IS NULL
                )
    SELECT  *
    FROM    ( SELECT    Id ,
                        Name ,
                        UnsignName ,
                        Description ,
                        HtmlCode ,
                        Avatar ,
                        KeyVideo ,
                        Status ,
                        DistributionDate ,
                        CreatedBy ,
                        CreatedDate ,
                        LastModifiedBy ,
                        LastModifiedDate ,
                        PublishDate ,
                        Url ,
                        FileName ,
                        Duration ,
                        Size ,
                        Capacity ,
                        EditedBy ,
                        PublishBy ,
                        ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
              FROM      Mp3
              WHERE     name LIKE N'%' + @Title + '%'
                        AND ( createdBy = @CreatedBy
                              OR @CreatedBy = ''
                            )
                        AND ( CreatedDate >= @FromDate
                              OR @FromDate IS NULL
                            )
                        AND ( CreatedDate <= @Todate
                              OR @ToDate IS NULL
                            )
            ) abc
    WHERE   RowNum BETWEEN ( @PageIndex - 1 ) * @PageSize
                   AND     @PageIndex * @PageSize