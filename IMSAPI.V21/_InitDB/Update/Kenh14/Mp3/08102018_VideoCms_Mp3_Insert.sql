--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Mp3_Insert]    Script Date: 10/13/2018 10:03:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VideoCms_Mp3_Insert]
    @Id INT OUT ,
    @Name NVARCHAR(250) ,
    @UnsignName VARCHAR(250) ,
    @Description NVARCHAR(MAX) ,
    @HtmlCode NVARCHAR(MAX) ,
    @Avatar VARCHAR(255) ,
    @KeyVideo VARCHAR(100) ,
    @Status INT ,
    @DistributionDate DATETIME ,
    @CreatedBy VARCHAR(255) ,    
    @Url VARCHAR(300) ,
    @FileName VARCHAR(500) ,
    @Duration VARCHAR(50) ,
    @Size VARCHAR(20) ,
    @Capacity INT
AS 
    INSERT  INTO [Mp3]
            ( [Name] ,
              [UnsignName] ,
              [Description] ,
              [HtmlCode] ,
              [Avatar] ,
              [KeyVideo] ,
              [Status] ,
              [DistributionDate] ,
              [CreatedBy] ,             
              [PublishDate] ,
              [Url] ,
              [FileName] ,
              [Duration] ,
              [Size] ,
              [Capacity],
			  [CreatedDate]
            )
    VALUES  ( @Name ,
              @UnsignName ,
              @Description ,
              @HtmlCode ,
              @Avatar ,
              @KeyVideo ,
              @Status ,
              @DistributionDate ,
              @CreatedBy ,
              GETDATE() ,             
              @Url ,
              @FileName ,
              @Duration ,
              @Size ,
              @Capacity,
			  GETDATE()
            )
            SELECT @Id = @@IDENTITY