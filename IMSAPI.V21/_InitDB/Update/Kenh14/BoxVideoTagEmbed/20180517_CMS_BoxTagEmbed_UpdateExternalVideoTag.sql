USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagEmbed_UpdateExternalTag]    Script Date: 05/17/2018 16:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--17/05/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagEmbed_UpdateExternalVideoTag] 
	@ListOfPriority nvarchar(MAX),
	@ZoneId int,
	@Type int,
	@ListOfTitle nvarchar(MAX),
	@ListOfUrl nvarchar(MAX)
AS
BEGIN   
	DELETE BoxTagEmbed WHERE ZoneId = @ZoneId and Type = @Type and TagId = 0
	DECLARE @Index int	
	IF @ListOfTitle <> '' AND @ListOfUrl <> '' 
		BEGIN
			DECLARE @TblTempPriority TABLE(Id int identity(1,1), Priority int)
			INSERT @TblTempPriority (Priority) SELECT CONVERT(int, Part) FROM SplitString(@ListOfPriority, ',');
			
			DECLARE @TblTempTitle TABLE(Id int identity(1,1), Title nvarchar(255))
			INSERT @TblTempTitle (Title) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@ListOfTitle, ',');
			
			DECLARE @TblTempUrl TABLE(Id int identity(1,1), Url nvarchar(255))
			INSERT @TblTempUrl (Url) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@ListOfUrl, ',');
			
			SET @Index = 0;
			DECLARE @NumberRow@TblTempPriority int,
					@TagId int,
					@Title nvarchar(255),
					@Url nvarchar(255);
			SET @NumberRow@TblTempPriority = (SELECT COUNT(Id) FROM @TblTempPriority);
			
			WHILE(@Index <= @NumberRow@TblTempPriority)
			BEGIN
				SET @TagId = (SELECT Priority FROM @TblTempPriority WHERE Id = @Index);
				SET @Title = (SELECT Title FROM @TblTempTitle WHERE Id = @Index);
				SET @Url = (SELECT Url FROM @TblTempUrl WHERE Id = @Index);
				IF NOT EXISTS(SELECT 1 FROM BoxTagEmbed WHERE ZoneId = @ZoneId and Type = @Type and TagId = 0 and Title = @Title) and @TagId=0
					BEGIN
						INSERT INTO BoxTagEmbed (ZoneId,TagId,SortOrder,Type,Title,Url) values( @ZoneId, @TagId, @Index, @Type, @Title, @Url)
					END
				SET @Index = @Index + 1;
			END
		END	
END













