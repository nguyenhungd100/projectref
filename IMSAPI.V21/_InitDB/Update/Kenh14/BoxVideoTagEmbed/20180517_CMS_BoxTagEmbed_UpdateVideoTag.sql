USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagEmbed_Update]    Script Date: 05/17/2018 16:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--17/05/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagEmbed_UpdateVideoTag] 
	@ListOfPriority nvarchar(MAX),
	@ZoneId int,
	@Type int
	
AS
BEGIN
    DELETE BoxTagEmbed WHERE ZoneId =@ZoneId and Type=@Type and TagId <> 0
	DECLARE @Priority int, 
			@TagId bigint,
			@TagName NVARCHAR(200),
			@TagUrl NVARCHAR(300)
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	
	FETCH NEXT FROM PriorityCursor INTO @Priority, @TagId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
	
		SELECT @TagName = Name, @TagUrl = Url FROM dbo.VideoTag WHERE Id = @TagId
			
		IF NOT EXISTS(SELECT 1 FROM BoxTagEmbed WHERE TagId = @TagId and ZoneId = @ZoneId and Type = @Type and TagId <> 0) AND @TagId<>0
			INSERT INTO BoxTagEmbed (ZoneId,TagId,SortOrder,Type,Title,Url) values( @ZoneId,@TagId,@Priority,@Type,@TagName,@TagUrl)
			FETCH NEXT FROM PriorityCursor INTO @Priority, @TagId 
	END   

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor
END














