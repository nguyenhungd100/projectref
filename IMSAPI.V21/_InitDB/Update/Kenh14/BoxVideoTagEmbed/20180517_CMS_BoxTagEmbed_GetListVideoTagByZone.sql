USE [IMS2_AFAMILY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagEmbed_GetListByZone]    Script Date: 05/17/2018 16:13:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--17/05/2018
CREATE PROCEDURE [dbo].[CMS_BoxTagEmbed_GetListVideoTagByZone]
@ZoneId int,
@Type int
AS
	BEGIN
		SELECT TOP(100) BT.Id, BT.ZoneId, ISNULL(BT.TagId, 0) AS TagId, BT.SortOrder, BT.Type, ISNULL(TP.Avatar, '') AS Avatar, 
			ISNULL(TP.Name, BT.Title) AS Name, BT.Url
		FROM BoxTagEmbed as BT
			left join VideoTag as TP on BT.TagId =TP.Id
		where BT.ZoneId =@ZoneId and BT.Type=@Type
		ORDER BY BT.SortOrder ASC
	END













