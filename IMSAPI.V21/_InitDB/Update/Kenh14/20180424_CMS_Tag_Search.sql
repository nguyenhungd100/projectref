USE [IMS2_WEBTHETHAO]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Search]    Script Date: 04/27/2018 15:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/04/2018
ALTER PROCEDURE [dbo].[CMS_Tag_Search] --'',0, 0,-1,-1,0,0,1,20,0,0
	@Keyword nvarchar(500),
	@ParentId BIGINT,
	@IsThread int,
	@IsHotTag int = -1,
	@ZoneId int,
	@Type int,
	@OrderBy int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@GetTagHasNewsOnly bit = 0,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ParentId int,'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Name) > 0)' 
		
	IF @ParentId > 0
		SET @where = @where + ' AND (T.ParentId = @ParentId)' 
		
--	IF @IsThread = 0
		SET @where = @where + ' AND (T.IsThread = 0)' 
--	ELSE IF @IsThread = 1
--		SET @where = @where + ' AND (T.IsThread = 1)' 
		
	IF @IsHotTag = 0
		SET @where = @where + ' AND (T.IsHotTag = 0)' 
	ELSE IF @IsHotTag = 1
		SET @where = @where + ' AND (T.IsHotTag = 1)' 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId)' 
	ELSE
		SET @where = @where + ' AND (TZ.IsPrimary = 1 OR TZ.IsPrimary IS NULL)' 
		
	IF @Type > 0
		SET @where = @where + ' AND (T.ParentId = @Type)' 
	
	IF @OrderBy = 1
		SET @order = ' ORDER BY COUNT(TN.NewsId) DESC, T.IsHotTag DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY COUNT(TN.NewsId) ASC'
	ELSE IF @OrderBy = 3
		SET @order = ' ORDER BY T.IsHotTag DESC, T.Status DESC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Tag AS T LEFT JOIN TagZone AS TZ ON T.Id = TZ.TagId ' + @where + ';'
	SET @search = @search + 'SELECT Id, ParentId, Name, Url, Invisibled, IsHotTag, Type, CreatedDate, ModifiedDate, '
	SET @search = @search + '	CreatedBy, EditedBy, UnsignName, IsThread, Avatar, Priority, TemplateId, NewsCount, ZoneName '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag,  '
	SET @search = @search + '		T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName, '
	SET @search = @search + '		T.IsThread, T.Avatar, T.Priority, T.TemplateId, SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum, Z.Name AS ZoneName '
	SET @search = @search + '	FROM Tag AS T '
	SET @search = @search + '		LEFT JOIN TagZone AS TZ ON T.Id = TZ.TagId '
	SET @search = @search + '		LEFT JOIN TagNews AS TN ON T.Id = TN.TagId '
	SET @search = @search + '		LEFT JOIN Zone AS Z ON Z.Id = TZ.ZoneId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.Id, T.ParentId, T.Name, T.Description, T.Url, T.Invisibled, T.IsHotTag, '
	SET @search = @search + '		T.Type, T.CreatedDate, T.ModifiedDate, T.CreatedBy, T.EditedBy, T.UnsignName, '
	SET @search = @search + '		T.IsThread, T.Avatar, T.Priority, T.TemplateId, Z.Name,T.Status'
	SET @search = @search + ') AS TagTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	IF @GetTagHasNewsOnly = 1
		SET @search = @search + ' AND NewsCount > 0'
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ParentId,
								@ZoneId,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END







