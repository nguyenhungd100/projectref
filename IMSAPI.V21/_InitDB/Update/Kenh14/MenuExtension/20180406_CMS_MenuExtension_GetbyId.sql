USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_GetbyId]    Script Date: 04/07/2018 09:25:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--07/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_GetbyId]
   @Id int
AS
BEGIN
	SELECT Id, ParentMenuId
           ,GroupMenuId
           ,ZoneId
           ,ParentZoneId
           ,Name
           ,Avatar
           ,Url
           ,Priority
           ,Status
           ,TagId
    FROM MenuExtension
    Where Id = @Id
END

