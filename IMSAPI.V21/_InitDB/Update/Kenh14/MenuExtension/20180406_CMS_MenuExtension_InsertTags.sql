USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_InsertTags]    Script Date: 04/10/2018 15:02:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--07/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_InsertTags]
	@MenuId int
   ,@GroupId int
   ,@ZoneId int
   ,@Tags varchar(1000)
AS
BEGIN
	
	DELETE MenuExtension WHERE ParentMenuId = @MenuId AND GroupMenuId = @GroupId AND ZoneId = @ZoneId AND ISNULL(TagId,0) > 0
	
	IF @Tags <> '' 
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId int, @Name nvarchar(250), @Avatar nvarchar(250),@UrlZone varchar(250), @Url nvarchar(250), @ParentZone int,@Index int; 
			
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@Tags, ';')
			
			--IF(@MenuId > 0)
				--SET @ZoneId = (SELECT ZoneId FROM MenuExtension WHERE Id = @MenuId)
				
			SELECT @ParentZone = ParentId,@UrlZone=ShortURL FROM Zone WHERE Id = @ZoneId;
			
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM MenuExtension WHERE TagId = @TagItemId AND GroupMenuId = @GroupId AND ZoneId = @ZoneId AND ParentMenuId = @MenuId)
					BEGIN
						SET @Name ='';
						SET @Avatar ='';
						SET @Url ='';
						SELECT @Name = Name, @Avatar = @Avatar, @Url = Url FROM Tag WHERE Id = @TagItemId;
						SET @Url = @UrlZone + '/'+ISNULL(@Url,'');
						INSERT INTO  MenuExtension(ParentMenuId,GroupMenuId, ZoneId,ParentZoneId,TagId, Name,Avatar,Url, Status,Priority) 
											VALUES (@MenuId, @GroupId, @ZoneId,@ParentZone,@TagItemId, @Name,@Avatar,@Url, 1, @Index);
					END
						
				SET @Index = @Index + 1
			END
		END			
END

