USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_Insert]    Script Date: 04/06/2018 17:33:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--06/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_Insert]
	@ParentMenuId int
   ,@GroupMenuId int
   ,@ZoneId int
   ,@ParentZoneId int
   ,@Name nvarchar(200)
   ,@Avatar nvarchar(200)
   ,@Url nvarchar(200)
   ,@Priority int
   ,@Status int
   ,@listTag varchar(1000)
   ,@Id INT = 0 OUT
AS
BEGIN
	IF(@ParentMenuId > 0)
	BEGIN
		SELECT @ZoneId = ZoneId, @ParentZoneId = ParentZoneId, @GroupMenuId = GroupMenuId
		FROM MenuExtension WHERE Id = @ParentMenuId;
	END
	ELSE IF(@ZoneId > 0)
		SET @ParentZoneId = ISNULL((SELECT ParentId FROM Zone WHERE Id = @ZoneId),0)
	
	INSERT INTO MenuExtension
           (ParentMenuId
           ,GroupMenuId
           ,ZoneId
           ,ParentZoneId
           ,Name
           ,Avatar
           ,Url
           ,Priority
           ,Status)
     VALUES(@ParentMenuId
           ,@GroupMenuId
           ,@ZoneId
           ,@ParentZoneId
           ,@Name
           ,@Avatar
           ,@Url
           ,@Priority
           ,@Status)
           
    SET @Id = SCOPE_IDENTITY() ;
END

