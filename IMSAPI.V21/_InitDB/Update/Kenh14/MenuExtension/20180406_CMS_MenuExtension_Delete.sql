USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_Delete]    Script Date: 04/07/2018 09:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--07/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_Delete]
	@Id int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM MenuExtension
    WHERE Id= @Id
END

