USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_Search]    Script Date: 04/06/2018 17:37:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--06/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_Search]
	@ZoneId int
	,@GroupId int
	,@Status int
    ,@ParentMenuId int
    ,@IsTag bit
AS
BEGIN
		
	SELECT Id, ParentMenuId
           ,GroupMenuId
           ,ZoneId
           ,ParentZoneId
           ,Name
           ,Avatar
           ,Url
           ,Priority
           ,Status
           ,TagId
    FROM MenuExtension
    Where (@Status <= 0 OR Status = @Status) 
    AND (@ZoneId <= 0 OR (ZoneId = @ZoneId OR ParentZoneId = @ZoneId) )
    AND (@GroupId <= 0 OR GroupMenuId = @GroupId) 
    AND (@ParentMenuId < 0 OR ParentMenuId = @ParentMenuId)
    AND ((@IsTag = 1 AND TagId > 0) OR (@IsTag = 0 AND ISNULL(TagId,0) = 0))
    ORDER BY Priority ASC
END

