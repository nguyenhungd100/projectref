USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MenuExtension_Update]    Script Date: 04/06/2018 17:36:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--06/04/2018
CREATE PROCEDURE [dbo].[CMS_MenuExtension_Update]
	@ParentMenuId int
   ,@GroupMenuId int
   ,@ZoneId int
   ,@ParentZoneId int
   ,@Name nvarchar(200)
   ,@Avatar nvarchar(200)
   ,@Url nvarchar(200)
   ,@Priority int
   ,@Status int
   ,@listTag varchar(1000)
   ,@Id int
AS
BEGIN
	UPDATE MenuExtension
           SET ParentMenuId = @ParentMenuId
           ,GroupMenuId = @GroupMenuId
           ,ZoneId = @ZoneId
           ,ParentZoneId = @ParentZoneId
           ,Name = @Name
           ,Avatar = @Avatar
           ,Url = @Url
           ,Priority = @Priority
           ,Status = @Status
     WHERE Id = @Id
END

