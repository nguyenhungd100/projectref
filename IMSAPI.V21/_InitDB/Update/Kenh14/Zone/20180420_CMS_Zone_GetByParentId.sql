USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_GetByParentId]    Script Date: 04/20/2018 10:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-26>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE (ParentId = @ParentId) ORDER BY SortOrder asc
END












