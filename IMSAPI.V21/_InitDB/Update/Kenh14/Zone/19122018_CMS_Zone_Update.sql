--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_Update]    Script Date: 12/19/2018 10:55:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Update]
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(100),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@AllowComment bit = 1,
	@Domain nvarchar(300),
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max),
	@ZoneContent nvarchar(max),
	@Id int
AS
	BEGIN TRANSACTION CMS_Zone_Update
	
	BEGIN TRY
		IF EXISTS (SELECT Id FROM Zone WHERE Id = @Id)
		BEGIN
			UPDATE Zone
			SET
				Name = @Name, 
				Description = @Description, 
				ShortURL = @ShortURL, 
				SortOrder = @SortOrder, 
				ParentId = @ParentId, 
				Invisibled = @Invisibled, 
				Status = @Status,
				ModifiedDate = GETDATE(),
				AllowComment = @AllowComment,
				Domain=@Domain,
				Avatar=@Avatar,
				AvatarCover=@AvatarCover,
				Logo=@Logo,
				MetaAvatar=@MetaAvatar,
				ZoneContent=@ZoneContent
			WHERE Id = @Id
			COMMIT TRANSACTION CMS_Zone_Update
		END
		ELSE BEGIN
			RAISERROR ('This zone does not existed in database.', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Update
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Update
	END CATCH

