USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_Active_GetByParentId]    Script Date: 04/20/2018 11:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		longlh
-- Create date: 04/09/2014
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id with status=1>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_Active_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone WHERE Status=1  ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE ParentId = @ParentId AND Status=1 ORDER BY SortOrder asc
END













