USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_Insert]    Script Date: 04/20/2018 10:31:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Insert]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max)
AS
	BEGIN TRANSACTION CMS_Zone_Insert
	
	BEGIN TRY
		IF @Name IS NOT NULL AND @Name <> ''
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment,
				[Avatar]
				,[AvatarCover]
				,[Logo]				
				,[MetaAvatar]
			)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment,
				@Avatar
				,@AvatarCover
				,@Logo			
				,@MetaAvatar
			)
			COMMIT TRANSACTION CMS_Zone_Insert
		END
		ELSE BEGIN
			RAISERROR ('Invalid Zone Name', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Insert
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Insert
	END CATCH

















