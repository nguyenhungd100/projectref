USE [IMS2_WEBTHETHAO_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_GetById]    Script Date: 04/20/2018 10:59:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-24>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetById]
	@Id int
AS
BEGIN
	SELECT * FROM Zone	WHERE (Id = @Id)
END














