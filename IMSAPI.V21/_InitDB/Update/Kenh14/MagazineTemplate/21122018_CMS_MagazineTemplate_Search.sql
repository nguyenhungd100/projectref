--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Search]    Script Date: 12/22/2018 9:55:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Search]
	@Keyword nvarchar(300),
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int OUTPUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	-- WHERE
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, Title) > 0)'
		
	IF @Status > 0
		SET @where = @where + ' AND (Status = @Status)'
		
	IF @Type > 0
		SET @where = @where + ' AND (Type = @Type)'
	
	SET @order = ' ORDER BY CreatedDate DESC'
		
	-- SELECT TotalRow count
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM MagazineTemplate ' + @where + ';'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP ' + CONVERT(varchar(5), @PageSize) + ' * '
		SET @search = @search + ' FROM MagazineTemplate '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('		
		SET @search = @search + ' SELECT *, '
		SET @search = @search + '	ROW_NUMBER() OVER (' + @order + ') AS RowNum '
		SET @search = @search + ' FROM MagazineTemplate '
		SET @search = @search + @where
		SET @search = @search + ' ) AS MagazineTemplateTempTable'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
	
	EXEC SP_EXECUTESQL  @search,@params, 
								@Keyword,
								@Status,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END