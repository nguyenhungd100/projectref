--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[MagazineTemplate]    Script Date: 12/22/2018 10:25:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MagazineTemplate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Avatar] [nvarchar](500) NULL,
	[Type] [int] NULL,
	[Cover] [nvarchar](500) NULL,
	[MobileCover] [nvarchar](500) NULL,
	[VideoCover] [nvarchar](500) NULL,
	[TextLayer] [nvarchar](500) NULL,
	[BackgroundColor] [varchar](50) NULL,
	[TextColor] [varchar](50) NULL,
	[DesktopZipUrl] [nvarchar](500) NULL,
	[MobileZipUrl] [nvarchar](500) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [int] NULL,
	[Sapo] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
 CONSTRAINT [PK_MagazineTemplate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[MagazineTemplate] ADD  CONSTRAINT [DF_MagazineTemplate_Type]  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[MagazineTemplate] ADD  CONSTRAINT [DF_MagazineTemplate_Status]  DEFAULT ((1)) FOR [Status]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'dang bai 0=editor, 1=zip' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MagazineTemplate', @level2type=N'COLUMN',@level2name=N'Type'
GO



--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Insert]    Script Date: 12/21/2018 4:26:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Insert]	
	@Title nvarchar(500),
	@Avatar nvarchar(500),
	@Type int,
	@Cover nvarchar(500),
	@MobileCover nvarchar(500),
	@VideoCover nvarchar(500),
	@TextLayer nvarchar(500),
	@BackgroundColor varchar(50),
	@TextColor varchar(50),
	@DesktopZipUrl nvarchar(500),
	@MobileZipUrl nvarchar(500),
	@CreatedBy varchar(100),	
	@ModifiedBy varchar(100),	
	@Status int,
	@Sapo nvarchar(max),
	@Content nvarchar(max),
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO MagazineTemplate (Title, Avatar, Type, Cover, MobileCover, VideoCover, TextLayer, BackgroundColor, 
		TextColor, DesktopZipUrl, MobileZipUrl, CreatedBy, CreatedDate, ModifiedBy,  ModifiedDate, Status, Sapo, Content)
	VALUES (@Title, @Avatar, @Type, @Cover, @MobileCover, @VideoCover, @TextLayer, @BackgroundColor, 
		@TextColor, @DesktopZipUrl, @MobileZipUrl, @CreatedBy, GETDATE(), @ModifiedBy, GETDATE(), @Status,@Sapo,@Content)
		
	SET @Id = SCOPE_IDENTITY()
END

--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_GetbyId]    Script Date: 12/21/2018 4:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_GetbyId]
	@Id int
AS
BEGIN
	SELECT * 
	FROM MagazineTemplate
	WHERE Id = @Id
END

--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Delete]    Script Date: 12/21/2018 4:24:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Delete]
	@Id int
AS
BEGIN	
	DELETE FROM MagazineTemplate
	WHERE Id = @Id
END


--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Search]    Script Date: 12/22/2018 9:55:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Search]
	@Keyword nvarchar(300),
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int OUTPUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	-- WHERE
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, Title) > 0)'
		
	IF @Status > 0
		SET @where = @where + ' AND (Status = @Status)'
		
	IF @Type > 0
		SET @where = @where + ' AND (Type = @Type)'
	
	SET @order = ' ORDER BY CreatedDate DESC'
		
	-- SELECT TotalRow count
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM MagazineTemplate ' + @where + ';'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP ' + CONVERT(varchar(5), @PageSize) + ' * '
		SET @search = @search + ' FROM MagazineTemplate '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('		
		SET @search = @search + ' SELECT *, '
		SET @search = @search + '	ROW_NUMBER() OVER (' + @order + ') AS RowNum '
		SET @search = @search + ' FROM MagazineTemplate '
		SET @search = @search + @where
		SET @search = @search + ' ) AS MagazineTemplateTempTable'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
	
	EXEC SP_EXECUTESQL  @search,@params, 
								@Keyword,
								@Status,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Update]    Script Date: 12/22/2018 9:57:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Update]
	@Title nvarchar(500),
	@Avatar nvarchar(500),
	@Type int,
	@Cover nvarchar(500),
	@MobileCover nvarchar(500),
	@VideoCover nvarchar(500),
	@TextLayer nvarchar(500),
	@BackgroundColor varchar(50),
	@TextColor varchar(50),
	@DesktopZipUrl nvarchar(500),
	@MobileZipUrl nvarchar(500),	
	@ModifiedBy varchar(100),	
	@Status int,
	@Sapo nvarchar(max),
	@Content nvarchar(max),
	@Id int
AS
BEGIN
	UPDATE MagazineTemplate 
	SET Title = @Title, 
		Avatar = @Avatar, 		
		[Type] = @Type, 
		Cover=@Cover,
		MobileCover=@MobileCover,
		VideoCover =@VideoCover,
		TextLayer=@TextLayer,
		BackgroundColor=@BackgroundColor,
		TextColor=@TextColor,
		DesktopZipUrl=@DesktopZipUrl,
		MobileZipUrl=@MobileZipUrl,		
		ModifiedBy = @ModifiedBy, 
		ModifiedDate = GETDATE(), 
		[Status] = @Status,
		Sapo=@Sapo,
		Content=@Content
	WHERE (Id = @Id)	
END

