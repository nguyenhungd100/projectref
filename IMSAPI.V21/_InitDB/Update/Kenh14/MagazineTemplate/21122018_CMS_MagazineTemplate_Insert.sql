--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Insert]    Script Date: 12/21/2018 4:26:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_Insert]	
	@Title nvarchar(500),
	@Avatar nvarchar(500),
	@Type int,
	@Cover nvarchar(500),
	@MobileCover nvarchar(500),
	@VideoCover nvarchar(500),
	@TextLayer nvarchar(500),
	@BackgroundColor varchar(50),
	@TextColor varchar(50),
	@DesktopZipUrl nvarchar(500),
	@MobileZipUrl nvarchar(500),
	@CreatedBy varchar(100),	
	@ModifiedBy varchar(100),	
	@Status int,
	@Sapo nvarchar(max),
	@Content nvarchar(max),
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO MagazineTemplate (Title, Avatar, Type, Cover, MobileCover, VideoCover, TextLayer, BackgroundColor, 
		TextColor, DesktopZipUrl, MobileZipUrl, CreatedBy, CreatedDate, ModifiedBy,  ModifiedDate, Status, Sapo, Content)
	VALUES (@Title, @Avatar, @Type, @Cover, @MobileCover, @VideoCover, @TextLayer, @BackgroundColor, 
		@TextColor, @DesktopZipUrl, @MobileZipUrl, @CreatedBy, GETDATE(), @ModifiedBy, GETDATE(), @Status,@Sapo,@Content)
		
	SET @Id = SCOPE_IDENTITY()
END