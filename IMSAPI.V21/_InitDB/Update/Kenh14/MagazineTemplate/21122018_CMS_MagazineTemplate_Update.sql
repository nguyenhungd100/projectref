--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_Update]    Script Date: 12/22/2018 9:57:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
alter PROCEDURE [dbo].[CMS_MagazineTemplate_Update]
	@Title nvarchar(500),
	@Avatar nvarchar(500),
	@Type int,
	@Cover nvarchar(500),
	@MobileCover nvarchar(500),
	@VideoCover nvarchar(500),
	@TextLayer nvarchar(500),
	@BackgroundColor varchar(50),
	@TextColor varchar(50),
	@DesktopZipUrl nvarchar(500),
	@MobileZipUrl nvarchar(500),	
	@ModifiedBy varchar(100),	
	@Status int,
	@Sapo nvarchar(max),
	@Content nvarchar(max),
	@Id int
AS
BEGIN
	UPDATE MagazineTemplate 
	SET Title = @Title, 
		Avatar = @Avatar, 		
		[Type] = @Type, 
		Cover=@Cover,
		MobileCover=@MobileCover,
		VideoCover =@VideoCover,
		TextLayer=@TextLayer,
		BackgroundColor=@BackgroundColor,
		TextColor=@TextColor,
		DesktopZipUrl=@DesktopZipUrl,
		MobileZipUrl=@MobileZipUrl,		
		ModifiedBy = @ModifiedBy, 
		ModifiedDate = GETDATE(), 
		[Status] = @Status,
		Sapo=@Sapo,
		Content=@Content
	WHERE (Id = @Id)	
END
