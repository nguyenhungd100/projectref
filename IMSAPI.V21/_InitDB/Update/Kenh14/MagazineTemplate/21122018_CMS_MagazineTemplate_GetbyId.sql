--USE [IMS2_BIZFLY]
GO
/****** Object:  StoredProcedure [dbo].[CMS_MagazineTemplate_GetbyId]    Script Date: 12/21/2018 4:25:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_MagazineTemplate_GetbyId]
	@Id int
AS
BEGIN
	SELECT * 
	FROM MagazineTemplate
	WHERE Id = @Id
END

