USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItemMobile_GetById]    Script Date: 8/24/2018 2:32:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Get StreamItemMobile by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItemMobile_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM StreamItemMobile
	WHERE (Id = @Id)
END

