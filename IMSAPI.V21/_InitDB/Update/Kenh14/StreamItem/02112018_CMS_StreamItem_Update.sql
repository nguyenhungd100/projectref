--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Update]    Script Date: 11/2/2018 3:16:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Update StreamItem>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_StreamItem_Update]
	@Id BIGINT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,	
	@DataJson nvarchar(max),
	@Mode int = 0,
	@Title nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE StreamItem
		SET [Order] = @Order, 
			TypeId = @TypeId, 
			TemplateId = @TemplateId, 
			[Status] = @Status, 
			DataJson = @DataJson,
			Mode=@Mode,
			Title=@Title
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

