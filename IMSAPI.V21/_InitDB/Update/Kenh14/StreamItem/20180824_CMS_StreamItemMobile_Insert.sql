USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItemMobile_Insert]    Script Date: 8/24/2018 2:33:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Insert new StreamItemMobile>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItemMobile_Insert]
	@Id BIGINT OUTPUT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,
	@CreatedDate datetime,
	@CreatedBy varchar(100) = '',
	@PublishedDate datetime,
	@PublishedBy varchar(100) = '',
	@DataJson nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		IF @PublishedDate IS NULL
			SET @PublishedDate = GETDATE()
												
		INSERT INTO StreamItemMobile ([Order], TypeId, TemplateId, [Status], CreatedDate, CreatedBy, PublishedDate, PublishedBy, DataJson)
		VALUES (@Order,@TypeId,@TemplateId,@Status,@CreatedDate,@CreatedBy,@PublishedDate, @PublishedBy, @DataJson)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

