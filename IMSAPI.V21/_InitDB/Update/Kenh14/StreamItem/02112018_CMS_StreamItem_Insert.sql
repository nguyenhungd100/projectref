--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Insert]    Script Date: 11/2/2018 3:09:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Insert new StreamItem>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_StreamItem_Insert]
	@Id BIGINT OUTPUT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,
	@CreatedDate datetime,
	@CreatedBy varchar(100) = '',
	@PublishedDate datetime,
	@PublishedBy varchar(100) = '',
	@DataJson nvarchar(max) = '',
	@Mode int = 0,
	@Title nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		IF @PublishedDate IS NULL
			SET @PublishedDate = GETDATE()
												
		INSERT INTO StreamItem ([Order], TypeId, TemplateId, [Status], CreatedDate, CreatedBy, PublishedDate, PublishedBy, DataJson, Mode, Title)
		VALUES (@Order,@TypeId,@TemplateId,@Status,@CreatedDate,@CreatedBy,@PublishedDate, @PublishedBy, @DataJson, @Mode, @Title)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

