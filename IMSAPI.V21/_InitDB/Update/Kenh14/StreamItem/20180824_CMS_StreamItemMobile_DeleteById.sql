USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItemMobile_DeleteById]    Script Date: 8/24/2018 2:32:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Delete StreamItemMobile by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItemMobile_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM StreamItemMobile WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

