USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Search]    Script Date: 8/22/2018 11:24:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Search StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Search] --0, '', 0, 0, 1, 20, 0	
	@TypeId int,
	@TemplateId varchar(250),	
	@Status int,
	@OrderBy int=0,
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@TypeId int,'
	SET @params = @params + '@TemplateId varchar(250),'
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @TypeId > 0
		SET @where = @where + ' AND (T.TypeId = @TypeId)'
	IF @TemplateId <> ''
		SET @where = @where + ' AND (T.TemplateId = @TemplateId)'
	SET @where = @where + ' AND (T.Status = @Status)' 
			
	IF @OrderBy = 0
		SET @order = ' ORDER BY T.Id DESC'
	ELSE IF @OrderBy = 1
		SET @order = ' ORDER BY T.[Order] DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY T.[Order] ASC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM StreamItem AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM StreamItem AS T '+ @where + ' '	
	SET @search = @search + ') AS StreamItemTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,								
								@TypeId,
								@TemplateId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END







