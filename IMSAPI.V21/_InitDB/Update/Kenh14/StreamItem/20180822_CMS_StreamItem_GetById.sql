USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_GetStreamItemByStreamItemID]    Script Date: 8/22/2018 11:23:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Get StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM StreamItem
	WHERE (Id = @Id)
END
















