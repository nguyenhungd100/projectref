alter table StreamItem
Add Mode int, Title nvarchar(max)

--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Insert]    Script Date: 11/2/2018 3:09:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Insert new StreamItem>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_StreamItem_Insert]
	@Id BIGINT OUTPUT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,
	@CreatedDate datetime,
	@CreatedBy varchar(100) = '',
	@PublishedDate datetime,
	@PublishedBy varchar(100) = '',
	@DataJson nvarchar(max) = '',
	@Mode int = 0,
	@Title nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		IF @PublishedDate IS NULL
			SET @PublishedDate = GETDATE()
												
		INSERT INTO StreamItem ([Order], TypeId, TemplateId, [Status], CreatedDate, CreatedBy, PublishedDate, PublishedBy, DataJson, Mode, Title)
		VALUES (@Order,@TypeId,@TemplateId,@Status,@CreatedDate,@CreatedBy,@PublishedDate, @PublishedBy, @DataJson, @Mode, @Title)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Update]    Script Date: 11/2/2018 3:16:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Update StreamItem>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_StreamItem_Update]
	@Id BIGINT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,	
	@DataJson nvarchar(max),
	@Mode int = 0,
	@Title nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE StreamItem
		SET [Order] = @Order, 
			TypeId = @TypeId, 
			TemplateId = @TemplateId, 
			[Status] = @Status, 
			DataJson = @DataJson,
			Mode=@Mode,
			Title=@Title
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Search]    Script Date: 11/2/2018 3:24:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Search StreamItem>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_StreamItem_Search] --0, '', 0, 0, 1, 20, 0	
	@Keyword nvarchar(500)='',
	@TypeId int,
	@Mode int=-1,
	@TemplateId varchar(250),	
	@Status int,
	@OrderBy int=0,
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Keyword nvarchar(500),'	
	SET @params = @params +	'@TypeId int,'
	SET @params = @params +	'@Mode int,'
	SET @params = @params + '@TemplateId varchar(250),'
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Mode > -1
		SET @where = @where + ' AND (T.Mode = @Mode)'
	IF @TypeId > 0
		SET @where = @where + ' AND (T.TypeId = @TypeId)'
	IF @TemplateId <> ''
		SET @where = @where + ' AND (T.TemplateId = @TemplateId)'
	SET @where = @where + ' AND (T.Status = @Status)' 
			
	IF @OrderBy = 0
		SET @order = ' ORDER BY T.Id DESC'
	ELSE IF @OrderBy = 1
		SET @order = ' ORDER BY T.[Order] DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY T.[Order] ASC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM StreamItem AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM StreamItem AS T '+ @where + ' '	
	SET @search = @search + ') AS StreamItemTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,
								@TypeId,
								@Mode,
								@TemplateId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

