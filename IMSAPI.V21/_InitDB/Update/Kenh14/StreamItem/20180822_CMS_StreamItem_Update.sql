USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_Update]    Script Date: 8/22/2018 11:17:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Update StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Update]
	@Id BIGINT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,	
	@DataJson nvarchar(max)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE StreamItem
		SET [Order] = @Order, 
			TypeId = @TypeId, 
			TemplateId = @TemplateId, 
			[Status] = @Status, 
			DataJson = @DataJson
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END
















