--USE [IMS2_Sport5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_GetById]    Script Date: 9/6/2018 3:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-06>
-- Description:	<Get News for StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_GetNewsForStreamItemById]
	@Id BIGINT
AS
BEGIN
	SELECT N.Id as ObjectId,N.Title,N.Sapo,N.Avatar,N.Avatar2,N.Type,N.DisplayStyle,N.Url,N.DistributionDate
			,Z.ShortURL as ZoneUrl,Z.Name as ZoneName,Z.Id as ZoneId,A.Avatar as AuthorAvatar,A.AuthorName,A.AuthorTitle,N.Author
	FROM News N
	LEFT JOIN NewsInZone NZ ON NZ.NewsId=N.Id
	LEFT JOIN Zone Z ON Z.Id=NZ.ZoneId
	LEFT JOIN NewsByAuthor NA ON NA.NewsId=N.Id
	LEFT JOIN NewsAuthor A ON A.Id=NA.AuthorId
	WHERE (N.Id = @Id AND N.Status=8 AND NZ.IsPrimary=1)
END

