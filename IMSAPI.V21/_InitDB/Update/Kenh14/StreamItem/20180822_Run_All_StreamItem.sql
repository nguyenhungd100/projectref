GO
CREATE TABLE [dbo].[StreamItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Order] [bigint] NULL,
	[TypeId] [int] NULL,
	[TemplateId] [varchar](250) NOT NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](100) NULL,
	[DataJson] [nvarchar](max) NULL,
 CONSTRAINT [PK_StreamItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[StreamItem] ADD  CONSTRAINT [DF_StreamItem_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[StreamItem] ADD  CONSTRAINT [DF_StreamItem_Status]  DEFAULT ((1)) FOR [Status]
GO


-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Search StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Search] --0, '', 0, 0, 1, 20, 0	
	@TypeId int,
	@TemplateId varchar(250),	
	@Status int,
	@OrderBy int=0,
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@TypeId int,'
	SET @params = @params + '@TemplateId varchar(250),'
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @TypeId > 0
		SET @where = @where + ' AND (T.TypeId = @TypeId)'
	IF @TemplateId <> ''
		SET @where = @where + ' AND (T.TemplateId = @TemplateId)'
	SET @where = @where + ' AND (T.Status = @Status)' 
			
	IF @OrderBy = 0
		SET @order = ' ORDER BY T.Id DESC'
	ELSE IF @OrderBy = 1
		SET @order = ' ORDER BY T.[Order] DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY T.[Order] ASC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM StreamItem AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM StreamItem AS T '+ @where + ' '	
	SET @search = @search + ') AS StreamItemTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,								
								@TypeId,
								@TemplateId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Get StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM StreamItem
	WHERE (Id = @Id)
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Delete StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM StreamItem WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Update StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Update]
	@Id BIGINT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,	
	@DataJson nvarchar(max)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE StreamItem
		SET [Order] = @Order, 
			TypeId = @TypeId, 
			TemplateId = @TemplateId, 
			[Status] = @Status, 
			DataJson = @DataJson
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Insert new StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Insert]
	@Id BIGINT OUTPUT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,
	@CreatedDate datetime,
	@CreatedBy varchar(100) = '',
	@PublishedDate datetime,
	@PublishedBy varchar(100) = '',
	@DataJson nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		IF @PublishedDate IS NULL
			SET @PublishedDate = GETDATE()
												
		INSERT INTO StreamItem ([Order], TypeId, TemplateId, [Status], CreatedDate, CreatedBy, PublishedDate, PublishedBy, DataJson)
		VALUES (@Order,@TypeId,@TemplateId,@Status,@CreatedDate,@CreatedBy,@PublishedDate, @PublishedBy, @DataJson)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO