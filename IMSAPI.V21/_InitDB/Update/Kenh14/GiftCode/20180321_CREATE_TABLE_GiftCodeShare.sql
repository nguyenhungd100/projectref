USE [IMS_KENH14_EXT]
GO

/***** Object:  Table [dbo].[GiftCodeShare]    Script Date: 03/28/2018 14:34:59 *****/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GiftCodeShare](
 [ID] [int] IDENTITY(1,1) NOT NULL,
 [Email] [nvarchar](100) NULL,
 [Code] [varchar](200) NULL,
 [News_ID] [bigint] NULL,
 [isMobile] [bit] NOT NULL,
 [Status] [bit] NULL,
 [Date] [datetime] NULL,
 [MingId] [int] NULL,
 [Tags] [nvarchar](1000) NULL,
 [FacebookId] [bigint] NULL,
 CONSTRAINT [PK_GiftCode] PRIMARY KEY CLUSTERED 
(
 [ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GiftCodeShare] ADD  CONSTRAINT [DF_GiftCode_isMobile]  DEFAULT ((0)) FOR [isMobile]
GO

ALTER TABLE [dbo].[GiftCodeShare] ADD  CONSTRAINT [DF_GiftCode_Status]  DEFAULT ((0)) FOR [Status]
GO