USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_GiftCode_Insert]    Script Date: 03/29/2018 15:24:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-03-28>
-- Description:	insert gift code
-- =============================================
CREATE PROCEDURE [dbo].[CMS_GiftCode_Insert]
 @NewsId bigint,
 @GiftCode varchar(MAX),
 @Tags nvarchar(300)=''
AS
BEGIN
 BEGIN TRANSACTION
 
 BEGIN TRY 
  DECLARE @Index int
  DECLARE @TblTempGiftCode TABLE (Id int identity(1,1), Code varchar(200))
  DECLARE @CodeItemId varchar(200); 
  SET @Index = 0;
  INSERT INTO @TblTempGiftCode(Code) SELECT Part FROM dbo.SplitString(@GiftCode, ';')
  WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempGiftCode)) 
  BEGIN
   SET @CodeItemId = (SELECT Code FROM @TblTempGiftCode WHERE Id = @Index)
   IF @CodeItemId IS NOT NULL AND @CodeItemId !=''
    IF NOT EXISTS(SELECT Code FROM GiftCodeShare WHERE News_ID = @NewsId AND Code = @CodeItemId)
     INSERT INTO GiftCodeShare (
         News_ID,
         Code,   
         [Status],
         [Date],
         Tags
         ) 
     VALUES (@NewsId, @CodeItemId, 1, GETDATE(), @Tags);
   SET @Index = @Index + 1
  END
  
  COMMIT TRANSACTION
 END TRY
 BEGIN CATCH
  DECLARE @ErrorMessage nvarchar(MAX)
  SET @ErrorMessage = ERROR_MESSAGE()
  RAISERROR (@ErrorMessage, 16, 1);
  ROLLBACK TRANSACTION
 END CATCH
END