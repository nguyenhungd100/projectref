USE [IMS_KENH14_EXT]
GO
/****** Object:  StoredProcedure [dbo].[TUPA_GiftCode_Insert]    Script Date: 03/28/2018 14:20:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-03-28>
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CMS_GiftCode_GetCodeByNewsId]
 @NewsId BIGINT
AS
BEGIN
 DECLARE @UsedCode INT
 DECLARE @FreeCode INT
 SELECT @UsedCode=COUNT(0) FROM GiftCodeShare WHERE News_ID = @NewsId AND FaceBookId IS NOT NULL
 SELECT @FreeCode=COUNT(0) FROM GiftCodeShare WHERE News_ID = @NewsId AND FaceBookId IS NULL
 SELECT @UsedCode as UsedCode, @FreeCode as FreeCode
END