
GO
ALTER TABLE [User]
ADD TelegramId bigint, DepartmentId int


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_Addnew]    Script Date: 10/8/2018 4:44:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass,TelegramId,DepartmentId)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate(),@TelegramId,@DepartmentId)
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateById]    Script Date: 10/8/2018 5:02:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE(),
				TelegramId=@TelegramId,
				DepartmentId=@DepartmentId
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_GetUserById]    Script Date: 10/8/2018 5:04:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode,U.TelegramId,U.DepartmentId
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END



--USE [Nldv3]
GO

/****** Object:  Table [dbo].[Mp3]    Script Date: 10/8/2018 10:29:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Mp3](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[UnsignName] [varchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[HtmlCode] [nvarchar](max) NULL,
	[Avatar] [varchar](255) NULL,
	[KeyVideo] [varchar](100) NULL,
	[Status] [int] NOT NULL,
	[DistributionDate] [datetime] NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [varchar](255) NULL,
	[LastModifiedDate] [datetime] NULL,
	[PublishDate] [datetime] NULL,
	[Url] [varchar](300) NULL,
	[FileName] [varchar](500) NULL,
	[Duration] [varchar](50) NULL,
	[Size] [varchar](20) NULL,
	[Capacity] [int] NULL,
	[EditedBy] [nvarchar](255) NULL,
	[PublishBy] [nvarchar](255) NULL,
 CONSTRAINT [PK_TVC_MP3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Mp3_Search]    Script Date: 10/8/2018 11:06:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_Search]
    @Title NVARCHAR(300) ,
    @FromDate DATETIME ,
    @ToDate DATETIME ,
    @CreatedBy NVARCHAR(250) ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT OUT
AS 
    SELECT  @TotalRow = COUNT(0)
    FROM    Mp3
    WHERE   name LIKE N'%' + @Title + '%'
            AND ( createdBy = @CreatedBy
                  OR @CreatedBy = ''
                )
            AND ( CreatedDate >= @FromDate
                  OR @FromDate IS NULL
                )
            AND ( CreatedDate <= @Todate
                  OR @ToDate IS NULL
                )
    SELECT  *
    FROM    ( SELECT    Id ,
                        Name ,
                        UnsignName ,
                        Description ,
                        HtmlCode ,
                        Avatar ,
                        KeyVideo ,
                        Status ,
                        DistributionDate ,
                        CreatedBy ,
                        CreatedDate ,
                        LastModifiedBy ,
                        LastModifiedDate ,
                        PublishDate ,
                        Url ,
                        FileName ,
                        Duration ,
                        Size ,
                        Capacity ,
                        EditedBy ,
                        PublishBy ,
                        ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
              FROM      Mp3
              WHERE     name LIKE N'%' + @Title + '%'
                        AND ( createdBy = @CreatedBy
                              OR @CreatedBy = ''
                            )
                        AND ( CreatedDate >= @FromDate
                              OR @FromDate IS NULL
                            )
                        AND ( CreatedDate <= @Todate
                              OR @ToDate IS NULL
                            )
            ) abc
    WHERE   RowNum BETWEEN ( @PageIndex - 1 ) * @PageSize
                   AND     @PageIndex * @PageSize


--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Mp3_GetById]    Script Date: 10/8/2018 11:33:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_GetById]
@Id INT 
AS
SELECT * FROM Mp3 WHERE id=@Id


--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Mp3_Insert]    Script Date: 10/8/2018 11:49:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_Insert]
    @Id INT OUT ,
    @Name NVARCHAR(250) ,
    @UnsignName VARCHAR(250) ,
    @Description NVARCHAR(MAX) ,
    @HtmlCode NVARCHAR(MAX) ,
    @Avatar VARCHAR(255) ,
    @KeyVideo VARCHAR(100) ,
    @Status INT ,
    @DistributionDate DATETIME ,
    @CreatedBy VARCHAR(255) ,    
    @Url VARCHAR(300) ,
    @FileName VARCHAR(500) ,
    @Duration VARCHAR(50) ,
    @Size VARCHAR(20) ,
    @Capacity INT
AS 
    INSERT  INTO [Mp3]
            ( [Name] ,
              [UnsignName] ,
              [Description] ,
              [HtmlCode] ,
              [Avatar] ,
              [KeyVideo] ,
              [Status] ,
              [DistributionDate] ,
              [CreatedBy] ,             
              [PublishDate] ,
              [Url] ,
              [FileName] ,
              [Duration] ,
              [Size] ,
              [Capacity]
            )
    VALUES  ( @Name ,
              @UnsignName ,
              @Description ,
              @HtmlCode ,
              @Avatar ,
              @KeyVideo ,
              @Status ,
              @DistributionDate ,
              @CreatedBy ,
              GETDATE() ,             
              @Url ,
              @FileName ,
              @Duration ,
              @Size ,
              @Capacity
            )
            SELECT @Id = @@IDENTITY


--USE [Nldv3]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Mp3_SaveOnPlugin]    Script Date: 10/8/2018 1:42:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_Mp3_Update]
    @Id INT ,
    @Title NVARCHAR(300) ,
    @Sapo NVARCHAR(500) ,
    @CreatedBy VARCHAR(100),
	@HtmlCode nvarchar(max),
	@Avatar varchar(255)
AS 
    UPDATE  dbo.Mp3
    SET     Name = @Title ,
            Description = @Sapo ,
            LastModifiedDate = GETDATE(),
            LastModifiedBy = @CreatedBy,
			HtmlCode=@HtmlCode,
			Avatar=@Avatar
    WHERE   Id = @Id
