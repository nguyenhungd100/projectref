GO
CREATE TABLE [dbo].[StreamItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Order] [bigint] NULL,
	[TypeId] [int] NULL,
	[TemplateId] [varchar](250) NOT NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](100) NULL,
	[DataJson] [nvarchar](max) NULL,
 CONSTRAINT [PK_StreamItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[StreamItem] ADD  CONSTRAINT [DF_StreamItem_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[StreamItem] ADD  CONSTRAINT [DF_StreamItem_Status]  DEFAULT ((1)) FOR [Status]
GO


-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Search StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Search] --0, '', 0, 0, 1, 20, 0	
	@TypeId int,
	@TemplateId varchar(250),	
	@Status int,
	@OrderBy int=0,
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@TypeId int,'
	SET @params = @params + '@TemplateId varchar(250),'
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @TypeId > 0
		SET @where = @where + ' AND (T.TypeId = @TypeId)'
	IF @TemplateId <> ''
		SET @where = @where + ' AND (T.TemplateId = @TemplateId)'
	SET @where = @where + ' AND (T.Status = @Status)' 
			
	IF @OrderBy = 0
		SET @order = ' ORDER BY T.Id DESC'
	ELSE IF @OrderBy = 1
		SET @order = ' ORDER BY T.[Order] DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY T.[Order] ASC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM StreamItem AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM StreamItem AS T '+ @where + ' '	
	SET @search = @search + ') AS StreamItemTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,								
								@TypeId,
								@TemplateId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Get StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM StreamItem
	WHERE (Id = @Id)
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Delete StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM StreamItem WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Update StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Update]
	@Id BIGINT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,	
	@DataJson nvarchar(max)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE StreamItem
		SET [Order] = @Order, 
			TypeId = @TypeId, 
			TemplateId = @TemplateId, 
			[Status] = @Status, 
			DataJson = @DataJson
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-08-22>
-- Description:	<Insert new StreamItem>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_StreamItem_Insert]
	@Id BIGINT OUTPUT,
	@Order BIGINT,
	@TypeId int,
	@TemplateId varchar(250),
	@Status int,
	@CreatedDate datetime,
	@CreatedBy varchar(100) = '',
	@PublishedDate datetime,
	@PublishedBy varchar(100) = '',
	@DataJson nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		IF @PublishedDate IS NULL
			SET @PublishedDate = GETDATE()
												
		INSERT INTO StreamItem ([Order], TypeId, TemplateId, [Status], CreatedDate, CreatedBy, PublishedDate, PublishedBy, DataJson)
		VALUES (@Order,@TypeId,@TemplateId,@Status,@CreatedDate,@CreatedBy,@PublishedDate, @PublishedBy, @DataJson)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO

--USE [IMS2_Sport5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StreamItem_GetById]    Script Date: 9/6/2018 3:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-06>
-- Description:	<Get News for StreamItem by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_GetNewsForStreamItemById]
	@Id BIGINT
AS
BEGIN
	SELECT N.Id as ObjectId,N.Title,N.Sapo,N.Avatar,N.Avatar2,N.Type,N.DisplayStyle,N.Url,N.DistributionDate
			,Z.ShortURL as ZoneUrl,Z.Name as ZoneName,Z.Id as ZoneId,A.Avatar as AuthorAvatar,A.AuthorName,A.AuthorTitle,N.Author
	FROM News N
	LEFT JOIN NewsInZone NZ ON NZ.NewsId=N.Id
	LEFT JOIN Zone Z ON Z.Id=NZ.ZoneId
	LEFT JOIN NewsByAuthor NA ON NA.NewsId=N.Id
	LEFT JOIN NewsAuthor A ON A.Id=NA.AuthorId
	WHERE (N.Id = @Id AND N.Status=8 AND NZ.IsPrimary=1)
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToPublished]    Script Date: 9/12/2018 5:31:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-08-08>
-- Edite:<12-09-2018>
-- Description:	<change news status to Published version 2>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToPublished]
	@Id bigint,
	@PublishedBy varchar(200),
	@NewsPositionTypeForHome int = 0,
	@NewsPositionOrderForHome int = 0,
	@NewsPositionTypeForList int = 0,
	@NewsPositionOrderForList int = 0,
	@PublishedDate bigint,
	@PublishedContent ntext = null
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @NewsInZoneTemp TABLE(ZoneId int, IsPrimary bit)
		DECLARE @PrimaryZoneId int,
				@OldDistributionDate datetime,		
				@Title nvarchar(250),
				@SubTitle nvarchar(250),
				@Sapo nvarchar(MAX),
				@Avatar nvarchar(500),
				@AvatarDesc nvarchar(500),
				@Avatar2 nvarchar(500),
				@Avatar3 nvarchar(500),
				@Avatar4 nvarchar(500),
				@Avatar5 nvarchar(500),
				@Author nvarchar(500),
				@NewsRelation nvarchar(MAX),
				@Source nvarchar(100),
				@IsFocus bit,
				@Type tinyint,
				@ThreadId int,
				@DistributionDate datetime,
				@Tag nvarchar(MAX),
				@TagPrimary nvarchar(500),
				@DisplayStyle int,
				@DisplayPosition int,
				@DisplayInSlide int,
				@AvatarCustom varchar(500),
				@OriginalId int,
				@NewsType tinyint,
				@IsOnHome bit,
				@Url nvarchar(500),
				@TagItem nvarchar(1000),
				@InitSapo nvarchar(500),
				@OriginalUrl nvarchar(500),
				@InterviewId int,
				@IsBreakingNews bit,
				@IsPr bit,
				@AdStore bit,
				@AdStoreUrl nvarchar(500),
				@IsOnMobile bit,
				@RollingNewsId int,
				@TagSubTitleId int,
				@LocationType int,
				@ExpiredDate datetime,
				@SourceUrl nvarchar(500),
				@PrPosition int,
				@ShortTitle nvarchar(250),
				@Priority int,
				@ParentNewsId bigint
				
		INSERT INTO @NewsInZoneTemp (ZoneId, IsPrimary)
		SELECT ZoneId, IsPrimary
		FROM NewsInZone
		WHERE NewsId = @Id
		
		SELECT @PrimaryZoneId = ZoneId
		FROM @NewsInZoneTemp
		WHERE IsPrimary = 1

		SELECT  @Title = Title,
				@SubTitle = SubTitle,
				@Sapo = Sapo,
				@Avatar = Avatar,
				@AvatarDesc = AvatarDesc,
				@Avatar2 = Avatar2,
				@Avatar3 = Avatar3,
				@Avatar4 = Avatar4,
				@Avatar5 = Avatar5,
				@Author = Author,
				@NewsRelation = NewsRelation,
				@Source = Source,
				@IsFocus = IsFocus,
				@Type = Type,
				@ThreadId = ThreadId,
				@DistributionDate = ISNULL(DistributionDate, GETDATE()),
				@Tag = Tag,
				@TagPrimary = TagPrimary,
				@DisplayStyle = DisplayStyle,
				@DisplayPosition = DisplayPosition,
				@DisplayInSlide = DisplayInSlide,
				@AvatarCustom = AvatarCustom,
				@OriginalId = OriginalId,
				@NewsType = NewsType,
				@IsOnHome = IsOnHome,
				@Url = Url,
				@TagItem = TagItem,
				@InitSapo = InitSapo,
				@OriginalUrl = OriginalUrl,
				@InterviewId = InterviewId,
				@IsBreakingNews = IsBreakingNews,
				@IsPr = IsPr,
				@AdStore = AdStore,
				@AdStoreUrl = AdStoreUrl,
				@IsOnMobile = IsOnMobile,
				@RollingNewsId = RollingNewsId,
				@TagSubTitleId = TagSubTitleId,
				@LocationType = LocationType,
				@ExpiredDate = ExpiredDate,
				@SourceUrl = SourceUrl,
				@PrPosition = PrPosition,
				@ShortTitle = ShortTitle,
				@Priority = Priority,
				@ParentNewsId=ParentNewsId
		FROM News
		WHERE Id = @Id

		IF @ShortTitle IS NULL OR @ShortTitle = ''
			SET @ShortTitle = @Title

		IF @PublishedContent IS NULL
			SELECT @PublishedContent = Body FROM News WHERE Id = @Id

		SET @PublishedDate = dbo.UNIX_TIMESTAMP(@DistributionDate)

		SELECT @OldDistributionDate = PublishedDate
		FROM NewsContent
		WHERE NewsId = @Id
		PRINT 1
		IF @OldDistributionDate IS NULL
		BEGIN
			SET @OldDistributionDate = '1800-01-01'
			PRINT 2
			INSERT INTO NewsContent
				(NewsId, ZoneId, Title, SubTitle, Sapo, Body, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, Type, ThreadId, 
				PublishedDate, Tags, TagPrimary, OriginalId, Url, TagItem, InitSapo, OriginalUrl, InterviewId, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, LocationType, ExpiredDate, SourceUrl, PrPosition, NewsType, ParentNewsId)
			VALUES (@Id,@PrimaryZoneId,@Title,@SubTitle,@Sapo, @PublishedContent, @Avatar, @AvatarDesc, @Avatar2, @Avatar3, @Avatar4, @Avatar5, @Author, @NewsRelation, @Source, @Type, @ThreadId, 
				@DistributionDate, @Tag, @TagPrimary, @OriginalId, @Url, @TagItem, @InitSapo, @OriginalUrl, @InterviewId, @AdStore, @AdStoreUrl, 
				@RollingNewsId, @TagSubTitleId, @LocationType, @ExpiredDate, @SourceUrl, @PrPosition, @NewsType, @ParentNewsId)
		END
		ELSE
			PRINT 3
			UPDATE NewsContent
			SET ZoneId = @PrimaryZoneId,
				Title = @Title,
				SubTitle = @SubTitle,
				Sapo = @Sapo,
				Body = @PublishedContent,
				Avatar = @Avatar,
				AvatarDesc = @AvatarDesc,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				Author = @Author,
				NewsRelation = @NewsRelation,
				Source = @Source,
				Type = @Type,
				ThreadId = @ThreadId,
				PublishedDate = @DistributionDate,
				Tags = @Tag,
				TagPrimary = @TagPrimary,
				OriginalId = @OriginalId,
				Url = @Url,
				TagItem = @TagItem,
				InitSapo = @InitSapo,
				OriginalUrl = @OriginalUrl,
				InterviewId = @InterviewId,
				AdStore = @AdStore,
				AdStoreUrl = @AdStoreUrl,
				RollingNewsId = @RollingNewsId,
				TagSubTitleId = @TagSubTitleId,
				LocationType = @LocationType,
				ExpiredDate = @ExpiredDate,
				SourceUrl = @SourceUrl,
				PrPosition = @PrPosition,
				NewsType = @NewsType,
				ParentNewsId=@ParentNewsId
			WHERE NewsId = @Id
		
		DECLARE @RemovedZoneIds TABLE(ZoneId int)
		INSERT INTO @RemovedZoneIds (ZoneId)
		SELECT ZoneId
		FROM NewsPublish
		WHERE (NewsId = @Id) AND ZoneId NOT IN (SELECT ZoneId FROM @NewsInZoneTemp)
		
		DELETE FROM NewsPublish
		WHERE (NewsId = @Id) AND ZoneId NOT IN (SELECT ZoneId FROM @NewsInZoneTemp)
		
		DECLARE @CurrentNewsInZoneTemp TABLE(ZoneId int)
		
		INSERT INTO @CurrentNewsInZoneTemp (ZoneId)
		SELECT ZoneId
		FROM NewsPublish
		WHERE NewsId = @Id
		
		PRINT 4
		UPDATE NewsPublish
		SET IsPrimary = 0,
			LastModifiedDate = GETDATE(),
			PublishedDate = @PublishedDate,			
			Title = @ShortTitle,
			TitleDetail = @Title,
			SubTitle = @SubTitle,
			Sapo = @Sapo,
			Avatar = @Avatar,
			AvatarDesc = @AvatarDesc,
			Avatar2 = @Avatar2,
			Avatar3 = @Avatar3,
			Avatar4 = @Avatar4,
			Avatar5 = @Avatar5,
			Author = @Author,
			NewsRelation = @NewsRelation,
			Source = @Source,
			IsFocus = @IsFocus,
			Type = @Type,
			ThreadId = @ThreadId,
			DistributionDate = @DistributionDate,
			DisplayStyle = @DisplayStyle,
			DisplayPosition = @DisplayPosition,
			DisplayInSlide = @DisplayInSlide,
			AvatarCustom = @AvatarCustom,
			OriginalId = @OriginalId,
			NewsType = @NewsType,
			IsOnMobile = @IsOnMobile,
			Url = @Url,
			TagItem = @TagItem,
			InitSapo = @InitSapo,
			OriginalUrl = @OriginalUrl,
			InterviewId = @InterviewId,
			IsBreakingNews = @IsBreakingNews,
			IsPr = @IsPr,
			AdStore = @AdStore,
			AdStoreUrl = @AdStoreUrl,
			IsOnHome = @IsOnHome,
			RollingNewsId = @RollingNewsId,
			TagSubTitleId = @TagSubTitleId,
			LocationType = @LocationType,
			ExpiredDate = @ExpiredDate,
			PrPosition = @PrPosition,
			PrimaryZoneId = @PrimaryZoneId,
			Priority = @Priority,
			ParentNewsId=@ParentNewsId
		WHERE NewsId = @Id
		PRINT 5
		INSERT INTO NewsPublish (NewsId, ZoneId, Title, 
			SubTitle, Sapo, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
			NewsRelation, Source, IsFocus, Type, ThreadId, DistributionDate, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, NewsType, IsOnMobile, 
			Url, TagItem, InitSapo, OriginalUrl, InterviewId, IsBreakingNews, IsPr, AdStore, 
			AdStoreUrl, IsOnHome, RollingNewsId, TagSubTitleId, LastModifiedDate, PublishedDate, 
			IsPrimary, LocationType, ExpiredDate, PrPosition, PrimaryZoneId, TitleDetail, Priority, ParentNewsId)
		SELECT @Id, ZoneId, @ShortTitle, 
			@SubTitle, @Sapo, @Avatar, @AvatarDesc, @Avatar2, @Avatar3, @Avatar4, @Avatar5, @Author, 
			@NewsRelation, @Source, @IsFocus, @Type, @ThreadId, @DistributionDate, @DisplayStyle, 
			@DisplayPosition, @DisplayInSlide, @AvatarCustom, @OriginalId, @NewsType, @IsOnMobile, 
			@Url, @TagItem, @InitSapo, @OriginalUrl, @InterviewId, @IsBreakingNews, @IsPr, @AdStore, 
			@AdStoreUrl, @IsOnHome, @RollingNewsId, @TagSubTitleId, GETDATE(), @PublishedDate, 
			0, @LocationType, @ExpiredDate, @PrPosition, @PrimaryZoneId, @Title, @Priority, @ParentNewsId
		FROM @NewsInZoneTemp
		WHERE ZoneId NOT IN (SELECT ZoneId FROM @CurrentNewsInZoneTemp)
		PRINT 6
		UPDATE NewsPublish SET IsPrimary = 1
		WHERE (NewsId = @Id) AND (ZoneId = @PrimaryZoneId)
		PRINT 7
		UPDATE News
		SET [Status] = 8, LastModifiedDate = GETDATE(),
			EditedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE EditedBy END,
			LastModifiedBy = @PublishedBy,
			PublishedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE PublishedBy END,
			DistributionDate = @DistributionDate
		WHERE Id = @Id

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_FileUpload_Insert]    Script Date: 9/18/2018 11:59:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Fox>
-- Create date: <2012-11-15>
-- Edit: chinhnb, 19-09-2018
-- Description:	<Insert new file upload>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_FileUpload_Insert]
	@Id int output,
	@ZoneId int,
	@Title nvarchar(200),
	@Description nvarchar(2000),
	@FileDownloadPath varchar(500),
	@FilePath varchar(500),
	@FileExt varchar(5),
	@FileSize int,
	@UploadedBy nvarchar(50),
	@Status int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		IF(select count(*) from FileUpload where Title=@Title) <= 0
			BEGIN
			INSERT INTO FileUpload (			
				ZoneId,
				Title,
				Description,
				FileDownloadPath,
				FilePath,
				FileExt,
				FileSize,
				UploadedDate,
				UploadedBy,
				LastModifiedDate,
				LastModifiedBy,
				Status
				)
			VALUES (
				@ZoneId,
				@Title,
				@Description,
				@FileDownloadPath,
				@FilePath,
				@FileExt,
				@FileSize,
				GETDATE(),
				@UploadedBy,
				GETDATE(),
				@UploadedBy,
				@Status
				)
			
				SET @Id = SCOPE_IDENTITY()
			END
		ELSE
			SET @Id = 0  
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[CMS_FileUpload_Insert]'+ ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialEmbed]    Script Date: 9/14/2018 2:41:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialEmbed](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RawId] [varchar](36) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[Priority] [bigint] NULL,
	[Status] [int] NULL,
	[OriginalId] [int] NULL,
	[Url] [nvarchar](1000) NULL,
	[Embed] [nvarchar](max) NULL,
 CONSTRAINT [PK_NewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialEmbed] ADD  CONSTRAINT [DF_NewsSocialEmbed_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1=Facebook|2=Tweeter|3=Intagram' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'NewsSocialEmbed', @level2type=N'COLUMN',@level2name=N'OriginalId'
GO

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[NewsSocialContent]    Script Date: 9/14/2018 7:10:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NewsSocialContent](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Sapo] [nvarchar](max) NULL,
	[Body] [ntext] NULL,
	[Avatar] [nvarchar](500) NULL,
	[OriginalId] [int] NULL,
	[OriginalName] [nvarchar](250) NULL,
	[Url] [nvarchar](500) NULL,
	[OriginalUrl] [nvarchar](500) NULL,
	[AvatarCustom] [nvarchar](500) NULL,
	[Avatar1] [nvarchar](500) NULL,
	[Avatar2] [nvarchar](500) NULL,
	[Avatar3] [nvarchar](500) NULL,
	[Status] [int] NULL,
	[Priority] [bigint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [varchar](100) NULL,
	[WordCount] [int] NULL,
	[Note] [nvarchar](2000) NULL,
	[RawId] [varchar](36) NULL,
 CONSTRAINT [PK_NewsSocialContent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_OriginalId]  DEFAULT ((0)) FOR [OriginalId]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[NewsSocialContent] ADD  CONSTRAINT [DF_NewsSocialContent_WordCount]  DEFAULT ((0)) FOR [WordCount]
GO


--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[BoxNewsSocialEmbed]    Script Date: 9/14/2018 4:58:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BoxNewsSocialEmbed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL,
	[NewsId] [bigint] NOT NULL,
	[SortOrder] [int] NULL,
	[Type] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_BoxNewsSocialEmbed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_GetListByZone]    Script Date: 9/14/2018 8:01:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
CREATE PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_GetListByType]
@Type int,
@ZoneId int
AS
BEGIN
	SELECT TOP(20) BN.NewsId,BN.SortOrder,BN.Type,NC.Avatar,NC.Title, BN.ZoneId
	FROM BoxNewsSocialEmbed as BN 
	inner join NewsSocialContent as NC on BN.NewsId =NC.Id
	where BN.Type=@Type And BN.ZoneId=@ZoneId
	ORDER BY BN.SortOrder ASC
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxNewsSocialEmbed_Update]    Script Date: 9/14/2018 8:13:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-14
CREATE PROCEDURE [dbo].[CMS_BoxNewsSocialEmbed_UpdateListId] 
	@ListOfPriority nvarchar(MAX),	
	@Type int,
	@ZoneId int
AS
BEGIN
    DELETE BoxNewsSocialEmbed WHERE Type=@Type and ZoneId=@ZoneId
	DECLARE @Priority int, 
			@NewsId bigint
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	
	FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM BoxNewsSocialEmbed WHERE NewsId = @NewsId and Type =@Type and ZoneId=@ZoneId)
			INSERT INTO BoxNewsSocialEmbed (NewsId,SortOrder,Type, ZoneId, LastModifiedDate) values(@NewsId,@Priority,@Type,@ZoneId,DATEADD(second, @Priority, GETDATE()))
			FETCH NEXT FROM PriorityCursor INTO @Priority, @NewsId 
	END

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_DeleteById]    Script Date: 9/14/2018 8:42:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Toggle Status NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_ToggleStatus]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		Update NewsSocialEmbed SET [Status] = CASE WHEN [Status]=1 THEN 0 ELSE 1 END WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_DeleteById]    Script Date: 9/14/2018 8:42:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Toggle Status NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_ToggleStatus]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		Update NewsSocialContent SET [Status] = CASE WHEN [Status]=1 THEN 0 ELSE 1 END WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_DeleteById]    Script Date: 9/14/2018 2:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Delete NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM NewsSocialContent WHERE Id = @Id

		DELETE FROM BoxNewsSocialEmbed WHERE NewsId = @Id

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocial_GetById]    Script Date: 9/14/2018 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (Id = @Id)
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Insert]
	@Id BIGINT OUTPUT
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    ,@OriginalId int
    ,@OriginalName nvarchar(250)
    ,@Url nvarchar(500)
    ,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    ,@Status int
    ,@Priority bigint
    ,@CreatedDate datetime
    ,@CreatedBy varchar(100)
    --,@UpdatedDate datetime
    --,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)
    ,@RawId varchar(36)
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialContent (
		[Title]
      ,[Sapo]
      ,[Body]
      ,[Avatar]
      ,[OriginalId]
      ,[OriginalName]
      ,[Url]
      ,[OriginalUrl]
      ,[AvatarCustom]
      ,[Avatar1]
      ,[Avatar2]
      ,[Avatar3]
      ,[Status]
      ,[Priority]
      ,[CreatedDate]
      ,[CreatedBy]      
      ,[WordCount]
      ,[Note]
      ,[RawId]
	  )
		VALUES (
		@Title
		,@Sapo
		,@Body
		,@Avatar
		,@OriginalId
		,@OriginalName
		,@Url
		,@OriginalUrl
		,@AvatarCustom
		,@Avatar1
		,@Avatar2
		,@Avatar3
		,@Status
		,@Priority
		,@CreatedDate
		,@CreatedBy		
		,@WordCount
		,@Note
		,@RawId)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Search]    Script Date: 9/14/2018 6:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Search NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Search] --'', 0, 1, 20, 0	
	@Keyword nvarchar(500)='',
	@Status int=-1,	
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Title) > 0 )'
	IF @Status > -1
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM NewsSocialContent AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM NewsSocialContent AS T '+ @where + ' '	
	SET @search = @search + ') AS NewsSocialContentTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,								
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Update]
	@Id BIGINT	
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    --,@OriginalId int
    --,@OriginalName nvarchar(250)
    --,@Url nvarchar(500)
    --,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    --,@Status int
    ,@Priority bigint
    --,@CreatedDate datetime
    --,@CreatedBy varchar(100)
    ,@UpdatedDate datetime
    ,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)    
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialContent
		SET Title = @Title, 			
			Sapo = @Sapo, 
			Body = @Body,
			Avatar=@Avatar,
			AvatarCustom=@AvatarCustom,
			Avatar1=@Avatar1,
			Avatar2=@Avatar2,
			Avatar3=@Avatar3,
			Priority=@Priority,
			UpdatedDate=@UpdatedDate,
			UpdatedBy=@UpdatedBy,
			WordCount=@WordCount,
			Note=@Note
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_GetById]    Script Date: 9/15/2018 10:07:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialContent by rawid>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_GetByRawId]
	@RawId varchar(36)
AS
BEGIN
	SELECT *
	FROM NewsSocialContent
	WHERE (RawId = @RawId)
END


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_DeleteById]    Script Date: 9/14/2018 2:42:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Delete NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_DeleteById]
	@Id BIGINT
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY		
		DELETE FROM NewsSocialEmbed WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocial_GetById]    Script Date: 9/14/2018 2:43:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialEmbed by id>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_GetById]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM NewsSocialEmbed
	WHERE (Id = @Id)
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Insert]    Script Date: 9/14/2018 2:44:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Insert new NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Insert]
	@Id BIGINT OUTPUT,
	@RawId varchar(36),
	@CreatedBy varchar(100) = '',
	@CreatedDate datetime,
	@Priority bigint,	
	@Status int,		
	@OriginalId int,
	@Url nvarchar(1000) = '',
	@Embed nvarchar(max) = ''
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
							
		INSERT INTO NewsSocialEmbed (RawId, CreatedBy, CreatedDate, [Priority],[Status], OriginalId, [Url], Embed)
		VALUES (@RawId,@CreatedBy,@CreatedDate,@Priority,@Status,@OriginalId,@Url, @Embed)
			
		SET @Id = SCOPE_IDENTITY();					
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_Search]    Script Date: 9/14/2018 6:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Search NewsSocialEmbed>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_Search] --0, 1, 20, 0	
	@Status int=-1,	
	@PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUT
AS
BEGIN
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS	
	SET @params =			'@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'	
	IF @Status > -1
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.CreatedDate DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM NewsSocialEmbed AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM NewsSocialEmbed AS T '+ @where + ' '	
	SET @search = @search + ') AS NewsSocialEmbedTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	
	EXEC SP_EXECUTESQL  @search, @params,								
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialContent_Update]    Script Date: 9/14/2018 2:46:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Update NewsSocialContent>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialContent_Update]
	@Id BIGINT	
	,@Title nvarchar(500)
    ,@Sapo nvarchar(max)
    ,@Body ntext
    ,@Avatar nvarchar(500)
    --,@OriginalId int
    --,@OriginalName nvarchar(250)
    --,@Url nvarchar(500)
    --,@OriginalUrl nvarchar(500)
    ,@AvatarCustom nvarchar(500)
    ,@Avatar1 nvarchar(500)
    ,@Avatar2 nvarchar(500)
    ,@Avatar3 nvarchar(500)
    --,@Status int
    ,@Priority bigint
    --,@CreatedDate datetime
    --,@CreatedBy varchar(100)
    ,@UpdatedDate datetime
    ,@UpdatedBy varchar(100)
    ,@WordCount int
    ,@Note nvarchar(2000)    
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		
		UPDATE NewsSocialContent
		SET Title = @Title, 			
			Sapo = @Sapo, 
			Body = @Body,
			Avatar=@Avatar,
			AvatarCustom=@AvatarCustom,
			Avatar1=@Avatar1,
			Avatar2=@Avatar2,
			Avatar3=@Avatar3,
			Priority=@Priority,
			UpdatedDate=@UpdatedDate,
			UpdatedBy=@UpdatedBy,
			WordCount=@WordCount,
			Note=@Note
		WHERE (Id = @Id)
					
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END



--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsSocialEmbed_GetById]    Script Date: 9/15/2018 10:07:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-09-14>
-- Description:	<Get NewsSocialEmbed by rawid>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsSocialEmbed_GetByRawId]
	@RawId varchar(36)
AS
BEGIN
	SELECT *
	FROM NewsSocialEmbed
	WHERE (RawId = @RawId)
END


--USE [IMS_VTV]
GO

/****** Object:  Table [dbo].[VideoAutoDownloadMonitor]    Script Date: 9/15/2018 6:24:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoAutoDownloadMonitor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NewsId] [bigint] NULL,
	[SourceUrl] [varchar](300) NULL,
	[SaveFilePath] [varchar](300) NULL,
	[KeyVideo] [varchar](100) NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[SentDownloadRequest] [datetime] NULL,
	[DownloadCompleted] [datetime] NULL,
	[UpdateDatabase] [datetime] NULL,
	[CounterCheckDownLoad] [int] NULL,
 CONSTRAINT [PK_VideoAutoDownloadMonitor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_VINFAST]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]    Script Date: 9/28/2018 10:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit:28-09-2018
CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_UpdateStatus]
	@Ids nvarchar(1000),
	@Status INT
AS
BEGIN
	IF(@Status = 3)
		UPDATE VideoAutoDownloadMonitor SET Status = @Status, UpdateDatabase = GETDATE()
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(bigint,part))
	ELSE
		UPDATE VideoAutoDownloadMonitor SET Status = @Status
		WHERE  EXISTS(SELECT VideoAutoDownloadMonitor.NewsId FROM CmsDL_fSplitString(@Ids,',') WHERE VideoAutoDownloadMonitor.NewsId = CONVERT(bigint,part))
END



--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_Update]    Script Date: 9/15/2018 7:28:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_Update]
    @Id INT OUT ,
    @NewsId bigint ,
    @SourceUrl VARCHAR(300) ,
    @Status INT
AS 
    IF NOT EXISTS ( SELECT  1
                    FROM    [VideoAutoDownloadMonitor]
                    WHERE   NewsId = @NewsId ) 
        BEGIN
            INSERT  INTO [dbo].[VideoAutoDownloadMonitor]
                    ( [NewsId] ,
                      [SourceUrl] ,
                      [Status] ,
                      [CreatedDate] 
                    )
            VALUES  ( @NewsId ,
                      @SourceUrl ,
                      @Status ,
                      GETDATE()
                    )
            SELECT  @Id = @@IDENTITY
        END
    
    ELSE 
        BEGIN
            UPDATE  [dbo].[VideoAutoDownloadMonitor]
            SET     [SourceUrl] = @SourceUrl ,
                    [Status] = @Status
            WHERE   NewsId = @NewsId
            SELECT  @Id = Id
            FROM    [VideoAutoDownloadMonitor]
            WHERE   NewsId = @NewsId
        END



--USE [IMS_VTV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_GetByInterview]    Script Date: 9/15/2018 7:25:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_GetByNewsId] @NewsId bigint
AS 
    SELECT  *
    FROM    VideoAutoDownloadMonitor
    WHERE   NewsId = @NewsId


--USE [IMS2_VINFAST]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]    Script Date: 9/28/2018 10:34:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit:28-09-2018
CREATE PROCEDURE [dbo].[CMS_VideoAutoDownloadMonitor_DownloadCompleted]
	@NewsId bigint,
	@VideoInfo nvarchar(1000),
	@SaveFilePath nvarchar(500),
	@KeyVideo nvarchar(500)
AS
BEGIN
	 UPDATE VideoAutoDownloadMonitor SET Status = 2,DownloadCompleted = GETDATE(), KeyVideo = @KeyVideo, SaveFilePath = @SaveFilePath
	 WHERE NewsId = @NewsId;
	 
	 --UPDATE Interview SET VideoInfo = @VideoInfo WHERE Id = @InterviewId;
END

GO

alter table Magazine
add [Url] nvarchar(500),FilePath nvarchar(500)

Go

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Insert]    Script Date: 9/18/2018 2:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Insert]
	@Title nvarchar(200),
	@Avatar nvarchar(500),
	@Url nvarchar(500),
	@FilePath nvarchar(500),
	@Type int,	
	@PublishedDate datetime,
	@CreatedDate datetime,
	@CreatedBy nvarchar(50),
	@LastModifiedDate datetime,
	@LastModifiedBy nvarchar(50),
	@Status int,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO Magazine (Title, Avatar, [Url], [FilePath], [Type], [PublishedDate],
		CreatedDate, CreatedBy, LastModifiedBy, LastModifiedDate, [Status])
	VALUES (@Title,@Avatar, @Url, @FilePath, @Type, @PublishedDate, 
		GETDATE(), @CreatedBy, @CreatedBy, GETDATE(), @Status)
		
	SET @Id = SCOPE_IDENTITY()
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Update]    Script Date: 9/18/2018 2:51:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Update]
	@Title nvarchar(200),
	@Avatar nvarchar(500),
	@Url nvarchar(500),
	@FilePath nvarchar(500),
	@Type int,	
	@PublishedDate datetime,	
	@LastModifiedDate datetime,
	@LastModifiedBy nvarchar(50),
	@Status int,
	@Id int
AS
BEGIN
	UPDATE Magazine 
	SET Title = @Title, 
		Avatar = @Avatar, 
		[Url] = @Url, 
		FilePath = @FilePath, 
		[Type] = @Type, 
		PublishedDate=@PublishedDate,
		LastModifiedBy = @LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		[Status] = @Status
	WHERE (Id = @Id)	
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_GetbyId]    Script Date: 9/18/2018 2:58:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_GetbyId]
	@Id int
AS
BEGIN
	SELECT * 
	FROM Magazine
	WHERE Id = @Id
END


--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Search]    Script Date: 9/18/2018 2:59:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Search]
	@Keyword nvarchar(300),
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int OUTPUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	-- WHERE
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, Title) > 0)'
		
	IF @Status > 0
		SET @where = @where + ' AND (Status = @Status)'
		
	IF @Type > 0
		SET @where = @where + ' AND (Type = @Type)'
	
	SET @order = ' ORDER BY CreatedDate DESC'
		
	-- SELECT TotalRow count
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Magazine ' + @where + ';'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP ' + CONVERT(varchar(5), @PageSize) + ' Id, Title, Avatar, Url, FilePath, PublishedDate,'
		SET @search = @search + '	Type, Status, CreatedDate, CreatedBy, LastModifiedDate, LastModifiedBy '
		SET @search = @search + ' FROM Magazine '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('		
		SET @search = @search + ' SELECT Id, Title, Avatar, Url, FilePath, PublishedDate,'
		SET @search = @search + '	Type, Status, CreatedDate, CreatedBy, LastModifiedDate, LastModifiedBy, '
		SET @search = @search + '	ROW_NUMBER() OVER (' + @order + ') AS RowNum '
		SET @search = @search + ' FROM Magazine '
		SET @search = @search + @where
		SET @search = @search + ' ) AS MagazineTempTable'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
	
	EXEC SP_EXECUTESQL  @search,@params, 
								@Keyword,
								@Status,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


GO

alter table NewsConfig
add ConfigGroup nvarchar(200),LastModifiedDate datetime

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_SetValueAndLabelByType]    Script Date: 9/20/2018 2:57:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <2018-09-20>
-- Description:	<set value and label for news config>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_NewsConfig_Update]
	@ConfigName varchar(200),
	@ConfigValue ntext,
	@ConfigLabel nvarchar(200),
	@ConfigType int,
	@ConfigInitValue varchar(max),
	@ConfigGroupKey varchar(50),
	@ConfigGroup nvarchar(200)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM NewsConfig WHERE ConfigName = @ConfigName)
		UPDATE NewsConfig
		SET ConfigValue = @ConfigValue,
			ConfigLabel = @ConfigLabel,
			ConfigValueType = @ConfigType,
			ConfigInitValue=@ConfigInitValue,
			ConfigGroupKey=@ConfigGroupKey,
			ConfigGroup=@ConfigGroup,
			LastModifiedDate=getdate()
		WHERE ConfigName = @ConfigName
	ELSE
		INSERT INTO NewsConfig (ConfigName,ConfigValue, ConfigValueType, ConfigLabel, ConfigInitValue, ConfigGroupKey, ConfigGroup)
		VALUES (@ConfigName, @ConfigValue, @ConfigType, @ConfigLabel, @ConfigInitValue, @ConfigGroupKey, @ConfigGroup)
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetAll]    Script Date: 9/20/2018 4:03:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edit: chinhnb 20-09-2018
-- Description:	<Get all news config>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetAll]
@PageIndex INT = 1 ,
@PageSize INT = 10 ,
@TotalRow INT = 0 OUT
AS
BEGIN
	SELECT @TotalRow = COUNT(*) FROM NewsConfig

	SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY LastModifiedDate DESC ) AS RowNum
                  FROM      NewsConfig                  
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByConfigName]    Script Date: 9/20/2018 3:50:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb 20-09-2018
-- Create date: <2012-10-23>
-- Description:	<Get news config by config name>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByConfigName]
	@ConfigName varchar(200)
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE (ConfigName = @ConfigName)
END

--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsConfig_GetByGroupConfigKey]    Script Date: 9/20/2018 3:48:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-10-23>
-- Edite: chinhnb 20-09-2018
-- Description:	<Get news config by GroupConfigKey>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsConfig_GetByGroupConfigKey]
	@ConfigGroupKey varchar(50),
	@ConfigType int
AS
BEGIN
	SELECT *
	FROM NewsConfig
	WHERE ConfigGroupKey LIKE @ConfigGroupKey AND ((@ConfigType <= 0) OR (@ConfigType > 0 AND ConfigValueType = @ConfigType))
END

