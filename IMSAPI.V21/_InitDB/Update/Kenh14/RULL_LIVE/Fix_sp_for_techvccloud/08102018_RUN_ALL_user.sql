
GO

ALTER table UserProfile
ADD StaffCode varchar(20)

GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate())
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE()
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: Chinhnb<23-07-2018>
-- Create date: <2012-09-24>
-- Description:	<get user information by username>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserByUsername]
	@UserName varchar(255)
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem, P.StaffCode
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE LOWER(U.UserName) = LOWER(@UserName)
END


GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END


-------------------------------------------------2-------------


GO
ALTER TABLE [User]
ADD TelegramId bigint, DepartmentId int


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_Addnew]    Script Date: 10/8/2018 4:44:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass,TelegramId,DepartmentId)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate(),@TelegramId,@DepartmentId)
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateById]    Script Date: 10/8/2018 5:02:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE(),
				TelegramId=@TelegramId,
				DepartmentId=@DepartmentId
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_GetUserById]    Script Date: 10/8/2018 5:04:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode,U.TelegramId,U.DepartmentId
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END


