--USE [IMS2_AFAMILY_BETA]
GO

/****** Object:  Table [dbo].[VideoInChannel]    Script Date: 11/10/2018 1:25:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoInChannel](
	[VideoId] [int] NOT NULL,
	[ChannelId] [int] NOT NULL,
	[LastModifledDate] [datetime] NULL,
 CONSTRAINT [PK_VideoInChannel] PRIMARY KEY CLUSTERED 
(
	[VideoId] ASC,
	[ChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_AFAMILY_BETA]
GO

/****** Object:  Table [dbo].[VideoChannel]    Script Date: 11/10/2018 1:23:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoChannel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ZoneId] [int] NULL,
	[PublisherId] [int] NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Avatar] [varchar](500) NULL,
	[Priority] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[Url] [nvarchar](500) NULL,
	[FollowCount] [int] NULL,
	[VideoCount] [int] NULL,
	[Rank] [int] NULL,
	[IntroClip] [nvarchar](4000) NULL,
	[Status] [int] NULL,
	[LastModifiedBy] [varchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
	[Cover] [nvarchar](500) NULL,
	[Description] [nvarchar](max) NULL,
	[ChannelRelation] [nvarchar](max) NULL,
	[MetaJson] [nvarchar](max) NULL,
	[Mode] [int] NULL,
	[MetaAvatar] [nvarchar](max) NULL,
 CONSTRAINT [PK_VideoChannel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[VideoChannel] ADD  CONSTRAINT [DF_VideoChannel_FollowCount]  DEFAULT ((0)) FOR [FollowCount]
GO

ALTER TABLE [dbo].[VideoChannel] ADD  CONSTRAINT [DF_VideoChannel_VideoCount]  DEFAULT ((0)) FOR [VideoCount]
GO

ALTER TABLE [dbo].[VideoChannel] ADD  CONSTRAINT [DF_VideoChannel_Rank]  DEFAULT ((0)) FOR [Rank]
GO

ALTER TABLE [dbo].[VideoChannel] ADD  CONSTRAINT [DF_VideoChannel_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[VideoChannel] ADD  CONSTRAINT [DF_VideoChannel_Mode_1]  DEFAULT ((0)) FOR [Mode]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Thong thuong, 1: Noi bat' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VideoChannel', @level2type=N'COLUMN',@level2name=N'Mode'
GO

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoChannel_GetVideoChannelByVideoId]    Script Date: 11/10/2018 1:20:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CHINHNB
--09/01/2018
CREATE PROCEDURE  [dbo].[CMS_VideoChannel_GetVideoChannelByVideoId]
	@VideoId int = 0
AS
	SELECT VideoChannel.*
	FROM VideoChannel INNER JOIN VideoInChannel ON Id = ChannelId
	WHERE VideoInChannel.VideoId = @VideoId



--USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoChannel_GetById]    Script Date: 01/03/2018 13:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_VideoChannel_GetById]
@Id int
AS
BEGIN
  SELECT *
  FROM [VideoChannel]
  WHERE Id = @Id
END

--USE [IMS2_AFAMILY_DEV]
GO

ALTER TABLE VideoChannel
Add Mode int,MetaAvatar nvarchar(max)

GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Insert VideoChannel>
-- =============================================
ALTER PROCEDURE  [dbo].[CMS_VideoChannel_Insert]
	@PublisherId int,	
	@Name nvarchar(200),
	@Avatar nvarchar(300),	
	@Description nvarchar(max) = '',
	@Priority int = 0,
	@CreatedBy varchar(50),
	@CreatedDate datetime,
	@Url nvarchar(500)='',
	@FollowCount int = 0,
	@VideoCount int = 0,
	@Rank int = 0,
	@IntroClip nvarchar(500)='',
	@ChannelRelation nvarchar(max)='',	
	@Status bit = 0,	
	@LastModifiedBy varchar(50),
	@LastModifiedDate datetime,	
	
	@ZoneId int,
	@LabelId int,
	@ZoneIdList varchar(500) = '',
	@LabelIdList varchar(500) = '',	
	@PlayListIdList varchar(500) = '',
	@VideoIdList varchar(500) = '',	
	@Cover nvarchar(500),
	
	@MetaJson nvarchar(max)='',
	@Mode int=0,
	@MetaAvatar nvarchar(max)='',
	
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION

    BEGIN		
		INSERT INTO VideoChannel(
							ZoneId,
							PublisherId,
							Name,
							Avatar,						
							[Description],
							Priority,
							CreatedBy,
							CreatedDate,
							Url,
							FollowCount,
							VideoCount,
							[Rank],
							IntroClip,
							ChannelRelation,
							[Status],
							LastModifiedBy,
							LastModifiedDate,
							Cover,
							MetaJson,
							Mode,
							MetaAvatar
							)
					VALUES	(
							@ZoneId,
							@PublisherId,
							@Name,	
							@Avatar,					
							@Description,
							@Priority,
							@CreatedBy,
							@CreatedDate,
							@Url,
							@FollowCount,
							@VideoCount,
							@Rank,
							@IntroClip,
							@ChannelRelation,
							@Status,
							@LastModifiedBy,
							@LastModifiedDate,
							@Cover,
							@MetaJson,
							@Mode,
							@MetaAvatar
							)
						
		SET @Id = SCOPE_IDENTITY();		
		
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert video channel error. Try again or create a other video channel.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN			
			DECLARE @Index int;
			
			-- Process VideoChannelInZone					
			INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
			VALUES (@ZoneId, @Id, @CreatedDate, 1)
			
			DECLARE @TblVideoChannelInZone TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInZone(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInZone)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblVideoChannelInZone WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInZone WHERE ChannelId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
						VALUES (@ZoneIdTemp, @Id, @CreatedDate, 0)
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- Process VideoChannelInLabel					
			INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
			VALUES (@Id, @LabelId, 1)
			
			DECLARE @TblVideoChannelInLabel TABLE(Id int identity(1,1), LabelId int NOT NULL);
			DECLARE @LabelIdTemp int
			IF (LTRIM(RTRIM(@LabelIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInLabel(LabelId)
				SELECT CONVERT(int, part) FROM SplitString(@LabelIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInLabel)) BEGIN
					SET @LabelIdTemp = (SELECT LabelId FROM @TblVideoChannelInLabel WHERE Id = @Index);
					
					IF @LabelIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInLabel WHERE ChannelId = @Id AND LabelId = @LabelIdTemp)
					BEGIN
						INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
						VALUES (@Id, @LabelIdTemp, 0)
					END
					SET @Index = @Index + 1;
				END
			END	
			
			-- Process PlayListInChannel
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblPlayListInChannel TABLE(Id int identity(1,1), PlayListId int NOT NULL);
				DECLARE @PlayListId int;
				
				INSERT @TblPlayListInChannel(PlayListId)
				SELECT CONVERT(int, part) FROM SplitString(@PlayListIdList, ';')
								
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblPlayListInChannel))
				BEGIN
					SET @PlayListId = (SELECT PlayListId FROM @TblPlayListInChannel WHERE Id = @Index)
					
					IF @PlayListId IS NOT NULL AND @PlayListId > 0
					BEGIN
						INSERT PlayListInChannel (PlayListId, ChannelId)
						VALUES (@PlayListId, @Id)
					END												
					SET @Index = @Index + 1;
				END
			END	
			
			-- Process VideoInChannel: chưa xử lý						
		END		
    END
    
IF @@ERROR <> 0
BEGIN
	DECLARE @Message nvarchar(500);
	SET @Message = ERROR_MESSAGE();
	RAISERROR (@Message, 16, 1)
	ROLLBACK TRANSACTION
END ELSE
	COMMIT TRANSACTION


GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Update VideoChannel>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_VideoChannel_Update]
	@PublisherId int,	
	@Name nvarchar(200),
	@Avatar nvarchar(300),	
	@Description nvarchar(max) = '',
	@Priority int = 0,
	@CreatedBy varchar(50),
	@CreatedDate datetime,
	@Url nvarchar(500)='',
	@FollowCount int = 0,
	@VideoCount int = 0,
	@Rank int = 0,
	@IntroClip nvarchar(500)='',
	@ChannelRelation nvarchar(max)='',	
	@Status bit = 0,	
	@LastModifiedBy varchar(50),
	@LastModifiedDate datetime,	
	
	@ZoneId int,
	@LabelId int,
	@ZoneIdList varchar(500) = '',
	@LabelIdList varchar(500) = '',	
	@PlayListIdList varchar(500) = '',
	@VideoIdList varchar(500) = '',
	@Cover nvarchar(500),	
	
	@MetaJson nvarchar(max)='',
	@Mode int=0,
	@MetaAvatar nvarchar(max)='',
	
    @Id int
AS
	BEGIN TRANSACTION
	
    IF (@Id <= 0 OR NOT EXISTS (SELECT Id FROM VideoChannel WHERE Id = @Id))
		BEGIN
			RAISERROR ('Can not found this videochannel.', 16, 1);
			ROLLBACK TRANSACTION
		END
    ELSE
		BEGIN						
			UPDATE VideoChannel
			SET
				ZoneId=@ZoneId,
				PublisherId=@PublisherId,
				Name=@Name,
				Avatar=@Avatar,						
				[Description]=@Description,
				Priority=@Priority,
				--CreatedBy=@CreatedBy,
				--CreatedDate=@CreatedDate,
				Url=@Url,
				FollowCount=@FollowCount,
				VideoCount=@VideoCount,
				[Rank]=@Rank,
				IntroClip=@IntroClip,
				ChannelRelation=@IntroClip,
				[Status]=@Status,
				LastModifiedBy=@LastModifiedBy,
				LastModifiedDate=@LastModifiedDate,
				Cover=@Cover,
				MetaJson=@MetaJson,
				Mode=@Mode,
				MetaAvatar=@MetaAvatar
			WHERE Id = @Id
			
			DECLARE @Index int;
						
			-- DELETE ALL VideoChannelInZone of this ChannelId
			DELETE FROM VideoChannelInZone  WHERE ChannelId = @Id			
			-- Process VideoChannelInZone					
			INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
			VALUES (@ZoneId, @Id, @CreatedDate, 1)
			
			DECLARE @TblVideoChannelInZone TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInZone(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInZone)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblVideoChannelInZone WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInZone WHERE ChannelId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoChannelInZone(ZoneId, ChannelId, CreatedDate, IsPrimary)
						VALUES (@ZoneIdTemp, @Id, @CreatedDate, 0)
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- DELETE ALL VideoChannelInLabel of this VideoChannelIdChannelId
			DELETE FROM VideoChannelInLabel  WHERE ChannelId = @Id
			-- Process VideoChannelInLabel					
			INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
			VALUES (@Id, @LabelId, 1)
			
			DECLARE @TblVideoChannelInLabel TABLE(Id int identity(1,1), LabelId int NOT NULL);
			DECLARE @LabelIdTemp int
			IF (LTRIM(RTRIM(@LabelIdList)) <> '') BEGIN
				INSERT INTO @TblVideoChannelInLabel(LabelId)
				SELECT CONVERT(int, part) FROM SplitString(@LabelIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoChannelInLabel)) BEGIN
					SET @LabelIdTemp = (SELECT LabelId FROM @TblVideoChannelInLabel WHERE Id = @Index);
					
					IF @LabelIdTemp IS NOT NULL AND NOT EXISTS (SELECT ChannelId  FROM VideoChannelInLabel WHERE ChannelId = @Id AND LabelId = @LabelIdTemp)
					BEGIN
						INSERT INTO VideoChannelInLabel(ChannelId, LabelId, IsPrimary)
						VALUES (@Id, @LabelIdTemp, 0)
					END
					SET @Index = @Index + 1;
				END
			END	
			
			-- DELETE ALL PlayListInChannel of this VideoChannelIdChannelId
			DELETE FROM PlayListInChannel  WHERE ChannelId = @Id
			-- Process PlayListInChannel
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblPlayListInChannel TABLE(Id int identity(1,1), PlayListId int NOT NULL);
				DECLARE @PlayListId int;
				
				INSERT @TblPlayListInChannel(PlayListId)
				SELECT CONVERT(int, part) FROM SplitString(@PlayListIdList, ';')
								
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblPlayListInChannel))
				BEGIN
					SET @PlayListId = (SELECT PlayListId FROM @TblPlayListInChannel WHERE Id = @Index)
					
					IF @PlayListId IS NOT NULL AND @PlayListId > 0
					BEGIN
						INSERT PlayListInChannel (PlayListId, ChannelId)
						VALUES (@PlayListId, @Id)
					END												
					SET @Index = @Index + 1;
				END
			END	
			
			-- Process VideoInChannel: chưa xử lý	
			
		END
		      
IF @@ERROR <> 0
BEGIN
	DECLARE @Message nvarchar(500);
	SET @Message = ERROR_MESSAGE();
	RAISERROR (@Message, 16, 1)
	ROLLBACK TRANSACTION
END ELSE
	COMMIT TRANSACTION
