--USE [IMS2_AFAMILY_BETA]
GO

/****** Object:  Table [dbo].[VideoPosition]    Script Date: 11/10/2018 1:32:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoPosition](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ZoneId] [int] NULL,
	[TypeId] [int] NULL,
	[Order] [int] NULL,
	[Lockable] [bit] NULL,
	[Locked] [bit] NULL,
	[ExpiredLock] [datetime] NULL,
	[Name] [nvarchar](250) NULL,
	[Avatar] [nvarchar](500) NULL,
	[Title] [nvarchar](250) NULL,
	[VideoId] [bigint] NULL,
	[AvatarDesc] [nvarchar](500) NULL,
	[Avatar1] [nvarchar](500) NULL,
	[Avatar2] [nvarchar](500) NULL,
	[Avatar3] [nvarchar](500) NULL,
	[Avatar4] [nvarchar](500) NULL,
	[Avatar5] [nvarchar](500) NULL,
	[Url] [nvarchar](500) NULL,
	[OriginalUrl] [nvarchar](500) NULL,
	[Source] [nvarchar](100) NULL,
	[IsFocus] [bit] NULL,
	[DistributionDate] [datetime] NULL,
	[AllowAutoUpdate] [bit] NOT NULL,
	[DisplayStyle] [int] NULL,
	[DisplayPosition] [int] NULL,
	[DisplayInSlide] [int] NULL,
	[AvatarCustom] [varchar](500) NULL,
	[LastModifiedDate] [datetime] NULL,
	[Position] [int] NULL,
	[Type] [int] NULL,
	[PlayListIdForVideo] [int] NULL,
	[Priority] [int] NULL,
	[ParentVideoId] [bigint] NULL,
	[LastModifiedDateStamp] [bigint] NULL,
	[PublishedDateStamp] [bigint] NULL,
	[LocationType] [int] NULL,
	[ExpiredDate] [datetime] NULL,
	[ScheduleDate] [datetime] NULL,
	[IsOnHome] [bit] NULL,
	[Duration] [nvarchar](50) NULL,
	[VideoRelation] [nvarchar](max) NULL,
 CONSTRAINT [PK_VideoPosition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_ZoneId]  DEFAULT ((0)) FOR [ZoneId]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_Order]  DEFAULT ((0)) FOR [Order]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_Lockable]  DEFAULT ((0)) FOR [Lockable]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_Locked]  DEFAULT ((0)) FOR [Locked]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_IsFocus]  DEFAULT ((0)) FOR [IsFocus]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_AllowAutoUpdate]  DEFAULT ((0)) FOR [AllowAutoUpdate]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF_VideoPosition_DisplayStyle]  DEFAULT ((1)) FOR [DisplayStyle]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__LastM__4381D8D3]  DEFAULT (getdate()) FOR [LastModifiedDate]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosit__Type__456A2145]  DEFAULT ((0)) FOR [Type]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__ZoneI__4475FD0C]  DEFAULT ((0)) FOR [PlayListIdForVideo]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__Prior__465E457E]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__Paren__4B22FA9B]  DEFAULT ((0)) FOR [ParentVideoId]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__LastM__475269B7]  DEFAULT ((0)) FOR [LastModifiedDateStamp]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__Publi__48468DF0]  DEFAULT ((0)) FOR [PublishedDateStamp]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__Locat__493AB229]  DEFAULT ((0)) FOR [LocationType]
GO

ALTER TABLE [dbo].[VideoPosition] ADD  CONSTRAINT [DF__VideoPosi__Sched__4A2ED662]  DEFAULT (getdate()) FOR [ScheduleDate]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-PlayList, 2- Video, 3-Live' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'VideoPosition', @level2type=N'COLUMN',@level2name=N'Type'
GO


