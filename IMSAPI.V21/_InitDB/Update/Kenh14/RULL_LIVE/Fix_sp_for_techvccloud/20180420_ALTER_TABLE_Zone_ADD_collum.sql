--USE [IMS2_AFAMILY_DEV]

-------------------------ADD COLLUM FOR TABLE ZONE----------------------------
GO
--chinhnb
--20/04/2018
ALTER TABLE Zone
ADD Avatar varchar(300),
	AvatarCover varchar(300),
	Logo varchar(300),	
	MetaAvatar varchar(max);
	
	
--------------------------CMS_Zone_Insert-------------------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Insert]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max)
AS
	BEGIN TRANSACTION CMS_Zone_Insert
	
	BEGIN TRY
		IF @Name IS NOT NULL AND @Name <> ''
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment,
				[Avatar]
				,[AvatarCover]
				,[Logo]				
				,[MetaAvatar]
			)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment,
				@Avatar
				,@AvatarCover
				,@Logo			
				,@MetaAvatar
			)
			COMMIT TRANSACTION CMS_Zone_Insert
		END
		ELSE BEGIN
			RAISERROR ('Invalid Zone Name', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Insert
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Insert
	END CATCH
	
	
-------------------------------CMS_Zone_Update-------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Update]
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(100),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@AllowComment bit = 1,
	@Domain nvarchar(300),
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max),
	@Id int
AS
	BEGIN TRANSACTION CMS_Zone_Update
	
	BEGIN TRY
		IF EXISTS (SELECT Id FROM Zone WHERE Id = @Id)
		BEGIN
			UPDATE Zone
			SET
				Name = @Name, 
				Description = @Description, 
				ShortURL = @ShortURL, 
				SortOrder = @SortOrder, 
				ParentId = @ParentId, 
				Invisibled = @Invisibled, 
				Status = @Status,
				ModifiedDate = GETDATE(),
				AllowComment = @AllowComment,
				Domain=@Domain,
				Avatar=@Avatar,
				AvatarCover=@AvatarCover,
				Logo=@Logo,
				MetaAvatar=@MetaAvatar
			WHERE Id = @Id
			COMMIT TRANSACTION CMS_Zone_Update
		END
		ELSE BEGIN
			RAISERROR ('This zone does not existed in database.', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Update
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Update
	END CATCH
		
-----------------------------------CMS_Zone_GetByParentId--------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-26>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE (ParentId = @ParentId) ORDER BY SortOrder asc
END

-----------------------------------------CMS_Zone_GetById---------------------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-24>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetById]
	@Id int
AS
BEGIN
	SELECT * FROM Zone	WHERE (Id = @Id)
END

---------------------------------------CMS_Zone_Active_GetByParentId------------------------
GO
-- =============================================
-- Author:		longlh
-- Create date: 04/09/2014
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id with status=1>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_Active_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone WHERE Status=1  ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE ParentId = @ParentId AND Status=1 ORDER BY SortOrder asc
END

GO
---------------------------------------