
GO
ALTER TABLE Video
Add ParentId int, HashId varchar(50),[Location] varchar(50),PolicyContentId varchar(50),TrailerUrl nvarchar(2000), MetaAvatar nvarchar(max), DisplayStyle nvarchar(max)


GO
--chinhhb
--Created: 07/06/2018
CREATE PROCEDURE [dbo].[CMS_VideoAuthor_GetListAuthorInVideo]
	@VideoId bigint
AS
	SELECT     va.VideoId, va.AuthorId, va.AuthorName, va.Note, a.Avatar, a.AuthorTitle, a.AuthorType
	FROM         VideoByAuthor va
	INNER JOIN	NewsAuthor a ON va.AuthorId=a.Id
	WHERE     (VideoId = @VideoId)


GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Update VideoChannel>
-- =============================================
ALTER PROCEDURE  [dbo].[VideoCms_Video_InsertV3]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100) = '',
	@Pname varchar(100) = '',
	@Status int = 0,
	@FileName varchar(500),
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@CreatedBy varchar(255),
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@OriginalUrl nvarchar(500) = '',
	@Source nvarchar(200) = '',
	@TagIdList varchar(300) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	@ChannelIdList varchar(500) = '',
	@Duration varchar(50),
	@Size varchar(20),
	@Capacity int,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	@OriginalId int = 0,
	@Author nvarchar(205) = '',
	@ParentId int,
	@HashId varchar(50) = '',
	@Type int,
	@TrailerUrl nvarchar(500) = '',
	@MetaAvatar nvarchar(max) = '',
	@Location varchar(50) = '',
	@PolicyContentId varchar(50) = '',
	@DisplayStyle nvarchar(max) = '',
	-- INSERT INTO VideoByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION
	
    IF (EXISTS (SELECT Id FROM Video WHERE	KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0))
	BEGIN
		SELECT @Id = Id FROM Video WHERE KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0		
	END ELSE
    BEGIN
		DECLARE @ParentPrimaryZoneId int
		SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
		IF @ParentPrimaryZoneId > 0
		BEGIN
			IF @ZoneIdList <> ''
				SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
			ELSE
				SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
		END
		ELSE
		BEGIN
			DECLARE @ChildZoneId int
			SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
			IF @ChildZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
				SET @ZoneId = @ChildZoneId
			END
		END

		INSERT	INTO Video(	ZoneId,
							Name,
							UnsignName,
							[Description],
							HtmlCode,
							Avatar,
							KeyVideo,
							Pname,
							[Status],
							[FileName],
							NewsId,
							Views,
							Mode,
							Tags,
							CreatedBy,
							LastModifiedBy,
							Url,
							OriginalUrl,
							[Source],
							DistributionDate,
							CreatedDate,
							LastModifiedDate,
							VideoRelation,
							Duration,
							Size,
							Capacity,
							AllowAd,
							IsRemoveLogo,
							AvatarShareFacebook,
							OriginalId,
							Author,
							ParentId,
							HashId,
							[Type],
							TrailerUrl,
							MetaAvatar,
							Location,
							PolicyContentId,
							DisplayStyle
							)
					VALUES	(
							@ZoneId,
							@Name,
							@UnsignName,
							@Description,
							@HtmlCode,
							@Avatar,
							@KeyVideo,
							@Pname,
							@Status,
							@FileName,
							@NewsId,
							@Views,
							@Mode,
							@Tags,
							@CreatedBy,
							@CreatedBy,
							@Url,
							@OriginalUrl,
							@Source,
							@DistributionDate,
							GETDATE(),
							GETDATE(),
							@VideoRelationIdList,
							@Duration,
							@Size,
							@Capacity,
							@AllowAd,
							@IsRemoveLogo,
							@AvatarShareFacebook,
							@OriginalId,
							@Author,
							@ParentId,
							@HashId,
							@Type,
							@TrailerUrl,
							@MetaAvatar,
							@Location,
							@PolicyContentId,
							@DisplayStyle
						)
						
		SET @Id = SCOPE_IDENTITY();
		
		UPDATE Video SET Url = REPLACE(Url, '{VideoId}', @Id),OriginalUrl = REPLACE(OriginalUrl, '{VideoId}', @Id) WHERE Id = @Id
		
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert video error. Try again or create a other video.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN
			-- Process Tags
			DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
			DECLARE @Index int,
					@MaxPriority int,
					@TagId int;

				
			IF  @TagIdList <> ''
			BEGIN
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
					
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- Process Playlist
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					
					IF @PlaylistId IS NOT NULL AND @PlayListId > 0
						BEGIN
						select @MaxPriority = Max(Priority) from VideoPlayList WHERE PlayListId = @PlaylistId
						if(@MaxPriority is null)
							set @MaxPriority=0
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @MaxPriority+1)
						END
					SET @Index = @Index + 1
				END
			END
			
			-- Process VideoInChannel			
			IF @ChannelIdList IS NOT NULL AND @ChannelIdList <> ''
			BEGIN
				DECLARE @TblVideoInChannel TABLE(Id int identity(1,1), ChannelId int NOT NULL);
				DECLARE @ChannelId int;
				
				INSERT @TblVideoInChannel(ChannelId)
				SELECT CONVERT(int, part) FROM SplitString(@ChannelIdList, ';')
								
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoInChannel))
				BEGIN
					SET @ChannelId = (SELECT ChannelId FROM @TblVideoInChannel WHERE Id = @Index)
					IF @ChannelId IS NOT NULL AND @ChannelId > 0
					BEGIN
						INSERT VideoInChannel (VideoId, ChannelId, LastModifledDate)
						VALUES (@Id, @ChannelId, GETDATE())
					END
					SET @Index = @Index + 1
				END
			END
			
			-- Insert VideoRelation
			IF @VideoRelationIdList <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), VideoId int)
				DECLARE @VideoItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(VideoId) SELECT CONVERT(int, Part) FROM SplitString(@VideoRelationIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @VideoItemId = (SELECT VideoId FROM @TblTempRelation WHERE Id = @Index)
					IF @VideoItemId IS NOT NULL AND @VideoItemId > 0
						IF NOT EXISTS(SELECT VideoRelationId FROM VideoRelation WHERE VideoRelationId = @VideoItemId AND VideoId = @Id AND ZoneId = @ZoneId)
							INSERT INTO VideoRelation(VideoId, VideoRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @VideoItemId, @ZoneId, 0, @Index);
					SET @Index = @Index + 1
				END
			END
			
			-- insert VideoByAuthor		
			IF @AuthorNameList <> '' AND @AuthorIdList <> '' 
			BEGIN
				DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
				INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
				DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
				INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
				DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
				IF @AuthorNoteList <> ''
					INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
					
				SET @Index = 0;
				DECLARE @NumberRow@TblTempAuthorName int,
						@AuthorId int,
						@AuthorName nvarchar(255),
						@AuthorNote nvarchar(255);
				SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
				
				WHILE(@Index <= @NumberRow@TblTempAuthorName)
				BEGIN
					SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
					SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
					SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
					IF @AuthorId > 0
					BEGIN
						INSERT VideoByAuthor (VideoId, AuthorId, AuthorName, Note)
						VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
					END
					SET @Index = @Index + 1;
				END
			END
						
		END		
    END
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Update VideoChannel>
-- =============================================
ALTER PROCEDURE [dbo].[VideoCms_Video_UpdateV4]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100),
	@Pname varchar(100) = '',
	@Status int = 0,
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@LastModifiedBy varchar(255) = '',
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@Source nvarchar(200) ='',
	@TagIdList varchar(300) = '',
	@TagNameList nvarchar(1000) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	@ChannelIdList varchar(500) = '',
	-- Video File Info 
	@FileName	varchar(500) = '',
	@Duration	varchar(50) = '',
	@Size	varchar(20) = '',
	@Capacity	int = 0,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	@OriginalId int = 0,
	@Author nvarchar(205) = '',
	@ParentId int,
	@HashId	varchar(50) = '',
	@Type int,
	@TrailerUrl nvarchar(500) = '',
	@MetaAvatar nvarchar(max) = '',
	@Location varchar(50) = '',
	@PolicyContentId varchar(50) = '',
	@DisplayStyle nvarchar(max) = '',
	-- INSERT INTO VideoByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
	-- Primary Key
    @Id int
AS
	BEGIN TRANSACTION
	
    IF (@Id <= 0 OR NOT EXISTS (SELECT Id 
					FROM Video 
					WHERE	Id	= @Id))
		BEGIN
			RAISERROR ('Can not found this video.', 16, 1);
			ROLLBACK TRANSACTION
		END
    ELSE
		BEGIN

--			IF @ZoneIdList <> ''
--				SET @ZoneIdList = dbo.CMS_fGetListOfZoneVideoIdIncludeParentZoneVideoId(@ZoneId, @ZoneIdList + ';' + CONVERT(varchar(10), @ZoneId))

			DECLARE @ParentPrimaryZoneId int
			SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
			IF @ParentPrimaryZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
			END
			ELSE
			BEGIN
				DECLARE @ChildZoneId int
				SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
				IF @ChildZoneId > 0
				BEGIN
					IF @ZoneIdList <> ''
						SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
					ELSE
						SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
					SET @ZoneId = @ChildZoneId
				END
			END

			UPDATE Video
			SET
				ZoneId = @ZoneId,
				Name = @Name,
				UnsignName = @UnsignName,
				Description = @Description,
				HtmlCode = @HtmlCode,
				KeyVideo = @KeyVideo,
				Avatar = @Avatar,
				Pname = @Pname,
				Status = @Status,
				NewsId = @NewsId,
				Mode = @Mode,
				Tags = @Tags,
				LastModifiedBy = @LastModifiedBy,
				LastModifiedDate = GETDATE(),
				DistributionDate = @DistributionDate,
				Url = @Url,
				[Source] = @Source,
				VideoRelation = @VideoRelationIdList,
				FileName = @FileName,
				Duration = @Duration,
				Size = @Size,
				Capacity = @Capacity,
				AllowAd = @AllowAd,
				AvatarShareFacebook = @AvatarShareFacebook,
				OriginalId=@OriginalId,
				Author=@Author,
				ParentId=@ParentId,
				HashId=@HashId,
				[Type]=@Type,
				TrailerUrl= @TrailerUrl,
				MetaAvatar=@MetaAvatar,
				Location=@Location,
				PolicyContentId=@PolicyContentId,
				IsRemoveLogo=@IsRemoveLogo,
				DisplayStyle=@DisplayStyle
			WHERE Id = @Id
			
			-- Process Tags
			DECLARE @Index int,
					@MaxPriority int;
							
			-- DELETE ALL Tags of this video
			DELETE FROM VideoInTag WHERE VideoId = @Id						
				
			IF  @TagIdList <> '' AND @TagNameList <> ''
			BEGIN
				DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
				DECLARE @TblTagsList TABLE (Id int identity(1,1), Tag nvarchar(200) NOT NULL);
				DECLARE @TagName nvarchar(200),						
						@TagId int;
						
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
				-- Tag List
				INSERT INTO @TblTagsList (Tag)
				SELECT part FROM SplitString(@TagNameList, ';');
					
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					SET @TagName = (SELECT Tag FROM @TblTagsList WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			-- DELETE ALL VideoInZone of this Video
			DELETE FROM VideoInZone  WHERE VideoId = @Id
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') 
			BEGIN
				DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
				DECLARE @ZoneIdTemp int
			
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
					
			-- Process Playlist
			DELETE FROM VideoPlayList WHERE VideoId = @Id
			
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					IF @PlaylistId IS NOT NULL AND @PlaylistId > 0
					BEGIN
					select @MaxPriority = Max(Priority) from VideoPlayList WHERE PlayListId = @PlaylistId
					if(@MaxPriority is null)
						set @MaxPriority=0
					INSERT VideoPlayList (PlaylistId, VideoId, Priority)
					VALUES (@PlaylistId, @Id, @MaxPriority+1)
					END
					SET @Index = @Index + 1
				END
			END
			
			-- Process VideoInChannel
			DELETE FROM VideoInChannel WHERE VideoId = @Id			
			IF @ChannelIdList IS NOT NULL AND @ChannelIdList <> ''
			BEGIN
				DECLARE @TblVideoInChannel TABLE(Id int identity(1,1), ChannelId int NOT NULL);
				DECLARE @ChannelId int;
				
				INSERT @TblVideoInChannel(ChannelId)
				SELECT CONVERT(int, part) FROM SplitString(@ChannelIdList, ';')
								
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoInChannel))
				BEGIN
					SET @ChannelId = (SELECT ChannelId FROM @TblVideoInChannel WHERE Id = @Index)
					IF @ChannelId IS NOT NULL AND @ChannelId > 0
					BEGIN
						INSERT VideoInChannel (VideoId, ChannelId, LastModifledDate)
						VALUES (@Id, @ChannelId, GETDATE())
					END
					SET @Index = @Index + 1
				END
			END
			
			--Update title v� avatar cho video trong videoposition
			UPDATE VideoPosition set Title=@Name, Avatar= @Avatar WHERE VideoId = @Id and [Type]=2
			
			-- DELETE All VideoRelation
			DELETE FROM VideoRelation WHERE VideoId = @Id
			-- Insert VideoRelation
			IF @VideoRelationIdList <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), VideoId int)
				DECLARE @VideoItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(VideoId) SELECT CONVERT(int, Part) FROM SplitString(@VideoRelationIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @VideoItemId = (SELECT VideoId FROM @TblTempRelation WHERE Id = @Index)
					IF @VideoItemId IS NOT NULL AND @VideoItemId > 0
						IF NOT EXISTS(SELECT VideoRelationId FROM VideoRelation WHERE VideoRelationId = @VideoItemId AND VideoId = @Id AND ZoneId = @ZoneId)
							INSERT INTO VideoRelation(VideoId, VideoRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @VideoItemId, @ZoneId, 0, @Index);
					SET @Index = @Index + 1
				END
			END
			
			-- CALL STORE DELETE ALL VideoByAuthor
			DELETE FROM VideoByAuthor WHERE VideoId = @Id		
			-- insert VideoByAuthor		
			IF @AuthorNameList <> '' AND @AuthorIdList <> '' 
			BEGIN
				DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
				INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
				DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
				INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
				DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
				IF @AuthorNoteList <> ''
					INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
					
				SET @Index = 0;
				DECLARE @NumberRow@TblTempAuthorName int,
						@AuthorId int,
						@AuthorName nvarchar(255),
						@AuthorNote nvarchar(255);
				SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
				
				WHILE(@Index <= @NumberRow@TblTempAuthorName)
				BEGIN
					SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
					SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
					SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
					IF @AuthorId > 0
					BEGIN
						INSERT VideoByAuthor (VideoId, AuthorId, AuthorName, Note)
						VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
					END
					SET @Index = @Index + 1;
				END
			END
			
		END
    
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
		
GO
--chinhnb
--edit: 07/06/2018
ALTER PROCEDURE  [dbo].[VideoCms_Video_Unpublish]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Video WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this video.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN
		UPDATE Video SET [Status] = 2
		WHERE Id = @Id
		
		DECLARE @VideoPositionId int,
				@PositionTypeId int,
				@ZoneId int,
				@Order int
		
		DECLARE PositionCursor CURSOR FOR
		SELECT Id, TypeId, ZoneId, [Order]
		FROM VideoPosition
		WHERE (VideoId = @Id)
		ORDER BY [Order]
		
		OPEN PositionCursor
		
		FETCH NEXT FROM PositionCursor INTO @VideoPositionId, @PositionTypeId, @ZoneId, @Order
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- videonoibattrangchu typeid=8
			IF @PositionTypeId = 8
			BEGIN				
					DECLARE @VideoId bigint,
							@Title nvarchar(250),							
							@Avatar nvarchar(250),							
							@Url nvarchar(500)							
					SELECT TOP(1) @VideoId=v.Id,
							@Title=v.Name,							
							@Avatar=v.Avatar,							
							@Url=v.Url
					FROM Video v					
					WHERE v.Status=1 and v.Id not in(select videoid from VideoPosition where TypeId=8)
						--AND ((@ZoneId = 0) OR (@ZoneId <> 0 AND v.ZoneId = @ZoneId)) 
						AND (v.DistributionDate <= GETDATE()) order by v.DistributionDate desc						
					
					IF @VideoId IS NOT NULL AND @VideoId > 0
					BEGIN
						UPDATE VideoPosition
						SET VideoId = @VideoId,
							Title = @Title,							
							Avatar = @Avatar,							
							LastModifiedDate = GETDATE(),
							ScheduleDate = GETDATE(),
							Url = @Url
						WHERE Id = @VideoPositionId
					END								
			END
		
			FETCH NEXT FROM PositionCursor INTO @VideoPositionId, @PositionTypeId, @ZoneId, @Order
		END
		
		CLOSE PositionCursor   
		DEALLOCATE PositionCursor
		
		--xoa videoid trong videopsition
		--Delete VideoPosition where VideoId=@Id and [Type]=2
		--xoa videoid noibattrangchu=8
		--Delete VideoPosition where VideoId=@Id and [TypeId]=8
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
