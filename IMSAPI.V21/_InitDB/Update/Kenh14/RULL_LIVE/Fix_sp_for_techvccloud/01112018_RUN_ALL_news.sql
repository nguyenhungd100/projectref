﻿--USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Update]    Script Date: 04/11/2018 13:43:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-25>
-- Edited:		<CHINHNB>
-- ModifiedDate: <2018-04-11>
-- Description:	<Update news>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_Update]
	@Id bigint,
	@Title nvarchar(250),
	@SubTitle nvarchar(250),
	@Sapo nvarchar(MAX),
	@Body ntext,
	@Avatar nvarchar(500),
	@AvatarDesc nvarchar(500),
	@Avatar2 nvarchar(500),
	@Avatar3 nvarchar(500),
	@Avatar4 nvarchar(500),
	@Avatar5 nvarchar(500),
	@Author nvarchar(500),
	@NewsRelation nvarchar(max),
	@Status int,
	@Source nvarchar(100),
	@IsFocus bit,
	@Type tinyint,
	@ThreadId int,
	@DistributionDate datetime,
	@LastModifiedBy varchar(200),
	@WordCount int,
	@ViewCount int,
	@Priority tinyint,
	@Tag nvarchar(max),
	@Note nvarchar(2000),
	@TagPrimary nvarchar(250) = '',
	@Price decimal(18,3) = 0,
	@IsOnHome bit = 1,
	@NewsType int = 0,
	@OriginalId int = 0,
	-- Extension fields
	@DisplayStyle int = 0,
	@DisplayPosition int = 0,
	@DisplayInSlide int = 0,
	@AvatarCustom varchar(500) = NULL,
	-- INSERT INTO OTHER TABLES
	@ZoneId int = 0,
	@ZoneIdList varchar(500) = '',
	@TagIdList varchar(500) = '',
	@TagIdListForPrimary varchar(500) = '',
	@NewsRelationIdList varchar(max) = '',
	-- INSERT INTO NewsByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
	-- thanhtn add 2012-12-13
	@TagItem nvarchar(1000),
	@Url nvarchar(500),
	@NoteRoyalties nvarchar(1000),
	@NewsCategory int = 0,
	@InitSapo nvarchar(500) = '',
	-- Insert into NewsBySource table
	@SourceId int = 0,
	@TemplateName varchar(200),
	@TemplateConfig nvarchar(max),
	@IsBreakingNews bit,
	@PegaBreakingNews int = 0,
	@IsPr bit,
	@AdStore bit,
	@AdStoreUrl nvarchar(500),
	@PrBookingNumber varchar(30),
	@IsOnMobile bit = 1,
	@TagSubTitleId int = 0,
	@RollingNewsId int = 0,
	@InterviewId int = 0,
	@LocationType int = 0,
	@ExpiredDate datetime = null,
	@SourceUrl nvarchar(500) = null,
	--BonusPrice
	@BonusPrice decimal(18,3) = 0,
	@ShortTitle nvarchar(250) = '',
	@ApprovedBy varchar(200) = '',
	@ParentNewsId bigint
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @Source = ''
			SELECT @Source = Name FROM NewsSource WHERE Id = @SourceId

		IF @Id=20160704030726973
			SET @Type=27
		
		IF ISNULL((SELECT IsPublish FROM NewsTemp WHERE Id = @Id), 0) = 1 
			SET @Url = Null;
		-- Update News
		UPDATE News
		SET Title = @Title,
			SubTitle = @SubTitle,
			Sapo = @Sapo,
			Body = @Body,
			Avatar = @Avatar,
			AvatarDesc = @AvatarDesc,
			Avatar2 = @Avatar2,
			Avatar3 = @Avatar3,
			Avatar4 = @Avatar4,
			Avatar5 = @Avatar5,
			Author = @Author,
			NewsRelation = @NewsRelation,
			[Status] = @Status,
			[Source] = @Source,
			IsFocus = @IsFocus,
			[Type] = @Type,
			ThreadId = @ThreadId,
			LastModifiedDate = GETDATE(),
			DistributionDate = @DistributionDate,
			LastModifiedBy = @LastModifiedBy,
			WordCount = @WordCount,
			ViewCount = @ViewCount,
			Priority = @Priority,
			Tag = @Tag,
			Note = @Note,
			Price = @Price,
			TagPrimary = @TagPrimary
			,DisplayStyle = @DisplayStyle
			,DisplayPosition = @DisplayPosition
			,DisplayInSlide = @DisplayInSlide
			,AvatarCustom = @AvatarCustom
			,IsOnHome = @IsOnHome
			,NewsType = @NewsType
			,OriginalId = @OriginalId
			,TagItem = @TagItem
			,OriginalUrl = Url
			,Url = ISNULL(@Url, Url)
			,NoteRoyalties = @NoteRoyalties
			,NewsCategory = @NewsCategory
			,InitSapo = @InitSapo
			,TemplateName = @TemplateName
			,IsBreakingNews = @IsBreakingNews
			,PegaBreakingNews = @PegaBreakingNews
			,IsPr = @IsPr
			,AdStore = @AdStore
			,AdStoreUrl = @AdStoreUrl
			,PrBookingNumber = @PrBookingNumber
			,IsOnMobile = @IsOnMobile
			,TagSubTitleId = @TagSubTitleId
			,RollingNewsId = @RollingNewsId
			,InterviewId = @InterviewId
			,LocationType = @LocationType
			,ExpiredDate = @ExpiredDate
			,SourceUrl = @SourceUrl
			,BonusPrice = @BonusPrice
			,ShortTitle = @ShortTitle
			,ApprovedBy = @ApprovedBy
			,ParentNewsId=@ParentNewsId
		WHERE Id = @Id

		SET NOCOUNT ON
		
		DECLARE @Index int;
		
		-- CALL STORE DELETE ALL NewsByAuthor
		DELETE FROM NewsByAuthor WHERE NewsId = @Id
		-- RE INSERT
		IF @AuthorNameList <> '' AND @AuthorIdList <> ''
		BEGIN
			DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
			INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
			DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
			INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
			
			DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
			IF @AuthorNoteList <> ''
				INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
			SET @Index = 0;
			DECLARE @NumberRow@TblTempAuthorName int,
					@AuthorId int,
					@AuthorName nvarchar(255),
					@AuthorNote nvarchar(255);
			SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
			
			WHILE(@Index <= @NumberRow@TblTempAuthorName)
			BEGIN
				SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
				SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
				SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
				IF @AuthorId > 0
				BEGIN
					INSERT NewsByAuthor (NewsId, AuthorId, AuthorName, Note)
					VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
				END
				SET @Index = @Index + 1;
			END
		END
				
		-- DELETE all NewsInZone width NewsId
		DELETE FROM NewsInZone WHERE NewsId = @Id
		
		-- Insert Primary Zone NewsInZone
		INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneId, 1, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneId));
		
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0 AND @ZoneItemId <> @ZoneId
					IF NOT EXISTS(SELECT 1 FROM NewsInZone WHERE ZoneId = @ZoneItemId AND NewsId = @Id)
						INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneItemId, 0, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneItemId));
				SET @Index = @Index + 1
			END
		END		
		
		DELETE FROM TagNews WHERE (TagMode = 0 OR TagMode = 3) AND NewsID = @Id
		
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @Id AND (TagMode = 0 or TagMode = 3))
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemId, 0, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemId AND NewsId = @Id AND (TagMode = 0 or TagMode = 3)
				SET @Index = @Index + 1
			END
		END
		
		DELETE FROM TagNews WHERE TagMode = 1 AND NewsID = @Id
		-- Insert TagNewsForPrimary
		IF @TagIdListForPrimary <> ''
		BEGIN
			DECLARE @TblTempTagForPrimary TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemIdForPrimary bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTagForPrimary(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdListForPrimary, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTagForPrimary)) 
			BEGIN
				SET @TagItemIdForPrimary = (SELECT TagId FROM @TblTempTagForPrimary WHERE Id = @Index)
				IF @TagItemIdForPrimary IS NOT NULL AND @TagItemIdForPrimary > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemIdForPrimary, 1, @Index);
					ELSE
						UPDATE TagNews
						SET Priority = @Index
						WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1
				SET @Index = @Index + 1
			END
		END

		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @Id)
				INSERT INTO ThreadNews (ThreadId, NewsId)
				VALUES (@ThreadId, @Id)
		END
		
		
		-- DELETE All NewsRelation
		DELETE FROM NewsRelation WHERE NewsId = @Id
		-- Insert NewsRelation
		IF @NewsRelationIdList <> ''
		BEGIN
			DECLARE @TblTempRelation TABLE (Id int identity(1,1), NewsId bigint)
			DECLARE @NewsItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempRelation(NewsId) SELECT CONVERT(bigint, Part) FROM SplitString(@NewsRelationIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
			BEGIN
				SET @NewsItemId = (SELECT NewsId FROM @TblTempRelation WHERE Id = @Index)
				IF @NewsItemId IS NOT NULL AND @NewsItemId > 0
					IF NOT EXISTS(SELECT NewsRelationId FROM NewsRelation WHERE NewsRelationId = @NewsItemId AND NewsId = @Id AND ZoneId = @ZoneId)
						INSERT INTO NewsRelation(NewsId, NewsRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @NewsItemId, @ZoneId, 0, @Index);
				SET @Index = @Index + 1
			END
		END

		-- insert NewsBySource 
		DELETE FROM NewsBySource where NewsId = @Id
		IF @SourceId <> 0
		BEGIN
			INSERT NewsBySource (NewsId, SourceId)
		    VALUES (@Id, @SourceId)
		END
		/* Xử lý autotag */
--		DELETE FROM AutoTag WHERE NewsId = @Id
--			
--		INSERT INTO AutoTag (NewsId, IsProcessed)
--		VALUES (@Id, 0)
		
		/***** NANIA Edited: Check them cho an toan ****/
--		IF @Status = 8
--		BEGIN
--			/***** NewsPosition ****/
--			UPDATE NewsPosition 
--				SET Avatar = @Avatar,
--					SubTitle = @SubTitle,
--					Title = @Title,
--					Sapo = @Sapo,
--					InitSapo = @InitSapo,
--					Author = @Author,
--					DisplayStyle = @DisplayStyle,
--					DisplayPosition = @DisplayPosition,
--					Url = @Url,
--					NewsRelation = @NewsRelation
--			WHERE NewsId = @Id
--		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Insert]    Script Date: 04/11/2018 13:37:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-25>
-- Edited:		<CHINHNB>
-- ModifiedDate: <2018-04-11>
-- Description:	<insert news>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_Insert]
	@Id bigint,
	@Title nvarchar(250),
	@SubTitle nvarchar(250),
	@Sapo nvarchar(MAX),
	@Body ntext,
	@Avatar nvarchar(500),
	@AvatarDesc nvarchar(500),
	@Avatar2 nvarchar(500),
	@Avatar3 nvarchar(500),
	@Avatar4 nvarchar(500),
	@Avatar5 nvarchar(500),
	@Author nvarchar(500),
	@NewsRelation nvarchar(max),
	@Source nvarchar(100),
	@IsFocus bit,
	@Type tinyint,
	@ThreadId int,
	@DistributionDate datetime,
	@CreatedBy varchar(200),
	@EditedBy varchar(200),
	@PublishedBy varchar(200),
	@WordCount int,
	@ViewCount int,
	@Priority tinyint,
	@Status int,
	@Tag nvarchar(max),
	@Note nvarchar(2000),
	@TagPrimary nvarchar(250) = '',
	@Price decimal(18,3) = 0,
	@IsOnHome bit = 1,
	@NewsType int = 0,
	@OriginalId int = 0,
	-- Extension fields
	@DisplayStyle int = 0,
	@DisplayPosition int = 0,
	@DisplayInSlide int = 0,
	@AvatarCustom varchar(500) = NULL,
	-- INSERT INTO OTHER TABLES
	@ZoneId int = 0,
	@ZoneIdList varchar(500) = '',
	@TagIdList varchar(500) = '',
	@TagIdListForPrimary varchar(500) = '',
	@NewsRelationIdList varchar(max) = '',
	-- INSERT INTO NewsByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
	-- thanhtn add 2012-12-13
	@TagItem nvarchar(1000),
	@Url nvarchar(500),
	@NoteRoyalties nvarchar(1000),
	@NewsCategory int = 0,
	@InitSapo nvarchar(500) = '',
	-- Insert into NewsBySource table
	@SourceId int = 0,
	@TemplateName varchar(200),
	@TemplateConfig nvarchar(max),
	@IsBreakingNews bit,
	@PegaBreakingNews int = 0,
	@IsPr bit,
	@AdStore bit,
	@AdStoreUrl nvarchar(500),
	@PrBookingNumber varchar(30),
	@IsOnMobile bit = 1,
	@TagSubTitleId int = 0,
	@RollingNewsId int = 0,
	@InterviewId int = 0,
	@LocationType int = 0,
	@ExpiredDate datetime = null,
	@SourceUrl nvarchar(500) = null,
	--BonusPrice
	@BonusPrice decimal(18,3) = 0,
	@LastReceiver nvarchar(200) = '',
	@ShortTitle nvarchar(250) = '',
	@ParentNewsId bigint
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @Source = ''
			SELECT @Source = Name FROM NewsSource WHERE Id = @SourceId

		DECLARE @LastModifiedBy varchar(255);
		SET @LastModifiedBy = @CreatedBy
		IF @Type = 5 SET @LastModifiedBy = @EditedBy
		-- Insert News (Save news with status Temporary = 1)
		INSERT INTO News (Id,Title,SubTitle,Sapo,Body,Avatar,AvatarDesc,Avatar2,Avatar3,Avatar4,Avatar5
			,Author,NewsRelation,[Status],[Source],IsFocus,[Type],ThreadId,CreatedDate,LastModifiedDate
			,DistributionDate,CreatedBy,LastModifiedBy,PublishedBy,EditedBy,LastReceiver,WordCount,ViewCount
			,Priority,Tag, Note, TagPrimary, Price,DisplayStyle, DisplayPosition, DisplayInSlide, AvatarCustom
			,NewsType, IsOnHome, OriginalId, TagItem, Url, NoteRoyalties, NewsCategory, InitSapo, OriginalUrl
			,TemplateName, TemplateConfig, IsBreakingNews, PegaBreakingNews, IsPr, AdStore, AdStoreUrl
			, PrBookingNumber, IsOnMobile, TagSubTitleId, RollingNewsId, InterviewId,LocationType, ExpiredDate,SourceUrl,BonusPrice,ShortTitle,ParentNewsId
			)
		VALUES (@Id,@Title,@SubTitle,@Sapo,@Body,@Avatar,@AvatarDesc,@Avatar2,@Avatar3,@Avatar4,@Avatar5
			,@Author,@NewsRelation,@Status,@Source,@IsFocus,@Type,@ThreadId,GETDATE(),GETDATE()
			,@DistributionDate,@CreatedBy,@LastModifiedBy,@PublishedBy,@EditedBy,@LastReceiver,@WordCount,@ViewCount
			,@Priority,@Tag, @Note, @TagPrimary, @Price, @DisplayStyle, @DisplayPosition, @DisplayInSlide, @AvatarCustom
			,@NewsType, @IsOnHome, @OriginalId, @TagItem, @Url, @NoteRoyalties, @NewsCategory, @InitSapo, @Url
			,@TemplateName, @TemplateConfig, @IsBreakingNews, @PegaBreakingNews, @IsPr, @AdStore, @AdStoreUrl
			, @PrBookingNumber, @IsOnMobile, @TagSubTitleId, @RollingNewsId, @InterviewId,@LocationType, @ExpiredDate,@SourceUrl,@BonusPrice,@ShortTitle,@ParentNewsId
			);
			
		SET NOCOUNT ON
		
		DECLARE @Index int
			
		IF @AuthorNameList <> '' AND @AuthorIdList <> '' 
		BEGIN
			DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
			INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
			DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
			INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
			DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
			IF @AuthorNoteList <> ''
				INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
				
			SET @Index = 0;
			DECLARE @NumberRow@TblTempAuthorName int,
					@AuthorId int,
					@AuthorName nvarchar(255),
					@AuthorNote nvarchar(255);
			SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
			
			WHILE(@Index <= @NumberRow@TblTempAuthorName)
			BEGIN
				SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
				SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
				SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
				IF @AuthorId > 0
				BEGIN
					INSERT NewsByAuthor (NewsId, AuthorId, AuthorName, Note)
					VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
				END
				SET @Index = @Index + 1;
			END
		END
		
			
		-- Insert Primary Zone NewsInZone
		INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneId, 1, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneId));
		
		-- INSERT Other Zone into NewsInZone
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0 AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM NewsInZone WHERE ZoneId = @ZoneItemId AND NewsId = @Id)
						INSERT INTO NewsInZone(NewsId, ZoneId, IsPrimary, RootZoneId) VALUES (@Id, @ZoneItemId, 0, dbo.CMS_fGetRootZoneIdByCurrentZoneId(@ZoneItemId));
				SET @Index = @Index + 1
			END
		END
		-- Insert TagNews
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemId AND NewsId = @Id AND TagMode = 0)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemId, 0, @Index);
				SET @Index = @Index + 1
			END
		END
		-- Insert TagNewsForSubtitle
		IF @TagIdListForPrimary <> ''
		BEGIN
			DECLARE @TblTempTagForPrimary TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemIdForPrimary bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempTagForPrimary(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdListForPrimary, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTagForPrimary)) 
			BEGIN
				SET @TagItemIdForPrimary = (SELECT TagId FROM @TblTempTagForPrimary WHERE Id = @Index)
				IF @TagItemIdForPrimary IS NOT NULL AND @TagItemIdForPrimary > 0
					IF NOT EXISTS(SELECT TagID FROM TagNews WHERE TagID = @TagItemIdForPrimary AND NewsId = @Id AND TagMode = 1)
						INSERT INTO TagNews(NewsId, TagID, TagMode, Priority) 
						VALUES (@Id, @TagItemIdForPrimary, 1, @Index);
				SET @Index = @Index + 1
			END
		END

		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE ThreadId = @ThreadId AND NewsId = @Id)
				INSERT INTO ThreadNews (ThreadId, NewsId)
				VALUES (@ThreadId, @Id)
		END

	
		-- Insert NewsRelation
		IF @NewsRelationIdList <> ''
		BEGIN
			DECLARE @TblTempRelation TABLE (Id int identity(1,1), NewsId bigint)
			DECLARE @NewsItemId bigint; 
			SET @Index = 0;
			INSERT INTO @TblTempRelation(NewsId) SELECT CONVERT(bigint, Part) FROM SplitString(@NewsRelationIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
			BEGIN
				SET @NewsItemId = (SELECT NewsId FROM @TblTempRelation WHERE Id = @Index)
				IF @NewsItemId IS NOT NULL AND @NewsItemId > 0
					IF NOT EXISTS(SELECT NewsRelationId FROM NewsRelation WHERE NewsRelationId = @NewsItemId AND NewsId = @Id AND ZoneId = @ZoneId)
						INSERT INTO NewsRelation(NewsId, NewsRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @NewsItemId, @ZoneId, 0, @Index);
				SET @Index = @Index + 1
			END
		END
		-- insert NewsBySource 
		IF @SourceId <> 0
		BEGIN
			DELETE FROM NewsBySource where NewsId = @Id
			INSERT NewsBySource (NewsId, SourceId)
		    VALUES (@Id, @SourceId)
		END
		/* Xử lý autotag */
		DELETE FROM AutoTag WHERE NewsId = @Id
			
		INSERT INTO AutoTag (NewsId, IsProcessed)
		VALUES (@Id, 0)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END



--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_GetCountNewsPublishByZone]    Script Date: 07/13/2018 17:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/07/2018
CREATE PROCEDURE [dbo].[CMS_NewsPublish_GetCountNewsPublishByZone]
    @ZoneId int,
    @FromDate datetime,
    @EndDate datetime
AS 
BEGIN
    SELECT 0 as Id, COUNT(*) as TotalRow
    FROM    dbo.NewsPublish
    WHERE   ZoneId = @ZoneId and DistributionDate BETWEEN @FromDate AND @EndDate
END











