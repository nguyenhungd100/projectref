--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[VideoByAuthor]    Script Date: 06/07/2018 14:38:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VideoByAuthor](
	[VideoId] [bigint] NOT NULL,
	[AuthorId] [int] NOT NULL,
	[AuthorName] [nvarchar](255) NOT NULL,
	[Note] [nvarchar](255) NULL,
 CONSTRAINT [PK_VideoByAuthor] PRIMARY KEY CLUSTERED 
(
	[VideoId] ASC,
	[AuthorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[VideoRelation](
	[VideoId] [bigint] NOT NULL,
	[VideoRelationId] [bigint] NOT NULL,
	[ZoneId] [int] NOT NULL,
	[IsChange] [bit] NULL,
	[Priority] [int] NOT NULL,
	[Type] [int] NULL,
 CONSTRAINT [PK_VideoRelation] PRIMARY KEY CLUSTERED 
(
	[VideoId] ASC,
	[VideoRelationId] ASC,
	[ZoneId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VideoRelation] ADD  CONSTRAINT [DF_VideoRelation_IsChange]  DEFAULT ((0)) FOR [IsChange]
GO

ALTER TABLE [dbo].[VideoRelation] ADD  CONSTRAINT [DF_VideoRelation_Priority]  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[VideoRelation] ADD  DEFAULT ((1)) FOR [Type]
GO