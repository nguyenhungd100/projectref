GO

ALTER TABLE SEOMetaNews
Add KeywordSub nvarchar(4000)

Go

--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_Insert]
@NewsId bigint,
@MetaTitle nvarchar(300) = '',
@MetaKeyword nvarchar(300) = '',
@MetaDescription nvarchar(300) = '',
@MetaNewsKeyword nvarchar(500) = '',
@KeywordFocus nvarchar(500) = '',
@CreatedBy varchar(100) = '',
@SocialTitle NVARCHAR(250) = '',
@KeywordSub nvarchar(4000) = ''
AS
BEGIN
DELETE SEOMetaNews WHERE  NewsId =@NewsId

INSERT INTO SEOMetaNews (
		NewsId,
		MetaTitle,
		MetaKeyword,
		MetaDescription,
		MetaNewsKeyword,
		KeywordFocus,
		CreatedBy,
		SocialTitle,
		KeywordSub) 
		VALUES(
		@NewsId,
		@MetaTitle,
		@MetaKeyword,
		@MetaDescription,
		@MetaNewsKeyword,
		@KeywordFocus,
		@CreatedBy,
		@SocialTitle,
		@KeywordSub)
END


GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_Update]
@NewsId bigint,
@MetaTitle nvarchar(300) = '',
@MetaKeyword nvarchar(300) = '',
@MetaDescription nvarchar(300) = '',
@MetaNewsKeyword nvarchar(500) = '',
@KeywordFocus nvarchar(500) = '',
@CreatedBy varchar(100) = '',
@SocialTitle NVARCHAR(250) = '',
@KeywordSub nvarchar(4000) = ''
AS
BEGIN
UPDATE  SEOMetaNews SET
		MetaTitle =@MetaTitle,
		MetaKeyword =@MetaKeyword,
		MetaDescription =@MetaDescription,
		MetaNewsKeyword =@MetaNewsKeyword,
		KeywordFocus =@KeywordFocus,
		CreatedBy =@CreatedBy,
		SocialTitle = @SocialTitle,
		KeywordSub=@KeywordSub		
WHERE NewsId =@NewsId
END


GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_GetById]
@NewsId bigint
AS

BEGIN

	SELECT
		 NewsId,
		 MetaTitle,
		 MetaKeyword,
		 MetaDescription,
		 MetaNewsKeyword,
		 KeywordFocus,
		 CreatedBy,
		 SocialTitle,
		 KeywordSub
	 FROM SEOMetaNews WHERE NewsId = @NewsId
 
END
