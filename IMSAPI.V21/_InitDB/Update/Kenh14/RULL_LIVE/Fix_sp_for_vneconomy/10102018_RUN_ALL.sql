--USE [IMS2_TOQUOC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_InsertV3]    Script Date: 10/10/2018 1:44:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Edit: <chinhnb 12-05-2018>
-- Description:	<Update VideoChannel>
-- =============================================
ALTER PROCEDURE  [dbo].[VideoCms_Video_InsertV3]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100) = '',
	@Pname varchar(100) = '',
	@Status int = 0,
	@FileName varchar(500),
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@CreatedBy varchar(255),
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@OriginalUrl nvarchar(500) = '',
	@Source nvarchar(200) = '',
	@TagIdList varchar(300) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	@ChannelIdList varchar(500) = '',
	@Duration varchar(50),
	@Size varchar(20),
	@Capacity int,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	@OriginalId int = 0,
	@Author nvarchar(205) = '',
	@ParentId int,
	@HashId varchar(50) = '',
	@Type int,
	@TrailerUrl nvarchar(500) = '',
	@MetaAvatar nvarchar(max) = '',
	@Location varchar(50) = '',
	@PolicyContentId varchar(50) = '',
	@DisplayStyle nvarchar(max) = '',
	-- INSERT INTO VideoByAuthor TABLE
	@AuthorNameList nvarchar(500) = '',
	@AuthorIdList varchar(200) = '',
	@AuthorNoteList nvarchar(500) = '',
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION
	
    IF (EXISTS (SELECT Id FROM Video WHERE	KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0))
	BEGIN
		SELECT @Id = Id FROM Video WHERE KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0		
	END ELSE
    BEGIN
		DECLARE @ParentPrimaryZoneId int
		SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
		IF @ParentPrimaryZoneId > 0
		BEGIN
			IF @ZoneIdList <> ''
				SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
			ELSE
				SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
		END
		ELSE
		BEGIN
			DECLARE @ChildZoneId int
			SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
			IF @ChildZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
				SET @ZoneId = @ChildZoneId
			END
		END

		INSERT	INTO Video(	ZoneId,
							Name,
							UnsignName,
							[Description],
							HtmlCode,
							Avatar,
							KeyVideo,
							Pname,
							[Status],
							[FileName],
							NewsId,
							Views,
							Mode,
							Tags,
							CreatedBy,
							LastModifiedBy,
							Url,
							OriginalUrl,
							[Source],
							DistributionDate,
							CreatedDate,
							LastModifiedDate,
							VideoRelation,
							Duration,
							Size,
							Capacity,
							AllowAd,
							IsRemoveLogo,
							AvatarShareFacebook,
							OriginalId,
							Author,
							ParentId,
							HashId,
							[Type],
							TrailerUrl,
							MetaAvatar,
							Location,
							PolicyContentId,
							DisplayStyle
							)
					VALUES	(
							@ZoneId,
							@Name,
							@UnsignName,
							@Description,
							@HtmlCode,
							@Avatar,
							@KeyVideo,
							@Pname,
							@Status,
							@FileName,
							@NewsId,
							@Views,
							@Mode,
							@Tags,
							@CreatedBy,
							@CreatedBy,
							@Url,
							@OriginalUrl,
							@Source,
							@DistributionDate,
							GETDATE(),
							GETDATE(),
							@VideoRelationIdList,
							@Duration,
							@Size,
							@Capacity,
							@AllowAd,
							@IsRemoveLogo,
							@AvatarShareFacebook,
							@OriginalId,
							@Author,
							@ParentId,
							@HashId,
							@Type,
							@TrailerUrl,
							@MetaAvatar,
							@Location,
							@PolicyContentId,
							@DisplayStyle
						)
						
		SET @Id = SCOPE_IDENTITY();
		
		UPDATE Video SET Url = REPLACE(Url, '{VideoId}', @Id),OriginalUrl = REPLACE(OriginalUrl, '{VideoId}', @Id) WHERE Id = @Id
		
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert video error. Try again or create a other video.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN
			-- Process Tags
			DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
			DECLARE @Index int,
					@MaxPriority int,
					@TagId int;

				
			IF  @TagIdList <> ''
			BEGIN
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
					
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			
			IF(@ZoneId>0)
				INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary) VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- Process Playlist
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					
					IF @PlaylistId IS NOT NULL AND @PlayListId > 0
						BEGIN
						select @MaxPriority = Max(Priority) from VideoPlayList WHERE PlayListId = @PlaylistId
						if(@MaxPriority is null)
							set @MaxPriority=0
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @MaxPriority+1)
						END
					SET @Index = @Index + 1
				END
			END
			
			-- Process VideoInChannel			
			IF @ChannelIdList IS NOT NULL AND @ChannelIdList <> ''
			BEGIN
				DECLARE @TblVideoInChannel TABLE(Id int identity(1,1), ChannelId int NOT NULL);
				DECLARE @ChannelId int;
				
				INSERT @TblVideoInChannel(ChannelId)
				SELECT CONVERT(int, part) FROM SplitString(@ChannelIdList, ';')
								
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoInChannel))
				BEGIN
					SET @ChannelId = (SELECT ChannelId FROM @TblVideoInChannel WHERE Id = @Index)
					IF @ChannelId IS NOT NULL AND @ChannelId > 0
					BEGIN
						INSERT VideoInChannel (VideoId, ChannelId, LastModifledDate)
						VALUES (@Id, @ChannelId, GETDATE())
					END
					SET @Index = @Index + 1
				END
			END
			
			-- Insert VideoRelation
			IF @VideoRelationIdList <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), VideoId int)
				DECLARE @VideoItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(VideoId) SELECT CONVERT(int, Part) FROM SplitString(@VideoRelationIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @VideoItemId = (SELECT VideoId FROM @TblTempRelation WHERE Id = @Index)
					IF @VideoItemId IS NOT NULL AND @VideoItemId > 0
						IF NOT EXISTS(SELECT VideoRelationId FROM VideoRelation WHERE VideoRelationId = @VideoItemId AND VideoId = @Id AND ZoneId = @ZoneId)
							INSERT INTO VideoRelation(VideoId, VideoRelationId, ZoneId, IsChange, Priority) VALUES (@Id, @VideoItemId, @ZoneId, 0, @Index);
					SET @Index = @Index + 1
				END
			END
			
			-- insert VideoByAuthor		
			IF @AuthorNameList <> '' AND @AuthorIdList <> '' 
			BEGIN
				DECLARE @TblTempAuthorName TABLE(Id int identity(1,1), Name nvarchar(255))
				INSERT @TblTempAuthorName (Name) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNameList, ';');
				DECLARE @TblTempAuthorId TABLE(Id int identity(1,1), AuthorId int)
				INSERT @TblTempAuthorId (AuthorId) SELECT CONVERT(int, Part) FROM SplitString(@AuthorIdList, ';');
				DECLARE @TblTempAuthorNote TABLE(Id int identity(1,1), Note nvarchar(255))
				IF @AuthorNoteList <> ''
					INSERT @TblTempAuthorNote (Note) SELECT CONVERT(nvarchar(255), Part) FROM SplitString(@AuthorNoteList, ';');
					
				SET @Index = 0;
				DECLARE @NumberRow@TblTempAuthorName int,
						@AuthorId int,
						@AuthorName nvarchar(255),
						@AuthorNote nvarchar(255);
				SET @NumberRow@TblTempAuthorName = (SELECT COUNT(Id) FROM @TblTempAuthorName);
				
				WHILE(@Index <= @NumberRow@TblTempAuthorName)
				BEGIN
					SET @AuthorId = (SELECT AuthorId FROM @TblTempAuthorId WHERE Id = @Index);
					SET @AuthorName = (SELECT Name FROM @TblTempAuthorName WHERE Id = @Index);
					SET @AuthorNote = (SELECT Note FROM @TblTempAuthorNote WHERE Id = @Index);
					IF @AuthorId > 0
					BEGIN
						INSERT VideoByAuthor (VideoId, AuthorId, AuthorName, Note)
						VALUES (@Id, @AuthorId, @AuthorName, @AuthorNote)
					END
					SET @Index = @Index + 1;
				END
			END
						
		END		
    END
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		


--================== User=======================

GO

ALTER table UserProfile
ADD StaffCode varchar(20)

GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate())
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE()
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: Chinhnb<23-07-2018>
-- Create date: <2012-09-24>
-- Description:	<get user information by username>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserByUsername]
	@UserName varchar(255)
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem, P.StaffCode
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE LOWER(U.UserName) = LOWER(@UserName)
END


GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<23-07-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END



GO
ALTER TABLE [User]
ADD TelegramId bigint, DepartmentId int


--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_Addnew]    Script Date: 10/8/2018 4:44:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2012-09-24>
-- Description:	<Add new user information>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_Addnew]
	@Id int output,
	@UserName varchar(255),
	@Password varchar(255),
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		INSERT INTO [User] (UserName, Password, FullName, Avatar, Email, Mobile, IsFullPermission, 
			IsFullZone, Status, CreatedDate, ModifiedDate, LastLogined, LastChangePass,TelegramId,DepartmentId)
		VALUES (@UserName,@Password,@FullName,@Avatar,@Email,@Mobile,@IsFullPermission,
			@IsFullZone,@Status,GETDATE(),GETDATE(),null,getdate(),@TelegramId,@DepartmentId)
			
		SET @Id = SCOPE_IDENTITY();
		
		INSERT INTO UserProfile (UserId, Address, Birthday, Description, StaffCode)
		VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SET @Id = 0
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateById]    Script Date: 10/8/2018 5:02:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole tinyint,
	@IsSendOver bit,
	@StaffCode varchar(20)=NULL,
	@TelegramId bigint=NULL,
	@DepartmentId int=NULL
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE(),
				TelegramId=@TelegramId,
				DepartmentId=@DepartmentId
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description,
					StaffCode = @StaffCode
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description,StaffCode)
				VALUES (@Id,@Address,@Birthday,@Description,@StaffCode)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_GetUserById]    Script Date: 10/8/2018 5:04:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Edit: chinhnb<08-10-2018>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver,P.StaffCode,U.TelegramId,U.DepartmentId
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END


--=========================== TAG ===============================

GO

CREATE TABLE [dbo].[TagRelation](
	[TagId] [int] NOT NULL,
	[TagRelationId] [int] NOT NULL,
	[Ordinary] [int] NULL,
 CONSTRAINT [PK_TagRelation] PRIMARY KEY CLUSTERED 
(
	[TagId] ASC,
	[TagRelationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--chinhnb
--add: 15/06/2018
CREATE PROCEDURE [dbo].[CMS_Tag_GetRelationTag]
@TagId bigint
AS
BEGIN
	SELECT t.*
	FROM TagRelation tr LEFT JOIN Tag t on t.Id=tr.TagId
	WHERE tr.TagId=@TagId
END

--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Search]    Script Date: 07/23/2018 15:16:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--23/07/2018
CREATE PROCEDURE [dbo].[CMS_TagNews_CountTagNewsByListTagId] --'1,2,3'
	@ListTagId varchar(max)
AS
BEGIN
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	
	-- PARAMETERS
	SET @params = '@ListTagId varchar(max)'
	
	SET @search ='SELECT TagId,count(TagId) as CountNews FROM TagNews WHERE TagId in ('+@ListTagId+') GROUP BY TagId'
	
	EXEC SP_EXECUTESQL  @search, @params, @ListTagId
END


--======================== ZONE =============================

--USE [IMS2_AFAMILY_DEV]

-------------------------ADD COLLUM FOR TABLE ZONE----------------------------
GO
--chinhnb
--20/04/2018
ALTER TABLE Zone
ADD Avatar varchar(300),
	AvatarCover varchar(300),
	Logo varchar(300),	
	MetaAvatar varchar(max);
	
	
--------------------------CMS_Zone_Insert-------------------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Insert]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max)
AS
	BEGIN TRANSACTION CMS_Zone_Insert
	
	BEGIN TRY
		IF @Name IS NOT NULL AND @Name <> ''
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment,
				[Avatar]
				,[AvatarCover]
				,[Logo]				
				,[MetaAvatar]
			)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment,
				@Avatar
				,@AvatarCover
				,@Logo			
				,@MetaAvatar
			)
			COMMIT TRANSACTION CMS_Zone_Insert
		END
		ELSE BEGIN
			RAISERROR ('Invalid Zone Name', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Insert
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Insert
	END CATCH
	
	
-------------------------------CMS_Zone_Update-------------------------------
GO
--chinhnb
--20/04/2018
ALTER PROCEDURE [dbo].[CMS_Zone_Update]
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(100),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@AllowComment bit = 1,
	@Domain nvarchar(300),
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max),
	@Id int
AS
	BEGIN TRANSACTION CMS_Zone_Update
	
	BEGIN TRY
		IF EXISTS (SELECT Id FROM Zone WHERE Id = @Id)
		BEGIN
			UPDATE Zone
			SET
				Name = @Name, 
				Description = @Description, 
				ShortURL = @ShortURL, 
				SortOrder = @SortOrder, 
				ParentId = @ParentId, 
				Invisibled = @Invisibled, 
				Status = @Status,
				ModifiedDate = GETDATE(),
				AllowComment = @AllowComment,
				Domain=@Domain,
				Avatar=@Avatar,
				AvatarCover=@AvatarCover,
				Logo=@Logo,
				MetaAvatar=@MetaAvatar
			WHERE Id = @Id
			COMMIT TRANSACTION CMS_Zone_Update
		END
		ELSE BEGIN
			RAISERROR ('This zone does not existed in database.', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_Update
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_Update
	END CATCH
		
-----------------------------------CMS_Zone_GetByParentId--------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-26>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE (ParentId = @ParentId) ORDER BY SortOrder asc
END

-----------------------------------------CMS_Zone_GetById---------------------------------
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-24>
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_GetById]
	@Id int
AS
BEGIN
	SELECT * FROM Zone	WHERE (Id = @Id)
END

---------------------------------------CMS_Zone_Active_GetByParentId------------------------
GO
-- =============================================
-- Author:		longlh
-- Create date: 04/09/2014
-- Edit: chinhnb <20-04-2018>
-- Description:	<get zone by parent zone id with status=1>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Zone_Active_GetByParentId]
	@ParentId int
AS
BEGIN
	IF @ParentId < 0
		SELECT * FROM Zone WHERE Status=1  ORDER BY SortOrder asc
	ELSE
		SELECT * FROM Zone WHERE ParentId = @ParentId AND Status=1 ORDER BY SortOrder asc
END

GO



--==========================SEOMetaNews====================
GO

ALTER TABLE SEOMetaNews
Add KeywordSub nvarchar(4000)

Go

--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_Insert]
@NewsId bigint,
@MetaTitle nvarchar(300) = '',
@MetaKeyword nvarchar(300) = '',
@MetaDescription nvarchar(300) = '',
@MetaNewsKeyword nvarchar(500) = '',
@KeywordFocus nvarchar(500) = '',
@CreatedBy varchar(100) = '',
@SocialTitle NVARCHAR(250) = '',
@KeywordSub nvarchar(4000) = ''
AS
BEGIN
DELETE SEOMetaNews WHERE  NewsId =@NewsId

INSERT INTO SEOMetaNews (
		NewsId,
		MetaTitle,
		MetaKeyword,
		MetaDescription,
		MetaNewsKeyword,
		KeywordFocus,
		CreatedBy,
		SocialTitle,
		KeywordSub) 
		VALUES(
		@NewsId,
		@MetaTitle,
		@MetaKeyword,
		@MetaDescription,
		@MetaNewsKeyword,
		@KeywordFocus,
		@CreatedBy,
		@SocialTitle,
		@KeywordSub)
END


GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_Update]
@NewsId bigint,
@MetaTitle nvarchar(300) = '',
@MetaKeyword nvarchar(300) = '',
@MetaDescription nvarchar(300) = '',
@MetaNewsKeyword nvarchar(500) = '',
@KeywordFocus nvarchar(500) = '',
@CreatedBy varchar(100) = '',
@SocialTitle NVARCHAR(250) = '',
@KeywordSub nvarchar(4000) = ''
AS
BEGIN
UPDATE  SEOMetaNews SET
		MetaTitle =@MetaTitle,
		MetaKeyword =@MetaKeyword,
		MetaDescription =@MetaDescription,
		MetaNewsKeyword =@MetaNewsKeyword,
		KeywordFocus =@KeywordFocus,
		CreatedBy =@CreatedBy,
		SocialTitle = @SocialTitle,
		KeywordSub=@KeywordSub		
WHERE NewsId =@NewsId
END


GO
--chinhnb
--02/08/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaNews_GetById]
@NewsId bigint
AS

BEGIN

	SELECT
		 NewsId,
		 MetaTitle,
		 MetaKeyword,
		 MetaDescription,
		 MetaNewsKeyword,
		 KeywordFocus,
		 CreatedBy,
		 SocialTitle,
		 KeywordSub
	 FROM SEOMetaNews WHERE NewsId = @NewsId
 
END


--=========================SEOMetaTags================
GO
--chinhnb
--03/04/2018
ALTER PROC [dbo].[CMS_SEOMetaTags_Insert]
@Name nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@ZoneId int,
@Id int OUT
AS
BEGIN 
	IF(select count(*) from SEOMetaTags where ZoneId=@ZoneId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTags (Name, MetaKeyword, MetaDescription,ZoneId) 
		  VALUES (@Name,@MetaKeyword,@MetaDescription,@ZoneId)  
			
			SET @Id = SCOPE_IDENTITY()
		END
	ELSE
		SET @Id = 0   
END

--USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTags_GetList]    Script Date: 04/03/2018 10:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaTags_GetList]
@Keyword nvarchar(2000),
@ZoneId int ,
@PageIndex int,
@PageSize int,
@TotalRow int out
AS

DECLARE @sql nvarchar(max),
		@where nvarchar(500);
		
DECLARE @lbound int,
		@ubound int,
		@recct  int;

SET @where ='1=1';

IF @ZoneId = -1
	BEGIN
   SET @where= @where+ ' '
   END
ELSE 
BEGIN
    SET @where = @where+ ' AND S.ZoneId='+CONVERT(varchar,@ZoneId)
    END
IF @KeyWord IS NOT NULL AND @KeyWord <>''
BEGIN
SET @KeyWord ='N''%'+@KeyWord +'%''';
SET @where=@where+'AND (S.Name LIKE '+@KeyWord+'OR S.MetaKeyword LIKE '+@KeyWord+'OR S.MetaDescription LIKE '+@KeyWord+')';

END

SET @sql ='SELECT @recct=COUNT(*)FROM SEOMetaTags AS S INNER JOIN Zone AS Z ON S.ZoneId = Z.Id WHERE '+@where
PRINT   @sql
EXEC sp_executeSQL @sql, @params = N'@recct INT OUTPUT', @recct = @recct OUTPUT

	SET @lbound = (@PageIndex - 1)*@PageSize;
	SET @ubound = @lbound + @PageSize
	
	IF @recct IS NOT NULL AND @recct > 0
		SET @TotalRow = @recct;
	ELSE SET @TotalRow = 0;
DECLARE @Field nvarchar(1000);
SET @Field ='[Id]
			 ,[Name]
			 ,[MetaKeyword]
			 ,[MetaDescription]
			 ,[ZoneId]
			 ,[ZoneName]
			
'
SET @sql ='SELECT '+@Field+' FROM (SELECT ROW_NUMBER() OVER(ORDER BY Id DESC)AS row,'+@Field+'
                  FROM(SELECT S.Id, S.Name, S.MetaKeyword, S.MetaDescription, S.ZoneId, Z.Name AS ZoneName FROM SEOMetaTags AS S LEFT JOIN Zone AS Z ON S.ZoneId = Z.Id WHERE '+@where+'
                                
                  
                  )AS TblTemp

)AS TblTemp2 WHERE row > '+CONVERT(varchar(9),@lbound)+'
              AND row<='+CONVERT(varchar(9),@ubound)
   EXEC (@sql);



--=================================ZoneVideo====================================

--USE [IMS2_AFAMILY_BETA]
GO

ALTER TABLE ZoneVideo
Add Mode int,[Description] nvarchar(max)

GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Insert]
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@CreatedDate datetime,
	@Status int,
	@DisplayStyle int = 0,
	@ShowOnHome bit = 1,
	@ListVideoTagId varchar(1000) = '',
	@Invisibled bit = 1,
	@Id int output,
	@Avatar varchar(300) = '',
	@AvatarCover varchar(300) = '',
	@ZoneRelation varchar(100) = '',
	@Logo varchar(300) = '',
	@MetaAvatar varchar(max) = '',
	@CatId int,
	@Mode int,
	@Description nvarchar(max)
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZoneVideo (Name, Url, [Order], ParentId, [Status], CreatedDate, ModifiedDate,DisplayStyle,ShowOnHome,Invisibled,Avatar,AvatarCover,ZoneRelation,Logo,MetaAvatar,CatId,Mode,[Description])
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@CreatedDate,@CreatedDate,@DisplayStyle,@ShowOnHome,@Invisibled,@Avatar,@AvatarCover,@ZoneRelation,@Logo,@MetaAvatar,@CatId,@Mode,@Description)
	
	SET @Id = SCOPE_IDENTITY()
	
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@ModifiedDate datetime,
	@Status int,
	@ListVideoTagId varchar(1000) = '',
	@DisplayStyle int=0,
	@ShowOnHome bit = 1,
	@Invisibled bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@ZoneRelation varchar(100),
	@Logo varchar(300),
	@MetaAvatar varchar(max),
	@CatId int,	
	@Mode int,
	@Description nvarchar(max)
AS
	BEGIN TRANSACTION
	
	UPDATE ZoneVideo
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		[Status] = @Status, 
		ModifiedDate = @ModifiedDate,
		DisplayStyle =@DisplayStyle,
		ShowOnHome =@ShowOnHome,
		Invisibled = @Invisibled,
		AvatarCover = @AvatarCover,
		Avatar = @Avatar,
		ZoneRelation=@ZoneRelation,
		Logo=@Logo,
		MetaAvatar=@MetaAvatar,
		CatId=@CatId,
		Mode=@Mode,
		[Description]=@Description
	WHERE (Id = @Id)

	DELETE FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
-----------------------------------CMS_Zone_GetByParentId--------------------

GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM ZoneVideo
	WHERE Id = @Id
	
-----------------------
GO
--chinhnb
--18/05/2018
ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListByParentId]
	@ParentId int = 0,
	@Status int 
AS
	SELECT *, (SELECT COUNT(*) FROM ZoneVideo WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZoneVideo AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status <= 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]

--USE [IMS2_WEBTHETHAO]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_GetActivedByUsernameAndPermissionIds]    Script Date: 06/15/2018 10:03:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <15/06/2018>
-- Description:	<get zonevideo by username and permission ids>
-- =============================================
CREATE PROCEDURE [dbo].[VideoCms_ZoneVideo_GetActivedByUsernameAndPermissionIds] 
	@Username nvarchar(50),
	@PermissionIds varchar(1000)
AS
BEGIN
	DECLARE @UserId int,
			@IsFullZone bit,
			@IsFullPermission bit
	
	SET @IsFullZone = 0
	SET @IsFullPermission = 0
	
	SELECT @UserId = Id, @IsFullZone=IsFullZone, @IsFullPermission=IsFullPermission 
	FROM [User] WHERE Username = @Username
	
	IF @UserId > 0
	BEGIN
		IF @IsFullZone = 1 AND @IsFullPermission = 1
		BEGIN
			SELECT Id, Name, Description, ModifiedDate, CreatedDate, Url, [Order], 
				ParentId, Invisibled, Status
			FROM ZoneVideo
			WHERE Status = 1
		END
		ELSE IF @IsFullZone = 1 AND @IsFullPermission = 0
		BEGIN
			IF EXISTS(SELECT 1 FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
				SELECT Id, Name, Description, ModifiedDate, CreatedDate, Url, [Order], 
					ParentId, Invisibled, Status
				FROM ZoneVideo
				WHERE  Status = 1
		END
		ELSE IF @IsFullZone = 0 AND @IsFullPermission = 1
		BEGIN
			SELECT DISTINCT Z.Id, Z.Name, Z.Description, Z.ModifiedDate, Z.CreatedDate, Z.Url, Z.[Order], 
				Z.ParentId, Z.Invisibled, Z.Status
			FROM ZoneVideo AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1)
			UNION
			SELECT Id, Name, Description, ModifiedDate, CreatedDate, Url, [Order], 
				ParentId, Invisibled, Status
			FROM ZoneVideo
			WHERE ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId) AND (Status = 1)
		END
		ELSE
		BEGIN
			SET @PermissionIds = ';' + @PermissionIds + ';'
			
			SELECT DISTINCT Z.Id, Z.Name, Z.Description, Z.ModifiedDate, Z.CreatedDate, Z.Url, Z.[Order], 
				Z.ParentId, Z.Invisibled, Z.Status
			FROM ZoneVideo AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1) AND 
				(PATINDEX('%;' + CONVERT(varchar(100), UP.PermissionId) + ';%', @PermissionIds) > 0)
			UNION
			SELECT Id, Name, Description, ModifiedDate, CreatedDate, Url, [Order], 
				ParentId, Invisibled, Status
			FROM ZoneVideo
			WHERE (Status = 1) AND ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
		END
	END
END


--===========================Sticker=========================
--USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StickerNews_GetByNewsId]    Script Date: 03/12/2018 16:17:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_StickerNews_GetByNewsId] @NewsId BIGINT
AS 
    BEGIN
        SELECT  a.*,b.NewsId
        FROM    Sticker a
                INNER JOIN StickerInNews b ON a.Id = b.StickerId
                                              AND b.NewsId = @NewsId
    END


--USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Sticker_Search]    Script Date: 03/12/2018 09:26:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_Sticker_Search]
    @Keyword NVARCHAR(500) ,
    @PageIndex INT = 1 ,
    @PageSize INT = 10 ,
    @TotalRow INT = 0 OUT
AS 
    BEGIN
		SELECT @TotalRow = COUNT(*) FROM Sticker n where StickerName LIKE '%' + @Keyword + '%'
		
        SELECT  *
        FROM    ( SELECT TOP ( @PageIndex * @PageSize )
                            * ,
                            ROW_NUMBER() OVER ( ORDER BY id DESC ) AS RowNum
                  FROM      Sticker
                  WHERE     StickerName LIKE '%' + @Keyword + '%'
                ) baseTable
        WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                       AND     ( @PageIndex * @PageSize )
	
    END


--USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Sticker_AddNews]    Script Date: 03/12/2018 16:47:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_Sticker_AddNews]
    @NewsIds VARCHAR(MAX) = '' ,
    @StickerId BIGINT = 0
AS 
    BEGIN
        BEGIN TRANSACTION
	
        IF @NewsIds = '' 
            BEGIN
                RAISERROR ('Invalid NewsId list', 16, 1)
                ROLLBACK TRANSACTION
            END
        ELSE 
            IF NOT EXISTS ( SELECT  1
                            FROM    Sticker
                            WHERE   Id = @StickerId ) 
                BEGIN
					--IF	@StickerId=0
					DELETE FROM StickerInNews WHERE NewsId=@NewsIds
                    --RAISERROR ('This Sticker does not exist', 16, 1)
                    --ROLLBACK TRANSACTION
                END
            ELSE 
                BEGIN
                    DECLARE @StickerName NVARCHAR(200)

                    SELECT  @StickerName = StickerName
                    FROM    Sticker
                    WHERE   Id = @StickerId
		
                    IF @StickerName IS NOT NULL
                        AND @StickerName <> '' 
                        BEGIN
                            DELETE  StickerInNews
                            WHERE   StickerID = @StickerId
				
                            DECLARE @TblTemp TABLE
                                (
                                  Id INT IDENTITY(1, 1) ,
                                  NewsId BIGINT
                                )
                            INSERT  @TblTemp
                                    ( NewsId
                                    )
                                    SELECT  CONVERT(BIGINT, part)
                                    FROM    SplitString(@NewsIds, ';')
			
                            DECLARE @Index INT ,
                                @TotalRowInTemp INT ,
                                @NewsId BIGINT;
					
                            SET @Index = 0
                            SET @TotalRowInTemp = ISNULL(( SELECT
                                                              COUNT(Id)
                                                           FROM
                                                              @TblTemp
                                                         ), 0)
                            WHILE ( @Index <= @TotalRowInTemp ) 
                                BEGIN
                                    SET @NewsId = ISNULL(( SELECT
                                                              NewsId
                                                           FROM
                                                              @TblTemp
                                                           WHERE
                                                              Id = @Index
                                                         ), 0)
                                    IF @NewsId > 0 
                                        BEGIN
											DELETE FROM StickerInNews WHERE NewsId=@NewsId
                                            INSERT  INTO StickerInNews
                                                    ( NewsID, StickerID )
                                            VALUES  ( @NewsId, @StickerId )
                                        END
                                    SET @Index = @Index + 1
                                END
                        END
                END
	
        IF @@ERROR <> 0 
            BEGIN
                DECLARE @Message NVARCHAR(500)
                SET @Message = ERROR_MESSAGE();
                RAISERROR (@Message, 16, 1)
                ROLLBACK TRANSACTION
            END
        ELSE 
            BEGIN
                COMMIT TRANSACTION
            END
    END


