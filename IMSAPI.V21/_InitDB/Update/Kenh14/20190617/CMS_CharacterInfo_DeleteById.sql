--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_DeleteById]    Script Date: 6/17/2019 2:25:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_DeleteById]
@Id int
AS
BEGIN
  DELETE [CharacterInfo] WHERE Id = @Id
END