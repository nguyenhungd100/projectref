--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_InsertInitDB]    Script Date: 6/19/2019 5:24:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--19/06/2019
CREATE PROCEDURE  [dbo].[VideoCms_ZoneVideo_InsertInitDB]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@CreatedDate datetime,
	@Status int,
	@DisplayStyle int = 0,
	@ShowOnHome bit = 1,
	@ListVideoTagId varchar(1000) = '',
	@Invisibled bit = 1,	
	@Avatar varchar(300) = '',
	@AvatarCover varchar(300) = '',
	@ZoneRelation varchar(100) = '',
	@Logo varchar(300) = '',
	@MetaAvatar varchar(max) = '',
	@CatId int,
	@Mode int,
	@Description nvarchar(max)
AS
	SET IDENTITY_INSERT [ZoneVideo] ON;

	BEGIN TRANSACTION
	
	IF NOT EXISTS(Select * from ZoneVideo where Id=@Id)
	BEGIN
		INSERT INTO ZoneVideo (Id, Name, Url, [Order], ParentId, [Status], CreatedDate, ModifiedDate,DisplayStyle,ShowOnHome,Invisibled,Avatar,AvatarCover,ZoneRelation,Logo,MetaAvatar,CatId,Mode,[Description])
		VALUES(@Id, @Name,@Url,@Order,@ParentId,@Status,@CreatedDate,@CreatedDate,@DisplayStyle,@ShowOnHome,@Invisibled,@Avatar,@AvatarCover,@ZoneRelation,@Logo,@MetaAvatar,@CatId,@Mode,@Description)	
	
		IF  @ListVideoTagId <> ''
		BEGIN
			DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
			DECLARE @Index int,
					@VideoTagId int
			-- Tag Id List
			INSERT INTO @TblVideoTags (VideoTagId)
			SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
			SET @Index = 0;
			WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
				SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
				-- Add to VideoTags
				IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
				BEGIN
					INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
					VALUES (@Id, @VideoTagId, @Index)
				END
				SET @Index = @Index + 1;
			END
		END
	END

	SET IDENTITY_INSERT [ZoneVideo] OFF;

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
