--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_Update]    Script Date: 6/17/2019 2:44:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_Update]
@Id int,
@Name nvarchar(100),
@Avatar nvarchar(500),
@Description nvarchar(500),
@Quote nvarchar(500),
@BackgroundColor varchar(20),
@BorderColor varchar(20),
@Status int,
@ModifiedBy varchar(100)
AS
BEGIN

  UPDATE CharacterInfo
  SET
  Name = @Name,
   Avatar =@Avatar,
   Description =@Description,
   Quote = @Quote,
   BackgroundColor = @BackgroundColor,
   BorderColor =@BorderColor,
   Status = @Status,
   ModifiedDate=GETDATE(),
   ModifiedBy =@ModifiedBy,
  WHERE Id = @Id

END

