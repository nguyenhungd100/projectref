--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_Insert]    Script Date: 6/17/2019 2:30:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_Insert]
@Name nvarchar(100),
@Avatar nvarchar(500),
@Description nvarchar(500),
@Quote nvarchar(500),
@BackgroundColor varchar(20),
@BorderColor varchar(20),
@Status int,
@CreatedBy varchar(100),
@Id INT = 0 OUT 
AS
BEGIN

 INSERT INTO CharacterInfo(
	 Name,
	 Avatar,
	 Description,
	 Quote,
	 BackgroundColor,
	 BorderColor,
	 Status,
	 CreatedDate,
	 CreatedBy
 )
 VALUES(
	  @Name,
	  @Avatar,
	  @Description,
	  @Quote,
	  @BackgroundColor,
	  @BorderColor,
	  @Status,
	  GETDATE(),
	  @CreatedBy
 )
 
 SET @Id = SCOPE_IDENTITY();

END

