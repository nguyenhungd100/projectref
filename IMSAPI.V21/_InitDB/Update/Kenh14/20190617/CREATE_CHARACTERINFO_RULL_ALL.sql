--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[CharacterInfo]    Script Date: 6/17/2019 3:43:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CharacterInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Avatar] [nvarchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[Quote] [nvarchar](500) NULL,
	[BackgroundColor] [varchar](20) NULL,
	[BorderColor] [varchar](20) NULL,
	[Status] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
 CONSTRAINT [PK_CharacterInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CharacterInfo] ADD  CONSTRAINT [DF_CharacterInfo_Status]  DEFAULT ((1)) FOR [Status]
GO

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_GetList]    Script Date: 6/17/2019 2:20:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_GetList]
@Keyword nvarchar(1000) ='',
@Status int=-1,
@PageIndex int,
@PageSize int,
@TotalRows int =0 OUTPUT 
AS
BEGIN

    DECLARE @UpperBand  INT,
	        @LowerBand  INT
	
	SET @LowerBand = (@PageIndex - 1) * @PageSize
	SET @UpperBand = (@PageIndex * @PageSize) + 1
	
	PRINT @LowerBand
	PRINT @UpperBand
	
	DECLARE @tmbCharacterInfo TABLE  
	(
		RowIndex  INT PRIMARY KEY IDENTITY(1, 1),
		Id   INT
	)
	INSERT INTO @tmbCharacterInfo 
    (
	    Id
    )
	SELECT Id FROM   CharacterInfo
	WHERE ((@Status=-1 AND 1=1) OR (@Status<>-1 AND Status=@Status)) AND [Name] LIKE '%' + @Keyword + '%' 
	ORDER BY Id DESC
	
	SET @TotalRows = @@ROWCOUNT

	
	SELECT CharacterInfo.*,tmCharacterInfo.RowIndex
	FROM   CharacterInfo ,
	       @tmbCharacterInfo  AS tmCharacterInfo
	WHERE  CharacterInfo.Id= tmCharacterInfo.Id 
	       AND ((@Status=-1 AND 1=1) OR (@Status<>-1 AND Status=@Status)) AND Name LIKE '%' + @Keyword + '%' 
	       AND tmCharacterInfo.RowIndex > @LowerBand
	       AND tmCharacterInfo.RowIndex < @UpperBand
    ORDER BY Id DESC
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_GetById]    Script Date: 6/17/2019 2:25:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_GetById]
@Id int
AS
BEGIN
  SELECT * FROM [CharacterInfo] WHERE Id = @Id
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_Insert]    Script Date: 6/17/2019 2:30:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_Insert]
@Name nvarchar(100),
@Avatar nvarchar(500),
@Description nvarchar(500),
@Quote nvarchar(500),
@BackgroundColor varchar(20),
@BorderColor varchar(20),
@Status int,
@CreatedBy varchar(100),
@Id INT = 0 OUT 
AS
BEGIN

 INSERT INTO CharacterInfo(
	 Name,
	 Avatar,
	 Description,
	 Quote,
	 BackgroundColor,
	 BorderColor,
	 Status,
	 CreatedDate,
	 CreatedBy
 )
 VALUES(
	  @Name,
	  @Avatar,
	  @Description,
	  @Quote,
	  @BackgroundColor,
	  @BorderColor,
	  @Status,
	  GETDATE(),
	  @CreatedBy
 )
 
 SET @Id = SCOPE_IDENTITY();

END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_Update]    Script Date: 6/17/2019 2:44:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_Update]
@Id int,
@Name nvarchar(100),
@Avatar nvarchar(500),
@Description nvarchar(500),
@Quote nvarchar(500),
@BackgroundColor varchar(20),
@BorderColor varchar(20),
@Status int,
@ModifiedBy varchar(100)
AS
BEGIN

  UPDATE CharacterInfo
  SET
  Name = @Name,
   Avatar =@Avatar,
   Description =@Description,
   Quote = @Quote,
   BackgroundColor = @BackgroundColor,
   BorderColor =@BorderColor,
   Status = @Status,
   ModifiedDate=GETDATE(),
   ModifiedBy =@ModifiedBy
  WHERE Id = @Id

END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_DeleteById]    Script Date: 6/17/2019 2:25:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_DeleteById]
@Id int
AS
BEGIN
  DELETE [CharacterInfo] WHERE Id = @Id
END