--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_GetNewsRelationByNewsId]    Script Date: 6/19/2019 12:01:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author	  :	<ThanhTN>
-- Create date: <2012-10-01>
-- Description:	<get news relation by newsid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPublish_GetNewsRelationByNewsId]
	@NewsId bigint
AS
BEGIN
	SELECT * 
	FROM
		(
			SELECT DISTINCT NP.NewsId, 0 AS ZoneId, NP.Title, NP.Url, NP.Avatar, NR.Priority, NR.Type, NP.DistributionDate, NP.Author
			FROM NewsPublish AS NP INNER JOIN
				NewsRelation AS NR ON NP.NewsId = NR.NewsRelationId
			WHERE (NR.NewsId = @NewsId)
			AND NP.IsPrimary=1
		) AS TMP
	ORDER BY Priority ASC
END

