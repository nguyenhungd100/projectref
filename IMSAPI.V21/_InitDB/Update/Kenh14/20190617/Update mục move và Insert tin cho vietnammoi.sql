﻿
--1.chuyển tin từ mục Du lịch(206) của Lối sống(22) sang mục Du lịch(15): 206 -->move 15
BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=15;
SET @zoneB=206;

UPDATE NewsPublish set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE NewsContent set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE NewsInZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE TopicInZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE ThreadInZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE TagZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE SEOTagZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
UPDATE VoteInZone set ZoneId=@zoneA WHERE ZoneId=@zoneB
COMMIT TRANSACTION

GO

--2.thêm tin từ mục Tiêu dùng(154) vào mục Hàng hóa(521): 154 -->insert 521, isprimary=0
BEGIN TRANSACTION

DECLARE @zoneA int;
DECLARE @zoneB int;
SET @zoneA=521;
SET @zoneB=154;

INSERT INTO NewsPublish(NewsId,ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId)
select NewsId,@zoneA as ZoneId,Title,SubTitle,Sapo,Avatar,AvatarDesc,Avatar1,Avatar2,
Avatar3,Avatar4,Avatar5,Author,NewsRelation,Source,IsFocus,Type,ThreadId,DistributionDate,Url,DisplayStyle,
DisplayPosition,DisplayInSlide,AvatarCustom,OriginalId,PublishedDate,IsOnHome,TagItem,0 as IsPrimary,InitSapo,
NewsType,InterviewId,IsBreakingNews,OriginalUrl,IsPr,AdStore,AdStoreUrl,LastModifiedDate,RollingNewsId,
IsOnMobile,TagSubTitleId,Position,PrPosition,Priority,LastModifiedDateStamp,PublishedDateStamp,LocationType,
ExpiredDate,DistributionID,PrimaryZoneId,TitleDetail,ParentNewsId 
from NewsPublish WHERE ZoneId=@zoneB

INSERT INTO NewsInZone(NewsId,ZoneId,IsPrimary,DistributionID,RootZoneId)
select NewsId,@zoneA as ZoneId,0 as IsPrimary,DistributionID,RootZoneId from NewsInZone WHERE ZoneId=@zoneB

COMMIT TRANSACTION

