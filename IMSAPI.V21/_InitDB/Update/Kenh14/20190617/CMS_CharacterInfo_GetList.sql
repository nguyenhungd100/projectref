--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_GetList]    Script Date: 6/17/2019 2:20:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_GetList]
@Keyword nvarchar(1000) ='',
@Status int=-1,
@PageIndex int,
@PageSize int,
@TotalRows int =0 OUTPUT 
AS
BEGIN

    DECLARE @UpperBand  INT,
	        @LowerBand  INT
	
	SET @LowerBand = (@PageIndex - 1) * @PageSize
	SET @UpperBand = (@PageIndex * @PageSize) + 1
	
	PRINT @LowerBand
	PRINT @UpperBand
	
	DECLARE @tmbCharacterInfo TABLE  
	(
		RowIndex  INT PRIMARY KEY IDENTITY(1, 1),
		Id   INT
	)
	INSERT INTO @tmbCharacterInfo 
    (
	    Id
    )
	SELECT Id FROM   CharacterInfo
	WHERE ((@Status=-1 AND 1=1) OR (@Status<>-1 AND Status=@Status)) AND [Name] LIKE '%' + @Keyword + '%' 
	ORDER BY Id DESC
	
	SET @TotalRows = @@ROWCOUNT

	
	SELECT CharacterInfo.*,tmCharacterInfo.RowIndex
	FROM   CharacterInfo ,
	       @tmbCharacterInfo  AS tmCharacterInfo
	WHERE  CharacterInfo.Id= tmCharacterInfo.Id 
	       AND ((@Status=-1 AND 1=1) OR (@Status<>-1 AND Status=@Status)) AND Name LIKE '%' + @Keyword + '%' 
	       AND tmCharacterInfo.RowIndex > @LowerBand
	       AND tmCharacterInfo.RowIndex < @UpperBand
    ORDER BY Id DESC
END




