--select id,name,parentid from zone where id=500 or parentid=500

--select id,name,parentid from zone where id=500 or parentid=500 or parentid in(select id from zone where parentid=500) order by parentid asc

--select top(100) id, threadid from News

--select top(100) id,newsid,zoneid from newspublish order by id desc

--select top(100) newsid,zoneid from newscontent order by newsid desc

--select top(100) * from newsinzone order by newsid desc

--select top(100) * from topicinzone order by id desc

--select top(100) * from ThreadInZone order by threadid desc

--select top(100) * from TagZone order by tagid desc

--select top(100) * from SEOTagZone order by tagid desc

--select top(100) * from VoteInZone order by voteid desc


--update tin muc [a] -> [b]
Go
BEGIN TRANSACTION
--UPDATE NewsPublish set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE NewsContent set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE NewsInZone set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE TopicInZone set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE ThreadInZone set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE TagZone set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE SEOTagZone set ZoneId=[b] WHERE ZoneId=[a]
--UPDATE VoteInZone set ZoneId=[b] WHERE ZoneId=[a]
COMMIT TRANSACTION

--update tin muc [a] v� all con -> [b]
Go
BEGIN TRANSACTION
--UPDATE NewsPublish set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE NewsContent set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE NewsInZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE TopicInZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE ThreadInZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE TagZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE SEOTagZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
--UPDATE VoteInZone set ZoneId=[b] WHERE ZoneId IN (Select Id From [Zone] Where Id=[a] OR ParentId=[a] OR ParentId in(select id from [Zone] where ParentId=[a]))
COMMIT TRANSACTION
