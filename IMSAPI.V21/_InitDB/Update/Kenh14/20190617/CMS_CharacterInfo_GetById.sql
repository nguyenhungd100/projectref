--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_CharacterInfo_GetById]    Script Date: 6/17/2019 2:25:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-06-17
CREATE PROCEDURE [dbo].[CMS_CharacterInfo_GetById]
@Id int
AS
BEGIN
  SELECT * FROM [CharacterInfo] WHERE Id = @Id
END

