--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Zone_InsertInitDB]    Script Date: 6/19/2019 5:31:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--20/04/2018
CREATE PROCEDURE [dbo].[CMS_Zone_InsertInitDB]
	@Id int,
	@Name nvarchar(255),
	@Description nvarchar(255),
	@ShortURL varchar(255),
	@SortOrder int,
	@ParentId int = 0,
	@Invisibled bit = 0,
	@Status int = 0,
	@Domain nvarchar(300),
	@AllowComment bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@Logo varchar(300),	
	@MetaAvatar varchar(max),
	@ZoneContent nvarchar(max)=''
AS
	BEGIN TRANSACTION CMS_Zone_InsertInitDB
	
	BEGIN TRY
		IF NOT EXISTS(Select * from [Zone] where Id=@Id)
		BEGIN
			INSERT INTO Zone (
				Id,
				Name, 
				Description, 
				ShortURL, 
				SortOrder, 
				ParentId, 
				Invisibled, 
				Status,
				CreatedDate,
				ModifiedDate,
				Domain,
				AllowComment,
				[Avatar]
				,[AvatarCover]
				,[Logo]				
				,[MetaAvatar]
				,ZoneContent
			)
			VALUES (
				@Id,
				@Name,
				@Description,
				@ShortURL,
				@SortOrder,
				@ParentId,
				@Invisibled,
				@Status,
				GETDATE(),
				GETDATE(),
				@Domain,
				@AllowComment,
				@Avatar
				,@AvatarCover
				,@Logo			
				,@MetaAvatar
				,@ZoneContent
			)
			COMMIT TRANSACTION CMS_Zone_InsertInitDB
		END
		ELSE BEGIN
			RAISERROR ('ZoneId EXISTS', 16, 1)
			ROLLBACK TRANSACTION CMS_Zone_InsertInitDB
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_Zone_InsertInitDB
	END CATCH
	
