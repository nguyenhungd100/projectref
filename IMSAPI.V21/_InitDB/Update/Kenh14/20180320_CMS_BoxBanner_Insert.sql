USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBanner_Insert]    Script Date: 03/20/2018 11:26:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-10-22>
-- Edited date: 2018-03-20 - chinhnb
-- Description:	<Insert box banner>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_BoxBanner_Insert]	
	@ZoneId int,
	@Position int,
	@Priority int,
	@Avatar nvarchar(500),
	@Title nvarchar(250),
	@Sapo nvarchar(MAX),
	@Url varchar(500),
	@Status int,
	@DisplayStyle int = 0,
	@Type int =0,
	@ObjectId bigint,
	@ZoneIdList varchar(500) = ''
AS
BEGIN
BEGIN TRANSACTION
	
	BEGIN TRY
	INSERT INTO BoxBanner (ZoneId, Position, Priority, Avatar, Title, Sapo, Url, Status, DisplayStyle,Type,ObjectId)
	VALUES (@ZoneId,@Position,@Priority,@Avatar,@Title,@Sapo,@Url,@Status,@DisplayStyle, @Type, @ObjectId)
	
	DECLARE @Id int
	
	SET @Id = SCOPE_IDENTITY()
	DECLARE @Index int;
	-- INSERT Other Zone into BoxBannerInZone
		--INSERT INTO BoxBannerInZone(BoxBannerId, ZoneId) VALUES (@Id, @ZoneId);
		IF @ZoneIdList <> '' 
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId int)
			DECLARE @ZoneItemId int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(int, Part) FROM SplitString(@ZoneIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > -1 --AND @ZoneId <> @ZoneItemId
					IF NOT EXISTS(SELECT ZoneId FROM BoxBannerInZone WHERE ZoneId = @ZoneItemId AND BoxBannerId = @Id)
						INSERT INTO BoxBannerInZone(BoxBannerId, ZoneId) VALUES (@Id, @ZoneItemId);
				SET @Index = @Index + 1
			END
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END













