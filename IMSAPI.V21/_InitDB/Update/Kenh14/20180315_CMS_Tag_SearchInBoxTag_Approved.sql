USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_SearchInBoxTag]    Script Date: 03/15/2018 15:23:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--15/03/2018
CREATE PROCEDURE [dbo].[CMS_Tag_SearchInBoxTag_Approved]
	@Keyword nvarchar(500),
	@IsHotTag int = -1,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SET @TotalRow = (SELECT COUNT(1) FROM Tag T
					WHERE (@IsHotTag < 0 OR IsHotTag = @IsHotTag) AND (@Keyword = '' OR PATINDEX(@Keyword, T.Name) > 0)
					AND EXISTS (SELECT TagId FROM TagNews TN WHERE T.Id = TN.TagId));
					
	SELECT * FROM (
					SELECT Id, Name, Avatar, ROW_NUMBER() OVER(ORDER BY COUNT(NewsId) DESC) AS RowNum,
						SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
						FROM Tag T
					INNER JOIN TagNews TN ON T.Id = TN.TagId
					WHERE (@IsHotTag < 0 OR IsHotTag = @IsHotTag) AND (@Keyword = '' OR PATINDEX(@Keyword, T.Name) > 0)
					GROUP BY Id,Name,Avatar
					)AS TagTable
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
END










