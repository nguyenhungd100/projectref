--USE [IMS2_VIETNAMBIZ]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTagEmbed_GetListByZone]    Script Date: 2/12/2019 4:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
--12-02-2019
ALTER PROCEDURE [dbo].[CMS_BoxTagEmbed_GetListByZone] --0, 1
@ZoneId int,
@Type int
AS
	BEGIN
		SELECT TOP(100) BT.Id, BT.ZoneId, ISNULL(BT.TagId, 0) AS TagId, BT.SortOrder, BT.Type, ISNULL(TP.Avatar, '') AS Avatar, 
			ISNULL(TP.Name, BT.Title) AS Name, BT.Url
		FROM BoxTagEmbed as BT
			inner join Tag as TP on BT.TagId =TP.Id
		where BT.ZoneId =@ZoneId and BT.Type=@Type
		ORDER BY BT.SortOrder ASC
	END

