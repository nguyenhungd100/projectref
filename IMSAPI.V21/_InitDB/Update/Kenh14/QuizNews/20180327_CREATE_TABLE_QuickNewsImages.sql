USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[QuickNewsImages]    Script Date: 03/27/2018 11:49:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE TABLE [dbo].[QuickNewsImages](
	[QuickNewsImagesId] [bigint] NOT NULL,
	[QuickNewsId] [bigint] NULL,
	[Image] [nvarchar](500) NULL,
	[Description] [nvarchar](500) NULL,
	[IsAvatar] [bit] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_QuickNewsImages] PRIMARY KEY CLUSTERED 
(
	[QuickNewsImagesId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Image - 1:video' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuickNewsImages', @level2type=N'COLUMN',@level2name=N'Type'
GO


