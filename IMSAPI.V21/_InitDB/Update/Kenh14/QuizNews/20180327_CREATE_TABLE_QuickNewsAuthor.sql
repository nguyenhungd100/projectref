USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[QuickNewsAuthor]    Script Date: 03/27/2018 11:47:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[QuickNewsAuthor](
	[VietId] [bigint] NOT NULL,
	[UserName] [nvarchar](200) NULL,
	[FullName] [nvarchar](200) NULL,
	[Avatar] [nvarchar](200) NULL,
	[Phone] [varchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[Email] [varchar](50) NULL,
	[Status] [int] NULL,
	[Job] [nvarchar](200) NULL,
	[Slogan] [nvarchar](1000) NULL,
	[PenName] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastLoginDate] [datetime] NULL,
 CONSTRAINT [PK_QuickNewsAuthor] PRIMARY KEY CLUSTERED 
(
	[VietId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


