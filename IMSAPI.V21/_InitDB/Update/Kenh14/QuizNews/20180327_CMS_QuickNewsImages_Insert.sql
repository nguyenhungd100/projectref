USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickNewsImages_Insert]    Script Date: 03/27/2018 14:24:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickNewsImages_Insert]
	@CreatedDate datetime,
	@Description nvarchar(500),
	@Image nvarchar(300),
	@IsAvatar bit,
	@QuickNewsId bigint,
	@QuickNewsImagesId bigint,
	@Status int,
	@Type int
AS
BEGIN
	INSERT INTO QuickNewsImages (QuickNewsImagesId, QuickNewsId, [Image], [Description], IsAvatar, [Status], [Type], CreatedDate)
	VALUES (@QuickNewsImagesId, @QuickNewsId, @Image, @Description, @IsAvatar, @Status, @Type, @CreatedDate)	
END


