USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickNews_Insert]    Script Date: 03/27/2018 14:18:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickNews_Insert]
	@QuickNewsId bigint,
	@CatId int,
	@VietId bigint,
	@News_ID bigint,
	@Title nvarchar(300),
	@Sapo nvarchar(500),
	@Content nvarchar(MAX),
	@Note nvarchar(300),
	@Link nvarchar(300),
	@FullName nvarchar(200),
	@Phone varchar(200),
	@Email nvarchar(200),
	@Type int,
	@Status int,
	@CreatedDate datetime,
	@LastModifiedDate datetime,
	@PenName nvarchar(200)
AS
BEGIN
	INSERT INTO QuickNews (QuickNewsId, CatId, VietId, News_ID, Title, Sapo, Content, Note, Link, FullName, Phone, Email, Type, Status, PenName, CreatedDate, LastModifiedDate)
	VALUES (@QuickNewsId, @CatId, @VietId, @News_ID, @Title, @Sapo, @Content, @Note, @Link, @FullName, @Phone, @Email, @Type, @Status, @PenName, @CreatedDate, @LastModifiedDate)	
END





