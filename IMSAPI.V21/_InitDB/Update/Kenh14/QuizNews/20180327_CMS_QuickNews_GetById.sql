USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickNews_GetById]    Script Date: 03/27/2018 11:50:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickNews_GetById]
	@Id bigint
AS
BEGIN
	SELECT QuickNewsId, CatId, VietId, News_ID, Title, Sapo, Content, Note, Link, FullName, Phone, Email, Type, Status, PenName, CreatedDate, LastModifiedDate, ReceivedBy, ReceivedDate, DeletedBy, DeletedDate
	FROM QuickNews
	WHERE QuickNewsId = @Id
END

