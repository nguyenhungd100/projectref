USE [IMS2_KENH14_DEV]
GO

/****** Object:  Table [dbo].[QuickNews]    Script Date: 03/27/2018 11:01:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
--chinhnb
--27/03/2018
CREATE TABLE [dbo].[QuickNews](
	[QuickNewsId] [bigint] NOT NULL,
	[CatId] [int] NULL,
	[VietId] [bigint] NULL,
	[News_ID] [bigint] NULL,
	[Title] [nvarchar](500) NULL,
	[Sapo] [nvarchar](1000) NULL,
	[Content] [ntext] NULL,
	[Note] [nvarchar](500) NULL,
	[Link] [varchar](500) NULL,
	[FullName] [nvarchar](250) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [nvarchar](150) NULL,
	[Type] [int] NULL,
	[Status] [int] NULL,
	[PenName] [nvarchar](150) NULL,
	[Time] [nvarchar](50) NULL,
	[Location] [nvarchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[ReceivedBy] [nvarchar](50) NULL,
	[ReceivedDate] [datetime] NULL,
	[DeletedBy] [nvarchar](50) NULL,
	[DeletedDate] [datetime] NULL,
 CONSTRAINT [PK_QuickNews] PRIMARY KEY CLUSTERED 
(
	[QuickNewsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Use for mobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuickNews', @level2type=N'COLUMN',@level2name=N'Time'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Use for mobile' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'QuickNews', @level2type=N'COLUMN',@level2name=N'Location'
GO


