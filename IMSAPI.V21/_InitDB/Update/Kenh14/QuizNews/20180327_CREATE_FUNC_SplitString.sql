USE [IMS_KENH14_EXT]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 03/28/2018 11:34:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--28/03/2018
CREATE FUNCTION [dbo].[SplitString] 
(
    -- Add the parameters for the function here
    @myString nvarchar(max),
    @deliminator varchar(10)
)
RETURNS 
@ReturnTable TABLE 
(
    -- Add the column definitions for the TABLE variable here
    [id] int IDENTITY(1,1) NOT NULL,
    [part] nvarchar(500) NULL
)
AS
BEGIN
        Declare @iSpaces int
        Declare @part nvarchar(500)

        --initialize spaces
        Select @iSpaces = charindex(@deliminator,@myString,0)
        While @iSpaces > 0

        Begin
            Select @part = substring(@myString,0,charindex(@deliminator,@myString,0))

            Insert Into @ReturnTable(part) Select @part
            --Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString) - charindex(' ',@myString,0))
            Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString))
            Select @iSpaces = charindex(@deliminator,@myString,0)
        end

        If len(@myString) > 0
            Insert Into @ReturnTable
            Select @myString

    RETURN 
END



