USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_QuickNewsAuthor_Insert]    Script Date: 03/27/2018 14:22:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--27/03/2018
CREATE PROCEDURE [dbo].[CMS_QuickNewsAuthor_Insert]
	@VietId bigint,
	@UserName nvarchar(200),
	@FullName nvarchar(300),
	@Avatar nvarchar(300),
	@Phone varchar(200),
	@Address nvarchar(300),
	@Email nvarchar(200),
	@Status int,
	@Job nvarchar(200),
	@Slogan nvarchar(1000),
	@PenName nvarchar(500),
	@CreatedDate datetime,	
	@LastLoginDate datetime
	
AS
BEGIN
	IF NOT EXISTS(SELECT 1 FROM QuickNewsAuthor where VietId = @VietId)
	BEGIN
		INSERT INTO QuickNewsAuthor (VietId, UserName, FullName, Avatar, Phone, Address, Email, Status, Job, Slogan, PenName, CreatedDate, LastLoginDate)
		VALUES (@VietId, @UserName, @FullName, @Avatar, @Phone, @Address, @Email, @Status, @Job, @Slogan, @PenName, @CreatedDate, @LastLoginDate)	
	END
	ELSE
	BEGIN
		UPDATE QuickNewsAuthor SET
		UserName = @UserName, 
		FullName = @FullName, 
		Avatar = @Avatar, 
		Phone = @Phone, 
		Address = @Address, 
		Email = @Email, 
		Status = @Status, 
		Job = @Job, 
		Slogan = @Slogan, 
		PenName = @PenName, 
		CreatedDate = @CreatedDate, 
		LastLoginDate = @LastLoginDate
		WHERE VietId = @VietId
	END
END



