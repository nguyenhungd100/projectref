--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_ListPhotoByAlbumId]    Script Date: 8/17/2019 9:42:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<list photo by albumid>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_Photo_ListPhotoByAlbumId] --1,1,20,0
	@AlbumId int,	
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
				
	SELECT @TotalRow = COUNT(*) FROM Photo AS P where P.AlbumId=@AlbumId
	SELECT * FROM
	(
	SELECT P.*,	ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum
	FROM Photo AS P where P.AlbumId=@AlbumId
	) AS PhotoData
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY CreatedDate DESC
	
END

