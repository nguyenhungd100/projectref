--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_InsertV2]    Script Date: 8/13/2019 4:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-13>
-- Description:	<Insert new photo>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_InsertV2]
	@Id bigint output,	
	@ZoneId int,
	@Name nvarchar(1000),
	@ImageUrl nvarchar(500),
	@ImageNote nvarchar(max),
	@Size varchar(50),
	@Capacity float,
	@Note nvarchar(1000),
	@Status int,
	@CreatedBy nvarchar(100),
	@OriginalName nvarchar(500),
	@UnSignName nvarchar(500),
	@Tags nvarchar(max),

	@AlbumId int,
	@Author nvarchar(100),
	@ImagePreviewUrl nvarchar(500),
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ImageDistribution nvarchar(max),
	@Width int,
	@Height int,
	@ImageDimension tinyint,
	@ImageExif nvarchar(max),
	@RestrictionContent nvarchar(500),
	@Location nvarchar(250),
	
	@TagIdList varchar(500),

	@PhotoFolderId int,
	@PhotoLabelId int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Photo (Name, ImageUrl, ImageNote, Size, Capacity, Note, Status, ModifiedDate, 
			CreatedDate, ModifiedBy, CreatedBy, OriginalName, UnSignName, ZoneId, Tags,
			AlbumId,Author,ImagePreviewUrl,PublishedDate,PublishedBy,ImageDistribution,
			Width,Height,ImageDimension,ImageExif,RestrictionContent,Location)
		VALUES (@Name,@ImageUrl,@ImageNote,@Size,@Capacity,@Note,@Status, GETDATE(), 
            GETDATE(),@CreatedBy,@CreatedBy,@OriginalName,@UnSignName,@ZoneId, @Tags,
			@AlbumId,@Author,@ImagePreviewUrl,@PublishedDate,@PublishedBy,@ImageDistribution,
			@Width,@Height,@ImageDimension,@ImageExif,@RestrictionContent,@Location)
			
		SET @Id = SCOPE_IDENTITY()
		
		IF NOT EXISTS(SELECT 1 FROM PhotoInLabel WHERE PhotoLabelId = @PhotoLabelId AND PhotoId = @Id)
			INSERT INTO PhotoInLabel (PhotoLabelId, PhotoId, IsPrimary)
			VALUES (@PhotoLabelId, @Id, 1)
			
		IF NOT EXISTS(SELECT 1 FROM PhotoInFolder WHERE FolderId = @PhotoFolderId AND PhotoId = @Id)
			INSERT INTO PhotoInFolder (FolderId, PhotoId)
			VALUES (@PhotoFolderId, @Id)	
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM PhotoTags WHERE TagID = @TagItemId AND PhotoId = @Id AND TagMode = @TagMode)
						INSERT INTO PhotoTags(PhotoId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[PhotoCms_Photo_Insert]'+ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


