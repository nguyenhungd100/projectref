--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveDown]    Script Date: 8/9/2019 10:01:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveDown]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @NextId int, @NextOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			
			SELECT TOP 1 @NextOrder=[Order], @NextId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] >= @Order AND Id <> @Id
			ORDER BY [Order] ASC, Id ASC
			
			IF @NextOrder IS NOT NULL AND @NextOrder = @Order
				SET @NextOrder = @NextOrder + 1
			
			-- Co item o duoi
			IF @NextOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @NextOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @NextId
			END
			
			COMMIT TRANSACTIOn
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
