--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	UPDATE ZonePhoto
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		[Status] = @Status, 		
		ShowOnHome =@ShowOnHome,		
		Avatar = @Avatar,		
		Logo=@Logo,		
		[Description]=@Description,
		CatId=@CatId,
		ZoneRelation=@ZoneRelation,
		ModifiedDate=getdate()
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION


