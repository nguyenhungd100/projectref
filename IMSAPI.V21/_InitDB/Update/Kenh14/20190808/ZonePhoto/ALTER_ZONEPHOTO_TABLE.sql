DROP TABLE ZonePhoto

--USE [IMS2_SPORT5]
GO

/****** Object:  Table [dbo].[ZonePhoto]    Script Date: 8/9/2019 11:19:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZonePhoto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Status] [int] NOT NULL,
	[Url] [nvarchar](500) NULL,
	[Order] [int] NULL,
	[ShowOnHome] [bit] NULL,
	[Logo] [varchar](300) NULL,
	[Avatar] [varchar](500) NULL,
	[CatId] [int] NULL,
	[ZoneRelation] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ZonePhoto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_Status]  DEFAULT ((1)) FOR [Status]
GO