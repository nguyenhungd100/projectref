--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveUp]    Script Date: 8/9/2019 10:03:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveUp]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @PreviousId int, @PreviousOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			--SET @Order = @Order - 1;
			
			SELECT TOP 1 @PreviousOrder=[Order], @PreviousId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] <= @Order AND Id <> @Id
			ORDER BY [Order] DESC, Id DESC
			
			IF @PreviousOrder IS NOT NULL AND @PreviousOrder = @Order
				SET @PreviousOrder = @PreviousOrder - 1
				
			-- Co item o tren
			IF @PreviousOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @PreviousOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @PreviousId
			END
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH

