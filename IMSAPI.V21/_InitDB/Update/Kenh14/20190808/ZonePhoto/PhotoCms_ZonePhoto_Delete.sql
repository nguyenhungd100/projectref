--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Delete]    Script Date: 8/9/2019 10:04:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Delete]
	@Id int output
AS	
BEGIN
	--UPDATE ZonePhoto set Status=2 WHERE Id = @Id
	DELETE FROM ZonePhoto WHERE Id = @Id
END

