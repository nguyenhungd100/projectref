--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZonePhoto (Name, Url, [Order], ParentId, [Status], ShowOnHome,Avatar,Logo,[Description],CatId,ZoneRelation,CreatedDate)
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@ShowOnHome,@Avatar,@Logo,@Description,@CatId,@ZoneRelation,getdate())
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
