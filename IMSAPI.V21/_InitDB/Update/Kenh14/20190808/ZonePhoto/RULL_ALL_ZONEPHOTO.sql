DROP TABLE ZonePhoto

--USE [IMS2_SPORT5]
GO

/****** Object:  Table [dbo].[ZonePhoto]    Script Date: 8/9/2019 11:19:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZonePhoto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Status] [int] NOT NULL,
	[Url] [nvarchar](500) NULL,
	[Order] [int] NULL,
	[ShowOnHome] [bit] NULL,
	[Logo] [varchar](300) NULL,
	[Avatar] [varchar](500) NULL,
	[CatId] [int] NULL,
	[ZoneRelation] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ZonePhoto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_Status]  DEFAULT ((1)) FOR [Status]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZonePhoto (Name, Url, [Order], ParentId, [Status], ShowOnHome,Avatar,Logo,[Description],CatId,ZoneRelation,CreatedDate)
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@ShowOnHome,@Avatar,@Logo,@Description,@CatId,@ZoneRelation,getdate())
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	UPDATE ZonePhoto
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		[Status] = @Status, 		
		ShowOnHome =@ShowOnHome,		
		Avatar = @Avatar,		
		Logo=@Logo,		
		[Description]=@Description,
		CatId=@CatId,
		ZoneRelation=@ZoneRelation,
		ModifiedDate=getdate()
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetById]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--18/05/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM ZonePhoto
	WHERE Id = @Id
	
-----------------------


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetListByParentId]    Script Date: 8/8/2019 4:04:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_GetListByParentId]
	@ParentId int = 0,
	@Status int 
AS
	SELECT *, (SELECT COUNT(*) FROM ZonePhoto WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZonePhoto AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status <= 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveDown]    Script Date: 8/9/2019 10:01:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveDown]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @NextId int, @NextOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			
			SELECT TOP 1 @NextOrder=[Order], @NextId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] >= @Order AND Id <> @Id
			ORDER BY [Order] ASC, Id ASC
			
			IF @NextOrder IS NOT NULL AND @NextOrder = @Order
				SET @NextOrder = @NextOrder + 1
			
			-- Co item o duoi
			IF @NextOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @NextOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @NextId
			END
			
			COMMIT TRANSACTIOn
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveUp]    Script Date: 8/9/2019 10:03:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveUp]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @PreviousId int, @PreviousOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			--SET @Order = @Order - 1;
			
			SELECT TOP 1 @PreviousOrder=[Order], @PreviousId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] <= @Order AND Id <> @Id
			ORDER BY [Order] DESC, Id DESC
			
			IF @PreviousOrder IS NOT NULL AND @PreviousOrder = @Order
				SET @PreviousOrder = @PreviousOrder - 1
				
			-- Co item o tren
			IF @PreviousOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @PreviousOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @PreviousId
			END
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Delete]    Script Date: 8/9/2019 10:04:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Delete]
	@Id int output
AS	
BEGIN
	--UPDATE ZonePhoto set Status=2 WHERE Id = @Id
	DELETE FROM ZonePhoto WHERE Id = @Id
END



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds]    Script Date: 8/9/2019 10:22:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <09/08/2019>
-- Description:	<get zonePhoto by username and permission ids>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds] 
	@Username nvarchar(50),
	@PermissionIds varchar(1000)
AS
BEGIN
	DECLARE @UserId int,
			@IsFullZone bit,
			@IsFullPermission bit
	
	SET @IsFullZone = 0
	SET @IsFullPermission = 0
	
	SELECT @UserId = Id, @IsFullZone=IsFullZone, @IsFullPermission=IsFullPermission 
	FROM [User] WHERE Username = @Username
	
	IF @UserId > 0
	BEGIN
		IF @IsFullZone = 1 AND @IsFullPermission = 1
		BEGIN
			SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 1 AND @IsFullPermission = 0
		BEGIN
			IF EXISTS(SELECT 1 FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
				SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 0 AND @IsFullPermission = 1
		BEGIN
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId) AND (Status = 1)
		END
		ELSE
		BEGIN
			SET @PermissionIds = ';' + @PermissionIds + ';'
			
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1) AND 
				(PATINDEX('%;' + CONVERT(varchar(100), UP.PermissionId) + ';%', @PermissionIds) > 0)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE (Status = 1) AND ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
		END
	END
END