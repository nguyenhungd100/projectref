--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds]    Script Date: 8/9/2019 10:22:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <09/08/2019>
-- Description:	<get zonePhoto by username and permission ids>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds] 
	@Username nvarchar(50),
	@PermissionIds varchar(1000)
AS
BEGIN
	DECLARE @UserId int,
			@IsFullZone bit,
			@IsFullPermission bit
	
	SET @IsFullZone = 0
	SET @IsFullPermission = 0
	
	SELECT @UserId = Id, @IsFullZone=IsFullZone, @IsFullPermission=IsFullPermission 
	FROM [User] WHERE Username = @Username
	
	IF @UserId > 0
	BEGIN
		IF @IsFullZone = 1 AND @IsFullPermission = 1
		BEGIN
			SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 1 AND @IsFullPermission = 0
		BEGIN
			IF EXISTS(SELECT 1 FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
				SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 0 AND @IsFullPermission = 1
		BEGIN
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId) AND (Status = 1)
		END
		ELSE
		BEGIN
			SET @PermissionIds = ';' + @PermissionIds + ';'
			
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1) AND 
				(PATINDEX('%;' + CONVERT(varchar(100), UP.PermissionId) + ';%', @PermissionIds) > 0)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE (Status = 1) AND ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
		END
	END
END