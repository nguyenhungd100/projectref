--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByAlbumId]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetByAlbumId]
	@AlbumId int = 0
AS
	SELECT PT.* FROM AlbumTags AE inner join PhotoTag PT ON PT.Id=AE.TagId
	WHERE AE.AlbumId = @AlbumId
