--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_Search]    Script Date: 8/13/2019 10:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_Search]
	@Keyword nvarchar(500),	
	@Status int = -1,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Name) > 0)' 
			
	IF @Status >= 0
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoTag AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoTag AS T '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS TagTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'	
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,								
								@Status,								
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


