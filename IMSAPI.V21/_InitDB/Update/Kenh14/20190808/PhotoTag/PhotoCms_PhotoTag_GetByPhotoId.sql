--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoTag_GetByPhotoId]    Script Date: 8/13/2019 5:23:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-13-08>
-- Description:	<get phototag by photoid>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_PhotoTag_GetByPhotoId] --1
	@PhotoId bigint
AS
BEGIN
	SELECT T.Name, PT.PhotoId, PT.TagMode, PT.TagProperty, PT.Priority, PT.TagId
	FROM PhotoTags AS PT INNER JOIN
		PhotoTag AS T ON PT.TagId = T.Id
	WHERE (PT.PhotoId = @PhotoId)
END

