--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByName]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetById] @Id int
AS 
BEGIN
    SELECT  *
    FROM    dbo.PhotoTag
    WHERE   Id = @Id
END
