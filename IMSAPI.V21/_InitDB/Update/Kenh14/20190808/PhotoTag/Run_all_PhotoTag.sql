alter table PhotoTag
add Url nvarchar(250)

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByName]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetById] @Id int
AS 
BEGIN
    SELECT  *
    FROM    dbo.PhotoTag
    WHERE   Id = @Id
END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[CMS_PhotoTag_Update]
    @Id BIGINT,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY            
            BEGIN			
                UPDATE  dbo.PhotoTag
                SET     Name = @Name, Url = @Url, Avatar=@Avatar, Status=@Status
                WHERE   Id = @Id
            END
			COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

	--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[CMS_PhotoTag_Insert]
    @Id BIGINT OUTPUT ,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
            SET @Id = ISNULL(( SELECT TOP ( 1 )
                                        Id
                               FROM     PhotoTag
                               WHERE    LOWER(Name) = LOWER(@Name)
                             ), 0)
		
            IF @Id = 0 
                BEGIN
                    INSERT  INTO dbo.PhotoTag
                            ( Name, Avatar, Status, Url)
                    VALUES  ( @Name, @Avatar, @Status, @Url)
			
                    SET @Id = SCOPE_IDENTITY();
			
                END
            ELSE 
                BEGIN			
                    UPDATE  dbo.PhotoTag
                    SET     Name = @Name, Url = @Url
                    WHERE   Id = @Id
                END
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_Search]    Script Date: 8/13/2019 10:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_Search]
	@Keyword nvarchar(500),	
	@Status int = -1,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Name) > 0)' 
			
	IF @Status >= 0
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoTag AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoTag AS T '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS TagTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'	
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,								
								@Status,								
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_UpdateByName]    Script Date: 8/13/2019 11:26:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_UpdateByName]
	@Name nvarchar(250),	
	@Url nvarchar(250),
	@Status int,
	@Id int OUT
AS
BEGIN
	SELECT @Id = Id FROM PhotoTag WHERE Name LIKE @Name
	
	IF @Id IS NULL OR @Id <= 0
	BEGIN
		INSERT INTO PhotoTag (Name, Url, Status,Avatar)
		VALUES (@Name,@Url,@Status,'')
		
		SET @Id = SCOPE_IDENTITY()
	END
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoTag_GetByPhotoId]    Script Date: 8/13/2019 5:23:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-13-08>
-- Description:	<get phototag by photoid>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_PhotoTag_GetByPhotoId] --1
	@PhotoId bigint
AS
BEGIN
	SELECT T.Name, PT.PhotoId, PT.TagMode, PT.TagProperty, PT.Priority, PT.TagId
	FROM PhotoTags AS PT INNER JOIN
		PhotoTag AS T ON PT.TagId = T.Id
	WHERE (PT.PhotoId = @PhotoId)
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByAlbumId]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetByAlbumId]
	@AlbumId int = 0
AS
	SELECT PT.* FROM AlbumTags AE inner join PhotoTag PT ON PT.Id=AE.TagId
	WHERE AE.AlbumId = @AlbumId


