--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[CMS_PhotoTag_Insert]
    @Id BIGINT OUTPUT ,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
            SET @Id = ISNULL(( SELECT TOP ( 1 )
                                        Id
                               FROM     PhotoTag
                               WHERE    LOWER(Name) = LOWER(@Name)
                             ), 0)
		
            IF @Id = 0 
                BEGIN
                    INSERT  INTO dbo.PhotoTag
                            ( Name, Avatar, Status, Url)
                    VALUES  ( @Name, @Avatar, @Status, @Url)
			
                    SET @Id = SCOPE_IDENTITY();
			
                END
            ELSE 
                BEGIN			
                    UPDATE  dbo.PhotoTag
                    SET     Name = @Name, Url = @Url
                    WHERE   Id = @Id
                END
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

