--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_UpdateByName]    Script Date: 8/13/2019 11:26:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_UpdateByName]
	@Name nvarchar(250),	
	@Url nvarchar(250),
	@Status int,
	@Id int OUT
AS
BEGIN
	SELECT @Id = Id FROM PhotoTag WHERE Name LIKE @Name
	
	IF @Id IS NULL OR @Id <= 0
	BEGIN
		INSERT INTO PhotoTag (Name, Url, Status,Avatar)
		VALUES (@Name,@Url,@Status,'')
		
		SET @Id = SCOPE_IDENTITY()
	END
END

