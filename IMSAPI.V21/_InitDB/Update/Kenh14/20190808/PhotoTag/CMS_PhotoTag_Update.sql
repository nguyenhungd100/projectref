--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[CMS_PhotoTag_Update]
    @Id BIGINT,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY            
            BEGIN			
                UPDATE  dbo.PhotoTag
                SET     Name = @Name, Url = @Url, Avatar=@Avatar, Status=@Status
                WHERE   Id = @Id
            END
			COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END
