--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoEvent_Search]    Script Date: 8/12/2019 10:09:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE [dbo].[PhotoCms_PhotoEvent_Search]
	@Keyword nvarchar(500),	
	@ZoneId int,
	@Status int,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, PE.Name) > 0)' 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (PE.ZoneId = @ZoneId)' 	
		
	SET @order = ' ORDER BY PE.CreatedDate DESC, PE.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoEvent AS PE ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT PE.*, '	
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoEvent AS PE '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS PhotoEventTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	 
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END
