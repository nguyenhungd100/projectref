--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Update]
	@Id int,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ModifiedBy varchar(100),
	@Location nvarchar(500)
AS
	BEGIN TRANSACTION
	
	UPDATE PhotoEvent
	SET Name = @Name, 
		[Description]=@Description,
		Avatar = @Avatar,	
		Url = @Url, 
		BeginDate = @BeginDate, 
		EndDate = @EndDate, 
		[Status] = @Status, 		
		IsFocus =@IsFocus,					
		ZoneId=@ZoneId,
		PublishedDate=@PublishedDate,
		PublishedBy=@PublishedBy,
		ModifiedDate=getdate(),
		ModifiedBy=@ModifiedBy,
		Location=@Location
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION


