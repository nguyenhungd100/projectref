--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@CreatedBy varchar(100),
	@Location nvarchar(500)
AS
	BEGIN TRANSACTION
	
	INSERT INTO PhotoEvent (Name,[Description],Avatar, Url, BeginDate, EndDate, [Status], IsFocus,ZoneId,PublishedDate,PublishedBy,CreatedDate, CreatedBy,Location)
	VALUES(@Name,@Description,@Avatar,@Url,@BeginDate,@EndDate,@Status,@IsFocus,@ZoneId,@PublishedDate,@PublishedBy,getdate(),@CreatedBy,@Location)
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
