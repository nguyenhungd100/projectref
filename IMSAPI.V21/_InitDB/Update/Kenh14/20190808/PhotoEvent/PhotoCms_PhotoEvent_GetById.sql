--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_GetById]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM PhotoEvent
	WHERE Id = @Id
	
-----------------------
