--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[PhotoEvent]    Script Date: 8/23/2019 10:33:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PhotoEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Avatar] [varchar](300) NULL,
	[Url] [varchar](300) NULL,
	[BeginDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](100) NULL,
	[ZoneId] [int] NULL,
	[Status] [int] NULL,
	[IsFocus] [bit] NULL,
	[ImageCount] [int] NULL,
	[AlbumCount] [int] NULL,
	[ViewsCount] [int] NULL,
	[Location] [nvarchar](500) NULL,
 CONSTRAINT [PK_PhotoEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ZoneId]  DEFAULT ((0)) FOR [ZoneId]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_IsFocus]  DEFAULT ((0)) FOR [IsFocus]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ImageCount]  DEFAULT ((0)) FOR [ImageCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_AlbumCount]  DEFAULT ((0)) FOR [AlbumCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ViewsCount]  DEFAULT ((0)) FOR [ViewsCount]
GO


