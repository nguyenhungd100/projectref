--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[PhotoEvent]    Script Date: 8/23/2019 10:33:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PhotoEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Avatar] [varchar](300) NULL,
	[Url] [varchar](300) NULL,
	[BeginDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](100) NULL,
	[ZoneId] [int] NULL,
	[Status] [int] NULL,
	[IsFocus] [bit] NULL,
	[ImageCount] [int] NULL,
	[AlbumCount] [int] NULL,
	[ViewsCount] [int] NULL,
	[Location] [nvarchar](500) NULL,
 CONSTRAINT [PK_PhotoEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ZoneId]  DEFAULT ((0)) FOR [ZoneId]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_IsFocus]  DEFAULT ((0)) FOR [IsFocus]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ImageCount]  DEFAULT ((0)) FOR [ImageCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_AlbumCount]  DEFAULT ((0)) FOR [AlbumCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ViewsCount]  DEFAULT ((0)) FOR [ViewsCount]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoEvent_Search]    Script Date: 8/12/2019 10:09:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE [dbo].[PhotoCms_PhotoEvent_Search]
	@Keyword nvarchar(500),	
	@ZoneId int,
	@Status int,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, PE.Name) > 0)' 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (PE.ZoneId = @ZoneId)' 	
		
	SET @order = ' ORDER BY PE.CreatedDate DESC, PE.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoEvent AS PE ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT PE.*, '	
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoEvent AS PE '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS PhotoEventTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	 
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_GetById]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM PhotoEvent
	WHERE Id = @Id
	
--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Update]
	@Id int,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ModifiedBy varchar(100),
	@Location nvarchar(500)
AS
	BEGIN TRANSACTION
	
	UPDATE PhotoEvent
	SET Name = @Name, 
		[Description]=@Description,
		Avatar = @Avatar,	
		Url = @Url, 
		BeginDate = @BeginDate, 
		EndDate = @EndDate, 
		[Status] = @Status, 		
		IsFocus =@IsFocus,					
		ZoneId=@ZoneId,
		PublishedDate=@PublishedDate,
		PublishedBy=@PublishedBy,
		ModifiedDate=getdate(),
		ModifiedBy=@ModifiedBy,
		Location=@Location
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@CreatedBy varchar(100),
	@Location nvarchar(500)
AS
	BEGIN TRANSACTION
	
	INSERT INTO PhotoEvent (Name,[Description],Avatar, Url, BeginDate, EndDate, [Status], IsFocus,ZoneId,PublishedDate,PublishedBy,CreatedDate, CreatedBy,Location)
	VALUES(@Name,@Description,@Avatar,@Url,@BeginDate,@EndDate,@Status,@IsFocus,@ZoneId,@PublishedDate,@PublishedBy,getdate(),@CreatedBy,@Location)
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		

		
--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Delete]    Script Date: 8/9/2019 10:04:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Delete]
	@Id int output
AS	
BEGIN
	UPDATE PhotoEvent set Status=2 WHERE Id = @Id
	--DELETE FROM PhotoEvent WHERE Id = @Id
END



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_GetByAlbumId]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_GetByAlbumId] --1
	@AlbumId int = 0
AS
	SELECT PE.* FROM AlbumEvents AE inner join PhotoEvent PE ON PE.Id=AE.EventId
	WHERE AE.AlbumId = @AlbumId
	
-----------------------
