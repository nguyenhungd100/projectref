alter table photo
add AlbumId int,
	Author nvarchar(100),
	ImagePreviewUrl nvarchar(500),
	PublishedDate datetime,
	PublishedBy varchar(100),
	ImageDistribution nvarchar(max),
	Width int,
	Height int,
	ImageDimension tinyint,
	ImageExif nvarchar(max),
	RestrictionContent nvarchar(500),
	Location nvarchar(250);


	--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_InsertV2]    Script Date: 8/13/2019 4:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-13>
-- Description:	<Insert new photo>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_InsertV2]
	@Id bigint output,	
	@ZoneId int,
	@Name nvarchar(1000),
	@ImageUrl nvarchar(500),
	@ImageNote nvarchar(max),
	@Size varchar(50),
	@Capacity float,
	@Note nvarchar(1000),
	@Status int,
	@CreatedBy nvarchar(100),
	@OriginalName nvarchar(500),
	@UnSignName nvarchar(500),
	@Tags nvarchar(max),

	@AlbumId int,
	@Author nvarchar(100),
	@ImagePreviewUrl nvarchar(500),
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ImageDistribution nvarchar(max),
	@Width int,
	@Height int,
	@ImageDimension tinyint,
	@ImageExif nvarchar(max),
	@RestrictionContent nvarchar(500),
	@Location nvarchar(250),
	
	@TagIdList varchar(500),

	@PhotoFolderId int,
	@PhotoLabelId int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Photo (Name, ImageUrl, ImageNote, Size, Capacity, Note, Status, ModifiedDate, 
			CreatedDate, ModifiedBy, CreatedBy, OriginalName, UnSignName, ZoneId, Tags,
			AlbumId,Author,ImagePreviewUrl,PublishedDate,PublishedBy,ImageDistribution,
			Width,Height,ImageDimension,ImageExif,RestrictionContent,Location)
		VALUES (@Name,@ImageUrl,@ImageNote,@Size,@Capacity,@Note,@Status, GETDATE(), 
            GETDATE(),@CreatedBy,@CreatedBy,@OriginalName,@UnSignName,@ZoneId, @Tags,
			@AlbumId,@Author,@ImagePreviewUrl,@PublishedDate,@PublishedBy,@ImageDistribution,
			@Width,@Height,@ImageDimension,@ImageExif,@RestrictionContent,@Location)
			
		SET @Id = SCOPE_IDENTITY()
		
		IF NOT EXISTS(SELECT 1 FROM PhotoInLabel WHERE PhotoLabelId = @PhotoLabelId AND PhotoId = @Id)
			INSERT INTO PhotoInLabel (PhotoLabelId, PhotoId, IsPrimary)
			VALUES (@PhotoLabelId, @Id, 1)
			
		IF NOT EXISTS(SELECT 1 FROM PhotoInFolder WHERE FolderId = @PhotoFolderId AND PhotoId = @Id)
			INSERT INTO PhotoInFolder (FolderId, PhotoId)
			VALUES (@PhotoFolderId, @Id)	
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM PhotoTags WHERE TagID = @TagItemId AND PhotoId = @Id AND TagMode = @TagMode)
						INSERT INTO PhotoTags(PhotoId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[PhotoCms_Photo_Insert]'+ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_UpdateV2]    Script Date: 8/13/2019 4:35:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-13>
-- Description:	<Update new photo>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_UpdateV2]
	@Id bigint,	
	@ZoneId int,
	@Name nvarchar(1000),
	@ImageUrl nvarchar(500),
	@ImageNote nvarchar(max),
	@Size varchar(50),
	@Capacity float,
	@Note nvarchar(1000),
	@Status int,
	@ModifiedBy nvarchar(100),
	@OriginalName nvarchar(500),
	@UnSignName nvarchar(500),
	@Tags nvarchar(max),

	@AlbumId int,
	@Author nvarchar(100),
	@ImagePreviewUrl nvarchar(500),
	--@PublishedDate datetime,
	--@PublishedBy varchar(100),
	@ImageDistribution nvarchar(max),
	@Width int,
	@Height int,
	@ImageDimension tinyint,
	@ImageExif nvarchar(max),
	@RestrictionContent nvarchar(500),
	@Location nvarchar(250),

	@TagIdList varchar(500),

	@PhotoFolderId int,
	@PhotoLabelId int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE Photo
		SET Name = @Name, 
			ImageUrl=@ImageUrl,
			ImageNote = @ImageNote, 
			Size = @Size, 
			Capacity = @Capacity, 
			Note = @Note, 
			Status = @Status, 
			ModifiedDate = GETDATE(), 
			ModifiedBy = @ModifiedBy, 
			OriginalName = @OriginalName, 
			UnSignName = @UnSignName, 
			ZoneId = @ZoneId,
			Tags = @Tags,

			AlbumId=@AlbumId,
			Author =@Author,
			ImagePreviewUrl =@ImagePreviewUrl,
			--PublishedDate =@PublishedDate,
			--PublishedBy =@PublishedBy,
			ImageDistribution =@ImageDistribution,
			Width =@Width,
			Height =@Height,
			ImageDimension =@ImageDimension,
			ImageExif =@ImageExif,
			RestrictionContent =@RestrictionContent,
			Location =@Location
		WHERE (Id = @Id)
		
		DELETE FROM PhotoInLabel WHERE PhotoId = @Id
		IF @PhotoLabelId > 0 AND NOT EXISTS(SELECT 1 FROM PhotoInLabel WHERE PhotoLabelId = @PhotoLabelId AND PhotoId = @Id)
			INSERT INTO PhotoInLabel (PhotoLabelId, PhotoId, IsPrimary)
			VALUES (@PhotoLabelId, @Id, 1)
		
		DELETE FROM PhotoInFolder WHERE PhotoId = @Id
		IF @PhotoFolderId > 0 AND NOT EXISTS(SELECT 1 FROM PhotoInFolder WHERE FolderId = @PhotoFolderId AND PhotoId = @Id)
			INSERT INTO PhotoInFolder (FolderId, PhotoId)
			VALUES (@PhotoFolderId, @Id)
			
		DECLARE @Index int;
			
		DELETE FROM PhotoTags WHERE PhotoId = @Id
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM PhotoTags WHERE TagID = @TagItemId AND PhotoId = @Id AND TagMode = @TagMode)
						INSERT INTO PhotoTags(PhotoId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[PhotoCms_Photo_Update]'+ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_GetByIdV2]    Script Date: 8/13/2019 5:17:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[PhotoCms_Photo_GetByIdV2] 
 @Id bigint
AS
BEGIN
 SELECT *
 FROM Photo
 WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_SearchV2]    Script Date: 8/14/2019 3:08:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<photo search>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_SearchV2]
	@Keyword nvarchar(200),
	@AlbumId int,
	@Status int,
	@ZoneId int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@AlbumId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = 'WHERE (1 = 1)'
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (P.Name LIKE @Keyword)'
	END
	
	
	IF @AlbumId > 0
		SET @where = @where + ' AND (P.AlbumId = @AlbumId)'
				
	IF @Status > -1
		SET @where = @where + ' AND (P.Status = @Status)'
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (P.ZoneId = @ZoneId)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (P.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (P.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(*) FROM Photo AS P ' + @where + ';'
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT P.*, '
	SET @search = @search + '	ZP.Name as ZoneName, AB.Name as AlbumName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum '	
	SET @search = @search + '	FROM Photo AS P '
	SET @search = @search + '	LEFT JOIN ZonePhoto AS ZP ON ZP.Id=P.ZoneId '
	SET @search = @search + '	LEFT JOIN Album AS AB ON AB.Id=P.AlbumId '
	SET @search = @search + @where
	SET @search = @search + ' ) AS PhotoData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	SET @search = @search + ' ORDER BY CreatedDate DESC'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,
								@AlbumId,
								@Status,
								@ZoneId,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_Return]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_Return]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_Send]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_Send]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 3,
		ModifiedDate = GETDATE(),
		ModifiedBy = @UserDoAction 
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_PublishV2]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_PublishV2]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 1,
			ModifiedDate = GETDATE(),
			ModifiedBy = @UserDoAction,
			PublishedDate = GETDATE(),
			PublishedBy = @UserDoAction
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_UnPublish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_UnPublish]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 2			
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_ListPhotoByAlbumId]    Script Date: 8/17/2019 9:42:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<list photo by albumid>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_Photo_ListPhotoByAlbumId] --1,1,20,0
	@AlbumId int,	
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
				
	SELECT @TotalRow = COUNT(*) FROM Photo AS P where P.AlbumId=@AlbumId
	SELECT * FROM
	(
	SELECT P.*,	ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum
	FROM Photo AS P
	) AS PhotoData
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY CreatedDate DESC
	
END

