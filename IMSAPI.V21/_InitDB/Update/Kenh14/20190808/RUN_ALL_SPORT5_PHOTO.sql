alter table photo
add AlbumId int,
	Author nvarchar(100),
	ImagePreviewUrl nvarchar(500),
	PublishedDate datetime,
	PublishedBy varchar(100),
	ImageDistribution nvarchar(max),
	Width int,
	Height int,
	ImageDimension tinyint,
	ImageExif nvarchar(max),
	RestrictionContent nvarchar(500),
	Location nvarchar(250);


	--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_InsertV2]    Script Date: 8/13/2019 4:22:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-13>
-- Description:	<Insert new photo>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_InsertV2]
	@Id bigint output,	
	@ZoneId int,
	@Name nvarchar(1000),
	@ImageUrl nvarchar(500),
	@ImageNote nvarchar(max),
	@Size varchar(50),
	@Capacity float,
	@Note nvarchar(1000),
	@Status int,
	@CreatedBy nvarchar(100),
	@OriginalName nvarchar(500),
	@UnSignName nvarchar(500),
	@Tags nvarchar(max),

	@AlbumId int,
	@Author nvarchar(100),
	@ImagePreviewUrl nvarchar(500),
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ImageDistribution nvarchar(max),
	@Width int,
	@Height int,
	@ImageDimension tinyint,
	@ImageExif nvarchar(max),
	@RestrictionContent nvarchar(500),
	@Location nvarchar(250),
	
	@TagIdList varchar(500),

	@PhotoFolderId int,
	@PhotoLabelId int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Photo (Name, ImageUrl, ImageNote, Size, Capacity, Note, Status, ModifiedDate, 
			CreatedDate, ModifiedBy, CreatedBy, OriginalName, UnSignName, ZoneId, Tags,
			AlbumId,Author,ImagePreviewUrl,PublishedDate,PublishedBy,ImageDistribution,
			Width,Height,ImageDimension,ImageExif,RestrictionContent,Location)
		VALUES (@Name,@ImageUrl,@ImageNote,@Size,@Capacity,@Note,@Status, GETDATE(), 
            GETDATE(),@CreatedBy,@CreatedBy,@OriginalName,@UnSignName,@ZoneId, @Tags,
			@AlbumId,@Author,@ImagePreviewUrl,@PublishedDate,@PublishedBy,@ImageDistribution,
			@Width,@Height,@ImageDimension,@ImageExif,@RestrictionContent,@Location)
			
		SET @Id = SCOPE_IDENTITY()
		
		IF NOT EXISTS(SELECT 1 FROM PhotoInLabel WHERE PhotoLabelId = @PhotoLabelId AND PhotoId = @Id)
			INSERT INTO PhotoInLabel (PhotoLabelId, PhotoId, IsPrimary)
			VALUES (@PhotoLabelId, @Id, 1)
			
		IF NOT EXISTS(SELECT 1 FROM PhotoInFolder WHERE FolderId = @PhotoFolderId AND PhotoId = @Id)
			INSERT INTO PhotoInFolder (FolderId, PhotoId)
			VALUES (@PhotoFolderId, @Id)	
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM PhotoTags WHERE TagID = @TagItemId AND PhotoId = @Id AND TagMode = @TagMode)
						INSERT INTO PhotoTags(PhotoId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[PhotoCms_Photo_Insert]'+ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_UpdateV2]    Script Date: 8/13/2019 4:35:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-13>
-- Description:	<Update new photo>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_UpdateV2]
	@Id bigint,	
	@ZoneId int,
	@Name nvarchar(1000),
	@ImageUrl nvarchar(500),
	@ImageNote nvarchar(max),
	@Size varchar(50),
	@Capacity float,
	@Note nvarchar(1000),
	@Status int,
	@ModifiedBy nvarchar(100),
	@OriginalName nvarchar(500),
	@UnSignName nvarchar(500),
	@Tags nvarchar(max),

	@AlbumId int,
	@Author nvarchar(100),
	@ImagePreviewUrl nvarchar(500),
	--@PublishedDate datetime,
	--@PublishedBy varchar(100),
	@ImageDistribution nvarchar(max),
	@Width int,
	@Height int,
	@ImageDimension tinyint,
	@ImageExif nvarchar(max),
	@RestrictionContent nvarchar(500),
	@Location nvarchar(250),

	@TagIdList varchar(500),

	@PhotoFolderId int,
	@PhotoLabelId int
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE Photo
		SET Name = @Name, 
			ImageUrl=@ImageUrl,
			ImageNote = @ImageNote, 
			Size = @Size, 
			Capacity = @Capacity, 
			Note = @Note, 
			Status = @Status, 
			ModifiedDate = GETDATE(), 
			ModifiedBy = @ModifiedBy, 
			OriginalName = @OriginalName, 
			UnSignName = @UnSignName, 
			ZoneId = @ZoneId,
			Tags = @Tags,

			AlbumId=@AlbumId,
			Author =@Author,
			ImagePreviewUrl =@ImagePreviewUrl,
			--PublishedDate =@PublishedDate,
			--PublishedBy =@PublishedBy,
			ImageDistribution =@ImageDistribution,
			Width =@Width,
			Height =@Height,
			ImageDimension =@ImageDimension,
			ImageExif =@ImageExif,
			RestrictionContent =@RestrictionContent,
			Location =@Location
		WHERE (Id = @Id)
		
		DELETE FROM PhotoInLabel WHERE PhotoId = @Id
		IF @PhotoLabelId > 0 AND NOT EXISTS(SELECT 1 FROM PhotoInLabel WHERE PhotoLabelId = @PhotoLabelId AND PhotoId = @Id)
			INSERT INTO PhotoInLabel (PhotoLabelId, PhotoId, IsPrimary)
			VALUES (@PhotoLabelId, @Id, 1)
		
		DELETE FROM PhotoInFolder WHERE PhotoId = @Id
		IF @PhotoFolderId > 0 AND NOT EXISTS(SELECT 1 FROM PhotoInFolder WHERE FolderId = @PhotoFolderId AND PhotoId = @Id)
			INSERT INTO PhotoInFolder (FolderId, PhotoId)
			VALUES (@PhotoFolderId, @Id)
			
		DECLARE @Index int;
			
		DELETE FROM PhotoTags WHERE PhotoId = @Id
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM PhotoTags WHERE TagID = @TagItemId AND PhotoId = @Id AND TagMode = @TagMode)
						INSERT INTO PhotoTags(PhotoId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
			
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager='[dbo].[PhotoCms_Photo_Update]'+ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_GetByIdV2]    Script Date: 8/13/2019 5:17:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[PhotoCms_Photo_GetByIdV2] 
 @Id bigint
AS
BEGIN
 SELECT *
 FROM Photo
 WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_SearchV2]    Script Date: 8/14/2019 3:08:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<photo search>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_SearchV2]
	@Keyword nvarchar(200),
	@AlbumId int,
	@Status int,
	@ZoneId int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@AlbumId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = 'WHERE (1 = 1)'
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (P.Name LIKE @Keyword)'
	END
	
	
	IF @AlbumId > 0
		SET @where = @where + ' AND (P.AlbumId = @AlbumId)'
				
	IF @Status > -1
		SET @where = @where + ' AND (P.Status = @Status)'
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (P.ZoneId = @ZoneId)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (P.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (P.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(*) FROM Photo AS P ' + @where + ';'
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT P.*, '
	SET @search = @search + '	ZP.Name as ZoneName, AB.Name as AlbumName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum '	
	SET @search = @search + '	FROM Photo AS P '
	SET @search = @search + '	LEFT JOIN ZonePhoto AS ZP ON ZP.Id=P.ZoneId '
	SET @search = @search + '	LEFT JOIN Album AS AB ON AB.Id=P.AlbumId '
	SET @search = @search + @where
	SET @search = @search + ' ) AS PhotoData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	SET @search = @search + ' ORDER BY CreatedDate DESC'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,
								@AlbumId,
								@Status,
								@ZoneId,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_Return]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_Return]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_Send]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_Send]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 3,
		ModifiedDate = GETDATE(),
		ModifiedBy = @UserDoAction 
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_PublishV2]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_PublishV2]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 1,
			ModifiedDate = GETDATE(),
			ModifiedBy = @UserDoAction,
			PublishedDate = GETDATE(),
			PublishedBy = @UserDoAction
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_UnPublish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Photo_UnPublish]
	@Id bigint = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Photo WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Photo.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Photo 
		SET [Status] = 2			
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_ListPhotoByAlbumId]    Script Date: 8/17/2019 9:42:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<list photo by albumid>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_Photo_ListPhotoByAlbumId] --1,1,20,0
	@AlbumId int,	
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
				
	SELECT @TotalRow = COUNT(*) FROM Photo AS P where P.AlbumId=@AlbumId
	SELECT * FROM
	(
	SELECT P.*,	ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum
	FROM Photo AS P
	) AS PhotoData
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY CreatedDate DESC
	
END

DROP TABLE ZonePhoto

--USE [IMS2_SPORT5]
GO

/****** Object:  Table [dbo].[ZonePhoto]    Script Date: 8/9/2019 11:19:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ZonePhoto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Status] [int] NOT NULL,
	[Url] [nvarchar](500) NULL,
	[Order] [int] NULL,
	[ShowOnHome] [bit] NULL,
	[Logo] [varchar](300) NULL,
	[Avatar] [varchar](500) NULL,
	[CatId] [int] NULL,
	[ZoneRelation] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_ZonePhoto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO

ALTER TABLE [dbo].[ZonePhoto] ADD  CONSTRAINT [DF_ZonePhoto_Status]  DEFAULT ((1)) FOR [Status]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	INSERT INTO ZonePhoto (Name, Url, [Order], ParentId, [Status], ShowOnHome,Avatar,Logo,[Description],CatId,ZoneRelation,CreatedDate)
	VALUES(@Name,@Url,@Order,@ParentId,@Status,@ShowOnHome,@Avatar,@Logo,@Description,@CatId,@ZoneRelation,getdate())
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(500),
	@Order int,
	@ParentId int,	
	@Status int,	
	@ShowOnHome bit = 1,		
	@Avatar varchar(500) = '',	
	@Logo varchar(300) = '',	
	@Description nvarchar(max),
	@CatId int,	
	@ZoneRelation varchar(500)
AS
	BEGIN TRANSACTION
	
	UPDATE ZonePhoto
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		[Status] = @Status, 		
		ShowOnHome =@ShowOnHome,		
		Avatar = @Avatar,		
		Logo=@Logo,		
		[Description]=@Description,
		CatId=@CatId,
		ZoneRelation=@ZoneRelation,
		ModifiedDate=getdate()
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetById]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--18/05/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM ZonePhoto
	WHERE Id = @Id
	
-----------------------


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetListByParentId]    Script Date: 8/8/2019 4:04:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_GetListByParentId]
	@ParentId int = 0,
	@Status int 
AS
	SELECT *, (SELECT COUNT(*) FROM ZonePhoto WHERE ParentId = ZV.Id) AS NumberOfChild
	FROM ZonePhoto AS ZV
	WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
			(@Status <= 0 OR (@Status > 0 AND Status = @Status))
	ORDER BY [Order]

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveDown]    Script Date: 8/9/2019 10:01:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveDown]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @NextId int, @NextOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			
			SELECT TOP 1 @NextOrder=[Order], @NextId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] >= @Order AND Id <> @Id
			ORDER BY [Order] ASC, Id ASC
			
			IF @NextOrder IS NOT NULL AND @NextOrder = @Order
				SET @NextOrder = @NextOrder + 1
			
			-- Co item o duoi
			IF @NextOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @NextOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @NextId
			END
			
			COMMIT TRANSACTIOn
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_MoveUp]    Script Date: 8/9/2019 10:03:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_MoveUp]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM ZonePhoto WHERE Id = @Id)
		BEGIN
			RAISERROR ('This ZonePhoto does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Order int, @PreviousId int, @PreviousOrder int;
			SET @ParentId = (SELECT ParentId FROM ZonePhoto WHERE Id = @Id)
			SET @Order = (SELECT [Order] FROM ZonePhoto WHERE Id = @Id);
			
			--SET @Order = @Order - 1;
			
			SELECT TOP 1 @PreviousOrder=[Order], @PreviousId=Id
			FROM ZonePhoto
			WHERE ParentId = @ParentId AND [Order] <= @Order AND Id <> @Id
			ORDER BY [Order] DESC, Id DESC
			
			IF @PreviousOrder IS NOT NULL AND @PreviousOrder = @Order
				SET @PreviousOrder = @PreviousOrder - 1
				
			-- Co item o tren
			IF @PreviousOrder IS NOT NULL
			BEGIN
				UPDATE ZonePhoto SET [Order] = @PreviousOrder WHERE Id = @Id
				UPDATE ZonePhoto SET [Order] = @Order WHERE Id = @PreviousId
			END
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_Delete]    Script Date: 8/9/2019 10:04:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_ZonePhoto_Delete]
	@Id int output
AS	
	DELETE FROM ZonePhoto WHERE Id = @Id



--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds]    Script Date: 8/9/2019 10:22:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <09/08/2019>
-- Description:	<get zonePhoto by username and permission ids>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_ZonePhoto_GetActivedByUsernameAndPermissionIds] 
	@Username nvarchar(50),
	@PermissionIds varchar(1000)
AS
BEGIN
	DECLARE @UserId int,
			@IsFullZone bit,
			@IsFullPermission bit
	
	SET @IsFullZone = 0
	SET @IsFullPermission = 0
	
	SELECT @UserId = Id, @IsFullZone=IsFullZone, @IsFullPermission=IsFullPermission 
	FROM [User] WHERE Username = @Username
	
	IF @UserId > 0
	BEGIN
		IF @IsFullZone = 1 AND @IsFullPermission = 1
		BEGIN
			SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 1 AND @IsFullPermission = 0
		BEGIN
			IF EXISTS(SELECT 1 FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
				SELECT * FROM ZonePhoto WHERE Status = 1
		END
		ELSE IF @IsFullZone = 0 AND @IsFullPermission = 1
		BEGIN
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId) AND (Status = 1)
		END
		ELSE
		BEGIN
			SET @PermissionIds = ';' + @PermissionIds + ';'
			
			SELECT DISTINCT Z.*
			FROM ZonePhoto AS Z INNER JOIN
				UserPermission AS UP ON Z.Id = UP.ZoneId
			WHERE (UP.UserId = @UserId) AND (Z.Status = 1) AND 
				(PATINDEX('%;' + CONVERT(varchar(100), UP.PermissionId) + ';%', @PermissionIds) > 0)
			UNION
			SELECT *
			FROM ZonePhoto
			WHERE (Status = 1) AND ParentId IN (SELECT ZoneId FROM UserPermission WHERE UserId = @UserId AND (PATINDEX('%;' + CONVERT(varchar(100), PermissionId) + ';%', @PermissionIds) > 0))
		END
	END
END

alter table PhotoTag
add Url nvarchar(250)

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByName]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetById] @Id int
AS 
BEGIN
    SELECT  *
    FROM    dbo.PhotoTag
    WHERE   Id = @Id
END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[CMS_PhotoTag_Update]
    @Id BIGINT,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY            
            BEGIN			
                UPDATE  dbo.PhotoTag
                SET     Name = @Name, Url = @Url, Avatar=@Avatar, Status=@Status
                WHERE   Id = @Id
            END
			COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

	--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_Insert]    Script Date: 8/13/2019 10:29:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[CMS_PhotoTag_Insert]
    @Id BIGINT OUTPUT ,
    @Name NVARCHAR(500) = '' ,
    @Avatar NVARCHAR(500) = '' ,
    @Status INT,
	@Url NVARCHAR(250) = ''
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
            SET @Id = ISNULL(( SELECT TOP ( 1 )
                                        Id
                               FROM     PhotoTag
                               WHERE    LOWER(Name) = LOWER(@Name)
                             ), 0)
		
            IF @Id = 0 
                BEGIN
                    INSERT  INTO dbo.PhotoTag
                            ( Name, Avatar, Status, Url)
                    VALUES  ( @Name, @Avatar, @Status, @Url)
			
                    SET @Id = SCOPE_IDENTITY();
			
                END
            ELSE 
                BEGIN			
                    UPDATE  dbo.PhotoTag
                    SET     Name = @Name, Url = @Url
                    WHERE   Id = @Id
                END
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_Search]    Script Date: 8/13/2019 10:47:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_Search]
	@Keyword nvarchar(500),	
	@Status int = -1,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'	
	SET @params = @params + '@Status int,'	
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, T.Name) > 0)' 
			
	IF @Status >= 0
		SET @where = @where + ' AND (T.Status = @Status)'
			
	SET @order = ' ORDER BY T.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoTag AS T ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.*, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoTag AS T '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS TagTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'	
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,								
								@Status,								
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[Cms_PhotoTag_UpdateByName]    Script Date: 8/13/2019 11:26:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE  [dbo].[Cms_PhotoTag_UpdateByName]
	@Name nvarchar(250),	
	@Url nvarchar(250),
	@Status int,
	@Id int OUT
AS
BEGIN
	SELECT @Id = Id FROM PhotoTag WHERE Name LIKE @Name
	
	IF @Id IS NULL OR @Id <= 0
	BEGIN
		INSERT INTO PhotoTag (Name, Url, Status,Avatar)
		VALUES (@Name,@Url,@Status,'')
		
		SET @Id = SCOPE_IDENTITY()
	END
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoTag_GetByPhotoId]    Script Date: 8/13/2019 5:23:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-13-08>
-- Description:	<get phototag by photoid>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_PhotoTag_GetByPhotoId] --1
	@PhotoId bigint
AS
BEGIN
	SELECT T.Name, PT.PhotoId, PT.TagMode, PT.TagProperty, PT.Priority, PT.TagId
	FROM PhotoTags AS PT INNER JOIN
		PhotoTag AS T ON PT.TagId = T.Id
	WHERE (PT.PhotoId = @PhotoId)
END


--USE [IMS2_SPORT5]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoTag_GetByAlbumId]    Script Date: 8/13/2019 10:42:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
CREATE PROCEDURE [dbo].[Cms_PhotoTag_GetByAlbumId]
	@AlbumId int = 0
AS
	SELECT PT.* FROM AlbumTags AE inner join PhotoTag PT ON PT.Id=AE.AlbumId
	WHERE AE.AlbumId = @AlbumId

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[PhotoEvent]    Script Date: 8/12/2019 10:38:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PhotoEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[Avatar] [varchar](300) NULL,
	[Url] [varchar](300) NULL,
	[BeginDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[PublishedDate] [datetime] NULL,
	[PublishedBy] [varchar](100) NULL,
	[ZoneId] [int] NULL,
	[Status] [int] NULL,
	[IsFocus] [bit] NULL,
	[ImageCount] [int] NULL,
	[AlbumCount] [int] NULL,
	[ViewsCount] [int] NULL,
 CONSTRAINT [PK_PhotoEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ZoneId]  DEFAULT ((0)) FOR [ZoneId]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_Status]  DEFAULT ((1)) FOR [Status]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_IsFocus]  DEFAULT ((0)) FOR [IsFocus]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ImageCount]  DEFAULT ((0)) FOR [ImageCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_AlbumCount]  DEFAULT ((0)) FOR [AlbumCount]
GO

ALTER TABLE [dbo].[PhotoEvent] ADD  CONSTRAINT [DF_PhotoEvent_ViewsCount]  DEFAULT ((0)) FOR [ViewsCount]
GO




--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PhotoEvent_Search]    Script Date: 8/12/2019 10:09:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE [dbo].[PhotoCms_PhotoEvent_Search]
	@Keyword nvarchar(500),	
	@ZoneId int,
	@Status int,	
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)	
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1)'
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, PE.Name) > 0)' 
			
	IF @ZoneId > 0
		SET @where = @where + ' AND (PE.ZoneId = @ZoneId)' 	
		
	SET @order = ' ORDER BY PE.CreatedDate DESC, PE.Id DESC'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM PhotoEvent AS PE ' + @where + ';'
	SET @search = @search + 'SELECT * '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT PE.*, '	
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM PhotoEvent AS PE '	
	SET @search = @search + '	' + @where + ' '	
	SET @search = @search + ') AS PhotoEventTable '
	SET @search = @search + 'WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	 
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@Status,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_GetById]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_GetById] --21
	@Id int = 0
AS
	SELECT *
	FROM PhotoEvent
	WHERE Id = @Id
	
-----------------------
--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Update]    Script Date: 8/8/2019 4:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Update]
	@Id int,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@ModifiedBy varchar(100)
AS
	BEGIN TRANSACTION
	
	UPDATE PhotoEvent
	SET Name = @Name, 
		[Description]=@Description,
		Avatar = @Avatar,	
		Url = @Url, 
		BeginDate = @BeginDate, 
		EndDate = @EndDate, 
		[Status] = @Status, 		
		IsFocus =@IsFocus,					
		ZoneId=@ZoneId,
		PublishedDate=@PublishedDate,
		PublishedBy=@PublishedBy,
		ModifiedDate=getdate(),
		ModifiedBy=@ModifiedBy
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Insert]    Script Date: 8/8/2019 3:47:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--08/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Insert]
	@Id int output,
	@Name nvarchar(250),
	@Description nvarchar(500),
	@Avatar varchar(300) = '',
	@Url nvarchar(300),
	@BeginDate datetime,
	@EndDate datetime,
	@Status int=0,	
	@IsFocus bit = 0,	
	@ZoneId int,
	@PublishedDate datetime,
	@PublishedBy varchar(100),
	@CreatedBy varchar(100)
AS
	BEGIN TRANSACTION
	
	INSERT INTO PhotoEvent (Name,[Description],Avatar, Url, BeginDate, EndDate, [Status], IsFocus,ZoneId,PublishedDate,PublishedBy,CreatedDate, CreatedBy)
	VALUES(@Name,@Description,@Avatar,@Url,@BeginDate,@EndDate,@Status,@IsFocus,@ZoneId,@PublishedDate,@PublishedBy,getdate(),@CreatedBy)
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
		
--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_Delete]    Script Date: 8/9/2019 10:04:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--09/08/2018
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_Delete]
	@Id int output
AS	
	DELETE FROM PhotoEvent WHERE Id = @Id


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_PhotoEvent_GetByAlbumId]    Script Date: 8/12/2019 9:29:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--12/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_PhotoEvent_GetByAlbumId] --1
	@AlbumId int = 0
AS
	SELECT PE.* FROM AlbumEvents AE inner join PhotoEvent PE ON PE.Id=AE.EventId
	WHERE AE.AlbumId = @AlbumId
	
-----------------------
ALTER Table Album
Add [Description] nvarchar(500)
GO
ALTER Table Album
alter column ThumbImage nvarchar(300)

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[AlbumEvents]    Script Date: 8/12/2019 4:08:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AlbumEvents](
	[AlbumId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_AlbumEvents] PRIMARY KEY CLUSTERED 
(
	[AlbumId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_GetById]    Script Date: 8/12/2019 3:32:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<get album by id>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM Album
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Update]    Script Date: 8/12/2019 3:25:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <2019-08-12>
-- Description:	<update album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Update]
	@Id int,
	@ZoneId int,
	@Name nvarchar(250),
	@ThumbImage nvarchar(300),
	@ModifiedBy nvarchar(100),		
	@Status int,
	@Tags nvarchar(max),
	@Description nvarchar(500),
	@TagIdList varchar(500),
	@EventIdList varchar(500)	
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE Album
		SET ZoneId = @ZoneId,
			[Name] = @Name, 
			ThumbImage = @ThumbImage, 
			ModifiedBy = @ModifiedBy, 
			ModifiedDate = GETDATE(),
			[Status]=@Status,
			Tags=@Tags,
			[Description]=@Description
		WHERE (Id = @Id)		
		
		DECLARE @Index int;
		DELETE FROM AlbumTags WHERE AlbumId = @Id
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumTags WHERE TagID = @TagItemId AND AlbumId = @Id AND TagMode = @TagMode)
						INSERT INTO AlbumTags(AlbumId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
		
		DELETE FROM AlbumEvents WHERE AlbumId = @Id
		IF @EventIdList <> ''
		BEGIN
			DECLARE @TblTempEvent TABLE (Id int identity(1,1), EventId bigint)
			DECLARE @EventItemId bigint; 			
						
			SET @Index = 0;
			INSERT INTO @TblTempEvent(EventId) SELECT CONVERT(bigint, Part) FROM SplitString(@EventIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempEvent)) 
			BEGIN
				SET @EventItemId = (SELECT EventId FROM @TblTempEvent WHERE Id = @Index)
				IF @EventItemId IS NOT NULL AND @EventItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumEvents WHERE EventID = @EventItemId AND AlbumId = @Id)
						INSERT INTO AlbumEvents(AlbumId, EventId, Priority) 
						VALUES (@Id, @EventItemId, @Index);
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager=ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Insert]    Script Date: 8/12/2019 2:17:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<insert new album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Insert]
	@Id int output,
	@ZoneId int,
	@Name nvarchar(250),
	@ThumbImage nvarchar(300),
	@CreatedBy nvarchar(100),		
	@Status int,
	@Tags nvarchar(max),
	@Description nvarchar(500),
	@TagIdList varchar(500),
	@EventIdList varchar(500)
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Album (ZoneId, Name, ThumbImage, CreatedBy, ModifiedBy, CreatedDate, ModifiedDate, Status, Tags, [Description])
		VALUES (@ZoneId, @Name,@ThumbImage,@CreatedBy,@CreatedBy, GETDATE(), GETDATE(), @Status, @Tags, @Description)
		
		SET @Id = SCOPE_IDENTITY()
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumTags WHERE TagID = @TagItemId AND AlbumId = @Id AND TagMode = @TagMode)
						INSERT INTO AlbumTags(AlbumId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END

		IF @EventIdList <> ''
		BEGIN
			DECLARE @TblTempEvent TABLE (Id int identity(1,1), EventId bigint)
			DECLARE @EventItemId bigint; 			
						
			SET @Index = 0;
			INSERT INTO @TblTempEvent(EventId) SELECT CONVERT(bigint, Part) FROM SplitString(@EventIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempEvent)) 
			BEGIN
				SET @EventItemId = (SELECT EventId FROM @TblTempEvent WHERE Id = @Index)
				IF @EventItemId IS NOT NULL AND @EventItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumEvents WHERE EventID = @EventItemId AND AlbumId = @Id)
						INSERT INTO AlbumEvents(AlbumId, EventId, Priority) 
						VALUES (@Id, @EventItemId, @Index);
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager=ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Search]    Script Date: 8/12/2019 3:33:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<search album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Search] --'',0,0,-1,null,null,1,20,'',0
	@Keyword nvarchar(250),
	@ZoneId int,
	@EventId int,
	@Status int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@EventId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (A.Name LIKE @Keyword)'
	END
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (A.ZoneId = @ZoneId)'
	IF @EventId > 0
		SET @where = @where + ' AND (AE.EventId = @EventId)'
	IF @Status > -1
		SET @where = @where + ' AND (A.Status = @Status)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (A.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (A.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(distinct a.id) FROM Album AS A '
	SET @search = @search + 'LEFT JOIN AlbumEvents AS AE ON AE.AlbumId=A.Id '
	SET @search = @search + @where	
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT A.*,ZP.Name as ZoneName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY A.CreatedBy DESC) AS RowNum '
	SET @search = @search + '	FROM Album AS A '
	SET @search = @search + '	LEFT JOIN AlbumEvents as AE ON AE.AlbumId=A.Id '
	SET @search = @search + '	LEFT JOIN ZonePhoto as ZP ON ZP.Id=A.ZoneId '
	SET @search = @search + @where
	SET @search = @search + ' group by a.id,a.zoneid,a.name,a.ThumbImage,a.CreatedBy,a.ModifiedBy,a.CreatedDate,a.ModifiedDate,a.status,a.tags,a.description,ZP.Name'
	SET @search = @search + ' ) AS AlbumData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@EventId,
								@Status,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_UnPublish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_UnPublish]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 2
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Send]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_Send]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 3,
		ModifiedDate = GETDATE(),
		ModifiedBy = @UserDoAction 
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Return]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_Return]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Publish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
ALTER PROCEDURE  [dbo].[PhotoCms_Album_Publish]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 1,
			ModifiedDate = GETDATE(),
			ModifiedBy = @UserDoAction
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
