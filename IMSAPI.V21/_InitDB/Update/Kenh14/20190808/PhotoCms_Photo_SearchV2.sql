--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_SearchV2]    Script Date: 8/14/2019 3:08:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<photo search>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Photo_SearchV2]
	@Keyword nvarchar(200),
	@AlbumId int,
	@Status int,
	@ZoneId int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@AlbumId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = 'WHERE (1 = 1)'
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (P.Name LIKE @Keyword)'
	END
	
	
	IF @AlbumId > 0
		SET @where = @where + ' AND (P.AlbumId = @AlbumId)'
				
	IF @Status > -1
		SET @where = @where + ' AND (P.Status = @Status)'
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (P.ZoneId = @ZoneId)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (P.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (P.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (P.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(*) FROM Photo AS P ' + @where + ';'
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT P.*, '
	SET @search = @search + '	ZP.Name as ZoneName, AB.Name as AlbumName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY P.CreatedDate DESC) AS RowNum '	
	SET @search = @search + '	FROM Photo AS P '
	SET @search = @search + '	LEFT JOIN ZonePhoto AS ZP ON ZP.Id=P.ZoneId '
	SET @search = @search + '	LEFT JOIN Album AS AB ON AB.Id=P.AlbumId '
	SET @search = @search + @where
	SET @search = @search + ' ) AS PhotoData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
	SET @search = @search + ' ORDER BY CreatedDate DESC'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params,
								@Keyword,
								@AlbumId,
								@Status,
								@ZoneId,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


