--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_GetTagByTagID]    Script Date: 8/10/2019 9:52:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--10/08/2019
ALTER PROCEDURE [dbo].[CMS_Tag_GetTagByTagID]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM Tag
	WHERE (Id = @Id)
END
