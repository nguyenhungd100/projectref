--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetTopicByTopicID]    Script Date: 8/10/2019 9:57:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Edit:21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_GetTopicByTopicID] @Id BIGINT
AS 
    BEGIN
        SELECT top 1  T.*,
				TZ.ZoneId
        FROM    Topic T left join TopicInZone TZ on TZ.TopicId=T.Id
        WHERE   ( T.Id = @Id )
    END
