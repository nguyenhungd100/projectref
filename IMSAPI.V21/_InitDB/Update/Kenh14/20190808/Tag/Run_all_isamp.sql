﻿ALTER table Tag
add IsAmp bit

Go

ALTER table Topic
add IsAmp bit


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Insert]    Script Date: 8/9/2019 5:56:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
--Edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Insert]
    @Id BIGINT OUTPUT ,
    @TopicName NVARCHAR(300) ,
    @Logo VARCHAR(255) ,
    @Cover VARCHAR(255) ,
    @IsActive BIT ,
    @IsIconActive BIT ,
    @PrimaryZoneId INT ,
    @Description NVARCHAR(max) ,
    @DisplayUrl VARCHAR(300) ,
    @LogoFancyClose VARCHAR(300) ,
    @LogoTopicName VARCHAR(300) ,
    @LogoSubMenu VARCHAR(300) ,
    @ListTagId VARCHAR(300) ,
    @DefaultViewMode TINYINT ,
    @GuideToSendMail NVARCHAR(1000) ,
    @TopicEmail VARCHAR(300) ,
    @IsTopToolbar BIT = 0,
    @ParentId INT,
    @RelationTopic VARCHAR(500)='',
    @DisplayName NVARCHAR(300),
	@Priority INT,
	@ZoneIdList varchar(1000)='',
	@CreatedBy varchar(100)='',
	@IsAmp bit=0
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
		
            INSERT  INTO [dbo].[Topic]
                    ( [TopicName] ,
                      [Logo] ,
                      [Cover] ,
                      [IsActive] ,
                      [IsIconActive] ,
                      DESCRIPTION ,
                      DisplayUrl ,
                      LogoFancyClose ,
                      LogoTopicName ,
                      LogoSubMenu ,
                      DefaultViewMode ,
                      GuideToSendMail ,
                      TopicEmail ,
                      IsTopToolbar,
                      ParentId,
                      RelationTopic,
                      DisplayName,
					  [Priority],
					  CreatedBy,
					  CreatedDate,
					  IsAmp
                    )
            VALUES  ( @TopicName ,
                      @Logo ,
                      @Cover ,
                      @IsActive ,
                      @IsIconActive ,
                      @Description ,
                      @DisplayUrl ,
                      @LogoFancyClose ,
                      @LogoTopicName ,
                      @LogoSubMenu ,
                      @DefaultViewMode ,
                      @GuideToSendMail ,
                      @TopicEmail ,
                      @IsTopToolbar,
                      @ParentId,
                      @RelationTopic,
                      @DisplayName,
					  @Priority,
					  @CreatedBy,
					  getdate(),
					  @IsAmp
                    )
		
            SET @Id = SCOPE_IDENTITY();
            
            UPDATE Topic SET DisplayUrl = REPLACE(DisplayUrl, '{TopicId}', @Id) WHERE Id = @Id
		
			/* Insert vào bảng  TopicZone*/		
            INSERT  INTO TopicInZone(TopicId,ZoneId,IsPrimary)
            VALUES  ( @Id, @PrimaryZoneId, 1)

			DECLARE @Index INT;

			IF @ZoneIdList <> ''
			BEGIN
				DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
				DECLARE @ZoneItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@ZoneIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
				BEGIN
					SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
					IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
						IF NOT EXISTS(SELECT ZoneId FROM TopicInZone WHERE ZoneId = @ZoneItemId AND TopicId = @Id)
							INSERT INTO TopicInZone(TopicId, ZoneId, IsPrimary) 
							VALUES (@Id, @ZoneItemId, 0);
						
					SET @Index = @Index + 1
				END
			END

            --Tag
            DELETE  FROM TagInTopic
            WHERE   TopicId = @Id
            DECLARE @TblAddTemp TABLE
                (
                  Id INT IDENTITY(1, 1) ,
                  NewsId BIGINT
                )
            INSERT  @TblAddTemp
                    ( NewsId
                    )
                    SELECT  CONVERT(BIGINT, part)
                    FROM    SplitString(@ListTagId, ';')
		
            DECLARE @TotalRowInTemp INT ,
                @TagId BIGINT ,
                @Avatar NVARCHAR(500);
		
            SET @Index = 0
            SET @TotalRowInTemp = ISNULL(( SELECT   COUNT(Id)
                                           FROM     @TblAddTemp
                                         ), 0)
            WHILE ( @Index <= @TotalRowInTemp ) 
                BEGIN
                    SET @TagId = ISNULL(( SELECT    NewsId
                                          FROM      @TblAddTemp
                                          WHERE     Id = @Index
                                        ), 0)
                    IF @TagId > 0 
                        BEGIN
                            IF NOT EXISTS ( SELECT  1
                                            FROM    TagInTopic
                                            WHERE   TagId = @TagId
                                                    AND TopicId = @Id ) 
                                BEGIN
                                    INSERT  INTO TagInTopic
                                            ( TagId, TopicID, Priority )
                                    VALUES  ( @TagId, @Id, @Index + 1 )
                                END
                        END
                    SET @Index = @Index + 1
                END
			
			--Xu ly TopicRelation
			IF @RelationTopic <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), TopicId int)
				DECLARE @TopicItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(TopicId) SELECT CONVERT(int, Part) FROM SplitString(@RelationTopic, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @TopicItemId = (SELECT TopicId FROM @TblTempRelation WHERE Id = @Index)
					IF @TopicItemId IS NOT NULL AND @TopicItemId > 0
						IF NOT EXISTS(SELECT TopicRelationId FROM TopicRelation WHERE TopicRelationId = @TopicItemId AND TopicId = @Id)
							INSERT INTO TopicRelation(TopicId, TopicRelationId, Ordinary) VALUES (@Id, @TopicItemId,0);
					SET @Index = @Index + 1
				END
			END
			
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_Update]    Script Date: 8/9/2019 5:56:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Edit: 21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_Update]
    @Id BIGINT OUTPUT ,
    @TopicName NVARCHAR(300) ,
    @Logo VARCHAR(255) ,
    @Cover VARCHAR(255) ,
    @IsActive BIT ,
    @IsIconActive BIT ,
    @PrimaryZoneId INT ,
    @Description NVARCHAR(max) ,
    @DisplayUrl VARCHAR(300) ,
    @LogoFancyClose VARCHAR(300) ,
    @LogoTopicName VARCHAR(300) ,
    @LogoSubMenu VARCHAR(300) ,
    @ListTagId VARCHAR(300) ,
    @DefaultViewMode TINYINT ,
    @GuideToSendMail NVARCHAR(1000) ,
    @TopicEmail VARCHAR(300) ,
    @IsTopToolbar BIT = 0,
    @ParentId INT,
    @RelationTopic VARCHAR(500)='',
    @DisplayName NVARCHAR(300),
	@Priority INT,
	@ZoneIdList varchar(1000)='',
	@ModifiedBy varchar(100)='',
	@IsAmp bit=0
AS 
    BEGIN
        BEGIN TRANSACTION
	
        BEGIN TRY
            UPDATE  [dbo].[Topic]
            SET     [TopicName] = @TopicName ,
                    [Logo] = @Logo ,
                    [Cover] = @Cover ,
                    [IsActive] = @IsActive ,
                    [IsIconActive] = @IsIconActive ,
                    DESCRIPTION = @Description ,
                    DisplayUrl = @DisplayUrl ,
                    LogoFancyClose = @LogoFancyClose ,
                    LogoTopicName = @LogoTopicName ,
                    LogoSubMenu = @LogoSubMenu ,
                    DefaultViewMode = @DefaultViewMode ,
                    GuideToSendMail = @GuideToSendMail ,
                    TopicEmail = @TopicEmail ,
                    IsTopToolbar = @IsTopToolbar,
                    ParentId = @ParentId ,
                    RelationTopic = @RelationTopic,
                    DisplayName=@DisplayName,
					Priority=@Priority,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=getdate(),
					IsAmp=@IsAmp
            WHERE   ( Id = @Id )
		
			/* Xóa dữ liệu bảng TopicZone */
            DELETE  FROM TopicInZone WHERE TopicId = @Id
			
			INSERT  INTO TopicInZone(TopicId,ZoneId,IsPrimary)
            VALUES  ( @Id, @PrimaryZoneId, 1)

			DECLARE @Index INT;

			IF @ZoneIdList <> ''
			BEGIN
				DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
				DECLARE @ZoneItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@ZoneIdList, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
				BEGIN
					SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
					IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
						IF NOT EXISTS(SELECT ZoneId FROM TopicInZone WHERE ZoneId = @ZoneItemId AND TopicId = @Id)
							INSERT INTO TopicInZone(TopicId, ZoneId, IsPrimary) 
							VALUES (@Id, @ZoneItemId, 0);
						
					SET @Index = @Index + 1
				END
			END		            
		
			--Tag
            DECLARE @TblAddTemp TABLE
                (
                  Id INT IDENTITY(1, 1) ,
                  NewsId BIGINT
                )
            INSERT  @TblAddTemp
                    ( NewsId
                    )
                    SELECT  CONVERT(BIGINT, part)
                    FROM    SplitString(@ListTagId, ';')
		
            DELETE  FROM TagInTopic WHERE   TopicId = @Id

            DECLARE @TotalRowInTemp INT ,
                @TagId BIGINT ,
                @Avatar NVARCHAR(500);
		
            SET @Index = 0
            SET @TotalRowInTemp = ISNULL(( SELECT   COUNT(Id)
                                           FROM     @TblAddTemp
                                         ), 0)
            WHILE ( @Index <= @TotalRowInTemp ) 
                BEGIN
                    SET @TagId = ISNULL(( SELECT    NewsId
                                          FROM      @TblAddTemp
                                          WHERE     Id = @Index
                                        ), 0)
                    IF @TagId > 0 
                        BEGIN
                            IF NOT EXISTS ( SELECT  1
                                            FROM    TagInTopic
                                            WHERE   TagId = @TagId
                                                    AND TopicId = @Id ) 
                                BEGIN
                                    INSERT  INTO TagInTopic
                                            ( TagId, TopicID, Priority )
                                    VALUES  ( @TagId, @Id, @Index + 1 )
                                END
                        END
                    SET @Index = @Index + 1
                END
			
			--Xu ly TopicRelation			
            DELETE  FROM TopicRelation WHERE TopicId = @Id

			IF @RelationTopic <> ''
			BEGIN
				DECLARE @TblTempRelation TABLE (Id int identity(1,1), TopicId int)
				DECLARE @TopicItemId bigint; 
				SET @Index = 0;
				INSERT INTO @TblTempRelation(TopicId) SELECT CONVERT(int, Part) FROM SplitString(@RelationTopic, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempRelation)) 
				BEGIN
					SET @TopicItemId = (SELECT TopicId FROM @TblTempRelation WHERE Id = @Index)
					IF @TopicItemId IS NOT NULL AND @TopicItemId > 0
						IF NOT EXISTS(SELECT TopicRelationId FROM TopicRelation WHERE TopicRelationId = @TopicItemId AND TopicId = @Id)
							INSERT INTO TopicRelation(TopicId, TopicRelationId, Ordinary) VALUES (@Id, @TopicItemId,0);
					SET @Index = @Index + 1
				END
			END
		
            COMMIT TRANSACTION
        END TRY
        BEGIN CATCH
            DECLARE @ErrorMessage NVARCHAR(MAX)
            SET @ErrorMessage = ERROR_MESSAGE()
            RAISERROR (@ErrorMessage, 16, 1);
            ROLLBACK TRANSACTION
        END CATCH
    END

--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetTopicByTopicID]    Script Date: 8/10/2019 9:57:15 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--Edit:21/06/2018
ALTER PROCEDURE [dbo].[CMS_Topic_GetTopicByTopicID] @Id BIGINT
AS 
    BEGIN
        SELECT top 1  T.*,
				TZ.ZoneId
        FROM    Topic T left join TopicInZone TZ on TZ.TopicId=T.Id
        WHERE   ( T.Id = @Id )
    END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Insert]    Script Date: 8/9/2019 5:46:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Insert new tag>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Tag_Insert]
	@Id BIGINT OUTPUT,
	@ParentId BIGINT,
	@Name nvarchar(500),
	@Description nvarchar(250),
	@Url varchar(500),
	@Invisibled bit,
	@IsHotTag bit,
	@Type int,
	@CreatedBy varchar(255) = '',
	@UnsignName varchar(250) = '',
	@IsThread bit = 0,
	@Avatar nvarchar(500) = '',
	@Priority bigint = 0,
	@TagContent ntext,
	@TagTitle nvarchar(500),
	@TagInit nvarchar(500),
	@TagMetaKeyword nvarchar(500),
	@TagMetaContent nvarchar(500),
	@PrimaryZoneId int,
	@OtherZoneId varchar(500),
	@TemplateId tinyint = 1,
	@SubTitle nvarchar(300) = '',
	@CreatedDate datetime,
	@NewsCoverId bigint = 0,
	@IsAmp bit=0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		
		SET @Id = ISNULL((SELECT TOP(1) Id FROM Tag WHERE LOWER(Name) = LOWER(@Name) AND @IsThread=0), 0)
		
		IF @Id = 0
		BEGIN
			SET @Url = dbo.ConvertToBasicLatin(@Name)

			INSERT INTO Tag (ParentId, Name,SubTitle, Description, Url, Invisibled, IsHotTag, Type, CreatedDate, ModifiedDate, CreatedBy, EditedBy, UnsignName, IsThread, Avatar, Priority,TagContent,TagTitle,TagInit,TagMetaKeyword,TagMetaContent,TemplateId,NewsCoverId,IsAmp)
			VALUES (@ParentId,@Name,@SubTitle,@Description,@Url,@Invisibled,@IsHotTag,@Type, @CreatedDate, GETDATE(),@CreatedBy,@CreatedBy,@UnsignName,@IsThread,@Avatar,@Priority,@TagContent,@TagTitle,@TagInit,@TagMetaKeyword,@TagMetaContent,@TemplateId,@NewsCoverId,@IsAmp)
			
			SET @Id = SCOPE_IDENTITY();
			
			/* Insert vào bảng  TagZone*/
			
			INSERT INTO TagZone (TagId, ZoneId, IsPrimary)
			VALUES (@Id, @PrimaryZoneId, 1)
			/* Insert vào bảng  SEOTagZone*/
			INSERT INTO SEOTagZone(TagId, ZoneId,IsPrimary) VALUES (@Id, @PrimaryZoneId,1);
			
			IF @OtherZoneId <> ''
			BEGIN
				DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
				DECLARE @ZoneItemId bigint, @Index int; 
				SET @Index = 0;
				INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
				BEGIN
					SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
					IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
						IF NOT EXISTS(SELECT ZoneId FROM TagZone WHERE ZoneId = @ZoneItemId AND TagId = @Id)
							INSERT INTO TagZone(TagId, ZoneId, IsPrimary) 
							VALUES (@Id, @ZoneItemId, 0);
							
					SET @Index = @Index + 1
				END
			END
		END
		ELSE BEGIN
			-- UPDATE lại name cho tags
			UPDATE Tag
			SET		Name = @Name
			WHERE Id = @Id AND IsThread = @IsThread
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VIETNAMMOI]
--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Update]    Script Date: 8/9/2019 5:47:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Update tag>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Tag_Update]
	@Id BIGINT,
	@ParentId BIGINT,
	@Name nvarchar(500),
	@Description nvarchar(250),
	@Url varchar(500),
	@Invisibled bit,
	@IsHotTag bit,
	@Type int,
	@EditedBy varchar(255) = '',
	@UnsignName varchar(250) = '',
	@IsThread bit = 0,
	@Avatar nvarchar(500) = '',
	@Priority bigint = -1,
	@TagContent ntext,
	@TagTitle nvarchar(500),
	@TagInit nvarchar(500),
	@TagMetaKeyword nvarchar(500),
	@TagMetaContent nvarchar(500),
	@PrimaryZoneId int,
	@OtherZoneId varchar(500),
	@TemplateId tinyint = 1,
	@SubTitle nvarchar(300) = '',
	@CreatedDate datetime,
	@NewsCoverId bigint = 0,
	@IsAmp bit=0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		SET @Url = dbo.ConvertToBasicLatin(@Name)

		UPDATE Tag
		SET ParentId = @ParentId, 
			Name = @Name, 
			Description = @Description, 
			Url = @Url, 
			Invisibled = @Invisibled, 
			IsHotTag = @IsHotTag, 
			Type = @Type, 
            ModifiedDate = GETDATE(), 
            EditedBy = @EditedBy, 
            UnsignName = CASE @UnsignName WHEN '' THEN UnsignName ELSE @UnsignName END, 
            IsThread = @IsThread, 
            Avatar = CASE @Avatar WHEN '' THEN Avatar ELSE @Avatar END, 
            Priority = CASE @Priority WHEN -1 THEN Priority ELSE @Priority END,
            TagContent=@TagContent,
			TagTitle=@TagTitle ,
			TagInit=@TagInit,
			TagMetaKeyword=@TagMetaKeyword ,
			TagMetaContent=@TagMetaContent,
			TemplateId=@TemplateId,
			SubTitle = @SubTitle,
			CreatedDate=CASE @CreatedDate WHEN NULL THEN CreatedDate ELSE @CreatedDate END,
			NewsCoverId	= @NewsCoverId,
			IsAmp=@IsAmp
		WHERE (Id = @Id)
		
		/* Xóa dữ liệu bảng TagZone */
		DELETE FROM TagZone WHERE TagId = @Id
		/* Xóa dữ liệu bảng SEOTagZone */
		DELETE FROM SEOTagZone WHERE TagId = @Id
		
		INSERT INTO TagZone (TagId, ZoneId, IsPrimary)
		VALUES (@Id, @PrimaryZoneId, 1)
		/* Insert vào bảng SEOTagZone */
		INSERT INTO SEOTagZone(TagId, ZoneId) 
							VALUES (@Id, @PrimaryZoneId);
		
		IF @OtherZoneId <> ''
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
			DECLARE @ZoneItemId bigint, @Index int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
					IF NOT EXISTS(SELECT ZoneId FROM TagZone WHERE ZoneId = @ZoneItemId AND TagId = @Id)
						INSERT INTO TagZone(TagId, ZoneId, IsPrimary) 
						VALUES (@Id, @ZoneItemId, 0);
				  				
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

--USE [IMS2_BAODANSINH_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_GetTagByTagID]    Script Date: 8/10/2019 9:52:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--10/08/2019
ALTER PROCEDURE [dbo].[CMS_Tag_GetTagByTagID]
	@Id BIGINT
AS
BEGIN
	SELECT *
	FROM Tag
	WHERE (Id = @Id)
END



