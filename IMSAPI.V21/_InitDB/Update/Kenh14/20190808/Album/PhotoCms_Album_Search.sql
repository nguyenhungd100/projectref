--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Search]    Script Date: 8/12/2019 3:33:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<search album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Search] --'',0,0,-1,null,null,1,20,'',0
	@Keyword nvarchar(250),
	@ZoneId int,
	@EventId int,
	@Status int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@EventId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (A.Name LIKE @Keyword)'
	END
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (A.ZoneId = @ZoneId)'
	IF @EventId > 0
		SET @where = @where + ' AND (AE.EventId = @EventId)'
	IF @Status > -1
		SET @where = @where + ' AND (A.Status = @Status)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (A.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (A.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(distinct a.id) FROM Album AS A '
	SET @search = @search + 'LEFT JOIN AlbumEvents AS AE ON AE.AlbumId=A.Id '
	SET @search = @search + @where	
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT A.*,ZP.Name as ZoneName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY A.CreatedBy DESC) AS RowNum '
	SET @search = @search + '	FROM Album AS A '
	SET @search = @search + '	LEFT JOIN AlbumEvents as AE ON AE.AlbumId=A.Id '
	SET @search = @search + '	LEFT JOIN ZonePhoto as ZP ON ZP.Id=A.ZoneId '
	SET @search = @search + @where
	SET @search = @search + ' group by a.id,a.zoneid,a.name,a.ThumbImage,a.CreatedBy,a.ModifiedBy,a.CreatedDate,a.ModifiedDate,a.status,a.tags,a.description,ZP.Name'
	SET @search = @search + ' ) AS AlbumData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@EventId,
								@Status,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END
