--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_GetById]    Script Date: 8/12/2019 3:32:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<get album by id>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM Album
	WHERE (Id = @Id)
END

