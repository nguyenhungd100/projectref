--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Insert]    Script Date: 8/12/2019 2:17:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<insert new album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Insert]
	@Id int output,
	@ZoneId int,
	@Name nvarchar(250),
	@ThumbImage nvarchar(300),
	@CreatedBy nvarchar(100),		
	@Status int,
	@Tags nvarchar(max),
	@Description nvarchar(500),
	@TagIdList varchar(500),
	@EventIdList varchar(500)
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Album (ZoneId, Name, ThumbImage, CreatedBy, ModifiedBy, CreatedDate, ModifiedDate, Status, Tags, [Description])
		VALUES (@ZoneId, @Name,@ThumbImage,@CreatedBy,@CreatedBy, GETDATE(), GETDATE(), @Status, @Tags, @Description)
		
		SET @Id = SCOPE_IDENTITY()
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumTags WHERE TagID = @TagItemId AND AlbumId = @Id AND TagMode = @TagMode)
						INSERT INTO AlbumTags(AlbumId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END

		IF @EventIdList <> ''
		BEGIN
			DECLARE @TblTempEvent TABLE (Id int identity(1,1), EventId bigint)
			DECLARE @EventItemId bigint; 			
						
			SET @Index = 0;
			INSERT INTO @TblTempEvent(EventId) SELECT CONVERT(bigint, Part) FROM SplitString(@EventIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempEvent)) 
			BEGIN
				SET @EventItemId = (SELECT EventId FROM @TblTempEvent WHERE Id = @Index)
				IF @EventItemId IS NOT NULL AND @EventItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumEvents WHERE EventID = @EventItemId AND AlbumId = @Id)
						INSERT INTO AlbumEvents(AlbumId, EventId, Priority) 
						VALUES (@Id, @EventItemId, @Index);
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager=ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END
