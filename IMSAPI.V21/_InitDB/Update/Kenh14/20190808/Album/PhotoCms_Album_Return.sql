--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Return]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_Return]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

