ALTER Table Album
Add [Description] nvarchar(500)
GO
ALTER Table Album
alter column ThumbImage nvarchar(300)

--USE [IMS2_FULL]
GO

/****** Object:  Table [dbo].[AlbumEvents]    Script Date: 8/12/2019 4:08:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AlbumEvents](
	[AlbumId] [int] NOT NULL,
	[EventId] [int] NOT NULL,
	[Priority] [int] NULL,
 CONSTRAINT [PK_AlbumEvents] PRIMARY KEY CLUSTERED 
(
	[AlbumId] ASC,
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_GetById]    Script Date: 8/12/2019 3:32:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<get album by id>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_GetById]
	@Id int
AS
BEGIN
	SELECT *
	FROM Album
	WHERE (Id = @Id)
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Update]    Script Date: 8/12/2019 3:25:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<chinhnb>
-- Create date: <2019-08-12>
-- Description:	<update album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Update]
	@Id int,
	@ZoneId int,
	@Name nvarchar(250),
	@ThumbImage nvarchar(300),
	@ModifiedBy nvarchar(100),		
	@Status int,
	@Tags nvarchar(max),
	@Description nvarchar(500),
	@TagIdList varchar(500),
	@EventIdList varchar(500)	
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE Album
		SET ZoneId = @ZoneId,
			[Name] = @Name, 
			ThumbImage = @ThumbImage, 
			ModifiedBy = @ModifiedBy, 
			ModifiedDate = GETDATE(),
			[Status]=@Status,
			Tags=@Tags,
			[Description]=@Description
		WHERE (Id = @Id)		
		
		DECLARE @Index int;
		DELETE FROM AlbumTags WHERE AlbumId = @Id
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumTags WHERE TagID = @TagItemId AND AlbumId = @Id AND TagMode = @TagMode)
						INSERT INTO AlbumTags(AlbumId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END
		
		DELETE FROM AlbumEvents WHERE AlbumId = @Id
		IF @EventIdList <> ''
		BEGIN
			DECLARE @TblTempEvent TABLE (Id int identity(1,1), EventId bigint)
			DECLARE @EventItemId bigint; 			
						
			SET @Index = 0;
			INSERT INTO @TblTempEvent(EventId) SELECT CONVERT(bigint, Part) FROM SplitString(@EventIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempEvent)) 
			BEGIN
				SET @EventItemId = (SELECT EventId FROM @TblTempEvent WHERE Id = @Index)
				IF @EventItemId IS NOT NULL AND @EventItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumEvents WHERE EventID = @EventItemId AND AlbumId = @Id)
						INSERT INTO AlbumEvents(AlbumId, EventId, Priority) 
						VALUES (@Id, @EventItemId, @Index);
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager=ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Insert]    Script Date: 8/12/2019 2:17:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<insert new album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Insert]
	@Id int output,
	@ZoneId int,
	@Name nvarchar(250),
	@ThumbImage nvarchar(300),
	@CreatedBy nvarchar(100),		
	@Status int,
	@Tags nvarchar(max),
	@Description nvarchar(500),
	@TagIdList varchar(500),
	@EventIdList varchar(500)
AS
BEGIN
	SET XACT_ABORT ON
	
	BEGIN TRANSACTION

	BEGIN TRY
		INSERT INTO Album (ZoneId, Name, ThumbImage, CreatedBy, ModifiedBy, CreatedDate, ModifiedDate, Status, Tags, [Description])
		VALUES (@ZoneId, @Name,@ThumbImage,@CreatedBy,@CreatedBy, GETDATE(), GETDATE(), @Status, @Tags, @Description)
		
		SET @Id = SCOPE_IDENTITY()
		
		DECLARE @Index int;
			
		IF @TagIdList <> ''
		BEGIN
			DECLARE @TblTempTag TABLE (Id int identity(1,1), TagId bigint)
			DECLARE @TagItemId bigint; 
			DECLARE @TagMode tinyint;
			
			SET @TagMode = 0;
			SET @Index = 0;
			INSERT INTO @TblTempTag(TagId) SELECT CONVERT(bigint, Part) FROM SplitString(@TagIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempTag)) 
			BEGIN
				SET @TagItemId = (SELECT TagId FROM @TblTempTag WHERE Id = @Index)
				IF @TagItemId IS NOT NULL AND @TagItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumTags WHERE TagID = @TagItemId AND AlbumId = @Id AND TagMode = @TagMode)
						INSERT INTO AlbumTags(AlbumId, TagId, TagMode, Priority) 
						VALUES (@Id, @TagItemId, @TagMode, @Index);
				SET @Index = @Index + 1
			END
		END

		IF @EventIdList <> ''
		BEGIN
			DECLARE @TblTempEvent TABLE (Id int identity(1,1), EventId bigint)
			DECLARE @EventItemId bigint; 			
						
			SET @Index = 0;
			INSERT INTO @TblTempEvent(EventId) SELECT CONVERT(bigint, Part) FROM SplitString(@EventIdList, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempEvent)) 
			BEGIN
				SET @EventItemId = (SELECT EventId FROM @TblTempEvent WHERE Id = @Index)
				IF @EventItemId IS NOT NULL AND @EventItemId > 0
					IF NOT EXISTS(SELECT 1 FROM AlbumEvents WHERE EventID = @EventItemId AND AlbumId = @Id)
						INSERT INTO AlbumEvents(AlbumId, EventId, Priority) 
						VALUES (@Id, @EventItemId, @Index);
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
		DECLARE @ErrorMessager varchar(2000)
		SELECT @ErrorMessager=ERROR_MESSAGE()
		RAISERROR (@ErrorMessager,16,1)
	END CATCH
END

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Search]    Script Date: 8/12/2019 3:33:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-12>
-- Description:	<search album>
-- =============================================
ALTER PROCEDURE [dbo].[PhotoCms_Album_Search] --'',0,0,-1,null,null,1,20,'',0
	@Keyword nvarchar(250),
	@ZoneId int,
	@EventId int,
	@Status int,
	@CreatedDateFrom datetime,
	@CreatedDateTo datetime,
	@CreatedBy nvarchar(100),
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	DECLARE @search nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(200),'
	SET @params = @params + '@ZoneId int,'
	SET @params = @params + '@EventId int,'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@CreatedDateFrom datetime,'
	SET @params = @params + '@CreatedDateTo datetime,'
	SET @params = @params + '@CreatedBy varchar(100),'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
	BEGIN
		SET @Keyword = '%' + @Keyword + '%'
		SET @where = @where + ' AND (A.Name LIKE @Keyword)'
	END
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (A.ZoneId = @ZoneId)'
	IF @EventId > 0
		SET @where = @where + ' AND (AE.EventId = @EventId)'
	IF @Status > -1
		SET @where = @where + ' AND (A.Status = @Status)'

	IF @CreatedDateFrom IS NOT NULL AND @CreatedDateTo IS NOT NULL
		SET @where = @where + ' AND (A.CreatedDate BETWEEN @CreatedDateFrom AND @CreatedDateTo)'
	ELSE
		IF @CreatedDateFrom IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate >= @CreatedDateFrom)'
		ELSE IF @CreatedDateTo IS NOT NULL
			SET @where = @where + ' AND (A.CreatedDate <= @CreatedDateTo)'
		
	IF @CreatedBy <> ''
		SET @where = @where + ' AND (A.CreatedBy LIKE @CreatedBy)'
		
	SET @search =			' SELECT @TotalRow = COUNT(distinct a.id) FROM Album AS A '
	SET @search = @search + 'LEFT JOIN AlbumEvents AS AE ON AE.AlbumId=A.Id '
	SET @search = @search + @where	
	SET @search = @search + ' SELECT * FROM'
	SET @search = @search + ' ('
	SET @search = @search + '	SELECT A.*,ZP.Name as ZoneName, '
	SET @search = @search + '		ROW_NUMBER() OVER(ORDER BY A.CreatedBy DESC) AS RowNum '
	SET @search = @search + '	FROM Album AS A '
	SET @search = @search + '	LEFT JOIN AlbumEvents as AE ON AE.AlbumId=A.Id '
	SET @search = @search + '	LEFT JOIN ZonePhoto as ZP ON ZP.Id=A.ZoneId '
	SET @search = @search + @where
	SET @search = @search + ' group by a.id,a.zoneid,a.name,a.ThumbImage,a.CreatedBy,a.ModifiedBy,a.CreatedDate,a.ModifiedDate,a.status,a.tags,a.description,ZP.Name'
	SET @search = @search + ' ) AS AlbumData'
	SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
	
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneId,
								@EventId,
								@Status,
								@CreatedDateFrom,
								@CreatedDateTo,
								@CreatedBy,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END


--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_UnPublish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_UnPublish]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 2
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Send]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_Send]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 3,
		ModifiedDate = GETDATE(),
		ModifiedBy = @UserDoAction 
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Return]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
CREATE PROCEDURE  [dbo].[PhotoCms_Album_Return]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION

--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Album_Publish]    Script Date: 8/14/2019 4:40:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--14/08/2019
ALTER PROCEDURE  [dbo].[PhotoCms_Album_Publish]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Album WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Album.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN		
		UPDATE Album 
		SET [Status] = 1,
			ModifiedDate = GETDATE(),
			ModifiedBy = @UserDoAction
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
