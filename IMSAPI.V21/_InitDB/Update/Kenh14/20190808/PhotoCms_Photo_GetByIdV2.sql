--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_GetByIdV2]    Script Date: 8/13/2019 5:17:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/08/2019
ALTER PROCEDURE [dbo].[PhotoCms_Photo_GetByIdV2] 
 @Id bigint
AS
BEGIN
 SELECT *
 FROM Photo
 WHERE (Id = @Id)
END

