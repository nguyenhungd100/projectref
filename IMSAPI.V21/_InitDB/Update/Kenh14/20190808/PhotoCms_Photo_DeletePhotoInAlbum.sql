--USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[PhotoCms_Photo_DeletePhotoInAlbum]    Script Date: 8/19/2019 4:44:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2019-08-14>
-- Description:	<photo status>
-- =============================================
CREATE PROCEDURE [dbo].[PhotoCms_Photo_DeletePhotoInAlbum]
	@ListPhotoId nvarchar(500)	
AS
BEGIN
	DECLARE @search nvarchar(2000)	
		
	SET @search = 'Update Photo set Status=2 Where Id in(' + @ListPhotoId + ')'
	
	EXEC SP_EXECUTESQL  @search								
END

