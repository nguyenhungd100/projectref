USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_SEOMetaTags_GetList]    Script Date: 04/03/2018 10:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--03/04/2018
ALTER PROCEDURE [dbo].[CMS_SEOMetaTags_GetList]
@Keyword nvarchar(2000),
@ZoneId int ,
@PageIndex int,
@PageSize int,
@TotalRow int out
AS

DECLARE @sql nvarchar(max),
		@where nvarchar(500);
		
DECLARE @lbound int,
		@ubound int,
		@recct  int;

SET @where ='1=1';

IF @ZoneId = -1
	BEGIN
   SET @where= @where+ ' '
   END
ELSE 
BEGIN
    SET @where = @where+ ' AND S.ZoneId='+CONVERT(varchar,@ZoneId)
    END
IF @KeyWord IS NOT NULL AND @KeyWord <>''
BEGIN
SET @KeyWord ='N''%'+@KeyWord +'%''';
SET @where=@where+'AND (S.Name LIKE '+@KeyWord+'OR S.MetaKeyword LIKE '+@KeyWord+'OR S.MetaDescription LIKE '+@KeyWord+')';

END

SET @sql ='SELECT @recct=COUNT(*)FROM SEOMetaTags AS S INNER JOIN Zone AS Z ON S.ZoneId = Z.Id WHERE '+@where
PRINT   @sql
EXEC sp_executeSQL @sql, @params = N'@recct INT OUTPUT', @recct = @recct OUTPUT

	SET @lbound = (@PageIndex - 1)*@PageSize;
	SET @ubound = @lbound + @PageSize
	
	IF @recct IS NOT NULL AND @recct > 0
		SET @TotalRow = @recct;
	ELSE SET @TotalRow = 0;
DECLARE @Field nvarchar(1000);
SET @Field ='[Id]
			 ,[Name]
			 ,[MetaKeyword]
			 ,[MetaDescription]
			 ,[ZoneId]
			 ,[ZoneName]
			
'
SET @sql ='SELECT '+@Field+' FROM (SELECT ROW_NUMBER() OVER(ORDER BY Id DESC)AS row,'+@Field+'
                  FROM(SELECT S.Id, S.Name, S.MetaKeyword, S.MetaDescription, S.ZoneId, Z.Name AS ZoneName FROM SEOMetaTags AS S LEFT JOIN Zone AS Z ON S.ZoneId = Z.Id WHERE '+@where+'
                                
                  
                  )AS TblTemp

)AS TblTemp2 WHERE row > '+CONVERT(varchar(9),@lbound)+'
              AND row<='+CONVERT(varchar(9),@ubound)
   EXEC (@sql);
















