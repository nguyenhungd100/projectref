USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Expert_Search]    Script Date: 03/05/2018 15:35:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--CHINHNB---
--05/03/2018--
CREATE PROCEDURE [dbo].[CMS_Expert_Search_Paging]
    @Keyword NVARCHAR(255) = '',
    @PageIndex int = 1,
	@PageSize int = 10,	
	@TotalRow int = 0 OUTPUT
AS 
BEGIN
    DECLARE @KeyWordLike NVARCHAR(500) ;
    SET @KeyWordLike = '%' + @Keyword + '%' ;
    
    DECLARE @UpperBand int, @LowerBand int

	SELECT @totalRow = COUNT(*) FROM dbo.Expert where ExpertName LIKE @KeyWordLike

	SET @LowerBand  = (@pageIndex - 1) * @PageSize
	SET @UpperBand  = (@pageIndex * @PageSize)
	SELECT * FROM (
	SELECT Id ,
            ExpertName ,
            JobTitle ,
            Description ,
            Avatar ,
            DisplayJobTitle,
	ROW_NUMBER() OVER(ORDER BY Id DESC) AS RowNumber 
	FROM dbo.Expert n 	
	where ExpertName LIKE @KeyWordLike
	) AS temp
	WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
END






