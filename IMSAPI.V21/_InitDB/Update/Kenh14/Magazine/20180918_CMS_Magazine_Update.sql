--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Update]    Script Date: 9/18/2018 2:51:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Update]
	@Title nvarchar(200),
	@Avatar nvarchar(500),
	@Url nvarchar(500),
	@FilePath nvarchar(500),
	@Type int,	
	@PublishedDate datetime,	
	@LastModifiedDate datetime,
	@LastModifiedBy nvarchar(50),
	@Status int,
	@Id int
AS
BEGIN
	UPDATE Magazine 
	SET Title = @Title, 
		Avatar = @Avatar, 
		[Url] = @Url, 
		FilePath = @FilePath, 
		[Type] = @Type, 
		PublishedDate=@PublishedDate,
		LastModifiedBy = @LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		[Status] = @Status
	WHERE (Id = @Id)	
END












