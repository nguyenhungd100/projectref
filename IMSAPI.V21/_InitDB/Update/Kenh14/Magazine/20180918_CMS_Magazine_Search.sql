--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Search]    Script Date: 9/18/2018 2:59:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Search]
	@Keyword nvarchar(300),
	@Status int,
	@Type int,
	@PageIndex int,
	@PageSize int,
	@TotalRow int OUTPUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@Status int,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	-- WHERE
	SET @where = ' WHERE (1 = 1) '
	
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(@Keyword, Title) > 0)'
		
	IF @Status > 0
		SET @where = @where + ' AND (Status = @Status)'
		
	IF @Type > 0
		SET @where = @where + ' AND (Type = @Type)'
	
	SET @order = ' ORDER BY CreatedDate DESC'
		
	-- SELECT TotalRow count
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM Magazine ' + @where + ';'
	
	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10
		
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP ' + CONVERT(varchar(5), @PageSize) + ' Id, Title, Avatar, Url, FilePath, PublishedDate,'
		SET @search = @search + '	Type, Status, CreatedDate, CreatedBy, LastModifiedDate, LastModifiedBy '
		SET @search = @search + ' FROM Magazine '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('		
		SET @search = @search + ' SELECT Id, Title, Avatar, Url, FilePath, PublishedDate,'
		SET @search = @search + '	Type, Status, CreatedDate, CreatedBy, LastModifiedDate, LastModifiedBy, '
		SET @search = @search + '	ROW_NUMBER() OVER (' + @order + ') AS RowNum '
		SET @search = @search + ' FROM Magazine '
		SET @search = @search + @where
		SET @search = @search + ' ) AS MagazineTempTable'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
	
	EXEC SP_EXECUTESQL  @search,@params, 
								@Keyword,
								@Status,
								@Type,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END












