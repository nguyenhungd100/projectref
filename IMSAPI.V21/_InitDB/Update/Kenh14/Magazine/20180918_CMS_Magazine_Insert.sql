--USE [IMS2_VINFAST_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Magazine_Insert]    Script Date: 9/18/2018 2:49:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-18
ALTER PROCEDURE [dbo].[CMS_Magazine_Insert]
	@Title nvarchar(200),
	@Avatar nvarchar(500),
	@Url nvarchar(500),
	@FilePath nvarchar(500),
	@Type int,	
	@PublishedDate datetime,
	@CreatedDate datetime,
	@CreatedBy nvarchar(50),
	@LastModifiedDate datetime,
	@LastModifiedBy nvarchar(50),
	@Status int,
	@Id int OUTPUT
AS
BEGIN
	INSERT INTO Magazine (Title, Avatar, [Url], [FilePath], [Type], [PublishedDate],
		CreatedDate, CreatedBy, LastModifiedBy, LastModifiedDate, [Status])
	VALUES (@Title,@Avatar, @Url, @FilePath, @Type, @PublishedDate, 
		GETDATE(), @CreatedBy, @CreatedBy, GETDATE(), @Status)
		
	SET @Id = SCOPE_IDENTITY()
END












