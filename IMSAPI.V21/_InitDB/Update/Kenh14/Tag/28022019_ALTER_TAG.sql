﻿--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Insert]    Script Date: 2/28/2019 1:20:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Insert new tag>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Tag_Insert]
	@Id BIGINT OUTPUT,
	@ParentId BIGINT,
	@Name nvarchar(500),
	@Description nvarchar(250),
	@Url varchar(500),
	@Invisibled bit,
	@IsHotTag bit,
	@Type int,
	@CreatedBy varchar(255) = '',
	@UnsignName varchar(250) = '',
	@IsThread bit = 0,
	@Avatar nvarchar(500) = '',
	@Priority bigint = 0,
	@TagContent ntext,
	@TagTitle nvarchar(500),
	@TagInit nvarchar(500),
	@TagMetaKeyword nvarchar(500),
	@TagMetaContent nvarchar(500),
	@PrimaryZoneId int,
	@OtherZoneId varchar(500),
	@TemplateId tinyint = 1,
	@SubTitle nvarchar(300) = '',
	@CreatedDate datetime,
	@NewsCoverId bigint = 0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF @CreatedDate IS NULL
			SET @CreatedDate = GETDATE()
		
		SET @Id = ISNULL((SELECT TOP(1) Id FROM Tag WHERE LOWER(Name) = LOWER(@Name) AND @IsThread=0), 0)
		
		IF @Id = 0
		BEGIN
			SET @Url = dbo.ConvertToBasicLatin(@Name)

			INSERT INTO Tag (ParentId, Name,SubTitle, Description, Url, Invisibled, IsHotTag, Type, CreatedDate, ModifiedDate, CreatedBy, EditedBy, UnsignName, IsThread, Avatar, Priority,TagContent,TagTitle,TagInit,TagMetaKeyword,TagMetaContent,TemplateId,NewsCoverId)
			VALUES (@ParentId,@Name,@SubTitle,@Description,@Url,@Invisibled,@IsHotTag,@Type, @CreatedDate, GETDATE(),@CreatedBy,@CreatedBy,@UnsignName,@IsThread,@Avatar,@Priority,@TagContent,@TagTitle,@TagInit,@TagMetaKeyword,@TagMetaContent,@TemplateId,@NewsCoverId)
			
			SET @Id = SCOPE_IDENTITY();
			
			/* Insert vào bảng  TagZone*/
			
			INSERT INTO TagZone (TagId, ZoneId, IsPrimary)
			VALUES (@Id, @PrimaryZoneId, 1)
			/* Insert vào bảng  SEOTagZone*/
			INSERT INTO SEOTagZone(TagId, ZoneId,IsPrimary) VALUES (@Id, @PrimaryZoneId,1);
			
			IF @OtherZoneId <> ''
			BEGIN
				DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
				DECLARE @ZoneItemId bigint, @Index int; 
				SET @Index = 0;
				INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
				WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
				BEGIN
					SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
					IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
						IF NOT EXISTS(SELECT ZoneId FROM TagZone WHERE ZoneId = @ZoneItemId AND TagId = @Id)
							INSERT INTO TagZone(TagId, ZoneId, IsPrimary) 
							VALUES (@Id, @ZoneItemId, 0);
							
					SET @Index = @Index + 1
				END
			END
		END
		ELSE BEGIN
			-- UPDATE lại name cho tags
			UPDATE Tag
			SET		Name = @Name
			WHERE Id = @Id AND IsThread = @IsThread
		END
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_Update]    Script Date: 2/28/2019 1:19:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-10>
-- Description:	<Update tag>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_Tag_Update]
	@Id BIGINT,
	@ParentId BIGINT,
	@Name nvarchar(500),
	@Description nvarchar(250),
	@Url varchar(500),
	@Invisibled bit,
	@IsHotTag bit,
	@Type int,
	@EditedBy varchar(255) = '',
	@UnsignName varchar(250) = '',
	@IsThread bit = 0,
	@Avatar nvarchar(500) = '',
	@Priority bigint = -1,
	@TagContent ntext,
	@TagTitle nvarchar(500),
	@TagInit nvarchar(500),
	@TagMetaKeyword nvarchar(500),
	@TagMetaContent nvarchar(500),
	@PrimaryZoneId int,
	@OtherZoneId varchar(500),
	@TemplateId tinyint = 1,
	@SubTitle nvarchar(300) = '',
	@CreatedDate datetime,
	@NewsCoverId bigint = 0
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		SET @Url = dbo.ConvertToBasicLatin(@Name)

		UPDATE Tag
		SET ParentId = @ParentId, 
			Name = @Name, 
			Description = @Description, 
			Url = @Url, 
			Invisibled = @Invisibled, 
			IsHotTag = @IsHotTag, 
			Type = @Type, 
            ModifiedDate = GETDATE(), 
            EditedBy = @EditedBy, 
            UnsignName = CASE @UnsignName WHEN '' THEN UnsignName ELSE @UnsignName END, 
            IsThread = @IsThread, 
            Avatar = CASE @Avatar WHEN '' THEN Avatar ELSE @Avatar END, 
            Priority = CASE @Priority WHEN -1 THEN Priority ELSE @Priority END,
            TagContent=@TagContent,
			TagTitle=@TagTitle ,
			TagInit=@TagInit,
			TagMetaKeyword=@TagMetaKeyword ,
			TagMetaContent=@TagMetaContent,
			TemplateId=@TemplateId,
			SubTitle = @SubTitle,
			CreatedDate=CASE @CreatedDate WHEN NULL THEN CreatedDate ELSE @CreatedDate END,
			NewsCoverId	= @NewsCoverId
		WHERE (Id = @Id)
		
		/* Xóa dữ liệu bảng TagZone */
		DELETE FROM TagZone WHERE TagId = @Id
		/* Xóa dữ liệu bảng SEOTagZone */
		DELETE FROM SEOTagZone WHERE TagId = @Id
		
		INSERT INTO TagZone (TagId, ZoneId, IsPrimary)
		VALUES (@Id, @PrimaryZoneId, 1)
		/* Insert vào bảng SEOTagZone */
		INSERT INTO SEOTagZone(TagId, ZoneId) 
							VALUES (@Id, @PrimaryZoneId);
		
		IF @OtherZoneId <> ''
		BEGIN
			DECLARE @TblTempZone TABLE (Id int identity(1,1), ZoneId bigint)
			DECLARE @ZoneItemId bigint, @Index int; 
			SET @Index = 0;
			INSERT INTO @TblTempZone(ZoneId) SELECT CONVERT(bigint, Part) FROM SplitString(@OtherZoneId, ';')
			WHILE(@Index <= (SELECT COUNT(Id) FROM @TblTempZone)) 
			BEGIN
				SET @ZoneItemId = (SELECT ZoneId FROM @TblTempZone WHERE Id = @Index)
				IF @ZoneItemId IS NOT NULL AND @ZoneItemId > 0
					IF NOT EXISTS(SELECT ZoneId FROM TagZone WHERE ZoneId = @ZoneItemId AND TagId = @Id)
						INSERT INTO TagZone(TagId, ZoneId, IsPrimary) 
						VALUES (@Id, @ZoneItemId, 0);
				  				
				SET @Index = @Index + 1
			END
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END
