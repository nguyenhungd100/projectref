--USE [IMS2_VIETNAMBIZ]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_SearchInBoxTag]    Script Date: 2/12/2019 2:09:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_Tag_SearchInBoxTag]
	@Keyword nvarchar(500),
	@IsHotTag int = -1,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUT
AS
BEGIN
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
	
	SET @TotalRow = (SELECT COUNT(1) FROM Tag T
					WHERE (Invisibled<>1) and (@IsHotTag < 0 OR IsHotTag = @IsHotTag) AND (@Keyword = '' OR PATINDEX(@Keyword, T.Name) > 0)
					AND EXISTS (SELECT TagId FROM TagNews TN WHERE T.Id = TN.TagId));
					
	SELECT * FROM (
					SELECT Id, Name, Avatar, ROW_NUMBER() OVER(ORDER BY COUNT(NewsId) DESC) AS RowNum,
						SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount
						FROM Tag T
					INNER JOIN TagNews TN ON T.Id = TN.TagId
					WHERE (Invisibled<>1) and (@IsHotTag < 0 OR IsHotTag = @IsHotTag) AND (@Keyword = '' OR PATINDEX(@Keyword, T.Name) > 0)
					GROUP BY Id,Name,Avatar
					)AS TagTable
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
END










