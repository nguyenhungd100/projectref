USE [IMS2_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_StickerNews_GetByNewsId]    Script Date: 03/12/2018 16:17:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[CMS_StickerNews_GetByNewsId] @NewsId BIGINT
AS 
    BEGIN
        SELECT  a.*,b.NewsId
        FROM    Sticker a
                INNER JOIN StickerInNews b ON a.Id = b.StickerId
                                              AND b.NewsId = @NewsId
    END


