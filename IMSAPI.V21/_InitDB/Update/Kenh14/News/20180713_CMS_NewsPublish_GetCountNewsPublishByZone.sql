--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_GetCountNewsPublishByZone]    Script Date: 07/13/2018 17:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--13/07/2018
CREATE PROCEDURE [dbo].[CMS_NewsPublish_GetCountNewsPublishByZone]
    @ZoneId int,
    @FromDate datetime,
    @EndDate datetime
AS 
BEGIN
    SELECT 0 as Id, COUNT(*) as TotalRow
    FROM    dbo.NewsPublish
    WHERE   ZoneId = @ZoneId and DistributionDate BETWEEN @FromDate AND @EndDate
END