--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_Delete]    Script Date: 12/25/2018 3:45:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2018-12-25>
-- Description:	<delete news>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_DeleteNews]
	@Id bigint
AS
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		DELETE FROM NewsRelation WHERE NewsId = @Id
		DELETE FROM TagNews WHERE NewsID = @Id	
		DELETE FROM NewsInZone WHERE NewsId = @Id
		DELETE FROM News WHERE Id = @Id
		DELETE FROM NewsPublish WHERE NewsId = @Id
		DELETE FROM NewsContent WHERE NewsId = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END


















