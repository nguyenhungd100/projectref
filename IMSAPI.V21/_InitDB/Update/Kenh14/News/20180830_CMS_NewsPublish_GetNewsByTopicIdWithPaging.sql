USE [IMS2_SUCKHOEHANGNGAY_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTopicIdWithPaging]    Script Date: 8/30/2018 2:11:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--30/08/2018
CREATE PROCEDURE [dbo].[CMS_NewsPublish_GetNewsByTopicIdWithPaging]
	@TopicId bigint,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
BEGIN
	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
	WHERE TN.TopicID = @TopicId and N.Status=8

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url,
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
		WHERE TN.TopicId = @TopicId and N.Status=8
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC
END




