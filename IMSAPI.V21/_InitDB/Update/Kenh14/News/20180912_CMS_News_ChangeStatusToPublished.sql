--USE [IMS2_TOQUOC_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToPublished]    Script Date: 9/12/2018 5:31:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-08-08>
-- Edite:<12-09-2018>
-- Description:	<change news status to Published version 2>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToPublished]
	@Id bigint,
	@PublishedBy varchar(200),
	@NewsPositionTypeForHome int = 0,
	@NewsPositionOrderForHome int = 0,
	@NewsPositionTypeForList int = 0,
	@NewsPositionOrderForList int = 0,
	@PublishedDate bigint,
	@PublishedContent ntext = null
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @NewsInZoneTemp TABLE(ZoneId int, IsPrimary bit)
		DECLARE @PrimaryZoneId int,
				@OldDistributionDate datetime,		
				@Title nvarchar(250),
				@SubTitle nvarchar(250),
				@Sapo nvarchar(MAX),
				@Avatar nvarchar(500),
				@AvatarDesc nvarchar(500),
				@Avatar2 nvarchar(500),
				@Avatar3 nvarchar(500),
				@Avatar4 nvarchar(500),
				@Avatar5 nvarchar(500),
				@Author nvarchar(500),
				@NewsRelation nvarchar(MAX),
				@Source nvarchar(100),
				@IsFocus bit,
				@Type tinyint,
				@ThreadId int,
				@DistributionDate datetime,
				@Tag nvarchar(MAX),
				@TagPrimary nvarchar(500),
				@DisplayStyle int,
				@DisplayPosition int,
				@DisplayInSlide int,
				@AvatarCustom varchar(500),
				@OriginalId int,
				@NewsType tinyint,
				@IsOnHome bit,
				@Url nvarchar(500),
				@TagItem nvarchar(1000),
				@InitSapo nvarchar(500),
				@OriginalUrl nvarchar(500),
				@InterviewId int,
				@IsBreakingNews bit,
				@IsPr bit,
				@AdStore bit,
				@AdStoreUrl nvarchar(500),
				@IsOnMobile bit,
				@RollingNewsId int,
				@TagSubTitleId int,
				@LocationType int,
				@ExpiredDate datetime,
				@SourceUrl nvarchar(500),
				@PrPosition int,
				@ShortTitle nvarchar(250),
				@Priority int,
				@ParentNewsId bigint
				
		INSERT INTO @NewsInZoneTemp (ZoneId, IsPrimary)
		SELECT ZoneId, IsPrimary
		FROM NewsInZone
		WHERE NewsId = @Id
		
		SELECT @PrimaryZoneId = ZoneId
		FROM @NewsInZoneTemp
		WHERE IsPrimary = 1

		SELECT  @Title = Title,
				@SubTitle = SubTitle,
				@Sapo = Sapo,
				@Avatar = Avatar,
				@AvatarDesc = AvatarDesc,
				@Avatar2 = Avatar2,
				@Avatar3 = Avatar3,
				@Avatar4 = Avatar4,
				@Avatar5 = Avatar5,
				@Author = Author,
				@NewsRelation = NewsRelation,
				@Source = Source,
				@IsFocus = IsFocus,
				@Type = Type,
				@ThreadId = ThreadId,
				@DistributionDate = ISNULL(DistributionDate, GETDATE()),
				@Tag = Tag,
				@TagPrimary = TagPrimary,
				@DisplayStyle = DisplayStyle,
				@DisplayPosition = DisplayPosition,
				@DisplayInSlide = DisplayInSlide,
				@AvatarCustom = AvatarCustom,
				@OriginalId = OriginalId,
				@NewsType = NewsType,
				@IsOnHome = IsOnHome,
				@Url = Url,
				@TagItem = TagItem,
				@InitSapo = InitSapo,
				@OriginalUrl = OriginalUrl,
				@InterviewId = InterviewId,
				@IsBreakingNews = IsBreakingNews,
				@IsPr = IsPr,
				@AdStore = AdStore,
				@AdStoreUrl = AdStoreUrl,
				@IsOnMobile = IsOnMobile,
				@RollingNewsId = RollingNewsId,
				@TagSubTitleId = TagSubTitleId,
				@LocationType = LocationType,
				@ExpiredDate = ExpiredDate,
				@SourceUrl = SourceUrl,
				@PrPosition = PrPosition,
				@ShortTitle = ShortTitle,
				@Priority = Priority,
				@ParentNewsId=ParentNewsId
		FROM News
		WHERE Id = @Id

		IF @ShortTitle IS NULL OR @ShortTitle = ''
			SET @ShortTitle = @Title

		IF @PublishedContent IS NULL
			SELECT @PublishedContent = Body FROM News WHERE Id = @Id

		SET @PublishedDate = dbo.UNIX_TIMESTAMP(@DistributionDate)

		SELECT @OldDistributionDate = PublishedDate
		FROM NewsContent
		WHERE NewsId = @Id
		PRINT 1
		IF @OldDistributionDate IS NULL
		BEGIN
			SET @OldDistributionDate = '1800-01-01'
			PRINT 2
			INSERT INTO NewsContent
				(NewsId, ZoneId, Title, SubTitle, Sapo, Body, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, Avatar5, Author, NewsRelation, Source, Type, ThreadId, 
				PublishedDate, Tags, TagPrimary, OriginalId, Url, TagItem, InitSapo, OriginalUrl, InterviewId, AdStore, AdStoreUrl, 
				RollingNewsId, TagSubTitleId, LocationType, ExpiredDate, SourceUrl, PrPosition, NewsType, ParentNewsId)
			VALUES (@Id,@PrimaryZoneId,@Title,@SubTitle,@Sapo, @PublishedContent, @Avatar, @AvatarDesc, @Avatar2, @Avatar3, @Avatar4, @Avatar5, @Author, @NewsRelation, @Source, @Type, @ThreadId, 
				@DistributionDate, @Tag, @TagPrimary, @OriginalId, @Url, @TagItem, @InitSapo, @OriginalUrl, @InterviewId, @AdStore, @AdStoreUrl, 
				@RollingNewsId, @TagSubTitleId, @LocationType, @ExpiredDate, @SourceUrl, @PrPosition, @NewsType, @ParentNewsId)
		END
		ELSE
			PRINT 3
			UPDATE NewsContent
			SET ZoneId = @PrimaryZoneId,
				Title = @Title,
				SubTitle = @SubTitle,
				Sapo = @Sapo,
				Body = @PublishedContent,
				Avatar = @Avatar,
				AvatarDesc = @AvatarDesc,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				Author = @Author,
				NewsRelation = @NewsRelation,
				Source = @Source,
				Type = @Type,
				ThreadId = @ThreadId,
				PublishedDate = @DistributionDate,
				Tags = @Tag,
				TagPrimary = @TagPrimary,
				OriginalId = @OriginalId,
				Url = @Url,
				TagItem = @TagItem,
				InitSapo = @InitSapo,
				OriginalUrl = @OriginalUrl,
				InterviewId = @InterviewId,
				AdStore = @AdStore,
				AdStoreUrl = @AdStoreUrl,
				RollingNewsId = @RollingNewsId,
				TagSubTitleId = @TagSubTitleId,
				LocationType = @LocationType,
				ExpiredDate = @ExpiredDate,
				SourceUrl = @SourceUrl,
				PrPosition = @PrPosition,
				NewsType = @NewsType,
				ParentNewsId=@ParentNewsId
			WHERE NewsId = @Id
		
		DECLARE @RemovedZoneIds TABLE(ZoneId int)
		INSERT INTO @RemovedZoneIds (ZoneId)
		SELECT ZoneId
		FROM NewsPublish
		WHERE (NewsId = @Id) AND ZoneId NOT IN (SELECT ZoneId FROM @NewsInZoneTemp)
		
		DELETE FROM NewsPublish
		WHERE (NewsId = @Id) AND ZoneId NOT IN (SELECT ZoneId FROM @NewsInZoneTemp)
		
		DECLARE @CurrentNewsInZoneTemp TABLE(ZoneId int)
		
		INSERT INTO @CurrentNewsInZoneTemp (ZoneId)
		SELECT ZoneId
		FROM NewsPublish
		WHERE NewsId = @Id
		
		PRINT 4
		UPDATE NewsPublish
		SET IsPrimary = 0,
			LastModifiedDate = GETDATE(),
			PublishedDate = @PublishedDate,			
			Title = @ShortTitle,
			TitleDetail = @Title,
			SubTitle = @SubTitle,
			Sapo = @Sapo,
			Avatar = @Avatar,
			AvatarDesc = @AvatarDesc,
			Avatar2 = @Avatar2,
			Avatar3 = @Avatar3,
			Avatar4 = @Avatar4,
			Avatar5 = @Avatar5,
			Author = @Author,
			NewsRelation = @NewsRelation,
			Source = @Source,
			IsFocus = @IsFocus,
			Type = @Type,
			ThreadId = @ThreadId,
			DistributionDate = @DistributionDate,
			DisplayStyle = @DisplayStyle,
			DisplayPosition = @DisplayPosition,
			DisplayInSlide = @DisplayInSlide,
			AvatarCustom = @AvatarCustom,
			OriginalId = @OriginalId,
			NewsType = @NewsType,
			IsOnMobile = @IsOnMobile,
			Url = @Url,
			TagItem = @TagItem,
			InitSapo = @InitSapo,
			OriginalUrl = @OriginalUrl,
			InterviewId = @InterviewId,
			IsBreakingNews = @IsBreakingNews,
			IsPr = @IsPr,
			AdStore = @AdStore,
			AdStoreUrl = @AdStoreUrl,
			IsOnHome = @IsOnHome,
			RollingNewsId = @RollingNewsId,
			TagSubTitleId = @TagSubTitleId,
			LocationType = @LocationType,
			ExpiredDate = @ExpiredDate,
			PrPosition = @PrPosition,
			PrimaryZoneId = @PrimaryZoneId,
			Priority = @Priority,
			ParentNewsId=@ParentNewsId
		WHERE NewsId = @Id
		PRINT 5
		INSERT INTO NewsPublish (NewsId, ZoneId, Title, 
			SubTitle, Sapo, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, Avatar5, Author, 
			NewsRelation, Source, IsFocus, Type, ThreadId, DistributionDate, DisplayStyle, 
			DisplayPosition, DisplayInSlide, AvatarCustom, OriginalId, NewsType, IsOnMobile, 
			Url, TagItem, InitSapo, OriginalUrl, InterviewId, IsBreakingNews, IsPr, AdStore, 
			AdStoreUrl, IsOnHome, RollingNewsId, TagSubTitleId, LastModifiedDate, PublishedDate, 
			IsPrimary, LocationType, ExpiredDate, PrPosition, PrimaryZoneId, TitleDetail, Priority, ParentNewsId)
		SELECT @Id, ZoneId, @ShortTitle, 
			@SubTitle, @Sapo, @Avatar, @AvatarDesc, @Avatar2, @Avatar3, @Avatar4, @Avatar5, @Author, 
			@NewsRelation, @Source, @IsFocus, @Type, @ThreadId, @DistributionDate, @DisplayStyle, 
			@DisplayPosition, @DisplayInSlide, @AvatarCustom, @OriginalId, @NewsType, @IsOnMobile, 
			@Url, @TagItem, @InitSapo, @OriginalUrl, @InterviewId, @IsBreakingNews, @IsPr, @AdStore, 
			@AdStoreUrl, @IsOnHome, @RollingNewsId, @TagSubTitleId, GETDATE(), @PublishedDate, 
			0, @LocationType, @ExpiredDate, @PrPosition, @PrimaryZoneId, @Title, @Priority, @ParentNewsId
		FROM @NewsInZoneTemp
		WHERE ZoneId NOT IN (SELECT ZoneId FROM @CurrentNewsInZoneTemp)
		PRINT 6
		UPDATE NewsPublish SET IsPrimary = 1
		WHERE (NewsId = @Id) AND (ZoneId = @PrimaryZoneId)
		PRINT 7
		UPDATE News
		SET [Status] = 8, LastModifiedDate = GETDATE(),
			EditedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE EditedBy END,
			LastModifiedBy = @PublishedBy,
			PublishedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE PublishedBy END,
			DistributionDate = @DistributionDate
		WHERE Id = @Id

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END
