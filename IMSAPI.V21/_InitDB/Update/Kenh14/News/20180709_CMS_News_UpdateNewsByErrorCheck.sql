USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateNewsByErrorCheck]    Script Date: 07/10/2018 12:12:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit:09/07/2018
ALTER PROCEDURE [dbo].[CMS_News_UpdateNewsByErrorCheck]
	@NewsId bigint,	
	@NewsTitle nvarchar(300),
	@NewsSubTitle nvarchar(250),
	@NewsSapo nvarchar(500),
	@NewsBody nvarchar(MAX),
	@NewsUrl varchar(500),
	@NewsAvatarDesc nvarchar(500),
	@NewsNote nvarchar(2000),
	@NewsTag nvarchar(MAX),
	@AccountName varchar(200),
	@Avatar nvarchar(500),
	@Avatar2 nvarchar(500),
	@Avatar3 nvarchar(500),
	@Avatar4 nvarchar(500),
	@Avatar5 nvarchar(500),
	@TagSubTitleId int,
	@ShortTitle nvarchar(250)
AS
	BEGIN
		-- BEGIN TRANSACTION 
		BEGIN TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
			-- UPDATE News
			UPDATE News SET 
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc,
				Note = @NewsNote,
				Tag = @NewsTag,
				ErrorCheckedBy = @AccountName,
				ErrorCheckedDate = GETDATE(),
				Avatar = @Avatar,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				TagSubTitleId = @TagSubTitleId,
				ShortTitle=@ShortTitle
			WHERE Id = @NewsId
			
			-- UPDATE NewsPublish
			UPDATE NewsPublish SET
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc,
				Avatar = @Avatar,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				TagSubTitleId = @TagSubTitleId
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsPosition
			UPDATE NewsPosition SET
				Title = @NewsTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc,
				Avatar = @Avatar,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				TagSubTitleId = @TagSubTitleId
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsContent				
			UPDATE NewsContent SET
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc,
				Avatar = @Avatar,
				Avatar2 = @Avatar2,
				Avatar3 = @Avatar3,
				Avatar4 = @Avatar4,
				Avatar5 = @Avatar5,
				TagSubTitleId = @TagSubTitleId
			WHERE NewsId = @NewsId
			
			--SELECT Id, Title, SubTitle, Sapo, Body, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, 
			--	Avatar5, Author, NewsRelation, Status, Source, IsFocus, Type, ThreadId, CreatedDate, 
			--	LastModifiedDate, ISNULL(DistributionDate, GETDATE()) AS DistributionDate, CreatedBy, 
			--	LastModifiedBy, PublishedBy, EditedBy, LastReceiver, WordCount, ViewCount, Priority, 
			--	Tag, OriginalId, IsOnHome, DisplayInSlide, DisplayPosition,Url, TagItem, InitSapo, DisplayStyle
			--INTO #NewsData
			--FROM News
			--WHERE (Id = @NewsId)
			
			
			--DROP TABLE #NewsData
		-- COMMIT
		COMMIT TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
		IF @@ERROR <> 0
		BEGIN
			DECLARE @Message nvarchar(500)
			SET @Message = ERROR_MESSAGE();
			RAISERROR (@Message, 16, 1)
			ROLLBACK TRANSACTION
		END
	END