USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_SearchNewsPublishExcludeNewsInThread]    Script Date: 06/06/2018 16:13:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 06/06/2018
ALTER PROCEDURE [dbo].[CMS_News_SearchNewsPublishExcludeNewsInThread]
	@ZoneId int,
	@Keyword nvarchar(200),
	@ThreadId int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	--DECLARE @CurrentNewsInThread TABLE(NewsId bigint)
	--INSERT INTO @CurrentNewsInThread (NewsId) SELECT NewsId FROM ThreadNews WHERE ThreadId = @ThreadId

	SELECT @TotalRow = COUNT(*) 
	FROM NewsPublish
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
		AND (NewsId NOT IN (SELECT NewsId FROM ThreadNews WHERE ThreadId = @ThreadId))

	SELECT * FROM
	(
		SELECT NewsId AS Id, Title, Avatar, DistributionDate, Url, 
			row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
		FROM NewsPublish
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
			AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
			AND (NewsId NOT IN (SELECT NewsId FROM ThreadNews WHERE ThreadId = @ThreadId)) AND (DistributionDate <= GETDATE())
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--declare @Total int
--exec [CMS_News_SearchNewsPublishExcludeNewsInThread] -1,'',692,1,20,@TotalRow=@Total output
--select @Total



















