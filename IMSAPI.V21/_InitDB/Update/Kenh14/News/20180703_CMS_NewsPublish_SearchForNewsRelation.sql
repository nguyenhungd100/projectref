USE [IMS2_FULL]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_SearchForNewsRelation]    Script Date: 07/03/2018 11:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author	  :	<ThanhTN>
-- Create date: <2012-09-17>
--Edit: chinhnb 03/07/2018
-- Description:	<Search news for news relation>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPublish_SearchForNewsRelation]
	@ZoneId int,
	@Keyword nvarchar(200),
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
	IF @ZoneId < 0
		SET @ZoneId = 0

	DECLARE @ZoneIds varchar(100)
	IF @ZoneId > 0
		SET @ZoneIds = ',' + dbo.CMS_fGetListOfZoneIdByParentZoneId(@ZoneId) + ','
	ELSE
		SET @ZoneIds = ''

	IF @Keyword <> ''
	BEGIN
		IF LEFT(@Keyword, 1) = '"' AND RIGHT(@Keyword, 1) = '"'
			SET @Keyword = '%' + SUBSTRING(@Keyword, 2, LEN(@Keyword) - 2) + '%'
		ELSE
			SET @Keyword = '%' + REPLACE(@Keyword, ' ', '%') + '%'
	END

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	IF @ZoneIds = ''
	BEGIN
		SELECT @TotalRow = 1000 

		SELECT * FROM
		(
			SELECT NewsId, Title, Url, Avatar, Avatar as Avatar4, Sapo, DistributionDate,
				row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
			FROM newspublish
			WHERE IsPrimary = 1
				AND (DistributionDate <= GETDATE())				
				AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) 
		) AS NewsData
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	END
	ELSE
	BEGIN
		DECLARE @TblTempZone TABLE(ZoneId int)
		INSERT @TblTempZone (ZoneId)
		SELECT CONVERT(int, Part) FROM SplitString(@ZoneIds, ',');

		SELECT @TotalRow = 1000 

		SELECT * FROM
		(
			SELECT NewsId, Title, Url, Avatar, Avatar as Avatar4, Sapo, DistributionDate, 
				row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
			FROM newspublish INNER JOIN @TblTempZone AS Z
				ON newspublish.ZoneId=Z.ZoneId
			WHERE (DistributionDate <= GETDATE())				
				AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) 
			GROUP BY NewsId, Title, Url, Avatar, Sapo, DistributionDate
		) AS NewsData
		WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	END
		

END





















