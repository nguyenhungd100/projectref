USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTopic]    Script Date: 06/06/2018 13:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 06/06/2018
ALTER PROCEDURE [dbo].[CMS_News_SearchNewsPublishExcludeNewsInTopic]
	@ZoneId int,
	@Keyword nvarchar(200),
	@TopicId int,
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'
		
	--DECLARE @CurrentNewsInTopic TABLE(NewsId bigint)
	--INSERT INTO @CurrentNewsInTopic (NewsId) SELECT NewsId FROM NewsInTopic WHERE TopicId = @TopicId

	SELECT @TotalRow = COUNT(*) 
	FROM NewsPublish
	WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
		AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
		AND (NewsId NOT IN (SELECT NewsId FROM NewsInTopic WHERE TopicId = @TopicId))

	SELECT * FROM
	(
		SELECT NewsId AS Id, Title, Avatar, DistributionDate, Url, 
			row_number() OVER (ORDER BY DistributionDate DESC) AS RowNum
		FROM NewsPublish
		WHERE (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
			AND ((@ZoneId <= 0 AND IsPrimary = 1) OR (@ZoneId > 0 AND ZoneId = @ZoneId))
			AND (NewsId NOT IN (SELECT NewsId FROM NewsInTopic WHERE TopicId = @TopicId)) AND (DistributionDate <= GETDATE())
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC

--declare @Total int
--exec [CMS_News_SearchNewsPublishExcludeNewsInTopic] -1,'',12,1,20,@TotalRow=@Total output
--select @Total
