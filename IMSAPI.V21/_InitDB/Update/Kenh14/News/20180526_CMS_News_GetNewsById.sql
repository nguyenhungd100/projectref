USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsById]    Script Date: 05/26/2018 10:56:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author	  :	<ThanhTN>
-- Edit: <chinhnb 26/05/2018> 
-- Create date: <2012-09-17>
-- Description:	<Search news for news relation>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_GetNewsById] @Id BIGINT
AS
BEGIN
    SELECT  *,dbo.CMS_fGetListOfZoneIdByNewsId(@Id) AS ListZoneId
    FROM    News
    WHERE   ( Id = @Id );
END;











