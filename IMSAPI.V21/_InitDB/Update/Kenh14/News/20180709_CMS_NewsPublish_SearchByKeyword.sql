USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsPublish_SearchByKeyword]    Script Date: 07/09/2018 16:42:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author   : <CHINHNB>
-- Create date: <2018-03-27>
-- Description: <Search news publish by keyword and zoneid>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPublish_SearchByKeyword]
    @ZoneId INT = 0,
    @Keyword NVARCHAR(200) = '',
	@DisplayPosition int = -1,
	@Priority int = -1,
	@IsShowNewsBomd bit = 0,
    @PageIndex INT = 1,
    @PageSize INT = 10
AS 
BEGIN
    IF @ZoneId < 0 
        SET @ZoneId = 0

    DECLARE @ZoneIds VARCHAR(100)
    IF @ZoneId > 0 
        SET @ZoneIds = ','
            + dbo.CMS_fGetListOfZoneIdByParentZoneId(@ZoneId) + ','
    ELSE 
        SET @ZoneIds = ''

    IF @Keyword <> '' 
        SET @Keyword = '%' + @Keyword + '%'

    IF @PageIndex = 0 
        SET @PageIndex = 1

    IF @PageSize = 0 
        SET @PageSize = 10

    IF @ZoneIds = '' 
    BEGIN
		IF @PageIndex = 1
			SELECT TOP(@PageSize) ZoneId, NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source
			FROM NewsPublish
			WHERE IsPrimary = 1
				AND (@IsShowNewsBomd = 1 OR (@IsShowNewsBomd = 0 AND DistributionDate <= GETDATE()))
				AND (@DisplayPosition < 0 OR (@DisplayPosition >= 0 AND DisplayPosition = @DisplayPosition))
				AND (@Priority < 0 OR (@Priority = 3 AND (Priority = 1 OR Priority = 2)) OR (@Priority >= 0 AND @Priority <> 3 AND Priority = @Priority))
				AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
			ORDER BY DistributionDate DESC
		ELSE
			SELECT  * FROM (
				SELECT ZoneId, NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source,
					ROW_NUMBER() OVER ( ORDER BY DistributionDate DESC ) AS RowNum
				FROM NewsPublish
				WHERE IsPrimary = 1
					AND (@IsShowNewsBomd = 1 OR (@IsShowNewsBomd = 0 AND DistributionDate <= GETDATE()))
					AND (@DisplayPosition < 0 OR (@DisplayPosition >= 0 AND DisplayPosition = @DisplayPosition))
					AND (@Priority < 0 OR (@Priority = 3 AND (Priority = 1 OR Priority = 2)) OR (@Priority >= 0 AND @Priority <> 3 AND Priority = @Priority))
					AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
				) AS NewsData
			WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
			ORDER BY DistributionDate DESC
    END
    ELSE 
    BEGIN
        DECLARE @TblTempZone TABLE (ZoneId INT)
        INSERT  @TblTempZone (ZoneId)
        SELECT  CONVERT(INT, Part)
        FROM    SplitString(@ZoneIds, ',');

		IF @PageIndex = 1
			SELECT DATA.*, Z.ZoneId
			FROM (
				SELECT TOP(@PageSize) NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source
				FROM NewsPublish
					INNER JOIN @TblTempZone AS Z ON NewsPublish.ZoneId = Z.ZoneId
				WHERE (@IsShowNewsBomd = 1 OR (@IsShowNewsBomd = 0 AND DistributionDate <= GETDATE()))
					AND (@DisplayPosition < 0 OR (@DisplayPosition >= 0 AND DisplayPosition = @DisplayPosition))
					AND (@Priority < 0 OR (@Priority = 3 AND (Priority = 1 OR Priority = 2)) OR (@Priority >= 0 AND @Priority <> 3 AND Priority = @Priority))
					AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
				GROUP BY NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source
				ORDER BY DistributionDate DESC
			) AS DATA INNER JOIN NewsInZone AS Z ON DATA.NewsId = Z.NewsId AND Z.IsPrimary = 1
			ORDER BY DistributionDate DESC
		ELSE
			SELECT DATA.*, Z.ZoneId
			FROM (
				SELECT * FROM (
					SELECT NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source,
						ROW_NUMBER() OVER ( ORDER BY DistributionDate DESC ) AS RowNum
					FROM NewsPublish
						INNER JOIN @TblTempZone AS Z ON NewsPublish.ZoneId = Z.ZoneId
					WHERE (@IsShowNewsBomd = 1 OR (@IsShowNewsBomd = 0 AND DistributionDate <= GETDATE()))
						AND (@DisplayPosition < 0 OR (@DisplayPosition >= 0 AND DisplayPosition = @DisplayPosition))
						AND (@Priority < 0 OR (@Priority = 3 AND (Priority = 1 OR Priority = 2)) OR (@Priority >= 0 AND @Priority <> 3 AND Priority = @Priority))
						AND (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
					GROUP BY NewsId, Title, Url, Avatar, DistributionDate, OriginalId, Source
				) AS NewsData
				WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
			) AS DATA INNER JOIN NewsInZone AS Z ON DATA.NewsId = Z.NewsId AND Z.IsPrimary = 1
			ORDER BY DistributionDate DESC
    END
END