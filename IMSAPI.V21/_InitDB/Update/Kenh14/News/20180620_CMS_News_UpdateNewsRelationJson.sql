USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateNewsRelationJson]    Script Date: 06/20/2018 11:41:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_News_UpdateNewsRelationJson]
    @Id BIGINT ,
    @NewsRelation NVARCHAR(MAX)
AS 
    BEGIN
        UPDATE  News
        SET     NewsRelation = @NewsRelation
        WHERE   Id = @Id
        
        UPDATE  NewsContent
        SET     NewsRelation = @NewsRelation
        WHERE   NewsId = @Id
        
        UPDATE  NewsPublish
        SET     NewsRelation = @NewsRelation
        WHERE   NewsId = @Id
    END










