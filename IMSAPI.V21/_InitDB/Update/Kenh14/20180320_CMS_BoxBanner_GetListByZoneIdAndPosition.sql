USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxBanner_GetListByZoneIdAndPosition]    Script Date: 03/20/2018 11:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2013-10-22>
-- Edited 2018-03-20 - chinhnb
-- Description:	<Get box banner by zone id and position>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_BoxBanner_GetListByZoneIdAndPosition] 
	@ZoneId int,
	@Position int,
	@Status int,
	@Type int
AS
BEGIN
	SELECT BBN.Id, BBN.ZoneId, BBN.Position, BBN.Priority, BBN.Avatar, BBN.Title, BBN.Sapo, BBN.Url, BBN.Status, BBN.DisplayStyle, BBN.Type, BBN.ObjectId
	FROM BoxBanner as BBN
	INNER JOIN BoxBannerInZone as BZ ON BBN.Id = BZ.BoxBannerId 
	WHERE 
		 (@Position < 0 OR (@Position >= 0 AND Position = @Position))
		AND (@Status < 0 OR (@Status >= 0 AND Status = @Status))
		AND (@Type < 0 OR (@Type >= 0 AND Type = @Type))
		AND ((@ZoneId < 0) OR (@ZoneId >= 0 AND BZ.ZoneId = @ZoneId))
	Group By BBN.Id, BBN.ZoneId, BBN.Position, BBN.Priority, BBN.Avatar, BBN.Title, BBN.Sapo, BBN.Url, BBN.Status, BBN.DisplayStyle, BBN.Type, BBN.ObjectId
	ORDER BY Priority ASC 
END













