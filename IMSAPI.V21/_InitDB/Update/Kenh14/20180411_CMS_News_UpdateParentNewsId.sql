USE [IMS2_KENH14_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateParentNewsId]    Script Date: 04/11/2018 12:00:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--06/04/2018
CREATE PROCEDURE [dbo].[CMS_News_UpdateParentNewsId]
    @Id BIGINT ,
    @ParentNewsId BIGINT
AS 
    BEGIN
        UPDATE  news
        SET     ParentNewsId = @ParentNewsId
        WHERE   Id = @Id
    END;



