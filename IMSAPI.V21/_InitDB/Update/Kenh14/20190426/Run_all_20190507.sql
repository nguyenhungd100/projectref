drop PROCEDURE CMS_NewsExtension_ReturnExpert

--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsNotByExpert]    Script Date: 5/7/2019 11:44:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsNotByExpert] --0,0,'','',1,20,0
    @TopicId INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select distinct n.Id
		from news n	left join (select NewsId from newsextension where type=51) as newstemp
		on 	newstemp.NewsId=n.Id
		left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1
		left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
		where newstemp.NewsId IS NULL AND (n.Status in (5,6)) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
		AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
	select nn.*,nz.ZoneId
		,ROW_NUMBER() OVER ( ORDER BY nn.CreatedDate DESC ) AS RowNum
		from news nn inner join
		(select distinct n.id from news n left join (select NewsId from newsextension where type=51) as newstemp
			on 	newstemp.NewsId=n.Id
			left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1 
			left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
			where newstemp.NewsId IS NULL AND (n.Status in (5,6)) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) as tempall 
		on tempall.Id=nn.Id
		inner join NewsInZone nz on nz.NewsId=tempall.Id
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsAllByExpert]    Script Date: 5/13/2019 4:38:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsAllByExpert] --5,0,'','',1,20,0
    @ExpertStatus INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select n.Id
		from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
		on 	newstemp.NewsId=n.Id
		inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
		SELECT stemp.*,nes.Value as NoteExpert, nesdate.Value as SendDateExpert
		FROM (
			select n.*, nz.ZoneId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum
			from news n	left join (select NewsId from newsextension where type=52 and value=@ExpertStatus) as newstemp
			on 	newstemp.NewsId=n.Id
			inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			where newstemp.NewsId IS NOT NULL AND (n.Status in (5,6)) AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) AS stemp
		left join newsextension nes on nes.NewsId=stemp.Id 
		left join newsextension nesdate on nesdate.NewsId=stemp.Id 
		where nes.Type=53 and nesdate.Type=54		
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]


--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_SetValue]    Script Date: 5/7/2019 2:32:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsByExpertId]    Script Date: 5/13/2019 4:36:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsByExpertId] --4,0,0,'','',1,20,0
    @ExpertId INT ,
	@ExpertStatus INT ,
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
 --   SELECT  @TotalRow = COUNT(1) from (    
	--select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
	--inner join newsextension ne on ne.newsid=abc.newsid
	--where ne.type=52  and ne.value=@ExpertStatus) as tem
	SELECT  @TotalRow = COUNT(1) from (
		select n.Id from news n inner join 
		(select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
		inner join newsextension ne on ne.newsid=abc.newsid
		where ne.type=52  and ne.value=@ExpertStatus) as newstemp
		on newstemp.newsid=n.id
		inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
		where ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
	SELECT stemp.*,nes.Value as NoteExpert, nesdate.Value as SendDateExpert
		FROM (
			select n.*, nz.ZoneId
			,ROW_NUMBER() OVER ( ORDER BY n.CreatedDate DESC ) AS RowNum
			from news n inner join 
			(select ne.*,abc.value as ExpertId from (select * from newsextension where type=51  and value=@ExpertId) as abc 
			inner join newsextension ne on ne.newsid=abc.newsid
			where ne.type=52  and ne.value=@ExpertStatus) as newstemp
			on newstemp.newsid=n.id
			inner join NewsInZone nz on nz.NewsId=n.id and nz.IsPrimary=1 
			where ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) AS stemp
		left join newsextension nes on nes.NewsId=stemp.Id 
		left join newsextension nesdate on nesdate.NewsId=stemp.Id 
		where nes.Type=53 and nesdate.Type=54 
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END



--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_SearchForSuggest]    Script Date: 5/9/2019 11:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-05-09
ALTER PROCEDURE [dbo].[CMS_Tag_SearchForSuggest] --100, 0, N'test', -1, 1, 1
	@Top int,
	@ZoneId int,
	@Keyword nvarchar(500),
	@IsHotTag int = -1,
	@OrderBy int,
	@GetTagHasNewsOnly bit
AS
BEGIN
	DECLARE @BasicLatinKeyword varchar(500)
	SET @BasicLatinKeyword = ''
	
	IF @Keyword <> ''
	BEGIN
		SET @BasicLatinKeyword = '%' + [dbo].[ConvertToBasicLatinAndSpace](@Keyword) + '%'
		SET @Keyword = '%' + @Keyword + '%'
	END
	
	DECLARE @search nvarchar(max)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@BasicLatinKeyword varchar(500)'
	
	SET @where = ' WHERE (PATINDEX(@Keyword, T.Name) > 0 OR PATINDEX(@BasicLatinKeyword, T.UnsignName) > 0) AND (T.IsThread = 0)' 

	SET @where = @where + ' AND (T.Invisibled = 0 OR T.Invisibled IS NULL)'
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId)' 
	ELSE
		SET @where = @where + ' AND (TZ.IsPrimary = 1 OR TZ.IsPrimary IS NULL)' 
		
	IF @IsHotTag > 0
		SET @where = @where + ' AND (T.IsHotTag = 1)' 
	ELSE IF @IsHotTag = 0
		SET @where = @where + ' AND (T.IsHotTag = 0)'

	SET @where = @where + ' AND (T.Invisibled = 0)'
		
	IF @OrderBy = 1
		SET @order = ' ORDER BY COUNT(TN.NewsId) DESC, T.IsHotTag DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY COUNT(TN.NewsId) ASC, T.IsHotTag DESC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC, T.IsHotTag DESC'
	
	SET @search = 'SELECT TOP(' + CONVERT(varchar(10), @Top) + ') Id, Name, Url, UnsignName, NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.Id, T.Name, T.Url, T.UnsignName, SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Tag AS T '
	SET @search = @search + '		LEFT JOIN TagZone AS TZ ON T.Id = TZ.TagId '
	SET @search = @search + '		LEFT JOIN TagNews AS TN ON T.Id = TN.TagId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.Id, T.Name, T.Url, T.UnsignName, T.IsHotTag '
	SET @search = @search + ') AS TagTable '
	IF @GetTagHasNewsOnly = 1
		SET @search = @search + ' WHERE NewsCount > 0'
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, @Keyword, @BasicLatinKeyword
END
