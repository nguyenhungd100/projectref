
--USE [IMS2_suckhoehangngay]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsByExpertId]    Script Date: 4/26/2019 9:55:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
CREATE PROCEDURE [dbo].[CMS_NewsExtension_GetNewsExtensionByListIds] --'20190307115504946,20181022095701044,20181022095512794'
    @NewsIds varchar(max)=''
AS 
BEGIN
	DECLARE @search nvarchar(max);
	SET @search = 'SELECT * FROM NewsExtension WHERE NewsId IN('+@NewsIds+')';

	EXEC SP_EXECUTESQL @search;
END