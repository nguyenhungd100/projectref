--USE [IMS2_VIETNAMMOI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_NewsExtension_ListNewsNotByExpert]    Script Date: 5/7/2019 11:44:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-04-16
ALTER PROCEDURE [dbo].[CMS_NewsExtension_ListNewsNotByExpert] --0,0,'','',1,20,0
    @TopicId INT=0 ,	
	@ZoneId INT=0 ,
	@FromDate datetime='' ,
	@ToDate datetime='' ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT=0 OUTPUT
AS 
BEGIN
	SELECT  @TotalRow = COUNT(1) from (
		select distinct n.Id
		from news n	left join (select NewsId from newsextension where type=51) as newstemp
		on 	newstemp.NewsId=n.Id
		left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1
		left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
		where newstemp.NewsId IS NULL AND (n.Status in (5,6)) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
		AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
    ) as tem

	SELECT *
	FROM ( 
	select nn.*,nz.ZoneId
		,ROW_NUMBER() OVER ( ORDER BY nn.CreatedDate DESC ) AS RowNum
		from news nn inner join
		(select distinct n.id from news n left join (select NewsId from newsextension where type=51) as newstemp
			on 	newstemp.NewsId=n.Id
			left join NewsInZone nz on nz.NewsId=n.id --and nz.IsPrimary=1 
			left join NewsInTopic nt on nt.NewsId=n.id --and nt.IsPrimary=1 
			where newstemp.NewsId IS NULL AND (n.Status in (5,6)) AND ((@TopicId<=0 AND 1=1) OR (@TopicId>0 AND nt.TopicId=@TopicId)) 
			AND ((@ZoneId<=0 AND 1=1) OR (@ZoneId>0 AND nz.ZoneId=@ZoneId)) AND ((@FromDate='' AND @ToDate='' AND 1=1) OR (@FromDate<>'' AND n.CreatedDate>=@FromDate AND  @ToDate<>'' and n.CreatedDate<=@ToDate))
		) as tempall 
		on tempall.Id=nn.Id
		inner join NewsInZone nz on nz.NewsId=tempall.Id
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex-1)*@PageSize + 1) AND (@PageIndex*@PageSize)
	ORDER BY CreatedDate DESC
END

--USE [IMS2_suckhoehangngay]
