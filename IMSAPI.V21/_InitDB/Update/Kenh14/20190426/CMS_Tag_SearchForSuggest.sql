--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Tag_SearchForSuggest]    Script Date: 5/9/2019 11:30:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2019-05-09
ALTER PROCEDURE [dbo].[CMS_Tag_SearchForSuggest] --10, 0, N'viet nam', -1, 1, 1
	@Top int,
	@ZoneId int,
	@Keyword nvarchar(500),
	@IsHotTag int = -1,
	@OrderBy int,
	@GetTagHasNewsOnly bit
AS
BEGIN
	DECLARE @BasicLatinKeyword varchar(500)
	SET @BasicLatinKeyword = ''
	
	IF @Keyword <> ''
	BEGIN
		SET @BasicLatinKeyword = '%' + [dbo].[ConvertToBasicLatinAndSpace](@Keyword) + '%'
		SET @Keyword = '%' + @Keyword + '%'
	END
	
	DECLARE @search nvarchar(max)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)
	-- PARAMETERS
	SET @params = 			'@Keyword nvarchar(500),'
	SET @params = @params + '@BasicLatinKeyword varchar(500)'
	
	SET @where = ' WHERE (PATINDEX(@Keyword, T.Name) > 0 OR PATINDEX(@BasicLatinKeyword, T.UnsignName) > 0) AND (T.IsThread = 0)' 

	SET @where = @where + ' AND (T.Invisibled = 0 OR T.Invisibled IS NULL)'
		
	IF @ZoneId > 0
		SET @where = @where + ' AND (TZ.ZoneId = @ZoneId)' 
	ELSE
		SET @where = @where + ' AND (TZ.IsPrimary = 1 OR TZ.IsPrimary IS NULL)' 
		
	IF @IsHotTag > 0
		SET @where = @where + ' AND (T.IsHotTag = 1)' 
	ELSE IF @IsHotTag = 0
		SET @where = @where + ' AND (T.IsHotTag = 0)'

	SET @where = @where + ' AND (T.Invisibled = 0)'
		
	IF @OrderBy = 1
		SET @order = ' ORDER BY COUNT(TN.NewsId) DESC, T.IsHotTag DESC'
	ELSE IF @OrderBy = 2
		SET @order = ' ORDER BY COUNT(TN.NewsId) ASC, T.IsHotTag DESC'
	ELSE
		SET @order = ' ORDER BY T.CreatedDate DESC, T.IsHotTag DESC'
	
	SET @search = 'SELECT TOP(' + CONVERT(varchar(10), @Top) + ') Id, Name, Url, UnsignName, NewsCount '
	SET @search = @search + 'FROM ('
	SET @search = @search + '	SELECT T.Id, T.Name, T.Url, T.UnsignName, SUM(CASE WHEN TN.NewsId IS NULL THEN 0 ELSE 1 END) AS NewsCount, '
	SET @search = @search + '		ROW_NUMBER() OVER(' + @order + ') AS RowNum '
	SET @search = @search + '	FROM Tag AS T '
	SET @search = @search + '		LEFT JOIN TagZone AS TZ ON T.Id = TZ.TagId '
	SET @search = @search + '		LEFT JOIN TagNews AS TN ON T.Id = TN.TagId '
	SET @search = @search + '	' + @where + ' '
	SET @search = @search + '	GROUP BY T.Id, T.Name, T.Url, T.UnsignName, T.IsHotTag '
	SET @search = @search + ') AS TagTable '
	IF @GetTagHasNewsOnly = 1
		SET @search = @search + ' WHERE NewsCount > 0'
	
	--SET ANSI_WARNINGS OFF
	
	EXEC SP_EXECUTESQL  @search, @params, @Keyword, @BasicLatinKeyword
END
