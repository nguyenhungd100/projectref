GO
--chinhnb
--03/04/2018
ALTER PROC [dbo].[CMS_SEOMetaTags_Insert]
@Name nvarchar(300),
@MetaKeyword nvarchar(250),
@MetaDescription nvarchar(250),
@ZoneId int,
@Id int OUT
AS
BEGIN 
	IF(select count(*) from SEOMetaTags where ZoneId=@ZoneId) <= 0
		BEGIN
		  INSERT INTO SEOMetaTags (Name, MetaKeyword, MetaDescription,ZoneId) 
		  VALUES (@Name,@MetaKeyword,@MetaDescription,@ZoneId)  
			
			SET @Id = SCOPE_IDENTITY()
		END
	ELSE
		SET @Id = 0   
END