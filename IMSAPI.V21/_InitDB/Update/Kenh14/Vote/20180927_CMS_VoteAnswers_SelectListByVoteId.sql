--USE [IMS_VTV_EXT]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VoteAnswers_SelectListByVoteId]    Script Date: 9/27/2018 9:22:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--2018-09-27
CREATE PROCEDURE [dbo].[CMS_VoteAnswers_SelectListByVoteId] --1
	-- Add the parameters for the stored procedure here
	@VoteId	INT
AS
BEGIN
	SELECT	 
				Id,
				VoteId,
				Value,
				CONVERT(int, Value) AS VoteItemId,
				VoteRate,
				Status,
				Priority
	FROM	VoteAnswers
	WHERE	VoteId = @VoteId order by Priority asc
END