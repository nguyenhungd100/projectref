USE [IMS2_AFAMILY_DEV]
GO

/****** Object:  Table [dbo].[ThreadNews]    Script Date: 05/19/2018 10:35:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VoteInNews](
	[VoteId] [int] NOT NULL,
	[NewsId] [bigint] NOT NULL,
	[Ordinary] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_VoteInNews] PRIMARY KEY CLUSTERED 
(
	[VoteId] ASC,
	[NewsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VoteInNews] ADD  DEFAULT (getdate()) FOR [LastModifiedDate]
GO


