--USE [IMS2_VIETNAMBIZ]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxVideoEmbed_GetListByZone]    Script Date: 2/12/2019 4:15:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[CMS_BoxVideoEmbed_GetListByZone]
@ZoneId int,
@Type int
AS

	BEGIN
		SELECT * FROM(
		SELECT TOP(20) BV.Id, BV.ZoneId,BV.VideoId, BV.SortOrder, BV.Type, VP.Avatar, VP.Name, BV.Embed
		FROM BoxVideoEmbed as BV
			inner join Video as VP on BV.VideoId =VP.Id
		where BV.ZoneId =@ZoneId and BV.Type=@Type and VP.status=1
		UNION ALL
		SELECT Id, ZoneId, VideoId, SortOrder, Type, Avatar, Name, Embed
		FROM BoxVideoEmbed
		where ZoneId =@ZoneId and Type=@Type and VideoId = 0
		) AS T ORDER BY SortOrder ASC
	END





