--USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_User_UpdateById]    Script Date: 07/24/2017 16:35:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-07-24>
-- Description:	<update user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_UpdateById]
	@Id int,
	@FullName nvarchar(255),
	@Avatar varchar(500),
	@Email varchar(255),
	@Mobile varchar(100),
	@IsFullPermission bit,
	@IsFullZone bit,
	@Status int,
	@Address nvarchar(500),
	@Birthday datetime,
	@Description nvarchar(500),
	@IsRole int,
	@IsSendOver bit
AS
BEGIN
	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE [User]
			SET FullName = @FullName, 
				Avatar = @Avatar, 
				Email = @Email, 
				Mobile = @Mobile, 
				IsFullPermission = @IsFullPermission, 
				IsFullZone = @IsFullZone, 
				Status = @Status,
				IsRole=@IsRole,
				IsSendOver=@IsSendOver,
				ModifiedDate = GETDATE()
			WHERE (Id = @Id)
			
			IF EXISTS(SELECT 1 FROM UserProfile WHERE UserId = @Id)
			BEGIN
				UPDATE UserProfile
				SET Address = @Address, 
					Birthday = @Birthday, 
					Description = @Description
				WHERE (UserId = @Id)
			END
			ELSE
			BEGIN
				INSERT INTO UserProfile (UserId, Address, Birthday, Description)
				VALUES (@Id,@Address,@Birthday,@Description)
			END
			
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
END

GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-07-24>
-- Description:	<get user information by id>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_User_GetUserById]
	@Id int
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
		U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem,U.IsRole,U.IsSendOver
	FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
	WHERE U.Id = @Id
END

Go
--Thêm: 31/07/2017

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-07-31>
-- Description:	<change news status to WaitForEditorialBoard>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_ChangeStatusToWaitForEditorialBoard]
	@Id bigint,
	@PublishedBy varchar(200)
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9,
    WaitForEditorialBoard = 14
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 14,
			PublishedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE PublishedBy END, 
			LastModifiedDate = GETDATE()
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

Go

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-07-31>
-- Description:	<change news status to ReceivedForEditorialBoard>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_ChangeStatusToRecievedForEditorialBoard]
	@Id bigint,
	@PublishedBy varchar(200),
	@ApprovedBy varchar(200)
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9,
    WaitForEditorialBoard = 14,
    ReceivedForEditorialBoard = 16
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 16,
			PublishedBy = CASE PublishedBy WHEN '' THEN @PublishedBy ELSE PublishedBy END,
			ApprovedBy = @ApprovedBy, 
			LastModifiedDate = GETDATE()
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-07-24>
-- Description:	<Init user redis or es>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_User_InitAllUser]
AS
BEGIN
	SELECT U.Id, U.UserName, U.FullName, U.Password, U.Avatar, U.Email, U.Mobile, U.IsFullPermission, 
        U.IsFullZone, U.Status, U.CreatedDate, U.ModifiedDate, U.LastLogined, 
        U.LastChangePass, P.Address, P.Birthday, P.Description, U.IsSystem, U.IsRole, U.IsSendOver
    FROM [User] AS U LEFT OUTER JOIN
        UserProfile AS P ON U.Id = P.UserId
END

Go

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-08-07>
-- Description:	<change news status to ReturnedToReporter>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToReturnedToReporter]
	@Id bigint,
	@ReturnedBy varchar(200),
	@Note nvarchar(2000) = ''
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 4, 
			ReturnedBy=@ReturnedBy,
			LastModifiedDate = GETDATE(),
			Note = CASE @Note WHEN '' THEN Note ELSE @Note END
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

Go

-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-08-07>
-- Description:	<change news status to ReturnedToEditorialSecretary>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_ChangeStatusToReturnedToEditorialSecretary]
	@Id bigint,
	@ReturnedBy varchar(200),
	@Note nvarchar(2000) = ''
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9,
    ReturnedToEditorialSecretary=19
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 19, 
			ReturnedBy=@ReturnedBy,
			LastModifiedDate = GETDATE(),
			Note = CASE @Note WHEN '' THEN Note ELSE @Note END
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

GO
ALTER PROCEDURE  [dbo].[VideoCms_Video_InsertV3]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100) = '',
	@Pname varchar(100) = '',
	@Status int = 0,
	@FileName varchar(500),
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@CreatedBy varchar(255),
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@Source nvarchar(200) = '',
	@TagIdList varchar(300) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	@Duration varchar(50),
	@Size varchar(20),
	@Capacity int,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	@OriginalId int = 0,
	@Author nvarchar(205) = '',
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION
	
	--SET NOCOUNT ON;
--	IF @ZoneIdList <> ''
--		SET @ZoneIdList = dbo.CMS_fGetListOfZoneVideoIdIncludeParentZoneVideoId(@ZoneId, @ZoneIdList + ';' + CONVERT(varchar(10), @ZoneId))

    IF (EXISTS (SELECT Id FROM Video WHERE	KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0))
	BEGIN
		SELECT @Id = Id FROM Video WHERE KeyVideo = @KeyVideo AND NewsId = @NewsId AND @NewsId > 0
		--RAISERROR ('This video has existed in database. Try again or create a other video.', 16, 1);
		--ROLLBACK TRANSACTION
	END ELSE
    BEGIN
		DECLARE @ParentPrimaryZoneId int
		SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
		IF @ParentPrimaryZoneId > 0
		BEGIN
			IF @ZoneIdList <> ''
				SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
			ELSE
				SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
		END
		ELSE
		BEGIN
			DECLARE @ChildZoneId int
			SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
			IF @ChildZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
				SET @ZoneId = @ChildZoneId
			END
		END

		INSERT	INTO Video(	ZoneId,
							Name,
							UnsignName,
							Description,
							HtmlCode,
							Avatar,
							KeyVideo,
							Pname,
							Status,
							FileName,
							NewsId,
							Views,
							Mode,
							Tags,
							CreatedBy,
							LastModifiedBy,
							Url,
							[Source],
							DistributionDate,
							CreatedDate,
							LastModifiedDate,
							VideoRelation,
							Duration,
							Size,
							Capacity,
							AllowAd,
							IsRemoveLogo,
							AvatarShareFacebook,
							OriginalId,
							Author)
		VALUES	(		@ZoneId,
						@Name,
						@UnsignName,
						@Description,
						@HtmlCode,
						@Avatar,
						@KeyVideo,
						@Pname,
						@Status,
						@FileName,
						@NewsId,
						@Views,
						@Mode,
						@Tags,
						@CreatedBy,
						@CreatedBy,
						@Url,
						@Source,
						@DistributionDate,
						GETDATE(),
						GETDATE(),
						@VideoRelationIdList,
						@Duration,
						@Size,
						@Capacity,
						@AllowAd,
						@IsRemoveLogo,
						@AvatarShareFacebook,
						@OriginalId,
						@Author
					)
						
		SET @Id = SCOPE_IDENTITY();
		
		UPDATE Video SET Url = REPLACE(Url, '{VideoId}', @Id) WHERE Id = @Id
		
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert video error. Try again or create a other video.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN
			-- Process Tags
			DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
			DECLARE @Index int,
					@TagId int;

				
			IF  @TagIdList <> ''
			BEGIN
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
					
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
			-- Process Playlist
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					
					IF @PlaylistId IS NOT NULL AND @PlayListId > 0
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @Index)
						
					SET @Index = @Index + 1
				END
			END
		END		
    END
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
Go

USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_UpdateV4]    Script Date: 08/31/2017 15:33:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VideoCms_Video_UpdateV4]
	@ZoneId int,
	@Name nvarchar(250),
	@UnsignName varchar(250),
	@Description nvarchar(max) = '',
	@HtmlCode nvarchar(max) = '',
	@Avatar varchar(255) = '',
	@KeyVideo varchar(100),
	@Pname varchar(100) = '',
	@Status int = 0,
	@NewsId bigint = 0,
	@Views int = 0,
	@Mode int = 0,
	@Tags nvarchar(max) = '',
	@LastModifiedBy varchar(255) = '',
	@DistributionDate datetime,
	@Url varchar(300) = '',
	@Source nvarchar(200) ='',
	@TagIdList varchar(300) = '',
	@TagNameList nvarchar(1000) = '',
	@ZoneIdList varchar(200) = '',
	@VideoRelationIdList varchar(500) = '',
	@PlaylistIdList varchar(500) = '',
	-- Video File Info 
	@FileName	varchar(500) = '',
	@Duration	varchar(50) = '',
	@Size	varchar(20) = '',
	@Capacity	int = 0,
	@AllowAd bit = 1,
	@IsRemoveLogo bit = 0,
	@AvatarShareFacebook varchar(255) = '',
	@OriginalId int = 0,
	@Author nvarchar(205) = '',
	-- Primary Key
    @Id int
AS
	BEGIN TRANSACTION
	
    IF (@Id <= 0 OR NOT EXISTS (SELECT Id 
					FROM Video 
					WHERE	Id	= @Id))
		BEGIN
			RAISERROR ('Can not found this video.', 16, 1);
			ROLLBACK TRANSACTION
		END
    ELSE
		BEGIN

--			IF @ZoneIdList <> ''
--				SET @ZoneIdList = dbo.CMS_fGetListOfZoneVideoIdIncludeParentZoneVideoId(@ZoneId, @ZoneIdList + ';' + CONVERT(varchar(10), @ZoneId))

			DECLARE @ParentPrimaryZoneId int
			SET @ParentPrimaryZoneId = ISNULL((SELECT ParentId FROM ZoneVideo WHERE Id = @ZoneId), 0)
			IF @ParentPrimaryZoneId > 0
			BEGIN
				IF @ZoneIdList <> ''
					SET @ZoneIdList = CONVERT(varchar(10), @ParentPrimaryZoneId)
				ELSE
					SET @ZoneIdList = ';' + CONVERT(varchar(10), @ParentPrimaryZoneId)
			END
			ELSE
			BEGIN
				DECLARE @ChildZoneId int
				SET @ChildZoneId = ISNULL((SELECT TOP 1 Id FROM ZoneVideo WHERE PATINDEX('%;' + CONVERT(varchar(10), Id) + ';%', ';' + @ZoneIdList + ';') > 0 AND ParentId > 0 AND ParentId IN (37,38,40,41,42,43,95)), 0)
				IF @ChildZoneId > 0
				BEGIN
					IF @ZoneIdList <> ''
						SET @ZoneIdList = CONVERT(varchar(10), @ZoneId)
					ELSE
						SET @ZoneIdList = ';' + CONVERT(varchar(10), @ZoneId)
					SET @ZoneId = @ChildZoneId
				END
			END

			UPDATE Video
			SET
				ZoneId = @ZoneId,
				Name = @Name,
				UnsignName = @UnsignName,
				Description = @Description,
				HtmlCode = @HtmlCode,
				KeyVideo = @KeyVideo,
				Avatar = @Avatar,
				Pname = @Pname,
				Status = @Status,
				NewsId = @NewsId,
				Mode = @Mode,
				Tags = @Tags,
				LastModifiedBy = @LastModifiedBy,
				LastModifiedDate = GETDATE(),
				DistributionDate = @DistributionDate,
				Url = @Url,
				[Source] = @Source,
				VideoRelation = @VideoRelationIdList,
				FileName = @FileName,
				Duration = @Duration,
				Size = @Size,
				Capacity = @Capacity,
				AllowAd = @AllowAd,
				AvatarShareFacebook = @AvatarShareFacebook,
				OriginalId=@OriginalId,
				Author=@Author
			WHERE Id = @Id
			
			-- Process Tags

				
			-- DELETE ALL Tags of this video
			DELETE FROM VideoInTag WHERE VideoId = @Id
			
			-- DELETE ALL VideoInZone of this Video
			DELETE FROM VideoInZone  WHERE VideoId = @Id
				
			IF  @TagIdList <> '' AND @TagNameList <> ''
			BEGIN
				DECLARE @TblTags TABLE (Id int identity(1,1), TagId int NOT NULL);
				DECLARE @TblTagsList TABLE (Id int identity(1,1), Tag nvarchar(200) NOT NULL);
				DECLARE @TagName nvarchar(200),
						@Index int,
						@TagId int;
						
				-- Tag Id List
				INSERT INTO @TblTags (TagId)
				SELECT CONVERT(int, part) FROM SplitString(@TagIdList, ';')
				-- Tag List
				INSERT INTO @TblTagsList (Tag)
				SELECT part FROM SplitString(@TagNameList, ';');
					
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblTags)) BEGIN
					SET @TagId = (SELECT TagId FROM @TblTags WHERE Id = @Index);
					SET @TagName = (SELECT Tag FROM @TblTagsList WHERE Id = @Index);
					
					-- Add to VideoTags
					IF @TagId > 0 AND NOT EXISTS (SELECT VideoId FROM VideoInTag WHERE VideoId = @Id AND VideoTagId = @TagId)
					BEGIN
						INSERT INTO VideoInTag(VideoId, VideoTagId, Priority, TagMode)
						VALUES (@Id, @TagId, @Index, 1)
					END
					SET @Index = @Index + 1;
				END
			END
			
			
			-- Process ZoneVideo
			
			INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary)
			VALUES (@Id, @ZoneId, 1)
						
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') 
			BEGIN
				DECLARE @TblZoneVideo TABLE(Id int identity(1,1), ZoneId int NOT NULL);
				DECLARE @ZoneIdTemp int
			
				INSERT INTO @TblZoneVideo(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 1;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblZoneVideo)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblZoneVideo WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT VideoId  FROM VideoInZone WHERE VideoId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO VideoInZone(VideoId , ZoneId, IsPrimary, LastModifiedDate)
						VALUES (@Id, @ZoneIdTemp, 0, DATEADD(second, @Index, GETDATE()))
					END
					SET @Index = @Index + 1;
				END
			END
			
					
			-- Process Playlist
			DELETE FROM VideoPlayList WHERE VideoId = @Id
			
			IF @PlaylistIdList IS NOT NULL AND @PlaylistIdList <> ''
			BEGIN
				DECLARE @TblVideoPlaylist TABLE(Id int identity(1,1), PlaylistId int NOT NULL);
				DECLARE @PlaylistId int;
				
				INSERT @TblVideoPlaylist(PlaylistId)
				SELECT CONVERT(int, part) FROM SplitString(@PlaylistIdList, ';')
				
				
				SET @Index = 0;
				
				WHILE (@Index <= (SELECT COUNT(Id) FROM @TblVideoPlaylist))
				BEGIN
					SET @PlaylistId = (SELECT PlaylistId FROM @TblVideoPlaylist WHERE Id = @Index)
					IF @PlaylistId IS NOT NULL AND @PlaylistId > 0
					BEGIN
						INSERT VideoPlayList (PlaylistId, VideoId, Priority)
						VALUES (@PlaylistId, @Id, @Index)
					END
					SET @Index = @Index + 1
				END
			END
			
		END
    
    
    IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION
Go