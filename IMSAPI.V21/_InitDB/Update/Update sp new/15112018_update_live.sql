--USE [IMS2_THOIDAI_DEV]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxTopicOnPageEmbed_Update]    Script Date: 11/15/2018 4:06:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--15112018
CREATE PROCEDURE [dbo].[CMS_BoxTopicOnPageEmbed_UpdateBoxTopicOnPageEmbed] 
	@ListOfPriority nvarchar(MAX),
	@ZoneId int,
	@Type int
AS
BEGIN
    DELETE BoxTopicOnPageEmbed WHERE ZoneId =@ZoneId and Type=@Type
	DECLARE @Priority int, 
			@TopicId bigint
			
	DECLARE PriorityCursor CURSOR FOR
	SELECT Id, Part
	FROM dbo.SplitString(@ListOfPriority, ',')

	OPEN PriorityCursor
	

	FETCH NEXT FROM PriorityCursor INTO @Priority, @TopicId
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM BoxTopicOnPageEmbed WHERE TopicId = @TopicId and ZoneId =@ZoneId and Type =@Type)
			INSERT INTO BoxTopicOnPageEmbed (ZoneId,TopicId,SortOrder,Type) values( @ZoneId,@TopicId,@Priority,@Type)
			FETCH NEXT FROM PriorityCursor INTO @Priority, @TopicId 
	END   

	CLOSE PriorityCursor   
	DEALLOCATE PriorityCursor


END


--USE [IMS2_THOIDAI]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_MultiUpdateTopicNews]    Script Date: 11/15/2018 10:12:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--edit: 22/06/2018
CREATE PROCEDURE [dbo].[CMS_ThreadNews_MultiUpdateThreadNews]
	@NewsId bigint,
	@ListThreadId varchar(500)	
AS
BEGIN	
	BEGIN TRANSACTION		
	
	DELETE FROM ThreadNews WHERE NewsId = @NewsId

	DECLARE @Index int, 
			@TotalRowInTemp int,
			@ThreadId int;
				
	-- Insert later
	DECLARE @TblAddTemp TABLE (Id int identity(1,1), ThreadId int)
	INSERT @TblAddTemp (ThreadId)
	SELECT CONVERT(int, part) FROM SplitString(@ListThreadId, ';')
	
	SET @Index = 0
	SET @TotalRowInTemp = ISNULL((SELECT COUNT(Id) FROM @TblAddTemp), 0)
	WHILE (@Index <= @TotalRowInTemp)
	BEGIN
		SET @ThreadId = ISNULL((SELECT ThreadId FROM @TblAddTemp WHERE Id = @Index), 0)
		IF @ThreadId > 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM ThreadNews WHERE NewsId = @NewsId AND ThreadId = @ThreadId)
			BEGIN
				INSERT INTO ThreadNews(NewsID, ThreadId)
				VALUES(@NewsId, @ThreadId)
			END
		END
		SET @Index = @Index + 1
	END
	
	--update isparimary
	--update ThreadNews set IsPrimary=1 where ThreadId=@IsPrimaryThreadId and NewsId = @NewsId
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500)
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE 
	BEGIN
		COMMIT TRANSACTION
	END
END
