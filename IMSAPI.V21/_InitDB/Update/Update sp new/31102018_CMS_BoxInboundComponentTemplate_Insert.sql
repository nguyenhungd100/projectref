--USE [IMS2_VINF1]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxInboundComponentTemplate_Insert]    Script Date: 10/30/2018 3:39:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <29/06/2018>
-- Description:	<Insert box>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_BoxInboundComponentTemplate_Insert]	
	@Id int OUTPUT ,	
	@TypeId varchar(100),
	@Title nvarchar(500),
	@Description nvarchar(255),
	@CreatedBy varchar(100),
	@ModifiedBy varchar(100),
	@ListZoneId nvarchar(500),
	@ListTopicId nvarchar(500),
	@ListThreadId nvarchar(500),
	@DataJson nvarchar(max) = '',
	@DataEmbed nvarchar(max) = ''
AS
BEGIN
BEGIN TRANSACTION
	
	BEGIN TRY
		--check
		IF NOT EXISTS(Select 1 From BoxInboundComponentTemplate Where TypeId=@TypeId and Title=@Title)
		BEGIN
			INSERT INTO BoxInboundComponentTemplate(TypeId, Title, Description, CreatedBy, CreatedDate, ModifiedBy,ModifiedDate,ListZoneId,ListTopicId,ListThreadId,DataJson,DataEmbed)
			VALUES (@TypeId,@Title,@Description,@CreatedBy,getdate(),@ModifiedBy,getdate(), @ListZoneId, @ListTopicId,@ListThreadId,@DataJson,@DataEmbed)		
		
			SET @Id = SCOPE_IDENTITY()
		END
		ELSE
			SET @Id = 0

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

