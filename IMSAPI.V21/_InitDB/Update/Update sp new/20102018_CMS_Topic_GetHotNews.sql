--USE [IMS2_AFAMILY_BETA]
GO
/****** Object:  StoredProcedure [dbo].[CMS_Topic_GetHotNews]    Script Date: 10/20/2018 1:14:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_Topic_GetHotNews] @TopicId INT
AS 
    SELECT  b.Id , b.Title,
            a.Avatar
    FROM    dbo.HotNewsTopic a
            INNER JOIN news b ON a.TopicId = @TopicId AND a.NewsId = b.Id AND b.Status=8



