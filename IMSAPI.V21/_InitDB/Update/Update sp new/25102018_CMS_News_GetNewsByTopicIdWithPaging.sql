--USE [IMS2_DEMO]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_GetNewsByTopicIdWithPaging]    Script Date: 10/25/2018 6:37:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[CMS_News_GetNewsByTopicIdWithPaging]
	@Keyword nvarchar(500)='',
	@TopicId bigint,
	@Status int=-1,
	@PageIndex int,
	@PageSize int,
	@TotalRow int output
AS
	IF @Keyword <> ''
		SET @Keyword = '%' + @Keyword + '%'

	SELECT @TotalRow = COUNT(*) 
	FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
	WHERE TN.TopicID = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0)) and (@Status=-1 or (@Status<>-1 and N.Status=@Status))

	SELECT * FROM
	(
		SELECT  N.Id, N.Title, N.Avatar, N.Sapo, N.ViewCount, 
				N.LastModifiedDate, N.CreatedBy, N.EditedBy, 
				N.PublishedBy, N.DistributionDate, N.Url,
				row_number() OVER (ORDER BY N.DistributionDate DESC) AS RowNum
		FROM News AS N INNER JOIN NewsInTopic AS TN ON N.Id = TN.NewsID
		WHERE TN.TopicId = @TopicId and (@Keyword = '' OR (@Keyword <> '' AND PATINDEX(@Keyword, Title) > 0))
	) AS DATA
	WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)
	ORDER BY DistributionDate DESC




