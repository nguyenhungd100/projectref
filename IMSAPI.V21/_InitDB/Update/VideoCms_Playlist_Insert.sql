﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_Insert]    Script Date: 1/12/2018 8:05:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update Playlist>
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_Insert]
	@ZoneId int = 0,
	@Name nvarchar(200),
	@UnsignName varchar(200),
	@Description nvarchar(max) = '',
	@Status int = 0,
	@Mode int = 0,
	@Avatar varchar(300),
	@Priority int,
	@Url varchar(300),
	@VideoCount int,
	@CreatedBy varchar(50) = '',
	@CreatedDate datetime,
	@DistributionDate datetime,	
	@IntroClip nvarchar(500) = '',
	@PlaylistRelation nvarchar(max) = '',
	@Cover nvarchar(300),
	@MetaJson nvarchar(max),
	@MetaAvatar nvarchar(max),
	@ZoneIdList varchar(500) = '',
	@Id int OUT
AS
	BEGIN TRANSACTION
	
		INSERT INTO PlayList (
				ZoneId,
				Name,
				UnsignName,
				[Description],
				[Status],
				Mode,
				Avatar,
				Priority,
				VideoCount,
				Url,
				CreatedBy,
				LastModifiedBy,
				CreatedDate,
				LastModifiedDate,
				LastInsertVideoDate,				
				DistributionDate,
				IntroClip,
				PlaylistRelation,
				Cover,
				MetaJson,
				MetaAvatar
				)
		VALUES (
				@ZoneId,
				@Name,
				@UnsignName,
				@Description,
				@Status,
				@Mode,
				@Avatar,
				@Priority,
				@VideoCount,
				@Url,
				@CreatedBy,
				@CreatedBy,
				@CreatedDate,
				GETDATE(),
				GETDATE(),				
				@DistributionDate,
				@IntroClip,
				@PlaylistRelation,
				@Cover,
				@MetaJson,
				@MetaAvatar
				)
		
		SET @Id = SCOPE_IDENTITY()
	
		IF (@Id IS NULL OR @Id <= 0)
		BEGIN
			RAISERROR ('Insert playlist error. Try again or create a other playlist.', 16, 1);
			ROLLBACK TRANSACTION
		END ELSE 
		BEGIN			
			DECLARE @Index int;
			
			-- Process PlaylistInZone					
			INSERT INTO PlaylistInZone(ZoneId, PlaylistId, LastModifiedDate, IsPrimary)
			VALUES (@ZoneId, @Id, @CreatedDate, 1)
			
			DECLARE @TblPlaylistInZone TABLE(Id int identity(1,1), ZoneId int NOT NULL);
			DECLARE @ZoneIdTemp int
			IF (LTRIM(RTRIM(@ZoneIdList)) <> '') BEGIN
				INSERT INTO @TblPlaylistInZone(ZoneId)
				SELECT CONVERT(int, part) FROM SplitString(@ZoneIdList, ';');
				
				SET @Index = 0;
				WHILE (@Index <= (SELECT COUNT(*) FROM @TblPlaylistInZone)) BEGIN
					SET @ZoneIdTemp = (SELECT ZoneId FROM @TblPlaylistInZone WHERE Id = @Index);
					
					IF @ZoneIdTemp IS NOT NULL AND NOT EXISTS (SELECT PlaylistId  FROM PlaylistInZone WHERE PlaylistId = @Id AND ZoneId = @ZoneIdTemp)
					BEGIN
						INSERT INTO PlaylistInZone(ZoneId, PlaylistId, LastModifiedDate, IsPrimary)
						VALUES (@ZoneIdTemp, @Id, @CreatedDate, 0)
					END
					SET @Index = @Index + 1;
				END
			END
			-- Process VideoInChannel: chưa xử lý												
    END
    
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500)
		SET @Message = ERROR_MESSAGE()
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END
	ELSE
		COMMIT TRANSACTION
















