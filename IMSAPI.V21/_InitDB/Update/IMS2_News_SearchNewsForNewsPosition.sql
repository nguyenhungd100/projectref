USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[IMS2_News_SearchNewsForNewsPosition]   Script Date: 09/19/2017 18:03:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author	  :	<ThanhTN>
-- Create date: <2012-10-27>
-- Description:	<Search news publish for news>
-- =============================================
CREATE PROCEDURE [dbo].[IMS2_News_SearchNewsForNewsPosition]
	@ZoneId int,
	@Keyword nvarchar(200),
	@Type int = -1,
	@NewsType int = -1,
	@DisplayPosition int = -1,
	@DistributedDateFrom datetime,
	@DistributedDateTo datetime,
	@ExcludeNewsIds varchar(2000) = '',
	@PageIndex int = 1,
	@PageSize int = 10,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
	DECLARE @search nvarchar(max)
	DECLARE @select_count nvarchar(2000)
	DECLARE @params nvarchar(2000)
	DECLARE @where nvarchar(2000)
	DECLARE @order nvarchar(2000)

	DECLARE @ZoneIds varchar(100)
	SET @ZoneIds = dbo.CMS_fGetListOfZoneIdByParentZoneId(@ZoneId)

	IF @PageIndex = 0
		SET @PageIndex = 1

	IF @PageSize = 0
		SET @PageSize = 10

	SET @params = 			'@Keyword nvarchar(300),'
	SET @params = @params + '@ZoneIds varchar(200),'
	SET @params = @params + '@DistributedDateFrom datetime,'
	SET @params = @params + '@DistributedDateTo datetime,'
	SET @params = @params + '@Type int,'
	SET @params = @params + '@NewsType int,'
	SET @params = @params + '@DisplayPosition int,'
	SET @params = @params + '@PageIndex int,'
	SET @params = @params + '@PageSize int,'
	SET @params = @params + '@TotalRow int OUTPUT'
	
	SET @where = ' WHERE (Status = 8) '
		
	--IF @DisplayPosition >= 0
	--	SET @where = @where + ' AND (DisplayPosition = @DisplayPosition)'
		
	IF @Type >= 0
		SET @where = @where + ' AND ([Type] = @Type)'
		
	IF @NewsType >= 0
		SET @where = @where + ' AND ([NewsType] = @NewsType)'
	
	IF @ExcludeNewsIds IS NOT NULL AND @ExcludeNewsIds <> ''
		SET @where = @where + ' AND (PATINDEX(''%,'' + CONVERT(varchar(20), Id) + '',%'', '',' + @ExcludeNewsIds + ','') <= 0) '
	
--	IF @ZoneId > 0	-- Filter by zoneid
--		SET @where = @where + ' AND (PATINDEX(''%,'' + CONVERT(varchar(10), Z.ZoneId) + '',%'', '',' + @ZoneIds + ','') > 0)'
--	ELSE			-- Not filter by zone id -> get all news in primary zone and news which not in any zone
--		SET @where = @where + ' AND (Z.IsPrimary = 1 OR Z.IsPrimary IS NULL)'

	IF @ZoneIds <> ''					 -- Filter by zoneid
		IF PATINDEX('%,%', @ZoneIds) > 0 -- Has more than one zoneid -> get news in primary zone only
			SET @where = @where + ' AND (Z.IsPrimary = 1) AND (PATINDEX(''%,'' + CONVERT(varchar(10), Z.ZoneId) + '',%'', '','' + @ZoneIds + '','') > 0)'
		ELSE							 -- Has only one zoneid -> get all news in this zone
			SET @where = @where + ' AND (PATINDEX(''%,'' + CONVERT(varchar(10), Z.ZoneId) + '',%'', '','' + @ZoneIds + '','') > 0)'
	ELSE								 -- Not filter by zone id -> get all news in primary zone and news which not in any zone
		SET @where = @where + ' AND (Z.IsPrimary = 1)'
		--SET @where = @where + ' AND (Z.IsPrimary = 1 OR Z.IsPrimary IS NULL)'

	--IF @ZoneId > 0
	--	SET @where = @where + ' AND (PATINDEX(''%,'' + CONVERT(varchar(10), ZoneId) + '',%'', '','' + @ZoneIds + '','') > 0)'
	--ELSE 
	--	SET @where = @where + ' AND (IsPrimary = 1)'
		
	IF @Keyword <> ''
		SET @where = @where + ' AND (PATINDEX(''%'' + @Keyword + ''%'', Title) > 0)'
		
	IF @DistributedDateFrom IS NOT NULL AND @DistributedDateTo IS NOT NULL
		SET @where = @where + ' AND (DistributionDate BETWEEN @DistributedDateFrom AND @DistributedDateTo)'
	ELSE
		IF @DistributedDateFrom IS NOT NULL
			SET @where = @where + ' AND (DistributionDate >= @DistributedDateFrom)'
		ELSE IF @DistributedDateTo IS NOT NULL
			SET @where = @where + ' AND (DistributionDate <= @DistributedDateTo)'
			
	SET @order = ' ORDER BY DistributionDate DESC';
			
	SET @search = 'SELECT @TotalRow = COUNT(*) FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId ' + @where + ';'
			
	IF @PageIndex <= 1
	BEGIN
		SET @search = @search + ' SELECT TOP (' + CONVERT(varchar(5), @PageSize) + ') Id,Sapo, Title, Url, Avatar, DistributionDate, ViewCount, Source, Url '
		--IF @DistributedDateFrom IS NOT NULL OR @DistributedDateTo IS NOT NULL-- OR @Keyword <> ''
			SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		--ELSE
		--	SET @search = @search + ' FROM NewsTemp AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		SET @search = @search + @where
		SET @search = @search + @order
	END ELSE
	BEGIN	
		SET @search = @search + ' SELECT * FROM '
		SET @search = @search + ' ('
		SET @search = @search + '		SELECT Id,Sapo, Title, Url, Avatar, DistributionDate, ViewCount, Source, Url, '
		SET @search = @search + '		row_number() OVER (' + @order + ') AS RowNum '
		--IF @DistributedDateFrom IS NOT NULL OR @DistributedDateTo IS NOT NULL-- OR @Keyword <> ''
			SET @search = @search + ' FROM News AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		--ELSE
		--	SET @search = @search + ' FROM NewsTemp AS N INNER JOIN NewsInZone AS Z ON N.Id = Z.NewsId '
		SET @search = @search + @where
		SET @search = @search + @order
		SET @search = @search + ' ) AS N'
		SET @search = @search + ' WHERE RowNum BETWEEN ((@PageIndex - 1) * @PageSize + 1) AND (@PageIndex * @PageSize)'
		SET @search = @search + @order
	END
--PRINT @search
	EXEC SP_EXECUTESQL  @search, @params, 
								@Keyword,
								@ZoneIds,
								@DistributedDateFrom,
								@DistributedDateTo,
								@Type,
								@NewsType,
								@DisplayPosition,
								@PageIndex,
								@PageSize,
								@TotalRow OUTPUT
END






