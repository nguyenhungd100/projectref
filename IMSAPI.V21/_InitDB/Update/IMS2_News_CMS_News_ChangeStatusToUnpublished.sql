USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToUnpublished]    Script Date: 10/18/2017 17:48:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<ThanhTN>
-- Create date: <2012-09-28>
-- Description:	<change news status to Unpublished>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToUnpublished]
	@Id bigint,
	@LastModifiedBy nvarchar(200)
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		DELETE FROM NewsContent WHERE NewsId = @Id
		DELETE FROM NewsPublish WHERE NewsId = @Id
		
		UPDATE News
		SET [Status] = 10, LastModifiedDate = GETDATE(), LastModifiedBy = @LastModifiedBy
		WHERE Id = @Id
		
		DECLARE @NewsPositionId int,
				@PositionTypeId int,
				@NewsIdForBomd bigint,
				@ZoneId int
		
		DECLARE PositionCursor CURSOR FOR
		SELECT Id, TypeId, NewsIdForBomd, ZoneId
		FROM NewsPosition
		WHERE (NewsId = @Id)
		ORDER BY [Order]
		
		OPEN PositionCursor
		
		FETCH NEXT FROM PositionCursor INTO @NewsPositionId, @PositionTypeId, @NewsIdForBomd, @ZoneId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Bai noi bat trang chu va noi bat muc (set bang tay)
			IF @PositionTypeId = 1 OR @PositionTypeId = 3 
			BEGIN
				-- Neu co bai bomd doi san thi update bai bomd vao  vi tri nay
				--IF @NewsIdForBomd > 0
				IF 1 = 0
				BEGIN
					DECLARE @NewsId bigint,
							@Title nvarchar(250),
							@SubTitle nvarchar(250),
							@Sapo nvarchar(MAX),
							@Avatar nvarchar(250),
							@AvatarDesc nvarchar(500),
							@Avatar1 nvarchar(500),
							@Avatar2 nvarchar(500),
							@Avatar3 nvarchar(500),
							@Avatar4 nvarchar(500),
							@Avatar5 nvarchar(500),
							@Author nvarchar(50),
							@NewsRelation nvarchar(2000),
							@Source nvarchar(100),
							@IsFocus bit,
							@NewsType tinyint,
							@ThreadId int,
							@DistributionDate datetime,
							@Url nvarchar(500),
							@OriginalId int
					SELECT TOP(1) @NewsId=NewsId,
							@Title=Title,
							@SubTitle=SubTitle,
							@Sapo=Sapo,
							@Avatar=Avatar,
							@AvatarDesc=AvatarDesc,
							@Avatar1=Avatar1,
							@Avatar2=Avatar2,
							@Avatar3=Avatar3,
							@Avatar4=Avatar4,
							@Avatar5=Avatar5,
							@Author=Author,
							@NewsRelation=NewsRelation,
							@Source=Source,
							@IsFocus=IsFocus,
							@NewsType=Type,
							@ThreadId=ThreadId,
							@DistributionDate=DistributionDate,
							@Url=Url,
							@OriginalId=OriginalId
					FROM NewsPublish
					WHERE NewsId = @NewsIdForBomd 
						AND ((@ZoneId = 0 AND IsPrimary = 1) OR (@ZoneId <> 0 AND ZoneId = @ZoneId)) 
						AND (DistributionDate <= GETDATE())
					
					IF @NewsId IS NOT NULL AND @NewsId > 0
					BEGIN
						UPDATE NewsPosition
						SET NewsId = @NewsId,
							Title = @Title,
							SubTitle = @SubTitle,
							Sapo = @Sapo,
							Avatar = @Avatar,
							AvatarDesc = @AvatarDesc,
							Avatar1 = @Avatar1,
							Avatar2 = @Avatar2,
							Avatar3 = @Avatar3,
							Avatar4 = @Avatar4,
							Avatar5 = @Avatar5,
							Author = @Author,
							NewsRelation = @NewsRelation,
							Source = @Source,
							IsFocus = @IsFocus,
							NewsType = @NewsType,
							ThreadId = @ThreadId,
							DistributionDate = @DistributionDate,
							Url = @Url,
							OriginalId=@OriginalId
						WHERE Id = @NewsPositionId
					END
					ELSE
					BEGIN
						UPDATE NewsPosition
						SET NewsId = 0, NewsIdForBomd = 0, Title = N'', SubTitle = N'', Sapo = N'', Avatar = N'', AvatarDesc = N'', Avatar1 = N'', 
							Avatar2 = N'', Avatar3 = N'', Avatar4 = N'', Avatar5 = N'', Author = N'', NewsRelation = N'', 
							Source = N'', IsFocus = 0, NewsType = 0, ThreadId = 0, DistributionDate = NULL, Url = N'', 
							DisplayStyle = 0, DisplayPosition = 0, DisplayInSlide = 0, AvatarCustom = '', OriginalId = 0
						WHERE (Id = @NewsPositionId)
					END
				END
				ELSE -- Khong phai la bai bomd thi xoa tin o vi tri nay
				BEGIN
					UPDATE NewsPosition
					SET NewsId = 0, Title = N'', SubTitle = N'', Sapo = N'', Avatar = N'', AvatarDesc = N'', Avatar1 = N'', 
						Avatar2 = N'', Avatar3 = N'', Avatar4 = N'', Avatar5 = N'', Author = N'', NewsRelation = N'', 
						Source = N'', IsFocus = 0, NewsType = 0, ThreadId = 0, DistributionDate = NULL, Url = N'', 
						DisplayStyle = 0, DisplayPosition = 0, DisplayInSlide = 0, AvatarCustom = '', OriginalId = 0
					WHERE (Id = @NewsPositionId)
				END
			END
		
			FETCH NEXT FROM PositionCursor INTO @NewsPositionId, @PositionTypeId, @NewsIdForBomd, @ZoneId
		END
		
		CLOSE PositionCursor   
		DEALLOCATE PositionCursor
		
		
		
		-- Update to ExportToChannels for exporting to channels
		IF EXISTS(SELECT 1 FROM ExportToChannels WHERE NewsId = @Id)
		BEGIN
			UPDATE ExportToChannels SET IsDeleted = 1
			WHERE NewsId = @Id
		END
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END















