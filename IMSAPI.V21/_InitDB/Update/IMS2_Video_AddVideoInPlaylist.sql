USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms2_Playlist_AddVideoList]    Script Date: 09/30/2017 09:22:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE  [dbo].[VideoCms2_Playlist_AddVideoList]
	@VideoIdList varchar(2000) = '',
	@TimeToPlayList varchar(max) = '',
	@PlaylistId int = 0,
	@DeleteOldVideo bit = 0
AS
	BEGIN TRANSACTION
		
	BEGIN	
		DECLARE @TABLE_VIDEO TABLE (Id int identity(1,1), VideoId int);
		DECLARE @TABLE_VIDEO_TIME TABLE (Id int identity(1,1), TimeToPlay datetime);
		DECLARE @VideoId int,
				@Index int,
				@TimeToPlay datetime;
				
		INSERT INTO @TABLE_VIDEO (VideoId)
		SELECT CONVERT(int, Part) FROM SplitString(@VideoIdList, ';')
		
		INSERT INTO @TABLE_VIDEO_TIME (TimeToPlay)
		SELECT CONVERT(datetime, Part) FROM SplitString(@TimeToPlayList, ';')
		
		IF @DeleteOldVideo = 1
		BEGIN
			DELETE FROM VideoPlayList WHERE PlayListId = @PlayListId
			
			UPDATE PlayList SET VideoCount = 0
				WHERE Id = @PlaylistId
		END
		
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TABLE_VIDEO))
		BEGIN
			SET @VideoId = (SELECT VideoId FROM @TABLE_VIDEO WHERE Id = @Index);
			SET @TimeToPlay = (SELECT TimeToPlay FROM @TABLE_VIDEO_TIME WHERE Id = @Index);
			-- Check exists video
			IF (@VideoId IS NOT NULL 
				AND NOT EXISTS (SELECT VideoId 
								FROM VideoPlayList 
								WHERE VideoId = @VideoId AND PlaylistId = @PlaylistId)
			)
			BEGIN
				IF @TimeToPlay IS NULL
					SET @TimeToPlay = GETDATE();
					
				INSERT INTO VideoPlayList (
					PlaylistId, 
					VideoId, 
					Priority,
					PlayOnTime
					)
				VALUES (
					@PlaylistId,
					@VideoId,
					@Index,
					@TimeToPlay
				)
				UPDATE PlayList SET VideoCount = VideoCount + 1
				WHERE Id = @PlaylistId
			END
			SET @Index = @Index + 1;
		END
	END
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END
	ELSE
		COMMIT TRANSACTION














