

CREATE PROCEDURE [dbo].[CMS_News_UpdateNewsByErrorCheck]
	@NewsId bigint,	
	@NewsTitle nvarchar(300),
	@NewsSapo nvarchar(500),
	@NewsBody nvarchar(MAX),
	@NewsUrl varchar(500),
	@AccountName varchar(200)
AS
	BEGIN
		-- BEGIN TRANSACTION 
		BEGIN TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
			-- UPDATE News
			UPDATE News SET 
				Title = @NewsTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl,
				ErrorCheckedBy = @AccountName,
				ErrorCheckedDate = GETDATE()
			WHERE Id = @NewsId
			
			-- UPDATE NewsPublish
			UPDATE NewsPublish SET
				Title = @NewsTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsPosition
			UPDATE NewsPosition SET
				Title = @NewsTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsContent				
			UPDATE NewsContent SET
				Title = @NewsTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl
			WHERE NewsId = @NewsId
			
			SELECT Id, Title, SubTitle, Sapo, Body, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, 
				Avatar5, Author, NewsRelation, Status, Source, IsFocus, Type, ThreadId, CreatedDate, 
				LastModifiedDate, ISNULL(DistributionDate, GETDATE()) AS DistributionDate, CreatedBy, 
				LastModifiedBy, PublishedBy, EditedBy, LastReceiver, WordCount, ViewCount, Priority, 
				Tag, OriginalId, IsOnHome, DisplayInSlide, DisplayPosition,Url, TagItem, InitSapo, DisplayStyle
			INTO #NewsData
			FROM News
			WHERE (Id = @NewsId)
			
			
			DROP TABLE #NewsData
		-- COMMIT
		COMMIT TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
		IF @@ERROR <> 0
		BEGIN
			DECLARE @Message nvarchar(500)
			SET @Message = ERROR_MESSAGE();
			RAISERROR (@Message, 16, 1)
			ROLLBACK TRANSACTION
		END
	END














