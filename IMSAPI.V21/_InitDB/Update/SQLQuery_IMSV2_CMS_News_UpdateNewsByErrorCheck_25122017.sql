USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_UpdateNewsByErrorCheck]    Script Date: 12/23/2017 11:28:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[CMS_News_UpdateNewsByErrorCheck]
	@NewsId bigint,	
	@NewsTitle nvarchar(300),
	@NewsSubTitle nvarchar(250),
	@NewsSapo nvarchar(500),
	@NewsBody nvarchar(MAX),
	@NewsUrl varchar(500),
	@NewsAvatarDesc nvarchar(500),
	@NewsNote nvarchar(2000),
	@NewsTag nvarchar(MAX),
	@AccountName varchar(200)
AS
	BEGIN
		-- BEGIN TRANSACTION 
		BEGIN TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
			-- UPDATE News
			UPDATE News SET 
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc,
				Note = @NewsNote,
				Tag = @NewsTag,
				ErrorCheckedBy = @AccountName,
				ErrorCheckedDate = GETDATE()
			WHERE Id = @NewsId
			
			-- UPDATE NewsPublish
			UPDATE NewsPublish SET
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsPosition
			UPDATE NewsPosition SET
				Title = @NewsTitle,
				Sapo = @NewsSapo,				
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc				
			WHERE NewsId = @NewsId
			
			-- UPDATE NewsContent				
			UPDATE NewsContent SET
				Title = @NewsTitle,
				SubTitle = @NewsSubTitle,
				Sapo = @NewsSapo,
				Body = @NewsBody,
				Url = @NewsUrl,
				AvatarDesc = @NewsAvatarDesc
			WHERE NewsId = @NewsId
			
			--SELECT Id, Title, SubTitle, Sapo, Body, Avatar, AvatarDesc, Avatar2, Avatar3, Avatar4, 
			--	Avatar5, Author, NewsRelation, Status, Source, IsFocus, Type, ThreadId, CreatedDate, 
			--	LastModifiedDate, ISNULL(DistributionDate, GETDATE()) AS DistributionDate, CreatedBy, 
			--	LastModifiedBy, PublishedBy, EditedBy, LastReceiver, WordCount, ViewCount, Priority, 
			--	Tag, OriginalId, IsOnHome, DisplayInSlide, DisplayPosition,Url, TagItem, InitSapo, DisplayStyle
			--INTO #NewsData
			--FROM News
			--WHERE (Id = @NewsId)
			
			
			--DROP TABLE #NewsData
		-- COMMIT
		COMMIT TRANSACTION CMS_News_UpdateNewsByErrorCheck
		
		IF @@ERROR <> 0
		BEGIN
			DECLARE @Message nvarchar(500)
			SET @Message = ERROR_MESSAGE();
			RAISERROR (@Message, 16, 1)
			ROLLBACK TRANSACTION
		END
	END














