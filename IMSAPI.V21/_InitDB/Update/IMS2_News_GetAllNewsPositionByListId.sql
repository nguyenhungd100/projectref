CREATE PROCEDURE [dbo].[CMS_NewsPosition_GetAllPositionByListId] 
 @Ids varchar(500)
AS
BEGIN
 SELECT N.Id, ZoneId, TypeId, [Order]
 FROM NewsPosition N
 WHERE N.NewsId = 0 AND N.Id IN (SELECT * FROM SplitStrings(@Ids))
END