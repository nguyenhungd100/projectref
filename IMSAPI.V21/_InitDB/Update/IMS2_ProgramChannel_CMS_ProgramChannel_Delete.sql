USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ProgramChannel_Delete]    Script Date: 1/2/2018 7:04:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[CMS_ProgramChannel_Delete]
	@Id int
AS
BEGIN
	--DELETE FROM ProgramScheduleDetail 
	--WHERE ProgramScheduleId IN (SELECT Id FROM ProgramSchedule WHERE ProgramChannelId = @Id)
	
	--DELETE FROM ProgramSchedule 
	--WHERE ProgramChannelId = @Id
	
	DELETE FROM ProgramChannel
	WHERE Id = @Id
END













