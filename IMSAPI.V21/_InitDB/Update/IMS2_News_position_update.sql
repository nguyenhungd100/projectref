﻿/****** Object:  StoredProcedure [dbo].[CMS_NewsPosition_Update]    Script Date: 10/09/2017 14:03:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:				<ThanhTN>
-- Edited:				<NANIA>
-- Create date:			<2012-10-08>
-- Last modified date:	<2013-03-28>
-- Last modified by:	<ThanhTN>
-- Description:			<Update news position>
-- =============================================
ALTER PROCEDURE [dbo].[CMS_NewsPosition_Update]
	@TypeId int,
	@ZoneId int = 0,
	@Order int,
	@NewsId bigint,
	@ExpiredLock datetime = NULL,
	@NewAvatar nvarchar(500) = '',
	@AvatarIndex int = 0,
	@CheckNewsExists bit = 1
AS
BEGIN
	BEGIN TRANSACTION
	IF @CheckNewsExists = 0 OR (@CheckNewsExists = 1 AND NOT EXISTS (SELECT 1 FROM NewsPosition WHERE NewsId = @NewsId AND TypeId = @TypeId AND ZoneId = @ZoneId))
	BEGIN
		BEGIN TRY
			DECLARE @Id int,
					@IsAllowAutoUpdate bit
			
			DECLARE @IsFilterByZoneId tinyint
			SET @IsFilterByZoneId = ISNULL((SELECT FilterByZoneId FROM NewsPositionType WHERE Id = @TypeId), 0)
			
			SELECT @Id = Id, @IsAllowAutoUpdate = AllowAutoUpdate
			FROM NewsPosition
			WHERE TypeId = @TypeId AND (@IsFilterByZoneId <> 1 OR (@IsFilterByZoneId = 1 AND ZoneId = @ZoneId)) AND [Order] = @Order
			
			IF @Id > 0
			BEGIN
				DECLARE @Title nvarchar(250),
						@SubTitle nvarchar(250),
						@Sapo nvarchar(MAX),
						@Avatar nvarchar(250),
						@AvatarDesc nvarchar(500),
						@Avatar2 nvarchar(500),
						@Avatar3 nvarchar(500),
						@Avatar4 nvarchar(500),
						@Avatar5 nvarchar(500),
						@Author nvarchar(50),
						@NewsRelation nvarchar(max),
						@Source nvarchar(100),
						@IsFocus bit,
						@NewsType tinyint,
						@Type tinyint,
						@ThreadId int,
						@DistributionDate datetime,
						@Url nvarchar(500),						
						-- Extension fields
						@DisplayStyle int,
						@DisplayPosition int,
						@DisplayInSlide int,
						@AvatarCustom varchar(500),
						@OriginalId int,
						@InitSapo nvarchar(500),
						@OriginalUrl nvarchar(500),
						@IsPr bit,
						@AdStore bit,
						@AdStoreUrl nvarchar(500),
						@InterviewId int,
						@RollingNewsId int,
						@LocationType int,
						@ZoneIdForNews int
						
				SELECT @Title=Title, 
						@SubTitle=SubTitle, 
						@Sapo=Sapo, 
						@Avatar= Avatar, 
						@AvatarDesc=AvatarDesc, 
						@Avatar2=Avatar2, 
						@Avatar3=Avatar3, 
						@Avatar4=Avatar4, 
						@Avatar5=Avatar5, 
						@Author=Author, 
						@NewsRelation=NewsRelation, 
						@Source=Source, 
						@IsFocus=IsFocus, 
						@Type=[Type], 
						@NewsType=[NewsType], 
						@ThreadId=ThreadId, 
						@DistributionDate=DistributionDate, 
						@Url=Url,
						@DisplayStyle = DisplayStyle,
						@DisplayPosition = DisplayPosition,
						@DisplayInSlide = DisplayInSlide,
						@AvatarCustom = AvatarCustom,
						@OriginalId = OriginalId,
						@InitSapo = InitSapo,
						@OriginalUrl = OriginalUrl,
						@IsPr = IsPr,
						@AdStore = AdStore,
						@AdStoreUrl = AdStoreUrl,
						@InterviewId = InterviewId,
						@RollingNewsId = RollingNewsId,
						@LocationType = LocationType,
						@ZoneIdForNews = ZoneId
				FROM NewsPublish
				WHERE NewsId=@NewsId AND IsPrimary = 1

				DECLARE @ScheduleDate datetime
				SET @ScheduleDate = GETDATE()
				IF @ScheduleDate <= @DistributionDate
					SET @ScheduleDate = @DistributionDate
				
				UPDATE NewsPosition
				SET NewsIdForBomd = NewsId,
					NewsId=@NewsId,
					Title=@Title,
					SubTitle=@SubTitle,
					Sapo=@Sapo,
					Avatar=@Avatar,
					AvatarDesc=@AvatarDesc,
					Avatar2=@Avatar2,
					Avatar3=@Avatar3,
					Avatar4=@Avatar4,
					Avatar5=@Avatar5,
					Author=@Author,
					NewsRelation=@NewsRelation,
					Source=@Source,
					IsFocus=@IsFocus,
					NewsType=@NewsType,
					Type=@Type,
					ThreadId=@ThreadId,
					DistributionDate=@DistributionDate,
					Url=@Url,
					Locked=1,
					ExpiredLock = GETDATE(),					
					DisplayStyle = @DisplayStyle,
					DisplayPosition = @DisplayPosition,
					DisplayInSlide = @DisplayInSlide,
					AvatarCustom = @AvatarCustom,
					OriginalId = @OriginalId,
					InitSapo = @InitSapo,
					OriginalUrl = @OriginalUrl,
					IsPr = @IsPr,
					AdStore = @AdStore,
					AdStoreUrl = @AdStoreUrl,
					InterviewId = @InterviewId,
					RollingNewsId = @RollingNewsId,
					LocationType = @LocationType,
					ScheduleDate = @ScheduleDate,
					LastModifiedDate = GETDATE(),
					ZoneIdForNews = @ZoneIdForNews
				WHERE Id = @Id
				
				UPDATE NewsPublish
				SET DisplayPosition = 2
				WHERE NewsId = @NewsId
				
				UPDATE News
				SET DisplayPosition = 2
				WHERE Id = @NewsId
				
				UPDATE NewsPosition
				SET DisplayPosition = 2
				WHERE Id = @NewsId
			END
				
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage nvarchar(MAX)
			SET @ErrorMessage = ERROR_MESSAGE()
			RAISERROR (@ErrorMessage, 16, 1);
			ROLLBACK TRANSACTION
		END CATCH
	END
	ELSE
	BEGIN
		RAISERROR ('#403: This news existed in Position', 16, 1);
		ROLLBACK TRANSACTION
	END
END





















