Create PROCEDURE [dbo].[CMS_User_ResetOTPByUserId]
 @Id int
AS
BEGIN
 UPDATE [User]
 SET OtpSecretKey = '',
  ModifiedDate = GETDATE()
 WHERE Id = @Id
END