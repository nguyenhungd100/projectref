﻿USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_VideoLabel_MoveUp]    Script Date: 12/28/2017 19:37:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









CREATE PROCEDURE  [dbo].[VideoCms_VideoLabel_MoveUp]
	@Id int
AS
	BEGIN TRANSACTION
	
	BEGIN TRY
		IF NOT EXISTS (SELECT Id FROM VideoLabel WHERE Id = @Id)
		BEGIN
			RAISERROR ('This VideoLabel does not exist in database.', 16, 1);
			ROLLBACK TRANSACTION
		END
		ELSE BEGIN
			DECLARE @ParentId int, @Priority int, @PreviousId int, @PreviousPriority int;
			SET @ParentId = (SELECT ParentId FROM VideoLabel WHERE Id = @Id)
			SET @Priority = (SELECT [Priority] FROM VideoLabel WHERE Id = @Id);
			
			--SET @Priority = @Priority - 1;
			
			SELECT TOP 1 @PreviousPriority=[Priority], @PreviousId=Id
			FROM VideoLabel
			WHERE ParentId = @ParentId AND [Priority] <= @Priority AND Id <> @Id
			ORDER BY [Priority] DESC, Id DESC
			
			IF @PreviousPriority IS NOT NULL AND @PreviousPriority = @Priority
				SET @PreviousPriority = @PreviousPriority - 1
				
			-- Co item o tren
			IF @PreviousPriority IS NOT NULL
			BEGIN
				UPDATE VideoLabel SET [Priority] = @PreviousPriority WHERE Id = @Id
				UPDATE VideoLabel SET [Priority] = @Priority WHERE Id = @PreviousId
			END
			
			COMMIT TRANSACTION
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH



