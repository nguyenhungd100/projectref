--USE [IMS_KENH14]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_InitESAllNews]    Script Date: 08/10/2017 09:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

--ES
GO
ALTER PROCEDURE [dbo].[CMS_News_InitESAllNews]	
	@PageIndex int = 1,
	@PageSize int = 10,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
DECLARE @UpperBand int, @LowerBand int

SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

SET @LowerBand  = (@pageIndex - 1) * @PageSize
SET @UpperBand  = (@pageIndex * @PageSize)
SELECT * FROM (
SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.PublishedBy as ApprovedBy,
n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate,z.Name as ZoneName, ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
FROM News n 
left join NewsInZone nz on nz.NewsId=n.Id
left join Zone z on z.Id=nz.ZoneId
where nz.IsPrimary=1 and ((@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo))))
) AS temp
WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
END

--REDIS
GO
ALTER PROCEDURE [dbo].[CMS_News_InitRedisAllNews]	
	@PageIndex int = 1,
	@PageSize int = 10,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
DECLARE @UpperBand int, @LowerBand int

SELECT @totalRow = COUNT(*) FROM News n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

SET @LowerBand  = (@pageIndex - 1) * @PageSize
SET @UpperBand  = (@pageIndex * @PageSize)
SELECT * FROM (
SELECT n.Id,n.Title,n.SubTitle,n.Sapo,n.Body,n.Avatar,n.AvatarDesc,n.Avatar2,n.Avatar3,n.Avatar4,n.Avatar5,n.Author,n.NewsRelation,n.Status,
n.Source,n.IsFocus,n.Type,n.ThreadId,n.CreatedDate,n.LastModifiedDate,n.DistributionDate,n.CreatedBy,n.LastModifiedBy,n.PublishedBy,n.EditedBy,
n.LastReceiver,n.WordCount,n.ViewCount,n.Priority,n.Tag,n.Note,n.TagPrimary,n.Price,n.DisplayStyle,n.DisplayPosition,n.DisplayInSlide,n.AvatarCustom,
n.OriginalId,n.NewsType,n.IsOnHome,n.Url,n.NoteRoyalties,n.TagItem,n.NewsCategory,n.InitSapo,n.TemplateName,n.TemplateConfig,n.InterviewId,n.IsBreakingNews,
n.OriginalUrl,n.IsPr,n.AdStore,n.AdStoreUrl,n.PrBookingNumber,n.PegaBreakingNews,n.RollingNewsId,n.IsOnMobile,n.TagSubTitleId,n.PrPosition,n.LocationType,
n.ExpiredDate,n.SourceUrl,n.BonusPrice,n.ViewCountMobile,n.PhotoCount,n.VideoCount,n.ShortTitle,n.ParentNewsId,n.WarningLevel,n.PublishedBy as ApprovedBy,
n.ApprovedDate,n.ReturnedBy,n.SentBy,n.ErrorCheckedBy,n.ErrorCheckedDate,n.SensitiveCheckedBy,n.SensitiveCheckedDate, ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
FROM News n 
where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
) AS temp
WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
END

--dbo.CMS_fGetListOfZoneIdByNewsId(n.Id) AS ListZoneId,

--declare @TotalRow int 
--exec [dbo].[CMS_News_InitESAllNews] 1,500,'2017-03-01 11:03','2017-03-30 11:03',@TotalRow out
--select @TotalRow
Go

--Video ES
CREATE PROCEDURE [dbo].[VideoCms_Video_InitESAllVideo]
	@PageIndex int = 1,
	@PageSize int = 10,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@TotalRow int = 0 OUTPUT
as
begin
	DECLARE @UpperBand int, @LowerBand int

    SELECT @totalRow = COUNT(*) FROM Video v where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,v.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,v.CreatedDate)<=CONVERT(datetime,@DateTo)))

    SET @LowerBand  = (@pageIndex - 1) * @PageSize
    SET @UpperBand  = (@pageIndex * @PageSize)
    SELECT * FROM (
    SELECT v.*,zv.Name as ZoneName,ROW_NUMBER() OVER(ORDER BY v.Id DESC) AS RowNumber 
    FROM Video v   
    left join ZoneVideo zv on zv.Id=v.ZoneId                                      
    where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,v.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,v.CreatedDate)<=CONVERT(datetime,@DateTo)))
    ) AS temp
    WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
end

go
--declare @TotalRow int 
--exec [dbo].[VideoCms_Video_InitESAllVideo] 1,500,'2017-03-01 11:03','2017-03-30 11:03',@TotalRow out
--select @TotalRow