USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_ChangeStatusToReturnedToEditorialSecretary]    Script Date: 12/15/2017 15:35:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[CMS_News_ChangeStatusToReturnedToEditorialSecretary]
	@Id bigint,
	@ApprovedBy varchar(200),
	@UserName varchar(200),
	@Note nvarchar(2000) = ''
AS
BEGIN
	/*
	Temporary = 1,
    WaitForEdit = 2,
    RecievedForEdit = 3,
    ReturnedToReporter = 4,
    WaitForPublish = 5,
    RecievedForPublish = 6,
    ReturnedToEditor = 7,
    Published = 8,
    MovedToTrash = 9,
    ReturnedToEditorialSecretary=19
    */
	BEGIN TRANSACTION
	
	BEGIN TRY
		UPDATE News
		SET [Status] = 19, 
			ApprovedBy=@ApprovedBy,
			ReturnedBy=@UserName,
			LastModifiedDate = GETDATE(),
			Note = CASE @Note WHEN '' THEN Note ELSE @Note END
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

