﻿--<I> CẦN CHỈNH ĐUÔI URL LÀ '.chn'(KÊNH) HAY '.htm'(BÁO). TRONG SP_STORE: SYNC_News & SYNC_News_Published
--<II> CẦN CHECK VÀ UPDATE LẠI (nếu cần) OriginalUrl & Url cho dbo.NEWS, dbo.NEWS_CONTENT, dbo.NEWS_PUBLISHED

GO

print [dbo].[BuildCateUrl](6)
print dbo.ConvertToBasicLatin(N'[Video] Xu hướng nhà ở tương lai: Căn hộ “đóng hộp”')

GO

--DECLARE @ZoneUrl varchar(500), @N_Url varchar(500)

--SET @ZoneUrl = dbo.BuildCateUrl(6)
----SET @N_OriginalUrl = '/' + @ZoneUrl + '/' + dbo.ConvertToBasicLatin(@OriginalTitle) + '-' + CAST(@N_Id AS VARCHAR(30)) + '.htm'
--SET @N_Url = '/' + @ZoneUrl + '/' + dbo.ConvertToBasicLatin(N'[Video] Xu hướng nhà ở tương lai: Căn hộ “đóng hộp”') + '-' + CAST(2017072702292282 AS VARCHAR(30)) + '.htm'	

--print @N_Url

GO

SELECT TOP 1000 a.*, b.ZoneId, dbo.BuildCateUrl(b.ZoneId) as ZoneUrl, ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.Id AS VARCHAR(30)) + '.htm') as Url_Ref
  FROM [dbo].[News] a JOIN [dbo].[NewsInZone] b ON a.Id = b.NewsId
  WHERE Url LIKE '%.chn'

SELECT TOP 1000 a.*, b.ZoneId, dbo.BuildCateUrl(b.ZoneId) as ZoneUrl, ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm') as Url_Ref
  FROM [dbo].[NewsContent] a JOIN [dbo].[NewsInZone] b ON a.NewsId = b.NewsId    
  WHERE a.Url LIKE '%.chn'

SELECT TOP 1000 a.*, b.ZoneId, dbo.BuildCateUrl(b.ZoneId) as ZoneUrl, ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm') as Url_Ref
  FROM [dbo].[NewsPublish] a JOIN [dbo].[NewsInZone] b ON a.NewsId = b.NewsId    
  WHERE a.Url LIKE '%.chn'

GO

--<A> CAP NHAT URL & ORIGINAL_URL CHO CAC BANG NEWS LIEN QUAN.

-- (1)
--UPDATE [dbo].[News] SET 
--	Url = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.Id AS VARCHAR(30)) + '.htm'),
--	OriginalUrl = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.Id AS VARCHAR(30)) + '.htm')
--FROM [dbo].[News] a JOIN [dbo].[NewsInZone] b ON a.Id = b.NewsId    
--WHERE ID IN (2017072702292282);

--(2)
--UPDATE [dbo].[NewsContent] SET 
--	Url = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm'),
--	OriginalUrl = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm')
--FROM [dbo].[NewsContent] a JOIN [dbo].[NewsInZone] b ON a.NewsId = b.NewsId       
--WHERE a.NewsId IN (2017072702292282);

--(3)
--UPDATE [dbo].[NewsPublish] SET 
--	Url = ('/' + dbo.BuildCateUrl(a.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm'),
--	OriginalUrl = ('/' + dbo.BuildCateUrl(a.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm')
--FROM [dbo].[NewsPublish] a 
--WHERE ID IN (2017072702292282);

GO

--<B> INIT CAI BAI NOI BAT (cho ngoai trang hien thi bai). 2 bươc sau:

--(1)
SELECT TOP 1000 *
  FROM [IMS_VNE].[dbo].[NewsPosition] where Url LIKE '%.chn'
  
--TRUNCATE TABLE [IMS_VNE].[dbo].[NewsPosition]
--EXEC [dbo].[CMS_NewsPosition_InitData]

--(2)
--UPDATE [dbo].[NewsPosition] SET 
--	Url = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm'),
--	OriginalUrl = ('/' + dbo.BuildCateUrl(b.ZoneId) + '/' + dbo.ConvertToBasicLatin(a.Title) + '-' + CAST(a.NewsId AS VARCHAR(30)) + '.htm')
--FROM [dbo].[NewsPosition] a JOIN [dbo].[NewsInZone] b ON a.NewsId = b.NewsId  
--WHERE a.Url like N'%.chn'

GO

--<C> KIEM TRA VA CAP NAHT LAI ThreadId (neu thieu)

SELECT TOP 1000 threadId, *
  FROM [IMS_VNE].[dbo].[NewsContent] 
  WHERE NewsId = 20170925102554121
  
--UPDATE [dbo].[NewsContent] 
--SET ThreadId = tn.ThreadId
--FROM [dbo].[NewsContent] nc JOIN [dbo].[ThreadNews] tn ON tn.NewsId = nc.NewsId
----WHERE nc.NewsId = 20170925102554121

SELECT TOP 1000 threadId, *
  FROM [dbo].[NewsPublish] 
  WHERE NewsId = 20170925102554121
  
--UPDATE [dbo].[NewsPublish]
--SET ThreadId = tn.ThreadId
--FROM [dbo].[NewsPublish] nc JOIN [dbo].[ThreadNews] tn ON tn.NewsId = nc.NewsId
----WHERE nc.NewsId = 20170925102554121

SELECT TOP 1000 threadId, *
  FROM [dbo].[News] 
  WHERE Id = 20170925102554121
  
--UPDATE [dbo].[News]
--SET ThreadId = tn.ThreadId
--FROM [dbo].[News] nc JOIN [dbo].[ThreadNews] tn ON tn.NewsId = nc.Id
----WHERE nc.Id = 20170925102554121

GO


