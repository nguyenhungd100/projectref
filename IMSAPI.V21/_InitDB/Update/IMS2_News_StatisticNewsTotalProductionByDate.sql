﻿USE [IMS_VNE]
GO
/****** Object:  StoredProcedure [dbo].[CMS_News_StatisticNewsTotalProductionByDate]    Script Date: 10/09/2017 17:57:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







ALTER PROCEDURE [dbo].[CMS_News_StatisticNewsTotalProductionByDate] --0,'10-09-2017','10-10-2017'
@ZoneId nvarchar (2000),
@FromDate Datetime,
@ToDate  Datetime
AS
BEGIN

DECLARE @zoneIds nvarchar (1000);


  IF @ZoneId = ''
	BEGIN
	
		
		SELECT tmp.ValuesDate,SUM(ValuesDataCreated)AS ValuesDataCreated,SUM(ValuesDataPublish) AS  ValuesDataPublish FROM 
        (
			   SELECT
					  CONVERT (NVARCHAR(20),NewsContent.PublishedDate,103) as ValuesDate,
					  0 as ValuesDataCreated,
					  COUNT (*)  as ValuesDataPublish
			   FROM 	[News] INNER JOIN NewsContent ON News.Id = NewsContent.NewsId
			   
			   WHERE	
					 NewsContent.PublishedDate>=  @FromDate AND  NewsContent.PublishedDate <= (@ToDate + ' 23:59:59')
					 AND	[News].Status	=	8
			   GROUP BY CONVERT (NVARCHAR(20), NewsContent.PublishedDate,103)
		
	      UNION ALL
	      
		       SELECT
					  CONVERT (NVARCHAR(20),[News].CreatedDate,103) as ValuesDate,
					  COUNT (*) as ValuesDataCreated,
					  0 as ValuesDataCreated 
					  
			   FROM 	[News] 
			  
			   WHERE
						[News].CreatedDate >=  @FromDate AND  [News].CreatedDate <= (@ToDate + ' 23:59:59')
						AND	[News].Status <> 1
			   GROUP BY  CONVERT (NVARCHAR(20),[News].CreatedDate,103)
			  
         ) AS tmp GROUP BY tmp.ValuesDate ORDER BY  convert(datetime, tmp.ValuesDate, 103) ASC
			  

    END
	
  ELSE
  
   BEGIN
  	    SET @zoneIds =  dbo.CMS_fGetListZoneId(@ZoneId);
  	    print @zoneIds;
		SELECT tmp.ValuesDate,SUM(ValuesDataCreated)AS ValuesDataCreated,SUM(ValuesDataPublish) AS  ValuesDataPublish  FROM 
		 (
			    SELECT
					  CONVERT (NVARCHAR(50),NewsContent. PublishedDate,103) as ValuesDate,
					  0 as ValuesDataCreated,
					  COUNT (*)  as ValuesDataPublish
					 -- NewsContent.ZoneId AS ZoneId
				FROM 	[News] INNER JOIN NewsContent ON News.Id = NewsContent.NewsId
				WHERE	
						NewsContent.PublishedDate>= @FromDate AND  NewsContent.PublishedDate <= (@ToDate + ' 23:59:59')
						AND	[News].Status	=	8 AND NewsContent.ZoneId IN (	SELECT  CONVERT(int, TL.part) AS ZoneId 
											FROM dbo.SplitString(@zoneIds, ',') AS TL) 
			    GROUP BY CONVERT (NVARCHAR(50), NewsContent.PublishedDate,103)
					  
			    UNION ALL
			      
				SELECT
							  CONVERT (NVARCHAR(50),[News].CreatedDate,103) as ValuesDate,
							  COUNT (*) as ValuesDataCreated,
							  0 as ValuesDataCreated 
							  -- NewsInZone.ZoneId  AS ZoneId
			    FROM 	[News]   INNER JOIN NewsInZone ON News.Id = NewsInZone.NewsId
			    WHERE
							[News].CreatedDate >=  @FromDate AND  [News].CreatedDate<= (@ToDate + ' 23:59:59')
						
							AND	[News].Status <> 1 AND NewsInZone.ZoneId IN (	SELECT  CONVERT(int, TL.part) AS ZoneId 
											FROM dbo.SplitString(@zoneIds, ',') AS TL) AND NewsInZone.IsPrimary =1 
			    GROUP BY   CONVERT (NVARCHAR(50),[News].CreatedDate,103)
				  
		  ) AS tmp GROUP BY tmp.ValuesDate ORDER BY  convert(datetime, tmp.ValuesDate, 103) ASC
			  
 
    END
	
	
END

















