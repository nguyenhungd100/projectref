USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_VideoChannel_GetById]    Script Date: 01/03/2018 13:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CMS_VideoChannel_GetById]
@Id int
AS
BEGIN
  SELECT *
  FROM [VideoChannel]
  WHERE Id = @Id
END






