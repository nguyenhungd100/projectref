USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_StatictisFollowPlaylist]    Script Date: 01/29/2018 10:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
ALTER PROCEDURE  [dbo].[CMS_EndUser_StatictisFollowPlaylist]
	@From datetime,
	@To datetime
AS
	BEGIN TRANSACTION
	
	SELECT Name, PlayListId, count(PlayListId) as [Count] FROM PlayListFollow
	LEFT JOIN PlayList on PlayListId=Id
	where FollowedDate>=@From and FollowedDate<=@To and Name is not null
	GROUP BY PlayListId, Name
	order by [Count] desc
	
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION




