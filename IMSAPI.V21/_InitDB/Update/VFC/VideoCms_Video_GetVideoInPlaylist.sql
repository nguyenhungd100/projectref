USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_GetVideoInPlaylist]    Script Date: 01/20/2018 10:04:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE [dbo].[VideoCms_Video_GetVideoInPlaylist]
    @PlaylistId INT = 0 ,
    @PageIndex INT ,
    @PageSize INT ,
    @TotalRow INT OUTPUT,
    @MaxPriority INT OUTPUT
AS 
    SET NOCOUNT ON ;
    SET @TotalRow = ( SELECT    COUNT(0)
                      FROM      VideoPlaylist
                      WHERE     PlaylistId = @PlaylistId
                    )
    SET @MaxPriority = ( SELECT    max(priority)
                      FROM      VideoPlaylist
                      WHERE     PlaylistId = @PlaylistId
                    )
    SELECT  *
    FROM    ( SELECT    [ID] ,
                        [ZoneId] ,
                        [Name] ,
                        [Description] ,
                        [HtmlCode] ,
                        [Avatar] ,
                        [KeyVideo] ,
                        [PName] ,
                        [PublishDate] ,
                        [CreatedDate] ,
						[EditedDate] ,
                        [Status] ,
                        [NewsId] ,
                        [DistributionDate] ,
                        [Views] ,
                        [Mode] ,
                        [LastModifiedDate] ,
                        [LastModifiedBy] ,
						[CreatedBy],
						[EditedBy],
						[PublishBy],
						[ParentId],
						[Duration],
                        [Tags] ,
                        VdoPL.Priority ,
                        VdoPL.PlayOnTime ,
                        ROW_NUMBER() OVER ( ORDER BY VdoPL.PlayOnTime, VdoPL.Priority DESC ) AS RowNum
              FROM      Video AS Vdo
                        INNER JOIN VideoPlaylist AS VdoPL ON Vdo.Id = VdoPL.VideoId
              WHERE     VdoPL.PlaylistId = @PlaylistId              
            ) AS Temp
    WHERE   RowNum BETWEEN ( ( @PageIndex - 1 ) * @PageSize + 1 )
                   AND     @PageIndex * @PageSize
    ORDER BY Priority ASC















