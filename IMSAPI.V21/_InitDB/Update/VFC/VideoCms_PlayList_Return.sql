USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_Return]    Script Date: 01/10/2018 10:02:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_PlayList_Return]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM PlayList WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this PlayList.', 16, 1);
		ROLLBACK TRANSACTION
	END
	BEGIN
		UPDATE PlayList
		SET Status = 4
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
















