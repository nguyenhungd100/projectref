USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_Update]    Script Date: 01/19/2018 15:15:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE  [dbo].[VideoCms_ZoneVideo_Update]
	@Id int,
	@Name nvarchar(250),
	@Url nvarchar(50),
	@Order int,
	@ParentId int,
	@ModifiedDate datetime,
	@Status int,
	@ListVideoTagId varchar(1000) = '',
	@DisplayStyle int=0,
	@ShowOnHome bit = 1,
	@Invisibled bit = 1,
	@Avatar varchar(300),
	@AvatarCover varchar(300),
	@ZoneRelation varchar(100),
	@Logo varchar(300),
	@MetaAvatar varchar(max)
AS
	BEGIN TRANSACTION
	
	UPDATE ZoneVideo
	SET Name = @Name, 
		Url = @Url, 
		[Order] = @Order, 
		ParentId = @ParentId, 
		Status = @Status, 
		ModifiedDate = @ModifiedDate,
		DisplayStyle =@DisplayStyle,
		ShowOnHome =@ShowOnHome,
		Invisibled = @Invisibled,
		AvatarCover = @AvatarCover,
		Avatar = @Avatar,
		ZoneRelation=@ZoneRelation,
		Logo=@Logo,
		MetaAvatar=@MetaAvatar
	WHERE (Id = @Id)

	DELETE FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id
	IF  @ListVideoTagId <> ''
	BEGIN
		DECLARE @TblVideoTags TABLE (Id int identity(1,1), VideoTagId int NOT NULL)
		DECLARE @Index int,
				@VideoTagId int
		-- Tag Id List
		INSERT INTO @TblVideoTags (VideoTagId)
		SELECT CONVERT(int, part) FROM SplitString(@ListVideoTagId, ';')
			
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TblVideoTags)) BEGIN
			SET @VideoTagId = (SELECT VideoTagId FROM @TblVideoTags WHERE Id = @Index);
			
			-- Add to VideoTags
			IF @VideoTagId > 0 AND NOT EXISTS (SELECT 1 FROM ZoneVideoInVideoTag WHERE ZoneVideoId = @Id AND VideoTagId = @VideoTagId)
			BEGIN
				INSERT INTO ZoneVideoInVideoTag(ZoneVideoId, VideoTagId, Priority)
				VALUES (@Id, @VideoTagId, @Index)
			END
			SET @Index = @Index + 1;
		END
	END

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION



