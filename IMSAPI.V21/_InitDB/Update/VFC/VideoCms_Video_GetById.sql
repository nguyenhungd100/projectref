USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_GetById]    Script Date: 01/23/2018 11:41:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Video_GetById]
	@VideoId int = 0
AS
	IF @VideoId > 0
	BEGIN
		SELECT *,
            (SELECT     Name
			FROM          ZoneVideo
			WHERE      (Id = Video.ZoneId)) AS ZoneName
	FROM Video
	WHERE (Id = @VideoId)
	END


