USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_PlayList_InitRedisAllPlayList]    Script Date: 01/13/2018 15:27:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE [dbo].[CMS_PlayList_InitRedisAllPlayList]	
	@PageIndex int = 1,
	@PageSize int = 10,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@TotalRow int = 0 OUTPUT
AS
BEGIN
DECLARE @UpperBand int, @LowerBand int

SELECT @totalRow = COUNT(*) FROM PlayList n where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))

SET @LowerBand  = (@pageIndex - 1) * @PageSize
SET @UpperBand  = (@pageIndex * @PageSize)
SELECT * FROM (
SELECT n.*,zv.Name as ZoneName,
ROW_NUMBER() OVER(ORDER BY n.Id DESC) AS RowNumber 
FROM PlayList n 
left join ZoneVideo zv on zv.Id=n.ZoneId 
where (@DateFrom is null and (1=1)) or (@DateFrom is not null and (CONVERT(datetime,n.CreatedDate) >= CONVERT(datetime,@DateFrom) AND CONVERT(datetime,n.CreatedDate)<=CONVERT(datetime,@DateTo)))
) AS temp
WHERE RowNumber > @LowerBand AND RowNumber <= @UpperBand
END

