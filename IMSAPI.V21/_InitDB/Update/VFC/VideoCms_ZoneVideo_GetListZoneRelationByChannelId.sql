USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_GetListRelationByPlayListId]    Script Date: 01/17/2018 13:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListZoneRelationByChannelId]
	@ChannelId int = 0
AS
	IF @ChannelId > 0 AND EXISTS(SELECT Id FROM VideoChannel WHERE Id = @ChannelId)
	BEGIN
		SELECT Z.*
		FROM ZoneVideo AS Z INNER JOIN VideoChannelInZone AS ZV ON Z.Id = ZV.ZoneId
		WHERE ZV.ChannelId = @ChannelId
		ORDER BY Z.Id
	END
















