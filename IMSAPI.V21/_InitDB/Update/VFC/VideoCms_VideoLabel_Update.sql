USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_UpdateV2]    Script Date: 12/28/2017 13:46:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update Playlist>
CREATE PROCEDURE  [dbo].[VideoCms_VideoLabel_Update]
	@Id int,
	@ParentId int,
	@Name nvarchar(200),
	@Url nvarchar(50),
	@Avatar nvarchar(500) = '',
	@Priority int,
	@IsSystem int,
	@CreatedDate datetime,	
	@CreatedBy varchar(50) = '',
	@ModifiedDate datetime
AS
	BEGIN TRANSACTION
	
	UPDATE VideoLabel
	SET ParentId = @ParentId, 
		Name = @Name, 
		Url = @Url, 
		Avatar=@Avatar,
		Priority = @Priority, 		
		IsSystem = @IsSystem, 
		ModifiedDate = @ModifiedDate		
	WHERE (Id = @Id)	

	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION















