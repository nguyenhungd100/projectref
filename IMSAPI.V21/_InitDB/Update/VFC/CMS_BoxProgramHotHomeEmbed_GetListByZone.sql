USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxVideoEmbed_GetListByZone]    Script Date: 12/29/2017 21:22:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
CREATE PROCEDURE [dbo].[CMS_BoxProgramHotHomeEmbed_GetListByZone]
@ZoneId int,
@TypeId int
AS
	BEGIN
		SELECT * FROM VideoPosition
		where TypeId=@TypeId and ZoneId=@ZoneId
		ORDER BY [Order] ASC
	END





