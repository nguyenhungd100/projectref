USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_Send]    Script Date: 01/10/2018 10:01:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoCms_PlayList_Send]
    @Id INT = 0 ,
    @UserDoAction NVARCHAR(200)
AS 
    BEGIN TRANSACTION
	
    DECLARE @Status INT
    SELECT  @Status = Status
    FROM    PlayList
    WHERE   Id = @Id
	
    IF @Id <= 0
        OR @Status IS NULL
        OR @Status < 0 
        BEGIN
            RAISERROR ('Can not found this PlayList.', 16, 1) ;
            ROLLBACK TRANSACTION
        END
    BEGIN
        IF (@Status = 0) -- Temporary -> Wait for edit
            UPDATE  PlayList
            SET     Status = 3
            WHERE   Id = @Id
        ELSE 
            IF ( @Status = 4
               ) -- 4 = Return -> Wait for publish
                UPDATE  PlayList
                SET     Status = 3 ,
                        EditedBy = @UserDoAction
                WHERE   Id = @Id
    END
	
    IF @@ERROR <> 0 
        ROLLBACK TRANSACTION
    ELSE 
        COMMIT TRANSACTION