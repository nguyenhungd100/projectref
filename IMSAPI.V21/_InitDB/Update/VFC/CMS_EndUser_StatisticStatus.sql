USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_StatisticStatus]    Script Date: 01/08/2018 10:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
CREATE PROCEDURE [dbo].[CMS_EndUser_StatisticStatus] --1
@Status int ,
@FromDate datetime = null,
@ToDate datetime = null
AS    
	SELECT	
		CONVERT (NVARCHAR(20), EndUser.CreatedDate,103) as ValuesDate,
		COUNT (*) as ValuesData,
		EndUser.[Status]
	FROM EndUser
	WHERE	
		CreatedDate >=@FromDate AND CreatedDate <=@ToDate
		AND	EndUser.[Status] = @Status			
	GROUP BY  CONVERT (NVARCHAR(20), EndUser.CreatedDate,103),EndUser.[Status] ORDER BY ValuesDate ASC

	
	
	
	
	
	
	
	
	

















