USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxVideoEmbed_Insert]    Script Date: 12/30/2017 10:26:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
CREATE PROCEDURE [dbo].[CMS_BoxProgramHotHomeEmbed_Insert]
	@Name nvarchar(250)='',
	@ZoneId int=0,
	@Type int=0,
	@VideoId bigint,	
	@Order int=0,	
	@Title nvarchar(250) ='',
	@Avatar nvarchar(500) ='',
	@LastModifiedDate datetime,
	@Url nvarchar(500) ='',
	@TypeId int=8
AS
	BEGIN TRANSACTION CMS_BoxProgramHotEmbed_Insert
	
	BEGIN TRY
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM VideoPosition WHERE VideoId = @VideoId and TypeId=@TypeId and ZoneId=@ZoneId)
			BEGIN
				INSERT INTO VideoPosition (					
					Name,
					ZoneId,
					[Type],
					VideoId,
					[Order], 
					Title,
					Avatar,
					LastModifiedDate,
					Url,
					TypeId
					)
				VALUES(		
					@Name,
					@ZoneId,			
					@Type,
					@VideoId,
					@Order,
					@Title,
					@Avatar,
					@LastModifiedDate,
					@Url,
					@TypeId
				)
				COMMIT TRANSACTION CMS_BoxProgramHotEmbed_Insert
			END
			ELSE BEGIN
				RAISERROR ('This existed in BoxProgramHotEmbed.', 16, 1)
				ROLLBACK TRANSACTION CMS_BoxProgramHotEmbed_Insert
			END
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_BoxProgramHotEmbed_Insert
	END CATCH