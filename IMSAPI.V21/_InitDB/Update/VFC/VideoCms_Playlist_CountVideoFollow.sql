USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_GetPlaylistCounter]    Script Date: 01/15/2018 14:15:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
--15/01/2018
CREATE PROCEDURE  [dbo].[VideoCms_Playlist_CountVideoFollow] --'90,93'
@PlayListIds varchar(max)
AS
BEGIN
	DECLARE @search nvarchar(max)
	
	SET @search ='SELECT PlayListId,SUM(VideoCount)AS VideoCount,SUM(FollowCount) AS  FollowCount FROM 
        (
			   SELECT PlayListId, Count(*) as VideoCount, 0 as FollowCount					  
			   FROM VideoPlayList			   
			   WHERE PlayListId IN ('+@PlayListIds+')
			   GROUP BY PlayListId			 
		
	      UNION ALL
	      
		       SELECT PlayListId, 0 as VideoCount, Count(*) as FollowCount					  
			   FROM PlayListFollow 			  
			   WHERE PlayListId IN ('+@PlayListIds+')						
			   GROUP BY PlayListId
			  
         ) AS tmp GROUP BY tmp.PlayListId';
    EXEC SP_EXECUTESQL  @search
END












