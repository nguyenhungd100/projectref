USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_MoveTrash]    Script Date: 01/25/2018 16:07:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_MoveTrash]
	@Id int = 0,
	@UserName varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Playlist WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Playlist.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN
		UPDATE Playlist SET [Status] = 5,LastModifiedBy=@UserName,LastModifiedDate=Getdate()
		WHERE Id = @Id
		
		--xoa playlistid trong videopsition
		--Delete VideoPosition where VideoId=@Id and [Type]=1
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION













