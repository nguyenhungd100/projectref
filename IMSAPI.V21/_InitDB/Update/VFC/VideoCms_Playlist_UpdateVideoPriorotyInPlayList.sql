USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_ChangeMode]    Script Date: 01/19/2018 15:38:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<>
CREATE PROCEDURE  [dbo].[VideoCms_Playlist_UpdateVideoPriorotyInPlayList]
	@PlayListId int,
	@VideoId int,
	@Priority int
AS
	BEGIN TRANSACTION
	
	IF NOT EXISTS(SELECT Id FROM PlayList WHERE Id = @PlayListId)
	BEGIN
		RAISERROR ('This video does not exist.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN
		UPDATE VideoPlayList SET Priority = @Priority 
		WHERE PlayListId = @PlayListId and VideoId=@VideoId
	END
	
	IF @@ERROR <> 0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
















