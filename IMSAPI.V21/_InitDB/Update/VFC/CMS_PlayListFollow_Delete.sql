USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_Insert]    Script Date: 01/03/2018 13:48:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Insert EndUser>
-- =============================================
CREATE PROCEDURE  [dbo].[CMS_PlayListFollow_Delete]
	@PlayListId int,
	@UserId int
AS
	BEGIN TRANSACTION

    BEGIN		
		DELETE FROM PlayListFollow where PlayListId=@PlayListId and UserId=@UserId		
    END
    
IF @@ERROR <> 0
BEGIN
	DECLARE @Message nvarchar(500);
	SET @Message = ERROR_MESSAGE();
	RAISERROR (@Message, 16, 1)
	ROLLBACK TRANSACTION
END ELSE
	COMMIT TRANSACTION


