USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_DeleteById]    Script Date: 01/25/2018 16:22:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Video_DeleteById]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Video WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this video.', 16, 1);
		ROLLBACK TRANSACTION
	END ELSE
	IF EXISTS(SELECT Id FROM Video WHERE Id = @Id AND [Status] = 1)
	BEGIN
		RAISERROR ('Ban khong co quyen xoa video da published.', 16, 1);
		ROLLBACK TRANSACTION
	END ELSE
	BEGIN
		DELETE FROM Video
		WHERE Id = @Id AND [Status] <> 1
		
		--xoa playlistid trong videopsition
		--Delete VideoPosition where VideoId=@Id and [Type]=2
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION
















