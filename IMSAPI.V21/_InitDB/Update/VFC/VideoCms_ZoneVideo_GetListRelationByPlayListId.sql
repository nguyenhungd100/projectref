USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_GetListRelationVideo]    Script Date: 01/13/2018 12:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_ZoneVideo_GetListRelationByPlayListId]
	@PlayListId int = 0
AS
	IF @PlayListId > 0 AND EXISTS(SELECT Id FROM PlayList WHERE Id = @PlayListId)
	BEGIN
		SELECT Z.*
		FROM ZoneVideo AS Z INNER JOIN PlayListInZone AS ZV ON Z.Id = ZV.ZoneId
		WHERE ZV.PlayListId = @PlayListId
		ORDER BY Z.Id
	END
















