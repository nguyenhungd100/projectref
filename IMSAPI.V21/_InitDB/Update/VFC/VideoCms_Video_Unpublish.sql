USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_Unpublish]    Script Date: 01/25/2018 15:47:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Video_Unpublish]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Video WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this video.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN
		UPDATE Video SET [Status] = 2
		WHERE Id = @Id
		--xoa videoid trong videopsition
		Delete VideoPosition where VideoId=@Id and [Type]=2
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION













