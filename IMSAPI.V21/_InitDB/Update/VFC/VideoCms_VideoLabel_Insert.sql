USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_Insert]    Script Date: 12/28/2017 13:28:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update Playlist>
CREATE PROCEDURE  [dbo].[VideoCms_VideoLabel_Insert]
	@ParentId int,
	@Name nvarchar(200),
	@Url nvarchar(50),
	@Avatar nvarchar(500) = '',
	@Priority int,
	@IsSystem int,
	@CreatedDate datetime,	
	@CreatedBy varchar(50) = '',
	@ModifiedDate datetime,
	@Id int output
AS
	BEGIN TRANSACTION
	
	INSERT INTO VideoLabel(ParentId, Name, Url, Avatar, Priority, IsSystem, CreatedDate, CreatedBy)
	VALUES(@ParentId, @Name, @Url, @Avatar, @Priority, @IsSystem, @CreatedDate, @CreatedBy)
	
	SET @Id = SCOPE_IDENTITY()
		
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION