--chinhnb update lại vị trí video trong tất cả playlist

DECLARE @TABLE_VIDEO_P TABLE (Id int identity(1,1), PlayListId int);	
		INSERT INTO @TABLE_VIDEO_P (PlayListId)
			select Id from playlist

DECLARE @PlayListId int,
	@IndexP int;
	
set @IndexP=0;
while(@IndexP<=(select count(*) from @TABLE_VIDEO_P))
begin	
	SET @PlayListId = (SELECT PlayListId FROM @TABLE_VIDEO_P WHERE Id = @IndexP);	
	begin		
		DECLARE @TABLE_VIDEO TABLE (Id int, VideoId int);	
		INSERT INTO @TABLE_VIDEO (Id, VideoId)
			select ROW_NUMBER()OVER(ORDER BY createddate) AS Id,p.videoid from videoplaylist p
			left join video v on v.id=p.videoid
			where playlistid=@PlayListId
			order by createddate asc	
			
		DECLARE @VideoId int,
		@Index int;
		
		set @Index=0;
		while(@Index<=(select count(*) from @TABLE_VIDEO))
		begin
			SET @VideoId = (SELECT VideoId FROM @TABLE_VIDEO WHERE Id = @Index);
				begin
				update videoplaylist set priority=@Index
				where playlistid=@PlayListId and VideoId=@VideoId
				end
			SET @Index = @Index + 1;
		end
		DELETE FROM @TABLE_VIDEO
	end
	SET @IndexP = @IndexP + 1;
End

---video

select p.playlistid, p.videoid,p.priority,v.Name from videoplaylist p
		left join video v on v.id=p.videoid
		where playlistid=116
		order by priority asc