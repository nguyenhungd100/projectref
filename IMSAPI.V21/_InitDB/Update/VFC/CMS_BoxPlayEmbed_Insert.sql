USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_BoxPlayEmbed_Insert]    Script Date: 01/17/2018 14:13:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CMS_BoxPlayEmbed_Insert]
	@Name nvarchar(250),
	@ZoneId int= 0,
	@Type int=0,
	@VideoId int=0,	
	@Order int=0,	
	@Title nvarchar(250) ='',
	@Avatar nvarchar(500) ='',
	@LastModifiedDate datetime,
	@TypeId int=9,
	@DisplayStyle int=1,
	@Priority int=1
AS
	BEGIN TRANSACTION CMS_BoxPlayEmbed_Insert
	
	BEGIN TRY
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM VideoPosition WHERE VideoId = @VideoId and ZoneId =@ZoneId and TypeId=@TypeId)
			BEGIN
				INSERT INTO VideoPosition (
					Name,
					ZoneId,
					[Type],
					VideoId,
					[Order], 
					Title,
					Avatar,
					LastModifiedDate,
					TypeId,
					DisplayStyle,
					Priority
					)
				VALUES(
					@Name,					
					@ZoneId,
					@Type,
					@VideoId,
					@Order,
					@Title,
					@Avatar,
					@LastModifiedDate,
					@TypeId,
					@DisplayStyle,
					@Priority
				)
				COMMIT TRANSACTION CMS_BoxVideoEmbed_Insert
			END
			ELSE BEGIN
				RAISERROR ('This existed in BoxPlayEmbed.', 16, 1)
				ROLLBACK TRANSACTION CMS_BoxVideoEmbed_Insert
			END
		END
	END TRY
	BEGIN CATCH
		DECLARE @Message nvarchar(255);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1);
		ROLLBACK TRANSACTION CMS_BoxPlayEmbed_Insert
	END CATCH






















