USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_AddVideoList]    Script Date: 01/19/2018 10:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_Playlist_AddVideoList] --'106','46;90',10
	@AddVideoIdList varchar(max) = '',
	@DeleteVideoIdList varchar(max) = '',
	@PlaylistId int = 0	
AS
	BEGIN TRANSACTION
		
	BEGIN	
		DECLARE @TABLE_VIDEO TABLE (Id int identity(1,1), VideoId int);
		DECLARE @TABLE_VIDEO_DEL TABLE (Id int identity(1,1), VideoId int);
				
		DECLARE @VideoId int,
				@Index int,
				@MaxPriority int;
				
		INSERT INTO @TABLE_VIDEO (VideoId)
		SELECT CONVERT(int, Part) FROM SplitString(@AddVideoIdList, ';')
		
		INSERT INTO @TABLE_VIDEO_DEL (VideoId)
		SELECT CONVERT(int, Part) FROM SplitString(@DeleteVideoIdList, ';')
						
		SET @Index = 0;
		WHILE (@Index <= (SELECT COUNT(*) FROM @TABLE_VIDEO_DEL))
		BEGIN
			SET @VideoId = (SELECT VideoId FROM @TABLE_VIDEO_DEL WHERE Id = @Index);			
			
			BEGIN				
				DELETE VideoPlayList where VideoId = @VideoId AND PlaylistId = @PlaylistId
				UPDATE PlayList SET VideoCount = VideoCount - 1
				WHERE Id = @PlaylistId
			END
			SET @Index = @Index + 1;
		END
		
		SET @Index = 0;
		select @MaxPriority = Max(Priority) from VideoPlayList WHERE PlayListId = @PlaylistId
		if(@MaxPriority is null)
			set @MaxPriority=0
			
		WHILE (@Index <= (SELECT COUNT(*) FROM @TABLE_VIDEO))
		BEGIN
			SET @VideoId = (SELECT VideoId FROM @TABLE_VIDEO WHERE Id = @Index);			
			-- Check exists video
			IF (@VideoId IS NOT NULL 
				AND NOT EXISTS (SELECT VideoId 
								FROM VideoPlayList 
								WHERE VideoId = @VideoId AND PlaylistId = @PlaylistId)
			)
			BEGIN				
				INSERT INTO VideoPlayList (
					PlaylistId, 
					VideoId, 
					Priority
					)
				VALUES (
					@PlaylistId,
					@VideoId,
					@Index+@MaxPriority
				)
				UPDATE PlayList SET VideoCount = VideoCount + 1
				WHERE Id = @PlaylistId
			END
			SET @Index = @Index + 1;
		END
	END
	
	IF @@ERROR <> 0
	BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END
	ELSE
		COMMIT TRANSACTION
















