USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_GetById]    Script Date: 01/10/2018 11:26:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_GetById]
	@PlaylistID int
AS
	SELECT Id, ZoneId, Name, UnsignName, Description, Status, Mode, Avatar, Priority, DistributionDate, 
		CreatedDate, CreatedBy, LastModifiedDate, LastModifiedBy, PublishedDate, PublishedBy, 
        LastInsertVideoDate, VideoCount, FollowCount, Url, Cover, IntroClip,EditedDate, EditedBy,PlaylistRelation,
        MetaJson,MetaAvatar
	FROM PlayList
	WHERE (Id = @PlaylistID)














