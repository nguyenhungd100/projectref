USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_Publish]    Script Date: 01/18/2018 16:50:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_Publish]
	@Id int = 0,
	@UserDoAction varchar(50)
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Playlist WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Playlist.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE IF NOT EXISTS(SELECT v.Id FROM Video as v inner join VideoPlayList as vp on vp.VideoId=v.Id WHERE vp.PlayListId = @Id and v.[Status]=1)
	BEGIN
		RAISERROR ('Can not publish playlist. Error:17007', 16, 1);
		ROLLBACK TRANSACTION
	END
	ELSE
	BEGIN
		DECLARE @DistributionDate datetime
		
		SET @DistributionDate = ISNULL((SELECT DistributionDate FROM Playlist WHERE Id = @Id), GETDATE())
	
		UPDATE Playlist 
		SET [Status] = 1,
			LastModifiedDate = GETDATE(),
			LastModifiedBy = @UserDoAction,
			PublishedDate = GETDATE(),
			PublishedBy = @UserDoAction,
			DistributionDate = @DistributionDate
		WHERE Id = @Id
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION













