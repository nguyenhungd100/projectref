USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_ZoneVideo_GetListByParentId]    Script Date: 12/28/2017 14:41:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update label>
CREATE PROCEDURE  [dbo].[VideoCms_VideoLabel_GetListByParentId]
	@ParentId int = -1,
	@IsSystem int = -1
AS
	BEGIN TRANSACTION

	IF(@ParentId < 0 AND @IsSystem < 0)
		BEGIN
			SELECT * FROM VideoLabel
		END 
	ELSE
		BEGIN
			SELECT * FROM VideoLabel
			WHERE (@ParentId < 0 OR (@ParentId >= 0 AND ParentId = @ParentId)) AND
					(@IsSystem < 0 OR (@IsSystem >= 0 AND IsSystem = @IsSystem))
			ORDER BY Priority
		END
	
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION

















