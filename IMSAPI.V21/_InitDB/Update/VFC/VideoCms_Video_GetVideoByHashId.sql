USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_GetById]    Script Date: 01/26/2018 17:06:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_Video_GetVideoByHashId]
	@HashId varchar(50)
AS
	IF @HashId != ''
	BEGIN
		SELECT *,
            (SELECT     Name
			FROM          ZoneVideo
			WHERE      (Id = Video.ZoneId)) AS ZoneName
	FROM Video
	WHERE (HashId = @HashId)
	END


