USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_Insert]    Script Date: 01/03/2018 13:48:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Insert EndUser>
-- =============================================
CREATE PROCEDURE  [dbo].[CMS_VideoLabelFollow_Delete]
	@LabelId int,
	@UserId int
AS
	
BEGIN
	BEGIN TRANSACTION
	
	BEGIN TRY
		DELETE FROM VideoLabelFollow where LabelId=@LabelId and UserId=@UserId
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage nvarchar(MAX)
		SET @ErrorMessage = ERROR_MESSAGE()
		RAISERROR (@ErrorMessage, 16, 1);
		ROLLBACK TRANSACTION
	END CATCH
END

