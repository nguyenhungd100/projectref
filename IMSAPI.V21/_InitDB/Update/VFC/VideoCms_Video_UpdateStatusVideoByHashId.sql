USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_UpdateMode]    Script Date: 01/26/2018 16:34:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
CREATE PROCEDURE  [dbo].[VideoCms_Video_UpdateStatusVideoByHashId]	--'a2d6fd60-fa6c-11e7-8f42-dd8df248029e','admin'	
    @HashId varchar(50),
    @KeyVideo varchar(100),
    @UserDoAction varchar(50)
AS	
    IF (@HashId = '' OR NOT EXISTS (SELECT HashId 
					FROM Video 
					WHERE	HashId	= @HashId))
		BEGIN
			RAISERROR ('Can not found this video by HashId: ERROR#0984639770', 16, 1);			
		END
    ELSE
		BEGIN
			
			UPDATE Video
			SET				
				[Status] = 6,
				KeyVideo=@KeyVideo,
				LastModifiedBy = @UserDoAction,
				LastModifiedDate = GETDATE()
			WHERE HashId = @HashId			    
END













