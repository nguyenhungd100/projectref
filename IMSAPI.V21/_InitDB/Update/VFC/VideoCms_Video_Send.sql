USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_Send]    Script Date: 01/30/2018 11:24:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[VideoCms_Video_Send]
    @Id INT = 0 ,
    @UserDoAction NVARCHAR(200)
AS 
    BEGIN TRANSACTION
	
    DECLARE @Status INT
    SELECT  @Status = Status
    FROM    Video
    WHERE   Id = @Id
	
    IF @Id <= 0
        OR @Status IS NULL
        OR @Status < 0 
        BEGIN
            RAISERROR ('Can not found this video.', 16, 1) ;
            ROLLBACK TRANSACTION
        END
    BEGIN
        IF (@Status = 0 or @Status = 6) -- Temporary, VloneVideo -> Wait for edit
            UPDATE  Video
            SET     Status = 3,
					EditedBy = @UserDoAction,
					EditedDate=GetDate(),
					LastModifiedBy = @UserDoAction,
					LastModifiedDate=GetDate()
            WHERE   Id = @Id
        ELSE 
            IF ( @Status = 4
               ) -- 4 = Return -> Wait for publish
                UPDATE  Video
                SET     Status = 3 ,
                        EditedBy = @UserDoAction,
						EditedDate=GetDate(),
						LastModifiedBy = @UserDoAction,
						LastModifiedDate=GetDate()
                WHERE   Id = @Id
    END
	
    IF @@ERROR <> 0 
        ROLLBACK TRANSACTION
    ELSE 
        COMMIT TRANSACTION