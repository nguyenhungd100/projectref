USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_EndUser_Insert]    Script Date: 01/07/2018 14:40:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Insert EndUser>
-- =============================================
CREATE PROCEDURE  [dbo].[CMS_EndUser_Insert]
	@Email varchar(200),
	@Password varchar(500),
	@Name nvarchar(100),
	@VietId varchar(100),
	@FacebookId varchar(100),
	@GoogleId varchar(100),
	@Phone varchar(20),
	@Avatar nvarchar(500),	
	@Status tinyint = 0,
	@Sex tinyint = 1,	
	@Birthday datetime,
	@Address nvarchar(500),	
	@CreatedDate datetime,	
	@LastModifiedDate datetime,	
	
    @Id int = 0 OUTPUT
AS
	BEGIN TRANSACTION

    BEGIN		
		INSERT INTO EndUser(
							Email,
							[Password],
							Name,		
							VietId,				
							FacebookId,
							GoogleId,
							Phone,
							Avatar,
							[Status],
							Sex,
							Birthday,
							[Address],
							CreatedDate,
							LastModifiedDate
							)
					VALUES	(
							@Email,
							@Password,
							@Name,	
							@VietId,					
							@FacebookId,
							@GoogleId,
							@Phone,
							@Avatar,
							@Status,
							@Sex,
							@Birthday,
							@Address,
							@CreatedDate,
							@LastModifiedDate
							)
						
		SET @Id = SCOPE_IDENTITY();				
    END
    
IF @@ERROR <> 0
BEGIN
	DECLARE @Message nvarchar(500);
	SET @Message = ERROR_MESSAGE();
	RAISERROR (@Message, 16, 1)
	ROLLBACK TRANSACTION
END ELSE
	COMMIT TRANSACTION


