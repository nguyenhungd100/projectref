USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Video_GetVideoNewByPlayListId]    Script Date: 01/18/2018 15:44:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--chinhnb
--18/01/2018
CREATE PROCEDURE [dbo].[VideoCms_Video_GetVideoNewByPlayListId]
    @PlaylistId INT = 0    
AS     
    SELECT top 1  [ID] ,
            [ZoneId] ,
            [Name] ,
            [Description] ,
            [HtmlCode] ,
            [Avatar] ,
            [KeyVideo] ,
            [PName] ,
            [PublishDate] ,
            [CreatedDate] ,
			[EditedDate] ,
            [Status] ,
            [NewsId] ,
            [DistributionDate] ,
            [Views] ,
            [Mode] ,
            [LastModifiedDate] ,
            [LastModifiedBy] ,
			[CreatedBy],
			[EditedBy],
			[PublishBy],
			[ParentId],
			[Duration],
            [Tags] ,
            VdoPL.Priority ,
            VdoPL.PlayOnTime
  FROM      Video AS Vdo
            INNER JOIN VideoPlaylist AS VdoPL ON Vdo.Id = VdoPL.VideoId
  WHERE     VdoPL.PlaylistId = @PlaylistId and Vdo.[status] = 1
  ORDER BY DistributionDate DESC















