USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_VideoLabel_GetById]    Script Date: 12/28/2017 19:27:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<CHINHNB>
-- Create date: <2017-12-26>
-- Description:	<Update Playlist>
CREATE PROCEDURE  [dbo].[VideoCms_VideoLabel_GetById]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	SELECT * FROM VideoLabel WHERE Id = @Id
	
	IF @@ERROR <> 0
    BEGIN
		DECLARE @Message nvarchar(500);
		SET @Message = ERROR_MESSAGE();
		RAISERROR (@Message, 16, 1)
		ROLLBACK TRANSACTION
	END ELSE
		COMMIT TRANSACTION




