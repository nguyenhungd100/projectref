USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[VideoCms_Playlist_Unpublish]    Script Date: 01/25/2018 15:50:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--chinhnb
ALTER PROCEDURE  [dbo].[VideoCms_Playlist_Unpublish]
	@Id int = 0
AS
	BEGIN TRANSACTION
	
	IF @Id <= 0 OR NOT EXISTS(SELECT Id FROM Playlist WHERE Id = @Id)
	BEGIN
		RAISERROR ('Can not found this Playlist.', 16, 1);
		ROLLBACK TRANSACTION
	END 
	ELSE
	BEGIN
		UPDATE Playlist SET [Status] = 2
		WHERE Id = @Id
		--xoa playlistid trong videopsition
		Delete VideoPosition where VideoId=@Id and [Type]=1
	END
	
	IF @@ERROR <>  0
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION













