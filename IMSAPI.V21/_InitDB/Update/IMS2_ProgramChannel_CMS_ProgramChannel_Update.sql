USE [IMS_VTV_VFC]
GO
/****** Object:  StoredProcedure [dbo].[CMS_ProgramChannel_Update]    Script Date: 1/2/2018 7:01:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[CMS_ProgramChannel_Update]
	@Name nvarchar(200),
	@Avatar nvarchar(500),
	@LastModifiedBy nvarchar(50),
	@Priority int,
	@Status int,
	@Type int,
	@LiveTvThumb nvarchar(200),
	@LiveTvEmbed nvarchar(200),
	@FullName nvarchar(300),
	@Id int out
AS
BEGIN
	UPDATE ProgramChannel
	SET Name = @Name, 
		Avatar = @Avatar, 
		LastModifiedBy = LastModifiedBy, 
		LastModifiedDate = GETDATE(), 
		Priority = @Priority, 
		Status = @Status,
		Type = @Type,
		LiveTvThumb = @LiveTvThumb,
		LiveTvEmbed = @LiveTvEmbed,
		FullName = @FullName
	WHERE (Id = @Id)
END
















