SELECT TOP 1000 
	a.[Id],
	a.[Author],
	a.[CreatedBy],
	a.[PublishedBy],
	b.News_Author,	
	CASE WHEN a.[CreatedBy] LIKE 'channelvn' OR a.[CreatedBy] IS NULL THEN b.News_Author ELSE a.[CreatedBy] END AS Nguoi_Tao
FROM [IMS_VNE].[dbo].[News] a LEFT JOIN [VNEv6].[dbo].[News]  b ON b.News_ID = a.ID
WHERE a.ID IN (
	20171022064314154,20171022095532857,20171022115017201
	)
	--AND a.CreatedBy LIKE 'channelvn'
	--a.CreatedDate BETWEEN '2017-10-01 00:00:00' AND '2017-10-31 23:59:59' AND a.CreatedBy LIKE 'channelvn'
ORDER BY DistributionDate ASC