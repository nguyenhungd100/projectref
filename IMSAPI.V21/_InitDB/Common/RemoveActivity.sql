DECLARE @NumberOfKeepDate int
DECLARE @DiscussionTopicType int
SET @NumberOfKeepDate = 7
SET @DiscussionTopicType = 2
----------
DELETE FROM DiscussionAssignV2
WHERE Id IN (
	SELECT Id
	FROM DiscussionAssignV2 
	WHERE DiscussionTopicId IN
			(SELECT Id
			 FROM DiscussionTopicV2
			 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
		OR DiscussionId IN
			(SELECT Id
			 FROM DiscussionV2
			 WHERE DiscussionTopicId IN
					(SELECT Id
					 FROM DiscussionTopicV2
					 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
				AND (ParentId=0
					OR ParentId IN
							(SELECT Id
							 FROM DiscussionV2
							 WHERE DiscussionTopicId IN
									(SELECT Id
									 FROM DiscussionTopicV2
									 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
								AND ParentId=0))))
----------
DELETE FROM DiscussionAttachV2
WHERE Id IN (
	SELECT Id
	FROM DiscussionAttachV2 
	WHERE DiscussionTopicId IN
			(SELECT Id
			 FROM DiscussionTopicV2
			 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
		OR DiscussionId IN
			(SELECT Id
			 FROM DiscussionV2
			 WHERE DiscussionTopicId IN
					(SELECT Id
					 FROM DiscussionTopicV2
					 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
				AND (ParentId=0
					OR ParentId IN
						(SELECT Id
						 FROM DiscussionV2
						 WHERE DiscussionTopicId IN
								(SELECT Id
								 FROM DiscussionTopicV2
								 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
							AND ParentId=0))))
----------
DELETE FROM DiscussionTopicFollowV2
WHERE Id IN (
	SELECT Id
	FROM DiscussionTopicFollowV2 
	WHERE DiscussionTopicId IN
			(SELECT Id
			 FROM DiscussionTopicV2
			 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
)
----------
DELETE FROM DiscussionV2
WHERE Id IN (
	SELECT Id
	FROM DiscussionV2 
	WHERE DiscussionTopicId IN
			(SELECT Id
			 FROM DiscussionTopicV2
			 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
		AND (ParentId=0
			OR ParentId IN
				(SELECT Id
				 FROM DiscussionV2
				 WHERE DiscussionTopicId IN
						(SELECT Id
						 FROM DiscussionTopicV2
						 WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate AND Type = @DiscussionTopicType)
					AND ParentId=0)))
----------
DELETE FROM DiscussionTopicV2
WHERE Id IN (
	SELECT Id
	FROM DiscussionTopicV2
	WHERE DATEDIFF(DAY, CreatedDate, GETDATE()) > @NumberOfKeepDate
		AND Type = @DiscussionTopicType)
----------
