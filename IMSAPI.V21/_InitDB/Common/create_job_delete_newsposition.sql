USE [msdb]
GO
/*
	REPLACE {DATABASE_NAME} TO DATABASE. Ex: SohaNews
	REPLACE {JOB_NAME} TO JOB NAME. Ex: IMS_ClearNewsPosition_SohaNews
	REPLACE {SKIP_MAX_RECORD} TO MAX RECORD TO SKIP. Ex: 65
	REPLACE {SERVER_NAME} TO SERVER NAME. Ex: 2471-VDCSVR
*/
/****** Object:  Job [{JOB_NAME}]    Script Date: 06/07/2014 10:19:16 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 06/07/2014 10:19:17 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'{JOB_NAME}', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'{SERVER_NAME}\thanhtn', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [ClearNewsPositionData]    Script Date: 06/07/2014 10:19:18 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'ClearNewsPositionData', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'DELETE FROM NewsPosition WHERE Id IN (
	SELECT Id FROM
	(
		SELECT np.Id, np.DistributionDate, --np.TypeId, np.ZoneId, np.NewsId, np.Title, 
			ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId ORDER BY np.DistributionDate DESC) AS RowNum
		FROM NewsPosition AS np INNER JOIN NewsPositionType AS npt ON np.TypeId = npt.Id
		WHERE np.DistributionDate <= GETDATE() AND npt.AllowAutoUpdate = 1
	) AS DATA
	WHERE DistributionDate <= GETDATE() AND RowNum > 65 -- Max record in each type and zone
	
	UNION

	SELECT Id FROM
	(
		SELECT np.Id, np.DistributionDate, ISNULL(np.ScheduleDate, np.DistributionDate) AS ScheduleDate, --np.TypeId, np.ZoneId, np.NewsId, np.Title, 
			ROW_NUMBER() OVER (PARTITION BY np.TypeId, np.ZoneId, np.[Order] ORDER BY ISNULL(np.ScheduleDate, np.DistributionDate) DESC) AS RowNum
		FROM NewsPosition AS np INNER JOIN NewsPositionType AS npt ON np.TypeId = npt.Id
		WHERE ISNULL(np.ScheduleDate, np.DistributionDate) <= GETDATE() AND np.DistributionDate <= GETDATE() AND npt.AllowAutoUpdate = 0
	) AS DATA
	WHERE ScheduleDate <= GETDATE() AND DistributionDate <= GETDATE() AND RowNum > 1 -- Max record in each type and zone
)', 
		@database_name=N'{DATABASE_NAME}', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'ClearNewsPositionDataDaily', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20140513, 
		@active_end_date=99991231, 
		@active_start_time=30000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
