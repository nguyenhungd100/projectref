/*
ims_logview
ImsLog123$%^log
C:\WINDOWS\Microsoft.NET\Framework64\v4.0.30319>aspnet_regsql.exe -S 192.168.2.196 -U ims_logview -P "ImsLog123$%^log" -d GameK_CMS_v3 -ed -t Zone -et
*/

--ALTER TABLE News ADD ViewCount2 int
--GO

-- =============================================
-- Author:		NANIA
-- Create date: 2014-03-12
-- Description:	<Update view new version>
-- =============================================
CREATE PROCEDURE [dbo].[CMS_News_UpdateViewCount_V3]
	@NewsIds nvarchar(max),
	@ViewCountList nvarchar(max)
AS
BEGIN

	DECLARE @Index INT, @TotalRows INT, @Message nvarchar(500)
	DECLARE @TblTemp TABLE (Idx int, ViewCount int, NewsId bigint)
	BEGIN TRY
		INSERT @TblTemp (Idx, ViewCount, NewsId)
		SELECT c.Idx, ViewCount, NewsId 
		FROM
		(SELECT Row_Number() OVER (ORDER BY (SELECT 1)) AS Idx, Part AS NewsId 
				FROM dbo.SplitString (@NewsIds, ',')) AS c
			INNER JOIN 
				(SELECT Row_Number() OVER (ORDER BY (SELECT 1)) AS Idx , Part AS ViewCount 
				FROM dbo.SplitString (@ViewCountList, ',') ) AS d 
		ON d.Idx = c.Idx
		
		SET @TotalRows = (SELECT COUNT(0) FROM @TblTemp)
		SET @Index = 1
		WHILE (@Index <= @TotalRows)
		BEGIN
			Update News 
				--SET ViewCount2 = ISNULL(ViewCount2, 0) + (SELECT ViewCount FROM @TblTemp WHERE Idx = @Index)
				SET ViewCount = ISNULL(ViewCount, 0) + (SELECT ViewCount FROM @TblTemp WHERE Idx = @Index)
			WHERE Id = (SELECT NewsId FROM @TblTemp WHERE Idx = @Index)
			SET @Index = @Index + 1
		END
	END TRY
	BEGIN CATCH
		SET @Message = ERROR_MESSAGE()
		RAISERROR (@Message, 16, 1)
	END CATCH
END

GO

-- =============================================
-- Author:		NANIA
-- Create date: 2014-03-12
-- Description:	<Update view old version>
-- =============================================
CREATE PROCEDURE [dbo].[be_SynLogToCMS]
	@News_ID bigint,
	@ViewCount int = 0
AS
BEGIN
	BEGIN TRY
		UPDATE News 
		SET ViewCount = ISNULL(ViewCount, 0) + @ViewCount
		WHERE Id = @News_ID
	END TRY
	BEGIN CATCH	
		--PRINT 'ERROR'
	END CATCH
END

GO

GRANT SELECT ON SplitString TO ims_logview
GO
GRANT EXECUTE ON CMS_News_UpdateViewCount_V3 TO ims_logview -- New Version
GO
GRANT EXECUTE ON be_SynLogToCMS TO ims_logview -- Old Version
GO
GRANT SELECT ON Zone TO ims_logview
GO
GRANT SELECT ON NewsInZone TO ims_logview
GO
GRANT SELECT, UPDATE ON News TO ims_logview
GO
GRANT SELECT, INSERT, UPDATE ON NewsAnalytic TO ims_logview
GO
GRANT SUBSCRIBE QUERY NOTIFICATIONS TO ims_logview
GO



