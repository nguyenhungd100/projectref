DECLARE @NumberOfKeepDate int
SET @NumberOfKeepDate = 20
----------
DELETE FROM Dantri_CategoryVisit
WHERE DATEDIFF(day, CreatedDate, GETDATE()) > @NumberOfKeepDate