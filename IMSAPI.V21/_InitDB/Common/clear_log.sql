﻿/*
//get size log
SELECT (size * 8.0)/1024.0 AS size_in_mb
     , CASE
  WHEN max_size                                 = -1 
  THEN 9999999                  -- Unlimited growth, so handle this how you want
  ELSE (max_size * 8.0)/1024.0                  END AS max_size_in_mb
  FROM AdventureWorks2008R2.sys.database_files
 WHERE data_space_id                            = 0        

*/


USE AdventureWorks2008R2;
GO
-- Truncate the log by changing the database recovery model to SIMPLE.
ALTER DATABASE AdventureWorks2008R2
SET RECOVERY SIMPLE;
GO
-- Shrink the truncated log file to 1 MB.
DBCC SHRINKFILE (AdventureWorks2008R2_Log, 1);
GO
-- Reset the database recovery model.
ALTER DATABASE AdventureWorks2008R2
SET RECOVERY FULL;
GO