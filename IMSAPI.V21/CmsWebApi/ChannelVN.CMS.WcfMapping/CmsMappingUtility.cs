﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using System.Dynamic;
using System.Net.Mail;
using System.Net.Configuration;

namespace ChannelVN.CMS.WcfMapping
{
    public class CmsMappingUtility
    {
        public static uint Crc32Php(string input)
        {
            var table = new uint[]{
            0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F,
                0xE963A535, 0x9E6495A3, 0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
                0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91, 0x1DB71064, 0x6AB020F2,
                0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
                0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9,
                0xFA0F3D63, 0x8D080DF5, 0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
                0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B, 0x35B5A8FA, 0x42B2986C,
                0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
                0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423,
                0xCFBA9599, 0xB8BDA50F, 0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
                0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D, 0x76DC4190, 0x01DB7106,
                0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
                0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D,
                0x91646C97, 0xE6635C01, 0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
                0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457, 0x65B0D9C6, 0x12B7E950,
                0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
                0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7,
                0xA4D1C46D, 0xD3D6F4FB, 0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
                0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9, 0x5005713C, 0x270241AA,
                0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
                0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81,
                0xB7BD5C3B, 0xC0BA6CAD, 0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
                0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683, 0xE3630B12, 0x94643B84,
                0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
                0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB,
                0x196C3671, 0x6E6B06E7, 0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
                0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5, 0xD6D6A3E8, 0xA1D1937E,
                0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
                0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55,
                0x316E8EEF, 0x4669BE79, 0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
                0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F, 0xC5BA3BBE, 0xB2BD0B28,
                0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
                0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F,
                0x72076785, 0x05005713, 0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
                0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21, 0x86D3D2D4, 0xF1D4E242,
                0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
                0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69,
                0x616BFFD3, 0x166CCF45, 0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
                0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB, 0xAED16A4A, 0xD9D65ADC,
                0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
                0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693,
                0x54DE5729, 0x23D967BF, 0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
                0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
        };

            unchecked
            {
                uint crc = (uint)(((uint)0) ^ (-1));
                var len = input.Length;
                for (var i = 0; i < len; i++)
                {
                    crc = (crc >> 8) ^ table[
                        (crc ^ (byte)input[i]) & 0xFF
                ];
                }
                crc = (uint)(crc ^ (-1));

                if (crc < 0)
                {
                    crc += (uint)4294967296;
                }

                return crc;
            }
        }

        #region DataContract Extension

        public class FacebookShareInfo
        {
            public string url { get; set; }
            public string normalized_url { get; set; }
            public int share_count { get; set; }
            public int like_count { get; set; }
            public int comment_count { get; set; }
            public int total_count { get; set; }
            public int click_count { get; set; }
            public int comments_fbid { get; set; }
            public int commentsbox_count { get; set; }
        }
        public class AdtechViewCountInfo
        {
            public string item_id { get; set; }
            public AdtechViewCountInfoItem total_view { get; set; }
            public AdtechViewCountInfoItem view_current_date { get; set; }
            public AdtechViewCountInfoItem view_current_hourly { get; set; }

        }
        public class AdtechViewCountInfoItem
        {
            public int view_pc { get; set; }
            public int view_mob { get; set; }

        }

        #endregion
        //public static ResponseData CheckAuthorAndSource(string author, string source, int originalId)
        //{
        //    var responseData = new ResponseData() { Success = true };
        //    if (!IsVCChannel())
        //    {
        //        return responseData;
        //    }
        //    if (string.IsNullOrEmpty(author.Trim()))
        //    {
        //        responseData.Success = false;
        //        responseData.Message = "Video chưa nhập tác giả!";
        //    }
        //    if (string.IsNullOrEmpty(source) && originalId == 0)
        //    {
        //        responseData.Success = false;
        //        responseData.Message = "Video chưa chọn kênh hoặc điền nguồn lấy tin!";
        //    }
        //    return responseData;
        //}

        public static bool IsVCChannel()
        {
            var channelName = CmsChannelConfiguration.CurrentChannelNamespace;
            var vcChannel = new List<string>() { "AutoPro", "AFamily", "CafeF", "CafeBiz", "Kenh14", "GameK", "GenK" };
            return vcChannel.Contains(channelName);
        }
        //News In Using
        public static readonly IDictionary<long, string> DictsNewsInUsing = new Dictionary<long, string>();

        public static void EntityConverter<from, to>(from e1, to e2)
        {

        }

        //public static List<AnalyticEntity> GetViewCount(string lstNewsId)
        //{
        //    try
        //    {
        //        var viewCountMode = CmsChannelConfiguration.GetAppSettingInInt32("ViewCountMode");
        //        var lstAnalyticReturn = new List<AnalyticEntity>();
        //        if (viewCountMode == 0)
        //        {
        //            List<long> newsIdList = new List<long>();
        //            if (!string.IsNullOrEmpty(lstNewsId))
        //            {
        //                foreach (string newsId in lstNewsId.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
        //                {
        //                    if (newsId != "")
        //                    {
        //                        var _newsId = long.Parse(newsId);
        //                        if (!newsIdList.Contains(_newsId))
        //                            newsIdList.Add(_newsId);
        //                    }
        //                }
        //            }
        //            //List<NewsAnalyticServices.NewsAnalyticForResponseEntity> lstAnalyticInfo = new List<NewsAnalyticServices.NewsAnalyticForResponseEntity>();

        //            //var task = Task.Run(() => NewsAnalyticServices.GetListViewCountByListNewsId(newsIdList));

        //            //if (task.Wait(TimeSpan.FromSeconds(3)))
        //            //{
        //            //    lstAnalyticInfo = task.Result;                    
        //            //}
        //            var lstAnalyticInfo = NewsAnalyticServices.GetListViewCountByListNewsId(newsIdList);
        //            var lstAnalyticMobileInfo = NewsAnalyticServices.GetListViewCountMobileByListNewsId(newsIdList);
        //            if (lstAnalyticInfo != null)
        //                foreach (var responseEntity in lstAnalyticInfo)
        //                {
        //                    var analytic = new AnalyticEntity()
        //                    {
        //                        ViewCount = responseEntity.ViewCount,
        //                        ViewCountDaily = responseEntity.ViewCountDaily,
        //                        NewsId = responseEntity.NewsId,
        //                    };
        //                    if (lstAnalyticMobileInfo != null)
        //                    {
        //                        var responseMobile = lstAnalyticMobileInfo.FirstOrDefault(x => x.NewsId == responseEntity.NewsId);
        //                        analytic.ViewCountMobile = responseMobile != null ? responseMobile.ViewCount : 0;
        //                        analytic.ViewCountMobileDaily = responseMobile != null ? responseMobile.ViewCountDaily : 0;
        //                    }
        //                    if (analytic.ViewCountMobile < 0)
        //                    {
        //                        analytic.ViewCountMobile = 0;
        //                    }
        //                    lstAnalyticReturn.Add(analytic);
        //                }
        //        }
        //        else
        //        {
        //            var adTechApi = CmsChannelConfiguration.GetAppSetting("AdtechAnalyticApiUrl");
        //            var adTechApiKey = CmsChannelConfiguration.GetAppSetting("AdtechAnalyticApiKey");
        //            string myParamters = "report?news_ids=[" + lstNewsId + "]&cmskey=" + adTechApiKey;
        //            byte[] buffer = Encoding.UTF8.GetBytes(myParamters);
        //            //Logger.WriteLog(Logger.LogType.Trace, "Get adtech viewcount => " + adTechApi + myParamters);
        //            HttpWebRequest request = WebRequest.Create(adTechApi + myParamters) as HttpWebRequest;
        //            request.Method = "POST";
        //            request.ContentType = "application/x-www-form-urlencoded";
        //            request.ContentLength = buffer.Length;
        //            Stream post = request.GetRequestStream();
        //            post.Write(buffer, 0, buffer.Length);
        //            post.Close();
        //            HttpWebResponse response1 = request.GetResponse() as HttpWebResponse;
        //            Stream responsedata = response1.GetResponseStream();
        //            StreamReader responsereader = new StreamReader(responsedata);
        //            string response = responsereader.ReadToEnd();
        //            try
        //            {
        //                //Logger.WriteLog(Logger.LogType.Trace, response);
        //                var data = NewtonJson.Deserialize<List<AdtechViewCountInfo>>(response);
        //                if (data != null)
        //                {
        //                    foreach (var adTechAnalytic in data)
        //                    {
        //                        var analytic = new AnalyticEntity()
        //                        {
        //                            ViewCount = Utility.ConvertToInt(adTechAnalytic.total_view.view_pc),
        //                            ViewCountDaily = Utility.ConvertToInt(adTechAnalytic.view_current_date.view_pc),
        //                            ViewCountMobile = Utility.ConvertToInt(adTechAnalytic.total_view.view_mob),
        //                            ViewCountMobileDaily = Utility.ConvertToInt(adTechAnalytic.view_current_date.view_mob),
        //                            ViewCountMobileHourly = Utility.ConvertToInt(adTechAnalytic.view_current_hourly.view_mob),
        //                            ViewCountHourly = Utility.ConvertToInt(adTechAnalytic.view_current_hourly.view_pc),
        //                            NewsId = Utility.ConvertToString(adTechAnalytic.item_id)
        //                        };
        //                        lstAnalyticReturn.Add(analytic);
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                Logger.WriteLog(Logger.LogType.Error, "CmsMappingUtility.GetViewCount 1=> " + ex.ToString());
        //                return new List<AnalyticEntity>();
        //            }
        //        }
        //        return lstAnalyticReturn;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, "CmsMappingUtility.GetViewCount 2=> " + ex.ToString());
        //        return new List<AnalyticEntity>();
        //    }
        //}
        //public static List<AnalyticEntity> GetMostViewNews(DateTime fromDate, DateTime currentDate, int zoneId, int top)
        //{
        //    var hour = Math.Floor((currentDate - fromDate).TotalHours).ToString();
        //    var adtechApiUrl = CmsChannelConfiguration.GetAppSetting("AdtechAnalyticApiUrl");
        //    var adtechApiKey = CmsChannelConfiguration.GetAppSetting("AdtechAnalyticApiKey");
        //    // yyyy-MM-dd
        //    var paramRequestFormat = "";
        //    var paramRequest = string.Empty;
        //    if (zoneId == 0)
        //    {
        //        paramRequestFormat =
        //        "top?limit={0}&time={1}&cmskey={2}";
        //        paramRequest = string.Format(paramRequestFormat, top, hour, adtechApiKey);
        //    }
        //    else
        //    {
        //        paramRequestFormat =
        //        "top?limit={0}&category={1}&time={2}&cmskey={3}";
        //        var zone = NewsServices.GetZoneById(zoneId);
        //        if (zone != null)
        //        {
        //            if (zone.ParentId <= 0)
        //            {
        //                paramRequest = string.Format(paramRequestFormat, top, "/" + zone.ShortUrl + "/", hour, adtechApiKey);
        //            }
        //            else
        //            {
        //                var parentZone = NewsServices.GetZoneById(zone.ParentId);
        //                if (parentZone != null)
        //                {
        //                    paramRequest = string.Format(paramRequestFormat, top, "/" + parentZone.ShortUrl + "/" + zone.ShortUrl + "/", hour, adtechApiKey);
        //                }
        //            }
        //        }
        //    }
        //    Logger.WriteLog(Logger.LogType.Trace, "Get adtech viewcount => " + adtechApiUrl + paramRequest);

        //    string response =
        //        string.Empty;
        //    try
        //    {
        //        HttpWebRequest request = WebRequest.Create(adtechApiUrl + paramRequest) as HttpWebRequest;
        //        request.Method = "POST";
        //        request.ContentType = "application/x-www-form-urlencoded";
        //        Stream post = request.GetRequestStream();
        //        post.Close();
        //        HttpWebResponse response1 = request.GetResponse() as HttpWebResponse;
        //        Stream responsedata = response1.GetResponseStream();
        //        StreamReader responsereader = new StreamReader(responsedata);

        //        response = responsereader.ReadToEnd();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        //response = "{\"/dien-cao-the-bao-ve-cong-dinh-doc-lap-su-that-hay-la-tin-don-20160617171601527.htm\":24823,\"/che-tao-thanh-cong-mong-robot-buoc-dot-pha-trong-linh-vuc-y-hoc-20160616163924848.htm\":22821,\"/hai-loai-khung-long-ki-la-moi-duoc-phat-hien-nay-dang-lam-dau-dau-cac-nha-khao-co-20160218151819382.htm\":23420,\"/the-face-lo-quan-he-it-biet-cua-hoc-tro-ha-ho-va-vu-khac-tiep-20160202152911096.htm\":27887,\"/an-nguy-mac-do-tam-lo-nhuoc-diem-bat-loi-20150127101105934.htm\":60560,\"/choang-voi-cuoc-song-hien-tai-cua-truong-the-vinh-va-vo-sap-cuoi-sau-le-an-hoi-bi-mat-20141107152930478.htm\":22647,\"/ca-3-mien-deu-da-duoc-bao-ve-boi-ten-lua-phong-khong-cai-tien-20160706155147282.htm\":20946,\"/dang-ngoi-cua-nguoi-yeu-cuong-do-la-bi-che-phan-cam-20160705224928756.htm\":67497,\"/ky-duyen-thoai-mai-vui-dua-mac-ke-du-luan-chi-trich-20160706072649127.htm\":19565,\"/benh-vien-nhi-trung-uong-noi-gi-ve-vu-bao-ve-chan-xe-cuu-thuong-2016070610534313.htm\":19037}";
        //    }
        //    var lstAnalyticReturn = new List<AnalyticEntity>();
        //    try
        //    {
        //        var data = new Dictionary<string, int>();
        //        if (response != string.Empty && response.Trim() != "[]")
        //        {
        //            data = NewtonJson.Deserialize<Dictionary<string, int>>(response);
        //            if (data != null)
        //            {
        //                foreach (var adTechAnalytic in data)
        //                {
        //                    var analytic = new AnalyticEntity()
        //                    {
        //                        ViewCount = adTechAnalytic.Value,
        //                        NewsId = GetNewsId(adTechAnalytic.Key)
        //                    };
        //                    lstAnalyticReturn.Add(analytic);
        //                }
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        //Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //    }
        //    //Logger.WriteLog(Logger.LogType.Info, "lstAnalyticReturn => " + lstAnalyticReturn);
        //    return lstAnalyticReturn;
        //}
        public static dynamic GetFacebookInfo(string lstUrl)
        {
            var facebookInfoApiUrl = CmsChannelConfiguration.GetAppSetting("FacebookUrlInfo");
            string myParamters = lstUrl;
            byte[] buffer = Encoding.UTF8.GetBytes(myParamters);
            HttpWebRequest request = WebRequest.Create(facebookInfoApiUrl + myParamters) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = buffer.Length;
            Stream post = request.GetRequestStream();
            post.Write(buffer, 0, buffer.Length);
            post.Close();
            HttpWebResponse response1 = request.GetResponse() as HttpWebResponse;
            Stream responsedata = response1.GetResponseStream();
            StreamReader responsereader = new StreamReader(responsedata);
            string response = responsereader.ReadToEnd();
            try
            {
                //Logger.WriteLog(Logger.LogType.Trace, response);
                var data = NewtonJson.Deserialize<dynamic>(response);
                return data;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return new ExpandoObject();
            }
        }
        public enum RequestMethod
        {
            Get = 0,
            Post = 1,
            Head = 2,
            Put = 3,
            Delete = 4,
            Trace = 5,
            Connect = 6
        }
        public static string GetWebRequestDataFromUrl(string url, RequestMethod method, int MaximumAutomaticRedirections = 1, bool AllowAutoRedirect = true, bool KeepAlive = false, int ConnectionLimit = 1, int timeOut = 100000, string contentType = "", string userAgent = "", string accept = "", string jsonData = "")
        {
            var responseString = "";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                switch (method)
                {
                    case RequestMethod.Get:
                        request.Method = "GET";
                        break;
                    case RequestMethod.Post:
                        request.Method = "POST";
                        break;
                    case RequestMethod.Head:
                        request.Method = "HEAD";
                        break;
                    case RequestMethod.Put:
                        request.Method = "PUT";
                        break;
                    case RequestMethod.Delete:
                        request.Method = "DELETE";
                        break;
                    case RequestMethod.Trace:
                        request.Method = "TRACE";
                        break;
                    case RequestMethod.Connect:
                        request.Method = "CONNECT";
                        break;
                    default:
                        request.Method = "GET";
                        break;
                }
                request.MaximumAutomaticRedirections = MaximumAutomaticRedirections;
                request.AllowAutoRedirect = AllowAutoRedirect;
                request.Referer = url;
                request.ServicePoint.ConnectionLimit = ConnectionLimit;
                request.Headers.Add("Accept-Encoding", "gzip,deflate");
                if (!string.IsNullOrEmpty(userAgent))
                {
                    request.UserAgent = userAgent;
                }
                else
                {
                    request.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
                }
                //"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
                if (!string.IsNullOrEmpty(accept))
                {
                    request.Accept = accept;
                }
                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                request.Timeout = timeOut;
                if (!string.IsNullOrEmpty(contentType))
                {
                    request.ContentType = contentType;
                }
                if (!string.IsNullOrEmpty(jsonData))
                {
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(jsonData);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                }
                try
                {
                    var response = (HttpWebResponse)request.GetResponse();
                    using (StreamReader r = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = r.ReadToEnd();
                    }
                    response.Close();
                }
                catch (Exception ex)
                {
                    if (!(url.EndsWith(".png") || url.EndsWith(".jpg") || url.EndsWith(".gif") || url.EndsWith(".jpeg")))
                        Logger.WriteLog(Logger.LogType.Error, url + " :1 " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, url + " :2 " + ex.Message);
            }
            return responseString;
        }
        protected static string GetNewsId(string url)
        {
            return Utility.ConvertToLong(url).ToString();
            //var arrUrl = url.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
            //if (arrUrl.Length > 0)
            //{
            //    var arrNewsId = arrUrl[arrUrl.Length - 1].Split(new string[] { "." },
            //                                                    StringSplitOptions.RemoveEmptyEntries);
            //    return arrNewsId[0];
            //}
            //return "0";
        }
        public class SecureKmsVideoTokenEntity
        {

        }
        public class AnalyticEntity
        {
            public string NewsId { set; get; }
            public int ViewCount { set; get; }
            public int ViewCountHourly { set; get; }
            public int ViewCountDaily { set; get; }
            public int ViewCountMobile { set; get; }
            public int ViewCountMobileHourly { set; get; }
            public int ViewCountMobileDaily { set; get; }
        }
        public class StatisticTruyCap
        {
            public double PercentAcc { get; set; }
            public double PercentNoAcc { get; set; }
            public string GrowthTruyCap { get; set; }
            public int AverageTruyCap { get; set; }
            public double PercentWeb { get; set; }
            public double PercentMobile { get; set; }
            public int ActiveID { get; set; }
            public int TotalNow { get; set; }
        }
        public class ViewByTime
        {
            public DateTime Time { get; set; }
            public List<ViewItem> ListView { get; set; }
        }
        public class ViewItem
        {
            public string Id { get; set; }
            public int Count { get; set; }
        }
        //public static bool SendEmail(string FromName, string FromMail, string Emails, string Subject, string Msg, bool bodyHtml = true)
        //{
        //    bool result = false;
        //    try
        //    {
        //        MailMessage mailMessage = new MailMessage();


        //        mailMessage.From = new MailAddress(FromMail, FromName);
        //        mailMessage.IsBodyHtml = bodyHtml;
        //        mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
        //        mailMessage.HeadersEncoding = System.Text.Encoding.UTF8;
        //        mailMessage.SubjectEncoding = System.Text.Encoding.UTF8;

        //        var ccList = Emails.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
        //        int i = 0;
        //        var mail = "";
        //        foreach (var e in ccList)
        //        {
        //            mail = e.Trim();

        //            if (mail == string.Empty)
        //            {
        //                continue;
        //            }
        //            if (i == 0)
        //            {
        //                mailMessage.To.Add(mail);
        //            }
        //            else
        //            {
        //                mailMessage.CC.Add(mail);
        //            }
        //            i++;
        //        }

        //        mailMessage.Subject = Subject;
        //        mailMessage.Body = Msg;

        //        SmtpClient smtpClient = new SmtpClient();
        //        smtpClient.UseDefaultCredentials = false;
        //        smtpClient.Timeout = 1000 * 60;
        //        var section = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
        //        smtpClient.Host = section.Network.Host;
        //        smtpClient.Port = section.Network.Port;
        //        smtpClient.EnableSsl = section.Network.EnableSsl;
        //        smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //        smtpClient.Credentials = new NetworkCredential(section.Network.UserName, section.Network.Password);

        //        smtpClient.Send(mailMessage);
        //        result = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog(Logger.LogType.Error, ex.Message);
        //        result = false;
        //    }
        //    return result;
        //}
    }
}
