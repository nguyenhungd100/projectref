﻿
namespace ChannelVN.CMS.MessageQueue.Constants
{
    public class TopicName
    {
        public const string REDIS = "REDIS";
        public const string ES = "ES";
    }

    public class ActionName
    {
        //News
        public const string ADD_NEWS = "ADD_NEWS";
        public const string UPDATE_NEWS = "UPDATE_NEWS";
        public const string UPDATE_NEWS_STATUS_1 = "UPDATE_NEWS_STATUS_1";//luu tam
        public const string UPDATE_NEWS_STATUS_2 = "UPDATE_NEWS_STATUS_2";//cho bt
        public const string UPDATE_NEWS_STATUS_3 = "UPDATE_NEWS_STATUS_3";//nhan bt
        public const string UPDATE_NEWS_STATUS_4 = "UPDATE_NEWS_STATUS_4";//tra lai pv
        public const string UPDATE_NEWS_STATUS_5 = "UPDATE_NEWS_STATUS_5";//cho xb
        public const string UPDATE_NEWS_STATUS_6 = "UPDATE_NEWS_STATUS_6";//nhan xb
        public const string UPDATE_NEWS_STATUS_7 = "UPDATE_NEWS_STATUS_7";//tra lai btv
        public const string UPDATE_NEWS_STATUS_8 = "UPDATE_NEWS_STATUS_8";//xb bai viet
        public const string UPDATE_NEWS_STATUS_9 = "UPDATE_NEWS_STATUS_9";//xoa bai viet
        public const string UPDATE_NEWS_STATUS_10 = "UPDATE_NEWS_STATUS_10";//go bai viet
        public const string UPDATE_NEWS_STATUS_11 = "UPDATE_NEWS_STATUS_11";//tra ve ctv
        public const string UPDATE_NEWS_STATUS_14 = "UPDATE_NEWS_STATUS_14";//cho tong bt duyet
        public const string UPDATE_NEWS_STATUS_16 = "UPDATE_NEWS_STATUS_16";//Tong bt nhan duyet
        public const string UPDATE_NEWS_STATUS_19 = "UPDATE_NEWS_STATUS_19";//bbt tra lai tk
        public const string UPDATE_NEWS_STATUS_20 = "UPDATE_NEWS_STATUS_20";//tra lai dich danh btv
        public const string UPDATE_NEWS_FORWARD_TO_EDITOR = "UPDATE_NEWS_FORWARD_TO_EDITOR";
        public const string UPDATE_NEWS_FORWARD_TO_SECRETARY = "UPDATE_NEWS_FORWARD_TO_SECRETARY";

        public const string UPDATE_NEWS_ISONHOME = "UPDATE_NEWS_ISONHOME";
        public const string UPDATE_NEWS_ISFOCUS = "UPDATE_NEWS_ISFOCUS";
        public const string UPDATE_NEWS_ONLYTITLE = "UPDATE_NEWS_ONLYTITLE";
        public const string UPDATE_NEWS_DETAIL = "UPDATE_NEWS_DETAIL";
        public const string UPDATE_NEWS_DETAILSIMPLE = "UPDATE_NEWS_DETAILSIMPLE";
        public const string UPDATE_NEWS_CHECKERROR = "UPDATE_NEWS_CHECKERROR";

        //Zone
        public const string ADD_ZONE = "ADD_ZONE";
        public const string UPDATE_ZONE = "UPDATE_ZONE";
        public const string UPDATE_ZONE_INVISIBLED = "UPDATE_ZONE_INVISIBLED";
        public const string UPDATE_ZONE_STATUS = "UPDATE_ZONE_STATUS";
        public const string UPDATE_ZONE_COMMENT = "UPDATE_ZONE_COMMENT";

        //Tag
        public const string ADD_TAG = "ADD_TAG";
        public const string UPDATE_TAG = "UPDATE_TAG";
        public const string DELETE_TAG = "DELETE_TAG";
        public const string UPDATE_TAG_INVISIBLED = "UPDATE_TAG_INVISIBLED";
        public const string UPDATE_TAG_HOT = "UPDATE_TAG_HOT";
    }
}
