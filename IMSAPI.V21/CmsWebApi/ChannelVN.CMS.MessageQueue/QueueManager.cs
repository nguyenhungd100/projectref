﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MessageQueue.ExchangeQueue;
using System;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MessageQueue
{
    public static class QueueManager
    {
        public static Task<T> AddToQueue<T>(string action, string topic, T data, string message = "")
        {
            try
            {
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                var queue = new QueueEntity<T>
                {
                    NameSpace = nameSpace,
                    Action = action,
                    Data = data,
                    CreatedDate = DateTime.Now,
                    Message = message
                };

                ExchangeFactory.Send(new EQMessage<object>
                {
                    Headers = new EQMessageHeaders { Topic = topic },
                    Body = queue
                }, (err, effected) =>
                {
                    //TODO somethings
                    if (err != null)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "queue:" + action + err.Message);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
            }
            return Task.FromResult(data);
        }
    }
    public class QueueEntity<T>
    {
        public string NameSpace { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Action { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
    }
}
