﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.MessageQueue.ExchangeQueue;
using ChannelVN.CMS.MessageQueue.ReserveStream;
using System;

namespace ChannelVN.CMS.MessageQueue
{
    public class ExchangeFactory
    {
        /// <summary>
        /// Check If not exists than to create a global object.
        /// </summary>
        static ExchangeFactory()
        {
            var allowQueueDb = AppSettings.GetBool("AllowCmsQueueDb");
            var connectString = AppSettings.GetString("CmsQueueDb");
            if (allowQueueDb && !string.IsNullOrWhiteSpace(connectString))
            {
                var nameSpace = AppSettings.GetString("CmsApi.Namespace");
                if (_instance == null)
                {
                    _instance = new EQ<object>(new EQOptions
                    {
                        Type = ExchangeType.Direct,
                        Mode = StorageMode.Redis,
                        Engine = StorageEngine.Parse(connectString),
                        Retry = 3,
                        LIMIT_POOL_THREAD = 1,
                        LIMIT_STREAM = 4294967296,
                        NameSpace= nameSpace
                    });

                    _instance.OnError += (sender, e) =>
                    {
                        Logger.WriteLog(Logger.LogType.Error, $"OnError.e => {e.Error.Message}");
                    };
                }
            }
        }

        /// <summary>
        /// The queue object.
        /// </summary>
        private static EQ<object> _instance = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static Stream<object> CreateStream(EQConfiguration config, Action<Element<object>, Action<Exception>> handler)
        {
            try
            {
                if (_instance != null)
                {
                    var stream = _instance.CreateStream(config, handler);

                    stream.OnRaise += (sender, e) =>
                    {
                        Logger.WriteLog(Logger.LogType.Info, $"OnRaise.e => {e.Element.Id}");
                    };
                    stream.OnError += (sender, e) =>
                    {
                        Logger.WriteLog(Logger.LogType.Error, $"OnError.e => {e.Error.Message}");
                    };
                    return stream;
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topic"></param>
        public static Stream<object> GetStream(string topic)
        {
            if (_instance != null)
            {
                return _instance.GetStream(topic);
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="callback"></param>
        public static void Send(EQMessage<object> message, Action<Exception, int> callback = null)
        {
            if (_instance != null)
            {
                _instance.Send(message, callback);
            }
        }
    }
}
