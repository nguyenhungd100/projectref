﻿
namespace ChannelVN.CMS.MessageQueue.ExchangeQueue
{
    public enum ExchangeType
    {
        /// <summary>
        /// Point by word by word style
        /// </summary>
        Direct = 0,
        /// <summary>
        /// Point by Regx style
        /// </summary>
        Topic = 1,
        /// <summary>
        /// Point to all subscribers
        /// </summary>
        Fanout = 2
    }
}
