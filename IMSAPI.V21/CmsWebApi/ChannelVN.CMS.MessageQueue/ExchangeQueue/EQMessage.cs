﻿
namespace ChannelVN.CMS.MessageQueue.ExchangeQueue
{
    public class EQMessage<TPayload>
    {
        public EQMessageHeaders Headers { get; set; }
        public TPayload Body { get; set; }
    }

    public class EQMessageHeaders
    {
        public string Topic { get; set; }
        public string Group { get; set; }
    }
}
