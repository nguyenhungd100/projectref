﻿
namespace ChannelVN.CMS.MessageQueue.ExchangeQueue
{
    public class EQConfiguration
    {
        public string Topic { get; set; }

        //Extra fields
        public int? LIMIT_POOL_THREAD { get; set; }
        public long? LIMIT_STREAM { get; set; }
        public int? Retry { get; set; }
        public bool? Recovery { get; set; }
    }
}
