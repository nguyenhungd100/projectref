﻿using ChannelVN.CMS.MessageQueue.ReserveStream;
using System;
using System.Reflection;

namespace ChannelVN.CMS.MessageQueue.ExchangeQueue
{
    public class EQOptions : StreamOptions, ICloneable
    {
        public ExchangeType Type { get; set; }

        public EQOptions() : base()
        {
            Type = ExchangeType.Direct;
        }

        public virtual object Clone()
        {
            return CloneObject(this);
        }

        #region Clonable

        public static object CloneObject(object objSource)
        {
            //Get the type of source object and create a new instance of that type
            var typeSource = objSource.GetType();
            var objTarget = Activator.CreateInstance(typeSource);

            //Get all the properties of source object type
            var propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            //Assign all source property to taget object 's properties
            foreach (PropertyInfo property in propertyInfo)
            {
                //Check whether property can be written to
                if (property.CanWrite)
                {
                    //Check whether property type is value type, enum or string type
                    if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String)))
                    {
                        property.SetValue(objTarget, property.GetValue(objSource, null), null);
                    }
                    //else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
                    else
                    {
                        var objPropertyValue = property.GetValue(objSource, null);
                        if (objPropertyValue == null)
                        {
                            property.SetValue(objTarget, null, null);
                        }
                        else
                        {
                            property.SetValue(objTarget, CloneObject(objPropertyValue), null);
                        }
                    }
                }
            }
            return objTarget;
        }
        #endregion
    }
}
