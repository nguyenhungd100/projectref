﻿using ChannelVN.CMS.MessageQueue.ReserveStream;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChannelVN.CMS.MessageQueue.ExchangeQueue
{
    public class Exchange<TPayload>
    {
        private Dictionary<string, Stream<TPayload>> _streams = null;
        private ExchangeType _type = ExchangeType.Direct;

        public Exchange(Dictionary<string, Stream<TPayload>> streams, EQOptions options = null)
        {
            _streams = streams;
            _type = options == null ? ExchangeType.Direct : options.Type;
        }

        public List<Stream<TPayload>> GetReserveStream(ExchangeFilter filterCondition)
        {
            List<Stream<TPayload>> matchedStreams = null;
            switch (_type)
            {
                case ExchangeType.Direct:
                    {
                        //var stream = _streams.GetValueOrDefault(filterCondition.Match);
                        var stream = default(Stream<TPayload>);
                        _streams.TryGetValue(filterCondition.Match, out stream);

                        matchedStreams = new List<Stream<TPayload>> { stream };
                        break;
                    }
                case ExchangeType.Fanout:
                    {
                        matchedStreams = new List<Stream<TPayload>>(_streams.Values);
                        break;
                    }
                case ExchangeType.Topic:
                    {
                        matchedStreams = new List<Stream<TPayload>>();

                        var rgx = new Regex(filterCondition.Match, RegexOptions.IgnoreCase);
                        foreach (var key in _streams.Keys)
                        {
                            var matches = rgx.Matches(key);
                            if (matches.Count > 0)
                            {
                                //var stream = _streams.GetValueOrDefault(key);
                                var stream = default(Stream<TPayload>);
                                _streams.TryGetValue(key, out stream);

                                matchedStreams.Add(stream);
                            }
                        }
                        break;
                    }
            }
            return matchedStreams;
        }
    }
}
