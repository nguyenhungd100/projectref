﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MessageQueue.ReserveStream.Databases
{
    public abstract class Store<TContext, TStore> : IStore where TContext : class, IContext, new() where TStore : IStore, new()
    {
        /// <summary>
        /// Đối tượng lệnh kết nối kho lưu
        /// </summary>
        private TContext _context = null;

        /// <summary>
        /// Khởi tạo đối tượng kho lưu
        /// </summary>
        public Store() { Configuration = new ContextOptions(); }

        /// <summary>
        /// Nhận về một đối tượng lệnh để có thể tiến hành khai thác dữ liệu
        /// </summary>
        /// <returns></returns>
        public virtual TContext GetContext()
        {
            if (_context == null)
            {
                _context = Activator.CreateInstance(typeof(TContext), Configuration) as TContext;
            }
            return _context;
        }

        /// <summary>
        /// Cho phép khởi tạo nhanh một đối tượng kho lưu toàn cục.
        /// </summary>
        public static TStore NewStore
        {
            get { return new TStore(); }
        }

        /// <summary>
        /// Đối tượng quản lý cấu hình truy xuất dữ liệu
        /// </summary>
        public ContextOptions Configuration { get; private set; }

        /// <summary>
        /// Ký hiệu quy ước cho tên chuỗi kết nối
        /// </summary>
        public const string STO_CONNECTIONSTRING = "ConnectionString";

        /// <summary>
        /// Ký hiệu quy ước cho tên vùng dữ liệu làm việc
        /// </summary>
        public const string STO_NAMESPACE = "NameSpace";

        /// <summary>
        /// Ký hiệu quy ước cho tên vùng dữ liệu làm việc
        /// </summary>
        public const string STO_SEPARATECHAR = "SeparateChar";
    }
}
