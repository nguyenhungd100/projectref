﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChannelVN.CMS.MessageQueue.ReserveStream.Databases
{
    public interface IContext
    {
        /// <summary>
        /// Đối tượng quản lý tham số chung
        /// </summary>
        ContextOptions Options { get; set; }
    }
    public class ContextOptions : Dictionary<string, object>
    {
        public ContextOptions()
        {
        }

        public object GetValue(string key)
        {
            var value=new object();
            if (TryGetValue(key, out value))
            {
                return value;
            }
            return null;
        }

        //public bool SetValue(string key, object value)
        //{
        //    return TryAdd(key, value);
        //}
    }
}
