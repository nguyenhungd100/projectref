﻿
namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class StreamOptions : StorageOptions
    {
        public long LIMIT_STREAM { get; set; }
        public int LIMIT_POOL_THREAD { get; set; }

        public StorageMode Mode { get; set; }
        public int Retry { get; set; }
        public bool Recovery { get; set; }

        public StreamOptions()
        {
            LIMIT_STREAM = 4096; //4K
            LIMIT_POOL_THREAD = 4; //(4 core)standard

            Mode = StorageMode.Memory;
            Retry = 0;
            Recovery = false;
        }
    }
}
