﻿using System;

namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class Element<TPayload>
    {
        public long Id { get; set; }
        public TPayload Payload { get; set; }
        public int Retry { get; set; }
        public long Pid { get; set; }
        public long Time { get; set; }

        public Element(long id, TPayload payload)
        {
            Id = id;
            Payload = payload;
            Retry = 1;
            Pid = 0;
            Time = (long)DateTimeToUnixTimestamp(DateTime.UtcNow);
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime, long precision = TimeSpan.TicksPerSecond)
        {
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var unixTimestampInTicks = (dateTime.ToUniversalTime() - unixEpoch).Ticks;
            return (double)unixTimestampInTicks / precision;
        }
    }
}
