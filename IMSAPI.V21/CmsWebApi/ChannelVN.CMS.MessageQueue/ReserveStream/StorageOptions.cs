﻿
namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class StorageOptions
    {
        public StorageEngine Engine { get; set; }
        public int ExpiresIn { get; set; }
        internal string Stream { get; set; }
        public string PrefixStream { get; set; }
        public char SeparateChar { get; set; }
        public string NameSpace { get; set; }

        public StorageOptions()
        {
        }
    }
}
