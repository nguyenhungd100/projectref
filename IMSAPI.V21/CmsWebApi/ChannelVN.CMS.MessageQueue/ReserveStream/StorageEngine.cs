﻿
namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class StorageEngine
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public string User { get; set; }
        public string Database { get; set; }

        public StorageEngine()
        {
        }

        public StorageEngine(string host, int port, string password, string user = "", string database = "")
        {
            Host = host;
            Port = port;
            Password = password;
            User = user;
            Database = database;
        }

        public static StorageEngine Parse(string connectionString)
        {
            var returnValue = new StorageEngine();
            try
            {
                var cp = connectionString.Split(',', ';');
                foreach (var item in cp)
                {
                    var kv = item.Split('=');
                    if (kv.Length > 1)
                    {
                        if (kv[0].ToLower().Equals("password"))
                        {
                            returnValue.Password = kv[1];
                        }
                        else if (kv[0].ToLower().Equals("defaultdatabase"))
                        {
                            returnValue.Database = kv[1];
                        }
                    }
                    else if (kv.Length == 1)
                    {
                        var hp = kv[0].Split(':');
                        returnValue.Host = hp[0];
                        returnValue.Port = int.Parse(hp[1]);
                    }
                }
            }
            catch
            {
                returnValue.Host = "127.0.0.1";
                returnValue.Port = 6379;
            }
            return returnValue;
        }
    }
}
