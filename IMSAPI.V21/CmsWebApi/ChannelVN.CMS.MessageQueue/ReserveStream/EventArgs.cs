﻿using System;

namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class ErrorEventArgs : EventArgs
    {
        public Exception Error { get; private set; }

        public ErrorEventArgs(Exception e) : base()
        {
            Error = e;
        }          
    }

    public class RaiseEventArgs<TPayload> : EventArgs
    {
        public Element<TPayload> Element { get; private set; }

        public RaiseEventArgs(Element<TPayload> e) : base()
        {
            Element = e;
        }            
    }

    public class DrainEventArgs : EventArgs
    {
        public DrainEventArgs() : base()
        {
        }
    }

    public class DoneEventArgs : EventArgs
    {
        public DoneEventArgs(/*response*/) : base()
        {
        }
    }

    public class StartEventArgs : EventArgs
    {
        public StartEventArgs() : base()
        {
        }
    }

    public class StopEventArgs : EventArgs
    {
        public StopEventArgs() : base()
        {
        }
    }
}
