﻿
namespace ChannelVN.CMS.MessageQueue.ReserveStream
{
    public class ThreadPoolState
    {
        public bool IsRunnable { get; set; }

        public ThreadPoolState(bool state)
        {
            IsRunnable = state;
        }
    }
}
