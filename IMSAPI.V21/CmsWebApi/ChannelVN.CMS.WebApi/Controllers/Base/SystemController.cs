﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/system")]
    public class SystemController : ApiController
    {        
        [HttpGet, Route("init_data_redis")]
        public HttpResponseMessage InitDataRedis(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var name = (queryStrings.FirstOrDefault(nv => nv.Key == "name")).Value;
                var startDate = DateTime.MinValue;
                var sDate = (queryStrings.FirstOrDefault(nv => nv.Key == "startDate")).Value;
                if (!string.IsNullOrEmpty(sDate))
                {
                    startDate = Utility.ConvertToDateTimeByFormat(sDate, "dd/MM/yyyy HH:mm");
                }
                var endDate = DateTime.MinValue;
                var eDate = (queryStrings.FirstOrDefault(nv => nv.Key == "endDate")).Value;
                if (!string.IsNullOrEmpty(eDate))
                {
                    endDate = Utility.ConvertToDateTimeByFormat(eDate, "dd/MM/yyyy HH:mm");
                }
                var pageSize = Utility.ConvertToInt((queryStrings.FirstOrDefault(nv => nv.Key == "num")).Value);
                if (pageSize == 0)
                {
                    pageSize = 3000;
                }
                var zoneid = 0;

                var zid = (queryStrings.FirstOrDefault(nv => nv.Key == "zoneid")).Value;
                if (!string.IsNullOrEmpty(zid))
                {
                    zoneid = Utility.ConvertToInt(zid);
                }

                var action = (queryStrings.FirstOrDefault(nv => nv.Key == "action")).Value;
                if (string.IsNullOrEmpty(action))
                {
                    action = null;
                }
                var id = (queryStrings.FirstOrDefault(nv => nv.Key == "id")).Value;
                                
                var me = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var startPage = Utility.ConvertToInt((queryStrings.FirstOrDefault(nv => nv.Key == "startPage")).Value, 1);

                var data = new SystemServices().InitDataRedis(id, me, name, startPage, startDate, endDate, pageSize, action, zoneid);
                return this.ToJson<dynamic>(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex));
            }
        }
        
        [HttpGet, Route("init_data_es")]
        public HttpResponseMessage InitDataES(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var name = (queryStrings.FirstOrDefault(nv => nv.Key == "name")).Value;
                var startDate = DateTime.MinValue;
                var sDate = (queryStrings.FirstOrDefault(nv => nv.Key == "startDate")).Value;
                if (!string.IsNullOrEmpty(sDate))
                {
                    startDate = Utility.ConvertToDateTimeByFormat(sDate,"dd/MM/yyyy HH:mm");
                }
                var endDate = DateTime.MinValue;
                var eDate = (queryStrings.FirstOrDefault(nv => nv.Key == "endDate")).Value;
                if (!string.IsNullOrEmpty(eDate))
                {
                    endDate = Utility.ConvertToDateTimeByFormat(eDate, "dd/MM/yyyy HH:mm");
                }
                var pageSize = Utility.ConvertToInt((queryStrings.FirstOrDefault(nv => nv.Key == "num")).Value);
                if (pageSize == 0)
                {
                    pageSize = 3000;
                }

                var zoneid = 0;

                var zid = (queryStrings.FirstOrDefault(nv => nv.Key == "zoneid")).Value;
                if (!string.IsNullOrEmpty(zid))
                {
                    zoneid = Utility.ConvertToInt(zid);
                }

                var action = (queryStrings.FirstOrDefault(nv => nv.Key == "action")).Value;
                if (string.IsNullOrEmpty(action))
                {
                    action = null;
                }
                var me = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var startPage= Utility.ConvertToInt((queryStrings.FirstOrDefault(nv => nv.Key == "startPage")).Value,1);
                var data = new SystemServices().InitDataES(me, name, startPage, startDate, endDate, pageSize, action, zoneid);
                return this.ToJson<dynamic>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("delete_index_redis")]
        public HttpResponseMessage DeleteIndexRedis(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var indexName = (queryStrings.FirstOrDefault(nv => nv.Key == "indexName")).Value;
                
                if (string.IsNullOrEmpty(indexName))
                {
                    return this.ToJson<dynamic>(new { message = "Không tìm thấy index cần xóa." });
                }
                
                var data = new SystemServices().DeleteIndexRedis(indexName);
                return this.ToJson<dynamic>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("delete_index_es")]
        public HttpResponseMessage DeleteIndexEs(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var indexName = (queryStrings.FirstOrDefault(nv => nv.Key == "indexName")).Value;

                if (string.IsNullOrEmpty(indexName))
                {
                    return this.ToJson<dynamic>(new { message = "Không tìm thấy index cần xóa." });
                }

                var data = new SystemServices().DeleteIndexEs(indexName);
                return this.ToJson<dynamic>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("gen_connection_string")]
        public HttpResponseMessage GenConnectionString(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var connect = (queryStrings.FirstOrDefault(nv => nv.Key == "connect")).Value;
                var key = (queryStrings.FirstOrDefault(nv => nv.Key == "key")).Value;
                if (string.IsNullOrEmpty(key))
                {
                    key = "nfvsMof35XnUdQEWuxgAZta";
                }
                var action = (queryStrings.FirstOrDefault(nv => nv.Key == "action")).Value;
                if (string.IsNullOrEmpty(action))
                {
                    action = null;
                }

                if (string.IsNullOrEmpty(connect))
                {
                    return this.ToJson<dynamic>(new {message= "Không tìm thấy index cần xóa." });
                }
                connect = connect.Replace(" ", "+");
                var length1 = connect.Length;
                var data = new SystemServices().GenConnectionString(connect, key, action);
                return this.ToJson<dynamic>(new { Data = data });
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("init_es_and_redis_by_news_id")]
        public HttpResponseMessage InitEsAndRedisByNewsId(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var newsId = 0L;
                long.TryParse((queryStrings.FirstOrDefault(nv => nv.Key == "newsId")).Value,out newsId);                

                var data = new SystemServices().InitEsAndRedisByNewsId(newsId);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("init_es_and_redis_by_news_ids")]
        public HttpResponseMessage InitEsAndRedisByNewsIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                if (body == null)
                {
                    return null;
                }
                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }
                var ids = body.Get("newsIds") ?? string.Empty;
                var data = true;
                if (!string.IsNullOrWhiteSpace(ids)) {
                    var arrIds = ids.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var id in arrIds)
                    {
                        var newsId = 0L;
                        long.TryParse(id, out newsId);
                        data = new SystemServices().InitEsAndRedisByNewsId(newsId);
                    }                    
                }
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        
        [HttpGet, Route("delete_news_by_newsid_redis")]
        public HttpResponseMessage DeleteNewsByNewsIdRedis(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var newsId = (queryStrings.FirstOrDefault(nv => nv.Key == "newsId")).Value;

                if (string.IsNullOrEmpty(newsId))
                {
                    return this.ToJson<dynamic>(new { message = "Không tìm thấy newsId cần xóa." });
                }

                var data = new SystemServices().DeleteNewsByNewsIdRedis(newsId);
                return this.ToJson<dynamic>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("delete_newsids_es_redis")]
        public HttpResponseMessage DeleteNewsIdsEsRedis(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var newsids = (queryStrings.FirstOrDefault(nv => nv.Key == "newsids")).Value;

                if (string.IsNullOrEmpty(newsids))
                {
                    return this.ToJson<dynamic>(new { message = "Không tìm thấy index cần xóa." });
                }

                var data = new SystemServices().DeleteNewsIdsEsRedis(newsids);
                return this.ToJson<dynamic>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("init_from_redis_to_sql")]
        public HttpResponseMessage InitRedisToSql(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var startDate = DateTime.Now;
                var sDate = (queryStrings.FirstOrDefault(nv => nv.Key == "startDate")).Value;
                if (!string.IsNullOrEmpty(sDate))
                {
                    startDate = Utility.ConvertToDateTimeByFormat(sDate, "dd/MM/yyyy HH:mm");
                }
                var endDate = DateTime.Now;
                var eDate = (queryStrings.FirstOrDefault(nv => nv.Key == "endDate")).Value;
                if (!string.IsNullOrEmpty(eDate))
                {
                    endDate = Utility.ConvertToDateTimeByFormat(eDate, "dd/MM/yyyy HH:mm");
                }
                var me = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new SystemServices().InitRedisToSql(me, startDate, endDate);
                return this.ToJson(WcfActionResponse.CreateSuccessResponse("True"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpGet, Route("init_zone_redis_to_sql")]
        public HttpResponseMessage InitZoneRedisToSql(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }
                
                var data = new SystemServices().InitZoneRedisToSql();

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("True"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("init_es_redis_by_listvideoid")]
        public HttpResponseMessage InitEsRedisByListVideoId(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var listVideoId = (queryStrings.FirstOrDefault(nv => nv.Key == "ListVideoId")).Value;
                
                var data = new SystemServices().InitAndRedisByListVideoId(listVideoId);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpGet, Route("init_es_redis_by_listplaylistid")]
        public HttpResponseMessage InitEsRedisByListPlayListId(HttpRequestMessage request)
        {
            try
            {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var pass = (queryStrings.FirstOrDefault(nv => nv.Key == "password")).Value;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var listPlayListId = (queryStrings.FirstOrDefault(nv => nv.Key == "ListPlayListId")).Value;

                var data = new SystemServices().InitEsRedisByListPlayListId(listPlayListId);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("check_news")]
        public HttpResponseMessage CheckNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if(!string.IsNullOrEmpty(accountName) && !accountName.ToLower().Equals("admin")) {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền truy cập hệ thống."));
                }

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                if (fromDate == DateTime.MinValue)
                {
                    fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                }
                var to = string.IsNullOrEmpty(body.Get("to")) ? "" : body.Get("to") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                if (toDate == DateTime.MinValue)
                {
                    toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                }
                var mode = Utility.ConvertToInt(body.Get("mode"),0);
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                
                var totalRows = 0;
                //es
                var data = BoSearch.Base.News.NewsDalFactory.CheckNews(keyword, author, createdBy, zoneIds, "", fromDate, toDate, pageIndex, pageSize, ref totalRows);
                if (mode == 1)
                {
                    //db
                    var dataDb = new NewsServices().CheckNewsByKeyword(keyword, pageIndex, pageSize, ref totalRows);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = totalRows, NewsDB = dataDb }, "Success"));
                }
                else if (mode == 2)
                {
                    //redis
                    var newsId = 0L;
                    long.TryParse(keyword,out newsId);
                    var dataRedis = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = totalRows, NewsRedis = dataRedis }, "Success"));
                }                

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new {Total= totalRows,NewsES = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news")]
        public HttpResponseMessage SearchNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var user = BoCached.Base.Account.UserDalFactory.GetUserCachedByUsername(accountName);

                if (!string.IsNullOrEmpty(accountName) && (accountName.ToLower().Equals("admin")
                    || (user != null && user.User != null && user.User.IsRole == (int)IsRole.EditorialBoard)
                    || (user != null && user.User != null && user.User.IsFullPermission)))
                {
                    var keyword = body.Get("keyword") ?? string.Empty;
                    var author = body.Get("author") ?? string.Empty;
                    var createdBy = body.Get("createdby") ?? string.Empty;                    
                    var zoneId = Utility.ConvertToInt(body.Get("zone"));
                    var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                    var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                    if (fromDate == DateTime.MinValue)
                    {
                        fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                    }
                    var to = string.IsNullOrEmpty(body.Get("to")) ? "" : body.Get("to") + " 23:59:59";
                    var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                    if (toDate == DateTime.MinValue)
                    {
                        toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                    }
                    var pageIndex = Utility.ConvertToInt(body.Get("page"));
                    var pageSize = Utility.ConvertToInt(body.Get("num"));

                    var totalRows = 0;

                    var newsId = 0L;
                    long.TryParse(keyword, out newsId);
                    if (newsId > 0)
                    {
                        //redis                    
                        var dataRedis = BoCached.Base.News.NewsDalFactory.GetNewsById(newsId);
                        return this.ToJson(WcfActionResponse<NewsEntity>.CreateSuccessResponse(dataRedis, "Success"));
                    }
                    //es
                    var listStatusNot = "21,22";
                    var data = BoSearch.Base.News.NewsDalFactory.CheckNews(keyword, author, createdBy, zoneIds, listStatusNot, fromDate, toDate, pageIndex, pageSize, ref totalRows);

                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
                }
                else { 
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền truy cập hệ thống."));
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #region Check queue CD error
        [HttpPost, Route("check_queue_push_cd")]
        public HttpResponseMessage CheckQueuePushCD(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var action = body.Get("action") ?? string.Empty;
                var start = Utility.ConvertToInt(body.Get("start"), 0);
                var num = Utility.ConvertToInt(body.Get("num"),1);

                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var keyId = "queuepushdatacderror";
                var count = 0L;
                if (action == "run")
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueCD(keyId);
                }else if (action == "del")
                {
                    //delete key
                    var res=BoCached.Base.Account.UserDalFactory.DeleteIndexRedis("queuepushcderrorentity:"+keyId);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(res, "Success"));
                }
                else
                {
                    var data = BoCached.CmsExtension.Queue.QueueDalFactory.CheckQueueCD(keyId, start, num, ref count);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = count, QueueObj = data }, "Success"));
                }
                
                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(null, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueCD(string keyId)
        {            
            if (CmsChannelConfiguration.GetAppSettingInInt32("AllowPushDataCD") == 1)
            {                                
                var context = HttpContext.Current;
                var thread = new Thread(() =>
                {
                    int countError = 0;
                    HttpContext.Current = context;
                    while (true)
                    {
                        //Thread.Sleep(3000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueCD(keyId);

                        if (queue != null)
                        {
                            var url = CmsChannelConfiguration.GetAppSetting("UrlPushDataCD");
                            using (WebClient wc = new WebClient())
                            {
                                try
                                {
                                    wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                    var reqparm = new System.Collections.Specialized.NameValueCollection();
                                    reqparm.Add("sessionid", queue.SessionId);
                                    reqparm.Add("objid", queue.JsonKey);
                                    reqparm.Add("action", queue.Action);
                                    reqparm.Add("obj", queue.StrParamValue);
                                    reqparm.Add("channel", queue.ChannelName);
                                    reqparm.Add("distributiondate", queue.DistributionDate);

                                    byte[] responsebytes = wc.UploadValues(url, "POST", reqparm);
                                    string responsebody = Encoding.UTF8.GetString(responsebytes);
                                }
                                catch (Exception ex)
                                {
                                    //push queue redis                                
                                    BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueCD("queuepushdatacderror", queue.SessionId, queue.JsonKey, queue.Action, queue.StrParamValue, queue.ChannelName, queue.DistributionDate, ex.Message);
                                    countError++;                                                                       
                                }
                            }
                        }

                        if (queue == null || countError == 5)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "Run.CountError => countError:" + countError+" => Stop thread");
                            break;
                        }
                    }

                });

                thread.Start();
            }
        }
        #endregion

        #region Check Queue Appbot push news
        [HttpPost, Route("check_queue_push_news")]
        public HttpResponseMessage CheckQueuePushNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var action = body.Get("action") ?? string.Empty;
                var start = Utility.ConvertToInt(body.Get("start"), 0);
                var num = Utility.ConvertToInt(body.Get("num"), 1);

                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var keyId = "appbot_savenews";
                var count = 0L;
                if (action == "run")
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueNews(keyId);
                }
                else
                {
                    var data = BoCached.CmsExtension.Queue.QueueDalFactory.CheckQueue(keyId, start, num, ref count);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = count, QueueObj = data }, "Success"));
                }

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(null, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueNews(string keyId)
        {            
                var context = HttpContext.Current;
                var thread = new Thread(() =>
                {
                    int countError = 0;
                    HttpContext.Current = context;
                    while (true)
                    { 
                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);

                        if (queue != null)
                        {
                            var result = new NewsServices().AppBotSaveNews(queue.Message, queue.Action);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Trace, "CHINHNB Error PopQueue: => " + queue.Message.ParentNewsId.ToString() + NewtonJson.Serialize(queue));                                
                            }
                        }

                        if (queue == null || countError == 5)
                        {
                            Logger.WriteLog(Logger.LogType.Error, "Run.CountError => countError:" + countError + " => Stop thread");
                            break;
                        }
                    }

                });

                thread.Start();            
        }

        [HttpPost, Route("check_queue_push_crawlernews")]
        public HttpResponseMessage CheckQueuePushCrawlerNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var action = body.Get("action") ?? string.Empty;
                var start = Utility.ConvertToInt(body.Get("start"), 0);
                var num = Utility.ConvertToInt(body.Get("num"), 1);

                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var keyId = "crawlernews";
                var count = 0L;
                if (action == "run")
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueCrawlerNews(keyId);
                }
                else
                {
                    var data = BoCached.CmsExtension.Queue.QueueDalFactory.CheckQueue(keyId, start, num, ref count);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = count, QueueObj = data }, "Success"));
                }

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(null, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueCrawlerNews(string keyId)
        {
            var context = HttpContext.Current;
            var thread = new Thread(() =>
            {
                HttpContext.Current = context;

                while (true)
                {
                    Thread.Sleep(1000);

                    var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);

                    if (queue != null)
                    {
                        var news = queue.Message;
                        var zoneId = news.ZoneId;
                        var zoneRelationIdList = string.Empty;
                        var tagIdList = string.Empty;
                        var tagIdListForPrimary = string.Empty;
                        var newsRelationIdList = string.Empty;
                        var authorList = new List<string>();
                        var action = queue.Action;
                        var newsExtensions = new List<NewsExtensionEntity>();

                        //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(queue));

                        var result = new NewsServices().CrawlerNews(news);
                        if (!result.Success)
                        {
                            Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));

                            //var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyId, action, news);
                            //if (res > 0)
                            //{
                            //    //start job =>xử lý pop queue
                            //    TaskJobQueue(keyId);
                            //}
                        }
                    }

                    if (queue == null)
                    {
                        break;
                    }
                }

            });

            thread.Start();
        }
        #endregion

        #region Check Queue Appbot push video
        [HttpPost, Route("check_queue_push_video")]
        public HttpResponseMessage CheckQueuePushVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var action = body.Get("action") ?? string.Empty;
                var start = Utility.ConvertToInt(body.Get("start"), 0);
                var num = Utility.ConvertToInt(body.Get("num"), 1);

                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var keyId = "sharevideo_pushvideo";
                var count = 0L;
                if (action == "run")
                {
                    var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    var AlowShareVideoUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowShareVideoUserNamePublish");
                    if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowShareVideoUserNamePublishNews))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                    }
                    var AlowShareVideoPublish = CmsChannelConfiguration.GetAppSettingInBoolean("AlowShareVideoPublish");
                    //start job =>xử lý pop queue
                    TaskJobQueueVideo(keyId, accountName, AlowShareVideoPublish);
                }
                else
                {
                    var data = BoCached.CmsExtension.Queue.QueueDalFactory.CheckQueue(keyId, start, num, ref count);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = count, QueueObj = data }, "Success"));
                }

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(null, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueVideo(string keyId, string accountName, bool AlowShareVideoPublish)
        {
            var context = HttpContext.Current;
            var thread = new Thread(() =>
            {
                int countError = 0;
                HttpContext.Current = context;
                while (true)
                {
                    var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueShareVideo(keyId);

                    if (queue != null)
                    {
                        var result = new WcfActionResponse();
                        foreach (var payload in queue.PayLoad.Data)
                        {
                            if (payload != null)
                            {
                                payload.Video.Id = 0;
                                payload.Video.Status = (int)EnumVideoStatus.Published;
                                if (!AlowShareVideoPublish)
                                {
                                    payload.Video.Status = (int)EnumVideoStatus.ShareVideo;
                                }
                                payload.Video.Type = (int)EnumVideoType.Default;
                                payload.Video.CreatedBy = accountName;
                                payload.Video.CreatedDate = DateTime.Now;
                            }
                            result = new MediaServices().SaveVideoV4(payload.Video, payload.TagIdList, payload.ZoneIdList, payload.PlaylistIdList, "", new List<string>());
                        }
                        if (!result.Success)
                        {                            
                            BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueShareVideo(keyId + "_log", queue.SessionId, queue.Action, queue.ChannelName, queue.PayLoad);                            
                        }
                    }

                    if (queue == null || countError == 5)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Run.CountError => countError:" + countError + " => Stop thread");
                        break;
                    }
                }

            });

            thread.Start();
        }
        #endregion

        #region Check Queue Appbot push news
        [HttpPost, Route("check_appbot_publish")]
        public HttpResponseMessage CheckAppBotPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var action = body.Get("action") ?? string.Empty;
                var start = Utility.ConvertToInt(body.Get("start"), 0);
                var num = Utility.ConvertToInt(body.Get("num"), 1);

                var pass = body.Get("password") ?? string.Empty;
                if (pass != "chinhnb")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền truy cập api hệ thống này."));
                }

                var keyId = "appbot_publish";
                var count = 0L;
                if (action == "run")
                {
                    //start job =>xử lý pop queue
                    TaskJobAppBotPublish(keyId);
                }
                else
                {
                    var data = BoCached.CmsExtension.Queue.QueueDalFactory.CheckQueue(keyId, start, num, ref count);
                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { Total = count, QueueObj = data }, "Success"));
                }

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(null, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobAppBotPublish(string keyId)
        {
            var context = HttpContext.Current;
            var thread = new Thread(() =>
            {                
                HttpContext.Current = context;
                while (true)
                {
                    var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);
                    
                    if (queue == null)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Run.Stop");
                        break;
                    }
                }

            });

            thread.Start();
        }
        #endregion

        [HttpGet, Route("test_notify_tele_error")]
        public HttpResponseMessage TestNotifyTeleError(HttpRequestMessage request)
        {
            try {
                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return null;
                var message = (queryStrings.FirstOrDefault(nv => nv.Key == "message")).Value;

                Logger.WriteLog(Logger.LogType.Error, message);

                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(message,"Success"));
            }catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
    }
}
