﻿using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BO.Base.NewsSlave;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.MainDal.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{    
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/appbot")]
    public class AppBotController : ApiController
    {
        #region App bot

        [HttpPost, Route("list_zone")]
        public HttpResponseMessage GetAllZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var data = new NewsServices().GetAllZone();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Zones = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_not_ibtemp_by_zone_id")]
        public HttpResponseMessage GetListNewsNotIbtempByZoneId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var page = Utility.ConvertToInt(body.Get("Page"));
                var num = Utility.ConvertToInt(body.Get("Num"));

                var total = 0;
                var data = new NewsServices().GetListNewsNotIbtempByZoneId(zoneId, page, num, ref total);                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total= total, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }        

        [HttpPost, Route("list_topic_by_news_id")]
        public HttpResponseMessage GetListTopicAndTopicParrentByNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                                                
                var data = new TopicServices().GetListTopicAndTopicParrentByNewsId(newsId);

                return this.ToJson(WcfActionResponse<TopicDataAppBotEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_inbound_by_news_id")]
        public HttpResponseMessage SaveInBoundByNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var type = Utility.ConvertToInt(body.Get("NewsExtentionType"));
                var value = body.Get("NewsExtentionValue")??"";

                var listData = body.Get("ListDataInbound") ?? "";
                
                if (string.IsNullOrEmpty(listData))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListData không được để trống."));
                }

                if (newsId > 0)
                {
                    //update newsextention type=33 ->template inbound
                    new NewsServices().SetNewsExtensionValue(newsId, (EnumNewsExtensionType)type, value);
                }
                
                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<BoxInboundComponentEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;
                                var data = new NewsServices().InsertBoxInboundComponentEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new NewsServices().UpdateBoxInboundComponentEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }
                }
                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_topic")]
        public HttpResponseMessage SearchTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 10);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var parentId = Utility.ConvertToInt(body.Get("parentId"), -1);
                var isHotTopic = Utility.ConvertToInt(body.Get("isHotTag"), -1);
                var order = Utility.ConvertToInt(body.Get("orderBy"), 0);
                var isActive = Utility.ConvertToInt(body.Get("isActive"), -1);

                var totalRow = 0;
                var data = new TopicServices().SearchTopic(keyword, zoneId, order, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_inbound_by_news_id")]
        public HttpResponseMessage SearchBoxInboundComponentEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var position = Utility.ConvertToInt(body.Get("Position"), -1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"), -1);

                var data = new NewsServices().GetListBoxInboundComponentEmbed(newsId, position, status, type);

                return this.ToJson(WcfActionResponse<List<BoxInboundComponentEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_not_inbound_by_zone_id")]
        public HttpResponseMessage GetListNewsNotIbboundByZoneId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var page = Utility.ConvertToInt(body.Get("Page"));
                var num = Utility.ConvertToInt(body.Get("Num"));

                var total = 0;
                var data = new NewsServices().GetListNewsNotIbBoundByZoneId(zoneId, page, num, ref total);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = total, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_zone_by_news_id")]
        public HttpResponseMessage GetListZoneByNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));

                var data = new NewsServices().GetZoneByNewsId(newsId);

                return this.ToJson(WcfActionResponse<List<ZoneEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region News

        //fix api cho bot save news zoneid tin tong hop: cho Vccorp
        [HttpPost, Route("save_news")]
        public HttpResponseMessage SaveNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                //fix tạm dừng
                //return this.ToJson(WcfActionResponse.CreateErrorResponse("Api tạm dừng hoạt động."));

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");                
                if (!AlowAppBotUserNamePublishNews.Contains(accountName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }

                var newsid = body.Get("NewsId");
                //Logger.WriteLog(Logger.LogType.Trace, "chinhnb log NewsId befor: " + newsid);
                var parentNewsId = Utility.ConvertToLong(newsid);
                if (parentNewsId<0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param newsid."));
                }

                var title = body.Get("Title")??"";
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không lấy được tiêu đề bài viết."));
                }                

                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                if (zoneId <= 0)
                {
                    zoneId = CmsChannelConfiguration.GetAppSettingInInt32("AppBotCrawlerApiFixZoneId");//89;//fix zone Tin Tong Hop->ThoiDai
                }
                var bodyContent = body.Get("Body");
                bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);
                                
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var distributionDate = Utility.ParseDateTime(body.Get("DistributionDate"), formats);
                if (distributionDate == DateTime.MinValue) {
                    distributionDate = DateTime.Now;
                }                
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInslide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("IsAdstore"));
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));

                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                if (string.IsNullOrEmpty(body.Get("IsOnHome")))
                    isOnHome = true;

                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceUrl");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"));
                var type = Utility.ConvertToInt(body.Get("Type"));
                if (newsType == -1)
                {
                    newsType = (int)NewsType.Normal;
                }
                if (type == 0) type = newsType;
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = data[0];
                        var note = data[1] ?? string.Empty;

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);                

                var news = new NewsEntity
                {
                    Id = 0,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = DateTime.SpecifyKind(distributionDate, DateTimeKind.Unspecified),
                    Sapo = sapo,
                    Title = title,
                    SubTitle = subTitle,
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = initSapo,
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = shortTitle,
                    BonusPrice = bonusPrice,
                    ZoneId = zoneId,
                    Status = (int)NewsStatus.CrawlerByVccorp,
                    ParentNewsId= parentNewsId,
                    ListZoneId= zoneRelationIdList                    
                };

                var newNewsId = NewsBo.SetPrimaryId();
                news.Id = newNewsId;

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");
                var action = body.Get("Action");

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("publish"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                    //chec khi xuat ban
                    if (!string.IsNullOrEmpty(title) && title.Length > 255)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TieuDeBaiVietVuotQua255KyTu]));
                    }
                    
                    if (news.ZoneId < 0)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate!=null && news.DistributionDate<=DateTime.MinValue)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }
                    
                    news.Status = (int)NewsStatus.Published;
                    var FixAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("FixAppBotUserNamePublishNews");
                    if (string.IsNullOrEmpty(FixAppBotUserNamePublishNews))
                    {
                        FixAppBotUserNamePublishNews = accountName;

                    }
                    news.PublishedBy = FixAppBotUserNamePublishNews;
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("unpublish"))
                {                    
                    if(!AlowAppBotCrawlerPublishNews)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                    var keyId = news.ParentNewsId + "_" + news.OriginalId;
                    var existsNewsParent = BoCached.Base.News.NewsDalFactory.GetNewsParentIdAndOriginalId(keyId);
                    if (null != existsNewsParent && existsNewsParent.NewsInfo!=null)
                    {
                        var newsId = existsNewsParent.NewsInfo.Id;
                        var data = new NewsServices().Unpublish(newsId, accountName);
                        return this.ToJson(data);
                    }
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy tin này để thực hiện action delete."));
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("waitpublish"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                    //chec khi xuat ban
                    if (news.ZoneId < 0)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate != null && news.DistributionDate <= DateTime.MinValue)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }

                    news.Status = (int)NewsStatus.WaitForPublish;
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("waiteditor"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                    //chec khi xuat ban
                    if (news.ZoneId < 0)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate != null && news.DistributionDate <= DateTime.MinValue)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }

                    news.Status = (int)NewsStatus.WaitForEdit;
                }

                //Logger.WriteLog(Logger.LogType.Trace, string.Format("CHINHNB log app bot savenews=> objnews: {0}, action: {1}", NewtonJson.Serialize(news), action));
                Logger.WriteLog(Logger.LogType.Trace, string.Format("CHINHNB log app bot savenews=> parentNewsId: {0} DistributionDate: {1} OriginalId: {2} Time: {3} action: {4}", parentNewsId, distributionDate, originalId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), action));

                //nhận vào queue
                var keyQueueId = "appbot_savenews";
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyQueueId, action, news);
                if (res > 0)
                {                    
                    //start job =>xử lý pop queue
                    TaskJobQueue(keyQueueId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private void TaskJobQueue(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);

                        if (queue != null)
                        {

                            var result = new NewsServices().AppBotSaveNews(queue.Message, queue.Action);

                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Trace, "CHINHNB Error PopQueue: => " + queue.Message.ParentNewsId.ToString() + NewtonJson.Serialize(queue));                                
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();

        }

        public static Dictionary<long, long> DicMapNewsID = new Dictionary<long, long>();

        [HttpPost, Route("save_newsv2")]
        public HttpResponseMessage SaveNewsV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                //fix tạm dừng
                //return this.ToJson(WcfActionResponse.CreateErrorResponse("Api tạm dừng hoạt động."));

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");
                if (!AlowAppBotUserNamePublishNews.Contains(accountName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }

                var newsid = body.Get("NewsId");
                Logger.WriteLog(Logger.LogType.Trace, "chinhnb log NewsId befor: " + newsid);
                var parentNewsId = Utility.ConvertToLong(newsid);
                if (parentNewsId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param newsid."));
                }

                long id = 0;

                var newsResult = NewsBo.GetNewsIdByParentNewsIdForAppbotPublish(parentNewsId);

                if(newsResult != null)
                {
                    id = newsResult.Id;
                }

                long newNewsId = 0;

                if (id == 0)
                {
                    newNewsId = CreateNewsId(Utility.ConvertToInt(body.Get("OriginalId")));

                    if (!DicMapNewsID.ContainsKey(parentNewsId))
                    {
                        DicMapNewsID.Add(parentNewsId, newNewsId);
                    }
                    else
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse(DicMapNewsID[parentNewsId].ToString(), "Dữ liệu đang được xử lý."));
                }
                else
                {
                    newNewsId = id;
                    if(newsResult.Status == (int)NewsStatus.Published && WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                    {
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse(newNewsId.ToString(), "update success."));
                    }
                }


                var title = body.Get("Title") ?? "";
                if (string.IsNullOrEmpty(title))
                {
                    if (DicMapNewsID.ContainsKey(parentNewsId))
                        DicMapNewsID.Remove(parentNewsId);

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không lấy được tiêu đề bài viết."));
                }

                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                if (zoneId <= 0)
                {
                    zoneId = CmsChannelConfiguration.GetAppSettingInInt32("AppBotCrawlerApiFixZoneId");//89;//fix zone Tin Tong Hop->ThoiDai
                }
                var bodyContent = body.Get("Body");
                bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var distributionDate = Utility.ParseDateTime(body.Get("DistributionDate"), formats);
                if (distributionDate == DateTime.MinValue)
                {
                    distributionDate = DateTime.Now;
                }
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInslide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("IsAdstore"));
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));

                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                if (string.IsNullOrEmpty(body.Get("IsOnHome")))
                    isOnHome = true;

                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceUrl");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"));
                var type = Utility.ConvertToInt(body.Get("Type"));
                if (newsType == -1)
                {
                    newsType = (int)NewsType.Normal;
                }
                if (type == 0) type = newsType;
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = data[0];
                        var note = data[1] ?? string.Empty;

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);

                var news = new NewsEntity
                {
                    Id = 0,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = DateTime.SpecifyKind(distributionDate, DateTimeKind.Unspecified),
                    Sapo = sapo,
                    Title = title,
                    SubTitle = subTitle,
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Unspecified),
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = initSapo,
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = shortTitle,
                    BonusPrice = bonusPrice,
                    ZoneId = zoneId,
                    Status = (int)NewsStatus.CrawlerByVccorp,
                    ParentNewsId = parentNewsId,
                    ListZoneId = zoneRelationIdList
                };


                news.Id = newNewsId;

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");
                var action = body.Get("Action");

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("publish"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));
                    }

                    //chec khi xuat ban
                    if (!string.IsNullOrEmpty(title) && title.Length > 255)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TieuDeBaiVietVuotQua255KyTu]));
                    }

                    if (news.ZoneId < 0)
                    {
                        if(DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate != null && news.DistributionDate <= DateTime.MinValue)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }

                    news.Status = (int)NewsStatus.Published;
                    var FixAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("FixAppBotUserNamePublishNews");
                    if (string.IsNullOrEmpty(FixAppBotUserNamePublishNews))
                    {
                        FixAppBotUserNamePublishNews = accountName;

                    }
                    news.PublishedBy = FixAppBotUserNamePublishNews;
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("unpublish"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));
                    }

                    var keyId = news.ParentNewsId + "_" + news.OriginalId;
                    var existsNewsParent = BoCached.Base.News.NewsDalFactory.GetNewsParentIdAndOriginalId(keyId);
                    if (null != existsNewsParent && existsNewsParent.NewsInfo != null)
                    {
                        var newsId = existsNewsParent.NewsInfo.Id;
                        var data = new NewsServices().Unpublish(newsId, accountName);
                        return this.ToJson(data);
                    }
                    if (DicMapNewsID.ContainsKey(parentNewsId))
                        DicMapNewsID.Remove(parentNewsId);

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy tin này để thực hiện action delete."));
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("waitpublish"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));
                    }

                    //chec khi xuat ban
                    if (news.ZoneId < 0)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate != null && news.DistributionDate <= DateTime.MinValue)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }

                    news.Status = (int)NewsStatus.WaitForPublish;
                }

                if (!string.IsNullOrEmpty(action) && action.ToLower().Equals("waiteditor"))
                {
                    if (!AlowAppBotCrawlerPublishNews)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));
                    }
                       

                    //chec khi xuat ban
                    if (news.ZoneId < 0)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                    }
                    if (string.IsNullOrEmpty(news.Avatar))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                    }
                    if (string.IsNullOrEmpty(news.Sapo))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                    }
                    if (string.IsNullOrEmpty(news.Body))
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                    }
                    if (news.DistributionDate != null && news.DistributionDate <= DateTime.MinValue)
                    {
                        if (DicMapNewsID.ContainsKey(parentNewsId))
                            DicMapNewsID.Remove(parentNewsId);

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                    }

                    news.Status = (int)NewsStatus.WaitForEdit;
                }

                //Logger.WriteLog(Logger.LogType.Trace, string.Format("CHINHNB log app bot savenews=> objnews: {0}, action: {1}", NewtonJson.Serialize(news), action));
                Logger.WriteLog(Logger.LogType.Trace, string.Format("CHINHNB log app bot savenews=> parentNewsId: {0} DistributionDate: {1} OriginalId: {2} Time: {3} action: {4}", parentNewsId, distributionDate, originalId, DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), action));

                //nhận vào queue
                //var keyQueueId = "appbot_savenews";
                //var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyQueueId, action, news);
                //if (res > 0)
                //{
                //    //start job =>xử lý pop queue
                //    TaskJobQueue(keyQueueId);

                //    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                //}
                TaskJobQueueV2(news, action);
                return this.ToJson(WcfActionResponse.CreateSuccessResponse(news.Id.ToString(), "update success."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        static Dictionary<long, int> countLoop = new Dictionary<long, int>();
        private void TaskJobQueueV2(NewsEntity news, string action)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    Thread.Sleep(1000);

                    var result = new NewsServices().AppBotSaveNews(news, action);

                    if (!result.Success)
                    {
                        int count = 0;
                        if(!countLoop.TryGetValue(news.Id, out count))
                        {
                            countLoop.Add(news.Id, 1);
                        }
                        else
                        {
                            countLoop[news.Id]++;
                        }
                        if(countLoop[news.Id] <= 3)
                        {
                            var jsonKey = "{\"Id\":" + news.Id + ", \"OriginalId\":" + news.OriginalId + "}";
                            ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(new { Id = news.ParentNewsId, OriginalId = news.OriginalId }), "initappbotpushnews", string.Empty, jsonKey);
                        }
                        Logger.WriteLog(Logger.LogType.Trace, "CHINHNB Error PopQueue: => " + news.ParentNewsId.ToString() + NewtonJson.Serialize(news));
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();
        }
        public static long CreateNewsId(int source)
        {
            //Thread.Sleep(30);
            TaskDelay(TimeSpan.FromSeconds(5));
            if (source >= 100)
                source = 99;
            var r = new Random();

            var newsIdDestion = Convert.ToInt64(string.Format("{7}{0}{2}{1}{3}{4}{5}{6}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond, source));
            if (newsIdDestion == 0)
                newsIdDestion = Convert.ToInt64(string.Format("{0}{2}{1}{3}{4}{5}{6}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond));

            newsIdDestion = newsIdDestion + r.Next(0, source);

            return newsIdDestion;
        }
        public static Task TaskDelay(TimeSpan timespan)
        {
            var tcs = new TaskCompletionSource<bool>();
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += (obj, args) =>
            {
                tcs.TrySetResult(true);
            };
            timer.Interval = timespan.TotalMilliseconds;
            timer.AutoReset = false;
            timer.Start();
            return tcs.Task;
        }

        [HttpPost, Route("remove_news")]
        public HttpResponseMessage RemoverNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var AlowAppBotDeleteNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotDeleteNews");
                var AlowAppBotUserNameDeleteNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNameDeleteNews");
                if (AlowAppBotDeleteNews && !string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowAppBotUserNameDeleteNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().RemoveNews(newsId, username);
                if (data.Success)
                {
                    //remove in Slave
                    var conn = ServiceChannelConfiguration.GetConfigs(WcfExtensions.WcfMessageHeader.Current.Namespace).ConnectionStrings;
                    var connectionStrings = conn.ContainsKey("CmsSlaveDb") ? conn["CmsSlaveDb"] :"";
                    if (!string.IsNullOrEmpty(connectionStrings))
                    {
                        var arrConnSlave = connectionStrings.Split(new char[] {',',';'});

                        foreach (var connect in arrConnSlave)
                        {
                            var connectSlave= Crypton.DecryptByKey(connect, Constants.ConnectionDecryptKey);

                            NewsSlaveBo.DeleteNews(newsId, connectSlave);
                        }
                    }                    
                }

                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #region Danh cho nhuan but
        [HttpPost, Route("list_news_by_ids")]
        public HttpResponseMessage ListNewsByIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsIds = body.Get("NewsIds");                
                if (string.IsNullOrEmpty(newsIds))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("NewsIds not empty!"));
                }
                var data = BoCached.Base.News.NewsDalFactory.GetNewsByListId(newsIds.Split(',').ToList());
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data.Select(s => new {
                    Id = s.Id,
                    EncryptId = s.EncryptId,
                    Status=s.Status,
                    Title=s.Title,
                    ZoneId=s.ZoneId,
                    ZoneName=s.ZoneName,
                    Author=s.Author,
                    CreatedDate = s.CreatedDate,
                    DistributionDate = s.DistributionDate,
                    CreatedBy = s.CreatedBy,
                    Avatar = s.Avatar
                }), "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion
        #endregion

        #region OTP

        [AllowAnonymous]
        [HttpGet, Route("registerotp")]
        public HttpResponseMessage RegisterOtp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("invalid request."));
               
                var secretKey = (queryStrings.FirstOrDefault(nv => nv.Key == "secretkey")).Value;
                var nameSpace = (queryStrings.FirstOrDefault(nv => nv.Key == "namespace")).Value;
                //username chuyen thanh user id => fomat dk < 60 char
                var username = (queryStrings.FirstOrDefault(nv => nv.Key == "username")).Value;
                var id = (queryStrings.FirstOrDefault(nv => nv.Key == "telegramid")).Value;
                var otp = (queryStrings.FirstOrDefault(nv => nv.Key == "otp")).Value;
                var otpLogin = (queryStrings.FirstOrDefault(nv => nv.Key == "otplogin")).Value;
                
                var telegramId = 0L;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(nameSpace) && !string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {                    
                    WcfExtensions.WcfMessageHeader.Current.Namespace = nameSpace;
                    telegramId = Convert.ToInt64(id);

                    var InsertDbExt = new SecurityServices().UpdateTelegramId(username, telegramId, otp, nameSpace);
                    if (InsertDbExt.Success)
                    {                            
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse("IMS2 " + nameSpace.ToUpper() +" OTP: "+ otpLogin, "Đăng ký tích hợp tài khoản Telegram thành công!"));
                    }
                    if(InsertDbExt.ErrorCode==210585 || InsertDbExt.ErrorCode == 210586)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(InsertDbExt.Message));                                            
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Cú pháp không hợp lệ."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("registerexpert")]
        public HttpResponseMessage RegisterExpert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("invalid request."));
                              
                var expertId = Utility.ConvertToInt((queryStrings.FirstOrDefault(nv => nv.Key == "expertid")).Value,0);
                if (expertId<=0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm chuyên gia thẩm định."));
                }
                var telegramId = Utility.ConvertToLong((queryStrings.FirstOrDefault(nv => nv.Key == "telegramid")).Value);
                if (telegramId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy tài khoản Telegram."));
                }
                var secretKey = (queryStrings.FirstOrDefault(nv => nv.Key == "secretkey")).Value;
                var nameSpace = (queryStrings.FirstOrDefault(nv => nv.Key == "namespace")).Value;
                var expireTime = Utility.ConvertToLong((queryStrings.FirstOrDefault(nv => nv.Key == "expiretime")).Value);


                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(nameSpace) && !string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {
                    WcfExtensions.WcfMessageHeader.Current.Namespace = nameSpace;
                                        
                    var InsertDbExt = new ExpertServices().UpdateTelegramId(expertId, telegramId, nameSpace, expireTime);
                    if (InsertDbExt.Success)
                    {
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success.", "Đăng ký tích hợp tài khoản thẩm định qua Telegram thành công!"));
                    }
                    if (InsertDbExt.ErrorCode == 210585 || InsertDbExt.ErrorCode == 210586 || InsertDbExt.ErrorCode == 210587)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(InsertDbExt.Message));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Cú pháp không hợp lệ."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion
    }
}
