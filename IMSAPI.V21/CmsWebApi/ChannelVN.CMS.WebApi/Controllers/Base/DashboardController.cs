﻿using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsRoyaltiesEx;
using ChannelVN.CMS.Entity.Base.Statistic;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/dashboard")]
    public class DashboardController : ApiController
    {
        #region DashboardUser

        [HttpPost, Route("list_news_by_username")]
        public HttpResponseMessage GetNewsIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var top = Utility.ConvertToInt(body.Get("Top"), 10);
                if (top <= 0)
                {
                    top = 100;
                }
                var listZoneId = body.Get("ListZoneId")??string.Empty;
                var username = body.Get("UserName") ?? string.Empty;

                var data = new NewsServices().GetIds(username, listZoneId, top);

                return this.ToJson(WcfActionResponse<List<string>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region Box Hot News
        [HttpPost, Route("get_hot_news")]
        public HttpResponseMessage GetHotNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                

                var top = Utility.ConvertToInt(body.Get("top"), 10);
                var day = Utility.ConvertToInt(body.Get("day"), 1);

                //var data = new NewsServices().GetHotNews(top, day);

                var data = new NewsServices().GetHotNews2(top, day);

                return this.ToJson(WcfActionResponse<List<NewsWithAnalyticEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion

        #region Thong ke san luong

        //san luong toan trang
        [HttpPost, Route("get_statistic_news_total production")]
        public HttpResponseMessage StatisticNewsTotalProduction(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = string.IsNullOrEmpty(body.Get("zoneId")) ? string.Empty : body.Get("zoneId");
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var startDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var endDate = Utility.ParseDateTime(body.Get("toDate"), formats);
                var data = new StatisticServices().GetStatisticNewsTotalProduction(zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                return this.ToJson(WcfActionResponse<List<NewsStatisticTotalProduction>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_statistic_news_total_production")]
        public HttpResponseMessage StatisticNewsTotal_Production(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = string.IsNullOrEmpty(body.Get("zoneId")) ? string.Empty : body.Get("zoneId");
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var startDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var endDate = Utility.ParseDateTime(body.Get("toDate"), formats);

                var data = new StatisticServices().GetStatisticNewsTotalProduction2(zoneId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));

                return this.ToJson(WcfActionResponse<List<NewsStatisticTotalProduction>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //visit toan trang
        [HttpPost, Route("statistic_domain_bydate")]
        public HttpResponseMessage StatisticDomainByDate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUsePageviewStatisticData");

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var toDate = Utility.ParseDateTime(body.Get("toDate"), formats);                
                var type = Utility.ConvertToInt(body.Get("type"),1);
                var data = new List<StatisticV3ViewCountEntity>();

                if (isUseAdtechData == "" || isUseAdtechData == "0")
                {
                    switch (type)
                    {
                        case 1:
                            data = new StatisticServices().ListPageViewAllByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            break;
                        case 2:
                            data = new StatisticServices().ListPageViewAllMobileByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            break;
                        default:
                            var web = new StatisticServices().ListPageViewAllByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            var mobile = new StatisticServices().ListPageViewAllMobileByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            if (web.Count > mobile.Count)
                            {
                                foreach (var statisticV3ViewCountEntity in web)
                                {
                                    var mobileView = mobile.FirstOrDefault(s => s.DateStamp == statisticV3ViewCountEntity.DateStamp);
                                    if (mobileView != null)
                                    {
                                        statisticV3ViewCountEntity.ViewCount += mobileView.ViewCount;
                                        statisticV3ViewCountEntity.VisitCount += mobileView.VisitCount;
                                    }
                                }
                                data = web;
                            }
                            else
                            {
                                foreach (var statisticV3ViewCountEntity in mobile)
                                {
                                    var mobileView = web.FirstOrDefault(s => s.DateStamp == statisticV3ViewCountEntity.DateStamp);
                                    if (mobileView != null)
                                    {
                                        statisticV3ViewCountEntity.ViewCount += mobileView.ViewCount;
                                        statisticV3ViewCountEntity.VisitCount += mobileView.VisitCount;
                                    }
                                }
                                data = mobile;
                            }
                            break;
                    }
                }
                else if (isUseAdtechData == "1")
                {
                    ChannelEntity objChannel = new StatisticServices().GetChannelByName(currentChannelNamespace);
                    var rs = new StatisticServices().PageViewAllSiteGetData(fromDate, toDate, objChannel.ChannelName);

                    if (!rs.Success)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));

                    List<PageViewAllSiteEntity> result = NewtonJson.Deserialize<List<PageViewAllSiteEntity>>(rs.Data);                    

                    if (result == null)
                    {
                        return this.ToJson(WcfActionResponse<List<NewsStatisticTotalProduction>>.CreateSuccessResponse(null, "Success"));
                    }

                    switch (type)
                    {
                        case 1:
                            foreach (var item in result)
                            {
                                data.Add(new StatisticV3ViewCountEntity()
                                {
                                    Date = item.LogDate,
                                    DateStamp = item.LogDateStamp,
                                    ViewCount = item.PageView,
                                    VisitCount = (int)Math.Round((item.PageView / objChannel.VisitCountRatio))
                                });
                            }
                            break;
                        case 2:
                            foreach (var item in result)
                            {
                                data.Add(new StatisticV3ViewCountEntity()
                                {
                                    Date = item.LogDate,
                                    DateStamp = item.LogDateStamp,
                                    ViewCount = item.PageViewMobile,
                                    VisitCount = (int)Math.Round((item.PageViewMobile / objChannel.VisitCountRatio))
                                });
                            }
                            break;
                        default:
                            foreach (var item in result)
                            {
                                int visitCount = (int)Math.Round((item.PageView / objChannel.VisitCountRatio)) + (int)Math.Round((item.PageViewMobile / objChannel.VisitCountRatio));

                                data.Add(new StatisticV3ViewCountEntity()
                                {
                                    Date = item.LogDate,
                                    DateStamp = item.LogDateStamp,
                                    ViewCount = item.PageView + item.PageViewMobile,
                                    VisitCount = visitCount
                                });
                            }
                            break;
                    }
                }
                else if(isUseAdtechData == "2")
                {
                    data = ViewsNetCoreServices.PageViewAllSiteGetData(type, fromDate, toDate, currentChannelNamespace);
                }

                if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                {
                    int multiply = 1;
                    try
                    {
                        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                    }
                    catch (Exception) { }

                    foreach (var item in data)
                    {
                        item.ViewCount = item.ViewCount * multiply;
                        item.VisitCount = item.VisitCount * multiply;
                    }
                }

                return this.ToJson(WcfActionResponse<List<StatisticV3ViewCountEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        //visit toan chuyen muc
        [HttpPost, Route("statistic_all_cate_by_date")]
        public HttpResponseMessage StatisticCateByDate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                
                var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUsePageviewStatisticData");

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var toDate = Utility.ParseDateTime(body.Get("toDate"), formats);
                var type = Utility.ConvertToInt(body.Get("type"));
                var cate = body.Get("cateId");
                if (string.IsNullOrEmpty(cate))
                {
                    cate = "0";
                }
                var data = new List<StatisticV3CategoryEntity>();

                if (isUseAdtechData == "" || isUseAdtechData == "0")
                {
                    switch (type)
                    {
                        case 1:
                            data = new StatisticServices().ListCategoryViewCountByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, cate, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            break;
                        case 2:
                            data = new StatisticServices().ListCategoryViewCountMobileByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, cate, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            break;
                        default:
                            var web = new StatisticServices().ListCategoryViewCountByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, cate, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            var mobile = new StatisticServices().ListCategoryViewCountMobileByChannel((ChannelId)CmsTrigger.Instance.StatisticTagChannelForUpdateView, cate, Utility.DateTimeToSpan(fromDate), Utility.DateTimeToSpan(toDate));
                            if (web.Count > mobile.Count)
                            {
                                foreach (var statisticV3ViewCountEntity in web)
                                {
                                    var mobileView = mobile.FirstOrDefault(s => (s.DailyTimeStamp == statisticV3ViewCountEntity.DailyTimeStamp && s.CatId == statisticV3ViewCountEntity.CatId));
                                    if (mobileView != null)
                                    {
                                        statisticV3ViewCountEntity.ViewCount += mobileView.ViewCount;
                                    }
                                }
                                data = web;
                            }
                            else
                            {
                                foreach (var statisticV3ViewCountEntity in mobile)
                                {
                                    var mobileView = web.FirstOrDefault(s => s.DailyTimeStamp == statisticV3ViewCountEntity.DailyTimeStamp && s.CatId == statisticV3ViewCountEntity.CatId);
                                    if (mobileView != null)
                                    {
                                        statisticV3ViewCountEntity.ViewCount += mobileView.ViewCount;
                                    }
                                }
                                data = mobile;
                            }
                            break;
                    }
                }
                else if (isUseAdtechData == "1")
                {
                    //ChannelEntity objChannel = new StatisticServices().GetChannelByName(currentChannelNamespace);
                    var rs = new StatisticServices().PageViewByZoneGetData(fromDate, toDate, cate, currentChannelNamespace);

                    if (!rs.Success)
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));

                    //Logger.WriteLog(Logger.LogType.Debug, "PageViewByZoneGetData => " + rs.Data);

                    var result = NewtonJson.Deserialize<List<PageViewByZoneEntity>>(rs.Data);                    

                    if (result == null)
                    {
                        return this.ToJson(WcfActionResponse<List<NewsStatisticTotalProduction>>.CreateSuccessResponse(null, "Success"));
                    }

                    switch (type)
                    {
                        case 1:
                            foreach (var item in result)
                            {
                                var pageViewTotal = GetZoneTotalView(item, result);
                                if (pageViewTotal != null)
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = pageViewTotal.WebView
                                    });
                                }
                                else
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = 0
                                    });
                                }
                            }
                            break;
                        case 2:
                            foreach (var item in result)
                            {
                                var pageViewTotal = GetZoneTotalView(item, result);
                                if (pageViewTotal != null)
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = pageViewTotal.MobileView
                                    });
                                }
                                else
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = 0
                                    });
                                }
                            }
                            break;
                        default:
                            foreach (var item in result)
                            {
                                var pageViewTotal = GetZoneTotalView(item, result);
                                if (pageViewTotal != null)
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = pageViewTotal.MobileView + pageViewTotal.WebView
                                    });
                                }
                                else
                                {
                                    data.Add(new StatisticV3CategoryEntity()
                                    {
                                        CatId = item.ZoneId,
                                        DailyTime = item.LogDate,
                                        DailyTimeStamp = item.LogDateStamp,
                                        ViewCount = 0
                                    });
                                }
                            }
                            break;
                    }
                }
                else if (isUseAdtechData == "2")
                {
                    data = ViewsNetCoreServices.PageViewByZoneGetData(type, fromDate, toDate, cate, currentChannelNamespace);
                }

                if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                {
                    int multiply = 1;
                    try
                    {
                        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                    }
                    catch (Exception) { }

                    foreach (var item in data)
                    {
                        item.ViewCount = item.ViewCount * multiply;
                    }
                }

                return this.ToJson(WcfActionResponse<List<StatisticV3CategoryEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        //san luong theo tai khoan
        [HttpPost, Route("get_statistic_news_by_account")]
        public HttpResponseMessage StatisticNewsByAccount(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                

                var accountName = body.Get("userName") ?? string.Empty;
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var startDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var endDate = Utility.ParseDateTime(body.Get("toDate"), formats);
                var data = new StatisticServices().GetStatisticStatusNewsByAccount(accountName, 8, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                return this.ToJson(WcfActionResponse<List<NewsStatisticEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("statistic_news_by_source")]
        public HttpResponseMessage StatisticNewsBySource(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var sourceId = Utility.ConvertToInt(body.Get("sourceId"), 0);
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var startDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
                var endDate = Utility.ParseDateTime(body.Get("toDate"), formats);
                var data = new StatisticServices().GetStatisticNewsBySourceId(sourceId, startDate.ToString("dd/MM/yyyy"), endDate.ToString("dd/MM/yyyy"));
                return this.ToJson(WcfActionResponse<List<NewsStatisticSourceEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        //SL/pageview chuyen muc
        [HttpPost, Route("statistic_all_cate_by_date_view")]
        public HttpResponseMessage StatisticAllCateByDate(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;
            var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;

            var zoneId = body.Get("zoneId") ?? "0";
            var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
            var startDate = Utility.ParseDateTime(body.Get("fromDate"), formats);
            var endDate = Utility.ParseDateTime(body.Get("toDate"), formats);

            var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUsePageviewStatisticData");

            var data = new List<StatisticV3CategoryEntity>();

            if (isUseAdtechData == "1")
            {
                List<PageViewByZoneEntity> result = null;
                var rs = new StatisticServices().PageViewByZoneGetData(startDate, endDate, zoneId, currentChannelNamespace);
                if (!rs.Success)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
                else
                    result = NewtonJson.Deserialize<List<PageViewByZoneEntity>>(rs.Data);

                if (result != null && result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        var zoneNewsCount = 0;
                        try
                        {
                            var zoneCounter = new NewsServices().GetZoneCounter(item.ZoneId, startDate, endDate);
                            zoneNewsCount = zoneCounter.TotalRow;
                        }
                        catch (Exception exception)
                        {
                            Logger.WriteLog(Logger.LogType.Error, exception.Message);
                        }
                        var currentData = data.Find(x => x.CatId == item.ZoneId);
                        if (currentData != null)
                        {
                            currentData.ViewCount += (item.PageView + item.PageViewMobile);
                            //currentData.xValues = (Utility.ConvertToInt(currentData.xValues) + zoneNewsCount).ToString();
                        }
                        else
                        {
                            data.Add(new StatisticV3CategoryEntity()
                            {
                                CatId = item.ZoneId,
                                DailyTime = item.LogDate,
                                DailyTimeStamp = item.LogDateStamp,
                                ViewCount = item.PageView + item.PageViewMobile,
                                xValues = zoneNewsCount.ToString()
                            });
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(zoneId))
                    {
                        var arr = zoneId.Split(',').ToList();
                        if (arr != null && arr.Count > 0)
                        {
                            foreach (var item in arr)
                            {
                                var zone = Utility.ConvertToInt(item);
                                var zoneCounter = new NewsServices().GetZoneCounter(zone, startDate, endDate);

                                data.Add(new StatisticV3CategoryEntity()
                                {
                                    CatId = zone,
                                    DailyTime = DateTime.Now,
                                    DailyTimeStamp = 0,
                                    ViewCount = 0,
                                    xValues = zoneCounter.TotalRow.ToString()
                                });
                            }
                        }
                    }
                }
            }
            else if (isUseAdtechData == "2")
            {
                var result = ViewsNetCoreServices.PageViewByZoneGetData(0, startDate, endDate, zoneId, currentChannelNamespace);

                if (result != null && result.Count > 0)
                {
                    foreach (var item in result)
                    {
                        var zoneNewsCount = 0;
                        try
                        {
                            var zoneCounter = new NewsServices().GetZoneCounter(item.CatId, startDate, endDate);
                            zoneNewsCount = zoneCounter.TotalRow;
                        }
                        catch (Exception exception)
                        {
                            Logger.WriteLog(Logger.LogType.Error, exception.Message);
                        }
                        var currentData = data.Find(x => x.CatId == item.CatId);
                        if (currentData != null)
                        {
                            currentData.ViewCount += item.ViewCount;
                            //currentData.xValues = (Utility.ConvertToInt(currentData.xValues) + zoneNewsCount).ToString();
                        }
                        else
                        {
                            data.Add(new StatisticV3CategoryEntity()
                            {
                                CatId = item.CatId,
                                DailyTime = item.DailyTime,
                                DailyTimeStamp = item.DailyTimeStamp,
                                ViewCount = item.ViewCount,
                                xValues = zoneNewsCount.ToString()
                            });
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(zoneId))
                    {
                        var arr = zoneId.Split(',').ToList();
                        if (arr != null && arr.Count > 0)
                        {
                            foreach (var item in arr)
                            {
                                var zone = Utility.ConvertToInt(item);
                                var zoneCounter = new NewsServices().GetZoneCounter(zone, startDate, endDate);

                                data.Add(new StatisticV3CategoryEntity()
                                {
                                    CatId = zone,
                                    DailyTime = DateTime.Now,
                                    DailyTimeStamp = 0,
                                    ViewCount = 0,
                                    xValues = zoneCounter.TotalRow.ToString()
                                });
                            }
                        }
                    }
                }
            }
            if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
            {
                int multiply = 1;
                try
                {
                    multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                }
                catch (Exception) {}
                
                foreach (var item in data)
                {
                    item.ViewCount = item.ViewCount * multiply;
                }
            }

            return this.ToJson(WcfActionResponse<List<StatisticV3CategoryEntity>>.CreateSuccessResponse(data, "Success"));
        }

        //Luot xem tin
        [HttpPost, Route("statistic_news_by_view")]
        public HttpResponseMessage StatisticNewsByView(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = body.Get("zoneId") ?? string.Empty;
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to")?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }               
                var toDate = Utility.ParseDateTime(to, formats);

                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var viewFrom = Utility.ConvertToLong(body.Get("viewFrom"));
                var viewTo = Utility.ConvertToLong(body.Get("viewTo"));
                var orderby = Utility.ConvertToInt(body.Get("orderby"),0);//=0 desc theo view, =1 desc theo distributiondate
                var accountName = body.Get("userName") ?? string.Empty;

                var pageRequest = 1;
                var sizeRequest = 5000;

                var totalRows = 0;
                var data = new NewsServices().ListNewsPublish(accountName, fromDate, toDate, zoneId, pageRequest, sizeRequest, ref totalRows);
                if (data == null)
                    data = new List<NewsSearchEntity>();

                var page = totalRows / sizeRequest;
                if (totalRows % sizeRequest > 0)
                    page++;

                if (page > 1)
                {
                    for (int i = 2; i <= page; i++)
                    {
                        var data_ext = new NewsServices().ListNewsPublish(accountName, fromDate, toDate, zoneId, i, sizeRequest, ref totalRows);
                        data.AddRange(data_ext);
                    }
                }

                //check id<100000 -> crc32
                var ids = data.Select(n => n.Id < 100000 ? Utility.Crc32(n.Url).ToString() : n.EncryptId).ToList();
                
                var totalRowsView = 0;
                var dataViews = new ViewsServices().SortViews(pageIndex, pageSize, string.Join(";", ids), "desc", orderby, viewFrom, viewTo, ref totalRowsView);
                var dataResult = new List<NewsSearchViewEntity>();
                if (dataViews == null || (dataViews != null && dataViews.Count <= 0))
                {
                    totalRowsView = totalRows;
                    foreach (var lst in data)
                    {                       
                        dataResult.Add(new NewsSearchViewEntity()
                        {
                            Id = lst.Id,
                            EncryptId = lst.EncryptId,
                            Title = lst.Title,
                            ZoneIds = lst.ZoneIds,
                            Type = lst.Type,
                            CreatedBy = lst.CreatedBy,
                            LastModifiedBy = lst.LastModifiedBy,
                            EditedBy = lst.EditedBy,
                            PublishedBy = lst.PublishedBy,
                            LastReceiver = lst.LastReceiver,
                            ApprovedBy = lst.ApprovedBy,
                            ReturnedBy = lst.ReturnedBy,
                            CreatedDate = lst.CreatedDate,
                            DistributionDate = lst.DistributionDate,
                            LastModifiedDate = lst.LastModifiedDate,
                            ApprovedDate = lst.ApprovedDate,
                            ViewCount = 0,
                            ViewPcCount = 0,
                            ViewMobCount = 0,
                            Status = lst.Status,
                            IsOnHome = lst.IsOnHome,
                            Avatar = lst.Avatar,
                            ViewRoyalties = lst.ViewRoyalties,
                            ZoneName = lst.ZoneName,
                            Note = lst.Note,
                            Author = lst.Author,
                            ErrorCheckedBy = lst.ErrorCheckedBy,
                            ErrorCheckedDate = lst.ErrorCheckedDate,
                            SensitiveCheckedBy = lst.SensitiveCheckedBy,
                            SensitiveCheckedDate = lst.SensitiveCheckedDate,
                            WordCount = lst.WordCount
                        });                        
                    }
                }
                else {//sort view
                    if (orderby == 0)
                    {
                        foreach (var item in dataViews)
                        {
                            var lst = data.SingleOrDefault(n => n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString());
                            if (lst != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                    else if (orderby == 1)//sort ngay xb
                    {                        
                        foreach (var lst in data)
                        {
                            var item = dataViews.SingleOrDefault(n => lst.EncryptId == n.NewsId.ToString() || Utility.Crc32(lst.Url).ToString() == n.NewsId.ToString());
                            if (item != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in dataViews)
                        {
                            var lst = data.SingleOrDefault(n => n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString());
                            if (lst != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                }

                if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                {
                    int multiply = 1;
                    try
                    {
                        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                    }
                    catch (Exception) { }

                    foreach (var item in dataResult)
                    {
                        item.ViewCount = item.ViewCount * multiply;
                        item.ViewMobCount = item.ViewMobCount * multiply;
                        item.ViewPcCount = item.ViewPcCount * multiply;
                    }
                }

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRowsView, News = dataResult }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        //Luot xem tin theo the loai cua nhuan but =>vietnewscorp
        [HttpPost, Route("statistic_view_by_cat_royalty")]
        public HttpResponseMessage StatisticViewByCateRoyalty(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = body.Get("zoneId");
                if (string.IsNullOrWhiteSpace(zoneId))
                {
                    zoneId = "0";
                }
                var catId = body.Get("catId");
                if (string.IsNullOrWhiteSpace(catId))
                {
                    catId = "0";
                }
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var viewFrom = Utility.ConvertToLong(body.Get("viewFrom"));
                var viewTo = Utility.ConvertToLong(body.Get("viewTo"));
                var orderby = Utility.ConvertToInt(body.Get("orderby"), 0);//=0 desc theo view, =1 desc theo distributiondate
                var accountName = body.Get("userName");
                if (string.IsNullOrWhiteSpace(accountName))
                {
                    accountName = string.Empty;
                }

                var pageRequest = 1;
                var sizeRequest = 5000;

                var totalRows = 0;
                var data = new NewsRoyaltiesExServices().SearchNewsRoyalty(accountName, zoneId, catId, fromDate, toDate, pageRequest, sizeRequest, ref totalRows);
                if (data == null)
                    data = new List<NewsSearchEntity>();

                var page = totalRows / sizeRequest;
                if (totalRows % sizeRequest > 0)
                    page++;

                if (page > 1)
                {
                    for (int i = 2; i <= page; i++)
                    {
                        var data_ext = new NewsRoyaltiesExServices().SearchNewsRoyalty(accountName, zoneId, catId, fromDate, toDate, i, sizeRequest, ref totalRows);
                        data.AddRange(data_ext);
                    }
                }

                //check id<100000 -> crc32
                var ids = data.Select(n => n.Id < 100000 ? Utility.Crc32(n.Url).ToString() : n.EncryptId).ToList();

                var totalRowsView = 0;
                var dataViews = new ViewsServices().SortViews(pageIndex, pageSize, string.Join(";", ids), "desc", orderby, viewFrom, viewTo, ref totalRowsView);
                var dataResult = new List<NewsSearchViewEntity>();
                if (dataViews == null || (dataViews != null && dataViews.Count <= 0))
                {
                    totalRowsView = totalRows;
                    foreach (var lst in data.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList())
                    {
                        if (string.IsNullOrEmpty(lst.Title)) continue;
                        dataResult.Add(new NewsSearchViewEntity()
                        {
                            Id = lst.Id,
                            EncryptId = lst.EncryptId,
                            Title = lst.Title,
                            ZoneIds = lst.ZoneIds,
                            Type = lst.Type,
                            CreatedBy = lst.CreatedBy,
                            LastModifiedBy = lst.LastModifiedBy,
                            EditedBy = lst.EditedBy,
                            PublishedBy = lst.PublishedBy,
                            LastReceiver = lst.LastReceiver,
                            ApprovedBy = lst.ApprovedBy,
                            ReturnedBy = lst.ReturnedBy,
                            CreatedDate = lst.CreatedDate,
                            DistributionDate = lst.DistributionDate,
                            LastModifiedDate = lst.LastModifiedDate,
                            ApprovedDate = lst.ApprovedDate,
                            ViewCount = 0,
                            ViewPcCount = 0,
                            ViewMobCount = 0,
                            Status = lst.Status,
                            IsOnHome = lst.IsOnHome,
                            Avatar = lst.Avatar,
                            ViewRoyalties = lst.ViewRoyalties,
                            ZoneName = lst.ZoneName,
                            Note = lst.Note,
                            Author = lst.Author,
                            ErrorCheckedBy = lst.ErrorCheckedBy,
                            ErrorCheckedDate = lst.ErrorCheckedDate,
                            SensitiveCheckedBy = lst.SensitiveCheckedBy,
                            SensitiveCheckedDate = lst.SensitiveCheckedDate,
                            WordCount = lst.WordCount
                        });
                    }
                }
                else {//sort view
                    if (orderby == 0)
                    {
                        foreach (var item in dataViews)
                        {
                            var lst = data.SingleOrDefault(n => (n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString()) && !string.IsNullOrEmpty(n.Title));
                            if (lst != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                    else if (orderby == 1)//sort ngay xb
                    {
                        foreach (var lst in data)
                        {
                            var item = dataViews.SingleOrDefault(n => (lst.EncryptId == n.NewsId.ToString() || Utility.Crc32(lst.Url).ToString() == n.NewsId.ToString()) && !string.IsNullOrEmpty(lst.Title));
                            if (item != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in dataViews)
                        {
                            var lst = data.SingleOrDefault(n => n.EncryptId == item.NewsId.ToString() || Utility.Crc32(n.Url).ToString() == item.NewsId.ToString());
                            if (lst != null)
                            {
                                dataResult.Add(new NewsSearchViewEntity()
                                {
                                    Id = lst.Id,
                                    EncryptId = lst.EncryptId,
                                    Title = lst.Title,
                                    ZoneIds = lst.ZoneIds,
                                    Type = lst.Type,
                                    CreatedBy = lst.CreatedBy,
                                    LastModifiedBy = lst.LastModifiedBy,
                                    EditedBy = lst.EditedBy,
                                    PublishedBy = lst.PublishedBy,
                                    LastReceiver = lst.LastReceiver,
                                    ApprovedBy = lst.ApprovedBy,
                                    ReturnedBy = lst.ReturnedBy,
                                    CreatedDate = lst.CreatedDate,
                                    DistributionDate = lst.DistributionDate,
                                    LastModifiedDate = lst.LastModifiedDate,
                                    ApprovedDate = lst.ApprovedDate,
                                    ViewCount = Convert.ToInt32(item.TotalViews),
                                    ViewPcCount = Convert.ToInt32(item.ViewPc),
                                    ViewMobCount = Convert.ToInt32(item.ViewMobile),
                                    Status = lst.Status,
                                    IsOnHome = lst.IsOnHome,
                                    Avatar = lst.Avatar,
                                    ViewRoyalties = lst.ViewRoyalties,
                                    ZoneName = lst.ZoneName,
                                    Note = lst.Note,
                                    Author = lst.Author,
                                    ErrorCheckedBy = lst.ErrorCheckedBy,
                                    ErrorCheckedDate = lst.ErrorCheckedDate,
                                    SensitiveCheckedBy = lst.SensitiveCheckedBy,
                                    SensitiveCheckedDate = lst.SensitiveCheckedDate,
                                    WordCount = lst.WordCount
                                });
                            }
                        }
                    }
                }

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRowsView, News = dataResult }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        
        //lấy thể loại của nhuận
        [HttpPost, Route("get_list_cat_royalty")]
        public HttpResponseMessage GetListCateRoyalty(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var data = new NewsRoyaltiesExServices().GetListCateRoyalty();
                
                return this.ToJson(WcfActionResponse<List<NewsCategoryEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("check_statistic_news_publish")]
        public HttpResponseMessage CheckStatisticNewsPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = body.Get("zoneId") ?? string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var viewFrom = Utility.ConvertToLong(body.Get("viewFrom"));
                var viewTo = Utility.ConvertToLong(body.Get("viewTo"));
                var orderby = Utility.ConvertToInt(body.Get("orderby"), 0);//=0 desc theo view, =1 desc theo distributiondate
                var accountName = body.Get("userName") ?? string.Empty;

                var pageRequest = 1;
                var sizeRequest = 5000;

                var totalRows = 0;
                var data = new NewsServices().ListNewsPublish(accountName, fromDate, toDate, zoneId, pageRequest, sizeRequest, ref totalRows);                

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #region  extend functions
        static ZoneViewValueContainer GetZoneTotalView(PageViewByZoneEntity currentZone, List<PageViewByZoneEntity> zoneList)
        {
            try {
                var result = new ZoneViewValueContainer()
                {
                    MobileView = currentZone.PageViewMobile,
                    WebView = currentZone.PageView
                };

                var childList = zoneList.Where(z => z.ParentId == currentZone.ZoneId);
                if (childList.Count() > 0)
                {
                    foreach (var item in childList)
                    {
                        var cList = zoneList.Where(z => z.ParentId == item.ZoneId);
                        if (cList.Count() > 0)
                        {
                            var r = GetZoneTotalView(item, zoneList);
                            if (r != null)
                            {
                                result.WebView += r.WebView;
                                result.MobileView += r.MobileView;
                            }                            
                        }
                        else
                        {
                            result.MobileView += item.PageViewMobile;
                            result.WebView += item.PageView;
                        }
                    }
                }
                return result;
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error,ex.Message);
                return null;
            }            
        }
        class ZoneViewValueContainer
        {
            public int WebView { get; set; }
            public int MobileView { get; set; }
        }
        #endregion

        #endregion

        #region Export Excel

        [HttpPost, Route("export_excel_view_video")]
        public HttpResponseMessage ExportExcelViewVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneIds = body.Get("ZoneIds");
                var createdBy = body.Get("CreatedBy");

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("FromDate"), formats);
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                
                var pageIndex = 1;//Utility.ConvertToInt(body.Get("Page"),1);
                var pageSize = 20;//Utility.ConvertToInt(body.Get("Num"),50);
                
                var tempExcel = CmsChannelConfiguration.GetAppSetting("TemplateViewVideoFile");                

                var fileOut = string.Empty;
                var totalRow = 0;
                var data = new MediaServices().ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                if (data!=null && data.Count>0 && totalRow > 0)
                {
                    var page = totalRow / pageSize;
                    if (totalRow % pageSize > 0)
                        page++;

                    for (int i= pageIndex; i<= page; i++)
                    {
                        if (i > 1)
                        {
                            data = new MediaServices().ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, i, pageSize, ref totalRow);
                        }
                        
                        fileOut = ExportExcelServices.ExportViewVideo(username, tempExcel, fromDate, toDate, data, i, page);
                    }
                }                

                if (!string.IsNullOrEmpty(fileOut))
                {
                    var fileNameUpload = Path.GetFileName(fileOut);
                    var directoryName = Path.GetDirectoryName(fileOut);
                    var path = directoryName.Replace(HttpContext.Current.Server.MapPath("/Data/"), "").Replace("\\", "/") + "/";
                    //upload storage                    
                    var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(username, path, fileNameUpload, false);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var result = false;
                    try
                    {
                        result = Services.ExternalServices.FileStorageServices.UploadFile(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(fileOut));
                        if (result == false)
                        {
                            return this.ToJson(WcfActionResponse.CreateErrorResponse("fileNameUpload: " + fileNameUpload));                            
                        }
                        //delete file
                        ExportExcelServices.DeleteDirectoryContents(directoryName);
                    }
                    catch (Exception ex)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(fileNameUpload + " => " + ex.Message));                        
                    }                    

                    var linkIndex = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + path + fileNameUpload;                    

                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(linkIndex, "Success."));
                }                

                return this.ToJson(WcfActionResponse<object>.CreateErrorResponse("Data not found"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));                
            }
        }

        private static string Serialize(string fileName)
        {
            using (FileStream reader = new FileStream(fileName, FileMode.Open))
            {
                byte[] buffer = new byte[reader.Length];
                reader.Read(buffer, 0, (int)reader.Length);
                return Convert.ToBase64String(buffer);
            }
        }

        [HttpPost, Route("export_excel_view_news")]
        public HttpResponseMessage ExportExcelViewNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneIds = body.Get("ZoneIds");
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("FromDate"), formats);
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                
                var pageIndex = 1;//Utility.ConvertToInt(body.Get("Page"),1);
                var pageSize = 20;//Utility.ConvertToInt(body.Get("Num"),50);
                var viewFrom = Utility.ConvertToLong(body.Get("ViewFrom"));
                var viewTo = Utility.ConvertToLong(body.Get("ViewTo"));
                var orderby = Utility.ConvertToInt(body.Get("orderby"), 0);//=0 desc theo view, =1 desc theo distributiondate
                var accountName = body.Get("userName") ?? string.Empty;

                var tempExcel = CmsChannelConfiguration.GetAppSetting("TemplateViewNewsFile");

                var pageRequest = 1;
                var sizeRequest = 5000;

                var totalRows = 0;
                var data = new NewsServices().ListNewsPublish(accountName, fromDate, toDate, zoneIds, pageRequest, sizeRequest, ref totalRows);
                if (data == null)
                    data = new List<NewsSearchEntity>();

                var page = totalRows / sizeRequest;
                if (totalRows % sizeRequest > 0)
                    page++;

                if (page > 1)
                {
                    for (int i = 2; i <= page; i++)
                    {
                        var data_ext = new NewsServices().ListNewsPublish(accountName, fromDate, toDate, zoneIds, i, sizeRequest, ref totalRows);
                        data.AddRange(data_ext);
                    }
                }
                
                var fileOut = string.Empty;
                var totalRowsView = 0;
                var dataEx = new NewsServices().ExportNewsPublish(data, orderby, viewFrom, viewTo, pageIndex, pageSize, ref totalRowsView);

                if (dataEx != null && dataEx.Count > 0)
                {
                    var pageEx = totalRowsView / pageSize;
                    if (totalRowsView % pageSize > 0)
                        pageEx++;

                    for (int i = pageIndex; i <= pageEx; i++)
                    {
                        if (i > 1)
                        {
                            dataEx = new NewsServices().ExportNewsPublish(data, orderby, viewFrom, viewTo, i, pageSize, ref totalRowsView);
                        }

                        fileOut = ExportExcelServices.ExportViewNews(username, tempExcel, fromDate, toDate, dataEx, i, pageEx);
                    }
                }
                else
                {
                    dataEx = data.Select(lst => new NewsSearchViewEntity()
                    {
                        Id = lst.Id,
                        EncryptId = lst.EncryptId,
                        Title = lst.Title,
                        ZoneIds = lst.ZoneIds,
                        Type = lst.Type,
                        CreatedBy = lst.CreatedBy,
                        LastModifiedBy = lst.LastModifiedBy,
                        EditedBy = lst.EditedBy,
                        PublishedBy = lst.PublishedBy,
                        LastReceiver = lst.LastReceiver,
                        ApprovedBy = lst.ApprovedBy,
                        ReturnedBy = lst.ReturnedBy,
                        CreatedDate = lst.CreatedDate,
                        DistributionDate = lst.DistributionDate,
                        LastModifiedDate = lst.LastModifiedDate,
                        ApprovedDate = lst.ApprovedDate,
                        ViewCount = 0,
                        ViewPcCount = 0,
                        ViewMobCount = 0,
                        Status = lst.Status,
                        IsOnHome = lst.IsOnHome,
                        Avatar = lst.Avatar,
                        ViewRoyalties = lst.ViewRoyalties,
                        ZoneName = lst.ZoneName,
                        Note = lst.Note,
                        Author = lst.Author,
                        ErrorCheckedBy = lst.ErrorCheckedBy,
                        ErrorCheckedDate = lst.ErrorCheckedDate,
                        SensitiveCheckedBy = lst.SensitiveCheckedBy,
                        SensitiveCheckedDate = lst.SensitiveCheckedDate,
                        WordCount = lst.WordCount
                    }).ToList();

                    fileOut = ExportExcelServices.ExportViewNews(username, tempExcel, fromDate, toDate, dataEx, 1, 1);
                }

                if (!string.IsNullOrEmpty(fileOut))
                {
                    var fileNameUpload = Path.GetFileName(fileOut);
                    var directoryName = Path.GetDirectoryName(fileOut);
                    var path = directoryName.Replace(HttpContext.Current.Server.MapPath("/Data/"), "").Replace("\\", "/") + "/";
                    //upload storage                    
                    var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(username, path, fileNameUpload, false);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var result = false;
                    try
                    {
                        result = Services.ExternalServices.FileStorageServices.UploadFile(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(fileOut));
                        if (result == false)
                        {
                            return this.ToJson(WcfActionResponse.CreateErrorResponse("fileNameUpload: " + fileNameUpload));
                        }
                        //delete file
                        ExportExcelServices.DeleteDirectoryContents(directoryName);
                    }
                    catch (Exception ex)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(fileNameUpload + " => " + ex.Message));
                    }

                    var linkIndex = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + path + fileNameUpload;

                    //delete file
                    ExportExcelServices.DeleteDirectoryContents(directoryName);

                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(linkIndex, "Success."));
                }

                return this.ToJson(WcfActionResponse<object>.CreateErrorResponse("Data not found"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}