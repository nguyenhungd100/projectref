﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
//using ChannelVN.CMS.MessageQueue;
//using ChannelVN.CMS.MessageQueue.Constants;
using ChannelVN.CMS.WebApi.Caching;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{    
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/toolkit")]
    public class ToolkitController : ApiController
    {
        #region Crawler Zone       
        [HttpPost, Route("crawler_zone")]
        public HttpResponseMessage CrawlerZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                //return this.ToJson(WcfActionResponse.CreateErrorResponse("Zone đã được lấy từ DB => api này tạm dừng lấy Zone"));

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");
                if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowAppBotUserNamePublishNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),0);
                if (zoneId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param ZoneId."));
                }

                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Name is required"));

                var description = body.Get("Description") ?? string.Empty;
                var shortURL = body.Get("OriginalUrl") ?? string.Empty;                
                var sortOrder = Utility.ConvertToInt(body.Get("SortOrder"), 0);
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var invisibled = Utility.ConvertToBoolean(body.Get("Invisibled"), false);
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var allowComment = Utility.ConvertToBoolean(body.Get("AllowComment"), false);
                var domain = body.Get("Domain") ?? string.Empty;
                if (string.IsNullOrEmpty(domain))
                {
                    domain = shortURL;
                }
                var avatar = body.Get("Avatar") ?? string.Empty;
                var avatarcover = body.Get("AvatarCover") ?? string.Empty;
                var logo = body.Get("Logo") ?? string.Empty;
                var metaAvatar = body.Get("MetaAvatar") ?? string.Empty;
                var isRebuildUrl = Utility.ConvertToBoolean(body.Get("IsRebuildUrl"), false);
                var zoneContent = body.Get("ZoneContent") ?? string.Empty;

                var zone = new ZoneEntity
                {
                    Id= zoneId,
                    Name = name,                    
                    Description = description,
                    CreatedDate = DateTime.Now,
                    ShortUrl = Utility.UnicodeToKoDauAndGach(name),
                    SortOrder = sortOrder,
                    ParentId = parentId,
                    Invisibled = invisibled,
                    Status = status,
                    AllowComment = allowComment,
                    Domain = domain,
                    Avatar = avatar,
                    AvatarCover = avatarcover,
                    Logo = logo,
                    MetaAvatar = metaAvatar,
                    ZoneContent = zoneContent                    
                };

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");
                
                if (!AlowAppBotCrawlerPublishNews)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));                

                //nhận vào redis trước
                var keyQueueId = "crawler_initzone";
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueZone(keyQueueId, accountName, zone);
                if (res > 0)
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueZone(keyQueueId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueZone(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueZone(keyId);

                        if (queue != null)
                        {
                            var zone = queue.Message;                           
                            var accountName = queue.Action;
                            
                            var result = new NewsServices().InsertZoneDB(zone, accountName);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();

        }
        #endregion

        #region Crawler News       
        [HttpPost, Route("crawler_news")]
        public HttpResponseMessage CrawlerNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");
                if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowAppBotUserNamePublishNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                if (newsId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param newsid."));
                }

                var title = body.Get("Title")??"";
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không lấy được tiêu đề bài viết."));
                }                

                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);                
                var bodyContent = body.Get("Body");
                bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);
                var distributionDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInslide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("IsAdstore"));
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));

                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"), true);
                
                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceUrl");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"),0);
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                if (newsType == -1)
                {
                    newsType = (int)NewsType.Normal;
                }
                //if (type == 0) type = newsType;
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = data[0];
                        var note = data[1] ?? string.Empty;

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);

                //accountName = "baodansinh";

                var news = new NewsEntity
                {
                    Id = newsId,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = distributionDate,
                    Sapo = sapo,
                    Title = title,
                    SubTitle = subTitle,
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList?.Replace(",", ";"),
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = initSapo,
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = shortTitle,
                    BonusPrice = bonusPrice,
                    ZoneId = zoneId,
                    Status = (int)NewsStatus.Published,
                    ParentNewsId= 0,
                    ListZoneId= zoneRelationIdList?.Replace(",", ";"),
                    Tag=tagIdList?.Replace(",", ";"),
                    
                };
                
                //var newNewsId = NewsBo.SetPrimaryId();
                //news.Id = newNewsId;

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");
                var action = "publish";
                
                if (!AlowAppBotCrawlerPublishNews)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                //chec khi xuat ban
                if (!string.IsNullOrEmpty(title) && title.Length > 255)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TieuDeBaiVietVuotQua255KyTu]));
                }
                    
                if (news.ZoneId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có chuyên mục khi xuất bản."));
                }
                if (string.IsNullOrEmpty(news.Avatar))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có avatar mặc định."));
                }
                if (string.IsNullOrEmpty(news.Sapo))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có sapo bài viết."));
                }
                if (string.IsNullOrEmpty(news.Body))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Phải có nội dung bài viết."));
                }
                if (news.DistributionDate!=null && news.DistributionDate<=DateTime.MinValue)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Ngày xuất bản không hợp lệ."));
                }

                news.Status = (int)NewsStatus.Published;
                news.PublishedBy = accountName;

                //nhận vào redis trước
                var keyQueueId = "crawler_initnews";
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyQueueId, action, news);
                if (res > 0)
                {                    
                    //start job =>xử lý pop queue
                    TaskJobQueueNews(keyQueueId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueNews(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);

                        if (queue != null)
                        {
                            var news = queue.Message;

                            var result = new NewsServices().AppCrawlerInitNewsDB(news);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));                                
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();

        }
        #endregion

        #region Crawler ZoneVideo
        [HttpPost, Route("crawler_zonevideo")]
        public HttpResponseMessage CrawlerZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                return this.ToJson(WcfActionResponse.CreateErrorResponse("ZoneVideo đã được lấy từ DB => api này tạm dừng lấy ZoneVideo"));

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");
                if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowAppBotUserNamePublishNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }

                var id = Utility.ConvertToInt(body.Get("ZoneVideoId"), 0);
                if (id < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param Id video."));
                }

                
                var parentId = Utility.ConvertToInt(body.Get("ParentId"),0);
                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Name is required"));

                var url = Utility.UnicodeToKoDauAndGach(name);

                var shortURL = body.Get("OriginalUrl") ?? string.Empty;

                var order = Utility.ConvertToInt(body.Get("Order"));
                var listVideoTagId = body.Get("ListVideoTagId") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), (int)EnumZoneVideoStatus.Actived);

                var avatar = body.Get("Avatar") ?? string.Empty;
                var avatarcover = body.Get("AvatarCover") ?? string.Empty;
                var zones = body.Get("Zones") ?? string.Empty;
                var showOnHome = Utility.ConvertToBoolean(body.Get("ShowOnHome"),true);
                var createdDate = DateTime.Now;
                var logo = body.Get("Logo") ?? string.Empty;
                var metaAvatar = body.Get("MetaAvatar") ?? string.Empty;

                var catId = Utility.ConvertToInt(body.Get("CatId"));
                var mode = Utility.ConvertToInt(body.Get("Mode"));
                var description = body.Get("Description") ?? string.Empty;
                if (string.IsNullOrEmpty(description))
                {
                    description = shortURL;
                }

                var zoneVideo = new ZoneVideoEntity
                {
                    Id = id,
                    ParentId = parentId,
                    Name = name,
                    Url = url,
                    Order = order,
                    CreatedDate = createdDate,
                    Status = status,
                    Avatar = avatar,
                    AvatarCover = avatarcover,
                    ZoneRelation = zones,
                    ModifiedDate = DateTime.Now,
                    ShowOnHome = showOnHome,
                    Logo = logo,
                    MetaAvatar = metaAvatar,
                    CatId = catId,
                    Mode = mode,
                    Description = description
                };

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");

                if (!AlowAppBotCrawlerPublishNews)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                //nhận vào redis trước
                var keyQueueId = "crawler_initzonevideo";
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueZoneVideo(keyQueueId, accountName, zoneVideo);
                if (res > 0)
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueZoneVideo(keyQueueId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueZoneVideo(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueZoneVideo(keyId);

                        if (queue != null)
                        {
                            var zoneVideo = queue.Message;
                            var listVideoTagId = "";
                            var accountName = queue.Action;
                            
                            var result = new MediaServices().InsertVideoZoneInitDB(zoneVideo, listVideoTagId);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(new { result.Message, queue }));
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();

        }
        #endregion

        #region Crawler Video       
        [HttpPost, Route("crawler_video")]
        public HttpResponseMessage CrawlerVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var AlowAppBotUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowAppBotUserNamePublishNews");
                if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowAppBotUserNamePublishNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }
                var videoId = Utility.ConvertToInt(body.Get("VideoId"), 0);
                if (videoId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param videoId."));
                }

                var status = Utility.ConvertToInt(body.Get("Status"), 1);                
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoNotEmpty]));
                }
                var isExternalVideo = Utility.ConvertToBoolean(body.Get("IsExternalVideo"));
                var isRemoveLogo = Utility.ConvertToBoolean(body.Get("IsRemoveLogo"), false);
                var file = body.Get("FileName");
                if (string.IsNullOrEmpty(file))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("FileName not empty."));
                }
                var keyVideo = body.Get("KeyVideo");
                if (string.IsNullOrEmpty(keyVideo))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.KeyVideoNotEmpty]));
                }
                var source = body.Get("Source");
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var trailerUrl = body.Get("TrailerUrl");
                var metaAvatar = body.Get("MetaAvatar");
                var zoneName = body.Get("ZoneName");
                var location = body.Get("Location");
                var policyContentId = body.Get("PolicyContentId");
                                                                                
                var unsignName = Utility.ConvertTextToUnsignName(name);
                var url = Utility.ConvertTextToLink(unsignName);
                var originalUrl = body.Get("OriginalUrl") ??Utility.ConvertTextToLink(unsignName);
                var allowAd = Utility.ConvertToBoolean(body.Get("AllowAd"));
                var video = new VideoEntity
                {
                    Id = videoId,
                    ZoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0),
                    Name = name,
                    UnsignName = unsignName,
                    Description = body.Get("Desc"),
                    HtmlCode = body.Get("HtmlCode"),
                    Avatar = body.Get("Avatar"),
                    KeyVideo = keyVideo,
                    Pname = body.Get("PName"),
                    Status = status,
                    NewsId = Utility.ConvertToLong(body.Get("NewsId")),
                    Views = Utility.ConvertToInt(body.Get("Views")),
                    Mode = Utility.ConvertToInt(body.Get("Mode")),
                    DistributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm"),
                    Tags = body.Get("Tags"),
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    Url = url,
                    OriginalUrl = originalUrl,
                    Source = source,
                    VideoRelation = body.Get("VideoRelationIdList"),
                    FileName = file,
                    Duration = body.Get("Duration"),
                    Size = body.Get("Size"),
                    Capacity = Utility.ConvertToInt(body.Get("Capacity"), 0),
                    AllowAd = allowAd,
                    IsRemoveLogo = isRemoveLogo,
                    AvatarShareFacebook = body.Get("AvatarShareFacebook"),
                    OriginalId = Utility.ConvertToInt(body.Get("OriginalId"), 0),
                    Author = body.Get("Author"),
                    ParentId = parentId,
                    HashId = body.Get("HashId"),
                    Type = type,
                    TrailerUrl = trailerUrl,
                    MetaAvatar = metaAvatar,
                    ZoneName = zoneName,
                    Location = location,
                    PolicyContentId = policyContentId,
                    DisplayStyle = body.Get("DisplayStyle"),
                    Namespace = body.Get("NameSpace") ?? ""
                };
                var tagIdList = body.Get("TagIdList");
                var playlistIdList = body.Get("PlaylistIdList");
                var cateName = body.Get("CateName");
                var zoneIdList = body.Get("ZoneIdList");
                var channelIdList = body.Get("ChannelIdList");

                var listAuthorByVideo = body.Get("ListAuthorByVideo");
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByVideo))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByVideo.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = data[0];
                        var note = data[1] ?? string.Empty;

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var AlowAppBotCrawlerPublishNews = CmsChannelConfiguration.GetAppSettingInBoolean("AlowAppBotCrawlerPublishNews");

                if (!AlowAppBotCrawlerPublishNews)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Chức năng này đang tạm khóa."));

                //nhận vào redis trước
                var keyQueueId = "crawler_initvideo";
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueVideo(keyQueueId, accountName, video);
                if (res > 0)
                {
                    //start job =>xử lý pop queue
                    TaskJobQueueVideo(keyQueueId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueueVideo(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try
                {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueVideo(keyId);

                        if (queue != null)
                        {
                            var video = queue.Message;
                            var accountName = queue.Action;
                            var tagIdList = "";
                            var playlistIdList = "";
                            var zoneIdList = "";
                            var channelIdList = "";
                            var authorList = new List<string>();

                            //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(queue));

                            var result = new MediaServices().SaveVideoInitDB(video, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });

            if (checkStart)
                thread.Start();

        }
        #endregion

        #region Job recovery queuedb

        //[HttpPost, Route("job_recovery_queue")]
        //public HttpResponseMessage JobQueueRecoveryQueueDB(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;
        //        var action = body.Get("Action")??string.Empty;
        //        if (action.Equals("start"))
        //        {
        //            WebBackgrounderSetup.Start();
        //            //QueueJobHost.Start();
        //            return this.ToJson(WcfActionResponse.CreateSuccessResponse());
        //        }
        //        if (action.Equals("stop"))
        //        {
        //            WebBackgrounderSetup.Stop();
        //            //QueueJobHost.Stop();
        //            return this.ToJson(WcfActionResponse.CreateSuccessResponse());
        //        }
        //        if (action.Equals("shutdown"))
        //        {
        //            WebBackgrounderSetup.Shutdown();
        //            return this.ToJson(WcfActionResponse.CreateSuccessResponse());
        //        }

        //        QueueJob.JobQueueRecoveryQueueDB();

        //        return this.ToJson(WcfActionResponse.CreateSuccessResponse());
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}
        
        #endregion
    }
}
