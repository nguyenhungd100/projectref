﻿using ChannelVN.CMS.BO.Base.Vote;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Vote;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/vote")]
    public class VoteController : ApiController
    {
        #region Vote

        [HttpPost, Route("insert_vote")]
        public HttpResponseMessage Insert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var zoneIdList = body.Get("ZoneIdList");
                var title = body.Get("Title");
                var avatar = body.Get("Avatar");
                var sapo = body.Get("Sapo");
                var status = Utility.ConvertToInt(body.Get("Status"));
                var author = body.Get("Author");
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var maxAnswers = Utility.ConvertToInt(body.Get("MaxAnswers"));
                var showInZone = Utility.ConvertToBoolean(body.Get("ShowInZone"));
                var showInFooter = Utility.ConvertToBoolean(body.Get("ShowInFooter"));
                var startedDate = Utility.ConvertToDateTimeByFormat(body.Get("StartedDate"), "dd/MM/yyyy HH:mm");
                var endedDate = Utility.ConvertToDateTimeByFormat(body.Get("EndedDate"), "dd/MM/yyyy HH:mm");
                var userName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var strlistAnswers = body.Get("ListAnswers");

                var voteEntity = new VoteEntity
                {
                    Title = title,
                    Avatar = avatar,
                    Sapo = sapo,
                    Status = status,
                    Author = author,
                    CreatedBy = userName,
                    LastModifiedBy = userName,
                    Priority = priority,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    MaxAnswers = maxAnswers,
                    ShowInZone = showInZone,
                    ShowInFooter = showInFooter,
                    StartedDate = startedDate,
                    EndedDate = endedDate
                };

                var result = new VoteServices().Insert(voteEntity, zoneId, zoneIdList);
                if (result.Success) { result.Message = "Success."; };

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("update_vote")]
        public HttpResponseMessage Update(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var zoneIdList = body.Get("ZoneIdList");
                var title = body.Get("Title");
                var avatar = body.Get("Avatar");
                var sapo = body.Get("Sapo");
                var status = Utility.ConvertToInt(body.Get("Status"));
                var author = body.Get("Author");
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var maxAnswers = Utility.ConvertToInt(body.Get("MaxAnswers"));
                var showInZone = Utility.ConvertToBoolean(body.Get("ShowInZone"));
                var showInFooter = Utility.ConvertToBoolean(body.Get("ShowInFooter"));
                var startedDate = Utility.ConvertToDateTimeByFormat(body.Get("StartedDate"), "dd/MM/yyyy HH:mm");
                var endedDate = Utility.ConvertToDateTimeByFormat(body.Get("EndedDate"), "dd/MM/yyyy HH:mm");
                var userName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var strlistAnswers = body.Get("ListAnswers");

                var voteEntity = new VoteEntity
                {
                    Id = id,
                    Title = title,
                    Avatar = avatar,
                    Sapo = sapo,
                    Status = status,
                    Author = author,
                    CreatedBy = userName,
                    LastModifiedBy = userName,
                    Priority = priority,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    MaxAnswers = maxAnswers,
                    ShowInZone = showInZone,
                    ShowInFooter = showInFooter,
                    StartedDate = startedDate,
                    EndedDate = endedDate
                };

                var result = new VoteServices().Update(voteEntity, zoneId, zoneIdList);
                if (result.Success) { result.Message = "Success."; };

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("get_vote_detail")]
        public HttpResponseMessage GetInfoVote(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));                            
                
                var result = new VoteServices().GetInfo(id);
                
                return this.ToJson(WcfActionResponse<VoteDetailEntity>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete_vote")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));               
               
                var result = new VoteServices().Delete(id);
                if (result.Success) { result.Message = "Success."; };

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("search_vote")]
        public HttpResponseMessage SearchVote(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var isGetTotal = Utility.ConvertToBoolean(body.Get("isGetTotal"));
                var totalRow = 0;
                var data = new VoteServices().GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Votes = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #region vote answers

        [HttpPost, Route("insert_answers")]
        public HttpResponseMessage InsertAnswers(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var value = body.Get("value");
                var voteId = Utility.ConvertToInt(body.Get("voteId"));
                var voteRate = Utility.ConvertToInt(body.Get("voteRate"));
                var status = Utility.ConvertToInt(body.Get("status"));
                var priority = Utility.ConvertToInt(body.Get("priority"));

                var voteAnswersEntity = new VoteAnswersEntity()
                {
                    VoteId = voteId,
                    Value = value,
                    VoteRate = voteRate,
                    Status = status,
                    Priority = priority
                };

                var result = new VoteServices().InsertAnswers(voteAnswersEntity);
                if (result.Success) { result.Message = "Success."; };

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_answers")]
        public HttpResponseMessage UpdateAnswers(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var value = body.Get("value");
                var voteId = Utility.ConvertToInt(body.Get("voteId"));
                var voteRate = Utility.ConvertToInt(body.Get("voteRate"));
                var status = Utility.ConvertToInt(body.Get("status"));
                var priority = Utility.ConvertToInt(body.Get("priority"));

                var voteAnswersEntity = new VoteAnswersEntity()
                {
                    Id = id,
                    VoteId = voteId,
                    Value = value,
                    VoteRate = voteRate,
                    Status = status,
                    Priority = priority
                };

                var result = new VoteServices().UpdateAnswers(voteAnswersEntity);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_answers")]
        public HttpResponseMessage DeleteAnswers(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                
                var result = new VoteServices().DeleteAnswers(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_priority_answers")]
        public HttpResponseMessage UpdatePriorityAnswers(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listId = body.Get("listAnswersId");

                var result = new VoteServices().UpdatePriorityAnswers(listId);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("list_voteanswers_by_voteid")]
        public HttpResponseMessage GetListAnswersByVoteId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var voteId = Utility.ConvertToInt(body.Get("VoteId"));
                
                var data = new VoteServices().GetListAnswersByVoteId(voteId);
                return this.ToJson(WcfActionResponse<List<VoteAnswersEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }
        #endregion

        #endregion
    }
}
