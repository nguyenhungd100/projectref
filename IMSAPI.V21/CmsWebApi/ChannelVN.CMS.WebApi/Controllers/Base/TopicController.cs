﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/topic")]
    public class TopicController : ApiController
    {
        #region Topic

        [HttpPost, Route("search_topic")]
        public HttpResponseMessage SearchTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 10);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var parentId = Utility.ConvertToInt(body.Get("parentId"), -1);
                var isHotTopic = Utility.ConvertToInt(body.Get("isHotTag"), -1);
                var order = Utility.ConvertToInt(body.Get("orderBy"), 0);
                var isActive = Utility.ConvertToInt(body.Get("isActive"), -1);

                var totalRow = 0;
                var data = new TopicServices().SearchTopic(keyword, zoneId, order, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_topic_by_id")]
        public HttpResponseMessage GetTopicById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new TopicServices().GetTopicByTopicId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotTopicObject]));
                }

                return this.ToJson(WcfActionResponse<TopicDetailEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("save_topic")]
        public HttpResponseMessage SaveTopic(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var topicName = body.Get("topic_name") ?? string.Empty;
                var logo = body.Get("logo") ?? string.Empty;
                var cover = body.Get("cover") ?? string.Empty;
                var isActive = Utility.ConvertToBoolean(body.Get("is_active"));
                var isIconActive = Utility.ConvertToBoolean(body.Get("is_icon_active"));
                var desc = body.Get("description") ?? string.Empty;
                var logoFancyClose = body.Get("logo_fancy_close") ?? string.Empty;
                var logoTopicName = body.Get("logo_topic_name") ?? string.Empty;
                var logoSubMenu = body.Get("logo_sub_menu") ?? string.Empty;
                var defaultViewMode = Utility.ConvertToInt(body.Get("default_view_mode"));
                var guideToSendMail = body.Get("guide_to_send_mail") ?? string.Empty;
                var topicEmail = body.Get("topic_email") ?? string.Empty;
                var isTopToolbar = Utility.ConvertToBoolean(body.Get("is_top_toolbar"));
                var zoneId = Utility.ConvertToInt(body.Get("zone_id"), 0);
                var zoneIdList = body.Get("zoneidlist");
                var listTags = body.Get("list_tags") ?? string.Empty;
                var parentId = Utility.ConvertToInt(body.Get("parent_id"), 0);
                var relationTopic = body.Get("relation_topic") ?? string.Empty;
                var displayName = body.Get("display_name") ?? string.Empty;
                var priority = Utility.ConvertToInt(body.Get("priority"), 0);
                var isAmp = Utility.ConvertToBoolean(body.Get("IsAmp"), false);

                //seo
                var metaTitle = body.Get("MetaTitle") ?? string.Empty;
                var metaKeyword = body.Get("MetaKeyword") ?? string.Empty;
                var metaDescription = body.Get("MetaDescription") ?? string.Empty;

                var topicNew = new TopicEntity
                {
                    Id = id,
                    TopicName = topicName,
                    Logo = logo,
                    Cover = cover,
                    IsActive = isActive,
                    IsIconActive = isIconActive,
                    Description = desc,
                    LogoFancyClose = logoFancyClose,
                    LogoTopicName = logoTopicName,
                    LogoSubMenu = logoSubMenu,
                    DefaultViewMode = defaultViewMode,
                    GuideToSendMail = guideToSendMail,
                    TopicEmail = topicEmail,
                    IsTopToolbar = isTopToolbar,
                    DisplayUrl = Utility.UnicodeToKoDauAndGach(topicName),
                    TagInString = listTags,
                    ParentId=parentId,
                    RelationTopic= relationTopic,
                    DisplayName=displayName,
                    Priority=priority,
                    ZoneId= zoneId,
                    ZoneIdList = zoneIdList,
                    IsAmp=isAmp
                };
                                
                if (id > 0)
                {
                    topicNew.ModifiedDate = DateTime.Now;
                    topicNew.ModifiedBy = accountName;
                    var result = new TopicServices().Update(topicNew, zoneId, zoneIdList);

                    //seo metatopic
                    var metaRep = new Services.Extension.SeoServices().SEOMetaTopicUpdate(new SEO.Entity.SEOMetaTopicEntity
                    {                        
                        MetaTitle = metaTitle,
                        MetaKeyword = metaKeyword,
                        MetaDescription = metaDescription,
                        TopicId = topicNew.Id
                    });

                    return this.ToJson(result);
                }
                else {
                    var checkTopic = new TopicServices().GetTopicByName(topicNew.TopicName);
                    if (checkTopic != null && checkTopic.TopicName.Equals(topicNew.TopicName))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Topic \"" + checkTopic.TopicName + "\" đã tồn tại"));
                    }
                    topicNew.CreatedDate = DateTime.Now;
                    topicNew.CreatedBy = accountName;
                    topicNew.ModifiedDate = DateTime.Now;
                    topicNew.ModifiedBy = accountName;
                    var newsTopicId = 0;
                    var result = new TopicServices().Insert(topicNew, zoneId, zoneIdList, ref newsTopicId);
                    result.Data = newsTopicId.ToString();

                    if (newsTopicId > 0)
                    {
                        //seo metatopic
                        var metaRep = new Services.Extension.SeoServices().SEOMetaTopicInsert(new SEO.Entity.SEOMetaTopicEntity
                        {
                            MetaTitle = metaTitle,
                            MetaKeyword = metaKeyword,
                            MetaDescription = metaDescription,
                            TopicId = newsTopicId
                        });
                    }

                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("topic_delete")]
        public HttpResponseMessage TopicDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var topicId = Utility.ConvertToInt(body.Get("topic_id"));

                var result = new TopicServices().DeleteById(topicId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_topic_news")]
        public HttpResponseMessage UpdateTopicNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var topicId = Utility.ConvertToInt(body.Get("topic_id"),0);
                var listDelete = body.Get("list_delete_ids");
                var listAdd = body.Get("list_add_ids");

                var result = new TopicServices().UpdateTopicNews(topicId, listDelete, listAdd);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_topic_id")]
        public HttpResponseMessage SearchNewsByTopicId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var topicId = Utility.ConvertToLong(body.Get("topic_id"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("page_size"), 20);

                var totalRow = 0;
                var data = new NewsServices().SearchNewsByTopicIdWithPaging(keyword, topicId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<NewsInTopicSearch>.CreateSuccessResponse(new NewsInTopicSearch { TotalRow = totalRow, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_publish_by_topic_id")]
        public HttpResponseMessage SearchPublishNewsByNewsInTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var topicId = Utility.ConvertToLong(body.Get("topic_id"));
                var zoneId = Utility.ConvertToInt(body.Get("zone_id"), 0);
                var keyWords = body.Get("keywords");
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("page_size"), 20);
                var totalRows = 0;

                var data = new NewsServices().SearchNewsPublishExcludeNewsInTopic(zoneId, keyWords, topicId, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_parent_topic_by_id")]
        public HttpResponseMessage ListParentTopicById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var topicId = Utility.ConvertToInt(body.Get("TopicId"), 0);
                                                
                var data = new TopicServices().ListParentTopicById(topicId);

                return this.ToJson(WcfActionResponse<List<TopicParent>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //là topic hiện
        [HttpPost, Route("toggle_topic_isactive")]
        public HttpResponseMessage ToggleTopicActive(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("TopicId"), 0);                

                var data = new TopicServices().ToggleTopicActive(id);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        //cho icon hiệu lực
        [HttpPost, Route("toggle_topic_isiconactive")]
        public HttpResponseMessage ToggleTopicIconActive(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("TopicId"), 0);               

                var data = new TopicServices().ToggleTopicIconActive(id);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        //cho phép lên top heard
        [HttpPost, Route("toggle_topic_istoolbar")]
        public HttpResponseMessage ToggleTopicIsToolbar(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("TopicId"), 0);                

                var data = new TopicServices().ToggleTopicIsToolbar(id);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region HotNewsTopic

        [HttpPost, Route("add_news_topic_hot")]
        public HttpResponseMessage AddTopicNewsHot(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var topicId = Utility.ConvertToLong(body.Get("topic_id"));
                var listAddNewsAvatar = body.Get("list_news_avatar");
                var listAddNewsId = body.Get("list_newsid");

                var result = new TopicServices().UpdateTopicNewsHot(topicId, listAddNewsAvatar, listAddNewsId);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_hot_news_in_topic")]
        public HttpResponseMessage SearchHotNewsByTopicId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var topicId = Utility.ConvertToLong(body.Get("topic_id"));

                var data = new NewsServices().SearchHotNewsByTopicId(topicId);

                return this.ToJson(WcfActionResponse<List<NewsEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxTopicEmbed
        [HttpPost, Route("list_box_topic_embed")]
        public HttpResponseMessage ListBoxTopicEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var topicId = Utility.ConvertToInt(body.Get("TopicId"));
                var type = Utility.ConvertToInt(body.Get("Type"));

                var data = new TopicServices().ListBoxTopicEmbed(topicId, type);

                return this.ToJson(WcfActionResponse<List<BoxTopicEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_muilt_box_topic_embed")]
        public HttpResponseMessage SaveMultBoxTopicEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";
                
                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new TopicServices().DeleteBoxTopicEmbed(id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {                    
                    var listObj = NewtonJson.Deserialize<List<BoxTopicEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;                                
                                var data = new TopicServices().InsertBoxTopicEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new TopicServices().UpdateBoxTopicEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not parser list object in ListData."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("listData not emty."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_box_topic_embed")]
        public HttpResponseMessage SaveBoxTopicEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
                var topicId = Utility.ConvertToInt(body.Get("TopicId"),0);
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                var label = body.Get("Label") ?? string.Empty;
                var title = body.Get("Title") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var url = body.Get("Url") ?? string.Empty;
                var description = body.Get("Description") ?? string.Empty;
                var sortOrder = Utility.ConvertToInt(body.Get("SortOrder"),0);
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), 0);

                var box = new BoxTopicEmbedEntity
                {
                    Id = id,
                    TopicId = topicId,
                    Type=type,
                    Label = label,
                    Title = title,
                    Avatar = avatar,
                    Url = url,
                    Description= description,
                    SortOrder =sortOrder,
                    LastModifiedDate=DateTime.Now,
                    DisplayStyle= displayStyle
                };

                if (box.Id <= 0)
                {
                    var result = new TopicServices().InsertBoxTopicEmbed(box, ref id);

                    return this.ToJson(result);
                }
                else
                {
                    var result = new TopicServices().UpdateBoxTopicEmbed(box);

                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_box_topic_embed")]
        public HttpResponseMessage UpdateBoxTopicEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var id = Utility.ConvertToInt(body.Get("Id"),0);
                
                var result = new TopicServices().DeleteBoxTopicEmbed(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxTopicOnPageEmbed
        [HttpPost, Route("list_box_topic_onpage_embed")]
        public HttpResponseMessage ListBoxTopicOnPageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var type = Utility.ConvertToInt(body.Get("Type"));

                var data = new TopicServices().ListBoxTopicOnPageEmbed(zoneId, type);

                return this.ToJson(WcfActionResponse<List<BoxTopicOnPageEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_muilt_box_topic_onpage_embed")]
        public HttpResponseMessage SaveMultBoxTopicOnPageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";

                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new TopicServices().DeleteBoxTopicOnPageEmbed(id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<BoxTopicOnPageEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;
                                var data = new TopicServices().InsertBoxTopicOnPageEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new TopicServices().UpdateBoxTopicOnPageEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not parser list object in ListData."));
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_box_topic_onpage_embed")]
        public HttpResponseMessage UpdateBoxTopicOnPageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));
                var listThreadId = body.Get("listTopicId");
                var data = new TopicServices().UpdateBoxTopicOnPageEmbed(listThreadId, zoneId, type);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_box_topic_onpage_embed")]
        public HttpResponseMessage SaveBoxTopicOnPageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var topicId = Utility.ConvertToInt(body.Get("TopicId"), 0);
                var type = Utility.ConvertToInt(body.Get("Type"), 0);               
                var sortOrder = Utility.ConvertToInt(body.Get("SortOrder"), 0);

                var box = new BoxTopicOnPageEmbedEntity
                {
                    Id = id,
                    ZoneId = zoneId,
                    TopicId = topicId,
                    Type = type,                    
                    SortOrder = sortOrder,
                    LastModifiedDate = DateTime.Now
                };

                if (box.Id <= 0)
                {
                    var result = new TopicServices().InsertBoxTopicOnPageEmbed(box, ref id);

                    return this.ToJson(result);
                }
                else
                {
                    var result = new TopicServices().UpdateBoxTopicOnPageEmbed(box);

                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_box_topic_onpage_embed")]
        public HttpResponseMessage DeleteBoxTopicOnPageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var result = new TopicServices().DeleteBoxTopicOnPageEmbed(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
