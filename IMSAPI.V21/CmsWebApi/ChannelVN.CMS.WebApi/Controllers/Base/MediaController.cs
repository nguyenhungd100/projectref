﻿using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.FileUpload;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfMapping;
using ChannelVN.CMS.WcfMapping.ServicesVideoInfo;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/media")]
    public class MediaController : ApiController
    {
        #region updates
        [HttpPost, Route("insert_video")]
        public HttpResponseMessage InsertVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var videoId = Utility.ConvertToInt(body.Get("Id"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                //if (status == 1)
                //{
                //    var otp = body.Get("Otp");
                //    var responseData = new SecurityServices().IsValidOTP(otp, accountName);
                //    if (!responseData.Success) return this.ToJson(responseData);
                //}
                var name = body.Get("Name");
                var isExternalVideo = Utility.ConvertToBoolean(body.Get("IsExternalVideo"));
                var isRemoveLogo = Utility.ConvertToBoolean(body.Get("IsRemoveLogo"), false);
                var file = body.Get("FileName");
                var keyVideo = body.Get("KeyVideo");
                var source = body.Get("Source");
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var trailerUrl = body.Get("TrailerUrl");
                var metaAvatar = body.Get("MetaAvatar");
                var zoneName = body.Get("ZoneName");
                var location = body.Get("Location");
                var policyContentId = body.Get("PolicyContentId");
                //lay video từ vtv ->cho convert 7               
                var action = body.Get("Action");
                if(!string.IsNullOrEmpty(action) && action == "CloneVideo")
                {
                    status = (int)EnumVideoStatus.CloneVideo;
                }

                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"), false);
                var isAMP = Utility.ConvertToBoolean(body.Get("IsAMP"), false);
                var sourceId = Utility.ConvertToInt(body.Get("SourceId"), 0);

                if (string.IsNullOrEmpty(keyVideo) && !string.IsNullOrEmpty(file))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.KeyVideoNotEmpty]));

                    //var path = file.Replace(System.IO.Path.GetFileName(file), "");
                    //var filename = System.IO.Path.GetFileNameWithoutExtension(file);
                    //var extension = System.IO.Path.GetExtension(file);
                    //keyVideo = VideoStorageServices.VideoGetMediaKey(accountName, path, filename, extension);
                }

                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoNotEmpty]));
                }
                else if (string.IsNullOrEmpty(keyVideo) && action != "CloneVideo")
                {                    
                     return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFindKeyVideo]));                                        
                }
                else
                {
                    var unsignName = Utility.ConvertTextToUnsignName(name);
                    var url = Utility.ConvertTextToLink(unsignName);
                    var originalUrl = Utility.ConvertTextToLink(unsignName);
                    var allowAd = Utility.ConvertToBoolean(body.Get("AllowAd"));
                    var video = new VideoEntity
                    {
                        Id = isExternalVideo ? 0 : videoId,
                        ZoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0),
                        Name = name,
                        UnsignName = unsignName,
                        Description = body.Get("Desc"),
                        HtmlCode = body.Get("HtmlCode"),
                        Avatar = body.Get("Avatar"),
                        KeyVideo = keyVideo,
                        Pname = string.IsNullOrEmpty(VideoStorageServices.CustomPname) ? body.Get("PName") : VideoStorageServices.CustomPname,
                        Status = status,
                        NewsId = Utility.ConvertToLong(body.Get("NewsId")),
                        Views = Utility.ConvertToInt(body.Get("Views")),
                        Mode = Utility.ConvertToInt(body.Get("Mode")),
                        DistributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm"),
                        Tags = body.Get("Tags"),
                        CreatedBy = accountName,
                        CreatedDate = DateTime.Now,
                        LastModifiedBy = accountName,
                        Url = url,
                        OriginalUrl=originalUrl,
                        Source = source,
                        VideoRelation = body.Get("VideoRelationIdList"),
                        FileName = file,
                        Duration = body.Get("Duration"),
                        Size = body.Get("Size"),
                        Capacity = Utility.ConvertToInt(body.Get("Capacity"), 0),
                        AllowAd = allowAd,
                        IsRemoveLogo = isRemoveLogo,
                        AvatarShareFacebook = body.Get("AvatarShareFacebook"),
                        OriginalId = Utility.ConvertToInt(body.Get("OriginalId"), 0),
                        Author = body.Get("Author"),
                        ParentId = parentId,
                        HashId = body.Get("HashId"),
                        Type = type,
                        TrailerUrl = trailerUrl,
                        MetaAvatar = metaAvatar,
                        ZoneName = zoneName,
                        Location = location,
                        PolicyContentId = policyContentId,
                        DisplayStyle = body.Get("DisplayStyle"),
                        Namespace= body.Get("NameSpace")??"",
                        IsOnHome = isOnHome,
                        IsAMP = isAMP,
                        SourceId = sourceId
                    };
                    var tagIdList = body.Get("TagIdList");
                    var playlistIdList = body.Get("PlaylistIdList");
                    var cateName = body.Get("CateName");
                    var zoneIdList = body.Get("ZoneIdList");
                    var channelIdList = body.Get("ChannelIdList");

                    var listAuthorByVideo = body.Get("ListAuthorByVideo");
                    var authorList = new List<string>();
                    if (!string.IsNullOrEmpty(listAuthorByVideo))
                    {
                        var authorNameList = string.Empty;
                        var authorIdList = string.Empty;
                        var authorNoteList = string.Empty;

                        var authorParamList = listAuthorByVideo.Split(',');
                        foreach (var group in authorParamList)
                        {
                            var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                            if (!match.Success) continue;

                            var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                            var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                            var authorName = data[0];
                            var note = data[1] ?? string.Empty;

                            authorNameList += ";" + authorName;
                            authorNoteList += ";" + note;
                            authorIdList += ";" + authorId;
                        }
                        if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                        if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                        if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                        authorList.Add(authorIdList);
                        authorList.Add(authorNameList);
                        authorList.Add(authorNoteList);
                    }

                    var result = new MediaServices().SaveVideoV5(video, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList);
                    if (result.Success)
                    {
                        if (videoId <= 0)
                        {
                            videoId = Utility.ConvertToInt(result.Data);
                        }                        

                        var metaKeywordFocus = body.Get("MetaKeywordFocus");                        
                        var metaTitle = body.Get("MetaTitle");
                        var metaDescription = body.Get("MetaDescription");
                        var metaVideoKeyword = body.Get("MetaVideoKeyword");
                        var metaKeyword = body.Get("MetaKeyword");                        

                        TaskUpdateVideoDataAsync(videoId, metaKeywordFocus, metaTitle, metaDescription, metaVideoKeyword, metaKeyword);

                        //try
                        //{
                        //    //video mở rộng bỏ
                        //    //if (isExternalVideo)
                        //    //{
                        //    //    new ExternalVideoServices().RecieveExternalVideo(GetQueryString.GetPost("externalVideoId", 0));
                        //    //}

                        //    //sync sang video tap chung chỗ Hiếu -> giờ ko cần bỏ ->tất cả Microservice

                        //    //var primaryZoneTag = "";
                        //    //var tagForPrimaryZone = new MediaServices().GetVideoTagByZoneVideoId(video.ZoneId);
                        //    //if (tagForPrimaryZone != null)
                        //    //{
                        //    //    primaryZoneTag = tagForPrimaryZone.Aggregate(primaryZoneTag, (current, videoTagEntity) => current + (";" + videoTagEntity.Name));
                        //    //    if (!string.IsNullOrEmpty(primaryZoneTag)) primaryZoneTag = primaryZoneTag.Remove(0, 1);
                        //    //}
                        //    //var noAdv = 0;
                        //    //if (allowAd)
                        //    //{
                        //    //    noAdv = 1;
                        //    //}

                        //    //Logger.WriteLog(Logger.LogType.Info, string.Format("SyncVideo(KeyVideo={0}, Tag={1}, noAdv={2})", video.KeyVideo, primaryZoneTag, noAdv));

                        //    //VideoStorageServices.SyncVideoToVideoApi(accountName, video.Name, primaryZoneTag, video.ZoneId,
                        //    //                                         cateName,
                        //    //                                         zoneIdList, video.VideoRelation, noAdv, false, video.Status,
                        //    //                                         video.KeyVideo);
                        //    //if (!string.IsNullOrEmpty(video.Avatar))
                        //    //{
                        //    //    VideoStorageServices.UpdateThumbnail(accountName, keyVideo, video.Avatar);
                        //    //}
                        //}
                        //catch (Exception ex)
                        //{
                        //    Logger.WriteLog(Logger.LogType.Error, string.Format("SyncVideo Error:{0}", ex.Message));
                        //}

                        //RemoveLogo bỏ
                        //try
                        //{
                        //    VideoStorageServices.UpdateVideoLogo(accountName, video.KeyVideo, isRemoveLogo);
                        //}
                        //catch (Exception ex)
                        //{
                        //    Logger.WriteLog(Logger.LogType.Error, string.Format("UpdateVideoLogo Error:{0}", ex.Message));
                        //}
                    }

                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_video")]
        public HttpResponseMessage UpdateVideoV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("Id"));
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var status = Utility.ConvertToInt(body.Get("Status"));
                //if (status == 1)
                //{
                //    var otp = body.Get("Otp");
                //    var responseData = new SecurityServices().IsValidOTP(otp, accountName);
                //    if (!responseData.Success) return this.ToJson(responseData);
                //}
                var name = body.Get("Name");
                var isExternalVideo = Utility.ConvertToBoolean(body.Get("IsExternalVideo"));
                var isRemoveLogo = Utility.ConvertToBoolean(body.Get("IsRemoveLogo"), false);
                var file = body.Get("FileName");
                var keyVideo = body.Get("KeyVideo");
                var source = body.Get("Source");
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var trailerUrl = body.Get("TrailerUrl");
                var metaAvatar = body.Get("MetaAvatar");
                var zoneName = body.Get("ZoneName");
                var location = body.Get("Location");
                var policyContentId = body.Get("PolicyContentId");
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"), false);
                var isAMP = Utility.ConvertToBoolean(body.Get("IsAMP"), false);
                var sourceId = Utility.ConvertToInt(body.Get("SourceId"), 0);


                if (string.IsNullOrEmpty(keyVideo) && !string.IsNullOrEmpty(file))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.KeyVideoNotEmpty]));

                    //var path = file.Replace(System.IO.Path.GetFileName(file), "");
                    //var filename = System.IO.Path.GetFileNameWithoutExtension(file);
                    //var extension = System.IO.Path.GetExtension(file);
                    //keyVideo = VideoStorageServices.VideoGetMediaKey(accountName, path, filename, extension);
                }
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoNotEmpty]));
                }
                else if (string.IsNullOrEmpty(keyVideo))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFindKeyVideo]));
                }
                else
                {
                    var unsignName = Utility.ConvertTextToUnsignName(name);
                    var url = Utility.ConvertTextToLink(unsignName);
                    var allowAd = Utility.ConvertToBoolean(body.Get("AllowAd"));
                    var video = new VideoEntity
                    {
                        Id = isExternalVideo ? 0 : videoId,
                        ZoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0),
                        Name = name,
                        UnsignName = unsignName,
                        Description = body.Get("Desc"),
                        HtmlCode = body.Get("HtmlCode"),
                        Avatar = body.Get("Avatar"),
                        KeyVideo = keyVideo,
                        Pname = string.IsNullOrEmpty(VideoStorageServices.CustomPname) ? body.Get("PName") : VideoStorageServices.CustomPname,
                        Status = status,
                        NewsId = Utility.ConvertToLong(body.Get("NewsId")),
                        Views = Utility.ConvertToInt(body.Get("Views")),
                        Mode = Utility.ConvertToInt(body.Get("Mode")),
                        DistributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm"),
                        Tags = body.Get("Tags"),
                        CreatedBy = accountName,
                        LastModifiedBy = accountName,
                        Url = url,
                        Source = source,
                        VideoRelation = body.Get("VideoRelationIdList"),
                        FileName = file,
                        Duration = body.Get("Duration"),
                        Size = body.Get("Size"),
                        Capacity = Utility.ConvertToInt(body.Get("Capacity")),
                        AllowAd = allowAd,
                        IsRemoveLogo = isRemoveLogo,
                        AvatarShareFacebook = body.Get("AvatarShareFacebook"),
                        OriginalId = Utility.ConvertToInt(body.Get("OriginalId"), 0),
                        Author = body.Get("Author"),
                        ParentId = parentId,
                        HashId = body.Get("HashId"),
                        Type = type,
                        TrailerUrl = trailerUrl,
                        MetaAvatar = metaAvatar,
                        ZoneName = zoneName,
                        Location = location,
                        PolicyContentId = policyContentId,
                        DisplayStyle = body.Get("DisplayStyle"),
                        Namespace = body.Get("NameSpace") ?? "",
                        IsOnHome = isOnHome,
                        IsAMP = isAMP,
                        SourceId = sourceId
                    };
                    var tagIdList = body.Get("TagIdList");
                    var playlistIdList = body.Get("PlaylistIdList");
                    var cateName = body.Get("CateName");
                    var zoneIdList = body.Get("ZoneIdList");
                    var channelIdList = body.Get("ChannelIdList");

                    var listAuthorByVideo = body.Get("ListAuthorByVideo");
                    var authorList = new List<string>();
                    if (!string.IsNullOrEmpty(listAuthorByVideo))
                    {
                        var authorNameList = string.Empty;
                        var authorIdList = string.Empty;
                        var authorNoteList = string.Empty;

                        var authorParamList = listAuthorByVideo.Split(',');
                        foreach (var group in authorParamList)
                        {
                            var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                            if (!match.Success) continue;

                            var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                            var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                            var authorName = data[0];
                            var note = data[1] ?? string.Empty;

                            authorNameList += ";" + authorName;
                            authorNoteList += ";" + note;
                            authorIdList += ";" + authorId;
                        }
                        if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                        if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                        if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                        authorList.Add(authorIdList);
                        authorList.Add(authorNameList);
                        authorList.Add(authorNoteList);
                    }

                    var result = new MediaServices().SaveVideoV5(video, tagIdList, zoneIdList, playlistIdList, channelIdList, authorList);
                    if (result.Success)
                    {
                        var metaKeywordFocus = body.Get("MetaKeywordFocus");
                        var metaTitle = body.Get("MetaTitle");
                        var metaDescription = body.Get("MetaDescription");
                        var metaVideoKeyword = body.Get("MetaVideoKeyword");
                        var metaKeyword = body.Get("MetaKeyword");

                        TaskUpdateVideoDataAsync(videoId, metaKeywordFocus, metaTitle, metaDescription, metaVideoKeyword, metaKeyword);

                        //try
                        //{
                        //    //if (isExternalVideo)
                        //    //{
                        //    //    new ExternalVideoServices().RecieveExternalVideo(GetQueryString.GetPost("externalVideoId", 0));
                        //    //}

                        //    //var primaryZoneTag = "";
                        //    //var tagForPrimaryZone = new MediaServices().GetVideoTagByZoneVideoId(video.ZoneId);
                        //    //if (tagForPrimaryZone != null)
                        //    //{
                        //    //    primaryZoneTag = tagForPrimaryZone.Aggregate(primaryZoneTag, (current, videoTagEntity) => current + (";" + videoTagEntity.Name));
                        //    //    if (!string.IsNullOrEmpty(primaryZoneTag)) primaryZoneTag = primaryZoneTag.Remove(0, 1);
                        //    //}
                        //    //var noAdv = 0;
                        //    //if (allowAd)
                        //    //{
                        //    //    noAdv = 1;
                        //    //}
                        //    ////Logger.WriteLog(Logger.LogType.Info, string.Format("SyncVideo(KeyVideo={0}, Tag={1}, noAdv={2})", video.KeyVideo, primaryZoneTag, noAdv));

                        //    //VideoStorageServices.SyncVideoToVideoApi(accountName, video.Name, primaryZoneTag, video.ZoneId,
                        //    //                                         cateName,
                        //    //                                         zoneIdList, video.VideoRelation, noAdv, false, video.Status,
                        //    //                                         video.KeyVideo);
                        //    //if (!string.IsNullOrEmpty(video.Avatar))
                        //    //{
                        //    //    VideoStorageServices.UpdateThumbnail(accountName, keyVideo, video.Avatar);
                        //    //}
                        //}
                        //catch (Exception ex)
                        //{
                        //    Logger.WriteLog(Logger.LogType.Error, string.Format("SyncVideo Error:{0}", ex.Message));
                        //}
                        //try
                        //{
                        //    VideoStorageServices.UpdateVideoLogo(accountName, video.KeyVideo, isRemoveLogo);
                        //}
                        //catch (Exception ex)
                        //{
                        //    Logger.WriteLog(Logger.LogType.Error, string.Format("UpdateVideoLogo Error:{0}", ex.Message));
                        //}
                    }

                    result.Data = video.HtmlCode;

                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_video_info")]
        public HttpResponseMessage SaveVideoInfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var videoId = Utility.ConvertToInt(body.Get("Id"), 0);
                var isRemoveLogo = Utility.ConvertToBoolean(body.Get("IsRemoveLogo"), false);

                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoNotEmpty]));
                }
                
                var file = body.Get("FileName");
                if (string.IsNullOrEmpty(file))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFindKeyVideo]));
                }

                var keyVideo = body.Get("KeyVideo");
                if (string.IsNullOrEmpty(keyVideo))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.KeyVideoNotEmpty]));
                }      
                          
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var zoneName = body.Get("ZoneName");
                var source = body.Get("Source");
                var location = body.Get("Location");
                var policyContentId = body.Get("PolicyContentId");
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"), 0);
                var allowAd = Utility.ConvertToBoolean(body.Get("AllowAd"),false);                
                var description = body.Get("Description");
                var htmlCode = body.Get("HtmlCode");
                var avatar = body.Get("Avatar");
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var tags = body.Get("Tags");
                var duration = body.Get("Duration");
                var size = body.Get("Size");
                var capacity = Utility.ConvertToInt(body.Get("Capacity"), 0);
                var author = body.Get("Author");
                var pName = string.IsNullOrEmpty(VideoStorageServices.CustomPname) ? body.Get("PName") : VideoStorageServices.CustomPname;
                var status = Utility.ConvertToInt(body.Get("Status"), 0);

                var unsignName = Utility.ConvertTextToUnsignName(name);
                var url = Utility.ConvertTextToLink(unsignName);
                var originalUrl = Utility.ConvertTextToLink(unsignName);

                //check keyvideo
                //var dataVideo = new MediaServices().GetDetailVideoByKeyvideo(keyVideo);
                //if (dataVideo != null)
                //{
                //    videoId = dataVideo.Id;
                //    status = dataVideo.Status;
                //}

                var video = new VideoEntity
                {
                    Id = videoId,
                    ZoneId = zoneId,
                    Name = name,
                    UnsignName = unsignName,
                    Description = description,
                    HtmlCode = htmlCode,
                    Avatar = avatar,
                    KeyVideo = keyVideo,
                    Pname = pName,      
                    Status= 0,  //fix status            
                    NewsId = newsId,                                        
                    Tags = tags,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate= DateTime.Now,
                    LastModifiedBy = accountName,
                    Url = url,
                    OriginalUrl = originalUrl,
                    Source = source,                    
                    FileName = file,
                    Duration = duration,
                    Size = size,
                    Capacity = capacity,
                    AllowAd = allowAd,
                    IsRemoveLogo = isRemoveLogo,                    
                    OriginalId = originalId,
                    Author = author,
                    ZoneName = zoneName,
                    Location = location,
                    PolicyContentId = policyContentId                    
                };                
                  
                var result = new MediaServices().SaveVideoInfo(video);
                    
                return this.ToJson(result);                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_video_info_v2")]
        public HttpResponseMessage SaveVideoInfoV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var videoId = Utility.ConvertToInt(body.Get("Id"), 0);
                var isRemoveLogo = Utility.ConvertToBoolean(body.Get("IsRemoveLogo"), false);

                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoNotEmpty]));
                }

                var file = body.Get("FileName");
                if (string.IsNullOrEmpty(file))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFindKeyVideo]));
                }

                var keyVideo = body.Get("KeyVideo");
                if (string.IsNullOrEmpty(keyVideo))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.KeyVideoNotEmpty]));
                }

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var zoneName = body.Get("ZoneName");
                var source = body.Get("Source");
                var location = body.Get("Location");
                var policyContentId = body.Get("PolicyContentId");
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"), 0);
                var allowAd = Utility.ConvertToBoolean(body.Get("AllowAd"), false);
                var description = body.Get("Description");
                var htmlCode = body.Get("HtmlCode");
                var avatar = body.Get("Avatar");
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var tags = body.Get("Tags");
                var duration = body.Get("Duration");
                var size = body.Get("Size");
                var capacity = Utility.ConvertToInt(body.Get("Capacity"), 0);
                var author = body.Get("Author");
                var pName = string.IsNullOrEmpty(VideoStorageServices.CustomPname) ? body.Get("PName") : VideoStorageServices.CustomPname;
                var status = Utility.ConvertToInt(body.Get("Status"), 0);

                var unsignName = Utility.ConvertTextToUnsignName(name);
                var url = Utility.ConvertTextToLink(unsignName);
                var originalUrl = Utility.ConvertTextToLink(unsignName);              

                //check keyvideo
                //var dataVideo = new MediaServices().GetDetailVideoByKeyvideo(keyVideo);
                //if (dataVideo != null)
                //{
                //    videoId = dataVideo.Id;
                //    status = dataVideo.Status;
                //}

                var video = new VideoEntity
                {
                    Id = videoId,
                    ZoneId = zoneId,
                    Name = name,
                    UnsignName = unsignName,
                    Description = description,
                    HtmlCode = htmlCode,
                    Avatar = avatar,
                    KeyVideo = keyVideo,
                    Pname = pName,
                    Status = 0,  //fix status            
                    NewsId = newsId,
                    Tags = tags,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    Url = url,
                    OriginalUrl = originalUrl,
                    Source = source,
                    FileName = file,
                    Duration = duration,
                    Size = size,
                    Capacity = capacity,
                    AllowAd = allowAd,
                    IsRemoveLogo = isRemoveLogo,
                    OriginalId = originalId,
                    Author = author,
                    ZoneName = zoneName,
                    Location = location,
                    PolicyContentId = policyContentId                  
                };

                var result = new MediaServices().SaveVideoInfo(video);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private void TaskUpdateVideoDataAsync(int videoId, string metaKeywordFocus, string metaTitle, string metaDescription, string metaVideoKeyword, string metaKeyword)
        {
            var task = Task.Run(() =>
            {
                //Cập nhật SEO                
                var seoService = new SeoServices();
                var seoMetaVideo = seoService.GetSeoMetaVideoById(videoId);
                if (seoMetaVideo == null)
                {
                    seoMetaVideo = new SEO.Entity.SEOMetaVideoEntity();
                }
                seoMetaVideo.MetaTitle = metaTitle;
                seoMetaVideo.MetaKeyword = metaKeyword;
                seoMetaVideo.MetaVideoKeyword = metaVideoKeyword;
                seoMetaVideo.MetaDescription = metaDescription;
                seoMetaVideo.KeywordFocus = metaKeywordFocus;
                
                if (seoMetaVideo.VideoId <= 0)
                {
                    seoMetaVideo.VideoId = videoId;
                    seoService.SEOMetaVideoInsert(seoMetaVideo);
                }
                else
                {
                    seoService.SEOMetaVideoUpdate(seoMetaVideo);
                }                            
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }
        #endregion

        #region action
        [HttpPost, Route("publish_video")]
        public HttpResponseMessage PublishVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().PublishVideo(videoId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("unpublish_video")]
        public HttpResponseMessage UnpublishVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().UnpublishVideo(videoId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("send_video")]
        public HttpResponseMessage SendVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().SendVideo(videoId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("return_video")]
        public HttpResponseMessage ReturnVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().ReturnVideo(videoId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("delete_video")]
        public HttpResponseMessage DeleteVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().DeleteVideoById(videoId);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("update_mode")]
        public HttpResponseMessage UpdateVideoMode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var mode = Utility.ConvertToInt(body.Get("mode"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().UpdateVideoMode(mode, videoId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("update_status_by_hashid")]
        public HttpResponseMessage UpdateStatusVideoByHashId(HttpRequestMessage request)
        {
            try
            {
                //Logger.WriteLog(Logger.LogType.Debug, "1.Dung Do: update_status_by_hashid: begin");

                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var hashId = body.Get("hash_id");
                if (string.IsNullOrEmpty(hashId))
                {
                    //Logger.WriteLog(Logger.LogType.Debug, "2.Dung Do: update_status_by_hashid: hash_id rỗng không hợp lệ");
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("hash_id not empty!"));
                }
                var keyVideo = body.Get("key_video");
                if (string.IsNullOrEmpty(keyVideo))
                {
                    //Logger.WriteLog(Logger.LogType.Debug, "3.Dung Do: update_status_by_hashid: key_video rỗng không hợp lệ");
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("key_video not empty!"));
                }
                
                var data = new MediaServices().UpdateStatusVideoByHashId(hashId, keyVideo, username);

                //Logger.WriteLog(Logger.LogType.Debug, "4.Dung Do: update_status_by_hashid: End: hashId:" + hashId + ",keyVideo:" + keyVideo + ",username" + username +", result:"+ data.Success);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Debug, "5.Dung Do: update_status_by_hashid: key_video rỗng không hợp lệ");
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Search
        [HttpPost, Route("list_video_by_status")]
        public HttpResponseMessage ListVideoByStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var mode = Utility.ConvertToInt(body.Get("mode"));

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApproveVideo))
                {
                    if (status == (int)EnumVideoStatus.Temporary ||
                        status == (int)EnumVideoStatus.Return ||
                        status == (int)EnumVideoStatus.CloneVideo ||
                        status == (int)EnumVideoStatus.YoutubeUploading)
                    {
                        accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.VideoManager))
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().ListVideoByStatus(accountName, videoStatus, videoSortOrder, mode, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_video_by_status_single")]
        public HttpResponseMessage ListVideoByStatusSingle(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var mode = Utility.ConvertToInt(body.Get("mode"));

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApproveVideo))
                {
                    if (status == (int)EnumVideoStatus.Temporary ||
                        status == (int)EnumVideoStatus.Return ||
                        status == (int)EnumVideoStatus.DoneVideo ||
                        status == (int)EnumVideoStatus.YoutubeUploading)
                    {
                        accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.VideoManager))
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().ListVideoByStatusSingle(accountName, videoStatus, videoSortOrder, mode, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_my_video_single")]
        public HttpResponseMessage ListMyVideoSingle(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var mode = Utility.ConvertToInt(body.Get("mode"));
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("toDate")) ? "" : body.Get("toDate") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");

                var totalRow = 0;
                var data = new MediaServices().ListMyVideoSingle(accountName, zoneVideoId, keyword, fromDate, toDate, videoSortOrder, mode, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_video")]
        public HttpResponseMessage SearchVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var playlistId = Utility.ConvertToInt(body.Get("playlistId"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("toDate")) ? "" : body.Get("toDate") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                var templateType = Utility.ConvertToInt(body.Get("templateType"));
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApproveVideo))
                {
                    if (status == (int)EnumVideoStatus.Temporary ||
                        status == (int)EnumVideoStatus.Return ||
                        status == (int)EnumVideoStatus.DoneVideo ||
                        status == (int)EnumVideoStatus.YoutubeUploading)
                    {
                        accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.VideoManager))
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().SearchVideo(accountName, zoneVideoId, playlistId, keyword,
                                                   videoStatus, videoSortOrder, fromDate, toDate, mode, pageIndex, pageSize,
                                                   ref totalRow);
                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_video_public")]
        public HttpResponseMessage SearchVideoPublic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var playlistId = Utility.ConvertToInt(body.Get("playlistId"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                //fix =1
                videoStatus = EnumVideoStatus.Published;
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy HH:mm");                
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("toDate"), "dd/MM/yyyy HH:mm");
                var templateType = Utility.ConvertToInt(body.Get("templateType"));
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApproveVideo))
                {
                    if (status == (int)EnumVideoStatus.Temporary ||
                        status == (int)EnumVideoStatus.Return ||
                        status == (int)EnumVideoStatus.DoneVideo ||
                        status == (int)EnumVideoStatus.YoutubeUploading)
                    {
                        accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.VideoManager))
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().SearchVideo(accountName, zoneVideoId, playlistId, keyword,
                                                   videoStatus, videoSortOrder, fromDate, toDate, mode, pageIndex, pageSize,
                                                   ref totalRow);
                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_video_single")]
        public HttpResponseMessage SearchVideoSingle(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var playlistId = Utility.ConvertToInt(body.Get("playlistId"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));                
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("toDate")) ? "" : body.Get("toDate") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                var templateType = Utility.ConvertToInt(body.Get("templateType"));
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApproveVideo))
                {
                    if (status == (int)EnumVideoStatus.Temporary ||
                        status == (int)EnumVideoStatus.Return ||
                        status == (int)EnumVideoStatus.DoneVideo ||
                        status == (int)EnumVideoStatus.YoutubeUploading)
                    {
                        accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.VideoManager))
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().SearchVideoSingle (accountName, zoneVideoId, playlistId, keyword,
                                                   videoStatus, videoSortOrder, fromDate, toDate, mode, pageIndex, pageSize,
                                                   ref totalRow);
                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_video_by_parent_id")]
        public HttpResponseMessage ListVideoByParentId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), -1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var totalRow = 0;
                var data = new MediaServices().ListVideoByParentId(parentId, videoStatus, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("suggest_video_playlist")]
        public HttpResponseMessage SuggestVideoPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var keyword = body.Get("keyword");
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);
                var sortOrder = Utility.ConvertToInt(body.Get("sortOrder"), 0);
                var createdDateFrom = Utility.ConvertToDateTimeByFormat(body.Get("created_date_from"), "dd/MM/yyyy");
                var created_date_to = body.Get("created_date_to");
                if (!string.IsNullOrEmpty(created_date_to))
                    created_date_to = created_date_to + " 23:59:59";

                var createdDateTo = Utility.ConvertToDateTimeByFormat(created_date_to, "dd/MM/yyyy HH:mm:ss");

                var distributionDateFrom = Utility.ConvertToDateTimeByFormat(body.Get("distribution_date_from"), "dd/MM/yyyy");
                var distribution_date_to = body.Get("distribution_date_to");
                if (!string.IsNullOrEmpty(distribution_date_to))
                    distribution_date_to = distribution_date_to + " 23:59:59";

                var distributionDateTo = Utility.ConvertToDateTimeByFormat(distribution_date_to, "dd/MM/yyyy HH:mm:ss");
                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("num"), 10);
                var totalRow = 0;
                var result = new MediaServices().SearchPlayList("", zoneId, (EnumPlayListStatus)status, keyword, (EnumPlayListMode)mode,
                                                          (EnumPlayListSort)sortOrder, createdDateFrom, createdDateTo, distributionDateFrom, distributionDateTo, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, PlayList = result }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("video_tag_suggestion")]
        public HttpResponseMessage GetVideoTagSuggestion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("query");

                var responseData = new WcfActionResponse<dynamic>();

                var arrText = new List<string>();
                var arrValue = new List<string>();
                var arrUrl = new List<string>();

                //link wcf dang bi hong http://research.channelvn.net/Action.svc
                var externalResult = new string[] { };//ResearchTagServices.AutoSuggestion(keyword);

                if (externalResult != null && externalResult.Length > 0)
                {
                    foreach (var tagItem in externalResult)
                    {
                        arrText.Add(tagItem);
                        arrValue.Add("-1");
                        arrUrl.Add(tagItem);
                    }
                }
                else
                {
                    const int pageSize = 10;
                    var totalRow = 0;
                    var cmsResult = new MediaServices().SearchVideoTag(keyword, -1, EnumVideoTagMode.AllMode,
                                                                 EnumVideoTagStatus.Actived,
                                                                 EnumVideoTagSort.VideoCountDesc, false, 1, pageSize,
                                                                 ref totalRow);
                    if (cmsResult != null && cmsResult.Count > 0)
                    {
                        foreach (var tagItem in cmsResult)
                        {
                            arrText.Add(tagItem.Name);
                            arrValue.Add("-1");
                            arrUrl.Add(tagItem.Url);
                        }

                        //responseData.Data = cmsResult;
                        //responseData.Success = true;
                        //responseData.Message = "Success.";
                    }
                }
                var objectResponse = new
                {
                    query = keyword,
                    suggestions = arrText,
                    data = arrValue,
                    url = arrUrl
                };

                responseData.Data = objectResponse;
                responseData.Success = true;
                responseData.Message = "Success.";

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_video_for_embed")]
        public HttpResponseMessage SearchVideoForEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = "";
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var playlistId = Utility.ConvertToInt(body.Get("playlistId"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("order"));
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("toDate")) ? "" : body.Get("toDate") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                var templateType = Utility.ConvertToInt(body.Get("templateType"));
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);

                var totalRow = 0;
                var data = new MediaServices().SearchVideo(accountName, zoneVideoId, playlistId, keyword,
                                                   videoStatus, videoSortOrder, fromDate, toDate, mode, pageIndex, pageSize,
                                                   ref totalRow);
                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        
        #endregion

        #region Gets
        [HttpPost, Route("get_detail_video_for_edit")]
        public HttpResponseMessage GetDetailVideoForEdit(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var videoId = Utility.ConvertToInt(body.Get("video_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetDetailVideoForEdit(videoId);
                return this.ToJson(WcfActionResponse<VideoDetailForEdit>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_detail_video_by_keyvideo")]
        public HttpResponseMessage GetDetailVideoByKeyVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyVideo = body.Get("key_video") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetDetailVideoByKeyvideo(keyVideo);
                return this.ToJson(WcfActionResponse<VideoEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_video")]
        public HttpResponseMessage CountVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var status = body.Get("status");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ApproveVideo))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.VideoManager))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var data = new MediaServices().CountVideo(username, status);
                return this.ToJson(WcfActionResponse<List<CountStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_video_single")]
        public HttpResponseMessage CountVideoSingle(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var status = body.Get("status");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ApproveVideo))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.VideoManager))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var data = new MediaServices().CountVideoSingle(username, status);
                return this.ToJson(WcfActionResponse<List<CountStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #region Thong Ke
        //luot view theo chuyen muc
        [HttpPost, Route("get_view_by_cateids")]
        public HttpResponseMessage GetViewByCateids(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var arrCateIdList = body.Get("cate_list") ?? string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from_date"), formats);
                var to = body.Get("to_date") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to_date");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                
                //int[] cateId_list = NewtonJson.Deserialize<int[]>(arrCateIdList.ToString());
                if (string.IsNullOrEmpty(arrCateIdList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param cate_list not empty."));
                }
                var cateId_list = arrCateIdList.Split(';');

                int multiply = 1;
                if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                {
                    try
                    {
                        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                    }
                    catch (Exception) { }
                }

                List<dynamic> cateVideoViewResult = new List<dynamic>();

                foreach (var item in cateId_list)
                {
                    dynamic viewData = new
                    {
                        CateId = Convert.ToInt32(item),
                        ViewCount = get_totalView_from_videoCate(Convert.ToInt32(item), fromDate, toDate) * multiply
                    };
                    cateVideoViewResult.Add(viewData);
                }

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(cateVideoViewResult, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private decimal get_totalView_from_videoCate(int cateId, DateTime dtFromDate, DateTime dtToDate)
        {
            decimal totalView = 0;
            try {                               
                int itemPerPage = 50;

                #region get current zone view
                int totalRow = 0;
                var childZone = new MediaServices().GetVideoZoneListByParentId(cateId, 0);
                List<string> lstZoneIds = new List<string>();
                if (cateId > 0)
                {
                    lstZoneIds.Add(cateId.ToString());
                    if (childZone.Count > 0)
                    {
                        foreach (var item in childZone)
                            lstZoneIds.Add(item.Id.ToString());
                    }
                }

                var videoList = new MediaServices().SearchVideoKeyByZone(dtFromDate, dtToDate, "", lstZoneIds, 0, "", 1, 1, itemPerPage, ref totalRow).ToList().Where(v => v.DistributionDate <= DateTime.Now).ToList();

                int totalPage = 1;
                if ((totalRow % itemPerPage) != 0)
                    totalPage = (totalRow / itemPerPage) + 1;
                else
                    totalPage = totalRow / itemPerPage;
                
                var nameSpace = string.IsNullOrEmpty(WcfMessageHeader.Current.Namespace) ? "" : WcfMessageHeader.Current.Namespace;
                if (nameSpace == "TTVH")
                {
                    nameSpace = "thethaovanhoa";
                }

                var isUseAdtechData = CmsChannelConfiguration.GetAppSetting("AdtechUseViewVideoStatisticData");

                for (int i = 1; i < totalPage + 1; i++)
                {
                    if (i != 1)
                        videoList = new MediaServices().SearchVideoKeyByZone(dtToDate, dtToDate, "", lstZoneIds, 0, "", 1, i, itemPerPage, ref totalRow).ToList().Where(v => v.DistributionDate <= DateTime.Now).ToList();

                    if (videoList!=null && videoList.Count > 0)
                    {                                                
                        if (isUseAdtechData == "1")
                        {
                            //cách 2 lấy trực tiếp từ api của admicro
                            string[] arrVideoIdList = videoList.Select(v => CMS.Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + v.FileName)).ToArray();
                            var listView = AdtechServiceForViewVideo.GetViewVideo(string.Join(",", arrVideoIdList), dtFromDate, dtToDate);
                            if (listView.videos != null && listView.videos.Count > 0)
                            {
                                totalView += listView.videos.Sum(vt => vt.play);
                            }
                        }
                        else if (isUseAdtechData == "2")
                        {
                            //cách 3 lấy chỗ api .netcore
                            string[] arrVideoIdList = videoList.Select(v => Common.Crypton.Md5Encrypt(nameSpace.ToLower() + "/" + v.FileName)).ToArray();
                            var listView = ViewsNetCoreServices.GetViewVideo(string.Join(",", arrVideoIdList), dtFromDate, dtToDate, nameSpace);
                            if (listView.videos != null && listView.videos.Count > 0)
                            {
                                totalView += listView.videos.Sum(vt => vt.play);
                            }                           
                        }
                        else
                        {
                            //cách 1 lấy từ api Hiếu   
                            string strWcfServiceMethodUrl = "http://vscc1.hosting.vcmedia.vn/Info.svc/TrackingVieo";                     
                            string[] arrVideoIdList = videoList.Select(v => v.KeyVideo.ToString()).ToArray();
                            List<TrackingMediaInfo> listVideo = VideoInfoServices.GetVideoTracking(strWcfServiceMethodUrl, dtFromDate, dtToDate, arrVideoIdList);
                            totalView += listVideo.Sum(vt => vt.play);
                        }
                    }
                }

                #endregion                
            }
            catch
            {
                return totalView;
            }
            return totalView;
        }

        //Thong ke san luong
        [HttpPost, Route("get_production_statistic")]
        public HttpResponseMessage GetProductionStatistic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var arrCateIdList = body.Get("cate_list") ?? string.Empty;
                var strUserNameList = body.Get("username_list") ?? string.Empty;
                var username = WcfMessageHeader.Current.ClientUsername;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from_date"), formats);
                var to = body.Get("to_date") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to_date");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                if (string.IsNullOrEmpty(arrCateIdList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param cate_list not empty"));
                }
                var cateId_list = arrCateIdList.Split(';');

                if (string.IsNullOrEmpty(strUserNameList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param username_list not empty"));
                }
                var username_list = strUserNameList.Split(';');

                List<VideoProductionStatistic> data = new List<VideoProductionStatistic>();

                foreach (string userName in username_list)
                {
                    VideoProductionStatistic currentUserData = new VideoProductionStatistic();
                    currentUserData.UserName = userName;
                    foreach (var cateId in cateId_list)
                    {
                        int totalVideo = get_video_amount_in_cate(Convert.ToInt32(cateId), userName, fromDate, toDate);
                        currentUserData.ListVideoCount.Add(totalVideo);
                    }
                    data.Add(currentUserData);
                }

                //if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                //{
                //    int multiply = 1;
                //    try
                //    {
                //        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                //    }
                //    catch (Exception) { }

                //    foreach (var item in data)
                //    {
                //        item.ListVideoCount = item.ListVideoCount.Select(t => t * multiply).ToList();
                //    }
                //}

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private int get_video_amount_in_cate(int cateId, string userName, DateTime fromDate, DateTime toDate)
        {
            int totalRow = 0;
            int totalVideo = 0;

            var childZone = new MediaServices().GetVideoZoneListByParentId(cateId, (EnumZoneVideoStatus)0);
            List<string> lstZoneIds = new List<string>();
            if (cateId > 0)
            {
                lstZoneIds.Add(cateId.ToString());
                if (childZone.Count > 0)
                {
                    foreach (var item in childZone)
                        lstZoneIds.Add(item.Id.ToString());
                }
            }

            List<VideoSearchEntity> listVideo = new MediaServices().SearchVideoKeyByZone(fromDate, toDate, userName, lstZoneIds, 0, "", 1, 1, 10, ref totalRow).ToList().Where(v => v.DistributionDate <= toDate && v.DistributionDate >= fromDate).ToList();

            //List<VideoEntity> listVideo = MediaServices.GetListMyVideo(userName, 1, int.MaxValue, "", 1, 1, cateId, 0, ref totalRow)
            //    .Where(v => v.DistributionDate <= toDate && v.DistributionDate >= fromDate).ToList();
            totalVideo += totalRow;

            //List<ZoneVideoEntity> listChildZone = MediaServices.GetVideoZoneListByParentId(cateId, EnumZoneVideoStatus.Actived);
            //foreach (var item in listChildZone)
            //{
            //    totalVideo += get_video_amount_in_cate(item.CatId, userName, fromDate, toDate);
            //}
            return totalVideo;
        }

        //Luot xem video
        [HttpPost, Route("export_video_publish")]
        public HttpResponseMessage ExportVideoPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneIds = body.Get("ZoneIds");
                var createdBy = body.Get("CreatedBy");
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("FromDate"), formats);
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                
                var pageIndex = Utility.ConvertToInt(body.Get("Page"));
                var pageSize = Utility.ConvertToInt(body.Get("Num"));

                var totalRow = 0;
                var data = new MediaServices().ExportVideoPublish(zoneIds, createdBy, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                if (WcfExtensions.WcfMessageHeader.Current.Namespace.Equals("BaoDanSinh"))
                {
                    int multiply = 1;
                    try
                    {
                        multiply = Convert.ToInt32(CmsChannelConfiguration.GetAppSetting("StatisticMultiply"));
                    }
                    catch (Exception) { }

                    foreach (var item in data)
                    {
                        item.Views = item.Views * multiply;
                    }
                }

                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #endregion

        #region ZoneVideo        

        [HttpPost, Route("save_zone_video")]
        public HttpResponseMessage SaveZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var parentId = Utility.ConvertToInt(body.Get("parentId"));
                var name = body.Get("name") ?? string.Empty;
                var url = Utility.UnicodeToKoDauAndGach(name);
                var order = Utility.ConvertToInt(body.Get("order"));
                var listVideoTagId = body.Get("listVideoTagId") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("status"), (int)EnumZoneVideoStatus.Actived);

                var avatar = body.Get("avatar") ?? string.Empty;
                var avatarcover = body.Get("avatarcover") ?? string.Empty;
                var zones = body.Get("zones") ?? string.Empty;
                var showOnHome = Utility.ConvertToBoolean(body.Get("showOnHome"));
                var createdDate = DateTime.Now;
                var logo = body.Get("logo") ?? string.Empty;
                var metaAvatar = body.Get("metaavatar") ?? string.Empty;

                var catId = Utility.ConvertToInt(body.Get("CatId"));
                var mode = Utility.ConvertToInt(body.Get("Mode"));
                var description = body.Get("Description") ?? string.Empty;

                var invisibled = Utility.ConvertToBoolean(body.Get("invisibled"), false);

                var zoneVideo = new ZoneVideoEntity
                {
                    Id = id,
                    ParentId = parentId,
                    Name = name,
                    Url = url,
                    Order = order,
                    CreatedDate = createdDate,
                    Status = status,
                    Avatar = avatar,
                    AvatarCover = avatarcover,
                    ZoneRelation = zones,
                    ModifiedDate = DateTime.Now,
                    ShowOnHome = showOnHome,
                    Logo = logo,
                    MetaAvatar=metaAvatar,
                    CatId= catId,
                    Mode=mode,
                    Description=description,
                    Invisibled= invisibled
                };

                WcfActionResponse result;
                if (id > 0)
                    result = new MediaServices().UpdateVideoZone(zoneVideo, listVideoTagId);
                else
                {
                    var zoneVideoId = 0;
                    result = new MediaServices().InsertVideoZone(zoneVideo, listVideoTagId, ref zoneVideoId);
                    result.Data = zoneVideoId.ToString();
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_list_zone_video")]
        public HttpResponseMessage GetListZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneVideoId = -1;
                var zoneId = body.Get("ParentId");
                if (string.IsNullOrEmpty(zoneId) || !int.TryParse(zoneId.ToString(), out parentZoneVideoId))
                {
                    parentZoneVideoId = -1;
                }
                var statusZoneVideo = 0;
                var status = body.Get("Status");
                if (string.IsNullOrEmpty(status) || !int.TryParse(status.ToString(), out statusZoneVideo))
                {
                    statusZoneVideo = 0;
                }

                var data = new MediaServices().GetVideoZoneListByParentId(parentZoneVideoId, (EnumZoneVideoStatus)statusZoneVideo);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, ZoneVideos = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpGet, Route("get_zone_video_active")]
        public HttpResponseMessage GetListZoneVideoActive(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;         

                var data = new MediaServices().GetVideoZoneListByParentId(-1, EnumZoneVideoStatus.Actived);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_private_zone_video")]
        public HttpResponseMessage GetPrivateZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneId = -1;
                var param = body.Get("ParentId");
                if (string.IsNullOrEmpty(param) || !int.TryParse(param.ToString(), out parentZoneId))
                {
                    parentZoneId = -1;
                }
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var username = WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetPrivateZoneVideo(username, parentZoneId, status);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Zones = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_zone_video_up")]
        public HttpResponseMessage MoveZoneVideoUp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().MoveVideoZoneUp(id);

                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("move_zone_video_down")]
        public HttpResponseMessage MoveZoneVideoDown(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().MoveVideoZoneDown(id);

                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_zone_video")]
        public HttpResponseMessage DeleteZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().DeleteVideoZone(id);

                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_zone_video_detail")]
        public HttpResponseMessage GetVideoZoneDetailByVideoZoneId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("id"));

                var data = new MediaServices().GetVideoZoneDetailByVideoZoneId(id);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { ZoneVideo = data.ZoneVideo, VideoTags = data.VideoTags }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Video Tag
        [HttpPost, Route("addnew_video_tag")]
        public HttpResponseMessage AddNewVideoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tag = body.Get("tag");

                var tagId = 0;
                var result = new MediaServices().UpdateVideoTagWithTagName(tag, ref tagId);
                if (result.Success && tagId > 0)
                {
                    var data = new MediaServices().GetVideoTagById(tagId);
                    return this.ToJson(WcfActionResponse<VideoTagEntity>.CreateSuccessResponse(data, "Success"));
                }
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("search_video_tag")]
        public HttpResponseMessage SearchVideoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("keyword") ?? string.Empty;
                var parentId = Utility.ConvertToInt(body.Get("parentId"), -1);
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);
                var getTagHasVideoOnly = Utility.ConvertToBoolean(body.Get("getTagHasVideoOnly"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                var sortOrder = Utility.ConvertToInt(body.Get("sortOrder"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("num"), 12);
                var totalRow = 0;

                var data = new MediaServices().SearchVideoTag(keyword, parentId, (EnumVideoTagMode)mode, (EnumVideoTagStatus)status,
                                              (EnumVideoTagSort)sortOrder, getTagHasVideoOnly, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, VideoTag = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_detail_video_tag")]
        public HttpResponseMessage GetVideoTagById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("id"));

                var data = new MediaServices().GetVideoTagById(id);

                return this.ToJson(WcfActionResponse<VideoTagEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_video_tag_mode")]
        public HttpResponseMessage UpdateVideoTagMode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("id"));
                var mode = Utility.ConvertToInt(body.Get("mode"), 0);

                var result = new MediaServices().UpdateVideoTagMode(id, mode == 1);

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_video_tag")]
        public HttpResponseMessage UpdateVideoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("id"));
                var avatar = body.Get("avatar") ?? string.Empty;
                var mode = Utility.ConvertToInt(body.Get("mode"), 0);
                var status = Utility.ConvertToInt(body.Get("status"), 0);
                var parentId = Utility.ConvertToInt(body.Get("parentId"), 0);

                var description = body.Get("description") ?? string.Empty;
                var name = body.Get("name") ?? string.Empty;

                VideoTagEntity tag = new VideoTagEntity
                {
                    Avatar = avatar,
                    CreatedBy = username,
                    CreatedDate = DateTime.Now,
                    Description = description,
                    Id = id,
                    IsHotTag = mode == 1,
                    Name = name,
                    ParentId = parentId,
                    Status = status
                };

                WcfActionResponse result;
                if (id > 0)
                    result = new MediaServices().UpdateVideoTag(tag);
                else
                {
                    var tag_id = 0;
                    result = new MediaServices().InsertVideoTag(tag, ref tag_id);
                    result.Data = tag_id.ToString();
                }

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region VideoFromYoutube

        [HttpPost, Route("search_video_youtube")]
        public HttpResponseMessage SearchVideoYoutube(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneVideoId"));
                var status = Utility.ConvertToInt(body.Get("status"));
                EnumVideoFromYoutubeStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoFromYoutubeStatus.AllStatus;
                }
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var fromDate = Utility.ConvertToDateTime(body.Get("fromDate"));
                var toDate = Utility.ConvertToDateTime(body.Get("toDate"));

                var totalRow = 0;
                var data = new MediaServices().SearchVideoFromYoutube(username, keyword, videoStatus, zoneVideoId, -1, fromDate,
                                                  toDate, pageIndex, pageSize, ref totalRow);
                return this.ToJson<WcfActionResponse<dynamic>>(
                        WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion        

        #region Box video embed
        [HttpPost, Route("get_video_embed")]
        public HttpResponseMessage GetVideoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));

                var data = new MediaServices().GetListVideoEmbed(zoneId, type);
                return this.ToJson(WcfActionResponse<List<BoxVideoEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_video_embed")]
        public HttpResponseMessage UpdateVideoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));
                var listVideoId = body.Get("listVideoId") ?? string.Empty;
                var listembed = body.Get("listembed") ?? string.Empty;

                var result = new MediaServices().UpdateVideoEmbed(listVideoId, zoneId, type);

                //var esObject = new List<VideoEmbedEpl>();
                var listVideoEmbed = new List<BoxVideoEmbedEplEntity>();
                int c = 0;
                Dictionary<int, int> listSortOrder = new Dictionary<int, int>();
                if (listVideoId != "")
                {
                    var arrVideo = listVideoId.Split(',');
                    for (int i = 0; i < arrVideo.Count(); i++)
                    {
                        if (Utility.ConvertToLong(arrVideo[i]) == 0)
                        {
                            c++;
                            listSortOrder.Add(c, i + 1);
                        }
                        //var currentObject = new VideoEmbedEpl();
                        //currentObject.VideoId = Utility.ConvertToLong(arrVideo[i]);
                        //currentObject.Embed = string.Empty;
                        //currentObject.Avatar = string.Empty;
                        //currentObject.Name = string.Empty;
                        //currentObject.Type = type;
                        //currentObject.ZoneId = zoneId;
                        //currentObject.SortOrder = i + 1;
                        //esObject.Add(currentObject);
                    }
                }

                if (listembed != "" && !string.IsNullOrEmpty(listembed))
                {
                    c = 0;
                    var arr = listembed.Split(',');
                    var VideoStreamEmbed_Site = CmsChannelConfiguration.GetAppSetting("VideoStreamEmbed_Site");
                    foreach (var item in arr)
                    {
                        try
                        {
                            c++;
                            var arrItem = item.Split('|');
                            var Url = "";
                            if (arrItem[0].StartsWith("http")) Url = arrItem[0];
                            else
                            {
                                Url = arrItem[0].Substring(arrItem[0].IndexOf("src") + 5);
                                Url = Url.Substring(0, Url.IndexOf("\""));
                            }
                            var _url1 = Url.Substring(0, Url.IndexOf("_site") + 6);
                            var _url2 = Url.Substring(Url.IndexOf("_site") + 6);
                            _url2 = _url2.Substring(_url2.IndexOf("&"));

                            Url = string.Format("{0}{1}{2}", _url1, VideoStreamEmbed_Site, _url2);
                            var sort = listSortOrder.Where(k => k.Key == c).FirstOrDefault();
                            listVideoEmbed.Add(new BoxVideoEmbedEplEntity
                            {
                                Avatar = arrItem[2],
                                Embed = Url,
                                Type = type,
                                ZoneId = zoneId,
                                Name = arrItem[1],
                                SortOrder = sort.Key > 0 ? sort.Value : c
                            });
                        }
                        catch (Exception)
                        {

                        }
                    }
                    if (listVideoEmbed.Count > 0)
                        new MediaServices().UpdateVideoEmbed_Epl(listVideoEmbed);
                }
                //push cd -->Hoat bo action: updateboxvideoembed
                //try
                //{
                //    if (result.Success && esObject != null && esObject.Count > 0)
                //    {
                //        foreach (dynamic item in esObject)
                //        {
                //            if (item.VideoId == 0)
                //            {
                //                var embed = listVideoEmbed.Where(x => x.SortOrder == item.SortOrder).FirstOrDefault();
                //                item.Avatar = embed.Avatar;
                //                item.Embed = embed.Embed;
                //                item.Type = embed.Type;
                //                item.ZoneId = embed.ZoneId;
                //                item.Name = embed.Name;
                //            }
                //        }
                //        var jsonKey = "{\"ZoneId\":" + zoneId + ", \"Type\":" + type + "}";
                //        var objectJson = NewtonJson.Serialize(esObject);
                //        ContentDeliveryServices.PushToDataCD(objectJson, "updateboxvideoembed", string.Empty, jsonKey);                        
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                //}

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }
        #endregion

        #region Box play embed VideoBoxMuc=9
        [HttpPost, Route("get_play_embed")]
        public HttpResponseMessage GetPlayEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var type = Utility.ConvertToInt(body.Get("Type"));

                var data = new MediaServices().GetListPlayEmbed(zoneId, type);
                return this.ToJson(WcfActionResponse<List<BoxPlayEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_play_embed")]
        public HttpResponseMessage UpdatePlayEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var name = body.Get("Name") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var type = Utility.ConvertToInt(body.Get("Type"));
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), 1);
                var priority = Utility.ConvertToInt(body.Get("Priority"), 1);
                var listObject = body.Get("ListObject") ?? string.Empty;

                var result = new MediaServices().UpdatePlayEmbed(listObject, zoneId, type, name, displayStyle, priority);

                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region VideoLabel

        [HttpPost, Route("save_video_label")]
        public HttpResponseMessage SaveVideoLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameLableNotEmpty]));
                }
                var url = Utility.UnicodeToKoDauAndGach(name);
                var avatar = body.Get("Avatar") ?? string.Empty;
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var isSystem = Utility.ConvertToBoolean(body.Get("IsSystem"), true);

                var videoLabel = new VideoLabelEntity
                {
                    Id = id,
                    ParentId = parentId,
                    Name = name,
                    Url = url,
                    Avatar = avatar,
                    Priority = priority,
                    IsSystem = isSystem,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    CreatedBy = accountName
                };

                WcfActionResponse result;
                if (id > 0)
                    result = new MediaServices().UpdateVideoLabel(videoLabel);
                else
                {
                    var labelId = 0;
                    result = new MediaServices().InsertVideoLabel(videoLabel, ref labelId);
                    result.Data = labelId.ToString();
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_video_label")]
        public HttpResponseMessage DeleteVideoLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new MediaServices().DeleteVideoLabel(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_video_label_by_id")]
        public HttpResponseMessage GetVideoLabelById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new MediaServices().GetVideoLabelById(id);

                return this.ToJson(WcfActionResponse<VideoLabelEntity>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_list_video_label")]
        public HttpResponseMessage GetListVideoLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), -1);
                var isSystem = Utility.ConvertToInt(body.Get("IsSystem"), -1);

                var data = new MediaServices().GetVideoLabelListByParentId(parentId, isSystem);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, VideoLabels = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_video_label_up")]
        public HttpResponseMessage MoveVideoLabelUp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new MediaServices().MoveVideoLabelUp(id);

                return this.ToJson<WcfActionResponse>(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_video_label_down")]
        public HttpResponseMessage MoveVideoLabelDown(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new MediaServices().MoveVideoLabelDown(id);

                return this.ToJson<WcfActionResponse>(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region chương trình nổi bật VideoHighLight = 8
        [HttpPost, Route("get_video_highlight_embed")]
        public HttpResponseMessage GetVideoHighlightEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var data = new MediaServices().GetVideoHighlightEmbed();
                return this.ToJson(WcfActionResponse<List<BoxVideoHighlightEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_video_highlight_embed")]
        public HttpResponseMessage UpdateVideoHighlightEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var listObject = body.Get("ListObject") ?? string.Empty;

                var result = new MediaServices().UpdateVideoHighlightEmbed(listObject);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Chương trình hot trang home VideoProgramHotHome = 10
        //and Chương trình hot chuyên mục VideoProgramHotMuc = 11

        [HttpPost, Route("get_program_hothome_embed")]
        public HttpResponseMessage GetProgramHotHomeEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);

                var data = new MediaServices().GetProgramHotHomeEmbed(zoneId);
                return this.ToJson(WcfActionResponse<List<BoxProgramHotHomeEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_program_hothome_embed")]
        public HttpResponseMessage UpdateProgramHotHomeEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var name = body.Get("Name") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);

                var listObject = body.Get("ListObject") ?? string.Empty;

                var result = new MediaServices().UpdateProgramHotHomeEmbed(name, zoneId, listObject);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Suggest video vtv

        [HttpPost, Route("get_list_zone_video_vtv")]
        public HttpResponseMessage GetListZoneVideoVTV(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var data = VtvServices.GetVideoZoneListVTV();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, ZoneVideos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_video_vtv")]
        public HttpResponseMessage SearchVideoVTV(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                if (!CmsChannelConfiguration.GetAppSettingInBoolean("IsOpenApiSearchVideoVTV"))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotifyApiNotSupport]));
                }
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("Keyword");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneVideoId"), -1);

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("FromDate"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("ToDate"), "dd/MM/yyyy HH:mm");

                var totalRow = 0;
                var data = new MediaServices().SearchVideoVTV(zoneId, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<SearchVideoVTVResponse>.CreateSuccessResponse(new SearchVideoVTVResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("callback_video_crawler")]
        public HttpResponseMessage UpdateVideoVtvDone(HttpRequestMessage request)
        {
            //try
            //{
            //    var body = request.Content.ReadAsFormDataAsync().Result;

            //    var videoId = Utility.ConvertToInt(body.Get("Id"));
            //    var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            //    var status = Utility.ConvertToInt(body.Get("Status"));
            //    if (status == 1)
            //    {
            //        var otp = body.Get("Otp");
            //        var responseData = new SecurityServices().IsValidOTP(otp, accountName);
            //        if (!responseData.Success) return this.ToJson(responseData);
            //    }
            //    var name = body.Get("Name");
            //    var isExternalVideo = Utility.ConvertToBoolean(body.Get("IsExternalVideo"));
            //    var IsRemoveLogo = Utility.ConvertToInt(body.Get("IsRemoveLogo"));
            //    var file = body.Get("FileName");
            //    var keyVideo = body.Get("KeyVideo");
            //    var source = body.Get("Source");
            //    var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
            //    var type = Utility.ConvertToInt(body.Get("Type"), 0);
            //    var trailerUrl = body.Get("TrailerUrl");
            //    var metaAvatar = body.Get("MetaAvatar");

            //    if (string.IsNullOrEmpty(keyVideo) && !string.IsNullOrEmpty(file))
            //    {
            //        var path = file.Replace(System.IO.Path.GetFileName(file), "");
            //        var filename = System.IO.Path.GetFileNameWithoutExtension(file);
            //        var extension = System.IO.Path.GetExtension(file);
            //        keyVideo = VideoStorageServices.VideoGetMediaKey(accountName, path, filename, extension);
            //    }
            //    if (string.IsNullOrEmpty(name))
            //    {
            //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Tên video không được để trống."));
            //    }
            //    else
            //    {
            //        var unsignName = Utility.ConvertTextToUnsignName(name);
            //        var url = Utility.ConvertTextToLink(unsignName);
            //        var noAdv = Utility.ConvertToInt(body.Get("NoAdv"));
            //        var video = new VideoEntity
            //        {
            //            Id = isExternalVideo ? 0 : videoId,
            //            ZoneId = Utility.ConvertToInt(body.Get("ZoneId")),
            //            Name = name,
            //            UnsignName = unsignName,
            //            Description = body.Get("Desc"),
            //            HtmlCode = body.Get("HtmlCode"),
            //            Avatar = body.Get("Avatar"),
            //            KeyVideo = keyVideo,
            //            Pname = string.IsNullOrEmpty(VideoStorageServices.CustomPname) ? body.Get("PName") : VideoStorageServices.CustomPname,
            //            Status = 7, //status,
            //            NewsId = Utility.ConvertToLong(body.Get("NewsId")),
            //            Views = Utility.ConvertToInt(body.Get("Views")),
            //            Mode = Utility.ConvertToInt(body.Get("Mode")),
            //            DistributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm"),
            //            Tags = body.Get("Tags"),
            //            CreatedBy = accountName,
            //            LastModifiedBy = accountName,
            //            Url = url,
            //            Source = source,
            //            VideoRelation = body.Get("VideoRelation"),
            //            FileName = file,
            //            Duration = body.Get("Duration"),
            //            Size = body.Get("Size"),
            //            Capacity = Utility.ConvertToInt(body.Get("Capacity")),
            //            AllowAd = noAdv != 1,
            //            IsRemoveLogo = IsRemoveLogo == 1,
            //            AvatarShareFacebook = body.Get("AvatarShareFacebook"),
            //            OriginalId = Utility.ConvertToInt(body.Get("OriginalId"), 0),
            //            Author = body.Get("Author"),
            //            ParentId = parentId,
            //            HashId = body.Get("HashId"),
            //            Type = type,
            //            TrailerUrl = trailerUrl,
            //            MetaAvatar = metaAvatar
            //        };
            //        var tagIdList = body.Get("TagIdList");
            //        var playlistIdList = body.Get("PlaylistIdList");
            //        var cateName = body.Get("CateName");
            //        var zoneIdList = body.Get("ZoneIdList");
            //        var channelIdList = body.Get("ChannelIdList");

            //        var result = new MediaServices().SaveVideoV4(video, tagIdList, zoneIdList, playlistIdList, channelIdList);
            //        if (result.Success)
            //        {
            //            try
            //            {
            //                if (isExternalVideo)
            //                {
            //                    new ExternalVideoServices().RecieveExternalVideo(GetQueryString.GetPost("externalVideoId", 0));
            //                }

            //                var primaryZoneTag = "";
            //                var tagForPrimaryZone = new MediaServices().GetVideoTagByZoneVideoId(video.ZoneId);
            //                if (tagForPrimaryZone != null)
            //                {
            //                    primaryZoneTag = tagForPrimaryZone.Aggregate(primaryZoneTag, (current, videoTagEntity) => current + (";" + videoTagEntity.Name));
            //                    if (!string.IsNullOrEmpty(primaryZoneTag)) primaryZoneTag = primaryZoneTag.Remove(0, 1);
            //                }
            //                Logger.WriteLog(Logger.LogType.Info, string.Format("SyncVideo(KeyVideo={0}, Tag={1}, noAdv={2})", video.KeyVideo, primaryZoneTag, noAdv));

            //                VideoStorageServices.SyncVideoToVideoApi(accountName, video.Name, primaryZoneTag, video.ZoneId,
            //                                                         cateName,
            //                                                         zoneIdList, video.VideoRelation, noAdv, false, video.Status,
            //                                                         video.KeyVideo);
            //                if (!string.IsNullOrEmpty(video.Avatar))
            //                {
            //                    VideoStorageServices.UpdateThumbnail(accountName, keyVideo, video.Avatar);
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                Logger.WriteLog(Logger.LogType.Error, string.Format("SyncVideo Error:{0}", ex.Message));
            //            }
            //            try
            //            {
            //                VideoStorageServices.UpdateVideoLogo(accountName, video.KeyVideo, IsRemoveLogo == 0);
            //            }
            //            catch (Exception ex)
            //            {
            //                Logger.WriteLog(Logger.LogType.Error, string.Format("UpdateVideoLogo Error:{0}", ex.Message));
            //            }
            //        }

            //        result.Data = video.HtmlCode;

            //        return this.ToJson(result);
            //    }

            //    //return UpdateVideo(request);
            //}
            //catch (Exception ex)
            //{
            //    return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            //}

            return this.ToJson(WcfActionResponse.CreateSuccessResponse("Ok"));
        }

        #endregion        

        //#region Album
        //[HttpPost, Route("save_album")]
        //public HttpResponseMessage SaveAlbum(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var _accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
        //        var albumId = Utility.ConvertToInt(body.Get("Id"), 0);
        //        var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
        //        var name = body.Get("Name")?? "";
        //        var thumb = body.Get("Thumb")?? "";
        //        var status = Utility.ConvertToInt(body.Get("Status"), 1);
        //        var tagIdList = body.Get("Tags")?? "";
        //        var listOfPhotoId = body.Get("ListOfPhotoId")?? "";
        //        var listOfPhotoPublished = body.Get("ListOfPhotoPublished")?? "";

        //        var album = new AlbumEntity
        //        {
        //            Id = albumId,
        //            ZoneId = zoneId,
        //            Name = name,
        //            CreatedBy = _accountName,
        //            ThumbImage = thumb,
        //            Status = status
        //        };

        //        var responseData = new WcfActionResponse();
        //        //Cập nhật thông tin album, nếu chưa có thì tạo mới
        //        if (albumId > 0)
        //        {
        //            responseData = new MediaServices().UpdateAlbum(album, tagIdList);
        //            if (responseData.Success)
        //            {
        //                album.Id = Utility.ConvertToInt(responseData.Data);                        
        //            }
        //        }
        //        else
        //        {
        //            responseData = new MediaServices().InsertAlbum(album, tagIdList);
        //            if (responseData.Success)
        //            {
        //                album.Id = Utility.ConvertToInt(responseData.Data);                        
        //            }
        //        }
        //        //nếu có ảnh mới thì insert vào album
        //        var photopublished = new MediaServices().PublishPhoto(listOfPhotoId, album.Id, 0, album.ZoneId, _accountName);

        //        //kết hợp với ảnh cũ có sẵn trong album để trả ra 1 list ảnh đã theo thứ tự
        //        var i = 0;
        //        foreach (var photoPublishedEntity in photopublished)
        //        {
        //            listOfPhotoPublished = listOfPhotoPublished.Replace("[" + i + "]", photoPublishedEntity.Id + "");
        //            i++;
        //        }
        //        //cập nhật thứ tự vào DB.
        //        new MediaServices().UpdatePhotoPublishedPriority(listOfPhotoPublished);
        //        //lấy danh sách ảnh ra, so sánh nếu ảnh đã bị xóa thì xóa đi
        //        var realPhotoInAlbums = listOfPhotoPublished.Split(new char[] { ';' });
        //        int totalRows = 0;
        //        var photoInAlbums = new MediaServices().SearchPhotoPublishedInAlbum("", album.Id, 1, 1000, ref totalRows);
        //        string listOfRemovePhotoId = "";
        //        if (totalRows > 0)
        //        {
        //            foreach (var p in photoInAlbums)
        //            {

        //                bool isExits = false;
        //                if (realPhotoInAlbums.Contains(p.Id + ""))
        //                {
        //                    isExits = true;
        //                }
        //                if (!isExits)
        //                {
        //                    listOfRemovePhotoId += p.Id + ";";
        //                }
        //            }
        //            new MediaServices().DeleteListPhotoPublished(listOfRemovePhotoId);
        //        }

        //        return this.ToJson(responseData);
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}

        //[HttpPost, Route("search_album")]
        //public HttpResponseMessage SearchAlbum(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
        //        var keyword = body.Get("Keyword") ?? string.Empty;
        //        var status = Utility.ConvertToInt(body.Get("Status"), -1);
        //        var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
        //        var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 12);
        //        var createDateFrom = Utility.ConvertToDateTimeByFormat(body.Get("FromDate"),"dd/MM/yyyy HH:mm");
        //        var createDateTo = Utility.ConvertToDateTimeByFormat(body.Get("ToDate"), "dd/MM/yyyy HH:mm");
        //        var createdBy = body.Get("CreatedBy")??"";

        //        var totalRow = 0;
        //        var data = new MediaServices().SearchAlbum(keyword, zoneId, createDateFrom, createDateTo, createdBy, pageIndex, pageSize, ref totalRow);

        //        return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Albums = data }, "Success"));
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}

        //[HttpPost, Route("get_album")]
        //public HttpResponseMessage EditAlbum(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var id = Utility.ConvertToInt(body.Get("Id"), 0);
        //        var result = new MediaServices().GetAlbumByAlbumId(id);
        //        var photos = new List<PhotoPublishedEntity>();
        //        int totalRows = 0;
        //        if (id > 0)
        //        {
        //            photos = new MediaServices().SearchPhotoPublishedInAlbum("", id, 1, 1000, ref totalRows);
        //        }

        //        return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { album=result, photos = new { totalRows=totalRows, data= photos } }, "Success"));
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}

        //[HttpPost, Route("delete_album")]
        //public HttpResponseMessage RemoveAlbum(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        var body = request.Content.ReadAsFormDataAsync().Result;

        //        var id = Utility.ConvertToInt(body.Get("Id"), 0);
        //        if (id <= 0)
        //        {
        //            return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found embedAlbumId."));
        //        }

        //        var responseData = new MediaServices().DeleteAlbum(id);

        //        return this.ToJson(responseData);
        //    }
        //    catch (Exception ex)
        //    {
        //        return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
        //    }
        //}

        //#endregion

        #region EmbedAlbum
        [HttpPost, Route("save_embed_album")]
        public HttpResponseMessage UpdateEmbedAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var eAlbum = body.Get("EmbedAlbum")?? "";
                var listEmbedAlbumDetail = body.Get("ListEmbedAlbumDetail")?? "";

                var embedAlbum = NewtonJson.Deserialize<EmbedAlbumEntity>(eAlbum);
                var listPhotos = NewtonJson.Deserialize<List<EmbedAlbumDetailEntity>>(listEmbedAlbumDetail);                               

                var responseData = new WcfActionResponse();
                if (embedAlbum != null)
                {
                    embedAlbum.CreatedDate = DateTime.Now;
                    embedAlbum.CreatedBy = accountName;
                    embedAlbum.LastModifiedDate = DateTime.Now;
                    embedAlbum.LastModifiedBy = accountName;

                    var embedAlbumForEditEntity = new EmbedAlbumForEditEntity()
                    {
                        EmbedAlbum = embedAlbum,
                        ListEmbedAlbumDetail = listPhotos
                    };

                    if (embedAlbum.Id > 0)
                    {
                        responseData = new MediaServices().UpdateEmbedAlbum(embedAlbumForEditEntity);
                    }
                    else
                    {
                        var alId = 0;
                        responseData.Data = alId.ToString();
                        responseData = new MediaServices().CreateEmbedAlbumReturnId(embedAlbumForEditEntity,ref alId);
                    }
                }                
                
                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_embed_album")]
        public HttpResponseMessage SearchEmbedAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var keyword = body.Get("Keyword") ?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("Type"), 1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 12);

                var totalRow = 0;
                var data = new MediaServices().SearchEmbedAlbum(keyword, type, zoneId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Albums = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_embed_album")]
        public HttpResponseMessage GetEmbedAlbumForEditByEmbedAlbumId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var result = new MediaServices().GetEmbedAlbumForEditByEmbedAlbumId(id, 100);
                
                return this.ToJson(WcfActionResponse<EmbedAlbumForEditEntity>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_embed_album")]
        public HttpResponseMessage RemoveEmbedAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
                if (id <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found embedAlbumId."));
                }
                
                var responseData = new MediaServices().DeleteEmbedAlbum(id);

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Video API Kinhhub

        [HttpPost, Route("get_list_video_api")]
        public HttpResponseMessage GetListVideoApi(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy HH:mm");
                var todate = Utility.ConvertToDateTimeByFormat(body.Get("To"), "dd/MM/yyyy HH:mm");
                var pageSize = Utility.ConvertToInt(body.Get("Num"), 100);
                if (fromDate < new DateTime(1980, 01, 01))
                {
                    fromDate = DateTime.Now.AddDays(-30);
                }
                if (todate < new DateTime(1980, 01, 01))
                {
                    todate = DateTime.Now;
                }
                string url = string.Format("https://api.kinghub.vn/video/api/v1/getVideoDataByDateNum?date={0},{1}&num={2}", fromDate.ToString("yyyy-MM-dd"), todate.ToString("yyyy-MM-dd"), pageSize);
                var res = CmsMappingUtility.GetWebRequestDataFromUrl(url, CmsMappingUtility.RequestMethod.Get);
                var data = NewtonJson.Deserialize<List<Models.Base.VideoApiEntity>>(res);
                int totalrow;
                if (data != null && data.Count > 0)
                {
                    totalrow = data.Count;                    
                    foreach (var item in data)
                    {
                        item.isClone = 0;
                        if (BoSearch.Base.Video.VideoDalFactory.CheckCloneVideoEPL(item.videoID.ToString()))
                        {
                            item.isClone = 1;
                        }
                    }
                }
                else
                {
                    totalrow = 0;
                }
                                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalrow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        
        [HttpPost, Route("insert_video_api")]
        public HttpResponseMessage InsertVideoApiById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var videoId = body.Get("VideoId")?? string.Empty;
                var image = body.Get("Image")?? "";
                var duration = body.Get("Duration")?? "";                
                var title = body.Get("Title")?? "";
                var file = body.Get("File")?? "";
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                var nameSpace = body.Get("NameSpace")?? "";
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),0);
                var zoneName = body.Get("ZoneName");
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (videoId == string.Empty)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Video Error: videoId = Empty."));
                }
                else
                {
                    var unsignName = Utility.ConvertTextToUnsignName(title);
                    var url = Utility.ConvertTextToLink(unsignName);
                    var originalUrl = Utility.ConvertTextToLink(unsignName);
                    var video = new VideoEntity
                    {            
                        Id=0,            
                        ZoneId = zoneId,
                        Name = title,
                        Avatar = image,
                        KeyVideo = videoId,                        
                        Url = url,
                        CreatedDate = DateTime.Now,
                        CreatedBy = accountName,
                        LastModifiedBy=accountName,
                        LastModifiedDate=DateTime.Now,
                        Duration = duration,
                        FileName = file,
                        Type=0,
                        ZoneName= zoneName,
                        OriginalId= Utility.ConvertToInt(videoId,0),
                        OriginalUrl= originalUrl,
                        Status= (int)EnumVideoStatus.WaitForPublish,
                        ParentId=0,
                        EditedBy= accountName,
                        EditedDate=DateTime.Now
                    };
                    var data = new MediaServices().SaveVideoV4(video, "", "", "", "", new List<string>());
                    //var data = new MediaServices().InsertVideoApi(video);
                    return this.ToJson(data);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region FileUpload

        [HttpPost, Route("save_fileupload")]
        public HttpResponseMessage SaveFileUpload(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),0);
                var title = body.Get("Title");
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Title not empty."));
                }                                
                var description = body.Get("Description") ?? string.Empty;
                var fileDownloadPath = body.Get("FileDownloadPath")??string.Empty;
                var filePath = body.Get("FilePath");
                var fileExt = body.Get("FileExt") ?? string.Empty;
                var fileSize = Utility.ConvertToInt(body.Get("FileSize"),0);
                var status = Utility.ConvertToInt(body.Get("Status"),0);

                var fileUpload = new FileUploadEntity
                {
                    Id=id,
                    ZoneId=zoneId,
                    Title= title,
                    Description=description,
                    FileDownloadPath=fileDownloadPath,
                    FilePath=filePath,
                    FileExt=fileExt,
                    FileSize=fileSize,
                    Status=status,
                    UploadedDate=DateTime.Now,
                    UploadedBy=accountName,
                    LastModifiedBy= accountName,
                    LastModifiedDate=DateTime.Now
                };

                var result = new WcfActionResponse();
                if (id <= 0)
                {
                    result = new MediaServices().InsertFileUpload(fileUpload);                    
                }
                else
                {
                    result = new MediaServices().UpdateFileUpload(fileUpload);
                }
                                
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_fileupload")]
        public HttpResponseMessage SearchFileUpload(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfMessageHeader.Current.ClientUsername;

                var keyword = body.Get("Keyword") ?? string.Empty;
                var filterMode = body.Get("FilterMode")?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var statusActive = EnumFileUploadStatus.AllStatus;
                if (status == 1)
                {
                    statusActive = EnumFileUploadStatus.Actived;
                }
                else if(status == 0)
                {
                    statusActive = EnumFileUploadStatus.Inactived;
                }
                var ext = body.Get("Ext");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                string createdBy = "";
                if (filterMode == "myfile")
                {
                    createdBy = accountName;
                }                           

                var totalRow = 0;
                var data = new MediaServices().SearchFileUpload(keyword, zoneId, ext, createdBy, statusActive, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, FileUploads = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_fileupload")]
        public HttpResponseMessage GetFileUpload(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("id"));

                var data = new MediaServices().GetFileUpload(id);

                return this.ToJson(WcfActionResponse<FileUploadEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete_fileupload")]
        public HttpResponseMessage DeleteFileUpload(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));               

                var result = new MediaServices().DeleteFileUpload(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_video_storage")]
        public HttpResponseMessage DeleteFileStorage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fileName = body.Get("filename");

                var result = VideoStorageServices.DeleteFile(fileName);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
