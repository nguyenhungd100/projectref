﻿using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Interview;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.NewsSocial;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.TemplateEmbed;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WcfMapping.ServiceCrawler;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.Kenh14.Entity;
using ChannelVN.MagazineContent.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/news/plugin")]
    public class NewsPluginController : ApiController
    {
        #region For Zone

        [HttpGet, Route("get_all_zone_with_tree_view")]
        public HttpResponseMessage GetAllZoneWithTreeView(HttpRequestMessage request)
        {
            try
            {
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetAllZoneWithTreeView(false);

                return this.ToJson(WcfActionResponse<List<ZoneWithSimpleFieldEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpGet, Route("get_all_zone_by_user_with_tree_view")]
        public HttpResponseMessage GetAllZoneByUserWithTreeView(HttpRequestMessage request)
        {
            try
            {
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetAllZoneByUserWithTreeView(username);

                return this.ToJson(WcfActionResponse<List<ZoneWithSimpleFieldEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region ZoneVideo
        [HttpPost, Route("get_private_zone_video")]
        public HttpResponseMessage GetPrivateZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneId = -1;
                var param = body.Get("ParentId");
                if (string.IsNullOrEmpty(param) || !int.TryParse(param.ToString(), out parentZoneId))
                {
                    parentZoneId = -1;
                }
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetPrivateZoneVideo(username, parentZoneId, status);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Zones = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region For Tag

        [HttpPost, Route("search_tag")]
        public HttpResponseMessage SearchTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isThread = Utility.ConvertToInt(body.Get("isThread"));
                var isHotTag = -1;
                int.TryParse(body.Get("isHotTag"), out isHotTag);
                var parentTagId = Utility.ConvertToInt(body.Get("parentTagId"));
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new TagServices().SearchTag(key, parentTagId, isThread, zoneId, 0, order, isHotTag, pageIndex, pageSize, ref totalRow, false);
                if (data != null && data.Count > 0)
                {
                    var linkFormatTag = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");
                    data.Select(s => { s.Url = string.Format(linkFormatTag, s.Url); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchTagResponse>.CreateSuccessResponse(new SearchTagResponse { TotalRow = totalRow, Tags = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("suggest_tag_in_db")]
        public HttpResponseMessage GetCleanTagSuggest(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var content = body.Get("content");

                content =
                new Regex(@"<div[^>]*type=""RelatedNews""[^>]*>(.*?)</div>",
                          RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).Replace(content,
                                                                                                             string
                                                                                                                 .Empty);
                content =
                    new Regex(@"</div>", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                        Replace(content, Environment.NewLine);
                content =
                    new Regex(@"</br>", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                        Replace(content, Environment.NewLine);
                content =
                    new Regex(@"</p>", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                        Replace(content, Environment.NewLine);
                content =
                    new Regex(@"<img.*?alt=[""'?](.+?)[""'?].*?>",
                              RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                        Replace(content, match => match.Groups.Count >= 2 ? match.Groups[1].Value : match.Value);
                content =
                    new Regex(@"<.*?>", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase).
                        Replace(content, string.Empty);

                var listSelectedTag = GetAllCleanTagInContentWithAllTagToken(content, 3, 6);
                var defaultTagForZone = new NewsServices().GetAllDefaultTagForZone();

                //Test Data
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 159011,
                //                              ZoneId = 1026,
                //                              ZoneName = "Thể thao",
                //                              ZoneUrl = "the-thao"
                //                          });
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 162982,
                //                              ZoneId = 1026,
                //                              ZoneName = "Thể thao",
                //                              ZoneUrl = "the-thao"
                //                          });
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 162982,
                //                              ZoneId = -1,
                //                              ZoneName = "",
                //                              ZoneUrl = "",
                //                              CustomUrl = "http://nld.com.vn"
                //                          });
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 39683,
                //                              ZoneId = -1,
                //                              ZoneName = "",
                //                              ZoneUrl = "",
                //                              CustomUrl = "http://nld.com.vn"
                //                          });
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 162982,
                //                              ZoneId = -2,
                //                              ZoneName = "",
                //                              ZoneUrl = "",
                //                              CustomUrl = "http://nld.com.vn/video.htm"
                //                          });
                //defaultTagForZone.Add(new ZoneDefaultTagEntity
                //                          {
                //                              TagId = 462982,
                //                              ZoneId = -2,
                //                              ZoneName = "",
                //                              ZoneUrl = "",
                //                              CustomUrl = "http://nld.com.vn/video.htm"
                //                          });
                //end Test Data

                List<TagSuggestEntity> tagSuggest = new List<TagSuggestEntity>();
                foreach (var tag in listSelectedTag)
                {
                    TagSuggestEntity tagSuggestEntity = new TagSuggestEntity();
                    tagSuggestEntity.Id = tag.Id;
                    tagSuggestEntity.Name = tag.Name;
                    tagSuggestEntity.NewsCount = tag.NewsCount;
                    var tagFormat = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");
                    if (tagFormat.Contains("{1}"))
                    {
                        tagSuggestEntity.Url = string.Format(tagFormat, Utility.UnicodeToKoDauAndGach(tag.Name), tag.Id);
                    }
                    else
                    {
                        tagSuggestEntity.Url = string.Format(tagFormat, Utility.UnicodeToKoDauAndGach(tag.Name));
                    }
                    tagSuggestEntity.ZoneUrl = "";
                    tagSuggestEntity.ZoneId = 0;
                    tagSuggestEntity.ZoneName = "";
                    foreach (var defaultTagEntity in defaultTagForZone)
                    {
                        if (defaultTagEntity.TagId == tag.Id)
                        {
                            tagSuggestEntity.ZoneUrl = string.Format(CmsChannelConfiguration.GetAppSetting("UrlFormatZone"),
                                                                     defaultTagEntity.ZoneUrl);
                            tagSuggestEntity.ZoneId = defaultTagEntity.ZoneId;
                            tagSuggestEntity.ZoneName = defaultTagEntity.ZoneName;

                            if (defaultTagEntity.ZoneId == -1 || defaultTagEntity.ZoneId == -2)
                            {
                                tagSuggestEntity.ZoneUrl = defaultTagEntity.CustomUrl;
                                tagSuggestEntity.ZoneId = defaultTagEntity.ZoneId;
                            }
                        }
                    }
                    tagSuggest.Add(tagSuggestEntity);
                }

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = listSelectedTag.Count, Data = tagSuggest }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        private List<TagWithSimpleFieldEntity> GetAllCleanTagInContentWithAllTagToken(string content, int minWord, int maxWord)
        {
            var listSelectedTag = new List<TagWithSimpleFieldEntity>();

            var tempTagStartByKeyword = new Dictionary<string, List<TagWithSimpleFieldEntity>>();

            var tagFormat = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");

            var listTag = ResearchTagTokenServices.GetTagSugguest(content, minWord, maxWord);
            foreach (var tag in listTag)
            {
                //var tagTemp = tag.ToLower().Trim();
                //if (!listSelectedTag.Exists(item => item.Name.ToLower() == tagTemp))
                //{
                //    var tagInDb = TagIsClean(tagTemp, ref tempTagStartByKeyword);
                //    if (tagInDb != null)
                //    {
                //        listSelectedTag.Add(tagInDb);
                //    }
                //}
                var tagItem = new TagWithSimpleFieldEntity
                {
                    Id = tag.ID,
                    Name = tag.Name,
                    Url = string.IsNullOrEmpty(tag.Url) ? Utility.UnicodeToKoDauAndGach(tag.Name) : tag.Url,
                    NewsCount = tag.NewsCount
                };
                if (tagFormat.Contains("{1}"))
                {
                    tagItem.Url = string.Format(tagFormat, tagItem.Url, tagItem.Id);
                }
                else
                {
                    tagItem.Url = string.Format(tagFormat, tagItem.Url);
                }
                listSelectedTag.Add(tagItem);
            }
            listSelectedTag.Sort((item1, item2) => item2.NewsCount.CompareTo(item1.NewsCount));
            return listSelectedTag;
        }

        #endregion

        #region For Thread

        [HttpPost, Route("search_thread")]
        public HttpResponseMessage SearchThread(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isHotThread = -1;
                int.TryParse(body.Get("isHotTag"), out isHotThread);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new ThreadServices().SearchThread(key, zoneId, 0, isHotThread, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var linkFormatThread = CmsChannelConfiguration.GetAppSetting("UrlFormatThread");
                    data.Select(s => { s.Url = string.Format(linkFormatThread, s.Url, s.Id); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchThreadResponse>.CreateSuccessResponse(new SearchThreadResponse { TotalRow = totalRow, Threads = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        [HttpPost, Route("get_thread_by_id")]
        public HttpResponseMessage GetThreadEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new ThreadServices().GetThreadByThreadId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundThread]));
                }

                var data = new InterviewForSuggestionEntity
                {
                    Id = id,
                    Title = news.Name
                };

                return this.ToJson(WcfActionResponse<InterviewForSuggestionEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_thread_event_by_id")]
        public HttpResponseMessage GetThreadEventByThreadId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new ThreadServices().GetThreadByThreadId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundThread]));
                }

                return this.ToJson(WcfActionResponse<ThreadDetailEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For News

        [HttpPost, Route("search_news")]
        public HttpResponseMessage SearchNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var searchfull = Utility.ConvertToBoolean(body.Get("searchfull"));
                var isSearchQuery = false;
                if (nameSpace.ToLower().Equals("suckhoehangngay"))
                {
                    isSearchQuery = true;
                }
                var totalRows = 0;
                var data = new List<NewsPublishForObjectBoxEntity>();
                if (searchfull)
                    data = new NewsServices().SearchNewsForNewsRelation(zoneId, keyword, pageIndex, pageSize, ref totalRows, true, isSearchQuery);
                else
                    data = new NewsServices().SearchNewsForNewsRelation_ByNewsPublishTemp(zoneId, keyword, pageIndex, pageSize, ref totalRows, false, isSearchQuery);

                return this.ToJson(WcfActionResponse<SearchNewsPublishResponse>.CreateSuccessResponse(new SearchNewsPublishResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("search_news_tag")]
        public HttpResponseMessage SearchNewsTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var tagId = Utility.ConvertToInt(body.Get("tagid"));

                var totalRows = 0;
                var data = new NewsServices().SearchNewsForNewsTagRelation(zoneId, tagId, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<SearchNewsPublishResponse>.CreateSuccessResponse(new SearchNewsPublishResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_link")]
        public HttpResponseMessage SearchNewsByLink(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));

                List<NewsPublishForObjectBoxEntity> lst = new List<NewsPublishForObjectBoxEntity>();
                Regex detailRegex = new Regex(CmsChannelConfiguration.GetAppSetting("RegexForLinkDetail"), RegexOptions.IgnoreCase | RegexOptions.Multiline);
                System.Text.RegularExpressions.Match kenh14Match = detailRegex.Match(keyword);
                if (kenh14Match.Success)
                {
                    var array = keyword.Split('-');
                    var id = Utility.ConvertToLong(array[array.Length - 1].Replace(".chn", ""));//Utility.ConvertToLong(kenh14Match.Groups[1].Value);
                    var NewsDetail = new NewsServices().GetDetail(id, username);
                    if (NewsDetail.NewsInfo != null)
                    {
                        var info = new NewsPublishForObjectBoxEntity();
                        info.NewsId = id;
                        info.EncryptId = id.ToString();
                        info.Avatar = NewsDetail.NewsInfo.Avatar;
                        info.Avatar2 = NewsDetail.NewsInfo.Avatar2;
                        info.Title = NewsDetail.NewsInfo.Title;
                        info.Sapo = NewsDetail.NewsInfo.Sapo;
                        info.Url = NewsDetail.NewsInfo.Url;
                        info.Avatar3 = NewsDetail.NewsInfo.Avatar3;
                        info.Avatar4 = NewsDetail.NewsInfo.Avatar4;
                        info.Avatar5 = NewsDetail.NewsInfo.Avatar5;
                        info.DisplayStyle = NewsDetail.NewsInfo.DisplayStyle;
                        info.ZoneId = NewsDetail.NewsInfo.ZoneId;
                        info.DistributionDate = NewsDetail.NewsInfo.DistributionDate;
                        lst.Add(info);
                    }
                }

                return this.ToJson(WcfActionResponse<List<NewsPublishForObjectBoxEntity>>.CreateSuccessResponse(lst, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region For Vote

        [HttpPost, Route("search_poll")]
        public HttpResponseMessage SearchPoll(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var isGetTotal = Utility.ConvertToBoolean(body.Get("isGetTotal"));
                var totalRow = 0;
                var data = new VoteServices().GetList(keyword, zoneId, pageIndex, pageSize, isGetTotal, ref totalRow);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Votes = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For Video

        [HttpPost, Route("search_video")]
        public HttpResponseMessage SearchVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var mylist = Utility.ConvertToBoolean(body.Get("isMyList"),false);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword")??string.Empty;
                var zoneVideoId = Utility.ConvertToInt(body.Get("zoneId"),-1);
                var playlistId = Utility.ConvertToInt(body.Get("playlistId"),0);
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                EnumVideoStatus videoStatus;
                if (!Enum.TryParse(status.ToString(), out videoStatus))
                {
                    videoStatus = EnumVideoStatus.AllStatus;
                }
                var sortOrder = Utility.ConvertToInt(body.Get("sortOrder"),0);
                EnumVideoSortOrder videoSortOrder;
                if (!Enum.TryParse(sortOrder.ToString(), out videoSortOrder))
                {
                    videoSortOrder = EnumVideoSortOrder.CreatedDateDesc;
                }
                if (videoStatus == EnumVideoStatus.Published)
                {
                    videoSortOrder = EnumVideoSortOrder.PublishDateDesc;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"),20);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("toDate"), "dd/MM/yyyy");
                var templateType = Utility.ConvertToInt(body.Get("templateType"),0);
                var mode = Utility.ConvertToInt(body.Get("mode"), -1);

                var totalRow = 0;
                var data = new MediaServices().SearchVideo(mylist ? accountName : string.Empty, zoneVideoId, playlistId, keyword,
                                                   videoStatus, videoSortOrder, fromDate, toDate, mode, pageIndex, pageSize,
                                                   ref totalRow);
                //if (data != null && data.Count > 0)
                //{
                //    data.Select(s =>
                //    {
                //        s.Avatar = CorrectVideoThumbDomainForSelect(s.Avatar);
                //        s.HtmlCode = CorrectVideoEmbedHostForSelect(s.HtmlCode);
                //        return s;
                //    }).ToList();
                //}
                return this.ToJson(WcfActionResponse<SearchVideoResponse>.CreateSuccessResponse(new SearchVideoResponse { TotalRow = totalRow, Videos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private static string CorrectVideoThumbDomainForSelect(string input)
        {
            try {
                if (string.IsNullOrEmpty(input)) input = "";
                var videoThumbAvatarDomainIncludeHttps =
                    CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_THUMBSERVER);
                var videoThumbAvatarDomain = videoThumbAvatarDomainIncludeHttps.Replace("https://", "http://");
                if (input.StartsWith(videoThumbAvatarDomain))
                {
                    input = input.Replace(videoThumbAvatarDomain, videoThumbAvatarDomainIncludeHttps);
                }
                else
                {
                    if (!input.StartsWith("http://") && !input.StartsWith("https://"))
                    {
                        input = videoThumbAvatarDomainIncludeHttps + input;
                    }
                }
            }
            catch { }
            return input;
        }
        private static string CorrectVideoEmbedHostForSelect(string input)
        {
            try {
                if (string.IsNullOrEmpty(input)) input = "";
                var embedHostingWithSsl = CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_EMBED_HOSTING_SSL);
                if (!string.IsNullOrEmpty(embedHostingWithSsl))
                {
                    var embedHosting = CmsChannelConfiguration.GetAppSetting(VideoStorageServices.VIDEO_MANAGER_EMBED_HOSTING);
                    input = input.Replace(embedHosting, embedHostingWithSsl);
                }
            }
            catch { }
            return input;
        }

        #endregion

        #region For Quote            
        [HttpPost, Route("search_quote")]
        public HttpResponseMessage SearchQuote(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var position = Utility.ConvertToInt(body.Get("position"),-1);

                var totalRows = 0;
                var data = new Kenh14Services().SearchQuote(keyword, position, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, StarQuotes = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_quote_by_id")]
        public HttpResponseMessage GetQuoteById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var quoteId = Utility.ConvertToInt(body.Get("id"));
                var data = new Kenh14Services().GetQuoteById(quoteId);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_quote")]
        public HttpResponseMessage UpdateQuote(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var quoteId = Utility.ConvertToInt(body.Get("quoteId"));
                var name = Utility.ConvertToString(body.Get("title"));
                var content = Utility.ConvertToString(body.Get("quote"));
                var avatar = Utility.ConvertToString(body.Get("avatar"));
                var position = Utility.ConvertToInt(body.Get("position"));

                var quote = new QuoteEntity()
                {
                    QuoteId = quoteId,
                    QuoteTitle = name,
                    Quote = content,
                    Avatar = avatar,
                    Position = position
                };

                var result = new WcfActionResponse();
                if (quoteId > 0)
                    result = new Kenh14Services().UpdateQuote(quote);
                else
                    result = new Kenh14Services().InsertQuote(quote, ref quoteId);

                result.Data = quoteId.ToString();

                return this.ToJson(result);

            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region For CharacterInfo(box nhân vật)
        [HttpPost, Route("search_character_info")]
        public HttpResponseMessage SearchCharacterInfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"),20);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);//0 unActive, 1 active, -1 ALl

                var totalRows = 0;
                var data = new Kenh14Services().SearchCharacterInfo(keyword, status, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, CharacterInfos = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_character_info_by_id")]
        public HttpResponseMessage GetCharacterInfoById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var quoteId = Utility.ConvertToInt(body.Get("id"));
                var data = new Kenh14Services().GetCharacterInfoById(quoteId);

                return this.ToJson(WcfActionResponse<CharacterInfoEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_character_info")]
        public HttpResponseMessage UpdateCharacterInfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
                var name = Utility.ConvertToString(body.Get("Name"));
                var avatar = Utility.ConvertToString(body.Get("Avatar"));
                var description = Utility.ConvertToString(body.Get("Description"));
                var content = Utility.ConvertToString(body.Get("Quote"));                
                var backgroundColor = Utility.ConvertToString(body.Get("BackgroundColor"));
                var borderColor = Utility.ConvertToString(body.Get("BorderColor"));
                var status = Utility.ConvertToInt(body.Get("Status"),1);//0 unActive, 1 active, -1 ALl

                var characterInfo = new CharacterInfoEntity
                {
                    Id = id,
                    Name = name,                    
                    Avatar = avatar,
                    Description = description,
                    Quote = content,
                    BackgroundColor=backgroundColor,
                    BorderColor=borderColor,
                    Status = status,
                    CreatedDate=DateTime.Now,
                    CreatedBy= username,
                    ModifiedDate=DateTime.Now,
                    ModifiedBy= username
                };

                var result = new WcfActionResponse();
                if (id > 0)
                    result = new Kenh14Services().UpdateCharacterInfo(characterInfo);
                else
                    result = new Kenh14Services().InsertCharacterInfo(characterInfo, ref id);

                result.Data = id.ToString();

                return this.ToJson(result);

            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("remove_character_info_by_id")]
        public HttpResponseMessage RemoveCharacterInfoById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var quoteId = Utility.ConvertToInt(body.Get("id"));
                var data = new Kenh14Services().DeleteCharacterInfoById(quoteId);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region ForImportWordFile

        [HttpPost, Route("import_word")]
        public async Task<HttpResponseMessage> ImportWordFile()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[0];
                    var zoneId = Utility.ConvertToInt(GetQueryString.GetPost("zoneId", 0));
                    var option = Utility.ConvertToInt(GetQueryString.GetPost("option", 0));

                    var apiEnabled = CmsChannelConfiguration.GetAppSettingInBoolean("ImportWordApiEnabled");
                    if (apiEnabled)
                    {
                        var urlApi = CmsChannelConfiguration.GetAppSetting("ImportWordApiUrl");
                        var SecretKey = CmsChannelConfiguration.GetAppSetting("ImportWordApiSecretKey");
                        var Namespace = CmsChannelConfiguration.GetAppSetting("ImportWordApiNamespace");

                        string result = string.Empty;
                        var responseString = string.Empty;
                        using (var httpClient = new HttpClient())
                        {
                            //get token                   
                            var param = new List<KeyValuePair<string, string>>
                            {
                                new KeyValuePair<string, string>("Namespace", Namespace),
                                new KeyValuePair<string, string>("SecretKey", SecretKey),
                            };
                            using (var content = new FormUrlEncodedContent(param))
                            {
                                using (var message = await httpClient.PostAsync(urlApi + "authen/get_token", content))
                                {
                                    message.EnsureSuccessStatusCode();
                                    responseString = await message.Content.ReadAsStringAsync();
                                }
                            }
                            if (!string.IsNullOrEmpty(responseString))
                            {
                                var paserToken = NewtonJson.Deserialize<DataTokenEntity>(responseString);
                                if (paserToken != null && !string.IsNullOrEmpty(paserToken.access_token))
                                {
                                    httpClient.DefaultRequestHeaders.Add("Authorization", paserToken.access_token);
                                    using (var content = new MultipartFormDataContent("ImportWord----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                                    {
                                        var streamContent = new StreamContent(new MemoryStream(GetBytes(postedFile)));
                                        streamContent.Headers.Add("Content-Type", postedFile.ContentType);
                                        content.Add(streamContent, "file", postedFile.FileName);
                                        content.Add(new StringContent("" + option), "option");

                                        using (var message = await httpClient.PostAsync(urlApi + "plugins/import_word", content))
                                        {
                                            response.EnsureSuccessStatusCode();
                                            result = await message.Content.ReadAsStringAsync();
                                        }

                                        if (!string.IsNullOrEmpty(result))
                                        {
                                            var paserJson = NewtonJson.Deserialize<DataUploadV2Entity>(result);
                                            if (paserJson != null && paserJson.success && !string.IsNullOrEmpty(paserJson.data))
                                            {
                                                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(paserJson?.data, "Success"));
                                            }
                                            else
                                            {
                                                return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Data not found: " + paserJson?.data));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Get token not found."));
                                }
                            }
                            else
                            {
                                return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Get token not found."));
                            }
                        }

                        return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Data not found"));
                    }

                    var html = Utility.ImportWordFileV2(postedFile, zoneId);

                    return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(html, "Success"));
                }
                else {
                    return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Không tìm thấy file import: 0 files"));
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        public class DataTokenEntity
        {
            public string access_token { get; set; }

            public string token_type { get; set; }

            public long expires_in { get; set; }
        }

        [HttpPost, Route("import_google_docs")]
        public async Task<HttpResponseMessage> ImportWordToGoogleDocs(HttpRequestMessage request)
        {            
            var body = request.Content.ReadAsFormDataAsync().Result;

            var link = body.Get("link");
            var option = Utility.ConvertToInt(GetQueryString.GetPost("option", 0));

            var data = await ImportWordGoogleDocs(link, option);

            return this.ToJson(WcfResponseData.CreateSuccessResponse(data));
        }

        private async Task<string> ImportWordGoogleDocs(string link, int option)
        {
            try
            {
                var apiEnabled = CmsChannelConfiguration.GetAppSettingInBoolean("ImportWordApiEnabled");
                if (apiEnabled)
                {
                    var urlApi = CmsChannelConfiguration.GetAppSetting("ImportWordApiUrl");
                    var SecretKey = CmsChannelConfiguration.GetAppSetting("ImportWordApiSecretKey");
                    var Namespace = CmsChannelConfiguration.GetAppSetting("ImportWordApiNamespace");

                    string result = string.Empty;
                    var responseString = string.Empty;
                    using (var httpClient = new HttpClient())
                    {
                        //get token                   
                        var param = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>("Namespace", Namespace),
                            new KeyValuePair<string, string>("SecretKey", SecretKey),
                        };
                        using (var content = new FormUrlEncodedContent(param))
                        {
                            using (var message = await httpClient.PostAsync(urlApi + "authen/get_token", content))
                            {
                                message.EnsureSuccessStatusCode();
                                responseString = await message.Content.ReadAsStringAsync();
                            }
                        }
                        if (!string.IsNullOrEmpty(responseString))
                        {
                            var paserToken = NewtonJson.Deserialize<DataTokenEntity>(responseString);
                            if (paserToken != null && !string.IsNullOrEmpty(paserToken.access_token))
                            {
                                httpClient.DefaultRequestHeaders.Add("Authorization", paserToken.access_token);
                                //get token                   
                                var param2 = new List<KeyValuePair<string, string>>
                                {
                                    new KeyValuePair<string, string>("link", link),
                                    new KeyValuePair<string, string>("option", option.ToString())
                                };
                                using (var content = new FormUrlEncodedContent(param2))
                                {
                                    using (var message = await httpClient.PostAsync(urlApi + "plugins/import_google_docs", content))
                                    {
                                        message.EnsureSuccessStatusCode();
                                        result = await message.Content.ReadAsStringAsync();
                                    }

                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        var paserJson = NewtonJson.Deserialize<DataUploadV2Entity>(result);
                                        if (paserJson != null && paserJson.success && !string.IsNullOrEmpty(paserJson.data))
                                        {
                                            return paserJson?.data;
                                        }
                                        else
                                        {
                                            return "Data not found: " + paserJson?.data;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                return "Get token not found.";
                            }
                        }
                        else
                        {
                            return "Get token not found.";
                        }
                    }

                    return "Data not found";
                }

                return "Config ImportWordApiEnabled => false";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        }
        #endregion

        #region ForImportPDF

        [HttpPost, Route("import_pdf")]
        public async Task<HttpResponseMessage> ImportPDFFile()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var file = httpRequest.Files[0];
                    
                    var apiEnabled = CmsChannelConfiguration.GetAppSettingInBoolean("UploadApiEnabled");
                    if (apiEnabled)
                    {
                        var urlApi = CmsChannelConfiguration.GetAppSetting("UploadApiUrl");
                        var apiChannel = CmsChannelConfiguration.GetAppSetting("UploadApiChannel");
                        var url = string.Format("{0}/{1}/convertToHtml", urlApi, apiChannel);

                        string result = string.Empty;
                        using (var httpClient = new HttpClient())
                        {
                            using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                            {
                                content.Add(new StreamContent(new MemoryStream(GetBytes(file))), "file", file.FileName);
                                using (var message = await httpClient.PostAsync(url, content))
                                {
                                    result = await message.Content.ReadAsStringAsync();
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(result))
                        {
                            var paserJson = NewtonJson.Deserialize<DataUploadV2Entity>(result);
                            if (paserJson != null && paserJson.status && paserJson.success && !string.IsNullOrEmpty(paserJson.data))
                            {                                                                 
                                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(paserJson.data, "Success"));
                            }
                            else
                            {
                                return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Data not found."));
                            }
                        }

                        return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Data not found."));
                    }
                    
                    return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Api not enbale"));
                }
                else {
                    return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("not found file import: 0 files"));
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private static byte[] GetBytes(HttpPostedFile file)
        {
            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            return fileData;
        }
        public class DataUploadV2Entity
        {
            public bool status { get; set; }

            public string message { get; set; }

            public string data { get; set; }

            public string content { get; set; }

            public int code { get; set; }

            public bool success { get; set; }
        }
        #endregion

        #region For Crawler

        [HttpPost, Route("crawler_link")]
        public HttpResponseMessage CrawlerLink(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var link = body.Get("link");
                if (string.IsNullOrEmpty(link))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.LinkNotEmpty]));
                }
                var version = body.Get("version");

                var content = new newsInfo();

                var allowApiCrawleV2 = CmsChannelConfiguration.GetAppSettingInInt32("AllowCrawlerV2ApiUrl");
                if (!string.IsNullOrEmpty(version) && version.ToLower().Equals("v2") && allowApiCrawleV2 == 1)
                {
                    var urlApiV2 = CmsChannelConfiguration.GetAppSetting("CrawlerV2ApiUrl");
                    content = CrwalerV2NewsApiMic3(urlApiV2, link);
                    if (content == null || (content != null &&
                                            (string.IsNullOrEmpty(content.news_title) || content.news_title.ToLower() == "false") ||
                                            string.IsNullOrEmpty(content.news_content)
                                            )
                        )
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.WebsiteNotIdentifyTryAgainAfter5Minute]));
                    }
                }
                else {

                    content = NewsCrawlerServices.CrawlerLink(link);
                    if (content == null || (content != null &&
                                            (string.IsNullOrEmpty(content.news_title) || content.news_title.ToLower() == "false") ||
                                            string.IsNullOrEmpty(content.news_content)
                                            )
                        )
                    {                        
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.WebsiteNotIdentifyTryAgainAfter5Minute]));
                    }
                }
                
                return this.ToJson(WcfActionResponse<newsInfo>.CreateSuccessResponse(content, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception,ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        private newsInfo CrwalerV2NewsApiMic3(string urlApi, string link)
        {
            try
            {
                var url = string.Format(urlApi+"?url_down={0}", link);
                var req = (HttpWebRequest)WebRequest.Create(url);
                req.ContentType = "application/json; charset=utf-8";
                req.Accept = "text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8";
                req.Method = "GET";
                req.Timeout = 30 * 1000;

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();
                if (!string.IsNullOrEmpty(result))
                {
                    var paserJson = NewtonJson.Deserialize<DataNewsJson>(result);
                    if (paserJson != null)
                        return new newsInfo
                        {
                            news_title = paserJson.title,
                            news_content = paserJson.content,
                            news_image = paserJson.avatar,
                            news_url = paserJson.url,
                            news_des= paserJson.excerpt,
                            datecreate=DateTime.Now,
                            news_des_char= paserJson.textContent,
                            news_link= paserJson.url
                        };
                }
                
                return null;
            }
            catch
            {
                return null;
            }
        }

        [HttpPost, Route("crawler_link_v2")]
        public async Task<HttpResponseMessage> CrawlerNewsSocialContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var url = body.Get("Url");
                if (string.IsNullOrEmpty(url) || (!string.IsNullOrEmpty(url) && !url.StartsWith("http")))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var urlRegex = string.Empty;
                var originalId = GetOriginalId(url, out urlRegex);
                var rawId = Crypton.Md5Encrypt(string.Format("{0}_{1}", originalId, url));

                var socialService = new SocialServices();
                
                if (originalId == (int)EnumNewsSocialEmbedOriginalId.News)
                {
                    var urlApiV2 = CmsChannelConfiguration.GetAppSetting("CrawlerV2ApiUrl");
                    var dataNewJson = GetRequest(string.Format(urlApiV2 + "?url_down={0}", url));
                    if (!string.IsNullOrEmpty(dataNewJson))
                    {
                        var paserJson = NewtonJson.Deserialize<DataNewsJson>(dataNewJson);
                        if (paserJson != null)
                            return this.ToJson(new NewsSocialContentEntity
                            {
                                Title = paserJson.title,
                                Body = paserJson.content,
                                Avatar = paserJson.avatar,
                                Url = paserJson.url,
                                OriginalUrl = paserJson.url,
                                OriginalId = originalId,
                                OriginalName = paserJson.domain,
                                CreatedBy = accountName,
                                CreatedDate = DateTime.Now,
                                Sapo = paserJson.excerpt,
                                WordCount = paserJson.length.Value,
                                Status = (int)EnumNewsSocialContentStatus.NoActive,
                                RawId = rawId
                            });

                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể passer json data."));
                    }

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể lấy được dữ liệu từ link bài viết."));

                }else if(originalId == (int)EnumNewsSocialEmbedOriginalId.GoogleDocs)
                {
                    var option = Utility.ConvertToInt(GetQueryString.GetPost("option", 0));
                    var data = await ImportWordGoogleDocs(url, option);                                        
                    return this.ToJson(new NewsSocialContentEntity
                    {                        
                        Body = data,                       
                        OriginalId = originalId,                        
                        CreatedBy = accountName,
                        CreatedDate = DateTime.Now,                                                
                        Status = (int)EnumNewsSocialContentStatus.NoActive,
                        RawId = rawId
                    });
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể lấy được dữ liệu từ link bài viết."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
                
        private string GetRequest(string url)
        {
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(url);
                req.ContentType = "application/json; charset=utf-8";
                req.Accept = "text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8";
                req.Method = "GET";

                var webResponse = req.GetResponse();
                var sr = new StreamReader(webResponse.GetResponseStream());
                var result = sr.ReadToEnd().Trim();

                return result;
            }
            catch (WebException ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.ToString());
                return string.Empty;
            }
        }
        private int GetOriginalId(string url, out string urlRegex)
        {
            urlRegex = string.Empty;
            Uri NewUri;

            if (Uri.TryCreate(url, UriKind.Absolute, out NewUri))
            {
                if (NewUri.Authority.ToLower().Contains("twitter.com"))
                {
                    if (NewUri.AbsolutePath.ToLower().Contains("status"))
                        return (int)EnumNewsSocialEmbedOriginalId.Tweeter;
                    return 7777;
                }
                else if (NewUri.Authority.ToLower().Contains("instagram.com"))
                {
                    var reg = Regex.Match(NewUri.AbsolutePath, "^/p/[a-zA-Z-_0-9]+/+embed");
                    var reg2 = Regex.Match(NewUri.AbsolutePath, "^/p/[a-zA-Z-_0-9]+/");
                    if (reg.Success)
                        return (int)EnumNewsSocialEmbedOriginalId.Intagram;
                    else if (reg2.Success)
                    {
                        urlRegex = url.Replace(NewUri.AbsolutePath, NewUri.AbsolutePath + "embed");
                        return 8888;
                    }
                    else
                        return 9999;
                }
                else if (NewUri.Authority.ToLower().Contains("facebook.com"))
                {
                    if (NewUri.AbsolutePath.ToLower().Contains("videos"))
                        return (int)EnumNewsSocialEmbedOriginalId.FacebookVideo;
                    return (int)EnumNewsSocialEmbedOriginalId.Facebook;
                }
                else if (NewUri.Authority.ToLower().Contains("docs.google.com"))
                {
                    if (NewUri.AbsolutePath.ToLower().Contains("edit"))
                        return (int)EnumNewsSocialEmbedOriginalId.GoogleDocs;
                    return 1111;
                }
                else
                {
                    return (int)EnumNewsSocialEmbedOriginalId.News;
                }
            }

            return (int)EnumNewsSocialEmbedOriginalId.News;
        }

        #endregion

        #region SpellCheck

        [HttpPost, Route("check_spelling")]
        public HttpResponseMessage CheckSpelling(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var content = body.Get("inputContent");
                if (string.IsNullOrEmpty(content))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ContentNotEmpty]));
                }

                var result = SpellCheckerServices.CheckSpelling(content);
                if (result == null)
                {
                    var obj = new
                    {
                        ListErrorItem = "" + result,
                        ErrorCount = 0,
                        Content = content
                    };

                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(obj, "Success"));
                }

                var output = result.Output;
                if (!string.IsNullOrEmpty(output))
                {
                    var errorCount = 0;
                    output = new Regex("<serror[^>]*>(.*?)</serror>",
                                       RegexOptions.Multiline |
                                       RegexOptions.IgnoreCase |
                                       RegexOptions.Compiled).Replace(output, delegate (System.Text.RegularExpressions.Match match)
                                       {
                                           var error = match.ToString();
                                           if (error.IndexOf("type=\"0\"", StringComparison.CurrentCultureIgnoreCase) >=
                                           0 ||
                                           error.IndexOf("type='0'", StringComparison.CurrentCultureIgnoreCase) >= 0)
                                           {
                                               error = error.Replace("<serror ",
                                                                 "<span class=\"IMSSpellCheckItem IMSSpellError\" ");
                                           }
                                           else if (
                                           error.IndexOf("type=\"1\"", StringComparison.CurrentCultureIgnoreCase) >=
                                           0 ||
                                           error.IndexOf("type='1'", StringComparison.CurrentCultureIgnoreCase) >=
                                           0)
                                           {
                                               error = error.Replace("<serror ",
                                                                 "<span class=\"IMSSpellCheckItem IMSSpellWarning\" ");
                                           }
                                           else if (
                                           error.IndexOf("type=\"2\"",
                                                         StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                                           error.IndexOf("type='2'",
                                                         StringComparison.CurrentCultureIgnoreCase) >= 0)
                                           {
                                               error = error.Replace("<serror ",
                                                                 "<span class=\"IMSSpellCheckItem IMSSpellSuspect\" ");
                                           }
                                           error = error.Replace("</serror>", "</span>");
                                           errorCount++;
                                           return error;
                                       });

                    var listErrorItem = new List<ErrorItem>();
                    var errorList = result.SpellingErrors;
                    foreach (var errorItem in errorList)
                    {
                        var currentIndex = listErrorItem.FindIndex(item => item.MapId == errorItem.MapId);
                        if (currentIndex >= 0)
                        {
                            listErrorItem[currentIndex].Corrections.Add(new CorrectionItem
                            {
                                Confident = errorItem.Confident,
                                Correction = errorItem.Correction
                            });
                        }
                        else
                        {
                            listErrorItem.Add(new ErrorItem
                            {
                                MapId = errorItem.MapId,
                                Original = errorItem.Original,
                                Corrections = new List<CorrectionItem>
                                    {
                                        new CorrectionItem()
                                            {
                                                Confident = errorItem.Confident,
                                                Correction = errorItem.Correction
                                            }
                                    }
                            });
                        }
                    }
                    var obj = new
                    {
                        ListErrorItem = NewtonJson.Serialize(listErrorItem),
                        ErrorCount = errorCount,
                        Content = output
                    };

                    return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(obj, "Success"));
                }
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotCheckContent]));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        private struct ErrorItem
        {
            public int MapId;
            public string Original;
            public List<CorrectionItem> Corrections;
        }
        private struct CorrectionItem
        {
            public double Confident;
            public string Correction;
        }

        #endregion

        #region Wechoice

        [HttpPost, Route("wechoice_getmember")]
        public HttpResponseMessage GetMemberWC(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("id");
                if (string.IsNullOrEmpty(ids))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Id not empty."));
                }
                var id = 0;
                int.TryParse(ids,out id);

                var updateUrl = "http://wechoice.vn/Handlers/ImsHandler.ashx?secureKey=qwe6789&c=getmember&awardid=" + id;

                var updateRequest = (HttpWebRequest)WebRequest.Create(updateUrl);
                updateRequest.KeepAlive = true;
                updateRequest.Method = "GET";
                updateRequest.Timeout = 60 * 60 * 1000;
                updateRequest.ContentType = "text/xml; encoding='utf-8'";
                updateRequest.MaximumAutomaticRedirections = 10;
                updateRequest.AllowAutoRedirect = true;
                updateRequest.Referer = updateUrl;
                updateRequest.UserAgent = "Mozilla/5.0";

                var updateResponse = (HttpWebResponse)updateRequest.GetResponse();

                var str = string.Empty;
                if (updateResponse.StatusCode == HttpStatusCode.OK)
                {
                    var responseReader = new StreamReader(updateResponse.GetResponseStream());
                    var responseData = responseReader.ReadToEnd();

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        str = responseData;
                        responseReader.Close();
                        responseReader.Dispose();
                        updateResponse.Close();
                    }                    
                    responseReader.Close();
                    responseReader.Dispose();
                }
                else
                {                    
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Update error (Request error): " + updateUrl));
                }
                updateResponse.Close();

                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(str, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion        

        #region For slidephoto

        [HttpPost, Route("get_list_slide_photo")]
        public HttpResponseMessage SearchEmbedAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("num"), 5);
                var type = Utility.ConvertToInt(body.Get("type"), 1);
                var status = Utility.ConvertToInt(body.Get("status"), -1);

                var totalRow = 0;
                var data = new MediaServices().SearchEmbedAlbum(keyword, type, zoneId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion

        #region For MediaAlbum

        [HttpPost, Route("search_media_album")]
        public HttpResponseMessage SearchMediaAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 10);

                var totalRow = 0;
                var data = new MediaAlbumServices().SearchMediaAlbum(keyword, -1, -1, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_album_media_by_id")]
        public HttpResponseMessage GetMediaAlbumForEditByMediaAlbumId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                
                var albumId = Utility.ConvertToInt(body.Get("mediaId"), 0);                                
                var data = new MediaAlbumServices().GetMediaAlbumForEditByMediaAlbumId(albumId, 0);

                return this.ToJson(WcfActionResponse<Entity.Base.MediaAlbum.MediaAlbumForEditEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region For LiveMatch

        [HttpPost, Route("search_livematch")]
        public HttpResponseMessage SearchLiveMatch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);

                var totalRow = 0;
                var data = new LiveMatchServices().SearchLiveMatch(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For RollingNews

        [HttpPost, Route("search_rollingnews")]
        public HttpResponseMessage SearchRollingNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                //var keyword = body.Get("keyword");
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "yyyy-MM-dd HH:mm:ss");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("toDate"), "yyyy-MM-dd HH:mm:ss");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);
                //var status = Utility.ConvertToInt(body.Get("status"),-1);

                var totalRow = 0;
                var data = new RollingNewsServices().SearchRollingNews(fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_rollingnews_by_id")]
        public HttpResponseMessage GetRollingNewsById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");

                var news = new RollingNewsServices().GetRollingNewsByRollingNewsId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundRollingNews]));
                }
               
                return this.ToJson(WcfActionResponse<NodeJs_RollingNewsEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For Interview

        [HttpPost, Route("search_interview")]
        public HttpResponseMessage SearchInterview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                //var isFocus = Utility.ConvertToInt(body.Get("focus"),-1);//-1: không lọc theo đk, 1: nổi bật, 0: không nổi bật
                //var isActive = Utility.ConvertToInt(body.Get("active"),-1);//-1: không lọc theo đk, 1: đang có hiệu lực, 0: không có hiệu lực

                var totalRow = 0;
                var data = new InterviewServices().SearchInterviewV3(keyword,  pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_interview_by_id")]
        public HttpResponseMessage GetInterviewById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id") ?? string.Empty;

                var news = new InterviewServices().InterviewV3GetById(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundChuDe]));
                }

                return this.ToJson(WcfActionResponse<NodeJs_InterviewEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
       
        #endregion

        #region For NewsSource

        [HttpPost, Route("search_newssource")]
        public HttpResponseMessage NewsSourceGetAll(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                //var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                //var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);

                var totalRow = 0;
                var data = new NewsServices().NewsSourceGetAll(keyword);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_news_resource_by_id")]
        public HttpResponseMessage GetNewsSourceGetById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new NewsServices().NewsSourceGetById(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundChuDe]));
                }

                return this.ToJson(WcfActionResponse<NewsSourceEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For Topic

        [HttpPost, Route("search_topic")]
        public HttpResponseMessage SearchTopic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 10);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var parentId = Utility.ConvertToInt(body.Get("parentId"), -1);
                var isHotTopic = Utility.ConvertToInt(body.Get("isHotTag"), -1);
                var order = Utility.ConvertToInt(body.Get("orderBy"), 0);
                var isActive = Utility.ConvertToInt(body.Get("isActive"), -1);

                var totalRow = 0;
                var data = new TopicServices().SearchTopic(keyword, zoneId, order, isHotTopic, isActive, parentId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        [HttpPost, Route("get_topic_by_id")]
        public HttpResponseMessage GetTopicById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new TopicServices().GetTopicByTopicId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundChuDe]));
                }

                var data = new InterviewForSuggestionEntity
                {
                    Id = id,
                    Title = news.TopicName
                };

                return this.ToJson(WcfActionResponse<InterviewForSuggestionEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_topic_detail_by_id")]
        public HttpResponseMessage GetTopicDetailById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new TopicServices().GetTopicByTopicId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundChuDe]));
                }

                return this.ToJson(WcfActionResponse<TopicDetailEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For MagazineContent

        [HttpPost, Route("search_magazinecontent")]
        public HttpResponseMessage MagazineContent_Search(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);
                var status = Utility.ConvertToInt(body.Get("status"), -1);

                var totalRow = 0;
                var data = new MagazineContentServices().MagazineContent_Search(keyword, MagazineContentIsHot.All, MagazineContentStatus.Active, "", DateTime.MinValue, DateTime.MinValue, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("import_magazine_pr")]
        public HttpResponseMessage ImportMagazinePr(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var url = body.Get("url");
                if (string.IsNullOrEmpty(url))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamUrlNotExit]));
                }

                var data = new MagazineContentServices().ImportMagazinePr(url);

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(data, "Success."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("import_minimagazine_pr")]
        public HttpResponseMessage ImportMiniMagazinePr(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var url = body.Get("url");
                if (string.IsNullOrEmpty(url))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamUrlNotExit]));
                }

                var data = new MagazineContentServices().ImportMiniMagazinePr(url);

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(data, "Success."));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region TemplateEmbed(MediaUnit)

        [HttpPost, Route("search_template_embed")]
        public HttpResponseMessage SearchTemplateEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var keyword = body.Get("keyword")??string.Empty;
                var channelId = Utility.ConvertToInt(body.Get("channelId"),0);
                if (channelId<=0)
                {
                    channelId = CmsChannelConfiguration.GetAppSettingInInt32("TemplateEmbedChannelId");
                }
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var statusEnum = Utility.ConvertToInt(body.Get("status"));
                EnumTemplateEmbedStatus status;
                if (!Enum.TryParse(statusEnum.ToString(), out status))
                {
                    status = EnumTemplateEmbedStatus.AllStatus;
                }
                var totalRow = 0;
                var data = new TemplateEmbedServices().SearchTemplateEmbed(keyword, channelId, 0, status, pageIndex, pageSize, ref totalRow);
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, TemplateEmbeds = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion
    }
}
