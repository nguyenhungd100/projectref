﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.Expert.Entity;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/expert")]
    public class ExpertController : ApiController
    {
        #region Expert

        [HttpPost, Route("search_expert")]
        public HttpResponseMessage SearchExpert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new ExpertServices().SearchExpert(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_expert_by_id")]
        public HttpResponseMessage GetExpertById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var expert = new ExpertServices().ExpertGetById(id);
                if (expert == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFoundExpert]));
                }

                return this.ToJson(WcfActionResponse<ExpertEntity>.CreateSuccessResponse(expert, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_expert")]
        public HttpResponseMessage SaveExpert(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var description = body.Get("Description") ?? string.Empty;
                var expertName = body.Get("ExpertName") ?? string.Empty;
                var quote = body.Get("Quote") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var jobTitle = body.Get("JobTitle") ?? string.Empty;                
                var displayJobTitle = Utility.ConvertToBoolean(body.Get("DisplayJobTitle"),false);
                var acceptFeedback = Utility.ConvertToBoolean(body.Get("AcceptFeedback"),false);                
                var facebookLink = body.Get("FacebookLink") ?? string.Empty;                
                var mobile = body.Get("Mobile") ?? string.Empty;

                var expertEntity = new ExpertEntity
                {
                    Id = id,
                    ExpertName = expertName,
                    Avatar = avatar,
                    Description = description,                    
                    JobTitle = jobTitle,
                    DisplayJobTitle = displayJobTitle,
                    Quote = quote,
                    AcceptFeedback = acceptFeedback,
                    FacebookLink = facebookLink,
                    Mobile=mobile
                };

                var result = new WcfActionResponse();
                if (id > 0)
                {                    
                    result = new ExpertServices().ExpertUpdate(expertEntity);                    
                }
                else {
                    var newsExpertId = 0;
                    result = new ExpertServices().ExpertInsert(expertEntity, ref newsExpertId);
                    result.Data = newsExpertId.ToString();
                    expertEntity.Id = newsExpertId;
                }
                if (result.Success)
                {
                    var jsonKey = "{\"Id\":" + expertEntity.Id + "}";
                    var strPram = NewtonJson.Serialize(expertEntity, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updateexpert", string.Empty, jsonKey);
                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("expert_delete")]
        public HttpResponseMessage ExpertDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var expertId = Utility.ConvertToInt(body.Get("Id"));

                var result = new ExpertServices().ExpertDelete(expertId);

                if (result.Success)
                {
                    var jsonKey = "{\"Id\":" + expertId + "}";
                    var strPram = NewtonJson.Serialize(expertId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "removeexpert", string.Empty, jsonKey);
                }

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_expert_id")]
        public HttpResponseMessage SearchNewsByExpertId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                var data = new ExpertServices().ExpertInNews_GetByExpertId(expertId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("add_expertinnews")]
        public HttpResponseMessage AddExpertInNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"));
                var deletedIds = body.Get("DeletedIds")?? string.Empty;
                var addNewsId = body.Get("AddNewsId")?? string.Empty;
                var deleteNewsIds = body.Get("DeleteNewsIds") ?? string.Empty;
                if (deletedIds == "" & addNewsId == "")
                {
                    return this.ToJson(WcfActionResponse.CreateSuccessResponse());
                }
                if (deletedIds != "")
                {
                    var arr = deletedIds.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);                    
                    deletedIds = string.Join(";", arr);
                }

                var result = new ExpertServices().ExpertInNews_UpdateNews(expertId, deletedIds, addNewsId);

                if (result.Success)
                {
                    var jsonKey = "{\"ExpertId\":" + expertId + "}";
                    var strPram = NewtonJson.Serialize(new { ExpertId=expertId, AddNewsId= addNewsId, DeletedIds= deletedIds, DeleteNewsIds = deleteNewsIds }, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "updatemultiexpertinnews", string.Empty, jsonKey);
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Send Telegram
        [AllowAnonymous]
        [HttpPost, Route("send_telegram")]
        public HttpResponseMessage SendExpert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var message = body.Get("message");
                if (string.IsNullOrEmpty(message))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không thể gửi message rỗng"));
                }
                if (!string.IsNullOrEmpty(message) && message.Length > 225)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Nội dung tin nhắn dài tối đa 225 ký tự."));
                }
                var telegramId = body.Get("telegramid");
                var secretKey = body.Get("secretkey");
                var np = body.Get("namespace");
                var lang = body.Get("lang");

                if (!string.IsNullOrEmpty(np))
                    WcfExtensions.WcfMessageHeader.Current.Namespace = np;
                if (!string.IsNullOrEmpty(lang))
                    WcfExtensions.WcfMessageHeader.Current.Language = lang;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {                    
                    var result = ExpertSend(Base64.Encode(message), telegramId, np, secretKey);

                    if (result == "1")
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Not config url appbot"));

                    if (result != string.Empty)
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse(result, "Success"));

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Error send message."));
                }
                else {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private string ExpertSend(string message, string telegramId, string np, string secretKey)
        {
            try
            {
                var urlApiBot = CmsChannelConfiguration.GetAppSetting("AppBotTelegramApiUrl");
                if (string.IsNullOrEmpty(urlApiBot))
                {
                    return "1";
                }

                if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(telegramId))
                {
                    var action = "expert/send";
                    secretKey = Crypton.Md5Encrypt(secretKey);
                    string url = string.Format(urlApiBot + action + "?secretkey={0}&sms={1}&telegramid={2}&np={3}", secretKey, message, telegramId, np);

                    Logger.WriteLog(Logger.LogType.Trace, "Begin ExpertSend: AppBotTelegramApiUrl => " + url);

                    WebRequest webrequest = WebRequest.Create(url);
                    webrequest.Credentials = CredentialCache.DefaultCredentials;
                    webrequest.Timeout = 10000;
                    WebResponse response = webrequest.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    var result = reader.ReadToEnd();

                    Logger.WriteLog(Logger.LogType.Trace, "End ExpertSend: AppBotTelegramApiUrl => " + url);

                    reader.Close();
                    reader.Dispose();
                    response.Close();
                    response.Dispose();

                    return result;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "ExpertSend => " + ex.Message);
                return string.Empty;
            }
        }


        [HttpPost, Route("reset_telegram")]
        public HttpResponseMessage ResetTelegram(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var expertId = Utility.ConvertToInt(body.Get("expertid"),0);

                var result = new ExpertServices().ResetTelegram(expertId, accountName);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }
        #endregion
    }
}
