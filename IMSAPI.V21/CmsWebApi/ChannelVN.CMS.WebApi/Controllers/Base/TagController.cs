﻿using ChannelVN.CMS.BO.Base.Zone;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/tag")]
    public class TagController : ApiController
    {
        #region Tag

        #region action

        [HttpPost, Route("save")]
        public HttpResponseMessage Save(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                

                var id = Utility.ConvertToLong(body.Get("id"));
                var parentId = Utility.ConvertToInt(body.Get("parentId"), 0);
                var tagName = body.Get("tagname");
                var description = body.Get("description");
                var invisiable = Utility.ConvertToBoolean(body.Get("invisiable"));
                var isHotTag = Utility.ConvertToBoolean(body.Get("isHotTag"));
                var type = Utility.ConvertToInt(body.Get("type"), 0);
                var avartar = body.Get("avatar");
                var isthread = false;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zoneid"), 0);
                var zoneIdList = body.Get("zoneidlist");
                var tagContent = body.Get("tagContents");
                tagContent = HttpContext.Current.Server.UrlDecode(tagContent);
                var tagTitle = body.Get("tagTitle");
                var tagInit = body.Get("tagInit");
                var tagMetaKeyword = body.Get("tagMetaKeyword");
                var tagMetaContent = body.Get("tagMetaContent");
                var templateid = Utility.ConvertToInt(body.Get("templateid"), 1);
                //var createDate = Utility.ConvertToDateTimeDefaultDateNow(body.Get("createdDate"));
                var subTitle = GetQueryString.GetPost("subtitle", string.Empty);
                var newsCoverId = Utility.ConvertToLong(body.Get("newscoverid"));
                var isAmp = Utility.ConvertToBoolean(body.Get("IsAmp"), false);

                var zoneName = string.Empty;
                var zone = ZoneBo.GetZoneById(zoneId);
                if (zone != null)
                {
                    zoneName = zone.Name;
                }

                var tagNew = new TagEntity
                {
                    ParentId = parentId,
                    Name = tagName,
                    Description = description,
                    Url = Utility.UnicodeToKoDauAndGach(tagName),
                    Invisibled = invisiable,
                    IsHotTag = isHotTag,
                    Type = type,
                    Id = id,
                    EditedBy = accountName,
                    Avatar = avartar,
                    IsThread = isthread,
                    UnsignName = Utility.ConvertTextToUnsignName(tagName),
                    TagContent = tagContent,
                    TagTitle = tagTitle,
                    TagInit = tagInit,
                    TagMetaKeyword = tagMetaKeyword,
                    TagMetaContent = tagMetaContent,
                    TemplateId = templateid,                    
                    SubTitle = subTitle,
                    NewsCoverId = newsCoverId,
                    ZoneName= zoneName,
                    ZoneId= zoneId,
                    IsAmp=isAmp
                };
                var data = new WcfActionResponse();
                if (id > 0)
                {
                    data = new TagServices().Update(tagNew, zoneId, zoneIdList);
                }
                else
                {
                    //check name và url cho vietnammoi and vietnambiz
                    var currentTag = new TagServices().GetTagByTagName(tagNew.Name);
                    if (CmsChannelConfiguration.GetAppSettingInBoolean("AllowCheckNamOrUrlForTag"))
                    {
                        currentTag = new TagServices().GetTagByTagNameOrUrl(tagNew.Name, tagNew.Url);
                    }
                    if (currentTag != null)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(tagNew.Name + ", " + tagNew.Url + " đã tồn tại 'Tag' tương tự."));
                    }

                    data = new TagServices().InsertTag(tagNew, zoneId, zoneIdList);
                }
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var tagId = Utility.ConvertToLong(body.Get("id"));
                var data = new TagServices().DeleteById(tagId, username);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("add_tag_news")]
        public HttpResponseMessage AddTagNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("tagId"));
                var deletedNewsId = body.Get("deletedNewsId");
                var addNewsId = body.Get("addNewsId");
                var data = new TagServices().UpdateNewsInTagNews(tagId, deletedNewsId, addNewsId, 0);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("update_tag_hot")]
        public HttpResponseMessage UpdateTagHot(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("id"));
                var isHotTag = Utility.ConvertToBoolean(body.Get("isHotTag"));

                var data = new TagServices().UpdateTagHot(id, isHotTag);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("update_tag_invisibled")]
        public HttpResponseMessage UpdateTagInvisibled(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("id"));
                var invisibled = Utility.ConvertToBoolean(body.Get("invisibled"));

                var data = new TagServices().UpdateTagInvisibled(id, invisibled);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_tag")]
        public HttpResponseMessage SearchTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isThread = Utility.ConvertToInt(body.Get("isThread"));
                var isHotTag = -1;
                int.TryParse(body.Get("isHotTag"), out isHotTag);
                var parentTagId = Utility.ConvertToInt(body.Get("parentTagId"));
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"),20);
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new TagServices().SearchTag(key, parentTagId, isThread, zoneId, 0, order, isHotTag, pageIndex, pageSize, ref totalRow, false);
                if (data != null && data.Count > 0)
                {
                    var linkFormatTag = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");
                    data.Select(s => { s.Url = string.Format(linkFormatTag, s.Url); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchTagResponse>.CreateSuccessResponse(new SearchTagResponse { TotalRow = totalRow, Tags = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_tag_id")]
        public HttpResponseMessage SearchNewsByTagId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("tagId"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 15);
                var totalRows = 0;

                var data = new NewsServices().SearchNewsByTagIdWithPaging(tagId, status, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        #endregion

        #region get

        [HttpPost, Route("get_tag_by_id")]
        public HttpResponseMessage GetTagById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("id"));

                var data = new TagServices().GetTagByTagId(tagId);

                return this.ToJson(WcfActionResponse<TagEntityDetail>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_news_publish_by_tag_id")]
        public HttpResponseMessage SearchPublishNewsByNewsInTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("tag_id"));
                var zoneId = Utility.ConvertToInt(body.Get("zone_id"), 0);
                var keyWords = body.Get("keywords");
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("page_size"), 20);
                var totalRows = 0;

                var data = new NewsServices().SearchNewsPublishExcludeNewsInTag(zoneId, keyWords, tagId, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region plugin

        [HttpPost, Route("add_new")]
        public HttpResponseMessage AddNew(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var tagName = body.Get("tag");
                if (string.IsNullOrEmpty(tagName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Invalid tag's name"));
                }

                var zoneName = string.Empty;
                var zone = ZoneBo.GetZoneById(zoneId);
                if (zone != null)
                {
                    zoneName = zone.Name;
                }

                var tagNew = new TagEntity
                {
                    Name = tagName,
                    Description = tagName,
                    ParentId = 0,
                    Url = Utility.ConvertTextToLink(tagName),
                    ZoneId =zoneId,
                    ZoneName = zoneName
                };
                
                var currentTag = new TagServices().GetTagByTagName(tagNew.Name);                                                 
                if (currentTag==null)
                {
                    //check name và url cho vietnammoi and vietnambiz
                    if (CmsChannelConfiguration.GetAppSettingInBoolean("AllowCheckNamOrUrlForTag"))
                    {
                        currentTag = new TagServices().GetTagByTagNameOrUrl(tagNew.Name, tagNew.Url);
                        if (currentTag != null)
                        {
                            return this.ToJson(WcfActionResponse.CreateErrorResponse(tagNew.Name + ", " + tagNew.Url + " đã tồn tại 'Tag' tương tự."));
                        }
                    }
                    var result = new TagServices().InsertTag(tagNew, zoneId, "");

                    return this.ToJson(result);
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(currentTag),"Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("auto_suggestion")]
        public HttpResponseMessage AutoSuggestion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("query");
                var isHot = Utility.ConvertToInt(CmsChannelConfiguration.GetAppSetting("SuggestHotTagNewsEdit"));
                var responseData = new WcfActionResponse<List<TagWithSimpleFieldEntity>>();
                var totalRow = 0;
                var tagApproved = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting("ENABLE_TAG_APPROVED"));
                if (tagApproved)
                {
                    var task = Task.Run(() => ExpertSuggestionService.TagSuggestion(keyword, 15, 1, ref totalRow));
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(2)))
                        {
                            var result = task.Result;
                            
                            responseData.Data = result.Select(s => new TagWithSimpleFieldEntity
                            {
                                Id = s.TagChannelId,
                                Name = s.Name,
                                Url = s.Url,
                                UnsignName = s.UnsignName
                            }).ToList();
                            responseData.Success = true;
                            responseData.Message = "Success.";
                        }
                    }
                    catch (Exception ex)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
                    }
                }
                else
                {
                    var task = Task.Run(() => new TagServices().SearchTagForSuggestion(150, -1, keyword, isHot, EnumSearchTagOrder.NewsCountDesc, false));
                    try
                    {
                        if (task.Wait(TimeSpan.FromSeconds(2)))
                        {
                            var result = task.Result;
                                                      
                            responseData.Data = result;
                            responseData.Success = true;
                            responseData.Message = "Success.";
                        }
                    }
                    catch (Exception ex)
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
                    }
                }

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("tag_research")]
        public HttpResponseMessage GetTagResearchV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                               
                
                var content = body.Get("content")??string.Empty;
                var title = body.Get("title")??string.Empty;
                var sapo = body.Get("sapo")??string.Empty;
                var zoneName = body.Get("zoneName")??string.Empty;
                var zoneParentName = body.Get("zoneParentName")??string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var newsId = Utility.ConvertToLong(body.Get("newsId"));

                var minWord = 3;
                var maxWord = 6;

                var concept = new Concept
                {
                    TagTokens = new TagToken[] { }
                };
                
                if (title == "1")
                {
                    var listTagResult = new List<TagToken>();
                    listTagResult.Add(new TagToken()
                    {
                        Name = "Ba Chinh",
                        Rank = 0,
                        IsApproved = true
                    });
                    listTagResult.Add(new TagToken()
                    {
                        Name = "Ba Chinh Đẹp Trai",
                        Rank = 0,
                        IsApproved = true
                    });
                    listTagResult.Add(new TagToken()
                    {
                        Name = "Nguyễn Bá Chính",
                        Rank = 0,
                        IsApproved = true
                    });


                    concept.TagTokens = listTagResult.ToArray();
                    concept.Message = "Test";
                }
                else
                {
                    var listTagResult = new List<TagToken>();
                    var tagApproved = Utility.ConvertToBoolean(CmsChannelConfiguration.GetAppSetting("ENABLE_TAG_APPROVED"));
                    if (tagApproved)
                    {
                        var threadId = GetQueryString.GetPost("threadid", 0);
                        if (threadId > 0)
                        {
                            var taskThread = Task.Run(() => ExpertSuggestionService.IMS_GetTagInThread(threadId));
                            try
                            {
                                if (taskThread.Wait(TimeSpan.FromSeconds(3)))
                                {
                                    var result = taskThread.Result;
                                    int totalRow = result.Count();
                                    for (int i = 0; i < totalRow; i++)
                                    {
                                        listTagResult.Add(new TagToken()
                                        {
                                            Name = result[i].Name,
                                            Rank = 0,
                                            IsApproved = true
                                        });
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                            }
                        }
                    }
                    var task = Task.Run(() => ResearchTagTokenServices.GetTagAll(title, sapo, content, zoneId, newsId, minWord, maxWord));

                    if (task.Wait(TimeSpan.FromSeconds(3)))
                    {
                        var tagData = task.Result;
                        if (tagData != null)
                        {

                            var listContent = tagData.Content;
                            if (listContent != null)
                            {
                                foreach (var tagContent in listContent)
                                {
                                    var wordCount = Utility.CountWords(tagContent.Name);
                                    if (wordCount >= minWord && wordCount <= maxWord)
                                    {
                                        if (!listTagResult.Exists(item => item.Name.Equals(tagContent.Name, StringComparison.OrdinalIgnoreCase)))
                                        {
                                            listTagResult.Add(new TagToken()
                                            {
                                                Name = tagContent.Name,
                                                Rank = tagContent.NewsCount,
                                                IsApproved = true
                                            });
                                        }
                                    }
                                }
                            }

                            var listConcept = tagData.TopicRank;
                            if (listConcept != null)
                            {
                                foreach (var tagConcept in listConcept)
                                {
                                    var wordCount = Utility.CountWords(tagConcept);
                                    if (wordCount >= minWord && wordCount <= maxWord)
                                    {
                                        if (!listTagResult.Exists(item => item.Name.Equals(tagConcept, StringComparison.OrdinalIgnoreCase)))
                                        {
                                            listTagResult.Add(new TagToken()
                                            {
                                                Name = tagConcept,
                                                Rank = 0,
                                                IsApproved = true
                                            });
                                        }
                                    }
                                }
                            }
                            
                            concept.Message = tagData.IdSession.ToString();
                        }


                    }
                    else
                    {
                        Logger.WriteLog(Logger.LogType.Error, "Không lấy được tag gợi ý từ nội dung!");
                    }
                    concept.TagTokens = listTagResult.ToArray();
                }


                return this.ToJson(WcfActionResponse<Concept>.CreateSuccessResponse(concept, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region BoxTagEmbed        

        [HttpPost, Route("list_tag_event")]
        public HttpResponseMessage ListTagEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isThread = Utility.ConvertToInt(body.Get("isThread"));
                var isHotTag = -1;
                int.TryParse(body.Get("isHotTag"), out isHotTag);
                var parentTagId = Utility.ConvertToInt(body.Get("parentTagId"));
                //var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"),20);
                //var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                //EnumSearchTagOrder order;
                //if (!Enum.TryParse(orderBy.ToString(), out order))
                //{
                //    order = EnumSearchTagOrder.CreatedDateDesc;
                //}
                var isTagApproved = Utility.ConvertToBoolean(body.Get("isTagApproved"),false);
                var totalRow = 0;
                var data = new List<TagEntity>();
                if (isTagApproved)
                    data = new TagServices().Tag_SearchInBoxEmbed_Approved(key, isHotTag, pageIndex, pageSize, ref totalRow);
                else
                    data = new TagServices().Tag_SearchInBoxEmbed(key, isHotTag, pageIndex, pageSize, ref totalRow);

                //var data = new TagServices().SearchTag(key, parentTagId, isThread, zoneId, 0, order, isHotTag, pageIndex, pageSize, ref totalRow, false);

                if (data != null && data.Count > 0)
                {
                    var linkFormatTag = CmsChannelConfiguration.GetAppSetting("UrlFormatTag");
                    data.Select(s => { s.Url = string.Format(linkFormatTag, s.Url); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchTagResponse>.CreateSuccessResponse(new SearchTagResponse { TotalRow = totalRow, Tags = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_tag_embed")]
        public HttpResponseMessage GetListTagEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"),0);
                var type = Utility.ConvertToInt(body.Get("type"),0);

                var data = new TagServices().GetListTagEmbed(zoneId, type);
                                
                return this.ToJson(WcfActionResponse<List<BoxTagEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_tag_embed")]
        public HttpResponseMessage UpdateTagEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), 0);
                var listTagId = body.Get("listTagId")??string.Empty;
                var listTitles = body.Get("listTitles")??string.Empty;
                var listUrls = body.Get("listUrls")??string.Empty;

                var response = new TagServices().UpdateTagEmbed(listTagId, zoneId, type);
                if (response.Success == true)
                {
                    response = new TagServices().UpdateTagEmbedExternal(listTagId, zoneId, type, listTitles, listUrls);
                }

                return this.ToJson(response);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region BoxVideoTagEmbed        
        
        [HttpPost, Route("get_video_tag_embed")]
        public HttpResponseMessage GetListVideoTagEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), 0);

                var data = new TagServices().GetListVideoTagEmbed(zoneId, type);

                return this.ToJson(WcfActionResponse<List<BoxTagEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_video_tag_embed")]
        public HttpResponseMessage UpdateVideoTagEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), 0);
                var listTagId = body.Get("listTagId") ?? string.Empty;
                var listTitles = body.Get("listTitles") ?? string.Empty;
                var listUrls = body.Get("listUrls") ?? string.Empty;

                var response = new TagServices().UpdateVideoTagEmbed(listTagId, zoneId, type);
                if (response.Success == true)
                {
                    response = new TagServices().UpdateVideoTagEmbedExternal(listTagId, zoneId, type, listTitles, listUrls);
                }

                return this.ToJson(response);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region BoxTagSeoEmbed
        [HttpPost, Route("list_box_tag_seo_embed")]
        public HttpResponseMessage ListBoxTagSeoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var tagId = Utility.ConvertToLong(body.Get("TagId"));
                var type = Utility.ConvertToInt(body.Get("Type"));

                var data = new TagServices().ListBoxTagSeoEmbed(tagId, type);

                return this.ToJson(WcfActionResponse<List<BoxTagSeoEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_muilt_box_tag_seo_embed")]
        public HttpResponseMessage SaveMultBoxTagSeoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";

                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new TagServices().DeleteBoxTagSeoEmbed(id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<BoxTagSeoEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;
                                var data = new TagServices().InsertBoxTagSeoEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new TagServices().UpdateBoxTagSeoEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not parser list object in ListData."));
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_box_tag_seo_embed")]
        public HttpResponseMessage SaveBoxTagSeoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var tagId = Utility.ConvertToLong(body.Get("TagId"));
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var title = body.Get("Title") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var url = body.Get("Url") ?? string.Empty;
                var description = body.Get("Description") ?? string.Empty;
                var sortOrder = Utility.ConvertToInt(body.Get("SortOrder"), 0);
                var objectId = body.Get("ObjectId")??"";
                var objectType = Utility.ConvertToInt(body.Get("ObjectType"), 0);

                var box = new BoxTagSeoEmbedEntity
                {
                    Id = id,
                    TagId = tagId,
                    Type = type,                    
                    Title = title,
                    Avatar = avatar,
                    Url = url,
                    Description = description,
                    SortOrder = sortOrder,
                    LastModifiedDate = DateTime.Now,
                    ObjectId= objectId,
                    ObjectType= objectType
                };

                if (box.Id <= 0)
                {
                    var result = new TagServices().InsertBoxTagSeoEmbed(box, ref id);

                    return this.ToJson(result);
                }
                else
                {
                    var result = new TagServices().UpdateBoxTagSeoEmbed(box);

                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_box_tag_seo_embed")]
        public HttpResponseMessage UpdateBoxTagSeoEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var result = new TagServices().DeleteBoxTagSeoEmbed(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #endregion
    }
}
