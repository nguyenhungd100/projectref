﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.ProgramSchedule.Entity;
using System;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HtmlAgilityPack;
using Spire.Doc;
using System.Linq;
using System.Collections.Generic;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.Entity.ErrorCode;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/programchannel")]
    public class ProgramChannelController : ApiController
    {
        #region programchannel
        #region action Save
        [HttpPost, Route("save")]
        public HttpResponseMessage SaveProgramChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameProgramNotEmpty]));
                }
                var avatar = body.Get("Avatar") ?? string.Empty;
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var type = Utility.ConvertToInt(body.Get("Type"), 1);                
                var liveTvThumb = body.Get("LiveTvThumb") ?? string.Empty;                                             
                var liveTvEmbed = body.Get("LiveTvEmbed") ?? string.Empty;
                var fullName = body.Get("FullName") ?? string.Empty;
                var zoneVideoId = Utility.ConvertToInt(body.Get("ZoneVideoId"), 0);

                var programChannel = new ProgramChannelEntity
                {
                    Id = id,                    
                    Name = name,
                    Avatar = avatar,
                    Priority = priority,                                                      
                    CreatedBy = accountName,                  
                    LastModifiedBy = accountName,
                    Status = status,
                    Type = type,
                    LiveTvEmbed = liveTvEmbed,
                    LiveTvThumb = liveTvThumb,
                    FullName = fullName,
                    ZoneVideoId=zoneVideoId
                };

                var result = new ProgramScheduleServices().SaveProgramChannel(programChannel);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_program_channel_by_id")]
        public HttpResponseMessage DeleteProgramChannelById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);                           

                var result = new ProgramScheduleServices().DeleteProgramById(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_program_channel_up")]
        public HttpResponseMessage MoveProgramChannelUp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new ProgramScheduleServices().MoveProgramChannelUp(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_program_channel_down")]
        public HttpResponseMessage MoveProgramChannelDown(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new ProgramScheduleServices().MoveProgramChannelDown(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Get

        [HttpPost, Route("get_program_channel_by_id")]
        public HttpResponseMessage GetProgramChannelDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new ProgramScheduleServices().GetProgramChannelDetail(id);
                return this.ToJson(WcfActionResponse<ProgramChannelEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search

        [HttpPost, Route("search_program_channel")]
        public HttpResponseMessage SearchProgramChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("Keyword") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
               
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRows = 0;
                var data = new ProgramScheduleServices().SearchProgramChannel(keyword, status, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, ProgramChannels = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion
        #endregion

        #region ProgramSchedule

        [HttpPost, Route("save_program_schedule")]
        public HttpResponseMessage SaveProgramSchedule(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var scheduleId = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameScheduleNotEmpty]));
                }                
                var avatar = body.Get("Avatar") ?? string.Empty;                
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var programChannelId = Utility.ConvertToInt(body.Get("ProgramChannelId"), 0);
                var playListId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var scheduleDate = Utility.ConvertToDateTimeByFormat(body.Get("ScheduleDate"), "dd/MM/yyyy");                

                var channelEntity = new ProgramScheduleEntity
                {
                    Id= scheduleId,
                    ScheduleName = name,
                    Status = status,
                    ScheduleAvatar = avatar,
                    CreatedBy = accountName,
                    CreatedDate=DateTime.Now,
                    LastModifiedBy= accountName,
                    LastModifiedDate=DateTime.Now,                    
                    ProgramChannelId = programChannelId,
                    ScheduleDate = scheduleDate,                    
                    PlayListId = playListId
                };

                var result = new WcfActionResponse();
                if (scheduleId > 0)
                {
                    result = new ProgramScheduleServices().UpdateProgramSchedule(channelEntity);
                }
                else {
                    result = new ProgramScheduleServices().InsertProgramSchedule(channelEntity, ref scheduleId);
                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_program_schedule")]
        public HttpResponseMessage SearchProgramSchedule(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("Keyword") ?? string.Empty;
                var date = Utility.ConvertToDateTimeByFormat(body.Get("ScheduleDate"),"dd/MM/yyyy");
                var programChannelId = Utility.ConvertToInt(body.Get("ProgramChannelId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);                
                var ScheduleProgramStatus = EnumProgramScheduleStatus.AllStatus;
                if (!Enum.TryParse(status.ToString(), out ScheduleProgramStatus))
                {
                    ScheduleProgramStatus = EnumProgramScheduleStatus.AllStatus;
                }
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRows = 0;
                var data = new ProgramScheduleServices().SearchProgramSchedule(keyword, date, ScheduleProgramStatus, programChannelId, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, ProgramSchedules = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_program_schedule_by_id")]
        public HttpResponseMessage GetProgramScheduleById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new ProgramScheduleServices().GetProgramScheduleByProgramScheduleId(id);
                return this.ToJson(WcfActionResponse<ProgramScheduleEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_program_schedule_by_id")]
        public HttpResponseMessage DeleteProgramScheduleById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var result = new ProgramScheduleServices().DeleteProgramSchedule(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region ProgramScheduleDetail

        [HttpPost, Route("save_program_schedule_detail")]
        public HttpResponseMessage SaveProgramScheduleDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var scheduleDetailId = Utility.ConvertToInt(body.Get("Id"), 0);
                var title = body.Get("Title");
                if (string.IsNullOrEmpty(title))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TitleScheduleNotEmpty]));
                }
                var description = body.Get("Description") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var programScheduleId = Utility.ConvertToInt(body.Get("ProgramScheduleId"), 0);
                var playListId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var scheduleTime = Utility.ConvertToDateTimeByFormat(body.Get("ScheduleTime"), "dd/MM/yyyy");
                var showOnSchedule = Utility.ConvertToBoolean(body.Get("ShowOnSchedule"), true);
                var videoId = Utility.ConvertToInt(body.Get("VideoId"), 0);
                var zoneVideoId = Utility.ConvertToInt(body.Get("ZoneVideoId"), 0);

                var scheduleDetailEntity = new ProgramScheduleDetailEntity
                {
                    Id= scheduleDetailId,
                    Title = title,
                    Avatar = avatar,
                    Status = status,                    
                    CreatedBy = accountName,
                    CreatedDate=DateTime.Now,
                    ScheduleTime = scheduleTime,
                    ProgramScheduleId = programScheduleId,
                    Description = description,
                    VideoId = videoId,
                    ShowOnSchedule = showOnSchedule,
                    ZoneVideoId = zoneVideoId,
                    LastModifiedBy= accountName,
                    LastModifiedDate=DateTime.Now                    
                };

                var result = new WcfActionResponse();
                if (scheduleDetailId > 0)
                {
                    result = new ProgramScheduleServices().UpdateProgramScheduleDetail(scheduleDetailEntity);
                }
                result = new ProgramScheduleServices().InsertProgramScheduleDetail(scheduleDetailEntity, ref scheduleDetailId);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_program_schedule_detail")]
        public HttpResponseMessage SearchProgramScheduleDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var programScheduleId = Utility.ConvertToInt(body.Get("ProgramScheduleId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var scheduleDetailStatus = EnumProgramScheduleDetailStatus.AllStatus;
                if (!Enum.TryParse(status.ToString(), out scheduleDetailStatus))
                {
                    scheduleDetailStatus = EnumProgramScheduleDetailStatus.AllStatus;
                }
                
                var totalRows = 0;
                var data = new ProgramScheduleServices().GetProgramScheduleDetailByProgramScheduleId(programScheduleId, scheduleDetailStatus);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, ProgramScheduleDetails = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_program_schedule_detail_by_id")]
        public HttpResponseMessage GetProgramScheduleDetailById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
               
                var data = new ProgramScheduleServices().GetProgramScheduleDetailByProgramScheduleDetailId(id);
                return this.ToJson(WcfActionResponse<ProgramScheduleDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_program_schedule_detail_by_id")]
        public HttpResponseMessage DeleteProgramScheduleDetailById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var result = new ProgramScheduleServices().DeleteProgramScheduleDetail(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_list_program_schedule_detail")]
        public HttpResponseMessage SaveListProgramScheduleDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                
                var listOject = body.Get("ListObject")??string.Empty;
                if (string.IsNullOrEmpty(listOject))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var deleteIds = body.Get("DeleteIds") ?? string.Empty;
                
                var listProgramScheduleDetailEntity = NewtonJson.Deserialize<List<ProgramScheduleDetailEntity>>(listOject);                
                var result = new ProgramScheduleServices().InsertListProgramScheduleDetail(listProgramScheduleDetailEntity, deleteIds);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_program_schedule_detail_by_date")]
        public HttpResponseMessage GetProgramScheduleDetailByDate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var programScheduleId = Utility.ConvertToInt(body.Get("ProgramChannelId"), 0);
                var scheduleDate = Utility.ConvertToDateTimeByFormat(body.Get("ScheduleDate"), "dd/MM/yyyy");
                
                var data = new ProgramScheduleServices().GetProgramScheduleDetailByScheduleDate(programScheduleId, scheduleDate, EnumProgramScheduleDetailStatus.Actived);
                return this.ToJson(WcfActionResponse<List<ProgramScheduleDetailEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region ImportWordFile

        [HttpPost, Route("import_api_vtv")]
        public HttpResponseMessage ImportProgramScheduleDetailApiVtv(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var dateTime = body.Get("Datetime");
                if (string.IsNullOrEmpty(dateTime))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var date = Utility.ConvertToDateTimeByFormat(dateTime,"dd/MM/yyyy");
                var vtvId = Utility.ConvertToInt(body.Get("VtvProgramChannelId"),0);
                                
                var data = VtvServices.ImportProgramScheduleDetailApiVtv(date, vtvId);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, ProgramScheduleDetails = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("import_word_file")]
        public HttpResponseMessage ImportWordFile()
        {

            var outputHtml = "";
            var docFile = HttpContext.Current.Request.Files["WordFile"];
            var channelId = HttpContext.Current.Request["VtvProgramChannelId"];
            var datetime = HttpContext.Current.Request["Datetime"];
            try
            {
                if (docFile != null && !string.IsNullOrEmpty(docFile.FileName))
                {
                    var docTempFile =
                        HttpContext.Current.Server.MapPath("/Data/Temp/" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + docFile.FileName);

                    // if doc file => use Spire.Doc convert it to docx file to process
                    if (docTempFile.EndsWith(".doc", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docTempFile = docTempFile + "x";
                        var document = new Document(docFile.InputStream) { JPEGQuality = 100 };
                        document.SaveToFile(docTempFile, FileFormat.Docx);
                        document.Close();
                    }
                    else if (docTempFile.EndsWith(".odt", StringComparison.CurrentCultureIgnoreCase))
                    {
                        docTempFile = docTempFile.Substring(0, docTempFile.LastIndexOf(".", StringComparison.Ordinal)) + ".docx";
                        var document = new Document(docFile.InputStream) { JPEGQuality = 100 };
                        document.SaveToFile(docTempFile, FileFormat.Docx);
                        document.Close();
                    }
                    else
                    {
                        docFile.SaveAs(docTempFile);
                    }
                    if (File.Exists(docTempFile))
                    {
                        if (Utility.ConvertToHtml(docTempFile, ref outputHtml))
                        {
                            File.Delete(docTempFile);
                            // Remove Evaluation text if use Spire.Doc
                            outputHtml = outputHtml.Replace("<p>Evaluation Warning : The document was created with Spire.Doc for .NET.</p>", "");                            
                            var contentHtml = "";
                            switch (channelId)
                            {
                                case "1":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(1, "VTV1", datetime, outputHtml);
                                    break;
                                case "2":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(2, "VTV2", datetime, outputHtml);
                                    break;
                                case "3":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(3, "VTV3", datetime, outputHtml);
                                    break;
                                case "4":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(4, "VTV4", datetime, outputHtml);
                                    break;
                                case "5":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(5, "VTV5", datetime, outputHtml);
                                    break;
                                case "6":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(6, "VTV6", datetime, outputHtml);
                                    break;
                                case "7":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(7, "VTV7", datetime, outputHtml);
                                    break;
                                case "8":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(8, "VTV8", datetime, outputHtml);
                                    break;
                                case "9":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(9, "VTV9", datetime, outputHtml);
                                    break;
                                case "10":
                                    contentHtml = GetFormHtmlVTVLive(10, "VTVLive", datetime, outputHtml);
                                    break;
                                case "11":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(11, "VTVCab2", datetime, outputHtml);
                                    break;
                                case "12":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(12, "VTVCab4", datetime, outputHtml);
                                    break;
                                case "13":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(13, "VTVCab6", datetime, outputHtml);
                                    break;
                                case "14":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(14, "VTVCab8", datetime, outputHtml);
                                    break;
                                case "15":
                                    contentHtml = GetFormHtmlKPlus(15, "K+1", outputHtml);
                                    break;
                                case "16":
                                    contentHtml = GetFormHtmlKPlus(16, "K+NS", outputHtml);
                                    break;
                                case "17":
                                    contentHtml = GetFormHtmlKPlus(17, "K1W35", outputHtml);
                                    break;
                                case "18":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(18, "VTVCab1", datetime, outputHtml);
                                    break;
                                case "19":
                                    contentHtml = GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(19, "VTVCab3", datetime, outputHtml);
                                    break;
                                case "20":
                                    contentHtml = GetFormHtmlCab7Cab17(20, "VTVCab7", datetime, outputHtml);
                                    break;
                                case "21":
                                    contentHtml = GetFormHtmlCab7Cab17(21, "VTVCab17", datetime, outputHtml);
                                    break;
                                case "22":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(22, "VTV Cần Thơ 1", datetime, outputHtml);
                                    break;
                                case "23":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(23, "VTV Cần Thơ 2", datetime, outputHtml);
                                    break;
                                case "24":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(24, "VTV Đà nẵng", datetime, outputHtml);
                                    break;
                                case "25":
                                    contentHtml = GetFormHtmlV1V2V3V4V6(25, "VTV5 Tây Nam Bộ", datetime, outputHtml);
                                    break;
                                    //case "26":
                                    //    contentHtml = GetFormHtmlVTVLive(26, "VTVLive", datetime, outputHtml);
                                    //    break;
                            }
                            return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Content = contentHtml }, "Success."));
                        }
                    }
                    File.Delete(docTempFile);
                }
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotSelectFileUpload]));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        public string GetFormHtmlV1V2V3V4V6(int channelid, string channelname, string datetime, string outputHtml)
        {
            try
            {
                var zoneVideos = new ProgramScheduleServices().GetZoneVideo();
                var message = "";
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(outputHtml);
                // List table
                var listtable = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "table");
                var listtableArray = listtable as HtmlNode[] ?? listtable.ToArray();
                for (var i = 0; i < listtableArray.Count(); i++)
                {
                    var listtr = listtableArray[i].Descendants().Where(n => n.Name == "tr");
                    var listtrArray = listtr as HtmlNode[] ?? listtr.ToArray();
                    for (var j = 0; j < listtrArray.Count(); j++)
                    {
                        var listtd = listtrArray[j].Descendants().Where(n => n.Name == "td");
                        var enumerable = listtd as HtmlNode[] ?? listtd.ToArray();
                        // Check zonevideo
                        var title = enumerable[1].InnerText.Trim();
                        var zoneid = 0;
                        var classSelect = "";
                        var zonenameSelect = "Chuyên mục";
                        zoneVideos = zoneVideos.Where(c => c.ProgramChannelId == channelid).ToList();
                        for (var k = 0; k < zoneVideos.Count; k++)
                        {
                            var zonename = zoneVideos[k].ZoneName.ToLower();
                            var keyword = zoneVideos[k].Keyword.ToLower();
                            if (title.ToLower().Contains(zonename))
                            {
                                zoneid = zoneVideos[k].ZoneId;
                                classSelect = "select";
                                zonenameSelect = zoneVideos[k].ZoneName;
                                break;
                            }
                            var keyslpit = keyword.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            for (var t = 0; t < keyslpit.Count(); t++)
                            {
                                var key = keyslpit[t].ToLower();
                                if (title.ToLower().Contains(key))
                                {
                                    zoneid = zoneVideos[k].ZoneId;
                                    classSelect = "select";
                                    zonenameSelect = zoneVideos[k].ZoneName;
                                    break;
                                }
                            }
                        }
                        //-----------------
                        htmltr += string.Format(htmltrFormat, datetime, enumerable[0].InnerText.Trim().Replace('h', ':'), enumerable[1].InnerText.Trim(), enumerable[2].InnerText.Trim(), zoneid, classSelect, zonenameSelect, "");
                    }
                    //Get html Message
                    message += GetMessageDateDuplicate(datetime, channelid, channelname);
                    //---
                    htmltable += GetSelectHtmlZoneVideo(channelid, zoneVideos) + string.Format(htmlchannelFormat, channelname, channelid) + string.Format(htmltableFormat, htmltr, channelid);
                    htmltr = "";
                }
                if (message != "")
                {
                    htmlMessage = string.Format(htmlMessageBox, message);
                }
                return htmlMessage + htmltable;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }

        public string GetFormHtmlVTVLive(int channelid, string channelname, string datetime, string outputHtml)
        {
            try
            {
                var zoneVideos = new ProgramScheduleServices().GetZoneVideo();
                var message = "";
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(outputHtml);
                // List table
                var listtable = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "table");
                var listtableArray = listtable as HtmlNode[] ?? listtable.ToArray();
                for (var i = 0; i < listtableArray.Count(); i++)
                {
                    var listtr = listtableArray[i].Descendants().Where(n => n.Name == "tr");
                    var listtrArray = listtr as HtmlNode[] ?? listtr.ToArray();
                    for (var j = 0; j < listtrArray.Count(); j++)
                    {
                        var listtd = listtrArray[j].Descendants().Where(n => n.Name == "td");
                        var enumerable = listtd as HtmlNode[] ?? listtd.ToArray();
                        // Check zonevideo
                        var title = enumerable[1].InnerText.Trim();
                        var externalProgram = enumerable[3].InnerText.Trim() != "" ? enumerable[3].InnerText.Trim() : "...";
                        var zoneid = 0;
                        var classSelect = "";
                        var zonenameSelect = "Chuyên mục";
                        zoneVideos = zoneVideos.Where(c => c.ProgramChannelId == channelid).ToList();
                        for (var k = 0; k < zoneVideos.Count; k++)
                        {
                            var zonename = zoneVideos[k].ZoneName.ToLower();
                            var keyword = zoneVideos[k].Keyword.ToLower();
                            if (title.ToLower().Contains(zonename))
                            {
                                zoneid = zoneVideos[k].ZoneId;
                                classSelect = "select";
                                zonenameSelect = zoneVideos[k].ZoneName;
                                break;
                            }
                            var keyslpit = keyword.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                            for (var t = 0; t < keyslpit.Count(); t++)
                            {
                                var key = keyslpit[t].ToLower();
                                if (title.ToLower().Contains(key))
                                {
                                    zoneid = zoneVideos[k].ZoneId;
                                    classSelect = "select";
                                    zonenameSelect = zoneVideos[k].ZoneName;
                                    break;
                                }
                            }
                        }
                        //-----------------
                        htmltr += string.Format(htmltrFormat, datetime, enumerable[0].InnerText.Trim().Replace('h', ':'), enumerable[1].InnerText.Trim(), enumerable[2].InnerText.Trim(), zoneid, classSelect, zonenameSelect, externalProgram);
                    }
                    //Get html Message
                    message += GetMessageDateDuplicate(datetime, channelid, channelname);
                    //---
                    htmltable += GetSelectHtmlZoneVideo(channelid, zoneVideos) + string.Format(htmlchannelFormat, channelname, channelid) + string.Format(htmltableFormat, htmltr, channelid);
                    htmltr = "";
                }
                if (message != "")
                {
                    htmlMessage = string.Format(htmlMessageBox, message);
                }
                return htmlMessage + htmltable;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }

        public string GetFormHtmlCab1Cab2Cab3Cab4Cab6Cab8(int channelid, string channelname, string datetime, string outputHtml)
        {
            try
            {
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(outputHtml);

                var listP = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "p");
                var listPArray = listP as HtmlNode[] ?? listP.ToArray();

                for (var i = 0; i < listPArray.Count(); i++)
                {
                    var str = listPArray[i].InnerText.Trim();
                    if (!string.IsNullOrEmpty(str))
                    {
                        var slpit = str.Split(new[] { "@@@@@" }, StringSplitOptions.None);
                        if (slpit[0].Trim() != "Giờ")
                        {
                            var hour = slpit[0].Replace('h', ':');
                            var program = slpit[1];
                            var programdetail = "";
                            if (slpit.Count() > 2)
                            {
                                programdetail = slpit[2];
                            }
                            htmltr += string.Format(htmltrFormat, datetime, hour, program, programdetail, 0, "", "Chuyên mục", "");
                        }
                    }
                }
                //Get html Message
                var message = GetMessageDateDuplicate(datetime, channelid, channelname);
                if (message != "")
                {
                    htmlMessage = string.Format(htmlMessageBox, message);
                }
                //---
                htmltable = string.Format(htmlchannelFormat, channelname, channelid) + string.Format(htmltableFormat, htmltr, channelid);
                return htmlMessage + htmltable;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }
        public string GetFormHtmlCab7Cab17(int channelid, string channelname, string datetime, string outputHtml)
        {
            try
            {
                var zoneVideos = new ProgramScheduleServices().GetZoneVideo();
                var message = "";
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(outputHtml);
                // List table
                var listtable = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "table");
                var listtableArray = listtable as HtmlNode[] ?? listtable.ToArray();

                for (var i = 0; i < listtableArray.Count(); i++)
                {
                    var listtr = listtableArray[i].Descendants().Where(n => n.Name == "tr");
                    var listtrArray = listtr as HtmlNode[] ?? listtr.ToArray();
                    for (var j = 0; j < listtrArray.Count(); j++)
                    {
                        var listtd = listtrArray[j].Descendants().Where(n => n.Name == "td");
                        var enumerable = listtd as HtmlNode[] ?? listtd.ToArray();
                        htmltr += string.Format(htmltrFormat, datetime, enumerable[0].InnerText.Trim().Replace('h', ':'),
                            enumerable[1].InnerText.Trim(), enumerable[2].InnerText.Trim(), 0, "", "Chuyên mục", "");
                    }
                    //Get html Message
                    message += GetMessageDateDuplicate(datetime, channelid, channelname);
                    //---
                    htmltable += GetSelectHtmlZoneVideo(channelid, zoneVideos) + string.Format(htmlchannelFormat, channelname, channelid) + string.Format(htmltableFormat, htmltr, channelid);
                    htmltr = "";
                }
                if (message != "")
                {
                    htmlMessage = string.Format(htmlMessageBox, message);
                }
                return htmlMessage + htmltable;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }
        public string GetSelectHtmlZoneVideo(int channelid, List<ProgramChannelZoneVideoEntity> zoneVideos)
        {
            zoneVideos = zoneVideos.Where(c => c.ProgramChannelId == channelid).ToList();
            var itemall = "<div class='VMZoneVideoItem' data-zonevideoid='0'>Chuyên mục</div>";
            var itemformat = "<div class='VMZoneVideoItem' data-zonevideoid='{0}'>{1}</div>";
            var htmlItem = "";
            var html = "<div id='VMImportZoneVideo_{0}' class='VMImportZoneVideo'>{1}</div>";
            for (var i = 0; i < zoneVideos.Count; i++)
            {
                if (i == 0) { htmlItem += itemall; }
                htmlItem += string.Format(itemformat, zoneVideos[i].ZoneId, zoneVideos[i].ZoneName);
            }
            if (!string.IsNullOrEmpty(htmlItem))
            {
                return string.Format(html, channelid, htmlItem);
            }
            return "";
        }

        public string GetMessageDateDuplicate(string listDate, int channelid, string channelname)
        {
            var message = "";
            // Check tồn tại
            if (string.IsNullOrEmpty(listDate)) return message;
            var list = new ProgramScheduleServices().CheckExistListProgramScheduleByDate(listDate, channelid);
            if (list.Count <= 0) return message;
            var strMessage = "";
            foreach (var programScheduleEntity in list)
            {
                strMessage += programScheduleEntity.ScheduleDate.ToString("dd/MM/yyyy") + ", ";
            }
            strMessage = "- " + channelname + ": " + strMessage.Substring(0, strMessage.Length - 2);
            message = string.Format(htmlMessageFormat, strMessage);
            return message;
        }

        public string GetFormHtmlKPlus(int channelid, string channelname, string outputHtml)
        {
            try
            {
                var zoneVideos = new ProgramScheduleServices().GetZoneVideo();
                var strDate = "";
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.OptionCheckSyntax = false;
                htmlDoc.LoadHtml(outputHtml);
                var listtr = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "tr");
                var htmlNodes = listtr as HtmlNode[] ?? listtr.ToArray();
                for (var i = 0; i < htmlNodes.Count(); i++)
                {
                    var listtd = htmlNodes[i].Descendants().Where(n => n.Name == "td");
                    var enumerable = listtd as HtmlNode[] ?? listtd.ToArray();
                    if (enumerable[0].InnerText.Trim() == "Ngày") continue;
                    if (!strDate.Contains(enumerable[0].InnerText.Trim()))
                    {
                        strDate += enumerable[0].InnerText.Trim() + ",";
                    }
                    htmltr += string.Format(htmltrFormat, enumerable[0].InnerText.Trim(), enumerable[1].InnerText.Trim(),
                        enumerable[2].InnerText.Trim(), enumerable[3].InnerText.Trim(), 0, "", "Chuyên mục", "");
                }
                //Get html Message
                var message = GetMessageDateDuplicate(strDate, channelid, channelname);
                if (message != "")
                {
                    htmlMessage = string.Format(htmlMessageBox, message);
                }
                //---
                htmltable = GetSelectHtmlZoneVideo(channelid, zoneVideos) + string.Format(htmlchannelFormat, channelname, channelid) + string.Format(htmltableFormat, htmltr, channelid);
                return htmlMessage + htmltable;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Fatal, ex.ToString());
            }
            return "";
        }

        private string htmlMessage = "";

        private string htmlMessageBox =
            "<div class='VMImportMessage'><div class='VMImportMessageTitle'>Lịch chương trình của các kênh theo ngày dưới đây đã tồn tại. Nếu tiếp tục xử lý, hệ thống sẽ ghi đè lại lịch của các ngày này. Bấm lưu lịch phát sóng nếu muốn ghi đè.</div>{0}</div>";

        private string htmlMessageFormat = "<div class='VMImportMessageDetail'>{0}</div>";
        private string htmlchannelFormat = "<div class='VMChannelName' data-channelid='{1}'>{0}</div>";
        private string htmltable = "";
        private string htmltableFormat = "<table cellspacing='0' class='VMChannelNameTable-{1}'><tbody>{0}</tbody></table>";
        private string htmltr = "";

        private string htmltrFormat =
            "<tr><td class='VMProgramDay'><p>{0}</p></td><td class='VMProgramHour'><p>{1}</p></td><td class='VMProgramTitle'><p>{2}</p></td><td class='VMProgramDescription'><p>{3}</p></td><td class='VMProgramExternalProgram'><p>{7}</p></td><td class='VMProgramZoneVideo' data-zonevideoid='{4}'><p class='{5}'>{6}</p></td></tr>";

        #endregion
    }
}