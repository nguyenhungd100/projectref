﻿using ChannelVN.CMS.BO.Base.News;
using ChannelVN.CMS.BoCached.Entity.Queue;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{    
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/sharevideo")]
    public class ShareVideoController : ApiController
    {       
        #region Video
        
        [HttpPost, Route("push_video")]
        public HttpResponseMessage PushVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                
                var AlowShareVideoUserNamePublishNews = CmsChannelConfiguration.GetAppSetting("AlowShareVideoUserNamePublish");
                if (!string.IsNullOrEmpty(accountName) && !accountName.Equals(AlowShareVideoUserNamePublishNews))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tài khoản không có quyền sử dụng api này."));
                }

                var sessionId = body.Get("SessionId");
                var playload= body.Get("PayLoad");
                var action = body.Get("Action")?? "publish";

                if (string.IsNullOrEmpty(playload))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy PlayLoad not empty."));
                }
                var paserPlayload = NewtonJson.Deserialize<PayLoadShareVideoEntity>(playload);
                if (paserPlayload == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("not paser playLoad obj."));
                }
                var channelName = WcfExtensions.WcfMessageHeader.Current.Namespace;

                //nhận vào queue
                var keyQueueId = "sharevideo_pushvideo";                
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueShareVideo(keyQueueId, sessionId, action, channelName, paserPlayload);
                if (res > 0)
                {
                    var AlowShareVideoPublish = CmsChannelConfiguration.GetAppSettingInBoolean("AlowShareVideoPublish");
                    //start job =>xử lý pop queue
                    TaskJobQueueShareVideo(keyQueueId, accountName, AlowShareVideoPublish);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "push video in queue success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error push video data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private void TaskJobQueueShareVideo(string keyId, string accountName, bool AlowShareVideoPublish)
        {
            var context = HttpContext.Current;
            var thread = new Thread(() =>
            {
                HttpContext.Current = context;
                while (true)
                {
                    //Thread.Sleep(2000);

                    var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueueShareVideo(keyId);

                    if (queue != null)
                    {
                        Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(queue));
                        //BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueShareVideo(keyId + "_log", queue.SessionId, queue.Action, queue.ChannelName, queue.PlayLoad);

                        var result = new WcfActionResponse();
                        foreach (var payload in queue.PayLoad.Data) {
                            if (payload != null)
                            {
                                payload.Video.Id = 0;
                                payload.Video.Status = (int)EnumVideoStatus.Published;
                                if (!AlowShareVideoPublish)
                                {
                                    payload.Video.Status = (int)EnumVideoStatus.ShareVideo;
                                }                                
                                payload.Video.Type = (int)EnumVideoType.Default;
                                payload.Video.CreatedBy = accountName;
                                payload.Video.CreatedDate = DateTime.Now;
                            }
                            result = new MediaServices().SaveVideoV4(payload.Video, payload.TagIdList, payload.ZoneIdList, payload.PlaylistIdList, "", new List<string>());
                        }                        
                        if (!result.Success)
                        {
                            //Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));
                            BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueShareVideo(keyId + "_log", queue.SessionId, queue.Action, queue.ChannelName, queue.PayLoad);

                            //var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueueShareVideo(keyId, queue.SessionId, queue.Action, queue.ChannelName, queue.PlayLoad);
                            //if (res > 0)
                            //{
                            //    break;

                            //    //start job =>xử lý pop queue
                            //    //TaskJobQueue(keyId);
                            //}
                        }
                    }

                    if (queue == null)
                    {
                        break;
                    }
                }

            });

            thread.Start();

        }

        #endregion        
    }
}
