﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.Entity.Base.Security;
using System.Text.RegularExpressions;
using ChannelVN.CMS.BoCached.Entity.Security;
using ChannelVN.CMS.Entity.Base.AuthenticateLog;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.Entity.ErrorCode;
using System.IO;
using ChannelVN.CMS.Common.ChannelConfig;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/security")]
    public class SecurityController : ApiController
    {
        #region UserDataBo

        [AllowAnonymous]
        [HttpPost, Route("valid_account")]
        public HttpResponseMessage ValidAccount(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;

            var username = body.Get("username");
            var password = body.Get("password");
            var secretKey = body.Get("secret_key");

            var userInfo = new UserCachedEntity();
            var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
            {
                var result = new SecurityServices().ValidAccount(username, password, ref userInfo);                
                result.Data = userInfo;
                return this.ToJson(result);
            }
            else {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
            }            
        }

        [HttpPost, Route("check_user")]
        public HttpResponseMessage CheckUser(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;

            var username = body.Get("username");

            var result = new SecurityServices().CheckUser(username);
            return this.ToJson(result);
        }

        [HttpPost, Route("valid_account_with_md5_encrypt_password")]
        public HttpResponseMessage ValidAccountWithMd5EncryptPassword(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;

            var username = body.Get("username");
            var password = body.Get("password");

            var result = new SecurityServices().ValidAccountWithMd5EncryptPassword(username, password);
            return this.ToJson(result);
        }

        [HttpPost, Route("valid_sms_code")]
        public HttpResponseMessage ValidSmsCode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = body.Get("username");
                var smsCode = body.Get("smsCode");

                var result = new SecurityServices().ValidSmsCode(username, smsCode);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("reset_password")]
        public HttpResponseMessage ResetPassword(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var encryptUserId = body.Get("userid");
                var newPassword = body.Get("pwd");

                var result = new SecurityServices().ResetPassword(encryptUserId, newPassword, accountName);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("reset_my_password")]
        public HttpResponseMessage ResetMyPassword(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = body.Get("accountname");
                var newPassword = body.Get("newpass");
                var cfNewPassword = body.Get("cfnewpass");
                var oldPassword = body.Get("oldpass");

                var result = new SecurityServices().ResetMyPassword(accountName, newPassword, cfNewPassword, oldPassword);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("update_user_profile")]
        public HttpResponseMessage UpdateUserProfile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var encryptUserId = body.Get("userid");
                var fullName = body.Get("fullname");
                var username = body.Get("username");
                if (string.IsNullOrWhiteSpace(username))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("UserName not empty."));
                }
                var password = body.Get("password");
                var address = body.Get("address");
                var description = body.Get("description");
                var avatar = body.Get("avatar");
                var mobile = body.Get("mobile");
                var email = body.Get("email");
                var birthday = Utility.ConvertToDateTime(body.Get("birthday"));
                var status = Utility.ConvertToInt(body.Get("status"),0);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var staffcode = body.Get("staffcode");
                if (!string.IsNullOrEmpty(staffcode) && staffcode.Length > 20)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.CodeUserIsGreaterThan20Characte]));
                }
                var telegramId = Utility.ConvertToLong(body.Get("telegramId"));
                var departmentId = Utility.ConvertToInt(body.Get("departmentId"), 0);

                var user = new UserEntity
                {
                    Id = 0,
                    EncryptId = encryptUserId,
                    Password = password,
                    UserName = username.ToLower(),
                    FullName = fullName,
                    Avatar = avatar,
                    Email = email,
                    Mobile = mobile,
                    Status = status,
                    Address = address,
                    Birthday = birthday,
                    Description = description,
                    StaffCode=staffcode,
                    TelegramId = telegramId,
                    DepartmentId = departmentId,
                    ModifiedDate = DateTime.Now
                };
                if (!string.IsNullOrEmpty(encryptUserId))
                {
                    var result = new SecurityServices().UpdateUserProfile(user, accountName);
                    return this.ToJson(result);
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("not found encryptUserId."));
                }
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("add_new_user_profile")]
        public HttpResponseMessage AddnewUserProfile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var encryptUserId = body.Get("userid");
                var fullName = body.Get("fullname");
                var username = body.Get("username");
                if (string.IsNullOrWhiteSpace(username))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("UserName not empty."));
                }
                var password = body.Get("password");                
                var address = body.Get("address");
                var description = body.Get("description");
                var avatar = body.Get("avatar");
                var mobile = body.Get("mobile");
                var email = body.Get("email");
                var birthday = Utility.ConvertToDateTime(body.Get("birthday"));
                var status = Utility.ConvertToInt(body.Get("status"),0);
                var staffcode = body.Get("staffcode");
                if(!string.IsNullOrEmpty(staffcode) && staffcode.Length > 20)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.CodeUserIsGreaterThan20Characte]));
                }
                var telegramId = Utility.ConvertToLong(body.Get("telegramId"));
                var departmentId = Utility.ConvertToInt(body.Get("departmentId"), 0);
                //auto set quyền khi tạo user theo templateid
                //var templateid = Utility.ConvertToInt(body.Get("templateid"),0);

                var user = new UserEntity
                {
                    Id = 0,
                    EncryptId = encryptUserId,
                    Password = password,
                    UserName = username.ToLower(),//anh Hưng bảo fix tolower
                    FullName = fullName,
                    Avatar = avatar,
                    Email = email,
                    Mobile = mobile,
                    Status = status,
                    Address = address,
                    Birthday = birthday,
                    Description = description,
                    StaffCode=staffcode,
                    TelegramId= telegramId,
                    DepartmentId= departmentId,
                    CreatedDate=DateTime.Now,
                    LastChangePass=DateTime.Now,
                    ModifiedDate=DateTime.Now
                };

                int userId = 0;
                var result = new SecurityServices().AddnewUserProfile(user, accountName, ref userId);
                                
                result.Data = NewtonJson.Serialize(new { UserId=userId, EncryptId = CryptonForId.EncryptId(userId) }); //CryptonForId.EncryptId(userId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_avatar")]
        public HttpResponseMessage UpdateAvatar(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = body.Get("accountname");
                var avatar = body.Get("avatar");

                var result = new SecurityServices().UpdateAvatar(accountName, avatar);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("get_user_by_userid")]
        public HttpResponseMessage GetUserByUserId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var encryptUserId = body.Get("userid");
                //var isGetChildZone = Convert.ToBoolean(body.Get("isGetChildZone"));

                var service = new SecurityServices();
                var data = service.GetUserByUserId(encryptUserId);
                if(data!=null)
                    data.Id = 0;

                var listUserPermission = service.GetPermisionByUserId(encryptUserId, true);
                var userCached = new UserCachedEntity
                {
                    User = data,
                    PermissionList = listUserPermission
                };

                return this.ToJson(WcfActionResponse<UserCachedEntity>.CreateSuccessResponse(userCached, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ex.Message));
            }
        }

        [HttpPost, Route("get_user_by_username")]
        public HttpResponseMessage GetUserByUsername(string username)
        {
            var result = new SecurityServices().GetUserByUsername(username);
            return this.ToJson(result);
        }

        [AllowAnonymous]
        [HttpPost, Route("update_otp_secret_key_for_username")]
        public HttpResponseMessage UpdateOtpSecretKeyForUsername(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;
            var username = body.Get("username");
            var otpSecretKey = body.Get("otpSecretKey");
            var secretKey = body.Get("secretKey");
            var np = body.Get("namespace");
            var lang = body.Get("lang");

            if (!string.IsNullOrEmpty(np))
                WcfExtensions.WcfMessageHeader.Current.Namespace = np;
            if (!string.IsNullOrEmpty(lang))
                WcfExtensions.WcfMessageHeader.Current.Language = lang;

            var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
            {
                var result = new SecurityServices().UpdateOtpSecretKeyForUsername(username, otpSecretKey);
                return this.ToJson(result);
            }
            else {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
            }
        }                

        [AllowAnonymous]
        [HttpPost, Route("sendotp")]
        public HttpResponseMessage SendOtp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var otp = body.Get("otp");
                var telegramId = body.Get("telegramid");
                var secretKey = body.Get("secretkey");
                var np = body.Get("namespace");
                var lang = body.Get("lang");

                if (!string.IsNullOrEmpty(np))
                    WcfExtensions.WcfMessageHeader.Current.Namespace = np;
                if (!string.IsNullOrEmpty(lang))
                    WcfExtensions.WcfMessageHeader.Current.Language = lang;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {                
                    var result = OtpSend(otp, telegramId, np, secretKey);

                    if (result == "1")
                        return this.ToJson(WcfActionResponse.CreateErrorResponse("Not config url appbot"));                    

                    if (result != string.Empty)
                        return this.ToJson(WcfActionResponse.CreateSuccessResponse(result, "Success"));

                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Error send OTP."));
                }
                else {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
                }                               
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private string OtpSend(string otp, string telegramId, string np, string secretKey)
        {
            try
            {                
                var urlApiBot = CmsChannelConfiguration.GetAppSetting("AppBotTelegramApiUrl");
                if (string.IsNullOrEmpty(urlApiBot))
                {
                    return "1";
                }

                if (!string.IsNullOrEmpty(otp) && !string.IsNullOrEmpty(telegramId))
                {
                    var action = "otp/send";
                    secretKey = Crypton.Md5Encrypt(secretKey);
                    string url = string.Format(urlApiBot + action + "?secretkey={0}&otp={1}&telegramid={2}&np={3}", secretKey, otp, telegramId, np);

                    Logger.WriteLog(Logger.LogType.Trace, "Begin OtpSend: AppBotTelegramApiUrl => " + url);

                    WebRequest webrequest = WebRequest.Create(url);
                    webrequest.Credentials = CredentialCache.DefaultCredentials;
                    webrequest.Timeout = 10000;
                    WebResponse response = webrequest.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    var result = reader.ReadToEnd();

                    Logger.WriteLog(Logger.LogType.Trace, "End OtpSend: AppBotTelegramApiUrl => " + url);

                    reader.Close();
                    reader.Dispose();
                    response.Close();
                    response.Dispose();

                    return result;
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "OtpSend => " + ex.Message);
                return string.Empty;
            }
        }

        [HttpPost, Route("is_user_has_permission_in_zone")]
        public HttpResponseMessage IsUserHasPermissionInZone(string username, EnumPermission permissionId, int zoneId)
        {
            var result = new SecurityServices().IsUserHasPermissionInZone(username, permissionId, zoneId);
            return this.ToJson(result);
        }

        [HttpPost, Route("is_user_in_group_permission")]
        public HttpResponseMessage IsUserInGroupPermission(string username, GroupPermission groupPermissionId)
        {
            var result = new SecurityServices().IsUserInGroupPermission(username, groupPermissionId);
            return this.ToJson<WcfActionResponse>(result);
        }

        [HttpPost, Route("user_change_status")]
        public HttpResponseMessage User_ChangeStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var userName = body.Get("username");
                var status = (UserStatus)Convert.ToInt32(body.Get("status"));

                var result = new SecurityServices().User_ChangeStatus(userName, status, accountName);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("get_user_with_permission_detail_by_userid")]
        public HttpResponseMessage GetUserWithPermissionDetailByUserId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var encryptUserId = body.Get("userid");
                var isGetChildZone = Convert.ToBoolean(body.Get("isGetChildZone"));

                var data = new SecurityServices().GetUserWithPermissionDetailByUserId(encryptUserId, isGetChildZone);

                return this.ToJson(WcfActionResponse<UserWithPermissionDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("get_otp_by_user")]
        public HttpResponseMessage GetOtpSecretKeyByUsername(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = body.Get("username");
                var secretKey = body.Get("secret_key");
                var np = body.Get("namespace");
                var lang = body.Get("lang");

                if (!string.IsNullOrEmpty(np))
                    WcfExtensions.WcfMessageHeader.Current.Namespace = np;
                if (!string.IsNullOrEmpty(lang))
                    WcfExtensions.WcfMessageHeader.Current.Language = lang;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {
                    var data = new SecurityServices().GetOtpSecretKeyByUsername(username);

                    return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(data, "Success"));
                }
                else {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
                }
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("get_telegram_by_username")]
        public HttpResponseMessage GetTelegramByUsername(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = body.Get("username");
                var secretKey = body.Get("secretKey");
                var np = body.Get("namespace");
                var lang = body.Get("lang");

                if (!string.IsNullOrEmpty(np))
                    WcfExtensions.WcfMessageHeader.Current.Namespace = np;
                if (!string.IsNullOrEmpty(lang))
                    WcfExtensions.WcfMessageHeader.Current.Language = lang;

                var apiPasscodes = WcfExtensions.AppConfigs.CmsApiPasscode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (!string.IsNullOrEmpty(secretKey) && apiPasscodes.Contains(secretKey))
                {
                    var data = new SecurityServices().GetTelegramByUsername(username);

                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(data, "Success"));
                }
                else {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("secretKey Error"));
                }
        }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("reset_otp")]
        public HttpResponseMessage ResetOTP(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var encryptUserId = body.Get("userid");                

                var result = new SecurityServices().ResetOTP(encryptUserId, accountName);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("reset_telegram_by_username")]
        public HttpResponseMessage UpdateOtpTelegramForUsername(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var encryptUserId = body.Get("userid");
                var np = WcfExtensions.WcfMessageHeader.Current.Namespace;
            
                var result = new SecurityServices().ResetTelegramId(encryptUserId, 0L, np);
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("update_user_permission_by_userid")]
        public HttpResponseMessage UpdateUserPermissionByUserId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var encryptUserId = body.Get("userid");
                var permissionWithZoneList = body.Get("zonelist");
                var isFullPermission = Utility.ConvertToBoolean(body.Get("isFullPermission"));
                var isFullZone = Utility.ConvertToBoolean(body.Get("isFullZone"));
                var isRole = Utility.ConvertToInt(body.Get("isRole"), 1);
                var isSendOver = Utility.ConvertToBoolean(body.Get("isSendOver"));
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername; //body.Get("accountName");

                var userPermissionEntityList = new List<UserPermissionEntity>();
                //if (!isFullZone || !isFullPermission)
                //{
                    //cách 1
                    //var groupList = permissionWithZoneList.Split(';');
                    //foreach (var group in groupList)
                    //{
                    //    var match = Regex.Match(group, @"G(?<PermissionId>[0-9]+)\((?<ZoneIdList>[0-9,]+)\)");

                    //    if (!match.Success) continue;

                    //    var permissionId = int.Parse(match.Groups["PermissionId"].ToString());
                    //    var zoneIdList = match.Groups["ZoneIdList"].ToString();

                    //    var zoneIds = zoneIdList.Split(',');
                    //    foreach (var zoneId in zoneIds)
                    //    {
                    //        if (!string.IsNullOrEmpty(zoneId) && zoneId != "0")
                    //            userPermissionEntityList.Add(new UserPermissionEntity
                    //            {
                    //                UserId = 0,
                    //                PermissionId = permissionId,
                    //                ZoneId = Utility.ConvertToInt(zoneId)
                    //            });
                    //    }
                    //}

                    //cách 2
                    if (!string.IsNullOrEmpty(permissionWithZoneList))
                    {
                        userPermissionEntityList = this.DeJson<List<UserPermissionEntity>>(permissionWithZoneList);
                    }
                //}

                var result = new SecurityServices().UpdateUserPermissionByUserId(encryptUserId, userPermissionEntityList, isFullPermission, isFullZone, isRole, isSendOver, accountName);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UpdateUserPermissionByUserId => " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        #endregion

        #region Authenticate Log

        [AllowAnonymous]
        [HttpPost, Route("add_new_auth_log")]
        public HttpResponseMessage AddnewAuthLog(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ipaddress = body.Get("ipaddress");
                var machinename = body.Get("machinename");
                var urlreferrer = body.Get("urlreferrer");
                var useragent = body.Get("useragent");
                var username = body.Get("username");
                var os = body.Get("os");
                var description = body.Get("description");
                var status = Convert.ToBoolean(body.Get("status"));
                var otp = body.Get("otp");

                var auth = new AuthenticateLogEntity()
                {
                    IpAddress = ipaddress,
                    LogDate = DateTime.Now,
                    MachineName = machinename,
                    UrlReferrer = urlreferrer,
                    UserAgent = useragent,
                    UserName = username,
                    Os = os,
                    Description = description,
                    Status = status,
                    Otp= otp
                };

                long logId = 0;
                var result = new SecurityServices().AddnewAuthLog(auth, ref logId);
                result.Data = logId.ToString();
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }
        #endregion

        #region Search

        [HttpPost, Route("search_user")]
        public HttpResponseMessage SearchUser2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var status = body.Get("status");
                var start = Convert.ToInt32(body.Get("start"));
                var rows = Convert.ToInt32(body.Get("rows"));

                UserStatus userStatus;
                if (!Enum.TryParse(status.ToString(), out userStatus))
                {
                    userStatus = UserStatus.Unknow;
                }

                var totalRow = 0;
                var data = new SecurityServices().SearchUser2(keyword, userStatus, start, rows, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchUserResponse>.CreateSuccessResponse(new SearchUserResponse { TotalRow = totalRow, Users = data }, "Success"));
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "SearchUser2 => "+ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        [HttpPost, Route("user_autocomplete")]
        public HttpResponseMessage UserSearchAutocomplete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var keyword = body.Get("Keyword")??"";                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneiId"),0);                            

                var data = new SecurityServices().User_SearchAutocomplete(zoneId, keyword, true);

                return this.ToJson(WcfActionResponse<List<UserSearchAutocompleteEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.Exception, ErrorMapping.Current[ErrorMapping.ErrorCodes.Exception]));
            }
        }

        #endregion

        #region PermisstionTemplate

        [HttpPost, Route("save_permisstion_template")]
        public HttpResponseMessage SavePermisstionTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var templateName = body.Get("TemplateName");                
                if (string.IsNullOrEmpty(templateName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("TemplateName null empty."));
                }

                var result = new WcfActionResponse();
                if (id > 0)
                {
                    result = new SecurityServices().PermissionTemplateUpdate(id, templateName);
                    return this.ToJson(result);
                }

                result = new SecurityServices().PermissionTemplateInsert(templateName);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_permisstion_template")]
        public HttpResponseMessage DeletePermisstionTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);
                                
                var result = new SecurityServices().PermissionTemplateDelete(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_permisstion_template")]
        public HttpResponseMessage ListPermisstionTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
        
                var data = new SecurityServices().PermissionTemplateGetAll();

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }


        [HttpPost, Route("save_permisstion_template_detail")]
        public HttpResponseMessage SavePermisstionTemplateDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var templateId = Utility.ConvertToInt(body.Get("TemplateId"), 0);
                var permissionList = body.Get("PermissionList");
                if (string.IsNullOrEmpty(permissionList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("PermissionList null empty."));
                }
                var permissionTemplate = this.DeJson<List<PermissionTemplateDetailEntity>>(permissionList);

                var result = new SecurityServices().PermissionTemplateDetailUpdate(templateId, permissionTemplate);
                return this.ToJson(result);                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_permisstion_template_detail_by_id")]
        public HttpResponseMessage GetPermisstionTemplateDetailById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("TemplateId"), 0);

                var data = new SecurityServices().GetPermisstionTemplateDetailById(id);

                return this.ToJson(WcfActionResponse<List<PermissionTemplateDetailEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
