﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Topic;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/mp3")]
    public class Mp3Controller : ApiController
    {
        [HttpPost, Route("search")]
        public HttpResponseMessage SearchVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = string.Empty;
                var filterMode = body.Get("filterMode")??"myvideo";
                if (filterMode == "myvideo")
                {
                    accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                var keyword = body.Get("keyword")??string.Empty;
                
                var pageIndex = Utility.ConvertToInt(body.Get("page"),1);
                var pageSize = Utility.ConvertToInt(body.Get("num"),20);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("toDate")) ? "" : body.Get("toDate") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");                              

                var totalRow = 0;
                var data = new MediaServices().SearchMp3(accountName, keyword, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, ListMp3 = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_info")]
        public HttpResponseMessage GetInfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                
                var data = new MediaServices().GetMp3ById(id);
                return this.ToJson(WcfActionResponse<Mp3Entity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save")]
        public HttpResponseMessage SaveVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var mp3Id = Utility.ConvertToInt(body.Get("Id"), 0);
                //var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var name = body.Get("Name")??string.Empty;
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Tên file không được để trống."));
                }

                var unsignName = Utility.ConvertTextToUnsignName(name);
                var url = Utility.UnicodeToKoDauAndGach(name);
                
                var file = body.Get("FileName")??string.Empty;                                        
                var duration = body.Get("Duration")?? string.Empty;
                var size = body.Get("Size")?? string.Empty;
                var capacity = Utility.ConvertToInt(body.Get("Capacity"), 0);
                var desc = body.Get("Description")?? string.Empty;
                var htmlCode = body.Get("HtmlCode")?? string.Empty;
                var avatar = body.Get("Avatar")?? string.Empty;
                var keyvideo = body.Get("KeyVideo")?? string.Empty;

                var mp3 = new Mp3Entity
                {
                    Id = mp3Id,                    
                    Name = name,
                    UnsignName = unsignName,
                    Description = desc,
                    HtmlCode = htmlCode,
                    Avatar = avatar,
                    KeyVideo = keyvideo,
                    Status = 1,
                    CreatedDate= DateTime.Now,                    
                    DistributionDate = DateTime.Now,
                    CreatedBy = accountName,
                    LastModifiedBy = accountName,
                    Url = url,
                    FileName = file,
                    Duration = duration,
                    Size = size,
                    Capacity = capacity
                };

                var result = new MediaServices().SaveMp3(mp3);

                return this.ToJson(result);
                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
    }
}
