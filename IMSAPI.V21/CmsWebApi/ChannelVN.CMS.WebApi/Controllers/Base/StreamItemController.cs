﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.StreamItem;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/streamitem")]
    public class StreamItemController : ApiController
    {
        #region StreamItem 

        #region action

        [HttpPost, Route("save_streamitem")]
        public HttpResponseMessage SaveStreamItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToLong(body.Get("Id"));
                var order = Utility.ConvertToLong(body.Get("Order"));
                var typeId = Utility.ConvertToInt(body.Get("TypeId"),0);
                var templateId = body.Get("TemplateId");
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var publishedDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("PublishedDate"),"dd/MM/yyyy HH:mm");
                var publishedBy = body.Get("PublishedBy");
                var dataJson = body.Get("DataJson")??string.Empty;
                var mode = Utility.ConvertToInt(body.Get("Mode"), 0);
                var title = body.Get("Title");

                var streamItem = new StreamItemEntity
                {
                    Id = id,
                    Order=order,
                    TypeId=typeId,
                    TemplateId= templateId,
                    Status=status,
                    CreatedDate=DateTime.Now,
                    CreatedBy=accountName,
                    PublishedDate=publishedDate,
                    PublishedBy=publishedBy,
                    DataJson=dataJson,
                    Mode=mode,
                    Title=title
                };
                var data = new WcfActionResponse();
                if (id > 0)
                {
                    data = new StreamItemServices().UpdateStreamItem(streamItem);
                }
                else
                {                    
                    data = new StreamItemServices().InsertStreamItem(streamItem,ref id);
                    data.Data = id.ToString();
                }
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("save_muilt_streamitem")]
        public HttpResponseMessage SaveMultStreamItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";
                if (string.IsNullOrEmpty(listData) && string.IsNullOrEmpty(listIdDelete))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListData không được để trống."));
                }

                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new StreamItemServices().DeleteStreamItemById(id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<StreamItemEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0L;
                                item.CreatedBy = accountName;
                                var data = new StreamItemServices().InsertStreamItem(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new StreamItemServices().UpdateStreamItem(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_streamitem")]
        public HttpResponseMessage DeleteStreamItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new StreamItemServices().DeleteStreamItemById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }        

        #endregion

        #region Search
        [HttpPost, Route("search_streamitem")]
        public HttpResponseMessage SearchStreamItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var keyword = body.Get("Keyword");
                var typeId = Utility.ConvertToInt(body.Get("TypeId"));
                var mode = Utility.ConvertToInt(body.Get("Mode"), -1);
                var templateId = body.Get("TemplateId");
                var status = Utility.ConvertToInt(body.Get("Status"),-1);
                var orderBy = Utility.ConvertToInt(body.Get("OrderBy"));

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"));
                
                var totalRow = 0;
                var data = new StreamItemServices().SearchStreamItem(keyword, typeId, mode, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);                
                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { TotalRow = totalRow, StreamItems = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("count_streamitem")]
        public HttpResponseMessage CountStreamItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var data = new StreamItemServices().CountStreamItem();
                return this.ToJson(WcfActionResponse<string>.CreateSuccessResponse(data.ToString(), "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region get

        [HttpPost, Route("get_streamitem_by_id")]
        public HttpResponseMessage GetStreamItemById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));

                var data = new StreamItemServices().GetStreamItemById(id);

                return this.ToJson(WcfActionResponse<StreamItemEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"+ex.Message));
            }
        }


        [HttpPost, Route("get_news_by_id")]
        public HttpResponseMessage GetNewsForStreamItemById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var data = new StreamItemServices().GetNewsForStreamItemById(newsId);

                return this.ToJson(WcfActionResponse<StreamItemDetailEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #endregion

        #region StreamItemMobile 

        #region action

        [HttpPost, Route("save_streamitemmobile")]
        public HttpResponseMessage SaveStreamItemMobile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToLong(body.Get("Id"));
                var order = Utility.ConvertToLong(body.Get("Order"));
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), 0);
                var templateId = body.Get("TemplateId");
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var publishedDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("PublishedDate"), "dd/MM/yyyy HH:mm");
                var publishedBy = body.Get("PublishedBy");
                var dataJson = body.Get("DataJson") ?? string.Empty;

                var streamItem = new StreamItemMobileEntity
                {
                    Id = id,
                    Order = order,
                    TypeId = typeId,
                    TemplateId = templateId,
                    Status = status,
                    CreatedDate = DateTime.Now,
                    CreatedBy = accountName,
                    PublishedDate = publishedDate,
                    PublishedBy = publishedBy,
                    DataJson = dataJson
                };
                var data = new WcfActionResponse();
                if (id > 0)
                {
                    data = new StreamItemServices().UpdateStreamItemMobile(streamItem);
                }
                else
                {
                    data = new StreamItemServices().InsertStreamItemMobile(streamItem);
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_streamitemmobile")]
        public HttpResponseMessage DeleteStreamItemMobile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var data = new StreamItemServices().DeleteStreamItemMobileById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_streamitemmobile")]
        public HttpResponseMessage SearchStreamItemMobile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var typeId = Utility.ConvertToInt(body.Get("TypeId"));
                var templateId = body.Get("TemplateId");
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var orderBy = Utility.ConvertToInt(body.Get("OrderBy"));

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"));

                var totalRow = 0;
                var data = new StreamItemServices().SearchStreamItemMobile(typeId, templateId, status, orderBy, pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(new { TotalRow = totalRow, StreamItems = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region get

        [HttpPost, Route("get_streamitemmobile_by_id")]
        public HttpResponseMessage GetStreamItemMobileById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));

                var data = new StreamItemServices().GetStreamItemMobileById(id);

                return this.ToJson(WcfActionResponse<StreamItemMobileEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!" + ex.Message));
            }
        }

        #endregion

        #endregion
    }
}
