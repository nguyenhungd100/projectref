﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Photo;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.WcfExtensions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{    
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/photo")]
    public class PhotoController : ApiController
    {
        #region Photo
        [HttpPost, Route("save_photo")]
        public HttpResponseMessage SavePhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var name = body.Get("Name") ?? "";                
                var imageUrl = body.Get("ImageUrl") ?? "";
                var imageNote = body.Get("ImageNote") ?? "";
                var size = body.Get("Size") ?? "";
                var capacity = Utility.ConvertToDouble(body.Get("Capacity"));
                var note = body.Get("Note") ?? "";
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                //fix luu tam
                //status = 0;

                var originalName = body.Get("OriginalName") ?? "";
                var unSignName = body.Get("UnSignName") ?? "";
                var tags = body.Get("Tags") ?? "";
                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);
                var author = body.Get("Author") ?? "";
                var imagePreviewUrl = body.Get("ImagePreviewUrl") ?? "";                                
                var imageDistribution = body.Get("ImageDistribution") ?? "";                
                var width = Utility.ConvertToInt(body.Get("Width"),0);
                var height = Utility.ConvertToInt(body.Get("Height"),0);
                var imageDimension = Utility.ConvertToShort(body.Get("ImageDimension"));
                var imageExif = body.Get("ImageExif") ?? "";
                var restrictionContent = body.Get("RestrictionContent") ?? "";
                var location = body.Get("Location") ?? "";

                var tagIdList = body.Get("TagIdList") ?? "";                               
                var authorId = Utility.ConvertToInt(body.Get("AuthorId"), 0);

                var photo = new PhotoEntity
                {
                    Id = id,
                    ZoneId = zoneId,
                    Name = name,
                    ImageUrl = imageUrl,
                    ImageNote=imageNote,
                    Size=size,
                    Capacity=capacity,
                    Note=note,
                    Status = status,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    OriginalName=originalName,
                    UnSignName=unSignName,
                    Tags = tags,
                    AlbumId=albumId,
                    Author=author,
                    ImagePreviewUrl=imagePreviewUrl,
                    PublishedDate = DateTime.Now,
                    PublishedBy = accountName,
                    ImageDistribution=imageDistribution,
                    Width=width,
                    Height=height,
                    ImageDimension=imageDimension,
                    ImageExif=imageExif,
                    RestrictionContent=restrictionContent,
                    Location=location
                };               

                var responseData = new WcfActionResponse();
                //Cập nhật thông tin Photo, nếu chưa có thì tạo mới
                if (id > 0)
                {
                    photo.ModifiedDate = DateTime.Now;
                    photo.ModifiedBy = accountName;
                    responseData = new MediaServices().UpdatePhotoV2(photo, tagIdList);                 
                }
                else
                {
                    responseData = new MediaServices().InsertPhotoV2(photo, tagIdList);                  
                }
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    if (responseData.Success)
                    {
                        photo.Id = Utility.ConvertToInt(responseData.Data);
                        responseData = new MediaServices().SavePhotoByAuthor(new PhotoByAuthorEntity
                        {
                            AuthorId = authorId,
                            PhotoId = photo.Id
                        });
                    }
                }                   
                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_muilt_photo")]
        public HttpResponseMessage SaveMuiltPhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfMessageHeader.Current.ClientUsername;

                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);
                var addListPhoto = body.Get("AddListPhoto")??string.Empty;
                var deleteListPhoto = body.Get("DeleteListPhotoId") ?? string.Empty;

                var responseList = new List<WcfActionResponse>();
                var authorId = Utility.ConvertToInt(body.Get("AuthorId"), 0);

                if (!string.IsNullOrWhiteSpace(addListPhoto))
                {                   
                    var listPhoto = NewtonJson.Deserialize<List<PhotoEntity>>(addListPhoto);
                    if(listPhoto!=null && listPhoto.Count > 0)
                    {
                        foreach(var photo in listPhoto)
                        {
                            photo.AlbumId = albumId;                            
                            var responseData = new WcfActionResponse();
                            if (photo.Id > 0)
                            {
                                photo.ModifiedDate = DateTime.Now;
                                photo.ModifiedBy = accountName;
                                responseData = new MediaServices().UpdatePhotoV2(photo, "");
                                responseList.Add(responseData);
                            }
                            else {
                                photo.CreatedDate = DateTime.Now;
                                photo.CreatedBy = accountName;
                                photo.PublishedDate = DateTime.Now;
                                photo.PublishedBy = accountName;
                                responseData = new MediaServices().InsertPhotoV2(photo, "");
                                if (responseData.Success)
                                {
                                    photo.Id = Utility.ConvertToInt(responseData.Data);
                                }
                                responseList.Add(responseData);                             
                            }

                            if (responseData.Success)
                            {
                                photo.Id = Utility.ConvertToInt(responseData.Data);
                                responseData = new MediaServices().SavePhotoByAuthor(new PhotoByAuthorEntity
                                {
                                    AuthorId = authorId,
                                    PhotoId = photo.Id
                                });
                            }
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(deleteListPhoto))
                {
                    var listDelete = deleteListPhoto.Split(new char[] { ',', ';'}, StringSplitOptions.RemoveEmptyEntries);
                    if (listDelete != null && listDelete.Length > 0)
                    {
                        var listTemp = string.Join(",", listDelete);
                        var responseData = new MediaServices().DeletePhotoInAlbum(listTemp);
                        responseList.Add(responseData);
                    }
                }                                            

                return this.ToJson(responseList);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_photo_status")]
        public HttpResponseMessage UpdatePhotoStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfMessageHeader.Current.ClientUsername;
                var photoId = Utility.ConvertToInt(body.Get("PhotoId"));
                var action = body.Get("Action") ?? string.Empty;

                var data = new WcfActionResponse {
                    ErrorCode=9997,
                    Success=false,
                    Message="Action not support."
                };
                switch (action?.ToLower())
                {
                    case "send":
                        data = new MediaServices().SendPhoto(photoId, username);
                        break;
                    case "publish":
                        data = new MediaServices().PublishPhotoV2(photoId, username);
                        break;
                    case "unpublish":
                        data = new MediaServices().UnPublishPhoto(photoId, username);
                        break;
                    case "return":
                        data = new MediaServices().ReturnPhoto(photoId, username);
                        break;
                    default:

                        break;
                }
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_photo")]
        public HttpResponseMessage SearchPhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("Keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);                
                var status = Utility.ConvertToInt(body.Get("Status"), -1);                
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 12);
                var createDateFrom = Utility.ParseDateTime(body.Get("FromDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var createDateTo = Utility.ParseDateTime(body.Get("ToDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApprovePhoto))
                {
                    if (status == (int)EnumPhotoStatus.Temporary || status == (int)EnumPhotoStatus.Return)
                    {
                        accountName = WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.PhotoManager))
                {
                    accountName = WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRow = 0;
                var data = new MediaServices().SearchPhotoV2(keyword, zoneId, albumId, status, createDateFrom, createDateTo, accountName, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Photos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_photo_by_album_id")]
        public HttpResponseMessage ListPhotoByAlbumId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);                
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                if (albumId <= 0)
                {
                    return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = 0, Photos=new List<PhotoEntity>() }, "Success"));
                }           
                var totalRow = 0;
                var data = new MediaServices().ListPhotoByAlbumId(albumId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Photos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_photo_by_id")]
        public HttpResponseMessage GetDetailPhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var result = new MediaServices().GetPhotoByPhotoIdV2(id);
                return this.ToJson(WcfActionResponse<PhotoDetailEntity>.CreateSuccessResponse(result, "Success"));                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_photo")]
        public HttpResponseMessage RemovePhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                if (id <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found embedAlbumId."));
                }

                var responseData = new MediaServices().DeletePhoto(id);

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_photo")]
        public HttpResponseMessage CountPhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listStatus = body.Get("ListStatus");
                var accountName = WcfMessageHeader.Current.ClientUsername;
                var userAction = accountName;

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApprovePhoto))
                {
                    userAction = string.Empty;
                }

                var data = new MediaServices().CountPhoto(accountName, listStatus, userAction);
                return this.ToJson(WcfActionResponse<List<CountAlbumStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region ZonePhoto
        [HttpPost, Route("save_zone")]
        public HttpResponseMessage SaveZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var parentId = Utility.ConvertToInt(body.Get("ParentId"));
                var name = body.Get("Name") ?? string.Empty;
                var url = Utility.UnicodeToKoDauAndGach(name);
                var description = body.Get("Description") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), (int)EnumZonePhotoStatus.Actived);

                var order = Utility.ConvertToInt(body.Get("Order"));                   
                var avatar = body.Get("Avatar") ?? string.Empty;
                var showOnHome = Utility.ConvertToBoolean(body.Get("ShowOnHome"));
                var logo = body.Get("Logo") ?? string.Empty;

                var catId = Utility.ConvertToInt(body.Get("CatId"));
                var zoneRelation = body.Get("ZoneRelation") ?? string.Empty;

                var zonePhoto = new ZonePhotoEntity
                {
                    Id = id,
                    ParentId = parentId,
                    Name = name,
                    Url = url,
                    Order = order,                    
                    Status = status,
                    Avatar = avatar,
                    Description = description,
                    ShowOnHome= showOnHome,
                    Logo=logo,
                    CatId=catId,
                    ZoneRelation= zoneRelation,
                    CreatedDate=DateTime.Now
                };

                WcfActionResponse result;
                if (id > 0)
                {
                    zonePhoto.ModifiedDate = DateTime.Now;
                    result = new MediaServices().UpdateZonePhoto(zonePhoto);
                }
                else
                {
                    var zoneVideoId = 0;
                    result = new MediaServices().InsertZonePhoto(zonePhoto, ref zoneVideoId);
                    result.Data = zoneVideoId.ToString();
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_zone")]
        public HttpResponseMessage GetListZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneId = -1;
                var zoneId = body.Get("ParentId");
                if (string.IsNullOrEmpty(zoneId) || !int.TryParse(zoneId.ToString(), out parentZoneId))
                {
                    parentZoneId = -1;
                }
                var statusZone = 0;
                var status = body.Get("Status");
                if (string.IsNullOrEmpty(status) || !int.TryParse(status.ToString(), out statusZone))
                {
                    statusZone = 0;
                }

                var data = new MediaServices().GetZonePhotoListByParentId(parentZoneId, (EnumZonePhotoStatus)statusZone);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, ZonePhotos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_private_zone")]
        public HttpResponseMessage GetPrivateZonePhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneId = -1;
                var param = body.Get("ParentId");
                if (string.IsNullOrEmpty(param) || !int.TryParse(param.ToString(), out parentZoneId))
                {
                    parentZoneId = -1;
                }
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var username = WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetPrivateZonePhoto(username, parentZoneId, status);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, ZonePhotos = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_zone_photo_by_id")]
        public HttpResponseMessage GetZonePhotoByd(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new MediaServices().GetZonePhotoById(id);

                return this.ToJson(WcfActionResponse<ZonePhotoEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_zone_up")]
        public HttpResponseMessage MoveZonePhotoUp(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().MoveZonePhotoUp(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_zone_down")]
        public HttpResponseMessage MoveZonePhotoDown(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().MoveZonePhotoDown(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_zone")]
        public HttpResponseMessage DeleteZonePhoto(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().DeleteZonePhoto(id);

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion

        #region PhotoEvent
        [HttpPost, Route("save_event")]
        public HttpResponseMessage SaveEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"));                
                var name = body.Get("Name") ?? string.Empty;
                var url = Utility.UnicodeToKoDauAndGach(name);
                var description = body.Get("Description") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), (int)EnumPhotoEventStatus.Actived);                
                var avatar = body.Get("Avatar") ?? string.Empty;
                var isFocus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),0);
                var location = body.Get("Location") ?? string.Empty;

                var beginDate = Utility.ParseDateTime(body.Get("BeginDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var endDate = Utility.ParseDateTime(body.Get("EndDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                //var publishedDate = Utility.ParseDateTime(body.Get("PublishedDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });

                var photoEvent = new PhotoEventEntity
                {
                    Id = id,                    
                    Name = name,
                    Description = description,
                    Avatar = avatar,
                    Url = url,
                    BeginDate = beginDate,
                    EndDate= endDate,
                    CreatedDate = DateTime.Now,
                    CreatedBy=accountName,
                    PublishedDate= DateTime.Now,
                    PublishedBy=accountName,
                    ZoneId = zoneId,
                    Status = status,                                       
                    IsFocus = isFocus,
                    Location=location
                };

                WcfActionResponse result;
                if (id > 0)
                {
                    photoEvent.ModifiedDate = DateTime.Now;
                    photoEvent.ModifiedBy = accountName;
                    result = new MediaServices().UpdatePhotoEvent(photoEvent);
                }
                else
                {
                    var photoEventId = 0;
                    result = new MediaServices().InsertPhotoEvent(photoEvent, ref photoEventId);
                    result.Data = photoEventId.ToString();
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_event")]
        public HttpResponseMessage GetListPhotoEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")??string.Empty;                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);                
                var status = Utility.ConvertToInt(body.Get("Status"),0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                var data = new MediaServices().SearchPhotoEvent(keyword, zoneId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, PhotoEvents = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_photo_event_by_id")]
        public HttpResponseMessage GetPhotoEventByd(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new MediaServices().GetPhotoEventById(id);

                return this.ToJson(WcfActionResponse<PhotoEventEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_event")]
        public HttpResponseMessage DeleteEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var result = new MediaServices().DeletePhotoEvent(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Album
        [HttpPost, Route("save_album")]
        public HttpResponseMessage SaveAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfMessageHeader.Current.ClientUsername;
                var albumId = Utility.ConvertToInt(body.Get("Id"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var name = body.Get("Name") ?? "";
                var thumb = body.Get("ThumbImage") ?? "";
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var tags = body.Get("Tags") ?? "";
                var description = body.Get("Description") ?? "";
                var tagIdList = body.Get("TagIdList") ?? "";                         
                var eventIdList = body.Get("EventIdList") ?? "";
                var zoneName = body.Get("ZoneName");
                var author = body.Get("Author");
                var authorId = Utility.ConvertToInt(body.Get("AuthorId"), 0);
                var album = new AlbumEntity
                {
                    Id = albumId,
                    ZoneId = zoneId,
                    Name = name,
                    ThumbImage = thumb,
                    CreatedBy = accountName,
                    CreatedDate=DateTime.Now,
                    Status = status,
                    Tags=tags,
                    Description=description,
                    ZoneName=zoneName,
                    Author = author
                };

                var responseData = new WcfActionResponse();
                //Cập nhật thông tin album, nếu chưa có thì tạo mới
                if (albumId > 0)
                {
                    album.ModifiedDate = DateTime.Now;
                    album.ModifiedBy = accountName;
                    responseData = new MediaServices().UpdateAlbum(album, tagIdList, eventIdList);
                    if (responseData.Success)
                    {
                        album.Id = Utility.ConvertToInt(responseData.Data);
                    }
                }
                else
                {
                    responseData = new MediaServices().InsertAlbum(album, tagIdList, eventIdList);
                    if (responseData.Success)
                    {
                        album.Id = Utility.ConvertToInt(responseData.Data);
                    }
                }
                if (WcfMessageHeader.Current.Namespace == "Sport5")
                {
                    if (responseData.Success)
                    {
                        album.Id = Utility.ConvertToInt(responseData.Data);
                        responseData = new MediaServices().SaveAlbumByAuthor(new AlbumByAuthorEntity
                        {
                            AuthorId = authorId,
                            AlbumId = album.Id
                        });
                    }
                }                
                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_album_status")]
        public HttpResponseMessage UpdateAlbumStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfMessageHeader.Current.ClientUsername;
                var albumId = Utility.ConvertToInt(body.Get("AlbumId"));
                var action = body.Get("Action") ?? string.Empty;

                var data = new WcfActionResponse
                {
                    ErrorCode = 9997,
                    Success = false,
                    Message = "Action not support."
                };
                switch (action?.ToLower())
                {
                    case "send":
                        data = new MediaServices().SendAlbum(albumId, username);
                        break;
                    case "publish":
                        data = new MediaServices().PublishAlbum(albumId, username);
                        break;
                    case "unpublish":
                        data = new MediaServices().UnPublishAlbum(albumId, username);
                        break;
                    case "return":
                        data = new MediaServices().ReturnAlbum(albumId, username);
                        break;
                    default:

                        break;
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("add_photo_in_album")]
        public HttpResponseMessage AddPhotoInAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var albumId = Utility.ConvertToInt(body.Get("AlbumId"), 0);
                var albumName = body.Get("AlbumName") ?? "";
                var addPhotoIds = body.Get("AddPhotoIds") ?? string.Empty;
                var deletePhotoIds = body.Get("DeletePhotoIds") ?? string.Empty;
                var waitPublishPhotoIds = body.Get("WaitPublishPhotoIds") ?? string.Empty;

                if (albumId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var result = new MediaServices().AddPhotoInAlbum(addPhotoIds, deletePhotoIds, waitPublishPhotoIds, albumId, albumName);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_album")]
        public HttpResponseMessage SearchAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfMessageHeader.Current.ClientUsername;

                var keyword = body.Get("Keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var eventId = Utility.ConvertToInt(body.Get("EventId"), 0);               
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 12);
                var createDateFrom = Utility.ParseDateTime(body.Get("FromDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var createDateTo = Utility.ParseDateTime(body.Get("ToDate"), new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" });
                var createdBy = body.Get("CreatedBy") ?? "";

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApprovePhoto))
                {
                    if (status == (int)EnumPhotoAlbumStatus.Temporary || status == (int)EnumPhotoAlbumStatus.Return)
                    {
                        accountName = WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        accountName = "";
                        if (!string.IsNullOrWhiteSpace(createdBy))
                        {
                            accountName = createdBy;
                        }
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.PhotoManager))
                {
                    accountName = WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }                

                var totalRow = 0;
                var data = new MediaServices().SearchAlbum(keyword, zoneId, eventId, status, createDateFrom, createDateTo, accountName, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, Albums = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_album_by_id")]
        public HttpResponseMessage GetAlbumById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var result = new MediaServices().GetAlbumByAlbumId(id);
                return this.ToJson(WcfActionResponse<AlbumDetailEntity>.CreateSuccessResponse(result, "Success"));                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_album")]
        public HttpResponseMessage RemoveAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                if (id <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found embedAlbumId."));
                }

                var responseData = new MediaServices().DeleteAlbum(id, accountName);

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_album")]
        public HttpResponseMessage CountAlbum(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listStatus = body.Get("ListStatus");
                var accountName = WcfMessageHeader.Current.ClientUsername;
                var userAction = accountName;

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ApprovePhoto))
                {
                    userAction = string.Empty;
                }
                
                var data = new MediaServices().CountAlbum(accountName, listStatus, userAction);
                return this.ToJson(WcfActionResponse<List<CountAlbumStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Photo Tag
        [HttpPost, Route("addnew_photo_tag")]
        public HttpResponseMessage AddNewPhotoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tag = body.Get("TagName");

                var tagId = 0;
                var result = new MediaServices().UpdatePhotoTagWithTagName(tag, ref tagId);
                if (result.Success && tagId > 0)
                {
                    var data = new MediaServices().GetPhotoTagById(tagId);
                    return this.ToJson(WcfActionResponse<PhotoTagEntity>.CreateSuccessResponse(data, "Success"));
                }
                return this.ToJson(result);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("save_photo_tag")]
        public HttpResponseMessage SavePhotoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var avatar = body.Get("Avatar") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var name = body.Get("Name") ?? string.Empty;
                var url = Utility.UnicodeToKoDauAndGach(name);

                var tag = new PhotoTagEntity
                {
                    Avatar = avatar,
                    Id = id,
                    Name = name,
                    Status = status,
                    Url = url
                };

                WcfActionResponse result;
                if (id > 0)
                    result = new MediaServices().UpdatePhotoTag(tag);
                else
                {
                    var tag_id = 0;
                    result = new MediaServices().InsertPhotoTag(tag, ref tag_id);
                    result.Data = tag_id.ToString();
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_photo_tag")]
        public HttpResponseMessage SearchPhotoTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("Keyword") ?? string.Empty;                
                var status = Utility.ConvertToInt(body.Get("Status"), 0);                
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                var totalRow = 0;

                var data = new MediaServices().SearchPhotoTag(keyword, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, PhotoTags = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_photo_tag_by_id")]
        public HttpResponseMessage GetVideoTagById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new MediaServices().GetPhotoTagById(id);

                return this.ToJson(WcfActionResponse<PhotoTagEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        
       
        #endregion
    }
}
