﻿using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/playlist")]
    public class PlayListController : ApiController
    {        
        #region action save
        [HttpPost, Route("save")]
        public HttpResponseMessage SavePlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NamePlayListNotEmpty]));
                }                
                var unsignName = Utility.ConvertTextToUnsignName(name);
                var description = body.Get("Description");
                //var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var mode = Utility.ConvertToInt(body.Get("Mode"), 0);
                var avatar = body.Get("Avatar");
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var introClip = body.Get("IntroClip");                
                var playlistRelation = body.Get("PlayListRelation");
                var url = Utility.ConvertTextToLink(name);
                var cover = body.Get("Cover");
                var metaJson = body.Get("MetaJson");
                var metaAvatar = body.Get("MetaAvatar");
                var zoneName = body.Get("ZoneName");

                var zoneIdList = body.Get("ZoneIdList");

                var playList = new PlaylistEntity
                {
                    Id = id,
                    ZoneId = zoneId,
                    Name = name,                    
                    UnsignName = unsignName,
                    Description = description,
                    Status = 0,
                    Mode = mode,
                    Avatar = avatar,
                    Priority = priority,
                    IntroClip = introClip,                    
                    PlaylistRelation = playlistRelation,
                    Url = url,
                    DistributionDate = distributionDate,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    //EditedBy = accountName,
                    //EditedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    //PublishedBy = accountName,
                    //PublishedDate = distributionDate,                    
                    LastInsertVideoDate = DateTime.Now,
                    VideoCount = 0,
                    FollowCount=0,                 
                    Cover=cover,
                    MetaJson=metaJson,
                    MetaAvatar= metaAvatar,
                    ZoneName=zoneName
                };
                if (playList.Id > 0)
                {
                    var playListDB = new MediaServices().GetPlayListById(playList.Id);
                    if (playListDB != null)
                    {
                        playList.CreatedBy = playListDB.CreatedBy;
                        playList.CreatedDate = playListDB.CreatedDate;
                        playList.EditedBy = accountName;
                        playList.EditedDate = DateTime.Now;
                        playList.PublishedBy = playListDB.PublishedBy;
                        playList.PublishedDate = playListDB.PublishedDate;
                    }
                }

                var result = new MediaServices().SavePlayList(playList, zoneIdList);                
                return this.ToJson(result);
            }
            catch (Exception ex)
            {                
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("add_video_to_playlist")]
        public HttpResponseMessage AddVideoListIntoPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var playlistName = body.Get("PlayListName")??"";
                var addVideoIds = body.Get("AddVideoIds") ?? string.Empty;                
                var deleteVideoIds = body.Get("DeleteVideoIds") ?? string.Empty;
                var waitPublishVideoIds = body.Get("WaitPublishVideoIds") ?? string.Empty;

                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                
                var result = new MediaServices().AddVideoListIntoPlaylist(addVideoIds, deleteVideoIds, waitPublishVideoIds, playlistId, playlistName);                                
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }                

        [HttpPost, Route("remove_video_in_playlist")]
        public HttpResponseMessage RemoveVideoInPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var videoIds = body.Get("VideoIds") ?? string.Empty;
                if (playlistId <= 0 || string.IsNullOrEmpty(videoIds))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var result = new MediaServices().RemoveVideoOutOfPlaylist(playlistId, videoIds);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("publish_playlist")]
        public HttpResponseMessage PublishPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);                
                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var result = new MediaServices().PublishPlaylist(playlistId);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("unpublish_playlist")]
        public HttpResponseMessage UnpublishPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }

                var result = new MediaServices().UnpublishPlaylist(playlistId);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("send_playlist")]
        public HttpResponseMessage SendPlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playListId = Utility.ConvertToInt(body.Get("PlayListId"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().SendPlaylist(playListId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("return_playlist")]
        public HttpResponseMessage ReturnPlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playListId = Utility.ConvertToInt(body.Get("PlayListId"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().ReturnPlayList(playListId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("move_trash_playlist")]
        public HttpResponseMessage MoveTrashPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var result = new MediaServices().MoveTrashPlaylist(playlistId, username);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("change_mode")]
        public HttpResponseMessage ChangeMode(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var mode = Utility.ConvertToInt(body.Get("Mode"), 0);

                var result = new MediaServices().ChangeMode(playlistId, (EnumPlayListMode)mode);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_playlist_by_ids")]
        public HttpResponseMessage DeleteVideoPlaylistByIdList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistIds = body.Get("PlayListIds");
                if (string.IsNullOrEmpty(playlistIds))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var mode = Utility.ConvertToInt(body.Get("Mode"), 0);

                var result = new MediaServices().DeleteVideoPlaylistByIdList(playlistIds);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_video_priority_in_playlist")]
        public HttpResponseMessage UpdateVideoPriorotyInPlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                if (playlistId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.InvalidRequest]));
                }
                var listVideoPriority = body.Get("ListVideoPriority")??string.Empty;
                if (string.IsNullOrEmpty(listVideoPriority))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse("Param ListVideoPriority: Invalid"));
                }
                var videoPriorityList = this.DeJson<List<VideoPriorityEntity>>(listVideoPriority);

                var result = new MediaServices().UpdateVideoPriorotyInPlayList(playlistId, videoPriorityList);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_playlist")]
        public HttpResponseMessage SearchPlayList(HttpRequestMessage request)
        {
            try
            {                
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var keyword = body.Get("Keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var status = body.Get("Status");
                var playlistStatus = EnumPlayListStatus.AllStatus;
                if (!string.IsNullOrEmpty(status))
                {
                    playlistStatus = (EnumPlayListStatus)Utility.ConvertToInt(status);
                }               
                var mode = body.Get("Mode");
                var playlistMode = EnumPlayListMode.AllMode;
                if (!string.IsNullOrEmpty(mode))
                {
                    playlistMode = (EnumPlayListMode)Utility.ConvertToInt(mode);
                }                
                var createdDateFrom = Utility.ConvertToDateTimeByFormat(body.Get("CreatedDateFrom"), "dd/MM/yyyy");
                var created_date_to = body.Get("CreatedDateTo");
                if (!string.IsNullOrEmpty(created_date_to))
                    created_date_to = created_date_to + " 23:59:59";               
                var createdDateTo = Utility.ConvertToDateTimeByFormat(created_date_to, "dd/MM/yyyy HH:mm:ss");
                var distributionDateFrom = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDateFrom"), "dd/MM/yyyy");
                var distribution_date_to = body.Get("DistributionDateTo");
                if (!string.IsNullOrEmpty(distribution_date_to))
                    distribution_date_to = distribution_date_to + " 23:59:59";
                var distributionDateTo = Utility.ConvertToDateTimeByFormat(distribution_date_to, "dd/MM/yyyy HH:mm:ss");
                var sortBy = EnumPlayListSort.CreatedDateDesc;
                var order = body.Get("Order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (EnumPlayListSort)Utility.ConvertToInt(order);
                }
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ApproveVideo))
                {
                    if (playlistStatus == EnumPlayListStatus.Temporary || playlistStatus == EnumPlayListStatus.ReturnForEditor)
                    {
                        username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                    }
                    else
                    {
                        username = "";
                    }
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.VideoManager))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var totalRows = 0;            
                var data = new MediaServices().SearchPlayList(username, zoneId, playlistStatus, keyword, playlistMode, sortBy, createdDateFrom, createdDateTo, distributionDateFrom, distributionDateTo, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, PlayLists = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_playlist_active")]
        public HttpResponseMessage GetPlayListActive(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var keyword = body.Get("Keyword") ?? string.Empty;
                
                var playlistStatus = EnumPlayListStatus.Published;
                                
                var playlistMode = EnumPlayListMode.AllMode;
                                
                var sortBy = EnumPlayListSort.CreatedDateDesc;
                
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                
                var totalRows = 0;
                var data = new MediaServices().SearchPlayList("", -1, playlistStatus, keyword, playlistMode, sortBy, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, pageIndex, pageSize, ref totalRows);
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, PlayLists = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_playlist")]
        public HttpResponseMessage CountPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var status = body.Get("Status");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.ApproveVideo))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(username, (int)EnumPermission.VideoManager))
                {
                    username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                else
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.YouNotPermissionPage]));
                }

                var data = new MediaServices().CountPlaylist(username, status);
                return this.ToJson(WcfActionResponse<List<CountStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("count_video_follow")]
        public HttpResponseMessage CountVideoFollow(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playListIds = body.Get("PlayListIds");

                var data = new MediaServices().CountVideoFollow(playListIds);
                return this.ToJson(WcfActionResponse<List<PlaylistCountFollowEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region get
        [HttpPost, Route("get_playlist_by_id")]
        public HttpResponseMessage GetPlayListDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new MediaServices().GetVideoPlaylistById(id);
                return this.ToJson(WcfActionResponse<PlayListCachedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_video_in_playlist")]
        public HttpResponseMessage GetVideoInPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                var totalRow = 0;
                var maxPriority = 0;
                var data = new MediaServices().GetVideoInPlaylist(playlistId, pageIndex, pageSize, ref totalRow, ref maxPriority);
                return this.ToJson(WcfActionResponse<VideoInPlaylistSearch>.CreateSuccessResponse(new VideoInPlaylistSearch { TotalRow = totalRow, MaxPriority= maxPriority, Videos = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_video_new_by_playlist_id")]
        public HttpResponseMessage GetVideoNewByPlayListId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var playlistId = Utility.ConvertToInt(body.Get("PlayListId"), 0);                
                var data = new MediaServices().GetVideoNewByPlayListId(playlistId);
                return this.ToJson(WcfActionResponse<VideoEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        #endregion        
    }
}
