﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Tag;
using ChannelVN.CMS.Entity.Base.Thread;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/thread")]
    public class ThreadController : ApiController
    {
        #region Thread

        #region action
        [HttpPost, Route("save")]
        public HttpResponseMessage Save(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var tagName = body.Get("tagname");
                var description = body.Get("description");
                var invisiable = Utility.ConvertToBoolean(body.Get("invisiable"));
                var isHotThread = Utility.ConvertToBoolean(body.Get("isHotTag"));
                var avartar = body.Get("avatar");
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zoneid"), 0);
                var zoneIdList = body.Get("zoneidlist");
                var tagTitle = body.Get("tagTitle");
                var tagMetaKeyword = body.Get("tagMetaKeyword");
                var tagMetaContent = body.Get("tagMetaContent");
                var templateid = Utility.ConvertToInt(body.Get("templateid"), 1);
                var threadRelated = body.Get("ThreadRelated");
                var createDate = Utility.ConvertToDateTimeDefaultDateNow(body.Get("createdDate"));
                var newscoverid = Utility.ConvertToLong(body.Get("newscoverid"));
                if (createDate == DateTime.MinValue) createDate = DateTime.Now;
                     
                var tagNew = new ThreadEntity
                {
                    Name = tagName,
                    Description = description,
                    Url = Utility.UnicodeToKoDauAndGach(tagName),
                    Invisibled = invisiable,
                    IsHot = isHotThread,
                    Id = id,
                    EditedBy = accountName,
                    Avatar = avartar,
                    UnsignName = Utility.ConvertTextToUnsignName(tagName),
                    Title = tagTitle,
                    MetaKeyword = tagMetaKeyword,
                    MetaContent = tagMetaContent,
                    TemplateId = templateid,
                    CreatedDate = DateTime.MinValue,
                    NewsCoverId = newscoverid

                };
                var data = new WcfActionResponse();
                if (id > 0)
                {
                    data = new ThreadServices().Update(tagNew, zoneId, zoneIdList, threadRelated);
                }
                else
                {
                    var newThreadId = 0;
                    data = new ThreadServices().Insert(tagNew, zoneId, zoneIdList, threadRelated, ref newThreadId);
                }                

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToLong(body.Get("id"));
                var data = new ThreadServices().DeleteById(tagId);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }        

        [HttpPost, Route("update_is_hot")]
        public HttpResponseMessage UpdateIsHot(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var threadId = Utility.ConvertToLong(body.Get("id"));
                var isHot = Utility.ConvertToBoolean(body.Get("isHot"));
                var data = new ThreadServices().UpdateThreadHot(threadId, isHot);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_is_invisibled")]
        public HttpResponseMessage UpdateIsInvisibled(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var threadId = Utility.ConvertToLong(body.Get("id"));
                var invisibled = Utility.ConvertToBoolean(body.Get("invisibled"));
                var data = new ThreadServices().UpdateIsInvisibled(threadId, invisibled);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("add_thread_news")]
        public HttpResponseMessage UpdateThreadNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var threadId = Utility.ConvertToLong(body.Get("threadId"));
                var deletedNewsId = body.Get("deletedNewsId");
                var addNewsId = body.Get("addNewsId");
                var data = new ThreadServices().UpdateThreadNews(threadId, deletedNewsId, addNewsId);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_thread_embed")]
        public HttpResponseMessage UpdateThreadEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));
                var listThreadId = body.Get("listThreadId");
                var data = new ThreadServices().UpdateThreadEmbed(listThreadId, zoneId, type);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region Search

        [HttpPost, Route("search_thread")]
        public HttpResponseMessage SearchThread(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var key = body.Get("keyword");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var isHotThread = -1;
                int.TryParse(body.Get("isHotTag"), out isHotThread);
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"));
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"));
                var orderBy = Utility.ConvertToInt(body.Get("orderBy"));
                EnumSearchTagOrder order;
                if (!Enum.TryParse(orderBy.ToString(), out order))
                {
                    order = EnumSearchTagOrder.CreatedDateDesc;
                }
                var totalRow = 0;
                var data = new ThreadServices().SearchThread(key, zoneId, (int)order, isHotThread, pageIndex, pageSize, ref totalRow);
                if (data != null && data.Count > 0)
                {
                    var linkFormatThread = CmsChannelConfiguration.GetAppSetting("UrlFormatThread");
                    data.Select(s => { s.Url = string.Format(linkFormatThread, s.Url, s.Id); return s; }).ToList();
                }
                return this.ToJson(WcfActionResponse<SearchThreadResponse>.CreateSuccessResponse(new SearchThreadResponse { TotalRow = totalRow, Threads = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("search_news_publish_by_thread_id")]
        public HttpResponseMessage SearchPublishNewsByNewsInThread(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var threadId = Utility.ConvertToLong(body.Get("threadId"));
                var zoneId = Utility.ConvertToInt(body.Get("zoneid"), 0);
                var keyWords = body.Get("keywords");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 15);
                var totalRows = 0;

                var data = new NewsServices().SearchNewsPublishExcludeNewsInThread(zoneId, keyWords, threadId, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_thread_id")]
        public HttpResponseMessage SearchPublishNewsByThreadId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var threadId = Utility.ConvertToLong(body.Get("threadId"));
                var status = Utility.ConvertToInt(body.Get("status"),-1);
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 15);
                var totalRows = 0;

                var data = new NewsServices().SearchNewsByThreadIdWithPaging(threadId, status, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        [HttpPost, Route("get_thread_by_id")]
        public HttpResponseMessage GetThreadEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));

                var news = new ThreadServices().GetThreadByThreadId(id);
                if (news == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotThreaObject]));
                }                
                return this.ToJson(WcfActionResponse<ThreadDetailEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_thread_embed")]
        public HttpResponseMessage GetListThreadEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
             
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"),0);
                var type = Utility.ConvertToInt(body.Get("type"),1);
            
                var data = new ThreadServices().GetListThreadEmbed(zoneId, type);               
                return this.ToJson(WcfActionResponse<List<BoxThreadEmbedEntity>>.CreateSuccessResponse(data, "Success"));                
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion
    }
}
