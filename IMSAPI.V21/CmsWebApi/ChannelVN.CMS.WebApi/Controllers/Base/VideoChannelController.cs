﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/videochannel")]
    public class VideoChannelController : ApiController
    {
        #region Action save
        [HttpPost, Route("save")]
        public HttpResponseMessage SaveVideoChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var publisherId = Utility.ConvertToInt(body.Get("PublisherId"), 0);
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NameVideoChannelNotEmpty]));
                }
                var avatar = body.Get("Avatar");
                var description = body.Get("Description");
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var introClip = body.Get("IntroClip");
                var channelRelation = body.Get("ChannelRelation");
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var url = Utility.ConvertTextToLink(name);
                var cover = body.Get("Cover");
                var zoneName = body.Get("ZoneName");

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var labelId = Utility.ConvertToInt(body.Get("LabelId"), 0);

                var metaJson = body.Get("MetaJson");
                var mode = Utility.ConvertToInt(body.Get("Mode"), 0);
                var metaAvatar = body.Get("MetaAvatar");

                var channel = new VideoChannelEntity
                {
                    Id = id,
                    PublisherId = publisherId,
                    Name = name,
                    Avatar = avatar,
                    Description = description,
                    Priority = priority,
                    IntroClip = introClip,
                    ChannelRelation = channelRelation,
                    Status = status,
                    Url = url,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    FollowCount = 0,
                    Rank = 0,
                    VideoCount = 0,
                    ZoneId = zoneId,
                    LabelId = labelId,
                    Cover=cover,
                    ZoneName=zoneName,
                    MetaJson=metaJson,
                    Mode=mode,
                    MetaAvatar=metaAvatar
                };
                var zoneIdList = body.Get("ZoneIdList");
                var labelIdList = body.Get("LabelIdList");                
                var playlistIdList = body.Get("PlayListIdList");
                var videoIdList = body.Get("VideoIdList");

                var result = new VideoChannelServices().SaveVideoChannel(channel, zoneIdList, labelIdList, playlistIdList, videoIdList);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("delete_channel")]
        public HttpResponseMessage DeleteVideoLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new VideoChannelServices().DeleteChannel(id);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_video_channel")]
        public HttpResponseMessage SearchVideoChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("keyword") ?? string.Empty;
                var zoneIds = body.Get("zoneIds") ?? string.Empty;
                var labelIds = body.Get("labelIds") ?? string.Empty;
                var playlistIds = body.Get("playlistIds") ?? string.Empty;
                var videoIds = body.Get("videoIds") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("status"), -1);

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";

                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");

                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("num"), 20);

                var totalRows = 0;

                var data = new VideoChannelServices().SearchVideoChannel(keyword, zoneIds, labelIds, playlistIds, videoIds, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, VideoChannels = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region get
        [HttpPost, Route("get_video_channel_by_id")]
        public HttpResponseMessage GetVideoChannelDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new VideoChannelServices().GetVideoChannelDetailEdit(id, username);
                return this.ToJson(WcfActionResponse<VideoChannelCachedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion
    }
}
