﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.EndUser;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/enduser")]
    public class EndUserController : ApiController
    {
        #region Action save
        [HttpPost, Route("save")]
        public HttpResponseMessage SaveEndUser(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var email = body.Get("Email");
                var password = body.Get("Password");
                var name = body.Get("Name");
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Name user not empty."));
                }
                var vietId = body.Get("VietId");
                var facebookId = body.Get("FacebookId");
                var googleId = body.Get("GoogleId");
                var phone = body.Get("Phone");
                var avatar = body.Get("Avatar");
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var sex = Utility.ConvertToInt(body.Get("Sex"), 1);
                var birthday = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("Birthday"), "dd/MM/yyyy");
                var address = body.Get("Address");
                               
                var channel = new EndUserEntity
                {
                    Id = id,
                    Email = email,
                    Password=password,                    
                    Name = name,
                    VietId=vietId,
                    FacebookId=facebookId,
                    GoogleId=googleId,
                    Phone = phone,
                    Avatar = avatar,                                       
                    Status = status,
                    Sex = sex,
                    Birthday=birthday,
                    Address=address,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now                                        
                };
                
                var result = new EndUserServices().SaveEndUser(channel);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        #endregion

        #region Search
        [HttpPost, Route("search_end_user")]
        public HttpResponseMessage SearchEndUser(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("keyword") ?? string.Empty;                
                var status = Utility.ConvertToInt(body.Get("status"), 1);

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";

                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");

                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("num"), 20);

                var totalRows = 0;

                var data = new EndUserServices().SearchEndUser(keyword, status, fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, News = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region get
        [HttpPost, Route("get_end_user_by_id")]
        public HttpResponseMessage GetEndUserDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new EndUserServices().GetEndUserDetail(id, username);
                return this.ToJson(WcfActionResponse<EndUserEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region User follow

        [HttpPost, Route("user_follow_playlist")]
        public HttpResponseMessage UserFollowPlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var playListId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);
                               
                var userFollow = new PlayListFollowEntity
                {
                    PlayListId = playListId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };                

                var result = new EndUserServices().UserFollowPlayList(userFollow);                
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_follow_zonevideo")]
        public HttpResponseMessage UserFollowZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new ZoneVideoFollowEntity
                {
                    ZoneId = zoneId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserFollowZoneVideo(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_follow_channel")]
        public HttpResponseMessage UserFollowChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var channelId = Utility.ConvertToInt(body.Get("ChannelId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new VideoChannelFollowEntity
                {
                    ChannelId = channelId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserFollowChannel(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_follow_label")]
        public HttpResponseMessage UserFollowLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var labelId = Utility.ConvertToInt(body.Get("LabelId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new VideoLabelFollowEntity
                {
                    LabelId = labelId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserFollowLabel(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_follow_publisher")]
        public HttpResponseMessage UserFollowPublisher(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var publisherId = Utility.ConvertToInt(body.Get("PublisherId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new PublisherFollowEntity
                {
                    PublisherId = publisherId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserFollowPublisher(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region User unfollow

        [HttpPost, Route("user_unfollow_playlist")]
        public HttpResponseMessage UserUnFollowPlayList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var playListId = Utility.ConvertToInt(body.Get("PlayListId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new PlayListFollowEntity
                {
                    PlayListId = playListId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserUnFollowPlayList(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_unfollow_zonevideo")]
        public HttpResponseMessage UserUnFollowZoneVideo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new ZoneVideoFollowEntity
                {
                    ZoneId = zoneId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserUnFollowZoneVideo(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_unfollow_channel")]
        public HttpResponseMessage UserUnFollowChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var channelId = Utility.ConvertToInt(body.Get("ChannelId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new VideoChannelFollowEntity
                {
                    ChannelId = channelId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserUnFollowChannel(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_unfollow_label")]
        public HttpResponseMessage UserUnFollowLabel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var labelId = Utility.ConvertToInt(body.Get("LabelId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new VideoLabelFollowEntity
                {
                    LabelId = labelId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserUnFollowLabel(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("user_unfollow_publisher")]
        public HttpResponseMessage UserUnFollowPublisher(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var publisherId = Utility.ConvertToInt(body.Get("PublisherId"), 0);
                var userId = Utility.ConvertToInt(body.Get("UserId"), 0);

                var userFollow = new PublisherFollowEntity
                {
                    PublisherId = publisherId,
                    UserId = userId,
                    FollowedDate = DateTime.Now
                };

                var result = new EndUserServices().UserUnFollowPublisher(userFollow);
                if (result.Success)
                {

                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region Thống kê

        //thống kê người dùng join
        //Thống kê người dùng active
        //Tỷ lệ active / all client
        [HttpPost, Route("statistic_end_user")]
        public HttpResponseMessage StatictisEndUser(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("To")) ? "" : body.Get("To") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");
                

                var data = new EndUserServices().GetStatisticStatus(status, fromDate, toDate);
                return this.ToJson(WcfActionResponse<List<UserStatisticEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        //Thống kê lượng theo dõi
        [HttpPost, Route("statistic_follow_playlist")]
        public HttpResponseMessage StatictisFollowPlaylist(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("To")) ? "" : body.Get("To") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");


                var data = new EndUserServices().StatictisFollowPlaylist(username, fromDate, toDate);
                return this.ToJson(WcfActionResponse<List<PlayListCountEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("statistic_follow_videochannel")]
        public HttpResponseMessage StatictisFollowVideoChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy");
                var to = string.IsNullOrEmpty(body.Get("To")) ? "" : body.Get("To") + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to, "dd/MM/yyyy HH:mm:ss");


                var data = new EndUserServices().StatictisFollowVideoChannel(username, fromDate, toDate);
                return this.ToJson(WcfActionResponse<List<VideoChannelCountEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion
    }
}
