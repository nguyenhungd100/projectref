﻿using ChannelVN.CMS.BoSearch.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Nodejs;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Models.Base;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.SEO.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using HtmlAgilityPack;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.LiveMatch.Entity;
using ChannelVN.BigStory.Entity;
using Newtonsoft.Json;
using ChannelVN.CMS.Entity.Base.Zone;
using ChannelVN.CMS.Entity.Base.NewsPosition;
using ChannelVN.CMS.Entity.Base.NewsMobileStream;
using ChannelVN.CMS.Entity.Base.Security;
using System.Net;
using ChannelVN.CMS.WebApi.Services.ExternalServices.Kenh14;
using ChannelVN.CMS.Entity.External.Kenh14.HotNewsConfig;
using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.Expert.Entity;
using ChannelVN.InfoGraph.Entity;
using com.mxgraph;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml;
using System.Drawing.Imaging;
using ChannelVN.SocialNetwork.Entity;
using System.Dynamic;
using ChannelVN.CMS.Entity.Base.NewsWarning;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Models;
using ChannelVN.CMS.BO.Base.Zone;
using System.Threading;
using ChannelVN.CMS.Entity.ErrorCode;
using ChannelVN.SecureLib.TwoFactor;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/base/news")]
    public class NewsController : ApiController
    {
        #region For News

        #region Update & Insert
        [HttpPost, Route("insert_news")]
        public HttpResponseMessage InsertNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("Id"));
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var title = body.Get("Title")??"";
                if(!string.IsNullOrEmpty(title) && title.Length > 255)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TieuDeBaiVietVuotQua255KyTu]));
                }
                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var bodyContent = body.Get("Body");
                //bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInSlide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("AdStore"), false);
                var isPr = Utility.ConvertToBoolean(body.Get("IsPr"), false);
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceURL");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"), 0);
                if (newsType < 0)
                {
                    newsType = (int)NewsType.Normal;
                }
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                if (type < 0)
                {
                    type = (int)NewsType.Normal;
                }
                
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                //publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                //var interviewId = body.Get("InterviewId")??string.Empty;
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = string.Empty;
                        if (data.Length > 0)
                            authorName = data[0];
                        var note = string.Empty;
                        if (data.Length>1)
                            note = data[1];

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));
                var action = body.Get("action");

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);

                var label = Utility.ConvertToInt(body.Get("Label"), 0);
                var labelTime = body.Get("LabelTime");
                var lstLabelValue = new List<dynamic>();
                if (label > 0)
                {
                    dynamic labelType = new ExpandoObject();
                    labelType.Type = NewsServices.EnumLabelType.LabelType;
                    labelType.Value = label;
                    lstLabelValue.Add(labelType);
                    if (label == (int)EnumNewsLabelType.UpdateLabel)
                    {
                        labelType = new ExpandoObject();
                        labelType.Type = NewsServices.EnumLabelType.LabelTime;
                        labelType.Value = labelTime;
                        lstLabelValue.Add(labelType);
                    }
                    initSapo = JsonConvert.SerializeObject(lstLabelValue);
                }
                //chinhnb 09/03/2018
                var isNotify = Utility.ConvertToBoolean(body.Get("IsNotify"), false);
                //tin tâm sự
                var parentNewsId = Utility.ConvertToLong(body.Get("ParentNewsId"));
                //bai tu san xuat
                var isProd = Utility.ConvertToBoolean(body.Get("IsProd"), false);

                var news = new NewsEntity
                {
                    Id = newsId,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = distributionDate,
                    Sapo = RemoveChar8(sapo),
                    Title = RemoveChar8(title),
                    SubTitle = RemoveChar8(subTitle),
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = RemoveChar8(initSapo),
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    IsPr=isPr,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = RemoveChar8(shortTitle),
                    BonusPrice = bonusPrice,
                    ParentNewsId= parentNewsId,
                    IsProd=isProd
                };
                var newsExtensions = new List<NewsExtensionEntity>();
                var service = Services.Services.CreatedInstance<Services.Base.NewsServices>(body);

                var result = service.InsertNews(news, zoneId, zoneRelationIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, accountName, authorList.ToArray(), newsChildOrder, sourceId, newsExtensions, action);

                if (result.Success)
                {
                    if (newsId <= 0)
                    {
                        newsId = Utility.ConvertToLong(result.Data);
                    }
                    else
                    {
                        var responeDatas = result.Data.ToString().Split(';');
                        result.Data = responeDatas[0];
                    }

                    var metaKeywordFocus = body.Get("MetaKeywordFocus");
                    var metaKeywordSub = body.Get("MetaKeywordSub");
                    var metaTitle = body.Get("MetaTitle");
                    var metaDescription = body.Get("MetaDescription");
                    var metaNewsKeyword = body.Get("MetaNewsKeyword");
                    var metaKeyword = body.Get("MetaKeyword");
                    var currentTopic = Utility.ConvertToInt(body.Get("CurrentTopic"),0);
                    var topicId = Utility.ConvertToInt(body.Get("TopicId"),0);
                    var listTopicId = body.Get("ListTopicId");
                    var expertId = Utility.ConvertToInt(body.Get("ExpertId"), 0);
                    var quote = body.Get("Quote");
                    var stickerId = Utility.ConvertToInt(body.Get("Sticker"), 0);
                    var brandId = body.Get("brandcontent")??"";
                    var listSpecialRelation = body.Get("SpecialNewsRelationIdList");
                    var currentThread = Utility.ConvertToInt(body.Get("CurrentThread"), 0);
                    //threadId = Utility.ConvertToInt(body.Get("ThreadId"), 0);
                    var listThreadId = body.Get("ListThreadId");

                    TaskUpdateDataAsync(newsId, zoneId, metaKeywordFocus, metaKeywordSub, metaTitle, metaDescription, metaNewsKeyword, metaKeyword, shortTitle, currentTopic, topicId, listTopicId, expertId, quote, stickerId, brandId, listSpecialRelation, newsRelationIdList, listThreadId, threadId, currentThread);

                    //call unlock
                    UnlockedNewsInUsing(newsId, accountName);
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "InsertNews => " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        private void TaskUpdateDataAsync(long newsId, int zoneId, string metaKeywordFocus, string metaKeywordSub, string metaTitle, string metaDescription, string metaNewsKeyword, string metaKeyword, string shortTitle, int currentTopic, int topicId, string listTopicId, int expertId, string quote, int stickerId, string brandId, string listSpecialRelation, string newsRelationIdList, string listThreadId="", int threadId=0, int currentThread=0)
        {
            var task = Task.Run(() =>
            {
                //Cập nhật SEO
                #region For Update Seo meta news action

                var seoService = new SeoServices();
                var seoMetaNews = seoService.GetSeoMetaNewsById(newsId);
                if (seoMetaNews == null)
                {
                    seoMetaNews = new SEOMetaNewsEntity();
                }
                seoMetaNews.MetaTitle = metaTitle;
                seoMetaNews.MetaKeyword = metaKeyword;
                seoMetaNews.MetaNewsKeyword = metaNewsKeyword;
                seoMetaNews.MetaDescription = metaDescription;
                seoMetaNews.KeywordFocus = metaKeywordFocus;
                seoMetaNews.SocialTitle = shortTitle;
                seoMetaNews.KeywordSub = metaKeywordSub;

                if (seoMetaNews.NewsId <= 0)
                {
                    seoMetaNews.NewsId = newsId;
                    seoService.SEOMetaNewsInsert(seoMetaNews);
                }
                else
                {
                    seoService.SEOMetaNewsUpdate(seoMetaNews);
                }
                
                if (newsId > 0)
                {                    
                    // lưu tin liên quan dạng ảnh
                    try
                    {
                        if (!string.IsNullOrEmpty(listSpecialRelation))
                        {
                            new Kenh14NewsServices().UpdateSpecialNewsRelation(newsId, zoneId, listSpecialRelation);

                            var arrId = newsRelationIdList.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                            List<NewsPublishForNewsRelationEntity> lstRelation = arrId.Select(ids => new NewsPublishForNewsRelationEntity() { NewsId = Utility.ConvertToLong(ids), NewsRelationType = 1 }).ToList();
                            arrId = listSpecialRelation.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                            lstRelation.AddRange(arrId.Select(ids => new NewsPublishForNewsRelationEntity() { NewsId = Utility.ConvertToLong(ids), NewsRelationType = 2 }));

                            new NewsServices().UpdateNewsRelationJson(newsId, lstRelation);
                        }                        
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "UpdateNewsRelationJson =>" + ex.Message);
                    }
                    
                    try
                    {
                        //Topic muil -> topicId dùng cho topic chính
                        if (!string.IsNullOrEmpty(listTopicId))
                        {
                            new TopicServices().MultiUpdateTopicNews(listTopicId, newsId, topicId);
                        }
                        else
                        {
                            //Kiểu cũ -> 1 topic in news
                            //current topic->xóa
                            if (currentTopic > 0)
                            {
                                new TopicServices().UpdateTopicNews(currentTopic, newsId.ToString(), string.Empty);
                            }
                            //add topic
                            if (topicId > 0)
                            {
                                new TopicServices().UpdateTopicNews(topicId, string.Empty, newsId.ToString());
                            }
                        }                       
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "MultiUpdateTopicNews =>" + ex.Message);
                    }
                    
                    try
                    {
                        //Thread muil -> threadId dùng cho topic chính
                        if (!string.IsNullOrEmpty(listThreadId))
                        {
                            new ThreadServices().MultiUpdateThreadNews(listThreadId, newsId, threadId);
                        }
                        else
                        {
                            //Kiểu cũ -> 1 thread in news
                            //current thread->xóa
                            if (currentThread > 0)
                            {
                                new ThreadServices().UpdateThreadNews(currentTopic, newsId.ToString(), string.Empty);
                            }
                            //add thread
                            if (threadId > 0)
                            {
                                new ThreadServices().UpdateThreadNews(topicId, string.Empty, newsId.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "MultiUpdateThreadNews =>" + ex.Message);
                    }

                    // check share facebook box
                    //try
                    //{
                    //    HtmlDocument doc = new HtmlDocument();
                    //    doc.LoadHtml(currentNews.Body);
                    //    var lstFacebookBox = doc.DocumentNode.SelectNodes("//div//a[@class='fbPhotoButton' and @nid='0']");
                    //    if (lstFacebookBox != null)
                    //    {
                    //        foreach (var facebookBox in lstFacebookBox)
                    //        {
                    //            facebookBox.Attributes["nid"].Value = newsId.ToString();
                    //        }
                    //        NewsServices.UpdateDetail(newsId, currentNews.Title, currentNews.Sapo, doc.DocumentNode.OuterHtml,
                    //                                  PolicyProviderManager.Provider.GetAccountName());
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    //}

                    // Update Video In News
                    //try
                    //{
                    //    var autoPlayNewsId = GetQueryString.GetPost("AutoPlayNewsId", "");
                    //    if (type == 13 && !string.IsNullOrEmpty(autoPlayNewsId))
                    //    {
                    //        MediaServices.UpdateVideoInNews(autoPlayNewsId, newsId);
                    //    }
                    //}
                    //catch (Exception ex)
                    //{
                    //    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    //}

                    //Expert
                    try
                    {
                        //Logger.WriteLog(Logger.LogType.Debug, string.Format("NewsId {0}, ExpertId {1}, Quote {2}", newsId, expertId, quote));
                        var expertNews = new ExpertInNews { NewsId = newsId, ExpertId = expertId, Quote = quote };
                        new ExpertServices().UpdateExpertInNews(expertNews);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "UpdateExpertInNews =>" + ex.Message);
                    }

                    //Sticker
                    try
                    {                        
                        //if (stickerId > 0)
                        //{
                            new Kenh14Services().AddNewsSticker(newsId.ToString(), stickerId);
                        //}
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "AddNewsSticker =>" + ex.Message);
                    }

                    //Brand Content
                    try
                    {        
                        if(!string.IsNullOrEmpty(brandId))                
                            new BrandContentServices().BrandInNews_Insert(brandId, newsId);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, "BrandInNews_Insert =>" + ex.Message);
                    }
                    
                    //đếm ảnh video trong bài
                    //SyncData(newsId, bodyContent);
                }
                #endregion
            });
            try
            {
                task.Wait(TimeSpan.FromSeconds(3));
            }
            catch { }
        }
        private string RemoveChar8(string text)
        {            
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    var char_arr = text.ToCharArray().ToList();
                    char_arr.Remove((char)8);
                    return string.Concat(char_arr);
                }
            }
            catch { }
            return text;
        }

        [HttpPost, Route("update_news")]
        public HttpResponseMessage UpdateNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("Id"));
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                if (newsId > 0)
                {
                    var oldNews = new NewsServices().GetNewsForValidateById(newsId);
                    if (oldNews != null && oldNews.Status == (int)NewsStatus.Published)
                    {
                        //bật otp
                        if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                        {
                            var otp = body.Get("Otp");
                            var SecurityServices = new SecurityServices();
                            var responseData = SecurityServices.IsValidOTPV2(otp, accountName);
                            if (!responseData.Success)
                            {
                                //gửi opt tới telegram
                                var sendOtp = SecurityServices.GetOtpAndSendOtp(accountName, nameSpace);
                                if (sendOtp == null)
                                {
                                    responseData.ErrorCode = 12002;
                                    responseData.Message = "Tài khoản của bạn chưa đăng ký nhận OTP qua Telegram.";
                                }
                                return this.ToJson(responseData);
                            }
                        }
                    }
                }
                
                //chinhnb tạm bỏ 06/09/2019
                //var updateAnyway = Utility.ConvertToBoolean(body.Get("UpdateAnyway"));
                //if (newsId > 0 && IsNewsInUsing(accountName, newsId))
                //{
                //    // Nếu đồng ý sửa ngay khi đang bị khóa (có người khác đang sửa) => unlock bài này va update nguoi dang sua
                //    if (updateAnyway)
                //    {
                //        UnlockedNewsInUsing(newsId, accountName);
                //        AddNewsInUsing(newsId, accountName);
                //    }
                //    else // Nếu không đồng ý sửa bài bị khóa bởi người khác hoặc mới yêu cầu sửa lần đầu (chưa xác nhận) thì báo lỗi
                //    {
                //        // Đang bị lock thì báo lỗi
                //        return this.ToJson(WcfActionResponse.CreateErrorResponse((int)ErrorMapping.ErrorCodes.UpdateNewsLockedByOtherUserForEdit,
                //                                                string.Format(
                //                                                    ErrorMapping.Current[ErrorMapping.ErrorCodes.UpdateNewsLockedByOtherUserForEdit],
                //                                                    DictsNewsInUsing[newsId])));
                //    }
                //}

                var title = body.Get("Title");
                if (!string.IsNullOrEmpty(title) && title.Length > 255)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.TieuDeBaiVietVuotQua255KyTu]));
                }
                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var bodyContent = body.Get("Body");
                //bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);->mất dấu + do ko encode
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInSlide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("AdStore"), false);
                var isPr = Utility.ConvertToBoolean(body.Get("IsPr"), false);
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceURL");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"),0);
                if (newsType < 0)
                {
                    newsType = (int)NewsType.Normal;
                }
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                if (type < 0)
                {
                    type = (int)NewsType.Normal;
                }
                
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                //publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                //var interviewId = body.Get("InterviewId") ?? string.Empty;
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = string.Empty;
                        if (data.Length > 0)
                            authorName = data[0];
                        var note = string.Empty;
                        if (data.Length > 1)
                            note = data[1];

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);

                var label = Utility.ConvertToInt(body.Get("Label"), 0);
                var labelTime = body.Get("LabelTime");
                var lstLabelValue = new List<dynamic>();
                if (label > 0)
                {
                    dynamic labelType = new ExpandoObject();
                    labelType.Type = NewsServices.EnumLabelType.LabelType;
                    labelType.Value = label;
                    lstLabelValue.Add(labelType);
                    if (label == (int)EnumNewsLabelType.UpdateLabel)
                    {
                        labelType = new ExpandoObject();
                        labelType.Type = NewsServices.EnumLabelType.LabelTime;
                        labelType.Value = labelTime;
                        lstLabelValue.Add(labelType);
                    }
                    initSapo = JsonConvert.SerializeObject(lstLabelValue);
                }                
                //chinhnb 09/03/2018
                var isNotify = Utility.ConvertToBoolean(body.Get("IsNotify"), false);
                //tin tâm sự
                var parentNewsId = Utility.ConvertToLong(body.Get("ParentNewsId"));
                //bai tu san xuat
                var isProd = Utility.ConvertToBoolean(body.Get("IsProd"), false);

                var news = new NewsEntity
                {
                    Id = newsId,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = distributionDate,
                    Sapo = RemoveChar8(sapo),
                    Title = RemoveChar8(title),
                    SubTitle = RemoveChar8(subTitle),
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = RemoveChar8(initSapo),
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    IsPr=isPr,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = RemoveChar8(shortTitle),
                    BonusPrice = bonusPrice,
                    ParentNewsId=parentNewsId,
                    IsProd= isProd
                };
                var newsExtensions = new List<NewsExtensionEntity>();

                var service = Services.Services.CreatedInstance<Services.Base.NewsServices>(body);

                var result = service.UpdateNews(news, zoneId, zoneRelationIdList, tagIdList,
                                                tagIdListForPrimary,
                                                newsRelationIdList, accountName,
                                                isRebuildLink, authorList.ToArray(),
                                                newsChildOrder, sourceId, publishedContent, newsExtensions, isNotify);
                if (result.Success)
                {                    
                    if (newsId <= 0)
                    {
                        newsId = Utility.ConvertToLong(result.Data);
                    }
                    else
                    {
                        var responeDatas = result.Data.ToString().Split(';');
                        result.Data = responeDatas[0];
                    }

                    var metaKeywordFocus = GetQueryString.GetPost("MetaKeywordFocus", "");
                    var metaKeywordSub = GetQueryString.GetPost("MetaKeywordSub", "");
                    var metaTitle = GetQueryString.GetPost("MetaTitle", "");
                    var metaDescription = GetQueryString.GetPost("MetaDescription", "");
                    var metaNewsKeyword = GetQueryString.GetPost("MetaNewsKeyword", "");
                    var metaKeyword = GetQueryString.GetPost("MetaKeyword", "");
                    var currentTopic = Utility.ConvertToInt(body.Get("CurrentTopic"));
                    var topicId = Utility.ConvertToInt(body.Get("TopicId"));
                    var listTopicId = body.Get("ListTopicId");
                    var expertId = Utility.ConvertToInt(body.Get("ExpertId"), 0);
                    var quote = body.Get("Quote");
                    var stickerId = Utility.ConvertToInt(body.Get("Sticker"), 0);
                    var brandId = body.Get("brandcontent")??"";
                    var listSpecialRelation = body.Get("SpecialNewsRelationIdList");                    
                    var currentThread = Utility.ConvertToInt(body.Get("CurrentThread"), 0);
                    //threadId = Utility.ConvertToInt(body.Get("ThreadId"), 0);
                    var listThreadId = body.Get("ListThreadId");

                    TaskUpdateDataAsync(newsId, zoneId, metaKeywordFocus, metaKeywordSub, metaTitle, metaDescription, metaNewsKeyword, metaKeyword, shortTitle, currentTopic, topicId, listTopicId, expertId, quote, stickerId, brandId, listSpecialRelation, newsRelationIdList, listThreadId, threadId, currentThread);
                }

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UpdateNews => " + ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete_news")]
        public HttpResponseMessage MoveToTrash(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().MoveToTrash(newsId, username);
                return this.ToJson<WcfActionResponse>(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_news_title")]
        public HttpResponseMessage UpdateOnlyTitle(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var title = body.Get("title");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateOnlyTitle(newsId, RemoveChar8(title), username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_isonhome")]
        public HttpResponseMessage UpdateIsOnHome(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsId"));
                var isOnHome = Utility.ConvertToBoolean(body.Get("isOnHome"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateIsOnHome(newsId, isOnHome, username);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_news_isfocus")]
        public HttpResponseMessage UpdateIsFocus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsId"));
                var isFocus = Utility.ConvertToBoolean(body.Get("isFocus"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateIsFocus(newsId, isFocus, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_seoreviewstatus")]
        public HttpResponseMessage UpdateSeoReviewStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsId"));                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateSeoReviewStatus(newsId, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_avatar")]
        public HttpResponseMessage UpdateNewsAvatar(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var avatar = body.Get("avatar");
                var position = Utility.ConvertToInt(body.Get("position"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateNewsAvatar(newsId, avatar, position);
                return this.ToJson<WcfActionResponse>(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_detail")]
        public HttpResponseMessage UpdateDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var title = body.Get("title");
                var sapo = body.Get("sapo");
                var bodyContent = body.Get("body");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateDetail(newsId, RemoveChar8(title), RemoveChar8(sapo), bodyContent, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_detail_simple")]
        public HttpResponseMessage UpdateDetailSimple(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var title = body.Get("title");
                var avatar = body.Get("avatar");
                var avatar2 = body.Get("avatar2");
                var avatar3 = body.Get("avata3");
                var avatar4 = body.Get("avata4");
                var avatar5 = body.Get("avata5");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateDetailSimple(newsId, RemoveChar8(title), avatar, avatar2, avatar3, avatar4, avatar5, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_status")]
        public HttpResponseMessage UpdateNewsStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var action = body.Get("Action");
                var listNewsId = body.Get("NewsId");
                if (string.IsNullOrEmpty(listNewsId))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamNewsIdNotExit]));
                }
                var newsIds = listNewsId.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                var data = WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.AcctionIsNotSupport]);
                //var newsId = 0;
                var listNewsIdUpdated = string.Empty;
                var receiverAccountName = string.Empty;
                var note = string.Empty;
                var isSendOver = false;
                switch (action)
                {
                    case "send":
                        receiverAccountName = body.Get("receiver");
                        isSendOver = Utility.ConvertToBoolean(body.Get("isSendOver"));

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Send(id, username, isSendOver, receiverAccountName);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "return":
                        receiverAccountName = body.Get("receiver")??"";
                        isSendOver = Utility.ConvertToBoolean(body.Get("isSendOver"));
                        note = body.Get("note")??"";

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Return(id, username, isSendOver, receiverAccountName, note);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "receive":
                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Receive(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "release":
                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Release(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "forward":
                        receiverAccountName = body.Get("receiver");

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Forward(id, username, receiverAccountName);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "publish":
                        var newsPositionTypeForHome = Utility.ConvertToInt(body.Get("PositionTypeForHome"));
                        var newsPositionOrderForHome = Utility.ConvertToInt(body.Get("PositionOrderForHome"));
                        var newsPositionTypeForList = Utility.ConvertToInt(body.Get("PositionTypeForList"));
                        var newsPositionOrderForList = Utility.ConvertToInt(body.Get("PositionOrderForList"));
                        var publishedContent = body.Get("PublishedContent");                        
                        //bật otp
                        if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                        {
                            var otp = body.Get("Otp");
                            var SecurityServices = new SecurityServices();
                            var responseData = SecurityServices.IsValidOTPV2(otp, username);
                            if (!responseData.Success)
                            {
                                //gửi opt tới telegram
                                var sendOtp = SecurityServices.GetOtpAndSendOtp(username, nameSpace);
                                if (sendOtp == null)
                                {
                                    responseData.ErrorCode = 12002;
                                    responseData.Message = "Tài khoản của bạn chưa đăng ký nhận OTP qua Telegram.";
                                }
                                return this.ToJson(responseData);
                            }
                        }

                        //chinhnb 09/03/2018
                        var isNotify = Utility.ConvertToBoolean(body.Get("IsNotify"), false);

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Publish(id, newsPositionTypeForHome, newsPositionOrderForHome, newsPositionTypeForList, newsPositionOrderForList, username, publishedContent, isNotify);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "unpublish":
                        //bật otp
                        if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                        {
                            var otp = body.Get("Otp");
                            var SecurityServices = new SecurityServices();
                            var responseData = SecurityServices.IsValidOTPV2(otp, username);
                            if (!responseData.Success)
                            {
                                //gửi opt tới telegram
                                var sendOtp = SecurityServices.GetOtpAndSendOtp(username, nameSpace);
                                if (sendOtp == null)
                                {
                                    responseData.ErrorCode = 12002;
                                    responseData.Message = "Tài khoản của bạn chưa đăng ký nhận OTP qua Telegram.";
                                }
                                return this.ToJson(responseData);
                            }
                        }

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().Unpublish(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "getback":
                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().GetBack(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "remove":
                        //bật otp
                        if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                        {
                            var otp = body.Get("Otp");
                            var SecurityServices = new SecurityServices();
                            var responseData = SecurityServices.IsValidOTPV2(otp, username);
                            if (!responseData.Success)
                            {
                                //gửi opt tới telegram
                                var sendOtp = SecurityServices.GetOtpAndSendOtp(username, nameSpace);
                                if (sendOtp == null)
                                {
                                    responseData.ErrorCode = 12002;
                                    responseData.Message = "Tài khoản của bạn chưa đăng ký nhận OTP qua Telegram.";
                                }
                                return this.ToJson(responseData);
                            }
                        }

                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().MoveToTrash(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "restore":
                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().ReceiveFromTrash(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                    case "updateandsend":
                        var newsId = Utility.ConvertToLong(listNewsId);
                        data = new NewsServices().ReceiveFromTrash(newsId, username);
                        if (data.Success)
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsId, "");
                        }
                        break;
                    case "updateandpublish":
                        newsId = Utility.ConvertToLong(listNewsId);
                        data = new NewsServices().ReceiveFromTrash(newsId, username);
                        if (data.Success)
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsId, "");
                        }
                        break;
                    case "crawlernews":
                        foreach (var id in newsIds.Select(Utility.ConvertToLong).Where(id => id > 0))
                        {
                            data = new NewsServices().ReceiveCrawlerNews(id, username);
                            if (data.Success)
                            {
                                listNewsIdUpdated += ";" + id;
                            }
                        }
                        if (!string.IsNullOrEmpty(listNewsIdUpdated))
                        {
                            data = WcfActionResponse.CreateSuccessResponse(listNewsIdUpdated.Remove(0, 1), "");
                        }
                        break;
                }
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("check_required_otp")]
        public HttpResponseMessage CheckRequiredOTP(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var action = body.Get("Action");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;                
                switch (action)
                {
                    case "update":
                    case "publish":
                    case "unpublish":
                    case "remove":
                        //bật otp
                        if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                        {
                            var otp = body.Get("Otp");
                            var SecurityServices = new SecurityServices();
                            var responseData = SecurityServices.IsValidOTPV2(otp, username);
                            if (!responseData.Success)
                            {
                                //gửi opt tới telegram
                                var sendOtp = SecurityServices.GetOtpAndSendOtp(username, nameSpace);
                                if (sendOtp == null)
                                {
                                    responseData.ErrorCode = 12002;
                                    responseData.Message = "Tài khoản của bạn chưa đăng ký nhận OTP qua Telegram.";
                                }
                                else {
                                    responseData.ErrorCode = 12003;
                                    responseData.Message = "Đã gửi mã OPT tới telegarm của bạn.";
                                    return this.ToJson(responseData);
                                }
                            }
                            return this.ToJson(responseData);
                        }
                        break;
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse());
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("register_telegram_otp")]
        public HttpResponseMessage RegisterTelegramOTP(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                                
                if (CmsChannelConfiguration.GetAppSettingInBoolean("OTP_ENABLE"))
                {                                        
                    var responseData = new SecurityServices().GetOtpAndSendSMS(username, nameSpace);

                    return this.ToJson(responseData);
                }                                 

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Kênh chưa mở đăng ký nhận otp qua telegram."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("set_news_extension")]
        public HttpResponseMessage SetNewsExtensionValue(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("newsid"));
                var value = body.Get("value") ?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("type"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().SetNewsExtensionValue(newsId, (EnumNewsExtensionType)type, value);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_news_by_errorcheck")]
        public HttpResponseMessage UpdateNewsByErrorCheck(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("Id"));
                var title = body.Get("Title");
                var shortTitle = body.Get("ShortTitle");
                var subTitle = body.Get("SubTitle");
                if (string.IsNullOrEmpty(shortTitle))
                {
                    shortTitle = subTitle;
                }                
                var sapo = body.Get("Sapo");
                var bodyContent = body.Get("Body");
                var avatarDesc = body.Get("AvatarDesc");
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var newsNote = body.Get("Note");                
                var tagIdList = body.Get("TagList");
                var tagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"),0);

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var news = new NewsEntity
                {
                    Id = newsId,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,                    
                    Sapo = RemoveChar8(sapo),
                    Title = RemoveChar8(title),
                    SubTitle = RemoveChar8(subTitle),
                    Body = bodyContent,              
                    Note= newsNote,
                    TagSubTitleId = tagSubTitleId,
                    ShortTitle= RemoveChar8(shortTitle)
                };

                var data = new NewsServices().UpdateNewsByErrorCheck(news, tagIdList, username);
                if (data.Success)
                {                    
                    var metaKeywordFocus = GetQueryString.GetPost("MetaKeywordFocus", "");
                    var metaKeywordSub = GetQueryString.GetPost("MetaKeywordSub", "");
                    var metaTitle = GetQueryString.GetPost("MetaTitle", "");
                    var metaDescription = GetQueryString.GetPost("MetaDescription", "");
                    var metaNewsKeyword = GetQueryString.GetPost("MetaNewsKeyword", "");
                    var metaKeyword = GetQueryString.GetPost("MetaKeyword", "");
                    var currentTopic = 0;
                                        
                    TaskUpdateDataAsync(newsId, 0, metaKeywordFocus, metaKeywordSub, metaTitle, metaDescription, metaNewsKeyword, metaKeyword, shortTitle, currentTopic, 0, "", 0, "", 0, "", "","");
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("crawler_news")]
        public HttpResponseMessage CrawlerNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if(!string.IsNullOrEmpty(accountName) && accountName!= "crawler")
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.AccountCrawlerUseApi]));
                }
                var parentNewsId = Utility.ConvertToLong(body.Get("NewsId"));
                if (parentNewsId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy param newsid."));
                }

                var title = body.Get("Title");
                var subTitle = body.Get("SubTitle");
                var sapo = body.Get("Sapo");
                var initSapo = body.Get("InitSapo");
                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                if (zoneId <= 0)
                {
                    zoneId = CmsChannelConfiguration.GetAppSettingInInt32("AppBotCrawlerApiFixZoneId");
                }

                var bodyContent = body.Get("Body");
                //bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var avatar = body.Get("Avatar");
                var avatar2 = body.Get("Avatar2");
                var avatar3 = body.Get("Avatar3");
                var avatar4 = body.Get("Avatar4");
                var avatar5 = body.Get("Avatar5");
                var avatarCustom = body.Get("AvatarCustom");
                var avatarDesc = body.Get("AvatarDesc");
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInslide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList");
                var zoneRelationIdList = body.Get("ListZoneId");
                var tagIdList = body.Get("TagList");
                var tagIdListForPrimary = body.Get("SubTitleList");
                var author = body.Get("Author");
                var listAuthorByNews = body.Get("ListAuthorByNews");
                var source = body.Get("Source");
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isBreakingNews = Utility.ConvertToBoolean(body.Get("IsBreakingNews"));
                var isAdStore = Utility.ConvertToBoolean(body.Get("IsAdstore"));
                var pegaBreakingNews = Utility.ConvertToInt(body.Get("PegaBreakingNews"));
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                var isRebuildLink = Utility.ConvertToBoolean(body.Get("IsRebuildLink"));
                var newsNote = body.Get("Note");
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceUrl");
                var newsType = Utility.ConvertToInt(body.Get("NewsType"));
                var type = Utility.ConvertToInt(body.Get("Type"));
                if (newsType == -1)
                {
                    newsType = (int)NewsType.Normal;
                }
                if (type == 0) type = newsType;
                var noteRoyalties = body.Get("NoteRoyalties");
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig");
                var publishedContent = body.Get("PublishedContent");
                publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);

                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));

                var sourceId = Utility.ConvertToInt(body.Get("SourceId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                if (priority == -1)
                {
                    priority = 0;//set cho db tinyint
                }

                //if (sourceId <= 0) source = "";
                var authorList = new List<string>();
                if (!string.IsNullOrEmpty(listAuthorByNews))
                {
                    var authorNameList = string.Empty;
                    var authorIdList = string.Empty;
                    var authorNoteList = string.Empty;

                    var authorParamList = listAuthorByNews.Split(',');
                    foreach (var group in authorParamList)
                    {
                        var match = Regex.Match(group, @"G(?<AuthorId>[0-9]+)\((?<data>.*?)\)");

                        if (!match.Success) continue;

                        var authorId = int.Parse(match.Groups["AuthorId"].ToString());
                        var data = match.Groups["data"].ToString().Replace("\"", string.Empty).Split(';');
                        var authorName = data[0];
                        var note = data[1] ?? string.Empty;

                        authorNameList += ";" + authorName;
                        authorNoteList += ";" + note;
                        authorIdList += ";" + authorId;
                    }
                    if (authorNameList != string.Empty) authorNameList = authorNameList.Substring(1);
                    if (authorIdList != string.Empty) authorIdList = authorIdList.Substring(1);
                    if (authorNoteList != string.Empty) authorNoteList = authorNoteList.Substring(1);
                    authorList.Add(authorIdList);
                    authorList.Add(authorNameList);
                    authorList.Add(authorNoteList);
                }

                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var shortTitle = body.Get("ShortTitle");
                var extensionType = Utility.ConvertToInt(body.Get("ExtensionType"));
                var action = "crawler_news";

                var bonusPrice = Utility.ConvertToDecimal(body.Get("BonusPrice"));

                if (extensionType > 0)
                    type = extensionType;

                if (type == 13)
                    sapo = HttpUtility.HtmlDecode(sapo);

                //tam fix cho wowholiday => bai mac dinh crawler la magazine
                if (WcfExtensions.WcfMessageHeader.Current.Namespace.ToLower().Equals("wowholiday"))
                    type = 27;

                var news = new NewsEntity
                {
                    Id = 0,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = distributionDate,
                    Sapo = sapo,
                    Title = RemoveChar8(title),
                    SubTitle = RemoveChar8(subTitle),
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = RemoveChar8(initSapo),
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsBreakingNews = isBreakingNews,
                    AdStore = isAdStore,
                    PegaBreakingNews = pegaBreakingNews,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    ShortTitle = RemoveChar8(shortTitle),
                    BonusPrice = bonusPrice,
                    ZoneId = zoneId,
                    Status= (int)NewsStatus.CrawlerNews,
                    ParentNewsId = parentNewsId
                };

                //nhận vào redis trước
                var keyId = "crawlernews";                
                var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyId, action, news);
                if (res > 0)
                {
                    //start job =>xử lý pop queue
                    TaskJobQueue(keyId);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse(res.ToString(), "update queue success."));                                     
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(6868, "Error update data in queue."));                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        private void TaskJobQueue(string keyId)
        {
            var checkStart = true;
            var thread = new Thread(() =>
            {
                try {
                    checkStart = false;
                    while (true)
                    {
                        Thread.Sleep(1000);

                        var queue = BoCached.CmsExtension.Queue.QueueDalFactory.PopQueue(keyId);

                        if (queue != null)
                        {
                            var news = queue.Message;
                            var zoneId = news.ZoneId;
                            var zoneRelationIdList = string.Empty;
                            var tagIdList = string.Empty;
                            var tagIdListForPrimary = string.Empty;
                            var newsRelationIdList = string.Empty;
                            var authorList = new List<string>();
                            var action = queue.Action;
                            var newsExtensions = new List<NewsExtensionEntity>();

                            //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(queue));

                            var result = new NewsServices().CrawlerNews(news);
                            if (!result.Success)
                            {
                                Logger.WriteLog(Logger.LogType.Warning, NewtonJson.Serialize(queue));

                                //var res = BoCached.CmsExtension.Queue.QueueDalFactory.AddQueue(keyId, action, news);
                                //if (res > 0)
                                //{
                                //    //start job =>xử lý pop queue
                                //    TaskJobQueue(keyId);
                                //}
                            }
                        }

                        if (queue == null)
                        {
                            checkStart = true;
                            break;
                        }
                    }
                }
                catch
                {
                    checkStart = true;
                }
                finally
                {
                    checkStart = true;
                }
            });
            
            if(checkStart)
                thread.Start();

        }

        #endregion

        #region Search

        [HttpPost, Route("list_news_by_status")]
        public HttpResponseMessage ListNewsByStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var status = body.Get("status");
                var newsStatus = NewsStatus.Unknow;
                if (!string.IsNullOrEmpty(status))
                {
                    newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                }
                var newsType = Utility.ConvertToInt(body.Get("type"),0);
                var pageIndex = Utility.ConvertToInt(body.Get("page"),0);
                var pageSize = Utility.ConvertToInt(body.Get("num"),20);
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"),0);
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }
                var filterBy = NewsFilterFieldForUsername.CreatedBy;

                var totalRows = 0;
                var data = new NewsServices().ListNewsByStatus(username, typeOfCheck, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news")]
        public HttpResponseMessage SearchNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var status = body.Get("status");
                var newsStatus = NewsStatus.Unknow;
                if (!string.IsNullOrEmpty(status))
                {
                    newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                }
                var newsType = NewsType.All;
                var type = body.Get("type");
                if (!string.IsNullOrEmpty(type))
                {
                    newsType = (NewsType)Utility.ConvertToInt(type);
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order)) 
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }
                var filterBy = NewsFilterFieldForUsername.CreatedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNews(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_subsite")]
        public HttpResponseMessage SearchNewsByZoneParent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var status = body.Get("status");
                var newsStatus = NewsStatus.Unknow;
                if (!string.IsNullOrEmpty(status))
                {
                    newsStatus = (NewsStatus)Utility.ConvertToInt(status);
                }
                var newsType = NewsType.All;
                var type = body.Get("type");
                if (!string.IsNullOrEmpty(type))
                {
                    newsType = (NewsType)Utility.ConvertToInt(type);
                }
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }
                var filterBy = NewsFilterFieldForUsername.CreatedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNewsByZoneParent(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_publish")]
        public HttpResponseMessage SearchNewsPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneIds = body.Get("zone") ?? string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                var newsStatus = NewsStatus.Published;
                var newsType = NewsType.All;
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.PublishedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNewsPublishES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_publish_subsite")]
        public HttpResponseMessage SearchNewsPublishSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneIds = body.Get("zone") ?? string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                var newsStatus = NewsStatus.Published;
                var newsType = NewsType.All;
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.PublishedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNewsPublishSubsiteES(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_publish_for_comment")]
        public HttpResponseMessage SearchNewsPublishForComment(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneIds = body.Get("zone") ?? string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                var newsStatus = NewsStatus.Published;
                var newsType = NewsType.All;
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.PublishedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNewsPublishForComment(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_publish_for_comment_subsite")]
        public HttpResponseMessage SearchNewsPublishForCommentSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneIds = body.Get("zone") ?? string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                var newsStatus = NewsStatus.Published;
                var newsType = NewsType.All;
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.PublishedBy;

                var totalRows = 0;
                var data = new NewsServices().SearchNewsPublishForCommentSubsite(keyword, typeOfCheck, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_type")]
        public HttpResponseMessage SearchNewsByType(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var newsType = (NewsType)Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var totalRows = 0;
                var data = new NewsServices().SearchNewsByType(username, keyword, author, newsType, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_news_by_type_subsite")]
        public HttpResponseMessage SearchNewsByTypeSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var newsType = (NewsType)Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var totalRows = 0;
                var data = new NewsServices().SearchNewsByTypeSubsite(username, keyword, author, newsType, zoneIds, fromDate, toDate, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_my_news")]
        public HttpResponseMessage ListMyNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                //var newsStatus = NewsStatus.Unknow;
                var newsType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }
                var filterBy = new List<int>();
                filterBy.Add(0);

                var totalRows = 0;
                var data = new NewsServices().ListMyNews(username, filterBy, sortBy, newsType, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("list_news_ispr")]
        public HttpResponseMessage ListNewsIsPr(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var keyword = body.Get("keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"), 0);
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), format);
                var toDate = Utility.ParseDateTime(body.Get("to"), format);
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var newsStatus = (NewsStatus)Utility.ConvertToInt(body.Get("status"));
                var filterBy = new List<int>();
                filterBy.Add(0);

                var totalRows = 0;
                var data = new NewsServices().ListNewsIsPr(keyword, username, zoneIds, newsStatus, filterBy, fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_ispr_subsite")]
        public HttpResponseMessage ListNewsIsPrSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var keyword = body.Get("keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"), 0);
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), format);
                var toDate = Utility.ParseDateTime(body.Get("to"), format);
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var newsStatus = (NewsStatus)Utility.ConvertToInt(body.Get("status"));
                var filterBy = new List<int>();
                filterBy.Add(0);

                var totalRows = 0;
                var data = new NewsServices().ListNewsIsPrSubsite(keyword, username, zoneIds, newsStatus, filterBy, fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_publish_by_date")]
        public HttpResponseMessage ListNewsPublishByDate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy HH:mm");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy HH:mm");
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var totalRows = 0;
                var data = new NewsServices().ListNewsPublishByDate(fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        [HttpPost, Route("list_news_by_multi_status")]
        public HttpResponseMessage ListNewsByMultiStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var listStatus = body.Get("status");                
                if (string.IsNullOrEmpty(listStatus))
                {
                    listStatus = string.Empty;
                }
                var newsType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }
                
                var totalRows = 0;
                var data = new NewsServices().ListNewsByMultiStatus(keyword, author, zoneIds, fromDate, toDate, username, listStatus, typeOfCheck, newsType, (int)sortBy, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_by_multi_status_subsite")]
        public HttpResponseMessage ListNewsByMultiStatusSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from"), "dd/MM/yyyy");
                var to_date = body.Get("to");
                if (!string.IsNullOrEmpty(to_date))
                    to_date = to_date + " 23:59:59";
                var toDate = Utility.ConvertToDateTimeByFormat(to_date, "dd/MM/yyyy HH:mm:ss");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var listStatus = body.Get("status");
                if (string.IsNullOrEmpty(listStatus))
                {
                    listStatus = string.Empty;
                }
                var newsType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }

                var totalRows = 0;
                var data = new NewsServices().ListNewsByMultiStatusSubsite(keyword, author, zoneIds, fromDate, toDate, username, listStatus, typeOfCheck, newsType, (int)sortBy, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_processing")]
        public HttpResponseMessage ListNewsProcessing(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var newsType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }

                var totalRows = 0;
                var data = new NewsServices().ListNewsProcessing(keyword, author, zoneIds, fromDate, toDate, username, typeOfCheck, newsType, (int)sortBy, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_processing_subsite")]
        public HttpResponseMessage ListNewsProcessingSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var newsType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                var typeOfCheck = Utility.ConvertToInt(body.Get("typeOfCheck"));
                var sortBy = NewsSortExpression.LastModifiedDateDesc;
                var order = body.Get("order");
                if (!string.IsNullOrEmpty(order))
                {
                    sortBy = (NewsSortExpression)Utility.ConvertToInt(order);
                }

                var totalRows = 0;
                var data = new NewsServices().ListNewsProcessingSubsite(keyword, author, zoneIds, fromDate, toDate, username, typeOfCheck, newsType, (int)sortBy, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_seo_review")]
        public HttpResponseMessage ListNewsSeoReview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var newsStatus = NewsStatus.Published;                
                var newsType = NewsType.All;               
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));
                
                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.CreatedBy;
                
                var reviewStatus = Utility.ConvertToInt(body.Get("reviewstatus"));
                if(reviewStatus<=0 || reviewStatus > 2)
                {
                    return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = 0, News = null }, "Success"));
                }

                var totalRows = 0;
                var data = new NewsServices().SearchNewsSeoReviewStatus(keyword, reviewStatus, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_seo_review_subsite")]
        public HttpResponseMessage ListNewsSeoReviewSubsite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var author = body.Get("author") ?? string.Empty;
                var createdBy = body.Get("createdby") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var zoneId = Utility.ConvertToInt(body.Get("zone"));
                var zoneIds = zoneId > 0 ? zoneId.ToString() : string.Empty;

                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from"), formats);
                var to = body.Get("to") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("to");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);

                var newsStatus = NewsStatus.Published;
                var newsType = NewsType.All;
                var pageIndex = Utility.ConvertToInt(body.Get("page"));
                var pageSize = Utility.ConvertToInt(body.Get("num"));

                var sortBy = NewsSortExpression.DistributionDateDesc;
                var filterBy = NewsFilterFieldForUsername.CreatedBy;

                var reviewStatus = Utility.ConvertToInt(body.Get("reviewstatus"));
                if (reviewStatus <= 0 || reviewStatus > 2)
                {
                    return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = 0, News = null }, "Success"));
                }

                var totalRows = 0;
                var data = new NewsServices().SearchNewsSeoReviewStatusSubsite(keyword, reviewStatus, author, username, zoneIds, fromDate, toDate, filterBy, sortBy, newsStatus, newsType, pageIndex, pageSize, ref totalRows, createdBy);
                return this.ToJson(WcfActionResponse<SearchNewsResponse>.CreateSuccessResponse(new SearchNewsResponse { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_news")]
        public HttpResponseMessage CountNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var status = body.Get("status");
                var type = Utility.ConvertToInt(body.Get("type"), -1);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().CountNews(username, status, type);
                return this.ToJson(WcfActionResponse<List<CountStatus>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region Get

        [HttpPost, Route("get_news_detail_by_id")]
        public HttpResponseMessage GetDetail(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetDetail(newsId, username);
                return this.ToJson(WcfActionResponse<NewsDetailForEditEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_news_detail_by_id_cddata")]
        public HttpResponseMessage GetDetailCDData(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("id"));
                //var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetDetailCDData(newsId, "");
                return this.ToJson(WcfActionResponse<NewsDetailForEditEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }        

        [HttpPost, Route("get_news_extension_by_type_and_value")]
        public HttpResponseMessage GetExtensionByTypeAndValue(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var type = Utility.ConvertToInt(body.Get("type"));
                var value = body.Get("value") ?? string.Empty;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetNewsExtensionByTypeAndVaue((EnumNewsExtensionType)type, value);
                return this.ToJson(WcfActionResponse<List<NewsExtensionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_news_extension_by_type_and_newsid")]
        public HttpResponseMessage GetExtensionByTypeAndNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var type = Utility.ConvertToInt(body.Get("type"));
                var news_id = Utility.ConvertToLong(body.Get("news_id"));

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetNewsExtensionValue(news_id, (EnumNewsExtensionType)type);
                return this.ToJson(WcfActionResponse<NewsExtensionEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        [HttpPost, Route("get_news_info_by_id")]
        public HttpResponseMessage GetNewsForValidateById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("id"));
                var data = new NewsServices().GetNewsForValidateById(newsId);

                return this.ToJson(WcfActionResponse<NewsForValidateEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_news_info_by_ids")]
        public HttpResponseMessage GetNewsByListNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsIds = body.Get("ids");
                if(string.IsNullOrEmpty(newsIds))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ids is not empty."));                

                var data = new NewsServices().GetNewsByListNewsId(NewsStatus.Published, newsIds.Replace(",", ";"));

                return this.ToJson(WcfActionResponse<List<NewsWithAnalyticEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #endregion

        #region For Zone

        [HttpGet, Route("get_all_zone")]
        public HttpResponseMessage GetAllZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var data = new NewsServices().GetAllZone();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Zones = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpGet, Route("get_zone_active")]
        public HttpResponseMessage GetAllZoneActive(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var data = new NewsServices().GetAllZoneActive();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_private_zone")]
        public HttpResponseMessage GetPrivateZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var parentZoneId = -1;
                var param = body.Get("parentZoneId");
                if (string.IsNullOrEmpty(param) || !int.TryParse(param.ToString(), out parentZoneId))
                {
                    parentZoneId = -1;
                }
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetPrivateZone(username, parentZoneId);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Zones = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_zone")]
        public HttpResponseMessage SaveZone(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Name is required"));

                var description = body.Get("Description") ?? string.Empty;
                //var shortURL = body.Get("ShortURL") ?? string.Empty;
                var sortOrder = Utility.ConvertToInt(body.Get("SortOrder"));
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                if (parentId == -1)
                {
                    parentId = 0;
                }
                var invisibled = Utility.ConvertToBoolean(body.Get("Invisibled"));
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var allowComment = Utility.ConvertToBoolean(body.Get("AllowComment"));
                var domain = body.Get("Domain") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var avatarcover = body.Get("AvatarCover") ?? string.Empty;
                var logo = body.Get("Logo") ?? string.Empty;
                var metaAvatar = body.Get("MetaAvatar") ?? string.Empty;
                var isRebuildUrl = Utility.ConvertToBoolean(body.Get("IsRebuildUrl"),false);
                var zoneContent = body.Get("ZoneContent") ?? string.Empty;

                var zone = new ZoneEntity
                {
                    Name = name,
                    Description = description,
                    CreatedDate = DateTime.Now,                    
                    ShortUrl = Utility.UnicodeToKoDauAndGach(name),
                    SortOrder = sortOrder,
                    ParentId = parentId,
                    Invisibled = invisibled,
                    Status = status,
                    AllowComment = allowComment,
                    Domain = domain,
                    Avatar=avatar,
                    AvatarCover=avatarcover,
                    Logo=logo,
                    MetaAvatar=metaAvatar,
                    ZoneContent= zoneContent,
                };                

                var data = new WcfActionResponse();
                if (id > 0)
                {
                    zone.Id = id;
                    zone.ModifiedDate = DateTime.Now;
                    if (!isRebuildUrl)
                    {
                        //ko cho phep update shorturl
                        var zoneDb = ZoneBo.GetZoneById(id);
                        if (zoneDb != null)
                        {
                            zone.ShortUrl = zoneDb.ShortUrl;
                        }
                    }

                    data = new NewsServices().Update(zone, accountName);
                }
                else
                {
                    data = new NewsServices().Insert(zone, accountName);
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }
        //là chuyên mục ẩn
        [HttpPost, Route("toggle_zone_invisibled")]
        public HttpResponseMessage ToggleZoneInvisibled(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().ToggleZoneInvisibled(id, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        //cho hiệu lực
        [HttpPost, Route("toggle_zone_status")]
        public HttpResponseMessage ToggleZoneStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().ToggleZoneStatus(id, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        //cho phép bình luận
        [HttpPost, Route("toggle_zone_allowcomment")]
        public HttpResponseMessage ToggleZoneAllowComment(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().ToggleZoneAllowComment(id, username);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region NewsExtension
        [HttpPost, Route("list_newsextension_by_ids")]
        public HttpResponseMessage ListNewsExtensionByIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("ListId")??string.Empty;
                var type = Utility.ConvertToInt(body.Get("Type"), 0);                
                if (string.IsNullOrEmpty(ids))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListId not empty"));
                }

                var data = new NewsServices().ListNewsExtensionByIds(ids, type);

                return this.ToJson(WcfActionResponse<List<NewsExtensionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region //Gửi thẩm định
        [HttpPost, Route("send_waiting_expert")]
        public HttpResponseMessage SendExpertInNewsExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                if (expertId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Expert not empty."));
                }
                var listNewsId = body.Get("ListNewsId") ?? string.Empty;
                if (string.IsNullOrEmpty(listNewsId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListNewsId not empty."));
                }
                var note = body.Get("Note") ?? "";               

                //v1 =>NewsExtension
                //var result = new NewsServices().SendExpertInNewsExtension(expertId, listNewsId);

                //v2 =>NewsExpert
                var result = new NewsServices().SendNewsInExpert(accountName, expertId, listNewsId, note);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("remind_waiting_expert")]
        public HttpResponseMessage ReSendExpertInNewsExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                if (expertId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Expert not empty."));
                }
                var expireDate = Utility.ConvertToInt(body.Get("ExpireDate"),24);                
                var note = body.Get("Note") ?? "";
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));

                //v1 =>NewsExtension
                //var result = new NewsServices().ReSendExpertInNewsExtension(expertId, expireDate);

                //v2 =>NewsExpert
                var result = new NewsServices().ReSendNewsInExpert(accountName, expertId, expireDate, note, newsId);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("recover_waiting_expert")]
        public HttpResponseMessage RecoverExpertInNewsExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var account = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var listNewsId = body.Get("ListNewsId") ?? string.Empty;
                if (string.IsNullOrEmpty(listNewsId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListNewsId not empty."));
                }
                var note = body.Get("Note") ?? "";
                               
                var result = new NewsServices().RecoverNewsInExpert(account, listNewsId, note);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("confirm_expert_by_newsid")]
        public HttpResponseMessage ConfirmExpertInNewsExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));                
                if (newsId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("NewsId not empty."));
                }
                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                if (expertId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ExpertId not empty."));
                }

                //v1 =>NewsExtension               
                //var result = new NewsServices().ConfirmExpertInNewsExtension(newsId, accountName);

                //v2 =>NewsExpert
                var result = new NewsServices().ConfirmNewsInExpert(newsId, expertId, accountName);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("return_expert_by_newsid")]
        public HttpResponseMessage ReturnExpertInNewsExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var note = body.Get("Note") ?? "";
                if (newsId < 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("News not empty."));
                }
                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                if (expertId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Expert not empty."));
                }

                //v1 =>NewsExtension
                //var result = new NewsServices().ReturnExpertInNewsExtension(newsId, note?.Trim());

                //v2 =>NewsExpert
                var result = new NewsServices().ReturnNewsInExpert(accountName, newsId, expertId, note?.Trim());

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        [HttpPost, Route("list_news_by_expert_id")]
        public HttpResponseMessage ListNewsByExpertId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"), 0);
                var expertStatus = Utility.ConvertToInt(body.Get("ExpertStatus"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };

                var from = body.Get("FromDate") ?? "";
                var fromDate = Utility.ParseDateTime(from, formats);                
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                //v1 =>NewsExtension
                //var data = new NewsServices().ListNewsByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                //v2 =>NewsExpert
                var data = new NewsServices().ListNewsInExpertByExpertId(expertId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_not_by_expert")]
        public HttpResponseMessage ListNewsNotByExpert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var account = string.Empty;
                var topicId = Utility.ConvertToInt(body.Get("TopicId"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                if (status == (int)NewsStatus.ReceivedForPublish)
                {
                    account = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                }
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };

                var from = body.Get("FromDate") ?? "";
                var fromDate = Utility.ParseDateTime(from, formats);
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                //v1 =>NewsExtension
                //var data = new NewsServices().ListNewsNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);
                
                //v2 =>NewsExpert
                var data = new NewsServices().ListNewsInExpertNotByExpert(account, topicId, zoneId, status, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_news_all_by_expert")]
        public HttpResponseMessage ListNewsAllByExpert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"), 0);
                var topicId = Utility.ConvertToInt(body.Get("TopicId"), 0);
                var expertStatus = Utility.ConvertToInt(body.Get("ExpertStatus"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var formats = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };

                var from = body.Get("FromDate") ?? "";
                var fromDate = Utility.ParseDateTime(from, formats);
                var to = body.Get("ToDate") ?? "";
                if (to.Contains(":"))
                {
                    to = body.Get("ToDate");
                }
                else
                {
                    to = string.IsNullOrEmpty(to) ? "" : to + " 23:59:59";
                }
                var toDate = Utility.ParseDateTime(to, formats);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRow = 0;
                //v1 =>NewsExtension
                //var data = new NewsServices().ListNewsAllByExpert(expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                //v2 =>NewsExpert
                var data = new NewsServices().ListNewsInExpertAllByExpert(expertId, topicId, expertStatus, zoneId, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_newsexpert_by_ids")]
        public HttpResponseMessage ListNewsExpertByIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("ListId") ?? string.Empty;                
                if (string.IsNullOrEmpty(ids))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListId not empty"));
                }
                if (ids.Split(',').Count()>50)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Max ListId is 50 item"));
                }

                var data = new NewsServices().ListNewsExpertByIds(ids);

                return this.ToJson(WcfActionResponse<List<NewsInExpertEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_expert_by_newsid")]
        public HttpResponseMessage ExpertUpdateBodyVesion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                if (newsId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("NewsId not empty."));
                }
                var expertName = body.Get("ExpertName");
                if (string.IsNullOrWhiteSpace(expertName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Expert name not empty."));
                }
                var bodyContent = body.Get("Body");
                if (string.IsNullOrWhiteSpace(bodyContent))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Body not empty."));
                }

                var expertNewsEntity = new ExpertNewsEntity
                {
                    NewsId = newsId,
                    Body = bodyContent,
                    ExpertName = Utility.UnicodeToKoDauNotSpace(expertName),
                    AccountName= accountName
                };
                                
                var result = new NewsServices().Expert_UpdateBody(expertNewsEntity);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("confirm_newsversion_of_expert")]
        public HttpResponseMessage ConfirmExpertUpdateBody(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                if (newsId <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("NewsId not empty."));
                }
                var expertName = body.Get("ExpertName");
                if (string.IsNullOrWhiteSpace(expertName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Expert name not empty."));
                }
                var bodyContent = body.Get("Body");
                if (string.IsNullOrWhiteSpace(bodyContent))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Body not empty."));
                }

                var expertNewsEntity = new ExpertNewsEntity
                {
                    NewsId = newsId,
                    Body = bodyContent,
                    ExpertName = Utility.UnicodeToKoDauNotSpace(expertName),
                    AccountName= accountName
                };

                var result = new NewsServices().Confirm_UpdateBody(expertNewsEntity);

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region NewsAuthor
        [HttpPost, Route("news_author_insert")]
        public HttpResponseMessage NewsAuthorInsert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var refAuthorId = 0;
                byte authorType = (int)AuthorType.Default;
                var accountName = body.Get("account_name");
                var authorTitle = body.Get("author_title");
                var description = body.Get("description");
                var authorid = Utility.ConvertToInt(body.Get("author_id"));
                var authorName = body.Get("author_name");
                var avatar = body.Get("avatar");
                var roleId = Utility.ConvertToInt(body.Get("role_id"));
                if (string.IsNullOrEmpty(accountName))
                {
                    authorType = (int)AuthorType.UserDefine;
                    accountName = "";
                }

                var newsAuthorEntity = new NewsAuthorEntity
                {
                    AuthorName = authorName,
                    AuthorType = authorType,
                    OriginalName = Utility.UnicodeToKoDauAndGach(authorName),
                    UnsignName = Utility.UnicodeToKoDau(authorName),
                    UserData = accountName,
                    Avatar = avatar,
                    NewsRoyaltiesRoleId = roleId,
                    AuthorTitle = authorTitle,
                    Description = description
                };

                var result = new NewsServices().NewsAuthorInsert(newsAuthorEntity, ref refAuthorId);
                result.Data = refAuthorId.ToString();
                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_author_update")]
        public HttpResponseMessage NewsAuthorUpdate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                byte authorType = (int)AuthorType.Default;
                var accountName = body.Get("account_name");
                var authorTitle = body.Get("author_title");
                var description = body.Get("description");
                var authorid = Utility.ConvertToInt(body.Get("author_id"));
                var authorName = body.Get("author_name") ?? string.Empty;
                var avatar = body.Get("avatar");
                var roleId = Utility.ConvertToInt(body.Get("role_id"));
                if (string.IsNullOrEmpty(accountName))
                {
                    authorType = (int)AuthorType.UserDefine;
                    accountName = "";
                }

                var newsAuthorEntity = new NewsAuthorEntity
                {
                    Id = authorid,
                    AuthorName = authorName,
                    AuthorType = authorType,
                    OriginalName = Utility.UnicodeToKoDauAndGach(authorName),
                    UnsignName = Utility.UnicodeToKoDau(authorName),
                    UserData = accountName,
                    Avatar = avatar,
                    NewsRoyaltiesRoleId = roleId,
                    AuthorTitle = authorTitle,
                    Description = description
                };

                var result = new NewsServices().NewsAuthorUpdate(newsAuthorEntity);
                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_author_delete")]
        public HttpResponseMessage NewsAuthorDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var authorid = Utility.ConvertToInt(body.Get("author_id"));

                var result = new NewsServices().NewsAuthorDelete(authorid);
                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_author_get_by_id")]
        public HttpResponseMessage NewsAuthorGetById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var authorid = Utility.ConvertToInt(body.Get("author_id"));

                var data = new NewsServices().NewsAuthorGetById(authorid);
                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_author_get_by_user")]
        public HttpResponseMessage ListNewsAuthorByUser(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = body.Get("account_name");

                var data = new NewsServices().ListNewsAuthorByUser(username);
                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_insert_by_author")]
        public HttpResponseMessage NewsByAuthorInsert(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsid = Utility.ConvertToLong(body.Get("news_id"));
                var authorid = Utility.ConvertToInt(body.Get("author_id"));
                var authorName = body.Get("author_name");
                var note = body.Get("note");
                var newsByAuthorEntity = new NewsByAuthorEntity
                {
                    NewsId = newsid,
                    AuthorId = authorid,
                    AuthorName = authorName,
                    Note = note
                };

                var result = new NewsServices().NewsByAuthorInsert(newsByAuthorEntity);
                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("news_author_search")]
        public HttpResponseMessage ListNewsAuthorSearch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");

                var data = new NewsServices().ListNewsAuthorSearch(keyword);
                return this.ToJson<WcfActionResponse<dynamic>>(
                        WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count(), NewsAuthor = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_news_author_in_news_id")]
        public HttpResponseMessage ListNewsAuthorInNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsid = Utility.ConvertToLong(body.Get("news_id"));

                var data = new NewsServices().ListNewsAuthorInNews(newsid);
                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region NewsConfig
        [HttpPost, Route("set_news_config")]
        public HttpResponseMessage SetNewsConfigValue(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configName = body.Get("ConfigName");
                if (string.IsNullOrEmpty(configName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("invalid request"));
                }
                var configValue = body.Get("ConfigValue") ?? string.Empty;
                var configValueType = Utility.ConvertToInt(body.Get("ConfigValueType"),0);
                var configLabel = body.Get("ConfigLabel")??string.Empty;
                var configInitValue = body.Get("ConfigInitValue") ?? string.Empty;
                var configGroupKey = body.Get("ConfigGroupKey") ?? "FrontEndConfig";
                var configGroup = body.Get("ConfigGroup") ?? "FrontEndConfig";

                var obj = new NewsConfigEntity {
                    ConfigName= configName,
                    ConfigGroupKey= configGroupKey,
                    ConfigGroup= configGroup,
                    ConfigValue=configValue,
                    ConfigInitValue=configInitValue,
                    ConfigLabel=configLabel,
                    ConfigValueType=configValueType,
                    LastModifiedDate=DateTime.Now                
                };

                var result = new NewsServices().SetNewsConfigValue(obj);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        [HttpPost, Route("get_news_config_by_config_name")]
        public HttpResponseMessage GetNewsConfigByConfigName(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configName = body.Get("ConfigName");

                var data = new NewsServices().GetNewsConfigByConfigName(configName);
                return this.ToJson(WcfActionResponse<NewsConfigEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        [HttpPost, Route("get_news_config_by_group_key")]
        public HttpResponseMessage GetListConfigByGroupKey(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configGroupKey = body.Get("ConfigGroupKey");

                var data = new NewsServices().GetListConfigByGroupKey(configGroupKey);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count(), NewsConfigs = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_news_config_all")]
        public HttpResponseMessage GetListConfigByAll(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"),20);

                var totalRow = 0;
                var data = new NewsServices().GetListConfigByAll(pageIndex, pageSize, ref totalRow);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, NewsConfigs = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("delete_news_config_by_config_name")]
        public HttpResponseMessage DeleteNewsConfigByConfigName(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configName = body.Get("ConfigName");
                if (string.IsNullOrEmpty(configName))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("invalid request"));
                }
                
                var result = new NewsServices().DeleteNewsConfigByConfigName(configName);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_news_config_by_type_initvalue")]
        public HttpResponseMessage GetNewsConfigByTypeInitValue(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configValueType = Utility.ConvertToInt(body.Get("ConfigValueType"),0);
                var configInitValue = body.Get("ConfigInitValue")??string.Empty;

                var data = new NewsServices().GetNewsConfigByTypeInitValue(configValueType, configInitValue);
                return this.ToJson(WcfActionResponse<NewsConfigEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("set_news_config_value_for_static_html_template")]
        public HttpResponseMessage SetValueForStaticHtmlTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configName = body.Get("config_name");
                if (string.IsNullOrEmpty(configName))
                {
                    return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
                }
                var configLabel = body.Get("config_label");
                var configValue = body.Get("config_value");
                var configType = (EnumStaticHtmlTemplateType)Utility.ConvertToInt(body.Get("config_type"));

                var result = new NewsServices().SetValueForStaticHtmlTemplate(configName, configLabel, configValue, configType);
                return this.ToJson<WcfActionResponse>(result);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        [HttpPost, Route("get_news_config_value_for_static_html_template")]
        public HttpResponseMessage GetListConfigForStaticHtmlTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var configGroupKey = body.Get("config_group_key");
                var configType = (EnumStaticHtmlTemplateType)Utility.ConvertToInt(body.Get("config_type"));

                var data = new NewsServices().GetListConfigForStaticHtmlTemplate(configGroupKey, configType);
                return this.ToJson<WcfActionResponse<dynamic>>(
                       WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count(), NewsConfig = data }, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region NewsHistory

        [HttpPost, Route("get_news_history_by_news_id")]
        public HttpResponseMessage GetNewsHistoryByNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsid = Utility.ConvertToLong(body.Get("news_id"));
                var from = Utility.ConvertToDateTimeByFormat(body.Get("from"),"dd/MM/yyyy");
                var to = Utility.ConvertToDateTimeByFormat(body.Get("to"), "dd/MM/yyyy");

                var data = new NewsServices().GetNewsHistoryByNewsId(newsid,from,to);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_history_swap_newsposition")]
        public HttpResponseMessage SearchLogs(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 50;
                var sourceId = body.Get("sourceId")??string.Empty;
                var totalRows = 0;
                var type = EnumActivityType.News;
                var data = new NewsServices().SearchLogs(pageIndex, pageSize, 0, (int)NewsActionType.SwapNewsPosition, (int)type, sourceId, DateTime.MinValue, DateTime.MaxValue, ref totalRows);
                return this.ToJson(WcfActionResponse<List<ActivityEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region NewsInUsing

        private static IDictionary<long, string> DictsNewsInUsing = new Dictionary<long, string>();

        [HttpPost, Route("update_news_in_using")]
        public HttpResponseMessage UpdateNewsInUsing(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var action = body.Get("Action");
                var id = body.Get("NewsId");
                if (string.IsNullOrEmpty(id))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamNewsIdNotExit]));
                }
                var newsId = Utility.ConvertToLong(id);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                
                var data = WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.AcctionIsNotSupport]);
                switch (action)
                {
                    case "unlock":
                        UnlockedNewsInUsing(newsId, accountName);                        
                        data = WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "Unlock success");
                        break;
                    case "allow_unlock":
                        UnlockedNewsInUsing(newsId, accountName, true);
                        data = WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "Unlock and Add lock current account success.");
                        break;
                    case "lock":
                        var currentAccount = string.Empty;
                        var isUsing = IsNewsInUsing(accountName, newsId, ref currentAccount);
                        if (isUsing)
                        {
                            data = WcfActionResponse.CreateSuccessResponse(isUsing.ToString(), string.Format("Tài khoản {0} đang sửa bài viết này.",currentAccount));
                            data.Success = false;
                        }
                        else
                        {
                            AddNewsInUsing(newsId, accountName);
                            data = WcfActionResponse.CreateSuccessResponse(newsId.ToString(), "Lock success.");
                        }
                        break;
                    case "get_all":
                        data= WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(BoCached.Base.News.NewsDalFactory.GetAllNewsInUsing()), "DATA");
                        break;
                    case "reset":
                        data = WcfActionResponse.CreateSuccessResponse(NewtonJson.Serialize(BoCached.Base.News.NewsDalFactory.ResetNewsInUsing(newsId)), "DATA");
                        break;
                }
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }        
        private bool IsNewsInUsing(string accountName, long newsId, ref string currentAccount)
        {            
            if (newsId <= 0) return false;

            //v1
            //string accountNameInUsing;
            //if (DictsNewsInUsing.TryGetValue(newsId, out accountNameInUsing))
            //{
            //    var result= !string.IsNullOrEmpty(accountNameInUsing) && accountNameInUsing.ToLower() != accountName.ToLower();

            //    Logger.WriteLog(Logger.LogType.Trace, string.Format("After check used lock NewsID {0}:{1}:{2}:{3}:{4}", newsId, accountNameInUsing, accountName, result, NewtonJson.Serialize(DictsNewsInUsing)));

            //    return result;
            //}
            //Logger.WriteLog(Logger.LogType.Trace, string.Format("After check used lock NewsID {0}:{1}:{2}:{3}:{4}", newsId, accountNameInUsing, accountName, false, NewtonJson.Serialize(DictsNewsInUsing)));

            //v2
            var obj = BoCached.Base.News.NewsDalFactory.GetNewsInUsingById(newsId);
            if (obj != null)
            {
                var result= !string.IsNullOrEmpty(obj.AccountName) && obj.AccountName.ToLower() != accountName.ToLower() && Utility.DateTimeToSpan10(DateTime.Now)<=obj.ExprireTime;
                if (result)
                {
                    currentAccount = obj.AccountName.ToLower();
                }
                else
                {
                    BoCached.Base.News.NewsDalFactory.ResetNewsInUsing(newsId);
                }
                return result;
            }

            return false;
        }

        private void UnlockedNewsInUsing(long newsId, string accountName, bool allowUnLock = false)
        {
            //v1
            //if (DictsNewsInUsing.ContainsKey(newsId))
            //{
            //    DictsNewsInUsing.Remove(newsId);
            //    Logger.WriteLog(Logger.LogType.Trace,string.Format("After remove lock NewsID {0}:{1}", newsId,NewtonJson.Serialize(DictsNewsInUsing)));
            //}

            //v2
            BoCached.Base.News.NewsDalFactory.RemoveNewsInUsing(newsId, accountName, allowUnLock);
        }

        private void AddNewsInUsing(long newsId, string accountName)
        {
            //v1  
            //if (DictsNewsInUsing.ContainsKey(newsId))
            //{
            //    DictsNewsInUsing[newsId] = accountName;
            //}
            //else
            //{
            //    DictsNewsInUsing.Add(newsId, accountName);
            //}

            //v2
            var expireTime = Utility.DateTimeToSpan10(DateTime.Now.AddMinutes(60));
            BoCached.Base.News.NewsDalFactory.AddNewsInUsing(new BoCached.Entity.News.NewsInUsingEntity { Id=newsId,AccountName=accountName, ExprireTime= expireTime });
            
            //Logger.WriteLog(Logger.LogType.Trace, string.Format("After Add lock NewsID {0}:{1}:{2}", newsId, accountName, NewtonJson.Serialize(DictsNewsInUsing)));
        }
        #endregion

        #region newsVersion
        [HttpPost, Route("get_news_version_by_id")]
        public HttpResponseMessage GetNewsVersionById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().GetNewsVersionById(id);

                return this.ToJson(WcfActionResponse<NewsVersionNodeJsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_list_version")]
        public HttpResponseMessage GetListVersion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsid = Utility.ConvertToLong(body.Get("news_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
                if (newsid > 0)
                    data = new NewsServices().GetNewsVersionByNewsId(newsid);
                else
                    data = new NewsServices().GetNewsVersionByNewsIdForUser(newsid, username);

                return this.ToJson(WcfActionResponse<List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("check_news_version")]
        public HttpResponseMessage CheckNewsVersion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsid = Utility.ConvertToLong(body.Get("news_id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new List<NewsVersionWithSimpleFieldsNodejsForParseJsonEntity>();
                if (newsid > 0)
                    data = new NewsServices().GetNewsVersionByNewsId(newsid);
                else
                    data = new NewsServices().GetNewsVersionByNewsIdForUser(newsid, username);

                if (data.Count > 0)
                {
                    var dataEdit = data.Where(n => n.IsEditing).ToList();
                    if (dataEdit.Count > 0)
                    {
                        var newsversion = new NewsServices().GetNewsVersionById(dataEdit[0].Id);
                        return this.ToJson(WcfActionResponse<NewsVersionNodeJsEntity>.CreateSuccessResponse(newsversion, "Success"));
                    }
                    else {
                        return this.ToJson(WcfActionResponse<NewsVersionNodeJsEntity>.CreateSuccessResponse(null, "Success"));
                    }
                }
                else {
                    return this.ToJson(WcfActionResponse<NewsVersionNodeJsEntity>.CreateSuccessResponse(null, "Success"));
                }
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("auto_save_news_version")]
        public HttpResponseMessage AutoSaveNewsVersion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsId = Utility.ConvertToLong(body.Get("Id"));
                var title = body.Get("Title") ?? string.Empty;
                var subTitle = body.Get("SubTitle") ?? string.Empty;
                var sapo = body.Get("Sapo") ?? string.Empty;
                var initSapo = body.Get("InitSapo") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var bodyContent = body.Get("Body") ?? string.Empty;
                //bodyContent = HttpContext.Current.Server.UrlDecode(bodyContent);
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var avatar = body.Get("Avatar") ?? string.Empty;
                var avatar2 = body.Get("Avatar2") ?? string.Empty;
                var avatar3 = body.Get("Avatar3") ?? string.Empty;
                var avatar4 = body.Get("Avatar4") ?? string.Empty;
                var avatar5 = body.Get("Avatar5") ?? string.Empty;
                var avatarCustom = body.Get("AvatarCustom") ?? string.Empty;
                var avatarDesc = body.Get("AvatarDesc") ?? string.Empty;
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"));
                var displayInSlide = Utility.ConvertToInt(body.Get("DisplayInslide"));
                var originalId = Utility.ConvertToInt(body.Get("OriginalId"));
                var newsRelationIdList = body.Get("NewsRelationIdList") ?? string.Empty;
                var zoneRelationIdList = body.Get("ListZoneId") ?? string.Empty;
                var tagIdList = body.Get("TagList") ?? string.Empty;
                var tagIdListForPrimary = body.Get("SubTitleList") ?? string.Empty;
                var author = body.Get("Author") ?? string.Empty;
                var listAuthorByNews = body.Get("ListAuthorByNews") ?? string.Empty;
                var source = body.Get("Source") ?? string.Empty;
                var isForcus = Utility.ConvertToBoolean(body.Get("IsFocus"));
                var isPr = Utility.ConvertToBoolean(body.Get("IsPr"));
                var isOnHome = Utility.ConvertToBoolean(body.Get("IsOnHome"));
                var newsNote = body.Get("Note") ?? string.Empty;
                var price = Utility.ConvertToDecimal(body.Get("Price"));
                var sourceUrl = body.Get("SourceUrl") ?? string.Empty;
                var newsType = Utility.ConvertToInt(body.Get("NewsType"));
                var type = Utility.ConvertToInt(body.Get("Type"));
                if (newsType == -1)
                {
                    newsType = (int)NewsType.Normal;
                }
                if (type < 0)
                {
                    type = (int)NewsType.Normal;
                }
                var noteRoyalties = body.Get("NoteRoyalties") ?? string.Empty;
                var newsCategory = Utility.ConvertToInt(body.Get("NewsCategory"));
                var threadId = Utility.ConvertToInt(body.Get("ThreadId"));
                var newsChildOrder = Utility.ConvertToInt(body.Get("NewsChildOrder"));
                var templateName = body.Get("TemplateName");
                var templateConfig = body.Get("TemplateConfig") ?? "[]";
                var publishedContent = body.Get("PublishedContent") ?? string.Empty;
                //publishedContent = HttpContext.Current.Server.UrlDecode(publishedContent);
                var interviewId = Utility.ConvertToInt(body.Get("InterviewId"));
                var rollingNewsId = Utility.ConvertToInt(body.Get("RollingNewsId"));
                var magazineContentId = Utility.ConvertToInt(body.Get("MagazineContentId"));
                var locationType = Utility.ConvertToInt(body.Get("LocationType"));
                var priority = Utility.ConvertToInt(body.Get("Priority"));
                var TagSubTitleId = Utility.ConvertToInt(body.Get("TagSubTitleId"));
                var newsUrl = body.Get("Url");
                var status = Utility.ConvertToInt(body.Get("Status"), 0);

                var news = new NewsEntity
                {
                    Id = newsId,
                    Author = author,
                    Avatar = avatar,
                    Avatar2 = avatar2,
                    Avatar3 = avatar3,
                    Avatar4 = avatar4,
                    Avatar5 = avatar5,
                    AvatarDesc = avatarDesc,
                    DistributionDate = distributionDate,
                    Sapo = sapo,
                    Title = title,
                    SubTitle = subTitle,
                    Body = bodyContent,
                    NewsRelation = newsRelationIdList,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = accountName,
                    LastModifiedDate = DateTime.Now,
                    WordCount = Utility.CountWords(bodyContent),
                    ViewCount = 0,
                    Source = source,
                    IsFocus = isForcus,
                    ThreadId = threadId,
                    Note = newsNote,
                    Price = price,
                    AvatarCustom = avatarCustom,
                    DisplayInSlide = displayInSlide,
                    DisplayPosition = displayPosition,
                    DisplayStyle = displayStyle,
                    OriginalId = originalId,
                    Type = type,
                    NewsType = newsType,
                    IsOnHome = isOnHome,
                    NoteRoyalties = noteRoyalties,
                    NewsCategory = newsCategory,
                    InitSapo = initSapo,
                    TemplateName = templateName,
                    TemplateConfig = templateConfig,
                    IsPr = isPr,
                    InterviewId = interviewId,
                    RollingNewsId = rollingNewsId,
                    LocationType = locationType,
                    Priority = priority,
                    SourceURL = sourceUrl,
                    TagSubTitleId = TagSubTitleId,
                    Url = newsUrl,
                    Status = status
                };
                var data = new NewsServices().AutoSaveNewsVersion(news, zoneId, zoneRelationIdList, tagIdList, tagIdListForPrimary, newsRelationIdList, accountName);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("remove_news_version_by_news_id")]
        public HttpResponseMessage RemoveNewsVersionByNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var newsId = Utility.ConvertToLong(body.Get("news_id"));

                var data = new NewsServices().RemoveNewsVersionByNewsId(newsId, accountName);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("remove_news_version_by_id")]
        public HttpResponseMessage RemoveNewsVersionById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var versionId = body.Get("version_id")??string.Empty;
                if (string.IsNullOrWhiteSpace(versionId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tim thấy versionId."));
                }

                var data = new NewsServices().RemoveNewsVersionById(versionId, accountName);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }


        #endregion

        #region Magazine

        [HttpPost, Route("get_list_magazine_by_type")]
        public HttpResponseMessage GetListMagazineByType(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                string keyword = body.Get("keyword") ??string.Empty;
                var magazineType = Utility.ConvertToInt(body.Get("type"));
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"),20);

                var totalRows = 0;
                var data = new NewsServices().GetListMagazineByType(keyword, 27, magazineType, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<SearchNewsMagazineEntity>.CreateSuccessResponse(new SearchNewsMagazineEntity { TotalRow = totalRows, News = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        [HttpPost, Route("import_magazine")]
        public HttpResponseMessage ImportMagazine(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var url = body.Get("url");
                if (string.IsNullOrEmpty(url))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamUrlNotExit]));
                }

                var data = new MagazineContentServices().ImportMagazine(url);

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(data, "Success."));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("import_minimagazine")]
        public HttpResponseMessage ImportMiniMagazine(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var url = body.Get("url");
                if (string.IsNullOrEmpty(url))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.ParamUrlNotExit]));
                }

                var data = new MagazineContentServices().ImportMiniMagazine(url);

                return this.ToJson(WcfActionResponse<object>.CreateSuccessResponse(data, "Success."));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("upload_file_zip_magazine")]
        public HttpResponseMessage UploadFileZip()
        {
            try
            {
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var myFile = httpRequest.Files[0];
                    var checkCommentTag = Utility.ConvertToInt(httpRequest.Params["check_comment_tag"],0);//GetQueryString.GetPost("check_comment_tag", 0);
                    var isMobile = Utility.ConvertToInt(httpRequest.Params["is_mobile"], 0);//GetQueryString.GetPost("is_mobile", 0);
                    var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                    var fileName = myFile.FileName.Replace(".zip", "") + DateTime.Now.ToString("yyyyMMddhhmmss") + ".zip";
                    var folder = HttpContext.Current.Server.MapPath("/Data/Magazine/");
                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }

                    var tempFile = HttpContext.Current.Server.MapPath("/Data/Magazine/" + fileName);

                    myFile.SaveAs(tempFile);

                    var zipFileName = HttpContext.Current.Server.MapPath("~/Data/Magazine/" + fileName);
                    var targetDir = HttpContext.Current.Server.MapPath("~/Data/Magazine/" + fileName.Replace(".zip", ""));
                    FastZip fastZip = new FastZip();
                    fastZip.ExtractZip(zipFileName, targetDir, null);
                    fileName = fileName.Replace(".zip", "");
                    var filePaths = Directory.GetFiles(targetDir, "*.*", SearchOption.AllDirectories);

                    if (!File.Exists(targetDir + "\\index.html"))
                    {
                        return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.NotFileHtmlRootFolder]));
                    }
                    // Html
                    var html = File.ReadAllText(targetDir + "\\index.html");
                    var htmlDoc = new HtmlDocument
                    {
                        OptionFixNestedTags = true,
                        OptionCheckSyntax = false
                    };
                    htmlDoc.LoadHtml(html);

                    #region check thẻ magazine-object có tồn tại hay không
                    if (checkCommentTag == 1)
                    {
                        if (isMobile == 0)
                        {
                            var searchResult = htmlDoc.DocumentNode.SelectNodes("//div[@magazine-object='comment-box']");
                            if (searchResult == null || searchResult.Count < 1)
                            {
                                return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.BodyHtmlNotClassMagazineObject]));
                            }
                        }
                    }
                    #endregion

                    #region Format lại thẻ của ảnh, js, css
                    ReFormatLink(ref htmlDoc, CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + "Magazine/" + fileName + "/", "");
                    File.WriteAllText(targetDir + "\\index.html", htmlDoc.DocumentNode.OuterHtml);
                    #endregion

                    //var link = "";
                    var errorList = new List<string>();

                    //upload file zip and unzip
                    if (CmsChannelConfiguration.GetAppSettingInInt32("IS_UPLOAD_ZIP_MAGAZINE")==1)
                    {                       
                        try
                        {       
                            //zip folder da check pass                     
                            fastZip.CreateZip(zipFileName, targetDir, true, null);

                            var fileNameUpload = Path.GetFileName(zipFileName);
                            var directoryName = Path.GetDirectoryName(zipFileName) + "/" + fileNameUpload.Replace(".zip", "");
                            var path = directoryName.Replace(HttpContext.Current.Server.MapPath("~/Data/"), "").Replace("\\", "/") + "/";
                            var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(username, path, fileNameUpload, false);
                            var policy = policyData[1];
                            var signature = policyData[2];
                            var result = false;
                            try
                            {
                                result = Services.ExternalServices.FileStorageServices.UploadFileZip(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(zipFileName));
                                if (result == true)
                                {
                                    var fileStorage = path + fileNameUpload;
                                    //unzip
                                    var status_key = Services.ExternalServices.FileStorageServices.UnArchiveFileZip(fileStorage);
                                    //status_key = "unarchive:ftptest/boi-canh-apec-web20180515111728.zip";
                                    errorList.Add("status_key: "+ status_key);

                                    if (!string.IsNullOrEmpty(status_key))
                                    {
                                        int j = 0;
                                        while (true)
                                        {
                                            var res = Services.ExternalServices.FileStorageServices.GetStatusUnArchiveFileZip(status_key);
                                            errorList.Add("res."+ j + " " + res);
                                            if (!string.IsNullOrEmpty(res))
                                            {
                                                var oStatus = NewtonJson.Deserialize<dynamic>(res);
                                                if (oStatus != null && oStatus.status == "Finished")
                                                {                                                    
                                                    break;
                                                }
                                            }
                                            System.Threading.Thread.Sleep(3000);
                                            j++;
                                            if (j == 10)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    errorList.Add("Lỗi upload file zip: "+fileNameUpload);
                                    return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("Lỗi upload file zip: " + fileNameUpload));
                                }
                            }
                            catch (Exception ex)
                            {
                                errorList.Add(ex.Message);
                                return this.ToJson(WcfActionResponse<string>.CreateErrorResponse("UnArchiveFileZip" + ex.Message));
                            }
                        }
                        catch (Exception ex)
                        {
                            errorList.Add(ex.Message);
                            return this.ToJson(WcfActionResponse<string>.CreateErrorResponse(ex.Message));
                        }
                    }
                    else {
                        foreach (var filePath in filePaths)
                        {
                            var fileNameUpload = Path.GetFileName(filePath);
                            var directoryName = Path.GetDirectoryName(filePath);
                            var path = directoryName.Replace(HttpContext.Current.Server.MapPath("~/Data/"), "").Replace("\\", "/") + "/";
                            //link += path + ", ";
                            var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(username, path, fileNameUpload, false);
                            var policy = policyData[1];
                            var signature = policyData[2];
                            var result = false;
                            try
                            {
                                result = Services.ExternalServices.FileStorageServices.UploadFile(policy, signature, fileNameUpload, "data:application/x-rar-compressed;base64," + Serialize(filePath));
                                if (result == false)
                                {
                                    errorList.Add(fileNameUpload);
                                }
                            }
                            catch (Exception ex)
                            {
                                errorList.Add(fileNameUpload+"=> "+ex.Message);
                            }
                        }
                    }

                    //var Prefix = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_PREFIX");
                    //var IsIncludingPrefix = CmsChannelConfiguration.GetAppSettingInBoolean("FILE_MANAGER_INCLUDING_PREFIX");

                    var pr = "Magazine/";
                    //if (IsIncludingPrefix && !string.IsNullOrEmpty(Prefix))
                    //    pr = Prefix + "/" + pr;

                    var linkIndex = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD") + pr + fileName + "/index.html";
                    var content = string.Empty;

                    if (errorList.Count > 0)
                    {
                        content = NewtonJson.Serialize(errorList);
                    }

                    var title = htmlDoc.DocumentNode.SelectSingleNode("//title").InnerText;
                    var cover = string.Empty;
                    var elms4 = htmlDoc.DocumentNode.SelectNodes("//*[@class='cover-top']");
                    if (elms4 != null)
                    {
                        for (int i = 0; i < elms4.Count; i++)
                        {
                            cover = elms4[i].Attributes["src"].Value;
                        }
                    }
                    else
                    {
                        elms4 = htmlDoc.DocumentNode.SelectNodes("//div[@class='sp-cover']//img");
                        if (elms4 != null && elms4.Count > 0)
                        {
                            cover = elms4.ElementAt(0).Attributes["src"].Value;
                        }
                        else
                        {
                            elms4 = htmlDoc.DocumentNode.SelectNodes("//*[@class='bg-cover']");
                            if (elms4 != null && elms4.Count > 0)
                            {
                                cover = elms4.ElementAt(0).Attributes["src"].Value;
                            }
                        }
                    }

                    File.Delete(zipFileName);
                    clearFolder(targetDir);
                    Directory.Delete(targetDir);

                    return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { LinkIndex = linkIndex, Total = 1, Title = title, Cover = cover, Content = content }, "Success."));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Select file upload!"));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "UploadFileZip => " + ex.ToString());
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private static HtmlDocument ReFormatLink(ref HtmlDocument htmlDoc, string domainUrl, string currentModuleUrl)
        {
            var jsNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "script");
            //HtmlNode head = htmlDoc.DocumentNode.SelectSingleNode("/html/head");
            // Thêm base tag để hiển thị ảnh nếu ảnh không bắt đầu với http
            //HtmlNode baseTag = htmlDoc.CreateElement("base");
            //head.PrependChild(baseTag);
            //baseTag.SetAttributeValue("href", domainUrl);
            //Nếu script không bắt đầu bằng http thì cộng thêm vào
            foreach (var js in jsNode)
            {
                if (js.Attributes["src"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(js.Attributes["src"].Value))
                    {
                        js.SetAttributeValue("src", domainUrl + js.Attributes["src"].Value);
                    }
                }
            }
            //Nếu style không bắt đầu bằng http thì phải cộng thêm domain vào
            var cssNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "style" || n.Name == "link");
            foreach (var css in cssNode)
            {
                if (css.Attributes["href"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(css.Attributes["href"].Value))
                    {
                        css.SetAttributeValue("href", domainUrl + css.Attributes["href"].Value);
                    }
                }
            }


            // replace style background-image
            //foreach (HtmlNode bodyNode in htmlDoc.DocumentNode.Descendants())
            //{
            //    if (bodyNode.Attributes.Contains("style") && bodyNode.Attributes["style"].Value.Contains(""))
            //    {
            //        string styleValue = bodyNode.Attributes["style"].Value;
            //        if (styleValue.Contains("background-image"))
            //        {
            //            string style = bodyNode.Attributes["style"].Value;
            //            string oldImg = Regex.Match(style, @"(?<=\().+?(?=\))").Value;
            //            string oldStyle = bodyNode.Attributes["style"].Value;
            //            string newStyle = oldStyle.Replace(oldImg, domainUrl + oldImg);

            //            bodyNode.Attributes.Remove("style");
            //            bodyNode.Attributes.Add("style", newStyle);
            //        }
            //    }
            //}

            //Nếu Image không bắt đầu bằng http thì phải cộng thêm domain vào
            var imgNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "img");
            foreach (var img in imgNode)
            {
                if (img.Attributes["src"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(img.Attributes["src"].Value))
                    {
                        img.SetAttributeValue("src", domainUrl + img.Attributes["src"].Value);
                    }
                }
            }
            //Check thẻ <a> có href là link img
            var aHrefImgNode = htmlDoc.DocumentNode.Descendants().Where(n => n.Name == "a");
            foreach (var aHrefImg in aHrefImgNode)
            {
                if (aHrefImg.Attributes["href"] != null)
                {
                    if (!IMSBrowserHelper.IsValidLink(aHrefImg.Attributes["href"].Value))
                    {
                        aHrefImg.SetAttributeValue("href", domainUrl + aHrefImg.Attributes["href"].Value);
                    }
                }
            }
            return htmlDoc;
        }
        private void clearFolder(string FolderName)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(FolderName);

                foreach (FileInfo fi in dir.GetFiles())
                {
                    fi.Delete();
                }

                foreach (DirectoryInfo di in dir.GetDirectories())
                {
                    clearFolder(di.FullName);
                    di.Delete();
                }
            }catch(Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "clearFolder(" + FolderName + ") => " + ex.ToString());
            }            
        }
        private static string Serialize(string fileName)
        {
            using (FileStream reader = new FileStream(fileName, FileMode.Open))
            {
                byte[] buffer = new byte[reader.Length];
                reader.Read(buffer, 0, (int)reader.Length);
                return Convert.ToBase64String(buffer);
            }
        }
        [HttpPost, Route("get_file_magazine")]
        public HttpResponseMessage GetFileMagazine(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var folder = "";
                var url = body.Get("url") ?? string.Empty;
                if (url != "")
                {
                    url = url.Replace("http://", "").Replace("https://", "");
                    var urls = url.Split('/');
                    for (var i = 1; i < urls.Count() - 1; i++)
                    {
                        folder += urls[i] + "/";
                    }
                    if (folder == "")
                        return this.ToJson(WcfActionResponse<List<MagazineTreeFolder>>.CreateSuccessResponse(new List<MagazineTreeFolder>(), "Success"));

                    treeData = new List<MagazineTreeFolder>();
                    count = 0;
                    total = 0;

                    var list = GetMagazineFromDir(folder);
                    return this.ToJson(WcfActionResponse<List<MagazineTreeFolder>>.CreateSuccessResponse(list, "Success"));
                }
                else {
                    return this.ToJson(WcfActionResponse<List<MagazineTreeFolder>>.CreateSuccessResponse(new List<MagazineTreeFolder>(), "Success"));
                }
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        private int count = 0, total = 0;
        private List<MagazineTreeFolder> treeData;

        private List<MagazineTreeFolder> GetMagazineFromDir(string folder)
        {
            try
            {
                var Prefix = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_PREFIX") + "/";
                var IsIncludingPrefix =  CmsChannelConfiguration.GetAppSettingInBoolean("FILE_MANAGER_INCLUDING_PREFIX");
                if (IsIncludingPrefix)
                {
                    folder = Prefix + folder;
                }
                string url = string.Format("{0}ls?secret_key={1}&dirname={2}", CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPSERVER"),
                           CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_SECRET_KEY"), folder);

                var client = new WebClient();
                var content = client.DownloadString(url);
                var data = NewtonJson.Deserialize<MagazineJsonResponse>(content);

                count++;

                if (data.files != null && data.files.Count > 0)
                {
                    foreach (var item in data.files)
                    {
                        var file = folder + item;
                        if (IsIncludingPrefix)
                        {
                            file = file.Replace(Prefix, "");
                        }
                        treeData.Add(new MagazineTreeFolder()
                        {
                            Id = file,
                            Parent = folder.Replace(Prefix, ""),
                            Text = item,
                            Type = "file"
                        });
                    }
                }

                if (data.directories != null && data.directories.Count > 0)
                {
                    total += data.directories.Count;
                    foreach (var item in data.directories)
                    {
                        var newFolder = folder + item + '/';
                        var parent = (folder != "/") ? folder : "#";
                        if (IsIncludingPrefix)
                        {
                            newFolder=newFolder.Replace(Prefix, "");
                            parent=parent.Replace(Prefix, "");
                        }
                        treeData.Add(new MagazineTreeFolder()
                        {
                            Id = newFolder,
                            Parent = parent,
                            Text = item,
                            Type = "folder"
                        });

                        GetMagazineFromDir(newFolder.Replace(Prefix, ""));
                    }
                }
                else {
                    if (count > total)
                    {
                        return treeData;
                    }
                }

                return treeData;
            }
            catch (Exception ex)
            {
                return new List<MagazineTreeFolder>();
            }
        }

        [HttpPost, Route("update_file")]
        public HttpResponseMessage UpdateFile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fileUrl = body.Get("fileUrl") ?? string.Empty;
                if (string.IsNullOrEmpty(fileUrl))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse("File url not found!"));
                }
                var fileData = body.Get("fileData") ?? string.Empty;
                if (string.IsNullOrEmpty(fileData))
                {
                    this.ToJson(WcfActionResponse.CreateErrorResponse("Body file not found!"));
                }
                var env = body.Get("env") ?? "pc";
                var domain = CmsChannelConfiguration.GetAppSetting("FILE_MANAGER_HTTPDOWNLOAD");
                var dataDirectory = getDirectoryFile(domain, fileUrl);

                var filename = dataDirectory.filename;
                var ext = dataDirectory.ext;
                var directory = dataDirectory.directory;                

                var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload("", directory, filename, true);
                var policy = policyData[1];
                var signature = policyData[2];

                var isBase64 = false;
                if (ext == "jpg" || ext == "png")
                {
                    isBase64 = true;
                }

                var result = Services.ExternalServices.FileStorageServices.UpdateFile(policy, signature, fileUrl, fileData, isBase64);
                if (result)
                {
                    fileUrl = domain.Replace("http://", "").Replace("https://", "") + fileUrl.Replace(domain, "");
                    Services.ExternalServices.FileStorageServices.removeCachedFile(fileUrl);

                    return this.ToJson(WcfActionResponse.CreateSuccessResponse());
                }
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error update file data."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private dynamic getDirectoryFile(string domain, string fileUrl)
        {
            var filename = string.Empty;
            var ext = string.Empty;
            var directory = string.Empty;

            fileUrl = fileUrl.Split('?')[0];

            var st = new Stack<string>(fileUrl.Split(new string[] { "/" }, StringSplitOptions.None));
            filename = st.Pop();
            st = new Stack<string>(fileUrl.Split(new string[] { "." }, StringSplitOptions.None));
            ext = st.Pop();
            directory = fileUrl.Replace(domain, "");
            directory = directory.Replace('/' + filename, "");
            return new
            {
                directory,
                filename,
                ext
            };
        }

        #endregion

        #region For LiveMatch

        //livematch
        [HttpPost, Route("search_livematch")]
        public HttpResponseMessage SearchLiveMatch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("pageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("pageSize"), 20);

                var totalRow = 0;
                var data = new LiveMatchServices().SearchLiveMatch(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<SearchLivematchResponse>.CreateSuccessResponse(new SearchLivematchResponse { TotalRow = totalRow, Livematch = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("list_livematch_by_ids")]
        public HttpResponseMessage ListLiveMatchByIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("ids");
                if (string.IsNullOrEmpty(ids))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse(ErrorMapping.Current[ErrorMapping.ErrorCodes.IdsNotEmpty]));
                }

                var data = new LiveMatchServices().ListLiveMatchByIds(ids);

                return this.ToJson(WcfActionResponse<List<NodeJs_LiveMatchEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("insert_livematch")]
        public HttpResponseMessage InsertLiveMatch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var title = body.Get("title") ?? string.Empty;
                var team_a = body.Get("team_a") ?? string.Empty;
                var team_b = body.Get("team_b") ?? string.Empty;
                var logo_team_a = body.Get("logo_team_a") ?? string.Empty;
                var logo_team_b = body.Get("logo_team_b") ?? string.Empty;
                var score_of_team_a = Utility.ConvertToInt(body.Get("score_of_team_a"));
                var score_of_team_b = Utility.ConvertToInt(body.Get("score_of_team_b"));
                var stadium = body.Get("stadium") ?? string.Empty;
                var referee = body.Get("referee") ?? string.Empty;
                var link_livetv_1 = body.Get("link_livetv_1") ?? string.Empty;
                var link_livetv_2 = body.Get("link_livetv_2") ?? string.Empty;
                var link_livetv_3 = body.Get("link_livetv_3") ?? string.Empty;
                var link_livetv_4 = body.Get("link_livetv_4") ?? string.Empty;
                var link_livetv_5 = body.Get("link_livetv_5") ?? string.Empty;
                var show_on_livetv = body.Get("show_on_livetv") ?? "0";
                var time_of_match_begin = Utility.ConvertToDateTimeByFormat(body.Get("time_of_match_begin"), "yyyy-MM-dd HH:mm:ss");

                var livematch = new NodeJs_LiveMatchEntity
                {
                    Title = title,
                    TeamA = team_a,
                    TeamB = team_b,
                    LogoTeamA = logo_team_a,
                    LogoTeamB = logo_team_b,
                    ScoreOfTeamA = score_of_team_a,
                    ScoreOfTeamB = score_of_team_b,
                    Stadium = stadium,
                    Referee = referee,
                    LinkLiveTV1 = link_livetv_1,
                    LinkLiveTV2 = link_livetv_2,
                    LinkLiveTV3 = link_livetv_3,
                    LinkLiveTV4 = link_livetv_4,
                    LinkLiveTV5 = link_livetv_5,
                    ShowOnLiveTV = show_on_livetv,
                    TimeOfMatchBegin = time_of_match_begin,
                    CreatedBy = accountName
                };

                var liveMatchId = "";
                var data = new LiveMatchServices().InsertLiveMatchV2(livematch, ref liveMatchId);
                data.Data = liveMatchId;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_by_id")]
        public HttpResponseMessage GetLiveMatchById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchById(id);

                return this.ToJson(WcfActionResponse<NodeJs_LiveMatchEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_livematch")]
        public HttpResponseMessage UpdateLiveMatch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentLivematch = new LiveMatchServices().GetLiveMatchById(id);
                if (currentLivematch == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("livematch Not Found!"));

                var title = body.Get("title");
                if (title != null)
                    currentLivematch.Title = title;

                var team_a = body.Get("team_a");
                if (team_a != null)
                    currentLivematch.TeamA = team_a;

                var team_b = body.Get("team_b");
                if (team_b != null)
                    currentLivematch.TeamB = team_b;

                var logo_team_a = body.Get("logo_team_a");
                if (logo_team_a != null)
                    currentLivematch.LogoTeamA = logo_team_a;

                var logo_team_b = body.Get("logo_team_b");
                if (logo_team_b != null)
                    currentLivematch.LogoTeamB = logo_team_b;

                var score_of_team_a = body.Get("score_of_team_a");
                if (score_of_team_a != null)
                    currentLivematch.ScoreOfTeamA = Utility.ConvertToInt(score_of_team_a);

                var score_of_team_b = body.Get("score_of_team_b");
                if (score_of_team_b != null)
                    currentLivematch.ScoreOfTeamB = Utility.ConvertToInt(score_of_team_b);

                var stadium = body.Get("stadium");
                if (stadium != null)
                    currentLivematch.Stadium = stadium;

                var referee = body.Get("referee");
                if (referee != null)
                    currentLivematch.Referee = referee;

                var begin_first_half = body.Get("begin_first_half");
                if (begin_first_half != null)
                    currentLivematch.BeginFirstHalf = begin_first_half;

                var finish_first_half = body.Get("finish_first_half");
                if (finish_first_half != null)
                    currentLivematch.FinishFirstHalf = finish_first_half;

                var begin_second_half = body.Get("begin_second_half");
                if (begin_second_half != null)
                    currentLivematch.BeginSecondHalf = begin_second_half;

                var finish_second_half = body.Get("finish_second_half");
                if (finish_second_half != null)
                    currentLivematch.FinishSecondHalf = finish_second_half;

                var begin_first_extra_time = body.Get("begin_first_extra_time");
                if (begin_first_extra_time != null)
                    currentLivematch.BeginFirstExtraTime = begin_first_extra_time;

                var finish_first_extra_time = body.Get("finish_first_extra_time");
                if (finish_first_extra_time != null)
                    currentLivematch.FinishFirstExtraTime = finish_first_extra_time;

                var begin_second_extra_time = body.Get("begin_second_extra_time");
                if (begin_second_extra_time != null)
                    currentLivematch.BeginSecondExtraTime = begin_second_extra_time;

                var finish_second_extra_time = body.Get("finish_second_extra_time");
                if (finish_second_extra_time != null)
                    currentLivematch.FinishSecondExtraTime = finish_second_extra_time;

                var is_penalty = body.Get("is_penalty");
                if (is_penalty != null)
                    currentLivematch.IsPenalty = is_penalty;

                var match_finished = body.Get("match_finished");
                if (match_finished != null)
                    currentLivematch.MatchFinished = match_finished;

                var time_of_match_begin = body.Get("time_of_match_begin");
                if (time_of_match_begin != null)
                    currentLivematch.TimeOfMatchBegin = Utility.ConvertToDateTimeByFormat(time_of_match_begin, "yyyy-MM-dd HH:mm:ss");

                var time_of_finish_first_half = body.Get("time_of_finish_first_half");
                if (time_of_finish_first_half != null)
                    currentLivematch.TimeOfFinishFirstHalf = Utility.ConvertToDateTimeByFormat(time_of_finish_first_half, "yyyy-MM-dd HH:mm:ss");

                var time_of_begin_second_half = body.Get("time_of_begin_second_half");
                if (time_of_begin_second_half != null)
                    currentLivematch.TimeOfBeginSecondHalf = Utility.ConvertToDateTimeByFormat(time_of_begin_second_half, "yyyy-MM-dd HH:mm:ss");

                var time_of_finish_second_half = body.Get("time_of_finish_second_half");
                if (time_of_finish_second_half != null)
                    currentLivematch.TimeOfFinishSecondHalf = Utility.ConvertToDateTimeByFormat(time_of_finish_second_half, "yyyy-MM-dd HH:mm:ss");

                var time_of_begin_first_extra_time = body.Get("time_of_begin_first_extra_time");
                if (time_of_begin_first_extra_time != null)
                    currentLivematch.TimeOfBeginFirstExtraTime = Utility.ConvertToDateTimeByFormat(time_of_begin_first_extra_time, "yyyy-MM-dd HH:mm:ss");

                var time_of_finish_first_extra_time = body.Get("time_of_finish_first_extra_time");
                if (time_of_finish_first_extra_time != null)
                    currentLivematch.TimeOfFinishFirstExtraTime = Utility.ConvertToDateTimeByFormat(time_of_finish_first_extra_time, "yyyy-MM-dd HH:mm:ss");

                var time_of_begin_second_extra_time = body.Get("time_of_begin_second_extra_time");
                if (time_of_begin_second_extra_time != null)
                    currentLivematch.TimeOfBeginSecondExtraTime = Utility.ConvertToDateTimeByFormat(time_of_begin_second_extra_time, "yyyy-MM-dd HH:mm:ss");

                var time_of_finish_second_extra_time = body.Get("time_of_finish_second_extra_time");
                if (time_of_finish_second_extra_time != null)
                    currentLivematch.TimeOfFinishSecondExtraTime = Utility.ConvertToDateTimeByFormat(time_of_finish_second_extra_time, "yyyy-MM-dd HH:mm:ss");

                var time_of_match_finished = body.Get("time_of_match_finished");
                if (time_of_match_finished != null)
                    currentLivematch.TimeOfMatchFinished = Utility.ConvertToDateTimeByFormat(time_of_match_finished, "yyyy-MM-dd HH:mm:ss");

                var status = body.Get("status");
                if (status != null)
                    currentLivematch.Status = Utility.ConvertToInt(status);

                var link_livetv_1 = body.Get("link_livetv_1");
                if (link_livetv_1 != null)
                    currentLivematch.LinkLiveTV1 = link_livetv_1;

                var link_livetv_2 = body.Get("link_livetv_2");
                if (link_livetv_2 != null)
                    currentLivematch.LinkLiveTV2 = link_livetv_2;

                var link_livetv_3 = body.Get("link_livetv_3");
                if (link_livetv_3 != null)
                    currentLivematch.LinkLiveTV3 = link_livetv_3;

                var link_livetv_4 = body.Get("link_livetv_4");
                if (link_livetv_4 != null)
                    currentLivematch.LinkLiveTV4 = link_livetv_4;

                var link_livetv_5 = body.Get("link_livetv_5");
                if (link_livetv_5 != null)
                    currentLivematch.LinkLiveTV5 = link_livetv_5;

                var show_on_livetv = body.Get("show_on_livetv");
                if (show_on_livetv != null)
                    currentLivematch.ShowOnLiveTV = show_on_livetv;

                var infomation_before_match = body.Get("infomation_before_match");
                if (infomation_before_match != null)
                    currentLivematch.InfomationBeforeMatch = infomation_before_match;

                var match_status = body.Get("match_status");
                if (match_status != null)
                    currentLivematch.MatchStatus = match_status;

                var data = new LiveMatchServices().UpdateLiveMatch(currentLivematch);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        //event
        [HttpPost, Route("insert_livematch_event")]
        public HttpResponseMessage InsertLiveMatchEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var livematch_id = body.Get("livematch_id");
                var in_minute = Utility.ConvertToInt(body.Get("in_minute"));
                var event_type = Utility.ConvertToInt(body.Get("event_type"));
                var is_team_a = body.Get("is_team_a") ?? "0";
                var player_name = body.Get("player_name") ?? string.Empty;
                var player_name_out = body.Get("player_name_out") ?? string.Empty;

                var livematchEvent = new NodeJs_LiveMatchEventEntity
                {
                    LiveMatchId = livematch_id,
                    InMinute = in_minute,
                    EventType = event_type,
                    IsTeamA = is_team_a,
                    PlayerName = player_name,
                    PlayerNameOut = player_name_out,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new LiveMatchServices().InsertLiveMatchEvent(livematchEvent, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_event_by_id")]
        public HttpResponseMessage GetLiveMatchEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchEventById(id);

                return this.ToJson(WcfActionResponse<NodeJs_LiveMatchEventEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_event_by_livematch_id")]
        public HttpResponseMessage GetLiveMatchEventByLivematchId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var livematch_id = body.Get("livematch_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchEventByLiveMatchId(livematch_id);

                return this.ToJson(WcfActionResponse<List<NodeJs_LiveMatchEventEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_livematch_event")]
        public HttpResponseMessage UpdateLiveMatchEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentLivematchEvent = new LiveMatchServices().GetLiveMatchEventById(id);
                if (currentLivematchEvent == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("event Not Found!"));

                var in_minute = body.Get("in_minute");
                if (in_minute != null)
                    currentLivematchEvent.InMinute = Utility.ConvertToInt(in_minute);

                var event_type = body.Get("event_type");
                if (event_type != null)
                    currentLivematchEvent.EventType = Utility.ConvertToInt(event_type);

                var is_team_a = body.Get("is_team_a");
                if (is_team_a != null)
                    currentLivematchEvent.IsTeamA = is_team_a;

                var player_name = body.Get("player_name");
                if (player_name != null)
                    currentLivematchEvent.PlayerName = player_name;

                var player_name_out = body.Get("player_name_out");
                if (player_name_out != null)
                    currentLivematchEvent.PlayerNameOut = player_name_out;


                var data = new LiveMatchServices().UpdateLiveMatchEvent(currentLivematchEvent);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_livematch_event")]
        public HttpResponseMessage DeleteLiveMatchEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().DeleteLiveMatchEvent(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        //penalty
        [HttpPost, Route("insert_livematch_penalty")]
        public HttpResponseMessage InsertLiveMatchPenalty(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var livematch_id = body.Get("livematch_id");
                var turn = Utility.ConvertToInt(body.Get("turn"));
                var player_name = body.Get("player_name") ?? string.Empty;
                var is_team_a = body.Get("is_team_a") ?? "0";
                var is_goal = body.Get("is_goal") ?? "0";

                var livematchPenalty = new NodeJs_LiveMatchPenaltyEntity
                {
                    LiveMatchId = livematch_id,
                    Turn = turn,
                    PlayerName = player_name,
                    IsTeamA = is_team_a,
                    IsGoal = is_goal,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new LiveMatchServices().InsertLiveMatchPenalty(livematchPenalty, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_penalty_by_id")]
        public HttpResponseMessage GetLiveMatchPenaltyById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchPenaltyById(id);

                return this.ToJson(WcfActionResponse<NodeJs_LiveMatchPenaltyEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_penalty_by_livematch_id")]
        public HttpResponseMessage GetLiveMatchPenaltyByLivematchId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var livematch_id = body.Get("livematch_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchPenaltyByLiveMatchId(livematch_id);

                return this.ToJson(WcfActionResponse<List<NodeJs_LiveMatchPenaltyEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_livematch_penalty")]
        public HttpResponseMessage UpdateLiveMatchPenalty(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentLivematchPenalty = new LiveMatchServices().GetLiveMatchPenaltyById(id);
                if (currentLivematchPenalty == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("penalty Not Found!"));

                var turn = body.Get("turn");
                if (turn != null)
                    currentLivematchPenalty.Turn = Utility.ConvertToInt(turn);

                var is_team_a = body.Get("is_team_a");
                if (is_team_a != null)
                    currentLivematchPenalty.IsTeamA = is_team_a;

                var player_name = body.Get("player_name");
                if (player_name != null)
                    currentLivematchPenalty.PlayerName = player_name;

                var is_goal = body.Get("is_goal");
                if (is_goal != null)
                    currentLivematchPenalty.IsGoal = is_goal;


                var data = new LiveMatchServices().UpdateLiveMatchPenalty(currentLivematchPenalty);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_livematch_penalty")]
        public HttpResponseMessage DeleteLiveMatchPenalty(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().DeleteLiveMatchPenalty(id);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        //timeline
        [HttpPost, Route("insert_livematch_timeline")]
        public HttpResponseMessage InsertLiveMatchTimeline(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var livematch_id = body.Get("livematch_id");
                var in_minute = Utility.ConvertToInt(body.Get("in_minute"));
                var event_type = Utility.ConvertToInt(body.Get("event_type"));
                var description = body.Get("description") ?? string.Empty;
                var video_key = body.Get("video_key") ?? string.Empty;
                var video_avatar = body.Get("video_avatar") ?? string.Empty;
                var images = body.Get("images") ?? string.Empty;
                var extra_time = Utility.ConvertToInt(body.Get("extra_time"));
                var status = Utility.ConvertToInt(body.Get("status"));

                var livematchTimeline = new NodeJs_LiveMatchTimeLinesEntity
                {
                    LiveMatchId = livematch_id,
                    InMinute = in_minute,
                    EventType = event_type,
                    Description = description,
                    VideoKey = video_key,
                    VideoAvatar = video_avatar,
                    Images = images,
                    ExtraTime = extra_time,
                    Status = status,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new LiveMatchServices().InsertLiveMatchTimeline(livematchTimeline, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_livematch_timeline_by_id")]
        public HttpResponseMessage GetLiveMatchTimelineById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchTimelineById(id);

                return this.ToJson(WcfActionResponse<NodeJs_LiveMatchTimeLinesEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_livematch_timeline_by_livematch_id")]
        public HttpResponseMessage GetLiveMatchTimelineByLivematchId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var livematch_id = body.Get("livematch_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().GetLiveMatchTimelineByLiveMatchId(livematch_id);

                return this.ToJson(WcfActionResponse<List<NodeJs_LiveMatchTimeLinesEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_livematch_timeline")]
        public HttpResponseMessage UpdateLiveMatchTimeline(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentLivematchTimeline = new LiveMatchServices().GetLiveMatchTimelineById(id);
                if (currentLivematchTimeline == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("timeline Not Found!"));

                var in_minute = body.Get("in_minute");
                if (in_minute != null)
                    currentLivematchTimeline.InMinute = Utility.ConvertToInt(in_minute);

                var event_type = body.Get("event_type");
                if (event_type != null)
                    currentLivematchTimeline.EventType = Utility.ConvertToInt(event_type);

                var description = body.Get("description");
                if (description != null)
                    currentLivematchTimeline.Description = description;

                var video_key = body.Get("video_key");
                if (video_key != null)
                    currentLivematchTimeline.VideoKey = video_key;

                var video_avatar = body.Get("video_avatar");
                if (video_avatar != null)
                    currentLivematchTimeline.VideoAvatar = video_avatar;

                var images = body.Get("images");
                if (images != null)
                    currentLivematchTimeline.Images = images;

                var extra_time = body.Get("extra_time");
                if (extra_time != null)
                    currentLivematchTimeline.ExtraTime = Utility.ConvertToInt(extra_time);

                var status = body.Get("status");
                if (status != null)
                    currentLivematchTimeline.Status = Utility.ConvertToInt(status);


                var data = new LiveMatchServices().UpdateLiveMatchTimeline(currentLivematchTimeline);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_livematch_timeline")]
        public HttpResponseMessage DeleteLiveMatchTimeline(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new LiveMatchServices().DeleteLiveMatchTimeline(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_news_title_for_livematch")]
        public HttpResponseMessage GetNewstitleForLivematch(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var livematch_id = body.Get("livematch_id") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var newsExt = new NewsServices().GetNewsExtensionByTypeAndVaue(EnumNewsExtensionType.LiveMatchId, livematch_id);
                var news = new NewsForValidateEntity();

                if (newsExt != null && newsExt.Count() > 0)
                {
                    var newsId = Utility.ConvertToLong(newsExt.FirstOrDefault().NewsId);
                    if (newsId > 0)
                    {
                        news = new NewsServices().GetNewsForValidateById(newsId);
                    }
                }

                return this.ToJson(WcfActionResponse<NewsForValidateEntity>.CreateSuccessResponse(news, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse(ex.Message));
            }
        }

        #endregion

        #region For Bigstory

        //bigstory
        [HttpPost, Route("search_bigstory")]
        public HttpResponseMessage SearchBigstory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from_date"), "yyyy-MM-dd HH:mm:ss");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to_date"), "yyyy-MM-dd HH:mm:ss");
                var pageIndex = !string.IsNullOrEmpty(body.Get("page_index")) ? Utility.ConvertToInt(body.Get("page_index")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("page_size")) ? Utility.ConvertToInt(body.Get("page_size")) : 20;

                var totalRow = 0;
                var data = new BigStoryServices().BigStory_Search(fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<List<NodeJs_BigStoryEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_bigstory_by_id")]
        public HttpResponseMessage GetBigStoryById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new BigStoryServices().BigStory_GetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_BigStoryEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("insert_bigstory")]
        public HttpResponseMessage InsertBigstory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var title = body.Get("title");
                var is_focus = Utility.ConvertToInt(body.Get("is_focus"));
                var avatar = body.Get("avatar") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("status"));
                var description = body.Get("description") ?? string.Empty;

                var bigstory = new NodeJs_BigStoryEntity
                {
                    Title = title,
                    IsFocus = is_focus,
                    Avatar = avatar,
                    Status = status,
                    Description = description,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new BigStoryServices().BigStory_Insert(bigstory, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_bigstory")]
        public HttpResponseMessage UpdateBigstory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentBigstory = new BigStoryServices().BigStory_GetById(id);
                if (currentBigstory == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("bigstory Not Found!"));

                var title = body.Get("title");
                if (title != null)
                    currentBigstory.Title = title;

                var is_focus = body.Get("is_focus");
                if (is_focus != null)
                    currentBigstory.IsFocus = Utility.ConvertToInt(is_focus);

                var description = body.Get("description");
                if (description != null)
                    currentBigstory.Description = description;

                var avatar = body.Get("avatar");
                if (avatar != null)
                    currentBigstory.Avatar = avatar;

                var status = body.Get("status");
                if (status != null)
                    currentBigstory.Status = Utility.ConvertToInt(status);

                currentBigstory.ModifiedBy = accountName;


                var data = new BigStoryServices().BigStory_Update(currentBigstory);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }


        //bigstory item
        [HttpPost, Route("get_bigstory_item_by_id")]
        public HttpResponseMessage GetBigStoryItemById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new BigStoryServices().BigStoryItem_GetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_BigStoryItemEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_bigstory_item_by_bigstory_id")]
        public HttpResponseMessage GetBigStoryItemByBigstoryId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("bigstory_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new BigStoryServices().BigStoryItem_GetByBigStoryId(id);

                return this.ToJson(WcfActionResponse<List<NodeJs_BigStoryItemEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("insert_bigstory_item")]
        public HttpResponseMessage InsertBigstoryItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var bigstory_id = body.Get("bigstory_id");
                if (string.IsNullOrEmpty(bigstory_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing bigstory_id!"));

                var title = body.Get("title") ?? string.Empty;
                var url = body.Get("url") ?? string.Empty;
                var body_data = body.Get("body") ?? string.Empty;
                var is_highlight = Utility.ConvertToInt(body.Get("is_highlight"));
                var status = Utility.ConvertToInt(body.Get("status"));
                var tab_id = body.Get("tab_id") ?? string.Empty;
                var is_focus = Utility.ConvertToInt(body.Get("is_focus"));
                var is_hot = Utility.ConvertToInt(body.Get("is_hot"));
                var avatar = body.Get("avatar") ?? string.Empty;
                var published_date = Utility.ConvertToDateTimeByFormat(body.Get("published_date"), "yyyy-MM-dd HH:mm:ss");

                var bigstoryItem = new NodeJs_BigStoryItemEntity
                {
                    BigstoryId = bigstory_id,
                    Title = title,
                    Url = url,
                    Body = body_data,
                    IsHighlight = is_highlight,
                    Status = status,
                    CreatedBy = accountName,
                    TabId = tab_id,
                    IsFocus = is_focus,
                    IsHot = is_hot,
                    Avatar = avatar,
                    PublishedDate = published_date
                };

                var id = "";
                var data = new BigStoryServices().BigStoryItem_Insert(bigstoryItem, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_bigstory_item_by_id")]
        public HttpResponseMessage DeleteBigStoryItemById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new BigStoryServices().BigStoryItem_Delete(id);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_bigstory_item")]
        public HttpResponseMessage UpdateBigstoryItem(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentBigstoryItem = new BigStoryServices().BigStoryItem_GetById(id);
                if (currentBigstoryItem == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("bigstory_item Not Found!"));

                var title = body.Get("title");
                if (title != null)
                    currentBigstoryItem.Title = title;

                var url = body.Get("url");
                if (url != null)
                    currentBigstoryItem.Url = url;

                var body_data = body.Get("body");
                if (body_data != null)
                    currentBigstoryItem.Body = body_data;

                var is_highlight = body.Get("is_highlight");
                if (is_highlight != null)
                    currentBigstoryItem.IsHighlight = Utility.ConvertToInt(is_highlight);

                var status = body.Get("status");
                if (status != null)
                    currentBigstoryItem.Status = Utility.ConvertToInt(status);

                var tab_id = body.Get("tab_id");
                if (tab_id != null)
                    currentBigstoryItem.TabId = tab_id;

                var is_focus = body.Get("is_focus");
                if (is_focus != null)
                    currentBigstoryItem.IsFocus = Utility.ConvertToInt(is_focus);

                var is_hot = body.Get("is_hot");
                if (is_hot != null)
                    currentBigstoryItem.IsHot = Utility.ConvertToInt(is_hot);

                var avatar = body.Get("avatar");
                if (avatar != null)
                    currentBigstoryItem.Avatar = avatar;

                var published_date = body.Get("published_date");
                if (published_date != null)
                    currentBigstoryItem.PublishedDate = Utility.ConvertToDateTimeByFormat(published_date, "yyyy-MM-dd HH:mm:ss");


                var data = new BigStoryServices().BigStoryItem_Update(currentBigstoryItem);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("insert_bigstory_item_by_list")]
        public HttpResponseMessage InsertBigstoryItemByList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var bigstory_id = body.Get("bigstory_id");
                if (string.IsNullOrEmpty(bigstory_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing bigstory_id!"));

                var rawItems = GetQueryString.GetPost("bigstory_items", "");
                var items = JsonConvert.DeserializeObject<List<BigStoryItemMappingEntity>>(rawItems);
                var listItems = new List<BigStoryItemMappingEntity>();

                listItems = items.Select(bigStoryItem => new BigStoryItemMappingEntity
                {
                    BigStoryId = bigstory_id,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    Avatar = bigStoryItem.Avatar,
                    Body = bigStoryItem.Body,
                    IsFocus = bigStoryItem.IsFocus,
                    IsHighlight = bigStoryItem.IsHighlight,
                    IsHot = bigStoryItem.IsHot,
                    PublishedDate = bigStoryItem.PublishedDate,
                    Status = bigStoryItem.Status,
                    TabId = bigStoryItem.TabId,
                    Title = bigStoryItem.Title,
                    Url = bigStoryItem.Url,
                    Id = bigStoryItem.BigStoryId,
                    IsRemove = bigStoryItem.IsRemove
                }).ToList();

                var data = new BigStoryServices().BigStoryItem_InsertByList(listItems, bigstory_id);
                data.Data = bigstory_id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region For Rollingnews

        [HttpPost, Route("search_rollingnews")]
        public HttpResponseMessage SearchRollingNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("fromDate"), "yyyy-MM-dd HH:mm:ss");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("toDate"), "yyyy-MM-dd HH:mm:ss");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 20;

                var totalRow = 0;
                var data = new RollingNewsServices().SearchRollingNews(fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<List<NodeJs_RollingNewsEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_rollingnews_by_id")]
        public HttpResponseMessage GetRollingNewsById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().GetRollingNewsByRollingNewsId(id);

                return this.ToJson(WcfActionResponse<NodeJs_RollingNewsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("insert_rollingnews")]
        public HttpResponseMessage InsertRollingNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var title = body.Get("title");
                var show_time = Utility.ConvertToInt(body.Get("show_time"));
                var show_author = Utility.ConvertToInt(body.Get("show_author"));
                var start_date = Utility.ConvertToDateTimeByFormat(body.Get("start_date"), "yyyy-MM-dd HH:mm:ss");
                var status = Utility.ConvertToInt(body.Get("status"));
                var show_label = Utility.ConvertToInt(body.Get("show_label"));

                var rollingnews = new NodeJs_RollingNews
                {
                    Title = title,
                    ShowTime = show_time == 1 ? true : false,
                    ShowAuthor = show_author == 1 ? true : false,
                    StartDate = start_date,
                    Status = status,
                    IsShowRollingNewsLabel = show_label == 1 ? true : false,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new RollingNewsServices().InsertRollingNewsV2(rollingnews, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_rollingnews")]
        public HttpResponseMessage UpdateRollingNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var current = new RollingNewsServices().GetRollingNewsByRollingNewsId(id);
                if (current == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("rollingnews Not Found!"));
                var currentRollingnews = new NodeJs_RollingNews()
                {
                    Id = current.Id,
                    ShowTime = current.ShowTime == 1 ? true : false,
                    ShowAuthor = current.ShowAuthor == 1 ? true : false,
                    StartDate = current.StartDate,
                    LastModifiedBy = accountName,
                    Status = current.Status,
                    IsShowRollingNewsLabel = current.IsShowRollingNewsLabel == 1 ? true : false
                };


                var title = body.Get("title");
                if (title != null)
                    currentRollingnews.Title = title;

                var show_time = body.Get("show_time");
                if (show_time != null)
                    currentRollingnews.ShowTime = Utility.ConvertToInt(show_time) == 1 ? true : false;

                var show_author = body.Get("show_author");
                if (show_author != null)
                    currentRollingnews.ShowAuthor = Utility.ConvertToInt(show_author) == 1 ? true : false;

                var start_date = body.Get("start_date");
                if (start_date != null)
                    currentRollingnews.StartDate = Utility.ConvertToDateTimeByFormat(start_date, "yyyy-MM-dd HH:mm:ss");

                var status = body.Get("status");
                if (status != null)
                    currentRollingnews.Status = Utility.ConvertToInt(status);

                var show_label = body.Get("show_label");
                if (show_label != null)
                    currentRollingnews.IsShowRollingNewsLabel = Utility.ConvertToInt(show_label) == 1 ? true : false;

                var data = new RollingNewsServices().UpdateRollingNews(currentRollingnews);
                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        //rollingnews event
        [HttpPost, Route("get_rollingnews_event_by_id")]
        public HttpResponseMessage GetRollingNewsEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().GetRollingNewsEventByRollingNewsEventId(id);

                return this.ToJson(WcfActionResponse<NodeJs_RollingNewsEventEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_rollingnews_event_by_rollingnews_id")]
        public HttpResponseMessage GetRollingNewsEventByRollingnewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("rollingnews_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().GetRollingNewsEventByRollingNewsId(id);

                return this.ToJson(WcfActionResponse<List<NodeJs_RollingNewsEventEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("insert_rollingnews_event")]
        public HttpResponseMessage InsertRollingNewsEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var rollingnews_id = body.Get("rollingnews_id");
                if (string.IsNullOrEmpty(rollingnews_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing rollingnews_id!"));

                var event_type = Utility.ConvertToInt(body.Get("event_type"));
                var event_content = body.Get("event_content") ?? string.Empty;
                var event_note = body.Get("event_note") ?? string.Empty;
                var event_time = Utility.ConvertToDateTimeByFormat(body.Get("event_time"), "yyyy-MM-dd HH:mm:ss");
                var is_focus = Utility.ConvertToInt(body.Get("is_focus"));
                var published_date = Utility.ConvertToDateTimeByFormat(body.Get("published_date"), "yyyy-MM-dd HH:mm:ss");
                //var published_by = body.Get("published_by") ?? string.Empty;
                var status = Utility.ConvertToInt(body.Get("status"));


                var rollingnewsEvent = new NodeJs_RollingNewsEventEntity
                {
                    RollingNewsId = rollingnews_id,
                    EventType = event_type,
                    EventContent = event_content,
                    EventNote = event_note,
                    EventTime = event_time,
                    IsFocus = is_focus,
                    CreatedBy = accountName,
                    PublishedDate = published_date,
                    PublishedBy = accountName,
                    Status = status
                };

                var id = "";
                var data = new RollingNewsServices().InsertRollingNewsEvent(rollingnewsEvent, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_rollingnews_event")]
        public HttpResponseMessage UpdateRollingNewsEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentRollingNewsEvent = new RollingNewsServices().GetRollingNewsEventByRollingNewsEventId(id);
                if (currentRollingNewsEvent == null || currentRollingNewsEvent.Id == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("rollingnews_event Not Found!"));

                var rollingnews_id = body.Get("rollingnews_id");
                if (rollingnews_id != null)
                    currentRollingNewsEvent.RollingNewsId = rollingnews_id;

                var event_type = body.Get("event_type");
                if (event_type != null)
                    currentRollingNewsEvent.EventType = Utility.ConvertToInt(event_type);

                var event_content = body.Get("event_content");
                if (event_content != null)
                    currentRollingNewsEvent.EventContent = event_content;

                var event_note = body.Get("event_note");
                if (event_note != null)
                    currentRollingNewsEvent.EventNote = event_note;

                var event_time = body.Get("event_time");
                if (event_time != null)
                    currentRollingNewsEvent.EventTime = Utility.ConvertToDateTimeByFormat(event_time, "yyyy-MM-dd HH:mm:ss");

                var is_focus = body.Get("is_focus");
                if (is_focus != null)
                    currentRollingNewsEvent.IsFocus = Utility.ConvertToInt(is_focus);

                currentRollingNewsEvent.LastModifiedBy = accountName;

                var status = body.Get("status");
                if (status != null)
                    currentRollingNewsEvent.Status = Utility.ConvertToInt(status);

                var published_date = body.Get("published_date");
                if (published_date != null)
                {
                    currentRollingNewsEvent.PublishedDate = Utility.ConvertToDateTimeByFormat(published_date, "yyyy-MM-dd HH:mm:ss");
                    currentRollingNewsEvent.PublishedBy = accountName;
                }


                var data = new RollingNewsServices().UpdateRollingNewsEvent(currentRollingNewsEvent);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_rollingnews_event_by_id")]
        public HttpResponseMessage DeleteRollingNewsEventById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().DeleteRollingNewsEvent(id);

                return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete_rollingnews_by_rollingnews_id")]
        public HttpResponseMessage DeleteRollingNewsByRollingNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("rollingnews_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().DeleteRollingNewsEventByRollingNews(id);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_rollingnews_by_list_ids")]
        public HttpResponseMessage DeleteRollingNewsByListIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("ids");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().DeleteRollingNewsEventByListIds(ids);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("publish_rollingnews_event")]
        public HttpResponseMessage PublishRollingNewsEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var rollingnews_id = body.Get("rollingnews_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().PublishRollingNewsEvent(id, rollingnews_id, username);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        [HttpPost, Route("unpublish_rollingnews_event")]
        public HttpResponseMessage UnPublishRollingNewsEvent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                var rollingnews_id = body.Get("rollingnews_id");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new RollingNewsServices().UnpublishRollingNewsEvent(id, rollingnews_id, username);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }
        #endregion

        #region For Interview

        #region interview
        [HttpPost, Route("search_interview")]
        public HttpResponseMessage SearchInterview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("page_index")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("page_size")) : 20;

                var totalRow = 0;
                var data = new InterviewServices().SearchInterviewV3(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<List<NodeJs_InterviewEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("list_interview_by_ids")]
        public HttpResponseMessage ListInterviewByIds(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var ids = body.Get("ids");
                if (string.IsNullOrEmpty(ids))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ids không được để trống!"));
                }
                
                var data = new InterviewServices().ListInterviewByIds(ids);

                return this.ToJson(WcfActionResponse<List<NodeJs_InterviewEntity2>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_interview_by_id")]
        public HttpResponseMessage GetInterviewById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");                

                var data = new InterviewServices().InterviewV3GetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_InterviewEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_interview")]
        public HttpResponseMessage GetInterview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");                

                var data = new InterviewServices().InterviewV3GetInterview(id);

                return this.ToJson(WcfActionResponse<NodeJs_InterviewEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("insert_interview")]
        public HttpResponseMessage InsertInterview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var title = body.Get("title");
                var unsign_title = body.Get("unsign_title");
                var start_date = Utility.ConvertToDateTimeByFormat(body.Get("start_date"), "yyyy-MM-dd HH:mm:ss");
                var is_focus = Utility.ConvertToInt(body.Get("is_focus"));
                var is_actived = Utility.ConvertToInt(body.Get("is_actived"));
                var status = Utility.ConvertToInt(body.Get("status"));
                var published_date = Utility.ConvertToDateTimeByFormat(body.Get("published_date"), "yyyy-MM-dd HH:mm:ss");
                var published_by = string.IsNullOrEmpty(body.Get("published_date")) ? "" : accountName;

                var interview = new NodeJs_Interview
                {
                    Title = title,
                    UnsignTitle = unsign_title,
                    StartDate = start_date,
                    IsFocus = is_focus == 1 ? true : false,
                    IsActived = is_actived == 1 ? true : false,
                    Status = status,
                    CreatedBy = accountName,
                    PublisedDate = published_date,
                    PublishedBy = published_by
                };

                var id = "";
                var data = new InterviewServices().InterviewV3Insert(interview, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_interview")]
        public HttpResponseMessage UpdateInterview(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var current = new InterviewServices().InterviewV3GetById(id);
                if (current == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("interview Not Found!"));

                var currentInterview = new NodeJs_Interview()
                {
                    Id = current.Id,
                    Title = current.Title,
                    UnsignTitle = current.UnsignTitle,
                    StartDate = current.StartDate,
                    IsFocus = current.IsFocus == 1 ? true : false,
                    IsActived = current.IsActived == 1 ? true : false,
                    Status = current.Status,
                    PublisedDate = current.PublishedDate,
                    PublishedBy = current.PublishedBy,
                    CreatedBy = current.CreatedBy,
                    CreatedDate = current.CreatedDate
                };

                var title = body.Get("title");
                if (title != null)
                    currentInterview.Title = title;

                var unsign_title = body.Get("unsign_title");
                if (unsign_title != null)
                    currentInterview.UnsignTitle = unsign_title;

                var start_date = body.Get("start_date");
                if (start_date != null)
                    currentInterview.StartDate = Utility.ConvertToDateTimeByFormat(start_date, "yyyy-MM-dd HH:mm:ss");

                var is_focus = body.Get("is_focus");
                if (is_focus != null)
                    currentInterview.IsFocus = Utility.ConvertToInt(is_focus) == 1 ? true : false;

                var is_actived = body.Get("is_actived");
                if (is_actived != null)
                    currentInterview.IsActived = Utility.ConvertToInt(is_actived) == 1 ? true : false;

                var status = body.Get("status");
                if (status != null)
                    currentInterview.Status = Utility.ConvertToInt(status);

                var published_date = body.Get("published_date");
                if (published_date != null)
                {
                    currentInterview.PublisedDate = Utility.ConvertToDateTimeByFormat(published_date, "yyyy-MM-dd HH:mm:ss");
                    currentInterview.PublishedBy = accountName;
                }

                var data = new InterviewServices().InterviewV3Update(currentInterview);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region interview guest
        [HttpPost, Route("get_interview_guest_by_id")]
        public HttpResponseMessage GetInterviewGuestById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");                

                var data = new InterviewServices().InterviewV3GuestsGetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_InterviewGuestEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_interview_guest_by_interview_id")]
        public HttpResponseMessage GetInterviewGuestByInterviewId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var interview_id = body.Get("interview_id");
                var status = Utility.ConvertToInt(body.Get("status"), 1);                

                var data = new InterviewServices().InterviewV3GuestsGetByInterviewId(interview_id, status);

                return this.ToJson(WcfActionResponse<List<NodeJs_InterviewGuestEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("insert_interview_guest")]
        public HttpResponseMessage InsertInterviewGuest(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var interview_id = body.Get("interview_id");
                if (string.IsNullOrEmpty(interview_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing interview_id!"));

                var online_username = body.Get("online_username") ?? string.Empty;
                var fullname = body.Get("fullname") ?? string.Empty;
                var avatar = body.Get("avatar") ?? string.Empty;
                var role = Utility.ConvertToInt(body.Get("role"));
                var desc = body.Get("desc");
                var status = Utility.ConvertToInt(body.Get("status"));

                var interviewGuest = new NodeJs_InterviewGuestEntity
                {
                    InterviewId = interview_id,
                    OnlineUserName = online_username,
                    FullName = fullname,
                    Avatar = avatar,
                    Role = role,
                    Desc = desc,
                    Status = status,
                    CreatedBy = accountName
                };

                var id = "";
                var data = new InterviewServices().InterviewV3GuestsInsert(interviewGuest, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_interview_guest")]
        public HttpResponseMessage UpdateInterviewGuest(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentInterviewGuest = new InterviewServices().InterviewV3GuestsGetById(id);
                if (currentInterviewGuest == null || currentInterviewGuest.Id == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("interview_guest Not Found!"));

                var online_username = body.Get("online_username");
                if (online_username != null)
                    currentInterviewGuest.OnlineUserName = online_username;

                var fullname = body.Get("fullname");
                if (fullname != null)
                    currentInterviewGuest.FullName = fullname;

                var avatar = body.Get("avatar");
                if (avatar != null)
                    currentInterviewGuest.Avatar = avatar;

                var role = body.Get("role");
                if (role != null)
                    currentInterviewGuest.Role = Utility.ConvertToInt(role);

                var desc = body.Get("desc");
                if (desc != null)
                    currentInterviewGuest.Desc = desc;

                var status = body.Get("status");
                if (status != null)
                    currentInterviewGuest.Status = Utility.ConvertToInt(status);

                var data = new InterviewServices().InterviewV3GuestsUpdateInfo(currentInterviewGuest);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_interview_guest_by_id")]
        public HttpResponseMessage DeleteInterviewGuestById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                
                var data = new InterviewServices().InterviewV3GuestsDelete(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region interview question
        [HttpPost, Route("get_interview_question_by_interview_id")]
        public HttpResponseMessage GetInterviewQuestionByInterviewId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var interview_id = body.Get("interview_id");
                var status = Utility.ConvertToInt(body.Get("status"), 0);                

                var data = new InterviewServices().InterviewV3QuestionGetListByInterviewId(interview_id, (EnumNodeJsInterviewQuestionStatus)status);

                return this.ToJson(WcfActionResponse<List<NodeJs_InterviewQuestionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_interview_question_by_id")]
        public HttpResponseMessage GetInterviewQuestionById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                
                var data = new InterviewServices().InterviewV3QuestionGetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_InterviewQuestionEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("insert_interview_question")]
        public HttpResponseMessage InsertInterviewQuestion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var interview_id = body.Get("interview_id");
                if (string.IsNullOrEmpty(interview_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing interview_id!"));


                var priority = Utility.ConvertToInt(body.Get("priority"));
                var type = Utility.ConvertToInt(body.Get("type"));
                var status = Utility.ConvertToInt(body.Get("status"));
                var process_user = body.Get("process_user") ?? string.Empty;
                var content = body.Get("content") ?? string.Empty;
                var fullname = body.Get("fullname") ?? string.Empty;
                var sex = Utility.ConvertToInt(body.Get("sex"));
                var age = Utility.ConvertToInt(body.Get("age"));
                var email = body.Get("email") ?? string.Empty;
                var job = body.Get("job") ?? string.Empty;
                var address = body.Get("address") ?? string.Empty;
                var mobile = body.Get("mobile") ?? string.Empty;
                var received_date = Utility.ConvertToDateTimeByFormat(body.Get("received_date"), "yyyy-MM-dd HH:mm:ss");
                var received_by = string.IsNullOrEmpty(body.Get("received_date")) ? "" : accountName;
                var published_date = Utility.ConvertToDateTimeByFormat(body.Get("published_date"), "yyyy-MM-dd HH:mm:ss");
                var published_by = string.IsNullOrEmpty(body.Get("published_date")) ? "" : accountName;

                var interviewQuestion = new NodeJs_InterviewQuestionEntity
                {
                    InterviewId = interview_id,
                    Priority = priority,
                    Type = type,
                    Status = status,
                    ProcessUser = process_user,
                    Content = content,
                    FullName = fullname,
                    Sex = sex,
                    Age = age,
                    Email = email,
                    Job = job,
                    Address = address,
                    Mobile = mobile,
                    CreatedBy = accountName,
                    ReceivedBy = received_by,
                    ReceivedDate = received_date,
                    PublishedBy = published_by,
                    PublishedDate = published_date
                };

                var id = "";
                var data = new InterviewServices().InterviewV3QuestionInsert(interviewQuestion, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_interview_question")]
        public HttpResponseMessage UpdateInterviewQuestion(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentInterviewQuestion = new InterviewServices().InterviewV3QuestionGetById(id);
                if (currentInterviewQuestion == null || currentInterviewQuestion.Id == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("interview_question Not Found!"));

                var priority = body.Get("priority");
                if (priority != null)
                    currentInterviewQuestion.Priority = Utility.ConvertToInt(priority);

                var type = body.Get("type");
                if (type != null)
                    currentInterviewQuestion.Type = Utility.ConvertToInt(type);

                var status = body.Get("status");
                if (status != null)
                    currentInterviewQuestion.Status = Utility.ConvertToInt(status);


                var process_user = body.Get("process_user");
                if (process_user != null)
                    currentInterviewQuestion.ProcessUser = process_user;

                var content = body.Get("content");
                if (content != null)
                    currentInterviewQuestion.Content = content;

                var fullname = body.Get("fullname");
                if (fullname != null)
                    currentInterviewQuestion.FullName = fullname;

                var sex = body.Get("sex");
                if (sex != null)
                    currentInterviewQuestion.Sex = Utility.ConvertToInt(sex);

                var age = body.Get("age");
                if (age != null)
                    currentInterviewQuestion.Age = Utility.ConvertToInt(age);

                var email = body.Get("email");
                if (email != null)
                    currentInterviewQuestion.Email = email;

                var job = body.Get("job");
                if (job != null)
                    currentInterviewQuestion.Job = job;

                var address = body.Get("address");
                if (address != null)
                    currentInterviewQuestion.Address = address;

                var mobile = body.Get("mobile");
                if (mobile != null)
                    currentInterviewQuestion.Mobile = mobile;

                var received_date = body.Get("received_date");
                if (received_date != null)
                {
                    currentInterviewQuestion.ReceivedDate = Utility.ConvertToDateTimeByFormat(received_date, "yyyy-MM-dd HH:mm:ss");
                    currentInterviewQuestion.ReceivedBy = accountName;
                }

                var published_date = body.Get("published_date");
                if (published_date != null)
                {
                    currentInterviewQuestion.PublishedDate = Utility.ConvertToDateTimeByFormat(published_date, "yyyy-MM-dd HH:mm:ss");
                    currentInterviewQuestion.PublishedBy = accountName;
                }

                currentInterviewQuestion.ModifiedBy = accountName;

                var data = new InterviewServices().InterviewV3QuestionUpdate(currentInterviewQuestion);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_list_interview_question_priority")]
        public HttpResponseMessage UpdateListInterviewQuestionPriority(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var ids = body.Get("ids") ?? string.Empty;
                if (string.IsNullOrEmpty(ids))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing ids!"));

                var priorities = body.Get("priorities") ?? string.Empty;
                if (string.IsNullOrEmpty(priorities))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing priorities!"));

                var listId = ids.Split(',');
                var listPriority = priorities.Split(',');

                for (var i = 0; i < listId.Count(); i++)
                {
                    var data = new InterviewServices().InterviewQuestionUpdatePriority(listId[i], Convert.ToInt32(listPriority[i]), accountName);
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse());
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("interview_question_publish")]
        public HttpResponseMessage InterviewQuestionPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var interview_id = body.Get("interview_id");
                if (string.IsNullOrEmpty(interview_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing interview_id!"));

                var data = new InterviewServices().InterviewV3QuestionPublish(id, interview_id, accountName);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("interview_question_un_publish")]
        public HttpResponseMessage InterviewQuestionUnPublish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var interview_id = body.Get("interview_id");
                if (string.IsNullOrEmpty(interview_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing interview_id!"));

                var data = new InterviewServices().InterviewV3QuestionUnPublish(id, interview_id, accountName);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("interview_question_return_wait_for_answer")]
        public HttpResponseMessage InterviewQuestionReturnWaitForAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var interview_id = body.Get("interview_id");
                if (string.IsNullOrEmpty(interview_id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing interview_id!"));

                var data = new InterviewServices().InterviewQuestionReturnWaitForAnswer(id, interview_id, accountName);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_question_by_id")]
        public HttpResponseMessage DeleteInterviewQuestionById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                
                var data = new InterviewServices().InterviewV3QuestionDelete(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region answer
        [HttpPost, Route("get_interview_answer_by_id")]
        public HttpResponseMessage GetInterviewAnswerById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                
                var data = new InterviewServices().InterviewV3AnswersGetById(id);

                return this.ToJson(WcfActionResponse<NodeJs_InterviewAnswerEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_interview_answer_by_question_id")]
        public HttpResponseMessage GetInterviewAnswerByQuestionId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var question_id = body.Get("question_id");
               
                var data = new InterviewServices().InterviewV3AnswersGetListByQuestionId(question_id);

                return this.ToJson(WcfActionResponse<List<NodeJs_InterviewAnswerEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("insert_interview_answer")]
        public HttpResponseMessage InsertInterviewAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var question_id = body.Get("question_id");

                var guests_id = body.Get("guest_id") ?? string.Empty;
                var answers_content = body.Get("answer_content") ?? string.Empty;
                var answers_date = Utility.ConvertToDateTimeByFormat(body.Get("answer_date"), "yyyy-MM-dd HH:mm:ss");
                var status = Utility.ConvertToInt(body.Get("status"));

                var interviewAnswer = new NodeJs_InterviewAnswerEntity
                {

                    QuestionId = question_id,
                    GuestId = guests_id,
                    AnswerContent = answers_content,
                    AnswerDate = answers_date,
                    Status = status,
                    AnswerBy = accountName
                };

                var id = "";
                var data = new InterviewServices().InterviewV3AnswersInsert(interviewAnswer, ref id);
                data.Data = id;
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_interview_answer")]
        public HttpResponseMessage UpdateInterviewAnswer(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var id = body.Get("id");
                if (string.IsNullOrEmpty(id))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Missing id!"));

                var currentInterviewAnswer = new InterviewServices().InterviewV3AnswersGetById(id);
                if (currentInterviewAnswer == null || currentInterviewAnswer.Id == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("interview_answer Not Found!"));

                var question_id = body.Get("question_id");
                if (question_id != null)
                    currentInterviewAnswer.QuestionId = question_id;

                var guest_id = body.Get("guest_id");
                if (guest_id != null)
                    currentInterviewAnswer.GuestId = guest_id;

                var answer_content = body.Get("answer_content");
                if (answer_content != null)
                    currentInterviewAnswer.AnswerContent = answer_content;

                var answer_by = body.Get("answer_by");
                if (answer_by != null)
                    currentInterviewAnswer.AnswerBy = answer_by;

                var answer_date = body.Get("answer_date");
                if (answer_date != null)
                {
                    currentInterviewAnswer.AnswerDate = Utility.ConvertToDateTimeByFormat(answer_date, "yyyy-MM-dd HH:mm:ss");
                }

                var status = body.Get("status");
                if (status != null)
                    currentInterviewAnswer.Status = Utility.ConvertToInt(status);

                var data = new InterviewServices().InterviewV3AnswersUpdate(currentInterviewAnswer);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_interview_answer_by_id")]
        public HttpResponseMessage DeleteInterviewAnswerById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id");
                
                var data = new InterviewServices().InterviewV3AnswersDelete(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #endregion

        #region for NewsInTopic
        [HttpPost, Route("search_news_by_topic_id")]
        public HttpResponseMessage SearchNewsByTopicId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword")??string.Empty;
                var topicId = Utility.ConvertToLong(body.Get("topic_id"));
                var status = Utility.ConvertToInt(body.Get("status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"),1);
                var pageSize = Utility.ConvertToInt(body.Get("page_size"),20);

                var totalRow = 0;
                var data = new NewsServices().SearchNewsByTopicIdWithPaging(keyword, topicId, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<NewsInTopicSearch>.CreateSuccessResponse(new NewsInTopicSearch { TotalRow = totalRow, News = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_newspublish_by_topic_id")]
        public HttpResponseMessage SearchNewsPublishByTopicId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var topicId = Utility.ConvertToLong(body.Get("topic_id"));
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("page_size"), 20);

                var totalRow = 0;
                var data = new NewsServices().SearchNewsPublishByTopicIdWithPaging(topicId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<NewsInTopicSearch>.CreateSuccessResponse(new NewsInTopicSearch { TotalRow = totalRow, News = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region for NewsPosition (cài bài)
        [HttpPost, Route("swap_news_position")]
        public HttpResponseMessage SwapNewsPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"));
                var positionData = body.Get("PositionData") ?? string.Empty;
                var positionDataMienNam = body.Get("PositionDataMienNam") ?? string.Empty;
                var positionDataStream = body.Get("PositionDataStream") ?? string.Empty;
                var positionDataStreamAdd = body.Get("PositionDataStreamAdd") ?? string.Empty;
                var positionNewsType = body.Get("PositionDataNewsType") ?? string.Empty;
                var positionPriority = body.Get("PositionDataPriority") ?? string.Empty;
                var configPosition = Utility.ConvertToInt(body.Get("ConfigPosition"), 0);

                var listNewsMobileStream = new List<UpdateNewsMobileStreamEntity>();
                var listNewsMobileStreamAdd = new List<UpdateNewsMobileStreamEntity>();
                var listNewsType = new List<DisplayPositionEmtity>();
                var listPriority = new List<PriorityEmtity>();
                var rs = new WcfActionResponse();
                // Stream Mobile
                if (!string.IsNullOrEmpty(positionDataStreamAdd))
                {
                    listNewsMobileStreamAdd = JsonConvert.DeserializeObject<List<UpdateNewsMobileStreamEntity>>(positionDataStreamAdd);
                }
                if (listNewsMobileStreamAdd.Count > 0)
                {
                    rs = new NewsServices().NewsMobileStream_RemoveTopLessView(listNewsMobileStreamAdd.Count, 16, 5);
                    rs = new NewsServices().SaveNewsMobileStream(listNewsMobileStreamAdd);
                }
                if (!string.IsNullOrEmpty(positionDataStream))
                {
                    listNewsMobileStream = JsonConvert.DeserializeObject<List<UpdateNewsMobileStreamEntity>>(positionDataStream);
                }
                if (listNewsMobileStream.Count > 0)
                {
                    rs = new NewsServices().SaveNewsMobileStream(listNewsMobileStream);
                }

                // Update Position Website
                if (!string.IsNullOrEmpty(positionData))
                {
                    var positionList = GetListNewsPosition(positionData, zoneId);
                    rs = new NewsServices().SaveNewsPosition(positionList, -1, false, accountName, true);

                    //configposition
                    if (configPosition > 0 && positionList.Count>0)
                    {
                        new NewsServices().RemoveNewsPosition(configPosition, zoneId, positionList[0].TypeId, accountName);
                    }
                }
                if (!string.IsNullOrEmpty(positionDataMienNam))
                {
                    var positionList = GetListNewsPosition(positionDataMienNam, zoneId);
                    rs = new NewsServices().SaveNewsPosition(positionList, -1, false, accountName, true);
                }
                // Update New Suggest Mobile
                if (!string.IsNullOrEmpty(positionPriority))
                {
                    listPriority = JsonConvert.DeserializeObject<List<PriorityEmtity>>(positionPriority);
                    if (listPriority.Count > 0)
                    {
                        foreach (var priorityEmtity in listPriority)
                        {
                            rs = new NewsServices().UpdatePriority(Convert.ToInt64(priorityEmtity.NewsId), Convert.ToInt32(priorityEmtity.NewsType), accountName);
                        }
                        //foreach (var newsPosition in GetListNewsPosition(positionData, zoneId))
                        //{
                        //    NewsServices.UpdatePriority(Convert.ToInt64(newsPosition.NewsId),
                        //       Convert.ToInt32(0), Policy.GetAccountName());
                        //}
                    }
                }

                // Update New Suggest Web
                if (!string.IsNullOrEmpty(positionNewsType))
                {
                    listNewsType = JsonConvert.DeserializeObject<List<DisplayPositionEmtity>>(positionNewsType);
                    if (listNewsType.Count > 0)
                    {
                        foreach (var newsTypeEmtity in listNewsType)
                        {
                            rs = new NewsServices().UpdateDisplayPosition(Convert.ToInt64(newsTypeEmtity.NewsId), Convert.ToInt32(newsTypeEmtity.NewsType), accountName);
                        }
                    }
                }
                
                var data = WcfActionResponse.CreateSuccessResponse();
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        [HttpPost, Route("save_newsposition_schedule")]
        public HttpResponseMessage SaveNewsPositionSchedule(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var order = Utility.ConvertToInt(body.Get("Order"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), 0);
                var listNewsId = body.Get("ListNewsId") ?? string.Empty;
                var listScheduleDate = body.Get("ListScheduleDate") ?? string.Empty;

                Logger.WriteLog(Logger.LogType.Trace, string.Format("updatenewsposition => ListNewsId: {0} | ListScheduleDate: {1}", listNewsId, listScheduleDate));

                if (!string.IsNullOrEmpty(listScheduleDate))
                {
                    //fomart lại date
                    var formats = new string[] { "dd/MM/yyyy HH:mm", "yyyy-MM-dd HH:mm", "dd/MM/yyyy HH:mm:ss", "dd/MM/yyyy"};
                    var arrayDate = listScheduleDate.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (arrayDate != null && arrayDate.Count > 0)
                    {
                        var fomatDateArray = new List<DateTime>();
                        foreach (var date in arrayDate)
                        {
                            fomatDateArray.Add(Utility.ParseDateTime(date, formats));
                        }
                        listScheduleDate = string.Join<string>(";", fomatDateArray.Select(s => s.ToString("yyyy-MM-dd HH:mm")));
                    }
                }

                var listRemovePosition = body.Get("ListRemovePosition") ?? string.Empty;
                var listNewsUrl = body.Get("ListNewsUrl") ?? string.Empty;

                var currentData = new NewsServices().GetScheduleByTypeAndOrder(typeId, order, zoneId);
                //var lstNewNewsId = listNewsId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                //var lstRemovedItem = (from newsPositionEntity in currentData where !lstNewNewsId.Any(x => x == newsPositionEntity.NewsId.ToString()) select newsPositionEntity.NewsId).ToList();
                var data = new NewsServices().SaveNewsPositionSchedule(typeId, zoneId, order, listNewsId, listScheduleDate, listRemovePosition, accountName);

                return this.ToJson(data);

            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }
        [HttpPost, Route("get_newspositon_schedule")]
        public HttpResponseMessage GetNewsPositionScheduleByTypeAndOrder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var order = Utility.ConvertToInt(body.Get("Order"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var typeId = Utility.ConvertToInt(body.Get("TypeId"), 0);
                var data = new NewsServices().GetScheduleByTypeAndOrder(typeId, order, zoneId);

                return this.ToJson(WcfActionResponse<List<NewsPositionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error "+ex.Message));
            }
        }
        private List<NewsPositionEntity> GetListNewsPosition(string stringPattern, int zoneId)
        {
            var newsPositions = new List<NewsPositionEntity>();

            //const string pattern = @"G(?<TypeId>[0-9]+)\((?<Position>[0-9a-zA-Z=:\-|;\s\+]+)\)";

            var groupPattern = stringPattern.Split('^');

            foreach (var groupString in groupPattern)
            {
                var match = Regex.Match(groupString, @"G(?<TypeId>[0-9]+)\((?<Position>[0-9a-zA-Z=:\-|;\s\+]+)\)");

                if (match.Success)
                {
                    var typeId = Utility.ConvertToInt(match.Groups["TypeId"]);
                    var positionString = match.Groups["Position"].ToString();

                    if (typeId == (int)NewsPositionTypeForAction.HomeLastestNews ||
                        typeId == (int)NewsPositionTypeForAction.HomeNewsFocus
                        || typeId == (int)NewsPositionTypeForAction.HomeMobileFocus
                        || typeId == (int)NewsPositionTypeForAction.HomeNewsFocus_MienNam
                        )
                    {
                        if (!HasPermission(EnumPermission.SetupNewsPosition, 0))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (!HasPermission(EnumPermission.SetupNewsPositionByZone, zoneId))
                        {
                            continue;
                        }
                    }

                    var positionList = positionString.Split(';');
                    newsPositions.AddRange(from item in positionList
                                           select item.Split('|')
                                               into stringItem
                                           where stringItem.Length == 3
                                           let order = Utility.ConvertToInt(stringItem[0])
                                           let newsId = Utility.ConvertToLong(stringItem[1])
                                           let expiredDate = Utility.ConvertToDateTime(stringItem[2])
                                           select new NewsPositionEntity()
                                           {
                                               TypeId = typeId,
                                               ZoneId = zoneId,
                                               Order = order,
                                               NewsId = newsId,
                                               ExpiredLock = expiredDate
                                           });
                }
            }

            return newsPositions;
        }
        private bool HasPermission(EnumPermission permissionId, int zoneId)
        {
            return new SecurityServices().IsUserHasPermissionInZone(WcfExtensions.WcfMessageHeader.Current.ClientUsername, permissionId, zoneId).Success;
        }
        [HttpPost, Route("get_news_list_suggest")]
        public HttpResponseMessage GetNewsListSuggest(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("id"));
                var keyword = body.Get("keyword") ?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("zone"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), -1);
                var newsType = Utility.ConvertToInt(body.Get("news_type"), -1);
                var displayPosition = Utility.ConvertToInt(body.Get("display_position"), -1);
                var priority = -1;
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("from_date"), "dd/MM/yyyy HH:mm:ss");
                var toDate = Utility.ConvertToDateTimeByFormat(body.Get("to_date"), "dd/MM/yyyy HH:mm:ss");
                var pageIndex = Utility.ConvertToInt(body.Get("page"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("size"), 10);
                var showBomb = Utility.ConvertToBoolean(body.Get("show_bomb"), false);

                var data = new List<NewsInListEntity>();
                var totalRows = 0;
                //if (showBomb)
                //{
                //    data = new NewsServices().SearchNewsForNewsPosition(zoneId, keyword, type, displayPosition, fromDate, toDate,
                //                                                  pageIndex, pageSize, "", newsType, ref totalRows);
                //}
                //else {
                //    data = new NewsServices().SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, fromDate, toDate,
                //                                                     pageIndex, pageSize, "", newsType, ref totalRows);
                //    //data = new NewsServices().SearchNewsPublishedByKeywordAndZoneId(zoneId, keyword, displayPosition, priority, showBomb, pageIndex, pageSize, true);

                //}

                if (WcfExtensions.WcfMessageHeader.Current.Namespace.ToLower().Equals("vneconomy"))
                {
                    data = new NewsServices().SearchNewsWhichPublished(zoneId, keyword, type, displayPosition, priority, fromDate, toDate, pageIndex, pageSize, "", newsType, ref totalRows);
                }
                else if (WcfExtensions.WcfMessageHeader.Current.Namespace.ToLower().Equals("afamily"))
                {
                    //voi afamily thi chi sugget trong ngay
                    data = new NewsServices().SearchNewsForNewsPositionESInDateNow(keyword, zoneId, displayPosition, priority, pageIndex, pageSize, ref totalRows);
                }
                else {
                    //search tu es
                    data = new NewsServices().SearchNewsForNewsPositionES(keyword, zoneId, displayPosition, priority, showBomb, pageIndex, pageSize, ref totalRows);
                }
                return this.ToJson(WcfActionResponse<List<NewsInListEntity>>.CreateSuccessResponse(data, "Success"));

                //chinhnb 27/03/2018
                //var result = new NewsServices().SearchNewsPublishedByKeywordAndZoneId(zoneId, keyword, displayPosition, priority, false, pageIndex, pageSize, true);
                //if (result != null)
                //{
                //    data = result.Select(s => new NewsInListEntity
                //    {
                //        Title = s.Title,
                //        Avatar = s.Avatar,
                //        Id = s.NewsId,
                //        Source = s.Source,
                //        Sapo = s.Sapo
                //    }).ToList();
                //}
                //return this.ToJson(WcfActionResponse<List<NewsInListEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_list_news_position_for_listpage")]
        public HttpResponseMessage GetListNewsPositionForListPage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), 1);

                var data = new List<NewsPositionEntity>();
                if (zoneId == 0)
                {
                    data = new NewsServices().GetNewsPositionByPositionType(type, zoneId, string.Empty);
                }
                else {
                    data = new NewsServices().GetNewsPositionForListPage(zoneId,"").HighlightListFocusByZone;
                }
                return this.ToJson(WcfActionResponse<List<NewsPositionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_list_news_position_all_bom")]
        public HttpResponseMessage GetListNewsPositionAllBom(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"), 0);
                var type = Utility.ConvertToInt(body.Get("type"), 1);

                var data = new NewsServices().GetListNewsPositionAllBom(type, zoneId);
                
                return this.ToJson(WcfActionResponse<List<NewsPositionEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //danh cho web -> sugget
        [HttpPost, Route("update_displayposition")]
        public HttpResponseMessage UpdateDisplayPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"),0);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdateDisplayPosition(newsId, displayPosition, accountName);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        //danh cho mobile -> sugget
        [HttpPost, Route("update_priority")]
        public HttpResponseMessage UpdatePriority(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().UpdatePriority(newsId, priority, accountName);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region cai bai mobile
        [HttpPost, Route("get_list_suggest_mobile")]
        public HttpResponseMessage GetNewsListSuggestMobile(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
               
                var keyword = body.Get("Keyword")?? string.Empty;
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                //var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var displayPosition = -1;
                var priority = Utility.ConvertToInt(body.Get("Priority"), 3);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 0);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                var showBomb = Utility.ConvertToBoolean(body.Get("ShowBomb"), false);
                
                var data = new NewsServices().SearchNewsPublishedByKeywordAndZoneId(zoneId, keyword, displayPosition, priority, showBomb, pageIndex, pageSize, true);

                return this.ToJson(WcfActionResponse<List<NewsPublishForSearchEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }
        [HttpPost, Route("get_list_stream_mobile")]
        public HttpResponseMessage GetNewsMobileStreamList(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                

                var data = new NewsServices().GetNewsMobileStreamList(0);

                return this.ToJson(WcfActionResponse<List<NewsMobileStreamEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("save_video_position")]
        public HttpResponseMessage SaveVideoPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = body.Get("Data") ?? "";
                if (string.IsNullOrEmpty(data))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Object data null."));
                }

                var res = new WcfActionResponse<List<WcfActionResponse>>();

                var list = NewtonJson.Deserialize<List<NewsVidepPositionEntity>>(data);
                var type = 3089; //được quy định để kết hợp với newsid ->Obj video được cài

                if(list!=null && list.Count > 0)
                {
                    var resultList = new List<WcfActionResponse>();
                    foreach (var item in list)
                    {
                       var abc = new NewsServices().SaveLinkPosition((NewsPositionType)item.TypeId, item.Position, item.ZoneId, item.VideoId, type, item.Title, item.Avatar, item.VideoUrl, item.Sapo);                        
                        resultList.Add(abc);
                    }
                    res.Success = true;
                    res.Message = "Success";
                    res.Data = resultList;
                }

                return this.ToJson(res);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion

        #region NewsStream
        [HttpPost, Route("swap_newsstream_position")]
        public HttpResponseMessage SwapNewsStreamPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var typeStream = Utility.ConvertToInt(body.Get("ZoneId"));
                var positionData = body.Get("PositionData") ?? string.Empty;
                var listAdd = body.Get("ListAdd") ?? string.Empty;
                var listDelete = body.Get("ListDelete") ?? string.Empty;
                
                var data = new NewsServices().NewsHot_GetByType(typeStream);
                
                var positionList = GetListNewsStreamPosition(positionData, typeStream);
                if (positionList != null && positionList.Count > 0) {
                    foreach (var newsPositionEntity in positionList)
                    {
                        var currentPosition = data.FirstOrDefault(x => x.SortOrder == newsPositionEntity.Order);
                        if(currentPosition!=null)
                            new NewsServices().NewsHot_ChangeNewsInLockPosition(currentPosition.Id, newsPositionEntity.NewsId, currentPosition.newsId, newsPositionEntity);
                                                
                        //adtech -> cd call
                        //AdtechServiceForHomeStream.ReplaceNewsIntoHomeStream(currentPosition.newsId, newsPositionEntity.NewsId);
                        //AdtechServiceForHomeStream.RemoveNewsIntoHomeStream(currentPosition.newsId);
                    }                    
                }
                //add them
                var positionListAdd = GetListNewsStreamPosition(listAdd, typeStream);
                if (positionListAdd != null && positionListAdd.Count > 0)
                {
                    foreach (var item in positionListAdd)
                    {                        
                        new NewsServices().NewsHot_AddNewsInLockPosition(item);                       
                    }
                }

                //Xoa them
                var positionListDelete = GetListNewsStreamPosition(listDelete, typeStream);
                if (positionListDelete != null && positionListDelete.Count > 0)
                {
                    foreach (var item in positionListDelete)
                    {
                        new NewsServices().NewsHot_DeleteNewsInLockPosition(item);
                    }
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse());
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        [HttpPost, Route("get_newsstream_position_for_list")]
        public HttpResponseMessage GetNewsPositionDataForListPage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);               

                var data = new NewsServices().NewsHot_GetByType(zoneId);

                return this.ToJson(WcfActionResponse<List<NewsHotEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        private List<NewsPositionEntity> GetListNewsStreamPosition(string stringPattern, int zoneId)
        {
            var newsPositions = new List<NewsPositionEntity>();

            //const string pattern = @"G(?<TypeId>[0-9]+)\((?<Position>[0-9a-zA-Z=:\-|;\s\+]+)\)";

            var groupPattern = stringPattern.Split('^');

            foreach (var groupString in groupPattern)
            {
                var match = Regex.Match(groupString, @"G(?<TypeId>[0-9]+)\((?<Position>[0-9a-zA-Z=:\-|;\s\+]+)\)");

                if (match.Success)
                {
                    var typeId = Utility.ConvertToInt(match.Groups["TypeId"]);
                    var positionString = match.Groups["Position"].ToString();

                    //stream chi co o trang chu zoneid=0
                    if (!HasPermission(EnumPermission.SetupNewsPosition, 0))
                    {
                        continue;
                    }

                    var positionList = positionString.Split(';');
                    newsPositions.AddRange(from item in positionList
                                           select item.Split('|')
                                               into stringItem
                                           where stringItem.Length == 3
                                           let order = Utility.ConvertToInt(stringItem[0])
                                           let newsId = Utility.ConvertToLong(stringItem[1])
                                           let expiredDate = Utility.ConvertToDateTime(stringItem[2])
                                           select new NewsPositionEntity()
                                           {
                                               TypeId = typeId,
                                               ZoneId = zoneId,
                                               Order = order,
                                               NewsId = newsId,
                                               ExpiredLock = expiredDate
                                           });
                }
            }

            return newsPositions;
        }
        [HttpPost, Route("check_news_stream")]
        public HttpResponseMessage CheckNewsStream(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                                
                var listId = body.Get("ListId") ?? string.Empty;

                var arrIds = listId.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                var lstReturn = new List<dynamic>();
                for (int i = 0; i < 3; i++)
                {
                    var newsStream = new NewsServices().NewsHot_GetByType(i + 1);
                    foreach (var newsHotEntity in newsStream)
                    {
                        foreach (var arrId in arrIds)
                        {
                            if (newsHotEntity.newsId.ToString() == arrId)
                            {
                                dynamic newsHot = new ExpandoObject();
                                newsHot.NewsId = newsHotEntity.newsId + "";
                                newsHot.Stream = i + 1;
                                lstReturn.Add(newsHot);
                            }
                        }
                    }
                }

                return this.ToJson(WcfActionResponse<List<dynamic>>.CreateSuccessResponse(lstReturn,"Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region Cài đặt StreamV2 => cd
        [HttpPost, Route("list_all_stream_v2")]
        public HttpResponseMessage ListStreamV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                

                var data = new NewsServices().ListAllStreamV2();

                return this.ToJson(WcfActionResponse<List<NewsHotEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_stream_v2")]
        public HttpResponseMessage SaveStreamV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                //var typeStream = Utility.ConvertToInt(body.Get("Type"),1);
                var positionData = body.Get("PositionData") ?? string.Empty;//[{"Type":2,"SortOrder":1,"EncryptId":"20190530093411281","ExpireDate":"2019-06-03 15:49"},{"Type":3,"SortOrder":1,"EncryptId":"20190531154513296","ExpireDate":"2019-06-03 15:49"}]
                var locked = Utility.ConvertToBoolean(body.Get("Locked"), false);

                var data = new NewsServices().SaveStreamV2(accountName, positionData, locked);

                return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region HotNewsConfig
        [HttpPost, Route("get_list_hot_news_config")]
        public HttpResponseMessage GetListHotNewsConfig(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var totalRow = 0;
                var data = new HotNewsConfigServices().GetListHotNewsConfig();

                return this.ToJson(WcfActionResponse<List<HotNewsConfigEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("save_hot_news_config")]
        public HttpResponseMessage SaveHotNewsConfig(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var strHotNewsConfig = body.Get("hotNewsConfig");
                var liststrHotNewsConfig = JsonConvert.DeserializeObject<List<HotNewsConfigEntity>>(strHotNewsConfig);
                new HotNewsConfigServices().DeleteHotNewsConfig(0);

                foreach (var hotNewsConfigEntity in liststrHotNewsConfig)
                {
                    var rs = new HotNewsConfigServices().UpdateHotNewsConfig(hotNewsConfigEntity);
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse());
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region NewsSource

        [HttpPost, Route("get_all_news_source")]
        public HttpResponseMessage GetAllNewsSource(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("keyword");

                var data = new NewsServices().NewsSourceGetAll(keyword);

                return this.ToJson(WcfActionResponse<List<NewsSourceEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_news_source_by_id")]
        public HttpResponseMessage GetNewsSourceById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().NewsSourceGetById(id);

                return this.ToJson(WcfActionResponse<NewsSourceEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_news_source_by_id")]
        public HttpResponseMessage DeleteNewsSourceById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"));
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new NewsServices().NewsSourceDelete(id);

                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("save_news_source")]
        public HttpResponseMessage SaveNewsSource(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("id"), 0);

                var name = body.Get("name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("name is required"));

                var avatar = body.Get("avatar") ?? string.Empty;
                var url = body.Get("url") ?? string.Empty;

                var source = new NewsSourceEntity
                {
                    Name = name,
                    UnsignName = Utility.UnicodeToKoDau(name),
                    SourceType = 1,
                    Avatar = avatar,
                    Url = url
                };

                var data = new WcfActionResponse();
                var sourceId = 0;
                if (id > 0)
                {
                    source.Id = id;
                    data = new NewsServices().NewsSourceUpdate(source);
                }
                else
                {
                    data = new NewsServices().NewsSourceInsert(source, ref sourceId);
                    data.Data = sourceId.ToString();
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region NewsEmbedOnPage
        [HttpPost, Route("get_news_embed_on_page")]
        public HttpResponseMessage GetListNewsEmbedBoxOnPage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));

                var data = new NewsServices().GetListNewsEmbedBoxOnPage(zoneId, type);

                return this.ToJson(WcfActionResponse<List<NewsEmbedBoxOnPageListEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_news_embed_on_page")]
        public HttpResponseMessage UpdateNewsEmbedBoxOnPage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));
                var listNewsId = body.Get("listNewsId") ?? string.Empty;

                var result = new NewsServices().UpdateNewsEmbedBoxOnPage(listNewsId, zoneId, type);

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region NewsEmbed - Box nhung ngoai trang
        [HttpPost, Route("get_news_embed")]
        public HttpResponseMessage GetListNewsEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var status = Utility.ConvertToInt(body.Get("status"));
                var type = Utility.ConvertToInt(body.Get("type"));

                var data = new NewsServices().GetListNewsEmbedBox(type, status);

                return this.ToJson(WcfActionResponse<List<NewsEmbedBoxListEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_news_embed")]
        public HttpResponseMessage UpdateNewsEmbedBox(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var type = Utility.ConvertToInt(body.Get("type"));
                var listNewsId = body.Get("listNewsId") ?? string.Empty;

                var result = new NewsServices().UpdateNewsEmbedBox(listNewsId, type);

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        #endregion

        #region BoxExpertNewsEmbed

        [HttpPost, Route("get_news_embed_box_onpage")]
        public HttpResponseMessage GetNewsEmbedBoxOnpage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var type = Utility.ConvertToInt(body.Get("type"));

                var data = new ExpertServices().GetListBoxExpertNewsEmbed(zoneId, type);

                return this.ToJson(WcfActionResponse<List<BoxExpertNewsEmbedListEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }        

        [HttpPost, Route("update_news_embed_box_onpage")]
        public HttpResponseMessage UpdateBoxExpertNewsEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var type = Utility.ConvertToInt(body.Get("type"));
                var zoneId = Utility.ConvertToInt(body.Get("zoneId"));
                var listNewsId = body.Get("listNewsId") ?? string.Empty;

                var result = new ExpertServices().UpdateBoxExpertNewsEmbed(listNewsId, zoneId, type);
                if (result.Success)
                {
                    try
                    {
                        var currentNewsPosition = new BoxExpertNewsCdData();
                        var jsonKey = "{\"ZoneId\":" + zoneId + ", \"TypeId\":" + type + "}";
                        currentNewsPosition.TypeId = type;
                        currentNewsPosition.ZoneId = zoneId;
                        currentNewsPosition.ListNewsId = listNewsId;
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(currentNewsPosition), "updateboxexpertnewsembed", string.Empty, jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }

                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_expert_in_news")]
        public HttpResponseMessage UpdateExpertInNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var expertId = Utility.ConvertToInt(body.Get("ExpertId"),0);
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var quote = body.Get("Quote") ?? string.Empty;

                var result = new ExpertServices().UpdateExpertInNews(new ExpertInNews() { ExpertId = expertId, NewsId = newsId, Quote = quote });
                if (result.Success)
                {
                    try
                    {
                        var jsonKey = "{\"ExpertId\":" + expertId + "}";
                        ContentDeliveryServices.PushToDataCD(NewtonJson.Serialize(new ExpertInNews() { ExpertId = expertId, NewsId = newsId, Quote = quote }), "updateexpertinnews", string.Empty, jsonKey);
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    }
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_expert_news_published")]
        public HttpResponseMessage SearchExpertNewsWhichPublished(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var keyword = body.Get("Keyword");
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"), -1);
                var newsType = Utility.ConvertToInt(body.Get("NewsType"), -1);
                var displayPosition = Utility.ConvertToInt(body.Get("DisplayPosition"), -1);
                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From"), "dd/MM/yyyy HH:mm");
                var todate = Utility.ConvertToDateTimeByFormat(body.Get("To"), "dd/MM/yyyy HH:mm");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);

                var totalRows = 0;
                var data = new ExpertServices().SearchExpertNewsWhichPublished(zoneId, keyword, type, displayPosition, fromDate, todate, pageIndex, pageSize, "", newsType, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total=totalRows, NewsExperts= data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxInfoGraph
        [HttpPost, Route("get_list_infograph")]
        public HttpResponseMessage ListInfoGraph(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 20;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraph(keyword, username, -1, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_list_infograph_export")]
        public HttpResponseMessage ListInfoGraphExport(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 20;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraph(keyword, username, 1, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("get_infograph_by_id")]
        public HttpResponseMessage GetInfoGraphById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("id"));
                var data = new InfoGraphServices().SelectInfoGraph(id);

                return this.ToJson(WcfActionResponse<InfoGraphEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("save_infograph")]
        public HttpResponseMessage SaveInfoGraph(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("id"), 0);

                var name = body.Get("name") ?? string.Empty;
                var description = body.Get("description") ?? string.Empty;
                var refId = 0;
                var data = new WcfActionResponse();
                if (id == 0) // insert
                {
                    data = new InfoGraphServices().InsertInfoGraph(new InfoGraphEntity()
                    {
                        Name = name,
                        Description = description,
                        Avatar = null,
                        XmlContent = "",
                        ImgPath = "",
                        CreatedBy = accountName,
                        CreatedDate = DateTime.Now,
                        Status = 0
                    }, ref refId);

                    data.Data = refId.ToString();
                }
                else // update
                {
                    data = new InfoGraphServices().UpdateInfoGraph(new InfoGraphEntity()
                    {
                        Id = id,
                        Name = name,
                        Description = description,
                        LastUpdateBy = accountName,
                        LastUpdateDate = DateTime.Now
                    });
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("delete_infograph")]
        public HttpResponseMessage DeleteInfoGraph(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().DeleteInfoGraph(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_status_infograph")]
        public HttpResponseMessage UpdateStatusInfoGraph(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var status = Utility.ConvertToInt(body.Get("status"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().UpdateInfoGraph(new InfoGraphEntity()
                {
                    Id = id,
                    Status = status
                });
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("update_xml_content_infograph")]
        public HttpResponseMessage UpdateXmlContentInfoGraph(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var xmlcontent = body.Get("xmlcontent");
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().UpdateInfoGraph(new InfoGraphEntity()
                {
                    Id = id,
                    XmlContent = xmlcontent,
                    LastUpdateBy = username,
                    LastUpdateDate = DateTime.Now
                });
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("export_infograph")]
        public HttpResponseMessage ExportInfoGraph(HttpRequestMessage request)
        {
            try
            {
                string FormatPathTime = "{0:yyyy}";

                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var xml = HttpUtility.UrlDecode(body.Get("xml") ?? "");
                var width = body.Get("w") ?? "";
                var height = body.Get("h") ?? "";
                var bg = body.Get("bg") ?? "";
                var format = body.Get("format") ?? "";
                var imageUrl = "";

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                if (xml != null && width != null && height != null && bg != null && format != null)
                {
                    var background = (!bg.Equals(mxConstants.NONE)) ? ColorTranslator.FromHtml(bg) : (Color?)null;
                    var image = mxUtils.CreateImage(int.Parse(width), int.Parse(height), background);
                    var g = Graphics.FromImage(image);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    var handler = new mxSaxOutputHandler(new mxGdiCanvas2D(g));
                    handler.Read(new XmlTextReader(new StringReader(xml)));

                    var imageFileName = "img" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + format;
                    var path = string.Format(FormatPathTime, DateTime.Now);
                    var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(accountName, path, imageFileName, false);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var remotePath = policyData[3];
                    var contentType = "image/" + format;

                    var memStream = new MemoryStream();
                    image.Save(memStream, ImageFormat.Png);
                    var imageInBytes = memStream.ToArray();
                    memStream.Close();
                    memStream.Dispose();
                    if (Services.ExternalServices.FileStorageServices.UploadImage(policy, signature, imageFileName, contentType, "data:" + contentType + ";base64," + Convert.ToBase64String(imageInBytes)))
                    {
                        imageUrl = Utility.BuildRealImageUrl(remotePath + "/" + imageFileName);
                        new InfoGraphServices().UpdateInfoGraph(new InfoGraphEntity()
                        {
                            Id = id,
                            ImgPath = imageUrl,
                            LastUpdateBy = accountName,
                            LastUpdateDate = DateTime.Now
                        });
                    }
                }

                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateSuccessResponse());
                //var data = new InfoGraphServices().UpdateInfoGraph(new InfoGraphEntity()
                //{
                //    Id = id,
                //    XmlContent = xmlcontent,
                //    LastUpdateBy = username,
                //    LastUpdateDate = DateTime.Now
                //});
                //return this.ToJson(data);
            }
            catch(Exception ex)
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region EditorInfoGraph
        [HttpPost, Route("editor_infograph_manager_image")]
        public HttpResponseMessage ListInfoGraphFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword") ?? string.Empty;
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphFolder(keyword, username, 1, -1, 1, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_show_image")]
        public HttpResponseMessage ListInfoGraphImage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword") ?? string.Empty;
                var folderId = Utility.ConvertToInt(body.Get("folderId"));
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphImage(keyword, folderId, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_manager_composite")]
        public HttpResponseMessage ListInfoGraphFolderComposite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphFolder(keyword, username, 1, -1, 2, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_show_composite")]
        public HttpResponseMessage ListInfoGraphShowComposite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword") ?? string.Empty;
                var folderId = Utility.ConvertToInt(body.Get("folderId"));
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphTemplate(keyword, folderId, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_changename_template")]
        public HttpResponseMessage InfoGraphChangeNameTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var name = body.Get("name") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().UpdateInfoGraphTemplate(new InfoGraphTemplateEntity()
                {
                    Id = id,
                    Name = name
                });
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_manager_template")]
        public HttpResponseMessage ListInfoGraphFolderTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphFolder(keyword, username, 1, -1, 4, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_list_folder_image")]
        public HttpResponseMessage ListInfoGraphListFolderImage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var keyword = body.Get("keyword");
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 100;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphFolder(keyword, username, 1, -1, 1, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_list_folder_svg")]
        public HttpResponseMessage ListSvgFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;
                var keyword = body.Get("keyword");
                var totalRow = 0;
                var data = new InfoGraphServices().SvgFolder(false, currentChannelNamespace, keyword);

                return this.ToJson(WcfActionResponse<List<InfoGraphSvgFolderEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_svg_folder")]
        public HttpResponseMessage GetSvgFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                var data = new InfoGraphServices().SvgFolder(true, currentChannelNamespace, "");

                return this.ToJson(WcfActionResponse<List<InfoGraphSvgFolderEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_list_template_folder")]
        public HttpResponseMessage ListInfoGraphListTemplateFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphFolder("", username, 1, -1, 4, 1, 100, ref totalRow);

                return this.ToJson(WcfActionResponse<List<InfoGraphFolderEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_save_folder")]
        public HttpResponseMessage SaveFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var name = body.Get("name") ?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("type"), 1);
                var visible = Utility.ConvertToBoolean(body.Get("visible"), true);
                var pub = Utility.ConvertToBoolean(body.Get("pub"), true);

                var refId = 0;
                var data = new WcfActionResponse();
                if (id == 0) // insert
                {
                    data = new InfoGraphServices().InsertInfoGraphFolder(new InfoGraphFolderEntity()
                    {
                        Name = name,
                        CreatedBy = accountName,
                        CreatedDate = DateTime.Now,
                        Public = pub,
                        Type = type
                    }, ref refId);

                    id = refId;
                    data.Data = refId.ToString();
                }
                else // update
                {
                    data = new InfoGraphServices().UpdateInfoGraphFolder(new InfoGraphFolderEntity()
                    {
                        Id = id,
                        Name = name,
                        Public = pub
                    });
                }

                new InfoGraphServices().DeleteInfoGraphUsage(id, accountName);
                if (visible)
                {
                    var reId = 0;
                    new InfoGraphServices().InsertInfoGraphUsage(id, accountName, ref reId);
                }


                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_delete_folder")]
        public HttpResponseMessage DeleteFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().DeleteInfoGraphFolder(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_delete_infograph_image")]
        public HttpResponseMessage DeleteInfoGraphImage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().DeleteInfoGraphImage(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_changename_image")]
        public HttpResponseMessage InfoGraphChangeNameImage(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var name = body.Get("name") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().UpdateInfoGraphImage(new InfoGraphImageEntity()
                {
                    Id = id,
                    Name = name
                });
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_save_image_to_folder")]
        public HttpResponseMessage InfoGraphSaveImageToFolder(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var folderId = Utility.ConvertToInt(body.Get("id"), 0);
                var filename = body.Get("filename") ?? string.Empty;
                var url = body.Get("url") ?? string.Empty;
                var width = Utility.ConvertToInt(body.Get("width"), 32);
                var height = Utility.ConvertToInt(body.Get("height"), 32);
               
                var refId = 0;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().InsertInfoGraphImage(new InfoGraphImageEntity()
                {
                    FolderId = folderId,
                    Name = filename,
                    Url = url,
                    Width = width,
                    Height = height,
                    Color = ""
                }, ref refId);
                data.Data = refId.ToString();
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_save_composite")]
        public HttpResponseMessage InfoGraphSaveComposite(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;            
                var refId = 0;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var returnImg = "";
                var xml = body.Get("xml") ?? string.Empty;                
                var xmlContent = HttpUtility.UrlDecode(body.Get("xmlContent") ?? string.Empty);
                var width = Utility.ConvertToInt(body.Get("width"), 32);
                var height = Utility.ConvertToInt(body.Get("height"), 32);
                var bg = body.Get("bg") ?? string.Empty;

                const string format = "png";
                if (xml != null && width != null && height != null && bg != null)
                {
                    var background = (!bg.Equals(mxConstants.NONE)) ? ColorTranslator.FromHtml(bg) : (Color?)null;
                    var image = mxUtils.CreateImage(width, height, background);
                    var g = Graphics.FromImage(image);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    var handler = new mxSaxOutputHandler(new mxGdiCanvas2D(g));
                    handler.Read(new XmlTextReader(new StringReader(xml)));
                   
                    var imageFileName = "img" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + format;
                    var path = string.Format("{0:yyyy}", DateTime.Now);
                    var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(accountName, path, imageFileName, false);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var remotePath = policyData[3];
                    const string contentType = "image/" + format;

                    var memStream = new MemoryStream();
                    image.Save(memStream, ImageFormat.Png);
                    var imageInBytes = memStream.ToArray();

                    memStream.Close();
                    memStream.Dispose();
                    if (Services.ExternalServices.FileStorageServices.UploadImage(policy, signature, imageFileName, contentType, "data:" + contentType + ";base64," + Convert.ToBase64String(imageInBytes)))
                    {
                        var imageUrl = remotePath + "/" + imageFileName;
                        var sizeimg = Services.ExternalServices.FileStorageServices.GetFileInfo(imageUrl);
                        var imgwidth = sizeimg.width;
                        var imgheight = sizeimg.height;
                        returnImg = Utility.BuildRealImageUrl(imageUrl);
                        var returnThumb = Utility.FormatThumb(64, 64, imageUrl);
                        var ids = body.Get("ids") ?? string.Empty;
                        var arr = ids.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);                        
                        foreach (var item in arr)
                        {
                            var data = new InfoGraphServices().InsertInfoGraphTemplate(new InfoGraphTemplateEntity()
                            {
                                FolderId = Convert.ToInt32(item),
                                Name = "unname",
                                Xml = xmlContent,
                                Preview = returnImg,
                                Thumb = returnThumb,
                                Width = imgwidth,
                                Height = imgheight
                            }, ref refId);
                        }
                    }
                }

                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateSuccessResponse());                
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_sidebar_list_svg_in_folder")]
        public HttpResponseMessage ListInfoGraphSvg(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var filter = body.Get("filter") ?? string.Empty;
                var folderId = Utility.ConvertToInt(body.Get("folderId"));
                var pageIndex = !string.IsNullOrEmpty(body.Get("pageIndex")) ? Utility.ConvertToInt(body.Get("pageIndex")) : 1;
                var pageSize = !string.IsNullOrEmpty(body.Get("pageSize")) ? Utility.ConvertToInt(body.Get("pageSize")) : 30;

                var totalRow = 0;
                var data = new InfoGraphServices().ListInfoGraphSvg(filter, folderId, pageIndex, pageSize, ref totalRow);

                return this.ToJson<WcfActionResponse<dynamic>>(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_save_template")]
        public HttpResponseMessage InfoGraphSaveTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var folderId = Utility.ConvertToInt(body.Get("folderId"), 0);
                var xmlcontent = body.Get("xmlcontent") ?? string.Empty;              

                var refId = 0;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().InsertInfoGraphTemplate(new InfoGraphTemplateEntity()
                {
                    FolderId = folderId,
                    Name = "unname",
                    Xml = xmlcontent,
                    Preview = "",
                    Thumb = "",
                    Width = 0,
                    Height = 0
                }, ref refId);
                data.Data = refId.ToString();
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_update_template")]
        public HttpResponseMessage InfoGraphUpdateTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var returnImg = "";
                var xml = body.Get("xml") ?? string.Empty;
                var tempId = Utility.ConvertToInt(body.Get("tempId"), 0);
                var width = Utility.ConvertToInt(body.Get("width"), 32);
                var height = Utility.ConvertToInt(body.Get("height"), 32);
                var bg = body.Get("bg") ?? string.Empty;

                const string format = "png";
                if (xml != null && width != null && height != null && bg != null)
                {
                    var background = (!bg.Equals(mxConstants.NONE)) ? ColorTranslator.FromHtml(bg) : (Color?)null;
                    var image = mxUtils.CreateImage(width, height, background);
                    var g = Graphics.FromImage(image);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    var handler = new mxSaxOutputHandler(new mxGdiCanvas2D(g));
                    handler.Read(new XmlTextReader(new StringReader(xml)));
                    
                    var imageFileName = "img" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + format;
                    var path = string.Format("{0:yyyy}", DateTime.Now);
                    var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(accountName, path, imageFileName, false);
                    var policy = policyData[1];
                    var signature = policyData[2];
                    var remotePath = policyData[3];
                    const string contentType = "image/" + format;

                    var memStream = new MemoryStream();
                    image.Save(memStream, ImageFormat.Png);
                    var imageInBytes = memStream.ToArray();

                    memStream.Close();
                    memStream.Dispose();
                    if (Services.ExternalServices.FileStorageServices.UploadImage(policy, signature, imageFileName, contentType, "data:" + contentType + ";base64," + Convert.ToBase64String(imageInBytes)))
                    {
                        var imageUrl = remotePath + "/" + imageFileName;
                        var sizeimg = Services.ExternalServices.FileStorageServices.GetFileInfo(imageUrl);
                        var imgwidth = sizeimg.width;
                        var imgheight = sizeimg.height;
                        returnImg = Utility.BuildRealImageUrl(imageUrl);
                        new InfoGraphServices().UpdateInfoGraphTemplate(new InfoGraphTemplateEntity()
                        {
                            Id = tempId,
                            Preview = returnImg,
                            Width = imgwidth,
                            Height = imgheight
                        });
                    }

                }

                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateSuccessResponse());
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_get_svg_folder_public")]
        public HttpResponseMessage InfoGraphGetSvgFolderPublic(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
             
                var ids = body.Get("arr") ?? string.Empty;
                var channelName = WcfExtensions.WcfMessageHeader.Current.Namespace; ;             
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                new InfoGraphServices().DeleteSvgChannel(channelName);

                var arr = ids.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in arr)
                {
                    new InfoGraphServices().InsertSvgChannel(channelName, Convert.ToInt32(item));
                }
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateSuccessResponse());
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_get_svg_item")]
        public HttpResponseMessage SelectInfoGraphSvg(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var id = Utility.ConvertToInt(body.Get("id"));
                var data = new InfoGraphServices().SelectInfoGraphSvg(id);

                return this.ToJson(WcfActionResponse<InfoGraphSvgEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_share_template")]
        public HttpResponseMessage InfoGraphShareTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var graphId = Utility.ConvertToInt(body.Get("graphId"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var currentChannelNamespace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                var data = new InfoGraphServices().SelectInfoGraph(graphId);
                if (data != null)
                {
                    var rs = new InfoGraphServices().InsertTemplateChannel(new InfoGraphTemplateChannelEntity()
                    {
                        FolderId = 0,
                        Name = data.Name,
                        XmlContent = data.XmlContent,
                        Preview = data.ImgPath,
                        ChannelName = currentChannelNamespace,
                        CreatedBy = username,
                        CreatedDate = DateTime.Now,
                        Status = false
                    });
                    return this.ToJson(rs);
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_get_template_xml")]
        public HttpResponseMessage GetTemplateXml(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var tempId = Utility.ConvertToInt(body.Get("tempId"), 0);
                var data = new InfoGraphServices().SelectInfoGraphTemplate(tempId);

                return this.ToJson(WcfActionResponse<InfoGraphTemplateEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_get_template_channel_xml")]
        public HttpResponseMessage GetTemplateChannelXml(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var tempId = Utility.ConvertToInt(body.Get("tempId"), 0);
                var data = new InfoGraphServices().SelectTemplateChannel(tempId);

                return this.ToJson(WcfActionResponse<InfoGraphTemplateChannelEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        [HttpPost, Route("editor_infograph_delete_template")]
        public HttpResponseMessage DeleteTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("id"), 0);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var data = new InfoGraphServices().DeleteInfoGraphTemplate(id);
                return this.ToJson(data);
            }
            catch
            {
                return this.ToJson<WcfActionResponse>(WcfActionResponse.CreateErrorResponse("Error!"));
            }
        }

        #endregion

        #region ContentWanning check nhậy cảm

        [HttpPost, Route("check_news_content_warning")]
        public HttpResponseMessage CheckNewsContentWarning(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var title = body.Get("Title")??string.Empty;
                var sapo = body.Get("Sapo") ?? string.Empty;
                var content = body.Get("Body") ?? string.Empty;
                //var tags = body.Get("Tags") ?? string.Empty;
                //var topic = body.Get("Topic") ?? string.Empty;

                //var data = new NewsServices().CheckNewsContentWarning(title, sapo, content, topic, tags);

                //V2 call Hoat
                var data = new NewsServices().CheckNewsContentWarningV2(title, sapo, content);

                return this.ToJson(WcfActionResponse<CheckContentWarningV2Entity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_news_content_warning")]
        public HttpResponseMessage UpdateNewsContentWarning(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var data = body.Get("DataCheckResult") ?? string.Empty;
                var isConfirm = Utility.ConvertToBoolean(body.Get("IsConfirm"), false);
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                //var result = new NewsServices().UpdateNewsContentWarning(newsId, data, isConfirm, username);

                var result = new NewsServices().UpdateNewsContentWarningV2(newsId, data, isConfirm, username);

                return this.ToJson(WcfActionResponse<ContentWarningEntity>.CreateSuccessResponse(result, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("list_content_warning_by_ids")]
        public HttpResponseMessage ListContentWarningByListId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsIds = body.Get("NewsIds")?? string.Empty;                                

                var data = new NewsServices().GetContentWarningByListNewsId(newsIds);

                return this.ToJson(WcfActionResponse<List<ContentWarningEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("recheck_news_content_warning")]
        public HttpResponseMessage ReCheckNewsContentWarning(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));                
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                //var data = new NewsServices().ReCheckNewsContentWarning(newsId, username);

                var data = new NewsServices().ReCheckNewsContentWarningV2(newsId, username);

                return this.ToJson(WcfActionResponse<ContentWarningEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxBanner

        [HttpPost, Route("search_boxbanner")]
        public HttpResponseMessage GetListBoxBannerByZoneIdAndPosition(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"),-1);
                var position = Utility.ConvertToInt(body.Get("Position"), -1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"), (int)EnumBoxBannerType.All);
                EnumBoxBannerStatus boxBannerStatus;
                if (!Enum.TryParse(status.ToString(), out boxBannerStatus))
                {
                    boxBannerStatus = EnumBoxBannerStatus.All;
                }                

                var data = new NewsServices().GetListBoxBannerByZoneIdAndPosition(zoneId, position, boxBannerStatus, (EnumBoxBannerType)type);

                return this.ToJson(WcfActionResponse<List<BoxBannerEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_boxbanner")]
        public HttpResponseMessage SaveBoxBanner(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var position = Utility.ConvertToInt(body.Get("Position"), 0);
                var displayStyle = Utility.ConvertToInt(body.Get("DisplayStyle"), 0);
                var url = body.Get("Url") ?? "";
                var title = body.Get("Title")??"";
                var sapo = body.Get("Sapo") ??"";
                var avatar = body.Get("Avatar")??"";
                var status = Utility.ConvertToInt(body.Get("Status"), 1);
                var type = Utility.ConvertToInt(body.Get("Type"), (int)EnumBoxBannerType.Link);
                var objectid = Utility.ConvertToLong(body.Get("ObjectId"));
                var zoneIdList = body.Get("ZoneIdList")??"";
                var banner = new BoxBannerEntity
                {
                    Id = id,
                    Avatar = avatar,
                    Position = position,
                    DisplayStyle = displayStyle,
                    Priority = 0,
                    Sapo = sapo,
                    Status = status,
                    Title = title,
                    Url = url,
                    ZoneId = zoneId,
                    Type = type,
                    ObjectId = objectid,
                    CreatedDate=DateTime.Now
                };
                var data = new WcfActionResponse();
                if (id == 0)
                {                    
                    data = new NewsServices().InsertBoxBanner(banner, zoneIdList, ref id);
                    data.Data = id.ToString();
                }
                else
                {
                    data = new NewsServices().UpdateBoxBanner(banner, zoneIdList);
                    data.Data = id.ToString();
                }
               
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_boxbanner_by_id")]
        public HttpResponseMessage GetBoxBannerById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                
                var data = new NewsServices().GetBoxBannerById(id);

                return this.ToJson(WcfActionResponse<BoxBannerEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_boxbanner")]
        public HttpResponseMessage DeleteBoxBanner(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                
                var data = new NewsServices().DeleteBoxBanner(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_priority_boxbanner")]
        public HttpResponseMessage UpdatePriorityBoxBanner(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listIds = body.Get("ListIds") ?? "";
                
                var data = new NewsServices().UpdateBoxBannerPriority(listIds);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxInboundComponentEmbed

        [HttpPost, Route("search_boxinboundcomponentembed")]
        public HttpResponseMessage SearchBoxInboundComponentEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var position = Utility.ConvertToInt(body.Get("Position"), -1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"),-1);
                
                var data = new NewsServices().GetListBoxInboundComponentEmbed(newsId, position, status, type);

                return this.ToJson(WcfActionResponse<List<BoxInboundComponentEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_boxinboundcomponentembed")]
        public HttpResponseMessage SaveBoxInboundComponentEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";
                if (string.IsNullOrEmpty(listData) && string.IsNullOrEmpty(listIdDelete))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListData not empty."));
                }                

                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new NewsServices().DeleteBoxInboundComponentEmbed(id);                            
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<BoxInboundComponentEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;
                                var data = new NewsServices().InsertBoxInboundComponentEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                var data = new NewsServices().UpdateBoxInboundComponentEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_boxinboundcomponentembed_by_id")]
        public HttpResponseMessage GetBoxInboundComponentEmbedById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().GetBoxInboundComponentEmbedById(id);

                return this.ToJson(WcfActionResponse<BoxInboundComponentEmbedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_boxinboundcomponentembed")]
        public HttpResponseMessage DeleteBoxInboundComponentEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().DeleteBoxInboundComponentEmbed(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_priority_boxinboundcomponentembed")]
        public HttpResponseMessage UpdatePriorityBoxInboundComponentEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listIds = body.Get("ListIds") ?? "";

                var data = new NewsServices().UpdateBoxInboundComponentEmbedPriority(listIds);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxInboundComponentTemplate

        [HttpPost, Route("search_boxinboundcomponenttemplate")]
        public HttpResponseMessage SearchBoxInboundComponnetTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var typeId = body.Get("TypeId");
                var title = body.Get("Title");
                var listZoneId = body.Get("ListZoneId");
                if (listZoneId == "-1")
                    listZoneId = "";
                var listTopicId = body.Get("ListTopicId");
                if (listTopicId == "-1")
                    listTopicId = "";
                var listThreadId = body.Get("ListThreadId");
                if (listThreadId == "-1")
                    listThreadId = "";
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);

                var totalRow = 0;
                var data = new NewsServices().GetListBoxInboundTemplate(typeId, title, listZoneId, listTopicId, listThreadId, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_boxinboundcomponenttemplate")]
        public HttpResponseMessage SaveBoxInboundComponnetTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var createdBy = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var typeId = body.Get("TypeId")??string.Empty;
                var title = body.Get("Title") ?? string.Empty;
                var description = body.Get("Description") ?? string.Empty;                
                var listZoneId = body.Get("ListZoneId") ?? string.Empty;
                var listTopicId = body.Get("ListTopicId") ?? string.Empty;
                var listThreadId = body.Get("ListThreadId") ?? string.Empty;
                var dataJson = body.Get("DataJson") ?? string.Empty;
                var dataEmbed = body.Get("DataEmbed") ?? string.Empty;

                var boxInbound = new BoxInboundTemplateEntity
                {
                    Id = id,
                    TypeId = typeId,
                    Title = title,
                    Description = description,
                    CreatedBy = createdBy,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = createdBy,
                    ModifiedDate = DateTime.Now,                    
                    ListZoneId = listZoneId,
                    ListTopicId = listTopicId,
                    ListThreadId = listThreadId,
                    DataJson = dataJson,
                    DataEmbed= dataEmbed
                };
                var data = new WcfActionResponse();

                if (id == 0)
                {
                    data = new NewsServices().InsertBoxInboundTemplate(boxInbound, ref id);
                    data.Data = id.ToString();
                }
                else
                {
                    data = new NewsServices().UpdateBoxInboundTemplate(boxInbound);
                    data.Data = id.ToString();
                }

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_boxinboundcomponenttemplate_by_id")]
        public HttpResponseMessage GetBoxInboundComponnetTemplateById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().GetBoxInboundTemplateById(id);

                return this.ToJson(WcfActionResponse<BoxInboundTemplateEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_boxinboundcomponenttemplate")]
        public HttpResponseMessage DeleteBoxInboundComponnetTemplate(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().DeleteBoxInboundTemplate(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region BoxLivePageEmbed

        [HttpPost, Route("search_boxlivepageembed")]
        public HttpResponseMessage SearchBoxLivePageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var pageName = body.Get("PageName")??"";
                var pageUrl = body.Get("PageUrl") ?? "";
                var templateId = Utility.ConvertToInt(body.Get("TemplateId"),-1);
                var position = Utility.ConvertToInt(body.Get("Position"), -1);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"), -1);

                var data = new NewsServices().GetListBoxLivePageEmbed(pageName, pageUrl, position, status, type, templateId);

                return this.ToJson(WcfActionResponse<List<BoxLivePageEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_boxlivepageembed")]
        public HttpResponseMessage SaveBoxLivePageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var userName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var listData = body.Get("ListData") ?? "";
                var listIdDelete = body.Get("ListIdDelete") ?? "";
                if (string.IsNullOrEmpty(listData) && string.IsNullOrEmpty(listIdDelete))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("ListData not empty."));
                }

                if (!string.IsNullOrEmpty(listIdDelete))
                {
                    var listId = listIdDelete.Split(',').ToList();
                    if (listId != null && listId.Count > 0)
                    {
                        foreach (var idStr in listId)
                        {
                            var id = Utility.ConvertToInt(idStr, 0);
                            new NewsServices().DeleteBoxLivePageEmbed(id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(listData))
                {
                    var listObj = NewtonJson.Deserialize<List<BoxLivePageEmbedEntity>>(listData);

                    if (listObj != null && listObj.Count > 0)
                    {
                        var res = new WcfActionResponse<List<WcfActionResponse>>();
                        var listRes = new List<WcfActionResponse>();
                        foreach (var item in listObj)
                        {
                            if (item != null && item.Id <= 0)
                            {
                                var id = 0;
                                item.CreatedDate = DateTime.Now;
                                item.CreatdBy = userName;
                                var data = new NewsServices().InsertBoxLivePageEmbed(item, ref id);
                                data.Data = id.ToString();

                                listRes.Add(data);
                            }
                            else
                            {
                                item.ModifiedDate = DateTime.Now;
                                item.ModifiedBy = userName;
                                var data = new NewsServices().UpdateBoxLivePageEmbed(item);
                                data.Data = item.Id.ToString();

                                listRes.Add(data);
                            }
                        }

                        res.Success = true;
                        res.Message = "Success";
                        res.Data = listRes;

                        return this.ToJson(res);
                    }
                }

                return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_boxlivepageembed_by_id")]
        public HttpResponseMessage GetBoxLivePageEmbedById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().GetBoxLivePageEmbedById(id);

                return this.ToJson(WcfActionResponse<BoxLivePageEmbedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_boxlivepageembed")]
        public HttpResponseMessage DeleteBoxLivePageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new NewsServices().DeleteBoxLivePageEmbed(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_priority_boxlivepageembed")]
        public HttpResponseMessage UpdatePriorityBoxLivePageEmbed(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var listIds = body.Get("ListIds") ?? "";

                var data = new NewsServices().UpdateBoxLivePageEmbedPriority(listIds);
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
