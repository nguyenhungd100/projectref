﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.VideoAutoDownloadMonitor.Entity;
using System;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using ChannelVN.CMS.BO.Nodejs;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/external/videovod")]
    public class VideoVodController : ApiController
    {
        #region VodCallback

        [AllowAnonymous]
        [HttpGet, Route("callback")]
        public HttpResponseMessage VodCallback(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var queryStrings = request.GetQueryNameValuePairs();
                if (queryStrings == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("invalid request."));
                
                var id = Utility.ConvertToLong((queryStrings.FirstOrDefault(nv => nv.Key == "id")).Value);
                if (id <= 0)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("id không hợp lệ."));
                }
                var fileUrl = (queryStrings.FirstOrDefault(nv => nv.Key == "fileUrl")).Value;
                var secretKey = (queryStrings.FirstOrDefault(nv => nv.Key == "secretkey")).Value;
                var nameSpace = (queryStrings.FirstOrDefault(nv => nv.Key == "namespace")).Value;
                if (string.IsNullOrEmpty(nameSpace))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("nameSpace không hợp lệ."));
                }
                WcfExtensions.WcfMessageHeader.Current.Namespace = nameSpace;

                Logger.WriteLog(Logger.LogType.Trace, HttpContext.Current.Request.RawUrl + "[id=" + id + " | seckey=" + secretKey + " | fileUrl=" + fileUrl + "]");
                var prirateKey = CmsChannelConfiguration.GetAppSetting("EXTERNAL_REQUEST_SECRET_KEY");
                //var consMessage = "Da convert xong VOD GLTT {0}: {1} update db {2}";
                var secretKeyMade = Crypton.Md5Encrypt(prirateKey + id);
                if (secretKey != secretKeyMade)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("secretkey không hợp lệ."));
                }

                if (!string.IsNullOrEmpty(fileUrl) && id > 0)
                {
                    //try
                    //{
                    //    var VIDEO_MANAGER_UPLOAD_NAMESPACE = CmsChannelConfiguration.GetAppSetting("VIDEO_MANAGER_UPLOAD_NAMESPACE");
                    //    if (VIDEO_MANAGER_UPLOAD_NAMESPACE != "")
                    //        fileUrl = fileUrl.Replace(VIDEO_MANAGER_UPLOAD_NAMESPACE + "/", "");
                    //}
                    //catch (Exception)
                    //{
                    //}
                    try
                    {
                        //var accountName = "service_crawler";
                        var MAXIMUM_CALL_RETRY = 3;
                        int CountReTryCall = 0;
                        while (CountReTryCall < MAXIMUM_CALL_RETRY)
                        {
                            try
                            {
                                //call open api mic3
                                //var fileInfo = VideoStorageServices.GetFileInfo(fileUrl);
                                var fileInfo = VideoApiServices.GetVideoInfo(fileUrl);

                                if (fileInfo != null && !string.IsNullOrEmpty(fileInfo.FileName))
                                {                                    
                                    var keyvideo = Crypton.Md5Encrypt(string.Format("{0}/{1}",nameSpace, fileUrl.Trim()).ToLower());

                                    //tạm thời comment check keyvideo ở quản lý video tập chung của Hiếu
                                    //var callback = VcStorageServices.VideoDownloadCallback(fileUrl, accountName, ref keyvideo);
                                    //Logger.WriteLog(Logger.LogType.Debug, "Have fileInfo Interview");
                                    //if (callback & keyvideo != "")
                                    //{
                                        Logger.WriteLog(Logger.LogType.Debug, "Start update db");
                                        CountReTryCall = 999;

                                        InsertVideoComplete(fileUrl, keyvideo, id, fileInfo);
                                        //SendSmsNotify(string.Format(consMessage, id, fileUrl, " success"));

                                        return this.ToJson(WcfActionResponse.CreateSuccessResponse("Success."));
                                    //}
                                }
                                else
                                {
                                    Logger.WriteLog(Logger.LogType.Debug, "File info is null or don't have file name. Start sleep 5 second");
                                    Thread.Sleep(5000);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.WriteLog(Logger.LogType.Error, "VcStorageServices.GetFileInfo:Error=>>" + ex.Message);
                            }

                            CountReTryCall++;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Logger.LogType.Error, ex.Message);                        
                    }
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error update data in queue."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        private void InsertVideoComplete(string savepath, string keyvideo, long newsid, NodeJs_VideoApiEntity fileInfo)
        {
            var videoAutoDownloadMonitorServices = new VideoAutoDownloadMonitorServices();
            try
            {               
                //Call api
                var ZoneId = Utility.ConvertToInt(CmsChannelConfiguration.GetAppSetting("InterviewVideoZoneId"), 0);
                var accountName = "service_crawler";
                string VideoInfor = string.Format("\"KeyVideo\":\"{0}\",\"FileName\":\"{1}\",\"Location\":\"\"", keyvideo, savepath, "");

                var InsertDb = videoAutoDownloadMonitorServices.DownloadCompleted(newsid, "{" + VideoInfor + "}", savepath, keyvideo);
                if (!InsertDb.Success)
                    Logger.WriteLog(Logger.LogType.Error, "DownloadCompleted:" + InsertDb.Message);

                //set newsextention
                var InsertDbExt = new NewsServices().SetNewsExtensionValue(newsid, EnumNewsExtensionType.LiveVODInfo, "{" + VideoInfor + "}");
                if (!InsertDbExt.Success)
                    Logger.WriteLog(Logger.LogType.Error, "InsertVideoComplete => SetNewsExtensionValue:" + InsertDbExt.Message);
                
                var embed = VideoStorageServices.GetEmbed(keyvideo);
                if (embed.StartsWith("\""))
                {
                    embed = embed.Remove(embed.Length - 1, 1).Remove(0, 1);
                    embed = embed.Replace("\\\"", "\"").Replace("\\/", "/");
                }

                var News = new NewsServices().GetNewsByNewsId(newsid);

                var avatar = fileInfo.ThumbUrl;

                var video = new VideoEntity()
                {
                    Avatar = avatar,
                    FileName = savepath,
                    CreatedBy = accountName,
                    LastModifiedBy = accountName,
                    DistributionDate = DateTime.Now,
                    Description = "",
                    KeyVideo = keyvideo,
                    Name = News.Title,
                    UnsignName = Utility.ConvertTextToUnsignName(News.Title),
                    Url = Utility.UnicodeToKoDauAndGach(News.Title),
                    Pname = "vn",
                    HtmlCode = embed,
                    Source = "VOD",
                    Mode = 1,
                    AllowAd = false,
                    VideoRelation = "",
                    Duration = fileInfo.Duration,
                    Size = fileInfo.Size.ToString(),
                    Capacity = 0,
                    IsRemoveLogo = false,
                    NewsId = 0,
                    Views = 0,
                    ZoneId = ZoneId,
                    Status = 3
                };
                //Insert video sang ims
                //var resInsertVideo = new MediaServices().SaveVideo(video, "", ZoneId.ToString(), "");
                var resInsertVideo = new MediaServices().SaveVideoV4(video, "", ZoneId.ToString(), "", "", new List<string>());
                if (!resInsertVideo.Success)
                    Logger.WriteLog(Logger.LogType.Error, "MediaServices.SaveVideo:" + resInsertVideo.Message);

                if (resInsertVideo.Success & Utility.ConvertToInt(resInsertVideo.Data) > 0 & InsertDb.Success)
                {
                    videoAutoDownloadMonitorServices.Update_Status(newsid.ToString(), VideoAutoDownloadMonitorStatus.UpdateDb);
                }
                else
                    videoAutoDownloadMonitorServices.Update_Status(newsid.ToString(), VideoAutoDownloadMonitorStatus.UpdateDbError);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                videoAutoDownloadMonitorServices.Update_Status(newsid.ToString(), VideoAutoDownloadMonitorStatus.UpdateDbError);
            }
        }

        #endregion

        #region Get Vod
        [HttpPost, Route("get_vod")]
        public HttpResponseMessage GetVod(HttpRequestMessage request)
        {
            var body = request.Content.ReadAsFormDataAsync().Result;

            Logger.WriteLog(Logger.LogType.Trace, "interview_finish:urlRequest=" + HttpContext.Current.Request.Url.AbsoluteUri);

            var responseData = new WcfActionResponse();
            var responseValue = string.Empty;
            string urlApi = CmsChannelConfiguration.GetAppSetting("InterviewFinishCallBackApi");
            try
            {
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var vid = body.Get("channelId")??string.Empty;
                if (string.IsNullOrEmpty(vid))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("channelId not empty"));
                }
                
                //check status
                var data = new VideoAutoDownloadMonitorServices().GetVideoAutoDownloadMonitorByInterviewId(newsId);
                if(data!=null && (data.Status == (int)VideoAutoDownloadMonitorStatus.DownloadCompleted || data.Status == (int)VideoAutoDownloadMonitorStatus.UpdateDb))
                {
                    responseData.Success = true;
                    responseData.Data = "{" + string.Format("\"KeyVideo\":\"{0}\",\"FileName\":\"{1}\",\"Location\":\"{2}\"", data.KeyVideo, data.SaveFilePath, "")+ "}";
                    responseData.Message = "Success.";
                    responseData.ErrorCode = 3;

                    return this.ToJson(responseData);
                }

                var prirateKey = CmsChannelConfiguration.GetAppSetting("EXTERNAL_REQUEST_SECRET_KEY");
                var secretKeyMade = Crypton.Md5Encrypt(prirateKey + newsId);
                var nameSpace = WcfExtensions.WcfMessageHeader.Current.Namespace;

                var VodCallBackIMSApi = CmsChannelConfiguration.GetAppSetting("VodCallBackIMSApi");
                if (string.IsNullOrEmpty(VodCallBackIMSApi) || (!string.IsNullOrEmpty(VodCallBackIMSApi) && (!VodCallBackIMSApi.StartsWith("http://") || !VodCallBackIMSApi.StartsWith("http://"))))
                {
                    var Req = HttpContext.Current.Request;
                    if(Req.IsLocal)
                        VodCallBackIMSApi = Req.Url.AbsoluteUri.Replace(HttpContext.Current.Request.RawUrl, "");
                    else
                    {                        
                        var match = Regex.Match(Req.Url.Host, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
                        if (match.Success)
                        {
                            VodCallBackIMSApi = Req.Url.AbsoluteUri.Replace(HttpContext.Current.Request.RawUrl, "");
                        }
                        else
                        {
                            var Port = Req.Url.Port;
                            VodCallBackIMSApi = Req.Url.AbsoluteUri.Replace(HttpContext.Current.Request.RawUrl, "").Replace(":"+Port,"");
                        }                        
                    }
                }

                var urlcallback = string.Format("{0}/api/external/videovod/callback?id={1}&secretkey={2}&namespace={3}", VodCallBackIMSApi, newsId, secretKeyMade, nameSpace);

                Logger.WriteLog(Logger.LogType.Trace, "interview_finish:urlcallback=" + urlcallback);

                urlApi += "?vid=" + vid + "&page="+ nameSpace + "&callback=" + HttpContext.Current.Server.UrlEncode(urlcallback);

                Logger.WriteLog(Logger.LogType.Trace, "interview_finish:ApiUrl=" + urlApi);

                var httpRequest = (HttpWebRequest)WebRequest.Create(urlApi);
                httpRequest.Method = WebRequestMethods.Http.Get;
                httpRequest.Timeout = 30 * 1000;

                using (var response = (HttpWebResponse)httpRequest.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = string.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        Logger.WriteLog(Logger.LogType.Error, message);
                    }

                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }
                    response.Close();
                }
                try
                {
                    Logger.WriteLog(Logger.LogType.Trace, "interview_finish:responseValue=" + responseValue);

                    var autoVideoId = 0;
                    dynamic item = NewtonJson.Deserialize<ExpandoObject>(responseValue);
                    var SourceUrl = string.Empty;
                    var Message = "Success.";
                    int status = (int)VideoAutoDownloadMonitorStatus.SendDownload;
                    if (item != null && item.status == 1)
                    {
                        SourceUrl = item.fileUrl;
                        Message = item.message;
                    }
                    if (string.IsNullOrEmpty(SourceUrl))
                    {
                        status = (int)VideoAutoDownloadMonitorStatus.NoActived;
                    }
                    if (item != null && item.status == 2)
                    {                        
                        Message = item.message;
                        status = (int)VideoAutoDownloadMonitorStatus.Downloading;
                    }
                    var resInsert = new VideoAutoDownloadMonitorServices().InsertVideoAutoDownloadMonitor(new VideoAutoDownloadMonitorEntity
                    {
                        NewsId = newsId,
                        SourceUrl = SourceUrl,
                        Status = status
                    }, ref autoVideoId);

                    if (resInsert.Success /*& status > 0*/)
                    {
                        responseData.Success = true;
                        //responseData.Data = "Success.";
                        responseData.Message = Message;
                        responseData.ErrorCode = status;
                    }
                    else {
                        responseData.Success = false;
                        //responseData.Data = "Lỗi khi auto tạo download monitor.";
                        responseData.Message = "Lỗi khi auto tạo download monitor.";
                        responseData.ErrorCode = status;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Logger.LogType.Error, ex.Message);
                    responseData.Success = false;
                    //responseData.Data = ex.Message;
                    responseData.Message = ex.Message;
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, "interview_finish[" + urlApi + "] error => " + ex.ToString());
                responseData.Success = false;
                //responseData.Data = ex.Message;
                responseData.Message = ex.Message;
            }

            //Logger.WriteLog(Logger.LogType.Trace, "interview_finish:responseValue=" + responseValue);

            return this.ToJson(responseData);
        }

        [HttpPost, Route("get_status")]
        public HttpResponseMessage GetStatus(HttpRequestMessage request)
        {            
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                
                var data= new VideoAutoDownloadMonitorServices().GetVideoAutoDownloadMonitorByInterviewId(newsId);

                return this.ToJson(WcfActionResponse<VideoAutoDownloadMonitorEntity>.CreateSuccessResponse(data,"Success"));
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Logger.LogType.Error, ex.Message);
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }
        #endregion
    }
}
