﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using ChannelVN.CMS.WebApi.Services.Triggers.Base;
using ChannelVN.Kenh14.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/external/newspr")]
    public class NewsPrController : ApiController
    {
        #region NewsPr

        [HttpPost, Route("search")]
        public HttpResponseMessage SearchNewsPr(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")??string.Empty;                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var username = body.Get("Account")?? WcfExtensions.WcfMessageHeader.Current.ClientUsername;                
                var status = Utility.ConvertToInt(body.Get("Status"), 0);               
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRows = 0;
                var data = CmsPrServices.GetListNewsPrByStatus((CmsPrServices.NewsPrStatus)status, zoneId, pageIndex, pageSize, ref totalRows);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_by_id")]
        public HttpResponseMessage GetNewsPrById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var contentId = Utility.ConvertToInt(body.Get("ContentId"), 0);
                var data = CmsPrServices.GetNewsPrDetail(contentId);                
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy data"));
                }

                return this.ToJson(WcfActionResponse<CmsPrServices.NewsEntityApi>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("approve")]
        public HttpResponseMessage Approve(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                
                //var newsId = Utility.ConvertToInt(body.Get("NewsId"), 0);                
                var contentId = Utility.ConvertToInt(body.Get("ContentId"), 0);
                var distributionId = Utility.ConvertToInt(body.Get("DistributionId"), 0);
                var _message = body.Get("Message")??"";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var channelName = CmsPrServices.ChannelName;
                var subStatus = Utility.ConvertToInt(body.Get("SubStatus"), (int)CmsPrServices.NewsPrStatus.WaitForApprove);
                string message = string.Format("[{0}] đã duyệt bài này", username + "-" + channelName);
                var contentDetail = CmsPrServices.GetNewsPrDetail(contentId);

                if (contentDetail.Status != 11)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bài PR đã bị hủy! Không thể duyệt bài này!"));                    
                }
                else
                {
                    var data = CmsPrServices.UpdateNewsPrStatus(contentId, CmsPrServices.NewsPrStatus.Approved, message, username);
                    return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(data, "Success"));
                }                                               
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("return")]
        public HttpResponseMessage Return(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var newsId = Utility.ConvertToInt(body.Get("NewsId"), 0);
                var contentId = Utility.ConvertToInt(body.Get("ContentId"), 0);
                var distributionId = Utility.ConvertToInt(body.Get("DistributionId"), 0);
                var _message = body.Get("Message") ?? "";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var channelName = CmsPrServices.ChannelName;
                var subStatus = Utility.ConvertToInt(body.Get("SubStatus"), (int)CmsPrServices.NewsPrStatus.WaitForApprove);
                string message = string.Format("[{0}] đã trả lại bài này: {1}", username + "-" + channelName, _message);

                var contentDetail = CmsPrServices.ReturnNewsPr(newsId, distributionId, message, username);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(contentDetail, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("publish")]
        public HttpResponseMessage Publish(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                //var newsId = Utility.ConvertToInt(body.Get("NewsId"), 0);
                var contentId = Utility.ConvertToInt(body.Get("ContentId"), 0);
                var distributionId = Utility.ConvertToInt(body.Get("DistributionId"), 0);                
                var username = body.Get("Account")=="" ? WcfExtensions.WcfMessageHeader.Current.ClientUsername: body.Get("Account");
                var channelName = CmsPrServices.ChannelName;
                var subStatus = Utility.ConvertToInt(body.Get("SubStatus"), (int)CmsPrServices.NewsPrStatus.WaitForApprove);
                string message = string.Format("[{0}] đã nhận bài này về IMS", username + "-" + channelName);
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var a = CmsPrServices.GetNewsPrDetail(contentId);
                var zoneId = Utility.ConvertToInt(a.ZoneID);
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleAdmin, zoneId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền xuất bản bài trong mục này!"));                    
                }
                
                var status = 1;
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleAdmin, zoneId))
                {
                    status = (int)NewsStatus.ReceivedForPublish;
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleEditor, zoneId))
                {
                    status = (int)NewsStatus.ReceivedForEdit;
                }

                string lstTag = string.Empty;
                if (!string.IsNullOrEmpty(a.TagsItem))
                {
                    var arrTag = a.TagsItem.Split(new string[] { ";" }, StringSplitOptions.None);
                    foreach (var s in arrTag)
                    {
                        var tag = new TagServices().GetTagByTagName(s.Trim());
                        if (tag != null)
                            lstTag += tag.Id + ";";
                    }
                }
                var authorPr = CmsChannelConfiguration.GetAppSetting("AuthorPR");
                if (string.IsNullOrEmpty(authorPr))
                    authorPr = "A.D";

                var newsEntity = new NewsEntity()
                {
                    Title = a.Headline,
                    SubTitle = a.SubTitle,
                    Sapo = a.Teaser,
                    Body = a.Body,
                    Avatar = a.Avatar,
                    Avatar5 = a.SmallAvatar,
                    AvatarDesc = a.AvatarDesc,
                    Source = a.Source,
                    Author = authorPr, //"A.D",
                    WordCount = int.Parse(a.WordCount),
                    IsPr = true,
                    PrPosition = int.Parse(a.PRNumber),
                    AdStore = bool.Parse(a.AdStore),
                    AdStoreUrl = "",
                    PrBookingNumber = a.ContractNo,
                    Url = "",
                    LocationType = a.LocationId,
                    Status = status,
                    CreatedBy = accountName,
                    EditedBy= accountName,
                    PublishedBy= accountName,
                    ApprovedBy = accountName,
                    ApprovedDate=DateTime.Now,
                    ZoneId = zoneId,
                    NewsRelation = a.ContentRelation,
                    TagItem = lstTag,
                    NewsCategory = 0,
                    Type = 0,
                    Price = 0,
                    ListZoneId=zoneId.ToString(),
                    ZoneName=a.ZoneTite
                };
                if (CmsTrigger.Instance.CmsPrChannelId == CmsPrServices.ChannelId.Kenh14)
                {
                    newsEntity.DisplayStyle = 1;
                }
                //Logger.WriteLog(Logger.LogType.Trace, NewtonJson.Serialize(newsEntity));
                //var disDate = a.DistributionDate.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);
                //var pubDate = a.PublishDate.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries);

                var contract = CmsPrServices.ShowContractInfo(a.ContractNo, a.Mode, zoneId);
                var price = contract != null && contract.Table2.Count > 0 ? contract.Table2[0].gia_baiviet : 0;

                NewsPrEntity newsPrEntity = new NewsPrEntity()
                {
                    ContentId = contentId,
                    DistributionId = long.Parse(a.DistributionID),
                    Mode = a.Mode,
                    IsFocus = bool.Parse(a.IsFocus),
                    DistributionDate = a.DeployDate,//;Utility.ConvertToDateTime(disDate[1]+"/"+disDate[0]+"/"+disDate[2]),
                    PublishDate = a.DeployDate,//Utility.ConvertToDateTime(pubDate[1] + "/" + pubDate[0] + "/" + pubDate[2]),//DateTime.ParseExact(a.PublishDate, "dd/MM/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture),
                    ExpireDate = a.DeployDate.AddMinutes(a.Duration),//DateTime.Now,//DateTime.ParseExact(a.PublishDate, "dd/MM/yyyy HH:mm:ss tt", CultureInfo.InvariantCulture)
                    Duration = a.Duration,
                    BookingId = a.BookingId,
                    Price = price,
                    ZoneId = zoneId
                };
                Logger.WriteLog(Logger.LogType.Trace, "CmsPrServices.GetNewsPrDetail=>" + NewtonJson.Serialize(newsPrEntity));
                long newsId = 0L;
                var responseData =new NewsServices().RecieveNewsPrIntoCms(newsEntity, newsPrEntity, a.ContentRelation, ref newsId);
                if (responseData.Success)
                {
                    responseData.Data = newsId.ToString();
                    //CmsPrServices.RecieveNewsPr(contentId, int.Parse(a.DistributionID), message);
                }
                                
                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region NewsPr V2
        private readonly string rootUrl = "http://channel.mediacdn.vn";

        [HttpPost, Route("search_v2")]
        public async Task<HttpResponseMessage> SearchNewsPrV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);

                var totalRows = 0;
                var data = await CmsPrV2Services.GetListNewsPrByStatus((CmsPrV2Services.NewsPrStatus)status, zoneId, pageIndex, pageSize);
                if (data != null && data.success)
                {
                    //buil full link avatar
                    var list = data.data?.Select(s => {
                        s.Avatar = (string.IsNullOrEmpty(s.Avatar)? s.Avatar: s.Avatar.StartsWith("http://") ? s.Avatar : string.Format("{0}/{1}", rootUrl, s.Avatar));
                        s.Avatar2 = (string.IsNullOrEmpty(s.Avatar2) ? s.Avatar2 : s.Avatar2.StartsWith("http://") ? s.Avatar2 : string.Format("{0}/{1}", rootUrl, s.Avatar2));
                        s.Avatar3 = (string.IsNullOrEmpty(s.Avatar3) ? s.Avatar3 : s.Avatar3.StartsWith("http://") ? s.Avatar3 : string.Format("{0}/{1}", rootUrl, s.Avatar3));
                        s.Avatar4 = (string.IsNullOrEmpty(s.Avatar4) ? s.Avatar4 : s.Avatar4.StartsWith("http://") ? s.Avatar4 : string.Format("{0}/{1}", rootUrl, s.Avatar4));
                        s.Avatar5 = (string.IsNullOrEmpty(s.Avatar5) ? s.Avatar5 : s.Avatar5.StartsWith("http://") ? s.Avatar5 : string.Format("{0}/{1}", rootUrl, s.Avatar5));
                        s.AvatarThumb = (string.IsNullOrEmpty(s.AvatarThumb) ? s.AvatarThumb : s.AvatarThumb.StartsWith("http://") ? s.AvatarThumb : string.Format("{0}/{1}", rootUrl, s.AvatarThumb));
                        return s; }).ToList();
                    return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, Data = list }, "Success"));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse(data.message));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_by_id_v2")]
        public async Task<HttpResponseMessage> GetNewsPrV2ById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var contentId = Utility.ConvertToLong(body.Get("ContentId"));
                var data = await CmsPrV2Services.GetNewsPrDetail(contentId);
                if (data != null)
                {
                    data.Avatar = (string.IsNullOrEmpty(data.Avatar) ? data.Avatar : data.Avatar.StartsWith("http://") ? data.Avatar : string.Format("{0}/{1}", rootUrl, data.Avatar));
                    data.Avatar2 = (string.IsNullOrEmpty(data.Avatar2) ? data.Avatar2 : data.Avatar2.StartsWith("http://") ? data.Avatar2 : string.Format("{0}/{1}", rootUrl, data.Avatar2));
                    data.Avatar3 = (string.IsNullOrEmpty(data.Avatar3) ? data.Avatar3 : data.Avatar3.StartsWith("http://") ? data.Avatar3 : string.Format("{0}/{1}", rootUrl, data.Avatar3));
                    data.Avatar4 = (string.IsNullOrEmpty(data.Avatar4) ? data.Avatar4 : data.Avatar4.StartsWith("http://") ? data.Avatar4 : string.Format("{0}/{1}", rootUrl, data.Avatar4));
                    data.Avatar5 = (string.IsNullOrEmpty(data.Avatar5) ? data.Avatar5 : data.Avatar5.StartsWith("http://") ? data.Avatar5 : string.Format("{0}/{1}", rootUrl, data.Avatar5));
                    data.AvatarThumb = (string.IsNullOrEmpty(data.AvatarThumb) ? data.AvatarThumb : data.AvatarThumb.StartsWith("http://") ? data.AvatarThumb : string.Format("{0}/{1}", rootUrl, data.AvatarThumb));

                    var ext = await CmsPrV2Services.GetNewsExtension(contentId);
                    var obj = new CmsPrV2Services.NewsEntityApiEdit {
                        NewsInfo= data,
                        NewsExtension= ext.data
                    };
                    return this.ToJson(WcfActionResponse<CmsPrV2Services.NewsEntityApiEdit>.CreateSuccessResponse(obj, "Success"));
                    
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy data"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("approve_v2")]
        public async Task<HttpResponseMessage> ApproveV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var contentId = Utility.ConvertToLong(body.Get("ContentId"));
                var channelName = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName");
                var _message = body.Get("Message") ?? "";
                                               
                string message = string.Format("[{0}] đã duyệt bài này: {1}", username + "-" + channelName, _message);
                
                var data = await CmsPrV2Services.UpdateNewsPrStatus(contentId, CmsPrV2Services.NewsPrStatus.Approved, message, username, 0, 0, "", "");
                if (data)
                {                    
                    return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(data, "Success"));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Xảy ra lỗi duyệt bài."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("return_v2")]
        public async Task<HttpResponseMessage> ReturnV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var contentId = Utility.ConvertToLong(body.Get("ContentId"));
                var channelName = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName");
                var _message = body.Get("Message") ?? "";

                string message = string.Format("[{0}] đã trả lại bài này: {1}", username + "-" + channelName, _message);

                var data = await CmsPrV2Services.UpdateNewsPrStatus(contentId, CmsPrV2Services.NewsPrStatus.Return, message, username, 0, 0, "", "");
                if (data)
                    return this.ToJson(WcfActionResponse<bool>.CreateSuccessResponse(data, "Success"));

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Xảy ra lỗi trả lại bài."));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("recieve_v2")]
        public async Task<HttpResponseMessage> RecieveV2(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var contentId = Utility.ConvertToLong(body.Get("ContentId"));                
                var channelName = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName");
                var _message = body.Get("Message") ?? "";
                string message = string.Format("[{0}] đã nhận bài này về IMS {1}", accountName + "-" + channelName, _message);

                var a = await CmsPrV2Services.GetNewsPrDetail(contentId);
                if (a == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy dữ liệu của tin bài."));
                }

                var zoneId = Utility.ConvertToInt(a.ZoneID);
                if (!BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleAdmin, zoneId))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Bạn không có quyền nhận bài trong mục này!"));
                }

                var status = 1;
                if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleAdmin, zoneId))
                {
                    status = (int)NewsStatus.ReceivedForPublish;
                }
                else if (BoCached.Base.Security.PermissionDalFactory.CheckUserPermission(accountName, (int)EnumPermission.ArticleEditor, zoneId))
                {
                    status = (int)NewsStatus.ReceivedForEdit;
                }

                string lstTag = string.Empty;
                if (!string.IsNullOrEmpty(a.TagsItem))
                {
                    var arrTag = a.TagsItem.Split(new string[] { ";" }, StringSplitOptions.None);
                    foreach (var s in arrTag)
                    {
                        var tag = new TagServices().GetTagByTagName(s.Trim());
                        if (tag != null)
                            lstTag += tag.Id + ";";
                    }
                }
                var authorPr = CmsChannelConfiguration.GetAppSetting("AuthorPR");
                if (string.IsNullOrEmpty(authorPr))
                    authorPr = "A.D";

                var newsEntity = new NewsEntity()
                {
                    Title = a.Headline,
                    SubTitle = a.SubTitle,
                    Sapo = a.Teaser,
                    Body = a.Body,
                    Avatar = (string.IsNullOrEmpty(a.Avatar) ? a.Avatar : a.Avatar.StartsWith("http://") ? a.Avatar : string.Format("{0}/{1}", rootUrl, a.Avatar)),
                    Avatar2 = (string.IsNullOrEmpty(a.Avatar2) ? a.Avatar2 : a.Avatar2.StartsWith("http://") ? a.Avatar2 : string.Format("{0}/{1}", rootUrl, a.Avatar2)),
                    Avatar3 = (string.IsNullOrEmpty(a.Avatar3) ? a.Avatar3 : a.Avatar3.StartsWith("http://") ? a.Avatar3 : string.Format("{0}/{1}", rootUrl, a.Avatar3)),
                    Avatar4 = (string.IsNullOrEmpty(a.Avatar4) ? a.Avatar4 : a.Avatar4.StartsWith("http://") ? a.Avatar4 : string.Format("{0}/{1}", rootUrl, a.Avatar4)),                  
                    Avatar5 = (string.IsNullOrEmpty(a.Avatar5) ? a.Avatar5 : a.Avatar5.StartsWith("http://") ? a.Avatar5 : string.Format("{0}/{1}", rootUrl, a.Avatar5)),
                    AvatarCustom= (string.IsNullOrEmpty(a.AvatarThumb) ? a.AvatarThumb : a.AvatarThumb.StartsWith("http://") ? a.AvatarThumb : string.Format("{0}/{1}", rootUrl, a.AvatarThumb)),
                    AvatarDesc = (string.IsNullOrEmpty(a.AvatarDesc) ? a.AvatarDesc : a.AvatarDesc.StartsWith("http://") ? a.AvatarDesc : string.Format("{0}/{1}", rootUrl, a.AvatarDesc)),
                    Source = a.Source,
                    Author = authorPr, //"A.D",
                    WordCount = int.Parse(a.WordCount),
                    IsPr = true,
                    PrPosition = int.Parse(a.PRNumber??"0"),
                    AdStore = bool.Parse(a.AdStore??"false"),
                    AdStoreUrl = "",
                    PrBookingNumber = a.ContractNo,
                    Url = a.Url,
                    LocationType = a.LocationId,
                    Status = status,
                    CreatedBy = accountName,
                    EditedBy = accountName,
                    PublishedBy = accountName,
                    ApprovedBy = accountName,
                    ApprovedDate = DateTime.Now,
                    ZoneId = zoneId,
                    NewsRelation = a.ContentRelation,
                    TagItem = lstTag,
                    NewsCategory = 0,
                    Type = a.Type,
                    NewsType=a.NewsType,
                    Price = 0,
                    ListZoneId= zoneId.ToString(),
                    ZoneName = a.ZoneTitle
                };                                

                //var contract = CmsPrServices.ShowContractInfo(a.ContractNo, a.Mode, zoneId);
                //var price = contract != null && contract.Table2.Count > 0 ? contract.Table2[0].gia_baiviet : 0;

                NewsPrEntity newsPrEntity = new NewsPrEntity()
                {
                    ContentId = contentId,
                    DistributionId = a.DistributionID,
                    Mode = a.Mode,
                    IsFocus = bool.Parse(a.IsFocus),
                    DistributionDate = a.DistributionDate,
                    PublishDate = a.PublishedDate,
                    ExpireDate = a.ExpireDate,
                    Duration = a.Duration,
                    BookingId = a.BookingId,
                    Price = 0,
                    ZoneId = zoneId,
                    CreatedDate=DateTime.Now                    
                };

                var newsExtension = new List<NewsExtensionEntity>();
                var ext = await CmsPrV2Services.GetNewsExtension(contentId);
                if (ext.success && ext.data != null && ext.data.Count > 0)
                    newsExtension = ext.data.Select(s => new NewsExtensionEntity { NewsId=0,Type=s.Type,Value=s.Value,NumericValue=s.NumericValue}).ToList();

                Logger.WriteLog(Logger.LogType.Trace, "CmsPrV2Services.GetNewsPrDetail=>" + NewtonJson.Serialize(newsPrEntity));

                long newsId = 0L;
                var responseData = new NewsServices().RecieveNewsPrV2IntoCms(newsEntity, newsPrEntity, a.ContentRelation, newsExtension, ref newsId);
                if (responseData.Success)
                {                    
                    responseData.Data = newsId.ToString();
                    var data = await CmsPrV2Services.UpdateNewsPrStatus(contentId, CmsPrV2Services.NewsPrStatus.Recieved, message, accountName, newsId, a.BookingId, a.Url, "");
                }

                return this.ToJson(responseData);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_bookinginfo")]
        public async Task<HttpResponseMessage> GetBookinginfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var bookingId = Utility.ConvertToInt(body.Get("BookingId"),0);

                var data = await CmsPrV2Services.GetBookingId(bookingId);
                if (data != null)
                {                    
                    return this.ToJson(WcfActionResponse<CmsPrV2Services.BookingInfo>.CreateSuccessResponse(data.data, "Success"));
                }

                return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy data"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_newspr_info")]
        public HttpResponseMessage UpdateNewsPrInfo(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var distributionDate = Utility.ConvertToDateTimeByFormat(body.Get("DistributionDate"), "dd/MM/yyyy HH:mm");
                var mode = Utility.ConvertToInt(body.Get("Mode"),0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);

                var channelName = CmsChannelConfiguration.GetAppSetting("ApiCmsPrV2ChannelName");
                var _message = body.Get("Message") ?? "";
                string message = string.Format("[{0}] cập nhật thay đổi Booking bài này: {1}, {2}_{3}_{4}_{5}", username + "-" + channelName, _message, newsId, mode, zoneId, distributionDate);
                
                Logger.WriteLog(Logger.LogType.Trace, "UpdateNewsPrInfo: message =>" + message);

                var newsPr = new NewsServices().GetNewsPrByNewsId(newsId);
                
                var data = new NewsServices().UpdateNewsPrV2Info_2(new NewsPrEntity
                {
                    NewsId = newsId,
                    Mode = mode,                                      
                    DistributionDate = distributionDate,   
                    PublishDate = distributionDate,
                    ExpireDate = distributionDate.AddMinutes(newsPr.Duration),
                    ZoneId=zoneId,
                }, message);   
                                     
                return this.ToJson(data);               
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion
    }
}
