﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.MenuExtension.Entity;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/menuextension")]
    public class MenuExtensionController : ApiController
    {
        #region MenuExtension

        [HttpPost, Route("save_menu")]
        public HttpResponseMessage SaveMenuExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var parentMenuId = Utility.ConvertToInt(body.Get("ParentMenuId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var sortOrder = Utility.ConvertToInt(body.Get("Priority"), 0);
                var groupMenuId = Utility.ConvertToInt(body.Get("GroupMenuId"), 0);
                var parentZoneId = Utility.ConvertToInt(body.Get("ParentZoneId"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var tags = body.Get("Tags") ?? "";
                var url = body.Get("Url") ?? "";
                var userName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Invalid zone's name."));
                }
                var shortUrl = Utility.UnicodeToKoDauAndGach(name);
                var zone = new MenuExtensionEntity
                {
                    Name = name,
                    ParentMenuId = parentMenuId,
                    Avatar = avatar,
                    GroupMenuId = groupMenuId,
                    ParentZoneId = parentZoneId,
                    Id = id,
                    Status = status,
                    Priority = sortOrder,
                    ZoneId = zoneId,
                };
                if (string.IsNullOrEmpty(url))
                    zone.Url = Utility.UnicodeToKoDauAndGach(name);
                else
                    zone.Url = url;

                if (id > 0)
                {
                    zone.Id = id;
                    var result = new MenuExtensionServices().MenuExtension_Update(zone, tags);
                    result.Data = id.ToString();
                    return this.ToJson(result);
                }
                else
                {
                    int menuId = 0;
                    var result = new MenuExtensionServices().MenuExtension_Insert(zone, tags, ref menuId);
                    result.Data = menuId.ToString();
                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        
        [HttpPost, Route("get_menu_by_id")]
        public HttpResponseMessage GetMenuExtensionById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);

                var menuextension = new MenuExtensionServices().MenuExtension_GetById(id);
                if (menuextension == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy menuextension"));
                }

                return this.ToJson(WcfActionResponse<MenuExtensionEntity>.CreateSuccessResponse(menuextension, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search")]
        public HttpResponseMessage ListTagMenuExtension(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var parentId = Utility.ConvertToInt(body.Get("ParentId"), -1);
                var zoneid = Utility.ConvertToInt(body.Get("ZoneId"), -1);
                var groupid = Utility.ConvertToInt(body.Get("GroupId"), -1);
                var isTag = Utility.ConvertToBoolean(body.Get("IsTag"), false);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var extStatus = MenuExtensionStatus.All;
                if (status == 1)
                {
                    extStatus = MenuExtensionStatus.Actived;
                }
                if (status == 0)
                {
                    extStatus = MenuExtensionStatus.Lock;
                }

                if (parentId > 0)
                {
                    zoneid = -1;
                    groupid = -1;
                }                

                var data = new MenuExtensionServices().MenuExtension_Search(zoneid, extStatus, groupid, parentId, isTag);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = data.Count, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_tag")]
        public HttpResponseMessage SaveTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var menuid = Utility.ConvertToInt(body.Get("MenuId"), 0);                
                var groupid = Utility.ConvertToInt(body.Get("GroupId"), 0);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var tags = body.Get("Tags") ?? "";
                                                                
                var result = new MenuExtensionServices().MenuExtension_InsertTag(menuid < 0 ? 0 : menuid, groupid, zoneId, tags);
                return this.ToJson(result);               
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage MenuExtensionDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var menuextensionId = Utility.ConvertToInt(body.Get("Id"));

                var result = new Kenh14Services().DeleteById(menuextensionId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
