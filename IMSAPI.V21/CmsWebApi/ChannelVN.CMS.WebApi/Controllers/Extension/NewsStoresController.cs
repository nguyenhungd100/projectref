﻿using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/newsstores")]
    public class NewsStoresController : ApiController
    {
        [HttpPost, Route("list_channel")]
        public HttpResponseMessage ListChannel(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                          

                var data = NewsStoresServices.ListChannel();
                if (data == null)
                {
                    data = new NodeJs_ChannelsEntity
                    {                        
                        Channels= new List<NodeJs_ChannelEntity>{
                            new NodeJs_ChannelEntity
                            {
                                Id = 1,
                                Name = "Kênh 3'9"
                            }
                        }
                    };
                }
                return this.ToJson(WcfActionResponse<NodeJs_ChannelsEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("search_newsstores")]
        public HttpResponseMessage SearchNewsStores(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword") ?? string.Empty;
                var channelId = Utility.ConvertToInt(body.Get("ChannelId"),-1);
                var fDate = body.Get("FromDate");
                if (!string.IsNullOrEmpty(fDate))
                {
                    fDate = fDate + " 00:00";
                }
                var fromDate = Utility.ConvertToDateTimeByFormat(fDate, "dd/MM/yyyy HH:mm");

                var tDate = body.Get("ToDate");
                if (!string.IsNullOrEmpty(tDate))
                {
                    tDate = tDate + " 23:59";
                }
                var toDate = Utility.ConvertToDateTimeByFormatAndDateNow(tDate, "dd/MM/yyyy HH:mm");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 20);
                if (pageSize > 100)
                {
                    pageSize = 100;
                }

                var data = NewsStoresServices.SearchNewsStore(keyword, channelId, fromDate, toDate, pageIndex, pageSize);

                return this.ToJson(WcfActionResponse<NodeJs_NewsResponseEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        
    }
}
