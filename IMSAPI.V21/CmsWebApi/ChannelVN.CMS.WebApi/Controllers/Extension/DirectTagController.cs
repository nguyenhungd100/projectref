﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Entity.Base.DirectTag;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/directtag")]
    public class DirectTagController : ApiController
    {
        #region DirectTag

        [HttpPost, Route("search_tag")]
        public HttpResponseMessage SearchDirectTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var search = body.Get("Keyword");
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);

                List<DirectTagEntity> lst = null;
                var totalRows = 0;
                if (!string.IsNullOrEmpty(search))
                {
                    lst = new DirectTagServices().SearchDirectTag(search, type, pageIndex, pageSize, ref totalRows);
                }
                else
                {
                    if (type == 0)
                        lst = new DirectTagServices().SelectAllDirectTagNews(pageIndex, pageSize, ref totalRows);
                    else
                    {
                        if (type == 1)
                            lst = new DirectTagServices().SelectAllByTypeDirectTagNews(pageIndex, pageSize, type, ref totalRows);
                    }
                }
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = lst }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("search_news")]
        public HttpResponseMessage SearchDirectTagNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var fromDate = Utility.ConvertToDateTimeByFormat(body.Get("From")??"", "dd/MM/yyyy");
                var todate = Utility.ConvertToDateTimeByFormat(body.Get("To")??""+" 23:59:59", "dd/MM/yyyy HH:mm:ss");
                var zoneId = body.Get("ZoneId")?? "";        
                var keyword = body.Get("Keyword");
                var lessThan3Tag = Utility.ConvertToInt(body.Get("LessThan3Tag"), 0);                               
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);

                List<NewsPublishForSearchEntity> lst = null;
                var totalRows = 0;
                if (lessThan3Tag == 0)
                    lst = new DirectTagServices().SearchNewsPublish(keyword, "", zoneId, fromDate, todate, pageIndex, pageSize, ref totalRows);
                if (lessThan3Tag == 1)
                    lst = new DirectTagServices().SearchNewsPublish_DirectTag_LessThan_3Tag(keyword, "", zoneId, fromDate, todate, pageIndex, pageSize, ref totalRows);                

                if(lst!=null && lst.Count > 0)
                {
                    foreach (var item in lst)
                    {
                        var data = new DirectTagServices().SelectDirectTagByNewsId(item.NewsId);

                        var lstTag = new List<DirectTagEntity>();
                        var lstTagLink = new List<DirectTagEntity>();

                        foreach (var a in data)
                        {
                            if (a.Type > 0)
                            {
                                lstTagLink.Add(a);
                            }
                            else lstTag.Add(a);
                        }

                        if (lstTag != null && lstTag.Count > 0)
                        {
                            item.ListTag = lstTag;
                        }

                        if (lstTagLink != null && lstTagLink.Count > 0)
                        {
                            item.ListTagLink = lstTagLink;
                        }
                    }
                }

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRows, Data = lst }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_tag")]
        public HttpResponseMessage SaveDirectTag(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagId = Utility.ConvertToInt(body.Get("TagId"), 0);
                var tagName = body.Get("TagName")?? string.Empty;
                var tagLink = body.Get("TagLink")?? string.Empty;
                var quoteFormat = body.Get("QuoteFormat")??string.Empty;
                var tagType = Utility.ConvertToInt(body.Get("TagType"), 0);
                
                if (tagId > 0)
                {
                    var result = new DirectTagServices().UpdateDirectTag(tagId, tagName, tagLink, quoteFormat, tagType);
                    result.Data = tagId.ToString();
                    if (result.Success)
                    {
                        var jsonKey = "{\"Id\":" + tagId + "}";
                        dynamic obj = new System.Dynamic.ExpandoObject();
                        obj.Id = tagId;
                        obj.TagName = tagName;
                        obj.TagLink = tagLink;
                        obj.QuoteFormat = quoteFormat;
                        obj.Type = tagType;
                        var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strPram, "updatedirecttag", string.Empty, jsonKey);
                    }
                    return this.ToJson(result);
                }
                else {                    
                    var result = new DirectTagServices().InsertDirectTag(ref tagId, tagName, tagLink, quoteFormat, tagType);
                    result.Data = tagId.ToString();
                    if (result.Success)
                    {
                        var jsonKey = "{\"Id\":" + tagId + "}";
                        dynamic obj = new System.Dynamic.ExpandoObject();
                        obj.Id = tagId;
                        obj.TagName = tagName;
                        obj.TagLink = tagLink;
                        obj.QuoteFormat = quoteFormat;
                        obj.Type = tagType;
                        var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                        ContentDeliveryServices.PushToDataCD(strPram, "insertdirecttag", string.Empty, jsonKey);
                    }
                    return this.ToJson(result);
                }
                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_tag")]
        public HttpResponseMessage DirectTagDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagIds = body.Get("Ids")??string.Empty;
                if (string.IsNullOrEmpty(tagIds))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param Ids không được để trống."));
                }

                var result = new DirectTagServices().DeleteDirectTag(tagIds);
                if (result.Success)
                {
                    var lstObjId = new List<dynamic>();
                    string[] arr = tagIds.Split(',');
                    dynamic obj = null;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        obj = new System.Dynamic.ExpandoObject();
                        obj.Id = arr[i];
                        lstObjId.Add(obj);
                    }
                    var jsonKey = NewtonJson.Serialize(lstObjId);
                    var strPram = NewtonJson.Serialize(lstObjId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "deletedirecttag", string.Empty, jsonKey);
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_tagnews")]
        public HttpResponseMessage DeleteTagsNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var account = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var tagId = Utility.ConvertToInt(body.Get("TagId"), 0);
                var newsId = Utility.ConvertToLong(body.Get("NewsId"));
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                
                var result = new DirectTagServices().DeleteDirectTagNewsFromNews(tagId, newsId, type, account);
                if (result.Success)
                {
                    var jsonKey = "{\"TagId\":" + tagId + ",\"NewsId\":" + newsId + ",\"Type\":" + type + "}";
                    dynamic obj = new System.Dynamic.ExpandoObject();
                    obj.TagId = tagId;
                    obj.NewsId = newsId;
                    obj.Type = type;
                    var strPram = NewtonJson.Serialize(obj, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "deletedirecttagnews", string.Empty, jsonKey);
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("update_tagsnews")]
        public HttpResponseMessage InsertTagsNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var account = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var tagIds = body.Get("TagIds")?? string.Empty;
                var newsIds = body.Get("NewsIds")?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("Type"), 0);

                WcfActionResponse result = null;
                if (type > 0)
                    result = new DirectTagServices().InsertDirectTagNewsMultiByType(tagIds, newsIds, type, account);
                else
                    result = new DirectTagServices().InsertDirectTagNewsMulti(tagIds, newsIds, account);
                if (result.Success)
                {
                    string[] arrTag = tagIds.Split(',');
                    string[] arrNews = newsIds.Split(',');
                    var lstObjId = new List<dynamic>();

                    var tmpTagId = string.Empty;
                    var tmpNewsId = string.Empty;
                    dynamic obj = null;
                    for (int i = 0; i < arrTag.Length; i++)
                    {
                        tmpTagId = arrTag[i];
                        for (int j = 0; j < arrNews.Length; j++)
                        {
                            tmpNewsId = arrNews[j];
                            obj = new System.Dynamic.ExpandoObject();
                            obj.TagId = tmpTagId;
                            obj.NewsId = tmpNewsId;
                            obj.Type = type;
                            lstObjId.Add(obj);
                        }
                    }

                    var jsonKey = NewtonJson.Serialize(lstObjId);
                    var strPram = NewtonJson.Serialize(lstObjId, "yyyy-MM-dd'T'HH:mm:ss");
                    ContentDeliveryServices.PushToDataCD(strPram, "insertdirecttagnews", string.Empty, jsonKey);
                }

                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("is_available_tag")]
        public HttpResponseMessage IsTagAvailable(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var tagName = body.Get("TagName")?? string.Empty;                
                var result = new DirectTagServices().IsDirectTagAvailable(tagName);
                if (result==null)
                {
                    result = new DirectTagEntity();
                }
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
