﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.Kenh14.Entity;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/boxbottomembed")]
    public class BoxBottomEmbedController : ApiController
    {
        #region BoxBottomEmbed (K14Embed)

        [HttpPost, Route("search")]
        public HttpResponseMessage Search(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                                
                var data = new Kenh14Services().ListBoxBottomEmbedSearch(keyword);

                return this.ToJson(WcfActionResponse<List<BoxBottomEmbedEntity>>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_by_id")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var data = new Kenh14Services().BoxBottomEmbedGetById(id);
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy embed"));
                }

                return this.ToJson(WcfActionResponse<BoxBottomEmbedEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save")]
        public HttpResponseMessage Save(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var name = body.Get("Name") ?? string.Empty;                
                var zoneId = body.Get("ZoneId") ?? string.Empty;
                var code = body.Get("Code") ?? string.Empty;
                var isActive = Utility.ConvertToBoolean(body.Get("IsActive"), false);
                var endDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("EndDate"), "dd/MM/yyyy hh:mm");

                var entity = new BoxBottomEmbedEntity
                {
                    Id = id,
                    CategoryId = zoneId,
                    Status = 1,
                    Name = name,
                    IsActive = isActive,
                    EndDate = endDate,
                    Code = code
                };


                if (id > 0)
                {
                    var result = new Kenh14Services().BoxBottomEmbedUpdate(entity);
                    return this.ToJson(result);
                }
                else {
                    var refId = 0;
                    var result = new Kenh14Services().BoxBottomEmbedInsert(entity, ref refId);
                    result.Data = refId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var result = new Kenh14Services().BoxBottomEmbedDelete(id);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
