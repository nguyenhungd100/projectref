﻿using ChannelVN.Magazine.Entity;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/magazine")]
    public class MagazineController : ApiController
    {
        #region Magazine

        [HttpPost, Route("search_magazine")]
        public HttpResponseMessage SearchMagazine(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var type = Utility.ConvertToInt(body.Get("Type"),0);
                var status = EnumMagazineStatus.AllStatus;
                var statusInput = Utility.ConvertToInt(body.Get("Status"),-1);
                status = (EnumMagazineStatus)statusInput;

                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new MagazineServices().MagazineSearch(keyword, type, status, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_magazine_by_id")]
        public HttpResponseMessage GetMagazineById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var magazine = new MagazineServices().GetMagazineById(id);
                if (magazine == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Magazine is empty."));
                }

                return this.ToJson(WcfActionResponse<MagazineEntity>.CreateSuccessResponse(magazine, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_magazine")]
        public HttpResponseMessage SaveMagazine(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var title = body.Get("Title") ?? string.Empty;
                var url = body.Get("Url") ?? string.Empty;
                var filePath = body.Get("FilePath") ?? string.Empty;
                var publishedDate = Utility.ConvertToDateTimeByFormatAndDateNow(body.Get("PublishedDate"),"dd/MM/yyyy HH:mm");
                var type = Utility.ConvertToInt(body.Get("Type"), 1);
                var status = Utility.ConvertToInt(body.Get("Status"),1);
                var avatar = body.Get("Avatar") ?? string.Empty;

                var magazineEntity = new MagazineEntity
                {
                    Id = id,
                    Title = title,
                    Url = url,
                    FilePath=filePath,
                    PublishedDate = publishedDate,                    
                    Type = type,
                    Status = status,
                    Avatar=avatar,
                    CreatedBy=accountName,
                    CreatedDate=DateTime.Now,
                    LastModifiedBy= accountName,
                    LastModifiedDate=DateTime.Now
                };


                if (id > 0)
                {                    
                    var result = new MagazineServices().UpdateMagazine(magazineEntity);
                    return this.ToJson(result);
                }
                else {
                    var newsMagazineId = 0;
                    var result = new MagazineServices().InsertMagazine(magazineEntity, ref newsMagazineId);
                    result.Data = newsMagazineId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("magazine_delete")]
        public HttpResponseMessage MagazineDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var magazineId = Utility.ConvertToInt(body.Get("Id"));

                var result = new MagazineServices().DeleteMagazine(magazineId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
