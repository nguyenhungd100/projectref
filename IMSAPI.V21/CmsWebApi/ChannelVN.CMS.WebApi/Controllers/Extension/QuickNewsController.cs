﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.Entity.Base.News;
using ChannelVN.CMS.Entity.Base.Security;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.CMS.WebApi.Services.ExternalServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using ChannelVN.CMS.Entity.Base.Photo;
using System.Text.RegularExpressions;
using ChannelVN.CMS.Action;
using ChannelVN.CMS.Entity.Base.Video;
using ChannelVN.ExternalCms.Entity;
using System.IO;
using ChannelVN.CMS.WebApi.Helper;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/quicknews")]
    public class QuickNewsController : ApiController
    {
        #region QuickNews

        [HttpPost, Route("search")]
        public HttpResponseMessage SearchQuickNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new QuickNewsServices().ExternalSearchQuickNews(keyword, type, status, pageIndex, pageSize, ref totalRow);
                
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { Total = totalRow, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_quicknews_history")]
        public HttpResponseMessage GetQuickNewsHistory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var quickNews = new QuickNewsServices().ExternalGetQuickNewsByQuickNewsId(id);

                return this.ToJson(WcfActionResponse<QuickNewsEntity>.CreateSuccessResponse(quickNews, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_quicknews_content")]
        public HttpResponseMessage GetQuickNewsContent(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var quickNews = new QuickNewsServices().ExternalGetQuickNewsByQuickNewsId(id);

                return this.ToJson(WcfActionResponse<QuickNewsEntity>.CreateSuccessResponse(quickNews, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("preview_quicknews")]
        public HttpResponseMessage ExternalGetQuickNewsDetailByQuickNewsId(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToLong(body.Get("Id"));
                var quickNews = new QuickNewsServices().ExternalGetQuickNewsDetailByQuickNewsId(id);

                return this.ToJson(WcfActionResponse<QuickNewsDetailEntity>.CreateSuccessResponse(quickNews, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("receive_quicknews")]
        public HttpResponseMessage ReceiveQuickNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var processedContent = body.Get("ProcessedContent") ?? string.Empty;
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var cmsQuickNews = new QuickNewsServices().GetQuickNewsDetailByQuickNewsId(id);
                if (cmsQuickNews != null)
                {
                    this.ToJson(new WcfActionResponse(){ Data = cmsQuickNews.News_ID.ToString(), Success = true });                    
                }
                var quickNewsInfo = new QuickNewsServices().ExternalGetQuickNewsDetailByQuickNewsId(id);

                if (quickNewsInfo == null)
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy tin."));                

                var quickNews = quickNewsInfo.QuickNews;
                if (quickNews.CatId <= 0)
                {
                    var zone = new NewsServices().GetAllZoneByUserAndListPermissionWithTreeView(username, (int)EnumPermission.ArticleAdmin, (int)EnumPermission.ArticleEditor, (int)EnumPermission.ArticleReporter);
                    if (zone.Count > 0)
                    {
                        quickNews.CatId = zone[0].Id;
                    }
                }

                var quickNewsAuthor = quickNewsInfo.QuickNewsAuthor;
                var quickNewsImages = quickNewsInfo.QuickNewsImages.Where(it => it.Type == 0);
                var quickNewsVideos = quickNewsInfo.QuickNewsImages.Where(it => it.Type == 1);
                
                var imageHost = CmsChannelConfiguration.GetAppSetting(Services.ExternalServices.FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD);
                if (!imageHost.EndsWith("/")) imageHost += "/";
                var maxWidth = CmsChannelConfiguration.GetAppSettingInInt32(Constants.IMAGE_MAX_WIDTH);
                var keyThumb = CmsChannelConfiguration.GetAppSetting(Constants.KeyThumbWidth);
                const string imageFormat =
                    "<div class=\"VCSortableInPreviewMode\" style=\"display:inline-block;width:100%; text-align:center;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;\" type=\"Photo\"><div><img alt=\"\" id=\"img_{0}\" photoid=\"{0}\" rel=\"{1}\" src=\"{2}\" style=\"max-width:100%;\" title=\"{3}\" type=\"photo\" /></div><div class=\"PhotoCMS_Caption\">{3}</div></div><p>&nbsp;</p>";
                const string videoFormat =
                                "<div class=\"VCSortableInPreviewMode\" style=\"display:inline-block;width:100%; text-align:center;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;\" type=\"Video\"><div id=\"videoid_{0}\" videoid=\"{0}\">{1}</div><div class=\"VideoCMS_Caption\">{2}</div></div><p>&nbsp;</p>";

                if (processedContent != "")
                {
                    quickNews.Content = processedContent;
                }

                var avatar = "";
                var avatarDesc = "";
                var bodyContent = quickNews.Content;
                if (!string.IsNullOrEmpty(quickNews.Link))
                {
                    bodyContent += string.Format("<p>Link: <a href=\"{0}\">{0}</a></p>", quickNews.Link);
                }

                var photoPublishedIds = ProcessImagePhotoPublishedIds;
                var photoPublishedTitles = "";

                var videoPublishedIds = "";
                var videoPublishedTitles = "";

                //Ảnh trong nội dung
                bodyContent = ProcessImageOnContent(bodyContent, quickNews.CatId, username);


                //Ảnh trong QuickNewsImages
                int i = 0;
                foreach (var quicknewsImage in quickNewsImages)
                {
                    #region Process photo data

                    var photoInfo = Services.ExternalServices.FileStorageServices.GetFileInfo(quicknewsImage.Image);
                    if (photoInfo != null)
                    {
                        i++;
                        var photo = new PhotoEntity
                        {
                            ZoneId = quickNews.CatId,
                            Name = string.IsNullOrEmpty(quicknewsImage.Description) ? quickNews.Title + " ảnh " + i : quicknewsImage.Description,
                            CreatedBy = username,
                            Status = 1,
                            Capacity = 0,
                            ImageNote = string.IsNullOrEmpty(quicknewsImage.Description) ? quickNews.Title + " ảnh " + i : quicknewsImage.Description,
                            ImageUrl = quicknewsImage.Image.Replace(imageHost, ""),
                            Note = string.IsNullOrEmpty(quicknewsImage.Description) ? quickNews.Title + " ảnh " + i : quicknewsImage.Description,
                            PhotoLabelId = 0,
                            Size = photoInfo.width + "x" + photoInfo.height
                        };
                        var responseData = new MediaServices().InsertPhoto(photo, "");
                        if (responseData.Success)
                        {
                            var photoPublishId = 0L;
                            var photoPublish = new MediaServices().PublishPhoto(responseData.Data, 0, 0, quickNews.CatId, username);
                            if (photoPublish != null && photoPublish.Count > 0)
                            {
                                photoPublishId = photoPublish[0].Id;
                                var url = imageHost;
                                var relUrl = imageHost;
                                var tempImageUrl = quicknewsImage.Image;
                                if (tempImageUrl.StartsWith("/")) tempImageUrl = tempImageUrl.Remove(0, 1);
                                if (photoInfo.width > maxWidth && !quicknewsImage.IsAvatar)
                                {
                                    //Nếu là avatar thì không thumb!
                                    url += keyThumb + "/" + maxWidth + "/";
                                }
                                url += tempImageUrl;
                                relUrl += "/" + tempImageUrl.Replace("///", "/").Replace("//", "/");
                                url = url.Replace("///", "/");
                                if (!quicknewsImage.IsAvatar)
                                {
                                    bodyContent += string.Format(imageFormat, photoPublishId, relUrl, url, string.IsNullOrEmpty(quicknewsImage.Description) ? quickNews.Title + " ảnh " + i : quicknewsImage.Description);
                                }
                                quicknewsImage.Image = url;
                                if (quicknewsImage.IsAvatar)
                                {
                                    avatar = quicknewsImage.Image;
                                    if (avatar.StartsWith("http://"))
                                    {
                                        avatar = avatar.Remove(0, "http://".Length);
                                        avatar = avatar.Substring(avatar.IndexOf("/") + 1);
                                    }
                                    avatarDesc = quickNews.Title;
                                }

                                photoPublishedIds += ";" + photoPublishId;
                                photoPublishedTitles += ";" + (string.IsNullOrEmpty(quicknewsImage.Description) ? quickNews.Title + " ảnh " + i : quicknewsImage.Description);
                            }
                        }
                    }

                    #endregion
                }

                //Video trong QuickNewsImages
                int j = 0;
                foreach (var quickNewsVideo in quickNewsVideos)
                {
                    if (quickNewsVideo.Image.StartsWith("/"))
                    {
                        quickNewsVideo.Image = quickNewsVideo.Image.Substring(1);
                    }
                    #region Process video data

                    var info = VideoStorageServices.GetFileInfo(quickNewsVideo.Image);
                    string resultEmbed = "";
                    if (info != null)
                    {

                        resultEmbed = VideoStorageServices.GetEmbed(quickNewsVideo.Image, username);
                        if (string.IsNullOrEmpty(resultEmbed) || resultEmbed == "\"-1\"" || resultEmbed == "\"-2\"")
                        {
                            resultEmbed = string.Empty;
                        }
                        else
                        {
                            resultEmbed = Regex.Replace(resultEmbed, "\\\\", "");
                            resultEmbed = resultEmbed.Remove(0, 1);
                            resultEmbed = resultEmbed.Remove(resultEmbed.Length - 1, 1);
                        }
                        //embed
                        var keyVideo = "";
                        var reg = new Regex("[\\?]key=([^\\&]*)");
                        var m = reg.Matches(resultEmbed);
                        if (m.Count > 0)
                        {
                            keyVideo = m[1].Value;
                        }
                        keyVideo = keyVideo.Replace("?key=", "");

                        //thumb
                        var thumbVideo = "";
                        Regex reg2 = new Regex("[\\&;]img=([^\\\"]*)");
                        var m2 = reg2.Matches(resultEmbed);
                        if (m2.Count > 0)
                        {
                            thumbVideo = m2[1].Value;
                        }
                        thumbVideo = thumbVideo.Replace("img=", "").Replace("&img=", "").Replace("&http", "");
                        j++;
                        var video = new Entity.Base.Video.VideoEntity
                        {
                            ZoneId = quickNews.CatId,
                            Name = quickNews.Title + " video " + j,
                            CreatedBy = username,
                            Status = (int)Entity.Base.Video.EnumVideoStatus.WaitForPublish,
                            Capacity = 0,
                            Description = quickNews.Title + " video " + j,
                            Url = imageHost + "/" + quickNewsVideo.Image,
                            Size = info.size,
                            HtmlCode = resultEmbed,
                            CreatedDate = DateTime.Now,
                            FileName = quickNewsVideo.Image,
                            KeyVideo = keyVideo,
                            Avatar = thumbVideo
                        };
                        var responseData = new MediaServices().SaveVideo(video, "", "", "");
                        if (responseData.Success)
                        {
                            var videoId = Utility.ConvertToInt(responseData.Data);

                            var publishId = videoId;
                            var url = imageHost;
                            var relUrl = CmsChannelConfiguration.GetAppSetting(Services.ExternalServices.FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD);
                            url += "/" + quickNewsVideo.Image;
                            relUrl += "/" + quickNewsVideo.Image.Replace("///", "/");
                            url = url.Replace("///", "/");
                            bodyContent += string.Format(videoFormat, publishId, resultEmbed, quickNews.Title + " video " + j);
                            quickNewsVideo.Image = url;
                            videoPublishedIds += ";" + publishId;
                            videoPublishedTitles += ";" + quickNews.Title + " video " + j;

                        }
                    }

                    #endregion
                }

                var authorEmail = "";
                if (quickNewsAuthor != null && CmsChannelConfiguration.GetAppSettingInInt32(Constants.QUICKNEWS_SHOW_AUTHOR_INFO_IN_BODY) == 1)
                {
                    bodyContent += string.Format("<p>Họ tên: {0}</p>", quickNewsAuthor.FullName);
                    bodyContent += string.Format("<p>Số điện thoại: {0}</p>", quickNewsAuthor.Phone);
                    bodyContent += string.Format("<p>Email: {0}</p>", quickNewsAuthor.Email);
                    bodyContent += string.Format("<p>Địa chỉ: {0}</p>", quickNewsAuthor.Address);
                    bodyContent += string.Format("<p>Nghề nghiệp: {0}</p>", quickNewsAuthor.Job);
                    bodyContent += string.Format("<p>Sologan: {0}</p>", quickNewsAuthor.Slogan);
                    bodyContent += string.Format("<p>Ghi chú: {0}</p>", quickNews.Note);
                    if (!string.IsNullOrEmpty(quickNewsAuthor.Email))
                    {
                        authorEmail = "Email: " + quickNewsAuthor.Email;
                    }
                }
                var news = new NewsEntity()
                {
                    Avatar = avatar,
                    AvatarDesc = avatarDesc,
                    Title = quickNews.Title,
                    Sapo = quickNews.Sapo,
                    Body = bodyContent,
                    CreatedBy = username,
                    CreatedDate = DateTime.Now,
                    LastModifiedBy = username,
                    WordCount = Utility.CountWords(bodyContent),
                    Source = quickNews.Email,
                    IsFocus = false,
                    Type = (int)NewsType.EXT,
                    NewsType = (int)NewsType.EXT,
                    Note = quickNews.Type == 1 ? "Bài gửi nhanh. " : "Bài độc giả viết. " + authorEmail,
                    QuickNewsVietId = quickNews.VietId,
                    PenName = quickNews.PenName,
                    Author = quickNews.PenName,
                    OriginalId = 7
                };
                var response = new NewsServices().InsertNews(news, quickNews.CatId, "", "", "", "", username, new List<string>().ToArray(), 0, 0, new List<NewsExtensionEntity>(),"baidocgia");
                if (response.Success)
                {
                    var newsId = Utility.ConvertToLong(response.Data);
                    if (newsId > 0)
                    {
                        quickNews.News_ID = newsId;
                        //Chèn vào các bảng
                        var quickNewsInsertData = new QuickNewsServices().InsertQuickNews(quickNews);
                        if (quickNewsInsertData.Success)
                        {
                            new QuickNewsServices().ExternalReceiveQuickNews(id, newsId, username);
                            if (quickNewsAuthor != null)
                            {
                                new QuickNewsServices().InsertQuickNewsAuthor(quickNewsAuthor);
                            }
                            foreach (var image in quickNewsImages)
                            {
                                new QuickNewsServices().InsertQuickNewsImage(image);
                            }
                        }

                        if (!string.IsNullOrEmpty(photoPublishedIds))
                        {
                            photoPublishedIds = photoPublishedIds.Remove(0, 1);
                            photoPublishedTitles = photoPublishedTitles.Remove(0, 1);
                            new NewsServices().UpdatePhotoPublishedInNews(newsId, photoPublishedIds, photoPublishedTitles);
                        }
                        if (!string.IsNullOrEmpty(videoPublishedIds))
                        {
                            videoPublishedIds = videoPublishedIds.Remove(0, 1);
                            videoPublishedTitles = videoPublishedTitles.Remove(0, 1);
                            new NewsServices().UpdateVideoInNews(newsId, photoPublishedIds, photoPublishedTitles);
                        }
                    }
                }

                return this.ToJson(response);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #region option
        private int ProcessImageZoneId = 0;
        private string ProcessImageCreatedBy = "";
        private string ProcessImagePhotoPublishedIds = "";
        private string ProcessImageOnContent(string content, int zoneId, string createdBy)
        {
            var regex = new Regex(@"<img[^>]+src\s*=\s*([""'])((?:(?!\1).)*)\1[^>]*>", RegexOptions.Singleline | RegexOptions.Compiled | RegexOptions.IgnoreCase);
            ProcessImageZoneId = zoneId;
            ProcessImageCreatedBy = createdBy;
            return regex.Replace(content, new MatchEvaluator(ReplaceImageSrc));
        }
        private string ReplaceImageSrc(System.Text.RegularExpressions.Match match)
        {
            string src = "";
            if (match.Groups.Count >= 2)
            {
                src = match.Groups[2].Value;
            }
            else
            {
                src = match.Value;
            }
            return ProcessImage(src, ProcessImageZoneId, ProcessImageCreatedBy);
        }
        private string ProcessImage(string imageUrl, int zoneId, string createdBy)
        {
            Logger.WriteLog(Logger.LogType.Debug, "start " + imageUrl);
            var imageHost = CmsChannelConfiguration.GetAppSetting(Services.ExternalServices.FileStorageServices.FILE_MANAGER_HTTPDOWNLOAD);
            if (!imageHost.EndsWith("/")) imageHost += "/";
            var maxWidth = CmsChannelConfiguration.GetAppSettingInInt32(Constants.IMAGE_MAX_WIDTH);
            var keyThumb = CmsChannelConfiguration.GetAppSetting(Constants.KeyThumbWidth);
            var isReuploadPhoto = CmsChannelConfiguration.GetAppSettingInInt32(Constants.QUICKNEWS_IS_RE_UPLOAD_PHOTO_ON_RECEIVE) == 1;

            string photoPublishedIds = "";
            const string imageFormat =
               "<div class=\"VCSortableInPreviewMode\" style=\"display:inline-block;width:100%; text-align:center;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;\" type=\"Photo\"><div><img alt=\"\" id=\"img_{0}\" photoid=\"{0}\" rel=\"{1}\" src=\"{2}\" style=\"max-width:100%;\" title=\"{3}\" type=\"photo\" /></div><div class=\"PhotoCMS_Caption\">{3}</div></div><p>&nbsp;</p>";

            if (imageUrl.StartsWith("http"))
            {
                if (!isReuploadPhoto)
                {
                    Uri myUri = new Uri(imageUrl);
                    imageUrl = imageUrl.Replace("http://" + myUri.Host, "");
                }
                else
                {
                    imageUrl = UploadRemoteImage(imageUrl.Replace(" ", "%20"));
                }
            }
            Logger.WriteLog(Logger.LogType.Debug, "end " + imageUrl);
            var photoInfo = Services.ExternalServices.FileStorageServices.GetFileInfo(imageUrl);
            var returnhtml = "";
            if (photoInfo != null)
            {

                var photo = new PhotoEntity
                {
                    ZoneId = zoneId,
                    Name = "",
                    CreatedBy = createdBy,
                    Status = 1,
                    Capacity = 0,
                    ImageNote = "",
                    ImageUrl = imageUrl.Replace(imageHost, ""),
                    Note = "",
                    PhotoLabelId = 0,
                    Size = photoInfo.width + "x" + photoInfo.height
                };
                var responseData = new MediaServices().InsertPhoto(photo, "");
                if (responseData.Success)
                {
                    var photoPublishId = 0L;
                    var photoPublish = new MediaServices().PublishPhoto(responseData.Data.ToString(), 0, 0, zoneId,
                                                                  createdBy);
                    if (photoPublish != null && photoPublish.Count > 0)
                    {
                        photoPublishId = photoPublish[0].Id;
                        var url = imageHost;
                        var relUrl = imageHost;
                        var tempImageUrl = imageUrl;
                        if (tempImageUrl.StartsWith("/")) tempImageUrl = tempImageUrl.Remove(0, 1);
                        if (photoInfo.width > maxWidth)
                        {
                            url += keyThumb + "/" + maxWidth + "/";
                        }
                        url += tempImageUrl;
                        relUrl += "/" + tempImageUrl.Replace("///", "/");
                        url = url.Replace("///", "/")
                                 .Replace(keyThumb + "/" + maxWidth + "//", keyThumb + "/" + maxWidth + "/");
                        returnhtml = string.Format(imageFormat, photoPublishId, relUrl, url, "");

                        photoPublishedIds += ";" + photoPublishId;
                    }
                }
            }
            ProcessImagePhotoPublishedIds = photoPublishedIds;
            return returnhtml;
        }
        private const string FormatPathTime = "{0:yyyy}";
        private string UploadRemoteImage(string imageUrl)
        {
            #region Get file extension

            var extension = Path.GetExtension(imageUrl).ToLower().Replace(".", "");

            #endregion

            #region Init data for upload image

            var imageFileName = "img" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + extension;

            var path = SetRootFolder() + string.Format(FormatPathTime, DateTime.Now);
            var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
            var policyData = Services.ExternalServices.FileStorageServices.CreatePolicyForUpload(accountName, path, imageFileName, false);
            var policy = policyData[1];
            var signature = policyData[2];
            var remotePath = policyData[3];

            #endregion
            
            if (Services.ExternalServices.FileStorageServices.UploadImage(policy, signature, imageFileName, "image/" + extension, imageUrl))
            {
                return remotePath + "/" + imageFileName;
            }
            else
            {
                return "";
            }
        }
        private string SetRootFolder()
        {
            return "";
        }
        public class QuickNewsCount
        {
            public int Status { get; set; }
            public int Type { get; set; }
            public int Count { get; set; }
        }
        #endregion

        [HttpPost, Route("delete_quicknews")]
        public HttpResponseMessage DeleteQuickNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var msg = body.Get("Msg")??"";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (!string.IsNullOrEmpty(msg))
                {
                    var from = CmsChannelConfiguration.GetAppSetting(Constants.EMAIL_SENDER);

                    var quickNews = new QuickNewsServices().ExternalGetQuickNewsDetailByQuickNewsId(id);
                    var toEmail = quickNews.QuickNews.Email;
                    if (string.IsNullOrEmpty(toEmail))
                    {
                        var author = quickNews.QuickNewsAuthor;
                        if (author != null)
                        {
                            toEmail = author.Email;
                        }
                    }
                    if (!string.IsNullOrEmpty(toEmail))
                    {                        
                        EmailHelper.SendEmail(from, toEmail, "Thông báo về bài cộng tác viên", msg);
                    }
                }
                var data = new QuickNewsServices().ExternalDeleteQuickNews(id, username);
                
                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_multi_quicknews")]
        public HttpResponseMessage DeleteMultiQuickNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var idList = body.Get("Ids") ?? "";
                var username = WcfExtensions.WcfMessageHeader.Current.ClientUsername;
                if (string.IsNullOrEmpty(idList))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param Ids không được để trống."));
                }
                var data = new QuickNewsServices().ExternalDeleteMultiQuickNews(idList,username);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("count_quicknews")]
        public HttpResponseMessage CountQuickNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var quickNewsCount = new List<QuickNewsCount>();
                quickNewsCount.Add(new QuickNewsCount
                {
                    Count = new QuickNewsServices().ExternalCountQuickNews(1, 0),
                    Type = 1,
                    Status = 0
                });
                quickNewsCount.Add(new QuickNewsCount
                {
                    Count = new QuickNewsServices().ExternalCountQuickNews(0, 0),
                    Type = 0,
                    Status = 0
                });
               
                return this.ToJson(WcfActionResponse<List<QuickNewsCount>>.CreateSuccessResponse(quickNewsCount, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
