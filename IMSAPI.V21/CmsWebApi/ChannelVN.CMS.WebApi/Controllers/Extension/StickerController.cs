﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.Kenh14.Entity;
using System;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/sticker")]
    public class StickerController : ApiController
    {
        #region Sticker

        [HttpPost, Route("search_sticker")]
        public HttpResponseMessage SearchSticker(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword");
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 10);
                
                var totalRow = 0;
                var data = new Kenh14Services().SearchSticker(keyword, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_sticker_by_id")]
        public HttpResponseMessage GetStickerById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));

                var sticker = new Kenh14Services().GetStickerByStickerId(id);
                if (sticker == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Không tìm thấy sticker"));
                }

                return this.ToJson(WcfActionResponse<StickerEntity>.CreateSuccessResponse(sticker, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save_sticker")]
        public HttpResponseMessage SaveSticker(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                
                var id = Utility.ConvertToInt(body.Get("Id"), 0);                
                var stickerName = body.Get("StickerName") ?? string.Empty;                
                var avatar = body.Get("Avatar") ?? string.Empty;
               
                var stickerEntity = new StickerEntity
                {
                    Id = id,
                    StickerName = stickerName,
                    Avatar = avatar,
                    Status=1                    
                };


                if (id > 0)
                {                    
                    var result = new Kenh14Services().UpdateSticker(stickerEntity);
                    return this.ToJson(result);
                }
                else {
                    var newsStickerId = 0;
                    var result = new Kenh14Services().InsertSticker(stickerEntity);
                    result.Data = newsStickerId.ToString();
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("sticker_delete")]
        public HttpResponseMessage StickerDelete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var stickerId = Utility.ConvertToInt(body.Get("Id"));

                var result = new Kenh14Services().DeleteById(stickerId);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }        

        #endregion
    }
}
