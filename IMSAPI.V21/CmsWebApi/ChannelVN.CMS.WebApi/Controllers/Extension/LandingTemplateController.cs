﻿using ChannelVN.CMS.Common;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Extension;
using System;
using System.Net.Http;
using System.Web.Http;
using ChannelVN.CMS.Entity.Base.LandingTemplate;
using ChannelVN.CMS.BO.Nodejs;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]    
    [CompressionFilter]
    [RoutePrefix("api/extension/landingtemplate")]
    public class LandingTemplateController : ApiController
    {
        #region LandingTemplate

        [HttpPost, Route("search")]
        public HttpResponseMessage Search(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("Keyword")??string.Empty;
                var cateId= Utility.ConvertToInt(body.Get("CategoryId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), -1);
                var type = Utility.ConvertToInt(body.Get("Type"), -1);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"), 1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"), 50);
                
                var totalRow = 0;
                var data = new LandingTemplateServices().Search(keyword, cateId, status, type, pageIndex, pageSize, ref totalRow);

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("get_by_id")]
        public HttpResponseMessage GetById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new LandingTemplateServices().GetById(id);
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found data"));
                }

                return this.ToJson(WcfActionResponse<LandingTemplateEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ ex.Message));
            }
        }

        [HttpPost, Route("save")]
        public HttpResponseMessage Save(HttpRequestMessage request)
        {
            try {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty 'Name' landing template."));
                }
                var html = body.Get("Html") ?? string.Empty;
                var avatar = body.Get("Avatar") ?? string.Empty;
                var type = Utility.ConvertToInt(body.Get("Type"), 0);
                var cateId = Utility.ConvertToInt(body.Get("CategoryId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 0);//lưu tạm

                var obj = new LandingTemplateEntity
                {
                    Id = id,                    
                    Name = name,
                    Html=html,
                    Avatar = avatar,                    
                    Type = type,
                    CategoryId = cateId,
                    Status = status,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    ModifiedBy = accountName,
                    ModifiedDate = DateTime.Now
                }; 
                               
                if (id > 0)
                {                    
                    var result = new LandingTemplateServices().Update(obj);
                    return this.ToJson(result);
                }
                else {                    
                    var result = new LandingTemplateServices().Create(obj);                    
                    return this.ToJson(result);
                }                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("delete")]
        public HttpResponseMessage Delete(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"),0);

                var result = new LandingTemplateServices().Delete(id);
                return this.ToJson(result);
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("update_status")]
        public HttpResponseMessage UpdateStatus(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"));
                var action = body.Get("Action")??string.Empty;
                if (string.IsNullOrEmpty(action))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Action not empty."));
                }

                var result = new LandingTemplateServices().UpdateStatus(id, action);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion

        #region LandingTemplateCategory

        [HttpPost, Route("get_all_category")]
        public HttpResponseMessage GetAllCategory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                
                var totalRow = 0;
                var data = new LandingTemplateServices().GetAllCategory();

                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRow, Data = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("get_category_by_id")]
        public HttpResponseMessage GetCategoryById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var data = new LandingTemplateServices().GetCategoryById(id);
                if (data == null)
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not found data"));
                }

                return this.ToJson(WcfActionResponse<LandingTemplateCategoryEntity>.CreateSuccessResponse(data, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("save_category")]
        public HttpResponseMessage SaveCategory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;
                var accountName = WcfExtensions.WcfMessageHeader.Current.ClientUsername;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);
                var name = body.Get("Name") ?? string.Empty;
                if (string.IsNullOrEmpty(name))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Not empty 'Name' Category."));
                }
                var description = body.Get("Description") ?? string.Empty;               
                var priority = Utility.ConvertToInt(body.Get("Priority"), 0);
                var parentId = Utility.ConvertToInt(body.Get("ParentId"), 0);
                var status = Utility.ConvertToInt(body.Get("Status"), 1);//active
                var type = Utility.ConvertToInt(body.Get("Type"), 0);

                var obj = new LandingTemplateCategoryEntity
                {
                    Id = id,
                    Name = name,
                    Description = description,
                    Priority = priority,
                    ParentId = parentId,
                    Status = status,
                    CreatedBy = accountName,
                    CreatedDate = DateTime.Now,
                    Type=type
                };

                if (id > 0)
                {
                    var result = new LandingTemplateServices().UpdateCategory(obj);
                    return this.ToJson(result);
                }
                else {
                    var result = new LandingTemplateServices().CreateCategory(obj);
                    return this.ToJson(result);
                }
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("delete_category")]
        public HttpResponseMessage DeleteCategory(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = Utility.ConvertToInt(body.Get("Id"), 0);

                var result = new LandingTemplateServices().DeleteCategory(id);
                return this.ToJson(result);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        #endregion

        #region landing store (node js)

        [HttpPost, Route("categories")]
        public HttpResponseMessage Categories(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;                
                var data = TemplateStoreNodeJsServices.Categories();

                return this.ToJson(data);                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("types")]
        public HttpResponseMessage Types(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var category_id = body.Get("category_id") ?? string.Empty;
                var data = TemplateStoreNodeJsServices.Types(category_id);

                return this.ToJson(data);                
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("elements")]
        public HttpResponseMessage Elements(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var type_id = Utility.ConvertToInt(body.Get("type_id"),-1);
                var category_id = body.Get("category_id") ?? string.Empty;
                var page = Utility.ConvertToInt(body.Get("page"), 1);
                var size = Utility.ConvertToInt(body.Get("size"), 20);
                var format = new string[] {"dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };                
                var from = Utility.ParseDateTime(body.Get("from"),format);
                var to = Utility.ParseDateTime(body.Get("to"), format);

                var data = TemplateStoreNodeJsServices.Elements(keyword, category_id, type_id, from, to, page, size);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_element_by_id")]
        public HttpResponseMessage GetElementById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id") ?? string.Empty;
                if (string.IsNullOrEmpty(id))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param id not empty."));
                }              
                var data = TemplateStoreNodeJsServices.GetElementById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("pages")]
        public HttpResponseMessage Pages(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;
                var type_id = Utility.ConvertToInt(body.Get("type_id"), -1);
                var category_id = body.Get("category_id") ?? string.Empty;
                var page = Utility.ConvertToInt(body.Get("page"), 1);
                var size = Utility.ConvertToInt(body.Get("size"), 20);
                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var from = Utility.ParseDateTime(body.Get("from"), format);
                var to = Utility.ParseDateTime(body.Get("to"), format);

                var data = TemplateStoreNodeJsServices.Pages(keyword, category_id, type_id, from, to, page, size);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_page_by_id")]
        public HttpResponseMessage GetPageById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id") ?? string.Empty;
                if (string.IsNullOrEmpty(id))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param id not empty."));
                }
                var data = TemplateStoreNodeJsServices.GetPageById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        [HttpPost, Route("popups")]
        public HttpResponseMessage Popups(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var keyword = body.Get("keyword") ?? string.Empty;                
                var type_id = Utility.ConvertToInt(body.Get("type_id"), -1);
                var category_id = body.Get("category_id") ?? string.Empty;
                var page = Utility.ConvertToInt(body.Get("page"), 1);
                var size = Utility.ConvertToInt(body.Get("size"), 20);
                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var from = Utility.ParseDateTime(body.Get("from"), format);
                var to = Utility.ParseDateTime(body.Get("to"), format);

                var data = TemplateStoreNodeJsServices.Popups(keyword, category_id, type_id, from, to, page, size);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }
        [HttpPost, Route("get_popup_by_id")]
        public HttpResponseMessage GetPopupById(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var id = body.Get("id") ?? string.Empty;
                if (string.IsNullOrEmpty(id))
                {
                    return this.ToJson(WcfActionResponse.CreateErrorResponse("Param id not empty."));
                }
                var data = TemplateStoreNodeJsServices.GetPopupById(id);

                return this.ToJson(data);
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

        #endregion
    }
}
