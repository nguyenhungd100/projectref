﻿using ChannelVN.CMS.BO.Nodejs;
using ChannelVN.CMS.Common;
using ChannelVN.CMS.Common.ChannelConfig;
using ChannelVN.CMS.WebApi.Extensions;
using ChannelVN.CMS.WebApi.Services.Base;
using ChannelVN.CMS.WebApi.Services.Extension;
using ChannelVN.ForExternalApplication.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ChannelVN.CMS.WebApi.Controllers.Base
{
    [Authorize]
    [CompressionFilter]
    [RoutePrefix("api/extension/royalty")]
    public class RoyaltyController : ApiController
    {
        [HttpPost, Route("export_excel")]
        public HttpResponseMessage SearchRollingNews(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("FromDate"), format);
                var toDate = Utility.ParseDateTime(body.Get("ToDate"), format);
                var pageIndex = Utility.ConvertToInt(body.Get("PageIndex"),1);
                var pageSize = Utility.ConvertToInt(body.Get("PageSize"),50);
                var zoneId = Utility.ConvertToInt(body.Get("ZoneId"), 0);
                var createdBy = body.Get("CreatedBy")??string.Empty;
                var penName = body.Get("PenName")??string.Empty;

                var totalRow = 0;
                var data = NewsRoyaltyNodeJsServices.NewsRoyalty_Search(zoneId, createdBy, penName, fromDate, toDate, pageIndex, pageSize, ref totalRow);

                return this.ToJson(data);
                //return this.ToJson(WcfActionResponse<ResponseNewsRoyalty>.CreateSuccessResponse(data, "Success"));
            }
            catch(Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: "+ex.Message));
            }
        }

        [HttpPost, Route("list_newspublish_for_royalties")]
        public HttpResponseMessage ListNewsPublishForRoyalties(HttpRequestMessage request)
        {
            try
            {
                var body = request.Content.ReadAsFormDataAsync().Result;

                var format = new string[] { "dd/MM/yyyy", "dd/MM/yyyy HH:mm", "dd/MM/yyyy HH:mm:ss" };
                var fromDate = Utility.ParseDateTime(body.Get("from_date"), format);
                var toDate = Utility.ParseDateTime(body.Get("to_date"), format);
                var pageIndex = Utility.ConvertToInt(body.Get("page_index"));
                var pageSize = Utility.ConvertToInt(body.Get("page_size"));

                var totalRows = 0;
                var data = new NewsServices().ListNewsPublishForRoyalties(fromDate, toDate, pageIndex, pageSize, ref totalRows);
                return this.ToJson(WcfActionResponse<dynamic>.CreateSuccessResponse(new { TotalRow = totalRows, News = data }, "Success"));
            }
            catch (Exception ex)
            {
                return this.ToJson(WcfActionResponse.CreateErrorResponse("Error: " + ex.Message));
            }
        }

    }
}
